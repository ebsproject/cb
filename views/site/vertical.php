<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Contains sample page
 */
use yii\helpers\Url;
?>
<div style="float:left;width:15%; margin-top:15px">
  <ul class="tabs tabs-vertical">
    <li class="tab"><a href="<?= Url::to(['/site','tool'=>'cross lists','withNav'=>'t']) ?>" class="a-tab" >Cross lists</a></li>
    <li class="tab"><a href="<?= Url::to(['/site','tool'=>'parent lists','withNav'=>'t', 'vertical'=>'t']) ?>" class="active a-tab">Parent lists</a></li>
    <li class="tab "><a href="#test3" class="a-tab">Tasks</a></li>
    <li class="tab"><a href="#test4" class="a-tab">Seeds</a></li>
    <li class="tab"><a href="#test4" class="a-tab">Breeding values</a></li>
    <li class="tab"><a href="#test4" class="a-tab">Services</a></li>
  </ul>
</div>
<div style="float:left;width:83%;padding-left:1%">
  <?=
  '<h3>'.ucfirst($tool).'</h3>';
  ?>

  <div class="card-panel">
  <p>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
    ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
    ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
    ex ea commodo consequat. <br/>
  <br/>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
    fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
    ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
    ex ea commodo consequat. 
  </p>
  </div>

  <h5 class="header">Sample form</h5>
  <form class="col s12">
    <div class="row">
      <div class="input-field col s12">
        <textarea id="textarea1" class="materialize-textarea"></textarea>
        <label for="textarea1">Textarea</label>
      </div>

      <div class="input-field col s6">
        <textarea id="textarea1" class="materialize-textarea"></textarea>
        <label for="textarea1">Section1</label>
      </div>
      <div class="input-field col s6">
        <textarea id="textarea1" class="materialize-textarea"></textarea>
        <label for="textarea1">Section2</label>
      </div>

      <p>
        <input name="group1" type="radio" id="test1" />
        <label for="test1">Red</label>&emsp;
        <input name="group1" type="radio" id="test2" />
        <label for="test2">Yellow</label>
      </p>

      <p>
        <input type="checkbox" id="test5" />
        <label for="test5">Red</label>&emsp;
        <input type="checkbox" id="test6" checked="checked" />
        <label for="test6">Yellow</label>
      </p>
    </div>
  </form>
</div>
