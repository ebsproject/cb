<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Renders data filters
*/
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\controllers\Dashboard;
use app\models\Program;
use app\models\CropProgram;
use app\models\Season;
use app\models\Stage;
use app\models\GeospatialObject;

$programModel = new Program(new CropProgram());
$filterRaw = \Yii::t('app', 'Data filters');
$filterName = \Yii::t('app', 'Data filters');
$modalAttributes = 'data-target="#save-data-filter-modal" data-toggle="modal" ';
$deleteFilterClass = ' class="hidden"';
$saveBtnClass = '';
$updateBtnClass = 'disabled';
$saveBtnTitle = \Yii::t('app', 'Save data filters');
if(!empty($filtersExist)){
    $filterRaw = $filtersExist['raw'];
    $filterName = $filtersExist['name'];
    $modalAttributes = '';
    $saveBtnClass = 'readonly';
    $saveBtnTitle = \Yii::t('app', 'There is already an existing data filter.');
    $deleteFilterClass = '';
    $updateBtnClass = '';
}

$filters = (array) $filters;
extract($filters);
// current role dashboard filter

$ownedExperimentsSwitch = (isset($owned) && ($owned=='true')) ? 'checked' : '';
?>

<ul id="data-filters" class="side-nav" style="padding-bottom: 300px;">
    <li class="header">
        <a class="subheader" style="display: inline-block;width: 155px;white-space: nowrap;" title="<?= $filterRaw ?>"><i class="material-icons">filter_alt</i><?= $filterName ?></a>
         <a href="#" class="waves-effect waves-light btn btn-flat filter-more-actions dropdown-filter-button <?= $updateBtnClass ?>"" style="top:10px;float:right;right:15px;background-color: #f5f5f5 !important;padding:1px 0px !important;" title="<?= \Yii::t('app', 'More actions') ?>" data-activates="filter-btn-actions"  data-constrainwidth="false" data-beloworigin="true"><i class="material-icons" style="color:#616161 !important;height: 30px;line-height: 30px;margin: 0;">keyboard_arrow_down</i></a>

        <a href="#" class="waves-effect waves-light btn grey-text text-darken-2 btn-flat save-data-filter-btn <?= $saveBtnClass ?>" <?= $modalAttributes ?> style="background-color: #f5f5f5 !important;top:10px;float:right;right:17px;max-width: 60px;" title="<?= $saveBtnTitle ?>"><?= \Yii::t('app', 'Save') ?></a>
    </li>

    <div style="margin: 15px 0px 10px 15px;">
        <a href="#!" class="clear-dashboard-filters bold" title="<?= \Yii::t('app', 'Clear all selected values') ?>" ><?= \Yii::t('app', 'Clear all') ?></a>
    </div>

    <!-- Start render dashboard filters -->
    <?php
        $fields = [
            'program',
            'year',
            'season',
            'stage',
            'site'
        ];

        foreach ($fields as $key => $value) {

            $required = ($value == 'program') ? ' <span class="required">*</span>' : '';
            echo '<li><label class="control-label">'.\Yii::t('app', ucfirst($value)). $required .' </label>';

            $multiple = ['multiple' => true, 'tokenSeparators' => [',', ' ']];
            $options = [
                'placeholder' => 'Select ' . $value,
                'tags' => true,
                'class' => 'filter-dashboard-select'
            ];

            if($value == 'program'){
                
                echo Select2::widget([
                    'name' => 'dashboard_data_filter_'.$value,
                    'id' => $value.'-dashboard_select',
                    'maintainOrder' => true,
                    'options' => $options
                ]).'</li>'; 
            
            }else{

                $options = [
                    'placeholder' => 'Select ' . $value,
                    'tags' => true,
                    'class' => 'filter-dashboard-select',
                    'data-attr' => $value,
                    'minimumResultsForSearch' => 1,
                    'multiple' => true
                ];

                $dataUrl = Url::home() . '/occurrence/' . Url::to('default/get-filter-data');
                
                // select2 plugin options
                $pluginOptions = [
                    'allowClear' => true,
                    'minimumInputLength' => 0,
                    'placeholder' => 'Select ' . $value,
                    'minimumResultsForSearch' => 1,
                    'language' => [
                        'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
                    ], 
                    'ajax' => [
                        'url' => $dataUrl,
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression(
                            'function (params){
                                var attr = $(this).attr("data-attr");
                                return {
                                    q: params.term, 
                                    attr: attr,
                                    page: params.page,
                                    programId: '.$program_id.'
                                }
                            }'
                        ),
                        'processResults' => new JSExpression(
                            'function (data, params) {

                                params.page = params.page || 1;

                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 5) < data.totalCount
                                    },
                                };
                            }'
                        ),
                        'cache' => true
                    ]
                ];

                $field = $value;
                if(in_array($value, ['stage', 'site', 'season']))
                    $field = $value . '_id';

                $fieldVal = (isset($$field) && !empty($$field)) ? ($$field) : [];

                $initValueText = [];
                // load saved values
                if(!empty($occurrenceTextFilters)){
                    foreach ($occurrenceTextFilters as $k => $v) {
                        $fieldObj = explode('-', $k);
                        $fld = $fieldObj[0];

                        if($fld == $value){
                            $initValueText = $occurrenceTextFilters[$k];
                        }
                    }
                }

                if(count($initValueText) != count($fieldVal)){
                    $fieldVal = $initValueText = [];
                }

                echo Select2::widget([
                    'name' => 'dashboard_data_filter_'.$value,
                    'id' => $value . '-dashboard_filter',
                    'maintainOrder' => true,
                    'options' => $options,
                    'pluginOptions' => $pluginOptions,
                    'value' => $fieldVal,
                    'initValueText' => $initValueText,
                    'showToggleAll' => false
                ]).
                '</li>';

            }

        }
    ?>
    <!-- End render dashboard filters -->

    <!-- Switch to show owned studies only -->
    <li>
        <div class="switch">
            <label class="pull-right">
              Show all
              <input type="checkbox" id="data_filter_show_owned_experiments" <?= $ownedExperimentsSwitch ?>>
              <span class="lever" style="margin: 10px 7px;"></span>
              My experiments
            </label>
          </div>
    </li>

    <li class="header" style="margin-top: 10px">
        <div class="clearfix"></div>
        <button class="btn pull-right waves-effect waves-light" id="apply-dashboard-filter" style="margin-right:20px;max-width: 105px;"><?= \Yii::t('app', 'Apply') ?></button>
        <button class="btn pull-right waves-effect waves-light grey cancel-left-filter black-text lighten-2" style="margin-right:5px;max-width: 105px;"><?= \Yii::t('app', 'Cancel') ?></button>
    </li>
</ul>

<!-- filter actions -->
<ul id="filter-btn-actions" class="dropdown-content" style="width: 120px !important">
    <li class="tooltipped hidden" data-tooltip="<?= \Yii::t('app', 'Use existing saved filter') ?>" data-target="#use-saved-filter-modal" data-toggle="modal"><a href="#!"><?= \Yii::t('app', 'Use saved filter') ?></a></li>
    <li class="tooltipped" data-tooltip="<?= \Yii::t('app', 'Delete this filter.') .' '.\Yii::t('app', 'This action cannot be undone.') ?>" <?= $deleteFilterClass ?>><a href="#!" class="delete-filter-btn"><?= \Yii::t('app', 'Delete filter') ?></a></li>

    <li class="tooltipped" data-tooltip="<?= \Yii::t('app', 'Update this filter') ?>" data-target="#update-data-filter-modal" data-toggle="modal">
        <a href="#!" class="update-filter-btn"><?= \Yii::t('app', 'Update filter') ?></a>
    </li>
</ul>

<!-- save filter modal -->
<div id="save-data-filter-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4>Save filter<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
        </div>
        <div class="modal-body save-filter-modal-body">

            <div class="input-field form-group col-md-6 field-filter-name required">
                <label class="control-label" for="season-abbrev">Filter name</label>
                <input type="text" id="filter-name-input" class="form-control" title="Enter name for this filter" autofocus aria-required="true">
                <p class="help-block help-block-error">Enter name for this filter</p>
            </div>

            <div class="col-md-6">
                <div style="margin: 22px 10px 10px 10px">
                    <table class="table-dashboard-filters">
                        <?php

                            $dashboardFiltersTableStr = '';
                            //displays current filters
                            if(isset($program_id) && !empty($program_id)){
                                $program = $programModel->getProgram($program_id);
                                $programName = $program['programName'];
                                $dashboardFiltersTableStr .= '<tr><th class="filter-header">Program</th><td>'.
                                    '<span class="new badge green darken-2">'.$programName.'</span>'
                                .'</td></tr>';
                            }
                            if(isset($year) && !empty($year)){
                                $dashboardFiltersTableStr .= '<tr><th class="filter-header">Year</th><td>';
                                foreach ($year as $key => $value) {
                                    $dashboardFiltersTableStr .= '<span class="new badge cyan darken-1">'.$value.'</span>';
                                }
                                $dashboardFiltersTableStr .= '</td></tr>';
                            }
                            if(isset($season_id) && !empty($season_id)){
                                $dashboardFiltersTableStr .= '<tr><th class="filter-header">Season</th><td>';
                                foreach ($season_id as $key => $value) {
                                    
                                    $seasonModel = \Yii::$container->get('app\models\Season');
                                    $season = $seasonModel->getSeason($value);
                                    $code = $season['seasonCode'];
                                    $dashboardFiltersTableStr .= '<span class="new badge indigo darken-1">'.$code.'</span>';
                                }
                                $dashboardFiltersTableStr .= '</td></tr>';
                            }
                            if(isset($stage_id) && !empty($stage_id)){
                                $dashboardFiltersTableStr .= '<tr><th class="filter-header">Stage</th><td>';
                                foreach ($stage_id as $key => $value) {
                                    $params = ['stageDbId'=>$value];
                                    $stage = Stage::find($params);
                                    $code = $stage['stageCode'];
                                    $dashboardFiltersTableStr .= '<span class="new badge deep-purple">'.$code.'</span>';
                                }
                                $dashboardFiltersTableStr .= '</td></tr>';
                            }
                            if(isset($site_id) && !empty($site_id)){
                                $dashboardFiltersTableStr .= '<tr><th class="filter-header">Site</th><td>';
                                foreach ($site_id as $key => $value) {

                                    $siteCode = '';
                                    $geoModel = \Yii::$container->get('app\models\GeospatialObject');
                                    $site = $geoModel->getOne($value);
                                    if($site['success'] && isset($site['data']['geospatialObjectName'])){
                                        $siteCode = $site['data']['geospatialObjectName'];
                                    }
                                    $dashboardFiltersTableStr .= '<span class="new badge light-blue darken-3">'.$siteCode.'</span>';
                                }
                                $dashboardFiltersTableStr .= '</td></tr>';
                            }

                            echo $dashboardFiltersTableStr;
                        ?>
                    </table>
                </div>
            </div>

        </div>
        <div class="modal-footer">
        <a class="modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Cancel') ?></a>&emsp;
        <button class="btn waves-effect waves-light" id="save-data-filter-confirm" style="margin-right:20px;" title="Save data filters">Save</button>
        </div>
    </div>
  </div>
</div> 

<!-- update saved filter modal -->
<div id="update-data-filter-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4>Update filter name<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
        </div>
        <div class="modal-body update-filter-modal-body">

            <div class="input-field form-group col-md-6 update-field-filter-name required">
                <label class="control-label" for="season-abbrev">Filter name</label>
                <input type="text" id="update-filter-name-input" class="form-control" title="Enter name for this filter" autofocus aria-required="true" value="<?= $filterRaw ?>">
                <p class="help-block help-block-error">Enter name for this filter</p>
            </div>

            <div class="col-md-6">
                <div style="margin: 22px 10px 10px 10px">
                    <table class="table-dashboard-filters">
                        <?php
                            // displays current filters
                            echo $dashboardFiltersTableStr;
                        ?>
                    </table>
                </div>
            </div>

        </div>
        <div class="modal-footer">
        <a class="modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Cancel') ?></a>&emsp;
        <button class="btn waves-effect waves-light" id="update-data-filter-confirm" style="margin-right:20px;" title="Update filter name">Save</button>
        </div>
    </div>
  </div>
</div> 

<!-- use saved filter modal -->
<div id="use-saved-filter-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4><?= \Yii::t('app', 'Use saved filter') ?><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
        </div>
        <div class="modal-body use-saved-filter-modal-body">
            
            <div class="col col-md-6">
                <?php
                    echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app', 'Name').'</label>';
                    echo Select2::widget([
                        'name' => 'data_filter_name',
                        'id' => 'filter-name-select',
                        'maintainOrder' => true,
                        'options' => [
                            'placeholder' => \Yii::t('app', 'Select filter name'),
                            'tags' => true,
                        ]
                    ]);
                ?>
            </div>
            <div class="col-md-6">
                <div style="margin: 22px 10px 10px 10px" id="preview-selected-filter">
                    
                </div>
            </div>

        </div>
        <div class="modal-footer">
        <a class="modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Cancel') ?></a>&emsp;
        <button class="disabled btn waves-effect waves-light" id="save-apply-data-filter-confirm" style="margin-right:20px;"><?= \Yii::t('app', 'Apply') ?></button>
        </div>
    </div>
  </div>
</div> 

<style>
    #data-filters > li:not(.header){
        margin: 0px 20px 0px 15px;
        line-height: 28px;
    }
    .side-nav li>a.btn-flat:hover{
       box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    }
    a.save-data-filter-btn,a.filter-more-actions{
        display: inline-block !important;
        padding: 0px 10px 0px 10px !important;
        margin: 0 !important;
        height: 30px !important;
        line-height: 30px !important;
    }
    th.filter-header{
        text-align: right;
    }
    .table-dashboard-filters th,.table-dashboard-filters td{
        padding: 5px 5px 0px 5px;
    }
    .table-dashboard-filters td span.badge{
        margin: 2px 2px;
    }
    .btn-flat.readonly{
        color: #b3b2b2 !important;
        cursor: default;
    }
    .side-nav li>a.btn-flat.readonly:hover{
       box-shadow: none;
    }
    .dropdown-content li>a.filters-title-menu{
        text-overflow: ellipsis;
        overflow: hidden;
        padding: 5px !important;
        cursor: default;
        font-weight: bold;
    }
</style> 


<?php

// loop through each fields and retrieve saved data
$fieldVals = [];
$fields = ['program'];
foreach ($fields as $key => $value) {
    $field = $value;
    if(in_array($value, ['program', 'stage', 'site', 'season']))
        $field = $value . '_id';
    
    $fieldVals[$value] = (isset($$field) && !empty($$field)) ? ($$field) : [];

}

$fieldValsStr = json_encode($fieldVals);
$fieldsStr = json_encode($fields);
$getSavedFiltersUrl = Url::to(['/dashboard/default/getsavedfiltersurl']);

$saveDashboardFiltersUrl = Url::to(['/dashboard/default/savefilters']);
$dashboardUrl = Url::to(['/dashboard']);
$saveCurrentFilterUrl = Url::to(['/dashboard/default/savecurrentfilters']);
$deleteFilterUrl = Url::to(['/dashboard/default/deletefilter']);
$applyFilterUrl = Url::to(['/dashboard/default/applyselectedfilter']);
$previewSelectedFilterUrl = Url::to(['/dashboard/default/previewselectedfilter']);

$module = Yii::$app->controller->module->id; 
$controller = Yii::$app->controller->id; 
$action = Yii::$app->controller->action->id; 
//get url parameters
$currUrlParams = addslashes(json_encode($_GET));
// save filters URL
$applyFiltersUrl = Url::to(['/occurrence/default/apply-filters']);

$this->registerJs(<<<JS
//dropdown filter button
$('.dropdown-filter-button').dropdown({
    alignment: 'right' //Displays dropdown with edge aligned to the right of button
});

// clear all filters
$('.clear-dashboard-filters').click(function(){
    $('.filter-dashboard-select').each(function(index, element){
        var elemId = $(element).attr('id');
        var elemObj = elemId.split('-');
        var elemAttr = elemObj[0];

        // exclude clearing selected program
        if(elemAttr != 'program'){
            $(element).val(null);
            $(element).trigger('change');
        }
    });
});

//apply filters
$('#apply-dashboard-filter').click(function(){
    var elemValsArr = [];
    var elemValsTextArr = [];
    
    // show owned occurrences filter
    var showOwnedExperiments = document.getElementById('data_filter_show_owned_experiments').checked;
    var ownedVal = {
        owned: showOwnedExperiments
    };
    elemValsArr.push(ownedVal);

    // loop through each elements
    $('.filter-dashboard-select').each(function(index, element){
        var elemId = $(element).attr('id');

        var elemObj = elemId.split('-');
        var elemAttr = elemObj[0];
        var elemVal = $(element).val();

        var newElemVal = {
            [elemAttr]: elemVal
        };

        var elemValsTextVals = [];
        var textVals = $('#'+elemId).select2('data'); 

        textVals.forEach(function (item) { 
            elemValsTextVals.push(item.text);
        })

        var newElemText = {
            [elemAttr+'-text']: elemValsTextVals
        };

        if(textVals !== null)
            elemValsTextArr.push(newElemText);
        if(elemVal !== null)
            elemValsArr.push(newElemVal);

    });

    $.ajax({
        url: '$applyFiltersUrl',
        type: 'post',
        data: {
            data: elemValsArr,
            texts: elemValsTextArr,
            curr_url_params: '$currUrlParams',
            module: '$module',
            controller: '$controller',
            action: '$action',
        },
        success: function(response) {
            window.location = response;
        },
        error: function() {
        }
    });

});

//saves current data filter
$('#save-data-filter-confirm').click(function(){
    var filter_name = $("#filter-name-input").val();

    if(filter_name == ''){
        $(".field-filter-name").addClass("has-error");
        $(".field-filter-name").find(".help-block").html('Filter name cannot be blank.');
    }else{
        $.ajax({
            url: '$saveCurrentFilterUrl',
            type: 'post',
            dataType: 'json',
            data: {
                filter_name: filter_name
            },
            success: function(response) {
                if(response == 'exists'){
                    $(".field-filter-name").addClass("has-error");
                    $(".field-filter-name").find(".help-block").html('Filter with same name already exists.');
                }else{
                    location.reload();
                }
            },
            error: function() {
            }
        });
    }
});

// update current data filter name
$('#update-data-filter-confirm').click(function(){
    var updateFilterName = $("#update-filter-name-input").val();

    if(updateFilterName == ''){
        $(".update-field-filter-name").addClass("has-error");
        $(".update-field-filter-name").find(".help-block").html('Filter name cannot be blank.');
    }else{
        $.ajax({
            url: '$saveCurrentFilterUrl',
            type: 'post',
            dataType: 'json',
            data: {
                original_filter_name: "$filterRaw",
                filter_name: updateFilterName
            },
            success: function(response) {
                if(response == 'exists'){
                    $(".update-field-filter-name").addClass("has-error");
                    $(".update-field-filter-name").find(".help-block").html('Filter with same name already exists.');
                }else{
                    location.reload();
                }
            },
            error: function() {
            }
        });
    }
});

//delete saved data filter
$('.delete-filter-btn').click(function(){
    $.ajax({
        url: '$deleteFilterUrl',
        type: 'post',
        data: {
            filter_name: "$filterRaw"
        },
        success: function(response) {
            location.reload();
        },
        error: function() {
        }
    });
});

$(document).ready(function(){

    /* Apply Dashboard Filters BEGIN */
    var fields = JSON.parse('$fieldsStr');
    var fieldsValStr = '$fieldValsStr';
    var fieldVals = JSON.parse(fieldsValStr.replace(/ 0+(?![\. }])/g, ' '));

    var i;
    for(i=0; i < fields.length; i++) {

        var field = fields[i];
        var variableVal = field[0].toUpperCase() + field.slice(1);
        var url = '/index.php/dashboard/default/getstudyattrtags?attr='+field;

        if(field == 'program')
            var url = '/index.php/dashboard/default/getprogramtags';

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            async: false,
            success: function(response) {
                option = response;
                var lookup = {};
                this['s'+field] = document.getElementById(field+'-dashboard_select');

                for(var i=0; i<option.length;i++){
                    this['new' + variableVal +'Option'] = document.createElement("option");

                    if (!(parseInt(option[i]['id']) in lookup)) {

                        lookup[parseInt(option[i]['id'])] = 1;

                        this['new' + variableVal +'Option'].value = parseInt(option[i]['id']);
                        this['new' + variableVal +'Option'].innerHTML = option[i]['text'];
                        this['s'+field].options.add(this['new' + variableVal +'Option']);
                    }
                }

                var selectedVals = fieldVals[field];
                $('#'+field+'-dashboard_select').val(selectedVals);
                $('#'+field+'-dashboard_select').trigger('change');

            },
            error: function() {
            }
        });
    }
    /* Apply Dashboard Filters END */

    //change attributes of the save button if there are changes in the filter form
    setTimeout(function(){
        //get filter tags
        $.ajax({
            url: '$getSavedFiltersUrl',
            type: 'post',
            dataType: 'json',
            async: true,
            data: {
                selected: "$filterRaw"
            },
            success: function(response) {
                option = response;
                var lookup = {};
                var sfiltername = document.getElementById('filter-name-select');

                for(var i=0; i<option.length;i++){
                    var newFilterNameOption = document.createElement("option");
                    newFilterNameOption.value = option[i]['id'];
                    newFilterNameOption.innerHTML = option[i]['text'];
                    sfiltername.options.add(newFilterNameOption);
                }
            },
            error: function() {
            }
        });

        //preview selected filter
        $( "#filter-name-select" ).change(function(){
            var selected = this.value;
            $("#save-apply-data-filter-confirm").removeClass('disabled');

            $.ajax({
                url: '$previewSelectedFilterUrl',
                type: 'post',
                async: true,
                data: {
                    selected: selected
                },
                success: function(response) {
                    $('#preview-selected-filter').html(response);
                },
                error: function() {
                }
            });
        });

        //apply selected filter
        $("#save-apply-data-filter-confirm").click(function(){
            var selected = $( "#filter-name-select" ).val();

            $.ajax({
                url: '$applyFilterUrl',
                type: 'post',
                async: true,
                data: {
                    selected: selected
                },
                success: function(response) {
                    window.location = response;
                },
                error: function() {
                }
            });
        });

    }, 300);
});
JS
);


?>