<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders menu items
 */
use app\controllers\Constants;
use app\models\Application;
use app\models\Config;
use app\models\Program;
use app\models\CropProgram;
use app\models\Space;
use app\models\Stage;
use app\modules\dashboard\models\DashboardModel;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

$applicationModel = new Application();
$programModel = new Program(new CropProgram());
$program = Yii::$app->userprogram->get('abbrev'); //current program

extract($layoutObj);
$baseUrl = Url::base().'/index.php?program='.$program;
$session = Yii::$app->session;

$menuItems = Space::getMenuItems($program); //get menu items
$appAbbrevs = []; //initialize application abbrevs

$filters = (array) $filters; //filters
extract($filters);

$savedFilters = ''; //saved filters string
$sf = ''; //list of saved filters
$filterRaw = ''; //currently saved filter
if(!empty($filtersExist)){ //if has currently applied saved filter
    $filterRaw = $filtersExist['raw'];      
}

$sf = DashboardModel::getSavedDashboardFilters($filterRaw);
if(!empty($sf)){ //build list of data filters in menu
    	$savedFilters = $savedFilters.'<li class="divider"></li>';
    	foreach ($sf as $key => $value) {
    		$savedFilters = $savedFilters.'<li class="tooltipped" data-tooltip="'.\Yii::t('app','Use {key} data filter',['key'=>$key]).'">
    			<a href="'.Url::to(['/dashboard/default/applyselectedfilterget','selected'=>$key]).'">'.$key.'</a></li>';
    	}
    }
?>   

<!-- search -->
<nav id="search-bar-input-nav"  class=" <?= $themeColor . ' '. $darken ?> container" style="display:none;">
	<div id="search-bar-row" class="nav-wrapper row">
		<div class="col s5 m8 l10" style="padding: 0 !important;">
			<input id="search-bar-input" type="text" placeholder="Search here...">
		</div>
		<div class="col s4 m2 l1" style="padding: 0 !important;">
			<div class="row" style="margin-bottom: 0px;">
				<div class="input-field col s12" style="padding: 0 !important;">
					<?php
						echo Select2::widget([
							'name' => 'nav-search-bar-entity-select',
							'id' => 'nav-search-bar-entity-select',
							'data' => ['germplasm','experiment','package','person','trait','tool'],
							'theme' => Select2::THEME_MATERIAL,
							'options' => [
							],
							'pluginOptions' => [
								'minimumResultsForSearch' => -1,
							]
						]);
					?>
				</div>
			</div>
		</div>
		<div class="col s3 m2 l1" style="padding: 0 !important;">
			<div class="row" style="margin-bottom: 0px;">
				<div class="col s4" style="padding: 0 !important;">
					<a id="nav-search-bar-search-btn" class="btn"><i class="material-icons" style="line-height: inherit;">search</i></a>
				</div>
				<div class="col s4" style="cursor: pointer;">
					<i class="material-icons" id="help-search-btn" title="<?= \Yii::t('app', 'Help') ?>" data-target="#help-modal" data-toggle="modal">help</i>
				</div>
				<div class="col s4" style="cursor: pointer;">
					<a id="close-search-btn" title="<?= \Yii::t('app', 'Close') ?>"><i class="material-icons">clear</i></a>
				</div>
			</div>
		</div>
	</div>
</nav>

<!-- render main nav -->
<nav class=" <?= $themeColor . ' '. $darken ?> header" id="top-nav-bar">
	<div class="nav-wrapper">
		<span class="show-side-nav-btn" <?php echo (!empty($sideNavClass)) ? 'style = "display:none" ' : ''; ?> >
			<a href="#" data-tooltip="<?= \Yii::t('app', 'Show left nav') ?> (<?= \Yii::t('app', 'Alt') ?>+L)" style="margin-left:20px" data-activates="slide-out" id="go-to-show-left-nav" class="waves-effect waves-light button-collapse in-nav tooltipped"><i class="material-icons">menu</i></a>
			</span>
		<span class="brand-logo">
		<img src="<?= Yii::getAlias('@web/images/b4r-logo-circle.png')?>" style="width:29px;margin-top:-4px">&nbsp;
		<?= Constants::DOMAIN_NAME ?>
		</span>
		<!-- items in left main menu -->
		<ul style="margin-left: 250px" class="menu-item-start">
			
			<li data-tooltip="<?= \Yii::t('app', 'Dashboard') ?> (<?= \Yii::t('app', 'Alt') ?>+X)" class="<?= (Yii::$app->controller->module->id == 'dashboard') ? 'active' : ''?> tooltipped">
				<a class="in-nav waves-effect waves-light" id="go-to-dashboard" href="<?= Url::to(['/dashboard','program'=>$program]) ?>"><i class="material-icons">dashboard</i></a>
			</li>

			<li class="hidden">
				<a id="go-to-configure-dashboard" href="<?= Url::to(['/dashboard/default/configure','program'=>$program]) ?>"></a>
			</li>
			<li class="tooltipped" data-tooltip="<?= \Yii::t('app', 'Recently used tools') ?> (<?= \Yii::t('app', 'Alt') ?>+R)">
				<a class="recently-used-menu in-nav waves-effect waves-light" id="go-to-recently-used-tools" data-target="#recently-used-modal" data-toggle="modal"><i class="material-icons">watch_later</i></a>
			</li>
			<li class="tooltipped" data-tooltip="<?= \Yii::t('app', 'Favorites') ?> (<?= \Yii::t('app', 'Alt') ?>+A)">
				<a class="favorites-menu in-nav waves-effect waves-light" data-target="#favorites-modal" id="go-to-favorites" data-toggle="modal"><i class="material-icons">star</i></a>
			</li>
			<li class="tooltipped" data-tooltip="<?= \Yii::t('app', 'Data filters') ?> (<?= \Yii::t('app', 'Alt') ?>+V)">
				<a data-activates="data-filters" id="go-to-data-filters" class="button-collapse in-nav waves-effect waves-light" ><i class="material-icons">filter_alt</i></a>
			</li>
			<li class="menu-item-filters">
				<a data-tooltip="<?= \Yii::t('app', 'Current filters') ?>" data-activates="current-filters" class="tooltipped dropdown-button waves-effect waves-light" data-constrainwidth="false" data-beloworigin="true">
					<?php
					//displays current filters
					$currFltrs = '';
					$filtersTable = '<table class="table-dashboard-filters" style="white-space: normal;">';

					if(isset($program_id) && !empty($program_id) && isset($_GET['program']) && !empty($_GET['program'])){
							$programObj = $programModel->getProgram($program_id);
							$code = $programObj['programCode'];
							$program = $programObj['programCode'];
					    $currFltrs .= '<span class="new badge green darken-2" title="'.\Yii::t('app', 'Program').'">'.$code.'</span>';

					    $filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'Program').'</th><td>'.
					            '<span class="new badge green darken-2">'.$code.'</span>'
					        .'</td></tr>';
					}
					if(isset($year) && !empty($year)){

						$count = count($year);
						if($count > 1){
							$currFltrs .= '<span class="new badge cyan darken-1" title="'.\Yii::t('app', 'Year').'">'.$count.'</span>';
							
							$filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'Year').'</th><td>';
							
							foreach ($year as $key => $value) {
					            $filtersTable .= '<span class="new badge cyan darken-1">'.$value.'</span>';
					        }

					        $filtersTable .= '</td></tr>';

						}else{
							$currFltrs .= '<span class="new badge cyan darken-1" title="'.\Yii::t('app', 'Year').'">'.$year[0].'</span>';
							
							$filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'Year').'</th><td>'.
					            '<span class="new badge cyan darken-1">'.$year[0].'</span>'
					        .'</td></tr>';
						}					    
					}
					if(isset($season_id) && !empty($season_id)){

						$seasonModel = \Yii::$container->get('app\models\Season'); 
						$count = count($season_id);

						if($count > 1){
							$currFltrs .= '<span class="new badge indigo darken-1" title="'.\Yii::t('app', 'Season').'">'.$count.'</span>';

							$filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'Season').'</th><td>';
							
							foreach ($season_id as $key => $value) {
								$code = '';
								$season = $seasonModel->getOne($value);

								if($season['success'] && isset($season['data']['seasonCode'])){
									$code = $season['data']['seasonCode'];
								}
								$filtersTable .= '<span class="new badge indigo darken-1">'.$code.'</span>';
							}

							$filtersTable .= '</td></tr>';

						}else{
							$code = '';
							$season = $seasonModel->getOne($season_id[0]);

							if($season['success'] && isset($season['data']['seasonCode'])){
								$code = $season['data']['seasonCode'];
							}
							$currFltrs .= '<span class="new badge indigo darken-1" title="'.\Yii::t('app', 'Season').'">'.$code.'</span>';

							$filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'Season').'</th><td>'.
					            '<span class="new badge indigo darken-2">'.$code.'</span>'
					        .'</td></tr>';
						}
					}
					if(isset($stage_id) && !empty($stage_id)){
						$count = count($stage_id);
						if($count > 1){
							$currFltrs .= '<span class="new badge deep-purple" title="'.\Yii::t('app', 'Stage').'">'.$count.'</span>';

							$filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'Stage').'</th><td>';
							
							foreach ($stage_id as $key => $value) {
											$params = ['stageDbId'=>$value];
											$stage = Stage::find($params);
											$code = $stage['stageCode'];
					            $filtersTable .= '<span class="new badge deep-purple">'.$code.'</span>';
					        }

					        $filtersTable .= '</td></tr>';

						}else{
							$params = ['stageDbId'=>$stage_id[0]];
							$stage = Stage::find($params);
							$code = $stage['stageCode'];
							$currFltrs .= '<span class="new badge deep-purple" title="'.\Yii::t('app', 'Stage').'">'.$code.'</span>';

							$filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'Phase').'</th><td>'.
					            '<span class="new badge deep-purple">'.$code.'</span>'
					        .'</td></tr>';
						}
					}
					if(isset($site_id) && !empty($site_id)){

						$geoModel = \Yii::$container->get('app\models\GeospatialObject');
						$count = count($site_id);
						if($count > 1){
							$currFltrs .= '<span class="new badge light-blue darken-3" title="'.\Yii::t('app', 'Site').'">'.$count.'</span>';

							$filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'Site').'</th><td>';
							
							foreach ($site_id as $key => $value) {
								$siteCode = '';
								$site = $geoModel->getOne($value);
								if($site['success'] && isset($site['data']['geospatialObjectCode'])){
									$siteCode = $site['data']['geospatialObjectCode'];
								}
								$filtersTable .= '<span class="new badge light-blue darken-3">'.$siteCode.'</span>';
							}

							$filtersTable .= '</td></tr>';

						}else{
							$siteCode = '';
							$site = $geoModel->getOne($site_id[0]);
							if($site['success'] && isset($site['data']['geospatialObjectCode'])){
								$siteCode = $site['data']['geospatialObjectCode'];
							}
							$currFltrs .= '<span class="new badge light-blue darken-3" title="'.\Yii::t('app', 'Site').'">'.$siteCode.'</span>';

							$filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'Site').'</th><td>'.
					            '<span class="new badge light-blue darken-3">'.$siteCode.'</span>'
					        .'</td></tr>';
						}
					}

					if(isset($owned) && !empty($owned)){
						if ($owned=='true'){ 
							$currFltrs .= '<span class="new badge lime darken-2" title="'.\Yii::t('app', 'My Experiments').'">Owned</span>';

							$filtersTable .= '<tr><th class="filter-header">'.\Yii::t('app', 'My Experiments').'</th><td>';
							$filtersTable .= '<span class="new badge lime darken-2">OWNED</span>';
							$filtersTable .= '</td></tr>';
						
						}
					}

					$filtersTable .= '</table>';

					// if there is currently applied saved filter
					if(isset($filtersExist['raw']) && !empty($filtersExist['raw'])){
						echo '<span class="lime darken-3 new badge">'. $filtersExist['raw'] . '</span>';
					} else {
						echo $currFltrs;
					}

					?>					
				</a>
			</li>
		</ul>

		<ul id="current-filters" class="dropdown-content" style="max-width: 320px !important">
			<li class="item-main-menu-filters tooltipped" data-tooltip="<?= \Yii::t('app', 'Current filters') ?>">
				<?= (isset($filtersExist) && !empty($filtersExist)) ? '<span href="#" class="filters-title-menu theme-font-color">'.$filtersExist['raw'].'</span>' : ''; ?>

				<?= $filtersTable ?>
			</li>
			<?= $savedFilters ?>
		</ul>

		<!-- items in right main menu -->
		<ul class="right ">
			<!-- Organization logo -->
			<?php
				$configModel = new Config();
				$logo = $configModel->getConfigByAbbrev('ORGANIZATION_LOGO');
				$orgUrl = '';
				if(isset($logo['logo_url']) && !empty($logo['logo_url'])){
					$orgUrl = '';
					$logoUrl = $logo['logo_url'];
					$orgUrl = isset($logo['organization_site_url']) ? ' href="'.$logo['organization_site_url'].'" target="_blank"' : '';
					$orgDescription = isset($logo['description']) ? ' title="'.$logo['description'].'" ' : '';
					?>
					<li class="organization-logo">
						<a class="hide-in-mobile" <?= $orgUrl . $orgDescription ?> >
							<img src="<?= $logoUrl ?>" style="width:50px;" />
						</a>
					</li>
					<?php
				}
			?>

			<!-- search -->
			<?php 
				// check if program has access to search tool
				$showSearchTool = Space::showSearchTool($program);
				if ($showSearchTool) {
			?>
				<li data-tooltip="<?= \Yii::t('app', 'Search') ?> (<?= \Yii::t('app', 'Alt') ?>+S)" class="tooltipped <?= (Yii::$app->controller->module->id == 'search') ? 'active' : ''?>">
					<a href="#" id="search-icon" class="waves-effect waves-light in-nav"><i class="material-icons">search</i></a>
				</li>
			<?php
				}
			?>
			
			<?php
			//main menu items
			if(!empty($menuItems['mainMenuItems'])){
				$mainMenuItems = $menuItems['mainMenuItems'];

				foreach ($mainMenuItems as $keym => $valuem) {
					$icon = isset($valuem['icon']) ? '<i class="material-icons">'.$valuem['icon'].'</i>' : '';
					
					echo '<li class="'.$valuem['name'].'-main-menu">
							<a class="tooltipped dropdown-button waves-effect waves-light" data-constrainwidth="false" href="#" data-activates="'.$valuem['name'].'" data-beloworigin="true" data-tooltip="'. \Yii::t('app',$valuem['label']).'">
								'.$icon.'
							</a>
						</li>';

					//check if has sub items
					if(isset($valuem['items']) && !empty($valuem['items'])){
						echo '<ul id="'.$valuem['name'].'" class="dropdown-content"  style="width: 247px !important">';
						foreach ($valuem['items'] as $km => $vm) {

							//check if parent menu is active
							if(Yii::$app->session->get('applicationAbbrev') !== null && isset(Yii::$app->session->get('applicationAbbrev')[0])
							&& isset($vm['appAbbrev']) && Yii::$app->session->get('applicationAbbrev')[0] == $vm['appAbbrev']){
								$mainParent = explode('_', $vm['name']);
								if(isset($mainParent[0])){
									$this->registerJs(<<<JS
									$('.{$mainParent[0]}-main-menu').addClass('active');
JS
									);
								}
							}
							$appMainMenuUrl = null;
							//get application url by abbrev
							if(isset($vm['appAbbrev'])){
								$appMainMenuUrl = $applicationModel->getUrlByAppAbbrev($vm['appAbbrev'],$program); //get application url by application abbrev
							}

							$mainMenuUrl = (isset($appMainMenuUrl) && !empty($appMainMenuUrl)) ? Url::to([$appMainMenuUrl]) : '#';
							$mainMenuUrl = (isset($vm['url']) && !empty($vm['url'])) ? $vm['url'] : $mainMenuUrl;
							$target = (isset($vm['url']) && !empty($vm['url'])) ? '_blank' : '';
							$yii1Link = (isset($vm['url']) && !empty($vm['url'])) ? '' : 'yii1-link';
							$tooltip = (isset($vm['tooltip']) && !empty($vm['tooltip'])) ? $vm['tooltip'] : '';
							$icon = (isset($vm['icon']) && !empty($vm['icon'])) ? '<i class="material-icons">'.$vm['icon'].'</i>' : '';
							$sendFeedbackAttrs = (isset($vm['name']) && $vm['name'] == 'help_feedback') ? 'id="bug-report-btn" data-target="#bug-report-modal" data-toggle="modal"' : '';
							$class = isset($vm['class']) ? $vm['class'] : '';

							// if image is displayed
							if(isset($vm['image'])){
								$item = '<li class="main-menu-img"><img src="'.$vm['image'].'" style="width:220px; margin-left:13px;"></li>';
							}
							else if($mainMenuUrl != '#'){
								$item = '<li title="'.$tooltip.'">'.
										'<a class="'.$yii1Link.'" href="'.$mainMenuUrl.'" target="'.$target.'" '.$sendFeedbackAttrs.'>'.$icon. \Yii::t('app', $vm['label']).'</a>'
									.'</li>';
							}else {
								$item = '<li class="'.$class.'">' . \Yii::t('app',$valuem['label']) . '</li>';
							}

							echo $item;
							//save application abbrevs in session
							if(isset($vm['appAbbrev'])){
								$appAbbrevs[] = $vm['appAbbrev'];
							}
						}
						echo '</ul>';
					}
				}
			}
			?>
			<!-- account -->
			<li class="<?= (Yii::$app->controller->module->id == 'account') ? 'active' : ''?>">
				<a class="dropdown-button waves-effect waves-light tooltipped" data-tooltip="<?= \Yii::t('app', 'Account') ?>" data-constrainwidth="false" href="#!" data-activates="user-account" data-beloworigin="true" id="top-menu-user-account"><i class="material-icons">person</i></a>
			</li>

			<ul id="user-account" class="dropdown-content"  style="width: 140px !important">
				<li class="tooltipped" data-tooltip="<?= \Yii::t('app', 'Profile') ?> (<?= \Yii::t('app', 'Alt') ?>+M)"><a class="yii1-link" href="<?= Url::to(['/account/profile','program'=>$program]) ?>" id="go-to-profile"><i class="material-icons">person</i> <?= \Yii::t('app', 'My profile') ?></a></li>
				<li class="tooltipped" data-tooltip="<?= \Yii::t('app', 'Preferences') ?> (<?= \Yii::t('app', 'Alt') ?>+P)"><a href="#" data-activates="layout-preferences" id="go-to-preferences" class="button-collapse-layout" ><i class="material-icons">tune</i> <?= \Yii::t('app', 'Preferences') ?></a></li>
				<li class="tooltipped" data-tooltip="<?= \Yii::t('app', 'Logout') ?> (<?= \Yii::t('app', 'Alt') ?>+G)"><a href="<?= Url::to(['/auth/default/logout']) ?>" id="go-to-logout"><i class="material-icons">logout</i> <?= \Yii::t('app', 'Logout') ?></a></li>
			</ul>
		<!-- end account -->
		</ul>
		<!-- timezone; -->
		<strong style="float:right; margin-right: 10px; cursor: default; color: #e5e2e2 ;" title="Time zone" class="hide-in-mobile">UTC</strong>
	</div>      
</nav>
<!-- loading indicator -->
<span id="system-loading-indicator"></span>

<?php 
//left menu items
if(!empty($menuItems['leftMenuItems'])){
	$userPermission = Yii::$app->session->get('userPermissions')[$program] ?? [];
	$leftMenuItems = !empty($userPermission) ? $userPermission: $menuItems['leftMenuItems'];
?>
<ul id="slide-out" class="side-nav <?= $sideNavClass ?> left-side-nav" >
	<li>
		<div class="user-view" <?php echo (!empty($sideNavClass)) ? ' style = "display:none" ' : ''; ?> >
			<div class="background">
				
			</div>
			<a href="<?= $baseUrl ?>"><img src="<?= Yii::getAlias('@web/images/b4r-logo-circle.png')?>" style="width:60px;"></a></a>
			<a href="<?= $baseUrl ?>"><span class="white-text name"><?= Constants::DOMAIN_NAME ?></span></a>
			<a href="#" style="cursor: default;"><span class="white-text email">
				<span> 
					&copy; <?= date('Y') ?>
					<?= Constants::DOMAIN_NAME.' ('.Yii::$app->params["version"].')' ?>
			</span>
			</a>
		</div>
	</li>

	<?php
	//render left menu items
	foreach ($leftMenuItems as $key => $value) {
		$rightArrow = (isset($value['items']) && !empty($value['items'])) ? '<i class="material-icons left-menu-next-icon">arrow_forward</i>' : '';
		
		$appParentUrl = null; //initialize app parent url

		//save application abbrevs in session
		if(isset($value['appAbbrev'])){
			$appAbbrevs[] = $value['appAbbrev'];
			$appParentUrl = $applicationModel->getUrlByAppAbbrev($value['appAbbrev'],$program); //get application url by application abbrev
		}

		$parentUrl = (isset($appParentUrl) && !empty($appParentUrl)) ? Url::to([$appParentUrl]) : '#';
		$parentClass = (isset($value['appAbbrev']) && !empty($value['appAbbrev'])) ? '' : 'item-left-menu';

		//check if parent menu is active
		if(Yii::$app->session->get('applicationAbbrev') !== null && isset(Yii::$app->session->get('applicationAbbrev')[0]) 
		&& isset($value['appAbbrev']) && Yii::$app->session->get('applicationAbbrev')[0] == $value['appAbbrev']){

			$leftItem = $value['name'].'-left-menu';
			$this->registerJs(<<<JS
				$('.{$leftItem}').addClass('active');
JS
			);
		}

		echo '<li class="parent-menu '.$value['name'].'-left-menu">'.
		'<a class="yii1-link waves-effect fixed-color '.$parentClass.'" data-sub_menu="'.$value['name'].'-sub-menu" href="'.$parentUrl.'">'.\Yii::t('app', $value['label']) .$rightArrow.'</a></li>';

		//if there are sub items      
		if(isset($value['items']) && !empty($value['items'])){
			echo '<li class="sub-parent-menu '.$value['name'].'-sub-menu">
			<a class="yii1-link waves-effect fixed-color left-menu-back" data-sub_menu="'.$value['name'].'-sub-menu"><i class="material-icons">arrow_back</i> '.\Yii::t('app', $value['label']).'</a>
			</li>';

			$leftNavUrl = '#';
			$target = '';
			foreach ($value['items'] as $k => $v) {
				$subMenuClass=''; //initialize sub menu class
				$appChildUrl = null; //initialize app child url
				$label = isset($v['label']) ? $v['label'] : '';

				// if external link and link is in env file
				if(!isset($v['appAbbrev']) && isset($v['envVariable'])){
					$subPage = (isset($v['subPage'])) ? $v['subPage'] : '';

					$leftNavUrl = Url::to(getenv($v['envVariable'])) . $subPage;
					$target = (isset($v['isNewTab']) && $v['isNewTab'] == 'true') ? 'target="_blank" rel="noopener noreferrer"' : '';
				}
				else if(Yii::$app->session->get('applicationAbbrev') !== null && isset(Yii::$app->session->get('applicationAbbrev')[0]) && isset($v['appAbbrev']) && Yii::$app->session->get('applicationAbbrev')[0] == $v['appAbbrev']){
					$subMenuClass='active';

					$this->registerJs(<<<JS
					$('.{$value['name']}-sub-menu').css('display','block');
					$('.parent-menu').css('display','none');
JS
					);

					//if application abbrev is set, get url of app and save application abbrevs in session
					if(isset($v['appAbbrev'])){
						$appChildUrl = $applicationModel->getUrlByAppAbbrev($v['appAbbrev'],$program);
						$appAbbrevs[] = $v['appAbbrev'];
					}

					$leftNavUrl = Url::to([$appChildUrl]);

				}

				echo '<li class="sub-parent-menu '.$value['name'].'-sub-menu '.$subMenuClass.'">'.
				'<a class="waves-effect fixed-color" href="'.$leftNavUrl.'" '.$target.'>'.\Yii::t('app',$label).'</a></li>';
			}
		}
	}
	//set app abbrevs to session
	Yii::$app->session->set('appAbbrevs',json_encode($appAbbrevs));
} //end of checking if left menu is not empty
?>
<a class="always-show-left-nav tooltipped pull-right" href="#" data-tooltip="<?= \Yii::t('app', 'Always show left nav') ?>" data-position="top" ><i class="material-icons">chevron_right</i></a>
<!-- <a class="always-show-left-nav tooltipped pull-right sidenav-open" href="#" data-tooltip="<?= \Yii::t('app', 'Allways show left nav') ?>" data-position="top" ><i class="material-icons">arrow_forward</i></a> -->
</ul>

<a class="always-hide-left-nav tooltipped" id="go-to-hide-left-nav" href="#" data-tooltip="<?= \Yii::t('app', 'Hide left nav') ?>" data-position="top" ><i class="material-icons">chevron_left</i></a>

<!-- side nav footer -->
<div class="side-nav-footer">
	<span> &copy; <?= date('Y') ?>
	<?= Constants::DOMAIN_NAME.' ('.Yii::$app->params["version"].')' ?>
</div>

<a id="welcome-modal-btn" data-backdrop="static" data-target="#welcome-modal" data-toggle="modal"></a>
<!-- modal for favorites -->
<div id="favorites-modal" class="fade modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div style="float:left"><h4><?= \Yii::t('app', 'Favorites') ?>
			<i class="text-muted fa fa-question-circle-o tiny" title="<?php echo \Yii::t('app','To favorite an app, go to the application and click the star icon') ?>"></i>

			</h4></div><div style="float:right"><input placeholder="Filter" id="favorites_search_bar" style="margin-top:-10px;"></input></div>
			</div>
			<div class="modal-body favorites-tools favorites-modal-body">
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary waves-effect waves-light modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Close') ?></a>&emsp;
		</div>
		</div>
	</div>
</div>  

<!-- welcome modal -->
<div id="welcome-modal" class="fade modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
		<div class="modal-body welcome-modal-body text-center">
			<br/>
			<img src="<?= Yii::getAlias('@web/images/b4r-logo-circle.png')?>" style="width:130px;">&nbsp;
			<br/>
			<br/>
			<h4>Hey, <?= $session->get('user.first_name'); ?>!</h4>
			<h5> Welcome to EBS-Core Breeding.</h5>
			<p class="text-muted">
			    Introducing the <b>Dashboard</b>, a way to organize tools that are relevant to you and to your daily tasks.<br/>
			    Click <strong>Learn more</strong> to learn how to customize and use the Dashboard.
			</p>
			<br/>
			<a class="btn waves-effect waves-light learn-more-btn welcome-modal-close" target="_blank" href="<?= Url::to('https://ebsproject.atlassian.net/wiki/spaces/EUG/pages/29411901550/Dashboard')?>" >Learn more</a>
			&nbsp;
			<a class="btn waves-effect waves-light modal-close grey lighten-2 grey-text text-darken-3" href="#" data-dismiss="modal">No, Thanks</a>
			<br/><br/>
		</div>
		</div>
	</div>
</div>  

<!-- modal for recently used -->
<div id="recently-used-modal" class="fade modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div style="float:left"><h4><?= \Yii::t('app', 'Recently used tools') ?> </h4></div><div style="float:right"><input placeholder="Filter" id="recently_used_search_bar" style="margin-top:-10px;"></input></div>
		</div>
		<div class="modal-body recently-used-tools recently-used-modal-body">
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary waves-effect waves-light modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Close') ?></a>&emsp;
		</div>
		</div>
	</div>
</div>  

<?php
//help modal
Modal::begin([
    'id' => 'help-modal',
    'header' => '<h4 id="help-modal-header"><i class="fa fa-question-circle"></i> Search tool help</h4>',
    'footer' =>
        Html::a(\Yii::t('app','Close'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
]);
?>
<?= Yii::$app->controller->renderPartial('@app/modules/search/views/default/search_help.php'); ?>
<?php Modal::end();?>
<?php

$favoritesUrl = Url::to(['/dashboard/default/loadallfavoritesdata','program'=>$program]);
$recentlyUsedUrl = Url::to(['/dashboard/default/loadallrecentlyuseddata','program'=>$program]);
$removeToolInFavUrl = Url::to(['/dashboard/default/markunmarkfavorites']);
$resetYii1Url = Url::to(['/dashboard/default/resetyii1url','program'=>$program]);

//search in tool dashboard
$this->registerJs(<<<JS

//loads favorites
$(document).on('click', '.favorites-menu', function(e) {
	$('.modal-notif').html('');
	$('.favorites-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

	//load content of favorites widgets
	setTimeout(function(){ 
		$.ajax({
			url: '$favoritesUrl',
			type: 'POST',
			async: true,
			success: function(data) {
				$('.favorites-modal-body').html(data);          
			},
			error: function(){
				$('.favorites-modal-body').html('<i>There was a problem while loading content.</i>'); 
			}
		});
	}, 300);
});

//loads recently used tools
$(document).on('click', '.recently-used-menu', function(e) {
	$('.modal-notif').html('');
	$('.recently-used-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

	//load content of recently used tools
	setTimeout(function(){ 
		$.ajax({
			url: '$recentlyUsedUrl',
			type: 'POST',
			async: true,
			success: function(data) {
				$('.recently-used-modal-body').html(data);          
			},
			error: function(){
				$('.recently-used-modal-body').html('<i>There was a problem while loading content.</i>'); 
			}
		});
	}, 300);
});

//remove to favorites
$(document).on('click', '.remove-a-tool-icon', function(e) {
	var obj = $(this);
	var app_id = obj.data('appid');
	document.getElementById("favorites-"+app_id).remove();

	$.ajax({
		url: '$removeToolInFavUrl',
		type: 'POST',
		async: true,
		data: {
			app_id: app_id
		},
		success: function(data) {
		},
		error: function(){
		}
	});
});

//search tool
$('#tool_search_bar').on('change keyup paste click', function () {
	searchTerm = $(this).val();
	$('.dashboard-tools > .dashboard-tool').each(function (index) {
	toolId = '#' + $(this).attr('id');

	if(searchTerm.length == 0){
		$(toolId).show();
	}else if ($(toolId + ' div.card-panel > div.spec-dashboard-tool:containsCaseInsensitive(' + searchTerm + ')' ).length > 0 && searchTerm.length > 0) {
		$(toolId).show();
	}else{
		$(toolId).hide();
	}
	});
});

//search in favorites
$('#favorites_search_bar').on('change keyup paste click', function () {
	searchTerm = $(this).val();
	$('.favorites-tools > .favorites-tool').each(function (index) {
	toolId = '#' + $(this).attr('id');

	if(searchTerm.length == 0){
		$(toolId).show();
	}else if ($(toolId + ' div.card-panel > div.spec-favorites-tool:containsCaseInsensitive(' + searchTerm + ')' ).length > 0 && searchTerm.length > 0) {
		$(toolId).show();
	}else{
		$(toolId).hide();
	}
	});
});

//search recently used
$('#recently_used_search_bar').on('change keyup paste click', function () {
searchTerm = $(this).val();
	$('.recently-used-tools > .recently-used-tool').each(function (index) {
	toolId = '#' + $(this).attr('id');

	if(searchTerm.length == 0){
		$(toolId).show();
	}else if ($(toolId + ' div.card-panel > div.spec-recently-used-tool:containsCaseInsensitive(' + searchTerm + ')' ).length > 0 && searchTerm.length > 0) {
		$(toolId).show();
	}else{
		$(toolId).hide();
	}
	});
});

$.expr[':'].containsCaseInsensitive = function (a, i, m) {
	return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};

//reset if tool is accessed via favorites and recently used tools modals
$(document).on('click', '.yii1-faverecent-link', function(e) {
	e.preventDefault();
	var actionLink = $(this).attr("href");
	if(actionLink != undefined && actionLink != '#'){
		$.ajax({
			url: '$resetYii1Url',
			type: 'post',
			dataType: 'json',
			async:true,
			data: {
					actionLink: actionLink
			},
			success: function(response) {
				window.location.href = actionLink; 
			},
		});
	}
});
JS
);
?>

