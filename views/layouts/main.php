<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders main layout of the site
 */

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use app\controllers\CheckAccessToken;
use app\controllers\SiteController;
use yii\helpers\Url;
use app\controllers\Dashboard;
use \app\controllers\Constants;
use app\models\DataBrowserConfiguration;
use app\models\User;

JqueryAsset::register($this);
AppAsset::register($this);

$hasAccess = \Yii::$container->get('app\controllers\CheckAccessToken')->authenticate(); //authenticate access token

$layoutObj = SiteController::getLayout(); //get layout preferences
extract($layoutObj);

$session = Yii::$app->session;

$program = Yii::$app->userprogram->get('abbrev'); //current program

$rbacPermissions = $session->get('userPermissions'); //user permissions

if($hasAccess){ //if no access
    Yii::$app->session->setFlash('error','Sorry, you are not allowed to access this page.');
     Yii::$app->response->redirect(Url::base().'/index.php');
}else{
    $dashboardModel = new Dashboard();
    $dashboardModel->populateRecentlyUsed();
    $filters = $dashboardModel->getFilters(); //get dashboard filters
    $filtersExist = $dashboardModel->getSavedFilterByValues($filters); //get dashboard filters
    $defaultFilters = (array) $filters;

    // check if already retrieved and saved in session    
    if($session->get('dashboard-occurrence-text-filters') == NULL){
        $userModel = new User();
        $userId = $userModel->getUserId();
        $dataBrowserConfigurationModel = new DataBrowserConfiguration();
        $browserConfig = $dataBrowserConfigurationModel->getDataBrowserConfig($userId, $defaultFilters);
        extract($browserConfig);
    } else {
        $occurrenceFilters = $session->get('dashboard-occurrence-filters');
        $occurrenceTextFilters = $session->get('dashboard-occurrence-text-filters');
    }
?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>" class="font-size-<?= $fontSize ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="<?php echo Yii::getAlias('@web/images/b4r-logo-circle.png') ?>" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Constants::APP_NAME ?> - <?= Html::encode(Yii::$app->controller->module->id) ?></title>
        <?php $this->head() ?>
    </head>

    <body class="theme-<?= $themeColor . $bodyClass . $sideNavSkin?> " >
    <?php $this->beginBody() ?>
    <?php
        $this->registerCssFile("@web/css/b4r.css", [
            'depends' => [\yii\bootstrap\BootstrapAsset::className()],
        ], 'b4r-css');
        $this->registerCssFile("@web/css/font-awesome-4.7.0/css/font-awesome.min.css", [
        ], 'font-awesome-css');
        
        $saveThemeColorUrl = "var saveThemeColorUrl = '" . Url::to(['/site/savethemecolor']) .
        "';var saveSiteNavStateUrl = '" . Url::to(['/account/preferences/savesitenavstate']) .
        "';var saveYii1Url = '" . Url::to(['/dashboard/default/saveyii1url','program'=>$program]) .
        "';var resetYii1Url = '" . Url::to(['/dashboard/default/resetyii1url','program'=>$program]) . 
        "';var bugReportToolUrl = '" . Url::to(['/dashboard/bug-report/loadfields','program'=>$program]) . 
        "';var saveSideNavSkinUrl = '" . Url::to(['/account/preferences/savesidenavskin']) ."';";
        $this->registerJs($saveThemeColorUrl, \yii\web\View::POS_END, 'save-theme-color');
        $this->registerJsFile("@web/js/b4r.js", 
            ['depends' => ['\yii\web\JqueryAsset'],'position' => \yii\web\View::POS_END]);

        echo Yii::$app->controller->renderPartial('//site/nav.php',compact('themeColor','darken','filters','filtersExist','layoutObj'));

        echo Yii::$app->controller->renderPartial('//site/filter.php',compact('filters','filtersExist', 'occurrenceTextFilters'));
        echo Yii::$app->controller->renderPartial('@app/modules/account/views/preferences/index.php',compact('language'));

        ?>

        <div class="container body-container" style="margin-top:10px">
            <?= \macgyer\yii2materializecss\widgets\Alert::widget(); ?>
                <?= $content ?>            

            <!-- modal for bug reporting -->
            <div id="bug-report-modal" class="fade modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div style="float:left"><h4><?= \Yii::t('app', 'Send Feedback') ?> </h4></div><div style="float:right"></div>
                    </div>
                    <div class="modal-body bug-report-tools bug-report-modal-body">
                    </div>
                    <div class="modal-footer">
                        <a class="modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Cancel') ?></a>&emsp;
                        <a id="bug-report-create-btn" class="btn btn-primary waves-effect waves-light" href="#" url="#"><?= \Yii::t('app', 'Submit') ?><i class="material-icons right">send</i></i></a>&emsp;
                    </div>
                    </div>
                </div>
            </div>
            
        </div>

        <?php //Pjax::end(); ?>
        <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php 
    // Matomo variables
    $matomoEnabled = getenv('MATOMO_ENABLED');
    $matomoUrl = getenv('MATOMO_URL');
    $idSite = getenv('IDSITE');
    
    // if Matomo is enabled
    if(strtolower($matomoEnabled) === "true"){ ?>

        <!-- Matomo -->
        <script type="text/javascript">
            var idSite = <?= $idSite ?>;

            var _paq = window._paq = window._paq || [];
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                var u="//<?= $matomoUrl ?>/";
                _paq.push(['setTrackerUrl', u+'matomo.php']);
                _paq.push(['setSiteId', idSite]);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
            })();
        </script>
        <!-- End Matomo Code -->

<?php
    }
} ?>

<style type="text/css">
    #support-portal a{
        text-decoration: none !important;
    }
</style>