<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders home page
 */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\controllers\SiteController;
use yii\helpers\Url;
use \app\controllers\Constants;

AppAsset::register($this);

$layoutObj = SiteController::getLayout(); //get layout preferences
extract($layoutObj);

$session = Yii::$app->session;
Yii::$app->language  = $language; //set language

if(($session->get('user.is_logged_in') == NULL || $session->get('user.is_logged_in') != TRUE)){ //if not logged in
    $themeColor = 'teal';
}
?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>" class="font-size-<?= $fontSize ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="<?php echo Yii::getAlias('@web/images/b4r-logo-circle.png') ?>" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Constants::APP_NAME ?></title>
        <?php $this->head() ?>
    </head>

    <body class="theme-<?= $themeColor . $sideNavSkin?> " >
    <?php $this->beginBody() ?>
    	<div class="background-body">
	    </div>
	    <div class="background-body-lower">
	    </div>
	    <?php
	        $this->registerCssFile("@web/css/b4r.css", [
	            'depends' => [\yii\bootstrap\BootstrapAsset::className()],
	        ], 'b4r-css');
	    ?> 
        <div class="login-intro">
        	<?php echo \macgyer\yii2materializecss\widgets\Alert::widget(); ?>
    		<div class="home-logo">
    			<img src="<?= Yii::getAlias('@web/images/b4r-logo-circle.png') ?>" width="120px"/>
    		</div>
			<div class="login-title">
				<?= \Yii::t('app', 'Welcome to') .' '. Constants::APP_NAME . '!' ?>
			</div>

			<div class="login-text">                
			<?php
			if($session->get('user.is_logged_in') != NULL){
				echo '<a href="'.Url::to(['/dashboard','program'=> Yii::$app->userprogram->get('abbrev')]).'" class="btn waves-effect waves-light" type="submit" name="action">'.\Yii::t('app', 'Go to dashboard').'
				<i class="material-icons right">input</i>
				</a>';
			}else{
				echo '<a href="'.Url::to(['/auth/default/login']).'" class="btn waves-effect waves-light" type="submit" name="action">Login
				<i class="material-icons right">input</i>
				</a>';			  		
			}
			?>
			<br/><br/>
			<?= Constants::APP_NAME . ' '.\Yii::t('app', 'is a cloud-based breeding information management system. It provides applications for data management and knowledge work activities enabling a high-quality experience for decision-making processes.') 
			?>	
			</div>

			<!-- Support portal dropdown -->
			<ul id="support-portal" class="dropdown-content"  style="width: 160px !important">
			    <li><a href="<?= Constants::JIRA_SERVICE_DESK_URL ?>" target="_blank" title="<?= \Yii::t('app','Go to B4R Support portal'); ?>"><i class="material-icons">headset_mic</i> &nbsp;<?= \Yii::t('app', 'Support portal') ?></a></li>

			    <li class="divider"></li>

			    <li title="<?= \Yii::t('app', 'All requests created by me') ?>"><a href="<?= Constants::JIRA_SERVICE_DESK_URL ?>user/requests" target="_blank"><i class="material-icons">playlist_add</i> &nbsp;<?= \Yii::t('app', 'All my requests') ?></a></li>

			    <li title="<?= \Yii::t('app', 'All pending requests created by me') ?>"><a href="<?= Constants::JIRA_SERVICE_DESK_URL ?>user/requests?status=open" target="_blank"><i class="material-icons">access_time</i> &nbsp;<?= \Yii::t('app', 'My open requests') ?></a></li>

			    <li title="<?= \Yii::t('app', 'All closed request created by me') ?>"><a href="<?= Constants::JIRA_SERVICE_DESK_URL ?>user/requests?status=closed" target="_blank"><i class="material-icons">done_all</i> &nbsp;<?= \Yii::t('app', 'My closed requests') ?></a></li>
			</ul> 

			<div class="login-footer">
				<a class="fixed-color dropdown-button" data-activates="support-portal" data-constrainwidth="false" title="<?= \Yii::t('app','Support portal'); ?>" href="#">  <i class="material-icons" style="vertical-align:middle">headset_mic</i> </a>
				· <span>&copy; <?= date('Y') ?> <?= Constants::APP_NAME.' ('.Yii::$app->params["version"].')' ?></span>
			</div>
    	</div>

		<!-- modal for bug reporting -->
		<div id="bug-report-modal" class="fade modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<div style="float:left"><h4><?= \Yii::t('app', 'Contact Us') ?> </h4></div><div style="float:right"></div>
					</div>
					<div class="modal-body bug-report-tools bug-report-modal-body">
					</div>
					<div class="modal-footer">
						<a class="modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Cancel') ?></a>&emsp;
						<a id="bug-report-create-btn" class="btn btn-primary waves-effect waves-light" href="#" url="#"><?= \Yii::t('app', 'Submit') ?><i class="material-icons right">send</i></i></a>&emsp;
					</div>
				</div>
			</div>
		</div>

        <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>

<style>
	.background-body{
		background-color: #f1f4f5;
	    height: 1200px;
	    width: 110%;
	    left: -180px;
	    top: -810px;
	    transform: rotate(-50deg);
	    position: fixed;
	    box-shadow: -25px 28px 68px -18px rgba(0,0,0,0.75);
	    -moz-box-shadow: -25px 28px 68px -18px rgba(0,0,0,0.75);
	}

	.background-body-lower{
		background-color: #f1f4f5;
	    height: 1200px;
	    width: 110%;
	    left: 540px;
	    bottom: -1130px;
	    transform: rotate(-50deg);
	    position: fixed;
	    box-shadow: 25px -28px 68px -18px rgba(0,0,0,0.75);
	    -moz-box-shadow: 25px -28px 68px -18px rgba(0,0,0,0.75);
	}

	body{
		background-color: #009688;
		font-family: "Roboto", sans-serif;
		line-height: 1.4!important;
    	letter-spacing: -.1px!important;
    	-webkit-font-smoothing: auto;
	}

	.dark-side-nav .background-body, .dark-side-nav .background-body-lower{
		background-color: #262933 !important;
	}

	.dark-side-nav .login-intro, .dark-side-nav .login-footer, .dark-side-nav .login-footer a{
		color: #fff !important;
	}

	.login-intro{
		position: fixed;
		padding: 75px 128px 100px 150px;
		color:#000;
	}

	.home-logo{
		margin-bottom: 32px;
	    animation: 0.5s ease-out 0s 1 slideInFromTop;
	}

	.login-title{
		font-size: 42px;
	    font-weight: 300;
	    line-height: 1;
	    animation: 0.5s ease-out 0s 1 slideInFromLeft;
	}

	@keyframes slideInFromLeft {
		0% {
			transform: translateX(-100%);
		}
		100% {
			transform: translateX(0);
		}
	}

	@keyframes slideInFromBottom {
		0% {
			transform: translateY(100%);
		}
		100% {
			transform: translateY(0%);
		}
	}
	@keyframes slideInFromTop {
		0% {
			transform: translateY(-100%);
		}
		100% {
			transform: translateY(0%);
		}
	}

	.login-text{
		padding-top: 16px;
	    font-size: 14px;
	    max-width: 780px;
	    color: #333;
	    animation: 0.5s ease-out 0s 1 slideInFromBottom;
	}

	.dark-side-nav .login-text{
	    color: rgba(255,255,255,.54);
	}

	.login-footer{
		margin-top:20px;
	    color: #333;
	    animation: 0.5s ease-out 0s 1 slideInFromBottom;
	}

	.login-footer a{
		font-weight: bold;
	    color: #000 !important;
	}

	#support-portal a{
		font-weight: normal;
	}

	* {
	    text-rendering: optimizeLegibility;
	}

	@media only screen and (max-width: 992px){
	    .login-intro{
	    	padding: 108px 30px 40px 40px !important;
	    }
	    .login-title{
	    	font-size: 38px;
	    }
	   
	}

	@media only screen and (max-width: 1366px){
	    .background-body{			
		    left: -180px;
		    top: -630px;
		}
		.background-body-lower{
		    left: 540px;
		    bottom: -1250px;
		}
		.login-intro{
			padding: 128px;
		}
	}

	@media only screen and (max-width: 1244px){
	    .background-body{			
		    left: -180px;
		    top: -430px;
		}
		.background-body-lower{
		    left: 540px;
		    bottom: -1250px;
		}
	}

	@media only screen and (max-width: 944px){
	    .background-body{			
		    top: -530px;
		}

		.dark-side-nav .login-intro, .dark-side-nav .login-footer, .dark-side-nav .login-footer a{
			background-color: transparent;
		}
	}

	@media only screen and (max-width: 760px){
	    .background-body,.background-body-lower{			
		    display: none;
		}
		.login-intro{
			background-color: #f1f4f5;
			box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
		}
		.dark-side-nav .login-intro, .dark-side-nav .login-footer, .dark-side-nav .login-footer a{
			background-color: #262933;
		}
	}

	body.theme-rosegold {
	    background-color: #ee6e73 !important;
	}
	body.theme-blue{
		background-color: #1565C0 !important
	}
	body.theme-light-green{
		background-color: #33691e !important;
	}
	body.theme-cyan{
		background-color: #00838f !important;
	}
	body.theme-blue-grey{
		background-color: #455a64 !important;
	}
	body.theme-deep-purple{
		background-color: #512da8 !important;
	}
	body.theme-red{
		background-color: #C62828 !important;
	}
	.alert{
		position: absolute;
		top:0px;
	}
	.alert.error > .card-panel,.alert.success > .card-panel {
	    background: transparent;
	}
	.card-panel{
		-webkit-box-shadow: none;
		box-shadow: none;
	}
</style>
<?php
$bugReportToolUrl = Url::to(['/dashboard/bug-report/loadfields']);
?>
<script type="text/javascript">

	// renders send feedback form
	$('#bug-report-btn').on('click',function(){
		$('.modal-notif').html(''); 
		$('.bug-report-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>'); 
		var yii2Url =  window.location.href; 

		//load content of recently used tools 
		$.ajax({ 
			url: '<?= $bugReportToolUrl ?>', 
			type: 'POST', 
			data: { 
				yii2url: yii2Url 
			}, 
			async: false, 
			success: function(data) { 
				$('.bug-report-modal-body').html(data);           
			}, 
			error: function(){ 
				$('.bug-report-modal-body').html('<i>There was a problem while loading content.</i>');  
			} 
		});
	}); 
</script>
