<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use \yii\helpers\Url;
?>

<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>EBS-Core Breeding</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo Url::base(TRUE);?>/css/theme/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo Url::base(TRUE);?>/css/theme/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo Url::base(TRUE);?>/css/theme/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo Url::base(TRUE);?>/css/theme/magnific-popup.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo Url::base(TRUE);?>/css/theme/style.css">

	<!-- Modernizr JS -->
	<script src="<?php echo Url::base(TRUE);?>/js/theme/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-xs-2">
					<div id="fh5co-logo"><a href="<?php echo Url::base(TRUE);?>"><img src="<?php echo Url::base(TRUE);?>/images/b4r-logo-circle.png" height="60px" width="60px"/></a></div>
				</div>
				<div class="col-xs-10 text-right menu-1">
					<ul>
						<li class="active"><a href="<?php echo Url::base(TRUE);?>">Home</a></li>
						<li><a href="<?= Url::to(['/dashboard','enableNav'=>1]) ?>">Dashboard</a></li>
						<li class="has-dropdown">
							<a href="#">Tools</a>
							<ul class="dropdown">
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
							</ul>
						</li>
						<li><a href="#">About</a></li>
						<li><a href="#">Contact Us</a></li>
						<?php 
							$session = Yii::$app->session;

							if($session->get('user.is_logged_in')){
								$logoutUrl = Url::base(TRUE).'/index.php/authenticate/default/logout';
								echo '<li><a href="'.$logoutUrl.'">Logout</a></li>';
							}
							else{
								$loginUrl = Url::base(TRUE).'/index.php/authenticate/default/login';
								echo '<li><a href="'.$loginUrl.'">Login</a></li>';	
							}
						?>
						
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<header id="fh5co-header" class="fh5co-cover" role="banner">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 text-center">
					<div id="notifications__banner"><?= \macgyer\yii2materializecss\widgets\Alert::widget(); ?></div>
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1>EBS-Core Breeding</h1>
							
							<p><a href="#" class="btn btn-default">Click to Start</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<div class="container">
		<div class="row">
			<div id="fh5co-section">
				<div class="col-nineth">
					<div class="row">
						<div class="col-third">
							<div class="col-md-12">
								<span class="icon">
									<i class="icon-eye"></i>
								</span>
								<div class="desc">
									<h3>Accessibility</h3>
									<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
									<p><a href="#" class="btn btn-primary">Learn More</a></p>
								</div>
							</div>
						</div>
						<div class="col-third">
							<div class="col-md-12">
								<span class="icon">
									<i class="icon-power"></i>
								</span>
								<div class="desc">
									<h3>Reliability</h3>
									<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
									<p><a href="#" class="btn btn-primary">Learn More</a></p>
								</div>
							</div>
						</div>
						<div class="col-third">
							<div class="col-md-12">
								<span class="icon">
									<i class="icon-laptop"></i>
								</span>
								<div class="desc">
									<h3>Portability</h3>
									<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
									<p><a href="#" class="btn btn-primary">Learn More</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="fh5co-bg-section">
		<div class="container">
			<div class="row">
				<div id="fh5co-features-2">
					<div class="col-feature-9">
						<div class="row">
							<div class="col-md-12 fh5co-heading animate-box">
								<h2>Core Features</h2>
								<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-7 col-sm-7 col-md-pull-2 col-sm-pull-2 animate-box" data-animate-effect="fadeInRight">
								<div class="feature-image">
									<!--<img class="img-responsive" src="images/theme/work_1.png" alt="work">-->
								</div>
							</div>
							<div class="col-md-5 col-sm-5">
								<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
									<span class="icon">
										<i class="icon-check"></i>
									</span>
									<div class="feature-copy">
										<h3>Accessible</h3>
										<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit.</p>
									</div>
								</div>

								<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
									<span class="icon">
										<i class="icon-check"></i>
									</span>
									<div class="feature-copy">
										<h3>Reliable</h3>
										<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit.</p>
									</div>
								</div>

								<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
									<span class="icon">
										<i class="icon-check"></i>
									</span>
									<div class="feature-copy">
										<h3>Portable</h3>
										<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit.</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-feature-3">
						<div id="fh5co-counter" class="fh5co-bg fh5co-counter">
							<div class="row">
								<div class="display-t">
									<div class="display-tc">
										<div class="col-md-12 animate-box">
											<div class="feature-center">
												<span class="counter js-counter" data-from="0" data-to="2200" data-speed="5000" data-refresh-interval="50">1</span>
												<span class="counter-label">Creativity Fuel</span>

											</div>
										</div>
										<div class="col-md-12 animate-box">
											<div class="feature-center">
												<span class="counter js-counter" data-from="0" data-to="97" data-speed="5000" data-refresh-interval="50">1</span>
												<span class="counter-label">Happy Clients</span>
											</div>
										</div>
										<div class="col-md-12 animate-box">
											<div class="feature-center">
												<span class="counter js-counter" data-from="0" data-to="402" data-speed="5000" data-refresh-interval="50">1</span>
												<span class="counter-label">Successes</span>
											</div>
										</div>
										<div class="col-md-12 animate-box">
											<div class="feature-center">
												<span class="counter js-counter" data-from="0" data-to="9999" data-speed="5000" data-refresh-interval="50">1</span>
												<span class="counter-label">Hours Spent</span>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="fh5co-portfolio">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Finished Project</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row">
				<div class="project">
					<div class="col-md-8 col-md-push-5 animate-box" data-animate-effect="fadeInLeft">
						<!--<img class="img-responsive" src="images/theme/work_1.png" alt="work">-->
					</div>
					<div class="col-md-4 col-md-pull-8 animate-box" data-animate-effect="fadeInRight">
						<div class="mt">
							<h3>Real Project For Real Solutions</h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. </p>
							<ul class="list-nav">
								<li><i class="icon-check"></i>Far far away, behind the word</li>
								<li><i class="icon-check"></i>There live the blind texts</li>
								<li><i class="icon-check"></i>Separated they live in bookmarksgrove</li>
								<li><i class="icon-check"></i>Semantics a large language ocean</li>
								<li><i class="icon-check"></i>A small river named Duden</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="project">
					<div class="col-md-8 col-md-pull-1 animate-box" data-animate-effect="fadeInLeft">
						<!--<img class="img-responsive" src="images/theme/work_1.png" alt="work">-->
					</div>
					<div class="col-md-4 animate-box" data-animate-effect="fadeInRight">
						<div class="mt">
							<div>
								<h4><i class="icon-user"></i>Real Project For Real Solutions</h4>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
							</div>
							<div>
								<h4><i class="icon-video2"></i>Real Project For Real Solutions</h4>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
							</div>
							<div>
								<h4><i class="icon-shield"></i>Real Project For Real Solutions</h4>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>

	<div id="fh5co-testimonial">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Testimonial</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 animate-box">
					<div class="testimony">
						<blockquote>
							&ldquo;Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius&rdquo;
						</blockquote>
						<p class="author"><cite>John Doe</cite></p>
					</div>
				</div>
				<div class="col-md-4 animate-box">
					<div class="testimony">
						<blockquote>
							&ldquo;Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius&rdquo;
						</blockquote>
						<p class="author"><cite>Rob Smith</cite></p>
					</div>
				</div>
				<div class="col-md-4 animate-box">
					<div class="testimony">
						<blockquote>
							&ldquo;Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius&rdquo;
						</blockquote>
						<p class="author"><cite>Jane Doe</cite></p>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="fh5co-started">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Newsletter</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<form class="form-inline">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="email" class="sr-only">Email</label>
								<input type="email" class="form-control" id="email" placeholder="Email">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<button type="submit" class="btn btn-default btn-block">Subscribe</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-4 fh5co-widget">
					<h3>The Company</h3>
					<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
					<p><a href="#">Learn More</a></p>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
					<ul class="fh5co-footer-links">
						<li><a href="#">About</a></li>
						<li><a href="#">Help</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">Terms</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
					<ul class="fh5co-footer-links">
						<li><a href="#">Privacy</a></li>
						<li><a href="#">Testimonials</a></li>
						<li><a href="#">Handbook</a></li>
						<li><a href="#">Help Desk</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
					<ul class="fh5co-footer-links">
						<li><a href="#">Teams</a></li>
						<li><a href="#">API</a></li>
					</ul>
				</div>
			</div>

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2018. EBS-Core Breeding. All Rights Reserved.</small> 
					</p>
					<p>
						<ul class="fh5co-social-icons">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
						</ul>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo Url::base(TRUE);?>/js/theme/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo Url::base(TRUE);?>/js/theme/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo Url::base(TRUE);?>/js/theme/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo Url::base(TRUE);?>/js/theme/jquery.waypoints.min.js"></script>
	<!-- countTo -->
	<script src="<?php echo Url::base(TRUE);?>/js/theme/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo Url::base(TRUE);?>/js/theme/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo Url::base(TRUE);?>/js/theme/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="<?php echo Url::base(TRUE);?>/js/theme/main.js"></script>

	</body>
</html>

