<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders main layout for dashboard widgets
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use app\controllers\SiteController;
use yii\helpers\Url;
use yii\widgets\Pjax;

AppAsset::register($this);
JqueryAsset::register($this);
$layoutObj = SiteController::getLayout(); //get layout preferences
extract($layoutObj);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>EBS-Core Breeding - <?= Html::encode(Yii::$app->controller->module->id) ?></title>
    <?php $this->head() ?>
</head>

<body class="theme-<?= $themeColor ?> " >
<?php $this->beginBody() ?>
<?php
    $this->registerCssFile("@web/css/b4r.css", [
        'depends' => [\yii\bootstrap\BootstrapAsset::className()],
    ], 'b4r-css');
    $this->registerCssFile("@web/css/font-awesome-4.7.0/css/font-awesome.min.css", [
    ], 'font-awesome-css');
    ?>
    <?= \macgyer\yii2materializecss\widgets\Alert::widget(); ?>
    <?= $content ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
