<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders under maintenance page
 */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\controllers\SiteController;
use yii\helpers\Url;
use app\controllers\Dashboard;
use \app\controllers\Constants;
use yii\bootstrap\Modal;

AppAsset::register($this);

$program = Yii::$app->userprogram->get('abbrev'); //current program
$layoutObj = SiteController::getLayout(); //get layout preferences
extract($layoutObj);

$session = Yii::$app->session;
Yii::$app->language  = $language; //set language

$baseUrl = Url::base().'/index.php';
$sendFeedback = '<a href="#" class="btn waves-effect waves-light" id="bug-report-btn" data-target="#bug-report-modal" data-toggle="modal" title="'.\Yii::t('app', 'Contact Us').'">'.\Yii::t('app', 'Contact Us').'<i class="material-icons right">headset_mic</i>
</a>';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="font-size-<?= $fontSize ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="<?php echo Yii::getAlias('@web/images/b4r-logo-circle.png') ?>" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Constants::APP_NAME ?></title>
    <?php $this->head() ?>
</head>

<body class="theme-<?= $themeColor . $bodyClass . $sideNavSkin?> " >
<?php $this->beginBody() ?>
    <div class="background-body-error">
    </div>
    <?php
       
        $bugReportToolUrl = "var bugReportToolUrl = '" . Url::to(['/dashboard/bug-report/loadfields','program'=>$program])."';";
    
        $this->registerJs($bugReportToolUrl, \yii\web\View::POS_HEAD, 'bug-report-tool');
        $this->registerJsFile("@web/js/b4r.js", 
            ['depends' => ['\yii\web\JqueryAsset'],'position' => \yii\web\View::POS_HEAD]);

        $this->registerCssFile("@web/css/b4r.css", [
            'depends' => [\yii\bootstrap\BootstrapAsset::className()],
        ], 'b4r-css');
    ?> 
    <div class="login-intro">
        <?php echo \macgyer\yii2materializecss\widgets\Alert::widget(); ?>
        <div class="home-logo">
            <img src="<?= Yii::getAlias('@web/images/b4r-logo-circle.png') ?>" width="120px"/>
        </div>
        <div class="login-title" style="margin-top: 25px">
            <?= 'B4R Under maintenance' ?>
        </div>

        <div class="login-text">                
        
        <?php
            echo "We apologize for the inconvenience, but we're performing some maintenance at the moment.";
            echo "<br/>Please feel free to contact us, otherwise we'll be back shortly.";

            echo '<br/><br/>';            
            
            echo $sendFeedback;
        
        ?>  
        </div>

        <div class="login-footer" style="margin-top: 50px;">
            <a href="<?= Url::to('https://riceinfo.atlassian.net/wiki/')?>" class="fixed-color" target="_blank">Wiki</a> · 
            <a href="<?= Url::to(getenv('CB_API_URL'))?>" target="_blank" class="fixed-color"> API </a> · <span>© <?= date('Y') ?> <?= Constants::APP_NAME ?></span>
        </div>
    </div>

    <!-- modal for bug reporting -->
    <div id="bug-report-modal" class="fade modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div style="float:left"><h4><?= \Yii::t('app', 'Send Feedback') ?> </h4></div><div style="float:right"></div>
            </div>
            <div class="modal-body bug-report-tools bug-report-modal-body">
            </div>
            <div class="modal-footer">
                <a class="modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Cancel') ?></a>&emsp;
                <a id="bug-report-create-btn" class="btn btn-primary waves-effect waves-light" href="#" url="#"><?= \Yii::t('app', 'Submit') ?><i class="material-icons right">send</i></i></a>&emsp;
            </div>
            </div>
        </div>
    </div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<style>
    .background-body-error{
        background-color: #f1f4f5;
        height: 610px;
        width: 100%;
        position: fixed;
        box-shadow: -25px 28px 68px -18px rgba(0,0,0,0.75);
        -moz-box-shadow: -25px 28px 68px -18px rgba(0,0,0,0.75);
    }

    body{
        background-color: #009688;
        font-family: "Roboto", sans-serif;
        line-height: 1.4!important;
        letter-spacing: -.1px!important;
        -webkit-font-smoothing: auto;
    }

    .dark-side-nav .background-body-error, .dark-side-nav {
        background-color: #262933 !important;
    }

    .dark-side-nav .login-intro, .dark-side-nav .login-footer, .dark-side-nav .login-footer a, .dark-side-nav .error-message{
        color: #fff !important;
    }

    .login-intro{
        position: fixed;
        padding: 75px 128px 100px 150px;
        color:#000;
    }

    .home-logo{
        margin-bottom: 10px;
        animation: 0.5s ease-out 0s 1 slideInFromTop;
    }

    .login-title{
        font-size: 42px;
        font-weight: 300;
        line-height: 1;
        animation: 0.5s ease-out 0s 1 slideInFromLeft;
    }

    @keyframes slideInFromLeft {
        0% {
            transform: translateX(-100%);
        }
        100% {
            transform: translateX(0);
        }
    }

    @keyframes slideInFromBottom {
        0% {
            transform: translateY(100%);
        }
        100% {
            transform: translateY(0%);
        }
    }
    @keyframes slideInFromTop {
        0% {
            transform: translateY(-100%);
        }
        100% {
            transform: translateY(0%);
        }
    }

    .login-text{
        padding-top: 16px;
        font-size: 14px;
        max-width: 780px;
        color: #333;
        animation: 0.5s ease-out 0s 1 slideInFromBottom;
    }

    .dark-side-nav .login-text{
        color: rgba(255,255,255,.54);
    }

    .login-footer{
        margin-top:20px;
        color: #333;
        animation: 0.5s ease-out 0s 1 slideInFromBottom;
    }

    .login-footer a{
        font-weight: bold;
        color: #000 !important;
    }

    * {
        text-rendering: optimizeLegibility;
    }

    @media only screen and (max-width: 992px){
        .login-intro{
            padding: 108px 30px 40px 40px !important;
        }
        .login-title{
            font-size: 38px;
        }
       
    }

    @media only screen and (max-width: 1366px){
        .login-intro{
            padding: 128px;
        }
    }

    @media only screen and (max-width: 944px){
      
        .dark-side-nav .login-intro, .dark-side-nav .login-footer, .dark-side-nav .login-footer a{
            background-color: transparent;
        }
    }

    @media only screen and (max-width: 760px){
        
        .dark-side-nav .login-intro, .dark-side-nav .login-footer, .dark-side-nav .login-footer a{
            background-color: #262933;
        }
    }

    body.theme-rosegold {
        background-color: #ee6e73 !important;
    }
    body.theme-blue{
        background-color: #1565C0 !important
    }
    body.theme-light-green{
        background-color: #33691e !important;
    }
    body.theme-cyan{
        background-color: #00838f !important;
    }
    body.theme-blue-grey{
        background-color: #455a64 !important;
    }
    body.theme-deep-purple{
        background-color: #512da8 !important;
    }
    body.theme-red{
        background-color: #C62828 !important;
    }
    .alert{
        position: absolute;
        top:0px;
    }
    .alert.error > .card-panel,.alert.success > .card-panel {
        background: transparent;
    }
    .card-panel{
        -webkit-box-shadow: none;
        box-shadow: none;
    }
</style>

<script type="text/javascript">
    // trigger click feedback button to render modal
    $(document).ready(function() {
        $('#bug-report-btn').click();
    });
</script>>