#!/bin/bash 

CB_UI_COVERAGE_XML=$1
PASSING_COVERAGE=60

# colors
RED='\033[0;37;41m'
GREEN='\033[0;30;42m'
YELLOW='\033[0;30;43m'
NO_COLOR='\033[0m'

# checks if there is coverage file in argument
if [ -z "$CB_UI_COVERAGE_XML" ]
then
    printf "${RED}Code coverage file not entered. Try again.${NO_COLOR}\n";
    exit 1
fi

lineCount=$(wc -l < ${CB_UI_COVERAGE_XML})
overallMetrics=$((lineCount-2))
coverageStr=$(sed -n ${overallMetrics}p ${CB_UI_COVERAGE_XML})

IFS=" "
read -ra coverageStrArr <<< "$coverageStr"

for i in "${coverageStrArr[@]}"
do
    case $i in
        "methods"*)
            methodStr=$i
            ;;
        "coveredmethods"*)
            coveredMethodStr=$i
            ;;
        "statements"*)
            lineStr=$i
            ;;
        "coveredstatements"*)
            coveredLineStr=$i
            ;;
    esac
done

IFS="\""
read -ra methodArr <<< "$methodStr"
read -ra coveredMethodArr <<< "$coveredMethodStr"
read -ra lineArr <<< "$lineStr"
read -ra coveredLineArr <<< "$coveredLineStr"

lastIndex=$((${#methodArr[@]}-1))

method=${methodArr[$lastIndex]}
coveredMethod=${coveredMethodArr[$lastIndex]}
line=${lineArr[$lastIndex]}
coveredLine=${coveredLineArr[$lastIndex]}

methodCoverage=$(echo "(($coveredMethod/$method)*100)" | bc -l)
lineCoverage=$(echo "(($coveredLine/$line)*100)" | bc -l)

if (( $(echo "$methodCoverage < $PASSING_COVERAGE" |bc -l) ))
then
    echo "FALSE"
fi