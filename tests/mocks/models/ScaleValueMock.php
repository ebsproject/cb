<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IScaleValue;
use app\tests\mocks\Mock;


class ScaleValueMock extends Mock implements IScaleValue
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ScaleValueBase the static model class
     */
	public static function model($className=__CLASS__){

    }

    /**
     * @return array relational rules.
     */
	public function relations(){

    }

	/**
     * Retrieves an existing variable scale values in the database
     * @param $id integer variable Id
     * @return array variable scale values of a variable in the database
     */
    public static function getVariableScaleValues($id, $param){

    }

}