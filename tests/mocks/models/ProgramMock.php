<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the template for Mock Classes
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IProgram;
use app\tests\mocks\models\BaseModelMock;

class ProgramMock extends BaseModelMock implements IProgram
{
    /**
     * Get program name and abbrev by program id
     * @param $id integer program identifier
     * @return $program text program name and abbrev
     */
    public function getProgramNameById($id) {
        return $this->getReturnValue('getProgramNameById');
    }

    /**
     * Get program attribute
     *
     * @param $condAttr text attribute in where condition
     * @param $condVal text program identifier
     * @param $attr text attribute to be retrieved
     *
     * @return $program text program attribute
     */
    public function getProgramAttr($condAttr = 'id', $condVal = '', $attr = 'abbrev') {
        return $this->getReturnValue('getProgramAttr');
    }

    /**
     * Get configurations by program code
     *
     * @param $programCode text program program code
     * @return $data array program value
     */
    public function getProgramByProgramCode($programCode) {
        return $this->getReturnValue('getProgramByProgramCode');
    }

     /**
     * Get configurations by abbrev
     *
     * @param $id integer program id
     * @return $data array program record
     */
    public function getProgram($id) {
        return $this->getReturnValue('getProgram');
    }

    /**
     * Retrieves the crop code of the given program code
     * @param String programCode program code value
     * @return String crop code value
     */
    public function getCropCode($programCode) {
        return $this->getReturnValue('getCropCode');
    }
}