<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class for EntryList
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IEntryList;
use app\tests\mocks\models\BaseModelMock;

class EntryListMock extends BaseModelMock implements IEntryList
{
    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
    }

    /**
     * Returns entry lists from a specific experiment id
     * @param experimentDbId integer entry identifier
    */
    public function getEntryListsSearch($experimentDbId){
    }

    /**
     * Retrieve entry list/s given a specific experiment ID
     *
     * @param $experimentId integer experiment record identifier
     * 
     */
    public function getEntryList($experimentId){
    }
    
    /**
     * Return the entry list id of the experiment, create record if does not exist
     *
     * @param Integer $experimentId record id of the experiment
     * @param Integer $toCreate flag for creating entry list
     */
    public function getEntryListId($experimentId, $toCreate=false){
    }


    /**
     * Checks if an entry list name exists in the database (case-sensitive)
     *
     * @param string $name entry list name
     * @param int $exceptId entry list identifier to exclude (useful for checking against oneself)
     * @return bool true if it exists, false otherwise
     */
    public function nameExists($name, $exceptId=null){
    }

     /**
     * Search for entry list given search parameters
     *
     * @param $params array list of search parameters
     * @return $data array list of entry list given search parameters
     */
    public static function searchRecords($params){
    }

}