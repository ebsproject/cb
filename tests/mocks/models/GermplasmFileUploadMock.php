<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class for models\GermplasmFileUpload
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IGermplasmFileUpload;
use app\tests\mocks\models\BaseModelMock;

class GermplasmFileUploadMock extends BaseModelMock implements IGermplasmFileUpload
{
    /**
     * Search model for germplasm file upload
     * @param $params array list of parameters
     * @param $filters array list of additional search filters
     * 
     * @return $dataProvider array data provider for germplasm file upload browser
     */
    public function search($params, $filters){
        return $this->getReturnValue('search');
    }

    /**
     * Facilitate the extraction of browser search parameters and filters
     * 
     * @param Array $filters array of current filters
     * 
     * @return Array values to be used in searching file upload records
     */
    public function extractBrowserFilters($filters){
        return $this->getReturnValue('extractBrowserFilters');
    }

    /**
     * Facilitates retrieval of germplasm file upload status scale values
     * 
     * @return array variable configuration information
     */
    public function getFileUploadStatusValues(){
        return $this->getReturnValue('getFileUploadStatusValues');
    }

    /**
     * Build configuration abbrev based on level
     * 
     * @param String $entity entity
     * @param String $action action
     * @param String $level data level
     * @param String $levelValue data level value
     * 
     * @return $configAbbrev
     */
    public function buildConfigAbbrev($entity,$action,$level,$levelValue){
        return $this->getReturnValue('buildConfigAbbrev');
    }

    /**
     * Retrieve configuration values
     * 
     * @param String $entity entity of the file upload transaction (germplasm/germplasm-relation/germplasm-attribute)
     * @param String $action germplasm manager capability to be used (create/merge/update)
     * @param String $userRole user role value
     * @param String $program dashboard program value
     * @param String $cropCode crop code value for program
     * 
     * @return mixed
     */
    public function getConfigValues($entity,$action,$userRole,$program,$cropCode){
        return $this->getReturnValue('getConfigValues');
    }

    /**
     * Retrieve variables from configuration record matching entity, action, and level
     * 
     * @param Array $configValues configuration values
     * 
     * @return mixed
     */
    public function getFileUploadVariables($configValues){
        return $this->getReturnValue('getFileUploadVariables');
    }

    /**
     * Facilitates creation of new germplasm merge file upload record
     * @param Array $paramRequest parameter body build on controller
     * 
     * @return mixed
     */
    public function saveUploadedFile($paramRequest){
        return $this->getReturnValue('saveUploadedFile');
    }
}

?>