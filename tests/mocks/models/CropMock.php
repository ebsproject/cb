<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class for Crop
 */

namespace app\tests\mocks\models;

use app\interfaces\models\ICrop;
use app\tests\mocks\models\BaseModelMock;

class CropMock extends BaseModelMock implements ICrop
{
    /**
     * Set api endpoint
     */
    public static function apiEndPoint(){

    }

    /**
     * Retrieves an existing crop in the database
     * @param $id integer crop Id
     * @return array crop record in the database
     */
    public function getCrop($id){
        return $this->getReturnValue('getCrop');
    }

    /**
     * Retrieves an existing crop in the database
     * @param $id integer crop Id
     * @return array crop record in the database
     */
    public function getCropByAbbrev($abbrev){
        return $this->getReturnValue('getCropByAbbrev');
    }

}