<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the template for Mock Classes
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IVariable;
use app\tests\mocks\models\BaseModelMock;

class VariableMock extends BaseModelMock implements IVariable
{
    /**
     * @inheritdoc
     */
    public function tableName()
    {
        
    }

    /**
     * Checks if a string(haystack) starts with a specific string(needle)
     * 
     * @param string haystack string to search in
     * @param string needle string to check
     * @reference https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     */
    public function startsWith($haystack, $needle)
    {
        
    }
    
    /**
     * Checks if a string(haystack) ends with a specific string(needle)
     * 
     * @param string haystack string to search in
     * @param string needle string to check
     * @reference https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     */
    public function endsWith($haystack, $needle)
    {
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionaryAttributes()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterEntities()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterFacilityDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterFacilityMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterFormulas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterFormulaParameters()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterGeolocationDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterGeolocationMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterItemMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterPlaceDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterPlaceMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProductMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProgramMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterProgramPlaceMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterRecordVariables()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterSchemeMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterSchemeRelationMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterTvpMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterUserMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterUserVariableSetMembers()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethod()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScale()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifier()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTargetVariable()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariableBases()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberVariable()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariableBases0()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterVariableMappings()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterVariableMappings0()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterVariableRelations()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterVariableRelations0()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterVariableResults()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterVariableResults0()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterVariableSetMembers()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalCrossDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalCrossMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalEntryDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalEntryMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalFieldLayoutMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalFieldLayoutStudyMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalPlotDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalPlotMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalSeedStorageDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalSeedStorageMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalStudyMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalSubplotDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalSubplotMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalTempEntryDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalTempEntryMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalTempPlotDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalTempPlotMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalDataTerminalEntryDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalDataTerminalEntryMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalDataTerminalPlotDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalDataTerminalPlotMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationalDataTerminalStudyMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatformSavedListMemberMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatformSavedListMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeedWarehouseSampleMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeedWarehouseShipmentDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeedWarehouseShipmentItemMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeedWarehouseShipmentMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseTerminalAnalysisTransactions()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseTerminalEntryDatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseTerminalEntryMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseTerminalPlotDatas()
    {
        
    }

    /** 
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseTerminalPlotMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseTerminalStudyMetadatas()
    {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseTerminalStudyVariables()
    {
        
    }

    /**
     * Retrieve variable information in view variable
     *
     * @param $id integer variable identifier
     * @return $data array variable information
     */
    public function getVariableInfo($id)
    {
        
    }

    /**
     * Sort array by value
     *
     * @param $arr array array to be sorted
     * @param $col text column to be sorted
     * @param $dir text directing of sorting
     */
    public function arraySortByColumn($arr, $col, $dir = SORT_ASC)
    {
        
    }

    /**
     * Process ordering of data to be displayed
     *
     * @param $arr list of attributes
     * @param $order order of display of attributes
     * @return $array ordered list of attributes
     */
    public function processOrderOfData($arr, $order)
    {
        
    }

    /**
     * Get variable label by abbrev
     *
     * @param $abbrev text variable abbreviation
     */
    public function getAttrByAbbrev($abbrev,$attr='label')
    {
        
    }

    /**
     * Get variable info by abbrev
     *
     * @param $abbrev text variable abbrev
     * @return $data array variable value
     */
    public function getVariableByAbbrev($abbrev, $fields = [])
    {
        return $this->getReturnValue('getVariableByAbbrev');
    }

    /**
     * Retrive variable scales
     * 
     * @param $variableId varaible record identifier
     */
    public function getVariableScales($variableId)
    {
        return $this->getReturnValue('getVariableScales');
    }

    /**
     * Retrive tags for variable scale value - scale name pair
     * 
     * @param $scaleValues array list of scale values info
     */
    public function getVariableScaleTags($scaleValues)
    {
        return $this->getReturnValue('getVariableScaleTags');
    }

    /**
     * Method for retrieving all the scale value records of a variable using the POST search method
     * @param Integer $variableDbId Variable identifier
     * @param Array $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @return Array the records from the database
     */
    public function searchAllVariableScales($variableDbId, $params = null, $filters = '', $retrieveAll = true)
    {
        return $this->getReturnValue('searchAllVariableScales');
    }

    /**
     * Get variable attribute by abbrev
     *
     * @param text $abbrev abbreviation of the variable
     * @param text $attribute attribute to be retrieved
     *
     * @return text variable attribute value
     */
    public function getAttributeByAbbrev($abbrev, $attribute) {
        return $this->getReturnValue('getAttributeByAbbrev');
    }
}