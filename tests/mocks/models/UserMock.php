<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the template for Mock Classes
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IUser;
use app\tests\mocks\models\BaseModelMock;

class UserMock extends BaseModelMock implements IUser
{
    public function getAuthKey()
    {
        return $this->getReturnValue('getAuthKey');
    }

    public function validateAuthKey($authKey)
    {
        return $this->getReturnValue('validateAuthKey');
    }

    /**
     * Retrieves program where user belongs to
     *
     * @param $userId integer user identifier
     * @return $programId integer program where user belongs to
     */
    public function getUserProgram($userId){
        return $this->getReturnValue('getUserProgram');
    }

    /**
     * Retrieves all program filter tags where user has access to.
     * If the user is an admin, all programs are retrieved,
     * except when $ignoreAdmin is set to `true`, in which case
     * only programs which the user is a member of will be retrieved.
     *
     * @param Integer $userId integer user identifier
     * @param String $options string of url options
     * @param Boolean $ignoreAdmin (optional) whether or not the admin role will be checked.
     *      If set to `true`, only the programs which the admin user is a member of will be retrieved.
     * @return $result array list of program tags where user has access to
     */
    public function getUserPrograms($userId, $options = '', $ignoreAdmin = false) {
        return $this->getReturnValue('getUserPrograms');
    }

    /**
     * Retrieves all teams where user belongs to
     *
     * @param $userId integer user identifier
     * @param $removeSystemGroup boolean if true, system group team will not be part of the results
     * @return $result array list of teams user belongs to
     */
    public function getUserTeams($userId, $removeSystemGroup=false){
        return $this->getReturnValue('getUserTeams');
    }
     /**
     * Retrieves all team ids where user belongs to
     *
     * @param $userId integer user identifier
     * @param $removeSystemGroup boolean if true, system group team will not be part of the results
     * @return $result array list of teams user belongs to
     */
    public function getUserTeamsId($userId, $removeSystemGroup=false){
        return $this->getReturnValue('getUserTeamsId');
    }

    /**
     * Retrieves all team names where user belongs to
     *
     * @param $userId integer user identifier
     * @param $removeSystemGroup boolean if true, system group team will not be part of the results
     * @return $result array list of teams user belongs to
     */
    public function getUserTeamsName($userId, $removeSystemGroup=false){
        return $this->getReturnValue('getUserTeamsName');
    }

    /**
     * Retrieves user id of currently logged in user
     * @return $userId integer user identifier 
     */
    public function getUserId(){
        return $this->getReturnValue('getUserId');
    }

    /**
     * Retrieves user information
     * @param $attr text attribute user wants to get
     * @return $data object list of user information
     */
    public function getUserInfo($attr){
        return $this->getReturnValue('getUserInfo');
    }

    /**
     * Check if user is admin or not
     *
     * @return $userType boolean whether current logged in user is admin or not
     */
    public function isAdmin(){
        return $this->getReturnValue('isAdmin');
    }

    /**
     * Retrieves user information by id
     * @param $attr text attribute user wants to get
     * @return $data object list of user information
     */
    public function getUserInfoById($attr,$id){
        return $this->getReturnValue('getUserInfoById');
    }


    /**
     * Get the display name of the given user
     * @param $id integer user identifier
     * @return $displayName string display name of user
     */
    public function getDmsPersonDisplayName($id){
        return $this->getReturnValue('getDmsPersonDisplayName');
    }

    /**
     * Retrieves teams information of currently logged in user
     * @return $result array list of teams information
     */
    public function getUserTeamsInfo($userId=null){
        return $this->getReturnValue('getUserTeamsInfo');
    }

    /**
     * Adds user to team with the assigned role
     *
     * @param $userId integer user identifier
     * @param $teamId integer team identifier
     * @param $roleId integer role identifier
     */
    public function addUserToTeam($userId,$teamId,$roleId){
        return $this->getReturnValue('addUserToTeam');
    }

    /**
     * Refreshes user's session
     */
    public function refreshUserSession(){
        return $this->getReturnValue('refreshUserSession');
    }

    /**
     * Check if user has access to program
     *
     * @param $program text program abbrev identifier
     * @return $hasAccess boolean whether user has access to program or not
     */
    public function checkIfUserHasAccessToProgram($program){
        return $this->getReturnValue('checkIfUserHasAccessToProgram');
    }

    /**
    * Process user parameters
    *
    * @param object $userParams unprocessed parameters
    * @param string $method API method identifier 
    * 
    * @return object $processedParams contains record(s) of processed params
    */
    public function processUserParams ($userParams, $method=null){
        return $this->getReturnValue('processUserParams');
    }

    /**
     * Create a user account
     *
     * @param object $params POST parameters
     * 
     * @return mixed $data created user info
     */
    public function createUser ($params){
        return $this->getReturnValue('createUser');
    }

    /**
     * Retrieves registered users in the database given search parameters
     * 
     * @return array collection of registered users in the database
     * 
     */
    public function searchUsers($params){
        return $this->getReturnValue('searchUsers');
    }

    /** 
     * Returns an array data provider containing seasons info
     * 
     * @return mixed data provider for users
    */
    public function getUsersArrayDataProvider (){
        return $this->getReturnValue('getUsersArrayDataProvider');
    }

    /**
     * Retrieves registered users in the database
     * 
     * @return mixed collection of registered users in the database
     */
    public function getAllUsers (){
        return $this->getReturnValue('getAllUsers');
    }

    /**
     * Loads a model's props with an instance's values
     * 
     * @param integer $id unique identifier
     * 
     * @return User $model info of user
     */
    public function getInitializedUserModel ($id){
        return $this->getReturnValue('getInitializedUserModel');
    }

    /**
     * Retrieves a registered user in the database
     * 
     * @param integer $id unique identifier
     * 
     * @return object $data info of user
     */
    public function getUser ($id){
        return $this->getReturnValue('getUser');
    }

    /**
     * Retrieve person information in view person
     *
     * @param $id integer person identifier
     * @return $data array person information
     *
     */
    public function getPersonInfo($id){
        return $this->getReturnValue('getPersonInfo');
    }

    /**
     * Update a registered user's information
     *
     * @param $id integer user identifier
     * @param $params object PUT parameters
     * 
     * @return boolean
     */
    public function updateUser ($id, $params){
        return $this->getReturnValue('updateUser');
    }

    /**
     * Deletes a registered user in the database 
     *
     * @param $id integer user identifier
     * 
     * @return boolean
     */
    public function deleteUser ($id){
        return $this->getReturnValue('deleteUser');
    }

    /**
     * Checks if a given email address already exists in the database
     * @param $email string
     * @param $userId integer user identifier
     * @return boolean
     * 
     */
    public function isEmailAddressExisting($email, $userId = null, $action){
        return $this->getReturnValue('isEmailAddressExisting');
    }
    
    /**
     * Retrieves person by email
     *
     * @param $email text email of user
     * 
     * @return $result array person information
     */
    public function getPersonInfoByEmail($email){
        return $this->getReturnValue('getPersonInfoByEmail');
    }
}