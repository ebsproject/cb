<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class  for models\PlantingJob
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IPlantingJob;
use app\tests\mocks\models\BaseModelMock;

class PlantingJobMock extends BaseModelMock implements IPlantingJob
{
    /**
     * Get class of status value
     *
     * @param Text $status Status of Planting job
     * @return Text $class Class of status
     */
    public function getStatusClass($status)
    {
        return $this->getReturnValue('getStatusClass');
    }

    /**
     * Format string index to camelCase
     * 
     * @param $index string attribute index
     */
    public function formatToCamelCase($index){
        return $this->getReturnValue('formatToCamelCase');
    }

    /**
     * Get processed summary for view
     *
     * @param Array $summary Planting job summary
     * @return Array $processedSummary Processed summary
     */
    public function getProcessedSummary($summary)
    {
        return $this->getReturnValue('getProcessedSummary');
    }

    /**
     * Get processed additional data for view
     *
     * @param Array $plantingJobData Planting job additional data
     * @return Array $addtlInstructions Additional instructions
     */
    public function getProcessedAdditionalData($plantingJobData)
    {
        return $this->getReturnValue('getProcessedAdditionalData');
    }

    /**
     * Get processed widget options
     *
     * @param Array $data Config data
     * @return Array Data endpoing and widget options
     */
    public function getConfigForm($data)
    {
        return $this->getReturnValue('getConfigForm');
    }

}