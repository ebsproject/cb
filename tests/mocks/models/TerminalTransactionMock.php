<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the template for Mock Classes
 */

namespace app\tests\mocks\models;

use app\interfaces\models\ITerminalTransaction;
use app\tests\mocks\models\BaseModelMock;

class TerminalTransactionMock extends BaseModelMock implements ITerminalTransaction
{
    /**
     * Search functionality for data browser
     * 
     * @param Array $params contains the filter parameters
     * @return ArrayDataProvider the data provider for the browser
     */
    public function search($params=null) {
        return $this->getReturnValue('search');
    }

    /**
     * Delete a transaction given ID
     * @param Integer $transactionDbId ID of the transaction record
     * @return Array API response
     */
    public function delete($transactionDbId=null) {
        return $this->getReturnValue('delete');
    }

    /**
     * Retrieves the available files given transaction ID
     * @param Integer $transactionDbId ID of the transaction
     * @return Array contains the file records and total count
     */
    public function getFiles($transactionDbId=null) {
        return $this->getReturnValue('getFiles');
    }
    
    /**
     * Retrieves the most recent file given transaction ID
     * @param Integer $transactionDbId ID of the transaction
     * @return Array contains the file records and total count
     */
    public function getRecentFileId($transactionDbId=null) {
        return $this->getReturnValue('getRecentFileId');
    }

    /**
     * Retrieves the dataset summary given transaction ID
     * @param Integer $transactionDbId ID of the transaction
     * @param Boolean $retrieveOccurrences return occurrence list or not
     * @return Array contains the variable records and basic statistics
     */
    public function getDatasetSummary($transactionDbId, $retrieveOccurrences = true) {
        return $this->getReturnValue('getDatasetSummary');
    }

    /**
     * Method for retrieving all the dataset records in table format from a given transactionDbId using the POST search method
     * @param Integer $transactionDbId ID of the transaction
     * @param Json $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @return Array the records from the database
     */
    public function searchAllDatasetsTable($transactionDbId, $params=null, $filters='') {
        return $this->getReturnValue('searchAllDatasetsTable');
    }

    /**
     * Method for retrieving all the dataset records from a given transactionDbId using the POST search method
     * @param Integer $transactionDbId ID of the transaction
     * @param Json $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @param Boolean retrieveAll true if to retrieve all record, false = only the default or specified limit of records
     * @return Array the records from the database
     */
    public function searchAllDatasets($transactionDbId, $params=null, $filters='', $retrieveAll = true) {
        return $this->getReturnValue('searchAllDatasets');
    }

    /**
     * Method for creating one or more file records in the transaction
     * @param Integer $transactionDbId ID of the transaction
     * @param Array $requestData contains the information about the records
     * @return Array API response
     */
    public function createFiles($transactionDbId, $requestData) {
        return $this->getReturnValue('createFiles');
    }

    /**
     * Method for computation of traits
     * @param Integer $transactionDbId ID of the transaction
     * @param Array $requestData contains the information about the records
     * @return Array API response
     */
    public function variableComputations($transactionDbId, $requestData) {
        return $this->getReturnValue('variableComputations');
    }

    /**
     * Create a new dataset record for a specific and existing transaction record given a transaction ID
     *
     * @param Integer transactionId transaction identifier
     * @param Array requestBody API request body
     * @return Array API response
     */
    public function createDataset($transactionId, $requestBody)
    {
        return $this->getReturnValue('createDataset');
    }

    /**
     * Validate the dataset of a specific and existing transaction record given a transaction ID
     *
     * @param Integer transactionId transaction identifier
     * @return Array API response
     */
    public function validateDataset($transactionId)
    {
        return $this->getReturnValue('validateDataset');
    }
}