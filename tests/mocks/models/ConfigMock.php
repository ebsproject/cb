<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the template for Mock Classes
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IConfig;
use app\tests\mocks\models\BaseModelMock;

class ConfigMock extends BaseModelMock implements IConfig
{
    /**
     * Get configurations by abbrev
     *
     * @param $abbrev text config abbrev
     * @return $data array config value
     */
    public function getConfigByAbbrev($abbrev) {
        return $this->getReturnValue('getConfigByAbbrev');
    }
    
    /**
     * Get the query configuration depending on the entity type
     * 
     * @param String $entity the specified database entity, ie PRODUCT
     * @param String $returnType specifies whether to return a json object or an array
     * @return JSON/ARRAY the configuration value from the database
     */

    public function getQueryConfig ($entity, $returnType='jsonObject') {
        return $this->getReturnValue('getQueryConfig');
    }

    /**
     * Retrieve configuration of form
     * by configuration abbreviation and step
     *
     * @param Text $abbrev Abbreviation of configuration
     * @param Text $step Step to be retrieved
     *
     * @return Array $fields Fields configuration 
     */
    public function getFormConfig($abbrev, $step=null) {
        return $this->getReturnValue('getFormConfig');
    }
}