<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class for models\Person
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IGermplasm;
use app\tests\mocks\models\BaseModelMock;

class GermplasmMock extends BaseModelMock implements IGermplasm
{
    /**
     * API endpoint for tenantTeam
     */
    public function apiEndPoint ()
    {
        $this->getReturnValue('apiEndPoint');
    }

    /**
     * Retrieves existing distinct germplasm types in the database
     *
     * @return array distinct germplasm types
     */
    public function getGermplasmState(){
        return $this->getReturnValue('getGermplasmState');
    }

    /**
     * Retrieves existing distinct germplasm generation in the database
     *
     * @return array distinct germplasm generation
     */
    public function getGermplasmGeneration(){
        return $this->getReturnValue('getGermplasmGeneration');
    }

    /**
     * Updates germplasm given id and array of data
     *
     * @param $id integer germplasm identifier
     * @param $data array array of values
     *
     * @return $response text return message of API call
     */
    public function updateGermplasm($id, $data){
        return $this->getReturnValue('updateGermplasm');
    }

    /**
     * Search for germplasm given search parameters
     *
     * @param $params array list of search parameters
     * @return $data array list of germplasm given search parameters
     */
    public function searchGermplasm($params){
        return $this->getReturnValue('searchGermplasm');
    }

    /** 
     * Retrieves the number of entries associated with the given germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * @return Array germplasm entries count
     */
    public function getGermplasmEntriesCount($germplasmId){
        return $this->getReturnValue('getGermplasmEntriesCount');
    }

    /** 
     * Retrieves the number of seeds associated with the given germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * @return Array germplasm seeds count
     */
    public function getGermplasmSeedsCount($germplasmId){
        return $this->getReturnValue('getGermplasmSeedsCount');
    }
}
?>