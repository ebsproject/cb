<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class  for models\Application
 */

namespace app\tests\mocks\models;

use app\interfaces\models\IOccurrence;
use app\tests\mocks\models\BaseModelMock;

class OccurrenceMock extends BaseModelMock implements IOccurrence
{
    /**
     * API endpoint for occurrence
     */
    public function apiEndPoint ()
    {
        $this->getReturnValue('apiEndPoint');
    }

    /**
     * Search for an occurrence given search parameters
     *
     * @param $filter search parameters
     * @param $category text current filter category
     * @param $categorySelectedId integer selected category item
     * @return $data array list of occurrences given search parameters
     */
    public function searchOccurrences ($filter, $category, $categorySelectedId, $page)
    {
        return $this->getReturnValue('searchOccurrences');
    }

    /**
     * Get occurrence by ID
     *
     * @param $id integer occurrence identifier
     * @return $data occurrence information
     */
    public function get ($id)
    {
        return $this->getReturnValue('get');
    }

    /**
     * Get occurrence info for detail view
     *
     * @param $id integer occurrence identifier
     * @return $data array occurrence information
     */
    public function getDetailViewInfo ($id)
    {
        return $this->getReturnValue('getDetailViewInfo');
    }

    /**
     * Get occurrence data by occurrence ID
     *
     * @param $id integer occurrence identifier
     * @return $data occurrence data values
     */
    public function getDataByOccurrenceId ($id)
    {
        return $this->getReturnValue('getDataByOccurrenceId');
    }

    /**
     * Find Experiments in browser detail view catgory
     *
     * @return $results list of experiments
     */
    public function findExperimentCategories ()
    {
        return $this->getReturnValue('findExperimentCategories');
    }

    /**
     * Retrieve occurrence configuration by step
     *
     * @param text $step step to retrieve in config
     * @param 
     * @return array $config field configuration for the step
     */
    public function getConfig ($id, $step)
    {
        return $this->getReturnValue('getConfig');
    }

    /**
     * Save occurrence basic data
     *
     * @param integer $id occurrence identifier
     * @param array $formData occurrence info to save
     * @return boolean $result if successful or not
     */
    public function saveBasicData ($id, $formData)
    {
        return $this->getReturnValue('saveBasicData');
    }
    
    /**
     * Retrieve occurrence data provider
     * 
     * @param Integer $experimentDbId experiment record identifier
     * @param Array $configData configuration information
     * @param Array $params optional data parameters
     * @param Boolean $noPagination optional flag when grid view pagination should be set or not
     */
    public function getDataProvider ($experimentDbId, $configData, $params = [], $noPagination = false)
    {
        return $this->getReturnValue('getDataProvider');
    }
    
    /**
     * Retrieve plots data provider
     * 
     * @param Integer $occurrenceId occurrence record identifier
     * 
     */
    public function getPlotDataProvider ($occurrenceId)
    {
        return $this->getReturnValue('getPlotDataProvider');
    }

    /**
     * Retrieve plot information given a specific occurrence id
     * 
     * @param Integer $occurrenceDbId occurrence record identifier
     * @param Array $params optional parameters
     * @param String $filters optional filters
     * 
     */
    public function searchAllPlots ($occurrenceDbId, $params = null, $filters = '', $getAll = false)
    {
        return $this->getReturnValue('searchAllPlots');
    }

    /**
     * Update Experiment status
     *
     * @param integer $occurrenceDbId occurrence identifier
     */
    public function updateExperimentStatus ($occurrenceDbId)
    {
        return $this->getReturnValue('updateExperimentStatus');
    }

    /**
     * Arrange API-retrieved occurrence and plot data
     * into content for a mapping file
     *
     * @param mixed $occurrenceInstance - A set of Occurrence data
     * @param array $plots - Array of Plot data to append to mapping file content
     *
     * @return mixed - Contains CSV headers, occurrence, experiment, and plot information
     */
    public function arrangeDataIntoMappingFileData ($occurrenceInstance, $plots)
    {
        return $this->getReturnValue('arrangeDataIntoMappingFileData');
    }


    /**
     * Get percentage of count over total count
     *
     * @param Integer $count actual count
     * @param Integer $totalCount total count
     *
     * @return Float $status percentage  
     */
    public function getStatusPercentage ($count, $totalCount)
    {
        return $this->getReturnValue('getStatusPercentage');
    }

    /**
     * Retrieves occurrence info in the format
     * applicable to select2 elements
     * @param requestBody - search parameters
     * @param urlParams - sort, limit, and page
     */
    public function getOccurrenceSelect2Data($requestBody = null, $urlParams = '') {
        return $this->getReturnValue('getOccurrenceSelect2Data');
    }

    /** 
     * Retrieve each Occurrence's parent Experiment's Type and Status and
     * associated Location ID given an Occurrence ID
     * 
     * @param array $occurrenceDbIds Currently selected Occurrence(s) from
     *                                        Experiment Manager data browser
     * 
     * @return array An array of occurrence info containing Experiment Name, Type, Status,
     *               and Location ID
    */
    public function getOccurrenceExperimentInfoAndLocationDbId ($occurrenceDbIds) {
        return $this->getReturnValue('getOccurrenceExperimentInfoAndLocationDbId');        
    }
}