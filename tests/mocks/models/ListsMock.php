<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /**
  * Mock class for Lists
  */
namespace app\tests\mocks\models;

use app\interfaces\models\ILists;
use app\tests\mocks\models\BaseModelMock;

class ListsMock extends BaseModelMock implements ILists
{
    public function searchAllMembers($dbId, $params=null, $filters='', $retrieveAll=true) {
        return $this->getReturnValue('searchAllMembers');
    }

    public function createMembers($listId, $members = [], $backgroundProcess = false, $additionalParams = [], $memberIdsOnly=false, $parseMembers=true) {
        return $this->getReturnValue('createMembers');
    }
}