<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mocked class for the BaseModel class.
 */

namespace app\tests\mocks\models;

use app\tests\mocks\Mock;
use app\interfaces\models\IWorker;

class WorkerMock extends Mock implements IWorker
{
    /**
     * Creates a background job record and establishes a connection to the specified worker
     * @param string $workerName - name of the worker to connect to
     * @param string $description - description of the worker
     * @param string $entity - reference to the entity where the background job is used
     * @param string $entityDbId - identifier of the entity
     * @param string $endpointEntity - reference to the entity where the background job is used
     * @param string $application - reference to the application where the background job is used
     * @param string $method - method used in the background job
     * @param array $workerData - data to pass to the worker
     * @param array $additionalParams - (optional) additional data for the background job record insertion
     */
    public function invoke($workerName, $description, $entity, $entityDbId, $endpointEntity, $application, $method, $workerData, $additionalParams = null){
        return $this->getReturnValue('invoke');
    }
}