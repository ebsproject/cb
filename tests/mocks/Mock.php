<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the parent Mock class. 
 * This class contains the functions
 * needed by mocked classes.
 * Extend this Mock class in your mocked classes.
 */

namespace app\tests\mocks;

class Mock
{
    /**
     * @property returnValues - array containing return values
     */
    protected $returnValues;

    /**
     * Set the return value(s) of a mocked function
     * @param functionName - string, the name of the function
     * @param value - mixed, the desired return value for the function;
     *              - may contain the return value itself, or
     *                an object that has the key 'return_vals' which is an array
     *                of multiple return values, useful for when a mocked function
     *                is called more than once and must return different values per call
     *                in the system under test (SUT).
     */
    public function setReturnValue($functionName, $value)
    {
        $this->returnValues[$functionName] = $value;
    }

    /**
     * Get the return value of the given function name
     * @param functionName - string, the name of the function
     */
    public function getReturnValue($functionName)
    {
        if(isset($this->returnValues[$functionName]['return_vals'])) {
            return array_shift($this->returnValues[$functionName]['return_vals']);
        }
        return $this->returnValues[$functionName];
    }

    // put mocked functions here

    /**
     * Sample of a mocked method
     * Use the function name when retrieving the return value from the array returnValues
     */
    public function sampleMockedMethod($input) {
        return $this->getReturnValue('sampleMockedMethod');
    }
}