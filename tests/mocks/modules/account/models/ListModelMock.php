<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /**
  * Mock class for ListModel
  */
namespace app\tests\mocks\models;

use app\interfaces\modules\account\models\IListModel;
use app\tests\mocks\models\BaseModelMock;

class ListModelMock extends BaseModelMock implements IListModel
{
    public function validateSplit($listId,$listType,$childListCount,$splitOrder,$listMemberCount) {
        return $this->getReturnValue('validateSplit');
    }

    public function updateListInfo($listId,$processRemarks,$listMemberId=null) {
        return $this->getReturnValue('updateListInfo');
    }

    public function getUsers($listId, $userId, $teamId=null,$return='tags') {
        return $this->getReturnValue('getUsers');
    }

    public function getTeams(){
        return $this->getReturnValue('getTeams');
    }

    public function validateInputList($inputList, $id, $listType, $program){
        return $this->getReturnValue('validateInputList');
    }

    public function validateTraitInput($inputListSplitArr){
        return $this->getReturnValue('validateTraitInput');
    }

    public function validateDesignationInput($inputListSplitArr){
        return $this->getReturnValue('validateDesignationInput');
    }

    public function addMembersByInput($id, $listType){
        return $this->getReturnValue('addMembersByInput');
    }
}