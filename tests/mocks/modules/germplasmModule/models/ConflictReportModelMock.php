<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class for the ConflictReportModel model
 */

namespace app\tests\mocks\modules\germplasm\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\germplasmModule\models\IConflictReportModel;

class ConflictReportModelMock extends Mock implements IConflictReportModel
{

    /**
     * Retrieve merge conflict for the transaction
     * 
     * @param Integer $fileId germplasm file upload identifier
     * @param String $conflictReportName conflict report file name
     * @param Integer $browserId browser identifier
     * 
     * @return mixed
     */
    public function search($fileId,$conflictReportName,$browserId){
        return $this->getReturnValue('search');
    }

    public function getConflictColumns(){
        return $this->getReturnValue('getConflictColumns');
    }    
}

?>