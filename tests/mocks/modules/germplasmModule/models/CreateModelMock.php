<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class for the Transaction model
 */

namespace app\tests\mocks\modules\germplasm\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\germplasmModule\models\ICreateModel;

class CreateModelMock extends Mock implements ICreateModel
{

    /**
     * Facilitates the retrieval of variable configurations for a given program
     * 
     * @param Integer $programId id of current dashboard program
     * 
     * @return Array validated variables and corresponding data
     */
    public function getVariableConfig($programId){
        return $this->getReturnValue('getVariableConfig');
    }

    /**
     * Facilitates the verification of variables and extraction of relevant information
     * 
     * @param Array $configVar array of variables extracted from configuration record
     * 
     * @return Array validated variables
     */
    public function validateConfigVariables($configVar){
        return $this->getReturnValue('validateConfigVariables');
    }

    /**
     * Facilitate validation of uploaded file
     * 
     * @param Integer $programId program identifier
     * @param Array $headers name of uploaded file
     * @param Object $variableConfig variable configuration record
     * 
     * @return mixed
     */
    public function validateUploadedFile($programId,$headers,$variableConfig){
        return $this->getReturnValue('validateUploadedFile');
    }

    /**
     * Facilitate data retrieval for uploaded file
     * 
     * @param String $fileReference name of uploaded file
     * 
     * @return Array array of file column headers
     */
    public function retrieveUploadedFile($fileReference){
        return $this->getReturnValue('retrieveUploadedFile');
    }
    
    /**
     * Facilitates the creation of CSV template file
     * 
     * @param Array $templateHeaders array if column headers for template file
     */
    public function generateCSVTemplate($templateHeaders,$cropCode,$programCode){
        return $this->getReturnValue('generateCSVTemplate');
    }
}

?>