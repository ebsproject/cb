<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the Mock Class for VariableFilterComponent class
 */

namespace app\tests\mocks\modules\seedInventory\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\seedInventory\models\IWorkingList;

class WorkingListMock extends Mock implements IWorkingList
{
    /**
     * Retrievs the working list for find seeds
     * @param $userId integer ID of the current user
     * @return array contains the attribute of the working list
     */
    public function getWorkingList($userId){
        return $this->getReturnValue('getWorkingList');
    }

    /**
     * Retrievs the current working list member
     * @param integer integer ID of the current working list
     * @return array contains list member data
     */
    public function getWorkingListMembers($listDbId){
        return $this->getReturnValue('getWorkingListMembers');
    }

    /**
     * Search method for find seeds working lsit
     * @param $listDbId integer ID of the working list
     */
    public function search($listDbId){
        return $this->getReturnValue('search');
    }

}