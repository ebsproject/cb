<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class  for FindSeedDataBrowserModel class
 */

namespace app\tests\mocks\modules\seedInventory\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\seedInventory\models\IFindSeedDataBrowserModel;

class FindSeedDataBrowserModelMock extends Mock implements IFindSeedDataBrowserModel
{
    /**
    *   Set dynamic columns
    * @param $config array 
    */
    public function setColumnsForFiltering($config=null){
        return $this->getReturnValue('setColumnsForFiltering');
    }

    /**
     * Retrieve columns from configuration dataset
     * @return array Array of configuration
     */
    public function getColumns(){
        return $this->getReturnValue('getColumns');
    }

    /**
     * Check and sort variables with existing mariable information in the database
     * @param array config variables
     * @return array config variables
     */
    public function validateColumnConfigVariables($records){
        return $this->getReturnValue('validateColumnConfigVariables');
    }

    /**
     * Get search DataProvider
     * @return mixed
     */
    public function getDataProvider($condition, $condStr, $columns, $inputListOrder, $browserFilters, $getAllResponse, $getParamsOnly){
        return $this->getReturnValue('getDataProvider');
    }

    /**
     * Retrieve program code
     * @param array program value
     * @return string program code
     */
    public function getProgramCode($programIds){
        return $this->getReturnValue('getProgramCode');
    }

    /**
     * Process variable filter values
     * @param array passed filters
     * @return array variable filter
     */
    public function processFilters($passedFilters){
        return $this->getReturnValue('processFilters');
    }

    /**
     * Retrieve entry data
     * 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function search($params, $dataFilters, $inputListOrder){
        return $this->getReturnValue('search');
    }

    /**
     * Retrieve search dquery parameters
     * needed in query builder 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function getSearchData($params, $dataFilters, $inputListOrder){
        return $this->getReturnValue('getSearchData');
    }
}
?>