<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the template for Mock Classes
 */

namespace app\tests\mocks\modules\seedInventory\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\seedInventory\models\IHarvestManagerApiService;

class HarvestManagerApiServiceMock extends Mock implements IHarvestManagerApiService
{
    /**
     * Retrieve harvest manager configuration
     */
    public function getConfiguration($abbrev)
    {
        return $this->getReturnValue('getConfiguration');
    }

    /**
     * Retrieve plot browser configuration
     */
    public function getPlotConfiguration($abbrev)
    {
        return $this->getReturnValue('getPlotConfiguration');
    }

    /**
     * Retrieve harvest manager configuration
     */
    public function getPackageLabelConfiguration()
    {
        return $this->getReturnValue('getPackageLabelConfiguration');
    }

    /**
     * Retrieve programs accessible to user/person
     */
    public function getUserPrograms($limit, $page)
    {
        return $this->getReturnValue('getUserPrograms');
    }

    /**
     * Retrieve all programs where the current user belongs to
     */
    public function getAllUserPrograms() {
        return $this->getReturnValue('getAllUserPrograms');
    }

    /**
     * Retrieve experiments
     */
    public function getExperiments($requestBody, $limit, $page)
    {
        return $this->getReturnValue('getExperiments');
    }

    /**
     * Retrieve occurrences
     */
    public function getOccurrences($requestBody)
    {
        return $this->getReturnValue('getOccurrences');
    }
    
    /**
     * Retrieve occurrences with pagination
     * @param array requestBody - API request body
     * @param integer limit - limit of records to retrieve
     * @param integer page - page of records to retrieve
     */
    public function getOccurrencesPaginated($requestBody, $limit, $page) {
        return $this->getReturnValue('getOccurrencesPaginated');
    }

    /**
     * Retrieve location occurrence groups
     */
    public function getLocationOccurrenceGroups($requestBody)
    {
        return $this->getReturnValue('getLocationOccurrenceGroups');
    }

    /**
     * Retrieve locations by location ids
     */
    public function getLocationsByIds($requestBody)
    {
        return $this->getReturnValue('getLocationsByIds');
    }

    /**
     * Retrieve locations
     */
    public function getLocations($requestBody, $limit, $page)
    {
        return $this->getReturnValue('getLocations');
    }

    /**
     * Retrieve a program using its id
     */
    public function getProgramById($programId)
    {
        return $this->getReturnValue('getProgramById');
    }

    public function getProgramInfoById($programId)
    {
        return $this->getReturnValue('getProgramInfoById');
    }

    /**
     * Retrieve a experiment using its id
     */
    public function getExperimentNameById($experimentId)
    {
        return $this->getReturnValue('getExperimentNameById');
    }

    /**
     * Retrieve a location using its id
     */
    public function getLocationNameById($locationId)
    {
        return $this->getReturnValue('getLocationNameById');
    }

    /**
     * Retrieve plots of a location per page
     */
    public function getPlotsByLocation($locationId,$requestBody,$queryParams)
    {
        return $this->getReturnValue('getPlotsByLocation');
    }

    /**
     * Retrieve harvest data of a location per page
     */
    public function getHarvestDataByLocation($locationId,$requestBody,$queryParams,$dataLevel='plot')
    {
        return $this->getReturnValue('getHarvestDataByLocation');
    }

    /**
     * Retrieve all harvest data of a location
     */
    public function getAllHarvestDataByLocation($locationId,$requestBody,$urlParams=null,$dataLevel='plot')
    {
        return $this->getReturnValue('getAllHarvestDataByLocation');
    }


    /**
     * Retrieve packages of a location per page
     */
    public function getPackagesByLocation($locationId,$requestBody,$queryParams)
    {
        return $this->getReturnValue('getPackagesByLocation');
    }

    /**
     * Retrieve all plots of a location
     */
    public function getAllPlotsByLocation($locationId,$requestBody,$urlParams=null)
    {
        return $this->getReturnValue('getAllPlotsByLocation');
    }

    /**
     * Retrieve a plots by plot db ids
     */
    public function getPlotsByPlotDbId($plotDbIdArray)
    {
        return $this->getReturnValue('getPlotsByPlotDbId');
    }

    /**
     * Retrieve plots with status IN_QUEUE or IN_PROGRESS
     */
    public function getPlotsHarvestStatusReady($locationId,$experimentId)
    {
        return $this->getReturnValue('getPlotsHarvestStatusReady');
    }

    /**
     * Retrieve plots with status DONE
     */
    public function getPlotsHarvestStatusProcessed($locationId,$experimentId)
    {
        return $this->getReturnValue('getPlotsHarvestStatusProcessed');
    }

    /**
     * Retrieve plots with status IN_QUEUE, IN_PROGRESS, or DONE
     */
    public function getPlotsHarvestStatusTotal($locationId,$experimentId)
    {
        return $this->getReturnValue('getPlotsHarvestStatusTotal');
    }

    /**
     * Retrieve a package by package db ids
     */
    public function getPackageByPackageDbId($packageDbId)
    {
        return $this->getReturnValue('getPackageByPackageDbId');
    }

    /**
     * Delete a package and update notes
     */
    public function deletePackage($packageDbId,$notes)
    {
        return $this->getReturnValue('deletePackage');
    }

    /**
     * Retrieve stage of location
     */
    public function getLocationStage($locationId)
    {
        return $this->getReturnValue('getLocationStage');
    }

    /**
     * Retrieve terminal transaction id of a location (if transaction exists)
     */
    public function getTerminalTransactionId($locationId,$userId,$any=null)
    {
        return $this->getReturnValue('getTerminalTransactionId');
    }

    /**
     * Create terminal transaction for a location and return its transaction id     
     */
    public function createTerminalTransaction($locationId)
    {
        return $this->getReturnValue('createTerminalTransaction');
    }

    /**
     * Adds dataset to transaction
     * @param transactonId - transaction id
     * @param plotIds - array containing plot ids
     * @param variableAbbrevArray - array containing variable abbrevs
     * @param values - array containing dataset values
     * @return boolean - whether or not the datasets were successfully added     
     */
    public function addDataSetToTransaction($transactionId,$plotIds,$variableAbbrevArray,$values,$dataLevel)
    {
        return $this->getReturnValue('addDataSetToTransaction');
    }

    /**
     * Deletes transactions if there is no transaction dataset recor
     * @param transactionDbId transaction DB identifier
     */
    public function deleteTransaction($transactionDbId)
    {
        return $this->getReturnValue('deleteTransaction');
    }

    /**
     * Update Package Quantity and Unit
     * @param packageDbId - id of package
     * @param packageFields - fields to update
     * @param values - values of field to update
     * return true if successfull , false if not     
     */
    public function updatePackageData($packageDbId,$packageFields,$values)
    {
        return $this->getReturnValue('updatePackageData');
    }

    /**
     * Retrieve datasets of transaction
     * @param transactionId - id of transaction
     * @return datasets     
     */
    public function getTransactionDataSets($transactionId)
    {
        return $this->getReturnValue('getTransactionDataSets');
    }

    /** 
     * This function retreives packages
     * return $data
     */
    public function getPackages($locationId,$filters)
    {
        return $this->getReturnValue('getPackages');
    }

    /**
     * This method retrieves the variable id given an abbrev
     * @param $abbrev - variable abbrev
     * @return $variableDbId - variable  id
     */
    public function getVariableIdByAbbrev($abbrev)
    {
        return $this->getVariableIdByAbbrev('getVariableIdByAbbrev');
    }

    public function getProgramCropCode($programDbId)
    {
        return $this->getVariableIdByAbbrev('getProgramCropCode');
    }

    /**
     * This method retrieves the variable details
     * @param $variableDbId - variable id
     * @return $variableDetails - array containing variable detials     
     */
    public function getVariableDetails($variableDbId)
    {
        return $this->getReturnValue('getVariableDetails');
    }
    
    /**
     * retrieves location occurence group where supplied location id belongs to
     * @param locationDbId
     * @return locatonOccurrenceGroups - array containing location occurrence group
     */
    public function getLocationOccurrenceGroupByLocationId($locationDbId)
    {
        return $this->getReturnValue('getLocationOccurrenceGroupByLocationId');
    }

    /**
     * retrieves location harvest summary 
     * @param locationDbId
     * @return locatonOccurrenceGroups - array containing location occurrence group
     */
    public function getLocationHarvestSummaries($locationDbId,$experimentId)
    {
        return $this->getReturnValue('getLocationHarvestSummaries');
    }

    /**
     * retrieves Occurrence info of the supplied occurrence id
     * @param occurenceDbId
     * @return occurrences - array containing occurence
     */
    public function getOccurrenceById($occurrenceDbId)
    {
        return $this->getReturnValue('getOccurrenceById');
    }

    /**
     * retrieves Occurrence info of the supplied occurrence id
     * @param occurenceDbId
     * @return occurrences - array containing occurence
     */
    public function getExperimentById($experimentDbId)
    {
        return $this->getReturnValue('getExperimentById');
    }

    /**
     * Creates a seed record given the seed info
     * @param seedInfo
     * @return seedDbId - id of created seed record     
     */
    public function createSeedRecord($seedInfo)
    {
        return $this->getReturnValue('createSeedRecord');
    }

    /**
     * get seed record associated with given plot id
     * @param sourcePlotDbId
     * @return seedDbId
     */
    public function getSeedDbId($sourcePlotDbId)
    {
        return $this->getReturnValue('getSeedDbId');
    }

    /**
     * get seed record with given germplasm id
     * @param germplasmDbId
     * @return seedDbId
     */
    public function getSeedDbIdGivenGermplasm($germplasmDbId)
    {
        return $this->getReturnValue('getSeedDbIdGivenGermplasm');
    }

    /**
     * get seed id of seed with the given seed code
     * @param seedCode
     * @return seedDbId
     */
    public function getSeedDbIdBySeedCode($seedCode)
    {
        return $this->getReturnValue('getSeedDbIdBySeedCode');
    }

    /**
     * Creates a package record given the package info
     * @param packageInfo - array
     * @return packageDbId - id of created package record     
     */
    public function createPackageRecord($packageInfo)
    {
        return $this->getReturnValue('createPackageRecord');
    }

    /**
     * Retrieve package id associated with the given seed id
     * @param seedDbId 
     * @return packageDbId 
     */
    public function getPackageDbId($seedDbId)
    {
        return $this->getReturnValue('getPackageDbId');
    }

    /**
     * Delete a dataset given its ID     
     */
    public function deleteDataset($datasetId)
    {
        return $this->getReturnValue('deleteDataset');
    }

    /**
     * Retrieve dataset id using plot id and variable id
     */
    public function getDatasetId($plotDbId,$variableDbId,$transactionDbId)
    {
        return $this->getReturnValue('getDatasetId');
    }

    /**
     * Retrieve datasets of plot
     */
    public function getDatasets($plotDbId,$variableDbIds,$transactionDbId)
    {
        return $this->getReturnValue('getDatasets');
    }


     /**
     * Retrieve dataset ids using plot id
     */
    public function getDatasetIdsOfPlot($plotDbId,$transactionDbId)
    {
        return $this->getReturnValue('getDatasetIdsOfPlot');
    }

    /**
     * Retrieve information of germplasm given its ID     
     */
    public function getGermplasmInfo($sourceGID)
    {
        return $this->getReturnValue('getGermplasmInfo');
    }

    /**
     * Retrieve germplasm ID given its designation
     */
    public function getGermplasmId($designation)
    {
        return $this->getReturnValue('getGermplasmId');
    }

    /**
     * Retrieve germplasm names
     */
    public function getGermplasmNames($desigStr)
    {
        return $this->getReturnValue('getGermplasmNames');
    }

    /**
     * Retrieve germplasm names
     */
    public function getGermplasmNamesByGermplasmId($germplasmDbId)
    {
        return $this->getReturnValue('getGermplasmNamesByGermplasmId');
    }


    /**
     * Create germplasm and germplasm name record    
     */
    public function createGermplasmRecord($record)
    {
        return $this->getReturnValue('createGermplasmRecord');
    }

    /**
     * Update germplasm record     
     */
    public function updateGermplasmRecord($germplasmDbId,$requestBody)
    {
        return $this->getReturnValue('updateGermplasmRecord');
    }
    
    /**
     * Retrieve a package unit     
     */
    public function getPackageUnit()
    {
        return $this->getReturnValue('getPackageUnit');
    }

    /**
     * Delete seed given seed id
     * @param plotDbId
     */
    public function deleteSeeds($seedDbId)
    {
        return $this->getReturnValue('deleteSeeds');
    }

    /**
     * Delete germplasm record given germplasmDbId
     * @param plotDbId
     */
    public function deleteGermplasmRecord($germplasmDbId)
    {
        return $this->getReturnValue('deleteGermplasmRecord');
    }

    /**
     * Delete germplasm record given germplasmDbId
     * @param plotDbId
     */
    public function deleteGermplasmNames($germplasmNamesDbId)
    {
        return $this->getReturnValue('deleteGermplasmNames');
    }

    /**
     * Get sourceplot dbid  given ploId
     * @param plotDbId
     */
    public function getSourcePlotDbId($plotDbId)
    {
        return $this->getReturnValue('getSourcePlotDbId');
    }

    public function bulkDeleteCommitted($dbIds)
    {
        return $this->getReturnValue('bulkDeleteCommitted');
    }

    /** 
     * Commit Transaction
     */
    public function commitTransaction($transactionId, $datasetCount)
    {
        return $this->getReturnValue('commitTransaction');
    }

    /**
     * Retrieve plot data of given plot
     * @param plotDbId - plot identifier
     * @param variableDbIds - variable ids of data to retrieve
     * @return - array containing cross data records
     */
    public function getPlotData($plotDbId, $variableDbIds)
    {
        return $this->getReturnValue('getPlotData');
    }

    /**
     * Retrieve cross data of given cross
     * @param crossDbId - cross identifier
     * @param variableDbIds - variable ids of data to retrieve
     * @return - array containing cross data records
     */
    public function getCrossData($crossDbId, $variableDbIds)
    {
        return $this->getReturnValue('getCrossData');
    }

    /**
     * Retrieve plot data of given plot
     * @param plotDbId
     */
    public function getEntries($seedDbId)
    {
        return $this->getReturnValue('getEntries');
    }

    /**
     * Get backround job info
     * @param id - id of the background job
     */
    public function getBackgroundJob($id)
    {
        return $this->getReturnValue('getBackgroundJob');
    }

    /**
     * Search backround process by id
     * @param id - id of the background process
     */
    public function backgroundProcessesSearchById($id)
    {
        return $this->getReturnValue('backgroundProcessesSearchById');
    }

    /**
     * Search backround process by id
     * @param id - id of the background process
     */
    public function backgroundProcessesSearchByUser($userId)
    {
        return $this->getReturnValue('backgroundProcessesSearchByUser');
    }

    /**
     * Function for bulk operations using processor
     * @param httpMethod - HTTP method to be used
     * @param endpoint - endpoint where bulk operation will be performed
     * @param dbIds - array containing database identifiers of records included in the bulk operation
     * @param entity - the entity where the record belongs (eg. germplasm)
     * @param entityDbId - entity identifier
     * @param endpointEntity - the specific endpoint in the entity (eg. seed, package, etc.)
     * @param application - the application performing the bulk operation (eg. Harvest Manager)
     * @param optional - optional data for the bulk operations (eg. dependents, requestData)
     */
    public function processorBuilder($httpMethod, $endpoint, $dbIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional = [])
    {
        return $this->getReturnValue('processorBuilder');
    }

    /**
     * Processor function
     */
    public function processorOperation($requestBody)
    {
        return $this->getReturnValue('processorOperation');
    }

    /** 
     * 
     * function for bulk operations using broker
     */
    public function bulkBuilder($dbIds,$endpoint,$method,$requestData=null)
    {
        return $this->getReturnValue('bulkBuilder');
    }

    /**
     * Broker function
     */
    public function bulkOperation($requestBody)
    {
        return $this->getReturnValue('bulkOperation');
    }

    /**
     * get all data of havrest manager filter
     */
    public function getFilterData($filter)
    {
        return $this->getReturnValue('getFilterData');
    }

    /**
     * Retrieve packages of seed
     */
    public function seedPackagesSearch($seedDbId,$requestBody=null)
    {
        return $this->getReturnValue('seedPackagesSearch');
    }

    /**
     * Retrieve entries of seed
     */
    public function seedEntriesSearch($seedDbId,$requestBody=null)
    {
        return $this->getReturnValue('seedEntriesSearch');
    }

    /**
     * Retrieve germplasm names of a germplasm
     */
    public function germplasmNamesSearch($germplasmDbId,$requestBody=null)
    {
        return $this->getReturnValue('germplasmNamesSearch');
    }

    /**
     * Retrieve seed records
     */
    public function seedsSearch($requestBody = null,$queryParams = null) {
        return $this->getReturnValue('seedsSearch');
    }

    /**
     * Retrieve package records
     */
    public function packagesSearch($requestBody = null,$queryParams = null) {
        return $this->getReturnValue('packagesSearch');
    }

    /**
     * Retrieve germplasmName records
     */
    public function germplasmNamesSearchAll($requestBody = null,$queryParams = null) {
        return $this->getReturnValue('germplasmNamesSearchAll');
    }
}