<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the Mock Class for FindSeedListModel class
 */

namespace app\tests\mocks\modules\seedInventory\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\seedInventory\models\IFindSeedListModel;

class FindSeedListModelMock extends Mock implements IFindSeedListModel
{
    /**
     * Add items to platform.list_member
     * @param table name list id user id
     */
    public function addListItems($tableName, $listId, $userId){
        return $this->getReturnValue('addListItems');
    }

    /**
     * Check if the temp table is existing
     *
     * @param string $tableName name of the temp table
     *
     * @return boolean
     */
    public function checkIfTableExistWithoutCol($tableName,$userId){
        return $this->getReturnValue('checkIfTableExistWithoutCol');
    }

    /**
     * Check seeds if in working list
     *
     * @param mixed selected ids user id
     */
    public function validateSeedIds($selectedIds, $userId,$existingWorkingList){
        return $this->getReturnValue('validateSeedIds');
    }

    /**
     * Retrieve package label and id
     * @param Array list items
     * @return Array package data
     */
    public function retrievePackageDataById($newListItems){
        return $this->getReturnValue('retrievePackageDataById');
    }

    /**
     * Insert values to the temporary table created
     * @param Array list items
     * @param Integer list id
     * @return Boolean insert status
     */
    public function insertSelectedItemsToWorkingList($newListItems,$listId){
        return $this->getReturnValue('insertSelectedItemsToWorkingList');
    }

    /**
     * Insert selected items using a background job
     * @param Integer list id
     * @param Array search parameters 
     */
    public function insertItemsInBackground($listId,$searchParams){
        return $this->getReturnValue('insertItemsInBackground');
    }

    /**
     * Insert values to the temporary table created
     * @param Insert values to the temporary table created
     * @param text $selectedIds selected studies
     */
    public function insertTempValuesFromSeedList($newListItems,$listId){
        return $this->getReturnValue('insertTempValuesFromSeedList');
    }

    /**
     * Builds the prerequisite parameter for the processor
     * @param array $data contains the request data for the endpoint
     * @param integer $listId ID of the list
     * @param integer $totalCount total count of the records to be inserted
     */
    public function insertProcessorWithPrereq($data, $listId, $totalCount){
        return $this->getReturnValue('insertProcessorWithPrereq');
    }

    /**
     * processor implementation for inserting package records to list
     * @param boolean api call status
     */
    public function insertProcessor($newItemRecords=[],$listId=null,$prereq=[],$totalCount=0){
        return $this->getReturnValue('insertProcessor');
    }

    /**
     * Delete values to the temporary table created
     * @param array $selectedRemoveIds selected studies
     */
    public function deleteTempValuesFromSeedList($selectedRemoveIds, $userId,$itemCount){
        return $this->getReturnValue('deleteTempValuesFromSeedList');
    }

    /**
     * Get working list browser data provider
     * @return mixed
     */
    public function getListDataProvider($userId){
        return $this->getReturnValue('getListDataProvider');
    }

    /**
    * Retrieve saved lists of a certain type for working list
    * @param mixed user id list type
    * @param array data provider items count
    **/
    public function getSavedSeedListDataProvider($userId,$listType){
        return $this->getReturnValue('getSavedSeedListDataProvider');
    }

    /**
    * Retrieve saved list items by id
    * @param mixed list id
    * @param array list items
    **/
    public function getSavedListById($id){
        return $this->getReturnValue('getSavedListById');
    }

    /**
    * Retrieve saved list information
    * @param mixed list id
    * @param array list data
    **/
    public function getSavedListData($id){
        return $this->getReturnValue('getSavedListData');
    }

    /**
    * Retrieve saved list items by id
    * @param mixed list id
    * @param array list items
    **/
    public function getSavedSeedListById($id){
        return $this->getReturnValue('getSavedSeedListById');
    }

    /**
     * Save list basic information
     * @param array list info
     * @return mixed api call return
     */
    public function saveListBasicInformation($listInfo){
        return $this->getReturnValue('saveListBasicInformation');
    }

    /**
     * Removes the package IDs in the array if existing already in working list
     * @param Integer $listDbId ID of the working list
     * @param Array $idArray contains the package db ids
     * @return Array array of the package Ids that have been filtered out
     */
    public function removeDuplicates($listDbId, $idArray){
        return $this->getReturnValue('removeDuplicates');
    }

    /**
     * Retrieve working list items
     * @param integer user id
     * @return array data total count
     */
    public function retrieveWorkingList($userId){
        return $this->getReturnValue('retrieveWorkingList');
    }

    /**
     * Clear working list items
     * @param integer user id
     * @return boolean status
     */
    public function clearWorkingList($userId){
        return $this->getReturnValue('clearWorkingList');
    }

    /**
     * update list record information
     * @param mixed id params
     * @return array mixed
     */
    public function updateListRecord($listDbId,$params){
        return $this->getReturnValue('updateListRecord');
    }

    /**
     * Create working list
     * @param integer user id
     * @return array working list
     */
    public function createWorkingList($userId){
        return $this->getReturnValue('createWorkingList');
    }

    /**
     * Saves the items from the working list to a final list
     * @param Array $data contains the request data with the list ID and list name
     * @return Array contains whether the operation is succesfull with a notification message
     */
    public function saveListItems($data, $userId){
        return $this->getReturnValue('saveListItems');
    }

    /**
     * Retrieves the total count of the items in a working list
     * @return Integer total record count
     */
    public function getItemsCount(){
        return $this->getReturnValue('getItemsCount');
    }
}
?>