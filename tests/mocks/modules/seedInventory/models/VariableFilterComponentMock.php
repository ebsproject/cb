<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class  for VariableFilterComponent class
 */

namespace app\tests\mocks\modules\seedInventory\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\seedInventory\models\IVariableFilterComponent;

class VariableFilterComponentMock extends Mock implements IVariableFilterComponent
{

    /**
     * Get config values
     * @param none
     * @return array of config values
     */
    public function getConfig(){
        return $this->getReturnValue('getConfig');
    }

    /**
     * Check and sort variables with existing mariable information in the database
     * @param array config variables
     * @return array config variables
     */
    public function validateConfigVariables($records){
        return $this->getReturnValue('validateConfigVariables');
    }

    /**
     * Retrieve config file from db for options in variable filter selection
     * @param none
     * @return array of abbrev and corresponding field labels
     */
    public function retrieveSelectionOptions(){
        return $this->getReturnValue('retrieveSelectionOptions');
    }

    /**
     * Retrieve input list field type values and configuration
     * @param none
     * @return array of abbrev and corresponding field labels
     */
    public function retrieveInputListConfig(){
        return $this->getReturnValue('retrieveInputListConfig');
    }

    /**
     * Retrieve config file from db for options in variable filter selection
     * @param none
     * @return array of abbrev and corresponding field labels
     */
    public function retrieveRequiredFilters(){
        return $this->getReturnValue('retrieveRequiredFilters');
    }


    /**
     * Generate filter field html for given variables
     * @param Array configVariables
     * @param Array variableFilters
     * @param Integer userId
     * @param Boolean isBasic
     * @return Array variableFields
     */
    public function generateInputFields($configVariables, $variableFilters, $userId, $isBasic=true){
        return $this->getReturnValue('generateInputFields');
    }

    /**
     * Extract variable values and configuration information for existing filters
     * @param Array existingFilters 
     * @param Array configVar 
     * @param Integer id
     * @return Array variableFilters
     */
    public function extractVariableFilters($existingFilters,$configVar,$id){
        return $this->getReturnValue('extractVariableFilters');
    }

    /**
     * Generate variable filter field html
     *
     * @param array user id
     * @param Array variableFilters
     * @param Boolean isBasic
     * @param Array additionalFilter
     * @return array html elements
     */
    public function generateFilterFields($userId, $variableFilters, $isBasic){
        return $this->getReturnValue('generateFilterFields');
    }


    /**
     * Retrieve filter initial values; refactoring of retrieveInitialValues()
     *
     * @param Integer user id
     * @param Array variable filters
     * @param Array config variable
     * @return array initial value
     */
    public function getInitFilterValues($userId,$variableFilters,$confVar){
        return $this->getReturnValue('getInitFilterValues');
    }

    /**
	 * Generates fields params from existing variable filter values for search filter
	 * @param Integer userId 
     * @param Array existingFilters 
     * @param Array targetFilter
     * @param Boolean dependencyApplicable
     * @param String q
	 * @return Array $params with fields and filters for API search
	 */
    public function generateSearchFieldsParams($userId,$existingFilters,$targetFilter,$dependencyApplicable,$q){
        return $this->getReturnValue('generateSearchFieldsParams');
    }

    /**
	 * Retrieves matching and filtered package-level values using generated fields params
	 * @param Array searchParams
     * @param Array targetFilter
     * @param String resourceEndpoint
     * @param String resourceMethod
     * @param Integer limit
     * @param Integer offset
	 * @return Array $filterValues with corresponding id and text values 
	 */
    public function extractSearchFilterValues($searchParams,$targetFilter,$resourceEndpoint, $resourceMethod,$limit,$offset){
        return $this->getReturnValue('extractSearchFilterValues');
    }
  
    /**
     * Extract and format query params for storage
     * @param Integer userId
     * @param Array params
     * @param Array configVars
     * @param String inputListType
     * @return array display values query param values and id
     */
    public function extractQueryParamsValuesForStorage($userId,$params,$configVars,$inputListType){
        return $this->getReturnValue('extractQueryParamsValuesForStorage');
    }

    /**
     * Calls necessary api call to handle search
     *
     * @param Integer userId
     * @param Array variableFilters
     * @param Array variableInfo
     * @param Integer limit
     * @param Integer offset
     * @param String q
     * @return Array search data results
     */
    public function apiCallHandler($userId,$variableFilters,$variableInfo,$limit,$offset,$q){
        return $this->getReturnValue('apiCallHandler');
    }

    /**
     * Returns generated Select2 html fields
     *
     * @param String name
     * @param Integer variable id
     * @param Array data 
     * @param Array val
     * @param String required string 
     * @param String size
     * @param Boolean removeFunction
     * @param Array initVal
     * @param String input type 
     * @param Array variable config info
     * @return HTML htmlElement
     */
    public static function generateHTMLElements($name, $varId, $data, $val, $reqStr, $label, $size, $removeFxn, $initVal, $inputType,$varConfigInfo){
        return null;
    }

    /**
     * Returns generated input textfield variable filter fields
     *
     * @param String abbrev
     * @param String name
     * @param Integer variable ID
     * @param Array value
     * @param String reqStr
     * @param String label
     * @param String size
     * @param Boolean removeFxn
     * @param Array varConfigInfo
     * @param Array initVal
     * @return HTML htmlElement
     */
    public static function generateHTMLInputTextFieldElements($abbrev, $name, $varId, $val, $reqStr, $label, $size, $removeFxn,$varConfigInfo,$initVal){
        return null;
    }

}
?>