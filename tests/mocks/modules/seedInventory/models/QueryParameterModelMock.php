<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the Mock Class for QueryParameterModel class
 */

namespace app\tests\mocks\modules\seedInventory\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\seedInventory\models\IQueryParameterModel;

class QueryParameterModelMock extends Mock implements IQueryParameterModel
{
    /**
     * Validate query parameter
     * @param mixed name type data
     * @return array api call return
     */
    public function getQueryParam(){
        return $this->getQueryParam('getConfig');
    }

    /**
     * Get the query parameters
     *
     * @return Array $result Array of query parameters
     */
    public function getQueryParamsDataProvider(){
        return $this->getQueryParam('getQueryParamsDataProvider');
    }

    /**
     * Save query parameters
     *
     * @return Array $result Array of query parameters
     */
    public function saveQueryParam(){
        return $this->getQueryParam('saveQueryParam');
    }

    /**
     * Get the query parameters by id
     *
     * @return Array $result Array of query parameters
     */
    public function getQueryParamsById(){
        return $this->getQueryParam('getQueryParamsById');
    }
}
?>