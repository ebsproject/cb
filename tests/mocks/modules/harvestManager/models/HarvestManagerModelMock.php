<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class for the HarvestManagerModel
 */

namespace app\tests\mocks\modules\harvestManager\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;

class HarvestManagerModelMock extends Mock implements IHarvestManagerModel
{
    /**
     * Verifies if the URL Parameters are complete and valid.
     * Returns an error and error message if conditions are not met.
     * @param string parameters URL parameters passed onto the controller action
     * @param string url the current page url
     * @return array array containing (1) error - boolean, if there is an error or none, and (2) errorMessage
     */
    public function validateUrlParameters($parameters, $url) {
        return $this->getReturnValue('validateUrlParameters');
    }

    /**
     * Retrieve program code, given the ID
     * @param integer programId program identifier
     * @return string programCode
     */
    public function getProgram($programId) {
        return $this->getReturnValue('getProgram');
    }

    /**
     * Validates the program in the URL parameters
     * @param string program program code string from the controller action
     * @param integer occurrenceId (optional) the occurrence id from the controller action
     * @return array array containing (1) redirect - boolean, if redirection will occur, and (2) the program to use
     */
    public function validateUrlProgram($program, $occurrenceId = null) {
        return $this->getReturnValue('validateUrlProgram');
    }

    /**
     * Retrieves the variable id given the abbrev
     * @param string variableAbbrev the abbreviation of the variable
     * @return integer variable id
     */
    public function getVariableId($variableAbbrev) {
        return $this->getReturnValue('getVariableId');
    }

    /**
     * Retrieves the scale value of the given variable abbrev
     * @param string variableAbbrev the abbreviation of the variable
     * @param boolean scAbbrevMode whether or not to use scale value abbrev as key
     * @param array kvSettings default key and value to be used for the scale values
     * @return array scale value array where the key is the "value" and the value is the "displayName
     */
    public function getScaleValues($variableAbbrev, $scAbbrevMode = false, $kvSettings = ["key"=>"value","value"=>"displayName"]) {
        return $this->getReturnValue('getScaleValues');
    }

    /**
     * Check if the experiment is a Cross Parent Nursery.
     * If CPN, check status if 'planted' or 'planted;crossed'
     * @param integer experimentId experiment identifier
     * @return array array containing: (1) isCPN - boolean, whether or not experiment is CPN
     *                  (2) crossed - boolean, whether or not CPN has been crossed
     *                  (3) phase - string, CPN Phase (I or II)
     *                  (4) title - string, title to show on tab hover
     *                  (4) class - optional class name for the link
     */
    public function checkCPN($experimentId) {
        return $this->getReturnValue('checkCPN');
    }

    /**
     * Determines the pagination key format for the browser grid
     * Formats: 'page', 'db-N-page'
     * @param object queryParams browser URL parameters
     * @return string paginationKey the pagination key for the browser grid
     */
    public function getPaginationKey($queryParams) {
        return $this->getReturnValue('getPaginationKey');
    }

    /**
     * Determine if the pagination and filters will be reset
     * @param mixed uniqueIdentifier any unique identifier for the session (eg. occurenceId, userId, etc.)
     * @param object queryParams query parameters of the browser
     * @param string searchModelName name of search model
     */
    public function browserReset($uniqueIdentifier, $queryParams, $searchModelName) {
        return $this->getReturnValue('browserReset');
    }

    /**
     * Assemble the URL parameters for 
     * pagination and sorting of the browser
     * @param string gridId grid identifier
     * @param string defaultSortString default sorting when no sort is set
     * @param boolean resetPage to reset pagination or not
     */
    public function assembleUrlParameters($gridId, $defaultSortString, $resetPage) {
        return $this->getReturnValue('assembleUrlParameters');
    }

    /**
     * Invokes the deletion worker given
     * @param Integer $occurrenceId occurrence identifier
     * @param String $deletionMode mode of deletion (harvest data or package)
     * @param Object $deleteParameters object containing parameters for the deletion
     */
    public function deleteInBackground($occurrenceId, $deletionMode, $deleteParameters) {
        return $this->getReturnValue('deleteInBackground');
    }

    /**
     * Trim field values given an array of records.
     * To trim is to remove any whitespaces before and after a string
     * or a set of strings.
     * @param Array $records - array of record objects
     * @return Array $records
     */
    public function trimValues($records) {
        return $this->getReturnValue('trimValues');
    }

    /**
     * Find the best match config for the given record
     * @param Object $configs - array object containing configs
     * @param Object $record - array object containing record (plot/cross information)
     */
    public function findBestMatchConfig($configs, $record) {
        return $this->getReturnValue('findBestMatchConfig');
    }

    /**
     * Given the records (plots/crosses),
     * match the configs (stored in session) for easy access.
     * @param Array $records - array object containing plots or crosses
     * @param Integer $occurrenceId - occurrence identifier
     * @param String $dataLevel - data level (plot or cross)
     */
    public function matchConfigs($records, $occurrenceId, $dataLevel){
        return $this->getReturnValue('matchConfigs');
    }

    /**
     * Build the input columns for the data browsers
     * @param String $dataLevel - data level (plot or cross)
     * @param Integer $occurrenceId - occurrence identifier
     */
    public function buildInputColumns($dataLevel, $occurrenceId) {
        return $this->getReturnValue('buildInputColumns');
    }

    /**
     * Determines whether or not input will be disabled
     * based on the harvest status and/or state of the model
     * @param object model data model
     * @param string dataLevel data lavel (plot or cross)
     * @param string apiFieldName api field name of the input
     * @param string displayName display name of the input
     * @param object harvestDataConfig supported harvest data
     */
    public function disableInput($model, $dataLevel, $apiFieldName, $displayName, $harvestDataConfig) {
        return $this->getReturnValue('disableInput');
    }

    /**
     * Builds input elements for the supported harvest data
     * @param object model data model
     * @param string dataLevel data level (plot or cross)
     * @param object currentConfig harvest data config for the current plot/cross record
     * @param object templateConfig sample config for the column/input
     * @param boolean raw whether or not to enclose elements in divs
     */
    public function buildInputElements($model, $dataLevel, $currentConfig, $templateConfig, $raw = false) {
        return $this->getReturnValue('buildInputElements');
    }

    /**
     * Retrieves the scale value abbreviation for a given variable and value.
     * 
     * @param string $variableAbbrev - The abbreviation of the variable to search for.
     * @param string $value - The value to search for.
     * 
     * @return string - The scale value abbreviation.
     */
    function getScaleValueAbbrev($variableAbbrev, $value) {
        return $this->getReturnValue('getScaleValueAbbrev');
    }
}