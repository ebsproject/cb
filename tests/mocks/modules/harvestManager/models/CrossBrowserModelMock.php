<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the mock class for the CrossBrowserModel
 */

namespace app\tests\mocks\modules\harvestManager\models;

use app\tests\mocks\Mock;
use app\interfaces\modules\harvestManager\models\ICrossBrowserModel;

class CrossBrowserModelMock extends Mock implements ICrossBrowserModel
{
    /**
     * Retrieve cross records to display
     * @param object params browser parameters
     * @param integer experimentId experiment identifier
     * @param integer occurrenceId occurrence identifier
     * @param integer locationId location identifier
     * @return object containing data provider
     */
    public function search($params, $experimentId, $occurrenceId, $locationId) {
        return $this->getReturnValue('search');
    }

    /**
     * Retrieves crosses of the current occurrence in the given location
     * @param integer occurrenceId occurrence identifier
     * @param object requestBody column filters
     * @param array urlParams url params for sorting, pagination, and page size
     */
    public function getCrossRecords($occurrenceId, $requestBody, $urlParams) {
        return $this->getReturnValue('getCrossRecords');
    }

    /**
     * Retrieve crosses given the request body and url parameters
     * @param integer occurrenceId occurrence identifier
     * @param object requestBody column filters
     * @param array urlParams url params for sorting, pagination, and page size
     */
    public function searchAllCrosses($occurrenceId, $requestBody = null, $urlParams = null) {
        return $this->getReturnValue('searchAllCrosses');
    }

    /**
     * Retrieves the distinct values for the specified field of SELFING crosses
     * given the occurrence id
     * @param integer occurrenceId occurrence identifier
     * @param string field name of the field for which distinct values will be retrieved
     * @param boolean includeSelfing whether or not filter cross method abbrev with SELFING only
     */
    public function getDistinctField($occurrenceId, $field, $includeSelfing = true) {
        return $this->getReturnValue('getDistinctField');
    }

    /**
     * Assemble browser filters
     * @param object params - parameters passed by data browser
     * @param integer occurrenceId - occurrenceIdentifier
     * @return object columnFilters - browser filters in API request body format
     */
    public function assembleBrowserFilters($params, $occurrenceId) {
        return $this->getReturnValue('assembleBrowserFilters');
    }
}