<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Mock for Api Class
 */

namespace app\tests\mocks\controllers;

use app\interfaces\controllers\IDashboard;
use app\tests\mocks\Mock;

class DashboardMock extends Mock implements IDashboard
{
    /**
	 * Retrieves data filters saved in user dashboard config
	 *
	 * @param $attr text attribute that user wants to retrieve (i.e program_id, phase_id, etc.)
	 * @return $data mixed saved data filters 
	 */
	public function getFilters($attr=null) {
        return $this->getReturnValue('getFilters');
    }

	/**
	 * Get url with filter and access token parameters
	 */
	public function getFilterUrl() {
        return $this->getReturnValue('getFilterUrl');
    }

	/**
	 * Populate recently used tools in user config
	 */
	public function populateRecentlyUsed() {
        return $this->getReturnValue('populateRecentlyUsed');
    }

	/**
	 * Get formatted time interval between two dates
	 * 
	 * @param $datetime1 timestamp first date
	 * @param $datetime2 timestamp second date
	 * @param @int text formatted date interval
	 */
	public function getFormattedTimeInterval($datetime1,$datetime2) {
        return $this->getReturnValue('getFormattedTimeInterval');
    }


	/**
	 * Reset saved URL in session
	 * 
	 */
	public function resetSavedUrl($program) {
        return $this->getReturnValue('resetSavedUrl');
    }

	/**
	 * Return URL for Yii1
	 *  
	 */
	public function getCurrentUrl($origUrl, $actionLink, $refreshUrlSession = false) {
        return $this->getReturnValue('getCurrentUrl');
    }

	/**
	 * Checks if there is an existing saved filter values
	 * @param $filters currently applied data filters
	 * @return filter name if exists
	 */
	public function getSavedFilterByValues($filters) {
        return $this->getReturnValue('getSavedFilterByValues');
    }
}