<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;
class PlantingJobTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $plantingJob;
    
    protected function _before()
    {
        $this->plantingJob = Yii::$container->get('plantingJob');
    }

    /**
     * Test getting api endpoint for Planting job
     */
    public function testApiEndPointTest()
    {
        // ARRANGE
        $expected = 'planting-jobs';

        // ACT
        $actual = $this->plantingJob->apiEndpoint();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getting summary of Packing job
     */
    public function testRulesTest()
    {
        // ARRANGE
        $expected = [
            [[
                'occurrenceCount',
                'envelopeCount',
                'creatorDbId',
                'modifierDbId',
            ], 'integer'],
            [[
                'plantingJobCode',
                'experiments',
                'facilityName',
                'creator',
                'experiments'
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp',
                'plantingJobDueDate',
                'plantingJobStatus',
                'plantingJobType',
            ], 'safe'],
        ];

        // ACT
        $actual = $this->plantingJob->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getting summary of Packing job
     */
    public function testSummary()
    {
        // ARRANGE
        $id = 44;
        $expected = [
            'totalOccurrenceCount' => '2',
            'totalExperimentCount' => '2',
            'totalEnvelopesCount' => '580',
            'totalReplacementCount' => null,
            'experiments' => [
                [
                    'experimentDbId' => 168,
                    'experimentCode' => 'EXP00000168',
                    'experimentName' => 'IRSEA-F2-2016-DS-005',
                    'entryCount' => '17',
                    'envelopeCount' => '100',
                    'replacementCount' => '0',
                    'occurrenceCount' => '1'
                ], 
                [
                    'experimentDbId' => 218,
                    'experimentCode' => 'EXP00000218',
                    'experimentName' => 'IRSEA-PYT-2016-WS-002',
                    'entryCount' => '400',
                    'envelopeCount' => '3190',
                    'replacementCount' => '0',
                    'occurrenceCount' => '1'
                ]
            ]
        ];
       
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'success' => true,
                        'totalCount' => 1,
                        'currentPage' => 1,
                        'totalPages' => 1,
                        'message' => 'Request has been successfully completed.',
                        'data' => [
                            [
                                'facilityDbId' => null,
                                'plantingJobCode' => 'PJOB000044',
                                'plantingJobDbId' => 44,
                                'plantingJobDueDate' => null,
                                'plantingJobInstructions' => null,
                                'plantingJobStatus' => 'draft',
                                'plantingJobType' => 'packing job',
                                'summaries' => [
                                    'totalOccurrenceCount' => '2',
                                    'totalExperimentCount' => '2',
                                    'totalEnvelopesCount' => '580',
                                    'totalReplacementCount' => null,
                                    'experiments' => [
                                        [
                                            'experimentDbId' => 168,
                                            'experimentCode' => 'EXP00000168',
                                            'experimentName' => 'IRSEA-F2-2016-DS-005',
                                            'entryCount' => '17',
                                            'envelopeCount' => '100',
                                            'replacementCount' => '0',
                                            'occurrenceCount' => '1'
                                        ], 
                                        [
                                            'experimentDbId' => 218,
                                            'experimentCode' => 'EXP00000218',
                                            'experimentName' => 'IRSEA-PYT-2016-WS-002',
                                            'entryCount' => '400',
                                            'envelopeCount' => '3190',
                                            'replacementCount' => '0',
                                            'occurrenceCount' => '1'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        // ACT
        $actual = $this->plantingJob->summary($id);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getting class of status value
     */
    public function testGetStatusClass()
    {
        // ARRANGE
        $status = 'packed';

        $expected = 'blue darken-3';

        // ACT
        $actual = $this->plantingJob->getStatusClass($status);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * Test getting class of status value
     */
    public function testGetStatusClass2()
    {
        // ARRANGE
        $status = 'ready for packing';

        $expected = 'green darken-3';

        // ACT
        $actual = $this->plantingJob->getStatusClass($status);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * Test getting class of status value
     */
    public function testGetStatusClass3()
    {
        // ARRANGE
        $status = 'packing';

        $expected = 'orange darken-2';

        // ACT
        $actual = $this->plantingJob->getStatusClass($status);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * Test getting class of status value
     */
    public function testGetStatusClass4()
    {
        // ARRANGE
        $status = 'packing cancelled';

        $expected = 'red';

        // ACT
        $actual = $this->plantingJob->getStatusClass($status);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * Test formatting string to camel case
     */
    public function testFormatToCamelCase()
    {

        // ARRANGE
        $variable = 'PLANTING_JOB_STATUS';

        $expected = 'plantingJobStatus';

        // ACT
        $actual = $this->plantingJob->formatToCamelCase($variable);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * Test formatting string to camel case
     */
    public function testFormatToCamelCase2()
    {

        // ARRANGE
        $variable = 'PLANTING_JOB_DUE_DATE';

        $expected = 'plantingJobDueDate';

        // ACT
        $actual = $this->plantingJob->formatToCamelCase($variable);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * Test getting processed summary
     */
    public function testGetProcessedSummary()
    {
        // ARRANGE
        $summaryData = [
            'totalEnvelopesCount' => '12',
            'totalExperimentCount' => '3',
            'totalOccurrenceCount' => '5'
        ];

        $expected = [
            [
                'label' => 'Total number of Experiments',
                'value' => '3'
            ],
            [
                'label' => 'Total number of Occurrences',
                'value' => '5'
            ],
            [
                'label' => 'Total number of Envelopes in Packing job',
                'value' => '12'
            ]
        ];

        // ACT
        $actual = $this->plantingJob->getProcessedSummary($summaryData);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getting processed summary
     */
    public function testGetProcessedSummary2()
    {
        // ARRANGE
        $summaryData = [
            'totalEnvelopesCount' => '100',
            'totalExperimentCount' => '4',
            'totalOccurrenceCount' => '7'
        ];

        $expected = [
            [
                'label' => 'Total number of Experiments',
                'value' => '4'
            ],
            [
                'label' => 'Total number of Occurrences',
                'value' => '7'
            ],
            [
                'label' => 'Total number of Envelopes in Packing job',
                'value' => '100'
            ]
        ];

        // ACT
        $actual = $this->plantingJob->getProcessedSummary($summaryData);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getting processed additional data
     */
    public function testGetProcessedAdditionalData()
    {
        // ARRANGE
        $data = [
            'plantingJobDueDate' => '2021-05-26',
            'facilityName' => 'NCBL',
            'plantingJobInstructions' => 'Test'
        ];

        $expected = [
            [
                'label' => 'Packing Job Due Date',
                'value' => 'May 26, 2021'
            ],
            [
                'label' => 'Packing Job Facility',
                'value' => 'NCBL'
            ],
            [
                'label' => 'Packing Job Instructions',
                'value' => 'Test'
            ]
        ];

        // ACT
        $actual = $this->plantingJob->getProcessedAdditionalData($data);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getting processed additional data
     */
    public function testGetProcessedAdditionalData2()
    {
        // ARRANGE
        $data = [
            'plantingJobDueDate' => '2021-06-26',
            'facilityName' => 'NCBL',
            'plantingJobInstructions' => 'Test instructions'
        ];

        $expected = [
            [
                'label' => 'Packing Job Due Date',
                'value' => 'Jun 26, 2021'
            ],
            [
                'label' => 'Packing Job Facility',
                'value' => 'NCBL'
            ],
            [
                'label' => 'Packing Job Instructions',
                'value' => 'Test instructions'
            ]
        ];

        // ACT
        $actual = $this->plantingJob->getProcessedAdditionalData($data);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getting configuration
     */
    public function testGetConfigForm()
    {
        // ARRANGE
        $data = [
            'configVars' => 
                '
                    [{
                        "fixed": true,
                        "default": "1",
                        "required": "required",
                        "display_name": "Seeds / envelope",
                        "variable_type": "identification",
                        "variable_abbrev": "SEEDS_PER_ENVELOPE",
                        "api_resource_endpoint": "planting-job-occurrences"
                    }, {
                        "fixed": true,
                        "default": "g",
                        "required": "required",
                        "display_name": "Unit",
                        "variable_type": "identification",
                        "variable_abbrev": "PACKAGE_UNIT",
                        "api_resource_endpoint": "planting-job-occurrences"
                    }, {
                        "fixed": true,
                        "default": "1",
                        "required": "required",
                        "display_name": "Envelopes / plot",
                        "variable_type": "identification",
                        "variable_abbrev": "ENVELOPES_PER_PLOT",
                        "api_resource_endpoint": "planting-job-occurrences"
                    }]
                ',
            'formType' => 'modal',
            'selected' => 'ENVELOPES_PER_PLOT'
        ];

        $expected = [
            'dataEndpoint' => 'planting-job-occurrences',
            'widgetOptions' => [
                'config' => [
                    [
                        'api_resource_endpoint' => 'planting-job-occurrences',
                        'default' => '1',
                        'display_name' => 'Envelopes / plot',
                        'fixed' => true,
                        'required' => 'required',
                        'variable_abbrev' => 'ENVELOPES_PER_PLOT',
                        'variable_type' => 'identification'
                    ]
                ],
                'formType' => 'MODAL'
            ]
        ];

        // ACT
        $actual = $this->plantingJob->getConfigForm($data);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}