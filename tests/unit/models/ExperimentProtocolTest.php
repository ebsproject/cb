<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;

class ExperimentProtocolTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $experimentProtocol;
    
    protected function _before()
    {
        $this->experimentProtocol = Yii::$container->get('experimentProtocol');
    }

    protected function _after()
    {
    }

    // tests

    public function testApiEndPoint()
    {
        // ARRANGE
        $expected = 'experiment-protocols';

        // ACT
        $actual = $this->experimentProtocol->apiEndpoint();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * //@skip This function is doing too many things.
     *
     * Split it up then create tests for those functions. In particular, split into:
     * (1) get list id of an experiment; if it exists, return the id otherwise null
     * (2) create records (if list id doesn't exist)
     *
     * Caller must then make two calls, each for (1) and (2).
     */
    public function xtestGetListId()
    {

    }

}