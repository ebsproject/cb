<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use app\dataproviders\ArrayDataProvider;
use Yii;

class PlantingJobOccurrenceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $plantingJobOccurrence;
    
    protected function _before()
    {
        $this->plantingJobOccurrence = Yii::$container->get('plantingJobOccurrence');
    }

    /**
     * Test getting api endpoint for Planting job
     */
    public function testApiEndPointTest()
    {
        // ARRANGE
        $expected = 'planting-job-occurrences';

        // ACT
        $actual = $this->plantingJobOccurrence->apiEndpoint();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getting summary of Packing job
     */
    public function testRulesTest()
    {
        // ARRANGE
        $expected = [
            [[
                'occurrenceEntryCount',
                'occurrencePlotCount',
                'seedsPerEnvelope',
                'envelopesPerPlot',
                'creatorDbId',
                'modifierDbId',
            ], 'integer'],
            [[
                'experimentCode',
                'experimentName',
                'occurrenceName',
                'siteCode',
                'packageUnit',
                'plotDimensions'
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp'
            ], 'safe'],
        ];

        // ACT
        $actual = $this->plantingJobOccurrence->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}