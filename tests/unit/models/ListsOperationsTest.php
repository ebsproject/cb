<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;
class ListsOperationsTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $listsOperations;
    
    protected function _before()
    {
        $this->listsOperations = Yii::$container->get('listsOperations');
        // $this->lists = 
    }

    protected function _after()
    {
    }

    // tests
    public function testMergeList()
    {
        //ARRANGE
        // inputs
        $listDbIdArr = [1,2];
        $newListDbId = 1;
        $mode = 'copy';
        $removeDuplicates = false;

        $this->listsOperations->lists->setReturnValue('searchAll', 
            [
                'success' => true,
                'message' => '',
                'data' => [
                    [
                        'listDbId' => '1',
                        'abbrev' => 'LIST_1',
                        'type' => 'germplasm'
                    ],
                    [
                        'listDbId' => '2',
                        'abbrev' => 'LIST_2',
                        'type' => 'germplasm'
                    ]
                ],
                'totalCount' => 2,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAllMembers',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    [
                        'id' => '1',
                        'isActive' => 'true'
                    ],
                    [
                        'id' => '2',
                        'isActive' => 'true'
                    ]
                ],
                'totalCount' => 2,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('createMembers',
            [
                'success' => true,
                'message' => '',
                'data' => [
                ],
                'totalCount' => 2,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('updateOne',
            [
                'success' => true,
                'message' => '',
                'data' => [
                ],
                'totalCount' => 1,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $expected = [
            'success' => true
        ];

        $actual = $this->listsOperations->mergeList($listDbIdArr, $newListDbId, $mode, $removeDuplicates);
        $this->assertEquals($expected, $actual);
    }

    // test with lists that have different types
    public function testMergeListInvalid()
    {
        //ARRANGE
        // inputs
        $listDbIdArr = [1,2];
        $newListDbId = 1;
        $mode = 'copy';
        $removeDuplicates = false;

        $this->listsOperations->lists->setReturnValue('searchAll', 
            [
                'success' => true,
                'message' => '',
                'data' => [
                    [
                        'listDbId' => '1',
                        'abbrev' => 'LIST_1',
                        'type' => 'germplasm'
                    ],
                    [
                        'listDbId' => '2',
                        'abbrev' => 'LIST_2',
                        'type' => 'plot'
                    ]
                ],
                'totalCount' => 2,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $expected = [
            'success' => false,
            'message' => 'The lists you provided have different types.'
        ];

        $actual = $this->listsOperations->mergeList($listDbIdArr, $newListDbId, $mode, $removeDuplicates);
        $this->assertEquals($expected, $actual);
    }

    // test splitList with valid list input
    public function testSplitListValidInputHasChildLists()
    {
        // ARRANGE
        $listId = 1;
        $childListCount = 2;
        $splitOrder = 'desc';

        $this->listsOperations->lists->setReturnValue('getOne',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    'name' => 'Test List 1',
                    'abbrev' => 'TEST_LIST_1',
                    'displayName' => 'Test List 1',
                    'type' => 'germplasm',
                    'description' => '',
                    'remarks' => ''
                ],
                'totalCount' => 1,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAllMembers',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    [
                        'listMemberDbId' => '10',
                        'isActive' => 'true'
                    ],
                    [
                        'listMemberDbId' => '11',
                        'isActive' => 'true'
                    ]
                ],
                'totalCount' => 2,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAll',
            [
                'success' => true,
                'message' => '',
                'status' => 200,
                'data' => [],
                'totalCount' => 2,
                'totalPages' => 1,
                'body' => [
                    'metadata' => [
                        'pagination' => [
                            'totalCount' => 2,
                            'totalPages' => 1
                        ]
                    ],
                    'status' => [
                        'message' => ''
                    ]
                ]
            ]
        );
        
        $this->listsOperations->lists->setReturnValue('create',
            [
                'success' => true,
                'status'=>200,
                'message' => '',
                'data' => [
                    [
                        'listDbId' => '4',
                        'isActive' => 'true'
                    ]
                ]
            ]
        );

        $expected = [
            'success'=>true,
            'message'=>'',
            'targetLists'=>['4','1'],
            'childListMemberIds'=>[[10],[11]]
        ];

        // ACT
        $actual = $this->listsOperations->splitList($listId,$childListCount,$splitOrder);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    // test splitList with invalid list input no parent list
    public function testSplitListValidInputNoChildLists()
    {
        // ARRANGE
        $listId = 1;
        $childListCount = 2;
        $splitOrder = 'desc';

        $this->listsOperations->lists->setReturnValue('getOne',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    'name' => 'Test List 1',
                    'abbrev' => 'TEST_LIST_1',
                    'displayName' => 'Test List 1',
                    'type' => 'germplasm',
                    'description' => '',
                    'remarks' => ''
                ],
                'totalCount' => 1,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAllMembers',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    [
                        'listMemberDbId' => '10',
                        'isActive' => 'true'
                    ],
                    [
                        'listMemberDbId' => '11',
                        'isActive' => 'true'
                    ]
                ],
                'totalCount' => 2,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAll',
            [
                'success' => true,
                'message' => '',
                'status' => 200,
                'data' => [],
                'totalCount' => 0,
                'totalPages' => 1,
                'body' => [
                    'metadata' => [
                        'pagination' => [
                            'totalCount' => 0,
                            'totalPages' => 1
                        ]
                    ],
                    'status' => [
                        'message' => ''
                    ]
                ]
            ]
        );
        
        $this->listsOperations->lists->setReturnValue('create',
            [
                'success' => true,
                'status'=>200,
                'message' => '',
                'data' => [
                    [
                        'listDbId' => '3',
                        'isActive' => 'true'
                    ]
                ]
            ]
        );

        $expected = [
            'success'=>true,
            'message'=>'',
            'targetLists'=>['3','1'],
            'childListMemberIds'=>[[10],[11]]
        ];

        // ACT
        $actual = $this->listsOperations->splitList($listId,$childListCount,$splitOrder);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    // test splitList with invalid list input no parent list
    public function testSplitListInvalidInputNoListRecord()
    {
        // ARRANGE
        $listId = 1;
        $childListCount = 2;
        $splitOrder = 'desc';

        $this->listsOperations->lists->setReturnValue('getOne',
            [
                'success' => false,
                'message' => 'No record.',
                'data' => [],
                'totalCount' => 0,
                'status' => 400,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAllMembers',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    [
                        'listMemberDbId' => '10',
                        'isActive' => 'true'
                    ],
                    [
                        'listMemberDbId' => '11',
                        'isActive' => 'true'
                    ]
                ],
                'totalCount' => 2,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAll',
            [
                'success' => true,
                'status' => 200,
                'message' => '',
                'data' => [],
                'totalCount' => 2,
                'totalPages' => 1,
                'body' => [
                    'metadata' => [
                        'pagination' => [
                            'totalCount' => 2,
                            'totalPages' => 1
                        ]
                    ],
                    'status' => [
                        'message' => ''
                    ]
                ]
            ]
        );
        
        $this->listsOperations->lists->setReturnValue('create',
            [
                'success' => true,
                'status'=>200,
                'message' => '',
                'data' => [
                    [
                        'listDbId' => '4',
                        'isActive' => 'true'
                    ]
                ]
            ]
        );

        $expected = [
            'success'=>false,
            'message'=>'Error encountered in retrieving parent list information. Kindly contact Administrator.',
            'targetLists'=>[],
            'childListMemberIds'=>[]
        ];

        // ACT
        $actual = $this->listsOperations->splitList($listId,$childListCount,$splitOrder);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    // test splitList with invalid list input no parent list
    public function testSplitListInvalidInputNoListMembers()
    {
        // ARRANGE
        $listId = 1;
        $childListCount = 2;
        $splitOrder = 'desc';

        $this->listsOperations->lists->setReturnValue('getOne',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    'name' => 'Test List 1',
                    'abbrev' => 'TEST_LIST_1',
                    'displayName' => 'Test List 1',
                    'type' => 'germplasm',
                    'description' => '',
                    'remarks' => ''
                ],
                'totalCount' => 1,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAllMembers',
            [
                'success' => true,
                'message' => '',
                'data' => [],
                'totalCount' => 0,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAll',
            [
                'success' => true,
                'status' => 200,
                'message' => '',
                'data' => [],
                'totalCount' => 2,
                'totalPages' => 1,
                'body' => [
                    'metadata' => [
                        'pagination' => [
                            'totalCount' => 2,
                            'totalPages' => 1
                        ]
                    ],
                    'status' => [
                        'message' => ''
                    ]
                ]
            ]
        );

        $this->listsOperations->lists->setReturnValue('create',
            [
                'success' => true,
                'status'=>200,
                'message' => '',
                'data' => [
                    [
                        'listDbId' => '4',
                        'isActive' => 'true'
                    ]
                ]
            ]
        );

        $expected = [
            'success'=>false,
            'message'=>'List has no members.',
            'targetLists'=>[],
            'childListMemberIds'=>[]
        ];

        // ACT
        $actual = $this->listsOperations->splitList($listId,$childListCount,$splitOrder);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    // test splitList with invalid list input no parent list
    public function testSplitListInvalidInputNoCreatedChildLists()
    {
        // ARRANGE
        $listId = 1;
        $childListCount = 2;
        $splitOrder = 'desc';

        $this->listsOperations->lists->setReturnValue('getOne',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    'name' => 'Test List 1',
                    'abbrev' => 'TEST_LIST_1',
                    'displayName' => 'Test List 1',
                    'type' => 'germplasm',
                    'description' => '',
                    'remarks' => ''
                ],
                'totalCount' => 1,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAllMembers',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    [
                        'listMemberDbId' => '10',
                        'isActive' => 'true'
                    ],
                    [
                        'listMemberDbId' => '11',
                        'isActive' => 'true'
                    ]
                ],
                'totalCount' => 2,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAll',
            [
                'success' => true,
                'status' => 200,
                'message' => '',
                'data' => [],
                'totalCount' => 2,
                'totalPages' => 1,
                'body' => [
                    'metadata' => [
                        'pagination' => [
                            'totalCount' => 2,
                            'totalPages' => 1
                        ]
                    ],
                    'status' => [
                        'message' => ''
                    ]
                ]
            ]
        );

        $this->listsOperations->lists->setReturnValue('create',
            [
                'success' => false,
                'status'=>400,
                'message' => '',
                'data' => []
            ]
        );

        $expected = [
            'success'=>false,
            'message'=>'Error in creating new child lists.',
            'targetLists'=>[],
            'childListMemberIds'=>[[10],[11]]
        ];

        // ACT
        $actual = $this->listsOperations->splitList($listId,$childListCount,$splitOrder);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    // test with lists that have different types
    public function testGetChildCount()
    {
        // ARRANGE
        $listDbId = 1;

        $this->listsOperations->lists->setReturnValue('getOne',
            [
                'success' => true,
                'message' => '',
                'data' => [
                    'name' => 'Test List 1',
                    'abbrev' => 'TEST_LIST_1',
                    'display_name' => 'Test List 1'
                ],
                'totalCount' => 2,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        $this->listsOperations->lists->setReturnValue('searchAll',
            [
                'success' => true,
                'status' => 200,
                'message' => '',
                'data' => [                    
                    [
                        'listDbId' => '2',
                        'abbrev' => 'TEST_LIST_1_1',
                        'type' => 'germplasm',
                        'remarks' => 'Parent List: Test List 1'
                    ],
                    [
                        'listDbId' => '3',
                        'abbrev' => 'TEST_LIST_1_2',
                        'type' => 'germplasm',
                        'remarks' => 'Parent List: Test List 1'
                    ]
                ],
                'totalCount' => 2,
                'totalPages' => 1,
                'body' => [
                    'metadata' => [
                        'pagination' => [
                            'totalCount' => 2,
                            'totalPages' => 1
                        ]
                    ],
                    'status' => [
                        'message' => ''
                    ],
                    [
                        'listDbId' => '2',
                        'abbrev' => 'TEST_LIST_1_1',
                        'type' => 'germplasm',
                        'remarks' => 'Parent List: Test List 1'
                    ],
                    [
                        'listDbId' => '3',
                        'abbrev' => 'TEST_LIST_1_2',
                        'type' => 'germplasm',
                        'remarks' => 'Parent List: Test List 1'
                    ]
                ]
            ]
        );

        $expected = [
            'status'=>200,
            'message'=>'',
            'count'=>2
        ];

        // ACT
        $actual = $this->listsOperations->getChildCount($listDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    // test update parent info of newly processed list members
    public function testUpdateParentInfoValidInput(){
        // ARRANGE
        $expected = [
                'success'=>true,
                'message'=>''
            ];

        $listsMembersIds = [
            [1,2,3,4,5],
            [6,7,8,9,0]
        ];
        $newListIds = [11,12];

        $this->listsOperations->listMember->setReturnValue('updateMany',
            [
                'success' => true,
                'message' => 'Successfully updated list member record.',
                'data' => [],
                'totalCount' => 1,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        // ACT
        $actual = $this->listsOperations->updateParentInfo($listsMembersIds,$newListIds);
    
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test update parent info of newly processed list members with invalid updateMany() result
    public function testUpdateParentInfoInvalidInput(){
        // ARRANGE
        $expected = [
                'success'=>true,
                'message'=>''
            ];

        $listsMembersIds = [
            [1,2,3,4,5],
            [6,7,8,9,0]
        ];
        $newListIds = [11,12];

        $this->listsOperations->listMember->setReturnValue('updateMany',
            [
                'success' => false,
                'message' => 'Successfully updated list member record.',
                'data' => [],
                'totalCount' => 1,
                'status' => 200,
                'totalPages' => 1
            ]
        );

        // ACT
        $actual = $this->listsOperations->updateParentInfo($listsMembersIds,$newListIds);
    
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }
}