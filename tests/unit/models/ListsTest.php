<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace models;

use Yii;
class ListsTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $listsModel;
    
    protected function _before()
    {
        $this->listsModel = Yii::$container->get('listsModel');
    }

    protected function _after()
    {
    }

    public function testSearchAllMembers() {
        // ARRANGE
        $expected = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>2,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'id' => '1', 'isActive' => 'true' ],
                        [ 'id' => '2', 'isActive' => 'true' ]                     
                    ]
                ]
            ]
        ];

        $dbId = 123;
        $params = null;
        $filters = '';
        $retrieveAll = false;

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>2,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'id' => '1', 'isActive' => 'true' ],
                        [ 'id' => '2', 'isActive' => 'true' ]                     
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->listsModel->searchAllMembers($dbId, $params, $filters, $retrieveAll);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testCreateMembersDefaultParameters() {
        // ARRANGE
        $expected = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>2,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'id' => '1', 'isActive' => 'true' ],
                        [ 'id' => '2', 'isActive' => 'true' ],
                        [ 'id' => '3', 'isActive' => 'true' ]                     
                    ]
                ]
            ]
        ];

        $listId = 123;
        $members = [
            ['id' => 1, 'displayValue' => 'One', 'remarks' => 'one'],
            ['id' => 2, 'displayValue' => 'Two', 'remarks' => 'two'],
            ['id' => 3, 'displayValue' => 'Three', 'remarks' => 'three']
        ];
        $backgroundProcess = false;
        $additionalParams = [];
        $memberIdsOnly = false;
        $parseMembers = true;

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>2,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'id' => '1', 'isActive' => 'true' ],
                        [ 'id' => '2', 'isActive' => 'true' ],
                        [ 'id' => '3', 'isActive' => 'true' ]                     
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->listsModel->createMembers($listId, $members, $backgroundProcess, $additionalParams, $memberIdsOnly, $parseMembers);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testCreateMembersMemberIdsOnly(){
        // ARRANGE
        $expected = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>2,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'id' => '1', 'isActive' => 'true' ],
                        [ 'id' => '2', 'isActive' => 'true' ],
                        [ 'id' => '3', 'isActive' => 'true' ]                     
                    ]
                ]
            ]
        ];

        $listId = 123;
        $members = [1,2,3];
        $backgroundProcess = false;
        $additionalParams = [];
        $memberIdsOnly = true;
        $parseMembers = true;

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>2,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'id' => '1', 'isActive' => 'true' ],
                        [ 'id' => '2', 'isActive' => 'true' ],
                        [ 'id' => '3', 'isActive' => 'true' ]                     
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->listsModel->createMembers($listId, $members, $backgroundProcess, $additionalParams, $memberIdsOnly, $parseMembers);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testCreateMembersBgProcess(){
        // ARRANGE
        $expected = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>2,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'id' => '1', 'isActive' => 'true' ],
                        [ 'id' => '2', 'isActive' => 'true' ],
                        [ 'id' => '3', 'isActive' => 'true' ]                     
                    ]
                ]
            ]
        ];

        $listId = 123;
        $members = [1,2,3];
        $backgroundProcess = true;
        $additionalParams = [];
        $memberIdsOnly = true;
        $parseMembers = true;

        // Mock session variable
        $tokenValue = "";
        $refreshTokenValue = "";

        \Yii::$app->session->set("user.b4r_api_v3_token", $tokenValue);
        \Yii::$app->session->set("user.user.refresh_token", $refreshTokenValue);

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>2,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'id' => '1', 'isActive' => 'true' ],
                        [ 'id' => '2', 'isActive' => 'true' ],
                        [ 'id' => '3', 'isActive' => 'true' ]                     
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->listsModel->createMembers($listId, $members, $backgroundProcess, $additionalParams, $memberIdsOnly, $parseMembers);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

}