<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;

class TraitsTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $traits;
    
    protected function _before()
    {
        $this->traits = Yii::$container->get('traits');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * testRules: Test 1
     * Get validation rules for Traits model
     */
    public function testRulesTest1()
    {
        // ARRANGE
        $expected = [
            [['abbrev', 'bibliographicalReference', 'dataLevel', 'dataType', 'description', 'displayName', 'isComputed', 'label', 'name', 'notNull', 'ontologyReference', 'remarks', 'status', 'synonym', 'targetModel', 'targetTable', 'type', 'usage', 'creationTimestamp', 'creator', 'modificationTimestamp', 'modifier'], 'safe'],
        ];

        // ACT
        $actual = $this->traits->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testApiEndPoint: Test 1
     * Get api endpoint for Traits
     */
    public function testApiEndPointTest1()
    {
        // ARRANGE
        $expected = 'variables';

        // ACT
        $actual = $this->traits->apiEndpoint();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}