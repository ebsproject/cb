<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;

use app\dataproviders\ArrayDataProvider;
use app\models\Variable;

class GermplasmFileUploadTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $germplasmFileUpload;

    protected function _before(){
        $this->germplasmFileUpload = Yii::$container->get('germplasmFileUpload');
    }

    protected function _after(){
    }

    // tests

    /**
     * Verify that prefix string value is returned
     */
    public function testGetPrefix()
    {
        $expected = 'gefu-';
        $actual = $this->germplasmFileUpload->getPrefix();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that string model name is returned
     */
    public function testGetModelName()
    {
        $expected = 'GermplasmFileUpload';
        $actual = $this->germplasmFileUpload->getModelName();
        $this->assertEquals($expected, $actual);
    }

    public function testExtractBrowserFiltersNoFilters(){
        // ARRANGE
        $expected = [
            'limit' => "limit=10",
            'limitCount' => 10,
            'page' => "&page=1",
            'pageCount' => 1,
            'sort' => "&sort=germplasmFileUploadDbId:DESC",
            'filters' => []
        ];

        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => []
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->germplasmFileUpload->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 10);

        // ACT
        $actual = $this->germplasmFileUpload->extractBrowserFilters([]);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    public function testExtractBrowserFiltersWithFiltersSortDESC(){
        // ARRANGE
        $expected = [
            'limit' => "limit=10",
            'limitCount' => '10',
            'page' => "&page=1",
            'pageCount' => 1,
            'sort' => "&sort=fileStatus:DESC",
            'filters' => [
                "fileStatus" => 'equals validated'
            ]
        ];

        $queryParams = [
            "gefu-grid-page" => 1,
            "sort" => "-fileStatus",
            "GermplasmFileUpload" => [
                "fileStatus" => 'validated'
            ]
        ];

        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->germplasmFileUpload->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 10);

        // ACT
        $actual = $this->germplasmFileUpload->extractBrowserFilters([]);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    public function testExtractBrowserFiltersWithFiltersSortASC(){
        // ARRANGE
        $expected = [
            'limit' => "limit=10",
            'limitCount' => '10',
            'page' => "&page=1",
            'pageCount' => 1,
            'sort' => "&sort=fileStatus:ASC|germplasmFileUploadDbId:DESC",
            'filters' => [
                "fileStatus" => 'equals validated'
            ]
        ];


        $queryParams = [
            "gefu-grid-page" => 1,
            "sort" => "fileStatus|-germplasmFileUploadDbId",
            "GermplasmFileUpload" => [
                "fileStatus" => 'validated'
            ]
        ];

        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->germplasmFileUpload->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 10);

        // ACT
        $actual = $this->germplasmFileUpload->extractBrowserFilters([]);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    public function testSearchNoFilters(){
        // ARRANGE
        $expected = new ArrayDataProvider([
            'allModels' => [
                [
                    'germplasmFileUploadDbId'=>1,
                    'fileStatus'=>'validated'
                ]
            ],
            'key' => 'germplasmFileUploadDbId',
            'sort' => [
                'attributes' => [
                    'germplasmFileUploadDbId',
                    'programDbId',
                    'programCode',
                    'fileName',
                    'fileData',
                    'fileStatus',
                    'fileUploadOrigin',
                    'germplasmCount',
                    'seedCount',
                    'packageCount',
                    'errorLog',
                    'remarks',
                    'uploader',
                    'uploadTimestamp',
                    'modifier',
                    'modificationTimestamp'
                ],
                'defaultOrder' => [
                    'uploadTimestamp' => SORT_DESC,
                    'germplasmFileUploadDbId' => SORT_DESC,
                ],
            ],
            'restified' => true,
            'id' => 'gefu-grid',
            'totalCount' => 1
        ]);

        $params = null;
        $filters = null;

        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => []
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->germplasmFileUpload->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 10);

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'pageSize' => 100,
                                    'totalCount' => 1,
                                    'currentPage' => 1,
                                    'totalPages' => 1
                                ],
                                'status' => [
                                    [
                                        'message' => 'Test success message'
                                    ]
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'germplasmFileUploadDbId'=>1,
                                        'fileStatus'=>'validated'
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $returnValue = [
            'data' => [
                'germplasmFileUploadDbId'=>1,
                'fileStatus'=>'validated'
            ],
            'totalCount' => 1,
            'status' => 200
        ];

        $germplasmFileUploadMock = \Yii::$container->get('germplasmFileUploadMock');
        $germplasmFileUploadMock->setReturnValue("searchAll", $returnValue);

        // ACT
        $actual = $this->germplasmFileUpload->search($params,$filters);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    public function testSearchWithFilters(){
        // ARRANGE
        $expected = new ArrayDataProvider([
            'allModels' => [
                [
                    'germplasmFileUploadDbId'=>1,
                    'fileStatus'=>'validated'
                ],
                [
                    'germplasmFileUploadDbId'=>2,
                    'fileStatus'=>'in queue'
                ]
            ],
            'key' => 'germplasmFileUploadDbId',
            'sort' => [
                'attributes' => [
                    'germplasmFileUploadDbId',
                    'programDbId',
                    'programCode',
                    'fileName',
                    'fileData',
                    'fileStatus',
                    'fileUploadOrigin',
                    'germplasmCount',
                    'seedCount',
                    'packageCount',
                    'errorLog',
                    'remarks',
                    'uploader',
                    'uploadTimestamp',
                    'modifier',
                    'modificationTimestamp'
                ],
                'defaultOrder' => [
                    'uploadTimestamp' => SORT_DESC,
                    'germplasmFileUploadDbId' => SORT_DESC,
                ],
            ],
            'restified' => true,
            'id' => 'gefu-grid',
            'totalCount' => 2
        ]);

        $params = null;
        $filters = null;

        $queryParams = [
            "gefu-grid-page" => 1,
            "sort" => "-fileStatus"
        ];

        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->germplasmFileUpload->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 10);

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'pageSize' => 100,
                                    'totalCount' => 2,
                                    'currentPage' => 1,
                                    'totalPages' => 1
                                ],
                                'status' => [
                                    [
                                        'message' => 'Test success message'
                                    ]
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'germplasmFileUploadDbId'=>1,
                                        'fileStatus'=>'validated'
                                    ],
                                    [
                                        'germplasmFileUploadDbId'=>2,
                                        'fileStatus'=>'in queue'
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $returnValue = [
            'data' => [
                'germplasmFileUploadDbId'=>1,
                'fileStatus'=>'validated'
            ],
            'totalCount' => 1,
            'status' => 200
        ];

        $germplasmFileUploadMock = \Yii::$container->get('germplasmFileUploadMock');
        $germplasmFileUploadMock->setReturnValue("searchAll", $returnValue);

        // ACT
        $actual = $this->germplasmFileUpload->search($params,$filters);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    public function testApiEndPoint()
    {
        // ARRANGE
        $expected = 'germplasm-file-uploads';

        // ACT
        $actual = $this->germplasmFileUpload->apiEndpoint();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetFileUploadStatusValues()
    {
        // ARRANGE
        $expected = [
            'SCALEVALUE#1' => 'scaleValue#1',
            'SCALEVALUE#2' => 'scaleValue#2'
        ];

        $this->germplasmFileUpload->variableModel->setReturnValue(
            'getVariableByAbbrev', 
            ['variableDbId' => 123]
        );

        $this->germplasmFileUpload->variableModel->setReturnValue(
            'getVariableScales', 
            [
                'scaleValues' => [
                    ["value" => 'scaleValue#1'],
                    ["value" => 'scaleValue#2']
                ]
            ]
        );

        // ACT
        $actual = $this->germplasmFileUpload->getFileUploadStatusValues();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetFileUploadStatusValuesNoScaleValues()
    {
        // ARRANGE
        $expected = [];

        $this->germplasmFileUpload->variableModel->setReturnValue(
            'getVariableByAbbrev', ['variableDbId' => 123]);

        $this->germplasmFileUpload->variableModel->setReturnValue(
            'getVariableScales', []);

        // ACT
        $actual = $this->germplasmFileUpload->getFileUploadStatusValues();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetFileUploadStatusValuesNoVariable()
    {
        // ARRANGE
        $expected = [];

        $this->germplasmFileUpload->variableModel->setReturnValue(
            'getVariableByAbbrev', []);

        $this->germplasmFileUpload->variableModel->setReturnValue(
            'getVariableScales', []);

        // ACT
        $actual = $this->germplasmFileUpload->getFileUploadStatusValues();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testBuildConfigAbbrev()
    {
        // ARRANGE
        $expected = 'GM_GLOBAL_GERMPLASM_UPLOAD_CONFIG';

        $entity = 'germplasm';
        $action = 'upload';
        $level = 'global';
        $levelValue = '';

        // ACT
        $actual = $this->germplasmFileUpload->buildConfigAbbrev(
            $entity,$action,$level,$levelValue);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetConfigValues()
    {
        // ARRANGE
        $expected = [
            'level' => 'role',
            'levelValue' => 'COLLABORATOR',
            'configValues' => [
                'values' => [
                    ['configValues']
                ]
            ] 
        ];

        $entity = 'germplasm';
        $action = 'upload';
        $userRole = 'COLLABORATOR';
        $program = 'IRSEA';
        $cropCode = 'RICE';

        $configVariables = [
            'values' => [
                ['configValues']
            ]
        ];
        
        $this->germplasmFileUpload->configModel->setReturnValue(
            'getConfigByAbbrev',
            $configVariables);

        // ACT
        $actual = $this->germplasmFileUpload->getConfigValues(
            $entity,$action,$userRole,$program,$cropCode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetFileUploadVariablesNonEmptyConfig()
    {
        // ARRANGE
        $expected = [
            'variables' => ['TEST_ABBREV_1'],
            'requiredVariables' => ['TEST_ABBREV_1']
        ];

        $configValues = [
            'values' => [
                ['abbrev' => 'TEST_ABBREV_1', 'usage' => 'required']
            ]
        ];

        $this->germplasmFileUpload->variableModel->setReturnValue(
            'getVariableByAbbrev', ['abbrev' => 'TEST_ABBREV_1']);

        // ACT
        $actual = $this->germplasmFileUpload->getFileUploadVariables($configValues);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals($expected, $actual);
    }

    public function testGetFileUploadVariablesNoVariable()
    {
        // ARRANGE
        $expected = [
            'variables' => [],
            'requiredVariables' => []
        ];

        $configValues = [
            'values' => [
                ['abbrev' => 'TEST_ABBREV_1', 'usage' => 'required']
            ]
        ];

        $this->germplasmFileUpload->variableModel->setReturnValue(
            'getVariableByAbbrev', []);

        // ACT
        $actual = $this->germplasmFileUpload->getFileUploadVariables($configValues);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals($expected, $actual);
    }

    public function testGetFileUploadVariablesEmptyConfig()
    {
        // ARRANGE
        $expected = [];

        $configValues = [];

        // ACT
        $actual = $this->germplasmFileUpload->getFileUploadVariables($configValues);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}

?>