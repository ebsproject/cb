<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace models;

use Yii;
class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $user;
    
    protected function _before()
    {
        $this->user = Yii::$container->get('user');
    }

    protected function _after()
    {
    }

    /**
     * testDeleteUser: Test 1
     * Result is 200 OK
     */
    public function testDeleteUser1()
    {
        // ARRANGE
        $method = 'DELETE';
        $path = 'persons';
        $personId = 10;

        // Mock Yii app component
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'success' => true,
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = true;

        // ACT
        $actual = $this->user->deleteUser($personId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testDeleteUser: Test 2
     * Result is 4xx
     */
    public function testDeleteUser2()
    {
        // ARRANGE
        $method = 'DELETE';
        $path = 'persons';
        $personId = 10;

        // Mock Yii app component
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 400,
                        'success' => false,
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = false;

        // ACT
        $actual = $this->user->deleteUser($personId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }



}