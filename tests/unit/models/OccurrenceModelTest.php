<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;

class OccurrenceModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $occurrenceModel;
    
    protected function _before()
    {
        $this->occurrenceModel = Yii::$container->get('occurrenceModel');
    }

    protected function _after()
    {
    }

    // Tests

    /**
     * Candidate for splitting into smaller functions.
     *
     * In particular, each or most of the returned array values:
     * . get code
     * . get name
     * . get description
     * . get status and status array
     * . get primary info
     * . get secondary info
     * . get audit info
     * . get monitoring info
     * . get location id
     */
    public function xtestGetDetailViewInfo()
    {

    }

    /**
     * Test getting percentage value
     */
    public function testStatusPercentage()
    {
        // ARRANGE
        $count = 10;
        $totalCount = 10;

        $expected = 100;

        // ACT
        $actual = $this->occurrenceModel->getStatusPercentage(10,10);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    public function xtestGet() {
        // ARRANGE
        // inputs
        $id = 1;

        // mock - api class
        $apiReturnValue = [
            "status" => 200,
            "data" => [
                [
                    "occurrenceDbId" => "".$id
                ]
            ]
        ];

        $this->occurrenceModel->api->setReturnValue('getApiResults',$apiReturnValue);

        // expected
        $expected = ["occurrenceDbId" => "".$id];

        // ACT
        $actual = $this->occurrenceModel->get($id);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }
}