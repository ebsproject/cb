<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use app\dataproviders\ArrayDataProvider;
use Yii;

class PlantingJobEntryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $plantingJobEntry;
    
    protected function _before()
    {
        $this->plantingJobEntry = Yii::$container->get('plantingJobEntry');
    }

    /**
     * Test getting api endpoint for Planting job
     */
    public function testApiEndPointTest()
    {
        // ARRANGE
        $expected = 'planting-job-entries';

        // ACT
        $actual = $this->plantingJobEntry->apiEndpoint();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getting summary of Packing job
     */
    public function testRulesTest()
    {
        // ARRANGE
        $expected = [
            [[
                'envelopeCount',
                'packageDbId',
                'actualEntryDbId',
                'actualGermplasmDbId',
                'actualPackageDbId',                
                'creatorDbId',
                'modifierDbId',
                'entryPlotCount',
                'envelopeCount',
                'packageQuantity',
            ], 'integer'],
            [[
                'requiredPackageQuantity',
                'actualPackageReserved',
                'actualAvailableQuantity',
            ], 'double'],
            [[
                'experimentCode',
                'experimentName',
                'occurrenceName',
                'siteCode',
                'requiredPackageUnit',
                'designation',
                'parentage',
                'seedName',
                'packageLabel',
                'entryName',
                'entryCode',
                'isSufficient',
                'actualGermplasmName',
                'actualParentage',
                'actualSeedName',
                'actualPackageLabel',
                'actualPackageUnit',
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp',
                'isReplaced'
            ], 'safe'],
        ];

        // ACT
        $actual = $this->plantingJobEntry->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}