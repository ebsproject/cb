<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;

class GermplasmTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $germplasm;

    protected function _before()
    {
        $this->germplasm = Yii::$container->get('germplasm');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * testRules: Test 1
     * Get validation rules for Germplasm model
     */
    public function testRulesTest1()
    {
        // ARRANGE
        $expected = [
            [['program_id', 'season_id', 'cross_id', 'creator_id', 'modifier_id', 'gid', 'product_gid_id', 'seed_storage_id', 'organization_id', 'institute_id'], 'integer'],
            [['designation', 'name_type', 'product_type', 'parentage', 'generation'], 'required'],
            [['designation', 'name_type', 'product_type', 'mta_status', 'parentage', 'generation', 'iris_preferred_id', 'breeding_line_name', 'derivative_name', 'fixed_line_name', 'selection_method', 'notes', 'ip_rights'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'program', 'creationTimestamp', 'creator', 'germplasmNameType', 'germplasmType', 'germplasmState', 'germplasmNormalizedName', 'germplasmCode', 'designation', 'generation', 'modificationTimestamp', 'modifier', 'nameType', 'otherNames', 'parentage', 'programName', 'taxonomyName', 'cropCode'], 'safe'],
            [['is_void'], 'boolean'],
            [['display_name', 'product_key', 'system_product_name'], 'string', 'max' => 256],
            [['ip_status'], 'string', 'max' => 32],
            [['product_status'], 'string', 'max' => 255],
            [['product_key'], 'unique'],
        ];

        // ACTUAL
        $actual = $this->germplasm->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testApiEndPoint: Test 1
     * Get api endpoint for Germplasm
     */
    public function testApiEndPointTest1()
    {
        // ARRANGE
        $expected = 'germplasm';

        // ACTUAL
        $actual = $this->germplasm->apiEndpoint();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetGermplasmState: Test 1
     * Retrieves existing distinct germplasm state in the database
     */
    public function testGetGermplasmStateTest1()
    {
        // ARRANGE

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'variableDbId' => 1,
                                        'scales' => [
                                            [
                                                'scaleValues' => [
                                                    [
                                                        'orderNumber'=> 1,
                                                        'value'=> 'fixed',
                                                        'description'=> 'fixed',
                                                        'displayName'=> 'fixed',
                                                        'scaleValueStatus'=> null,
                                                        'remarks'=> null
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            [
                'orderNumber'=> 1,
                'value'=> 'fixed',
                'description'=> 'fixed',
                'displayName'=> 'fixed',
                'scaleValueStatus'=> null,
                'remarks'=> null
            ]
        ];

        // ACTUAL
        $actual = $this->germplasm->getGermplasmState();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetGermplasmState: Test 2
     * No germplasm state in the database
     */
    public function testGetGermplasmStateTest2()
    {
        // ARRANGE

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'variableDbId' => null
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [];

        // ACTUAL
        $actual = $this->germplasm->getGermplasmState();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetGermplasmState: Test 3
     * No scale values for germplasm state in the database
     */
    public function testGetGermplasmStateTest3()
    {
        // ARRANGE

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'variableDbId' => 1,
                                        'scales' => [
                                            [
                                                'scaleValues' => null
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [];

        // ACTUAL
        $actual = $this->germplasm->getGermplasmState();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetGermplasmGeneration: Test 1
     * Retrieves existing distinct germplasm generation in the database
     */
    public function testGetGermplasmGenerationTest1()
    {
        // ARRANGE

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'variableDbId' => 1,
                                        'scales' => [
                                            [
                                                'scaleValues' => [
                                                    [
                                                        'orderNumber'=> 1,
                                                        'value'=> 'UNKNOWN',
                                                        'description'=> 'UNKNOWN',
                                                        'displayName'=> 'UNKNOWN',
                                                        'scaleValueStatus'=> 'show',
                                                        'remarks'=> null
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            [
                'orderNumber'=> 1,
                'value'=> 'UNKNOWN',
                'description'=> 'UNKNOWN',
                'displayName'=> 'UNKNOWN',
                'scaleValueStatus'=> 'show',
                'remarks'=> null
            ]
        ];

        // ACTUAL
        $actual = $this->germplasm->getGermplasmGeneration();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetGermplasmGeneration: Test 2
     * No germplasm generation in the database
     */
    public function testGetGermplasmGenerationTest2()
    {
        // ARRANGE

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'variableDbId' => null
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [];

        // ACTUAL
        $actual = $this->germplasm->getGermplasmGeneration();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetGermplasmGeneration: Test 3
     * No scale values for germplasm generation in the database
     */
    public function testGetGermplasmGenerationTest3()
    {
        // ARRANGE

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'variableDbId' => 1,
                                        'scales' => [
                                            [
                                                'scaleValues' => null
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [];

        // ACTUAL
        $actual = $this->germplasm->getGermplasmGeneration();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testUpdateGermplasm: Test 1
     * Updates germplasm given id and array of data (success)
     */
    public function testUpdateGermplasmTest1()
    {
        // ARRANGE
        $id = 1;
        $data = [];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = true;

        // ACTUAL
        $actual = $this->germplasm->updateGermplasm($id, $data);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testUpdateGermplasm: Test 2
     * Updates germplasm given id and array of data (fail)
     */
    public function testUpdateGermplasmTest2()
    {
        // ARRANGE
        $id = 1;
        $data = [];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 400
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = false;

        // ACTUAL
        $actual = $this->germplasm->updateGermplasm($id, $data);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testSearchGermplasm: Test 1
     * Search for germplasm given search parameters
     */
    public function testSearchGermplasmTest1()
    {
        // ARRANGE
        $params = [
            'designation' => 'Germplasm'
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'germplasmDbId' => 1,
                                        'designation' => 'Germplasm',
                                        'parentage' => 'germ/plasm',
                                        'generation' => 'UNKNOWN',
                                        'germplasmState' => 'fixed',
                                        'germplasmNameType' => 'line_name',
                                        'germplasmNormalizedName' => 'GERMPLASM',
                                        'germplasmCode' => 'GE000000000001',
                                        'germplasmType' => 'fixed_line',
                                        'otherNames' => 'Germplasm',
                                        'germplasmDocument' => null,
                                        'creatorDbId' => 1,
                                        'creator' => 'EBS, Admin',
                                        'modifierDbId' => 2,
                                        'modifier' => 'Modifier',
                                        'creationTimestamp' => '2021-06-09T09:33:15.841Z',
                                        'modificationTimestamp' => '2021-10-17T15:24:41.125Z'
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            [
                'germplasmDbId' => 1,
                'designation' => 'Germplasm',
                'parentage' => 'germ/plasm',
                'generation' => 'UNKNOWN',
                'germplasmState' => 'fixed',
                'germplasmNameType' => 'line_name',
                'germplasmNormalizedName' => 'GERMPLASM',
                'germplasmCode' => 'GE000000000001',
                'germplasmType' => 'fixed_line',
                'otherNames' => 'Germplasm',
                'germplasmDocument' => null,
                'creatorDbId' => 1,
                'creator' => 'EBS, Admin',
                'modifierDbId' => 2,
                'modifier' => 'Modifier',
                'creationTimestamp' => '2021-06-09T09:33:15.841Z',
                'modificationTimestamp' => '2021-10-17T15:24:41.125Z'
            ]
        ];

        // ACTUAL
        $actual = $this->germplasm->searchGermplasm($params);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetGermplasmEntriesCount: Test 1
     * Retrieve entries count for germplasm
     */
    public function testGetGermplasmEntriesCount()
    {
        // ARRANGE
        $id = 1234;
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        "status" => 200,
                        "body" => [
                            "metadata"=>[
                                "pagination"=>[
                                    "totalCount"=>1,
                                    "totalPages"=>1
                                ]
                            ],
                            "result"=>[
                                "data" => [
                                    ["entriesCount" => 0, "germplasmDbId" => 1234]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            ["entriesCount" => 0, "germplasmDbId" => 1234]
        ];

        // ACTUAL
        $actual = $this->germplasm->getGermplasmEntriesCount($id);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetGermplasmSeedsCount: Test 1
     * Retrieve seeds count for germplasm
     */
    public function testGetGermplasmSeedsCount()
    {
        // ARRANGE
        $id = 1234;
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        "status" => 200,
                        "body" => [
                            "metadata"=>[
                                "pagination"=>[
                                    "totalCount"=>1,
                                    "totalPages"=>1
                                ]
                            ],
                            "result"=>[
                                "data" => [
                                    ["seedsCount" => 0, "germplasmDbId" => 1234]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [["seedsCount" => 0, "germplasmDbId" => 1234]];

        // ACTUAL
        $actual = $this->germplasm->getGermplasmSeedsCount($id);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}