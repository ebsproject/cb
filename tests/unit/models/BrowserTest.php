<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;
class BrowserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $browser;
    
    protected function _before()
    {
        $this->browser = Yii::$container->get('browser');
    }

    /**
     * Test formatting string to camel case
     */
    public function testFormatToCamelCase()
    {

        // ARRANGE
        $variable = 'PLANTING_DATE';

        $expected = 'plantingDate';

        // ACT
        $actual = $this->browser->formatToCamelCase($variable);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * Test building web form columns
     */
    public function testBuildFormColumns(){
        
        // arrange
        $fields = [
            [
                'variable_abbrev' => 'DESCRIPTION',
            ]
        ];

        $entity = 'occurrence';

        $returnValue = [
            'data' => [
                [
                    'variableDbId' => 725,
                    'abbrev' => 'DESCRIPTION',
                    'label' => 'DESCRIPTION',
                    'name' => 'description',
                    'displayName' => 'Description',
                    'type' => 'metadata',
                    'dataType' => 'character varying'
                ]
            ]
        ];

        $this->browser->variable->setReturnValue('searchAll', $returnValue);

        $expected = [
            [
                'attribute' => 'DESCRIPTION',
                'encodeLabel' => false,
                'format' => 'raw',
                'label' => '<span title="DESCRIPTION">DESCRIPTION </span>',
                'order' => 'middle',
                'vAlign' => 'middle',
                'value' => function(){},
                'visible' => true
            ]
        ];

        // ACT
        $actual = $this->browser->buildFormColumns($fields, $entity);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test building web form columns with invalid variable
     */
    public function testBuildFormColumnsInvalidVariable(){
        
        // arrange
        $fields = [
            [
                'variable_abbrev' => 'INVALID_VARIABLE',
            ]
        ];

        $entity = 'occurrence';

        $returnValue = [
            'data' => []
        ];

        $this->browser->variable->setReturnValue('searchAll', $returnValue);

        $expected = [];

        // ACT
        $actual = $this->browser->buildFormColumns($fields, $entity);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test transforming list of variables into form config
     */
    public function testBuildVarsAsFormConfig(){
        // arrange
        $vars = ['ECOSYSTEM'];

        $returnValue = [
            'data' => [
                [
                    'abbrev' => 'ECOSYSTEM',
                    'type' => 'metadata',
                    'label' => 'ECOSYS',
                    'dataType' => 'character varying'
                ]
            ]
        ];

        $this->browser->variable->setReturnValue('searchAll', $returnValue);

        $expected = [
            [
                'variable_abbrev' => 'ECOSYSTEM',
                'variable_type' => 'metadata'
            ]
        ];

        // ACT
        $actual = $this->browser->buildVarsAsFormConfig($vars);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test transforming list of variables into form config with invalid variable
     */
    public function testBuildVarsAsFormConfigInvalidVariable(){
        // arrange
        $vars = ['INVALID_VARIABLE'];

        $returnValue = [
            'data' => []
        ];

        $this->browser->variable->setReturnValue('searchAll', $returnValue);

        $expected = [];

        // ACT
        $actual = $this->browser->buildVarsAsFormConfig($vars);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}