<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Test file for app\models\BackgroundJob
 */

namespace models;

class BackgroundJobTest extends \Codeception\Test\Unit
{
    /**
     * @var \BackgroundJob
     */
    protected $backgroundJob;
    protected $experimentMock;
    protected $occurrenceMock;
    protected $germplasmFileUploadMock;
    protected $entryListMock;
    
    protected function _before()
    {
        $this->backgroundJob = \Yii::$container->get('backgroundJob');
        // mocks
        $this->experimentMock = \Yii::$container->get('experimentMock');
        $this->occurrenceMock = \Yii::$container->get('occurrenceMock');
        $this->germplasmFileUploadMock = \Yii::$container->get('germplasmFileUploadMock');
        $this->entryListMock = \Yii::$container->get('entryListMock');
        $this->listsMock = \Yii::$container->get('listsModel');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * getGenericNotifications: Test 1
     * Entity is not "OCCURRENCE", status is IN_PROGRESS
     */
    public function testGetGenericNotificationsTest1()
    {
        // ARRANGE
        // input 
        $backgroundJobs = [
            [
                "jobStatus" => "IN_PROGRESS",
                "entityDbId" => 1234,
                "workerName" => "Worker1",
                "message" => "Background job is running.",
                "entity" => "EXPERIMENT",
                "modificationTimestamp" => "2021-10-16T04:25:37.759Z"
            ]
        ];

        // mocks
        $this->backgroundJob = $this->construct($this->backgroundJob,
            [
                "experiment" => $this->experimentMock,
                "occurrence" => $this->occurrenceMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock,
                "entryList" => $this->entryListMock,
                "lists" => $this->listsMock,
            ], 
            [
            'ago' => function ($timestamp) { 
                return "10 minutes ago"; 
            }
        ]);

        // expected
        $expected = "<li class='bg_notif-btn' data-transaction_id='1234' data-worker-name='Worker1'>\n" . 
        "                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle grey small' >hourglass_empty</span>Background job is running.</a><time class='media-meta'> <b>Experiment ID: 1234</b></time> <time class='media-meta'>10 minutes ago</time></li>";

        // ACT
        $actual = $this->backgroundJob->getGenericNotifications($backgroundJobs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getGenericNotifications: Test 2
     * Entity is not "OCCURRENCE", status is DONE
     */
    public function testGetGenericNotificationsTest2()
    {
        // ARRANGE
        // input 
        $backgroundJobs = [
            [
                "backgroundJobDbId" => 1,
                "jobStatus" => "DONE",
                "entityDbId" => 1234,
                "workerName" => "Worker1",
                "message" => "Background job is running.",
                "entity" => "EXPERIMENT",
                "modificationTimestamp" => "2021-10-16T04:25:37.759Z"
            ]
        ];

        // mocks
        $this->backgroundJob = $this->construct($this->backgroundJob,
            [
                "experiment" => $this->experimentMock,
                "occurrence" => $this->occurrenceMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock,
                "entryList" => $this->entryListMock,
                "lists" => $this->listsMock,
            ], 
            [
            'ago' => function ($timestamp) { 
                return "10 minutes ago"; 
            }
        ]);

        // expected
        $expected = "<li class='bg_notif-btn' data-transaction_id='1234' data-process-id='1' data-worker-name='Worker1'>\n" . 
        "                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle green small' >check</span> Background job is running.</a><time class='media-meta'> <b>Experiment ID: 1234</b></time> <time class='media-meta'>10 minutes ago</time></li>";

        // ACT
        $actual = $this->backgroundJob->getGenericNotifications($backgroundJobs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getGenericNotifications: Test 3
     * Entity is not "OCCURRENCE", status is FAILED
     */
    public function testGetGenericNotificationsTest3()
    {
        // ARRANGE
        // input 
        $backgroundJobs = [
            [
                "jobStatus" => "FAILED",
                "entityDbId" => 1234,
                "workerName" => "Worker1",
                "message" => "Background job is running.",
                "entity" => "EXPERIMENT",
                "modificationTimestamp" => "2021-10-16T04:25:37.759Z"
            ]
        ];

        // mocks
        $this->backgroundJob = $this->construct($this->backgroundJob,
            [
                "experiment" => $this->experimentMock,
                "occurrence" => $this->occurrenceMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock,
                "entryList" => $this->entryListMock,
                "lists" => $this->listsMock,
            ], 
            [
            'ago' => function ($timestamp) { 
                return "10 minutes ago"; 
            }
        ]);

        // expected
        $expected = "<li class='bg_notif-btn' data-transaction_id='1234' data-worker-name='Worker1'>\n" . 
        "                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle red small' >priority_high</span>Background job is running.</a><time class='media-meta'> <b>Experiment ID: 1234</b></time> <time class='media-meta'>10 minutes ago</time></li>";

        // ACT
        $actual = $this->backgroundJob->getGenericNotifications($backgroundJobs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getGenericNotifications: Test 4
     * Entity is "OCCURRENCE", status is IN_PROGRESS
     */
    public function testGetGenericNotificationsTest4()
    {
        // ARRANGE
        // input 
        $backgroundJobs = [
            [
                "jobStatus" => "IN_PROGRESS",
                "entityDbId" => 1234,
                "workerName" => "Worker1",
                "message" => "Background job is running.",
                "entity" => "OCCURRENCE",
                "modificationTimestamp" => "2021-10-16T04:25:37.759Z"
            ]
        ];

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrenceName" => "Sample Occurrence #001"
                ]
            ]
        ];
        $this->occurrenceMock->setReturnValue("searchAll", $returnValue);

        $this->backgroundJob = $this->construct($this->backgroundJob,
            [
                "experiment" => $this->experimentMock,
                "occurrence" => $this->occurrenceMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock,
                "entryList" => $this->entryListMock,
                "lists" => $this->listsMock,
            ], 
            [
            'ago' => function ($timestamp) { 
                return "10 minutes ago"; 
            }
        ]);

        // expected
        $expected = "<li class='bg_notif-btn' data-transaction_id='1234' data-worker-name='Worker1'>\n" . 
        "                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle grey small' >hourglass_empty</span>Background job is running.</a><time class='media-meta'> <b>Sample Occurrence #001</b></time> <time class='media-meta'>10 minutes ago</time></li>";

        // ACT
        $actual = $this->backgroundJob->getGenericNotifications($backgroundJobs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getGenericNotifications: Test 5
     * Entity is "OCCURRENCE", status is DONE
     */
    public function testGetGenericNotificationsTest5()
    {
        // ARRANGE
        // input 
        $backgroundJobs = [
            [
                "backgroundJobDbId" => 1,
                "jobStatus" => "DONE",
                "entityDbId" => 1234,
                "workerName" => "Worker1",
                "message" => "Background job is running.",
                "entity" => "OCCURRENCE",
                "modificationTimestamp" => "2021-10-16T04:25:37.759Z"
            ]
        ];

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrenceName" => "Sample Occurrence #001"
                ]
            ]
        ];
        $this->occurrenceMock->setReturnValue("searchAll", $returnValue);

        $this->backgroundJob = $this->construct($this->backgroundJob,
            [
                "experiment" => $this->experimentMock,
                "occurrence" => $this->occurrenceMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock,
                "entryList" => $this->entryListMock,
                "lists" => $this->listsMock,
            ], 
            [
            'ago' => function ($timestamp) { 
                return "10 minutes ago"; 
            }
        ]);

        // expected
        $expected = "<li class='bg_notif-btn' data-transaction_id='1234' data-process-id='1' data-worker-name='Worker1'>\n" . 
        "                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle green small' >check</span> Background job is running.</a><time class='media-meta'> <b>Sample Occurrence #001</b></time> <time class='media-meta'>10 minutes ago</time></li>";

        // ACT
        $actual = $this->backgroundJob->getGenericNotifications($backgroundJobs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getGenericNotifications: Test 6
     * Entity is "OCCURRENCE", status is FAILED
     */
    public function testGetGenericNotificationsTest6()
    {
        // ARRANGE
        // input 
        $backgroundJobs = [
            [
                "jobStatus" => "FAILED",
                "entityDbId" => 1234,
                "workerName" => "Worker1",
                "message" => "Background job is running.",
                "entity" => "OCCURRENCE",
                "modificationTimestamp" => "2021-10-16T04:25:37.759Z"
            ]
        ];

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrenceName" => "Sample Occurrence #001"
                ]
            ]
        ];
        $this->occurrenceMock->setReturnValue("searchAll", $returnValue);

        $this->backgroundJob = $this->construct($this->backgroundJob,
            [
                "experiment" => $this->experimentMock,
                "occurrence" => $this->occurrenceMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock,
                "entryList" => $this->entryListMock,
                "lists" => $this->listsMock,
            ], 
            [
            'ago' => function ($timestamp) { 
                return "10 minutes ago"; 
            }
        ]);

        // expected
        $expected = "<li class='bg_notif-btn' data-transaction_id='1234' data-worker-name='Worker1'>\n" . 
        "                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle red small' >priority_high</span>Background job is running.</a><time class='media-meta'> <b>Sample Occurrence #001</b></time> <time class='media-meta'>10 minutes ago</time></li>";

        // ACT
        $actual = $this->backgroundJob->getGenericNotifications($backgroundJobs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getGenericNotifications: Test 7
     * Entity is "OCCURRENCE", status not known/not supported
     */
    public function testGetGenericNotificationsWhenStatusIsUnknown()
    {
        // ARRANGE
        // input 
        $backgroundJobs = [
            [
                "jobStatus" => "UNSUPPORTED_STATUS",
                "entityDbId" => 1234,
                "workerName" => "Worker1",
                "message" => "Background job is running.",
                "entity" => "OCCURRENCE",
                "modificationTimestamp" => "2021-10-16T04:25:37.759Z"
            ]
        ];

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrenceName" => "Sample Occurrence #001"
                ]
            ]
        ];
        $this->occurrenceMock->setReturnValue("searchAll", $returnValue);

        $this->backgroundJob = $this->construct($this->backgroundJob,
            [
                "experiment" => $this->experimentMock,
                "occurrence" => $this->occurrenceMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock,
                "entryList" => $this->entryListMock,
                "lists" => $this->listsMock,
            ], 
            [
            'ago' => function ($timestamp) { 
                return "10 minutes ago"; 
            }
        ]);

        // expected
        $expected = "<li class='bg_notif-btn' data-transaction_id='1234' data-worker-name='Worker1'>\n" . 
        "                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle grey small' >near_me</span>Background job is running.</a><time class='media-meta'> <b>Sample Occurrence #001</b></time> <time class='media-meta'>10 minutes ago</time></li>";

        // ACT
        $actual = $this->backgroundJob->getGenericNotifications($backgroundJobs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * pluralize: Test 1
     * count = 0
     */
    public function testPluralizeTest1()
    {
        // ARRANGE
        // input
        $count = 0;
        $text = "hour";

        // expected
        $expected = "0 hours";

        // ACT
        $actual = $this->backgroundJob->pluralize($count, $text);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * pluralize: Test 2
     * count = 1
     */
    public function testPluralizeTest2()
    {
        // ARRANGE
        // input
        $count = 1;
        $text = "minute";

        // expected
        $expected = "1 minute";

        // ACT
        $actual = $this->backgroundJob->pluralize($count, $text);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * pluralize: Test 3
     * count > 1
     */
    public function testPluralizeTest3()
    {
        // ARRANGE
        // input
        $count = 1024;
        $text = "second";

        // expected
        $expected = "1024 seconds";

        // ACT
        $actual = $this->backgroundJob->pluralize($count, $text);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}