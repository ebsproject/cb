<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\ServerException;
use Yii;

class SeedTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $seed;
    
    protected function _before()
    {
        $this->seed = Yii::$container->get('seed');
    }

    protected function _after()
    {
    }

    // tests
    public function testApiEndPoint()
    {
        // ARRANGE
        $expected = 'seeds';

        // ACT
        $actual = $this->seed->apiEndPoint();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testApiEndPointPackagesSearch()
    {
        // ARRANGE
        $expected = 'seed-packages-search';

        // ACT
        $actual = $this->seed->apiEndPointPackagesSearch();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an array list of package data is returned on successful call to API
     */
    public function testSearchPackageRecordsWithoutRequestBodySuccess()
    {
        // ARRANGE
        // Mock Yii app component
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData=null, $filter='', $retrieveAll=false) {
                    return [
                        'status' => 200,
                        'success' => true,
                        'data' => [
                            'package1' => 1,
                            'package2' => 2
                        ],
                        'totalCount' => 2,
                        'totalPages' => 1
                    ];
                }
            ]
        );
        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            'package1' => 1,
            'package2' => 2
        ];

        // ACT
        $actual = $this->seed->searchPackageRecords();

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    /**
     * Test that an empty list is returned when API call fails with 404
     */
    public function testSearchPackageRecordsWithoutRequestBodyFail4xx()
    {
        // ARRANGE
        // Mock Yii app component
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData=null, $filter='', $retrieveAll=false) {
                    return [
                        'status' => 404,
                        'success' => false,
                    ];
                }
            ]
        );
        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [];

        // ACT
        $actual = $this->seed->searchPackageRecords();

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    /**
     * Test that an empty list is returned when API call fails with 5xx
     */
    public function testSearchPackageRecordsWithoutRequestBodyFail5xx()
    {
        // ARRANGE
        // Mock Yii app component
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData=null, $filter='', $retrieveAll=false) {
                    throw new ServerException(
                        'Error 500',
                        new Request('POST', 'test'),
                        new Response(500)
                    );
                }
            ]
        );
        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [];

        // ACT
        $actual = $this->seed->searchPackageRecords();

        // ASSERT
        $this->assertEquals($expected,$actual);
    }
}