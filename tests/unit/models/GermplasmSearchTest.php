<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;
use yii\data\Sort;
use app\dataproviders\ArrayDataProvider;

class GermplasmSearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $germplasmSearch;

    protected function _before()
    {
        $this->germplasmSearch = Yii::$container->get('germplasmSearch');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * testSearch: Test 1
     * Search model for germplasm
     */
    public function testSearchTest1()
    {
        // ARRANGE
        $params = 1;
        $queryParams = null;

        $data = [
            [
                'germplasmDbId' => 1,
                'designation' => 'Germplasm',
                'parentage' => 'germ/plasm',
                'generation' => 'UNKNOWN',
                'germplasmState' => 'fixed',
                'germplasmNameType' => 'line_name',
                'germplasmNormalizedName' => 'GERMPLASM',
                'germplasmCode' => 'GE000000000001',
                'germplasmType' => 'fixed_line',
                'otherNames' => 'Germplasm',
                'germplasmDocument' => null,
                'taxonomyName' => 'Taxonomy Name',
                'cropDbId' => 1,
                'cropCode' => 'RICE',
                'cropName' => 'Rice',
                'cropDescription' => 'Rice crop',
                'creatorDbId' => 1,
                'creator' => 'EBS, Admin',
                'modifierDbId' => 2,
                'modifier' => 'Modifier',
                'creationTimestamp' => '2021-06-09T09:33:15.841Z',
                'modificationTimestamp' => '2021-10-17T15:24:41.125Z',
                'orderNumber' => 0,
                'entriesCount' => 0,
                'seedsCount' => 0
            ]
        ];

        $dataProviderId = 'germplasm-dp';

        $sort = new Sort();
        $sort->attributes = ['germplasmDbId', 'creationTimestamp', 'creator', 'designation',
            'generation', 'modificationTimestamp', 'modifier', 'nameType', 'otherNames',
            'parentage', 'germplasmNameType', 'germplasmState', 'germplasmNormalizedName',
            'germplasmCode', 'taxonomyName', 'cropCode', 'orderNumber'=>[
                "asc" => ["orderNumber"=>SORT_ASC],
                'desc' => ["orderNumber"=>SORT_DESC]
            ]
        ];
        $sort->defaultOrder = [
            'orderNumber'=>SORT_ASC
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'orderNumber',
            'restified' => true,
            'totalCount' => 1,
            'id' => $dataProviderId
        ]);
        $dataProvider->setSort($sort);

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    if(strstr($path,'germplasm-search')) {
                        $result = [
                            'status' => 200,
                            'body' => [
                                'metadata' => [
                                    'pagination' => [
                                        'pageSize' => 100,
                                        'totalCount' => 1,
                                        'currentPage' => 1,
                                        'totalPages' => 1
                                    ]
                                ],
                                'result' => [
                                    'data' => [
                                        [
                                            'germplasmDbId' => 1,
                                            'designation' => 'Germplasm',
                                            'parentage' => 'germ/plasm',
                                            'generation' => 'UNKNOWN',
                                            'germplasmState' => 'fixed',
                                            'germplasmNameType' => 'line_name',
                                            'germplasmNormalizedName' => 'GERMPLASM',
                                            'germplasmCode' => 'GE000000000001',
                                            'germplasmType' => 'fixed_line',
                                            'otherNames' => 'Germplasm',
                                            'germplasmDocument' => null,
                                            'taxonomyName' => 'Taxonomy Name',
                                            'cropDbId' => 1,
                                            'cropCode' => 'RICE',
                                            'cropName' => 'Rice',
                                            'cropDescription' => 'Rice crop',
                                            'creatorDbId' => 1,
                                            'creator' => 'EBS, Admin',
                                            'modifierDbId' => 2,
                                            'modifier' => 'Modifier',
                                            'creationTimestamp' => '2021-06-09T09:33:15.841Z',
                                            'modificationTimestamp' => '2021-10-17T15:24:41.125Z'
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    } else {
                        $result = [
                            'status' => 200,
                            'body' => [
                                'metadata' => [
                                    'pagination' => [
                                        'pageSize' => 100,
                                        'totalCount' => 1,
                                        'currentPage' => 1,
                                        'totalPages' => 1
                                    ]
                                ],
                                'result' => [
                                    'data' => [
                                        [
                                            'configValue' => 1
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    }
                    return $result;
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $this->germplasmSearch->germplasmModel->setReturnValue("getGermplasmEntriesCount", [['entriesCount' => 0]]);
        $this->germplasmSearch->germplasmModel->setReturnValue("getGermplasmSeedsCount", [['seedsCount' => 0]]);

        $expected = $dataProvider;

        // ACTUAL
        $actual = $this->germplasmSearch->search($params, $queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testSearch: Test 2
     * Search model for germplasm with $params['GermplasmSearch'] and $queryParams set
     */
    public function testSearchTest2()
    {
        // ARRANGE
        $params = [
            'program' => 'IRSEA',
            '_pjax' => '#germplasm-grid-pjax',
            'GermplasmSearch' => [
                'designation' => '%a%',
                'generation' => ''
            ]
        ];
        $queryParams = [
            'names' => [
                'equals' => 'germlpasm1|germplasm2'
            ]
        ];

        $data = [
            [
                'germplasmDbId' => 1,
                'designation' => 'Germplasm1',
                'orderNumber' => 0,
                'entriesCount' => 0,
                'seedsCount' => 0
            ],
            [
                'germplasmDbId' => 2,
                'designation' => 'Germplasm2',
                'orderNumber' => 1,
                'entriesCount' => 0,
                'seedsCount' => 0
            ]
        ];

        $dataProviderId = 'germplasm-dp';

        $sort = new Sort();
        $sort->attributes = ['germplasmDbId', 'creationTimestamp', 'creator', 'designation',
            'generation', 'modificationTimestamp', 'modifier', 'nameType', 'otherNames',
            'parentage', 'germplasmNameType', 'germplasmState', 'germplasmNormalizedName',
            'germplasmCode', 'taxonomyName', 'cropCode', 'orderNumber'=>[
                "asc" => ["orderNumber"=>SORT_ASC],
                'desc' => ["orderNumber"=>SORT_DESC]
            ]
        ];
        $sort->defaultOrder = [
            'orderNumber'=>SORT_ASC
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'orderNumber',
            'restified' => true,
            'totalCount' => 2,
            'id' => $dataProviderId
        ]);
        $dataProvider->setSort($sort);

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    if(strstr($path,'germplasm-search')) {
                        $result = [
                            'status' => 200,
                            'body' => [
                                'metadata' => [
                                    'pagination' => [
                                        'pageSize' => 100,
                                        'totalCount' => 2,
                                        'currentPage' => 1,
                                        'totalPages' => 1
                                    ]
                                ],
                                'result' => [
                                    'data' => [
                                        [
                                            'germplasmDbId' => 1,
                                            'designation' => 'Germplasm1',
                                            'orderNumber' => 0
                                        ],
                                        [
                                            'germplasmDbId' => 2,
                                            'designation' => 'Germplasm2',
                                            'orderNumber' => 1
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    } else {
                        $result = [
                            'status' => 200,
                            'body' => [
                                'metadata' => [
                                    'pagination' => [
                                        'pageSize' => 100,
                                        'totalCount' => 1,
                                        'currentPage' => 1,
                                        'totalPages' => 1
                                    ]
                                ],
                                'result' => [
                                    'data' => [
                                        [
                                            'configValue' => 1
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    }
                    return $result;
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $this->germplasmSearch->germplasmModel->setReturnValue("getGermplasmEntriesCount", [['entriesCount' => 0]]);
        $this->germplasmSearch->germplasmModel->setReturnValue("getGermplasmSeedsCount", [['seedsCount' => 0]]);

        $expected = $dataProvider;

        // ACTUAL
        $actual = $this->germplasmSearch->search($params, $queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetGermplasmById: Test 1
     * Retrieves germplasm information
     */
    public function testGetGermplasmByIdTest1()
    {
        // ARRANGE
        $id = 1;

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        'germplasmDbId' => 1,
                                        'designation' => 'Germplasm',
                                        'parentage' => 'germ/plasm',
                                        'generation' => 'UNKNOWN',
                                        'germplasmState' => 'fixed',
                                        'germplasmNameType' => 'line_name',
                                        'germplasmNormalizedName' => 'GERMPLASM',
                                        'germplasmCode' => 'GE000000000001',
                                        'germplasmType' => 'fixed_line',
                                        'otherNames' => 'Germplasm',
                                        'germplasmDocument' => null,
                                        'creatorDbId' => 1,
                                        'creator' => 'EBS, Admin',
                                        'modifierDbId' => 2,
                                        'modifier' => 'Modifier',
                                        'creationTimestamp' => '2021-06-09T09:33:15.841Z',
                                        'modificationTimestamp' => '2021-10-17T15:24:41.125Z'
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            'germplasmDbId' => 1,
            'designation' => 'Germplasm',
            'parentage' => 'germ/plasm',
            'generation' => 'UNKNOWN',
            'germplasmState' => 'fixed',
            'germplasmNameType' => 'line_name',
            'germplasmNormalizedName' => 'GERMPLASM',
            'germplasmCode' => 'GE000000000001',
            'germplasmType' => 'fixed_line',
            'otherNames' => 'Germplasm',
            'germplasmDocument' => null,
            'creatorDbId' => 1,
            'creator' => 'EBS, Admin',
            'modifierDbId' => 2,
            'modifier' => 'Modifier',
            'creationTimestamp' => '2021-06-09T09:33:15.841Z',
            'modificationTimestamp' => '2021-10-17T15:24:41.125Z'
        ];

        // ACTUAL
        $actual = $this->germplasmSearch->getGermplasmById($id);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetColumnNameAndDatatypes: Test 1
     * Retrieve column names and datatypes
     */
    public function testGetColumnNameAndDatatypesTest1()
    {
        // ARRANGE
        $expected = [
            'designation' => 'String',
            'otherNames' => 'String',
            'parentage' => 'String',
            'generation' => 'String',
            'creator' => 'String',
            'creationTimestamp' => 'Timestamp',
            'modifier' => 'String',
            'modificationTimestamp' => 'Timestamp',
            'germplasmNameType' => 'String',
            'germplasmState' => 'String',
            'germplasmNormalizedName' => 'String',
            'germplasmCode' => 'String',
            'taxonomyName' => 'String',
            'cropCode' => 'String'
        ];

        // ACTUAL
        $actual = $this->germplasmSearch->getColumnNameAndDatatypes();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}