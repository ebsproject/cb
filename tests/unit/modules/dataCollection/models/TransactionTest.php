<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Test file for app\modules\dataCollection\models\Transaction
 */

namespace modules\dataCollection\models;
use Yii;

class TransactionTest extends \Codeception\Test\Unit
{
    /**
     * @var \Transaction
     */
    protected $transaction;
    
    protected function _before()
    {
        $this->transaction = \Yii::$container->get('transaction');   
    }

    protected function _after()
    {
    }

    /**
     * Test handling a failed commitPlotData() Test #1
     */
    public function testCommitPlotDataWithError()
    {
        // ARRANGE
        $transactionDbId = "123";
        
        // Mocks
        $api = $this->make('app\components\Api', [
            "getParsedResponse" => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    "success" => false
                ];
            }
        ]);

        Yii::$app->set('api', $api);

        // Expected
        $expected =  [
            "success" => false,
            "error" => "There is an error while committing plot data."
        ];

        // ACT
        $actual = $this->transaction->commitPlotData($transactionDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test successful execution of commitPlotData() Test #2
     */
    public function testCommitPlotDataWithoutError()
    {
        // ARRANGE
        $transactionDbId = "123";
        
        // Mocks
        $api = $this->make('app\components\Api', [
            "getParsedResponse" => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    "success" => true
                ];
            }
        ]);

        Yii::$app->set('api', $api);

        // Expected
        $expected =  [
            "success" => true
        ];

        // ACT
        $actual = $this->transaction->commitPlotData($transactionDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test retrieving empty variable from getVariable() Test #1
     */
    public function testGetVariableWithEmptyData()
    {
        // Mocks
        $variableMock = $this->make('app\models\Variable', [
            'searchAll' => function($param, $options, $flag) {
                return ['data' => []];
            }
        ]);

        Yii::$app->set('variable', $variableMock);

        // Expected
        $expected = [];

        // ACT
        $actual = $this->transaction->getVariable('TEST');

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test retrieving a variable with data from getVariable() Test #2
     */
    public function testGetVariableWithData()
    {
        // Mocks
        $variableMock = $this->make('app\models\Variable', [
            'searchAll' => function($param, $options, $flag) {
                return ['data' => [['key' => 'value']]];
            }
        ]);

        $this->transaction->variable = $variableMock;

        // ACT
        $actual = $this->transaction->getVariable('TEST');

        // Expected
        $expected = ['key' => 'value'];

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test retrieval of dataset variables with multiple data 
     * units and unique variables in getDatasetVariables() Test #1
     */
    public function testGetDatasetVariablesWithMultipleDataUnits()
    {
        // Sort variables by variableDbId
        $sortByVariableDbId = function($array) {
            foreach ($array as &$variables) {
                usort($variables, function($a, $b) {
                    return $a['variableDbId'] <=> $b['variableDbId'];
                });
            }
            return $array;
        };

        $datasetVariables = [
            ['dataUnit' => 'unit1', 'variableDbId' => 2],
            ['dataUnit' => 'unit1', 'variableDbId' => 1],
            ['dataUnit' => 'unit2', 'variableDbId' => 4],
            ['dataUnit' => 'unit2', 'variableDbId' => 3],
        ];

        // Expected 
        $expected = [
            'unit1' => [
                ['dataUnit' => 'unit1', 'variableDbId' => 1],
                ['dataUnit' => 'unit1', 'variableDbId' => 2]
            ],
            'unit2' => [
                ['dataUnit' => 'unit2', 'variableDbId' => 3],
                ['dataUnit' => 'unit2', 'variableDbId' => 4]
            ]
        ];

        // ACT
        $actual = $this->transaction->getDatasetVariables($datasetVariables);

        // ASSERT
        $this->assertEquals($sortByVariableDbId($expected), $sortByVariableDbId($actual));
    }

    /**
     * Test retrieval of dataset variables with a single data 
     * unit in getDatasetVariables() Test #2
     */
    public function testGetDatasetVariablesWithSingleDataUnit()
    {
        // Sort variables by variableDbId
        $sortByVariableDbId = function($array) {
            foreach ($array as &$variables) {
                usort($variables, function($a, $b) {
                    return $a['variableDbId'] <=> $b['variableDbId'];
                });
            }
            return $array;
        };

        // Expected
        $datasetVariables = [
            ['dataUnit' => 'unit1', 'variableDbId' => 2],
            ['dataUnit' => 'unit1', 'variableDbId' => 1]
        ];

        $expected = [
            'unit1' => [
                ['dataUnit' => 'unit1', 'variableDbId' => 1],
                ['dataUnit' => 'unit1', 'variableDbId' => 2]
            ]
        ];

        // ACT
        $actual = $this->transaction->getDatasetVariables($datasetVariables);

        // ASSERT
        $this->assertEquals($sortByVariableDbId($expected), $sortByVariableDbId($actual));
    }

    /**
     * Test handling of an empty datasetVariables array 
     * in getDatasetVariables() Test #3
     */
    public function testGetDatasetVariablesWithEmptyDatasetVariables()
    {
        // ARRANGE
        $datasetVariables = [];

        // Expected
        $expected = [];

        // ACT
        $actual = $this->transaction->getDatasetVariables($datasetVariables);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test retrieval of occurrence data with 
     * matching records in getOccurrenceData() Test #1
     */
    public function testGetOccurrenceDataWithMatchingRecords()
    {
        // ARRANGE
        $occurrenceDbId = 1;
        $plotData = [];
        $formulaDbIdList = [101, 102];

        // Mocks
        // Occurrence Data
        $occurrenceDataMock = $this->make('app\models\OccurrenceData', [
            'searchAll' => function($param, $options, $flag) {
                return [
                    'data' => [
                        ['variableDbId' => 1, 'dataValue' => 'value1', 'variableLabel' => 'label1'],
                        ['variableDbId' => 2, 'dataValue' => 'value2', 'variableLabel' => 'label2']
                    ]
                ];
            }
        ]);

        // Formula Parameters Data
        $formulaParametersMock = $this->make('app\models\FormulaParameters', [
            'searchAll' => function($param, $options, $flag) {
                return [
                    'data' => [
                        ['paramVariableDbId' => 1, 'resultVariableDbId' => 2, 'formulaId' => 101],
                        ['paramVariableDbId' => 2, 'resultVariableDbId' => 3, 'formulaId' => 102]
                    ]
                ];
            }
        ]);

        $this->transaction->occurrenceData = $occurrenceDataMock;
        $this->transaction->formulaParameters = $formulaParametersMock;

        // Expected
        $expected = [
            1 => ['value' => 'value1', 'label' => 'label1'],
            2 => ['value' => 'value2', 'label' => 'label2']
        ];

        // ACT
        $actual = $this->transaction->getOccurrenceData($occurrenceDbId, $plotData, $formulaDbIdList);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test retrieval of occurrence data with an empty 
     * formulaDbIdList in getOccurrenceData() Test #2
     */
    public function testGetOccurrenceDataWithEmptyFormulaDbIdList()
    {
        // ARRANGE
        $occurrenceDbId = 1;
        $plotData = [];
        $formulaDbIdList = [];

        // Mocks
        // Occurrence Data
        $occurrenceDataMock = $this->make('app\models\OccurrenceData', [
            'searchAll' => function($param, $options, $flag) {
                return ['data' => []];
            }
        ]);

        // Formula Parameters Data
        $formulaParametersMock = $this->make('app\models\FormulaParameters', [
            'searchAll' => function($param, $options, $flag) {
                return ['data' => []];
            }
        ]);

        $this->transaction->occurrenceData = $occurrenceDataMock;
        $this->transaction->formulaParameters = $formulaParametersMock;

        // Expected
        $expected = [];

        // ACT
        $actual = $this->transaction->getOccurrenceData($occurrenceDbId, $plotData, $formulaDbIdList);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test retrieval of occurrence data with no occurrence 
     * data available in getOccurrenceData() Test #3
     */
    public function testGetOccurrenceDataWithNoOccurrence()
    {
        // ARRANGE
        $occurrenceDbId = 1;
        $plotData = [];
        $formulaDbIdList = [101, 102];

        // Mocks
        // Occurrence Data
        $occurrenceDataMock = $this->make('app\models\OccurrenceData', [
            'searchAll' => function($param, $options, $flag) {
                return ['data' => []];
            }
        ]);

        // Formula Parameters Data
        $formulaParametersMock = $this->make('app\models\FormulaParameters', [
            'searchAll' => function($param, $options, $flag) {
                return [
                    'data' => []
                ];
            }
        ]);

        $this->transaction->occurrenceData = $occurrenceDataMock;
        $this->transaction->formulaParameters = $formulaParametersMock;

        // Expected
        $expected = [];

        // ACT
        $actual = $this->transaction->getOccurrenceData($occurrenceDbId, $plotData, $formulaDbIdList);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}