<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\occurrence\models;

class LocationPlotTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
        $this->locationPlotModel = \Yii::$container->get('locationPlotModel');
    }

    protected function _after()
    {
    }

    // tests getLocationPlotRecords()
    public function xtestGetLocationPlotRecords()
    {
        // ARRANGE
        $expected = [
            [ 'plotDbId' => 1 ],
            [ 'plotDbId' => 2 ],
            [ 'plotDbId' => 3 ]
        ];

        $locationId= 1;
        $entity = 'plots';
        $attribute = 'plotDbId';
        $listType = 'plot';
        $selectedItemsStr = '';
   
        \Yii::$app->session->set("location-$entity-param-sort$locationId", "");
        \Yii::$app->session->set("location-$entity-filters$locationId", []);

        // Mock Yii app components
        $api = $this->make('app\components\Api', [
            'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                return [
                    'success' => true,
                    'message' => 'Request has been successfully completed.',
                    'data' => [
                        [ 'plotDbId' => 1 ],
                        [ 'plotDbId' => 2 ],
                        [ 'plotDbId' => 3 ]
                    ],
                    'totalCount' => 3,
                    'status' => 200,
                    'totalPages' => 1,
                ];
            }
        ]);

        $userprogram = $this->make('app\models\Browser', [
            'buildFieldsParam' => function() {
                return '';
            }
        ]);

        // Inject mocks into Yii::$app
        \Yii::$app->set('api', $api);
        \Yii::$app->set('userprogram', $userprogram);

        // ACT
        $actual = $this->locationPlotModel->getLocationPlotRecords($locationId,$entity,$attribute,$listType,$selectedItemsStr);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    // tests getLocationPlotRecords()
    public function xtestGetLocationPlotRecordsWithFilters()
    {
        // ARRANGE
        $expected = [
            [ 'plotDbId' => 1 ],
            [ 'plotDbId' => 2 ]
        ];

        $locationId= 1;
        $entity = 'plots';
        $attribute = 'plotDbId';
        $listType = 'plot';
        $selectedItemsStr = '';

        \Yii::$app->session->set("location-$entity-param-sort$locationId", "");
        \Yii::$app->session->set("location-$entity-filters$locationId", [["entryCode" => "1%"],["plotCode" => "1"]]);

        // Mock Yii app components
        $api = $this->make('app\components\Api', [
            'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                return [
                    'success' => true,
                    'message' => 'Request has been successfully completed.',
                    'data' => [
                        [ 'plotDbId' => 1 ],
                        [ 'plotDbId' => 2 ]
                    ],
                    'totalCount' => 3,
                    'status' => 200,
                    'totalPages' => 1,
                ];
            }
        ]);

        $userprogram = $this->make('app\models\Browser', [
            'buildFieldsParam' => function() {
                return 'pi.entry_code AS entryCode|plot_code AS plotCode';
            }
        ]);

        // Inject mocks into Yii::$app
        \Yii::$app->set('api', $api);
        \Yii::$app->set('userprogram', $userprogram);

        // ACT
        $actual = $this->locationPlotModel->getLocationPlotRecords($locationId,$entity,$attribute,$listType,$selectedItemsStr);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    // tests getLocationPlotRecords()
    public function xtestGetLocationPlotRecordsWithFailedRetrieval()
    {
        // ARRANGE
        $expected = [];

        $locationId= 1;
        $entity = 'plots';
        $attribute = 'plotDbId';
        $listType = 'plot';
        $selectedItemsStr = '';

        \Yii::$app->session->set("location-$entity-param-sort$locationId", "");
        \Yii::$app->session->set("location-$entity-filters$locationId", [["entryCode" => "1%"],["plotCode" => "1"]]);

        // Mock Yii app components
        $api = $this->make('app\components\Api', [
            'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                return null;
            }
        ]);

        $userprogram = $this->make('app\models\Browser', [
            'buildFieldsParam' => function() {
                return 'pi.entry_code AS entryCode|plot_code AS plotCode';
            }
        ]);

        // Inject mocks into Yii::$app
        \Yii::$app->set('api', $api);
        \Yii::$app->set('userprogram', $userprogram);

        // ACT
        $actual = $this->locationPlotModel->getLocationPlotRecords($locationId,$entity,$attribute,$listType,$selectedItemsStr);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    // tests getLocationPlotRecords()
    public function xtestGetLocationPlotRecordsWithEmptyRetrieval()
    {
        // ARRANGE
        $expected = [];

        $locationId= 1;
        $entity = 'plots';
        $attribute = 'plotDbId';
        $listType = 'plot';
        $selectedItemsStr = '';

        \Yii::$app->session->set("location-$entity-param-sort$locationId", "");
        \Yii::$app->session->set("location-$entity-filters$locationId", [["entryCode" => "1%"],["plotCode" => "1"]]);

        // Mock Yii app components
        $api = $this->make('app\components\Api', [
            'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                return [
                    'success' => false,
                    'message' => '',
                    'data' => [],
                    'totalCount' => 0,
                    'status' => 400,
                    'totalPages' => 0,
                ];
            }
        ]);

        $userprogram = $this->make('app\models\Browser', [
            'buildFieldsParam' => function() {
                return 'pi.entry_code AS entryCode|plot_code AS plotCode';
            }
        ]);

        // Inject mocks into Yii::$app
        \Yii::$app->set('api', $api);
        \Yii::$app->set('userprogram', $userprogram);

        // ACT
        $actual = $this->locationPlotModel->getLocationPlotRecords($locationId,$entity,$attribute,$listType,$selectedItemsStr);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }
}