<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\occurrence\models;

use Yii;

class ManageTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected object $manageModel;

    protected function _before()
    {
        $this->manageModel = Yii::$container->get('manageModel');
    }

    protected function _after()
    {
    }

    public function testGetDataByExperimentId ()
    {
        // ARRANGE
        $id = 123;
        $variableAbbrevs = ['VARIABLE_0', 'VARIABLE_1', 'VARIABLE_2'];

        //Mock Yii app components
        $api = $this->make('app\components\Api', [
            'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                return [
                    'data' => [[
                        'data' => ['dataValue'],
                    ]]
                ];
            }
        ]);

        // Inject mocks into Yii::$app
        Yii::$app->set('api', $api);

        $expected = ['dataValue'];

        // ACT
        $actual = $this->manageModel->getDataByExperimentId($id, $variableAbbrevs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetDataByExperimentIdNull ()
    {
        // ARRANGE
        $id = 123;
        $variableAbbrevs = ['NULL_VARIABLE_0', 'NULL_VARIABLE_1', 'NULL_VARIABLE_2'];

        //Mock Yii app components
        $api = $this->make('app\components\Api', [
            'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                return null;
            }
        ]);

        // Inject mocks into Yii::$app
        Yii::$app->set('api', $api);

        $expected = null;

        // ACT
        $actual = $this->manageModel->getDataByExperimentId($id, $variableAbbrevs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetSeedsHarvestedStatusOccurrence()
    {
        // ARRANGE
        $locationDbId = null;
        $occurrenceDbId = 10;

        // Mock Yii app components
        $api = $this->make('app\components\Api', [
            'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                return [
                    'success' => true,
                    'message' => 'Request has been successfully completed.',
                    'data' => [
                        [
                            'totalPlotCount' => '40000',
                            'totalEntryCount' => '20000',
                            'totalRepCount' => '2',
                            'totalCrossCount' => '10000',
                            'totalPackageCount' => '0',
                            'harvestedPlots' => '0',
                            'totalSeedsFromPlots' => '4000',
                            'totalPackagesFromPlots' => '0',
                            'harvestedCrosses' => '0',
                            'totalSeedsFromCrosses' => '1000',
                            'totalPackagesFromCrosses' => '0',
                        ],
                    ],
                    'totalCount' => 1,
                    'status' => 200,
                    'totalPages' => 1,
                ];
            }
        ]);
        $userprogram = $this->make('app\components\UserProgram', [
            'get' => function() {
                return 'IRSEA';
            }
        ]);

        // Inject mocks into Yii::$app
        Yii::$app->set('api', $api);
        Yii::$app->set('userprogram', $userprogram);

        $expected = [
            'label' => 'Seeds harvested',
            'allCount' => 50000,
            'doneCount' => 5000,
            'status' => 10.0,
            'url' => '/index-test.php?r=occurrence%2Fview%2Fplot&program=IRSEA&id=10',
            'urlTitle' => 'View plot data'
        ];

        // ACT
        $actual = $this->manageModel->getSeedsHarvestedStatus($locationDbId, $occurrenceDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetSeedsHarvestedStatusLocation()
    {
        // ARRANGE
        $locationDbId = 20;
        $occurrenceDbId = null;

        // Mock Yii app components
        $api = $this->make('app\components\Api', [
            'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                return [
                    'success' => true,
                    'message' => 'Request has been successfully completed.',
                    'data' => [
                        [
                            'totalPlotCount' => '40000',
                            'totalEntryCount' => '20000',
                            'totalRepCount' => '2',
                            'totalCrossCount' => '10000',
                            'totalPackageCount' => '0',
                            'harvestedPlots' => '0',
                            'totalSeedsFromPlots' => '4000',
                            'totalPackagesFromPlots' => '0',
                            'harvestedCrosses' => '0',
                            'totalSeedsFromCrosses' => '1000',
                            'totalPackagesFromCrosses' => '0',
                        ],
                    ],
                    'totalCount' => 1,
                    'status' => 200,
                    'totalPages' => 1,
                ];
            }
        ]);
        $userprogram = $this->make('app\components\UserProgram', [
            'get' => function() {
                return 'IRSEA';
            }
        ]);

        // Inject mocks into Yii::$app
        Yii::$app->set('api', $api);
        Yii::$app->set('userprogram', $userprogram);

        $expected = [
            'label' => 'Seeds harvested',
            'allCount' => 50000,
            'doneCount' => 5000,
            'status' => 10.0,
            'url' => '/index-test.php?r=location%2Fview%2Fplot&program=IRSEA&id=20',
            'urlTitle' => 'View plot data'
        ];

        // ACT
        $actual = $this->manageModel->getSeedsHarvestedStatus($locationDbId, $occurrenceDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test when call to API fails when retrieving harvest data
     */
    public function testGetSeedsHarvestedStatusAPIError(){
        // ARRANGE
        $locationDbId = null;
        $occurrenceDbId = 10;

        // Mock Yii app components
        $api = $this->make('app\components\Api', [
            'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                return [
                    'success' => false,
                    'message' => 'Some error occurred while querying the API',
                ];
            }
        ]);
        $userprogram = $this->make('app\components\UserProgram', [
            'get' => function() {
                return 'IRSEA';
            }
        ]);

        // Inject mocks into Yii::$app
        Yii::$app->set('api', $api);
        Yii::$app->set('userprogram', $userprogram);

        $expected = [
            'label' => 'Seeds harvested',
            'allCount' => 0,
            'doneCount' => 0,
            'status' => 0.0,
            'url' => '/index-test.php?r=occurrence%2Fview%2Fplot&program=IRSEA&id=10',
            'urlTitle' => 'View plot data'
        ];

        // ACT
        $actual = $this->manageModel->getSeedsHarvestedStatus($locationDbId, $occurrenceDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}