<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\occurrence\models;

use app\modules\occurrence\models\FileExport;

class FileExportTest extends \Codeception\Test\Unit
{
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Verify that constructing with an invalid file type throws an error
     */
    public function testInvalidFileType()
    {
        $this->expectException(\TypeError::class);
        $fileType = 'INVALID_FILE';
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);
    }

    /**
     * Verify that constructing with an invalid file format throws an error
     */
    public function testInvalidFileFormat()
    {
        $this->expectException(\TypeError::class);
        $fileType = FileExport::CROSS_LIST;
        $fileFormat = 'tsv';
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);
    }

    /**
     * Verify that the class formats a timestamp into YYYYMMDD_HHmmss
     */
    public function testFormatTimestamp()
    {
        $ts = strtotime('2022-03-11T01:02:03');
        $expected = '20220311_010203';
        $actual = FileExport::formatTimestamp($ts);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that the class formats an input to 3-digit zero-padded numeral
     */
    public function testFormatSerialCodes()
    {
        $codes = ['1', '02', 1, 300, 4000, 'K75'];
        $expected = ['001', '002', '001', '300', '4000', '000'];
        $actual = FileExport::formatSerialCodes($codes);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that the class returns all valid file types
     */
    public function testGetFileTypes()
    {
        $expected = [
            'Mapping_Files', 'Planting_Instructions', '{{location}}_Planting_Instructions', 'Trait_Data_Collection',
            '{{location}}_Trait_Data_Collection', 'Entry_List', 'Plot_List', '{{location}}_Plot_List', 'Design_Layout', 'Field_Layout',
            'Cross_List', 'Variable_List', 'Seed_Packages', '{{crossList}}_Trait_Data_Collection', 'Parent_List', 'Data_Collection_Summaries',
            'Metadata_and_Plot_Data', 'Occurrence_Data_Collection',
        ];
        $actual = FileExport::getFileTypes();
        $this->assertEqualsCanonicalizing($expected, $actual);
    }

    /**
     * Verify that the class returns all valid file formats
     */
    public function testGetFileFormats()
    {
        $expected = ['csv', 'json', 'xlsx'];
        $actual = FileExport::getFileFormats();
        $this->assertEqualsCanonicalizing($expected, $actual);
    }

    /**
     * Verify that adding an experiment data with no serial codes is valid
     */
    public function testAddExperimentDataEmptySerialCodes()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::JSON;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = $serialData = [];

        // number of experiments added
        $expected = 1;
        $actual = $ef->addExperimentData($experimentCode, $serialCodes, $serialData);
        $this->assertEquals($expected, $actual);

        // experiment codes
        $expected = ['EXP0062155'];
        $actual = $ef->getExperimentCodes();
        $this->assertEquals($expected, $actual);

        // individual files
        // note: this is special for JSON mapping files, otherwise expected value should be an empty array
        $expected = ['EXP0062155_Mapping_Files_20220311_010203.json'=>[]];
        $actual = $ef->getUnzipped();
        $this->assertEquals($expected, $actual);

        // multiple files
        $expected = 'Multi-Experiments_Mapping_Files_JSON_20220311_010203.json';
        $actual = $ef->getZippedFilename();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that adding data to an existing experiment appends the values
     */
    public function testAddExperimentCodeExists()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = ['001'];
        $serialData = ['occurrence1Data'];
        $expected = 1;
        $actual = $ef->addExperimentData($experimentCode, $serialCodes, $serialData);
        $this->assertEquals($expected, $actual);

        $serialCodes = ['002', '020'];
        $serialData = ['occurrence2Data', 2];
        $expected = 0;  // there should be no change to the number of experiments
        $actual = $ef->addExperimentData($experimentCode, $serialCodes, $serialData);
        $this->assertEquals($expected, $actual);

        $expected = ['EXP0062155'];
        $actual = $ef->getExperimentCodes();
        $this->assertEquals($expected, $actual);

        $expected = [
            'EXP0062155_001_Mapping_Files_20220311_010203.csv' => 'occurrence1Data',
            'EXP0062155_002_Mapping_Files_20220311_010203.csv' => 'occurrence2Data',
            'EXP0062155_020_Mapping_Files_20220311_010203.csv' => 2,
        ];
        $actual = $ef->getUnzipped();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that if no experiments are added then getExperimentCodes should return an empty list
     */
    public function testGetExperimentCodesEmpty()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $expected = [];
        $actual = $ef->getExperimentCodes();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that if ONE experiment is added then getExperimentCodes should return a list with ONE value
     */
    public function testGetExperimentCodesOne()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = ['001', '010', '100'];
        $serialData = ['occurrence1Data', 2, ['occurrence3Data'=>null]];
        $ef->addExperimentData($experimentCode, $serialCodes, $serialData);

        $expected = ['EXP0062155'];
        $actual = $ef->getExperimentCodes();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that if TWO experiments are added then getExperimentCodes should return a list with TWO values
     */
    public function testGetExperimentCodesTwo()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = ['001', '010', '100'];
        $serialData = ['occurrence1Data', 2, ['occurrence3Data'=>null]];
        $ef->addExperimentData($experimentCode, $serialCodes, $serialData);

        $experimentCode = 'EXP0062158';
        $serialCodes = ['002', '020'];
        $serialData = ['occurrence1Data', 2];
        $ef->addExperimentData($experimentCode, $serialCodes, $serialData);

        $expected = ['EXP0062155', 'EXP0062158'];
        $actual = $ef->getExperimentCodes();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a Mapping file type object returns corresponding filenames
     */
    public function testGetFilenamesMappingFiles()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = ['001', '010', '100'];

        $expected = [
            'EXP0062155_001_Mapping_Files_20220311_010203.csv',
            'EXP0062155_010_Mapping_Files_20220311_010203.csv',
            'EXP0062155_100_Mapping_Files_20220311_010203.csv'
        ];
        $actual = $ef->getFilenames($experimentCode, $serialCodes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a Planting instructions (occurrence) type object returns corresponding filenames
     */
    public function testGetFilenamesPlantingInstructionsOccurrence()
    {
        $fileType = FileExport::PLANTING_INSTRUCTIONS_OCCURRENCE;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = ['001', '010', '100'];

        $expected = [
            'EXP0062155_001_Planting_Instructions_20220311_010203.csv',
            'EXP0062155_010_Planting_Instructions_20220311_010203.csv',
            'EXP0062155_100_Planting_Instructions_20220311_010203.csv'
        ];
        $actual = $ef->getFilenames($experimentCode, $serialCodes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a Planting instructions (location) type object returns corresponding filenames
     */
    public function testGetFilenamesPlantingInstructionsLocation()
    {
        $fileType = FileExport::PLANTING_INSTRUCTIONS_LOCATION;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts, location: 'Y2021B-A9-801');

        $experimentCode = 'EXP0062155';
        $serialCodes = ['001', '010', '100'];

        $expected = [
            'EXP0062155_001_Y2021B-A9-801_Planting_Instructions_20220311_010203.csv',
            'EXP0062155_010_Y2021B-A9-801_Planting_Instructions_20220311_010203.csv',
            'EXP0062155_100_Y2021B-A9-801_Planting_Instructions_20220311_010203.csv'
        ];
        $actual = $ef->getFilenames($experimentCode, $serialCodes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a Trait data collection (occurrence) type object returns corresponding filenames
     */
    public function testGetFilenamesTraitDataCollectionOccurrence()
    {
        $fileType = FileExport::TRAIT_DATA_COLLECTION_OCCURRENCE;
        $fileFormat = '';  // Trait data collection can either be .tft or .csv, so leave it blank
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = [ '100' ];

        $expected = [ 'EXP0062155_100_Trait_Data_Collection_20220311_010203' ];
        $actual = $ef->getFilenames($experimentCode, $serialCodes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that an Entry list type object returns corresponding filenames
     */
    public function testGetFilenamesEntryList()
    {
        $fileType = FileExport::ENTRY_LIST;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = [ '100' ];

        $expected = [ 'EXP0062155_100_Entry_List_20220311_010203.csv' ];
        $actual = $ef->getFilenames($experimentCode, $serialCodes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a Plot list type object returns corresponding filenames
     */
    public function testGetFilenamesPlotList()
    {
        $fileType = FileExport::PLOT_LIST;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = [ '100' ];

        $expected = [ 'EXP0062155_100_Plot_List_20220311_010203.csv' ];
        $actual = $ef->getFilenames($experimentCode, $serialCodes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a Design layout type object returns corresponding filenames
     */
    public function testGetFilenamesDesignLayout()
    {
        $fileType = FileExport::DESIGN_LAYOUT;
        $fileFormat = FileExport::XLSX;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = [ '100' ];

        $expected = [ 'EXP0062155_100_Design_Layout_20220311_010203.xlsx' ];
        $actual = $ef->getFilenames($experimentCode, $serialCodes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a Field layout type object returns corresponding filenames
     */
    public function testGetFilenamesFieldLayout()
    {
        $fileType = FileExport::FIELD_LAYOUT;
        $fileFormat = FileExport::XLSX;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = [ '100' ];

        $expected = [ 'EXP0062155_100_Field_Layout_20220311_010203.xlsx' ];
        $actual = $ef->getFilenames($experimentCode, $serialCodes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a Cross list type object returns corresponding filenames
     */
    public function testGetFilenamesCrossList()
    {
        $fileType = FileExport::CROSS_LIST;
        $fileFormat = '';  // Cross List use kartik\export\ExportMenu to dynamically set extensions
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = [ '100' ];

        $expected = [ 'EXP0062155_100_Cross_List_20220311_010203' ];
        $actual = $ef->getFilenames($experimentCode, $serialCodes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that an object with no experiments added returns an empty list of files to export
     */
    public function testGetUnzippedNoExperiment() {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);
        $expected = [];
        $actual = $ef->getUnzipped();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that an object 1 experiment with 3 occurrences added returns corresponding filenames and their data
     */
    public function testGetUnzippedOneExperimentThreeOccurrences()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = ['001', '010', '100'];
        $serialData = ['occurrence1Data', 2, ['occurrence3Data'=>null]];
        $ef->addExperimentData($experimentCode, $serialCodes, $serialData);

        $expected = [
            'EXP0062155_001_Mapping_Files_20220311_010203.csv' => 'occurrence1Data',
            'EXP0062155_010_Mapping_Files_20220311_010203.csv' => 2,
            'EXP0062155_100_Mapping_Files_20220311_010203.csv' => ['occurrence3Data'=>null],
        ];
        $actual = $ef->getUnzipped();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that an object 2 experiments added returns corresponding filenames and their data
     */
    public function testGetUnzippedTwoExperiments()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062155';
        $serialCodes = ['010'];
        $serialData = ['occurrence1Data'];
        $ef->addExperimentData($experimentCode, $serialCodes, $serialData);

        $experimentCode = 'EXP0062158';
        $serialCodes = ['020', '200'];
        $serialData = ['occurrence2Data', 200];
        $ef->addExperimentData($experimentCode, $serialCodes, $serialData);

        $expected = [
            'EXP0062155_010_Mapping_Files_20220311_010203.csv' => 'occurrence1Data',
            'EXP0062158_020_Mapping_Files_20220311_010203.csv' => 'occurrence2Data',
            'EXP0062158_200_Mapping_Files_20220311_010203.csv' => 200,
        ];
        $actual = $ef->getUnzipped();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that empty string file formats are acceptable
     */
    public function testGetUnzippedEmptyFileFormat()
    {
        $fileType = FileExport::CROSS_LIST;
        $fileFormat = '';
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $experimentCode = 'EXP0062158';
        $serialCodes = ['020', '200'];
        $serialData = ['occurrence2Data', 200];
        $ef->addExperimentData($experimentCode, $serialCodes, $serialData);

        $expected = [
            'EXP0062158_020_Cross_List_20220311_010203' => 'occurrence2Data',
            'EXP0062158_200_Cross_List_20220311_010203' => 200,
        ];
        $actual = $ef->getUnzipped();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a CSV filename is returned for multi-occurrence/location
     */
    public function testGetZippedFilenameCsvMappingFiles()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $expected = 'Multi-Experiments_Mapping_Files_CSV_20220311_010203.zip';
        $actual = $ef->getZippedFilename();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a JSON filename is returned for multi-occurrence/location
     */
    public function testGetZippedFilenameJsonMappingFiles()
    {
        $fileType = FileExport::MAPPING_FILES;
        $fileFormat = FileExport::JSON;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);

        $expected = 'Multi-Experiments_Mapping_Files_JSON_20220311_010203.json';
        $actual = $ef->getZippedFilename();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a no-extension filename is returned for multi-location of Trait data collection
     */
    public function testGetZippedTraitDataCollectionLocation()
    {
        $fileType = FileExport::TRAIT_DATA_COLLECTION_LOCATION;
        $fileFormat = '';  // Trait data collection can either be .tft or .csv, so leave it blank
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts, location: 'Y2021B-A9-801');

        $expected = 'Y2021B-A9-801_Trait_Data_Collection_20220311_010203.zip';
        $actual = $ef->getZippedFilename();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a filename is returned for multi-location of Planting instruction
     */
    public function testGetZippedPlantingInstructionLocation()
    {
        $fileType = FileExport::PLANTING_INSTRUCTIONS_LOCATION;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts, location: 'Y2021B-A9-801');

        $expected = 'Y2021B-A9-801_Planting_Instructions_20220311_010203.zip';
        $actual = $ef->getZippedFilename();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a filename is returned for multi-location of Plot list
     */
    public function testGetZippedPlotListLocation()
    {
        $fileType = FileExport::PLOT_LIST_LOCATION;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts, location: 'Y2021B-A9-801');

        $expected = 'Y2021B-A9-801_Plot_List_20220311_010203.zip';
        $actual = $ef->getZippedFilename();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that correct file name format is returned
     */
    public function testGetFileName(){
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport(fileFormat: FileExport::CSV, time: $ts, fileName: 'Y2021B-A9-801', suffix: 'Plot_List');

        $expected = 'Y2021B-A9-801_Plot_List_20220311_010203.csv';
        $actual = $ef->getFileName();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that correct file name format with invalid character is returned
     */
    public function testGetFileNameInvalid(){
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport(fileFormat: FileExport::CSV, time: $ts, fileName: 'Y2021B-A9-801~+;"<>?', suffix: 'Plot_List');

        $expected = 'Y2021B-A9-801_Plot_List_20220311_010203.csv';
        $actual = $ef->getFileName();

        $this->assertEquals($expected, $actual);
    }
}