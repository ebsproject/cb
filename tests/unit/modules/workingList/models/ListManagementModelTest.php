<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\workingList\models;

use Yii;

use app\dataproviders\ArrayDataProvider;

class ListManagementModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $workingListWidgetModel;
    
    protected function _before()
    {
        $this->workingListWidgetModel = Yii::$container->get('workingListManagementModel');

        $this->variableConfig = [
            'values' => [
                [
                    "abbrev" => "PROGRAM",
                    "display_name" => "Program",
                    "label" => "Package Program",
                    "description" => "Package program",
                    "browser_column" => "true",
                    "visible_browser" => "true",
                    "visible_export" => "true",
                    "visible_template" => "true",
                    "sort" => "true",
                    "filter" => "true",
                    "data_level" => "package"
                ],
                [
                    "abbrev" => "SEED_CODE",
                    "display_name" => "Seed Code",
                    "label" => "Seed Code",
                    "description" => "Seed code",
                    "browser_column" => "true",
                    "visible_browser" => "true",
                    "visible_export" => "true",
                    "visible_template" => "true",
                    "sort" => "true",
                    "filter" => "true",
                    "data_level" => "seed"
                ]
            ]
        ];

        $this->attribEquiv = [
            'SEED_CODE' => 'GID',
            'GERMPLASM_NAME' => 'designation',
            'PACKAGE_LABEL' => 'label',
            'PROGRAM' => 'seedManager'
        ];
    }

    protected function _after()
    {
    }

    // Test getAttribEquiv
    public function testGetAttribEquiv(){
        // ACT
        $actual = $this->workingListWidgetModel->getAttribEquiv();

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($this->attribEquiv,$actual);
    }

    // Test buildCSVContent
    public function testBuildCSVContent(){
        // ARRANGE
        $expected = [
            ['PROGRAM','SEED_CODE'],
            ['IRSEA','testSeedCode#1'],
            ['IRSEA','testSeedCode#2'],
            [null,'testSeedCode#2']
        ];

        $columns = [
            'seedManager' => 'PROGRAM',
            'GID' => 'SEED_CODE'
        ];

        $listItems = [
            [
                'seedManager' => 'IRSEA',
                'GID' => 'testSeedCode#1'
            ],
            [
                'seedManager' => 'IRSEA',
                'GID' => 'testSeedCode#2'
            ],
            [
                'seedName' => 'testSeedName',
                'GID' => 'testSeedCode#2'
            ]
        ];

        // ACT
        $actual = $this->workingListWidgetModel->buildCSVContent($columns,$listItems);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testBuildExportColumns(){
        // ARRANGE
        $expected = [
            'seedManager' => 'PROGRAM',
            'GID' => 'SEED_CODE'
        ];

        // ACT
        $actual = $this->workingListWidgetModel->buildExportColumns($this->variableConfig['values']);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);        
    }

    public function testBuildExportColumnsEmptyVariables(){
        // ACT
        $actual = $this->workingListWidgetModel->buildExportColumns([]);

        // ASSERT
        $this->assertEquals([],$actual);        
    }

    public function testRemoveItems(){
        // ARRANGE
        $expected = 200;

        $listItemIds = [123,456,789];
        $bgprocess = true;

        // create mocks
        $deleteResults = [
            "status" => 200,
            "totalCount" => 3
        ];
        $this->workingListWidgetModel->listMemberModel->setReturnValue('deleteMany',$deleteResults);

        // ACT
        $actual = $this->workingListWidgetModel->removeItems($listItemIds,$bgprocess);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);        
    }

    public function testUpdateListItemOrder(){
        // ARRANGE
        $expected = 200;

        // Create mocks
        $sort = "attribute:ASC";
        $listId = 123;

        $returnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [
                            'listDbId'=>123
                        ]                     
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function() use ($returnValue) {
                    return $returnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->workingListWidgetModel->updateListItemOrder($sort,$listId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);  
    }

    public function testAddItemsToWorkingList(){
        // ASSERT
        $expected = [
            "status" => 200,
            "result" => [
                "data" => [
                    [ 'listDbId' => 1 ],
                    [ 'listDbId' => 2 ],
                    [ 'listDbId' => 3 ]    
                ]
            ]
        ];

        $listId = 123;
        $listItemIdArr = [
            [ 
                'id' => "1",
                'displayValue' => 'testPkg1'
            ],
            [
                'id' => "2",
                'displayValue' => 'testPkg2'
            ],
            [
                'id' => "3",
                'displayValue' => 'testPkg3'
            ]
        ];

        $returnValue = [
            "status" => 200,
            "result" => [
                "data" => [
                    [ 'listDbId' => 1 ],
                    [ 'listDbId' => 2 ],
                    [ 'listDbId' => 3 ]    
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function() use ($returnValue) {
                    return $returnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->workingListWidgetModel->addItemsToWorkingList($listId,$listItemIdArr);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testGetExperimentDataProvider(){
        // ASSERT
        $expected = [
            'success' => true,
            'message' => "",
            'data' => [
                [
                    'experimentDbId'=>123,
                    'experimentCode'=>'TESTEXPERIMENTCODE#1',
                    'experimentType'=>'Test Experiment Type',
                    'occurrenceCount'=>0,
                    'entryCount'=>0,
                    'project'=>'',
                    'season'=>'Dry',
                    'stage'=>'F1',
                    'experimentYear'=>2021,
                    'designType'=>'systematic Arrangement'
                ]
            ],
            'totalCount' => 1,
            'status' => 200,
            'totalPages' => 1
        ];

        $program = 'IRSEA';

        // Create mocks
        $experimentTestDP = [
            "abbrev" => 'LIST_ABBREV',
            "name" => 'List Name',
            "type" => 'type'
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function() {
                    return [
                        "status" => 200,
                        "body" => [
                            "metadata"=>[
                                "status"=>[
                                    [
                                        "message" => ""
                                    ]
                                ],
                                "pagination"=>[
                                    "totalCount"=>1,
                                    "totalPages"=>1
                                ]
                            ],
                            "result"=>[
                                "data" => [
                                    [
                                        'experimentDbId'=>123,
                                        'experimentCode'=>'TESTEXPERIMENTCODE#1',
                                        'experimentType'=>'Test Experiment Type',
                                        'occurrenceCount'=>0,
                                        'entryCount'=>0,
                                        'project'=>'',
                                        'season'=>'Dry',
                                        'stage'=>'F1',
                                        'experimentYear'=>2021,
                                        'designType'=>'systematic Arrangement'
                                    ]                     
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->workingListWidgetModel->getExperimentDataProvider($program);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual); 
    }

    public function testGetNewEntryListRecords(){
        // ASSERT
        $expected = [
            [
                'entryName' => 'test designation',
                'entryType' => 'entry_type',
                'entryStatus' => 'active',
                'entryListDbId' => '12',
                'germplasmDbId' => '1',
                'entryClass' => 'entry_class',
                'entryRole' => 'entry_role',
                'seedDbId' => '23',
                'packageDbId' => '45'
            ]
        ];

        $experimentId = 123;
        $entryListId = 12;
        $programId = 101;

        $experiment = [
            'experimentDbId'=>123,
            'experimentCode'=>'TESTEXPERIMENTCODE#1',
            'experimentType'=>'Test Experiment Type',
            'occurrenceCount'=>0,
            'entryCount'=>0,
            'project'=>'',
            'season'=>'Dry',
            'stage'=>'F1',
            'experimentYear'=>2021,
            'designType'=>'systematic Arrangement'
        ];

        $configData = [
            [
                'default' => "entry_type",
                'variable_abbrev' => 'ENTRY_TYPE'
            ],
            [
                'default' => "entry_class",
                'variable_abbrev' => 'ENTRY_CLASS'
            ],
            [
                'default' => "entry_role",
                'variable_abbrev' => 'ENTRY_ROLE'
            ],
            [
                'default' => "default",
                'variable_abbrev' => 'DEFAULT'
            ]
        ];

        $listInfo = [
            'listDbId' => 1234
        ];

        // Create mocks
        $searchResults = [
            "status" => 200,
            "data" => [
                [
                    'germplasmDbId' => 1,
                    'designation' => 'test designation',
                    'seedDbId' => 23,
                    'packageDbId' => 45,
                    'isActive' => true,
                    'programDbId' => 101
                ]
            ]
        ];

        // Create mock
        $this->workingListWidgetModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchResults);

        // ACT
        $actual = $this->workingListWidgetModel->getNewEntryListRecords($experimentId,$experiment,$configData,$entryListId,$listInfo,$programId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual); 
    }
}
?>