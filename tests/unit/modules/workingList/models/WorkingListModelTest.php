<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\workingList\models;

use Yii;

use app\dataproviders\ArrayDataProvider;

class WorkingListModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $workingListWidgetModel;
    
    protected function _before()
    {
        $this->workingListWidgetModel = Yii::$container->get('workingListWidgetModel');

        $this->variableConfig = [
            'values' => [
                [
                    "abbrev" => "PROGRAM",
                    "display_name" => "Program",
                    "label" => "Package Program",
                    "description" => "Package program",
                    "browser_column" => "true",
                    "visible_browser" => "true",
                    "visible_export" => "true",
                    "visible_template" => "true",
                    "sort" => "true",
                    "filter" => "true",
                    "data_level" => "package"
                ]
            ]
        ];
    }

    protected function _after()
    {
    }

    // If has no user session working list
    public function testGetWorkingListNoList(){
        // ARRANGE
        $expected = [];

        $userId = 470;
        $listType = 'package';

        $searchResults = [
            "status" => 200,
            "data" => []
        ];

        // Create mock
        $this->workingListWidgetModel->listsModel->setReturnValue('searchAll',$searchResults);
        
        // ACT
        $actual = $this->workingListWidgetModel->getWorkingList($userId,$listType);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    // If has user session working list
    public function testGetWorkingListHasList(){
        // ARRANGE
        $expected = [
            "abbrev" => "WORKING_LIST_PACKAGE_470",
            "displayName" => "Working List Package",
            "listDbId" => 1
        ];

        $userId = 470;
        $listType = 'package';

        $searchResults = [
            "status" => 200,
            "data" => [
                [
                    "abbrev" => "WORKING_LIST_PACKAGE_470",
                    "displayName" => "Working List Package",
                    "listDbId" => 1
                ]
            ]
        ];

        // Create mock
        $this->workingListWidgetModel->listsModel->setReturnValue('searchAll',$searchResults);
        
        // ACT
        $actual = $this->workingListWidgetModel->getWorkingList($userId,$listType);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // If has no user session working list
    public function testCreateWorkingList(){
        // ARRANGE
        $expected = 200;

        $userId = 470;
        $listType = 'package';

        // Create mock
        $returnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [
                            'listDbId'=>123,
                            'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                            'name'=>'Find Seed Working List 470',
                            'displayName'=>'Find Seed Working List 470',
                            'listType'=>'package' 
                        ]                     
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function() use ($returnValue) {
                    return $returnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);
        
        // ACT
        $actual = $this->workingListWidgetModel->createWorkingList($userId,$listType);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testGetVariableConfigurationHasCropConfig(){
        // ARRANGE
        $expected = [
            [
                "abbrev" => "PROGRAM",
                "display_name" => "Program",
                "label" => "Package Program",
                "description" => "Package program",
                "browser_column" => "true",
                "visible_browser" => "true",
                "visible_export" => "true",
                "visible_template" => "true",
                "sort" => "true",
                "filter" => "true",
                "data_level" => "package"
            ]
        ];

        // Create mock
        $testConfig = $this->variableConfig;

        $this->workingListWidgetModel->programModel->setReturnValue('getCropCode','RICE');
        $this->workingListWidgetModel->configModel->setReturnValue('getConfigByAbbrev',$testConfig);

        // ACT
        $actual = $this->workingListWidgetModel->getVariableConfiguration('IRSEA');

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testValidateVariables(){
        // ARRANGE
        $expected = [
            [
                "abbrev" => "PROGRAM",
                "display_name" => "Program",
                "label" => "Package Program",
                "description" => "Package program",
                "browser_column" => "true",
                "visible_browser" => "true",
                "visible_export" => "true",
                "visible_template" => "true",
                "sort" => "true",
                "filter" => "true",
                "data_level" => "package"
            ]
        ];

        $variables = $this->variableConfig['values'];

        // Create mock
        $variableInfo = [
            "abbrev" => "PROGRAM",
            "label" => "Package Program",
            "description" => "Package program",
        ];
        $this->workingListWidgetModel->variableModel->setReturnValue('getVariableByAbbrev',$variableInfo);

        // ACT
        $actual = $this->workingListWidgetModel->validateVariables($variables);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testBuildBrowserColumns(){
        // ARRANGE
        $expected = [
            [
                'attribute' => 'seedManager',
                'label' => 'Program',
                'visible' => true,
                'hidden' => false
            ]
        ];
    
        $variables = $variables = $this->variableConfig['values'];

        // ACT
        $actual = $this->workingListWidgetModel->buildBrowserColumns($variables);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test search function
    public function testSearch(){
        // ARRANGE
        $expected = new ArrayDataProvider([
                'allModels' => [
                    ["packageDbId" => "1"]
                ],
                'key' => "listMemberDbId",
                'restified' => true,
                'totalCount' => 1,
                'id' => 'datagrid-working-list'
            ]);

        $params = [];
        $listId = 123;
        $listType = 'package';

        // create mocks
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "result" => [
                    "data" => [
                        ["packageDbId" => "1"]
                    ]
                ],
                "metadata" => [
                    "pagination" => [
                        "totalCount" => 1
                    ]
                ]
            ]
        ];

        $request = $this->make(
            'yii\web\Request',
            [
                'queryParams' => [
                    'datagrid-working-list-page' => '2',
                    'page' => '2',
                    'sort' => '-attribute1|attribute2',
                    'WorkingListModel' => [
                        'packageDbId' => '1'
                    ]
                ]
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request', $request);

        $api = $this->make(
            'app\components\Api',
            [
                'getResponse' => function ($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        Yii::$app->set('api', $api);

        $this->workingListWidgetModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 5);

        // ACT
        $actual = $this->workingListWidgetModel->search($params,$listId,$listType);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test search function with no browser filter values
    public function testSearchNoFilters(){
        // ARRANGE
        $expected = new ArrayDataProvider([
                'allModels' => [
                    ["packageDbId" => "1"],
                    ["packageDbId" => "2"]
                ],
                'key' => "listMemberDbId",
                'restified' => true,
                'totalCount' => 1,
                'id' => 'datagrid-working-list'
            ]);

        $params = [];
        $listId = 123;
        $listType = 'package';

        // create mocks
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "result" => [
                    "data" => [
                        ["packageDbId" => "1"],
                        ["packageDbId" => "2"]
                    ]
                ],
                "metadata" => [
                    "pagination" => [
                        "totalCount" => 1
                    ]
                ]
            ]
        ];

        $request = $this->make(
            'yii\web\Request',
            [
                'queryParams' => [
                    'datagrid-working-list-page' => '2',
                    'page' => '2',
                    'sort' => '-attribute1|attribute2'
                ]
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request', $request);
        $api = $this->make(
            'app\components\Api',
            [
                'getResponse' => function ($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        Yii::$app->set('api', $api);

        $this->workingListWidgetModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 5);

        // ACT
        $actual = $this->workingListWidgetModel->search($params,$listId,$listType);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testAssembleFilters(){
        // ARRANGE
        $expected = 'limit=5&page=1&sort=attribute1:DESC|attribute2:ASC';

        // create mocks
        $this->workingListWidgetModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences',5);

        $request = $this->make('yii\web\Request',
            [
                'queryParams' => [
                    'datagrid-test-page' => '1',
                    'sort' => '-attribute1|attribute2'
                ]
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // ACT
        $actual = $this->workingListWidgetModel->assembleFilters('datagrid-test');

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testAssembleFiltersResetPage(){
        // ARRANGE
        $expected = 'limit=5&page=1&sort=attribute1:DESC|attribute2:ASC';

        // create mocks
        $this->workingListWidgetModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences',5);

        $request = $this->make('yii\web\Request',
            [
                'queryParams' => [
                    'page' => '2',
                    'sort' => '-attribute1|attribute2'
                ]
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // ACT
        $actual = $this->workingListWidgetModel->assembleFilters('datagrid-test',true);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testSetColumnsForFiltering(){
        // ARRANGE
        $expected = true;

        // ACT
        $actual = $this->workingListWidgetModel->setColumnsForFiltering($this->variableConfig['values']);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual); 
    }

    public function testProcessNotifParams(){
        // ARRANGE
        $expected = [
            "creatorDbId" => "equals 123",
            "workerName" => "equals WorkerName1|equals WorkerName2",
            "isSeen" => "equals 1"
        ];

        $workers = 'WorkerName1|WorkerName2';
        $remarks = "";
        $isSeen = true;
        $userId = 123;

        // ACT
        $actual = $this->workingListWidgetModel->processNotifParams($workers, $remarks, $isSeen, $userId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual); 
    }

    public function testCreateNewList(){
        // ARRANGE
        $expected = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [
                            'listDbId'=>123
                        ]                     
                    ]
                ]
            ]
        ];

        // Create mocks
        $listInfo = [
            "abbrev" => 'LIST_ABBREV',
            "name" => 'List Name',
            "type" => 'type'
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function() {
                    return [
                        "status" => 200,
                        "body" => [
                            "metadata"=>[
                                "pagination"=>[
                                    "totalCount"=>1,
                                    "totalPages"=>1
                                ]
                            ],
                            "result"=>[
                                "data" => [
                                    [
                                        'listDbId'=>123
                                    ]                     
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->workingListWidgetModel->createNewList($listInfo);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual); 
    }

    public function testGetListItems(){
        // ARRANGE
        $expected = [
            ['listMemberDbId' => 1, 'packageDbId' => 12],
            ['listMemberDbId' => 2, 'packageDbId' => 34]
        ];

        // Create mocks
        $params = [];
        $listId = 123;

        $searchResults = [
            'data' => [
                ['listMemberDbId' => 1, 'packageDbId' => 12],
                ['listMemberDbId' => 2, 'packageDbId' => 34]
            ],
            'totalCount' => 2
        ];

        $this->workingListWidgetModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchResults);

        // ACT
        $actual = $this->workingListWidgetModel->getListItems($params,$listId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual); 
    }

    public function testDeleteWorkingList(){
        // ARRANGE
        $expected = 200;

        // Create mocks
        $listId = 123;
        $listType = 'type';
        $userId = 1234;

        $results = [
            'status' => 200,
            'result' => [
                'data' => [
                    ['listDbId' => 123]
                ]
            ]
        ];

        $this->workingListWidgetModel->listsModel->setReturnValue('updateOne',$results);

        // ACT
        $actual = $this->workingListWidgetModel->deleteWorkingList($listId,$listType,$userId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual); 
    }
}
?>