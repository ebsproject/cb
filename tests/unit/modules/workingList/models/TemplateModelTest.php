<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\workingList\models;

use Yii;

use app\dataproviders\ArrayDataProvider;

class TemplateModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $templateModel;
    
    protected function _before()
    {
        $this->templateModel = Yii::$container->get('templateModel');

        $this->variableConfig = [
            'values' => [
                [
                    "abbrev" => "PROGRAM",
                    "display_name" => "Program",
                    "label" => "Package Program",
                    "description" => "Package program",
                    "browser_column" => "true",
                    "visible_browser" => "true",
                    "visible_export" => "true",
                    "visible_template" => "true",
                    "sort" => "true",
                    "filter" => "true",
                    "data_level" => "package"
                ]
            ]
        ];
    }

    protected function _after()
    {
    }

    // If has no user session working list
    public function testExtractTemplateHeaders(){
        // ARRANGE
        $expected = [];
        
        //MOCK
        $varConfig = $this->variableConfig;

        // ACT
        $actual = $this->templateModel->extractTemplateHeaders($varConfig,101);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }
}
?>