<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

// Import data providers
use app\dataproviders\ArrayDataProvider;

class CreationModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $creationModel;
    
    protected function _before()
    {
        $this->creationModel = \Yii::$container->get('creationModel');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * getActionButtons: Test 1
     * Only one browser is displayed
     */
    public function testGetActionButtonsWhenOneBrowserIsDisplayed()
    {
        // ARRANGE
        // inputs
        $twoBrowsers = false;

        // expected
        $expected = '<aid="hm-crt-complete-harvest-btn"class="btnbtn-defaulthm-tooltipped"href="#"style="margin-right:5px;background-color:#5cb85c!important;margin-left:10px;"data-position="top"data-tooltip="CompleteHarvest"data-toggle="modal"data-target="#hm-complete-harvest-modal">CompleteHarvest</a><aid="hm-crt-commit-data-btn"class="btnwaves-effectwaves-lightwhite-textlight-bluedarken-3hm-tooltipped"href="#"data-position="top"data-tooltip="Commitharvestdata"data-toggle="modal"data-target="#hm-harvest-data-commit-modal">Commit</a><aid="hm-crt-create-package-btn"class="btnwaves-effectwaves-lightwhite-textlight-greendarken-3hm-tooltipped"href="#"data-position="top"data-tooltip="Createpackages,seeds,andgermplasm"data-toggle="modal"data-target="#hm-harvest-record-creation-modal">Create</a><aid="hm-crt-notifs-btn"class="btnwaves-effectwaves-lightharvest-manager-notification-button-creationhm-tooltipped"href="#"data-pjax="0"style="overflow:visible!important;"data-position="top"data-tooltip="Notifications"data-activates="notifications-dropdown"><iclass="material-iconslarge">notifications_none</i><spanid="creation-notif-badge-id"class=""></span><spanid="notif-badge-id"class=""></span></a>';

        // ACT
        $actual = $this->creationModel->getActionButtons($twoBrowsers);

        // ASSERT
        // Remove spaces and new lines before asserting equality
        $noSpaceExpected = str_replace(PHP_EOL, '', str_replace(' ', '', $expected));
        $noSpaceActual = str_replace(PHP_EOL, '', str_replace(' ', '', $actual));
        $this->assertEquals($noSpaceExpected, $noSpaceActual);
    }

    /**
     * getActionButtons: Test 2
     * Two browsers are displayed
     */
    public function testGetActionButtonsWhenTwoBrowsersAreDisplayed()
    {
        // ARRANGE
        // inputs
        $twoBrowsers = true;

        // expected
        $expected = '<aid="hm-crt-complete-harvest-btn"class="btnbtn-defaulthm-tooltipped"href="#"style="margin-right:5px;background-color:#5cb85c!important;margin-left:10px;"data-position="top"data-tooltip="CompleteHarvest"data-toggle="modal"data-target="#hm-complete-harvest-modal">CompleteHarvest</a><aid="hm-crt-commit-data-btn"class="btnwaves-effectwaves-lightwhite-textlight-bluedarken-3hm-tooltipped"href="#"data-position="top"data-tooltip="Commitharvestdata"data-toggle="modal"data-target="#hm-harvest-data-commit-modal">Commit</a><aid="hm-crt-create-package-btn"class="btnwaves-effectwaves-lightwhite-textlight-greendarken-3hm-tooltipped"href="#"data-position="top"data-tooltip="Createpackages,seeds,andgermplasm"data-toggle="modal"data-target="#hm-harvest-record-creation-modal">Create</a><aid="hm-crt-notifs-btn"class="btnwaves-effectwaves-lightharvest-manager-notification-button-creationhm-tooltipped"href="#"data-pjax="0"style="overflow:visible!important;"data-position="top"data-tooltip="Notifications"data-activates="notifications-dropdown"><iclass="material-iconslarge">notifications_none</i><spanid="creation-notif-badge-id"class=""></span><spanid="notif-badge-id"class=""></span></a>';
        // ACT
        $actual = $this->creationModel->getActionButtons($twoBrowsers);

        // ASSERT
        // Remove spaces and new lines before asserting equality
        $noSpaceExpected = str_replace(PHP_EOL, '', str_replace(' ', '', $expected));
        $noSpaceActual = str_replace(PHP_EOL, '', str_replace(' ', '', $actual));
        $this->assertEquals($noSpaceExpected, $noSpaceActual);
    }

    /**
     * determineDataToLoad: Test 1
     * Both plots and crosses to be loaded
     */
    public function testDetermineDataToLoadWhenBothPlotsAndCrossesAreLoaded() {
        // ARRANGE
        // inputs
        $pageUrl = '';
        $postParams = [];

        // expected
        $expected = [
            'loadPlots' => true,
            'loadCrosses' => true
        ];

        //ACT
        $actual = $this->creationModel->determineDataToLoad($pageUrl, $postParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * determineDataToLoad: Test 2
     * Load crosses
     */
    public function testDetermineDataToLoadWhenCrossesAreLoaded() {
        // ARRANGE
        // inputs
        $pageUrl = 'dynagrid-harvest-manager-cross-crt-grid-pjax';
        $postParams = [
            'target' => 'dynagrid-harvest-manager-cross-crt-grid-pjax'
        ];

        // expected
        $expected = [
            'loadPlots' => false,
            'loadCrosses' => true
        ];

        //ACT
        $actual = $this->creationModel->determineDataToLoad($pageUrl, $postParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * determineDataToLoad: Test 3
     * Load plots
     */
    public function testDetermineDataToLoadWhenPlotsAreLoaded() {
        // ARRANGE
        // inputs
        $pageUrl = 'dynagrid-harvest-manager-plot-crt-grid-pjax';
        $postParams = [
            'target' => 'dynagrid-harvest-manager-plot-crt-grid-pjax'
        ];

        // expected
        $expected = [
            'loadPlots' => true,
            'loadCrosses' => false
        ];

        //ACT
        $actual = $this->creationModel->determineDataToLoad($pageUrl, $postParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * determineDataToLoad: Test 3
     * Both creation browser pjax are present in URL
     */
    public function testDetermineDataToLoadWhenCrossAndPlotPjaxAreBothInTheUrl() {
        // ARRANGE
        // inputs
        $pageUrl = 'dynagrid-harvest-manager-plot-crt-grid-pjax dynagrid-harvest-manager-cross-crt-grid-pjax';
        $postParams = [
            'target' => null
        ];

        // expected
        $expected = [
            'loadPlots' => true,
            'loadCrosses' => true
        ];

        //ACT
        $actual = $this->creationModel->determineDataToLoad($pageUrl, $postParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * renderHarvestDataColumn: Test 1
     * Missing required input - no terminal or committed value
     */
    public function testRenderHarvestDataColumnTest1() {
        // ARRANGE
        // inputs
        $columnName = 'harvestDate';
        $required = true;
        // Produce properly capitalized column name
        $splitName = preg_split('/(?=[A-Z])/',$columnName);
        $splitName[0] = ucfirst($splitName[0]);
        $columnNameProcessed = join(" ", $splitName);
        $terminal = null;
        $committed = null;
        $model = [
            'harvestDataValid' => false
        ];

        // expected
        $expected = "<span title='Missing input for $columnNameProcessed' class='badge center white red-text text-darken-3' style='margin: 0px 0px 5px 0px;'>?</span>";

        // ACT
        $actual = $this->creationModel->renderHarvestDataColumn($columnName, $terminal, $committed, $model, $required);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * renderHarvestDataColumn: Test 2
     * Input not required
     */
    public function testRenderHarvestDataColumnTest2() {
        // ARRANGE
        // inputs
        $columnName = 'harvestDate';
        $required = true;
        // Produce properly capitalized column name
        $splitName = preg_split('/(?=[A-Z])/',$columnName);
        $splitName[0] = ucfirst($splitName[0]);
        $columnNameProcessed = join(" ", $splitName);
        $terminal = null;
        $committed = null;
        $model = [
            'harvestDataValid' => true
        ];

        // expected
        $expected = "<span title='Input for $columnNameProcessed is not required' class='badge center grey lighten-2 text-darken-3' style='margin: 0px 0px 5px 0px;'>NR</span>";

        // ACT
        $actual = $this->creationModel->renderHarvestDataColumn($columnName, $terminal, $committed, $model, $required);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * renderHarvestDataColumn: Test 3
     * Terminal value is set
     */
    public function testRenderHarvestDataColumnTest3() {
        // ARRANGE
        // inputs
        $columnName = 'harvestDate';
        $required = true;
        // Produce properly capitalized column name
        $splitName = preg_split('/(?=[A-Z])/',$columnName);
        $splitName[0] = ucfirst($splitName[0]);
        $columnNameProcessed = join(" ", $splitName);
        $terminal = '2020-07-24';
        $committed = null;
        $model = [
            'harvestDataValid' => true
        ];

        // expected
        $expected = "<span title='Uncommitted $columnNameProcessed' class='badge center amber grey-text text-darken-2' style='margin: 0px 0px 5px 0px;'>" . $terminal . "</span>";

        // ACT
        $actual = $this->creationModel->renderHarvestDataColumn($columnName, $terminal, $committed, $model, $required);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * renderHarvestDataColumn: Test 4
     * Committed value is set
     */
    public function testRenderHarvestDataColumnTest4() {
        // ARRANGE
        // inputs
        $columnName = 'harvestDate';
        $required = true;
        // Produce properly capitalized column name
        $splitName = preg_split('/(?=[A-Z])/',$columnName);
        $splitName[0] = ucfirst($splitName[0]);
        $columnNameProcessed = join(" ", $splitName);
        $terminal = null;
        $committed = '2020-12-11';
        $model = [
            'harvestDataValid' => true
        ];

        // expected
        $expected = "<span title='Committed $columnNameProcessed' class='badge center light-green darken-4 white-text' style='margin: 0px 0px 5px 0px;'>" . $committed . "</span>";

        // ACT
        $actual = $this->creationModel->renderHarvestDataColumn($columnName, $terminal, $committed, $model, $required);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * renderHarvestDataColumn: Test 5
     * Terminal and committed values are set
     */
    public function testRenderHarvestDataColumnTest5() {
        // ARRANGE
        // inputs
        $columnName = 'harvestDate';
        $required = true;
        // Produce properly capitalized column name
        $splitName = preg_split('/(?=[A-Z])/',$columnName);
        $splitName[0] = ucfirst($splitName[0]);
        $columnNameProcessed = join(" ", $splitName);
        $terminal = '2020-07-24';
        $committed = '2020-12-11';
        $model = [
            'harvestDataValid' => true
        ];

        // expected
        $expected = "<span title='Committed $columnNameProcessed' class='badge center light-green darken-4 white-text' style='margin: 0px 0px 5px 0px;'>" . $committed . "</span>" .
                    "<br>" .
                    "<span title='Uncommitted $columnNameProcessed' class='badge center amber grey-text text-darken-2' style='margin: 0px 0px 5px 0px;'>" . $terminal . "</span>";

        // ACT
        $actual = $this->creationModel->renderHarvestDataColumn($columnName, $terminal, $committed, $model, $required);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 1
     * Record's harvest status is READY
     */
    public function testHarvestDataIsValidWhenHarvestStatusIsReady()
    {
        // ARRANGE
        // inputs
        $browserConfig = [];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "harvestStatus" => "READY"
        ];

        // expected
        $expected = [
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "harvestStatus" => "READY",
            "harvestRemarks" => '<span title="Ready for package, seed and germplasm creation" class="new badge light-green darken-3"><strong>FOR CREATION</strong></span>',
            "harvestDataValid" => true
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 2
     * Required column values are missing (harvest date and harvest method)
     */
    public function testHarvestDataIsValidWhenRequiredValuesAreMissing()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_EARS"
            ],
            "numeric_variables_field" => [
                "noOfEar"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SELFING" => [
                    "Bulk" => [ "" ],
                    "Individual Ear" => [ "NO_OF_EARS" ],
                ]    
            ],
            "CROSS_METHOD_SELFING" => [
                "not_fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                            ,
                            [
                                "abbrev" => "<none>",
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => null,
            "terminalHarvestDate" => null,
            "harvestMethod" => null,
            "terminalHarvestMethod" => null
        ];

        // expected
        $expected = [
            "harvestStatus" => "NO_HARVEST",
            "harvestRemarks" => '<span class="new badge red darken-3" style="margin-bottom:3px"><strong>MISSING VALUE</strong></span><br><span class="new badge white red-text text-darken-3" style="margin-bottom:3px"><strong>HARVEST DATE</strong></span><br><span class="new badge white red-text text-darken-3" style="margin-bottom:3px"><strong>HARVEST METHOD</strong></span>',
            "harvestDataValid" => false,
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestDate" => null,
            "terminalHarvestDate" => null,
            "harvestMethod" => null,
            "terminalHarvestMethod" => null
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 3
     * Required column values are missing (harvest date)
     */
    public function testHarvestDataIsValidWhenHarvestDateIsMissing()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_EARS"
            ],
            "numeric_variables_field" => [
                "noOfEar"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SELFING" => [
                    "Bulk" => [ "" ],
                    "Individual Ear" => [ "NO_OF_EARS" ],
                ]    
            ],
            "CROSS_METHOD_SELFING" => [
                "not_fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                            ,
                            [
                                "abbrev" => "<none>",
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => null,
            "terminalHarvestDate" => null,
            "harvestMethod" => "Bulk",
            "terminalHarvestMethod" => null
        ];

        // expected
        $expected = [
            "harvestStatus" => "NO_HARVEST",
            "harvestRemarks" => '<span class="new badge red darken-3" style="margin-bottom:3px"><strong>MISSING VALUE</strong></span><br><span class="new badge white red-text text-darken-3" style="margin-bottom:3px"><strong>HARVEST DATE</strong></span>',
            "harvestDataValid" => false,
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestDate" => null,
            "terminalHarvestDate" => null,
            "harvestMethod" => 'Bulk',
            "terminalHarvestMethod" => null
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 4
     * Required column values are missing (harvest method)
     */
    public function testHarvestDataIsValidWhenHarvestMethodIsMissing()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_EARS"
            ],
            "numeric_variables_field" => [
                "noOfEar"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SELFING" => [
                    "Bulk" => [ "" ],
                    "Individual Ear" => [ "NO_OF_EARS" ],
                ]    
            ],
            "CROSS_METHOD_SELFING" => [
                "not_fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                            ,
                            [
                                "abbrev" => "<none>",
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => null,
            "terminalHarvestDate" => '2020-07-24',
            "harvestMethod" => null,
            "terminalHarvestMethod" => null
        ];

        // expected
        $expected = [
            "harvestStatus" => "NO_HARVEST",
            "harvestRemarks" => '<span class="new badge red darken-3" style="margin-bottom:3px"><strong>MISSING VALUE</strong></span><br><span class="new badge white red-text text-darken-3" style="margin-bottom:3px"><strong>HARVEST METHOD</strong></span>',
            "harvestDataValid" => false,
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestDate" => null,
            "terminalHarvestDate" => '2020-07-24',
            "harvestMethod" => null,
            "terminalHarvestMethod" => null
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 5
     * Required numeric variable for committed harvest method is not set
     */
    public function testHarvestDataIsValidWhenRequiredNumVarForCommitedHMethodIsMissing()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_EARS"
            ],
            "numeric_variables_field" => [
                "noOfEar"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SELFING" => [
                    "Bulk" => [ "" ],
                    "Individual Ear" => [ "NO_OF_EARS" ],
                ]    
            ],
            "CROSS_METHOD_SELFING" => [
                "not_fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                            ,
                            [
                                "abbrev" => "<none>",
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => "Individual Ear",
            "terminalHarvestMethod" => null,
            "noOfEar" => null,
            "terminalNoOfEar" => null
        ];

        // expected
        $expected = [
            "harvestStatus" => "NO_HARVEST",
            "harvestRemarks" => '<span title="Ready to commit" class="new badge light-blue darken-3"><strong>TO COMMIT</strong></span>',
            "harvestDataValid" => true,
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => "Individual Ear",
            "terminalHarvestMethod" => null,
            "noOfEar" => null,
            "terminalNoOfEar" => null
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 6
     * Required numeric variable for terminal harvest method is not set
     */
    public function testHarvestDataIsValidWhenRequiredNumVarForTerminalHMethodIsMissing()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_EARS"
            ],
            "numeric_variables_field" => [
                "noOfEar"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SELFING" => [
                    "Bulk" => [ "" ],
                    "Individual Ear" => [ "NO_OF_EARS" ],
                ]    
            ],
            "CROSS_METHOD_SELFING" => [
                "not_fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                            ,
                            [
                                "abbrev" => "<none>",
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => null,
            "terminalHarvestDate" => "2020-12-11",
            "harvestMethod" => null,
            "terminalHarvestMethod" => "Individual Ear",
            "noOfEar" => null,
            "terminalNoOfEar" => null
        ];

        // expected
        $expected = [
            "harvestStatus" => "NO_HARVEST",
            "harvestRemarks" => '<span title="Ready to commit" class="new badge light-blue darken-3"><strong>TO COMMIT</strong></span>',
            "harvestDataValid" => true,
            "crossMethodAbbrev" => "CROSS_METHOD_SELFING",
            "state" => "not_fixed",
            "harvestDate" => null,
            "terminalHarvestDate" => "2020-12-11",
            "harvestMethod" => null,
            "terminalHarvestMethod" => "Individual Ear",
            "noOfEar" => null,
            "terminalNoOfEar" => null
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 7
     * Additional required variable is missing (eg Crossing date)
     */
    public function testHarvestDataIsValidWhenAdditionalRequiredVariableIsMissing()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_SEED"
            ],
            "numeric_variables_field" => [
                "noOfSeed"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SINGLE_CROSS" => [
                    "Bulk" => [ "NO_OF_SEED" ],
                    "Individual seed numbering" => [ "NO_OF_SEED" ],
                ]    
            ],
            "CROSS_METHOD_SINGLE_CROSS" => [
                "default" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                            ,
                            [
                                "abbrev" => "<none>",
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "additional_required_variables" => [
                            [
                                "abbrev" => "DATE_CROSSED",
                                "field_name" => "crossingDate"
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "crossingDate" => null,
            "state" => null,
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => null,
            "terminalHarvestMethod" => "Individual seed numbering",
            "noOfSeed" => null,
            "terminalNoOfSeed" => 20
        ];

        // expected
        $expected = [
            "harvestStatus" => "NO_HARVEST",
            "harvestRemarks" => '<span class="new badge red darken-3" style="margin-bottom:3px"><strong>MISSING VALUE</strong></span><br> <span class="new badge white red-text text-darken-3" style="margin-bottom:3px"><strong>CROSSING DATE</strong></span>',
            "harvestDataValid" => false,
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "crossingDate" => null,
            "state" => null,
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => null,
            "terminalHarvestMethod" => "Individual seed numbering",
            "noOfSeed" => null,
            "terminalNoOfSeed" => 20
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 8
     * All required information is valid and complete
     */
    public function testHarvestDataIsValidWhenAllRequiredInformationIsValidAndComplete()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_SEED"
            ],
            "numeric_variables_field" => [
                "noOfSeed"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SINGLE_CROSS" => [
                    "Bulk" => [ "NO_OF_SEED" ],
                    "Individual seed numbering" => [ "NO_OF_SEED" ],
                ]    
            ],
            "CROSS_METHOD_SINGLE_CROSS" => [
                "default" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                            ,
                            [
                                "abbrev" => "<none>",
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "additional_required_variables" => [
                            [
                                "abbrev" => "DATE_CROSSED",
                                "field_name" => "crossingDate"
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "crossingDate" => "2020-07-24",
            "state" => null,
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => null,
            "terminalHarvestMethod" => "Individual seed numbering",
            "noOfSeed" => null,
            "terminalNoOfSeed" => 20
        ];

        // expected
        $expected = [
            "harvestStatus" => "NO_HARVEST",
            "harvestRemarks" => '<span title="Ready to commit" class="new badge light-blue darken-3"><strong>TO COMMIT</strong></span>',
            "harvestDataValid" => true,
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "crossingDate" => "2020-07-24",
            "state" => null,
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => null,
            "terminalHarvestMethod" => "Individual seed numbering",
            "noOfSeed" => null,
            "terminalNoOfSeed" => 20
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 9
     * Record's harvest status is QUEUED_FOR_HARVEST
     */
    public function testHarvestDataIsValidWhenHarvestStatusIsQueuedForHarvest()
    {
        // ARRANGE
        // inputs
        $browserConfig = [];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "harvestStatus" => "QUEUED_FOR_HARVEST"
        ];

        // expected
        $expected = [
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "harvestStatus" => "QUEUED_FOR_HARVEST",
            'harvestRemarks' => '<span title="Queued for harvest" class="new badge grey lighten-2 grey-text text-darken-3"><strong>QUEUED FOR HARVEST</strong></span>',
            "harvestDataValid" => true
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 10
     * Record's harvest status is HARVEST_IN_PROGRESS
     */
    public function testHarvestDataIsValidWhenHarvestStatusIsHarvestInProgress()
    {
        // ARRANGE
        // inputs
        $browserConfig = [];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "harvestStatus" => "HARVEST_IN_PROGRESS"
        ];

        // expected
        $expected = [
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "harvestStatus" => "HARVEST_IN_PROGRESS",
            'harvestRemarks' => '<span title="Harvest in progress" class="new badge orange darken-3"><strong>HARVEST IN PROGRESS</strong></span>',
            "harvestDataValid" => true
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 11
     * Cross method is not supported
     */
    public function testHarvestDataIsInvalidWhenCrossMethodIsNotSupported()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_SEED"
            ],
            "numeric_variables_field" => [
                "noOfSeed"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SINGLE_CROSS" => [
                    "Bulk" => [ "NO_OF_SEED" ],
                    "Individual seed numbering" => [ "NO_OF_SEED" ],
                ]    
            ],
            "CROSS_METHOD_SINGLE_CROSS" => [
                "default" => [
                    "input_columns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => true
                        ]
                        ,
                        [
                            "abbrev" => "<none>",
                            "column_name" => "numericVar",
                            "required" => null
                        ]
                    ],
                    "additional_required_variables" => [
                        [
                            "abbrev" => "DATE_CROSSED",
                            "field_name" => "crossingDate"
                        ]
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_DOUBLE_CROSS",
            "crossingDate" => "2020-07-24",
            "state" => null,
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => null,
            "terminalHarvestMethod" => "Individual seed numbering",
            "noOfSeed" => null,
            "terminalNoOfSeed" => 20
        ];

        // expected
        $expected = [
            "harvestStatus" => "NO_HARVEST",
            "harvestRemarks" => '<span class="new badge red darken-3"><strong>UNSUPPORTED CROSS METHOD</strong></span>',
            "harvestDataValid" => false,
            "crossMethodAbbrev" => "CROSS_METHOD_DOUBLE_CROSS",
            "crossingDate" => "2020-07-24",
            "state" => null,
            "harvestStatus" => "NO_HARVEST",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => null,
            "terminalHarvestMethod" => "Individual seed numbering",
            "noOfSeed" => null,
            "terminalNoOfSeed" => 20
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 12
     * Harvest status is CONFLICT
     */
    public function testHarvestDataIsInvalidWhenHarvestStatusIsConflict()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_SEED"
            ],
            "numeric_variables_field" => [
                "noOfSeed"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SINGLE_CROSS" => [
                    "Bulk" => [ "NO_OF_SEED" ],
                    "Individual seed numbering" => [ "NO_OF_SEED" ],
                ]    
            ],
            "CROSS_METHOD_SINGLE_CROSS" => [
                "default" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                            ,
                            [
                                "abbrev" => "<none>",
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "additional_required_variables" => [
                            [
                                "abbrev" => "DATE_CROSSED",
                                "field_name" => "crossingDate"
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "crossingDate" => "2020-07-24",
            "state" => null,
            "harvestStatus" => "CONFLICT: Some conflict",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => "Single Seed Numbering",
            "terminalHarvestMethod" => null,
            "noOfSeed" => 20,
            "terminalNoOfSeed" => null
        ];

        // expected
        $expected = [
            "harvestStatus" => "CONFLICT: Some conflict",
            "harvestRemarks" => '<span class="new badge red darken-3"><strong>CONFLICT: SOME CONFLICT</strong></span>',
            "harvestDataValid" => false,
            "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
            "crossingDate" => "2020-07-24",
            "state" => null,
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => "Single Seed Numbering",
            "terminalHarvestMethod" => null,
            "noOfSeed" => 20,
            "terminalNoOfSeed" => null
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * harvestDataIsValid: Test 13
     * CrossMethod is missing
     */
    public function testHarvestDataIsInvalidWhenCrossMethodIsMissing()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "numeric_variables" => [
                "NO_OF_SEED"
            ],
            "numeric_variables_field" => [
                "noOfSeed"
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SINGLE_CROSS" => [
                    "Bulk" => [ "NO_OF_SEED" ],
                    "Individual seed numbering" => [ "NO_OF_SEED" ],
                ]    
            ],
            "CROSS_METHOD_SINGLE_CROSS" => [
                "default" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                            ,
                            [
                                "abbrev" => "<none>",
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "additional_required_variables" => [
                            [
                                "abbrev" => "DATE_CROSSED",
                                "field_name" => "crossingDate"
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $record = [
            "crossMethodAbbrev" => "",
            "crossingDate" => "2020-07-24",
            "state" => null,
            "harvestStatus" => "READY",
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => "Single Seed Numbering",
            "terminalHarvestMethod" => null,
            "noOfSeed" => 20,
            "terminalNoOfSeed" => null
        ];

        // expected
        $expected = [
            "harvestStatus" => "READY",
            "harvestRemarks" => '<span class="new badge red darken-3"><strong>MISSING CROSS METHOD</strong></span>',
            "harvestDataValid" => false,
            "crossMethodAbbrev" => "",
            "crossingDate" => "2020-07-24",
            "state" => null,
            "harvestDate" => "2020-12-11",
            "terminalHarvestDate" => null,
            "harvestMethod" => "Single Seed Numbering",
            "terminalHarvestMethod" => null,
            "noOfSeed" => 20,
            "terminalNoOfSeed" => null
        ];

        // ACT
        $actual = $this->creationModel->harvestDataIsValid($browserConfig, $record);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSummaryDataProvider: Test 1
     * Only plots will be harvested.
     * Plot count of ready for harvest is zero (0)
     */
    public function testGetSummaryDataProviderTest1() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $harvestCross = false;

        // mocks
        $returnValue = [
            "count" => 0,
            "data" => []
        ];
        $this->creationModel->plotBrowserModel->setReturnValue('getPlotRecords', $returnValue);

        // expected
        $summaryData = [];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $summaryData,
            'key' => 'summaryItemId',
            'restified' => true,
            'totalCount' => count($summaryData)
        ]);
        $dataProvider->id = null;
        $expected = [
            "dataProvider" => $dataProvider,
            "readyPlots" => 0,
            "readyCrosses" => 0
        ];

        // ACT
        $actual = $this->creationModel->getSummaryDataProvider($occurrenceId, $harvestCross);
        $actual['dataProvider']->id = null;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSummaryDataProvider: Test 2
     * Only plots will be harvested.
     * Plot count of ready for harvest is NOT zero (0)
     */
    public function testGetSummaryDataProviderTest2() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $harvestCross = false;

        // mocks
        $returnValue = [
            "count" => 2,
            "data" => [
                [
                    "plotDbId" => 111
                ],
                [
                    "plotDbId" => 112
                ]
            ]
        ];
        $this->creationModel->plotBrowserModel->setReturnValue('getPlotRecords', $returnValue);

        // expected
        $summaryData = [
            [
                "summaryItemId" => 0,
                "dataLevel" => "Plot",
                "plural" => "Plots",
                "count" => 2
            ]
        ];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $summaryData,
            'key' => 'summaryItemId',
            'restified' => true,
            'totalCount' => count($summaryData)
        ]);
        $dataProvider->id = null;
        $expected = [
            "dataProvider" => $dataProvider,
            "readyPlots" => 2,
            "readyCrosses" => 0
        ];

        // ACT
        $actual = $this->creationModel->getSummaryDataProvider($occurrenceId, $harvestCross);
        $actual['dataProvider']->id = null;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSummaryDataProvider: Test 3
     * Plots and crosses will be harvested.
     * Plot count and cross count of ready for harvest are both zero (0)
     */
    public function testGetSummaryDataProviderTest3() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $harvestCross = true;

        // mocks
        $returnValue = [
            "count" => 0,
            "data" => []
        ];
        $this->creationModel->plotBrowserModel->setReturnValue('getPlotRecords', $returnValue);

        $returnValue = [
            "count" => 0,
            "data" => []
        ];
        $this->creationModel->crossBrowserModel->setReturnValue('getCrossRecords', $returnValue);

        // expected
        $summaryData = [];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $summaryData,
            'key' => 'summaryItemId',
            'restified' => true,
            'totalCount' => count($summaryData)
        ]);
        $dataProvider->id = null;
        $expected = [
            "dataProvider" => $dataProvider,
            "readyPlots" => 0,
            "readyCrosses" => 0
        ];

        // ACT
        $actual = $this->creationModel->getSummaryDataProvider($occurrenceId, $harvestCross);
        $actual['dataProvider']->id = null;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSummaryDataProvider: Test 4
     * Plots and crosses will be harvested.
     * Plot count of ready for harvest is zero (0)
     */
    public function testGetSummaryDataProviderTest4() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $harvestCross = true;

        // mocks
        $returnValue = [
            "count" => 0,
            "data" => []
        ];
        $this->creationModel->plotBrowserModel->setReturnValue('getPlotRecords', $returnValue);

        $returnValue = [
            "count" => 3,
            "data" => [
                [
                    "crossDbId" => 123
                ],
                [
                    "crossDbId" => 124
                ],
                [
                    "crossDbId" => 125
                ]
            ]
        ];
        $this->creationModel->crossBrowserModel->setReturnValue('getCrossRecords', $returnValue);

        // expected
        $summaryData = [
            [
                "summaryItemId" => 1,
                "dataLevel" => "Cross",
                "plural" => "Crosses",
                "count" => 3
            ]
        ];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $summaryData,
            'key' => 'summaryItemId',
            'restified' => true,
            'totalCount' => count($summaryData)
        ]);
        $dataProvider->id = null;
        $expected = [
            "dataProvider" => $dataProvider,
            "readyPlots" => 0,
            "readyCrosses" => 3
        ];

        // ACT
        $actual = $this->creationModel->getSummaryDataProvider($occurrenceId, $harvestCross);
        $actual['dataProvider']->id = null;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSummaryDataProvider: Test 5
     * Plots and crosses will be harvested.
     * Cross count of ready for harvest is zero (0)
     */
    public function testGetSummaryDataProviderTest5() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $harvestCross = true;

        // mocks
        $returnValue = [
            "count" => 2,
            "data" => [
                [
                    "plotDbId" => 111
                ],
                [
                    "plotDbId" => 112
                ]
            ]
        ];
        $this->creationModel->plotBrowserModel->setReturnValue('getPlotRecords', $returnValue);

        $returnValue = [
            "count" => 0,
            "data" => []
        ];
        $this->creationModel->crossBrowserModel->setReturnValue('getCrossRecords', $returnValue);

        // expected
        $summaryData = [
            [
                "summaryItemId" => 0,
                "dataLevel" => "Plot",
                "plural" => "Plots",
                "count" => 2
            ]
        ];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $summaryData,
            'key' => 'summaryItemId',
            'restified' => true,
            'totalCount' => count($summaryData)
        ]);
        $dataProvider->id = null;
        $expected = [
            "dataProvider" => $dataProvider,
            "readyPlots" => 2,
            "readyCrosses" => 0
        ];

        // ACT
        $actual = $this->creationModel->getSummaryDataProvider($occurrenceId, $harvestCross);
        $actual['dataProvider']->id = null;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSummaryDataProvider: Test 6
     * Plots and crosses will be harvested.
     * Plot count and cross count of ready for harvest are both NOT zero (0)
     */
    public function testGetSummaryDataProviderTest6() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $harvestCross = true;

        // mocks
        $returnValue = [
            "count" => 2,
            "data" => [
                [
                    "plotDbId" => 111
                ],
                [
                    "plotDbId" => 112
                ]
            ]
        ];
        $this->creationModel->plotBrowserModel->setReturnValue('getPlotRecords', $returnValue);

        $returnValue = [
            "count" => 3,
            "data" => [
                [
                    "crossDbId" => 123
                ],
                [
                    "crossDbId" => 124
                ],
                [
                    "crossDbId" => 125
                ]
            ]
        ];
        $this->creationModel->crossBrowserModel->setReturnValue('getCrossRecords', $returnValue);

        // expected
        $summaryData = [
            [
                "summaryItemId" => 0,
                "dataLevel" => "Plot",
                "plural" => "Plots",
                "count" => 2
            ],
            [
                "summaryItemId" => 1,
                "dataLevel" => "Cross",
                "plural" => "Crosses",
                "count" => 3
            ]
        ];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $summaryData,
            'key' => 'summaryItemId',
            'restified' => true,
            'totalCount' => count($summaryData)
        ]);
        $dataProvider->id = null;
        $expected = [
            "dataProvider" => $dataProvider,
            "readyPlots" => 2,
            "readyCrosses" => 3
        ];

        // ACT
        $actual = $this->creationModel->getSummaryDataProvider($occurrenceId, $harvestCross);
        $actual['dataProvider']->id = null;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * commitHarvestData: Test 1
     * No harvest data is ready for commit
     */
    public function testCommitHarvestDataNoDataset() {
        // ARRANGE
        // input
        $occurrenceId = 1234;

        // mocks
        $this->creationModel->userModel->setReturnValue('getUserId', 1);
        $this->creationModel->occurrenceDetailsModel->setReturnValue('getLocationIdOfOccurrence', $occurrenceId);

        // Retrieve transaction ids ready for commit
        $returnValue['data'][0] = ['transactionDbId'  => 27];
        $this->creationModel->terminaltransaction->setReturnValue('searchAll', $returnValue);
        $this->creationModel->terminaltransaction->setReturnValue('searchAllDatasets', ['totalCount'  => 0]);

        // Commit harvest data
        $this->creationModel->transaction->setReturnValue('commitPlotData', ['success' =>  true]);
        $this->creationModel->terminaltransaction->setReturnValue('updateOne', ['success' =>  true]);

        // expected
        $expected = array ('response' => ['success' => false], "hasDataset" => false);

        // ACT
        $actual = $this->creationModel->commitHarvestData($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * commitHarvestData: Test 2
     * Harvest data will be committed
     */
    public function testCommitHarvestDataWithDataset() {
        // ARRANGE
        // input
        $occurrenceId = 1234;

        // mocks
        $this->creationModel->userModel->setReturnValue('getUserId', 1);
        $this->creationModel->occurrenceDetailsModel->setReturnValue('getLocationIdOfOccurrence', $occurrenceId);

        // Retrieve transaction ids ready for commit
        $returnValue['data'][0] = ['transactionDbId'  => 27];
        $this->creationModel->terminaltransaction->setReturnValue('searchAll', $returnValue);
        $this->creationModel->terminaltransaction->setReturnValue('searchAllDatasets', ['totalCount'  => 1]);

        // Commit harvest data
        $this->creationModel->transaction->setReturnValue('commitPlotData', ['success' =>  true]);
        $this->creationModel->terminaltransaction->setReturnValue('updateOne', ['success' =>  true]);

        // expected
        $expected = array ('response' => ['success' => true], "hasDataset" => true);

        // ACT
        $actual = $this->creationModel->commitHarvestData($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}