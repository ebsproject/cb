<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

use Codeception\Lib\Generator\Actor;

class HarvestManagerModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $harvestManagerModel;
    
    protected function _before()
    {
        $this->harvestManagerModel = \Yii::$container->get('harvestManagerModel');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * validateUrlParameters: Test 1
     * All required parameters are missing
     */
    public function testValidateUrlParametersWhenAllRequiredParametersAreMissing()
    {
        // ARRANGE
        // inputs
        $parameters = [
            "occurrenceId" => null,
            "source" => null
        ];
        $url = '/index.php/harvestManager/creation/index?program=BW';

        // expected
        $expected = [
            'error' => true,
            'errorMessage' => \Yii::t('app', 'Required URL parameters missing: occurrenceId, source')
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlParameters($parameters,$url);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * validateUrlParameters: Test 2
     * Some required parameter is missing
     */
    public function testValidateUrlParametersWhenSomeRequiredParameterIsMissing()
    {
        // ARRANGE
        // inputs
        $parameters = [
            "occurrenceId" => "1234",
            "source" => null
        ];
        $url = '/index.php/harvestManager/creation/index?program=BW&occurrenceId=1234';

        // expected
        $expected = [
            'error' => true,
            'errorMessage' => \Yii::t('app', 'Required URL parameter missing: source')
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlParameters($parameters, $url);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * validateUrlParameters: Test 3
     * Source parameter is invalid
     */
    public function testValidateUrlParametersWhenSourceParameterIsInvalid()
    {
        // ARRANGE
        // inputs
        $parameters = [
            "occurrenceId" => "1234",
            "source" => "entries"
        ];
        $url = '/index.php/harvestManager/creation/index?program=BW&occurrenceId=1234&source=entries';

        // expected
        $expected = [
            'error' => true,
            'errorMessage' => \Yii::t('app', 'Invalid parameter: Source string must be "plots" or "crosses".')
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlParameters($parameters, $url);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * validateUrlParameters: Test 4
     * Occurrence ID is not an integer
     */
    public function testValidateUrlParametersWhenOccurrenceIdIsNotAnInteger()
    {
        // ARRANGE
        // inputs
        $parameters = [
            "occurrenceId" => "1a2b3c"
        ];
        $url = '/index.php/harvestManager/creation/index?program=BW&occurrenceId=1a2b3c&source=plot';

        // expected
        $expected = [
            'error' => true,
            'errorMessage' => \Yii::t('app', 'Invalid parameter: OccurrenceId must be an integer.')
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlParameters($parameters, $url);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * validateUrlParameters: Test 5
     * Occurrence does not exist
     */
    public function testValidateUrlParametersWhenOccurrenceDoesNotExist()
    {
        // ARRANGE
        // inputs
        $parameters = [
            "occurrenceId" => "1234"
        ];
        $url = '/index.php/harvestManager/creation/index?program=BW&occurrenceId=1234&source=plot';

        // mocks
        $returnValue = [
            "data" => []
        ];
        $this->harvestManagerModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            'error' => false,
            'errorMessage' => \Yii::t('app', 'No errors found.')
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlParameters($parameters, $url);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * validateUrlParameters: Test 6
     * All parameters are valid
     */
    public function testValidateUrlParametersWhenAllParametersAreValid()
    {
        // ARRANGE
        // inputs
        $parameters = [
            "occurrenceId" => "1234",
            "source" => "plots"
        ];
        $url = '/index.php/harvestManager/creation/index?program=BW&occurrenceId=1234&source=plot';

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrenceDbId" => 1234
                ]
            ]
        ];
        $this->harvestManagerModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            'error' => false,
            'errorMessage' => \Yii::t('app', 'No errors found.')
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlParameters($parameters, $url);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * validateUrlParameters: Test 7
     * Pjax request
     */
    public function testValidateUrlParametersWhenRequestIsPjax()
    {
        // ARRANGE
        // inputs
        $parameters = [
            "occurrenceId" => null,
            "source" => null
        ];
        $url = '/index.php/harvestManager/creation/index?_pjax=%23dynagrid-harvest-manager-plot-crt-grid-pjax';

        // expected
        $expected = [
            'error' => false,
            'errorMessage' => \Yii::t('app', 'No errors found.')
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlParameters($parameters, $url);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getProgram: Test 1
     * Program exists
     */
    public function testGetProgramTest1()
    {
        // ARRANGE
        // inputs
        $programId = 123;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "programDbId" => 123,
                    "programCode" => "MYPROG"
                ]
            ]
        ];
        $this->harvestManagerModel->program->setReturnValue('searchAll', $returnValue);
        
        // expected
        $expected = "MYPROG";

        // ACT
        $actual = $this->harvestManagerModel->getProgram($programId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getProgram: Test 2
     * Program does not exist
     */
    public function testGetProgramTest2()
    {
        // ARRANGE
        // inputs
        $programId = 123;

        // mocks
        $returnValue = [
            "data" => []
        ];
        $this->harvestManagerModel->program->setReturnValue('searchAll', $returnValue);

        // ACT
        $actual = $this->harvestManagerModel->getProgram($programId);

        // ASSERT
        $this->assertNull($actual);
    }
    
    /**
     * validateUrlProgram: Test 1
     * Program is not set
     */
    public function testValidateUrlProgramTest1()
    {
        // ARRANGE
        // inputs
        $program = null;
        $occurrenceId = null;

        // mocks
        $returnValue = [
            "program_id" => 101
        ];
        $this->harvestManagerModel->dashboard->setReturnValue('getFilters', $returnValue);
        $returnValue = [
            "data" => [
                [
                    "programDbId" => 101,
                    "programCode" => "IRSEA"
                ]
            ]
        ];
        $this->harvestManagerModel->program->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            "redirect" => true,
            "program" => "IRSEA"
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlProgram($program, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * validateUrlProgram: Test 2
     * Program is set but does not match the occurrence's program
     */
    public function testValidateUrlProgramTest2()
    {
        // ARRANGE
        // inputs
        $program = "IRSEA";
        $occurrenceId = "12345";

        // mocks
        $returnValue = [
            "program_id" => 101
        ];
        $this->harvestManagerModel->dashboard->setReturnValue('getFilters', $returnValue);
        $returnValue = [
            "data" => [
                [
                    "programDbId" => 101,
                    "programCode" => "IRSEA"
                ]
            ]
        ];
        $this->harvestManagerModel->program->setReturnValue('searchAll', $returnValue);
        $returnValue = [
            "data" => [
                [
                    "occurrenceDbId" => 12345,
                    "programCode" => "BW"
                ]
            ]
        ];
        $this->harvestManagerModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            "redirect" => true,
            "program" => "BW"
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlProgram($program, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * validateUrlProgram: Test 3
     * Program is set and matches the occurrence's program
     */
    public function testValidateUrlProgramTest3()
    {
        // ARRANGE
        // inputs
        $program = "IRSEA";
        $occurrenceId = "4444";

        // mocks
        $returnValue = [
            "program_id" => 101
        ];
        $this->harvestManagerModel->dashboard->setReturnValue('getFilters', $returnValue);
        $returnValue = [
            "data" => [
                [
                    "programDbId" => 101,
                    "programCode" => "IRSEA"
                ]
            ]
        ];
        $this->harvestManagerModel->program->setReturnValue('searchAll', $returnValue);
        $returnValue = [
            "data" => [
                [
                    "occurrenceDbId" => 4444,
                    "programCode" => "IRSEA"
                ]
            ]
        ];
        $this->harvestManagerModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            "redirect" => false,
            "program" => null
        ];

        // ACT
        $actual = $this->harvestManagerModel->validateUrlProgram($program, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableId: Test 1
     * Variable exists
     */
    public function testGetVariableIdTest1()
    {
        // ARRANGE
        // input
        $variableAbbrev = "HVDATE_CONT";

        // mocks
        $returnValue = [
            "data" => [
                [
                    "variableDbId" => 141,
                    "abbrev" => "HVDATE_CONT"
                ]
            ]
        ];
        $this->harvestManagerModel->variable->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = 141;

        // ACT
        $actual = $this->harvestManagerModel->getVariableId($variableAbbrev);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableId: Test 2
     * Variable does not exist
     */
    public function testGetVariableIdTest2()
    {
        // ARRANGE
        // input
        $variableAbbrev = "NON_EXISTENT_VAR";

        // mocks
        $returnValue = [
            "data" => []
        ];
        $this->harvestManagerModel->variable->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = 0;

        // ACT
        $actual = $this->harvestManagerModel->getVariableId($variableAbbrev);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getScaleValues: Test 1
     * No scale values for variable
     */
    public function testGetScaleValuesTest1()
    {
        // ARRANGE
        // input
        $variableAbbrev = "HVDATE_CONT";
        $scAbbrevMode = false;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "variableDbId" => 141,
                    "abbrev" => "HVDATE_CONT"
                ]
            ]
        ];
        $this->harvestManagerModel->variable->setReturnValue('searchAll', $returnValue);
        $returnValue = [
            "data" => []
        ];
        $this->harvestManagerModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        // expected
        $expected = [];

        // ACT
        $actual = $this->harvestManagerModel->getScaleValues($variableAbbrev, $scAbbrevMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getScaleValues: Test 2
     * With scale values, scAbbrev mode is false: [ value => displayName ]
     */
    public function testGetScaleValuesTest2()
    {
        // ARRANGE
        // input
        $variableAbbrev = "HV_METH_DISC";
        $scAbbrevMode = false;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "variableDbId" => 536,
                    "abbrev" => "HV_METH_DISC"
                ]
            ]
        ];
        $this->harvestManagerModel->variable->setReturnValue('searchAll', $returnValue);
        $returnValue = [
            [
                "scaleValues" => [
                    [
                        "abbrev" => "HV_METH_DISC_BULK",
                        "value" => "Bulk",
                        "displayName" => "Bulk"
                    ],
                    [
                        "abbrev" => "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                        "value" => "Single Plant Selection",
                        "displayName" => "Single Plant Selection"
                    ],
                    [
                        "abbrev" => "HV_METH_DISC_INDIVIDUAL_EAR",
                        "value" => "Individual Ear",
                        "displayName" => "Individual Ear"
                    ]
                ]
            ]
        ];
        $this->harvestManagerModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        // expected
        $expected = [
            "Bulk" => "Bulk",
            "Single Plant Selection" => "Single Plant Selection",
            "Individual Ear" => "Individual Ear"
        ];

        // ACT
        $actual = $this->harvestManagerModel->getScaleValues($variableAbbrev, $scAbbrevMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getScaleValues: Test 3
     * With scale values, scAbbrev mode is true: [ abbrev => [ value => displayName ] ]
     */
    public function testGetScaleValuesTest3()
    {
        // ARRANGE
        // input
        $variableAbbrev = "HV_METH_DISC";
        $scAbbrevMode = true;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "variableDbId" => 536,
                    "abbrev" => "HV_METH_DISC"
                ]
            ]
        ];
        $this->harvestManagerModel->variable->setReturnValue('searchAll', $returnValue);
        $returnValue = [
            [
                "scaleValues" => [
                    [
                        "abbrev" => "HV_METH_DISC_BULK",
                        "value" => "Bulk",
                        "displayName" => "Bulk"
                    ],
                    [
                        "abbrev" => "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                        "value" => "Single Plant Selection",
                        "displayName" => "Single Plant Selection"
                    ],
                    [
                        "abbrev" => "HV_METH_DISC_INDIVIDUAL_EAR",
                        "value" => "Individual Ear",
                        "displayName" => "Individual Ear"
                    ]
                ]
            ]
        ];
        $this->harvestManagerModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        // expected
        $expected = [
            "HV_METH_DISC_BULK" => [ "Bulk" => "Bulk" ],
            "HV_METH_DISC_SINGLE_PLANT_SELECTION" => [ "Single Plant Selection" => "Single Plant Selection" ],
            "HV_METH_DISC_INDIVIDUAL_EAR" => [ "Individual Ear" => "Individual Ear" ]
        ];

        // ACT
        $actual = $this->harvestManagerModel->getScaleValues($variableAbbrev, $scAbbrevMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkCPN: Test 1
     * Experiment does not exist
     */
    public function testCheckCPNTest1()
    {
        // ARRANGE
        // inputs
        $expermentId = 99;

        // mocks
        $returnValue = [
            "data" => []
        ];
        $this->harvestManagerModel->experiment->setReturnValue('getOne', $returnValue);

        // expected
        $expected = [
            "isCPN" => false,
            "crossed" => false,
            "phase" => "",
            "title" => "",
            "class" => ""
        ];

        // ACT
        $actual = $this->harvestManagerModel->checkCPN($expermentId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkCPN: Test 2
     * Experiment exists, not CPN
     */
    public function testCheckCPNTest2()
    {
        // ARRANGE
        // inputs
        $expermentId = 99;

        // mocks
        $returnValue = [
            "data" => [
                "experimentType" => "Intentional Crossing Nursery",
                "experimentStatus" => "planted"
            ]
        ];
        $this->harvestManagerModel->experiment->setReturnValue('getOne', $returnValue);

        // expected
        $expected = [
            "isCPN" => false,
            "crossed" => false,
            "phase" => "",
            "title" => "",
            "class" => ""
        ];

        // ACT
        $actual = $this->harvestManagerModel->checkCPN($expermentId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkCPN: Test 3
     * Experiment exists, is CPN, planted but not crossed
     */
    public function testCheckCPNTest3()
    {
        // ARRANGE
        // inputs
        $expermentId = 99;

        // mocks
        $returnValue = [
            "data" => [
                "experimentType" => "Cross Parent Nursery",
                "experimentStatus" => "planted"
            ]
        ];
        $this->harvestManagerModel->experiment->setReturnValue('getOne', $returnValue);

        // expected
        $expected = [
            "isCPN" => true,
            "crossed" => false,
            "phase" => "I",
            "title" => "Experiment is not crossed yet",
            "class" => "hm-unavailable"
        ];

        // ACT
        $actual = $this->harvestManagerModel->checkCPN($expermentId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkCPN: Test 4
     * Experiment exists, is CPN, planted and crossed
     */
    public function testCheckCPNTest4()
    {
        // ARRANGE
        // inputs
        $expermentId = 99;

        // mocks
        $returnValue = [
            "data" => [
                "experimentType" => "Cross Parent Nursery",
                "experimentStatus" => "planted;crossed"
            ]
        ];
        $this->harvestManagerModel->experiment->setReturnValue('getOne', $returnValue);

        // expected
        $expected = [
            "isCPN" => true,
            "crossed" => true,
            "phase" => "II",
            "title" => "View crosses",
            "class" => null
        ];

        // ACT
        $actual = $this->harvestManagerModel->checkCPN($expermentId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getPaginationKey: Test 1
     * "page" key exists
     */
    public function testGetPaginationKeyTest1()
    {
        // ARRANGE
        // input
        $queryParams = [
            "page" => 2
        ];

        // expected
        $expected = "page";

        // ACT
        $actual = $this->harvestManagerModel->getPaginationKey($queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getPaginationKey: Test 2
     * "dp-N-page" key exists
     */
    public function testGetPaginationKeyTest2()
    {
        // ARRANGE
        // input
        $queryParams = [
            "dp-3-page" => 2
        ];

        // expected
        $expected = "dp-3-page";

        // ACT
        $actual = $this->harvestManagerModel->getPaginationKey($queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getPaginationKey: Test 3
     * page key not existent
     */
    public function testGetPaginationKeyTest3()
    {
        // ARRANGE
        // input
        $queryParams = [];

        // expected
        $expected = "";

        // ACT
        $actual = $this->harvestManagerModel->getPaginationKey($queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * browserReset: Test 1
     * forceReset = true
     */
    public function testBrowserResetWhenForceResetIsTrue()
    {
        // ARRANGE
        // inputs
        $uniqueIdentifier = "1234";
        $queryParams = [
            "SampleBrowserModel" => []
        ];
        $searchModelName = "SampleBrowserModel";

        // Create mock
        $postParams = [
            "forceReset" => true
        ];
        $request = $this->make('yii\web\Request',
            [
                'post' => function () use ($postParams) {
                    return $postParams;
                }
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // expected
        $expected = [
            "resetPage" => true,
            "resetFilters" => true,
        ];

        // ACT
        $actual = $this->harvestManagerModel->browserReset($uniqueIdentifier, $queryParams, $searchModelName);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * browserReset: Test 2
     * bulkOperation = true
     */
    public function testBrowserResetWhenBulkOperationIsTrue()
    {
        // ARRANGE
        // inputs
        $uniqueIdentifier = "1234";
        $queryParams = [
            "SampleBrowserModel" => []
        ];
        $searchModelName = "SampleBrowserModel";

        // Create mock
        $postParams = [
            "forceReset" => false,
            "bulkOperation" => true
        ];
        $request = $this->make('yii\web\Request',
            [
                'post' => function () use ($postParams) {
                    return $postParams;
                }
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // expected
        $expected = [
            "resetPage" => false,
            "resetFilters" => false,
        ];

        // ACT
        $actual = $this->harvestManagerModel->browserReset($uniqueIdentifier, $queryParams, $searchModelName);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * browserReset: Test 3
     * No changes detected in current filters
     */
    public function testBrowserResetWhenNoChangesDetectedInFilters()
    {
        // ARRANGE
        // inputs
        $uniqueIdentifier = "1234";
        $queryParams = [
            "SampleBrowserModel" => [
                "attributeA" => "valueX"
            ]
        ];
        $searchModelName = "SampleBrowserModel";

        // Create mock
        $postParams = [];
        $request = $this->make('yii\web\Request',
            [
                'post' => function () use ($postParams) {
                    return $postParams;
                }
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock session variable
        $previousParams = [
            "attributeA" => "valueX"
        ];

        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) use ($previousParams) {
                return $previousParams;
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        // expected
        $expected = [
            "resetPage" => false,
            "resetFilters" => false,
        ];
        $expectedSession = $queryParams[$searchModelName];

        // ACT
        $actual = $this->harvestManagerModel->browserReset($uniqueIdentifier, $queryParams, $searchModelName);
        $actualSession = \Yii::$app->cbSession->get("$searchModelName-$uniqueIdentifier");

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedSession, $actualSession);
    }

    /**
     * browserReset: Test 4
     * Changes detected in current filters
     */
    public function testBrowserResetWhenChangesDetectedInFilters()
    {
        // ARRANGE
        // inputs
        $uniqueIdentifier = "1234";
        $queryParams = [
            "SampleBrowserModel" => [
                "attributeA" => "hello",
                "attributeB" => 123,
                "attributeC" => true
            ]
        ];
        $searchModelName = "SampleBrowserModel";

        // Create mock
        $postParams = [];
        $request = $this->make('yii\web\Request',
            [
                'post' => function () use ($postParams) {
                    return $postParams;
                }
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock session variable
        $previousParams = [
            "attributeA" => "hello",
            "attributeB" => 444,
            "attributeC" => false
        ];
        
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) use ($previousParams) {
                return $previousParams;
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        // expected
        $expected = [
            "resetPage" => true,
            "resetFilters" => false,
        ];
        $expectedSession = $queryParams[$searchModelName];

        // ACT
        $actual = $this->harvestManagerModel->browserReset($uniqueIdentifier, $queryParams, $searchModelName);
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) {
                return [
                    "attributeA" => "hello",
                    "attributeB" => 123,
                    "attributeC" => true
                ];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        $actualSession = \Yii::$app->cbSession->get("$searchModelName-$uniqueIdentifier");

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedSession, $actualSession);
    }

    /**
     * assembleUrlParameters: Test 1
     * Default sort applied, page not reset
     */
    public function testAssembleUrlParametersTest1()
    {
        // ARRANGE
        // inputs
        $gridId = 'sample-grid';
        $defaultSortString = 'sort=attributeA:ASC';
        $resetPage = false;

        // mocks
        $gridPageSize = 15;
        $gridCurrentPage = 4;
        $queryParams = [
            "page" => $gridCurrentPage
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "$gridId-pjax",
                '_queryParams' => $queryParams
            ]
        );
        
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->harvestManagerModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $expected = [
            "limit=" . $gridPageSize,
            "page=" . $gridCurrentPage,
            $defaultSortString,
        ];

        // ACT
        $actual = $this->harvestManagerModel->assembleUrlParameters($gridId, $defaultSortString, $resetPage);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 2
     * Sort specified, page not reset
     */
    public function testAssembleUrlParametersTest2()
    {
        // ARRANGE
        // inputs
        $gridId = 'sample-grid';
        $defaultSortString = 'sort=attributeA:ASC';
        $resetPage = false;

        // mocks
        $gridPageSize = 15;
        $gridCurrentPage = 4;
        $gridSort = 'attributeB|-attributeC';
        $queryParams = [
            "dp-1-page" => $gridCurrentPage,
            "sort" => $gridSort
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "$gridId-pjax",
                '_queryParams' => $queryParams
            ]
        );
        
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->harvestManagerModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $expected = [
            "limit=" . $gridPageSize,
            "page=" . $gridCurrentPage,
            "sort=attributeB:ASC|attributeC:DESC"
        ];

        // ACT
        $actual = $this->harvestManagerModel->assembleUrlParameters($gridId, $defaultSortString, $resetPage);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 3
     * Sort specified, page reset (page)
     */
    public function testAssembleUrlParametersTest3()
    {
        // ARRANGE
        // inputs
        $gridId = 'sample-grid';
        $defaultSortString = 'sort=attributeA:ASC';
        $resetPage = true;

        // mocks
        $gridPageSize = 15;
        $gridCurrentPage = 4;
        $gridSort = 'attributeB|-attributeC';   
        $queryParams = [
            "page" => $gridCurrentPage,
            "sort" => $gridSort
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "$gridId-pjax",
                '_queryParams' => $queryParams
            ]
        );
        
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->harvestManagerModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $expected = [
            "limit=" . $gridPageSize,
            "page=1",
            "sort=attributeB:ASC|attributeC:DESC"
        ];

        // ACT
        $actual = $this->harvestManagerModel->assembleUrlParameters($gridId, $defaultSortString, $resetPage);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 4
     * Sort specified, page reset (dp-1-page)
     */
    public function testAssembleUrlParametersTest4()
    {
        // ARRANGE
        // inputs
        $gridId = 'sample-grid';
        $defaultSortString = 'sort=attributeA:ASC';
        $resetPage = true;

        // mocks
        $gridPageSize = 15;
        $gridCurrentPage = 4;
        $gridSort = 'attributeB|-attributeC';       
        $queryParams = [
            "dp-1-page" => $gridCurrentPage,
            "sort" => $gridSort
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "$gridId-pjax",
                '_queryParams' => $queryParams
            ]
        );
        
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->harvestManagerModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $expected = [
            "limit=" . $gridPageSize,
            "page=1",
            "sort=attributeB:ASC|attributeC:DESC"
        ];
        $expectedPage = 1;

        // ACT
        $actual = $this->harvestManagerModel->assembleUrlParameters($gridId, $defaultSortString, $resetPage);
        $actualQueryParams = \Yii::$app->request->getQueryParams();
        $actualPageValue = $actualQueryParams['dp-1-page'];

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedPage, $actualPageValue);
    }

    /**
     * assembleUrlParameters: Test 5
     * No cookies
     */
    public function testAssembleUrlParametersTest5()
    {
        // ARRANGE
        // inputs
        $gridId = 'sample-grid';
        $defaultSortString = 'sort=attributeA:ASC';
        $resetPage = false;

        // mocks
        $gridPageSize = 15;
        $gridSort = 'attributeB|-attributeC';
        $queryParams = [
            "sort" => $gridSort
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "$gridId-pjax",
                '_queryParams' => $queryParams
            ]
        );
        
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->harvestManagerModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $expected = [
            "limit=15",
            "sort=attributeB:ASC|attributeC:DESC"
        ];

        // ACT
        $actual = $this->harvestManagerModel->assembleUrlParameters($gridId, $defaultSortString, $resetPage);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 6
     * URL does not contain grid id
     */
    public function testAssembleUrlParametersTest6()
    {
        // ARRANGE
        // inputs
        $gridId = 'sample-grid';
        $defaultSortString = 'sort=attributeA:ASC';
        $resetPage = false;

        // mocks
        $queryParams = [];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "xxxx",
                '_queryParams' => $queryParams
            ]
        );
        
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->harvestManagerModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 10);

        $expected = [
            "limit=10",
            $defaultSortString
        ];

        // ACT
        $actual = $this->harvestManagerModel->assembleUrlParameters($gridId, $defaultSortString, $resetPage);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * deleteInBackground
     */
    public function testDeleteInBackground() {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $deletionMode = 'harvest data';
        $parameters = [
            'param1' => 'value1'
        ];

        // mocks
        $this->harvestManagerModel->worker->setReturnValue('invoke', [
            "status" => "success"
        ]);

        // expected
        $expected = [
            "status" => "success"
        ];

        // ACT
        $actual = $this->harvestManagerModel->deleteInBackground($occurrenceId, $deletionMode, $parameters);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}