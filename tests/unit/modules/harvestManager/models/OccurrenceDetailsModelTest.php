<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

class OccurrenceDetailsModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $occurrenceDetailsModel;
    
    protected function _before()
    {
        $this->occurrenceDetailsModel = \Yii::$container->get('occurrenceDetailsModel');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * getDisplayName: Test 1
     * Field name has corresponding abbrev
     */
    public function testGetDisplayNameTest1()
    {
        // ARRANGE
        // inputs
        $fieldName = 'experimentType';

        // mocks
        $returnValue = [
            'abbrev' => 'EXPERIMENT_TYPE',
            'display_name' => 'Experiment Type',
        ];
        $this->occurrenceDetailsModel->variable->setReturnValue('getVariableByAbbrev', $returnValue);

        // expected
        $expected = 'Experiment Type';

        // ACT
        $actual = $this->occurrenceDetailsModel->getDisplayName($fieldName);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDisplayName: Test 2
     * Substring of field name has corresponding abbrev
     */
    public function testGetDisplayNameTest2()
    {
        // ARRANGE
        // inputs
        $fieldName = 'experimentName';

        // mocks
        $returnValue = [
            "return_vals" => [
                [],
                [
                    'abbrev' => 'EXPERIMENT',
                    'display_name' => 'Experiment',
                ]
            ]
        ];
        $this->occurrenceDetailsModel->variable->setReturnValue('getVariableByAbbrev', $returnValue);

        // expected
        $expected = 'Experiment';

        // ACT
        $actual = $this->occurrenceDetailsModel->getDisplayName($fieldName);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDisplayName: Test 3
     * Field name or substring does not have a corresponding abbrev
     */
    public function testGetDisplayNameTest3()
    {
        // ARRANGE
        // inputs
        $fieldName = 'stageCode';

        // mocks
        $returnValue = [
            "return_vals" => [
                [],
                [],
                [],
                []
            ]
        ];
        $this->occurrenceDetailsModel->variable->setReturnValue('getVariableByAbbrev', $returnValue);

        // expected
        $expected = 'Stage';

        // ACT
        $actual = $this->occurrenceDetailsModel->getDisplayName($fieldName);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getLocationIdOfOccurrence: Test 1
     * Occurrence exists
     */
    public function testGetLocationIdOfOccurrenceTest1() {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;

        // mocks
        $returnValue = [
            'data' => [
                [
                    "locationDbId" => 5678
                ]
            ]
        ];
        $this->occurrenceDetailsModel->locationOccurrenceGroup->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = 5678;

        // ACT
        $actual = $this->occurrenceDetailsModel->getLocationIdOfOccurrence($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getLocationIdOfOccurrence: Test 2
     * Occurrence does not exist
     */
    public function testGetLocationIdOfOccurrenceTest2() {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;

        // mocks
        $returnValue = [
            'data' => []
        ];
        $this->occurrenceDetailsModel->locationOccurrenceGroup->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = 0;

        // ACT
        $actual = $this->occurrenceDetailsModel->getLocationIdOfOccurrence($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getExperimentIdOfOccurrence: Test 1
     * Occurrence exists
     */
    public function testGetExperimentIdOfOccurrenceTest1() {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;

        // mocks
        $returnValue = [
            'data' => [
                [
                    'experimentDbId' => 8888
                ]
            ]
        ];
        $this->occurrenceDetailsModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = 8888;

        // ACT
        $actual = $this->occurrenceDetailsModel->getExperimentIdOfOccurrence($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getExperimentIdOfOccurrence: Test 2
     * Occurrence does not exist
     */
    public function testGetExperimentIdOfOccurrenceTest2() {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;

        // mocks
        $returnValue = [
            'data' => []
        ];
        $this->occurrenceDetailsModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = 0;

        // ACT
        $actual = $this->occurrenceDetailsModel->getExperimentIdOfOccurrence($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
    
    /**
     * getHarvestSummary: Test 1
     * Harvest summary is available
     */
    public function testGetHarvestSummaryWithData() {
        // ARRANGE
        // input
        $occurrenceId = 4001;
        $experimentId = 101;

        // mocks
        $returnValue = [
            'data' => [
                [
                    'status' => 'this exists'
                ]
            ]
        ];
        $this->occurrenceDetailsModel->apiHelper->setReturnValue('getParsedResponse', $returnValue);

        // expected
        $expected = ['status' => 'this exists'];

        // ACT
        $actual = $this->occurrenceDetailsModel->getHarvestSummary($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getHarvestSummary: Test 2
     * Harvest summary is not available
     */
    public function testGetHarvestSummaryWithoutData() {
        // ARRANGE
        // input
        $occurrenceId = 4001;
        $experimentId = 101;

        // mocks
        $returnValue = [
            'data' => []
        ];
        $this->occurrenceDetailsModel->apiHelper->setReturnValue('getParsedResponse', $returnValue);

        // expected
        $expected = [];

        // ACT
        $actual = $this->occurrenceDetailsModel->getHarvestSummary($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOccurrenceDetails: Test 1
     * Occurrence details is available, with plot level harvest summary
     * for Breeding Trial, Observation, Generation Nursery, and CPN Phase I
     */
    public function testGetOccurrenceDetailsTest1() {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;

        // mocks
        // Set return value for occurrence->searchAll()
        $occurrenceSearchAllRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            'experimentDbId' => 8888
                        ]
                    ]
                ],
                [
                    'data' => [
                        [
                            'occurrenceNumber' => '3',
                            'occurrenceName' => 'EXP0001_OCC3',
                            'site' => 'IRRIHQ'
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->occurrence->setReturnValue('searchAll', $occurrenceSearchAllRetVal);

        // Set return value for locationOccurrenceGroup->searchAll()
        $locationOccurrenceGroupSearchAllRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            'locationDbId' => 8888
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->locationOccurrenceGroup->setReturnValue('searchAll', $locationOccurrenceGroupSearchAllRetVal);

        // Set return value for variable->getVariableByAbbrev()
        $variableGetVariableByAbbrevRetVal = [
            "return_vals" => [
                [
                    'display_name' => 'Experiment Type'
                ],
                [
                    'display_name' => 'Experiment Year'
                ],
                [
                    'display_name' => 'Stage'
                ],
                [
                    'display_name' => 'Season'
                ],
                [
                    'display_name' => 'Location Name'
                ],
                [
                    'display_name' => 'Location Code'
                ],
                [
                    'display_name' => 'Experiment Name'
                ],
                [
                    'display_name' => 'Occurrence Name'
                ],
                [
                    'display_name' => 'Occurrence Number'
                ],
                [
                    'display_name' => 'Site'
                ],
                [
                    'display_name' => 'Steward'
                ]
            ]
        ];
        $this->occurrenceDetailsModel->variable->setReturnValue('getVariableByAbbrev', $variableGetVariableByAbbrevRetVal);

        // Set return value for experiment->searchAll()
        $experimentSearchAllRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            "experimentType" => "Breeding Trial",
                            "experimentYear" => "2021",
                            "stageCode" => "AYT",
                            "seasonName" => "Wet",
                            "experimentName" => "EXP0001"
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->experiment->setReturnValue('searchAll', $experimentSearchAllRetVal);

        // Set return value for location->searchAll()
        $locationSearchAllRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            "locationName" => "IRRIHQ_LOC001",
                            "locationCode" => "LOCATION00000001",
                            "steward" => "Irri, Bims"
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->location->setReturnValue('searchAll', $locationSearchAllRetVal);

        // Set return value for harvestManagerModel->checkCPN()
        $harvestManagerModelCheckCPNRetVal = [
            "return_vals" => [
                [
                    "isCPN" => false,
                    "crossed" => false,
                    "phase" => "",
                    "title" => "",
                    "class" => ""
                ]
            ]
        ];
        $this->occurrenceDetailsModel->harvestManagerModel->setReturnValue('checkCPN', $harvestManagerModelCheckCPNRetVal);

        // Set return value for apiHelper->getParsedresponse()
        $apiHelperGetParsedResponseRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            "totalEntryCount" => 10,
                            "totalRepCount" => 1,
                            "totalPlotCount" => 10,
                            "totalCrossCount" => 0,
                            "harvestedPlots" => 5,
                            "harvestedCrosses" => 0,
                            "totalPackagesFromPlots" => 100,
                            "totalSeedsFromPlots" => 100,
                            "harvestedtCrosses" => 0,
                            "totalSeedsFromCrosses" => 0,
                            "totalPackagesFromCrosses" => 0
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->apiHelper->setReturnValue('getParsedResponse', $apiHelperGetParsedResponseRetVal);

        // expected
        $expected = [
            "occurrenceDetailsTab1" => [
                "Experiment Type" => "Breeding Trial",
                "Experiment Year" => "2021",
                "Stage" => "AYT",
                "Location Name" => "IRRIHQ_LOC001",
                "Location Code" => "LOCATION00000001",
                "Season" => "Wet",
                "Occurrence Number" => "3",
                "Site" => "IRRIHQ",
                "Steward" => "Irri, Bims",
            ],
            "occurrenceDetailsOther" => [
                "Experiment Name" => "EXP0001",
                "Occurrence Name" => "EXP0001_OCC3",
                "Location Name" => "IRRIHQ_LOC001",
                "Stage" => "AYT",
                "Season" => "Wet",
                "Site" => "IRRIHQ"
            ],
            "harvestSummaryDetails" => [
                "No. of entries" => 10,
                "No. of rep" => 1,
                "No. of packages (Cross/Plot)" => "0/100",
                "Harvested plots" => "5/10",
                "Harvested crosses" => "0/0"
            ]
        ];

        // ACT
        $actual = $this->occurrenceDetailsModel->getOccurrenceDetails($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOccurrenceDetails: Test 2
     * Occurrence details is available, with plot and cross level harvest summary
     * for ICN and CPN Phase II
     */
    public function testGetOccurrenceDetailsTest2() {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;

        // mocks
        // Set return value for occurrence->searchAll()
        $occurrenceSearchAllRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            'experimentDbId' => 8888
                        ]
                    ]
                ],
                [
                    'data' => [
                        [
                            'occurrenceNumber' => '1',
                            'occurrenceName' => 'EXP0001_OCC1',
                            'site' => 'IRRIHQ'
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->occurrence->setReturnValue('searchAll', $occurrenceSearchAllRetVal);

        // Set return value for locationOccurrenceGroup->searchAll()
        $locationOccurrenceGroupSearchAllRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            'locationDbId' => 8888
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->locationOccurrenceGroup->setReturnValue('searchAll', $locationOccurrenceGroupSearchAllRetVal);

        // Set return value for variable->getVariableByAbbrev()
        $variableGetVariableByAbbrevRetVal = [
            "return_vals" => [
                [
                    'display_name' => 'Experiment Type'
                ],
                [
                    'display_name' => 'Experiment Year'
                ],
                [
                    'display_name' => 'Stage'
                ],
                [
                    'display_name' => 'Season'
                ],
                [
                    'display_name' => 'Location Name'
                ],
                [
                    'display_name' => 'Location Code'
                ],
                [
                    'display_name' => 'Experiment Name'
                ],
                [
                    'display_name' => 'Occurrence Name'
                ],
                [
                    'display_name' => 'Occurrence Number'
                ],
                [
                    'display_name' => 'Site'
                ],
                [
                    'display_name' => 'Steward'
                ]
            ]
        ];
        $this->occurrenceDetailsModel->variable->setReturnValue('getVariableByAbbrev', $variableGetVariableByAbbrevRetVal);

        // Set return value for experiment->searchAll()
        $experimentSearchAllRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            "experimentType" => "Cross Parent Nursery",
                            "experimentYear" => "2021",
                            "stageCode" => "AYT",
                            "seasonName" => "Wet",
                            "experimentName" => "EXP0001"
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->experiment->setReturnValue('searchAll', $experimentSearchAllRetVal);

        // Set return value for location->searchAll()
        $locationSearchAllRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            "locationName" => "IRRIHQ_LOC001",
                            "locationCode" => "LOCATION00000001",
                            "steward" => "Irri, Bims"
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->location->setReturnValue('searchAll', $locationSearchAllRetVal);

        // Set return value for harvestManagerModel->checkCPN()
        $harvestManagerModelCheckCPNRetVal = [
            "return_vals" => [
                [
                    "isCPN" => true,
                    "crossed" => true,
                    "phase" => "II",
                    "title" => "View crosses",
                    "class" => null
                ]
            ]
        ];
        $this->occurrenceDetailsModel->harvestManagerModel->setReturnValue('checkCPN', $harvestManagerModelCheckCPNRetVal);

        // Set return value for apiHelper->getParsedresponse()
        $apiHelperGetParsedResponseRetVal = [
            "return_vals" => [
                [
                    'data' => [
                        [
                            "totalEntryCount" => 10,
                            "totalRepCount" => 1,
                            "totalPlotCount" => 10,
                            "totalCrossCount" => 25,
                            "harvestedPlots" => 8,
                            "totalSeedsFromPlots" => 80,
                            "totalPackagesFromPlots" => 80,
                            "harvestedCrosses" => 12,
                            "totalSeedsFromCrosses" => 345,
                            "totalPackagesFromCrosses" => 345
                        ]
                    ]
                ]
            ]
        ];
        $this->occurrenceDetailsModel->apiHelper->setReturnValue('getParsedResponse', $apiHelperGetParsedResponseRetVal);

        // expected
        $expected = [
            "occurrenceDetailsTab1" => [
                "Experiment Type" => "Cross Parent Nursery (Phase II)",
                "Experiment Year" => "2021",
                "Stage" => "AYT",
                "Location Name" => "IRRIHQ_LOC001",
                "Location Code" => "LOCATION00000001",
                "Season" => "Wet",
                "Occurrence Number" => "1",
                "Site" => "IRRIHQ",
                "Steward" => "Irri, Bims",
            ],
            "occurrenceDetailsOther" => [
                "Experiment Name" => "EXP0001",
                "Occurrence Name" => "EXP0001_OCC1",
                "Location Name" => "IRRIHQ_LOC001",
                "Stage" => "AYT",
                "Season" => "Wet",
                "Site" => "IRRIHQ"
            ],
            "harvestSummaryDetails" => [
                "No. of entries" => 10,
                "No. of rep" => 1,
                "No. of packages (Cross/Plot)" => "345/80",
                "Harvested crosses" => "12/25",
                "Harvested plots" => "8/10"
            ]
        ];

        // ACT
        $actual = $this->occurrenceDetailsModel->getOccurrenceDetails($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}