<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

class ApiHelperTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $apiHelper;
    
    protected function _before()
    {
        $this->apiHelper = \Yii::$container->get('apiHelper');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * sendRequest: Test 1
     * Result is not 200 OK
     */
    public function testSendRequestTest1()
    {
        // ARRANGE
        $method = 'POST';
        $endpoint = 'seeds-search';
        $requestBody = [
            "seedDbId" => "1001"
        ];
        $queryParams = [
            "sort=seedDbId:ASC",
            "limit=10"
        ];
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 400
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [];

        // ACTUAL
        $actual = $this->apiHelper->sendRequest($method, $endpoint, $requestBody, $queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * sendRequest: Test 2
     * Result is 200 OK, single page
     */
    public function testSendRequestTest2()
    {
        // ARRANGE
        $method = 'POST';
        $endpoint = 'seeds-search';
        $requestBody = [
            "seedDbId" => "1001"
        ];
        $queryParams = [
            "sort=seedDbId:ASC",
            "limit=10"
        ];
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        "seedDbId" => 1001
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            [
                "seedDbId" => 1001
            ]
        ];

        // ACTUAL
        $actual = $this->apiHelper->sendRequest($method, $endpoint, $requestBody, $queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * sendRequest: Test 3
     * Result is 200 OK, multi page
     */
    public function testSendRequestTest3()
    {
        // ARRANGE
        $method = 'POST';
        $endpoint = 'seeds-search';
        $requestBody = [
            "seedDbId" => "1001"
        ];
        $queryParams = [
            "sort=seedDbId:ASC",
            "limit=10"
        ];
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 5
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        "seedDbId" => 1001
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            [
                "seedDbId" => 1001
            ],
            [
                "seedDbId" => 1001
            ],
            [
                "seedDbId" => 1001
            ],
            [
                "seedDbId" => 1001
            ],
            [
                "seedDbId" => 1001
            ]
        ];

        // ACTUAL
        $actual = $this->apiHelper->sendRequest($method, $endpoint, $requestBody, $queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * sendRequestSinglePage: Test 1
     * Result is 200 OK
     */
    public function testSendRequestSinglePageSuccessful()
    {
        // ARRANGE
        $method = 'POST';
        $endpoint = 'seeds-search';
        $requestBody = [
            "seedDbId" => "1001"
        ];
        $queryParams = [
            "sort=seedDbId:ASC",
            "limit=10"
        ];
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 5,
                                    'totalCount' => 1
                                ],
                                'status' => [[
                                    'message' => 'Request has been successfully completed.',
                                    'messageType' => 'INFO',
                                ]]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        "seedDbId" => 1001
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            "status" => 200,
            "data" => [
                [
                    "seedDbId" => 1001
                ]
            ],
            "count" => 1,
            "message" => 'Request has been successfully completed.',
        ];

        // ACTUAL
        $actual = $this->apiHelper->sendRequestSinglePage($method, $endpoint, $requestBody, $queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * sendRequestSinglePage: Test 2
     * Result is not 200 OK
     */
    public function testSendRequestSinglePageFailed()
    {
        // ARRANGE
        $method = 'POST';
        $endpoint = 'seeds-search';
        $requestBody = [
            "seedDbId" => "1001"
        ];
        $queryParams = [
            "sort=seedDbId:ASC",
            "limit=10"
        ];
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 400,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 0,
                                    'totalCount' => 0
                                ],
                                'status' => [[
                                    'message' => 'Error occurred.',
                                    'messageType' => 'Internal',
                                ]]
                            ],
                            'result' => [
                                'data' => []
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            "status" => 400,
            "data" => [],
            "count" => 0,
            "message" => 'Error occurred.'
        ];

        // ACTUAL
        $actual = $this->apiHelper->sendRequestSinglePage($method, $endpoint, $requestBody, $queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * sendRequestSinglePage: Test 3
     * Request body = null
     */
    public function testSendRequestSinglePageWithNullBody()
    {
        // ARRANGE
        $method = 'POST';
        $endpoint = 'seeds-search';
        $requestBody = null;
        $queryParams = [
            "sort=seedDbId:ASC",
            "limit=10"
        ];
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 400,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 0,
                                    'totalCount' => 0
                                ],
                                'status' => [[
                                    'message' => 'Bad Request.',
                                    'messageType' => 'Invalid content',
                                ]]
                            ],
                            'result' => [
                                'data' => []
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            "status" => 400,
            "data" => [],
            "count" => 0,
            "message" => 'Bad Request.'
        ];

        // ACTUAL
        $actual = $this->apiHelper->sendRequestSinglePage($method, $endpoint, $requestBody, $queryParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getParsedResponse: Test 4
     * Request body = null
     */
    public function testGetParsedResponseTest4()
    {
        // ARRANGE
        $method = 'POST';
        $path = 'seeds-search';
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getParsedResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $expected = [
            'status' => 200
        ];

        // ACTUAL
        $actual = $this->apiHelper->getParsedResponse($method, $path);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * processorBuilder: Test 1
     * Method is PUT
     */
    public function testProcessorBuilderWhenMethodIsPut() {
        // ARRANGE
        // input
        $httpMethod = 'PUT';
        $endpoint = 'plots';
        $dbIds = ["1001", "1002", "1003"];
        $description = "Update plots";
        $entity = "LOCATION";
        $entityDbId = 100;
        $endpointEntity = "PLOT";
        $application = "HARVEST_MANAGER";
        $optional = [
            'requestData' => [
                'harvestStatus' => 'NO_HARVEST'
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        "backgroundJobId" => 100001
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        // expected
        $expected = [
            [
                "backgroundJobId" => 100001
            ]
        ];

        // ACT
        $actual = $this->apiHelper->processorBuilder($httpMethod, $endpoint, $dbIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * processorBuilder: Test 2
     * Method is DELETE
     */
    public function testProcessorBuilderWhenMethodIsDelete() {
        // ARRANGE
        // input
        $httpMethod = 'DELETE';
        $endpoint = 'seeds';
        $dbIds = ["1001", "1002", "1003"];
        $description = "Delete seeds";
        $entity = "LOCATION";
        $entityDbId = 100;
        $endpointEntity = "SEED";
        $application = "HARVEST_MANAGER";
        $optional = [
            'dependents' => [
                'packages',
                'seed-relations'
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    [
                                        "backgroundJobId" => 100001
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        // expected
        $expected = [
            [
                "backgroundJobId" => 100001
            ]
        ];

        // ACT
        $actual = $this->apiHelper->processorBuilder($httpMethod, $endpoint, $dbIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}