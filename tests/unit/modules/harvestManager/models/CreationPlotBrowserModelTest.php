<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

// Import data providers
use app\dataproviders\ArrayDataProvider;

class CreationPlotBrowserModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $creationPlotBrowserModel;
    
    protected function _before()
    {
        $this->creationPlotBrowserModel = \Yii::$container->get('creationPlotBrowserModel');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * search: Test 1
     * loadData = false
     */
    public function testSearchTest1()
    {
        // ARRANGE
        // inputs
        $params = [];
        $experimentId = 11;
        $occurrenceId = 22;
        $locationId = 33;
        $browserConfig = [];
        $loadData = false;
        $hidden = false;

        // expected
        $expected = array (
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [],
                'key' => 'plotDbId',
                'restified' => true,
                'totalCount' => 0
            ]),
            'germplasmStates' => []
        );
        $expected["dataProvider"]->id = null;
        $expected["dataProvider"]->sort = false;

        // ACT
        $actual = $this->creationPlotBrowserModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 2
     * hidden = true
     */
    public function testSearchTest2()
    {
        // ARRANGE
        // inputs
        $params = [];
        $experimentId = 11;
        $occurrenceId = 22;
        $locationId = 33;
        $browserConfig = [];
        $loadData = true;
        $hidden = true;

        // expected
        $expected = array (
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [],
                'key' => 'plotDbId',
                'restified' => true,
                'totalCount' => 0
            ]),
            'germplasmStates' => []
        );
        $expected["dataProvider"]->id = null;
        $expected["dataProvider"]->sort = false;

        // ACT
        $actual = $this->creationPlotBrowserModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 3
     * loadData = false, hidden = true
     */
    public function testSearchTest3()
    {
        // ARRANGE
        // inputs
        $params = [];
        $experimentId = 11;
        $occurrenceId = 22;
        $locationId = 33;
        $browserConfig = [];
        $loadData = false;
        $hidden = true;

        // expected
        $expected = array (
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [],
                'key' => 'plotDbId',
                'restified' => true,
                'totalCount' => 0
            ]),
            'germplasmStates' => []
        );
        $expected["dataProvider"]->id = null;
        $expected["dataProvider"]->sort = false;

        // ACT
        $actual = $this->creationPlotBrowserModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 4
     * loadData = true, hidden = false
     */
    public function testSearchTest4()
    {
        // ARRANGE
        // inputs
        $params = [];
        $experimentId = 11;
        $occurrenceId = 22;
        $locationId = 33;
        $browserConfig = [];
        $loadData = true;
        $hidden = false;

        // mocks
        $plots = [
            [
                "plotDbId" => 1001,
                "harvestStatus" => "READY"
            ],
            [
                "plotDbId" => 1002,
                "harvestStatus" => "READY"
            ],
            [
                "plotDbId" => 1003,
                "harvestStatus" => "READY"
            ]
        ];
        $totalCount = 3;

        $returnValue = [
            "resetPage" => false,
            "resetFilters" => true
        ];
        $this->creationPlotBrowserModel->harvestManagerModel->setReturnValue('browserReset', $returnValue);
        $returnValue = [];
        $this->creationPlotBrowserModel->harvestManagerModel->setReturnValue('assembleUrlParameters', $returnValue);

        $this->creationPlotBrowserModel->harvestManagerModel->setReturnValue('matchConfigs', $plots);

        $returnValue = [
            "count" => $totalCount,
            "data" => $plots
        ];
        $this->creationPlotBrowserModel->plotBrowserModel->setReturnValue('getPlotRecords', $returnValue);

        $this->creationPlotBrowserModel->creationModel->setReturnValue('generateBowserColumns', []);

        $validatedPlots = [
            [
                "plotDbId" => 1001,
                "harvestStatus" => "READY",
                "harvestDataValid" => true,
                "harvestRemarks" => '<span title="Ready for package, seed and germplasm creation" class="new badge light-green darken-3"><strong>' . \Yii::t('app', 'FOR CREATION') . '</strong></span>'
            ],
            [
                "plotDbId" => 1002,
                "harvestStatus" => "READY",
                "harvestDataValid" => true,
                "harvestRemarks" => '<span title="Ready for package, seed and germplasm creation" class="new badge light-green darken-3"><strong>' . \Yii::t('app', 'FOR CREATION') . '</strong></span>'
            ],
            [
                "plotDbId" => 1003,
                "harvestStatus" => "READY",
                "harvestDataValid" => true,
                "harvestRemarks" => '<span title="Ready for package, seed and germplasm creation" class="new badge light-green darken-3"><strong>' . \Yii::t('app', 'FOR CREATION') . '</strong></span>'
            ]
        ];
        $returnValue = [
            "return_vals" => $validatedPlots
        ];
        $this->creationPlotBrowserModel->creationModel->setReturnValue('harvestDataIsValid', $returnValue);
        $this->creationPlotBrowserModel->creationModel->setReturnValue('buildHarvestRemarksColumn', $returnValue);

        // expected
        $expected = array (
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $validatedPlots,
                'key' => 'plotDbId',
                'restified' => true,
                'totalCount' => $totalCount
            ]),
            'germplasmStates' => []
        );
        $expected["dataProvider"]->id = null;
        $expected["dataProvider"]->sort = false;

        // ACT
        $actual = $this->creationPlotBrowserModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
    
    /**
     * assembleBrowserFilters: Test 1
     */
    public function testAssembleBrowserFiltersTest1()
    {
        // ARRANGE
        // input
        $experimentId = 1234;

        $this->creationPlotBrowserModel->creationModel->setReturnValue('generateBowserColumns', []);

        // expected
        $expected = [
            'conditions' => [
                [
                    'experimentDbId' => "equals $experimentId",
                    'state' => 'not equals unknown',
                    'harvestStatus' => 'equals READY|equals QUEUED_FOR_HARVEST|equals HARVEST_IN_PROGRESS|equals BAD_QC_CODE|INCOMPLETE%|CONFLICT%'
                ],
                [
                    'experimentDbId' => "equals $experimentId",
                    'state' => 'not equals unknown',
                    'harvestStatus' => 'equals NO_HARVEST||INCOMPLETE%',
                    'terminalHarvestMethod' => '[OR] not null',
                    'harvestMethod' => '[OR] not null'
                ]
            ],
            'sort' => [
                "attribute" => "harvestStatus",
                "sortValue" => "DONE|HARVEST_IN_PROGRESS|QUEUED_FOR_HARVEST|READY|NO_HARVEST|INCOMPLETE%"
            ]
        ];

        // ACT
        $actual = $this->creationPlotBrowserModel->assembleBrowserFilters($experimentId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
    
    /**
     * rules: Test 1
     */
    public function testRulesTest1()
    {
        // ARRANGE
        // expected
        $expected = [
            [['designation', 'plotDbId', 'plotNumber', 'rep', 'state', 'type'], 'required'],
            [['plotDbId', 'plotNumber', 'entryNumber', 'rep'], 'number'],
            [['plotDbId', 'plotNumber', 'entryNumber', 'rep'], 'integer'],
            [['plotCode', 'harvestStatus', 'designation', 'parentage', 'state', 'type'], 'string'],
            [['designation', 'entryNumber', 'harvestStatus', 'parentage', 'plotCode', 'plotDbId', 'plotNumber', 'rep', 'state', 'type'], 'safe'],
            [[], 'boolean'],
            [['plotDbId'], 'unique']
        ];

        // ACT
        $actual = $this->creationPlotBrowserModel->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}