<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

// Import data providers
use app\dataproviders\ArrayDataProvider;

class PlotBrowserModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $plotBrowserModel;
    
    protected function _before()
    {
        $this->plotBrowserModel = \Yii::$container->get('plotBrowserModel');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * getPlotRecords: Test 1
     */
    public function testGetPlotRecordsTest1()
    {
        // ARRANGE
        // inputs
        $locationId = 1234;
        $requestBody = [
            "experimentDbId" => "111"
        ];
        $urlParams = [
            "limit=10"
        ];

        // mocks
        $plots = [
            [
                "plotDbId" => "123"
            ]
        ];
        $totalCount = 1;
        $returnValue = [
            "data" => $plots,
            "count" => $totalCount
        ];
        $this->plotBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', $returnValue);
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('trimValues', $returnValue['data']);

        // expected
        $expected = [
            "data" => $plots,
            "count" => $totalCount
        ];

        // ACT
        $actual = $this->plotBrowserModel->getPlotRecords($locationId, $requestBody, $urlParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * searchAllPlots: Test 1
     */
    public function testSearchAllPlotsTest1()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $requestBody = [
            "experimentDbId" => "111"
        ];
        $urlParams = [
            "limit=10"
        ];

        // mocks
        $returnValue = [["plotDbId" => "123"]];
        $this->plotBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('trimValues', [["plotDbId" => "123"]]);

        // expected
        $expected = [["plotDbId" => "123"]];

        // ACT
        $actual = $this->plotBrowserModel->searchAllPlots($occurrenceId, $requestBody, $urlParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDistinctField: Test 1
     */
    public function testGetDistinctFieldTest1()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 111;
        $field = 'state';

        // mocks
        $returnValue = [
            [
                "state" => "fixed"
            ],
            [
                "state" => "not_fixed"
            ]
        ];
        $this->plotBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('trimValues', $returnValue);

        // expected
        $expected = [ "fixed", "not_fixed" ];

        // ACT
        $actual = $this->plotBrowserModel->getDistinctField($occurrenceId, $field);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 1
     * Simple column filter (single value)
     */
    public function testAssembleBrowserFiltersWithSimpleSingleValueFilter()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "PlotBrowserModel" => [
                "attributeA" => "valueA",
                "attributeB" => ""
            ]
        ];

        // mocks
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "attributeA" => "equals valueA",
            "harvestStatus" => "equals NO_HARVEST",
            "state" => "equals fixed|equals not_fixed"
        ];

        // ACT
        $actual = $this->plotBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 2
     * Simple column filter (multiple value)
     */
    public function testAssembleBrowserFiltersWithSimpleMultiValueFilter()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "PlotBrowserModel" => [
                "attributeA" => "",
                "attributeB" => ["valueX","valueY","valueZ"]
            ]
        ];

        // mocks
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "attributeB" => "valueX|valueY|valueZ",
            'harvestStatus' => 'equals NO_HARVEST',
            'state' => 'equals fixed|equals not_fixed'
        ];

        // ACT
        $actual = $this->plotBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 3
     * Special case: harvest status = INCOMPLETE
     */
    public function testAssembleBrowserFiltersWithIncompleteStatus()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "PlotBrowserModel" => [
                "harvestStatus" => "INCOMPLETE"
            ]
        ];

        // mocks
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            'harvestStatus' => 'equals NO_HARVEST',
            'state' => 'equals fixed|equals not_fixed'

        ];

        // ACT
        $actual = $this->plotBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 4
     * Special case: harvest status = IN_PROGRESS
     */
    public function testAssembleBrowserFiltersWithInProgressStatus()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "PlotBrowserModel" => [
                "harvestStatus" => "IN_PROGRESS"
            ]
        ];

        // mocks
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            'harvestStatus' => 'equals NO_HARVEST',
            'state' => 'equals fixed|equals not_fixed'

        ];

        // ACT
        $actual = $this->plotBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 5
     * Special case: harvest status = INVALID_STATE, state not set
     */
    public function testAssembleBrowserFiltersWithInvalidStateAndStateNotSet()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "PlotBrowserModel" => [
                "state" => "",
                "harvestStatus" => "INVALID_STATE"
            ]
        ];

        // mocks
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            'harvestStatus' => 'equals NO_HARVEST',
            'state' => 'equals fixed|equals not_fixed'

        ];

        // ACT
        $actual = $this->plotBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 6
     * Special case: harvest status = INVALID_STATE, state is set
     */
    public function testAssembleBrowserFiltersWithInvalidStateAndStateIsSet()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "PlotBrowserModel" => [
                "state" => "fixed",
                "harvestStatus" => "INVALID_STATE"
            ]
        ];

        // mocks
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "harvestStatus" => "equals NO_HARVEST",
            "state" => "equals fixed"
        ];

        // ACT
        $actual = $this->plotBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 7
     * Special case: harvest status = NO_HARVEST
     */
    public function testAssembleBrowserFiltersWithNoHarvestStatus()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "PlotBrowserModel" => [
                "state" => "",
                "harvestStatus" => "NO_HARVEST"
            ]
        ];

        // mocks
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "harvestStatus" => "equals NO_HARVEST",
            "state" => "equals fixed|equals not_fixed"
        ];

        // ACT
        $actual = $this->plotBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 1
     * Special case: harvest status = INVALID_STATE, state is set
     */
    public function testSearchTest1()
    {
        // ARRANGE
        // inputs
        $params = [];
        $experimentId = 100;
        $occurrenceId = 200;
        $locationId = 300;

        // mocks
        $returnValue = [
            "resetPage" => false,
            "resetFilters" => true
        ];
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('browserReset', $returnValue);

        $returnValue = [];
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('assembleUrlParameters', $returnValue);

        $plots = [
            [
                "plotDbId" => 1001
            ],
            [
                "plotDbId" => 1002
            ],
            [
                "plotDbId" => 1003
            ],
            [
                "plotDbId" => 1004
            ],
            [
                "plotDbId" => 1005
            ]
        ];
        $totalCount = 100;
        $returnValue = [
            "count" => $totalCount,
            "data" => $plots
        ];
        $this->plotBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', $returnValue);
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('matchConfigs',
            [
                "return_vals" => [
                    [
                        [
                            "plotDbId" => 1001
                        ],
                        [
                            "plotDbId" => 1002
                        ],
                        [
                            "plotDbId" => 1003
                        ],
                        [
                            "plotDbId" => 1004
                        ],
                        [
                            "plotDbId" => 1005
                        ]
                    ]
                ]
            ]
        );
        $this->plotBrowserModel->harvestManagerModel->setReturnValue('trimValues', $returnValue['data']);

        // expected
        $dataProvider = new ArrayDataProvider([
            'allModels' => $plots,
            'key' => 'plotDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);
        $dataProvider->id = null;
        $dataProvider->sort = false;
        $expected = [
            "dataProvider" => $dataProvider,
            "germplasmStates" => []
        ];

        // ACT
        $actual = $this->plotBrowserModel->search($params, $experimentId, $occurrenceId, $locationId);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;
 
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * rules: Test 1
     */
    public function testRulesTest1()
    {
        // ARRANGE
        // expected
        $expected = [
            [['designation', 'plotDbId', 'plotNumber', 'rep', 'state', 'type'], 'required'],
            [['plotDbId', 'plotNumber', 'entryNumber', 'entryCode', 'rep'], 'number'],
            [['plotDbId', 'plotNumber', 'entryNumber', 'entryCode', 'rep'], 'integer'],
            [['plotCode', 'designation', 'parentage', 'state', 'type'], 'string'],
            [['designation', 'entryNumber', 'entryCode', 'parentage', 'plotCode', 'plotDbId', 'plotNumber', 'rep', 'state', 'type'], 'safe'],
            [[], 'boolean'],
            [['plotDbId'], 'unique']
        ];

        // ACT
        $actual = $this->plotBrowserModel->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}