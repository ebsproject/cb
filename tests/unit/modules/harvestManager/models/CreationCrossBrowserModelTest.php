<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

// Import data providers
use app\dataproviders\ArrayDataProvider;

class CreationCrossBrowserModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $creationCrossBrowserModel;
    
    protected function _before()
    {
        $this->creationCrossBrowserModel = \Yii::$container->get('creationCrossBrowserModel');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * search: Test 1
     * loadData = false
     */
    public function testSearchTest1()
    {
        // ARRANGE
        // inputs
        $params = [];
        $experimentId = 11;
        $occurrenceId = 22;
        $locationId = 33;
        $browserConfig = [];
        $loadData = false;
        $hidden = false;

        // expected
        $expected = array (
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [],
                'key' => 'crossDbId',
                'restified' => true,
                'totalCount' => 0
            ])
        );
        $expected["dataProvider"]->id = null;
        $expected["dataProvider"]->sort = false;

        // ACT
        $actual = $this->creationCrossBrowserModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 2
     * hidden = true
     */
    public function testSearchTest2()
    {
        // ARRANGE
        // inputs
        $params = [];
        $experimentId = 11;
        $occurrenceId = 22;
        $locationId = 33;
        $browserConfig = [];
        $loadData = true;
        $hidden = true;

        // expected
        $expected = array (
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [],
                'key' => 'crossDbId',
                'restified' => true,
                'totalCount' => 0
            ])
        );
        $expected["dataProvider"]->id = null;
        $expected["dataProvider"]->sort = false;

        // ACT
        $actual = $this->creationCrossBrowserModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 3
     * loadData = false, hidden = true
     */
    public function testSearchTest3()
    {
        // ARRANGE
        // inputs
        $params = [];
        $experimentId = 11;
        $occurrenceId = 22;
        $locationId = 33;
        $browserConfig = [];
        $loadData = false;
        $hidden = true;

        // expected
        $expected = array (
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [],
                'key' => 'crossDbId',
                'restified' => true,
                'totalCount' => 0
            ])
        );
        $expected["dataProvider"]->id = null;
        $expected["dataProvider"]->sort = false;

        // ACT
        $actual = $this->creationCrossBrowserModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 4
     * loadData = true, hidden = false
     */
    public function testSearchTest4()
    {
        // ARRANGE
        // inputs
        $params = [];
        $experimentId = 11;
        $occurrenceId = 22;
        $locationId = 33;
        $browserConfig = [];
        $loadData = true;
        $hidden = false;

        // mocks
        $crosses = [
            [
                "crossDbId" => 1001,
                "harvestStatus" => "READY"
            ],
            [
                "crossDbId" => 1002,
                "harvestStatus" => "READY"
            ],
            [
                "crossDbId" => 1003,
                "harvestStatus" => "READY"
            ]
        ];
        $totalCount = 3;

        $returnValue = [
            "resetPage" => false,
            "resetFilters" => true
        ];
        $this->creationCrossBrowserModel->harvestManagerModel->setReturnValue('browserReset', $returnValue);
        $returnValue = [];
        $this->creationCrossBrowserModel->harvestManagerModel->setReturnValue('assembleUrlParameters', $returnValue);

        $returnValue = [
            "count" => $totalCount,
            "data" => $crosses
        ];
        $this->creationCrossBrowserModel->crossBrowserModel->setReturnValue('getCrossRecords', $returnValue);

        $validatedCrosses = [
            [
                "crossDbId" => 1001,
                "harvestStatus" => "READY",
                "harvestDataValid" => true,
                "harvestRemarks" => '<span title="Ready for package, seed and germplasm creation" class="new badge light-green darken-3"><strong>' . \Yii::t('app', 'FOR CREATION') . '</strong></span>'
            ],
            [
                "crossDbId" => 1002,
                "harvestStatus" => "READY",
                "harvestDataValid" => true,
                "harvestRemarks" => '<span title="Ready for package, seed and germplasm creation" class="new badge light-green darken-3"><strong>' . \Yii::t('app', 'FOR CREATION') . '</strong></span>'
            ],
            [
                "crossDbId" => 1003,
                "harvestStatus" => "READY",
                "harvestDataValid" => true,
                "harvestRemarks" => '<span title="Ready for package, seed and germplasm creation" class="new badge light-green darken-3"><strong>' . \Yii::t('app', 'FOR CREATION') . '</strong></span>'
            ]
        ];
        $returnValue = [
            "return_vals" => $validatedCrosses
        ];
        $this->creationCrossBrowserModel->creationModel->setReturnValue('harvestDataIsValid', $returnValue);

        // expected
        $expected = array (
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $validatedCrosses,
                'key' => 'crossDbId',
                'restified' => true,
                'totalCount' => $totalCount
            ])
        );
        $expected["dataProvider"]->id = null;
        $expected["dataProvider"]->sort = false;

        // ACT
        $actual = $this->creationCrossBrowserModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 1
     */
    public function testAssembleBrowserFiltersTest1()
    {
        // ARRANGE
        // input
        $experimentId = 1234;

        // expected
        $expected = [
            'conditions' => [
                [
                    'experimentDbId' => "equals $experimentId",
                    'femaleGermplasmState' => 'not equals unknown',
                    'harvestStatus' => 'equals READY||equals QUEUED_FOR_HARVEST||equals BAD_QC_CODE||equals HARVEST_IN_PROGRESS'
                ],
                [
                    'experimentDbId' => "equals $experimentId",
                    'femaleGermplasmState' => 'not equals unknown',
                    'harvestStatus' => 'equals NO_HARVEST||INCOMPLETE%||CONFLICT%',
                    'terminalHarvestDate' => '[OR] not null',
                    'terminalHarvestMethod' => '[OR] not null',
                    'terminalNoOfPlant' => '[OR] not null',
                    'terminalNoOfPanicle' => '[OR] not null',
                    'terminalSpecificPlantNo' => '[OR] not null',
                    'terminalNoOfEar' => '[OR] not null',
                    'terminalNoOfSeed' => '[OR] not null',
                    'terminalGrainColor' => '[OR] not null',
                    'harvestDate' => '[OR] not null',
                    'harvestMethod' => '[OR] not null',
                    'noOfPlant' => '[OR] not null',
                    'noOfPanicle' => '[OR] not null',
                    'specificPlantNo' => '[OR] not null',
                    'noOfEar' => '[OR] not null',
                    'noOfSeed' => '[OR] not null',
                    'grainColor' => '[OR] not null'
                ]
            ],
            'sort' => [
                "attribute" => "harvestStatus",
                "sortValue" => "DONE|HARVEST_IN_PROGRESS|QUEUED_FOR_HARVEST|READY|NO_HARVEST|INCOMPLETE%"
            ]
        ];

        // ACT
        $actual = $this->creationCrossBrowserModel->assembleBrowserFilters($experimentId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
    
    /**
     * rules: Test 1
     */
    public function testRulesTest1()
    {
        // ARRANGE
        // expected
        $expected = [
            [['crossFemaleParent', 'crossMaleParent', 'crossName', 'crossDbId', 'femaleGermplasmState', 'maleGermplasmState'], 'required'],
            [['crossDbId'], 'number'],
            [['crossDbId'], 'integer'],
            [['crossFemaleParent', 'crossMaleParent', 'crossingDate', 'crossMethod', 'crossMethodAbbrev', 'crossName', 'femaleGermplasmState', 'femaleParentage', 'femaleParentSeedSource', 'femaleSourceSeedEntry', 'femaleSourcePlot', 'harvestStatus', 'maleGermplasmState', 'maleParentage', 'maleParentSeedSource', 'maleSourceSeedEntry', 'maleSourcePlot', 'remarks', 'femaleEntryCode', 'femaleEntryNumber', 'maleEntryCode', 'maleEntryNumber'], 'string'],
            [['crossDbId', 'crossFemaleParent', 'crossMaleParent', 'crossingDate', 'crossMethod', 'crossMethodAbbrev', 'crossName', 'femaleGermplasmState', 'femaleParentage', 'femaleParentSeedSource', 'femaleSourceSeedEntry', 'femaleSourcePlot', 'harvestStatus', 'maleGermplasmState', 'maleParentage', 'maleParentSeedSource', 'maleSourceSeedEntry', 'maleSourcePlot', 'remarks', 'femaleEntryCode', 'femaleEntryNumber', 'maleEntryCode', 'maleEntryNumber'], 'safe'],
            [[], 'boolean'],
            [[], 'unique']
        ];

        // ACT
        $actual = $this->creationCrossBrowserModel->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}