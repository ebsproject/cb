<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

// Import data providers
use app\dataproviders\ArrayDataProvider;

class CrossBrowserModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $crossBrowserModel;

    protected function _before()
    {
        $this->crossBrowserModel = \Yii::$container->get('crossBrowserModel');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * getCrossRecords: Test1
     */
    public function testGetCrossRecordsTest1()
    {
        // ARRANGE
        // inputs
        $locationId = 1234;
        $requestBody = [];
        $urlParams = [];

        // mocks
        $crosses = [
            [
                "crossDbId" => 6543
            ]
        ];
        $totalCount = 1;
        $returnValue = [
            "data" => $crosses,
            "count" => $totalCount
        ];
        $this->crossBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', $returnValue);

        $this->crossBrowserModel->harvestManagerModel->setReturnValue('trimValues', $returnValue['data']);

        // expected
        $expected = [
            "data" => $crosses,
            "count" => $totalCount
        ];

        // ACT
        $actual = $this->crossBrowserModel->getCrossRecords($locationId, $requestBody, $urlParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * searchAllCrosses: Test1
     */
    public function testSearchAllCrossesTest1()
    {
        // ARRANGE
        // inputs
        $locationId = 1234;
        $requestBody = [];
        $urlParams = [];

        // mocks
        $crosses = [
            [
                "crossDbId" => 6543
            ]
        ];
        $totalCount = 1;
        $returnValue = [
            "data" => $crosses,
            "count" => $totalCount
        ];
        $this->crossBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        $this->crossBrowserModel->harvestManagerModel->setReturnValue('trimValues', $returnValue);

        // expected
        $expected = $returnValue;

        // ACT
        $actual = $this->crossBrowserModel->searchAllCrosses($locationId, $requestBody, $urlParams);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDistinctField: Test1
     */
    public function testGetDistinctFieldTest1()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 101;
        $field = 'state';

        // mocks
        $returnValue = [
            [
                "crossDbId" => 6543,
                "state" => "fixed"
            ],
            [
                "crossDbId" => 7723,
                "state" => "not_fixed"
            ],
            [
                "crossDbId" => 8324,
                "state" => "unknown"
            ]
        ];
        $this->crossBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        $this->crossBrowserModel->harvestManagerModel->setReturnValue('trimValues', $returnValue);

        // expected
        $expected = [
            "fixed",
            "not_fixed",
            "unknown"
        ];

        // ACT
        $actual = $this->crossBrowserModel->getDistinctField($occurrenceId, $field);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 1
     * Simple filters, single value
     */
    public function testAssembleBrowserFiltersWithSimpleSingleValueFilter()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "CrossBrowserModel" => [
                "attributeA" => "value1",
                "attributeB" => "",
                "attributeC" => "value2"
            ]
        ];

        // mocks
        $this->crossBrowserModel->harvestManagerModel->setReturnValue(
            'getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "conditions" => [
                [
                    "attributeA" => "equals value1",
                    "attributeC" => "equals value2",
                    "harvestStatus" => "equals NO_HARVEST",
                    'crossMethodAbbrev' => 'equals CROSS_METHOD_SELFING',
                    'femaleGermplasmState' => 'equals fixed||equals not_fixed'
                ],
                [
                    'attributeA' => 'equals value1',
                    'attributeC' => 'equals value2',
                    'harvestStatus' => 'equals NO_HARVEST',
                    'crossMethodAbbrev' => 'not equals CROSS_METHOD_SELFING'

                ]
            ]
        ];

        // ACT
        $actual = $this->crossBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 2
     * Simple filters, multiple values
     */
    public function testAssembleBrowserFiltersWithSimpleMultiValueFilter()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "CrossBrowserModel" => [
                "attributeA" => ["value1", "value3", "value4"],
                "attributeB" => "",
                "attributeC" => "value2"
            ]
        ];

        // mocks
        $this->crossBrowserModel->harvestManagerModel->setReturnValue(
            'getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "conditions" => [
                [
                    'attributeA' => 'value1||value3||value4',
                    'attributeC' => 'equals value2',
                    'harvestStatus' => 'equals NO_HARVEST',
                    'crossMethodAbbrev' => 'equals CROSS_METHOD_SELFING',
                    'femaleGermplasmState' => 'equals fixed||equals not_fixed'

                ],
                [
                    'attributeA' => 'value1||value3||value4',
                    'attributeC' => 'equals value2',
                    'harvestStatus' => 'equals NO_HARVEST',
                    'crossMethodAbbrev' => 'not equals CROSS_METHOD_SELFING'
                    
                ]
            ]
        ];

        // ACT
        $actual = $this->crossBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 3
     * harvestStatus = INCOMPLETE
     */
    public function testAssembleBrowserFiltersWithIncompleteStatus()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "CrossBrowserModel" => [
                "harvestStatus" => "INCOMPLETE"
            ]
        ];

        // mocks
        $this->crossBrowserModel->harvestManagerModel->setReturnValue(
            'getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "conditions" => [
                [
                    'harvestStatus' => 'equals NO_HARVEST',
                    'crossMethodAbbrev' => 'equals CROSS_METHOD_SELFING',
                    'femaleGermplasmState' => 'equals fixed||equals not_fixed'
                ],
                [
                    'harvestStatus' => 'equals NO_HARVEST',
                    'crossMethodAbbrev' => 'not equals CROSS_METHOD_SELFING'
                ]
            ]
        ];

        // ACT
        $actual = $this->crossBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 4
     * harvestStatus = IN_PROGRESS
     */
    public function testAssembleBrowserFiltersWithInProgressStatus()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "CrossBrowserModel" => [
                "harvestStatus" => "IN_PROGRESS"
            ]
        ];

        // mocks
        $this->crossBrowserModel->harvestManagerModel->setReturnValue(
            'getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "conditions" => [
                [
                    'harvestStatus' => 'equals NO_HARVEST',
                    'crossMethodAbbrev' => 'equals CROSS_METHOD_SELFING',
                    'femaleGermplasmState' => 'equals fixed||equals not_fixed'
                ],
                [
                    'harvestStatus' => 'equals NO_HARVEST',
                    'crossMethodAbbrev' => 'not equals CROSS_METHOD_SELFING'
                ]
            ]
        ];

        // ACT
        $actual = $this->crossBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 5
     * harvestStatus = INVALID_STATE, femaleGermplasmState is not set
     */
    public function testAssembleBrowserFiltersWithInvalidStateAndStateNotSet()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "CrossBrowserModel" => [
                "harvestStatus" => "INVALID_STATE",
                "femaleGermplasmState" => ""
            ]
        ];

        // mocks
        $this->crossBrowserModel->harvestManagerModel->setReturnValue(
            'getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "conditions" => [
                [
                    "harvestStatus" => "equals NO_HARVEST",
                    "crossMethodAbbrev" => "equals CROSS_METHOD_SELFING",
                    "femaleGermplasmState" => "equals fixed||equals not_fixed",
                ],
                [
                    "harvestStatus" => "equals NO_HARVEST",
                    "crossMethodAbbrev" => "not equals CROSS_METHOD_SELFING"
                ]
            ]
        ];

        // ACT
        $actual = $this->crossBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 6
     * harvestStatus = INVALID_STATE, femaleGermplasmState is set
     */
    public function testAssembleBrowserFiltersWithInvalidStateAndStateIsSet()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "CrossBrowserModel" => [
                "harvestStatus" => "INVALID_STATE",
                "femaleGermplasmState" => "fixed"
            ]
        ];

        // mocks
        $this->crossBrowserModel->harvestManagerModel->setReturnValue(
            'getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "conditions" => [
                [
                    "harvestStatus" => "equals NO_HARVEST",
                    "crossMethodAbbrev" => "equals CROSS_METHOD_SELFING",
                    "femaleGermplasmState" => "equals fixed"
                ],
                [
                    "harvestStatus" => "equals NO_HARVEST",
                    "crossMethodAbbrev" => "not equals CROSS_METHOD_SELFING",
                    "femaleGermplasmState" => "equals fixed"
                ]
            ]
        ];

        // ACT
        $actual = $this->crossBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 7
     * harvestStatus = NO_HARVEST
     */
    public function testAssembleBrowserFiltersWithNoHarvestStatus()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 1234;
        $params = [
            "CrossBrowserModel" => [
                "harvestStatus" => "NO_HARVEST",
                "femaleGermplasmState" => ""
            ]
        ];

        // mocks
        $this->crossBrowserModel->harvestManagerModel->setReturnValue(
            'getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );

        // expected
        $expected = [
            "conditions" => [
                [
                    "harvestStatus" => "equals NO_HARVEST",
                    "crossMethodAbbrev" => "equals CROSS_METHOD_SELFING",
                    "femaleGermplasmState" => "equals fixed||equals not_fixed"
                ],
                [
                    "harvestStatus" => "equals NO_HARVEST",
                    "crossMethodAbbrev" => "not equals CROSS_METHOD_SELFING"
                ]
            ]
        ];

        // ACT
        $actual = $this->crossBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 1
     */
    public function testSearchTest1()
    {
        // ARRANGE
        // input
        $params = [
            "CrossBrowserModel" => [
                "femaleGermplasmState" => "not_fixed"
            ]
        ];
        $experimentId = 100;
        $occurrenceId = 200;
        $locationId = 300;

        // mocks
        $returnValue = [
            "resetPage" => false,
            "resetFilters" => true
        ];
        $this->crossBrowserModel->harvestManagerModel->setReturnValue('browserReset', $returnValue);

        $returnValue = [];
        $this->crossBrowserModel->harvestManagerModel->setReturnValue('assembleUrlParameters', $returnValue);

        $crosses = [
            [
                "crossDbId" => 2001
            ],
            [
                "crossDbId" => 2002
            ],
            [
                "crossDbId" => 2003
            ],
            [
                "crossDbId" => 2004
            ],
            [
                "crossDbId" => 2005
            ]
        ];
        $totalCount = 5;
        $returnValue = [
            "count" => $totalCount,
            "data" => $crosses
        ];
        $this->crossBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', $returnValue);
        $this->crossBrowserModel->harvestManagerModel->setReturnValue(
            'getScaleValues',
            [
                "fixed" => "fixed",
                "not_fixed" => "not_fixed",
                "unknown" => "unknown",
            ]
        );
        $this->crossBrowserModel->harvestManagerModel->setReturnValue('matchConfigs',
            [
                "return_vals" => [
                    [
                        [
                            "crossDbId" => 2001
                        ],
                        [
                            "crossDbId" => 2002
                        ],
                        [
                            "crossDbId" => 2003
                        ],
                        [
                            "crossDbId" => 2004
                        ],
                        [
                            "crossDbId" => 2005
                        ]
                    ]
                ]
            ]
        );
        $this->crossBrowserModel->harvestManagerModel->setReturnValue('trimValues', $crosses);

        // expected
        $dataProvider = new ArrayDataProvider([
            'allModels' => $crosses,
            'key' => 'crossDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);
        $dataProvider->id = null;
        $dataProvider->sort = false;
        $expected = [
            "dataProvider" => $dataProvider
        ];

        // ACT
        $actual = $this->crossBrowserModel->search($params, $experimentId, $occurrenceId, $locationId);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * rules: Test 1
     */
    public function testRulesTest1()
    {
        // ARRANGE
        // expected
        $expected = [
            [['crossFemaleParent', 'crossMaleParent', 'crossName', 'crossDbId', 'femaleGermplasmState', 'maleGermplasmState'], 'required'],
            [['crossDbId'], 'number'],
            [['crossDbId'], 'integer'],
            [['crossFemaleParent', 'crossMaleParent', 'crossingDate', 'crossMethod', 'crossMethodAbbrev', 'crossName', 'femaleGermplasmState', 'femaleParentage', 'femaleParentSeedSource', 'femaleSourceSeedEntry', 'femaleSourcePlot', 'maleGermplasmState', 'maleParentage', 'maleParentSeedSource', 'maleSourceSeedEntry', 'maleSourcePlot', 'remarks', 'femaleEntryCode', 'femaleEntryNumber', 'maleEntryCode', 'maleEntryNumber'], 'string'],
            [['crossDbId', 'crossFemaleParent', 'crossMaleParent', 'crossingDate', 'crossMethod', 'crossMethodAbbrev', 'crossName', 'femaleGermplasmState', 'femaleParentage', 'femaleParentSeedSource', 'femaleSourceSeedEntry', 'femaleSourcePlot', 'maleGermplasmState', 'maleParentage', 'maleParentSeedSource', 'maleSourceSeedEntry', 'maleSourcePlot', 'remarks', 'femaleEntryCode', 'femaleEntryNumber', 'maleEntryCode', 'maleEntryNumber'], 'safe'],
            [[], 'boolean'],
            [[], 'unique']
        ];

        // ACT
        $actual = $this->crossBrowserModel->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}
