<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

// Import data providers
use app\dataproviders\ArrayDataProvider;

class ManagementBrowserModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $managementBrowserModel;

    protected function _before()
    {
        $this->managementBrowserModel = \Yii::$container->get('managementBrowserModel');
    }

    protected function _after()
    {
    }
    
    // tests

    /**
     * getPackageRecords: Test 1
     */
    public function testGetPackageRecordsWithData()
    {
        // ARRANGE
        // inputs
        $requestBody = [
            "occurrenceDbId" => "123"
        ];
        $urlParams = [
            "limit=10"
        ];
        $occurrenceId = "123";
        
        // mocks
        $packages = [
            [
                "packageDbId" => 2001
            ],
            [
                "packageDbId" => 2002
            ],
            [
                "packageDbId" => 2003
            ],
            [
                "packageDbId" => 2004
            ]
        ];
        $totalCount = 4;
        $returnValue = [
            "count" => $totalCount,
            "data" => $packages
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', $returnValue);

        // expected
        $expected = [
            "count" => $totalCount,
            "data" => $packages
        ];

        // ACT
        $actual = $this->managementBrowserModel->getPackageRecords($requestBody, $urlParams, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 1
     * Simple column filters
     */
    public function testAssembleBrowserFiltersTest1()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "attributeA" => "value1",
                "attributeB" => "",
                "attributeC" => "value2",
                "attributeD" => ""
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "attributeA" => "equals value1",
            "attributeC" => "equals value2"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 2
     * seedSourcePlotNumber, single value
     */
    public function testAssembleBrowserFiltersTest2()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "seedSourcePlotNumber" => "3"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "seedSourcePlotNumber" => "3"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 3
     * seedSourcePlotNumber, comma separated integers
     */
    public function testAssembleBrowserFiltersTest3()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "seedSourcePlotNumber" => "1,4,7"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "seedSourcePlotNumber" => "1|4|7"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 4
     * seedSourcePlotNumber, range of integers
     */
    public function testAssembleBrowserFiltersTest4()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "seedSourcePlotNumber" => "6-10"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "seedSourcePlotNumber" => "6|7|8|9|10"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 5
     * seedSourcePlotNumber, range of integers, inverted (larger-smaller eg. 10-2)
     * Must be automatically flipped to 2-10
     */
    public function testAssembleBrowserFiltersTest5()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "seedSourcePlotNumber" => "10-2"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "seedSourcePlotNumber" => "2|3|4|5|6|7|8|9|10"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 6
     * seedSourcePlotNumber, range of integers, lower = higher (eg, 2-2)
     * Will just be set to 2
     */
    public function testAssembleBrowserFiltersTest6()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "seedSourcePlotNumber" => "2-2"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "seedSourcePlotNumber" => "2"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 7
     * seedSourcePlotNumber is invalid. Filter will not be included.
     */
    public function testAssembleBrowserFiltersWithInvalidPlotNo()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "seedSourcePlotNumber" => "2-4-7"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "seedSourcePlotNumber" => "2-4-7"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 8
     * seedSourcePlotNumber, comma separated integer ranges (eg. 1,5-8,10,12,15-18)
     */
    public function testAssembleBrowserFiltersTest8()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "seedSourcePlotNumber" => "1,5-8,10,12,15-18"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "seedSourcePlotNumber" => "1|5|6|7|8|10|12|15|16|17|18"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 9
     * quantity (invalid format)
     */
    public function testAssembleBrowserFiltersTest9()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "quantity" => "2.3.4 kgss"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 10
     * quantity (valid format, kg)
     */
    public function testAssembleBrowserFiltersTest10()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "quantity" => "2.4 kg"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "quantity" => "2.4",
            "unit" => "kg"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 11
     * quantity (valid format, seeds)
     */
    public function testAssembleBrowserFiltersTest11()
    {
        // ARRANGE
        // inputs
        $params = [
            "ManagementBrowserModel" => [
                "quantity" => "122seeds"
            ]
        ];
        $occurrenceId = "1234";

        // expected
        $expected = [
            "quantity" => "122",
            "unit" => "seeds"
        ];

        // ACT
        $actual = $this->managementBrowserModel->assembleBrowserFilters($params, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 1
     */
    public function testSearchBasic()
    {
        // ARRANGE
        // input
        $params = [
            "ManagementBrowserModel" => [
                "quantity" => "122seeds"
            ]
        ];
        $experimentId = 100;
        $occurrenceId = 200;
        $locationId = 300;

        // mocks
        $returnValue = [
            "resetPage" => false,
            "resetFilters" => true
        ];
        $this->managementBrowserModel->harvestManagerModel->setReturnValue('browserReset', $returnValue);

        $returnValue = [];
        $this->managementBrowserModel->harvestManagerModel->setReturnValue('assembleUrlParameters', $returnValue);

        $packages = [
            [
                "packageDbId" => 2001
            ],
            [
                "packageDbId" => 2002
            ],
            [
                "packageDbId" => 2003
            ],
            [
                "packageDbId" => 2004
            ]
        ];
        $totalCount = 4;
        $returnValue = [
            "count" => $totalCount,
            "data" => $packages
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', $returnValue);

        // expected
        $dataProvider = new ArrayDataProvider([
            'allModels' => $packages,
            'key' => 'packageDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);
        $dataProvider->id = null;
        $dataProvider->sort = false;
        $expected = $dataProvider;

        // ACT
        $actual = $this->managementBrowserModel->search($params, $experimentId, $occurrenceId, $locationId);
        $actual->id = null;
        $actual->sort = false;
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 2
     */
    public function testSearchWhenInitialResultIsZeroAndTotalCountIsZero()
    {
        // ARRANGE
        // input
        $params = [
            "ManagementBrowserModel" => [
                "quantity" => "122seeds"
            ]
        ];
        $experimentId = 100;
        $occurrenceId = 200;
        $locationId = 300;

        // mocks
        $returnValue = [
            "resetPage" => false,
            "resetFilters" => true
        ];
        $this->managementBrowserModel->harvestManagerModel->setReturnValue('browserReset', $returnValue);

        $returnValue = [];
        $this->managementBrowserModel->harvestManagerModel->setReturnValue('assembleUrlParameters', $returnValue);

        $packages = [
            [
                "packageDbId" => 2001
            ],
            [
                "packageDbId" => 2002
            ],
            [
                "packageDbId" => 2003
            ],
            [
                "packageDbId" => 2004
            ]
        ];
        $totalCount = 4;
        $returnValue = [
            'return_vals' => [
                [
                    "count" => 0,
                    "data" => []
                ],
                [
                    "count" => $totalCount,
                    "data" => $packages
                ]
            ]
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', $returnValue);

        // expected
        $dataProvider = new ArrayDataProvider([
            'allModels' => $packages,
            'key' => 'packageDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);
        $dataProvider->id = null;
        $dataProvider->sort = false;
        $expected = $dataProvider;

        // ACT
        $actual = $this->managementBrowserModel->search($params, $experimentId, $occurrenceId, $locationId);
        $actual->id = null;
        $actual->sort = false;
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * rules: Test 1
     */
    public function testRulesTest1()
    {
        // ARRANGE
        // expected
        $expected = [
            [['designation', 'label', 'packageDbId', 'seedName'], 'required'],
            [['packageDbId', 'seedSourcePlotNumber', 'replication'], 'integer'],
            [['designation', 'generation', 'germplasmState', 'harvestDate', 'harvestMethod', 'harvestSource', 'label', 'packageCode', 'parentage', 'seedName', 'seedSourcePlotCode', 'sourceEntryCode', 'crossFemaleParent', 'femaleParentage', 'crossMaleParent', 'maleParentage'], 'string'],
            [['designation', 'generation', 'germplasmState', 'seedCode', 'germplasmCode' ,'harvestDate', 'harvestMethod', 'harvestSource', 'label', 'packageCode', 'packageDbId', 'parentage', 'quantity', 'replication', 'seedName', 'seedSourcePlotCode', 'seedSourcePlotNumber', 'sourceEntryCode', 'crossFemaleParent', 'femaleParentage', 'crossMaleParent', 'maleParentage'], 'safe'],
            [[], 'boolean'],
            [['packageDbId'], 'unique']
        ];

        // ACT
        $actual = $this->managementBrowserModel->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkSeedEntries: Test 1
     * Seeds have no entries
     */
    public function testCheckSeedEntriesWhenSeedsHaveNoEntries() {
        // ARRANGE
        // input
        $seedIds = [ 101, 102, 103 ];

        // mocks
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', [
            "return_vals" => [
                [
                    "data" => [
                        [
                            "seedDbId" => 101,
                            "germplasmDbId" => 501
                        ]
                    ],
                    "count" => 0,
                    "status" => 200
                ],
                [
                    "data" => [
                        [
                            "seedDbId" => 102,
                            "germplasmDbId" => 502
                        ]
                    ],
                    "count" => 0,
                    "status" => 200
                ],
                [
                    "data" => [
                        [
                            "seedDbId" => 103,
                            "germplasmDbId" => 503
                        ]
                    ],
                    "count" => 0,
                    "status" => 200
                ]
            ]
        ]);

        // expected
        $expected = [
            'seedIdsToDelete' => [ 101, 102, 103 ],
            'seedIdsWithEntries' => [  ],
            'germplasmIdsToDelete' => [ 501, 502, 503 ],
            'germplasmSeedRelation' => [
                501 => [ 101 ],
                502 => [ 102 ],
                503 => [ 103 ]
            ],
            'seedsToDeleteCount' => 3,
            'seedsWithEntriesCount' => 0,
            'totalSeeds' => 3
        ];

        // ACT
        $actual = $this->managementBrowserModel->checkSeedEntries($seedIds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkSeedEntries: Test 2
     * Seeds have entries
     */
    public function testCheckSeedEntriesWhenSeedsHaveEntries() {
        // ARRANGE
        // input
        $seedIds = [ 101, 102, 103 ];

        // mocks
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', [
            "return_vals" => [
                [
                    "data" => [
                        [
                            "seedDbId" => 101,
                            "germplasmDbId" => 501
                        ]
                    ],
                    "count" => 3,
                    "status" => 200
                ],
                [
                    "data" => [
                        [
                            "seedDbId" => 102,
                            "germplasmDbId" => 502
                        ]
                    ],
                    "count" => 2,
                    "status" => 200
                ],
                [
                    "data" => [
                        [
                            "seedDbId" => 103,
                            "germplasmDbId" => 503
                        ]
                    ],
                    "count" => 1,
                    "status" => 200
                ]
            ]
        ]);

        // expected
        $expected = [
            'seedIdsToDelete' => [],
            'seedIdsWithEntries' => [ 101, 102, 103 ],
            'germplasmIdsToDelete' => [],
            'germplasmSeedRelation' => [],
            'seedsToDeleteCount' => 0,
            'seedsWithEntriesCount' => 3,
            'totalSeeds' => 3
        ];

        // ACT
        $actual = $this->managementBrowserModel->checkSeedEntries($seedIds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkGermplasm: Test 1
     * No other seeds use the germplasm
     */
    public function testCheckGermplasmWhenNoOtherSeedsUseTheGermplasm() {
        // ARRANGE
        // input
        $seedIdsToDelete = [ 101, 102, 103 ];
        $germplasmIdsToDelete = [ 501, 502, 503 ]; 
        $germplasmSeedRelation = [
            501 => [ 101 ],
            502 => [ 102 ],
            503 => [ 103 ]
        ];

        // mock
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 101
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 102
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 103
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        // expected
        $expected = [
            "seedIds" => [],
            "germplasmIds" => [ 501, 502, 503 ]
        ];

        // ACT
        $actual = $this->managementBrowserModel->checkGermplasm($seedIdsToDelete, $germplasmIdsToDelete, $germplasmSeedRelation);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkGermplasm: Test 2
     * Other seeds use the germplasm
     */
    public function testCheckGermplasmWhenOtherSeedsUseTheGermplasm() {
        // ARRANGE
        // input
        $seedIdsToDelete = [ 101, 102, 103 ];
        $germplasmIdsToDelete = [ 501, 502, 503 ]; 
        $germplasmSeedRelation = [
            501 => [ 101 ],
            502 => [ 102 ],
            503 => [ 103 ]
        ];

        // mock
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 101
                            ],
                            [
                                "seedDbId" => 301
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 102
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 101
                            ],
                            [
                                "seedDbId" => 551
                            ],
                            [
                                "seedDbId" => 678
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        // expected
        $expected = [
            "seedIds" => [ 101, 103 ],
            "germplasmIds" => [ 502 ]
        ];

        // ACT
        $actual = $this->managementBrowserModel->checkGermplasm($seedIdsToDelete, $germplasmIdsToDelete, $germplasmSeedRelation);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * deleteViaProcessor
     */
    public function testDeleteViaProcessor() {
        // ARRANGE
        // input
        $seedIds = [ 1003, 1004 ];
        $germplasmIds = [ 5001, 5234, 6345 ];
        $occurrenceId = 1234;

        // mock
        $this->managementBrowserModel->apiHelper->setReturnValue('processorBuilder', [
            "return_vals" => [
                [
                    [
                        "backgroundJobDbId" => 43001
                    ]
                ],
                [
                    [
                        
                        "backgroundJobDbId" => 43002
                        
                    ]
                ]
            ]
        ]);

        // expected
        $expected = [ 43001, 43002 ];

        // ACT
        $actual = $this->managementBrowserModel->deleteViaProcessor($seedIds, $germplasmIds, $occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getListPreviewDataProvider: Test 1
     * Test retrieval of list data provider with selected items
     */
    public function testGetListPreviewDataProviderWithSelectedItems(){
        // ARRANGE
        // input
        $occurrenceId = 123;
        $packageIdList = [1,2,3];
        $selectionMode = 'selected';

        \Yii::$app->session->set("ManagementBrowserModel-".$occurrenceId,['label' => "1"]);
        
        $packages = [
            [
                "packageDbId" => 1
            ],
            [
                "packageDbId" => 2
            ],
            [
                "packageDbId" => 3
            ]
        ];
        $totalCount = 3;
        $returnValue = [
            "count" => $totalCount,
            "data" => $packages,
            "status" => 200
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', $returnValue);
        $this->managementBrowserModel->apiHelper->setReturnValue('getPackageRecords', $returnValue);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $packages,
            'key' => 'packageDbId',
            'totalCount' => $totalCount,
            'pagination' => false,
            'id' => 'hm-list-preview'
        ]);

        // expected
        $expected = [
            "dataProvider" => $dataProvider,
            "count" => 3
        ];

        // ACT
        $actual = $this->managementBrowserModel->getListPreviewDataProvider($occurrenceId, $packageIdList, $selectionMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getListPreviewDataProvider: Test 2
     * Test throwing of error exception when error occurred
     */
    public function testGetListPreviewDataProviderThrowErrorException(){
        // ARRANGE
        // input
        $occurrenceId = 123;
        $packageIdList = [1,2,3];
        $selectionMode = 'selected';

        $packages = [
            [
                "packageDbId" => 1
            ],
            [
                "packageDbId" => 2
            ],
            [
                "packageDbId" => 3
            ]
        ];
        $totalCount = 3;
        $returnValue = [
            "count" => $totalCount,
            "data" => $packages,
            "status" => 500,
            "message" => "Error occurred."
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequestSinglePage', $returnValue);
        $this->managementBrowserModel->apiHelper->setReturnValue('getPackageRecords', $returnValue);

        // EXPECT
        $this->expectException(\yii\base\ErrorException::class);
        $this->expectExceptionMessage("Error occurred.");
        $this->managementBrowserModel->getListPreviewDataProvider($occurrenceId, $packageIdList, $selectionMode);

    }

    /**
     * getListPreviewDataProvider: Test 3
     * Test retrieval of list data provider with all harvested materials of an occurrence
     */
    public function testGetListPreviewDataProviderWithAllItems(){
        // ARRANGE
        $expected = [
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [
                        ['packageDbId' => 1],
                        ['packageDbId' => 2],
                        ['packageDbId' => 3]
                    ],
                'key' => 'packageDbId',
                'totalCount' => 3,
                'pagination' => false,
                'id' => 'hm-list-preview'
            ]),
            'count' => 3
        ];

        $occurrenceId = 123;
        $packageIdList = [];
        $selectionMode = 'all';

        $packages = [
            ['packageDbId' => 1],
            ['packageDbId' => 2],
            ['packageDbId' => 3]
        ];
        $totalCount = 3;

        // mock
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);
        
        $this->managementBrowserModel->apiHelper->setReturnValue(
            'getParsedResponse', 
            [
                'data' => $packages,
                'totalCount' => $totalCount,
                'success' => true,
                'message' => ''
            ]);
    
        $returnValue = [
            "count" => $totalCount,
            "data" => $packages,
            "status" => 200
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue(
            'sendRequestSinglePage', 
            [
                "count" => $totalCount,
                "data" => $packages,
                "success" => true,
                "message" => '',
                "status" => 200
            ]
        );

        // ACT
        $actual = $this->managementBrowserModel->getListPreviewDataProvider($occurrenceId, $packageIdList, $selectionMode);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }


    /**
     * validateList: Test 1
     * Test when list already exists
     */
    public function testValidateListAlreadyExists(){
        // ARRANGE
        // input
        $abbrev = 'list';
        $name = 'list';

        // $returnValue = ['abbrev' => $abbrev, 'fields' => 'list.id as listDbId|list.abbrev'], 'limit=1',;


        $lists = [
            [
                "listsDbId" => 1
            ],
            [
                "listsDbId" => 2
            ],
            [
                "listsDbId" => 3
            ]
        ];
        $totalCount = 3;
        $returnValue = [
            "totalCount" => $totalCount,
            "data" => $lists
        ];
        $this->managementBrowserModel->lists->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = ['result' => false, 'message' => 'Name or Abbrev already exists. Please provide a new one.'];

        // ACT
        $actual = $this->managementBrowserModel->validateList($abbrev, $name);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }


    /**
     * validateList: Test 2
     * Test when list does not exist
     */
    public function testValidateListDoesNotExist(){
        // ARRANGE
        // input
        $abbrev = 'abbrev';
        $name = 'name';

        $this->managementBrowserModel->lists->setReturnValue('searchAll', [
            "totalCount" => 0,
            "data" => [],
        ]);

        // expected
        $expected = ['result' => true];

        // ACT
        $actual = $this->managementBrowserModel->validateList($abbrev, $name);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * createMembersInBackground: Test 1
     * Test successful creation of list members in the background
     */
    public function testCreateMembersInBackgroundSuccessful(){
        // ARRANGE
        // input
        $occurrenceId = 1;
        $filters = [];
        $listId = 1;
        $listType = 'type';
        $ids = [1,2,3];
        $sortParam = 'packageDbId:ASC';

        // mock
        $this->managementBrowserModel->worker->setReturnValue('invoke', [
            "status" => "success"
        ]);

        // expected
        $expected = ["status" => "success"];

        // ACT
        $actual = $this->managementBrowserModel->createMembersInBackground($occurrenceId, $filters, $listId, $listType, $ids, $sortParam);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * saveList: Test 1
     * Test saving of list with no members, selected mode
     */
    public function testSaveListNoListMembers(){
        // ARRANGE
        // input
        $occurrenceId = 1;
        $listDetails =[
            'records' => [[
                'name' => 'List name',
                'abbrev' => 'List abbrev',
                'type' => 'package'
            ]]
        ];
        $ids = [];
        $selectionMode = 'selected';

        // mock
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);
        
        $config = $this->make('app\components\Config',
        [
            'getAppThreshold' => function($tool, $feature) {
                return 1000;
            }
        ]);

        \Yii::$app->set('config',$config);

        $returnValue = [];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        // expected
        $expected = [
            'success' => false, 
            'message' => 'No list members found. Please create packages first.',
            'inBackground' => false
        ];

        // ACT
        $actual = $this->managementBrowserModel->saveList($occurrenceId, $listDetails, $ids, $selectionMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * saveList: Test 2
     * Test saving of list with more than 0 members, selected mode
     */
    public function testSaveListWithListMembers(){
        // ARRANGE
        // input
        $occurrenceId = 1;
        $listDetails =[
            'records' => [[
                'name' => 'List name',
                'abbrev' => 'List abbrev',
                'type' => 'package'
            ]]
        ];
        $ids = [1,2,3];
        $selectionMode = 'selected';

        // mocks
        \Yii::$app->session->set("packagesTotalCount-$occurrenceId", count($ids));
        
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        //getAppThreshold()
        $config = $this->make('app\components\Config',
        [
            'getAppThreshold' => function($tool, $feature) {
                return 1000;
            }
        ]);

        $returnValue = [
            'data' => [
                ['packageDbId' => 1],
                ['packageDbId' => 2],
                ['packageDbId' => 3]
            ],
            'totalCount' => 3,
            'success' => 200,
            'message' => ''
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        \Yii::$app->set('config', $config);
        //lists->create()
        $this->managementBrowserModel->lists->setReturnValue('create',[
            'status' => 200,
            'data' => [[
                'listDbId' => 1
            ]],
        ]);
        //lists->createMembers()
        $createMembersResultsMock = [
            "totalCount" => 3,
            "success" => true 
        ];
        $this->managementBrowserModel->lists->setReturnValue('createMembers',$createMembersResultsMock);


        // expected
        $expected = [
            'success' => true, 
            'message' => 'Package List:&nbsp;<b>List name</b>&nbsp;successfully created.',
            'inBackground' => false
        ];

        // ACT
        $actual = $this->managementBrowserModel->saveList($occurrenceId, $listDetails, $ids, $selectionMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * saveList: Test 3
     * Test failed list member creation not in background
     */
    public function testSaveListFailedListMemberCreation(){
        // ARRANGE
        // input
        $occurrenceId = 1;
        $listDetails =[
            'records' => [[
                'name' => 'List name',
                'abbrev' => 'List abbrev',
                'type' => 'package'
            ]]
        ];
        $ids = [1,2,3];
        $selectionMode = 'selected';

        // mocks
        \Yii::$app->session->set("packagesTotalCount-$occurrenceId", count($ids));
        
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        //getAppThreshold()
        $config = $this->make('app\components\Config',
        [
            'getAppThreshold' => function($tool, $feature) {
                return 1000;
            }
        ]);

        \Yii::$app->set('config', $config);

        $returnValue = [
            'data' => [
                ['packageDbId' => 1],
                ['packageDbId' => 2],
                ['packageDbId' => 3]
            ],
            'totalCount' => 3,
            'success' => 200,
            'message' => ''
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        //lists->create()
        $this->managementBrowserModel->lists->setReturnValue('create',[
            'status' => 200,
            'data' => [[
                'listDbId' => 1
            ]],
        ]);
        //lists->createMembers()
        $createMembersResultsMock = [
            "totalCount" => 0,
            "success" => false 
        ];
        $this->managementBrowserModel->lists->setReturnValue('createMembers',$createMembersResultsMock);


        // expected
        $expected = [
            'success' => false, 
            'message' => 'There seems to be a problem while saving the list members.',
            'inBackground' => false
        ];

        // ACT
        $actual = $this->managementBrowserModel->saveList($occurrenceId, $listDetails, $ids, $selectionMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * saveList: Test 4
     * Test successful list member creation in background
     */
    public function testSaveListBackgroundListMemberCreationSuccessful(){
        // ARRANGE
        // input
        $occurrenceId = 1;
        $listDetails =[
            'records' => [[
                'name' => 'List name',
                'abbrev' => 'List abbrev',
                'type' => 'package'
            ]]
        ];
        $ids = [1,2,3];
        $selectionMode = 'selected';

        // mocks
        \Yii::$app->session->set("packagesTotalCount-$occurrenceId", count($ids));
        
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        //getAppThreshold()
        $config = $this->make('app\components\Config',
        [
            'getAppThreshold' => function($tool, $feature) {
                return 1;
            }
        ]);

        $returnValue = [
            'data' => [
                ['packageDbId' => 1],
                ['packageDbId' => 2],
                ['packageDbId' => 3]
            ],
            'totalCount' => 3,
            'success' => 200,
            'message' => ''
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        \Yii::$app->set('config', $config);
        //lists->create()
        $this->managementBrowserModel->lists->setReturnValue('create',[
            'status' => 200,
            'data' => [[
                'listDbId' => 1
            ]],
        ]);

        $returnValue = [
            "status" => "success",
            "success" => true
        ];
        //createMembersInBackground()
        $this->managementBrowserModel->worker->setReturnValue('invoke', $returnValue);
        $this->managementBrowserModel->lists->setReturnValue('createMembersInBackground', $returnValue);

        // expected
        $expected = [
            'success' => true, 
            'message' => 'The package list is being created in the background. You may proceed with other tasks. You will be notified in this page once done.',
            'inBackground' => true
        ];

        // ACT
        $actual = $this->managementBrowserModel->saveList($occurrenceId, $listDetails, $ids, $selectionMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * saveList: Test 5
     * Test failed list member creation in background
     */
    public function testSaveListBackgroundListMemberCreationFailed(){
        // ARRANGE
        // input
        $occurrenceId = 1;
        $listDetails =[
            'records' => [[
                'name' => 'List name',
                'abbrev' => 'List abbrev',
                'type' => 'package'
            ]]
        ];
        $ids = [1,2,3];
        $selectionMode = 'selected';

        // mocks
        \Yii::$app->session->set("packagesTotalCount-$occurrenceId", count($ids));
        
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        //getAppThreshold()
        $config = $this->make('app\components\Config',
        [
            'getAppThreshold' => function($tool, $feature) {
                return 1;
            }
        ]);

        $returnValue = [
            'data' => [
                ['packageDbId' => 1],
                ['packageDbId' => 2],
                ['packageDbId' => 3]
            ],
            'totalCount' => 3,
            'success' => 200,
            'message' => ''
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        \Yii::$app->set('config', $config);
        //lists->create()
        $this->managementBrowserModel->lists->setReturnValue('create',[
            'status' => 200,
            'data' => [[
                'listDbId' => 1
            ]],
        ]);

        $returnValue = [
            "status" => "success",
            "success" => false
        ];
        //createMembersInBackground()
        $this->managementBrowserModel->worker->setReturnValue('invoke', $returnValue);
        $this->managementBrowserModel->lists->setReturnValue('createMembersInBackground', $returnValue);

        // expected
        $expected = [
            'success' => false, 
            'message' => 'There seems to be a problem while saving the list members in the background.',
            'inBackground' => true
        ];

        // ACT
        $actual = $this->managementBrowserModel->saveList($occurrenceId, $listDetails, $ids, $selectionMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * saveList: Test 6
     * Test failed saving of a list
     */
    public function testSaveListFailed(){
        // ARRANGE
        // input
        $occurrenceId = 1;
        $listDetails =[
            'records' => [[
                'name' => 'List name',
                'abbrev' => 'List abbrev',
                'type' => 'package'
            ]]
        ];
        $ids = [1, 2, 3];
        $selectionMode = 'selected';

        // mocks
        \Yii::$app->session->set("packagesTotalCount-$occurrenceId", count($ids));
        
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        $config = $this->make('app\components\Config',
        [
            'getAppThreshold' => function($tool, $feature) {
                return 1000;
            }
        ]);

        $returnValue = [
            'data' => [
                ['packageDbId' => 1],
                ['packageDbId' => 2],
                ['packageDbId' => 3]
            ],
            'totalCount' => 3,
            'success' => 200,
            'message' => ''
        ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        \Yii::$app->set('config',$config);

        $this->managementBrowserModel->lists->setReturnValue('create',[
            'status' => 500,
            'data' => [[
                'listDbId' => 1
            ]],
        ]);

        // expected
        $expected = [
            'success' => false, 
            'message' => 'There seems to be a problem while saving the list.',
            'inBackground' => false
        ];

        // ACT
        $actual = $this->managementBrowserModel->saveList($occurrenceId, $listDetails, $ids, $selectionMode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getBrowserFilters: Test 1
     * Test successfully extracting browser column filters
     */
    public function testGetBrowserFilters(){
        // ARRANGE
        $occurrenceId = 123;
        \Yii::$app->session->set("ManagementBrowserModel-".$occurrenceId,['label' => "1"]);

        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) {
                return ['label' => "1"];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        $expected = ['label'=>'1'];

        // ACT
        $actual = $this->managementBrowserModel->getBrowserFilters($occurrenceId);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    /**
     * getBrowserFilters: Test 2
     * Test successfully even without browser filters
     */
    public function testGetBrowserFiltersWithNoColumnFilters(){
        // ARRANGE
        $occurrenceId = 123;
        \Yii::$app->session->set("ManagementBrowserModel-".$occurrenceId,[]);

        $expected = [];

        // ACT
        $actual = $this->managementBrowserModel->getBrowserFilters($occurrenceId);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    /**
     * getListPreviewDataProvider: Test 1
     * Test successfully retrieving germplasm, seed, and package records
     */
    public function testGetPackageSeedGermplasmIds(){
        // ARRANGE
        $occurrenceDbId = 123;
        $type = 'package';

        $expected = [
            'data' => [1,2,3], 
            'totalCount' => 3, 
            'success' => true, 
            'message' => ''
        ];

        $returnValue = [
                ['packageDbId' => 1],
                ['packageDbId' => 2],
                ['packageDbId' => 3]
            ];
        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        // mock
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        // ACT
        $actual = $this->managementBrowserModel->getPackageSeedGermplasmIds($occurrenceDbId, $type);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    /**
     * getListPreviewDataProvider: Test 2
     * Test successfully retrieving germplasm, seed, and package records
     */
    public function testGetPackageSeedGermplasmIdsFirstPageOnly(){
        // ARRANGE
        $occurrenceDbId = 123;
        $type = 'package';

        $expected = [
            'data' => [1,2,3], 
            'totalCount' => 3, 
            'success' => true, 
            'message' => ''
        ];

        $returnValue = [
                'data' => [
                    ['packageDbId' => 1],
                    ['packageDbId' => 2],
                    ['packageDbId' => 3]
                ],
                'totalCount' => 3,
                'success' => 200,
                'message' => ''
            ];
        $this->managementBrowserModel->apiHelper->setReturnValue('getParsedResponse', $returnValue);

        // mock
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        // ACT
        $actual = $this->managementBrowserModel->getPackageSeedGermplasmIds($occurrenceDbId, $type, false);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }

    /**
     * getListPreviewDataProvider: Test 3
     * Test retrieving germplasm, seed, and package records failed
     */
    public function testGetPackageSeedGermplasmIdsFailedRetrieval(){
        // ARRANGE
        $occurrenceDbId = 123;
        $type = 'package';
        $expected = [
            'data' => [],
            'totalCount' => 0,
            'success' => false,
            'message' => 'Process failed! Error encountered in completing the process.'
        ];

        $this->managementBrowserModel->apiHelper->setReturnValue('sendRequest', null);

        // mock
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName){
                return ['sort' => 'packageDbId:ASC'];
            },
            'set' => function($varName, $varValue) {
                return;
            }
        ]);
        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        // ACT
        $actual = $this->managementBrowserModel->getPackageSeedGermplasmIds($occurrenceDbId, $type);

        // ASSERT
        $this->assertEquals($expected,$actual);
    }
}