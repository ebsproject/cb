<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

class HarvestDataModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $harvestDataModel;
    
    protected function _before()
    {
        $this->harvestDataModel = \Yii::$container->get('harvestDataModel');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * getOccurrenceExperimentType: Test 1
     * Experiment exists
     */
    public function testGetOccurrenceExperimentTypeTest1()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 123;

        // mocks
        $returnValue = 15;
        $this->harvestDataModel->occurrenceDetailsModel->setReturnValue('getExperimentIdOfOccurrence', $returnValue);

        $returnValue = [
            "data" => [
                [
                    "experimentType" => "Breeding Trial"
                ]
            ]
        ];
        $this->harvestDataModel->experiment->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = "Breeding Trial";

        // ACT
        $actual = $this->harvestDataModel->getOccurrenceExperimentType($occurrenceId);
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOccurrenceExperimentType: Test 2
     * No experiment id returned
     */
    public function testGetOccurrenceExperimentTypeTest2()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 123;

        // mocks
        $returnValue = 0;
        $this->harvestDataModel->occurrenceDetailsModel->setReturnValue('getExperimentIdOfOccurrence', $returnValue);

        $returnValue = [
            "data" => []
        ];
        $this->harvestDataModel->experiment->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = "";

        // ACT
        $actual = $this->harvestDataModel->getOccurrenceExperimentType($occurrenceId);
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOccurrenceExperimentType: Test 3
     * Experiment search returned no results
     */
    public function testGetOccurrenceExperimentTypeTest3()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 123;

        // mocks
        $returnValue = 2;
        $this->harvestDataModel->occurrenceDetailsModel->setReturnValue('getExperimentIdOfOccurrence', $returnValue);

        $returnValue = [];
        $this->harvestDataModel->experiment->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = "";

        // ACT
        $actual = $this->harvestDataModel->getOccurrenceExperimentType($occurrenceId);
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOccurrenceName: Test 1
     * Occurrence searchAll has result
     */
    public function testGetOccurrenceNameTest1()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 123;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrenceName" => "OCC_1"
                ]
            ]
        ];
        $this->harvestDataModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = "OCC_1";
        
        // ACT
        $actual = $this->harvestDataModel->getOccurrenceName($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOccurrenceName: Test 2
     * Occurrence searchAll has result, but no occurrenceName
     */
    public function testGetOccurrenceNameTest2()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 123;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "otherVar" => "otherVal"
                ]
            ]
        ];
        $this->harvestDataModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = "";
        
        // ACT
        $actual = $this->harvestDataModel->getOccurrenceName($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOccurrenceName: Test 3
     * Occurrence searchAll has no result
     */
    public function testGetOccurrenceNameTest3()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 123;

        // mocks
        $returnValue = [
            "data" => []
        ];
        $this->harvestDataModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = "";
        
        // ACT
        $actual = $this->harvestDataModel->getOccurrenceName($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOccurrenceName: Test 4
     * Occurrence searchAll has no data
     */
    public function testGetOccurrenceNameTest4()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 123;

        // mocks
        $returnValue = [];
        $this->harvestDataModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = "";
        
        // ACT
        $actual = $this->harvestDataModel->getOccurrenceName($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * consumeConfig: Test 1
     * RICE Selfing fixed
     */
    public function testConsumeConfigTestForRiceSelfingFixed()
    {
        // ARRANGE
        // inputs
        $config = [
            "CROSS_METHOD_SELFING" => [
                "fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => false
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                'placeholder' => 'Not applicable'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ],
                "not_fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => true
                            ],
                            [
                                "column_name" => "numericVar",
                                "placeholder" => "Not applicable",
                                "retrieve_scale" => false,
                                "abbrev" => "<none>",
                                "required" => null
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK",
                                    "HV_METH_DISC_SINGLE_SEED_DESCENT",
                                ],
                                'placeholder' => 'Not applicable'
                            ],
                            [
                                "abbrev" => "NO_OF_PLANTS",
                                "field_name" => "noOfPlant",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 1,
                                "harvest_methods" => [
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION_AND_BULK"
                                ],
                                'placeholder' => 'No. of plants'
                            ],
                            [
                                "abbrev" => "PANNO_SEL",
                                "field_name" => "noOfPanicle",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 1,
                                "harvest_methods" => [
                                    "HV_METH_DISC_PANICLE_SELECTION"
                                ],
                                'placeholder' => 'No. of panicles'
                            ],
                            [
                                "abbrev" => "SPECIFIC_PLANT",
                                "field_name" => "specificPlantNo",
                                "type" => "text",
                                "sub_type" => "comma_sep_int",
                                "harvest_methods" => [
                                    "HV_METH_DISC_PLANT_SPECIFIC",
                                    "HV_METH_DISC_PLANT_SPECIFIC_AND_BULK"
                                ],
                                'placeholder' => 'Specific plant no.'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ]
        ];
        $states = ["fixed"];
        $types = ["default"];

        // mocks
        $returnValue = [ 
            "HV_METH_DISC_PLANT_SPECIFIC" => [ "Plant-specific" => "Plant-specific" ],
            "HV_METH_DISC_BULK" => [ "Bulk" => "Bulk" ],
            "HV_METH_DISC_SINGLE_PLANT_SELECTION_AND_BULK" => [ "Single Plant Selection and Bulk" => "Single Plant Selection and Bulk" ],
            "HV_METH_DISC_SINGLE_PLANT_SELECTION" => [ "Single Plant Selection" => "Single Plant Selection" ],
            "HV_METH_DISC_BULK_FIXEDLINE" => [ "Bulk fixedline" => "Bulk fixedline" ],
            "HV_METH_DISC_PANICLE_SELECTION" => [ "Panicle Selection" => "Panicle Selection" ],
            "HV_METH_DISC_SINGLE_PLANT_SEED_INCREASE" => [ "Single plant seed increase" => "Single plant seed increase" ],
            "HV_METH_DISC_PLANT_SPECIFIC_AND_BULK" => [ "Plant-Specific and Bulk" => "Plant-Specific and Bulk" ],
            "HV_METH_DISC_SINGLE_SEED_DESCENT" => [ "Single Seed Descent" => "Single Seed Descent" ],
            "HV_METH_DISC_SELECTED_BULK" => [ "Selected bulk" => "Selected bulk" ],
            "HV_METH_DISC_INDIVIDUAL_PLANT" => [ "Individual plant" => "Individual plant" ],
            "HV_METH_DISC_INDIVIDUAL_SPIKE" => [ "Individual spike" => "Individual spike" ],
            "HV_METH_DISC_INDIVIDUAL_EAR" => [ "Individual ear" => "Individual ear" ],
            "HV_METH_DISC_INDIVIDUAL_PANICLE" => [ "Individual panicle" => "Individual panicle" ],
            "HV_METH_DISC_HEAD_ROWS" => [ "Head-rows" => "Head-rows" ],
            "HV_METH_DISC_HEAD_ROW_PURIFICATION" => [ "Head-row purification" => "Head-row purification" ],
            "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING" => [ "Single seed numbering" => "Single seed numbering" ],
            "HV_METH_DISC_UNKNOWN" => [ "Unknown" => "Unknown" ]
        ];
        $this->harvestDataModel->harvestManagerModel->setReturnValue('getScaleValues', $returnValue);

        // expected
        $expected = [
            "CROSS_METHOD_SELFING" => [
                "fixed" => [
                    "default" => [
                        "additional_required_variables" => [],
                        "harvest_methods" => [ "Bulk" => "Bulk" ],
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "column_name" => "harvestMethod",
                                "required" => false
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "disabled" => true,
                                "field_name" => "<none>",
                                "min" => 0,
                                "placeholder" => "Not applicable",
                                "sub_type" => "single_int",
                                "type" => "number"
                            ]
                        ],
                        "variable_scale_values" => []
                    ]
                ]
            ],
            "input_columns" => [ "harvestDate", "harvestMethod" ],
            "method_numvar_requirement_compat" => [
                "CROSS_METHOD_SELFING" => [
                    "Bulk" => [ "" ]
                ]
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SELFING" => [
                    "Bulk" => [ "" ]
                ]
            ],
            "numeric_variables" => [],
            "numeric_variables_field" => [],
            "bulk_update_config" => [
                "numeric_variables" => [],
                "num_var_compat" => [],
                "harvest_methods" => [
                    'Bulk' => 'Bulk'
                ],
                "cmet_hmet_compat" => [
                    'CROSS_METHOD_SELFING' => [
                        'Bulk'
                    ]
                ],
                "variable_field_names" => [
                    'HVDATE_CONT' => 'harvestDate',
                    'HV_METH_DISC' => 'harvestMethod'
                ],
                "variable_placeholder" => [
                    'HVDATE_CONT' => 'Harvest Date',
                    'HV_METH_DISC' => 'Harvest Method'
                ],
                "variable_field_placeholder_mapping" => [
                    'harvestDate' => 'Harvest Date',
                    'harvestMethod' => 'Harvest Method'
                ],
                "gstate_gtype_hmet_compat" => [
                    "fixed" => [
                        "default" => [
                            "Bulk" => "Bulk"
                        ]
                    ]
                ],
                "variable_scale_values" => [],
                'variable_compat' => [
                    'CROSS_METHOD_SELFING' => [
                        'fixed' => [
                            'default' => [
                                'harvestDate',
                                'harvestMethod'
                            ]
                        ]
                    ]
                ],
                "method_numvar_requirement_compat" => [
                    "CROSS_METHOD_SELFING" => [
                        "Bulk" => [ "" ]
                    ]
                ]
            ],
            "bulk_delete_config" => [
                "all_variables" => [
                    [
                        'field_name' => 'harvestDate',
                        'abbrev' => 'HVDATE_CONT',
                        'placeholder' => 'Harvest Date'
                    ],
                    [
                        'field_name' => 'harvestMethod',
                        'abbrev' => 'HV_METH_DISC',
                        'placeholder' => 'Harvest Method'
                    ]
                ],
                "numeric_variables" => [],
                'numeric_variables_abbrevs' => [],
                "all_abbrevs" => [
                    'HVDATE_CONT',
                    'HV_METH_DISC'
                ],
                'abbrev_field_name_array' => [
                    'HVDATE_CONT' => 'harvestDate',
                    'HV_METH_DISC' => 'harvestMethod'
                ]
            ],
            'variable_compat' => [
                'CROSS_METHOD_SELFING' => [
                    'fixed' => [
                        'default' => [
                            'harvestDate',
                            'harvestMethod'
                        ]
                    ]
                ]
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->consumeConfig($config, $states, $types);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * consumeConfig: Test 2
     * RICE Selfing not_fixed
     */
    public function testConsumeConfigTestRiceSelfingNotFixed()
    {
        // ARRANGE
        // inputs
        $config = [
            "CROSS_METHOD_SELFING" => [
                "fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => false
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                'placeholder' => 'Not applicable'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ],
                "not_fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => true
                            ],
                            [
                                "column_name" => "numericVar",
                                "placeholder" => "Not applicable",
                                "retrieve_scale" => false,
                                "abbrev" => "<none>",
                                "required" => null
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK",
                                    "HV_METH_DISC_SINGLE_SEED_DESCENT",
                                ],
                                'placeholder' => 'Not applicable'
                            ],
                            [
                                "abbrev" => "NO_OF_PLANTS",
                                "field_name" => "noOfPlant",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 1,
                                "harvest_methods" => [
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION_AND_BULK"
                                ],
                                'placeholder' => 'No. of plants'
                            ],
                            [
                                "abbrev" => "PANNO_SEL",
                                "field_name" => "noOfPanicle",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 1,
                                "harvest_methods" => [
                                    "HV_METH_DISC_PANICLE_SELECTION"
                                ],
                                'placeholder' => 'No. of panicles'
                            ],
                            [
                                "abbrev" => "SPECIFIC_PLANT",
                                "field_name" => "specificPlantNo",
                                "type" => "text",
                                "sub_type" => "comma_sep_int",
                                "harvest_methods" => [
                                    "HV_METH_DISC_PLANT_SPECIFIC",
                                    "HV_METH_DISC_PLANT_SPECIFIC_AND_BULK"
                                ],
                                'placeholder' => 'Specific plant no.'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ],
            "CROSS_METHOD_SELFING_V2" => [
                "fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => false
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                'placeholder' => 'Not applicable'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ],
                "not_fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => true
                            ],
                            [
                                "column_name" => "numericVar",
                                "placeholder" => "Not applicable",
                                "retrieve_scale" => false,
                                "abbrev" => "<none>",
                                "required" => null
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK",
                                    "HV_METH_DISC_SINGLE_SEED_DESCENT",
                                ],
                                'placeholder' => 'Not applicable'
                            ],
                            [
                                "abbrev" => "NO_OF_PLANTS",
                                "field_name" => "noOfPlant",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 1,
                                "harvest_methods" => [
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION_AND_BULK"
                                ],
                                'placeholder' => 'No. of plants'
                            ],
                            [
                                "abbrev" => "PANNO_SEL",
                                "field_name" => "noOfPanicle",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 1,
                                "harvest_methods" => [
                                    "HV_METH_DISC_PANICLE_SELECTION"
                                ],
                                'placeholder' => 'No. of panicles'
                            ],
                            [
                                "abbrev" => "SPECIFIC_PLANT",
                                "field_name" => "specificPlantNo",
                                "type" => "text",
                                "sub_type" => "comma_sep_int",
                                "harvest_methods" => [
                                    "HV_METH_DISC_PLANT_SPECIFIC",
                                    "HV_METH_DISC_PLANT_SPECIFIC_AND_BULK"
                                ],
                                'placeholder' => 'Specific plant no.'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ]
        ];
        $states = ["not_fixed"];
        $types = ["default"];

        // mocks
        $returnValue = [ 
            "HV_METH_DISC_PLANT_SPECIFIC" => [ "Plant-specific" => "Plant-specific" ],
            "HV_METH_DISC_BULK" => [ "Bulk" => "Bulk" ],
            "HV_METH_DISC_SINGLE_PLANT_SELECTION_AND_BULK" => [ "Single Plant Selection and Bulk" => "Single Plant Selection and Bulk" ],
            "HV_METH_DISC_SINGLE_PLANT_SELECTION" => [ "Single Plant Selection" => "Single Plant Selection" ],
            "HV_METH_DISC_BULK_FIXEDLINE" => [ "Bulk fixedline" => "Bulk fixedline" ],
            "HV_METH_DISC_PANICLE_SELECTION" => [ "Panicle Selection" => "Panicle Selection" ],
            "HV_METH_DISC_SINGLE_PLANT_SEED_INCREASE" => [ "Single plant seed increase" => "Single plant seed increase" ],
            "HV_METH_DISC_PLANT_SPECIFIC_AND_BULK" => [ "Plant-Specific and Bulk" => "Plant-Specific and Bulk" ],
            "HV_METH_DISC_SINGLE_SEED_DESCENT" => [ "Single Seed Descent" => "Single Seed Descent" ],
            "HV_METH_DISC_SELECTED_BULK" => [ "Selected bulk" => "Selected bulk" ],
            "HV_METH_DISC_INDIVIDUAL_PLANT" => [ "Individual plant" => "Individual plant" ],
            "HV_METH_DISC_INDIVIDUAL_SPIKE" => [ "Individual spike" => "Individual spike" ],
            "HV_METH_DISC_INDIVIDUAL_EAR" => [ "Individual ear" => "Individual ear" ],
            "HV_METH_DISC_INDIVIDUAL_PANICLE" => [ "Individual panicle" => "Individual panicle" ],
            "HV_METH_DISC_HEAD_ROWS" => [ "Head-rows" => "Head-rows" ],
            "HV_METH_DISC_HEAD_ROW_PURIFICATION" => [ "Head-row purification" => "Head-row purification" ],
            "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING" => [ "Single seed numbering" => "Single seed numbering" ],
            "HV_METH_DISC_UNKNOWN" => [ "Unknown" => "Unknown" ]
        ];
        $this->harvestDataModel->harvestManagerModel->setReturnValue('getScaleValues', $returnValue);

        // expected
        $expected = [
            "CROSS_METHOD_SELFING" => [
                "not_fixed" => [
                    "default" => [
                        "additional_required_variables" => [],
                        "harvest_methods" => [ 
                            "Bulk" => "Bulk",
                            "Panicle Selection" => "Panicle Selection",
                            "Plant-Specific and Bulk" => "Plant-Specific and Bulk",
                            "Plant-specific" => "Plant-specific",
                            "Single Plant Selection" => "Single Plant Selection",
                            "Single Plant Selection and Bulk" => "Single Plant Selection and Bulk",
                            "Single Seed Descent" => "Single Seed Descent"
                        ],
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "column_name" => "harvestMethod",
                                "required" => true
                            ],
                            [
                                "abbrev" => "<none>",
                                "placeholder" => "Not applicable",
                                "retrieve_scale" => false,
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "disabled" => true,
                                "field_name" => "<none>",
                                "min" => 0,
                                "placeholder" => "Not applicable",
                                "sub_type" => "single_int",
                                "type" => "number"
                            ],
                            [
                                "abbrev" => "NO_OF_PLANTS",
                                "field_name" => "noOfPlant",
                                "min" => 1,
                                "placeholder" => "No. of plants",
                                "sub_type" => "single_int",
                                "type" => "number"
                            ],
                            [
                                "abbrev" => "PANNO_SEL",
                                "field_name" => "noOfPanicle",
                                "min" => 1,
                                "placeholder" => "No. of panicles",
                                "sub_type" => "single_int",
                                "type" => "number"
                            ],
                            [
                                "abbrev" => "SPECIFIC_PLANT",
                                "field_name" => "specificPlantNo",
                                "placeholder" => "Specific plant no.",
                                "sub_type" => "comma_sep_int",
                                "type" => "text"
                            ]
                        ],
                        "variable_scale_values" => []
                    ]
                ]
            ],
            "CROSS_METHOD_SELFING_V2" => [
                "not_fixed" => [
                    "default" => [
                        "additional_required_variables" => [],
                        "harvest_methods" => [ 
                            "Bulk" => "Bulk",
                            "Panicle Selection" => "Panicle Selection",
                            "Plant-Specific and Bulk" => "Plant-Specific and Bulk",
                            "Plant-specific" => "Plant-specific",
                            "Single Plant Selection" => "Single Plant Selection",
                            "Single Plant Selection and Bulk" => "Single Plant Selection and Bulk",
                            "Single Seed Descent" => "Single Seed Descent"
                        ],
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "column_name" => "harvestMethod",
                                "required" => true
                            ],
                            [
                                "abbrev" => "<none>",
                                "placeholder" => "Not applicable",
                                "retrieve_scale" => false,
                                "column_name" => "numericVar",
                                "required" => null
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "disabled" => true,
                                "field_name" => "<none>",
                                "min" => 0,
                                "placeholder" => "Not applicable",
                                "sub_type" => "single_int",
                                "type" => "number"
                            ],
                            [
                                "abbrev" => "NO_OF_PLANTS",
                                "field_name" => "noOfPlant",
                                "min" => 1,
                                "placeholder" => "No. of plants",
                                "sub_type" => "single_int",
                                "type" => "number"
                            ],
                            [
                                "abbrev" => "PANNO_SEL",
                                "field_name" => "noOfPanicle",
                                "min" => 1,
                                "placeholder" => "No. of panicles",
                                "sub_type" => "single_int",
                                "type" => "number"
                            ],
                            [
                                "abbrev" => "SPECIFIC_PLANT",
                                "field_name" => "specificPlantNo",
                                "placeholder" => "Specific plant no.",
                                "sub_type" => "comma_sep_int",
                                "type" => "text"
                            ]
                        ],
                        "variable_scale_values" => []
                    ]
                ]
            ],
            "input_columns" => [ "harvestDate", "harvestMethod", "numericVar" ],
            "method_numvar_requirement_compat" => [
                "CROSS_METHOD_SELFING" => [],
                "CROSS_METHOD_SELFING" => [
                    "Bulk" => [ "" ],
                    "Panicle Selection" => [ "PANNO_SEL" ],
                    "Plant-Specific and Bulk" => [ "SPECIFIC_PLANT" ],
                    "Plant-specific" => [ "SPECIFIC_PLANT" ],
                    "Single Plant Selection" => [ "NO_OF_PLANTS" ],
                    "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
                    "Single Seed Descent" => [ "" ],
                ],
                "CROSS_METHOD_SELFING_V2" => [
                    "Bulk" => [ "" ],
                    "Panicle Selection" => [ "PANNO_SEL" ],
                    "Plant-Specific and Bulk" => [ "SPECIFIC_PLANT" ],
                    "Plant-specific" => [ "SPECIFIC_PLANT" ],
                    "Single Plant Selection" => [ "NO_OF_PLANTS" ],
                    "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
                    "Single Seed Descent" => [ "" ],
                ]
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SELFING" => [
                    "Bulk" => [ "" ],
                    "Panicle Selection" => [ "PANNO_SEL" ],
                    "Plant-Specific and Bulk" => [ "SPECIFIC_PLANT" ],
                    "Plant-specific" => [ "SPECIFIC_PLANT" ],
                    "Single Plant Selection" => [ "NO_OF_PLANTS" ],
                    "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
                    "Single Seed Descent" => [ "" ],
                ],
                "CROSS_METHOD_SELFING_V2" => [
                    "Bulk" => [ "" ],
                    "Panicle Selection" => [ "PANNO_SEL" ],
                    "Plant-Specific and Bulk" => [ "SPECIFIC_PLANT" ],
                    "Plant-specific" => [ "SPECIFIC_PLANT" ],
                    "Single Plant Selection" => [ "NO_OF_PLANTS" ],
                    "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
                    "Single Seed Descent" => [ "" ],
                ]
            ],
            "numeric_variables" => [
                "NO_OF_PLANTS",
                "PANNO_SEL",
                "SPECIFIC_PLANT"
            ],
            "numeric_variables_field" => [
                "noOfPlant",
                "noOfPanicle",
                "specificPlantNo"
            ],
            "bulk_update_config" => [
                "numeric_variables" => [
                    [
                        'abbrev' => 'NO_OF_PLANTS',
                        'field_name' => 'noOfPlant',
                        'type' => 'number',
                        'sub_type' => 'single_int',
                        'min' => 1,
                        'placeholder' => 'No. of plants'
                    ],
                    [
                        'abbrev' => 'PANNO_SEL',
                        'field_name' => 'noOfPanicle',
                        'type' => 'number',
                        'sub_type' => 'single_int',
                        'min' => 1,
                        'placeholder' => 'No. of panicles'
                    ],
                    [
                        'abbrev' => 'SPECIFIC_PLANT',
                        'field_name' => 'specificPlantNo',
                        'type' => 'text',
                        'sub_type' => 'comma_sep_int',
                        'placeholder' => 'Specific plant no.'
                    ]
                ],
                "num_var_compat" => [
                    'NO_OF_PLANTS' => [
                        'harvestMethods' => [
                            'Single Plant Selection',
                            'Single Plant Selection and Bulk'
                        ],
                        'description' => 'No. of plants',
                        'crossMethods' => [ 'CROSS_METHOD_SELFING', 'CROSS_METHOD_SELFING_V2' ]                      
                    ],
                    'PANNO_SEL' => [
                        'harvestMethods' => [
                            'Panicle Selection'
                        ],
                        'description' => 'No. of panicles',
                        'crossMethods' => [ 'CROSS_METHOD_SELFING', 'CROSS_METHOD_SELFING_V2' ]
                    ],
                    'SPECIFIC_PLANT' => [
                        'harvestMethods' => [
                            'Plant-specific',
                            'Plant-Specific and Bulk'
                        ],
                        'description' => 'Specific plant no.',
                        'crossMethods' => [ 'CROSS_METHOD_SELFING', 'CROSS_METHOD_SELFING_V2' ]
                    ]
                ],
                "harvest_methods" => [
                    'Bulk' => 'Bulk',
                    'Single Seed Descent' => 'Single Seed Descent',
                    'Single Plant Selection' => 'Single Plant Selection',
                    'Single Plant Selection and Bulk' => 'Single Plant Selection and Bulk',
                    'Panicle Selection' => 'Panicle Selection',
                    'Plant-specific' => 'Plant-specific',
                    'Plant-Specific and Bulk' => 'Plant-Specific and Bulk'                    
                ],
                "cmet_hmet_compat" => [
                    'CROSS_METHOD_SELFING' => [
                        'Bulk',
                        'Single Seed Descent',
                        'Single Plant Selection',
                        'Single Plant Selection and Bulk',
                        'Panicle Selection',
                        'Plant-specific',
                        'Plant-Specific and Bulk'
                    ],
                    'CROSS_METHOD_SELFING_V2' => [
                        'Bulk',
                        'Single Seed Descent',
                        'Single Plant Selection',
                        'Single Plant Selection and Bulk',
                        'Panicle Selection',
                        'Plant-specific',
                        'Plant-Specific and Bulk'
                    ]
                ],
                "variable_field_names" => [
                    'HVDATE_CONT' => 'harvestDate',
                    'HV_METH_DISC' => 'harvestMethod',
                    'NO_OF_PLANTS' => 'noOfPlant',
                    'PANNO_SEL' => 'noOfPanicle',
                    'SPECIFIC_PLANT' => 'specificPlantNo'

                ],
                "variable_placeholder" => [
                    'HVDATE_CONT' => 'Harvest Date',
                    'HV_METH_DISC' => 'Harvest Method',
                    'NO_OF_PLANTS' => 'No. of plants',
                    'PANNO_SEL' => 'No. of panicles',
                    'SPECIFIC_PLANT' => 'Specific plant no.'
                ],
                "variable_field_placeholder_mapping" => [
                    'harvestDate' => 'Harvest Date',
                    'harvestMethod' => 'Harvest Method',
                    'noOfPlant' => 'No. of plants',
                    'noOfPanicle' => 'No. of panicles',
                    'specificPlantNo' => 'Specific plant no.'                    
                ],
                "gstate_gtype_hmet_compat" => [
                    "not_fixed" => [
                        "default" => [
                            'Bulk' => 'Bulk',
                            'Single Seed Descent' => 'Single Seed Descent',
                            'Single Plant Selection' => 'Single Plant Selection',
                            'Single Plant Selection and Bulk' => 'Single Plant Selection and Bulk',
                            'Panicle Selection' => 'Panicle Selection',
                            'Plant-specific' => 'Plant-specific',
                            'Plant-Specific and Bulk' => 'Plant-Specific and Bulk'
                        ]
                    ]
                ],
                "variable_scale_values" => [],
                'variable_compat' => [
                    'CROSS_METHOD_SELFING' => [
                        'not_fixed' => [
                            'default' => [
                                'harvestDate',
                                'harvestMethod',
                                'noOfPlant',
                                'noOfPanicle',
                                'specificPlantNo'
                            ]
                        ]
                    ],
                    'CROSS_METHOD_SELFING_V2' => [
                        'not_fixed' => [
                            'default' => [
                                'harvestDate',
                                'harvestMethod',
                                'noOfPlant',
                                'noOfPanicle',
                                'specificPlantNo'
                            ]
                        ]
                    ]
                ],
                "method_numvar_requirement_compat" => [
                    "CROSS_METHOD_SELFING" => [],
                    "CROSS_METHOD_SELFING" => [
                        "Bulk" => [ "" ],
                        "Panicle Selection" => [ "PANNO_SEL" ],
                        "Plant-Specific and Bulk" => [ "SPECIFIC_PLANT" ],
                        "Plant-specific" => [ "SPECIFIC_PLANT" ],
                        "Single Plant Selection" => [ "NO_OF_PLANTS" ],
                        "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
                        "Single Seed Descent" => [ "" ],
                    ],
                    "CROSS_METHOD_SELFING_V2" => [
                        "Bulk" => [ "" ],
                        "Panicle Selection" => [ "PANNO_SEL" ],
                        "Plant-Specific and Bulk" => [ "SPECIFIC_PLANT" ],
                        "Plant-specific" => [ "SPECIFIC_PLANT" ],
                        "Single Plant Selection" => [ "NO_OF_PLANTS" ],
                        "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
                        "Single Seed Descent" => [ "" ],
                    ]
                ]
            ],
            "bulk_delete_config" => [
                "all_variables" => [
                    [
                        'field_name' => 'noOfPlant',
                        'abbrev' => 'NO_OF_PLANTS',
                        'placeholder' => 'No. of plants'
                    ],
                    [
                        'field_name' => 'noOfPanicle',
                        'abbrev' => 'PANNO_SEL',
                        'placeholder' => 'No. of panicles'
                    ],
                    [
                        'field_name' => 'specificPlantNo',
                        'abbrev' => 'SPECIFIC_PLANT',
                        'placeholder' => 'Specific plant no.'
                    ],
                    [
                        'field_name' => 'harvestDate',
                        'abbrev' => 'HVDATE_CONT',
                        'placeholder' => 'Harvest Date'
                    ],
                    [
                        'field_name' => 'harvestMethod',
                        'abbrev' => 'HV_METH_DISC',
                        'placeholder' => 'Harvest Method'
                    ]
                ],
                "numeric_variables" => [
                    [
                        'field_name' => 'noOfPlant',
                        'abbrev' => 'NO_OF_PLANTS',
                        'placeholder' => 'No. of plants'
                    ],
                    [
                        'field_name' => 'noOfPanicle',
                        'abbrev' => 'PANNO_SEL',
                        'placeholder' => 'No. of panicles'
                    ],
                    [
                        'field_name' => 'specificPlantNo',
                        'abbrev' => 'SPECIFIC_PLANT',
                        'placeholder' => 'Specific plant no.'
                    ]
                ],
                'numeric_variables_abbrevs' => [
                    'NO_OF_PLANTS',
                    'PANNO_SEL',
                    'SPECIFIC_PLANT',
                ],
                "all_abbrevs" => [
                    'NO_OF_PLANTS',
                    'PANNO_SEL',
                    'SPECIFIC_PLANT',
                    'HVDATE_CONT',
                    'HV_METH_DISC',
                ],
                'abbrev_field_name_array' => [
                    'HVDATE_CONT' => 'harvestDate',
                    'HV_METH_DISC' => 'harvestMethod',
                    'NO_OF_PLANTS' => 'noOfPlant',
                    'PANNO_SEL' => 'noOfPanicle',
                    'SPECIFIC_PLANT' => 'specificPlantNo'
                ]
            ],
            'variable_compat' => [
                'CROSS_METHOD_SELFING' => [
                    'not_fixed' => [
                        'default' => [
                            'harvestDate',
                            'harvestMethod',
                            'noOfPlant',
                            'noOfPanicle',
                            'specificPlantNo'
                        ]
                    ]
                ],
                'CROSS_METHOD_SELFING_V2' => [
                    'not_fixed' => [
                        'default' => [
                            'harvestDate',
                            'harvestMethod',
                            'noOfPlant',
                            'noOfPanicle',
                            'specificPlantNo'
                        ]
                    ]
                ]
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->consumeConfig($config, $states, $types);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * consumeConfig: Test 3
     * WHEAT Crosses browser config
     */
    public function testConsumeConfigTestWheatCrossesBrowserConfig()
    {
        // ARRANGE
        // inputs
        $config = [
            "CROSS_METHOD_SELFING" => [
                "fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => false
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                'placeholder' => 'Not applicable'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ],
                "not_fixed" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => true
                            ],
                            [
                                "column_name" => "numericVar",
                                "placeholder" => "Not applicable",
                                "retrieve_scale" => false,
                                "abbrev" => "<none>",
                                "required" => null
                            ]
                        ],
                        "harvest_methods" => [
                            "HV_METH_DISC_BULK",
                            "HV_METH_DISC_SELECTED_BULK",
                            "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                            "HV_METH_DISC_INDIVIDUAL_SPIKE"
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                'placeholder' => 'Not applicable'
                            ],
                            [
                                "abbrev" => "NO_OF_PLANTS",
                                "field_name" => "noOfPlant",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 1,
                                "harvest_methods" => [
                                    "HV_METH_DISC_SELECTED_BULK",
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                                    "HV_METH_DISC_INDIVIDUAL_SPIKE"
                                ],
                                'placeholder' => 'No. of plants'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ],
            "CROSS_METHOD_SINGLE_CROSS" => [
                "default" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => true
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                'placeholder' => 'Not applicable'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ],
            "CROSS_METHOD_TOP_CROSS" => [
                "default" => [
                    "default" => [
                        "input_columns" => [
                            [
                                "column_name" => "harvestDate",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "abbrev" => "HVDATE_CONT",
                                "required" => true
                            ],
                            [
                                "column_name" => "harvestMethod",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "abbrev" => "HV_METH_DISC",
                                "required" => true
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "field_name" => "<none>",
                                "type" => "number",
                                "sub_type" => "single_int",
                                "min" => 0,
                                "disabled" => true,
                                "harvest_methods" => [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                'placeholder' => 'Not applicable'
                            ]
                        ],
                        "additional_required_variables" => []
                    ]
                ]
            ]
        ];
        $states = ["default"];
        $types = ["default"];

        // mocks
        $returnValue = [ 
            "HV_METH_DISC_PLANT_SPECIFIC" => [ "Plant-specific" => "Plant-specific" ],
            "HV_METH_DISC_BULK" => [ "Bulk" => "Bulk" ],
            "HV_METH_DISC_SINGLE_PLANT_SELECTION_AND_BULK" => [ "Single Plant Selection and Bulk" => "Single Plant Selection and Bulk" ],
            "HV_METH_DISC_SINGLE_PLANT_SELECTION" => [ "Single Plant Selection" => "Single Plant Selection" ],
            "HV_METH_DISC_BULK_FIXEDLINE" => [ "Bulk fixedline" => "Bulk fixedline" ],
            "HV_METH_DISC_PANICLE_SELECTION" => [ "Panicle Selection" => "Panicle Selection" ],
            "HV_METH_DISC_SINGLE_PLANT_SEED_INCREASE" => [ "Single plant seed increase" => "Single plant seed increase" ],
            "HV_METH_DISC_PLANT_SPECIFIC_AND_BULK" => [ "Plant-Specific and Bulk" => "Plant-Specific and Bulk" ],
            "HV_METH_DISC_SINGLE_SEED_DESCENT" => [ "Single Seed Descent" => "Single Seed Descent" ],
            "HV_METH_DISC_SELECTED_BULK" => [ "Selected bulk" => "Selected bulk" ],
            "HV_METH_DISC_INDIVIDUAL_PLANT" => [ "Individual plant" => "Individual plant" ],
            "HV_METH_DISC_INDIVIDUAL_SPIKE" => [ "Individual spike" => "Individual spike" ],
            "HV_METH_DISC_INDIVIDUAL_EAR" => [ "Individual ear" => "Individual ear" ],
            "HV_METH_DISC_INDIVIDUAL_PANICLE" => [ "Individual panicle" => "Individual panicle" ],
            "HV_METH_DISC_HEAD_ROWS" => [ "Head-rows" => "Head-rows" ],
            "HV_METH_DISC_HEAD_ROW_PURIFICATION" => [ "Head-row purification" => "Head-row purification" ],
            "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING" => [ "Single seed numbering" => "Single seed numbering" ],
            "HV_METH_DISC_UNKNOWN" => [ "Unknown" => "Unknown" ]
        ];
        $this->harvestDataModel->harvestManagerModel->setReturnValue('getScaleValues', $returnValue);

        // expected
        $expected = [
            "CROSS_METHOD_SINGLE_CROSS" => [
                "default" => [
                    "default" => [
                        "additional_required_variables" => [],
                        "harvest_methods" => [ 
                            "Bulk" => "Bulk"
                        ],
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "disabled" => true,
                                "field_name" => "<none>",
                                "min" => 0,
                                "placeholder" => "Not applicable",
                                "sub_type" => "single_int",
                                "type" => "number"
                            ]
                        ],
                        "variable_scale_values" => []
                    ]
                ]
            ],
            "CROSS_METHOD_TOP_CROSS" => [
                "default" => [
                    "default" => [
                        "additional_required_variables" => [],
                        "harvest_methods" => [ 
                            "Bulk" => "Bulk"
                        ],
                        "input_columns" => [
                            [
                                "abbrev" => "HVDATE_CONT",
                                "placeholder" => "Harvest Date",
                                "retrieve_scale" => false,
                                "column_name" => "harvestDate",
                                "required" => true
                            ],
                            [
                                "abbrev" => "HV_METH_DISC",
                                "placeholder" => "Harvest Method",
                                "retrieve_scale" => false,
                                "column_name" => "harvestMethod",
                                "required" => true
                            ]
                        ],
                        "numeric_variables" => [
                            [
                                "abbrev" => "<none>",
                                "disabled" => true,
                                "field_name" => "<none>",
                                "min" => 0,
                                "placeholder" => "Not applicable",
                                "sub_type" => "single_int",
                                "type" => "number"
                            ]
                        ],
                        "variable_scale_values" => []
                    ]
                ]
            ],
            "input_columns" => [ "harvestDate", "harvestMethod" ],
            "method_numvar_requirement_compat" => [
                "CROSS_METHOD_SINGLE_CROSS" => [
                    "Bulk" => [ "" ]
                ],
                "CROSS_METHOD_TOP_CROSS" => [
                    "Bulk" => [ "" ]
                ]
            ],
            "method_numvar_compat" => [
                "CROSS_METHOD_SINGLE_CROSS" => [
                    "Bulk" => [ "" ]
                ],
                "CROSS_METHOD_TOP_CROSS" => [
                    "Bulk" => [ "" ]
                ]
            ],
            "numeric_variables" => [],
            "numeric_variables_field" => [],
            "bulk_update_config" => [
                "numeric_variables" => [],
                "num_var_compat" => [],
                "harvest_methods" => [
                    'Bulk' => 'Bulk',
                ],
                "cmet_hmet_compat" => [
                    'CROSS_METHOD_SELFING' => [],
                    'CROSS_METHOD_SINGLE_CROSS' => [ 'Bulk' ],
                    'CROSS_METHOD_TOP_CROSS' => [ 'Bulk' ]
                ],
                "variable_field_names" => [
                    'HVDATE_CONT' => 'harvestDate',
                    'HV_METH_DISC' => 'harvestMethod'
                ],
                "variable_placeholder" => [
                    'HVDATE_CONT' => 'Harvest Date',
                    'HV_METH_DISC' => 'Harvest Method'
                ],
                "variable_field_placeholder_mapping" => [
                    'harvestDate' => 'Harvest Date',
                    'harvestMethod' => 'Harvest Method'
                ],
                "gstate_gtype_hmet_compat" => [
                    "default" => [
                        "default" => [
                            'Bulk' => 'Bulk'
                        ]
                    ]
                ],
                "variable_scale_values" => [],
                'variable_compat' => [
                    'CROSS_METHOD_SELFING' => [ ],
                    'CROSS_METHOD_SINGLE_CROSS' => [
                        'default' => [
                            'default' => [
                                'harvestDate',
                                'harvestMethod'
                            ]
                        ]
                    ],
                    'CROSS_METHOD_TOP_CROSS' => [
                        'default' => [
                            'default' => [
                                'harvestDate',
                                'harvestMethod'
                            ]
                        ]
                    ]
                ],
                "method_numvar_requirement_compat" => [
                    "CROSS_METHOD_SINGLE_CROSS" => [
                        "Bulk" => [ "" ]
                    ],
                    "CROSS_METHOD_TOP_CROSS" => [
                        "Bulk" => [ "" ]
                    ]
                ]
            ],
            "bulk_delete_config" => [
                "all_variables" => [
                    [
                        'field_name' => 'harvestDate',
                        'abbrev' => 'HVDATE_CONT',
                        'placeholder' => 'Harvest Date'
                    ],
                    [
                        'field_name' => 'harvestMethod',
                        'abbrev' => 'HV_METH_DISC',
                        'placeholder' => 'Harvest Method'
                    ]
                ],
                "numeric_variables" => [],
                'numeric_variables_abbrevs' => [],
                "all_abbrevs" => [
                    'HVDATE_CONT',
                    'HV_METH_DISC'
                ],
                'abbrev_field_name_array' => [
                    'HVDATE_CONT' => 'harvestDate',
                    'HV_METH_DISC' => 'harvestMethod'
                ]
            ],
            'variable_compat' => [
                'CROSS_METHOD_SELFING' => [ ],
                'CROSS_METHOD_SINGLE_CROSS' => [
                    'default' => [
                        'default' => [
                            'harvestDate',
                            'harvestMethod'
                        ]
                    ]
                ],
                'CROSS_METHOD_TOP_CROSS' => [
                    'default' => [
                        'default' => [
                            'harvestDate',
                            'harvestMethod'
                        ]
                    ]
                ]
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->consumeConfig($config, $states, $types);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * disableInput: Test 1
     * Harvest status = COMPLETED
     */
    public function testDisableInputTest1()
    {
        // ARRANGE
        // input
        $model = [
            "state" => "fixed",
            "harvestStatus" => "COMPLETED"
        ];
        $dataLevel = "plot";

        // expected
        $expected = [
            "disable" => true,
            "message" => \Yii::t('app', "This $dataLevel has been harvested. Kindly delete the committed data first before you input new data on this $dataLevel.")
        ];

        // ACT
        $actual = $this->harvestDataModel->disableInput($model, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * disableInput: Test 2
     * Harvest status = FAILED
     */
    public function testDisableInputTest2()
    {
        // ARRANGE
        // input
        $model = [
            "state" => null,
            "harvestStatus" => "FAILED"
        ];
        $dataLevel = "cross";

        // expected
        $expected = [
            "disable" => true,
            "message" => \Yii::t('app', "This harvest failed. Kindly delete the committed data first before you input new data on this $dataLevel.")
        ];

        // ACT
        $actual = $this->harvestDataModel->disableInput($model, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * disableInput: Test 3
     * Harvest status = QUEUED_FOR_HARVEST
     */
    public function testDisableInputTest3()
    {
        // ARRANGE
        // input
        $model = [
            "state" => null,
            "harvestStatus" => "QUEUED_FOR_HARVEST"
        ];
        $dataLevel = "cross";

        // expected
        $expected = [
            "disable" => true,
            "message" => \Yii::t('app', 'You cannot input harvest traits right now. Seed and package creation is in progress.')
        ];

        // ACT
        $actual = $this->harvestDataModel->disableInput($model, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * disableInput: Test 4
     * Harvest status = HARVEST_IN_PROGRESS
     */
    public function testDisableInputTest4()
    {
        // ARRANGE
        // input
        $model = [
            "state" => null,
            "harvestStatus" => "HARVEST_IN_PROGRESS"
        ];
        $dataLevel = "cross";

        // expected
        $expected = [
            "disable" => true,
            "message" => \Yii::t('app', 'You cannot input harvest traits right now. Seed and package creation is in progress.')
        ];

        // ACT
        $actual = $this->harvestDataModel->disableInput($model, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * disableInput: Test 5
     * Harvest status = DONE
     */
    public function testDisableInputTest5()
    {
        // ARRANGE
        // input
        $model = [
            "state" => null,
            "harvestStatus" => "DONE"
        ];
        $dataLevel = "plot";

        // expected
        $expected = [
            "disable" => true,
            "message" => \Yii::t('app', 'You cannot input harvest traits right now. Seed and package creation is in progress.')
        ];

        // ACT
        $actual = $this->harvestDataModel->disableInput($model, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * disableInput: Test 6
     * Harvest status = DELETION_HARVEST_IN_PROGRESS
     */
    public function testDisableInputTest6()
    {
        // ARRANGE
        // input
        $model = [
            "state" => null,
            "harvestStatus" => "DELETION_IN_PROGRESS"
        ];
        $dataLevel = "cross";

        // expected
        $expected = [
            "disable" => true,
            "message" => \Yii::t('app', 'You cannot input harvest traits right now. Harvest trait and/or seed and package deletion is in progress.')
        ];

        // ACT
        $actual = $this->harvestDataModel->disableInput($model, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * disableInput: Test 7
     * state = unknown
     */
    public function testDisableInputTest7()
    {
        // ARRANGE
        // input
        $model = [
            "state" => "unknown",
            "harvestStatus" => "NO_HARVEST"
        ];
        $dataLevel = "plot";

        // expected
        $expected = [
            "disable" => true,
            "message" => \Yii::t('app', 'Input not allowed due to unknown state. Please request for data curation.')
        ];

        // ACT
        $actual = $this->harvestDataModel->disableInput($model, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * disableInput: Test 8
     * Harvest status = NO_HARVEST and state is not unknown
     */
    public function testDisableInputTest8()
    {
        // ARRANGE
        // input
        $model = [
            "state" => "fixed",
            "harvestStatus" => "NO_HARVEST"
        ];
        $dataLevel = "plot";

        // expected
        $expected = [
            "disable" => false,
            "message" => ""
        ];

        // ACT
        $actual = $this->harvestDataModel->disableInput($model, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildHarvestDateColumn: Test 1
     * Committed data is not set
     */
    public function testBuildHarvestDateColumnTest1()
    {
        // ARRANGE
        // inputs
        $cmVarCompat = [
            'CROSS_METHOD_SELFING' => [
                'fixed' => [
                    'default' => [
                        'harvestDate'
                    ]
                ]
            ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestDate" => null,
            "terminalHarvestDate" => null,
            "state" => "fixed",
            "harvestStatus" => "NO_HARVEST"
        ];
        $actionId = "plots";

        // Mock dependencies
        $crossMock = \Yii::$container->get('crossMock');
        $experimentMock = \Yii::$container->get('experimentMock');
        $occurrenceMock = \Yii::$container->get('occurrenceMock');
        $plotMock = \Yii::$container->get('plotMock');
        $variableMock = \Yii::$container->get('variableMock');
        $terminalTransactionMock = \Yii::$container->get('terminalTransactionMock');
        $transactionDatasetMock = \Yii::$container->get('transactionDatasetMock');
        $userMock = \Yii::$container->get('userMock');
        $apiHelperMock = \Yii::$container->get('apiHelperMock');
        $crossBrowserModelMock = \Yii::$container->get('crossBrowserModelMock');
        $harvestManagerModelMock = \Yii::$container->get('harvestManagerModelMock');
        $occurrenceDetailsModelMock = \Yii::$container->get('occurrenceDetailsModelMock');
        $plotBrowserModelMock = \Yii::$container->get('plotBrowserModelMock');

        /**
         * Partially mock HarvestDataModel.
         * Replace buildDatePicker's return value.
         * 
         * This has to be mocked, as there is an issue with
         * the version compatibility of PHP and Composer that causes
         * the test to fail when buildDatePicker is not mocked.
         * More info about this in this thread: https://github.com/fxpio/composer-asset-plugin/issues/334
         * 
         * The error produced looks like this:
         *      [yii\base\InvalidArgumentException] The file or directory to be published does not exist: /home/jbantay/IRRIDev/cbdev/cb/vendor/bower/jquery/dist 
         * 
         * Manually renaming bower-asset to bower in the vendor directory solves the problem,
         * but is not an elegant solution, and should be avoided.
         */
        $this->harvestDataModel = $this->construct($this->harvestDataModel,
            [
                "cross" => $crossMock,
                "experiment" => $experimentMock,
                "occurrence" => $occurrenceMock,
                "plot" => $plotMock,
                "variable" => $variableMock,
                "terminalTransaction" => $terminalTransactionMock,
                "transactionDataset" => $transactionDatasetMock,
                "user" => $userMock,
                "apiHelper" => $apiHelperMock,
                "crossBrowserModel" => $crossBrowserModelMock,
                "harvestManagerModel" => $harvestManagerModelMock,
                "occurrenceDetailsModel" => $occurrenceDetailsModelMock,
                "plotBrowserModel" => $plotBrowserModelMock
            ], 
            [
            'buildDatePicker' => function () { 
                return "<placeholder>"; 
            }
        ]);

        $expected = '<div class="harvest-date-committed"></div><div class="harvest-date-input"><placeholder><div>';

        // ACT
        // If intelephense is enabled, buildHarvestDateColumn may be tagged
        // as an "undefined method". Ignore this for now. The test should run just fine
        // despite the error message.
        $actual = $this->harvestDataModel->buildHarvestDateColumn($cmVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildHarvestDateColumn: Test 2
     * Committed data is set
     */
    public function testBuildHarvestDateColumnTest2()
    {
        // ARRANGE
        // inputs
        $cmVarCompat = [
            'CROSS_METHOD_SELFING' => [
                'fixed' => [
                    'default' => [
                        'harvestDate'
                    ]
                ]
            ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestDate" => '2021-09-20',
            "terminalHarvestDate" => null,
            "state" => "fixed",
            "harvestStatus" => "NO_HARVEST"
        ];
        $actionId = "plots";

        // Mock dependencies
        $crossMock = \Yii::$container->get('crossMock');
        $experimentMock = \Yii::$container->get('experimentMock');
        $occurrenceMock = \Yii::$container->get('occurrenceMock');
        $plotMock = \Yii::$container->get('plotMock');
        $variableMock = \Yii::$container->get('variableMock');
        $terminalTransactionMock = \Yii::$container->get('terminalTransactionMock');
        $transactionDatasetMock = \Yii::$container->get('transactionDatasetMock');
        $userMock = \Yii::$container->get('userMock');
        $apiHelperMock = \Yii::$container->get('apiHelperMock');
        $crossBrowserModelMock = \Yii::$container->get('crossBrowserModelMock');
        $harvestManagerModelMock = \Yii::$container->get('harvestManagerModelMock');
        $occurrenceDetailsModelMock = \Yii::$container->get('occurrenceDetailsModelMock');
        $plotBrowserModelMock = \Yii::$container->get('plotBrowserModelMock');

        /**
         * Partially mock HarvestDataModel.
         * Replace buildDatePicker's return value.
         * 
         * This has to be mocked, as there is an issue with
         * the version compatibility of PHP and Composer that causes
         * the test to fail when buildDatePicker is not mocked.
         * More info about this in this thread: https://github.com/fxpio/composer-asset-plugin/issues/334
         * 
         * The error produced looks like this:
         *      [yii\base\InvalidArgumentException] The file or directory to be published does not exist: /home/jbantay/IRRIDev/cbdev/cb/vendor/bower/jquery/dist 
         * 
         * Manually renaming bower-asset to bower in the vendor directory solves the problem,
         * but is not an elegant solution, and should be avoided.
         */
        $this->harvestDataModel = $this->construct($this->harvestDataModel,
            [
                "cross" => $crossMock,
                "experiment" => $experimentMock,
                "occurrence" => $occurrenceMock,
                "plot" => $plotMock,
                "variable" => $variableMock,
                "terminalTransaction" => $terminalTransactionMock,
                "transactionDataset" => $transactionDatasetMock,
                "user" => $userMock,
                "apiHelper" => $apiHelperMock,
                "crossBrowserModel" => $crossBrowserModelMock,
                "harvestManagerModel" => $harvestManagerModelMock,
                "occurrenceDetailsModel" => $occurrenceDetailsModelMock,
                "plotBrowserModel" => $plotBrowserModelMock
            ], 
            [
            'buildDatePicker' => function () { 
                return "<placeholder>"; 
            }
        ]);

        $expected = '<div class="harvest-date-committed" style="padding-top:12px"><span title="Committed harvest date" class="badge center light-green darken-4 white-text">2021-09-20</span></div><div class="harvest-date-input"><placeholder><div>';

        // ACT
        // If intelephense is enabled, buildHarvestDateColumn may be tagged
        // as an "undefined method". Ignore this for now. The test should run just fine
        // despite the error message.
        $actual = $this->harvestDataModel->buildHarvestDateColumn($cmVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildHarvestMethodColumn: Test 1
     * Committed data is not set
     */
    public function testBuildHarvestMethodColumnTest1()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "fixed" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [
                        "Bulk" => "Bulk"
                    ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => false
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ]
                    ]
                ]
            ]
        ];
        $cmVarCompat = [
            'CROSS_METHOD_SELFING' => [
                'fixed' => [
                    'default' => [
                        'harvestDate',
                        'harvestMethod'
                    ]
                ]
            ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => null,
            "terminalHarvestMethod" => null,
            "state" => "fixed",
            "harvestStatus" => "NO_HARVEST"
        ];
        $actionId = "plots";

        // Mock dependencies
        $crossMock = \Yii::$container->get('crossMock');
        $experimentMock = \Yii::$container->get('experimentMock');
        $occurrenceMock = \Yii::$container->get('occurrenceMock');
        $plotMock = \Yii::$container->get('plotMock');
        $variableMock = \Yii::$container->get('variableMock');
        $terminalTransactionMock = \Yii::$container->get('terminalTransactionMock');
        $transactionDatasetMock = \Yii::$container->get('transactionDatasetMock');
        $userMock = \Yii::$container->get('userMock');
        $apiHelperMock = \Yii::$container->get('apiHelperMock');
        $crossBrowserModelMock = \Yii::$container->get('crossBrowserModelMock');
        $harvestManagerModelMock = \Yii::$container->get('harvestManagerModelMock');
        $occurrenceDetailsModelMock = \Yii::$container->get('occurrenceDetailsModelMock');
        $plotBrowserModelMock = \Yii::$container->get('plotBrowserModelMock');

        /**
         * Partially mock HarvestDataModel.
         * Replace buildDatePicker's return value.
         * 
         * This has to be mocked, as there is an issue with
         * the version compatibility of PHP and Composer that causes
         * the test to fail when buildDatePicker is not mocked.
         * More info about this in this thread: https://github.com/fxpio/composer-asset-plugin/issues/334
         * 
         * The error produced looks like this:
         *      [yii\base\InvalidArgumentException] The file or directory to be published does not exist: /home/jbantay/IRRIDev/cbdev/cb/vendor/bower/jquery/dist 
         * 
         * Manually renaming bower-asset to bower in the vendor directory solves the problem,
         * but is not an elegant solution, and should be avoided.
         */
        $this->harvestDataModel = $this->construct($this->harvestDataModel,
            [
                "cross" => $crossMock,
                "experiment" => $experimentMock,
                "occurrence" => $occurrenceMock,
                "plot" => $plotMock,
                "variable" => $variableMock,
                "terminalTransaction" => $terminalTransactionMock,
                "transactionDataset" => $transactionDatasetMock,
                "user" => $userMock,
                "apiHelper" => $apiHelperMock,
                "crossBrowserModel" => $crossBrowserModelMock,
                "harvestManagerModel" => $harvestManagerModelMock,
                "occurrenceDetailsModel" => $occurrenceDetailsModelMock,
                "plotBrowserModel" => $plotBrowserModelMock
            ], 
            [
            'buildSelect2' => function () { 
                return "<placeholder>"; 
            }
        ]);

        $expected = '<div class="harvest-method-committed"></div><div class="harvest-method-input" style="padding:5px 0px 5px 0px"><placeholder><div>';

        // ACT
        // If intelephense is enabled, buildHarvestDateColumn may be tagged
        // as an "undefined method". Ignore this for now. The test should run just fine
        // despite the error message.
        $actual = $this->harvestDataModel->buildHarvestMethodColumn($browserConfig, $cmVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildHarvestMethodColumn: Test 2
     * Committed data is set
     */
    public function testBuildHarvestMethodColumnTest2()
    {
        // ARRANGE
        // inputs
        $cmVarCompat = [
            'CROSS_METHOD_SELFING' => [
                'fixed' => [
                    'default' => [
                        'harvestDate',
                        'harvestMethod'
                    ]
                ]
            ]
        ];
        $browserConfig = [
            "fixed" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [
                        "Bulk" => "Bulk"
                    ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => false
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ]
                    ]
                ]
                
            ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => "Bulk",
            "terminalHarvestMethod" => null,
            "state" => "fixed",
            "harvestStatus" => "NO_HARVEST"
        ];
        $actionId = "plots";

        // Mock dependencies
        $crossMock = \Yii::$container->get('crossMock');
        $experimentMock = \Yii::$container->get('experimentMock');
        $occurrenceMock = \Yii::$container->get('occurrenceMock');
        $plotMock = \Yii::$container->get('plotMock');
        $variableMock = \Yii::$container->get('variableMock');
        $terminalTransactionMock = \Yii::$container->get('terminalTransactionMock');
        $transactionDatasetMock = \Yii::$container->get('transactionDatasetMock');
        $userMock = \Yii::$container->get('userMock');
        $apiHelperMock = \Yii::$container->get('apiHelperMock');
        $crossBrowserModelMock = \Yii::$container->get('crossBrowserModelMock');
        $harvestManagerModelMock = \Yii::$container->get('harvestManagerModelMock');
        $occurrenceDetailsModelMock = \Yii::$container->get('occurrenceDetailsModelMock');
        $plotBrowserModelMock = \Yii::$container->get('plotBrowserModelMock');

        /**
         * Partially mock HarvestDataModel.
         * Replace buildDatePicker's return value.
         * 
         * This has to be mocked, as there is an issue with
         * the version compatibility of PHP and Composer that causes
         * the test to fail when buildDatePicker is not mocked.
         * More info about this in this thread: https://github.com/fxpio/composer-asset-plugin/issues/334
         * 
         * The error produced looks like this:
         *      [yii\base\InvalidArgumentException] The file or directory to be published does not exist: /home/jbantay/IRRIDev/cbdev/cb/vendor/bower/jquery/dist 
         * 
         * Manually renaming bower-asset to bower in the vendor directory solves the problem,
         * but is not an elegant solution, and should be avoided.
         */
        $this->harvestDataModel = $this->construct($this->harvestDataModel,
            [
                "cross" => $crossMock,
                "experiment" => $experimentMock,
                "occurrence" => $occurrenceMock,
                "plot" => $plotMock,
                "variable" => $variableMock,
                "terminalTransaction" => $terminalTransactionMock,
                "transactionDataset" => $transactionDatasetMock,
                "user" => $userMock,
                "apiHelper" => $apiHelperMock,
                "crossBrowserModel" => $crossBrowserModelMock,
                "harvestManagerModel" => $harvestManagerModelMock,
                "occurrenceDetailsModel" => $occurrenceDetailsModelMock,
                "plotBrowserModel" => $plotBrowserModelMock
            ], 
            [
            'buildSelect2' => function () { 
                return "<placeholder>"; 
            }
        ]);

        // expected
        $expected = '<div class="harvest-method-committed"><span title="Committed harvest method" class="badge center light-green darken-4 white-text">Bulk</span></div><div class="harvest-method-input" style="padding-top:8px"><placeholder><div>';

        // ACT
        // If intelephense is enabled, buildHarvestDateColumn may be tagged
        // as an "undefined method". Ignore this for now. The test should run just fine
        // despite the error message.
        $actual = $this->harvestDataModel->buildHarvestMethodColumn($browserConfig, $cmVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildHarvestMethodColumn: Test 3
     * Stage is test cross verification (TCV)
     */
    public function testBuildHarvestMethodColumnTestWhenStageIsTestCrossVerification()
    {
        // ARRANGE
        // inputs
        $cmVarCompat = [
            'CROSS_METHOD_SELFING' => [
                'fixed' => [
                    'default' => [
                        'harvestDate',
                        'harvestMethod'
                    ]
                ]
            ]
        ];
        $browserConfig = [
            "TCV" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [ ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ]
                    ]
                ]
                
            ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => "Bulk",
            "terminalHarvestMethod" => null,
            "state" => "fixed",
            "stageCode" => "TCV",
            "harvestStatus" => "NO_HARVEST"
        ];
        $actionId = "plots";

        // Mock dependencies
        $crossMock = \Yii::$container->get('crossMock');
        $experimentMock = \Yii::$container->get('experimentMock');
        $occurrenceMock = \Yii::$container->get('occurrenceMock');
        $plotMock = \Yii::$container->get('plotMock');
        $variableMock = \Yii::$container->get('variableMock');
        $terminalTransactionMock = \Yii::$container->get('terminalTransactionMock');
        $transactionDatasetMock = \Yii::$container->get('transactionDatasetMock');
        $userMock = \Yii::$container->get('userMock');
        $apiHelperMock = \Yii::$container->get('apiHelperMock');
        $crossBrowserModelMock = \Yii::$container->get('crossBrowserModelMock');
        $harvestManagerModelMock = \Yii::$container->get('harvestManagerModelMock');
        $occurrenceDetailsModelMock = \Yii::$container->get('occurrenceDetailsModelMock');
        $plotBrowserModelMock = \Yii::$container->get('plotBrowserModelMock');

        /**
         * Partially mock HarvestDataModel.
         * Replace buildDatePicker's return value.
         * 
         * This has to be mocked, as there is an issue with
         * the version compatibility of PHP and Composer that causes
         * the test to fail when buildDatePicker is not mocked.
         * More info about this in this thread: https://github.com/fxpio/composer-asset-plugin/issues/334
         * 
         * The error produced looks like this:
         *      [yii\base\InvalidArgumentException] The file or directory to be published does not exist: /home/jbantay/IRRIDev/cbdev/cb/vendor/bower/jquery/dist 
         * 
         * Manually renaming bower-asset to bower in the vendor directory solves the problem,
         * but is not an elegant solution, and should be avoided.
         */
        $this->harvestDataModel = $this->construct($this->harvestDataModel,
            [
                "cross" => $crossMock,
                "experiment" => $experimentMock,
                "occurrence" => $occurrenceMock,
                "plot" => $plotMock,
                "variable" => $variableMock,
                "terminalTransaction" => $terminalTransactionMock,
                "transactionDataset" => $transactionDatasetMock,
                "user" => $userMock,
                "apiHelper" => $apiHelperMock,
                "crossBrowserModel" => $crossBrowserModelMock,
                "harvestManagerModel" => $harvestManagerModelMock,
                "occurrenceDetailsModel" => $occurrenceDetailsModelMock,
                "plotBrowserModel" => $plotBrowserModelMock
            ], 
            [
            'buildSelect2' => function () { 
                return "<placeholder>"; 
            }
        ]);

        // expected
        $expected = '<div class="harvest-method-committed"><span title="Committed harvest method" class="badge center light-green darken-4 white-text">Bulk</span></div><div class="harvest-method-input" style="padding-top:8px"><placeholder><div>';

        // ACT
        // If intelephense is enabled, buildHarvestDateColumn may be tagged
        // as an "undefined method". Ignore this for now. The test should run just fine
        // despite the error message.
        $actual = $this->harvestDataModel->buildHarvestMethodColumn($browserConfig, $cmVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildNumericVariableInput: Test 1
     * Harvest method is not empty
     */
    public function testBuildNumericVariableInputTest1()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "not_fixed" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [
                        "Bulk" => "Bulk",
                        "Panicle Selection" => "Panicle Selection",
                        "Plant-Specific and Bulk" => "Plant-Specific and Bulk",
                        "Plant-specific" => "Plant-specific",
                        "Single Plant Selection" => "Single Plant Selection",
                        "Single Plant Selection and Bulk" => "Single Plant Selection and Bulk",
                        "Single Seed Descent" => "Single Seed Descent"
                    ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => true
                        ],
                        [
                            "abbrev" => "<none>",
                            "column_name" => "harvestDate",
                            "required" => null
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "NO_OF_PLANTS",
                            "field_name" => "noOfPlant",
                            "min" => 1,
                            "placeholder" => "No. of plants",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "PANNO_SEL",
                            "field_name" => "noOfPanicle",
                            "min" => 1,
                            "placeholder" => "No. of panicles",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "SPECIFIC_PLANT",
                            "field_name" => "specificPlantNo",
                            "min" => 0,
                            "placeholder" => "Specific plant no.",
                            "sub_type" => "comma_sep_int",
                            "type" => "text"
                        ]
                    ]
                ]
            ]
        ];
        $methodNumVarCompat = [
            "Bulk" => [ "" ],
            "Panicle Selection" => [ "PANNO_SEL" ],
            "Plant-Specific and Bulk" => [ "SPEIFIC_PLANT" ],
            "Plant-specific" => [ "SPECIFIC_PLANT" ],
            "Single Plant Selection" => [ "NO_OF_PLANTS" ],
            "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
            "Single Seed Descent" => [ "" ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => "Single Plant Selection",
            "terminalHarvestMethod" => null,
            "noOfPlant" => 5,
            "terminalNoOfPlant" => null,
            "state" => "not_fixed",
            "harvestStatus" => "NO_HARVEST",
            "stageCode" => "ABC"
        ];
        $actionId = "plots";

        // expected
        $expected = '<div class="numeric-variable-committed" style="padding-top:12px"><span title="Committed No. of plants" style="margin-bottom:5px;" class="badge center light-green darken-4 white-text">No. of plants: 5</span></div><div class="numeric-variable"><input type="number" id="hm-numvar-id--numeric-var-placeholder--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--numeric-var-placeholder--plot-1234" value="" disabled title="Numeric variable is not applicable." placeholder="Not applicable" oninput="val = value||validity.valid||(value=val);" min="1"><input type="number" id="hm-numvar-id--NO_OF_PLANTS--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234" name="hm-numvar-id--NO_OF_PLANTS--plot-1234" title="" placeholder="No. of plants" oninput="val = value||validity.valid||(value=val);" min="1" subType="single_int"><input type="number" id="hm-numvar-id--PANNO_SEL--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--PANNO_SEL--plot-1234" title="" placeholder="No. of panicles" oninput="val = value||validity.valid||(value=val);" min="1" subType="single_int"><input type="text" id="hm-numvar-id--SPECIFIC_PLANT--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--SPECIFIC_PLANT--plot-1234" title="" placeholder="Specific plant no." oninput="val = value||validity.valid||(value=val);" onkeypress="return event.charCode == 44 || (event.charCode &gt;= 48 &amp;&amp; event.charCode &lt;= 57)" min="0" subType="comma_sep_int"><div>';

        // ACT
        $actual = $this->harvestDataModel->buildNumericVariableInput($browserConfig, $methodNumVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildNumericVariableInput: Test 2
     * Harvest method is empty
     */
    public function testBuildNumericVariableInputTest2()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "not_fixed" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [
                        "Bulk" => "Bulk",
                        "Panicle Selection" => "Panicle Selection",
                        "Plant-Specific and Bulk" => "Plant-Specific and Bulk",
                        "Plant-specific" => "Plant-specific",
                        "Single Plant Selection" => "Single Plant Selection",
                        "Single Plant Selection and Bulk" => "Single Plant Selection and Bulk",
                        "Single Seed Descent" => "Single Seed Descent"
                    ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => true
                        ],
                        [
                            "abbrev" => "<none>",
                            "column_name" => "harvestDate",
                            "required" => null
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "NO_OF_PLANTS",
                            "field_name" => "noOfPlant",
                            "min" => 1,
                            "placeholder" => "No. of plants",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "PANNO_SEL",
                            "field_name" => "noOfPanicle",
                            "min" => 1,
                            "placeholder" => "No. of panicles",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "SPECIFIC_PLANT",
                            "field_name" => "specificPlantNo",
                            "min" => 0,
                            "placeholder" => "Specific plant no.",
                            "sub_type" => "comma_sep_int",
                            "type" => "text"
                        ]
                    ]
                ]
            ]
        ];
        $methodNumVarCompat = [
            "Bulk" => [ "" ],
            "Panicle Selection" => [ "", "PANNO_SEL" ],
            "Plant-Specific and Bulk" => [ "SPEIFIC_PLANT" ],
            "Plant-specific" => [ "SPECIFIC_PLANT" ],
            "Single Plant Selection" => [ "NO_OF_PLANTS" ],
            "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
            "Single Seed Descent" => [ "" ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => null,
            "terminalHarvestMethod" => null,
            "noOfPlant" => null,
            "terminalNoOfPlant" => null,
            "state" => "not_fixed",
            "harvestStatus" => "NO_HARVEST",
            "stageCode" => "ABC"
        ];
        $actionId = "plots";

        // expected
        $expected = '<div class="numeric-variable-committed"></div><div class="numeric-variable-input"><input type="number" id="hm-numvar-id--numeric-var-placeholder--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234" name="hm-numvar-id--numeric-var-placeholder--plot-1234" value="" disabled title="Numeric variable is not applicable." placeholder="Not applicable" oninput="val = value||validity.valid||(value=val);" min="1"><input type="number" id="hm-numvar-id--NO_OF_PLANTS--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--NO_OF_PLANTS--plot-1234" title="" placeholder="No. of plants" oninput="val = value||validity.valid||(value=val);" min="1" subType="single_int"><input type="number" id="hm-numvar-id--PANNO_SEL--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--PANNO_SEL--plot-1234" title="" placeholder="No. of panicles" oninput="val = value||validity.valid||(value=val);" min="1" subType="single_int"><input type="text" id="hm-numvar-id--SPECIFIC_PLANT--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--SPECIFIC_PLANT--plot-1234" title="" placeholder="Specific plant no." oninput="val = value||validity.valid||(value=val);" onkeypress="return event.charCode == 44 || (event.charCode &gt;= 48 &amp;&amp; event.charCode &lt;= 57)" min="0" subType="comma_sep_int"><div>';

        // ACT
        $actual = $this->harvestDataModel->buildNumericVariableInput($browserConfig, $methodNumVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildNumericVariableInput: Test 3
     * Numeric variable is incompatible with the committed method
     */
    public function testBuildNumericVariableInputTest3()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "not_fixed" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [
                        "Bulk" => "Bulk",
                        "Panicle Selection" => "Panicle Selection",
                        "Plant-Specific and Bulk" => "Plant-Specific and Bulk",
                        "Plant-specific" => "Plant-specific",
                        "Single Plant Selection" => "Single Plant Selection",
                        "Single Plant Selection and Bulk" => "Single Plant Selection and Bulk",
                        "Single Seed Descent" => "Single Seed Descent"
                    ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => true
                        ],
                        [
                            "abbrev" => "<none>",
                            "column_name" => "harvestDate",
                            "required" => null
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "NO_OF_PLANTS",
                            "field_name" => "noOfPlant",
                            "min" => 1,
                            "placeholder" => "No. of plants",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "PANNO_SEL",
                            "field_name" => "noOfPanicle",
                            "min" => 1,
                            "placeholder" => "No. of panicles",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "SPECIFIC_PLANT",
                            "field_name" => "specificPlantNo",
                            "min" => 0,
                            "placeholder" => "Specific plant no.",
                            "sub_type" => "comma_sep_int",
                            "type" => "text"
                        ]
                    ]
                ]
            ]
        ];
        $methodNumVarCompat = [
            "Bulk" => [ "" ],
            "Panicle Selection" => [ "", "PANNO_SEL" ],
            "Plant-Specific and Bulk" => [ "SPEIFIC_PLANT" ],
            "Plant-specific" => [ "SPECIFIC_PLANT" ],
            "Single Plant Selection" => [ "NO_OF_PLANTS" ],
            "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
            "Single Seed Descent" => [ "" ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => "Single Plant Selection",
            "terminalHarvestMethod" => null,
            "noOfPlant" => null,
            "specificPlantNo" => "2,3,4",
            "terminalNoOfPlant" => null,
            "state" => "not_fixed",
            "harvestStatus" => "NO_HARVEST",
            "stageCode" => "ABC"
        ];
        $actionId = "plots";

        // expected
        $expected = '<div class="numeric-variable-committed" style="padding-top:12px"><span title="Committed Specific plant no. (Incompatible with committed method: Single Plant Selection)" style="margin-bottom:5px;" class="badge center red lighten-2 white-text">Specific plant no.: 2,3,4</span></div><div class="numeric-variable"><input type="number" id="hm-numvar-id--numeric-var-placeholder--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--numeric-var-placeholder--plot-1234" value="" disabled title="Numeric variable is not applicable." placeholder="Not applicable" oninput="val = value||validity.valid||(value=val);" min="1"><input type="number" id="hm-numvar-id--NO_OF_PLANTS--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234" name="hm-numvar-id--NO_OF_PLANTS--plot-1234" title="" placeholder="No. of plants" oninput="val = value||validity.valid||(value=val);" min="1" subType="single_int"><input type="number" id="hm-numvar-id--PANNO_SEL--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--PANNO_SEL--plot-1234" title="" placeholder="No. of panicles" oninput="val = value||validity.valid||(value=val);" min="1" subType="single_int"><input type="text" id="hm-numvar-id--SPECIFIC_PLANT--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--SPECIFIC_PLANT--plot-1234" title="" placeholder="Specific plant no." oninput="val = value||validity.valid||(value=val);" onkeypress="return event.charCode == 44 || (event.charCode &gt;= 48 &amp;&amp; event.charCode &lt;= 57)" min="0" subType="comma_sep_int"><div>';

        // ACT
        $actual = $this->harvestDataModel->buildNumericVariableInput($browserConfig, $methodNumVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildNumericVariableInput: Test 4
     * Numeric variable is irrelevant to the committed method
     */
    public function testBuildNumericVariableInputTest4()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "not_fixed" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [
                        "Bulk" => "Bulk",
                        "Panicle Selection" => "Panicle Selection",
                        "Plant-Specific and Bulk" => "Plant-Specific and Bulk",
                        "Plant-specific" => "Plant-specific",
                        "Single Plant Selection" => "Single Plant Selection",
                        "Single Plant Selection and Bulk" => "Single Plant Selection and Bulk",
                        "Single Seed Descent" => "Single Seed Descent"
                    ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => true
                        ],
                        [
                            "abbrev" => "<none>",
                            "column_name" => "harvestDate",
                            "required" => null
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "NO_OF_PLANTS",
                            "field_name" => "noOfPlant",
                            "min" => 1,
                            "placeholder" => "No. of plants",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "PANNO_SEL",
                            "field_name" => "noOfPanicle",
                            "min" => 1,
                            "placeholder" => "No. of panicles",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ],
                        [
                            "abbrev" => "SPECIFIC_PLANT",
                            "field_name" => "specificPlantNo",
                            "min" => 0,
                            "placeholder" => "Specific plant no.",
                            "sub_type" => "comma_sep_int",
                            "type" => "text"
                        ]
                    ]
                ]
            ]
        ];
        $methodNumVarCompat = [
            "Bulk" => [ "" ],
            "Panicle Selection" => [ "", "PANNO_SEL" ],
            "Plant-Specific and Bulk" => [ "SPEIFIC_PLANT" ],
            "Plant-specific" => [ "SPECIFIC_PLANT" ],
            "Single Plant Selection" => [ "NO_OF_PLANTS" ],
            "Single Plant Selection and Bulk" => [ "NO_OF_PLANTS" ],
            "Single Seed Descent" => [ "" ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => "Bulk",
            "terminalHarvestMethod" => null,
            "noOfPlant" => null,
            "specificPlantNo" => null,
            "noOfPanicle" => "3",
            "terminalNoOfPlant" => null,
            "state" => "not_fixed",
            "harvestStatus" => "NO_HARVEST",
            "stageCode" => "ABC"
        ];
        $actionId = "plots";

        // expected
        $expected = '<div class="numeric-variable-committed" style="padding-top:12px"><span title="Committed No. of panicles (Irrelevant to the committed method: Bulk)" style="margin-bottom:5px;" class="badge center light-grey darken-4 white-text">No. of panicles: 3</span></div><div class="numeric-variable"><input type="number" id="hm-numvar-id--numeric-var-placeholder--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234" name="hm-numvar-id--numeric-var-placeholder--plot-1234" value="" disabled title="Numeric variable is not applicable." placeholder="Not applicable" oninput="val = value||validity.valid||(value=val);" min="1"><input type="number" id="hm-numvar-id--NO_OF_PLANTS--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--NO_OF_PLANTS--plot-1234" title="" placeholder="No. of plants" oninput="val = value||validity.valid||(value=val);" min="1" subType="single_int"><input type="number" id="hm-numvar-id--PANNO_SEL--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--PANNO_SEL--plot-1234" title="" placeholder="No. of panicles" oninput="val = value||validity.valid||(value=val);" min="1" subType="single_int"><input type="text" id="hm-numvar-id--SPECIFIC_PLANT--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234 hidden" name="hm-numvar-id--SPECIFIC_PLANT--plot-1234" title="" placeholder="Specific plant no." oninput="val = value||validity.valid||(value=val);" onkeypress="return event.charCode == 44 || (event.charCode &gt;= 48 &amp;&amp; event.charCode &lt;= 57)" min="0" subType="comma_sep_int"><div>';

        // ACT
        $actual = $this->harvestDataModel->buildNumericVariableInput($browserConfig, $methodNumVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildNumericVariableInput: Test 5
     * When there are no numeric variables in the config
     */
    public function testBuildNumericVariableInputTestWhenNoNumVarsAreSupported()
    {
        // ARRANGE
        // inputs
        $browserConfig = [ ];
        $methodNumVarCompat = [ ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => null,
            "terminalHarvestMethod" => null,
            "noOfPlant" => null,
            "specificPlantNo" => null,
            "noOfPanicle" => null,
            "terminalNoOfPlant" => null,
            "state" => "unsupported",
            "harvestStatus" => "NO_HARVEST",
            "stageCode" => "ABC"
        ];
        $actionId = "plots";

        // expected
        $expected = '<div class="numeric-variable-committed"></div><div class="numeric-variable-input"><input type="number" id="hm-numvar-id--numeric-var-placeholder--plot-1234" class="hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--plot-1234" name="hm-numvar-id--numeric-var-placeholder--plot-1234" value="" disabled title="Numeric variable input is not supported." placeholder="Not applicable" oninput="val = value||validity.valid||(value=val);" min="1"><div>';

        // ACT
        $actual = $this->harvestDataModel->buildNumericVariableInput($browserConfig, $methodNumVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildGrainColorColumn: Test 1
     * Committed grain color is set
     */
    public function testBuildGrainColorColumnWhenCommittedValueIsSet()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "fixed" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [
                        "Bulk" => "Bulk"
                    ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => false
                        ],
                        [
                            "abbrev" => "GRAIN_COLOR",
                            "column_name" => "grainColor",
                            "required" => false
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ]
                    ]
                ]
            ]
        ];
        $cmVarCompat = [
            'CROSS_METHOD_SELFING' => [
                'fixed' => [
                    'default' => [
                        'harvestDate',
                        'harvestMethod',
                        'grainColor'
                    ]
                ]
            ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => null,
            "terminalHarvestMethod" => null,
            "grainColor" => "WG",
            "terminalGrainColor" => "RG",
            "state" => "fixed",
            "harvestStatus" => "NO_HARVEST"
        ];
        $actionId = "plots";

        // Mock dependencies
        $crossMock = \Yii::$container->get('crossMock');
        $experimentMock = \Yii::$container->get('experimentMock');
        $occurrenceMock = \Yii::$container->get('occurrenceMock');
        $plotMock = \Yii::$container->get('plotMock');
        $variableMock = \Yii::$container->get('variableMock');
        $terminalTransactionMock = \Yii::$container->get('terminalTransactionMock');
        $transactionDatasetMock = \Yii::$container->get('transactionDatasetMock');
        $userMock = \Yii::$container->get('userMock');
        $apiHelperMock = \Yii::$container->get('apiHelperMock');
        $crossBrowserModelMock = \Yii::$container->get('crossBrowserModelMock');
        $harvestManagerModelMock = \Yii::$container->get('harvestManagerModelMock');
        $occurrenceDetailsModelMock = \Yii::$container->get('occurrenceDetailsModelMock');
        $plotBrowserModelMock = \Yii::$container->get('plotBrowserModelMock');

        /**
         * Partially mock HarvestDataModel.
         * Replace buildDatePicker's return value.
         * 
         * This has to be mocked, as there is an issue with
         * the version compatibility of PHP and Composer that causes
         * the test to fail when buildDatePicker is not mocked.
         * More info about this in this thread: https://github.com/fxpio/composer-asset-plugin/issues/334
         * 
         * The error produced looks like this:
         *      [yii\base\InvalidArgumentException] The file or directory to be published does not exist: /home/jbantay/IRRIDev/cbdev/cb/vendor/bower/jquery/dist 
         * 
         * Manually renaming bower-asset to bower in the vendor directory solves the problem,
         * but is not an elegant solution, and should be avoided.
         */
        $this->harvestDataModel = $this->construct($this->harvestDataModel,
            [
                "cross" => $crossMock,
                "experiment" => $experimentMock,
                "occurrence" => $occurrenceMock,
                "plot" => $plotMock,
                "variable" => $variableMock,
                "terminalTransaction" => $terminalTransactionMock,
                "transactionDataset" => $transactionDatasetMock,
                "user" => $userMock,
                "apiHelper" => $apiHelperMock,
                "crossBrowserModel" => $crossBrowserModelMock,
                "harvestManagerModel" => $harvestManagerModelMock,
                "occurrenceDetailsModel" => $occurrenceDetailsModelMock,
                "plotBrowserModel" => $plotBrowserModelMock
            ], 
            [
            'buildSelect2' => function () { 
                return "<placeholder>"; 
            }
        ]);

        $expected = '<div class="grain-color-committed"><span title="Committed grain color" class="badge center light-green darken-4 white-text">WG</span></div><div class="grain-color-input" style="padding-top:8px"><placeholder><div>';

        // ACT
        // If intelephense is enabled, buildHarvestDateColumn may be tagged
        // as an "undefined method". Ignore this for now. The test should run just fine
        // despite the error message.
        $actual = $this->harvestDataModel->buildGrainColorColumn($browserConfig, $cmVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);   
    }

    /**
     * buildGrainColorColumn: Test 2
     * Committed grain color is NOT set
     */
    public function testBuildGrainColorColumnWhenCommittedGrainColorIsNotSet()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "fixed" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [
                        "Bulk" => "Bulk"
                    ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => false
                        ],
                        [
                            "abbrev" => "GRAIN_COLOR",
                            "column_name" => "grainColor",
                            "required" => false
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ]
                    ]
                ]
            ]
        ];
        $cmVarCompat = [
            'CROSS_METHOD_SELFING' => [
                'fixed' => [
                    'default' => [
                        'harvestDate',
                        'harvestMethod',
                        'grainColor'
                    ]
                ]
            ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => null,
            "terminalHarvestMethod" => null,
            "grainColor" => null,
            "terminalGrainColor" => "RG",
            "state" => "fixed",
            "harvestStatus" => "NO_HARVEST"
        ];
        $actionId = "plots";

        // Mock dependencies
        $crossMock = \Yii::$container->get('crossMock');
        $experimentMock = \Yii::$container->get('experimentMock');
        $occurrenceMock = \Yii::$container->get('occurrenceMock');
        $plotMock = \Yii::$container->get('plotMock');
        $variableMock = \Yii::$container->get('variableMock');
        $terminalTransactionMock = \Yii::$container->get('terminalTransactionMock');
        $transactionDatasetMock = \Yii::$container->get('transactionDatasetMock');
        $userMock = \Yii::$container->get('userMock');
        $apiHelperMock = \Yii::$container->get('apiHelperMock');
        $crossBrowserModelMock = \Yii::$container->get('crossBrowserModelMock');
        $harvestManagerModelMock = \Yii::$container->get('harvestManagerModelMock');
        $occurrenceDetailsModelMock = \Yii::$container->get('occurrenceDetailsModelMock');
        $plotBrowserModelMock = \Yii::$container->get('plotBrowserModelMock');

        /**
         * Partially mock HarvestDataModel.
         * Replace buildDatePicker's return value.
         * 
         * This has to be mocked, as there is an issue with
         * the version compatibility of PHP and Composer that causes
         * the test to fail when buildDatePicker is not mocked.
         * More info about this in this thread: https://github.com/fxpio/composer-asset-plugin/issues/334
         * 
         * The error produced looks like this:
         *      [yii\base\InvalidArgumentException] The file or directory to be published does not exist: /home/jbantay/IRRIDev/cbdev/cb/vendor/bower/jquery/dist 
         * 
         * Manually renaming bower-asset to bower in the vendor directory solves the problem,
         * but is not an elegant solution, and should be avoided.
         */
        $this->harvestDataModel = $this->construct($this->harvestDataModel,
            [
                "cross" => $crossMock,
                "experiment" => $experimentMock,
                "occurrence" => $occurrenceMock,
                "plot" => $plotMock,
                "variable" => $variableMock,
                "terminalTransaction" => $terminalTransactionMock,
                "transactionDataset" => $transactionDatasetMock,
                "user" => $userMock,
                "apiHelper" => $apiHelperMock,
                "crossBrowserModel" => $crossBrowserModelMock,
                "harvestManagerModel" => $harvestManagerModelMock,
                "occurrenceDetailsModel" => $occurrenceDetailsModelMock,
                "plotBrowserModel" => $plotBrowserModelMock
            ], 
            [
            'buildSelect2' => function () { 
                return "<placeholder>"; 
            }
        ]);

        $expected = '<div class="harvest-method-committed"></div><div class="harvest-method-input" style="padding:5px 0px 5px 0px"><placeholder><div>';

        // ACT
        // If intelephense is enabled, buildHarvestDateColumn may be tagged
        // as an "undefined method". Ignore this for now. The test should run just fine
        // despite the error message.
        $actual = $this->harvestDataModel->buildGrainColorColumn($browserConfig, $cmVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);   
    }

    /**
     * buildGrainColorColumn: Test 3
     * Stage is test cross verification
     */
    public function testBuildGrainColorColumnWhenStageIsTestCrossVerification()
    {
        // ARRANGE
        // inputs
        $browserConfig = [
            "fixed" => [
                "default" => [
                    "additional_required_variables" => [],
                    "harvest_methods" => [
                        "Bulk" => "Bulk"
                    ],
                    "inputColumns" => [
                        [
                            "abbrev" => "HVDATE_CONT",
                            "column_name" => "harvestDate",
                            "required" => true
                        ],
                        [
                            "abbrev" => "HV_METH_DISC",
                            "column_name" => "harvestMethod",
                            "required" => false
                        ],
                        [
                            "abbrev" => "GRAIN_COLOR",
                            "column_name" => "grainColor",
                            "required" => false
                        ]
                    ],
                    "numeric_variables" => [
                        [
                            "abbrev" => "<none>",
                            "disabled" => true,
                            "field_name" => "<none>",
                            "min" => 0,
                            "placeholder" => "Not applicable",
                            "sub_type" => "single_int",
                            "type" => "number"
                        ]
                    ]
                ]
            ]
        ];
        $cmVarCompat = [
            'CROSS_METHOD_SELFING' => [
                'fixed' => [
                    'default' => [
                        'harvestDate',
                        'harvestMethod',
                        'grainColor'
                    ]
                ]
            ]
        ];
        $model = [
            "plotDbId" => 1234,
            "harvestMethod" => null,
            "terminalHarvestMethod" => null,
            "grainColor" => null,
            "terminalGrainColor" => "RG",
            "state" => "fixed",
            "stageCode" => "TCV",
            "harvestStatus" => "NO_HARVEST"
        ];
        $actionId = "plots";

        // Mock dependencies
        $crossMock = \Yii::$container->get('crossMock');
        $experimentMock = \Yii::$container->get('experimentMock');
        $occurrenceMock = \Yii::$container->get('occurrenceMock');
        $plotMock = \Yii::$container->get('plotMock');
        $variableMock = \Yii::$container->get('variableMock');
        $terminalTransactionMock = \Yii::$container->get('terminalTransactionMock');
        $transactionDatasetMock = \Yii::$container->get('transactionDatasetMock');
        $userMock = \Yii::$container->get('userMock');
        $apiHelperMock = \Yii::$container->get('apiHelperMock');
        $crossBrowserModelMock = \Yii::$container->get('crossBrowserModelMock');
        $harvestManagerModelMock = \Yii::$container->get('harvestManagerModelMock');
        $occurrenceDetailsModelMock = \Yii::$container->get('occurrenceDetailsModelMock');
        $plotBrowserModelMock = \Yii::$container->get('plotBrowserModelMock');

        /**
         * Partially mock HarvestDataModel.
         * Replace buildDatePicker's return value.
         * 
         * This has to be mocked, as there is an issue with
         * the version compatibility of PHP and Composer that causes
         * the test to fail when buildDatePicker is not mocked.
         * More info about this in this thread: https://github.com/fxpio/composer-asset-plugin/issues/334
         * 
         * The error produced looks like this:
         *      [yii\base\InvalidArgumentException] The file or directory to be published does not exist: /home/jbantay/IRRIDev/cbdev/cb/vendor/bower/jquery/dist 
         * 
         * Manually renaming bower-asset to bower in the vendor directory solves the problem,
         * but is not an elegant solution, and should be avoided.
         */
        $this->harvestDataModel = $this->construct($this->harvestDataModel,
            [
                "cross" => $crossMock,
                "experiment" => $experimentMock,
                "occurrence" => $occurrenceMock,
                "plot" => $plotMock,
                "variable" => $variableMock,
                "terminalTransaction" => $terminalTransactionMock,
                "transactionDataset" => $transactionDatasetMock,
                "user" => $userMock,
                "apiHelper" => $apiHelperMock,
                "crossBrowserModel" => $crossBrowserModelMock,
                "harvestManagerModel" => $harvestManagerModelMock,
                "occurrenceDetailsModel" => $occurrenceDetailsModelMock,
                "plotBrowserModel" => $plotBrowserModelMock
            ], 
            [
            'buildSelect2' => function () { 
                return "<placeholder>"; 
            }
        ]);

        $expected = '<div class="harvest-method-committed"></div><div class="harvest-method-input" style="padding:5px 0px 5px 0px"><placeholder><div>';

        // ACT
        // If intelephense is enabled, buildHarvestDateColumn may be tagged
        // as an "undefined method". Ignore this for now. The test should run just fine
        // despite the error message.
        $actual = $this->harvestDataModel->buildGrainColorColumn($browserConfig, $cmVarCompat, $model, $actionId);

        // ASSERT
        $this->assertEquals($expected, $actual);   
    }

    /**
     * getTransactionOfLocation: Test 1
     * Transaction exists for location
     */
    public function testGetTransactionOfLocationTest1()
    {
        // ARRANGE
        // input
        $locationId = 1234;

        // mocks
        $returnValue = 200;
        $this->harvestDataModel->user->setReturnValue('getUserId', $returnValue);

        $returnValue = [
            "data" => [
                [
                    "transactionDbId" => 456
                ]
            ]
        ];
        $this->harvestDataModel->terminalTransaction->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = 456;

        // ACT
        $actual = $this->harvestDataModel->getTransactionOfLocation($locationId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getTransactionOfLocation: Test 2
     * No transaction yet for location (CREATE)
     */
    public function testGetTransactionOfLocationTest2()
    {
        // ARRANGE
        // input
        $locationId = 1234;

        // mocks
        $returnValue = 200;
        $this->harvestDataModel->user->setReturnValue('getUserId', $returnValue);

        $returnValue = [
            "data" => [
                [
                    "transactionDbId" => 0
                ]
            ]
        ];
        $this->harvestDataModel->terminalTransaction->setReturnValue('searchAll', $returnValue);

        $returnValue = [
            "data" => [
                [
                    "transactionDbId" => 888
                ]
            ]
        ];
        $this->harvestDataModel->terminalTransaction->setReturnValue('create', $returnValue);

        // expected
        $expected = 888;

        // ACT
        $actual = $this->harvestDataModel->getTransactionOfLocation($locationId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDataset: Test 1
     * Get simple dataset
     */
    public function testGetDatasetTest1()
    {
        // ARRANGE
        // input
        $entityId = 222;
        $variableId = 555;
        $transactionId = 3434;

        // mocks
        $returnValue = [
            [
                "datasetDbId" => 100
            ]
        ];
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        // expected
        $expected = [
            "datasetDbId" => 100
        ];

        // ACT
        $actual = $this->harvestDataModel->getDataset($entityId, $variableId, $transactionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * updateOccurrenceOfTransaction: Test 1
     * Empty dataset summary
     */
    public function testUpdateOccurrenceOfTransactionTest1()
    {
        // ARRANGE
        // input
        $transactionId = 1;

        // mocks
        $returnValue = [];
        $this->harvestDataModel->terminalTransaction->setReturnValue('getDatasetSummary', $returnValue);

        // expected
        $expected = true;

        // ACT
        $actual = $this->harvestDataModel->updateOccurrenceOfTransaction($transactionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * updateOccurrenceOfTransaction: Test 2
     * With dataset summary, update success
     */
    public function testUpdateOccurrenceOfTransactionTest2()
    {
        // ARRANGE
        // input
        $transactionId = 1;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrences" => [
                        [
                            "variableDbIds" => "1,2,3,4",
                            "occurrenceDbId" => "1000",
                            "dataUnit" => "plots",
                            "dataUnitCount" => 10
                        ]
                    ]
                ]
            ]
        ];
        $this->harvestDataModel->terminalTransaction->setReturnValue('getDatasetSummary', $returnValue);

        $returnValue = [
            "success" => true
        ];
        $this->harvestDataModel->apiHelper->setReturnValue('getParsedResponse', $returnValue);

        // expected
        $expected = true;

        // ACT
        $actual = $this->harvestDataModel->updateOccurrenceOfTransaction($transactionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * updateOccurrenceOfTransaction: Test 3
     * With dataset summary, update failed
     */
    public function testUpdateOccurrenceOfTransactionTest3()
    {
        // ARRANGE
        // input
        $transactionId = 1;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrences" => [
                        [
                            "variableDbIds" => "1,2,3,4",
                            "occurrenceDbId" => "1000",
                            "dataUnit" => "plots",
                            "dataUnitCount" => 10
                        ]
                    ]
                ]
            ]
        ];
        $this->harvestDataModel->terminalTransaction->setReturnValue('getDatasetSummary', $returnValue);

        $returnValue = [
            "success" => false
        ];
        $this->harvestDataModel->apiHelper->setReturnValue('getParsedResponse', $returnValue);

        // expected
        $expected = false;

        // ACT
        $actual = $this->harvestDataModel->updateOccurrenceOfTransaction($transactionId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * insertDataset: Test 1
     * Insert Value
     */
    public function testInsertDatasetTest1()
    {
        // ARRANGE
        // inputs
        $locationId = 11;
        $variableAbbrevs = [ "HVDATE_CONT" ];
        $values = [ [ "2020-07-24" ] ];
        $recordIds = [ 123 ];
        $idType = "plot";

        // mocks
        $returnValue = 22;
        $this->harvestDataModel->user->setReturnValue('getUserId', $returnValue);
        $returnValue = [
            "data" => [
                [
                    "transactionDbId" => 456
                ]
            ]
        ];
        $this->harvestDataModel->terminalTransaction->setReturnValue('searchAll', $returnValue);

        $returnValue = [
            [
                "success" => true
            ]
        ];
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        $returnValue = [
            "return_vals" => [
                [
                    [
                        "success" => true
                    ]
                ],
                [
                    [
                        "success" => true
                    ]
                ],
                []
            ]
        ];
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', $returnValue);
        $this->harvestDataModel->terminalTransaction->setReturnValue('updateOne', $returnValue);

        $returnValue = [];
        $this->harvestDataModel->terminalTransaction->setReturnValue('getDatasetSummary', $returnValue);
        $this->harvestDataModel->terminalTransaction->setReturnValue('deleteOne', $returnValue);

        // expected
        $expected = true;

        // ACT
        $actual = $this->harvestDataModel->insertDataset($locationId, $variableAbbrevs, $values, $recordIds, $idType);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * insertDataset: Test 2
     * Insert empty value
     */
    public function testInsertDatasetTest2()
    {
        // ARRANGE
        // inputs
        $locationId = 11;
        $variableAbbrevs = [ "HVDATE_CONT" ];
        $values = [ [ "" ] ];
        $recordIds = [ 123 ];
        $idType = "plot";

        // mocks
        $returnValue = 22;
        $this->harvestDataModel->user->setReturnValue('getUserId', $returnValue);
        $returnValue = [
            "data" => [
                [
                    "transactionDbId" => 456
                ]
            ]
        ];
        $this->harvestDataModel->terminalTransaction->setReturnValue('searchAll', $returnValue);

        $returnValue = [
            [
                "success" => true
            ]
        ];
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', $returnValue);

        $returnValue = [
            "return_vals" => [
                [
                    [
                        "success" => true
                    ]
                ],
                [
                    [
                        "success" => true
                    ]
                ],
                []
            ]
        ];
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', $returnValue);
        $this->harvestDataModel->terminalTransaction->setReturnValue('updateOne', $returnValue);

        $returnValue = [];
        $this->harvestDataModel->terminalTransaction->setReturnValue('getDatasetSummary', $returnValue);
        $this->harvestDataModel->terminalTransaction->setReturnValue('deleteOne', $returnValue);

        $returnValue = 123;
        $this->harvestDataModel->harvestManagerModel->setReturnValue('getVariableId', $returnValue);

        // expected
        $expected = false;

        // ACT
        $actual = $this->harvestDataModel->insertDataset($locationId, $variableAbbrevs, $values, $recordIds, $idType);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkHarvestStatusAndState: Test 1
     * State = 'unknown' (unsupported)
     */
    public function testCheckHarvestStatusAndStateWhenStateEqualsUnknown() {
        // ARRANGE
        // inputs
        $records = [
            [
                "harvestStatus" => "NO_HARVEST",
                "state" => "unknown"
            ]
        ];

        // expected
        $expected = [
            'statusList' => [],
            'completedCount' => 0,
            'deletionCount' => 0,
            'stateList' => [ "unknown" ],
            'invalidStateCount' => 1,
            'totalUnupdateable' => 1
        ];

        // ACT
        $actual = $this->harvestDataModel->checkHarvestStatusAndState($records);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkHarvestStatusAndState: Test 2
     * Harvest status = COMPLETED
     */
    public function testCheckHarvestStatusAndStateWhenHarvestStatusEqualsCompleted() {
        // ARRANGE
        // inputs
        $records = [
            [
                "harvestStatus" => "COMPLETED",
                "state" => "fixed"
            ]
        ];

        // expected
        $expected = [
            'statusList' => [ "COMPLETED" ],
            'completedCount' => 1,
            'deletionCount' => 0,
            'stateList' => [],
            'invalidStateCount' => 0,
            'totalUnupdateable' => 1
        ];

        // ACT
        $actual = $this->harvestDataModel->checkHarvestStatusAndState($records);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkHarvestStatusAndState: Test 3
     * Harvest status = DELETION_IN_PROGRESS
     */
    public function testCheckHarvestStatusAndStateWhenHarvestStatusEqualsDeletionInProgress() {
        // ARRANGE
        // inputs
        $records = [
            [
                "harvestStatus" => "DELETION_IN_PROGRESS",
                "state" => null
            ]
        ];

        // expected
        $expected = [
            'statusList' => [ "DELETION_IN_PROGRESS" ],
            'completedCount' => 0,
            'deletionCount' => 1,
            'stateList' => [],
            'invalidStateCount' => 0,
            'totalUnupdateable' => 1
        ];

        // ACT
        $actual = $this->harvestDataModel->checkHarvestStatusAndState($records);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getRecordHarvestStatusAndState: Test 1
     * data level = plot
     */
    public function testGetRecordHarvestStatusAndStateWhenDataLevelEqualsPlot() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $dataLevel = "plot";
        $recordIds = [ "1", "2", "3" ];

        // mocks
        $returnValue = [
            [
                "plotDbId" => 1,
                "harvestStatus" => "NO_HARVEST",
                "state" => "fixed"
            ],
            [
                "plotDbId" => 2,
                "harvestStatus" => "COMPLETED",
                "state" => "not_fixed"
            ],
            [
                "plotDbId" => 3,
                "harvestStatus" => "DELETION_HARVEST_IN_PROGRESS",
                "state" => null
            ]
        ];
        $this->harvestDataModel->plotBrowserModel->setReturnValue('searchAllPlots', $returnValue);

        // expected
        $expected = [
            [
                "plotDbId" => 1,
                "harvestStatus" => "NO_HARVEST",
                "state" => "fixed"
            ],
            [
                "plotDbId" => 2,
                "harvestStatus" => "COMPLETED",
                "state" => "not_fixed"
            ],
            [
                "plotDbId" => 3,
                "harvestStatus" => "DELETION_HARVEST_IN_PROGRESS",
                "state" => null
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getRecordHarvestStatusAndState($occurrenceId, $dataLevel, $recordIds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getRecordHarvestStatusAndState: Test 2
     * data level = cross
     */
    public function testGetRecordHarvestStatusAndStateWhenDataLevelEqualsCross() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $dataLevel = "cross";
        $recordIds = [ "1", "2", "3" ];

        // mocks
        $returnValue = [
            [
                "crossDbId" => 1,
                "harvestStatus" => "NO_HARVEST",
                "state" => "fixed"
            ],
            [
                "crossDbId" => 2,
                "harvestStatus" => "COMPLETED",
                "state" => "not_fixed"
            ],
            [
                "crossDbId" => 3,
                "harvestStatus" => "DELETION_HARVEST_IN_PROGRESS",
                "state" => null
            ]
        ];
        $this->harvestDataModel->crossBrowserModel->setReturnValue('searchAllCrosses', $returnValue);

        // expected
        $expected = [
            [
                "crossDbId" => 1,
                "harvestStatus" => "NO_HARVEST",
                "state" => "fixed"
            ],
            [
                "crossDbId" => 2,
                "harvestStatus" => "COMPLETED",
                "state" => "not_fixed"
            ],
            [
                "crossDbId" => 3,
                "harvestStatus" => "DELETION_HARVEST_IN_PROGRESS",
                "state" => null
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getRecordHarvestStatusAndState($occurrenceId, $dataLevel, $recordIds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildNumVarInput: Test 1
     * sub type = single_int
     */
    public function testBuildNumVarInputWhenSubtypeEqualsSingle_Int() {
        // ARRANGE
        // input
        $numVarConfig = [
            'abbrev' => 'PANNOSEL',
            'type' => 'number',
            'sub_type' => 'single_int',
            'placeholder' => 'No. of panicles',
            'min' => 1
        ];
        $idBase = 'test-id-base';
        $class = 'test-class';

        // expected
        $expected = '<div class="col-md-12 test-class-div" id="test-id-base-PANNOSEL-div" style="font-size:14px">
                    <dl>
                        <dt title="No. of panicles" style="margin-bottom:5px">No. of panicles</dt>
                        <dd><input
                        id="test-id-base-PANNOSEL" 
                        class="test-class"
                        variableAbbrev="PANNOSEL"
                        min="1"
                        subType="single_int"
                        step="1"
                        placeholder="No. of panicles"
                        onkeypress="return event.charCode >= 48 && event.charCode <= 57" 
                        type="number"></dd>
                    </dl>
                </div>';

        // ACT
        $actual = $this->harvestDataModel->buildNumVarInput($numVarConfig, $idBase, $class);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildNumVarInput: Test 2
     * sub type = comma_sep_int
     */
    public function testBuildNumVarInputWhenSubtypeEqualsComma_Sep_Int() {
        // ARRANGE
        // input
        $numVarConfig = [
            'abbrev' => 'SPECIFIC_PLANT',
            'type' => 'text',
            'sub_type' => 'comma_sep_int',
            'placeholder' => 'Specific plant no.',
            'min' => null
        ];
        $idBase = 'test-id-base';
        $class = 'test-class';

        // expected
        $expected = '<div class="col-md-12 test-class-div" id="test-id-base-SPECIFIC_PLANT-div" style="font-size:14px">
                    <dl>
                        <dt title="Specific plant no." style="margin-bottom:5px">Specific plant no.</dt>
                        <dd><input
                        id="test-id-base-SPECIFIC_PLANT" 
                        class="test-class"
                        variableAbbrev="SPECIFIC_PLANT"
                        min=""
                        subType="comma_sep_int"
                        step="1"
                        placeholder="Specific plant no."
                        onkeypress="return event.charCode == 44 || (event.charCode >= 48 && event.charCode <= 57)" 
                        type="text"></dd>
                    </dl>
                </div>';

        // ACT
        $actual = $this->harvestDataModel->buildNumVarInput($numVarConfig, $idBase, $class);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getAllRecordsCount: Test 1
     * dataLevel = cross
     */
    public function testGetAllRecordsCountWhenDataLevelIsCross() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $dataLevel = 'cross';

        // mock
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) {
                return [
                    'filter' => [],
                    'sort' => ''
                ];
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        $returnValue = [];
        $this->harvestDataModel->crossBrowserModel->setReturnValue('assembleBrowserFilters',$returnValue);
        $returnValue = [
            "count" => 100
        ];
        $this->harvestDataModel->crossBrowserModel->setReturnValue('getCrossRecords',$returnValue);

        $expected = 100;

        // ACT
        $actual = $this->harvestDataModel->getAllRecordsCount($occurrenceId, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getAllRecordsCount: Test 2
     * dataLevel = plot
     */
    public function testGetAllRecordsCountWhenDataLevelIsPlot() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $dataLevel = 'plot';

        // mock
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) {
                return [
                    'filter' => [],
                    'sort' => ''
                ];
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        $returnValue = [];
        $this->harvestDataModel->plotBrowserModel->setReturnValue('assembleBrowserFilters',$returnValue);
        $returnValue = [
            "count" => 200
        ];
        $this->harvestDataModel->plotBrowserModel->setReturnValue('getPlotRecords',$returnValue);

        $expected = 200;

        // ACT
        $actual = $this->harvestDataModel->getAllRecordsCount($occurrenceId, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getAllRecordsCount: Test 2
     * $result->count = null
     */
    public function testGetAllRecordsCountWhenCountIsNull() {
        // ARRANGE
        // input
        $occurrenceId = 1234;
        $dataLevel = 'plot';

        // mock
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) {
                return [
                    'filter' => [],
                    'sort' => ''
                ];
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        $returnValue = [];
        $this->harvestDataModel->plotBrowserModel->setReturnValue('assembleBrowserFilters',$returnValue);
        $returnValue = [
            "count" => null
        ];
        $this->harvestDataModel->plotBrowserModel->setReturnValue('getPlotRecords',$returnValue);

        $expected = 0;

        // ACT
        $actual = $this->harvestDataModel->getAllRecordsCount($occurrenceId, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getRecordInfo: Test 1
     * dataLevel = cross
     * Note: This test doubles as a test for HarvestDataModel::bulkModalAllRecordsFormat
     */
    public function testGetRecordInfoWhenDataLevelIsCross() {
        // ARRANGE
        // input
        $recordIds = [
            "444"
        ];
        $occurrenceId = 1234;
        $dataLevel = 'cross';

        // mocks
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) {
                return [
                    'filter' => [],
                    'sort' => ''
                ];
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        $returnValue = [
            [
                "crossDbId" => 444,
                "state" => null,
                "type" => null,
                "harvestDate" => "2020-12-12",
                "harvestMethod" => null,
                "noOfPlant" => null,
                "noOfPanicle" => null,
                "specificPlantNo" => null,
                "noOfEar" => null,
                "noOfSeed" => null,
                "terminalHarvestDate" => null,
                "terminalHarvestMethod" => "Bulk",
                "terminalNoOfPlant" => null,
                "terminalNoOfPanicle" => null,
                "terminalSpecificPlantNo" => null,
                "terminalNoOfEar" => null,
                "terminalNoOfSeed" => "3",
                "harvestStatus" => "INCOMPLETE",
                "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
                "stageCode" => "F1"
            ]
        ];
        $this->harvestDataModel->crossBrowserModel->setReturnValue('searchAllCrosses', $returnValue);
        $this->harvestDataModel->harvestManagerModel->setReturnValue('matchConfigs', $returnValue);
        $this->harvestDataModel->crossBrowserModel->setReturnValue('assembleBrowserFilters', [ 'state' => null ]);

        // expected
        $expected = [
            [
                "crossDbId" => 444,
                "state" => null,
                "type" => null,
                "harvestDate" => "2020-12-12",
                "harvestMethod" => null,
                "noOfPlant" => null,
                "noOfPanicle" => null,
                "specificPlantNo" => null,
                "noOfEar" => null,
                "noOfSeed" => null,
                "terminalHarvestDate" => null,
                "terminalHarvestMethod" => "Bulk",
                "terminalNoOfPlant" => null,
                "terminalNoOfPanicle" => null,
                "terminalSpecificPlantNo" => null,
                "terminalNoOfEar" => null,
                "terminalNoOfSeed" => "3",
                "harvestStatus" => "INCOMPLETE",
                "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
                "stageCode" => "F1"
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getRecordInfo($recordIds, $occurrenceId, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getRecordInfo: Test 2
     * dataLevel = plot
     * Note: This test doubles as a test for HarvestDataModel::bulkModalAllRecordsFormat
     */
    public function testGetRecordInfoWhenDataLevelIsPlot() {
        // ARRANGE
        // input
        $recordIds = [
            "54321"
        ];
        $occurrenceId = 1234;
        $dataLevel = 'plot';

        // mocks
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) {
                return [
                    'filter' => [],
                    'sort' => ''
                ];
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        $returnValue = [
            [
                "plotDbId" => 54321,
                "state" => "not_fixed",
                "type" => "progeny",
                "harvestDate" => "2020-12-12",
                "harvestMethod" => null,
                "noOfPlant" => null,
                "noOfPanicle" => null,
                "specificPlantNo" => null,
                "noOfEar" => null,
                "noOfSeed" => null,
                "terminalHarvestDate" => null,
                "terminalHarvestMethod" => null,
                "terminalNoOfPlant" => null,
                "terminalNoOfPanicle" => null,
                "terminalSpecificPlantNo" => "Plant-specific",
                "terminalNoOfEar" => null,
                "terminalNoOfSeed" => "5,10,14",
                "harvestStatus" => "INCOMPLETE",
                "stageCode" => "F1"
            ]
        ];
        $this->harvestDataModel->plotBrowserModel->setReturnValue('searchAllPlots', $returnValue);
        $this->harvestDataModel->harvestManagerModel->setReturnValue('matchConfigs', $returnValue);
        $this->harvestDataModel->plotBrowserModel->setReturnValue('assembleBrowserFilters', []);

        // expected
        $expected = [
            [
                "plotDbId" => 54321,
                "state" => "not_fixed",
                "type" => "progeny",
                "harvestDate" => "2020-12-12",
                "harvestMethod" => null,
                "noOfPlant" => null,
                "noOfPanicle" => null,
                "specificPlantNo" => null,
                "noOfEar" => null,
                "noOfSeed" => null,
                "terminalHarvestDate" => null,
                "terminalHarvestMethod" => null,
                "terminalNoOfPlant" => null,
                "terminalNoOfPanicle" => null,
                "terminalSpecificPlantNo" => "Plant-specific",
                "terminalNoOfEar" => null,
                "terminalNoOfSeed" => "5,10,14",
                "harvestStatus" => "INCOMPLETE",
                "stageCode" => "F1"
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getRecordInfo($recordIds, $occurrenceId, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getRecordInfo: Test 3
     * dataLevel = cross, recordIds is empty
     * Note: This test doubles as a test for HarvestDataModel::bulkModalAllRecordsFormat
     */
    public function testGetRecordInfoWhenDataLevelIsCrossWithNoRecordIds() {
        // ARRANGE
        // input
        $recordIds = [];
        $occurrenceId = 1234;
        $dataLevel = 'cross';

        // mocks
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) {
                return [
                    'filter' => [],
                    'sort' => ''
                ];
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);

        $returnValue = [
            [
                "crossDbId" => 444,
                "state" => null,
                "type" => null,
                "harvestDate" => "2020-12-12",
                "harvestMethod" => null,
                "noOfPlant" => null,
                "noOfPanicle" => null,
                "specificPlantNo" => null,
                "noOfEar" => null,
                "noOfSeed" => null,
                "terminalHarvestDate" => null,
                "terminalHarvestMethod" => "Bulk",
                "terminalNoOfPlant" => null,
                "terminalNoOfPanicle" => null,
                "terminalSpecificPlantNo" => null,
                "terminalNoOfEar" => null,
                "terminalNoOfSeed" => "3",
                "harvestStatus" => "INCOMPLETE",
                "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
                "stageCode" => "F1"
            ]
        ];
        $this->harvestDataModel->crossBrowserModel->setReturnValue('searchAllCrosses', $returnValue);
        $this->harvestDataModel->harvestManagerModel->setReturnValue('matchConfigs', $returnValue);
        $this->harvestDataModel->crossBrowserModel->setReturnValue('assembleBrowserFilters', [ 'state' => null ]);

        // expected
        $expected = [
            [
                "crossDbId" => 444,
                "state" => null,
                "type" => null,
                "harvestDate" => "2020-12-12",
                "harvestMethod" => null,
                "noOfPlant" => null,
                "noOfPanicle" => null,
                "specificPlantNo" => null,
                "noOfEar" => null,
                "noOfSeed" => null,
                "terminalHarvestDate" => null,
                "terminalHarvestMethod" => "Bulk",
                "terminalNoOfPlant" => null,
                "terminalNoOfPanicle" => null,
                "terminalSpecificPlantNo" => null,
                "terminalNoOfEar" => null,
                "terminalNoOfSeed" => "3",
                "harvestStatus" => "INCOMPLETE",
                "crossMethodAbbrev" => "CROSS_METHOD_SINGLE_CROSS",
                "stageCode" => "F1"
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getRecordInfo($recordIds, $occurrenceId, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getRecordInfo: Test 4
     * dataLevel = plot, recordIds is empty
     * Note: This test doubles as a test for HarvestDataModel::bulkModalAllRecordsFormat
     */
    public function testGetRecordInfoWhenDataLevelIsPlotWithNoRecordIds() {
        // ARRANGE
        // input
        $recordIds = [];
        $occurrenceId = 1234;
        $dataLevel = 'plot';

        // mocks
        $cbSession = $this->make('app\components\CBSession', [
            'get' => function($varName) {
                return [
                    'filter' => [],
                    'sort' => ''
                ];
            }
        ]);

        // Inject cbSession mock into Yii::$app
        \Yii::$app->set('cbSession', $cbSession);
        
        $returnValue = [
            [
                "plotDbId" => 54321,
                "state" => "not_fixed",
                "type" => "progeny",
                "harvestDate" => "2020-12-12",
                "harvestMethod" => null,
                "noOfPlant" => null,
                "noOfPanicle" => null,
                "specificPlantNo" => null,
                "noOfEar" => null,
                "noOfSeed" => null,
                "terminalHarvestDate" => null,
                "terminalHarvestMethod" => null,
                "terminalNoOfPlant" => null,
                "terminalNoOfPanicle" => null,
                "terminalSpecificPlantNo" => "Plant-specific",
                "terminalNoOfEar" => null,
                "terminalNoOfSeed" => "5,10,14",
                "harvestStatus" => "INCOMPLETE",
                "stageCode" => "F1"
            ]
        ];
        $this->harvestDataModel->plotBrowserModel->setReturnValue('searchAllPlots', $returnValue);
        $this->harvestDataModel->harvestManagerModel->setReturnValue('matchConfigs', $returnValue);
        $this->harvestDataModel->plotBrowserModel->setReturnValue('assembleBrowserFilters', []);

        // expected
        $expected = [
            [
                "plotDbId" => 54321,
                "state" => "not_fixed",
                "type" => "progeny",
                "harvestDate" => "2020-12-12",
                "harvestMethod" => null,
                "noOfPlant" => null,
                "noOfPanicle" => null,
                "specificPlantNo" => null,
                "noOfEar" => null,
                "noOfSeed" => null,
                "terminalHarvestDate" => null,
                "terminalHarvestMethod" => null,
                "terminalNoOfPlant" => null,
                "terminalNoOfPanicle" => null,
                "terminalSpecificPlantNo" => "Plant-specific",
                "terminalNoOfEar" => null,
                "terminalNoOfSeed" => "5,10,14",
                "harvestStatus" => "INCOMPLETE",
                "stageCode" => "F1"
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getRecordInfo($recordIds, $occurrenceId, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * hasHarvestData: Test 1
     * No harvest data
     */
    public function testHasHarvestDataWhenThereAreNoHarvestData() {
        // ARRANGE
        // inputs
        $recordInfo = [
            ['terminalFieldName' => '', 'fieldName' => '']
        ];

        $configArr = [
            ['apiFieldName' => 'fieldName']
        ];

        $expected = [
            "hasData" => false,
            "hasCommitted" => false,
            "hasTerminal" => false
        ];

        // ACT
        $actual = $this->harvestDataModel->hasHarvestData($recordInfo, $configArr);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * hasHarvestData: Test 2
     * Has committed data
     */
    public function testHasHarvestDataWhenThereAreCommittedHarvestData() {
        // ARRANGE
        // inputs
        $recordInfo = [
            ['terminalFieldName' => '', 'fieldName' => 'field name']
        ];

        $configArr = [
            ['apiFieldName' => 'fieldName']
        ];

        $expected = [
            "hasData" => true,
            "hasCommitted" => true,
            "hasTerminal" => false
        ];

        // ACT
        $actual = $this->harvestDataModel->hasHarvestData($recordInfo, $configArr);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * hasHarvestData: Test 3
     * Has terminal data
     */
    public function testHasHarvestDataWhenThereAreTerminalHarvestData() {
        // ARRANGE
        // inputs
        $recordInfo2 = [
            ['terminalFieldName' => 'field name']
        ];

        $configArr = [
            ['apiFieldName' => 'fieldName']
        ];

        $expected = [
            "hasData" => true,
            "hasCommitted" => true,
            "hasTerminal" => true
        ];

        // ACT
        $actual = $this->harvestDataModel->hasHarvestData($recordInfo2, $configArr);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildHarvestDataCheckbox: Test 1
     */
    public function testBuildHarvestDataCheckbox() {
        // ARRANGE
        // input
        $harvestDataConfigs = [
            [
                "variableAbbrev" => "ABC_XYZ",
                "placeholder" => "Core Breeding"
            ],
            [
                "variableAbbrev" => "FFF_EEE_XX",
                "placeholder" => "Core System"
            ]
        ];
        $numVarAbbrevs = [];

        // expected
        $expected = '<li><input type="checkbox" class="filled-in checkboxlist datacheck hm-hd-bd-checkbox hm-hd-bd-checkbox-item hm-hd-bd-checkbox-item-non-num-var" data-id-string="" id="hm-hd-bd-checkbox-ABC_XYZ" abbrev="ABC_XYZ" />'
            . '<label class="hm-hd-bd-checkbox hm-hd-bd-checkbox-item-non-num-var" style="padding-left: 20px; font-weight: normal; color: black;" for="hm-hd-bd-checkbox-ABC_XYZ">&nbsp;&nbsp; Abc Xyz</label></li><li><input type="checkbox" class="filled-in checkboxlist datacheck hm-hd-bd-checkbox hm-hd-bd-checkbox-item hm-hd-bd-checkbox-item-non-num-var" data-id-string="" id="hm-hd-bd-checkbox-FFF_EEE_XX" abbrev="FFF_EEE_XX" />'
            . '<label class="hm-hd-bd-checkbox hm-hd-bd-checkbox-item-non-num-var" style="padding-left: 20px; font-weight: normal; color: black;" for="hm-hd-bd-checkbox-FFF_EEE_XX">&nbsp;&nbsp; Fff Eee Xx</label></li>';


        // ACT
        $actual = $this->harvestDataModel->buildHarvestDataCheckbox($harvestDataConfigs, $numVarAbbrevs);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * updateRecordsStatus: Test 1
     * Data level is cross
     */
    public function testUpdateRecordsStatusWhenDataLevelIsCross() {
        // ARRANGE
        // input
        $recordIds = [2001,2002,2003,2004];
        $status = 'COMPLETED';
        $occurrenceId = 1234;
        $dataLevel = 'cross';

        // mock
        $returnValue = [
            "jobId" => 5001
        ];
        $this->harvestDataModel->cross->setReturnValue('updateMany', $returnValue);

        // expected
        $expected = [
            "jobId" => 5001
        ];

        // ACT
        $actual = $this->harvestDataModel->updateRecordsStatus($recordIds, $status, $occurrenceId, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * updateRecordsStatus: Test 2
     * Data level is plot
     */
    public function testUpdateRecordsStatusWhenDataLevelIsPlot() {
        // ARRANGE
        // input
        $recordIds = [3001,3002,3003,3004];
        $status = 'COMPLETED';
        $occurrenceId = 1234;
        $dataLevel = 'plot';

        // mock
        $returnValue = [
            "jobId" => 7001
        ];
        $this->harvestDataModel->plot->setReturnValue('updateMany', $returnValue);

        // expected
        $expected = [
            "jobId" => 7001
        ];

        // ACT
        $actual = $this->harvestDataModel->updateRecordsStatus($recordIds, $status, $occurrenceId, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * updateRecordsStatus: Test 3
     * Data level is unsupported (not plot or cross)
     */
    public function testUpdateRecordsStatusWhenDataLevelIsUnsupported() {
        // ARRANGE
        // input
        $recordIds = [44001,44002,44003,44004];
        $status = 'COMPLETED';
        $occurrenceId = 1234;
        $dataLevel = 'seed';

        // ACT
        $actual = $this->harvestDataModel->updateRecordsStatus($recordIds, $status, $occurrenceId, $dataLevel);

        // ASSERT
        $this->assertNull($actual);
    }

    public function testGetUncommittedTraitIds() {
        // ARRANGE
        // input
        $recordIds = [101];
        $selectedAbbrevs = ['HV_METH_DISC'];
        $locationId = 1001;

        // mock
        $this->harvestDataModel->user->setReturnValue('getUserId', 123);
        $this->harvestDataModel->terminalTransaction->setReturnValue('searchAll', [
            "data" => [
                [
                    "transactionDbId" => 444
                ]
            ]
        ]);
        $this->harvestDataModel->variable->setReturnValue('searchAll', [
            "data" => [
                [
                    "variableDbId" => 555
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            [
                "datasetDbId" => 12345
            ],
            [
                "datasetDbId" => 12346
            ]
        ]);

        // expected
        $expected = [ 12345, 12346 ];

        // ACT
        $actual = $this->harvestDataModel->getUncommittedTraitIds($recordIds, $selectedAbbrevs, $locationId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * deleteTransactionDatasetsViaProcessor
     */
    public function testDeleteTransactionDatasetsViaProcessor() {
        // ARRANGE
        // input
        $datasetIds = [220,221,222];
        $locationId = 5678;

        // mock
        $this->harvestDataModel->apiHelper->setReturnValue('processorBuilder', [
            [
                "jobId" => 777
            ]
        ]);

        // expected
        $expected = [
            "jobId" => 777
        ];

        // ACT
        $actual = $this->harvestDataModel->deleteTransactionDatasetsViaProcessor($datasetIds, $locationId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSeedCount: Test 1
     * Data level is cross
     */
    public function testGetSeedCountWhenDataLevelIsCross() {
        // ARRANGE
        // input
        $recordIds = [321, 322, 323];
        $dataLevel = 'cross';

        // mock
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequestSinglePage', [
            "return_vals" => [
                [
                    "count" => 20
                ],
                [
                    "count" => 10
                ],
                [
                    "count" => 20
                ]
            ]
        ]);
        
        // expected
        $expected = 50;

        // ACT
        $actual = $this->harvestDataModel->getSeedCount($recordIds, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSeedCount: Test 2
     * Data level is plot
     */
    public function testGetSeedCountWhenDataLevelIsPlot() {
        // ARRANGE
        // input
        $recordIds = [421, 422, 423];
        $dataLevel = 'plot';

        // mock
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequestSinglePage', [
            "return_vals" => [
                [
                    "count" => 5
                ],
                [
                    "count" => 15
                ],
                [
                    "count" => 25
                ]
            ]
        ]);
        
        // expected
        $expected = 45;

        // ACT
        $actual = $this->harvestDataModel->getSeedCount($recordIds, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkSeedEntries: Test 1
     * Data level is cross, seeds all have no entries
     */
    public function testCheckSeedEntriesWhenDataLevelIsCrossAndAllSeedsHaveNoEntries() {
        // ARRANGE
        // input
        $recordIds = [ 200 ];
        $dataLevel = 'cross';

        // mock
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 13001,
                                "crossDbId" => 200
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequestSinglePage', [
            "return_vals" => [
                [
                    "count" => 0
                ]
            ]
        ]);

        // expected
        $expected = [
            'recordsToDelete' => '["200"]',
            'recordsToDeleteCount' => 1,
            'recordsUnableToDelete' => '[]',
            'recordsUnableToDeleteCount' => 0
        ];

        // ACT
        $actual = $this->harvestDataModel->checkSeedEntries($recordIds, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkSeedEntries: Test 2
     * Data level is plot, seeds all have entries
     */
    public function testCheckSeedEntriesWhenDataLevelIsPlotAndAllSeedsHaveNoEntries() {
        // ARRANGE
        // input
        $recordIds = [ 3000 ];
        $dataLevel = 'plot';

        // mock
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 13001,
                                "sourcePlotDbId" => 3000
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequestSinglePage', [
            "return_vals" => [
                [
                    "count" => 20
                ]
            ]
        ]);

        // expected
        $expected = [
            'recordsToDelete' => '[]',
            'recordsToDeleteCount' => 0,
            'recordsUnableToDelete' => '["3000"]',
            'recordsUnableToDeleteCount' => 1
        ];

        // ACT
        $actual = $this->harvestDataModel->checkSeedEntries($recordIds, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * checkSeedEntries: Test 2
     * Data level is plot, and some seeds have entries
     */
    public function testCheckSeedEntriesWhenDataLevelIsPlotAndSomeSeedsHaveEntries() {
        // ARRANGE
        // input
        $recordIds = [ 3000 ];
        $dataLevel = 'plot';

        // mock
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 13001,
                                "sourcePlotDbId" => 3000
                            ],
                            [
                                "seedDbId" => 13002,
                                "sourcePlotDbId" => 3000
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequestSinglePage', [
            "return_vals" => [
                [
                    "count" => 0
                ],
                [
                    "count" => 20
                ]
            ]
        ]);

        // expected
        $expected = [
            'recordsToDelete' => '[]',
            'recordsToDeleteCount' => 0,
            'recordsUnableToDelete' => '["3000"]',
            'recordsUnableToDeleteCount' => 1
        ];

        // ACT
        $actual = $this->harvestDataModel->checkSeedEntries($recordIds, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDatabaseIdsToDelete: Test 1
     * Data level is cross, crosses have no seeds
     */
    public function testGetDatabaseIdsToDeleteWhenDataLevelIsCrossAndCrossesHaveNoSeeds() {
        // ARRANGE
        // input
        $recordDbIds = [ 123 ];
        $selectedAbbrevs = [ 'HVDATE_CONT', 'HV_METH_DISC' ];
        $dataLevel = 'cross';
        $hasSeeds = false;

        // mock
        $this->harvestDataModel->variable->setReturnValue('searchAll', [
            "data" => [
                [
                    "variableDbId" => 444
                ],
                [
                    "variableDbId" => 789
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            [
                "data" => [
                    [
                        "crossDataDbId" => 50201
                    ],
                    [
                        "crossDataDbId" => 50202
                    ]
                ]
            ]
        ]);

        // expected
        $expected = [
            'seedDbIds' => [],
            'germplasmDbIds' => [],
            'harvestDataDbIds' => [ 50201, 50202 ],
            'recordDbIds' => [ 123 ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getDatabaseIdsToDelete($recordDbIds, $selectedAbbrevs, $dataLevel, $hasSeeds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDatabaseIdsToDelete: Test 2
     * Data level is plot, plots have no seeds
     */
    public function testGetDatabaseIdsToDeleteWhenDataLevelIsPlotAndPlotsHaveNoSeeds() {
        // ARRANGE
        // input
        $recordDbIds = [ 123, 124 ];
        $selectedAbbrevs = [ 'HVDATE_CONT', 'HV_METH_DISC' ];
        $dataLevel = 'plot';
        $hasSeeds = false;

        // mock
        $this->harvestDataModel->variable->setReturnValue('searchAll', [
            "data" => [
                [
                    "variableDbId" => 444
                ],
                [
                    "variableDbId" => 789
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "data" => [
                            [
                                "plotDataDbId" => 77771
                            ],
                            [
                                "plotDataDbId" => 77772
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "data" => [
                            [
                                "plotDataDbId" => 77777
                            ],
                            [
                                "plotDataDbId" => 77778
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        // expected
        $expected = [
            'seedDbIds' => [],
            'germplasmDbIds' => [],
            'harvestDataDbIds' => [ 77771, 77772, 77777, 77778 ],
            'recordDbIds' => [ 123, 124 ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getDatabaseIdsToDelete($recordDbIds, $selectedAbbrevs, $dataLevel, $hasSeeds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDatabaseIdsToDelete: Test 3
     * Data level is cross, crosses have seeds, no other seeds use the germplasm
     */
    public function testGetDatabaseIdsToDeleteWhenDataLevelIsCrossAndCrossesHaveSeedsAndNoOtherSeedsUseTheGermplasm() {
        // ARRANGE
        // input
        $recordDbIds = [ 123, 124 ];
        $selectedAbbrevs = [ 'HVDATE_CONT', 'HV_METH_DISC' ];
        $dataLevel = 'cross';
        $hasSeeds = true;

        // mock
        $this->harvestDataModel->variable->setReturnValue('searchAll', [
            "data" => [
                [
                    "variableDbId" => 444
                ],
                [
                    "variableDbId" => 789
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "data" => [
                            [
                                "crossDataDbId" => 82341
                            ],
                            [
                                "crossDataDbId" => 82342
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45001,
                                "germplasmDbId" => 25001,
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45001
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "data" => [
                            [
                                "crossDataDbId" => 82355
                            ],
                            [
                                "crossDataDbId" => 82356
                            ]
                        ]
                    ]
                ],            
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45002,
                                "germplasmDbId" => 25035,
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45002
                            ]
                        ]
                    ]
                ],
            ]
        ]);

        // expected
        $expected = [
            'seedDbIds' => [],
            'germplasmDbIds' => [ 25001, 25035 ],
            'harvestDataDbIds' => [ 82341, 82342, 82355, 82356 ],
            'recordDbIds' => [ 123, 124 ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getDatabaseIdsToDelete($recordDbIds, $selectedAbbrevs, $dataLevel, $hasSeeds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDatabaseIdsToDelete: Test 4
     * Data level is cross, crosses have seeds, other seeds use the germplasm
     */
    public function testGetDatabaseIdsToDeleteWhenDataLevelIsCrossAndCrossesHaveSeedsAndOtherSeedsUseTheGermplasm() {
        // ARRANGE
        // input
        $recordDbIds = [ 123, 124 ];
        $selectedAbbrevs = [ 'HVDATE_CONT', 'HV_METH_DISC' ];
        $dataLevel = 'cross';
        $hasSeeds = true;

        // mock
        $this->harvestDataModel->variable->setReturnValue('searchAll', [
            "data" => [
                [
                    "variableDbId" => 444
                ],
                [
                    "variableDbId" => 789
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "data" => [
                            [
                                "crossDataDbId" => 82341
                            ],
                            [
                                "crossDataDbId" => 82342
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45001,
                                "germplasmDbId" => 25001,
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45001
                            ],
                            [
                                "seedDbId" => 45626
                            ],
                            [
                                "seedDbId" => 45966
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "data" => [
                            [
                                "crossDataDbId" => 82355
                            ],
                            [
                                "crossDataDbId" => 82356
                            ]
                        ]
                    ]
                ],            
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45002,
                                "germplasmDbId" => 25035,
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45002
                            ]
                        ]
                    ]
                ],
            ]
        ]);

        // expected
        $expected = [
            'seedDbIds' => [ 45001 ],
            'germplasmDbIds' => [ 25035 ],
            'harvestDataDbIds' => [ 82341, 82342, 82355, 82356 ],
            'recordDbIds' => [ 123, 124 ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getDatabaseIdsToDelete($recordDbIds, $selectedAbbrevs, $dataLevel, $hasSeeds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDatabaseIdsToDelete: Test 5
     * Data level is plot, plots have seeds, no other seeds use the germplasm
     */
    public function testGetDatabaseIdsToDeleteWhenDataLevelIsPlotAndPlotsHaveSeedsAndNoOtherSeedsUseTheGermplasm() {
        // ARRANGE
        // input
        $recordDbIds = [ 123, 124 ];
        $selectedAbbrevs = [ 'HVDATE_CONT', 'HV_METH_DISC' ];
        $dataLevel = 'plot';
        $hasSeeds = true;

        // mock
        $this->harvestDataModel->variable->setReturnValue('searchAll', [
            "data" => [
                [
                    "variableDbId" => 444
                ],
                [
                    "variableDbId" => 789
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "data" => [
                            [
                                "plotDataDbId" => 82341
                            ],
                            [
                                "plotDataDbId" => 82342
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45001,
                                "germplasmDbId" => 25001,
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45001
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "data" => [
                            [
                                "plotDataDbId" => 82355
                            ],
                            [
                                "plotDataDbId" => 82356
                            ]
                        ]
                    ]
                ],            
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45002,
                                "germplasmDbId" => 25035,
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45002
                            ]
                        ]
                    ]
                ],
            ]
        ]);

        // expected
        $expected = [
            'seedDbIds' => [],
            'germplasmDbIds' => [ 25001, 25035 ],
            'harvestDataDbIds' => [ 82341, 82342, 82355, 82356 ],
            'recordDbIds' => [ 123, 124 ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getDatabaseIdsToDelete($recordDbIds, $selectedAbbrevs, $dataLevel, $hasSeeds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getDatabaseIdsToDelete: Test 4
     * Data level is plot, plots have seeds, other seeds use the germplasm
     */
    public function testGetDatabaseIdsToDeleteWhenDataLevelIsPlotAndPlotsHaveSeedsAndOtherSeedsUseTheGermplasm() {
        // ARRANGE
        // input
        $recordDbIds = [ 123, 124 ];
        $selectedAbbrevs = [ 'HVDATE_CONT', 'HV_METH_DISC' ];
        $dataLevel = 'plot';
        $hasSeeds = true;

        // mock
        $this->harvestDataModel->variable->setReturnValue('searchAll', [
            "data" => [
                [
                    "variableDbId" => 444
                ],
                [
                    "variableDbId" => 789
                ]
            ]
        ]);
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "data" => [
                            [
                                "plotDataDbId" => 82341
                            ],
                            [
                                "plotDataDbId" => 82342
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45001,
                                "germplasmDbId" => 25001,
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45001
                            ],
                            [
                                "seedDbId" => 45626
                            ],
                            [
                                "seedDbId" => 45966
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "data" => [
                            [
                                "plotDataDbId" => 82355
                            ],
                            [
                                "plotDataDbId" => 82356
                            ]
                        ]
                    ]
                ],            
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45002,
                                "germplasmDbId" => 25035,
                            ]
                        ]
                    ]
                ],
                [
                    [
                        "seeds" => [
                            [
                                "seedDbId" => 45002
                            ]
                        ]
                    ]
                ],
            ]
        ]);

        // expected
        $expected = [
            'seedDbIds' => [ 45001 ],
            'germplasmDbIds' => [ 25035 ],
            'harvestDataDbIds' => [ 82341, 82342, 82355, 82356 ],
            'recordDbIds' => [ 123, 124 ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getDatabaseIdsToDelete($recordDbIds, $selectedAbbrevs, $dataLevel, $hasSeeds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * performDeleteViaProcessor
     */
    public function testPerformDeleteViaProcessor() {
        // ARRANGE
        // input
        $databaseIds = [
            "seedDbIds" => [ 1, 2, 3 ],
            "germplasmDbIds" => [ 1, 2, 3 ],
            "harvestDataDbIds" => [ 1, 2, 3 ],
            "recordDbIds" => [ 1, 2, 3 ]
        ];
        $locationDbId = 1234;
        $deleteTraits = true;
        $dataLevel = 'plot';

        // mock
        $this->harvestDataModel->apiHelper->setReturnValue('processorBuilder', [
            "return_vals" => [
                [
                    [
                        "jobId" => 1
                    ]
                ],
                [
                    [
                        "jobId" => 2
                    ]
                ],
                [
                    [
                        "jobId" => 3
                    ]
                ]
            ]
        ]);

        // expected
        $expected = [
            "seed" => [ "jobId" => 1 ],
            "germplasm" => [ "jobId" => 2 ],
            "harvestData" => [ "jobId" => 3 ]
        ];

        // ACT
        $actual = $this->harvestDataModel->performDeleteViaProcessor($databaseIds, $locationDbId, $deleteTraits, $dataLevel);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getBackgroundJobs
     */
    public function testGetBackgroundJobs() {
        // ARRANGE
        // input
        $backgroundJobIds = [ 1, 2, 3 ];

        // mock
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest', [
            "return_vals" => [
                [
                    [
                        "jobId" => 1
                    ]
                ],
                [
                    [
                        "jobId" => 2
                    ]
                ],
                [
                    [
                        "jobId" => 3
                    ]
                ]
            ]
        ]);

        // expected
        $expected = [
            'backgroundJobs' => [
                [ "jobId" => 1 ],
                [ "jobId" => 2 ],
                [ "jobId" => 3 ]
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->getBackgroundJobs($backgroundJobIds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildNotifRetrievalParams: Test 1
     * When DeleteHarvestRecords worker is not included
     */
    public function testBuildNotifRetrievalParamsWhenDeleteHarvestRecordsWorkerIsNotIncluded() {
        // ARRANGE
        // input
        $workerName = 'GenerateHarvestRecords';
        $remarks = null;
        $isSeen = true;

        // mocks
        $this->harvestDataModel->user->setReturnValue('getUserId', 123);

        // expected
        $expected = [
            "creatorDbId" => "equals 123",
            "workerName" => "equals $workerName",
            "isSeen" => "equals 1"
        ];

        // ACT
        $actual = $this->harvestDataModel->buildNotifRetrievalParams($workerName, $remarks, $isSeen);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * buildNotifRetrievalParams: Test 2
     * When DeleteHarvestRecords worker is included
     */
    public function testBuildNotifRetrievalParamsWhenDeleteHarvestRecordsWorkerIsIncluded() {
        // ARRANGE
        // input
        $workerName = 'DeleteHarvestRecords|CreateHarvestList';
        $remarks = "package";
        $isSeen = true;

        // mocks
        $this->harvestDataModel->user->setReturnValue('getUserId', 123);

        // expected
        $expected = [
            "creatorDbId" => "equals 123",
            "workerName" => "equals $workerName",
            "isSeen" => "equals 1"
        ];
        $expected = [
            "conditions" => [
                [
                    "creatorDbId" => "equals 123",
                    "workerName" => "equals CreateHarvestList",
                    "isSeen" => "equals 1"
                ],
                [
                    "creatorDbId" => "equals 123",
                    "workerName" => "equals DeleteHarvestRecords",
                    "jobRemarks" => "package",
                    "isSeen" => "equals 1"
                ]
            ]
        ];

        // ACT
        $actual = $this->harvestDataModel->buildNotifRetrievalParams($workerName, $remarks, $isSeen);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * updateOccurrenceStatus: Test 1
     */
    public function testUpdateOccurrenceStatus() {
        // ARRANGE
        // input
        $occurrenceId = 1234;

        // mocks
        $returnValue = [
            'return_vals' => [
                [
                    [
                        "occurrenceDbId" => 1234,
                        "plotDataCount" => 0
                    ]
                ],
                [
                    "status" => 200
                ]
            ]
        ];
        $this->harvestDataModel->apiHelper->setReturnValue('sendRequest',$returnValue);

        // ACT
        $this->harvestDataModel->updateOccurrenceStatus($occurrenceId);

        // ASSERT
        $this->assertTrue(true);
    }
}