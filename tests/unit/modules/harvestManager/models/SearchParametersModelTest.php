<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\harvestManager\models;

class SearchParametersModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $searchParametersModel;

    protected function _before()
    {
        $this->searchParametersModel = \Yii::$container->get('searchParametersModel');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * getFilterValuesById: Test 1
     * Filter consists of one word
     */
    public function testGetFilterValuesByIdTest1()
    {
        // ARRANGE
        // inputs
        $filter = [
            "abbrev" => "PROGRAM",
            "values" => 1
        ];

        // mocks
        $returnValue = [
            "programName" => "BW Wheat Breeding Program",
            "programCode" => "BW"
        ];
        $this->searchParametersModel->program->setReturnValue("getProgram", $returnValue);

        // expected
        $expected = [
            "texts" => "BW Wheat Breeding Program (BW)",
            "ids" => 1
        ];

        // ACT
        $actual = $this->searchParametersModel->getFilterValuesById($filter);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getFilterValuesById: Test 2
     * Filter consists of multiple words
     */
    public function testGetFilterValuesByIdTest2()
    {
        // ARRANGE
        // inputs
        $filter = [
            "abbrev" => "PROGRAM_NAME",
            "values" => 20
        ];

        // mocks
        $returnValue = [
            "programName" => "Irrigated South-East Asia",
            "programCode" => "IRSEA"
        ];
        $this->searchParametersModel->program->setReturnValue("getProgram", $returnValue);

        // expected
        $expected = [
            "texts" => "Irrigated South-East Asia (IRSEA)",
            "ids" => 20
        ];

        // ACT
        $actual = $this->searchParametersModel->getFilterValuesById($filter);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * generateFilterFields: Test 1
     * Configs are not available
     */
    public function testGenerateFilterFieldsWithoutConfig()
    {
        // ARRANGE
        // inputs
        $variableFilters = [];
        $dataUrl = '/index.php/harvestManager/occurrence-selection/get-filter-data';

        $returnValue = [];
        $this->searchParametersModel->config->setReturnValue('getConfigByAbbrev', $returnValue);

        // expected
        $expected = [];

        // ACT
        $actual = $this->searchParametersModel->generateFilterFields($dataUrl, $variableFilters);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * generateFilterFields: Test 2
     * Configs are available
     * 
     * NOTE: This test uses a PARTIAL MOCK of the SUT.
     *          This is achieved by using \Codeception\Test\Unit::construct.
     *          This is different from the typical Mock Classes.
     *          Here, we mock a portion of the System Under Test, instead of the dependencies.
     *          This enables us to control the behavior of any sibling functions of
     *          the current function that we are testing.
     */
    public function testGenerateFilterFieldsWithConfig()
    {
        // ARRANGE
        // inputs
        $variableFilters = [
            [
                "abbrev" => "PROGRAM",
                "values" => "101"
            ],
            [
                "abbrev" => "EXPERIMENT_NAME",
                "values" => "0"
            ],
            [
                "abbrev" => "OCCURRENCE_NAME",
                "values" => "0"
            ]
        ];
        $dataUrl = '/index.php/harvestManager/occurrence-selection/get-filter-data';

        // Mock dependencies
        $configMock = \Yii::$container->get('configMock');
        $returnValue = [
            "values" => [
                [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Program that manages the experiment",
                    "field_label" => "Program",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "1",
                    "required" => "false",
                    "variable_abbrev" => "PROGRAM"
                ],
                [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Name of the experiment",
                    "field_label" => "Experiment",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "3",
                    "required" => "false",
                    "variable_abbrev" => "EXPERIMENT_NAME"
                ],
                [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Name of the occurrence",
                    "field_label" => "Occurrence",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "5",
                    "required" => "true",
                    "variable_abbrev" => "OCCURRENCE_NAME"
                ]
            ]
        ];
        $configMock->setReturnValue('getConfigByAbbrev', $returnValue);
        $experimentMock = \Yii::$container->get('experimentMock');
        // mock retrieval of occurrence info
        $occurrenceMock = \Yii::$container->get('occurrenceMock');
        $returnValue = [
            "occurrenceName" => "test"
        ];
        $occurrenceMock->setReturnValue("get", $returnValue);
        // mock retrieval of program info
        $programMock = \Yii::$container->get('programMock');
        $returnValue = [
            "abbrev" => "PROGRAM",
            "values" => "101,102,103"
        ];
        $programMock->setReturnValue("getProgram", $returnValue);
        $userMock = \Yii::$container->get('userMock');

        /**
         * Partially mock SearchParametersModel.
         * Replace buildSelect2's return value.
         * 
         * This has to be mocked, as there is an issue with
         * the version compatibility of PHP and Composer that causes
         * the test to fail when buildSelect2 is not mocked.
         * More info about this in this thread: https://github.com/fxpio/composer-asset-plugin/issues/334
         * 
         * The error produced looks like this:
         *      [yii\base\InvalidArgumentException] The file or directory to be published does not exist: /home/jbantay/IRRIDev/cbdev/cb/vendor/bower/jquery/dist 
         * 
         * Manually renaming bower-asset to bower in the vendor directory solves the problem,
         * but is not an elegant solution, and should be avoided.
         */
        $this->searchParametersModel = $this->construct(
            $this->searchParametersModel,
            [
                "config" => $configMock,
                "experiment" => $experimentMock,
                "occurrence" => $occurrenceMock,
                "program" => $programMock,
                "user" => $userMock,
            ],
            [
                'buildSelect2' => function () {
                    return "<placeholder>";
                }
            ]
        );

        // expected
        $expected = [
            [
                "default" => 0,
                "label" => "Program",
                "order_number" => "1",
                "required" => true,
                "value" => '<div class="control-label col-md-12 col-md-12" style="margin-bottom:10px; padding-right:0px;"><div class="row" style="margin-bottom:5px;"><label data-label="Program" style="margin-top:10px;" for="PROGRAM">Program <span class="required">*</span></label></div><div class="row" style="margin-bottom:5px; padding-right:10px;"><placeholder><input type="text" id="program-filter-hidden" class="select2-input-validate-hidden hidden variable-filter" name="filter[PROGRAM]"></div></div>'
            ],
            [
                "default" => 0,
                "label" => "Experiment",
                "order_number" => "3",
                "required" => true,
                "value" => '<div class="control-label col-md-12 col-md-12" style="margin-bottom:10px; padding-right:0px;"><div class="row" style="margin-bottom:5px;"><label data-label="Experiment" style="margin-top:10px;" for="EXPERIMENT_NAME">Experiment</label></div><div class="row" style="margin-bottom:5px; padding-right:10px;"><placeholder><input type="text" id="experiment_name-filter-hidden" class="select2-input-validate-hidden hidden variable-filter" name="filter[EXPERIMENT_NAME]"></div></div>'
            ],
            [
                "default" => 0,
                "label" => "Occurrence",
                "order_number" => "5",
                "required" => true,
                "value" => '<div class="control-label col-md-12 col-md-12" style="margin-bottom:10px; padding-right:0px;"><div class="row" style="margin-bottom:5px;"><label data-label="Occurrence" style="margin-top:10px;" for="OCCURRENCE_NAME">Occurrence <span class="required">*</span></label></div><div class="row" style="margin-bottom:5px; padding-right:10px;"><placeholder><input type="text" id="occurrence_name-filter-hidden" class="select2-input-validate-hidden hidden variable-filter" name="filter[OCCURRENCE_NAME]"></div>
                <span>
                    <p class="hidden" id="occurrence-name-warning" style="color:#c62828">
                        Occurrence is required. Please select one.
                    </p>
                </span>
            </div>'
            ]
        ];

        // ACT
        // If intelephense is enabled, generateFilterFields may be tagged
        // as an "undefined method". Ignore this for now. The test should run just fine
        // despite the error message.
        $actual = $this->searchParametersModel->generateFilterFields($dataUrl, $variableFilters);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * extractVariableFilters: Test 1
     * Program has value
     */
    public function testExtractVariableFiltersTest1()
    {
        // ARRANGE
        // input
        $existingFilters = [
            [
                "name" => "filter[PROGRAM]",
                "value" => "102"
            ],
            [
                "name" => "filter[EXPERIMENT_NAME]",
                "value" => ""
            ],
            [
                "name" => "filter[OCCURRENCE_NAME]",
                "value" => ""
            ]
        ];

        $configVar = [
            [
                "allowed_values" => "",
                "basic_parameter" => "true",
                "default_value" => "",
                "field_description" => "Program that manages the experiment",
                "field_label" => "Program",
                "input_field" => "selection",
                "input_type" => "single",
                "order_number" => "1",
                "required" => "false",
                "variable_abbrev" => "PROGRAM"
            ],
            [
                "allowed_values" => "",
                "basic_parameter" => "true",
                "default_value" => "",
                "field_description" => "Name of the experiment",
                "field_label" => "Experiment",
                "input_field" => "selection",
                "input_type" => "single",
                "order_number" => "3",
                "required" => "false",
                "variable_abbrev" => "EXPERIMENT_NAME"
            ],
            [
                "allowed_values" => "",
                "basic_parameter" => "true",
                "default_value" => "",
                "field_description" => "Name of the occurrence",
                "field_label" => "Occurrence",
                "input_field" => "selection",
                "input_type" => "single",
                "order_number" => "5",
                "required" => "true",
                "variable_abbrev" => "OCCURRENCE_NAME"
            ]
        ];
        $id = "experiment_name-filter";

        // expected
        $expected = [
            [
                "abbrev" => "PROGRAM",
                "values" => "102",
                "config_info" => [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Program that manages the experiment",
                    "field_label" => "Program",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "1",
                    "required" => "false",
                    "variable_abbrev" => "PROGRAM"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->extractVariableFilters($existingFilters, $configVar, $id);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * extractVariableFilters: Test 2
     * Program and Experiment has value
     */
    public function testExtractVariableFiltersTest2()
    {
        // ARRANGE
        // input
        $existingFilters = [
            [
                "name" => "filter[PROGRAM]",
                "value" => "102"
            ],
            [
                "name" => "filter[PROGRAM]",
                "value" => "102"
            ],
            [
                "name" => "filter[EXPERIMENT_NAME]",
                "value" => "10123"
            ],
            [
                "name" => "filter[EXPERIMENT_NAME]",
                "value" => "10123"
            ],
            [
                "name" => "filter[OCCURRENCE_NAME]",
                "value" => ""
            ],
            [
                "name" => "filter[OCCURRENCE_NAME]",
                "value" => ""
            ]
        ];

        $configVar = [
            [
                "allowed_values" => "",
                "basic_parameter" => "true",
                "default_value" => "",
                "field_description" => "Program that manages the experiment",
                "field_label" => "Program",
                "input_field" => "selection",
                "input_type" => "single",
                "order_number" => "1",
                "required" => "false",
                "variable_abbrev" => "PROGRAM"
            ],
            [
                "allowed_values" => "",
                "basic_parameter" => "true",
                "default_value" => "",
                "field_description" => "Name of the experiment",
                "field_label" => "Experiment",
                "input_field" => "selection",
                "input_type" => "single",
                "order_number" => "3",
                "required" => "false",
                "variable_abbrev" => "EXPERIMENT_NAME"
            ],
            [
                "allowed_values" => "",
                "basic_parameter" => "true",
                "default_value" => "",
                "field_description" => "Name of the occurrence",
                "field_label" => "Occurrence",
                "input_field" => "selection",
                "input_type" => "single",
                "order_number" => "5",
                "required" => "true",
                "variable_abbrev" => "OCCURRENCE_NAME"
            ]
        ];
        $id = "occurrence_name-filter";

        // expected
        $expected = [
            [
                "abbrev" => "PROGRAM",
                "values" => "102",
                "config_info" => [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Program that manages the experiment",
                    "field_label" => "Program",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "1",
                    "required" => "false",
                    "variable_abbrev" => "PROGRAM"
                ]
            ],
            [
                "abbrev" => "EXPERIMENT_NAME",
                "values" => "10123",
                "config_info" => [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Name of the experiment",
                    "field_label" => "Experiment",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "3",
                    "required" => "false",
                    "variable_abbrev" => "EXPERIMENT_NAME"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->extractVariableFilters($existingFilters, $configVar, $id);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getExperimentOccurrences: Test 1
     * Single occurrence
     */
    public function testGetExperimentOccurrencesTest1()
    {
        // ARRANGE
        // inputs
        $experimentId = 123;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrenceDbId" => 123,
                    "occurrenceName" => "MY_OCC_1"
                ]
            ]
        ];
        $this->searchParametersModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = json_encode(
            [
                [
                    "id" => 123,
                    "text" => "MY_OCC_1"
                ]
            ]
        );

        // ACT
        $actual = $this->searchParametersModel->getExperimentOccurrences($experimentId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getExperimentOccurrences: Test 2
     * Multiple occurrences
     */
    public function testGetExperimentOccurrencesTest2()
    {
        // ARRANGE
        // inputs
        $experimentId = 101;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "occurrenceDbId" => 123,
                    "occurrenceName" => "MY_OCC_1"
                ],
                [
                    "occurrenceDbId" => 124,
                    "occurrenceName" => "MY_OCC_2"
                ],
                [
                    "occurrenceDbId" => 125,
                    "occurrenceName" => "MY_OCC_3"
                ]
            ]
        ];
        $this->searchParametersModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = json_encode(
            [
                [
                    "id" => 123,
                    "text" => "MY_OCC_1"
                ],
                [
                    "id" => 124,
                    "text" => "MY_OCC_2"
                ],
                [
                    "id" => 125,
                    "text" => "MY_OCC_3"
                ]
            ]
        );

        // ACT
        $actual = $this->searchParametersModel->getExperimentOccurrences($experimentId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOccurrenceExperiment: Test 1
     */
    public function testGetOccurrenceExperimentTest1()
    {
        // ARRANGE
        // inputs
        $occurrenceId = 123;

        // mocks
        $returnValue = [
            "data" => [
                [
                    "experimentDbId" => 101,
                    "experiment" => "MY_EXP_1"
                ]
            ]
        ];
        $this->searchParametersModel->occurrence->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = json_encode(
            [
                "experimentId" => 101,
                "data" => [
                    [
                        "id" => 101,
                        "text" => "MY_EXP_1"
                    ]
                ]
            ]
        );

        // ACT
        $actual = $this->searchParametersModel->getOccurrenceExperiment($occurrenceId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableFilterData: Test 1
     * Program filter, no dashboard filters
     */
    public function testGetVariableFilterDataTest1()
    {
        // ARRANGE
        // inputs
        $searchTerm = null;
        $userId = 1;
        $filterId = 'program-filter';
        $variableFilters = [];
        $defaultFilters = [
            "owned" => "true",
            "season_id" => ""
        ];
        $limit = 5;
        $page = 1;

        // mocks
        $returnValue = [
            [
                "id" => 101,
                "text" => "IRSEA"
            ]
        ];
        $this->searchParametersModel->user->setReturnValue('getUserPrograms', $returnValue);
        $this->searchParametersModel->user->setReturnValue('isAdmin', true);

        // expected
        $expected = [
            "count" => 1,
            "data" => [
                [
                    "id" => 101,
                    "text" => "IRSEA"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->getVariableFilterData($searchTerm, $userId, $filterId, $variableFilters, $defaultFilters, $limit, $page);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableFilterData: Test 2
     * Program filter, "owned" = true
     */
    public function testGetVariableFilterDataTest2()
    {
        // ARRANGE
        // inputs
        $searchTerm = null;
        $userId = 1;
        $filterId = 'program-filter';
        $variableFilters = [];
        $defaultFilters = [
            "owned" => "true",
            "season_id" => ""
        ];
        $limit = 5;
        $page = 1;

        // mocks
        $returnValue = [
            [
                "id" => 101,
                "text" => "IRSEA"
            ]
        ];
        $this->searchParametersModel->user->setReturnValue('getUserPrograms', $returnValue);
        $this->searchParametersModel->user->setReturnValue('isAdmin', true);

        // expected
        $expected = [
            "count" => 1,
            "data" => [
                [
                    "id" => 101,
                    "text" => "IRSEA"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->getVariableFilterData($searchTerm, $userId, $filterId, $variableFilters, $defaultFilters, $limit, $page);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableFilterData: Test 3
     * Program filter, _id filter has value,
     * with search term
     */
    public function testGetVariableFilterDataTest3()
    {
        // ARRANGE
        // inputs
        $searchTerm = "IRSEA";
        $userId = 1;
        $filterId = 'program-filter';
        $variableFilters = [];
        $defaultFilters = [
            "owned" => "true",
            "season_id" => ["1", "2"]
        ];
        $limit = 5;
        $page = 1;

        // mocks
        $returnValue = [
            [
                "id" => 101,
                "text" => "IRSEA"
            ]
        ];
        $this->searchParametersModel->user->setReturnValue('getUserPrograms', $returnValue);
        $this->searchParametersModel->user->setReturnValue('isAdmin', true);

        // expected
        $expected = [
            "count" => 1,
            "data" => [
                [
                    "id" => 101,
                    "text" => "IRSEA"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->getVariableFilterData($searchTerm, $userId, $filterId, $variableFilters, $defaultFilters, $limit, $page);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableFilterData: Test 4
     * Experiment name filter
     */
    public function testGetVariableFilterDataTest4()
    {
        // ARRANGE
        // inputs
        $searchTerm = null;
        $userId = 1;
        $filterId = 'experiment_name-filter';
        $variableFilters = [
            [
                "abbrev" => "PROGRAM",
                "values" => "102",
                "config_info" => [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Program that manages the experiment",
                    "field_label" => "Program",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "1",
                    "required" => "false",
                    "variable_abbrev" => "PROGRAM"
                ]
            ]
        ];
        $defaultFilters = [
            "owned" => "false"
        ];
        $limit = 5;
        $page = 1;

        // mocks
        $returnValue = [
            "totalCount" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1"
                ]
            ]
        ];
        $this->searchParametersModel->experiment->setReturnValue('getExperimentSelect2Data', $returnValue);
        $this->searchParametersModel->user->setReturnValue('isAdmin', true);
        $returnValue = [
            "data" => [
                [
                    "programCode" => "ABC"
                ]
            ]
        ];
        $this->searchParametersModel->program->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            "count" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->getVariableFilterData($searchTerm, $userId, $filterId, $variableFilters, $defaultFilters, $limit, $page);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableFilterData: Test 5
     * Experiment name filter with dashboard filters
     */
    public function testGetVariableFilterDataTest5()
    {
        // ARRANGE
        // inputs
        $searchTerm = null;
        $userId = 1;
        $filterId = 'experiment_name-filter';
        $variableFilters = [
            [
                "abbrev" => "PROGRAM",
                "values" => "102",
                "config_info" => [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Program that manages the experiment",
                    "field_label" => "Program",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "1",
                    "required" => "false",
                    "variable_abbrev" => "PROGRAM"
                ]
            ]
        ];
        $defaultFilters = [
            "owned" => "false",
            "season_id" => ["11", "12"],
            "site_id" => ["10001"],
            "stage_id" => ["121", "122"],
            "year" => ["2021", "2022"]
        ];
        $limit = 5;
        $page = 1;

        // mocks
        $returnValue = [
            "totalCount" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1"
                ]
            ]
        ];
        $this->searchParametersModel->experiment->setReturnValue('getExperimentSelect2Data', $returnValue);
        $this->searchParametersModel->user->setReturnValue('isAdmin', true);
        $returnValue = [
            "data" => [
                [
                    "programCode" => "ABC"
                ]
            ]
        ];
        $this->searchParametersModel->program->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            "count" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->getVariableFilterData($searchTerm, $userId, $filterId, $variableFilters, $defaultFilters, $limit, $page);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableFilterData: Test 6
     * Experiment name filter with dashboard filters, owned = true
     */
    public function testGetVariableFilterDataTest6()
    {
        // ARRANGE
        // inputs
        $searchTerm = null;
        $userId = 1;
        $filterId = 'experiment_name-filter';
        $variableFilters = [
            [
                "abbrev" => "PROGRAM",
                "values" => "102",
                "config_info" => [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Program that manages the experiment",
                    "field_label" => "Program",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "1",
                    "required" => "false",
                    "variable_abbrev" => "PROGRAM"
                ]
            ]
        ];
        $defaultFilters = [
            "owned" => "true",
            "season_id" => ["11", "12"],
            "site_id" => ["10001"],
            "stage_id" => ["121", "122"],
            "year" => ["2021", "2022"]
        ];
        $limit = 5;
        $page = 1;

        // mocks
        $returnValue = [
            "totalCount" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1"
                ]
            ]
        ];
        $this->searchParametersModel->experiment->setReturnValue('getExperimentSelect2Data', $returnValue);
        $this->searchParametersModel->user->setReturnValue('isAdmin', true);
        $returnValue = [
            "data" => [
                [
                    "programCode" => "ABC"
                ]
            ]
        ];
        $this->searchParametersModel->program->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            "count" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->getVariableFilterData($searchTerm, $userId, $filterId, $variableFilters, $defaultFilters, $limit, $page);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableFilterData: Test 7
     * Experiment name filter with dashboard filters, owned = true
     * with search term
     */
    public function testGetVariableFilterDataTest7()
    {
        // ARRANGE
        // inputs
        $searchTerm = "IRSEA_EXP_1";
        $userId = 1;
        $filterId = 'experiment_name-filter';
        $variableFilters = [
            [
                "abbrev" => "PROGRAM",
                "values" => "102",
                "config_info" => [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Program that manages the experiment",
                    "field_label" => "Program",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "1",
                    "required" => "false",
                    "variable_abbrev" => "PROGRAM"
                ]
            ]
        ];
        $defaultFilters = [
            "owned" => "true",
            "season_id" => ["11", "12"],
            "site_id" => ["10001"],
            "stage_id" => ["121", "122"],
            "year" => ["2021", "2022"]
        ];
        $limit = 5;
        $page = 1;

        // mocks
        $returnValue = [
            "totalCount" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1"
                ]
            ]
        ];
        $this->searchParametersModel->experiment->setReturnValue('getExperimentSelect2Data', $returnValue);
        $this->searchParametersModel->user->setReturnValue('isAdmin', true);
        $returnValue = [
            "data" => [
                [
                    "programCode" => "ABC"
                ]
            ]
        ];
        $this->searchParametersModel->program->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            "count" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->getVariableFilterData($searchTerm, $userId, $filterId, $variableFilters, $defaultFilters, $limit, $page);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getVariableFilterData: Test 8
     * Occurrence name filter with dashboard filters, owned = true
     * with search term
     */
    public function testGetVariableFilterDataTest8()
    {
        // ARRANGE
        // inputs
        $searchTerm = "IRSEA_EXP_1";
        $userId = 1;
        $filterId = 'occurrence_name-filter';
        $variableFilters = [
            [
                "abbrev" => "PROGRAM",
                "values" => "102",
                "config_info" => [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Program that manages the experiment",
                    "field_label" => "Program",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "1",
                    "required" => "false",
                    "variable_abbrev" => "PROGRAM"
                ]
            ],
            [
                "abbrev" => "EXPERIMENT_NAME",
                "values" => "10123",
                "config_info" => [
                    "allowed_values" => "",
                    "basic_parameter" => "true",
                    "default_value" => "",
                    "field_description" => "Name of the experiment",
                    "field_label" => "Experiment",
                    "input_field" => "selection",
                    "input_type" => "single",
                    "order_number" => "3",
                    "required" => "false",
                    "variable_abbrev" => "EXPERIMENT_NAME"
                ]
            ]
        ];
        $defaultFilters = [
            "owned" => "true",
            "season_id" => ["11", "12"],
            "site_id" => ["10001"],
            "stage_id" => ["121", "122"],
            "year" => ["2021", "2022"]
        ];
        $limit = 5;
        $page = 1;

        // mocks
        $returnValue = [
            "totalCount" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1_OCC_1"
                ]
            ]
        ];
        $this->searchParametersModel->occurrence->setReturnValue('getOccurrenceSelect2Data', $returnValue);
        $this->searchParametersModel->user->setReturnValue('isAdmin', true);
        $returnValue = [
            "data" => [
                [
                    "programCode" => "ABC"
                ]
            ]
        ];
        $this->searchParametersModel->program->setReturnValue('searchAll', $returnValue);

        // expected
        $expected = [
            "count" => 1,
            "data" => [
                [
                    "id" => 1001,
                    "text" => "IRSEA_EXP_1_OCC_1"
                ]
            ]
        ];

        // ACT
        $actual = $this->searchParametersModel->getVariableFilterData($searchTerm, $userId, $filterId, $variableFilters, $defaultFilters, $limit, $page);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}
