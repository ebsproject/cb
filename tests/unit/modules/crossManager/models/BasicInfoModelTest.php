<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\crossManager\models;

use Yii;

class BasicInfoModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $basicInfoModel;

    /**
     * Executed before each test
     */
    protected function _before()
    {
        $this->basicInfoModel = Yii::$container->get('basicInfoModel');
    }

    /**
     * Executed after each test
     */
    protected function _after()
    {
    }

    // tests
    /**
     * saveCrossList: Test 1
     * Test saving of existing cross list
     */
    public function testUpdateCrossList()
    {
        //ARRANGE
        $entryListId = 123;
        $formData = [
            'entryListName' => 'test'
        ];
        // Mock EntryList updateOne method
        $this->basicInfoModel->entryList->setReturnValue('updateOne', true);

        // ACT
        $actual = $this->basicInfoModel->saveCrossList($entryListId, $formData);
        $expected = true;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * saveCrossList: Test 2
     * Test saving of a new cross list
     */
    public function testCreateCrossList()
    {
        //ARRANGE
        $entryListId = 0;
        $formData = [
            'programDbId' => 101
        ];
        // Mock EntryList and Crop searchAll method
        $this->basicInfoModel->entryList->setReturnValue('searchAll', [
            'totalCount' => 0
        ]);
        $this->basicInfoModel->crop->setReturnValue('searchAll', [
            'data' => [[
                'cropDbId' => '1'
            ]]
        ]);
        $this->basicInfoModel->entryList->setReturnValue('create', true);

        // ACT
        $actual = $this->basicInfoModel->saveCrossList($entryListId, $formData);
        $expected = true;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}