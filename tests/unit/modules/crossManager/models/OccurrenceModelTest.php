<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\crossManager\models;

use Yii;
use \yii\web\CookieCollection;
use app\dataproviders\ArrayDataProvider;

class OccurrenceModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $occurrenceModel;

    /**
     * Executed before each test
     */
    protected function _before()
    {
        $this->occurrenceModel = Yii::$container->get('crossManagerOccurrenceModel');
    }

    /**
     * Executed after each test
     */
    protected function _after()
    {
    }

    // tests
    /**
     * search: Test 1
     * Test data provider search with no data
     */
    public function testSearchWithNoData()
    {
        //ARRANGE
        $entryListInfo['experimentYear'] = '1993';
        $entryListInfo['geospatialObjectName'] = 'Calamba';
        $entryListInfo['seasonCode'] = 'DS';

        // Mock Occurrence searchAll method
        $this->occurrenceModel->occurrence->setReturnValue('searchAll', [
            'data' => [],
            'totalCount' => 0,
            'totalPages' => 0,
        ]);

        // ACT
        $actual = $this->occurrenceModel->search([], '', $entryListInfo);
        $expected = new ArrayDataProvider([
            'allModels' => [],
            'key' => 'occurrenceDbId',
            'sort' => [
                'defaultOrder' => ['occurrenceDbId'=>SORT_ASC],
                'attributes' => [
                    'occurrenceDbId',
                    'occurrenceCode',
                    'occurrenceName',
                    'programDbId',
                    'siteDbId',
                    'site',
                    'experimentDbId',
                    'experimentCode',
                    'experiment',
                    'experimentType',
                    'experimentStageCode',
                    'experimentYear',
                    'experimentSeasonDbId',
                    'experimentSeasonCode',
                    'experimentStageCode',
                    'entryCount',
                    'occurrenceStatus',
                ] 
            ],
            'restified' => true,
            'totalCount' => 0,
            'id' => 'cm-occurrence-browser'
        ]);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * search: Test 2
     * Test data provider search with data
     */
    public function testSearchWithData()
    {
        // ARRANGE
        $data = [
            [
                'occurrenceDbId' => 123
            ]
        ];
        $entryListInfo['experimentYear'] = '1993';
        $entryListInfo['geospatialObjectName'] = 'Calamba';
        $entryListInfo['seasonCode'] = 'DS';

        // Mock Occurrence searchAll method
        $this->occurrenceModel->occurrence->setReturnValue('searchAll', [
            'data' => $data,
            'totalCount' => 1,
            'totalPages' => 1,
        ]);

        // ACT
        $actual = $this->occurrenceModel->search([], '', $entryListInfo, ['test']);
        $expected = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'occurrenceDbId',
            'sort' => [
                'defaultOrder' => ['occurrenceDbId'=>SORT_ASC],
                'attributes' => [
                    'occurrenceDbId',
                    'occurrenceCode',
                    'occurrenceName',
                    'programDbId',
                    'siteDbId',
                    'site',
                    'experimentDbId',
                    'experimentCode',
                    'experiment',
                    'experimentType',
                    'experimentStageCode',
                    'experimentYear',
                    'experimentSeasonDbId',
                    'experimentSeasonCode',
                    'experimentStageCode',
                    'entryCount',
                    'occurrenceStatus',
                ] 
            ],
            'restified' => true,
            'totalCount' => 1,
            'id' => 'cm-occurrence-browser'
        ]);
        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * getFilters: Test 1
     * Verify page size from cookies
     */
    public function xtestGetFiltersPageSize(){
        // ARRANGE
        $gridName = 'dynagrid-cm-occurrence-grid_';
        
        // MOCK
        $getPageSize = $this->occurrenceModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 50);

        $expected = 'limit=50';
        //ACT
        $actual = $this->occurrenceModel->getFilters([
            'id' => '253',
            'program' => 'IRSEA'
        ]);
        //ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * getFilters: Test 2
     * Verify ASC column pjax sorting
     */
    public function xtestGetFiltersAscendingColumnSort(){
        // ARRANGE
        $gridName = 'dynagrid-cm-occurrence-grid_';

        // MOCK
        $getPageSize = $this->occurrenceModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 50);

        $expected = 'limit=50&sort=experimentYear';
        $actual = $this->occurrenceModel->getFilters([
            'sort' => 'experimentYear',
            '_pjax' => '#dynagrid-cm-occurrence-grid-pjax'
        ]);
        $this->assertEquals($expected, $actual);
    }

    /**
     * getFilters: Test 3
     * Verify DESC column pjax sorting
     */
    public function xtestGetFiltersDescendingColumnSort(){
        // ARRANGE
        $gridName = 'dynagrid-cm-occurrence-grid_';

        // MOCK
        $getPageSize = $this->occurrenceModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 50);

        $expected = 'limit=50&sort=experimentYear:DESC';
        $actual = $this->occurrenceModel->getFilters([
            'sort' => '-experimentYear',
            '_pjax' => '#dynagrid-cm-occurrence-grid-pjax'
        ]);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that each crop's eligible experiment type and stages for crossing are returned.
     *
     * @skip Deleted function
     */
    public function xtestGetValidCrossesPerCrop() {
        // ARRANGE
        $validExperimentStages = [
            'Breeding Trial' => [
                'IYT',
                'OYT',
                'PYT',
                'AYT',
                'MET0',
                'MET1',
                'MET2',
                'RYT',
            ],
            'Cross Parent Nursery' => [
                'HB',
                'F1',
            ],
            'Generation Nursery' => [
                'BRE',
                'F1',
                'F2',
                'F3',
                'F4',
                'F5',
                'F6',
                'F7',
                'F8',
                'F9',
                'RGA',
                'SEM',
                'TCV',
            ],
            'Intentional Crossing Nursery' => [
                'HB',
                'F1',
            ],
            'Observation' => [
                'UND',
                'BRE',
                'SEM',
            ]
        ];

        $expected = [
            'RICE' => [
                'Breeding Trial' => [
                    'stages' => [
                        'IYT',
                        'OYT',
                        'PYT',
                        'AYT',
                        'MET0',
                        'MET1',
                        'MET2',
                        'RYT',
                    ],
                ],
                'Cross Parent Nursery' => [
                    'stages' => [
                        'HB',
                        'F1',
                    ],
                ],
                'Generation Nursery' => [
                    'stages' => [
                        'RGA',
                    ],
                ],
                'Intentional Crossing Nursery' => [
                    'stages' => [
                        'HB',
                        'F1',
                    ],
                ],
                'Observation' => [
                    'stages' => [
                        'BRE',
                        'SEM',
                    ],
                ],
            ],
            'WHEAT' => [
                'Breeding Trial' => [
                    'stages' => [
                        'IYT',
                        'OYT',
                        'PYT',
                        'AYT',
                        'MET0',
                        'MET1',
                        'MET2',
                        'RYT',
                    ],
                ],
                'Cross Parent Nursery' => [
                    'stages' => [
                        'HB',
                        'F1',
                    ],
                ],
                'Intentional Crossing Nursery' => [
                    'stages' => [
                        'HB',
                        'F1',
                    ],
                ],
                'Observation' => [
                    'stages' => [
                        'UND',
                        'BRE',
                        'SEM',
                    ],
                ],
            ],
            'MAIZE' => [
                'Breeding Trial' => [
                    'stages' => [
                        'IYT',
                        'OYT',
                        'PYT',
                        'AYT',
                        'MET0',
                        'MET1',
                        'MET2',
                        'RYT',
                    ],
                ],
                'Cross Parent Nursery' => [
                    'stages' => [
                        'HB',
                        'F1',
                    ],
                ],
                'Generation Nursery' => [
                    'stages' => [
                        'BRE',
                        'F1',
                        'F2',
                        'F3',
                        'F4',
                        'F5',
                        'F6',
                        'F7',
                        'F8',
                        'F9',
                        'RGA',
                        'SEM',
                        'TCV',
                    ],
                ],
                'Intentional Crossing Nursery' => [
                    'stages' => [
                        'HB',
                        'F1',
                    ],
                ],
                'Observation' => [
                    'stages' => [
                        'UND',
                        'BRE',
                        'SEM',
                    ],
                ],
            ],
            'BARLEY' => [
                'Breeding Trial' => [
                    'stages' => [
                        'IYT',
                        'OYT',
                        'PYT',
                        'AYT',
                        'MET0',
                        'MET1',
                        'MET2',
                        'RYT',
                    ],
                ],
                'Cross Parent Nursery' => [
                    'stages' => [
                        'HB',
                        'F1',
                    ],
                ],
                'Generation Nursery' => [
                    'stages' => [
                        'BRE',
                        'F1',
                        'F2',
                        'F3',
                        'F4',
                        'F5',
                        'F6',
                        'F7',
                        'F8',
                        'F9',
                        'RGA',
                        'SEM',
                        'TCV',
                    ],
                ],
                'Intentional Crossing Nursery' => [
                    'stages' => [
                        'HB',
                        'F1',
                    ],
                ],
                'Observation' => [
                    'stages' => [
                        'UND',
                        'BRE',
                        'SEM',
                    ],
                ],
            ]
        ];

        $actual = $this->occurrenceModel->getValidCrossesPerCrop($validExperimentStages);
        $this->assertEqualsCanonicalizing($expected, $actual);
    }


}