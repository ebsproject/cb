<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\crossManager\models;

use Yii;

class CrossModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $crossModel;
    protected $parentModel;

    protected function _before()
    {
        $this->crossModel = Yii::$container->get('crossManagerCrossModel');
        $this->parentModel = Yii::$container->get('crossManagerParentModel');
    }

    protected function _after()
    {
    }

    /**
     * Verify that supported cross methods for maize is returned
     */
    public function xtestGetValidCrossMethod() {

        // Mock config
        $returnValue = [
            'default' => [],
            'CROSS_METHOD_SELFING' => [],
            'CROSS_METHOD_BACKCROSS' => [],
            'CROSS_METHOD_DOUBLE_CROSS' => [],
            'CROSS_METHOD_SINGLE_CROSS' => [],
            'CROSS_METHOD_THREE_WAY_CROSS' => [],
            'CROSS_METHOD_HYBRID_FORMATION' => [],
            'CROSS_METHOD_MATERNAL_HAPLOID_INDUCTION' => []
        ];
        $this->crossModel->configModel->setReturnValue('getConfigByAbbrev', $returnValue);

        // TODO: Mock $this->crossModel->getScaleValues return value

        $expected = [
            'selfing' => 'selfing',
            'backcross' => 'backcross',
            'double cross' => 'double cross',
            'single cross' => 'single cross',
            'three way cross' => 'three way cross',
            'hybrid formation' => 'hybrid formation',
            'maternal haploid induction' => 'maternal haploid induction',
        ];

        $actual = $this->crossModel->getValidCrossMethod('MAIZE');
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that syntactically correct cross pairings are returned in a structured form
     */
    public function testParseCrossList()
    {
        $inputCrosses = [
            1 => 'ENTRY_CODE1|ENT_CODE1',
            5 => ' ENTRY_CODE2 | ENT_CODE2 ',
            10 => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
            50 => ' OCCURRENCE_CODE2 : ENTRY_CODE2 | OCC_CODE2 : ENT_CODE2 '
        ];

        $expected = [
            'crosses' => [
                1 => [
                    'female' => 'ENTRY_CODE1',
                    'male' => 'ENT_CODE1',
                    'femaleEntryCode' => 'ENTRY_CODE1',
                    'maleEntryCode' => 'ENT_CODE1',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                    'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                ],
                5 => [
                    'female' => 'ENTRY_CODE2',
                    'male' => 'ENT_CODE2',
                    'femaleEntryCode' => 'ENTRY_CODE2',
                    'maleEntryCode' => 'ENT_CODE2',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => ' ENTRY_CODE2 | ENT_CODE2 ',
                    'crossKey' => 'ENTRY_CODE2,ENT_CODE2',
                ],
                10 => [
                    'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                    'male' => 'OCC_CODE1:ENT_CODE1',
                    'femaleEntryCode' => 'ENTRY_CODE1',
                    'maleEntryCode' => 'ENT_CODE1',
                    'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                    'maleOccurrenceCode' => 'OCC_CODE1',
                    'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                    'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1'
                ],
                50 => [
                    'female' => 'OCCURRENCE_CODE2:ENTRY_CODE2',
                    'male' => 'OCC_CODE2:ENT_CODE2',
                    'femaleEntryCode' => 'ENTRY_CODE2',
                    'maleEntryCode' => 'ENT_CODE2',
                    'femaleOccurrenceCode' => 'OCCURRENCE_CODE2',
                    'maleOccurrenceCode' => 'OCC_CODE2',
                    'crossInput' => ' OCCURRENCE_CODE2 : ENTRY_CODE2 | OCC_CODE2 : ENT_CODE2 ',
                    'crossKey' => 'OCCURRENCE_CODE2:ENTRY_CODE2,OCC_CODE2:ENT_CODE2'
                ]
            ],
            'entryCodes' => [
                'ENTRY_CODE1' => 2,
                'ENT_CODE1' => 2,
                'ENTRY_CODE2' => 2,
                'ENT_CODE2' => 2,
            ],
            'crossKeys' => [
                'ENTRY_CODE1,ENT_CODE1' => 1,
                'ENTRY_CODE2,ENT_CODE2' => 1,
                'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1' => 1,
                'OCCURRENCE_CODE2:ENTRY_CODE2,OCC_CODE2:ENT_CODE2' => 1
            ]
        ];

        $actual = $this->crossModel->parseCrossList($inputCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that an empty cross list input returns an array of empty crosses and crossKeys
     */
    public function testParseCrossListEmpty() {
        $inputCrosses = [];
        $expected = [
            'crosses' => [],
            'entryCodes' => [],
            'crossKeys' => []
        ];
        $actual = $this->crossModel->parseCrossList($inputCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that an empty cross pair or all whitespace is returned in structured form
     */
    public function testParseCrossListEmptyCross() {
        $inputCrosses = [
            1 => '',
            10 => ' '
        ];

        $expected = [
            'crosses' => [
                1 => [
                    'female' => '',
                    'male' => '',
                    'femaleEntryCode' => '',
                    'maleEntryCode' => '',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => '',
                    'crossKey' => ','
                ],
                10 => [
                    'female' => '',
                    'male' => '',
                    'femaleEntryCode' => '',
                    'maleEntryCode' => '',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => ' ',
                    'crossKey' => ','
                ]
            ],
            'entryCodes' => [
              '' => 4
            ],
            'crossKeys' => [
                ',' => 2
            ]
        ];

        $actual = $this->crossModel->parseCrossList($inputCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that crosses with missing vertical bar '|' are returned in structured form
     */
    public function testParseCrossListMissingPipe() {
        $inputCrosses = [
            'ENTRY_CODE1ENT_CODE1',
            'ENTRY_CODE2  ENT_CODE2'
        ];

        $expected = [
            'crosses' => [
                0 => [
                    'female' => '',
                    'male' => '',
                    'femaleEntryCode' => '',
                    'maleEntryCode' => '',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => 'ENTRY_CODE1ENT_CODE1',
                    'crossKey' => ',',
                ],
                1 => [
                    'female' => '',
                    'male' => '',
                    'femaleEntryCode' => '',
                    'maleEntryCode' => '',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => 'ENTRY_CODE2  ENT_CODE2',
                    'crossKey' => ',',
                ],
            ],
            'entryCodes' => [
                '' => 4
            ],
            'crossKeys' => [
                ',' => 2
            ]
        ];

        $actual = $this->crossModel->parseCrossList($inputCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that crosses with wrong vertical bar '|' placement are returned in structured form
     */
    public function testParseCrossListMisplacedPipe() {
        $inputCrosses = [
            '|ENTRY_CODE1|ENT_CODE1',
            'ENTRY_CODE2ENT_CODE2|',
            'EXTRA|ENTRY_CODE3|ENT_CODE3',
        ];

        $expected = [
            'crosses' => [
                0 => [
                    'female' => 'ENTRY_CODE1',
                    'male' => 'ENT_CODE1',
                    'femaleEntryCode' => 'ENTRY_CODE1',
                    'maleEntryCode' => 'ENT_CODE1',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => '|ENTRY_CODE1|ENT_CODE1',
                    'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
                ],
                1 => [
                    'female' => '',
                    'male' => '',
                    'femaleEntryCode' => '',
                    'maleEntryCode' => '',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => 'ENTRY_CODE2ENT_CODE2|',
                    'crossKey' => ','
                ],
                2 => [
                    'female' => 'ENTRY_CODE3',
                    'male' => 'ENT_CODE3',
                    'femaleEntryCode' => 'ENTRY_CODE3',
                    'maleEntryCode' => 'ENT_CODE3',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => 'EXTRA|ENTRY_CODE3|ENT_CODE3',
                    'crossKey' => 'ENTRY_CODE3,ENT_CODE3'
                ]
            ],
            'entryCodes' => [
                'ENTRY_CODE1' => 1,
                'ENT_CODE1' => 1,
                '' => 2,
                'ENTRY_CODE3' => 1,
                'ENT_CODE3' => 1
            ],
            'crossKeys' => [
                'ENTRY_CODE1,ENT_CODE1' => 1,
                ',' => 1,
                'ENTRY_CODE3,ENT_CODE3' => 1
            ]
        ];

        $actual = $this->crossModel->parseCrossList($inputCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that crosses with missing colon ':' are returned in structured form
     */
    public function testParseCrossListMissingColon() {
        $inputCrosses = [
            'OCCURRENCE_CODE1ENTRY_CODE1|OCC_CODE1ENT_CODE1',
            'OCCURRENCE_CODE2ENTRY_CODE2|OCC_CODE:ENT_CODE2',
            'OCCURRENCE_CODE2:ENTRY_CODE2|OCC_CODEENT_CODE2',
        ];

        $expected = [
            'crosses' => [
                0 => [
                    'female' => 'OCCURRENCE_CODE1ENTRY_CODE1',
                    'male' => 'OCC_CODE1ENT_CODE1',
                    'femaleEntryCode' => 'OCCURRENCE_CODE1ENTRY_CODE1',
                    'maleEntryCode' => 'OCC_CODE1ENT_CODE1',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => 'OCCURRENCE_CODE1ENTRY_CODE1|OCC_CODE1ENT_CODE1',
                    'crossKey' => 'OCCURRENCE_CODE1ENTRY_CODE1,OCC_CODE1ENT_CODE1'
                ],
                1 => [
                    'female' => 'OCCURRENCE_CODE2ENTRY_CODE2',
                    'male' => 'OCC_CODE:ENT_CODE2',
                    'femaleEntryCode' => 'OCCURRENCE_CODE2ENTRY_CODE2',
                    'maleEntryCode' => 'ENT_CODE2',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => 'OCC_CODE',
                    'crossInput' => 'OCCURRENCE_CODE2ENTRY_CODE2|OCC_CODE:ENT_CODE2',
                    'crossKey' => 'OCCURRENCE_CODE2ENTRY_CODE2,OCC_CODE:ENT_CODE2'
                ],
                2 => [
                    'female' => 'OCCURRENCE_CODE2:ENTRY_CODE2',
                    'male' => 'OCC_CODEENT_CODE2',
                    'femaleEntryCode' => 'ENTRY_CODE2',
                    'maleEntryCode' => 'OCC_CODEENT_CODE2',
                    'femaleOccurrenceCode' => 'OCCURRENCE_CODE2',
                    'maleOccurrenceCode' => '',
                    'crossInput' => 'OCCURRENCE_CODE2:ENTRY_CODE2|OCC_CODEENT_CODE2',
                    'crossKey' => 'OCCURRENCE_CODE2:ENTRY_CODE2,OCC_CODEENT_CODE2'
                ]
            ],
            'entryCodes' => [
                'OCCURRENCE_CODE1ENTRY_CODE1' => 1,
                'OCC_CODE1ENT_CODE1' => 1,
                'OCCURRENCE_CODE2ENTRY_CODE2' => 1,
                'ENT_CODE2' => 1,
                'ENTRY_CODE2' => 1,
                'OCC_CODEENT_CODE2' => 1
            ],
            'crossKeys' => [
                'OCCURRENCE_CODE1ENTRY_CODE1,OCC_CODE1ENT_CODE1' => 1,
                'OCCURRENCE_CODE2ENTRY_CODE2,OCC_CODE:ENT_CODE2' => 1,
                'OCCURRENCE_CODE2:ENTRY_CODE2,OCC_CODEENT_CODE2' => 1
            ]
        ];

        $actual = $this->crossModel->parseCrossList($inputCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a duplicate cross key is counted correctly
     */
    public function testParseCrossListDuplicateEntryCode() {
        $inputCrosses = [
            'ENTRY_CODE1|ENT_CODE1',
            'ENTRY_CODE1|ENT_CODE1',
            'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE2:ENT_CODE1',
        ];

        $expected = [
            'ENTRY_CODE1,ENT_CODE1' => 2,
            'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE2:ENT_CODE1' => 1
        ];

        $actual = $this->crossModel->parseCrossList($inputCrosses)['crossKeys'];
        $this->assertEquals($expected, $actual);
    }

    public function testParseCrossListMissingFemaleParent() {
        $inputCrosses = ['|ENT_CODE1'];

        $expected = [
            'crosses' => [
                0 => [
                    'female' => '',
                    'male' => '',
                    'femaleEntryCode' => '',
                    'maleEntryCode' => '',
                    'femaleOccurrenceCode' => '',
                    'maleOccurrenceCode' => '',
                    'crossInput' => '|ENT_CODE1',
                    'crossKey' => ','
                ]
            ],
            'entryCodes' => [
                '' => 2
            ],
            'crossKeys' => [
                ',' => 1
            ]
        ];

        $actual = $this->crossModel->parseCrossList($inputCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that a cross name is generated for an input cross (without occurrence code) given that
     * the parent source has unique entry codes
     */
    public function testAddCrossNameWithoutOccurrenceCode() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1',
                'designation' => 'IR YYY',
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'crossName' => 'IR XXX|IR YYY'
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['crossName'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that a cross name is generated for an input cross (with occurrence code)
     */
    public function testAddCrossNameWithOccurrenceCode() {
        $crossList = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1',
                'designation' => 'IR YYY',
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'crossName' => 'IR XXX|IR YYY'
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['crossName'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that a cross name is not generated for an input cross whose parents are not found
     */
    public function testAddCrossNameMissingParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'crossName' => '',
                'experimentDbId' => '',
                'occurrenceDbId' => ''
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['crossName'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that a cross name is not generated for an input cross whose male parent is vague
     * due to duplicate entry codes from the male parent list and occurrence code is not specified
     */
    public function testAddCrossNameVagueParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1',
                'designation' => 'IR YYY',
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2',
                'designation' => 'IR ZZZ',
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'crossName' => '',
                'experimentDbId' => '',
                'occurrenceDbId' => ''
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['crossName'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that a cross name is fully generated for an input cross whose male parent source has duplicate entry codes
     * but unique occurrence codes, given that occurrence code is specified
     */
    public function testAddCrossNameExactParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE2',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE2:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1',
                'designation' => 'IR YYY',
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2',
                'designation' => 'IR ZZZ',
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE2',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE2:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'crossName' => 'IR XXX|IR ZZZ'
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['crossName'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an experiment ID is assigned for an input cross (without occurrence code) given that
     * the parent source has unique entry codes
     */
    public function testAddExperimentIdWithoutOccurrenceCode() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'experimentDbId' => 5,
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1',
                'experimentDbId' => 50
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'experimentDbId' => 5
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['experimentDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an experiment ID is generated for an input cross (with occurrence code)
     */
    public function testAddExperimentIdWithOccurrenceCode() {
        $crossList = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'experimentDbId' => 5,
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1',
                'experimentDbId' => 50
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'experimentDbId' => 5
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['experimentDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an experiment ID is not generated for an input cross whose parents are not found
     */
    public function testAddExperimentIdMissingParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'crossName' => '',
                'experimentDbId' => '',
                'occurrenceDbId' => ''
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['experimentDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an experiment ID is generated for an input cross whose male parent is vague
     * due to duplicate entry codes from the male parent list and occurrence code is not specified
     */
    public function testAddExperimentIdVagueMaleParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX',
                'experimentDbId' => 5,
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1',
                'designation' => 'IR YYY',
                'experimentDbId' => 50,
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2',
                'designation' => 'IR ZZZ',
                'experimentDbId' => 500
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'experimentDbId' => '5'
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['experimentDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an experiment ID is NOT generated for an input cross whose female parent is vague
     * due to duplicate entry codes from the female parent list and occurrence code is not specified
     */
    public function testAddExperimentIdVagueFemaleParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX',
                'experimentDbId' => 5,
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2',
                'designation' => 'IR YYY',
                'experimentDbId' => 5,
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2',
                'designation' => 'IR ZZZ',
                'experimentDbId' => 500
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'crossName' => '',
                'experimentDbId' => '',
                'occurrenceDbId' => ''
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['experimentDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an experiment ID is fully generated for an input cross whose female parent source has duplicate
     * entry codes but unique occurrence codes, given that occurrence code is specified
     */
    public function testAddExperimentIdExactParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE2',
                'maleOccurrenceCode' => 'OCC_CODE2',
                'crossInput' => 'OCCURRENCE_CODE2:ENTRY_CODE1|OCC_CODE2:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX',
                'experimentDbId' => 5
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1',
                'designation' => 'IR YYY',
                'experimentDbId' => 50
            ],
            2 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2',
                'designation' => 'IR ZZZ',
                'experimentDbId' => 500
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE2',
                'maleOccurrenceCode' => 'OCC_CODE2',
                'crossInput' => 'OCCURRENCE_CODE2:ENTRY_CODE1|OCC_CODE2:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'experimentDbId' => '500'
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['experimentDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an occurrence ID is assigned to an input cross (without occurrence code) given that
     * the parent source has unique entry codes
     */
    public function testAddOccurrenceIdWithoutOccurrenceCode() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceDbId' => 100,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'experimentDbId' => 5,
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceDbId' => 200,
                'occurrenceCode' => 'OCC_CODE1',
                'experimentDbId' => 50
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'occurrenceDbId' => '100'
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['occurrenceDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an occurrence ID is generated for an input cross (with occurrence code)
     */
    public function testAddOccurrenceIdWithOccurrenceCode() {
        $crossList = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceDbId' => 100,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'experimentDbId' => 5,
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceDbId' => 200,
                'occurrenceCode' => 'OCC_CODE1',
                'experimentDbId' => 50
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'occurrenceDbId' => '100'
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['occurrenceDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an occurrence ID is NOT generated for an input cross whose parents are not found
     */
    public function testAddOccurrenceIdMissingParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'crossName' => '',
                'experimentDbId' => '',
                'occurrenceDbId' => ''
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['occurrenceDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an occurrence ID is generated for an input cross whose male parent is vague
     * due to duplicate entry codes from the male parent list and occurrence code is not specified
     */
    public function testAddOccurrenceIdVagueMaleParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceDbId' => 100,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX',
                'experimentDbId' => 5,
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceDbId' => 200,
                'occurrenceCode' => 'OCC_CODE1',
                'designation' => 'IR YYY',
                'experimentDbId' => 50,
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 102,
                'occurrenceDbId' => 300,
                'occurrenceCode' => 'OCC_CODE2',
                'designation' => 'IR ZZZ',
                'experimentDbId' => 500
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'occurrenceDbId' => '100',
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['occurrenceDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an occurrence ID is NOT generated for an input cross whose female parent is vague
     * due to duplicate entry codes from the female parent list and occurrence code is not specified
     */
    public function testAddOccurrenceIdVagueFemaleParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceDbId' => 100,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX',
                'experimentDbId' => 5,
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceDbId' => 200,
                'occurrenceCode' => 'OCCURRENCE_CODE2',
                'designation' => 'IR YYY',
                'experimentDbId' => 5,
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 102,
                'occurrenceDbId' => 300,
                'occurrenceCode' => 'OCC_CODE2',
                'designation' => 'IR ZZZ',
                'experimentDbId' => 500
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'crossName' => '',
                'experimentDbId' => '',
                'occurrenceDbId' => ''
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['occurrenceDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that an occurrence ID is generated for an input cross whose female parent source has duplicate
     * entry codes but unique occurrence codes, given that occurrence code is specified
     */
    public function testAddOccurrenceIdExactParent() {
        $crossList = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE2',
                'maleOccurrenceCode' => 'OCC_CODE2',
                'crossInput' => 'OCCURRENCE_CODE2:ENTRY_CODE1|OCC_CODE2:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1'
            ]
        ];
        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceDbId' => 100,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'designation' => 'IR XXX',
                'experimentDbId' => 5
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceDbId' => 200,
                'occurrenceCode' => 'OCC_CODE1',
                'designation' => 'IR YYY',
                'experimentDbId' => 50
            ],
            2 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceDbId' => 300,
                'occurrenceCode' => 'OCCURRENCE_CODE2',
                'designation' => 'IR ZZZ',
                'experimentDbId' => 500
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentList);

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE2',
                'maleOccurrenceCode' => 'OCC_CODE2',
                'crossInput' => 'OCCURRENCE_CODE2:ENTRY_CODE1|OCC_CODE2:ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'occurrenceDbId' => '300'
            ]
        ];

        $actual = $this->crossModel->addCrossFields(['occurrenceDbId'], $crossList, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female_1 | male_1` cross is tagged
     * ```
     *   status='valid'
     *   remarks=''
     * ```
     */
    public function testValidateCrossesValid1() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female_1 | female-and-male_1` cross is tagged
     * ```
     *   status='valid'
     *   remarks=''
     * ```
     */
    public function testValidateCrossesValid2() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female-and-male_1 | female-and-male_2` cross is tagged
     * ```
     *   status='valid'
     *   remarks=''
     * ```
     */
    public function testValidateCrossesValid3() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female-and-male_1 | male_1` cross is tagged
     * ```
     *   status='valid'
     *   remarks=''
     * ```
     */
    public function testValidateCrossesValid4() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (with occurrence code) whose female parent has multiple
     * entry codes but unique occurrence codes in the parent source is tagged
     * ```
     *   status='valid'
     *   message=''
     * ```
     */
    public function testValidateCrossesValid5() {
        $result = $this->crossModel->parseCrossList(['OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that multiple crosses with pairing
     * ```
     *   female_1 | male_1
     *   female_2 | male_1
     * ```
     * are both tagged
     * ```
     *   status='valid'
     *   remarks=''
     * ```
     */
    public function testValidateCrossesMulti1() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENT_CODE1',
            20 => 'ENTRY_CODE2|ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE2',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ],
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ],
            20 => [
                'female' => 'ENTRY_CODE2',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE2',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE2|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE2,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that multiple crosses with pairing
     * ```
     *   female-and-male_1 | female-and-male_2
     *   female-and-male_3 | female-and-male_2
     * ```
     * are both tagged
     * ```
     *   status='valid'
     *   remarks=''
     * ```
     */
    public function testValidateCrossesMulti2() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENT_CODE1',
            20 => 'ENTRY_CODE2|ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE2',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ],
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ],
            20 => [
                'female' => 'ENTRY_CODE2',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE2',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE2|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE2,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that multiple crosses with pairing
     * ```
     *   female_1 | male_1
     *   female_1 | male_2
     * ```
     * are both tagged
     * ```
     *   status='valid'
     *   remarks=''
     * ```
     */
    public function testValidateCrossesMulti3() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENT_CODE1',
            20 => 'ENTRY_CODE1|ENT_CODE2'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE2',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2'
            ],
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ],
            20 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE2',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE2',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that multiple crosses with pairing
     * ```
     *   female-and-male_1 | female-and-male_2
     *   female-and-male_1 | female-and-male_3
     * ```
     * are both tagged
     * ```
     *   status='valid'
     *   remarks=''
     * ```
     */
    public function testValidateCrossesMulti4() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENT_CODE1',
            20 => 'ENTRY_CODE1|ENT_CODE2'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE2',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2'
            ],
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ],
            20 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE2',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE2',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses with pairing
     * ```
     *   female_1 | male_1
     *   female_1 | male_1
     * ```
     * are tagged
     * ```
     *   status='valid'
     *   remarks='Duplicate input cross.'
     * ```
     * for the first, while the second
     * ```
     *   status='invalid'
     *   remarks='Duplicate input cross.'
     * ```
     */
    public function testValidateCrossesMulti5() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENT_CODE1',
            20 => 'ENTRY_CODE1|ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => 'Duplicate input cross.'
            ],
            20 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Duplicate input cross.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `male_1 | male_2` cross is tagged
     * ```
     *   status='invalid'
     *   message='Female parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesInvalid1() {
        $result = $this->crossModel->parseCrossList(['ENT_CODE1|ENT_CODE2']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE2',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE2',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE2',
                'crossKey' => 'ENT_CODE1,ENT_CODE2',
                'status' => 'invalid',
                'remarks' => 'Female parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses with pairing
     * ```
     *   male_1 | male_2
     *   male_1 | male_2
     * ```
     * are both tagged
     * ```
     *   status='invalid'
     *   message='Female parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesMulti6() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENT_CODE1|ENT_CODE2',
            20 => 'ENT_CODE1|ENT_CODE2'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE2',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE2',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE2',
                'crossKey' => 'ENT_CODE1,ENT_CODE2',
                'status' => 'invalid',
                'remarks' => 'Female parent does not exist in the Parent List!'
            ],
            20 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE2',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE2',
                'crossKey' => 'ENT_CODE1,ENT_CODE2',
                'status' => 'invalid',
                'remarks' => 'Female parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `male_1 | female-and-male_1` cross is tagged
     * ```
     *   status='invalid'
     *   message='Female parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesInvalid2() {
        $result = $this->crossModel->parseCrossList(['ENT_CODE1|ENT_CODE2']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE2',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE2',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE2',
                'crossKey' => 'ENT_CODE1,ENT_CODE2',
                'status' => 'invalid',
                'remarks' => 'Female parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses with pairing
     * ```
     *   male_1 | female-and-male_1
     *   male_1 | female-and-male_1
     * ```
     * are both tagged
     * ```
     *   status='invalid'
     *   message='Female parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesMulti7() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENT_CODE1|ENT_CODE2',
            20 => 'ENT_CODE1|ENT_CODE2'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE2',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE2',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE2',
                'crossKey' => 'ENT_CODE1,ENT_CODE2',
                'status' => 'invalid',
                'remarks' => 'Female parent does not exist in the Parent List!'
            ],
            20 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE2',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE2',
                'crossKey' => 'ENT_CODE1,ENT_CODE2',
                'status' => 'invalid',
                'remarks' => 'Female parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `male_1 | male_1` cross is tagged
     * ```
     *   status='invalid'
     *   message='Female parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesInvalid3() {
        $result = $this->crossModel->parseCrossList(['ENT_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE1',
                'crossKey' => 'ENT_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Female parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses with pairing
     * ```
     *   male_1 | male_1
     *   male_1 | male_1
     * ```
     * are both tagged
     * ```
     *   status='invalid'
     *   message='Female parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesMulti8() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENT_CODE1|ENT_CODE1',
            20 => 'ENT_CODE1|ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE1',
                'crossKey' => 'ENT_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Female parent does not exist in the Parent List!'
            ],
            20 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE1',
                'crossKey' => 'ENT_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Female parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female_1 | female_1` cross is tagged
     * ```
     *   status='invalid'
     *   message='Male parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesInvalid4() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE1',
                'status' => 'invalid',
                'remarks' => 'Male parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses with pairing
     * ```
     *   female_1 | female_1
     *   female_1 | female_1
     * ```
     * are both tagged
     * ```
     *   status='invalid'
     *   message='Male parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesMulti9() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENTRY_CODE1',
            20 => 'ENTRY_CODE1|ENTRY_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE1',
                'status' => 'invalid',
                'remarks' => 'Male parent does not exist in the Parent List!'
            ],
            20 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE1',
                'status' => 'invalid',
                'remarks' => 'Male parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female-and-male_1 | female-and-male_1` cross is tagged
     * ```
     *   status='invalid'
     *   message='Same parent info. Please use SELF feature for selfing.'
     * ```
     */
    public function testValidateCrossesInvalid5() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE1',
                'status' => 'invalid',
                'remarks' => 'Same parent info. Please use SELF feature for selfing.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses with pairing
     * ```
     *   female-and-male_1 | female-and-male_1
     *   female-and-male_1 | female-and-male_1
     * ```
     * are both tagged
     * ```
     *   status='invalid'
     *   message='Same parent info. Please use SELF feature for selfing.'
     * ```
     */
    public function testValidateCrossesMulti10() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENTRY_CODE1',
            20 => 'ENTRY_CODE1|ENTRY_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE1',
                'status' => 'invalid',
                'remarks' => 'Same parent info. Please use SELF feature for selfing.'
            ],
            20 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE1',
                'status' => 'invalid',
                'remarks' => 'Same parent info. Please use SELF feature for selfing.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female_1` cross is tagged
     * ```
     *   status='invalid'
     *   message='Invalid cross format.'
     * ```
     */
    public function testValidateCrossesInvalid6() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => '',
                'male' => '',
                'femaleEntryCode' => '',
                'maleEntryCode' => '',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1',
                'crossKey' => ',',
                'status' => 'invalid',
                'remarks' => 'Invalid cross format.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses with pairing
     * ```
     *   female_1
     *   female_1
     * ```
     * are both tagged
     * ```
     *   status='invalid'
     *   message='Invalid cross format.'
     * ```
     */
    public function testValidateCrossesMulti11() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1',
            20 => 'ENTRY_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => '',
                'male' => '',
                'femaleEntryCode' => '',
                'maleEntryCode' => '',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1',
                'crossKey' => ',',
                'status' => 'invalid',
                'remarks' => 'Invalid cross format.'
            ],
            20 => [
                'female' => '',
                'male' => '',
                'femaleEntryCode' => '',
                'maleEntryCode' => '',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1',
                'crossKey' => ',',
                'status' => 'invalid',
                'remarks' => 'Invalid cross format.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female_1 |` cross is tagged
     * ```
     *   status='invalid'
     *   message='Invalid cross format.'
     * ```
     */
    public function testValidateCrossesInvalid7() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1 |']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => '',
                'male' => '',
                'femaleEntryCode' => '',
                'maleEntryCode' => '',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1 |',
                'crossKey' => ',',
                'status' => 'invalid',
                'remarks' => 'Invalid cross format.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female-and-male_1` cross is tagged
     * ```
     *   status='invalid'
     *   message='Invalid cross format.'
     * ```
     */
    public function testValidateCrossesInvalid8() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => '',
                'male' => '',
                'femaleEntryCode' => '',
                'maleEntryCode' => '',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1',
                'crossKey' => ',',
                'status' => 'invalid',
                'remarks' => 'Invalid cross format.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `male_1` cross is tagged
     * ```
     *   status='invalid'
     *   message='Invalid cross format.'
     * ```
     */
    public function testValidateCrossesInvalid9() {
        $result = $this->crossModel->parseCrossList(['ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => '',
                'male' => '',
                'femaleEntryCode' => '',
                'maleEntryCode' => '',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1',
                'crossKey' => ',',
                'status' => 'invalid',
                'remarks' => 'Invalid cross format.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `|male_1` cross is tagged
     * ```
     *   status='invalid'
     *   message='Invalid cross format.'
     * ```
     */
    public function testValidateCrossesInvalid10() {
        $result = $this->crossModel->parseCrossList(['|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => '',
                'male' => '',
                'femaleEntryCode' => '',
                'maleEntryCode' => '',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => '|ENT_CODE1',
                'crossKey' => ',',
                'status' => 'invalid',
                'remarks' => 'Invalid cross format.'
            ]
        ];


        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female_1 | female_2` cross is tagged
     * ```
     *   status='invalid'
     *   message='Male parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesInvalid11() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENTRY_CODE2']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE2',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE2',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE2',
                'status' => 'invalid',
                'remarks' => 'Male parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses with pairing
     * ```
     *   female_1 | female_2
     *   female_1 | female_2
     * ```
     * are both tagged
     * ```
     *   status='invalid'
     *   message='Male parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesMulti12() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENTRY_CODE2',
            20 => 'ENTRY_CODE1|ENTRY_CODE2'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE2',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE2',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE2',
                'status' => 'invalid',
                'remarks' => 'Male parent does not exist in the Parent List!'
            ],
            20 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE2',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE2',
                'status' => 'invalid',
                'remarks' => 'Male parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that `female-and-male_1 | female_1` cross is tagged
     * ```
     *   status='invalid'
     *   message='Male parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesInvalid12() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENTRY_CODE2']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE2',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE2',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE2',
                'status' => 'invalid',
                'remarks' => 'Male parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses with pairing
     * ```
     *   female-and-male_1 | female_1
     *   female-and-male_1 | female_1
     * ```
     * are both tagged
     * ```
     *   status='invalid'
     *   message='Male parent does not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesMulti13() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENTRY_CODE2',
            20 => 'ENTRY_CODE1|ENTRY_CODE2'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE2',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE2',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE2',
                'status' => 'invalid',
                'remarks' => 'Male parent does not exist in the Parent List!'
            ],
            20 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE2',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE2',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE2',
                'status' => 'invalid',
                'remarks' => 'Male parent does not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a cross whose parents do not exist in either list is tagged
     * ```
     *   status='invalid'
     *   message='Both parents do not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesInvalid13() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Both parents do not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate crosses whose parents do not exist in either parent list are both tagged
     * ```
     *   status='invalid'
     *   message='Both parents do not exist in the Parent List!'
     * ```
     */
    public function testValidateCrossesMulti14() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENT_CODE1',
            20 => 'ENTRY_CODE1|ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'XXX',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'YYY',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Both parents do not exist in the Parent List!'
            ],
            20 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Both parents do not exist in the Parent List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` that was crossed before is tagged
     * ```
     *   status='invalid'
     *   message='Already exists in the Cross List!'
     * ```
     */
    public function testValidateCrossesInvalid14() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [
            0 => [
                'crossDbId' => 1000,
                'parentDbId' => '11,101'
            ]
        ];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Already exists in the Cross List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate pairings
     * ```
     *   female_1 | male_1
     *   female_1 | male_1
     * ```
     * which has previously been crossed are both tagged
     * ```
     *   status='invalid'
     *   message='Already exists in the Cross List!'
     * ```
     */
    public function testValidateCrossesMulti15() {
        $result = $this->crossModel->parseCrossList([
            10 => 'ENTRY_CODE1|ENT_CODE1',
            20 => 'ENTRY_CODE1|ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [
            0 => [
                'crossDbId' => 1000,
                'parentDbId' => '11,101'
            ]
        ];

        $expected = [
            10 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Already exists in the Cross List!'
            ],
            20 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Already exists in the Cross List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (with occurrence code) whose female parent has multiple
     * copies of itself in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Repeated female parent found in the Parent List.'
     * ```
     */
    public function testValidateCrossesInvalid15() {
        $result = $this->crossModel->parseCrossList(['OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated female parent found in the Parent List.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate pairings (with occurrence code)
     * ```
     *   female_1 | male_1
     *   female_1 | male_1
     * ```
     * whose female parent has multiple copies of itself in the parent source are both tagged
     * ```
     *   status='invalid'
     *   message='Repeated female parent found in the Parent List.'
     * ```
     */
    public function testValidateCrossesMulti16() {
        $result = $this->crossModel->parseCrossList([
            10 => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
            20 => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated female parent found in the Parent List.'
            ],
            20 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated female parent found in the Parent List.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (with occurrence code) whose male parent has multiple
     * copies of itself in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Repeated male parent found in the Parent List.'
     * ```
     */
    public function testValidateCrossesInvalid16() {
        $result = $this->crossModel->parseCrossList(['OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated male parent found in the Parent List.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate pairings (with occurrence code)
     * ```
     *   female_1 | male_1
     *   female_1 | male_1
     * ```
     * whose male parent has multiple copies of itself in the parent source are both tagged
     * ```
     *   status='invalid'
     *   message='Repeated male parent found in the Parent List.'
     * ```
     */
    public function testValidateCrossesMulti17() {
        $result = $this->crossModel->parseCrossList([
            10 => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
            20 => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated male parent found in the Parent List.'
            ],
            20 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated male parent found in the Parent List.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (with occurrence code) whose parents have multiple
     * copies of themselves in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Repeated female and male parents found in the Parent List.'
     * ```
     */
    public function testValidateCrossesInvalid17() {
        $result = $this->crossModel->parseCrossList(['OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            3 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated female and male parents found in the Parent List.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate pairings (with occurrence code)
     * ```
     *   female_1 | male_1
     *   female_1 | male_1
     * ```
     * whose parents have multiple copies of themselves in the parent source are both tagged
     * ```
     *   status='invalid'
     *   message='Repeated female and male parents found in the Parent List.'
     * ```
     */
    public function testValidateCrossesMulti18() {
        $result = $this->crossModel->parseCrossList([
            10 => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
            20 => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            3 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated female and male parents found in the Parent List.'
            ],
            20 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated female and male parents found in the Parent List.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female-and-male_1|female-and-male_2` cross (with occurrence code)
     * whose parents have multiple copies of themselves in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Repeated female and male parents found in the Parent List.'
     * ```
     */
    public function testValidateCrossesInvalid18() {
        $result = $this->crossModel->parseCrossList(['OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            3 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated female and male parents found in the Parent List.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that duplicate pairings (with occurrence code)
     * ```
     *   female_1 | male_1
     *   female_1 | male_1
     * ```
     * whose parents have multiple copies of themselves in the parent source are both tagged
     * ```
     *   status='invalid'
     *   message='Repeated female and male parents found in the Parent List.'
     * ```
     */
    public function testValidateCrossesMulti19() {
        $result = $this->crossModel->parseCrossList([
            10 => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
            20 => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            3 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated female and male parents found in the Parent List.'
            ],
            20 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated female and male parents found in the Parent List.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female-and-male|female-and-male-1` cross (with occurrence code) whose male parent has multiple
     * copies of itself in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Repeated male parent found in the Parent List.'
     * ```
     */
    public function testValidateCrossesInvalid19() {
        $result = $this->crossModel->parseCrossList(['OCCURRENCE_CODE2:ENTRY_CODE1|OCCURRENCE_CODE1:ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            2 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'OCCURRENCE_CODE2:ENTRY_CODE1',
                'male' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE2',
                'maleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'crossInput' => 'OCCURRENCE_CODE2:ENTRY_CODE1|OCCURRENCE_CODE1:ENTRY_CODE1',
                'crossKey' => 'OCCURRENCE_CODE2:ENTRY_CODE1,OCCURRENCE_CODE1:ENTRY_CODE1',
                'status' => 'invalid',
                'remarks' => 'Repeated male parent found in the Parent List.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (no occurrence code) whose female parent has multiple
     * copies of itself in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
     * ```
     */
    public function testValidateCrossesInvalid20() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (no occurrence code) whose male parent has multiple
     * copies of itself in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
     * ```
     */
    public function testValidateCrossesInvalid21() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (no occurrence code) whose female and male parents have multiple
     * copies of itself in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
     * ```
     */
    public function testValidateCrossesInvalid22() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            3 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (without occurrence code) whose female parent has multiple
     * entry codes but unique occurrence codes in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
     * ```
     */
    public function testValidateCrossesInvalid23() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (without occurrence code) whose male parent has multiple
     * entry codes but unique occurrence codes in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
     * ```
     */
    public function testValidateCrossesInvalid24() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female_1|male_1` cross (without occurrence code) whose parents have multiple
     * entry codes but unique occurrence codes in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
     * ```
     */
    public function testValidateCrossesInvalid25() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2'
            ],
            3 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ],
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female-and-male-1|female-and-male-2` cross (without occurrence code) whose parents have multiple
     * entry codes but unique occurrence codes in the parent source is tagged
     * ```
     *   status='invalid'
     *   message='Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
     * ```
     */
    public function testValidateCrossesInvalid26() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ],
            2 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 102,
                'occurrenceCode' => 'OCC_CODE2'
            ],
            3 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ],
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENT_CODE1',
                'status' => 'invalid',
                'remarks' => 'Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a cross `female_1 | male_2` (with occurrence) having parents
     * with the same entry code but different occurrence code is tagged valid
     */
    public function testValidateCrossesValid6() {
        $result = $this->crossModel->parseCrossList(['OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENTRY_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCC_CODE1:ENTRY_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a cross `female-1 | male-1` (without occurrence) having parents
     * with the same entry code but different occurrence code is tagged valid
     */
    public function testValidateCrossesValid7() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'male',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that input
     * ```
     *   OCC00000006:10|OCC00000012:1
     *   OCC00000006:10|OCC00000006:1
     * ```
     * are tagged
     * ```
     *   status='valid'
     *   message=''
     * ```
     */
    public function testValidateCrossesMulti20() {
        $result = $this->crossModel->parseCrossList([
            10 => 'OCC00000006:10|OCC00000012:1',
            20 => 'OCC00000006:10|OCC00000006:1'
        ]);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => '10',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCC00000006'
            ],
            1 => [
                'parentRole' => 'female-and-male',
                'entryCode' => '1',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCC00000006'
            ],
            2 => [
                'parentRole' => 'male',
                'entryCode' => '1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC00000012'
            ],
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            10 => [
                'female' => 'OCC00000006:10',
                'male' => 'OCC00000012:1',
                'femaleEntryCode' => '10',
                'maleEntryCode' => '1',
                'femaleOccurrenceCode' => 'OCC00000006',
                'maleOccurrenceCode' => 'OCC00000012',
                'crossInput' => 'OCC00000006:10|OCC00000012:1',
                'crossKey' => 'OCC00000006:10,OCC00000012:1',
                'status' => 'valid',
                'remarks' => ''
            ],
            20 => [
                'female' => 'OCC00000006:10',
                'male' => 'OCC00000006:1',
                'femaleEntryCode' => '10',
                'maleEntryCode' => '1',
                'femaleOccurrenceCode' => 'OCC00000006',
                'maleOccurrenceCode' => 'OCC00000006',
                'crossInput' => 'OCC00000006:10|OCC00000006:1',
                'crossKey' => 'OCC00000006:10,OCC00000006:1',
                'status' => 'valid',
                'remarks' => ''
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female|female` cross (without occurrence code) is tagged
     * ```
     *   status='valid'
     *   message='Same parent info. Please use SELF feature for selfing.'
     * ```
     * when selfing is explicitly allowed
     */
    public function testValidateCrossesSelf1() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE1',
                'status' => 'valid',
                'remarks' => 'Same parent info. Please use SELF feature for selfing.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses, isSelfingValid: true);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `male|male` cross (without occurrence code) is tagged
     * ```
     *   status='valid'
     *   message='Same parent info. Please use SELF feature for selfing.'
     * ```
     * when selfing is explicitly allowed
     */
    public function testValidateCrossesSelf2() {
        $result = $this->crossModel->parseCrossList(['ENT_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE1',
                'crossKey' => 'ENT_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => 'Same parent info. Please use SELF feature for selfing.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses, isSelfingValid: true);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female-and-male|female-and-male` cross (without occurrence code) is tagged
     * ```
     *   status='valid'
     *   message='Same parent info. Please use SELF feature for selfing.'
     * ```
     * when selfing is explicitly allowed
     */
    public function testValidateCrossesSelf3() {
        $result = $this->crossModel->parseCrossList(['ENT_CODE1|ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCC_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENT_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENT_CODE1|ENT_CODE1',
                'crossKey' => 'ENT_CODE1,ENT_CODE1',
                'status' => 'valid',
                'remarks' => 'Same parent info. Please use SELF feature for selfing.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses, isSelfingValid: true);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female-and-male|female-and-male` cross (with occurrence code) is tagged
     * ```
     *   status='valid'
     *   message='Same parent info. Please use SELF feature for selfing.'
     * ```
     * when selfing is explicitly allowed
     */
    public function testValidateCrossesSelf4() {
        $result = $this->crossModel->parseCrossList(['OCCURRENCE_CODE1:ENT_CODE1|OCCURRENCE_CODE1:ENT_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female-and-male',
                'entryCode' => 'ENT_CODE1',
                'entryDbId' => 101,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'OCCURRENCE_CODE1:ENT_CODE1',
                'male' => 'OCCURRENCE_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENT_CODE1|OCCURRENCE_CODE1:ENT_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENT_CODE1,OCCURRENCE_CODE1:ENT_CODE1',
                'status' => 'valid',
                'remarks' => 'Same parent info. Please use SELF feature for selfing.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses, isSelfingValid: true);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female|female` cross (without occurrence code) is tagged
     * ```
     *   status='invalid'
     *   message='Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
     * ```
     * when selfing is explicitly allowed, and when female source has same entry codes but different occurrence
     */
    public function testValidateCrossesSelf5() {
        $result = $this->crossModel->parseCrossList(['ENTRY_CODE1|ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ],
            1 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 12,
                'occurrenceCode' => 'OCCURRENCE_CODE2'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [];

        $expected = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'crossKey' => 'ENTRY_CODE1,ENTRY_CODE1',
                'status' => 'invalid',
                'remarks' => 'Duplicate entry codes found in the Parent List. Please specify the occurrence code.'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses, isSelfingValid: true);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `female|female` cross (with occurrence code) is tagged
     * ```
     *   status='invalid'
     *   message='Already exists in the Cross List!'
     * ```
     * when selfing is explicitly allowed.
     */
    public function testValidateCrossesSelf6() {
        $result = $this->crossModel->parseCrossList(['OCCURRENCE_CODE1:ENTRY_CODE1|OCCURRENCE_CODE1:ENTRY_CODE1']);
        $crossList = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        $parentList = [
            0 => [
                'parentRole' => 'female',
                'entryCode' => 'ENTRY_CODE1',
                'entryDbId' => 11,
                'occurrenceCode' => 'OCCURRENCE_CODE1'
            ]
        ];
        $parentData = $this->parentModel->getMetadata($parentList);

        $existingCrosses = [
            0 => [
                'crossDbId' => 10,
                'crossName' => 'IR XXX|IR XXX',
                'crossMethod' => 'selfing',
                'parentDbId' => '11'
            ]
        ];

        $expected = [
            0 => [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCCURRENCE_CODE1:ENTRY_CODE1',
                'crossKey' => 'OCCURRENCE_CODE1:ENTRY_CODE1,OCCURRENCE_CODE1:ENTRY_CODE1',
                'status' => 'invalid',
                'remarks' => 'Already exists in the Cross List!'
            ]
        ];

        $actual = $this->crossModel->validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses, isSelfingValid: true);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `OCC_CODE:ENT_CODE` pair is generated when occurrence code and entry code are specified
     */
    public function testMakePairComplete() {
        $female = ' OCCURRENCE_CODE1 : ENTRY_CODE1 ';
        $male = ' OCC_CODE1 : ENT_CODE1 ';

        $expected = 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1';

        $actual = $this->crossModel->makePair($female, $male);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that a `ENT_CODE` pair is generated when only entry code is specified
     */
    public function testMakePairPartial() {
        $female = ' ENTRY_CODE1 ';
        $male = ' ENT_CODE1 ';

        $expected = 'ENTRY_CODE1|ENT_CODE1';

        $actual = $this->crossModel->makePair($female, $male);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that an empty string is returned when female and male parents are identical
     */
    public function testMakePairIdentical() {
        $female = $male = 'OCCURRENCE_CODE1:ENTRY_CODE1';

        $expected = '';

        $actual = $this->crossModel->makePair($female, $male);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that records data acceptable to POST /crosses is returned
     */
    public function testToRecords1()
    {
        $entryListDbId = 200;
        $crossData = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'crossName' => 'IR 1XXXX|IR 1YYYY',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'experimentDbId' => '50',
                'occurrenceDbId' => 100
            ],
            1 => [
                'female' => 'ENTRY_CODE2',
                'male' => 'ENT_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE2',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE2',
                'maleOccurrenceCode' => 'OCC_CODE2',
                'crossName' => 'IR 2XXXX|IR 2YYYY',
                'crossInput' => 'OCCURRENCE_CODE2:ENTRY_CODE2|OCC_CODE2:ENT_CODE2',
                'experimentDbId' => '60',
                'occurrenceDbId' => 200
            ]
        ];

        $expected = [
            'records' => [
                0 => [
                    'crossName' => 'IR 1XXXX|IR 1YYYY',
                    'crossMethod' => '',
                    'entryListDbId' => '200',
                    'experimentDbId' => '50',
                    'occurrenceDbId' => '100'
                ],
                1 => [
                    'crossName' => 'IR 2XXXX|IR 2YYYY',
                    'crossMethod' => '',
                    'entryListDbId' => '200',
                    'experimentDbId' => '60',
                    'occurrenceDbId' => '200'
                ]
            ]
        ];

        $actual = $this->crossModel->toRecords($entryListDbId, $crossData);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that NON-SELFING records (for POST /crosses) is returned
     * when only entry code is supplied but parents are coming from different occurrence
     */
    public function testToRecords2()
    {
        $entryListDbId = 200;
        $crossData = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'crossName' => 'IR 1XXXX|IR 1YYYY',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'experimentDbId' => '50',
                'occurrenceDbId' => '100'
            ]
        ];

        $expected = [
            'records' => [
                0 => [
                    'crossName' => 'IR 1XXXX|IR 1YYYY',
                    'crossMethod' => '',
                    'entryListDbId' => '200',
                    'experimentDbId' => '50',
                    'occurrenceDbId' => '100'
                ]
            ]
        ];

        $actual = $this->crossModel->toRecords($entryListDbId, $crossData);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that selfing records data acceptable to POST /crosses is returned
     */
    public function testToRecordsSelfing()
    {
        $entryListDbId = 200;
        $crossData = [
            0 => [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENTRY_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENTRY_CODE1',
                'crossName' => 'IR 1XXXX|IR 1XXXX',
                'crossInput' => 'ENTRY_CODE1|ENTRY_CODE1',
                'experimentDbId' => '50',
                'occurrenceDbId' => '100'
            ]
        ];

        $expected = [
            'records' => [
                0 => [
                    'crossName' => 'IR 1XXXX|IR 1XXXX',
                    'crossMethod' => 'selfing',
                    'entryListDbId' => '200',
                    'isMethodAutofilled' => true,
                    'experimentDbId' => '50',
                    'occurrenceDbId' => '100'
                ]
            ]
        ];

        $actual = $this->crossModel->toRecords($entryListDbId, $crossData);

        $this->assertEquals($expected, $actual);
    }
}