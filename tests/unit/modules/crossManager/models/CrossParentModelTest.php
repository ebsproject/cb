<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\crossManager\models;

use Yii;

class CrossParentModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $crossParentModel;
    protected $parentModel;
    
    protected function _before()
    {
        $this->crossParentModel = Yii::$container->get('crossManagerCrossParentModel');
        $this->parentModel = Yii::$container->get('crossManagerParentModel');
    }

    protected function _after()
    {
    }

    /**
     * GIVEN
     * . cross with different entry code, different occurrence code
     * . parents from different experiment, distinct entry codes, distinct occurrence codes
     *
     * Verify that records data acceptable to POST /cross-parents is returned
     */
    public function testToRecordsValid1()
    {
        $crossData = [
            0 =>  [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossName' => 'IR XXX|IR YYY',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossDbId' => 10000,
            ],
        ];
        $parentsData = [
            0 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 1,
                'entryCode' => 'ENTRY_CODE1',
                'seedDbId' => 5,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
            1 =>  [
                'germplasmDbId' => 20,
                'experimentDbId' => 200,
                'occurrenceDbId' => 2000,
                'occurrenceCode' => 'OCC_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENT_CODE1',
                'seedDbId' => 50,
                'plotDbId' => 20,
                'parentRole' => 'male'
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentsData);

        $expected = [
            'records' => [
                0 =>  [
                    'crossDbId' => '10000',
                    'orderNumber' => '1',
                    'parentRole' => 'female',
                    'germplasmDbId' => '10',
                    'experimentDbId' => '100',
                    'entryDbId' => '1',
                    'occurrenceDbId' => '1000',
                    'seedDbId' => '5',
                ],
                1 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '2',
                    'parentRole' => 'male',
                    'germplasmDbId' => '20',
                    'experimentDbId' => '200',
                    'entryDbId' => '2',
                    'occurrenceDbId' => '2000',
                    'seedDbId' => '50',
                    'plotDbId' => '20'
                ],
            ]
        ];

        $actual = $this->crossParentModel->toRecords($crossData, $metadata);

        $this->assertEquals($expected, $actual);
    }

    /**
     * GIVEN
     * . cross with different entry code, different occurrence code
     * . parents from same experiment, distinct entry codes, distinct occurrence codes
     *
     * Verify that records data acceptable to POST /cross-parents is returned
     */
    public function testToRecordsValid2()
    {
        $crossData = [
            0 =>  [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossName' => 'IR XXX|IR YYY',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossDbId' => 10000,
            ],
        ];
        $parentsData = [
            0 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 1,
                'entryCode' => 'ENTRY_CODE1',
                'seedDbId' => 5,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
            1 =>  [
                'germplasmDbId' => 20,
                'experimentDbId' => 100,
                'occurrenceDbId' => 2000,
                'occurrenceCode' => 'OCC_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENT_CODE1',
                'seedDbId' => 50,
                'plotDbId' => 20,
                'parentRole' => 'male'
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentsData);

        $expected = [
            'records' => [
                0 =>  [
                    'crossDbId' => '10000',
                    'orderNumber' => '1',
                    'parentRole' => 'female',
                    'germplasmDbId' => '10',
                    'experimentDbId' => '100',
                    'entryDbId' => '1',
                    'occurrenceDbId' => '1000',
                    'seedDbId' => '5',
                ],
                1 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '2',
                    'parentRole' => 'male',
                    'germplasmDbId' => '20',
                    'experimentDbId' => '100',
                    'entryDbId' => '2',
                    'occurrenceDbId' => '2000',
                    'seedDbId' => '50',
                    'plotDbId' => '20'
                ],
            ]
        ];

        $actual = $this->crossParentModel->toRecords($crossData, $metadata);

        $this->assertEquals($expected, $actual);
    }

    /**
     * GIVEN
     * . cross with different entry code, different occurrence codes
     * . 2 female parents with same entry code, different occurrence code
     *
     * Verify that records data acceptable to POST /cross-parents is returned
     */
    public function testToRecordsValid3() {
        $crossData = [
            0 =>  [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossName' => 'IR XXX|IR YYY',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossDbId' => 10000,
            ],
        ];
        $parentsData = [
            0 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,  # different occurrence
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 1,
                'entryCode' => 'ENTRY_CODE1',  # same entry code
                'seedDbId' => 5,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
            1 => [
                'germplasmDbId' => 20,
                'experimentDbId' => 200,
                'occurrenceDbId' => 2000,
                'occurrenceCode' => 'OCC_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENT_CODE1',
                'seedDbId' => 50,
                'plotDbId' => null,
                'parentRole' => 'male'
            ],
            2 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 3000,  # different occurrence
                'occurrenceCode' => 'OCCURRENCE_CODE2',
                'entryDbId' => 1,
                'entryCode' => 'ENTRY_CODE1',  # same entry code
                'seedDbId' => 5,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
        ];
        $metadata = $this->parentModel->getMetadata($parentsData);

        $expected = [
            'records' => [
                0 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '1',
                    'parentRole' => 'female',
                    'germplasmDbId' => '10',
                    'experimentDbId' => '100',
                    'entryDbId' => '1',
                    'occurrenceDbId' => '1000',
                    'seedDbId' => '5'
                ],
                1 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '2',
                    'parentRole' => 'male',
                    'germplasmDbId' => '20',
                    'experimentDbId' => '200',
                    'entryDbId' => '2',
                    'occurrenceDbId' => '2000',
                    'seedDbId' => '50',
                ]
            ]
        ];
        $actual = $this->crossParentModel->toRecords($crossData, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * GIVEN
     * . cross with same entry code, different occurrence code
     * . parents with same entry codes, distinct occurrence codes
     *
     * Verify that records data acceptable to POST /cross-parents is returned
     */
    public function testToRecordsValid4()
    {
        $crossData = [
            0 =>  [
                'female' => 'OCCURRENCE_CODE1:ENT_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENT_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossName' => 'IR XXX|IR YYY',
                'crossInput' => 'OCCURRENCE_CODE1:ENT_CODE1|OCC_CODE1:ENT_CODE1',
                'crossDbId' => 10000,
            ],
        ];
        $parentsData = [
            0 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 1,
                'entryCode' => 'ENT_CODE1',
                'seedDbId' => 5,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
            1 =>  [
                'germplasmDbId' => 20,
                'experimentDbId' => 200,
                'occurrenceDbId' => 2000,
                'occurrenceCode' => 'OCC_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENT_CODE1',
                'seedDbId' => 50,
                'plotDbId' => 20,
                'parentRole' => 'male'
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentsData);

        $expected = [
            'records' => [
                0 =>  [
                    'crossDbId' => '10000',
                    'orderNumber' => '1',
                    'parentRole' => 'female',
                    'germplasmDbId' => '10',
                    'experimentDbId' => '100',
                    'entryDbId' => '1',
                    'occurrenceDbId' => '1000',
                    'seedDbId' => '5',
                ],
                1 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '2',
                    'parentRole' => 'male',
                    'germplasmDbId' => '20',
                    'experimentDbId' => '200',
                    'entryDbId' => '2',
                    'occurrenceDbId' => '2000',
                    'seedDbId' => '50',
                    'plotDbId' => '20'
                ],
            ]
        ];

        $actual = $this->crossParentModel->toRecords($crossData, $metadata);

        $this->assertEquals($expected, $actual);
    }

    /**
     * GIVEN
     * . cross with different entry code, different occurrence codes
     * . parents with distinct entry codes, same occurrence code
     *
     * Verify that records data acceptable to POST /cross-parents is returned
     */
    public function testToRecordsValid5() {
        $crossData = [
            0 =>  [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCCURRENCE_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'crossName' => 'IR XXX|IR YYY',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCCURRENCE_CODE1:ENT_CODE1',
                'crossDbId' => 10000,
            ],
        ];
        $parentsData = [
            0 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,  # same occurrence
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 1,
                'entryCode' => 'ENTRY_CODE1',
                'seedDbId' => 5,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
            1 => [
                'germplasmDbId' => 20,
                'experimentDbId' => 200,
                'occurrenceDbId' => 1000,  # same occurrence
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENT_CODE1',
                'seedDbId' => 50,
                'plotDbId' => null,
                'parentRole' => 'male'
            ],
            2 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,  # same occurrence
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENTRY_CODE2',
                'seedDbId' => 500,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
        ];
        $metadata = $this->parentModel->getMetadata($parentsData);

        $expected = [
            'records' => [
                0 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '1',
                    'parentRole' => 'female',
                    'germplasmDbId' => '10',
                    'experimentDbId' => '100',
                    'entryDbId' => '1',
                    'occurrenceDbId' => '1000',
                    'seedDbId' => '5'
                ],
                1 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '2',
                    'parentRole' => 'male',
                    'germplasmDbId' => '20',
                    'experimentDbId' => '200',
                    'entryDbId' => '2',
                    'occurrenceDbId' => '1000',
                    'seedDbId' => '50',
                ]
            ]
        ];
        $actual = $this->crossParentModel->toRecords($crossData, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * GIVEN
     * . cross with different entry code, no occurrence code
     * . parents with distinct entry codes, same occurrence code
     *
     * Verify that records data acceptable to POST /cross-parents is returned
     */
    public function testToRecordsValid6() {
        $crossData = [
            0 =>  [
                'female' => 'ENTRY_CODE1',
                'male' => 'ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossName' => 'IR XXX|IR YYY',
                'crossInput' => 'ENTRY_CODE1|ENT_CODE1',
                'crossDbId' => 10000,
            ],
        ];
        $parentsData = [
            0 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,  # same occurrence
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 1,
                'entryCode' => 'ENTRY_CODE1',
                'seedDbId' => 5,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
            1 => [
                'germplasmDbId' => 20,
                'experimentDbId' => 200,
                'occurrenceDbId' => 1000,  # same occurrence
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENT_CODE1',
                'seedDbId' => 50,
                'plotDbId' => null,
                'parentRole' => 'male'
            ],
            2 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,  # same occurrence
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENTRY_CODE2',
                'seedDbId' => 500,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
        ];
        $metadata = $this->parentModel->getMetadata($parentsData);

        $expected = [
            'records' => [
                0 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '1',
                    'parentRole' => 'female',
                    'germplasmDbId' => '10',
                    'experimentDbId' => '100',
                    'entryDbId' => '1',
                    'occurrenceDbId' => '1000',
                    'seedDbId' => '5'
                ],
                1 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '2',
                    'parentRole' => 'male',
                    'germplasmDbId' => '20',
                    'experimentDbId' => '200',
                    'entryDbId' => '2',
                    'occurrenceDbId' => '1000',
                    'seedDbId' => '50',
                ]
            ]
        ];
        $actual = $this->crossParentModel->toRecords($crossData, $metadata);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that NON-SELFING records (for POST /cross-parents) is returned
     * when only entry code is supplied but parents are coming from different occurrence
     */
    public function testToRecordsValid7() {
        $crossData = [
            2 => [
                'female' => 'ENTRY_CODE',
                'male' => 'ENTRY_CODE',
                'femaleEntryCode' => 'ENTRY_CODE',
                'maleEntryCode' => 'ENTRY_CODE',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossName' => 'IR XXX|IR YYY',
                'crossInput' => 'ENTRY_CODE|ENTRY_CODE',
                'status' => 'valid',
                'remarks' => '',
                'crossDbId' => 1000,
            ],
        ];
        $parentsData = [
            0 => [
                'germplasmDbId' => 10,
                'designation' => 'IR XXX',
                'entryListDbId' => 20,
                'experimentDbId' => 30,
                'occurrenceDbId' => 40,
                'occurrenceCode' => 'OCCURRENCE_CODE',
                'entryDbId' => 50,
                'entryCode' => 'ENTRY_CODE',
                'seedDbId' => 60,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
            1 => [
                'germplasmDbId' => 11,
                'designation' => 'IR YYY',
                'entryListDbId' => 20,
                'experimentDbId' => 31,
                'occurrenceDbId' => 41,
                'occurrenceCode' => 'OCC_CODE',
                'entryDbId' => 51,
                'entryCode' => 'ENTRY_CODE',
                'seedDbId' => 61,
                'plotDbId' => null,
                'parentRole' => 'male'
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentsData);

        $expected = [
            'records' => [
                0 => [
                    'crossDbId' => '1000',
                    'orderNumber' => '1',
                    'parentRole' => 'female',
                    'germplasmDbId' => '10',
                    'experimentDbId' => '30',
                    'entryDbId' => '50',
                    'occurrenceDbId' => '40',
                    'seedDbId' => '60'
                ],
                1 => [
                    'crossDbId' => '1000',
                    'orderNumber' => '2',
                    'parentRole' => 'male',
                    'germplasmDbId' => '11',
                    'experimentDbId' => '31',
                    'entryDbId' => '51',
                    'occurrenceDbId' => '41',
                    'seedDbId' => '61',
                ]
            ]
        ];

        $actual = $this->crossParentModel->toRecords($crossData, $metadata);

        $this->assertEquals($expected, $actual);
    }

    /**
     * GIVEN
     * . 2 crosses with distinct entry codes and occurrence codes
     * . parents from different experiments
     *
     * Verify that records data acceptable to POST /cross-parents is returned
     */
    public function testToRecordsMulti()
    {
        $crossData = [
            10 =>  [
                'female' => 'OCCURRENCE_CODE1:ENTRY_CODE1',
                'male' => 'OCC_CODE1:ENT_CODE1',
                'femaleEntryCode' => 'ENTRY_CODE1',
                'maleEntryCode' => 'ENT_CODE1',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE1',
                'maleOccurrenceCode' => 'OCC_CODE1',
                'crossName' => 'IR XXX|IR YYY',
                'crossInput' => 'OCCURRENCE_CODE1:ENTRY_CODE1|OCC_CODE1:ENT_CODE1',
                'crossDbId' => 10000,
            ],
            20 =>  [
                'female' => 'OCCURRENCE_CODE2:ENTRY_CODE2',
                'male' => 'OCC_CODE2:ENT_CODE2',
                'femaleEntryCode' => 'ENTRY_CODE2',
                'maleEntryCode' => 'ENT_CODE2',
                'femaleOccurrenceCode' => 'OCCURRENCE_CODE2',
                'maleOccurrenceCode' => 'OCC_CODE2',
                'crossName' => 'IR AAA|IR BBB',
                'crossInput' => 'OCCURRENCE_CODE2:ENTRY_CODE2|OCC_CODE2:ENT_CODE2',
                'crossDbId' => 20000,
            ],
        ];
        $parentsData = [
            0 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 1,
                'entryCode' => 'ENTRY_CODE1',
                'seedDbId' => 5,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
            1 =>  [
                'germplasmDbId' => 20,
                'experimentDbId' => 200,
                'occurrenceDbId' => 2000,
                'occurrenceCode' => 'OCC_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENT_CODE1',
                'seedDbId' => 50,
                'plotDbId' => 20,
                'parentRole' => 'male'
            ],
            10 =>  [
                'germplasmDbId' => 30,
                'experimentDbId' => 300,
                'occurrenceDbId' => 3000,
                'occurrenceCode' => 'OCCURRENCE_CODE2',
                'entryDbId' => 3,
                'entryCode' => 'ENTRY_CODE2',
                'seedDbId' => 15,
                'plotDbId' => null,
                'parentRole' => 'female'
            ],
            20 =>  [
                'germplasmDbId' => 60,
                'experimentDbId' => 600,
                'occurrenceDbId' => 6000,
                'occurrenceCode' => 'OCC_CODE2',
                'entryDbId' => 6,
                'entryCode' => 'ENT_CODE2',
                'seedDbId' => 150,
                'plotDbId' => null,
                'parentRole' => 'male'
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentsData);

        $expected = [
            'records' => [
                0 =>  [
                    'crossDbId' => '10000',
                    'orderNumber' => '1',
                    'parentRole' => 'female',
                    'germplasmDbId' => '10',
                    'experimentDbId' => '100',
                    'entryDbId' => '1',
                    'occurrenceDbId' => '1000',
                    'seedDbId' => '5',
                ],
                1 => [
                    'crossDbId' => '10000',
                    'orderNumber' => '2',
                    'parentRole' => 'male',
                    'germplasmDbId' => '20',
                    'experimentDbId' => '200',
                    'entryDbId' => '2',
                    'occurrenceDbId' => '2000',
                    'seedDbId' => '50',
                    'plotDbId' => '20'
                ],
                2 => [
                    'crossDbId' => '20000',
                    'orderNumber' => '1',
                    'parentRole' => 'female',
                    'germplasmDbId' => '30',
                    'experimentDbId' => '300',
                    'entryDbId' => '3',
                    'occurrenceDbId' => '3000',
                    'seedDbId' => '15'
                ],
                3 => [
                    'crossDbId' => '20000',
                    'orderNumber' => '2',
                    'parentRole' => 'male',
                    'germplasmDbId' => '60',
                    'experimentDbId' => '600',
                    'entryDbId' => '6',
                    'occurrenceDbId' => '6000',
                    'seedDbId' => '150'
                ],

            ]
        ];

        $actual = $this->crossParentModel->toRecords($crossData, $metadata);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that selfing records data acceptable to POST /cross-parents is returned
     */
    public function testToRecordsSelfing() {
        $crossData = [
            2 => [
                'female' => 'ENTRY_CODE',
                'male' => 'ENTRY_CODE',
                'femaleEntryCode' => 'ENTRY_CODE',
                'maleEntryCode' => 'ENTRY_CODE',
                'femaleOccurrenceCode' => '',
                'maleOccurrenceCode' => '',
                'crossName' => 'IR XXX|IR XXX',
                'crossInput' => 'ENTRY_CODE|ENTRY_CODE',
                'status' => 'valid',
                'remarks' => 'Same parent info. Please use SELF feature for selfing.',
                'crossDbId' => 1000,
            ],
        ];
        $parentsData = [
            0 => [
                'germplasmDbId' => 10,
                'designation' => 'IR XXX',
                'entryListDbId' => 20,
                'experimentDbId' => 30,
                'occurrenceDbId' => 40,
                'occurrenceCode' => 'OCCURRENCE_CODE',
                'entryDbId' => 50,
                'entryCode' => 'ENTRY_CODE',
                'seedDbId' => 60,
                'plotDbId' => null,
                'parentRole' => 'female'
            ]
        ];
        $metadata = $this->parentModel->getMetadata($parentsData);

        $expected = [
            'records' => [
                0 => [
                    'crossDbId' => '1000',
                    'orderNumber' => '1',
                    'parentRole' => 'female-and-male',
                    'germplasmDbId' => '10',
                    'experimentDbId' => '30',
                    'entryDbId' => '50',
                    'occurrenceDbId' => '40',
                    'seedDbId' => '60',
                ]
            ]
        ];

        $actual = $this->crossParentModel->toRecords($crossData, $metadata);

        $this->assertEquals($expected, $actual);
    }
}