<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\crossManager\models;

use Yii;
use function Webmozart\Assert\Tests\StaticAnalysis\length;

class ParentModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $parentModel;
    
    protected function _before()
    {
        $this->parentModel = Yii::$container->get('crossManagerParentModel');
    }

    protected function _after()
    {
    }

    /**
     * Verify that correct metadata is returned given an "experiment.parent" data
     */
    public function testGetMetadata()
    {
        $parentsData = [
            0 =>  [
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 1,
                'entryCode' => 'ENTRY_CODE1',
                'parentRole' => 'female'
            ],
            1 =>  [
                'germplasmDbId' => 20,
                'experimentDbId' => 200,
                'occurrenceDbId' => 2000,
                'occurrenceCode' => 'OCC_CODE1',
                'entryDbId' => 2,
                'entryCode' => 'ENT_CODE1',
                'parentRole' => 'male'
            ],
            2 =>  [  // same entry code, different occurrence code as index 1
                'germplasmDbId' => 20,
                'experimentDbId' => 200,
                'occurrenceDbId' => 3000,
                'occurrenceCode' => 'OCC_CODE2',
                'entryDbId' => 2,
                'entryCode' => 'ENT_CODE1',
                'parentRole' => 'female-and-male'
            ],
            3 =>  [  // duplicate of index 0
                'germplasmDbId' => 10,
                'experimentDbId' => 100,
                'occurrenceDbId' => 1000,
                'occurrenceCode' => 'OCCURRENCE_CODE1',
                'entryDbId' => 1,
                'entryCode' => 'ENTRY_CODE1',
                'parentRole' => 'female'
            ],
        ];

        $expected = [
            'count' => [
                'ENTRY_CODE1' => 2,
                'OCCURRENCE_CODE1:ENTRY_CODE1' => 2,
                'ENT_CODE1' => 2,
                'OCC_CODE1:ENT_CODE1' => 1,
                'OCC_CODE2:ENT_CODE1' => 1
            ],
            'femaleCount' => [
                'ENTRY_CODE1' => 2,
                'OCCURRENCE_CODE1:ENTRY_CODE1' => 2,
                'ENT_CODE1' => 1,
                'OCC_CODE2:ENT_CODE1' => 1
            ],
            'maleCount' => [
                'ENT_CODE1' => 2,
                'OCC_CODE1:ENT_CODE1' => 1,
                'OCC_CODE2:ENT_CODE1' => 1
            ],
            'parents' => [
                'OCCURRENCE_CODE1:ENTRY_CODE1' => [
                    'germplasmDbId' => 10,
                    'experimentDbId' => 100,
                    'occurrenceDbId' => 1000,
                    'occurrenceCode' => 'OCCURRENCE_CODE1',
                    'entryDbId' => 1,
                    'entryCode' => 'ENTRY_CODE1',
                    'parentRole' => 'female'
                ],
                'OCC_CODE1:ENT_CODE1' => [
                    'germplasmDbId' => 20,
                    'experimentDbId' => 200,
                    'occurrenceDbId' => 2000,
                    'occurrenceCode' => 'OCC_CODE1',
                    'entryDbId' => 2,
                    'entryCode' => 'ENT_CODE1',
                    'parentRole' => 'male'
                ],
                'OCC_CODE2:ENT_CODE1' => [
                    'germplasmDbId' => 20,
                    'experimentDbId' => 200,
                    'occurrenceDbId' => 3000,
                    'occurrenceCode' => 'OCC_CODE2',
                    'entryDbId' => 2,
                    'entryCode' => 'ENT_CODE1',
                    'parentRole' => 'female-and-male'
                ]
            ],
            'femaleParents' => [
                'OCCURRENCE_CODE1:ENTRY_CODE1' => [
                    'germplasmDbId' => 10,
                    'experimentDbId' => 100,
                    'occurrenceDbId' => 1000,
                    'occurrenceCode' => 'OCCURRENCE_CODE1',
                    'entryDbId' => 1,
                    'entryCode' => 'ENTRY_CODE1',
                    'parentRole' => 'female'
                ],
                'ENT_CODE1' => [
                    'germplasmDbId' => 20,
                    'experimentDbId' => 200,
                    'occurrenceDbId' => 3000,
                    'occurrenceCode' => 'OCC_CODE2',
                    'entryDbId' => 2,
                    'entryCode' => 'ENT_CODE1',
                    'parentRole' => 'female-and-male'
                ],
                'OCC_CODE2:ENT_CODE1' => [
                    'germplasmDbId' => 20,
                    'experimentDbId' => 200,
                    'occurrenceDbId' => 3000,
                    'occurrenceCode' => 'OCC_CODE2',
                    'entryDbId' => 2,
                    'entryCode' => 'ENT_CODE1',
                    'parentRole' => 'female-and-male'
                ]
            ],
            'maleParents' => [
                'OCC_CODE1:ENT_CODE1' => [
                    'germplasmDbId' => 20,
                    'experimentDbId' => 200,
                    'occurrenceDbId' => 2000,
                    'occurrenceCode' => 'OCC_CODE1',
                    'entryDbId' => 2,
                    'entryCode' => 'ENT_CODE1',
                    'parentRole' => 'male'
                ],
                'OCC_CODE2:ENT_CODE1' => [
                    'germplasmDbId' => 20,
                    'experimentDbId' => 200,
                    'occurrenceDbId' => 3000,
                    'occurrenceCode' => 'OCC_CODE2',
                    'entryDbId' => 2,
                    'entryCode' => 'ENT_CODE1',
                    'parentRole' => 'female-and-male'
                ]
            ]
        ];

        $actual = $this->parentModel->getMetadata($parentsData);

        $this->assertEquals($expected, $actual);
        $this->assertEquals(sizeof($parentsData)*2, array_sum($actual['count']));
    }
}