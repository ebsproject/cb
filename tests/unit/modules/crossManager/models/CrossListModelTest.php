<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\crossManager\models;

use Yii;
use \yii\web\CookieCollection;
use app\dataproviders\ArrayDataProvider;

class CrossListModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $crossListModel;

    /**
     * Executed before each test
     */
    protected function _before()
    {
        $this->crossListModel = Yii::$container->get('crossListModel');
    }

    /**
     * Executed after each test
     */
    protected function _after()
    {
    }

    // tests
    /**
     * search: Test 1
     * Test data provider search with no data
     */
    public function testSearchWithNoData()
    {
        //ARRANGE
        $params['entryListType'] = 'equals cross list';

        // Mock EntryList searchAll method
        $this->crossListModel->entryList->setReturnValue('searchAll', [
            'data' => [],
            'totalCount' => 0,
            'totalPages' => 0,
        ]);

        // ACT
        $actual = $this->crossListModel->search($params);
        $expected = new ArrayDataProvider([
            'allModels' => [],
            'key' => 'entryListDbId',
            'sort' =>  [
                'attributes' => [
                    'entryListStatus',
                    'entryListName',
                    'entryListCode',
                    'experimentDbId',
                    'experimentYear',
                    'experimentType',
                    'seasonCode',
                    'geospatialObjectName',
                    'creator',
                    'entryListDbId',
                    'programDbId',
                    'siteDbId',
                    'seasonDbId',
                    'crossCount',
                    'creationTimestamp',
                ],
                'defaultOrder' => [
                    'experimentYear' => SORT_DESC,
                    'creationTimestamp' => SORT_DESC
                ]
            ],
            'restified' => true,
            'totalCount' => 0,
            'id' => 'cm-crosslist-browser'
        ]);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * search: Test 2
     * Test data provider search with data
     */
    public function testSearchWithData()
    {
        // ARRANGE
        $data = [
            [
                'entryListDbId' => 123
            ]
        ];

        $params['entryListType'] = 'equals cross list';

        // Mock EntryList searchAll method
        $this->crossListModel->entryList->setReturnValue('searchAll', [
            'data' => $data,
            'totalCount' => 1,
            'totalPages' => 1,
        ]);

        // ACT
        $actual = $this->crossListModel->search($params);
        $expected = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'entryListDbId',
            'sort' =>  [
                'attributes' => [
                    'entryListStatus',
                    'entryListName',
                    'entryListCode',
                    'experimentDbId',
                    'experimentYear',
                    'experimentType',
                    'seasonCode',
                    'geospatialObjectName',
                    'creator',
                    'entryListDbId',
                    'programDbId',
                    'siteDbId',
                    'seasonDbId',
                    'crossCount',
                    'creationTimestamp',
                ],
                'defaultOrder' => [
                    'experimentYear' => SORT_DESC,
                    'creationTimestamp' => SORT_DESC
                ]
            ],
            'restified' => true,
            'totalCount' => 1,
            'id' => 'cm-crosslist-browser'
        ]);
        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * getFilters: Test 1
     * Verify page size from cookies
     */
    public function xtestGetFiltersPageSize(){
        // ARRANGE
        $gridName = 'cm-cross-list-grid_';
        
        // MOCK
        $getPageSize = $this->crossListModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 50);

        $expected = 'limit=50&sort=experimentYear:desc|creationTimestamp:desc';
        //ACT
        $actual = $this->crossListModel->getFilters([
            'experimentYear' => '2022'
        ]);
        //ASSERT
        $this->assertEquals($expected, $actual);

    }

    /**
     * getFilters: Test 2
     * Verify ASC column pjax sorting
     */
    public function xtestGetFiltersAscendingColumnSort(){
        $gridName = 'dynagrid-cm-cross-list-grid_';

        // MOCK
        $getPageSize = $this->crossListModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 50);

        $expected = 'limit=50&sort=experimentYear';

        //ACT
        $actual = $this->crossListModel->getFilters([
            'sort' => 'experimentYear',
            '_pjax' => '#dynagrid-cm-cross-list-grid-pjax'
        ]);
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getFilters: Test 3
     * Verify DESC column pjax sorting
     */
    public function xtestGetFiltersDescendingColumnSort(){
        $gridName = 'dynagrid-cm-cross-list-grid_';

        // MOCK
        $getPageSize = $this->crossListModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 50);

        $expected = 'limit=50&sort=experimentYear:DESC';

        // ACT
        $actual = $this->crossListModel->getFilters([
            'sort' => '-experimentYear',
            '_pjax' => '#dynagrid-cm-cross-list-grid-pjax'
        ]);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getOptions: Test 1
     * Verify status option retrieval with empty scale values
     */
    public function testGetOptions(){

        $this->crossListModel->variable->setReturnValue('searchAll', []);
        $this->crossListModel->scaleValue->setReturnValue('getVariableScaleValues', []);
        $this->crossListModel->variable->setReturnValue('getVariableScaleTags', []);

        $actual = $this->crossListModel->getOptions('TEST');
        $expected = [];

        $this->assertEquals($expected, $actual);

    }
}