<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\location\models;

use Yii;

class FielLayoutTest extends \Codeception\Test\Unit
{
    /**
    * @var \UnitTester
    */
    protected $tester;
    protected $fieldLayout;

    protected function _before()
    {
        $this->fieldLayout = Yii::$container->get('fieldLayout');
    }

    public function testGetValidFileName1()
    {
        // ARRANGE
        $name = 'This IS. AN INVALID(*)&^%FILE NAM.E';

        // expected value
        $expected = 'This IS. AN INVALID()FILE NAM.E';

        // ACT
        $actual = $this->fieldLayout->getValidFileName($name);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetValidFileName2()
    {
        // ARRANGE
        $name = 'INVALID**&&&&^^^^^^^!@@##';

        // expected value
        $expected = 'INVALID';

        // ACT
        $actual = $this->fieldLayout->getValidFileName($name);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetValidFileName3()
    {
        // ARRANGE
        $name = '~`?><FILE+=_)(';

        // expected value
        $expected = '~FILE_)(';

        // ACT
        $actual = $this->fieldLayout->getValidFileName($name);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testColumnLetter1()
    {
        // ARRANGE
        $int = 2;

        // expected value
        $expected = 'B';

        // ACT
        $actual = $this->fieldLayout->columnLetter($int);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testColumnLetter2()
    {
        // ARRANGE
        $int = 3;

        // expected value
        $expected = 'C';

        // ACT
        $actual = $this->fieldLayout->columnLetter($int);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testColumnLetter3()
    {
        // ARRANGE
        $int = 10;

        // expected value
        $expected = 'J';

        // ACT
        $actual = $this->fieldLayout->columnLetter($int);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}