<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\germplasm\models;

use Yii;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsArray;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertNull;

class UpdateModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;

    protected function _before()
    {
        $this->germplasmUpdateModel = Yii::$container->get('germplasmUpdateModel');
        $this->germplasmFileUpload = Yii::$container->get('germplasmFileUpload');
    }

    protected function _after()
    {
        \Yii::$app->session->set("gm-global-update-variables", null);
        \Yii::$app->session->set("gm-global-update-variables-required", null);
    }

    /**
     * Verify KEY_VARIABLES constant is returned
     */
    public function testGetKeyVariables() {
        $expected = 'gm-global-update-variables';
        $actual = $this->germplasmUpdateModel->getKeyVariables();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify KEY_VARIABLES_REQUIRED constant is returned
     */
    public function testGetKeyVariablesRequired() {
        $expected = 'gm-global-update-variables-required';
        $actual = $this->germplasmUpdateModel->getKeyVariablesRequired();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify KEY_VARIABLES_OPTIONAL constant is returned
     */
    public function testGetKeyVariablesOptional() {
        $expected = 'gm-global-update-variables-optional';
        $actual = $this->germplasmUpdateModel->getKeyVariablesOptional();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify EXCLUDED_VARIABLES constant is returned
     */
    public function testGetExcludeVariables() {
        $expected = ['GERMPLASM_CODE','DESIGNATION'];
        $actual = $this->germplasmUpdateModel->getExcludeVariables();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify TEMPLATE_FILE_NAME constant is returned
     */
    public function testGetTemplateFileName() {
        $expected = 'GM_UPDATE_GERMPLASM_ATTRIBUTE_TEMPLATE';
        $actual = $this->germplasmUpdateModel->getTemplateFileName();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify ACTION constant is returned
     */
    public function testGetModelAction() {
        $expected = 'update';
        $actual = $this->germplasmUpdateModel->getModelAction();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify LEVEL constant is returned
     */
    public function testGetModelLevel() {
        $expected = 'GLOBAL';
        $actual = $this->germplasmUpdateModel->getModelLevel();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Retrieve file data summary for file upload transaction record
     */
    public function testGetFileDataSummary() {
        $expected = [
            'attributes' => ['KEY'],
            'attributeCount' => 1,
            'rowCount' => 1
        ];
        
        $configValues = [
            [ 'abbrev' => 'KEY', 'in_summary' => 'true' ]
        ];
        $fileData = [
            [ 'germplasm' => ['key' => 'value'] ]
        ];
        $action = 'update';

        $actual = $this->germplasmUpdateModel->getFileDataSummary($fileData,$action,$configValues);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Retrieve file data summary for file upload transaction record
     */
    public function testGetFileDataSummaryNoHeader() {
        $expected = [
            'attributes' => [],
            'attributeCount' => 0,
            'rowCount' => 1
        ];
        
        $fileData = [
            [ 'germplasm' => ['germplasm_code' => 'value'] ]
        ];
        $action = 'update';

        $actual = $this->germplasmUpdateModel->getFileDataSummary($fileData,$action);

        $this->assertEquals($expected, $actual);
    }
    
    /**
     * Validate column headers of the uploaded file
     */
    public function testValidateUploadedFileNoMissingRequiredColumn() {
        $programId = 101;
        $headers = ['GERMPLASM_CODE'];
        $variableConfig = [
            'values' => [
                [   'abbrev' => 'GERMPLASM_CODE', 'usage' => 'requred' ]
            ]
        ];

        $variable = [ 'abbrev' => 'GERMPLASM_CODE', 'name' => 'Germplasm Code' ];
        $variables = [
            'variables' => ['GERMPLASM_CODE'],
            'requiredVariables' => ['GERMPLASM_CODE']
        ];

        $this->germplasmFileUpload->variableModel->setReturnValue('getVariableByAbbrev',$variable);
        $this->germplasmUpdateModel->germplasmFileUpload->setReturnValue('getFileUploadVariables',$variables);

        $expected = [
            'success' => true,
            'error' => ''
        ];

        $actual = $this->germplasmUpdateModel->validateUploadedFile($programId,$headers,$variableConfig);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Validate column headers of the uploaded file
     */
    public function testValidateUploadedFileHasMissingRequiredColumn() {
        $programId = 101;
        $headers = ['DESIGNATION'];
        $variableConfig = [
            'values' => [
                [   'abbrev' => 'GERMPLASM_CODE', 'usage' => 'requred' ]
            ]
        ];

        $variable = [ 'abbrev' => 'GERMPLASM_CODE', 'name' => 'Germplasm Code' ];
        $variables = [
            'variables' => ['GERMPLASM_CODE'],
            'requiredVariables' => ['GERMPLASM_CODE']
        ];

        $this->germplasmFileUpload->variableModel->setReturnValue('getVariableByAbbrev',$variable);
        $this->germplasmUpdateModel->germplasmFileUpload->setReturnValue('getFileUploadVariables',$variables);

        $expected = [
            'success' => false,
            'error' => 'Missing required column header/s GERMPLASM_CODE.'
        ];

        $actual = $this->germplasmUpdateModel->validateUploadedFile($programId,$headers,$variableConfig);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Save uploaded file
     */
    public function testSaveUploadedFileNoFileData() {
        $programId = 101;
        $fileName = 'Test File Name.csv';
        $fileContent = [];
        $origin = 'GERMPLASM_cATALOG';
        $remarks = '';
        $action = 'update';

        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    'status' => 200,
                    'body' => [
                        'result' => [
                            'data' => [
                                [
                                    'germplasmFileUploadDbId' => 123
                                ]
                            ]
                        ]
                    ]
                ];
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        $results = [
            'success' => true,
            'error' => '',
            'id' => 123
        ];

        $this->germplasmUpdateModel->germplasmFileUpload->setReturnValue('saveUploadedFile',$results);

        $expected = [
            'success' => true,
            'error' => '',
            'id' => 123
        ];

        $actual = $this->germplasmUpdateModel->saveUploadedFile($programId,$fileName,$fileContent,$origin,$remarks,$action);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Save uploaded file
     */
    public function testSaveUploadedFileNoGermplasmUploadFile() {
        $programId = 101;
        $fileName = 'Test File Name.csv';
        $fileContent = [];
        $origin = 'GERMPLASM_CATALOG';
        $remarks = '';
        $action = 'update';

        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    'status' => 500,
                    'body' => [
                        'result' => [
                            'data' => []
                        ]
                    ]
                ];
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        $results = [
            'success' => false,
            'error' => 'Error encountered creating file upload record.'
        ];

        $this->germplasmUpdateModel->germplasmFileUpload->setReturnValue('saveUploadedFile',$results);

        $expected = [
            'success' => false,
            'error' => 'Error encountered creating file upload record.'
        ];

        $actual = $this->germplasmUpdateModel->saveUploadedFile($programId,$fileName,$fileContent,$origin,$remarks,$action);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Save uploaded file
     */
    public function testSaveUploadedFileHasFileData() {
        $programId = 101;
        $fileName = 'Test File Name.csv';
        $fileContent = [
            [
                'germplasm_code' => ' GE000000000001 ',
                'designation' => 'Germplasm #1',
            ]
        ];
        $origin = 'GERMPLASM_cATALOG';
        $remarks = '';
        $action = 'update';

        $sessionRequired = [ 'GERMPLASM_CODE' => [] ];
        $sessionOptional = [ 'GERMPLASM_CODE' => [] ];
        \Yii::$app->session->set("gm-global-update-variables-required", $sessionRequired);
        \Yii::$app->session->set("gm-global-update-variables-optional", $sessionOptional);

        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    'status' => 500,
                    'body' => [
                        'result' => [
                            'data' => []
                        ]
                    ]
                ];
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        $results = [
            'success' => false,
            'error' => 'Error encountered creating file upload record.'
        ];

        $this->germplasmUpdateModel->germplasmFileUpload->setReturnValue('saveUploadedFile',$results);

        $expected = [
            'success' => false,
            'error' => 'Error encountered creating file upload record.'
        ];

        $actual = $this->germplasmUpdateModel->saveUploadedFile($programId,$fileName,$fileContent,$origin,$remarks,$action);

        $this->assertEquals($expected, $actual);
    }
}
?>