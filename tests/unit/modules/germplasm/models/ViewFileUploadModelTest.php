<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\germplasm\models;

use Yii;
use app\widgets\GermplasmInfo;
// Import data providers
use app\dataproviders\ArrayDataProvider;

class ViewFileUploadModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $viewFileUploadModel;
    
    protected function _before()
    {
        $this->viewFileUploadModel = \Yii::$container->get('viewFileUploadModel');

        $this->configMock = \Yii::$container->get('configMock');
        $this->cropMock = \Yii::$container->get('cropMock');
        $this->programMock = \Yii::$container->get('programMock');
        $this->userDashboardConfigMock = \Yii::$container->get('userDashboardConfigMock');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * getFileUploadRecord: Test 1
     */
    public function testGetFileUploadRecordWhenRetrievalIsSuccessful()
    {
        // ARRANGE
        // input
        $fileUploadDbId = 123;

        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    'status' => 200,
                    'body' => [
                        'result' => [
                            'data' => [
                                [
                                    'germplasmFileUploadDbId' => 123
                                ]
                            ]
                        ]
                    ]
                ];
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        // expected
        $expected = [
            'status' => 200,
            'body' => [
                'result' => [
                    'data' => [
                        [
                            'germplasmFileUploadDbId' => 123
                        ]
                    ]
                ]
            ]
        ];

        // ACT
        $actual = $this->viewFileUploadModel->getFileUploadRecord($fileUploadDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getFileUploadRecord: Test 2
     */
    public function testGetFileUploadRecordWhenRetrievalIsNotSuccessful()
    {
        // ARRANGE
        // input
        $fileUploadDbId = 123;

        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    'status' => 500
                ];
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        // ACT + ASSERT
        $this->assertThrows(\yii\base\ErrorException::class,
            function() use ($fileUploadDbId) {
                $actual = $this->viewFileUploadModel->getFileUploadRecord($fileUploadDbId);
            }
        );
    }

    /**
     * getColumn: Test 1
     */
    public function testGetColumn() {
        // ARRANGE
        // expected
        $germplasmColumns = [
            [
                'attribute'=>'designation',
                'label' => 'Germplasm',
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ],
                'value'=>function($data) {
                    $germplasmDbId = $data['germplasmDbId'];
                    $designation = $data['designation'];

                    $linkAttrs = [
                        'id' => 'view-germplasm-widget-'.$germplasmDbId,
                        'class' => 'blue-text text-darken-4 header-highlight',
                        'title' => \Yii::t('app', 'Click to view more information'),
                        'data-label' => $designation,
                        'data-id' => $germplasmDbId,
                        'data-target' => '#view-germplasm-widget-modal',
                        'data-toggle' => 'modal'
                    ];

                    $widgetName = GermplasmInfo::widget([
                        'id' => $germplasmDbId,
                        'entityLabel' => $designation,
                        'linkAttrs' => $linkAttrs
                    ]);
                    return $widgetName;
                },
                'format'=>'raw',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'germplasmNameType',
                'label' => 'Germplasm Name Type',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'parentage',
                'contentOptions' => [
                    'class' => 'germplasm-name-col'
                ],
                'headerOptions' => [
                    'min-width' => '150px'
                ],
                'mergeHeader' => true
            ],
            [
                'attribute'=>'generation',
                'label' => 'Generation',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'germplasmState',
                'label' => 'Germplasm State',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'taxonomyName',
                'label' => 'Taxonomy',
                'mergeHeader' => true
            ]
        ];

        $seedColumns = [
            [
                'attribute'=>'designation',
                'label' => 'Germplasm',
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ],
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedName',
                'label' => 'Seed Name',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'programCode',
                'label' => 'Program',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'harvestDate',
                'label' => 'Harvest Date',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'harvestMethod',
                'label' => 'Harvest Method',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedMtaNumber',
                'label' => 'MTA Number',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedMtaStatus',
                'label' => 'MTA Status',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedIpStatus',
                'label' => 'IP Status',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedImport',
                'label' => 'Import',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedOrigin',
                'label' => 'Origin',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedSourceHarvestYear',
                'label' => 'Source Harvest Year',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedSourceOrganization',
                'label' => 'Source Organization',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedSourceStudy',
                'label' => 'Source Study',
                'mergeHeader' => true
            ]
        ];

        $packageColumns = [
            [
                'attribute'=>'designation',
                'label' => 'Germplasm',
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ],
                'mergeHeader' => true
            ],
            [
                'attribute'=>'packageLabel',
                'label' => 'Package Label',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'programCode',
                'label' => 'Program',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'packageStatus',
                'label' => 'Package Status',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'packageQuantity',
                'label' => 'Package Quantity',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'packageUnit',
                'label' => 'Package Unit',
                'mergeHeader' => true
            ]
        ];

        $expected = [
            'germplasmColumns' => $germplasmColumns,
            'seedColumns' => $seedColumns,
            'packageColumns' => $packageColumns
        ];

        // ACT
        $actual = $this->viewFileUploadModel->getColumns();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    // /**
    //  * @skip
    //  * getColumn: Test 2
    //  * This will be added in a future sprint
    //  */
    // public function testGetColumnSubFunction() {
        
    // }

    /**
     * paginate: Test 1
     */
    public function testPaginate() {
        // ARRANGE
        // input
        $fileData = [ 'a', 'b', 'c', 'd', 'e' ];
        $entity = 'germplasm';

        // mocks
        $gridPageSize = 1;
        $gridCurrentPage = 3;

        $queryParams = [
            "page" => $gridCurrentPage
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "m-file-upload-$entity-grid-pjax",
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->viewFileUploadModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // expected
        $expected = [
            "c"
        ];

        // ACT
        $actual = $this->viewFileUploadModel->paginate($fileData, $entity);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * paginate: Test 2
     */
    public function testPaginateWhenRecordCountExceedsPageLimit() {
        // ARRANGE
        // input
        $fileData = [ 
            'a', 'b', 'c', 'd', 'e',
            'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o',
        ];
        $entity = 'germplasm';

        // mocks
        $gridPageSize = 10;
        $gridCurrentPage = 10;

        $queryParams = [
            "page" => $gridCurrentPage
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "m-file-upload-$entity-grid-pjax",
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->viewFileUploadModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // expected
        $expected = [
            'a', 'b', 'c', 'd', 'e',
            'f', 'g', 'h', 'i', 'j'
        ];

        // ACT
        $actual = $this->viewFileUploadModel->paginate($fileData, $entity);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testSearch() {
        // ARRANGE
        // input
        $fileUploadDbId = 1001;
        $programDbId = 101;

        // mocks
        $this->viewFileUploadModel = $this->construct(
            $this->viewFileUploadModel,
            [
                "config" => $this->configMock,
                "crop" => $this->cropMock,
                "program" => $this->programMock,
                "userDashboardConfig" => $this->userDashboardConfigMock
            ]
        );

        $fileName = 'Test_File';
        $fileStatus = 'Validated';
        $uploader = 'Irri, Bims';
        $returnValue = [
            'status' => 200,
            'body' => [
                'metadata' => [
                    'pagination' => [
                        'totalCount' => 1
                    ]
                ],
                'result' => [
                    'data' => [
                        [
                            'germplasmFileUploadDbId' => 123,
                            'fileName' => $fileName,
                            'fileStatus' => $fileStatus,
                            'uploader' => $uploader,
                            'germplasm' => [
                                [
                                    'germplasmDbId' => 2001,
                                    'designation' => 'ABC'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') use ($returnValue) {
                return $returnValue;
            }
        ]);

        // expected
        $germplasmDataProvider = new ArrayDataProvider([
            'id' => 'gm-file-upload-germplasm-grid',
            'allModels' => [ ['germplasmDbId' => 2001, 'designation' => 'ABC'] ],
            'restified' => true,
            'totalCount' => 1
        ]);
        $seedDataProvider = new ArrayDataProvider([
            'id' => 'gm-file-upload-seed-grid',
            'allModels' => [ ['germplasmDbId' => 2001, 'designation' => 'ABC'] ],
            'restified' => true,
            'totalCount' => 1
        ]);
        $packageDataProvider = new ArrayDataProvider([
            'id' => 'gm-file-upload-package-grid',
            'allModels' => [ ['germplasmDbId' => 2001, 'designation' => 'ABC'] ],
            'restified' => true,
            'totalCount' => 1
        ]);
        $germplasmDataProvider->id = null;
        $seedDataProvider->id = null;
        $packageDataProvider->id = null;
        $columns = $this->viewFileUploadModel->getColumns();
        $expected =  [
            "fileName" => $fileName,
            "fileStatus" => $fileStatus,
            "uploader" => $uploader,
            "uploadTimestamp" => 'Unknown timestamp',
            "dataProviders" => [
                "germplasmDataProvider" => $germplasmDataProvider,
                "seedDataProvider" => $seedDataProvider,
                "packageDataProvider" => $packageDataProvider,
            ],
            "columns" => $columns
        ];

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        // Mock page limit
        $gridPageSize = 10;
        $this->viewFileUploadModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // ACT
        $actual = $this->viewFileUploadModel->search($fileUploadDbId, $programDbId);
        $actual["dataProviders"]["germplasmDataProvider"]->id = null;
        $actual["dataProviders"]["seedDataProvider"]->id = null;
        $actual["dataProviders"]["packageDataProvider"]->id = null;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test successful retrieval of data provider
     */
    public function testGetDataProviderSuccessful() {
        // ARRANGE
        $columns = [
            [
                'attribute' => 'germplasmCode',
                'label' => 'Germplasm Code',
                'visible' => true,
                'hidden' => false,
                'mergeHeader' => true
            ]
        ];

        $expected = [
            'data' => [
                [
                    ['germplasmDbId'=>1,'designation'=>"GERMPLASM#1"]
                ]
            ],
            'columns' => $columns,
            'totalCount' => 1
            
        ];

        $browserVariables = [
            [
                'abbrev' => 'GERMPLASM_CODE',
                'name' => 'Germplasm Code',
                'label' => 'Germplasm Code'
            ]
        ];
        $entity = '';
        $fileUploadDbId = 123;

        //mocks
        $this->viewFileUploadModel = $this->construct(
            $this->viewFileUploadModel,
            [
                "config" => $this->configMock,
                "crop" => $this->cropMock,
                "program" => $this->programMock,
                "userDashboardConfig" => $this->userDashboardConfigMock
            ]
        );

        $responseValue = [
            'status' => 200,
            'body' => [
                'metadata' => [
                    'pagination' => [
                        'totalCount' => 1
                    ]
                ],
                'result' => [
                    'data' => [
                        [
                            ['germplasmDbId'=>1,'designation'=>"GERMPLASM#1"]
                        ]
                    ]
                ]
            ]
        ];
        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') use ($responseValue) {
                return $responseValue;
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        // Mock page limit
        $gridPageSize = 10;
        $this->viewFileUploadModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // ACT
        $actual = $this->viewFileUploadModel->getDataProvider($browserVariables, $entity, $fileUploadDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getDataProvider when no columns are provided
     */
    public function testGetDataProviderNoColumns() {
        // ARRANGE
        $expected = [
            'data' => [],
            'columns' => [],
            'totalCount' => 0
        ];

        $browserVariables = [];
        $entity = 'germplasm';
        $fileUploadDbId = 123;

        // ACT
        $actual = $this->viewFileUploadModel->getDataProvider($browserVariables, $entity, $fileUploadDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getDataProvider when retrieval of records fail
     */
    public function testGetDataProviderFailedRetrieval() {
        // ARRANGE
        $columns = [
            [
                'attribute' => 'germplasmCode',
                'label' => 'Germplasm Code',
                'visible' => true,
                'hidden' => false,
                'mergeHeader' => true
            ]
        ];

        $expected = [
            'data' => [],
            'columns' => $columns,
            'totalCount' => 1
            
        ];

        $browserVariables = [
            [
                'abbrev' => 'GERMPLASM_CODE',
                'name' => 'Germplasm Code',
                'label' => 'Germplasm Code'
            ]
        ];
        $entity = '';
        $fileUploadDbId = 123;

        //mocks
        $responseValue = [
            'status' => 400,
            'body' => [
                'metadata' => [
                    'pagination' => [
                        'totalCount' => 0
                    ]
                ],
                'result' => [
                    'data' => []
                ]
            ]
        ];
        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') use ($responseValue) {
                return $responseValue;
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        // ACT
        $this->assertThrows(\yii\base\ErrorException::class,
            function() use ($browserVariables, $entity, $fileUploadDbId) {
                $this->viewFileUploadModel->getDataProvider($browserVariables, $entity, $fileUploadDbId);
            }
        );
    }
}