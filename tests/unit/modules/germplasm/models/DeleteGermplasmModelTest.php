<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\germplasm\models;

// Import data providers
use Yii;

class DeleteGermplasmModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $this->deleteGermplasmModel = \Yii::$container->get('deleteGermplasmModel');
    }

    protected function _after()
    {
    }

    /**
     * Test successful deleteGermplasmRelation()
     */
    public function testDeleteGermplasmRelationSuccessful()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ "germplasmDbId" => "1234"]
                    ]
                ]
            ]
        ];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = true;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasmRelation($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test failed deleteGermplasmRelation() #1
     */
    public function testDeleteGermplasmRelationFailedTest1()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [
            "status" => 500,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => []
                ]
            ]
        ];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = false;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasmRelation($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test failed deleteGermplasmRelation() #2
     */
    public function testDeleteGermplasmRelationFailedTest2()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = false;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasmRelation($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test successful deleteGermplasmAttribute()
     */
    public function testDeleteGermplasmAttributeSuccessful()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ "germplasmDbId" => "1234"]
                    ]
                ]
            ]
        ];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = true;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasmAttribute($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test failed deleteGermplasmAttribute() #1
     */
    public function testDeleteGermplasmAttributeFailedTest1()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [
            "status" => 500,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => []
                ]
            ]
        ];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = false;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasmAttribute($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test failed deleteGermplasmAttribute() #2
     */
    public function testDeleteGermplasmAttributeFailedTest2()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = false;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasmAttribute($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test successful deleteGermplasmName()
     */
    public function testDeleteGermplasmNameSuccessful()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ "germplasmDbId" => "1234"]
                    ]
                ]
            ]
        ];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = true;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasmName($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test failed deleteGermplasmName() #1
     */
    public function testDeleteGermplasmNameFailedTest1()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [
            "status" => 500,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => []
                ]
            ]
        ];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = false;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasmName($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test failed deleteGermplasmName() #2
     */
    public function testDeleteGermplasmNameFailedTest2()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = false;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasmName($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test successful deleteGermplasm()
     */
    public function testDeleteGermplasmSuccessful()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ "germplasmDbId" => "1234", "recordCount" => 1]
                    ]
                ]
            ]
        ];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = true;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasm($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test failed deleteGermplasm() #1
     */
    public function testDeleteGermplasmFailedTest1()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [
            "status" => 500,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => []
                ]
            ]
        ];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = false;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasm($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test failed deleteGermplasm() #2
     */
    public function testDeleteGermplasmFailedTest2()
    {
        // ARRANGE
        $germplasmId = 1234;

        // mocks
        $apiReturn = [];

        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturn) {
                return $apiReturn;
            }
        ]);
        Yii::$app->set('api',$api);

        // expected
        $expected = false;

        // ACT
        $actual = $this->deleteGermplasmModel->deleteGermplasm($germplasmId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}