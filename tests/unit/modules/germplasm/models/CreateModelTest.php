<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\germplasm\models;

use Yii;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsArray;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertNull;

class CreateModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $germplasmCreateModel;

    protected $programInfo = [
        "programDbId" => 1,
        "cropProgramDbId" => 1,
        "programCode" => 'TESTPROGRAM'
    ];

    protected $cropProgramInfo = 'TESTCROP';
    
    protected $cropInfo = [
        [
            "cropCode" => "TEST_CROP",
            "cropProgramDbId" => 1,
        ]
    ];

    protected $config = [
        'values' => [
            [
                "abbrev" => "TEST_VARIABLE",
                "name" => "test variable",
                "label" => "Test Variable"
            ]
        ]
    ];

    protected $variableInfo = [
        "variableDbId" => 1,
        "name" => "test variable",
        "abbrev" => "TEST_VARIABLE",
        "label" => "Test Variable"
    ];
    
    protected function _before()
    {
        $this->germplasmCreateModel = Yii::$container->get('germplasmCreateModel');
    }

    protected function _after()
    {
        \Yii::$app->session->set("gefu-variables-required", null);
        \Yii::$app->session->set("gefu-variables-optional", null);
    }

    // tests

    /**
     * Verify KEY_VARIABLES constant is returned
     */
    public function testGetKeyVariables() {
        $expected = 'gefu-variables';
        $actual = $this->germplasmCreateModel->getKeyVariables();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify KEY_VARIABLES_REQUIRED constant is returned
     */
    public function testGetKeyVariablesRequired() {
        $expected = 'gefu-variables-required';
        $actual = $this->germplasmCreateModel->getKeyVariablesRequired();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify KEY_VARIABLES_OPTIONAL constant is returned
     */
    public function testGetKeyVariablesOptional() {
        $expected = 'gefu-variables-optional';
        $actual = $this->germplasmCreateModel->getKeyVariablesOptional();
        $this->assertEquals($expected, $actual);
    }

    public function testGetVariableConfig(){
        // ARRAGE
        $expected = [
            "abbrev" => "TEST_VARIABLE",
                "variable_info" => [
                    "variableDbId" => 1,
                    "name" => "test variable",
                    "abbrev" => "TEST_VARIABLE",
                    "label" => "Test Variable"
                ]
        ];

        $varInfo = [
            "variableDbId" => 1,
            "name" => "test variable",
            "abbrev" => "TEST_VARIABLE",
            "label" => "Test Variable"
        ];

        $this->germplasmCreateModel->programModel->setReturnValue('getProgram',$this->programInfo);
        $this->germplasmCreateModel->programModel->setReturnValue('getCropCode',$this->cropProgramInfo);

        $this->germplasmCreateModel->configModel->setReturnValue('getConfigByAbbrev',$this->config);
        $this->germplasmCreateModel->variableModel->setReturnValue('getVariableByAbbrev',$varInfo);  

        // ACT
        $actual = $this->germplasmCreateModel->getVariableConfig(101);

        // ASSERT
        $this->assertNotNull($actual);
    }

    public function testValidateConfigVariables(){
        // ARRAGE
        $expected = [
            [
                "abbrev" => "TEST_VARIABLE",
                "name" => "test variable",
                "label" => "Test Variable",
                "variable_info" => [
                    "variableDbId" => 1,
                    "name" => "test variable",
                    "abbrev" => "TEST_VARIABLE",
                    "label" => 'Test Variable'
                ]
            ]
        ];

        $this->germplasmCreateModel->variableModel->setReturnValue('getVariableByAbbrev',$this->variableInfo);

        // ACT
        $actual = $this->germplasmCreateModel->validateConfigVariables($this->config['values']);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals($expected,$actual);
    }

    public function testGetVariableConfigNoProgramId(){
        // ARRAGE

        // ACT
        $actual = $this->germplasmCreateModel->getVariableConfig(null);

        // ASSERT
        $this->assertEquals([],$actual);
    }

    public function testGetVariableConfigNoProgramInfo(){
        // ARRAGE
        $this->germplasmCreateModel->programModel->setReturnValue('getProgram',[]);

        // ACT
        $actual = $this->germplasmCreateModel->getVariableConfig(101);

        // ASSERT
        $this->assertEquals([],$actual);
    }

    public function testGetVariableConfigNoCropInfo(){
        // ARRAGE
        $this->germplasmCreateModel->programModel->setReturnValue('getProgram',$this->programInfo);
        $this->germplasmCreateModel->programModel->setReturnValue('getCropCode','');

        // ACT
        $actual = $this->germplasmCreateModel->getVariableConfig(101);

        // ASSERT
        $this->assertEquals([],$actual);
    }

    public function testGetVariableConfigNoConfigValues(){
        // ARRAGE
        $this->germplasmCreateModel->programModel->setReturnValue('getProgram',$this->programInfo);
        $this->germplasmCreateModel->programModel->setReturnValue('getCropCode',$this->cropProgramInfo);

        $this->germplasmCreateModel->configModel->setReturnValue('getConfigByAbbrev',[]);

        // ACT
        $actual = $this->germplasmCreateModel->getVariableConfig(101);

        // ASSERT
        $this->assertEquals([],$actual);
    }

    public function testGetVariableConfigNoVariableInfo(){
        // ARRAGE
        $this->germplasmCreateModel->programModel->setReturnValue('getProgram',$this->programInfo);
        $this->germplasmCreateModel->programModel->setReturnValue('getCropCode',$this->cropProgramInfo);

        $this->germplasmCreateModel->configModel->setReturnValue('getConfigByAbbrev',$this->config);
        $this->germplasmCreateModel->variableModel->setReturnValue('getVariableByAbbrev',[]);

        // ACT
        $actual = $this->germplasmCreateModel->getVariableConfig(101);

        // ASSERT
        $this->assertEquals([],$actual);
    }

    /**
     * validateUploadedFile: Test 1
     * There are NO missing columns
     */
    public function testValidateUploadedFileTestWhenThereAreNoMissingColumns() {
        // ARRANGE
        // input
        $programId = 1234;
        $headers = [
            'DESIGNATION',
            'GERMPLASM_TYPE'
        ];
        $variableConfig = [
            [
                'entity' => 'germplasm',
                'abbrev' => 'DESIGNATION',
                'usage' => 'required',
                'required' => true
            ],
            [
                'entity' => 'germplasm',
                'abbrev' => 'GERMPLASM_TYPE',
                'usage' => 'optional',
                'required' => false
            ]
        ];

        // expected
        $expected = [
            'success' => true,
            'error' => ''
        ];
        $sessionRequired = [
            'germplasm' => [
                'DESIGNATION'
            ]
        ];
        $sessionOptional = [
            'germplasm' => [
                'GERMPLASM_TYPE'
            ]
        ];

        // ACT
        $actual = $this->germplasmCreateModel->validateUploadedFile($programId,$headers,$variableConfig);
        $actualSessionRequired = \Yii::$app->session->get("gefu-variables-required");
        $actualSessionOptional = \Yii::$app->session->get("gefu-variables-optional");

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($sessionRequired, $actualSessionRequired);
        $this->assertEquals($sessionOptional, $actualSessionOptional);
    }

    /**
     * validateUploadedFile: Test 2
     * There are missing columns
     */
    public function testValidateUploadedFileTestWhenThereAreMissingColumns() {
        // ARRANGE
        // input
        $programId = 1234;
        $headers = [
            'DESIGNATION',
            'GERMPLASM_TYPE',
            'SEED_NAME'
        ];
        $variableConfig = [
            [
                'entity' => 'germplasm',
                'abbrev' => 'DESIGNATION',
                'usage' => 'required',
                'required' => true
            ],
            [
                'entity' => 'germplasm',
                'abbrev' => 'GERMPLASM_STATE',
                'usage' => 'required',
                'required' => true
            ],
            [
                'entity' => 'germplasm',
                'abbrev' => 'GERMPLASM_TYPE',
                'usage' => 'optional',
                'required' => false
            ],
            [
                'entity' => 'seed',
                'abbrev' => 'SEED_NAME',
                'usage' => 'required',
                'required' => true
            ],
            [
                'entity' => 'seed',
                'abbrev' => 'PROGRAM_CODE',
                'usage' => 'required',
                'required' => true
            ],
            [
                'entity' => 'package',
                'abbrev' => 'PACKAGE_LABEL',
                'usage' => 'required',
                'required' => true
            ]
        ];

        // expected
        $expected = [
            'success' => false,
            'error' => 'Missing required column header/s GERMPLASM_STATE,PROGRAM_CODE,PACKAGE_LABEL.'
        ];
        $sessionRequired = [
            'germplasm' => [
                'DESIGNATION',
                'GERMPLASM_STATE'
            ],
            'seed' => [
                'SEED_NAME',
                'PROGRAM_CODE'
            ],
            'package' => [
                'PACKAGE_LABEL'
            ]
        ];
        $sessionOptional = [
            'germplasm' => [
                'GERMPLASM_TYPE'
            ]
        ];

        // ACT
        $actual = $this->germplasmCreateModel->validateUploadedFile($programId,$headers,$variableConfig);
        $actualSessionRequired = \Yii::$app->session->get("gefu-variables-required");
        $actualSessionOptional = \Yii::$app->session->get("gefu-variables-optional");

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($sessionRequired, $actualSessionRequired);
        $this->assertEquals($sessionOptional, $actualSessionOptional);
    }

    /**
     * saveUploadedFile: Test 1
     * If creation of germplasm file upload record is successful
     */
    public function testSaveUploadedFileWhenGfuRecordIsSuccessful() {
        // ARRANGE
        // input
        $programId = 1234;
        $fileName = "TestFile.csv";
        $fileContent = [
            // [
            //     'designation' => 'IR 12345-B',
            //     'germplasm_state' => 'not_fixed',
            //     'germplasm_type' => 'progeny',
            //     'seed_name' => '500000001',
            //     'program_code' => 'IRSEA',
            //     'package_label' => '40'
            // ]
        ];

        // mock session
        $sessionRequired = [
            'germplasm' => [
                'DESIGNATION',
                'GERMPLASM_STATE'
            ],
            'seed' => [
                'SEED_NAME',
                'PROGRAM_CODE'
            ],
            'package' => [
                'PACKAGE_LABEL'
            ]
        ];
        $sessionOptional = [
            'germplasm' => [
                'GERMPLASM_TYPE'
            ]
        ];
        \Yii::$app->session->set("gefu-variables-required", $sessionRequired);
        \Yii::$app->session->set("gefu-variables-optional", $sessionOptional);

        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    'status' => 200,
                    'body' => [
                        'result' => [
                            'data' => [
                                [
                                    'germplasmFileUploadDbId' => 123
                                ]
                            ]
                        ]
                    ]
                ];
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        // expected
        $expected = [
            'success' => true,
            'error' => '',
            'id' => 123
        ];

        // ACT
        $actual = $this->germplasmCreateModel->saveUploadedFile($programId,$fileName,$fileContent);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * saveUploadedFile: Test 2
     * If creation of germplasm file upload record is successful
     */
    public function testSaveUploadedFileWhenGfuRecordIsNotSuccessful() {
        // ARRANGE
        // input
        $programId = 1234;
        $fileName = "TestFile.csv";
        $fileContent = [
            [
                'designation' => 'IR 12345-B',
                'germplasm_state' => 'not_fixed',
                'germplasm_type' => 'progeny',
                'seed_name' => '500000001',
                'program_code' => 'IRSEA',
                'package_label' => '40'
            ]
        ];

        // mock session
        $sessionRequired = [
            'germplasm' => [
                'DESIGNATION',
                'GERMPLASM_STATE'
            ],
            'seed' => [
                'SEED_NAME',
                'PROGRAM_CODE'
            ],
            'package' => [
                'PACKAGE_LABEL'
            ]
        ];
        $sessionOptional = [
            'germplasm' => [
                'GERMPLASM_TYPE'
            ]
        ];
        \Yii::$app->session->set("gefu-variables-required", $sessionRequired);
        \Yii::$app->session->set("gefu-variables-optional", $sessionOptional);

        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    'status' => 500
                ];
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        // expected
        $expected = [
            'success' => false,
            'error' => 'Error encountered creating file upload record.'
        ];

        // ACT
        $actual = $this->germplasmCreateModel->saveUploadedFile($programId,$fileName,$fileContent);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
    
    /**
     * getFileUploadRecord: Test 1
     * Record retrieval is successful
     */
    public function testGetFileUploadRecordWhenRetrievalIsSuccessful() {
        // ARRANGE
        // input
        $germplasmFileUploadDbId = 123;

        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    'status' => 200,
                    'body' => [
                        'result' => [
                            'data' => [
                                [
                                    'germplasmFileUploadDbId' => 123
                                ]
                            ]
                        ]
                    ]
                ];
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        // expected
        $expected = [
            'status' => 200,
            'body' => [
                'result' => [
                    'data' => [
                        [
                            'germplasmFileUploadDbId' => 123
                        ]
                    ]
                ]
            ]
        ];

        // ACT
        $actual = $this->germplasmCreateModel->getFileUploadRecord($germplasmFileUploadDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getFileUploadRecord: Test 2
     * Record retrieval is unsuccessful
     */
    public function testGetFileUploadRecordWhenRetrievalIsUnsuccessful() {
        // ARRANGE
        // input
        $germplasmFileUploadDbId = 123;

        // Mock Yii::$app->api->getResponse(...)
        $api = $this->make('app\components\Api', [
            'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {
                return [
                    'status' => 500
                ];
            }
        ]);

        // Inject api mock into Yii::$app
        Yii::$app->set('api', $api);

        // ACT + ASSERT
        $this->assertThrows(\yii\base\ErrorException::class,
            function() use ($germplasmFileUploadDbId) {
                $this->germplasmCreateModel->getFileUploadRecord($germplasmFileUploadDbId);
            }
        );
    }

}

?>