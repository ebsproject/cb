<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\germplasm\models;

use Yii;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsArray;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertNull;

class TransactionTemplateModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;

    protected function _before()
    {
        $this->transactionTemplateModel = Yii::$container->get('transactionTemplateModel');
        $this->inventoryListsModel = Yii::$container->get('inventoryListsModel');
    }

    protected function _after()
    {
        
    }

    public function testGetEntityGermplasm() {
        $actual = $this->transactionTemplateModel->getEntityGermplasm();
        $this->assertEquals('germplasm', $actual);
    }

    public function testGetEntityGermplasmAttribute() {
        $actual = $this->transactionTemplateModel->getEntityGermplasmAttribute();
        $this->assertEquals('germplasm_attribute', $actual);
    }

    public function testGetGermplasmTemplate() {
        $actual = $this->transactionTemplateModel->getGermplasmTemplate();
        $this->assertEquals('germplasm', $actual);
    }

    public function testGetGermplasmAttributeTemplate() {
        $actual = $this->transactionTemplateModel->getGermplasmAttributeTemplate();
        $this->assertEquals('germplasm_attribute', $actual);
    }

    public function testGetActionUpdate() {
        $actual = $this->transactionTemplateModel->getActionUpdate();
        $this->assertEquals('update', $actual);
    }

    public function testGetTypeStringWithGermplasmType() {
        $actual = $this->transactionTemplateModel->getTypeString('germplasm');
        $this->assertEquals('GERMPLASM', $actual);
    }

    public function testGetTypeStringWithGermplasmAttributeType() {
        $actual = $this->transactionTemplateModel->getTypeString('germplasm_attribute');
        $this->assertEquals('', $actual);
    }

    public function testGetTypeStringWithNoType() {
        $actual = $this->transactionTemplateModel->getTypeString();
        $this->assertEquals('', $actual);
    }

    public function testGetTypeOptionsConfig() {
        $expected = [
            'germplasm' => [
                'update' => ['germplasm', 'germplasm_attribute']
            ]
        ];

        $actual = $this->transactionTemplateModel->getTypeOptionsConfig();
        $this->assertEquals($expected, $actual);
    }

    public function testGetTemplateOptionsGermplasm() {
        $expected = [
            [ "display_text" => "Germplasm", "type" => 'germplasm'],
            [ "display_text" => "Germplasm Attribute", "type" => 'germplasm_attribute']
        ];

        $actual = $this->transactionTemplateModel->getTemplateOptions('update','germplasm');
        $this->assertEquals($expected, $actual);
    }

    public function testGetTemplateOptionsGermplasmAttribute() {
        $actual = $this->transactionTemplateModel->getTemplateOptions('update','germplasm_attribute');
        $this->assertEquals([], $actual);
    }

    public function testGetTemplateOptionsNoEntity() {
        $actual = $this->transactionTemplateModel->getTemplateOptions('update');
        $this->assertEquals([], $actual);
    }

    public function testGetTemplateHeaders() {
        $expected = ['GERMPLASM_CODE','DESIGNATION'];
        $configValue = [
            'configValues' => [
                'values' => [
                    [
                        "in_export" => "true",
                        "entity" => "germplasm",
                        "type" => "basic",
                        "abbrev" => "GERMPLASM_CODE"
                    ],
                    [
                        "in_export" => "true",
                        "entity" => "germplasm",
                        "type" => "basic",
                        "abbrev" => "DESIGNATION"
                    ]
                ]
            ]
        ];

        $this->transactionTemplateModel->germplasmFileUploadModel->setReturnValue('getConfigValues',$configValue);

        $actual = $this->transactionTemplateModel->getTemplateHeaders('germplasm','update','admin','IRSEA','RICE','germplasm');
        $this->assertEquals($expected, $actual);
    }

    public function testGetTemplateHeadersNotGermplasm() {
        $expected = ['GERMPLASM_CODE','SEED_NAME'];
        $configValue = [
            'configValues' => [
                'values' => [
                    [
                        "in_export" => "true",
                        "entity" => "germplasm",
                        "type" => "basic",
                        "abbrev" => "GERMPLASM_CODE"
                    ],
                    [
                        "in_export" => "true",
                        "entity" => "germplasm",
                        "type" => "basic",
                        "abbrev" => "SEED_NAME"
                    ]
                ]
            ]
        ];

        $this->transactionTemplateModel->germplasmFileUploadModel->setReturnValue('getConfigValues',$configValue);

        $actual = $this->transactionTemplateModel->getTemplateHeaders('germplasm','update','admin','IRSEA','RICE','seed');
        $this->assertEquals($expected, $actual);
    }

    public function testGetListMemberDataNoHeaders() {
        $listId = 123;
        $templateType = 'germplasm';
        $action = 'merge';

        $headers = [];

        $searchAllResult = [
            "metadata" => [
                "pagination" => [
                    "pageSize" => 100,
                    "totalCount" => 1,
                    "currentPage" => 1,
                    "totalPages" => 1
                ],
                "status" => [
                    [
                        "message" => "Request has been successfully completed.",
                        "messageType" => "INFO"
                    ]
                ],
                "datafiles" => []
            ],
            "result" => [
                "data" => []
            ]
        ];

        $this->transactionTemplateModel->listsModel->setReturnValue('searchAllMembers',$searchAllResult);

        $actual = $this->transactionTemplateModel->getListMemberData($listId, $templateType, $headers, $action);
        $this->assertEquals([], $actual);
    }

    public function testGetListMemberDataWithHeaders() {
        $listId = 123;
        $templateType = 'germplasm';
        $action = 'merge';

        $headers = ['GERMPLASM_CODE','DESIGNATION'];

        $expected = [
            [ 'germplasmCode' => 'GE0000000000001', 'designation' => 'member germplasm code' ],
            [ 'germplasmCode' => 'GE0000000000002', 'designation' => 'member germplasm code #2' ]
        ];

        $searchAllResult = [
            "metadata" => [
                "pagination" => [
                    "pageSize" => 100,
                    "totalCount" => 2,
                    "currentPage" => 1,
                    "totalPages" => 1
                ],
                "status" => [
                    [
                        "message" => "Request has been successfully completed.",
                        "messageType" => "INFO"
                    ]
                ],
                "datafiles" => []
            ],
            "result" => [
                "data" => [
                    [ 
                        'listMemberDbId' => '456', 
                        'germplasmCode' => 'GE0000000000001', 
                        'designation' => 'member germplasm code'     
                    ],
                    [
                        'listMemberDbId' => '789', 
                        'germplasmCode' => 'GE0000000000002', 
                        'designation' => 'member germplasm code #2' 
                    ]
                ]
            ]
        ];

        $this->transactionTemplateModel->listsModel->setReturnValue('searchAllMembers',$searchAllResult);

        $actual = $this->transactionTemplateModel->getListMemberData($listId, $templateType, $headers, $action);

        // $this->assertEquals($expected, $actual);
    }
}
?>