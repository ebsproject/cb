<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\germplasm\models;

// Import data providers
use app\dataproviders\ArrayDataProvider;

class ErrorLogModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $errorLogModel;
    protected $configMock;
    protected $cropMock;
    protected $programMock;

    protected function _before()
    {
        $this->errorLogModel = \Yii::$container->get('errorLogModel');
        $this->configMock = \Yii::$container->get('configMock');
        $this->cropMock = \Yii::$container->get('cropMock');
        $this->programMock = \Yii::$container->get('programMock');
        $this->userDashboardConfigMock = \Yii::$container->get('userDashboardConfigMock');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * getFileUploadRecord: Test 1
     * When the retrieval is successful, the data should be returned
     */
    public function testGetFileUploadRecordWhenRetrievalIsSuccessful()
    {
        // ARRANGE
        // inputs
        $fileUploadDbId = 1234;

        // mocks
        $this->errorLogModel = $this->construct(
            $this->errorLogModel,
            [
                "config" => $this->configMock,
                "crop" => $this->cropMock,
                "program" => $this->programMock,
                "userDashboardConfig" => $this->userDashboardConfigMock
            ],
            [
                'getOne' => function () {
                    return [
                        "status" => 200,
                        "data" => ['data1','data2','data3']
                    ];
                }
            ]
        );

        // expected
        $expected = ['data1','data2','data3'];

        // ACT
        $actual = $this->errorLogModel->getFileUploadRecord($fileUploadDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getFileUploadRecord: Test 2
     * When the retrieval is successful, an error exception must be thrown
     */
    public function testGetFileUploadRecordWhenRetrievalIsUnsuccessful()
    {
        // ARRANGE
        // inputs
        $fileUploadDbId = 1234;

        // mocks
        $this->errorLogModel = $this->construct(
            $this->errorLogModel,
            [
                "config" => $this->configMock,
                "crop" => $this->cropMock,
                "program" => $this->programMock,
                "userDashboardConfig" => $this->userDashboardConfigMock
            ],
            [
                'getOne' => function () {
                    return [
                        "status" => 500,
                        "data" => null
                    ];
                }
            ]
        );

        // expected
        $expected = ['data1','data2','data3'];

        // ACT + ASSERT
        $this->assertThrows(\yii\base\ErrorException::class,
            function() use ($fileUploadDbId) {
                $this->errorLogModel->getFileUploadRecord($fileUploadDbId);
            }
        );
    }

    /**
     * getColumns: Test 2
     * Test sub-functions in the columns
     */
    public function testGetColumnsSubFunctions() {
        // ARRANGE
        // input
        $programDbId = 1234;

        // sub function inputs
        $data = [
            'entity' => 'germplasm_name',
            'status_code' => 'ERROR',
            'message' => 'Error: in this row.'
        ];

        // expected per sub function
        $expectedEntity = 'germplasm name';
        $expectedStatusCode = "<strong><p class='red-text text-darken-2'>ERROR</p></strong>";
        $expectedMessage = "<strong>Error</strong>:  in this row.";

        // ACT
        $columns = $this->errorLogModel->getColumns($programDbId);

        // invoking sub functions
        $entityFxn = $columns[1]['content'];
        $actualEntity = $entityFxn($data);
        $statusCodeFxn = $columns[2]['value'];
        $actualStatusCode = $statusCodeFxn($data);
        $messageFxn = $columns[3]['value'];
        $actualMessage = $messageFxn($data);

        // ASSERT
        $this->assertEquals($expectedEntity, $actualEntity);
        $this->assertEquals($expectedStatusCode, $actualStatusCode);
        $this->assertEquals($expectedMessage, $actualMessage);
    }

    /**
     * getColumns: Test 1
     */
    public function testGetColumns() {
        // ARRANGE
        // input
        $programDbId = 1234;

        // mocks
        $expected = [
            [
                'attribute'=>'row_number',
                'label' => 'Row No',
            ],
            [
                'attribute'=>'entity',
                'label' => 'Entity',
                'content' => function($data) {
                    $entity = str_replace('_', ' ', $data['entity']);
                    return $entity;
                }
            ],
            [
                'attribute'=>'status_code',
                'label' => 'Status Code',
                'format' => 'raw',
                'value' => function ($model) {
                    $status = $model['status_code'] ?? '';

                    if($status == 'ERROR') {
                        $status = "<p class='red-text text-darken-2'>$status</p>";
                    }

                    return "<strong>$status</strong>";
                }
            ],
            [
                'attribute'=>'message',
                'label' => 'Error Log Message',
                'contentOptions' => [
                    "class" => "germplasm-name-col"
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    $message = $model['message'] ?? '';
                    $messageParts = explode(':', $message);
                    $mainMessage = $messageParts[0] ?? '';
                    $messageParts[0] = "<strong>$mainMessage</strong>";
                    $message = implode(": ", $messageParts) ;

                    return $message;
                }
            ],
            [
                'attribute'=>'timestamp',
                'label' => 'Error Log Timestamp',
            ]
        ];

        // ACT
        $actual = $this->errorLogModel->getColumns($programDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * paginate: Test 1
     */
    public function testPaginate() {
        // ARRANGE
        // input
        $fileData = [ 'a', 'b', 'c', 'd', 'e' ];

        // mocks
        $gridPageSize = 1;
        $gridCurrentPage = 3;
        $queryParams = [
            "page" => $gridCurrentPage
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "gm-error-log-grid-pjax",
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->errorLogModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // expected
        $expected = [
            "c"
        ];

        // ACT
        $actual = $this->errorLogModel->paginate($fileData);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * paginate: Test 2
     */
    public function testPaginateWhenRecordCountExceedsPageLimit() {
        // ARRANGE
        // input
        $fileData = [ 
            'a', 'b', 'c', 'd', 'e',
            'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o',
        ];

        // mocks
        $gridPageSize = 10;
        $gridCurrentPage = 10;

        $queryParams = [
            "page" => $gridCurrentPage
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "gm-error-log-grid-pjax",
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->errorLogModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // expected
        $expected = [
            'a', 'b', 'c', 'd', 'e',
            'f', 'g', 'h', 'i', 'j'
        ];

        // ACT
        $actual = $this->errorLogModel->paginate($fileData);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 1
     *
     */
    public function testSearch()
    {
        // ARRANGE
        // inputs
        $fileUploadDbId = 1234;
        $programDbId = 567;

        // mocks
        $this->errorLogModel = $this->construct(
            $this->errorLogModel,
            [
                "config" => $this->configMock,
                "crop" => $this->cropMock,
                "program" => $this->programMock,
                "userDashboardConfig" => $this->userDashboardConfigMock
            ],
            [
                'getFileUploadRecord' => function () { 
                    return [
                        "fileName" => "file001",
                        "fileStatus" => "validated",
                        "uploader" => "User, User",
                        "uploaderTimestamp" => "2022",
                        "fileData" => [
                            [
                                "entity1" => [
                                    "e1var1" => "a",
                                    "e1var2" => "b",
                                    "e1var3" => "c"
                                ]
                            ]
                        ],
                        "errorLog" => [
                            [
                                "entity" => "a",
                                "message" => "b",
                                "timestamp" => "c",
                                "status_code" => "d",
                                "row_number" => 1
                            ]
                        ]
                    ];
                },
                'paginate' => function () {
                    return [
                        [
                            "entity1" => [
                                "entity" => "a",
                                "message" => "b",
                                "timestamp" => "c",
                                "status_code" => "d",
                                "row_number" => 1
                            ]
                        ]
                    ];
                },
                'getColumns' => function () {
                    return [ 'col1', 'col2' ];
                }
            ]
        );

        // expected
        $dataProvider = new ArrayDataProvider([
            'allModels' => [
                [
                    "entity1" => [
                        "entity" => "a",
                        "message" => "b",
                        "timestamp" => "c",
                        "status_code" => "d",
                        "row_number" => 1
                    ]
                ]
            ],
            'key' => 'row_number',
            'restified' => true,
            'totalCount' => 1
        ]);
        $dataProvider->id = null;
        $dataProvider->sort = false;
        $expected = [
            'fileName' => 'file001',
            'fileStatus' => 'Validated',
            'uploader' => 'User, User',
            'uploadTimestamp' => '2022',
            'dataProvider' => $dataProvider,
            'columns' => [ 'col1', 'col2' ]
        ];

        // ACT
        $actual = $this->errorLogModel->search($fileUploadDbId, $programDbId);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}