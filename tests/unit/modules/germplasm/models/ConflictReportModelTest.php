<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace models;

use Yii;

class ConflictReportModelTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $conflictReportModel;

    protected function _before(){
        $this->conflictReportModel = Yii::$container->get('conflictReportModel');
    }

    protected function _after(){
    }

    /**
     * Verify that conflict report columns are returned
     */
    public function testGetConflictColumns()
    {
        $expected = ['row_number','conflict_code','conflict_variable',
        'keep_germplasm_designation','merge_germplasm_designation','keep_germplasm_code','merge_germplasm_code',
        'keep_value','merge_value'];
        
        $actual = $this->conflictReportModel->getConflictColumns();
    
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that conflict report columns are returned
     */
    public function testSearchNoReturn()
    {
        $expected = [
            'data' => [],
            'totalCount' => 0,
            'success' => true
        ];

        \Yii::$app->set('basePath','basePath');

        $fileId = 123;
        $conflictReportName = 'fileName';
        $browserId = 'browser-id-grid';
        
        $actual = $this->conflictReportModel->search($fileId,$conflictReportName,$browserId);
    
        $this->assertEquals($expected, $actual);
    }
}

?>