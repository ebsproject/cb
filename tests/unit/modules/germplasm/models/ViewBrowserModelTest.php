<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\germplasm\models;

// Import data providers
use app\dataproviders\ArrayDataProvider;
use app\models\Person;

class ViewBrowserModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;
    
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $viewBrowserModel;
    protected $configMock;
    protected $cropMock;
    protected $programMock;
    protected $personMock;
    protected $germplasmFileUploadMock;
    
    protected function _before()
    {
        $this->viewBrowserModel = \Yii::$container->get('viewBrowserModel');
        $this->configMock = \Yii::$container->get('configMock');
        $this->cropMock = \Yii::$container->get('cropMock');
        $this->programMock = \Yii::$container->get('programMock');
        $this->personMock = \Yii::$container->get('person');
        $this->germplasmFileUploadMock = \Yii::$container->get('germplasmFileUpload');
        $this->userDashboardConfigMock = \Yii::$container->get('userDashboardConfigMock');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * getFileUploadRecord: Test 1
     * When the retrieval is successful, the data should be returned
     */
    public function testGetFileUploadRecordWhenRetrievalIsSuccessful()
    {
        // ARRANGE
        // inputs
        $fileUploadDbId = 1234;

        // mocks
        $this->viewBrowserModel = $this->construct(
            $this->viewBrowserModel,
            [
                "config" => $this->configMock,
                "crop" => $this->cropMock,
                "program" => $this->programMock,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "person" => $this->personMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock
            ],
            [
                'searchAll' => function () { 
                    return [
                        "status" => 200,
                        "data" => ['data1','data2','data3']
                    ]; 
                }
            ]
        );

        // expected
        $expected = ['data1','data2','data3'];
        
        // ACT
        $actual = $this->viewBrowserModel->getFileUploadRecord($fileUploadDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getFileUploadRecord: Test 2
     * When the retrieval is successful, an error exception must be thrown
     */
    public function testGetFileUploadRecordWhenRetrievalIsUnsuccessful()
    {
        // ARRANGE
        // inputs
        $fileUploadDbId = 1234;

        // mocks
        $this->viewBrowserModel = $this->construct(
            $this->viewBrowserModel,
            [
                "config" => $this->configMock,
                "crop" => $this->cropMock,
                "program" => $this->programMock,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "person" => $this->personMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock
            ],
            [
                'getOne' => function () { 
                    return [
                        "status" => 500,
                        "data" => null
                    ]; 
                }
            ]
        );

        // expected
        $expected = ['data1','data2','data3'];
        
        // ACT + ASSERT
        $this->assertThrows(\yii\base\ErrorException::class, 
            function() use ($fileUploadDbId) {
                $this->viewBrowserModel->getFileUploadRecord($fileUploadDbId);
            }
        );
    }

    /**
     * getColumns: Test 1
     * When no configs are available
     */
    public function testGetColumnsWhenNoConfigsAreAvailable() {
        // ARRANGE
        // input
        $programDbId = 1234;

        // mocks
        $returnValue = [
            "return_vals" => [
                [ ],
                [ ]
            ]
        ];
        $this->viewBrowserModel->config->setReturnValue('getConfigByAbbrev', $returnValue);
        $returnValue = [
            "cropProgramDbId" => 123,
            "programCode" => "PROGRAM_CODE"
        ];
        $this->viewBrowserModel->program->setReturnValue('getProgram', $returnValue);

        $this->viewBrowserModel->program->setReturnValue('getCropCode', "CROP_CODE");

        $expected = [];

        // ACT
        $actual = $this->viewBrowserModel->getColumns($programDbId);
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getColumns: Test 2
     * When the config abbrev is PROGRAM
     */
    public function testGetColumnsWhenConfigAbbrevIsProgram() {
        // ARRANGE
        // input
        $programDbId = 1234;

        // mocks
        $returnValue = [
            "return_vals" => [
                [ 
                    "values" => [
                        [
                            "abbrev" => "PROGRAM",
                            "name" => "Program"
                        ]
                    ]
                ],
                [ ]
            ]
        ];
        $this->viewBrowserModel->config->setReturnValue('getConfigByAbbrev', $returnValue);
        $returnValue = [
            "cropProgramDbId" => 123,
            "programCode" => "PROGRAM_CODE"
        ];
        $this->viewBrowserModel->program->setReturnValue('getProgram', $returnValue);

        $this->viewBrowserModel->program->setReturnValue('getCropCode', "CROP_CODE");

        $expected = [
            [
                "attribute" => "program_code_p",
                "label" => "Program (Package)",
                "hAlign" => "left",
                "vAlign" => "top",
                "mergeHeader" => true
            ]
        ];

        // ACT
        $actual = $this->viewBrowserModel->getColumns($programDbId);
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getColumns: Test 3
     * When the config abbrev is PROGRAM
     */
    public function testGetColumnsWhenConfigAbbrevIsProgramCode() {
        // ARRANGE
        // input
        $programDbId = 1234;

        // mocks
        $returnValue = [
            "return_vals" => [
                [ 
                    "values" => [
                        [
                            "abbrev" => "PROGRAM_CODE",
                            "name" => "Program Code"
                        ]
                    ]
                ],
                [ ]
            ]
        ];
        $this->viewBrowserModel->config->setReturnValue('getConfigByAbbrev', $returnValue);
        $returnValue = [
            "cropProgramDbId" => 123,
            "programCode" => "PROGRAM_CODE"
        ];
        $this->viewBrowserModel->program->setReturnValue('getProgram', $returnValue);

        $this->viewBrowserModel->program->setReturnValue('getCropCode', "CROP_CODE");

        $expected = [
            [
                "attribute" => "program_code_s",
                "label" => "Program (Seed)",
                "hAlign" => "left",
                "vAlign" => "top",
                "mergeHeader" => true
            ]
        ];

        // ACT
        $actual = $this->viewBrowserModel->getColumns($programDbId);
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * formatFileData: Test 1
     * Basic test
     */
    public function testFormatFileData() {
        // ARRANGE
        // input
        $originalFileData = [
            [
                "entity1" => [
                    "e1var1" => "a",
                    "e1var2" => "b",
                    "e1var3" => "c"
                ],
                "entity2" => [
                    "e2var1" => "d",
                    "e2var2" => "e",
                    "e2var3" => "f"
                ],
                "entity3" => [
                    "e3var1" => "g",
                    "e3var2" => "h",
                    "e3var3" => "i"
                ]
            ],
            [
                "entity1" => [
                    "e1var1" => "a",
                    "e1var2" => "b",
                    "e1var3" => "c"
                ],
                "entity2" => [
                    "e2var1" => "d",
                    "e2var2" => "e",
                    "e2var3" => "f"
                ],
                "entity3" => [
                    "e3var1" => "g",
                    "e3var2" => "h",
                    "e3var3" => "i"
                ]
            ]
        ];

        // expected
        $expected = [
            [
                "e1var1" => "a",
                "e1var2" => "b",
                "e1var3" => "c",
                "e2var1" => "d",
                "e2var2" => "e",
                "e2var3" => "f",
                "e3var1" => "g",
                "e3var2" => "h",
                "e3var3" => "i"
            ],
            [
                "e1var1" => "a",
                "e1var2" => "b",
                "e1var3" => "c",
                "e2var1" => "d",
                "e2var2" => "e",
                "e2var3" => "f",
                "e3var1" => "g",
                "e3var2" => "h",
                "e3var3" => "i"
            ]
        ];

        // ACT
        $actual = $this->viewBrowserModel->formatFileData($originalFileData);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * formatFileData: Test 2
     * Program
     */
    public function testFormatFileDataWhenProgramFieldIsIncluded() {
        // ARRANGE
        // input
        $originalFileData = [
            [
                "entity1" => [
                    "e1var1" => "a",
                    "e1var2" => "b",
                    "e1var3" => "c",
                    "program" => "x"
                ],
                "entity2" => [
                    "e2var1" => "d",
                    "e2var2" => "e",
                    "e2var3" => "f"
                ],
                "entity3" => [
                    "e3var1" => "g",
                    "e3var2" => "h",
                    "e3var3" => "i"
                ]
            ],
            [
                "entity1" => [
                    "e1var1" => "a",
                    "e1var2" => "b",
                    "e1var3" => "c",
                    "program" => "x"
                ],
                "entity2" => [
                    "e2var1" => "d",
                    "e2var2" => "e",
                    "e2var3" => "f"
                ],
                "entity3" => [
                    "e3var1" => "g",
                    "e3var2" => "h",
                    "e3var3" => "i"
                ]
            ]
        ];

        // expected
        $expected = [
            [
                "e1var1" => "a",
                "e1var2" => "b",
                "e1var3" => "c",
                "e2var1" => "d",
                "e2var2" => "e",
                "e2var3" => "f",
                "e3var1" => "g",
                "e3var2" => "h",
                "e3var3" => "i",
                "program_code_p" => "x"
            ],
            [
                "e1var1" => "a",
                "e1var2" => "b",
                "e1var3" => "c",
                "e2var1" => "d",
                "e2var2" => "e",
                "e2var3" => "f",
                "e3var1" => "g",
                "e3var2" => "h",
                "e3var3" => "i",
                "program_code_p" => "x"
            ]
        ];

        // ACT
        $actual = $this->viewBrowserModel->formatFileData($originalFileData);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * formatFileData: Test 3
     * Program Code
     */
    public function testFormatFileDataWhenProgramCodeFieldIsIncluded() {
        // ARRANGE
        // input
        $originalFileData = [
            [
                "entity1" => [
                    "e1var1" => "a",
                    "e1var2" => "b",
                    "e1var3" => "c",
                    "program_code" => "x"
                ],
                "entity2" => [
                    "e2var1" => "d",
                    "e2var2" => "e",
                    "e2var3" => "f"
                ],
                "entity3" => [
                    "e3var1" => "g",
                    "e3var2" => "h",
                    "e3var3" => "i"
                ]
            ],
            [
                "entity1" => [
                    "e1var1" => "a",
                    "e1var2" => "b",
                    "e1var3" => "c",
                    "program_code" => "x"
                ],
                "entity2" => [
                    "e2var1" => "d",
                    "e2var2" => "e",
                    "e2var3" => "f"
                ],
                "entity3" => [
                    "e3var1" => "g",
                    "e3var2" => "h",
                    "e3var3" => "i"
                ]
            ]
        ];

        // expected
        $expected = [
            [
                "e1var1" => "a",
                "e1var2" => "b",
                "e1var3" => "c",
                "e2var1" => "d",
                "e2var2" => "e",
                "e2var3" => "f",
                "e3var1" => "g",
                "e3var2" => "h",
                "e3var3" => "i",
                "program_code_s" => "x"
            ],
            [
                "e1var1" => "a",
                "e1var2" => "b",
                "e1var3" => "c",
                "e2var1" => "d",
                "e2var2" => "e",
                "e2var3" => "f",
                "e3var1" => "g",
                "e3var2" => "h",
                "e3var3" => "i",
                "program_code_s" => "x"
            ]
        ];

        // ACT
        $actual = $this->viewBrowserModel->formatFileData($originalFileData);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * paginate: Test 1
     */
    public function testPaginate() {
        // ARRANGE
        // input
        $fileData = [ 'a', 'b', 'c', 'd', 'e' ];

        // mocks
        $gridPageSize = 1;
        $gridCurrentPage = 3;

        $queryParams = [
            "page" => $gridCurrentPage
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "gm-fu-preview-grid-pjax",
                '_queryParams' => $queryParams
            ]
        );
        
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->viewBrowserModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // expected
        $expected = [
            "c"
        ];

        // ACT
        $actual = $this->viewBrowserModel->paginate($fileData,'gm-fu-preview-grid');

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * search: Test 1
     * 
     */
    public function testSearch()
    {
        // ARRANGE
        // inputs
        $fileUploadDbId = 1234;
        $programDbId = 567;

        // mocks
        $this->viewBrowserModel = $this->construct(
            $this->viewBrowserModel,
            [
                "config" => $this->configMock,
                "crop" => $this->cropMock,
                "program" => $this->programMock,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "person" => $this->personMock,
                "germplasmFileUpload" => $this->germplasmFileUploadMock
            ],
            [
                'getFileUploadRecord' => function () { 
                    return [
                        [
                            "fileName" => "file001",
                            "fileStatus" => "validated",
                            "uploader" => "User, User",
                            "uploadTimestamp" => "2022",
                            "remarks" => "germplasm",
                            "fileData" => [
                                [
                                    "entity1" => [
                                        "e1var1" => "a",
                                        "e1var2" => "b",
                                        "e1var3" => "c"
                                    ],
                                    "entity2" => [
                                        "e2var1" => "d",
                                        "e2var2" => "e",
                                        "e2var3" => "f"
                                    ],
                                    "entity3" => [
                                        "e3var1" => "g",
                                        "e3var2" => "h",
                                        "e3var3" => "i"
                                    ]
                                ]
                            ]
                        ]
                    ]; 
                },
                'paginate' => function () {
                    return [
                        [
                            "entity1" => [
                                "e1var1" => "a",
                                "e1var2" => "b",
                                "e1var3" => "c"
                            ],
                            "entity2" => [
                                "e2var1" => "d",
                                "e2var2" => "e",
                                "e2var3" => "f"
                            ],
                            "entity3" => [
                                "e3var1" => "g",
                                "e3var2" => "h",
                                "e3var3" => "i"
                            ]
                        ]
                    ];
                },
                'getColumns' => function () {
                    return [ 'col1', 'col2' ];
                }
            ]
        );

        // expected
        $dataProvider = new ArrayDataProvider([
            'allModels' => [
                [
                    "e1var1" => "a",
                    "e1var2" => "b",
                    "e1var3" => "c",
                    "e2var1" => "d",
                    "e2var2" => "e",
                    "e2var3" => "f",
                    "e3var1" => "g",
                    "e3var2" => "h",
                    "e3var3" => "i"
                ]
            ],
            'restified' => true,
            'totalCount' => 1
        ]);
        $dataProvider->id = null;
        $dataProvider->sort = false;
        $expected = [
            'fileName' => 'file001',
            'fileStatus' => 'Validated',
            'uploader' => 'User, User',
            'uploadTimestamp' => '2022',
            'dataProvider' => $dataProvider,
            'columns' => [ 'col1', 'col2' ],
            'browserId' => 'dynagrid-gm-fu-preview-grid-create-germplasm',
            'action' => 'create',
            'type' => 'germplasm'
        ];
        
        // ACT
        $actual = $this->viewBrowserModel->search($fileUploadDbId, $programDbId);
        $actual["dataProvider"]->id = null;
        $actual["dataProvider"]->sort = false;

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGetProgramCodeHasCode() {
        $expected = 'IRSEA';

        $returnValue = [
            "cropProgramDbId" => 101,
            "programCode" => 'IRSEA'
        ];
        $this->viewBrowserModel->program->setReturnValue('getProgram', $returnValue);

        $actual = $this->viewBrowserModel->getProgramCode(101);

        $this->assertEquals($expected, $actual);
    }

    public function testGetProgramCodeNoCode() {
        $expected = '';

        $returnValue = [
            "cropProgramDbId" => 123,
            "programCode" => ''
        ];
        $this->viewBrowserModel->program->setReturnValue('getProgram', $returnValue);

        $actual = $this->viewBrowserModel->getProgramCode(101);

        $this->assertEquals($expected, $actual);
    }
}