<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\germplasm\models;

use app\modules\germplasm\models\FileExport;

class GermplasmFileExportTest extends \Codeception\Test\Unit
{
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Verify that constructing with an invalid file type throws an error
     */
    public function testInvalidFileType()
    {
        $this->expectException(\TypeError::class);
        $fileType = 'INVALID_FILE';
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);
    }

    /**
     * Verify that constructing with an invalid file format throws an error
     */
    public function testInvalidFileFormat()
    {
        $this->expectException(\TypeError::class);
        $fileType = FileExport::FILE_UPLOAD_TEMPLATE;
        $fileFormat = 'tsv';
        $ts = strtotime('2022-03-11T01:02:03');
        $ef = new FileExport($fileType, $fileFormat, $ts);
    }

    /**
     * Verify that the class formats a timestamp into YYYYMMDD_HHmmss
     */
    public function testFormatTimestamp()
    {
        $ts = strtotime('2022-03-11T01:02:03');
        $expected = '20220311_010203';
        $actual = FileExport::formatTimestamp($ts);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that the class returns all valid file types
     */
    public function testGetFileTypes()
    {
        $expected = ['File_Upload_Template'];
        $actual = FileExport::getFileTypes();
        $this->assertEqualsCanonicalizing($expected, $actual);
    }

    /**
     * Verify that the class returns all valid file formats
     */
    public function testGetFileFormats()
    {
        $expected = ['csv'];
        $actual = FileExport::getFileFormats();
        $this->assertEqualsCanonicalizing($expected, $actual);
    }

    /**
     * Verify that adding data with only column headers is valid
     */
    public function testAddFileData()
    {
        $fileType = FileExport::FILE_UPLOAD_TEMPLATE;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $gfe = new FileExport($fileType, $fileFormat, $ts);

        // number of content added
        $expected = 2;

        $content = [
            ['TEST_HEADER'],
            []
        ];
        $name = 'TEST_CSV_TEMPLATE.csv';

        $actual = $gfe->addFileData($content,$name);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that CSV file with only column headers returns corresponding file name and data
     */
    public function testGetUnzipped()
    {
        $fileType = FileExport::FILE_UPLOAD_TEMPLATE;
        $fileFormat = FileExport::CSV;
        $ts = strtotime('2022-03-11T01:02:03');
        $gfe = new FileExport($fileType, $fileFormat, $ts);

        $content = [
            ['TEST_HEADER'],
            []
        ];
        $name = 'TEST_CSV_TEMPLATE.csv';
        $gfe->addFileData($content,$name);

        $expected = [
            'TEST_CSV_TEMPLATE.csv' => [
                ['TEST_HEADER'],
                []
            ],
        ];
        $actual = $gfe->getUnzipped();

        $this->assertEquals($expected, $actual);
    }
}

?>