<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\germplasm\models;

use Yii;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsArray;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertNull;

class MergeModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;

    protected function _before()
    {
        $this->germplasmMergeModel = Yii::$container->get('germplasmMergeModel');
    }

    protected function _after()
    {
        \Yii::$app->session->set("ge-merge-variables-required", null);
        \Yii::$app->session->set("ge-merge-variables-optional", null);
    }

    // unit tests
    /**
     * Verify KEY_VARIABLES constant is returned
     */
    public function testGetKeyVariables() {
        $expected = 'ge-merge-variables';
        $actual = $this->germplasmMergeModel->getKeyVariables();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify KEY_VARIABLES_REQUIRED constant is returned
     */
    public function testGetKeyVariablesRequired() {
        $expected = 'ge-merge-variables-required';
        $actual = $this->germplasmMergeModel->getKeyVariablesRequired();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify KEY_VARIABLES_OPTIONAL constant is returned
     */
    public function testGetKeyVariablesOptional() {
        $expected = 'ge-merge-variables-optional';
        $actual = $this->germplasmMergeModel->getKeyVariablesOptional();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify FILE_UPLOAD_CONFIG_ABBREV constant is returned
     */
    public function testGetFileUploadConfigAbbrev() {
        $expected = 'GM_MERGE_GERMPLASM_';
        $actual = $this->germplasmMergeModel->getFileUploadConfigAbbrev();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify TEMPLATE_FILE_NAME constant is returned
     */
    public function testGetTemplateFileName() {
        $expected = 'GM_MERGE_GERMPLASM_TEMPLATE_DEFAULT';
        $actual = $this->germplasmMergeModel->getTemplateFileName();
        $this->assertEquals($expected, $actual);
    }

    public function testAddMergeConflictResolutionEmptyResolution() {
        $getResultMock = [
            "data" => [ 
                "germplasmDbId" => 123,
                "fileData" => []
            ],
            "status" => 200
        ];

        $updateResultMock = [
            "metadata" => [
                "pagination" => [
                    "pageSize" => 100,
                    "totalCount" => 1,
                    "currentPage" => 1,
                    "totalPages" => 1
                ],
                "status" => [
                    [
                        "message" => "Request has been successfully completed.",
                        "messageType" => "INFO"
                    ]
                ],
                "datafiles" => []
            ],
            "result" => [
                "data" => [
                    [
                        "germplasmDbId" => "123",
                        "recordCount" => 1,
                        "href" => "https://cbapi.local/v3/lists/123"
                    ]
                ]
            ]
        ];

        $this->germplasmMergeModel->germplasmFileUploadModel->setReturnValue('getOne',$getResultMock);
        $this->germplasmMergeModel->germplasmFileUploadModel->setReturnValue('updateOne',$updateResultMock);

        $expected = $updateResultMock;

        $fileId = 123;
        $matchingReports = [];

        $actual = $this->germplasmMergeModel->addMergeConflictResolution($fileId,$matchingReports);

        $this->assertEquals($expected, $actual);
    }

    public function testAddMergeConflictResolutionHasResolution() {
        $getResultMock = [
            "data" => [ 
                "germplasmDbId" => 123,
                "fileData" => [
                    [
                        "keep" => [ "designation" => "test designation" ],
                        "merge" => [ "designation" => "new designation" ],
                        "conflict" => [ 
                            "status" => "unresolved",
                            "items" => []
                        ]
                    ]
                ]
            ],
            "status" => 200
        ];

        $updateResultMock = [
            "metadata" => [
                "pagination" => [
                    "pageSize" => 100,
                    "totalCount" => 1,
                    "currentPage" => 1,
                    "totalPages" => 1
                ],
                "status" => [
                    [
                        "message" => "Request has been successfully completed.",
                        "messageType" => "INFO"
                    ]
                ],
                "datafiles" => []
            ],
            "result" => [
                "data" => [
                    [
                        "germplasmDbId" => "123",
                        "recordCount" => 1,
                        "href" => "https://cbapi.local/v3/lists/123"
                    ]
                ]
            ]
        ];

        $this->germplasmMergeModel->germplasmFileUploadModel->setReturnValue('getOne',$getResultMock);
        $this->germplasmMergeModel->germplasmFileUploadModel->setReturnValue('updateOne',$updateResultMock);

        $expected = $updateResultMock;

        $fileId = 123;
        $matchingReports = ["","designation"];

        $actual = $this->germplasmMergeModel->addMergeConflictResolution($fileId,$matchingReports);

        $this->assertEquals($expected, $actual);
    }
}
?>