<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\printouts\models;

class PrintoutsModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $printoutsModel;
    
    protected function _before()
    {
        $this->printoutsModel = \Yii::$container->get("printoutsModel");
    }

    protected function _after()
    {
    }

    // tests
    /**
     * getOccurrenceSelect2Data: Test 1
     */
    public function testGetOccurrenceSelect2Data()
    {
        // ARRANGE
        // input
        $occurrenceIds = [ 1, 2, 3 ];
        
        //mocks
        $returnValue = [
            "data" => [
                [
                    "occurrenceDbId" => "1",
                    "occurrenceName" => "OCC1"
                ],
                [
                    "occurrenceDbId" => "2",
                    "occurrenceName" => "OCC2"
                ],
                [
                    "occurrenceDbId" => "3",
                    "occurrenceName" => "OCC3"
                ]
            ],
            "totalCount" => 3
        ];
        $this->printoutsModel->occurrence->setReturnValue('searchAll', $returnValue);

        $expected = [
            "occurrences" => [
                "1" => "OCC1",
                "2" => "OCC2",
                "3" => "OCC3"
            ],
            "occurrenceCount" => 3
        ];

        // ACT
        $actual = $this->printoutsModel->getOccurrenceSelect2Data($occurrenceIds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}