<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\experimentCreation\models;

use Yii;

class BrowserModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $browserModel;
    
    protected function _before()
    {
        $this->browserModel = Yii::$container->get('browserModel');
    }

    protected function _after()
    {
    }

    // tests
    public function testFormatFilters1()
    {
        // ARRANGE
        // Input
        //TEST 1: Experiment browser without filters
        $params = [
            "year" => null,
            "owned" => "false",
            "site_id" => null,
            "stage_id" => null,
            "season_id" => null,
            "program_id" => 101
        ];

        $expected = [
            "owned" => "false",
            "programDbId" => "equals 101"
        ];

        // ACT
        $actual = $this->browserModel->formatFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }

    // tests
    public function testFormatFilters2()
    {
        // ARRANGE
        // Input
        //TEST 1: Experiment browser with filters (steward)
        $params = [
            "experimentStatus" => "",
            "experimentName" => "",
            "experimentType" => "",
            "occurrenceCount" => "",
            "entryCount" => "",
            "projectName" => "",
            "seasonName" => "",
            "stageCode" => "",
            "experimentYear" => "",
            "experimentDesignType" => "",
            "steward" => "Gallardo%",
            "creator" => "",
            "creationTimestamp" => "",
            "year" => null,
            "owned" => "false",
            "site_id" => null,
            "stage_id" => null,
            "season_id" => null,
            "program_id" => 101
        ];

        $expected = [
            "steward" => "Gallardo%",
            "owned" => "false",
            "programDbId" => "equals 101"
        ];

        // ACT
        $actual = $this->browserModel->formatFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);

    }
}