<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\experimentCreation\models;


use Yii;

class EntryOrderModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $entryOrderModel;

    protected function _before()
    {
        $this->entryOrderModel = Yii::$container->get('entryOrderModel');
    }

    protected function _after()
    {
    }

    // tests
    public function testConfirmDesignType1()
    {   
        // ARRANGE
        // Input
        $experimentBlockData[] = [
                "experimentBlockDbId" => 20,
                "experimentBlockCode" => "EXP0241-NB-1",
                "experimentBlockName" => "Nursery block 1",
                "experimentDbId" => 241,
                "experimentCode" => "EXP0241",
                "experimentName" => "ICN test#1",
                "experimentType" => "Intentional Crossing Nursery",
                "experimentYear" => 2021,
                "parentExperimentBlockDbId" => null,
                "parentExperimentBlockCode" => null,
                "parentExperimentBlockName" => null,
                "orderNumber" => 1,
                "blockType" => "parent",
                "noOfBlocks" => 0,
                "noOfRanges" => null,
                "noOfCols" => null,
                "noOfReps" => null,
                "plotNumberingOrder" => null,
                "startingCorner" => null,
                "entryListIdList" => null,
                "layoutOrderData" => null,
                "notes" => null,
                "creationTimestamp" => "2021-03-09T07:21:12.070Z",
                "creatorDbId" => 487,
                "creator" => "Gallardo, Larise",
                "modificationTimestamp" => null,
                "modifierDbId" => null,
                "modifier" => null,
                "entryCount" => 0,
                "plotCount" => "0",
                "rowgroup" => "1",
                "isActive" => false
        ];

        $designType = "design-entry-order";

        $experimentBlockCount = 1;

        $expected = false;

        // ACT
        $actual = $this->entryOrderModel->confirmDesignType($experimentBlockData, $experimentBlockCount, $designType);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testConfirmDesignType2()
    {   
        // ARRANGE
        // Input
        $entryListIdList[] = [
          "repno" => 1,
          "entryDbId" => 1184684
        ];
        $entryListIdList[] = [
          "repno" => 1
          ,"entryDbId" => 1184685
        ];

        $experimentBlockData[] = [
          "experimentBlockDbId" => 21,
          "experimentBlockCode" => "EXP0241-EO-1",
          "experimentBlockName" => "Entry order block 1",
          "experimentDbId" => 241,
          "experimentCode" => "EXP0241",
          "experimentName" => "ICN test#1",
          "experimentType" => "Intentional Crossing Nursery",
          "experimentYear" => 2021,
          "parentExperimentBlockDbId" => null,
          "parentExperimentBlockCode" => null,
          "parentExperimentBlockName" => null,
          "orderNumber" => 1,
          "blockType" => "parent",
          "noOfBlocks" => 0,
          "noOfRanges" => 0,
          "noOfCols" => 0,
          "noOfReps" => null,
          "plotNumberingOrder" => "Column serpentine",
          "startingCorner" => "Top Left",
          "entryListIdList" => $entryListIdList,
          "layoutOrderData" => null,
          "notes" => "[]",
          "creationTimestamp" => "2021-03-09T07:37:17.763Z",
          "creatorDbId" => 487,
          "creator" => "Gallardo, Larise",
          "modificationTimestamp" => "2021-03-09T07:37:22.301Z",
          "modifierDbId" => 487,
          "modifier" => "Gallardo, Larise",
          "entryCount" => 18,
          "plotCount" => "18",
          "rowgroup" => "1",
          "isActive" => false
        ];

        $designType = "design-add-blocks";

        $experimentBlockCount = 1;

        $expected = true;

        // ACT
        $actual = $this->entryOrderModel->confirmDesignType($experimentBlockData, $experimentBlockCount, $designType);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

}