<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\experimentCreation\models;

use app\dataproviders\ArrayDataProvider;
use Yii;

class PAEntriesModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $paEntriesModel;
    
    protected function _before()
    {
        $this->paEntriesModel = Yii::$container->get('paEntriesModel');
    }

    protected function _after()
    {
    }

    /**
     * Use this to access private or protected methods temporarily
     */
    protected static function callMethod($obj, $name, array $args) {
        $class = new \ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method->invokeArgs($obj, $args);
    }

    // tests
    public function testGenerateReplicates1(){

         // ARRANGE
        $entryListArray = [];

        $expected = [];

        // ACT
        $actual = self::callMethod(
            $this->paEntriesModel,
            'generateReplicates',
            array($entryListArray, [])
        );

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGenerateReplicates2(){

         // ARRANGE
        $entryListArray = [
            [
                "entryDbId" => 1111,
                "entryListDbId" => 14324,
                "entryNumber" => 1,
                "designation" => "Entry1",
                "replicate" => 1
            ],
            [
                "entryDbId" => 2222,
                "entryListDbId" => 14324,
                "entryNumber" => 2,
                "designation" => "Entry2",
                "replicate" => 1
            ],
            [
                "entryDbId" => 3333,
                "entryListDbId" => 14324,
                "entryNumber" => 3,
                "designation" => "Entry3",
                "replicate" => 1
            ]
        ];

        $expected = [
            "1111" => [
                "repno" => 1,
                "replicates" => [
                        "1" => null
                    ]
            ],
            "2222"=> [
                "repno" => 1,
                "replicates" => [
                        "1" => null
                    ]
            ],
            "3333"=> [
                "repno" => 1,
                "replicates" => [
                        "1" => null
                    ]
            ],
        ];

        // ACT
        $actual = self::callMethod(
            $this->paEntriesModel,
            'generateReplicates',
            array($entryListArray, [])
        );

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    public function testGenerateWorkingList(){
        $experimentId = 10;
        $entryListId = 123;

        $entryData = [
            "success" => true,
            "message" => "Request has been successfully completed.",
            "data" => [
                [
                    "entryDbId" => 1,
                    "entryListDbId" => 123,
                    "entryNumber" => 1,
                    "designation" => "Entry1"
                ],
                [
                    "entryDbId" => 2,
                    "entryListDbId" => 123,
                    "entryNumber" => 2,
                    "designation" => "Entry2"
                ],
                [
                    "entryDbId" => 3,
                    "entryListDbId" => 123,
                    "entryNumber" => 3,
                    "designation" => "Entry3"
                ]
            ],
            "totalCount" => 3,
            "status" => 200,
            "totalPages" => 1
        ];

        $entryReplicates = [
            "1" => [
                "repno" => 1,
                "replicates" => [
                        "1" => null
                    ]
            ],
            "2"=> [
                "repno" => 1,
                "replicates" => [
                        "1" => null
                    ]
            ],
            "3"=> [
                "repno" => 1,
                "replicates" => [
                        "1" => null
                    ]
            ],
        ];

        $expected =  [
            "success" => true,
            "message" => "Request has been successfully completed.",
            "data" => [
                [
                    "entryDbId" => 1,
                    "entryListDbId" => 123,
                    "entryNumber" => 1,
                    "designation" => "Entry1",
                    "replicate" => 1
                ],
                [
                    "entryDbId" => 2,
                    "entryListDbId" => 123,
                    "entryNumber" => 2,
                    "designation" => "Entry2",
                    "replicate" => 1
                ],
                [
                    "entryDbId" => 3,
                    "entryListDbId" => 123,
                    "entryNumber" => 3,
                    "designation" => "Entry3",
                    "replicate" => 1
                ]
            ],
            "totalCount" => 3,
            "status" => 200,
            "totalPages" => 1
        ];

        // ACT
        $actual = self::callMethod(
            $this->paEntriesModel,
            'generateWorkingList',
            array($experimentId, $entryListId, $entryData, $entryReplicates)
        );

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}