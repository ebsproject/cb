<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\experimentCreation\models;

use app\dataproviders\ArrayDataProvider;
use Yii;

class PackageModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $packageModel;
    
    protected function _before()
    {
        $this->packageModel = Yii::$container->get('packageModel');
    }

    protected function _after()
    {
    }

    /**
     * Use this to access private or protected methods temporarily
     */
    protected static function callMethod($obj, $name, array $args) {
        $class = new \ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method->invokeArgs($obj, $args);
    }

    // tests

    /**
     * xfail Entry::searchRecords mock doesn't work
     */
    public function xtestGetEntriesWithEntryIds() {
        // ARRANGE
        $entryIds = '[10, 20]';
        $experimentId = 100;
        $expected = [
            'entryListDbId' => [10, 20],
            'germplasmDbId' => [100],
            'seedDbId' => [100, 200],
            'packageDbId' => null,
            'entryNumber' => [1, 2]
        ];
        // mock function
        $entryModelMock = $this->make('app\models\Entry', [
            'searchRecords' => $expected
        ]);

        // ACT
        $actual = self::callMethod(
            $this->packageModel,
            'getEntries',
            array($entryIds, $experimentId)
        );

        // ASSERT
        $entryModelMock->expects($this
            ->exactly(1))
            ->method('searchRecords')
            ->with([
                'fields' => 'entry.entry_list_id as entryListDbId|'.
                    'entry.id as entryDbId|'.
                    'entry.germplasm_id as germplasmDbId|'.
                    'entry.seed_id as seedDbId|'.
                    'package.id AS packageDbId|'.
                    'entry.entry_number as entryNumber',
                'entryDbId'=> 'equals 10|equals 20'
            ]);
        $this->assertEquals($expected, $actual);
    }

    public function xtestGetEntriesWithExperimentIdWithExistingEntryList() {

    }

    public function xtestGetEntriesWithExperimentIdWithNoEntryList() {

    }

    public function xtestGetPackagesWithFilter() {

    }

    public function xtestGetPackagesWithoutFilter() {

    }

    public function xtestGetFilterBulkPackageWithFilter() {
        // ARRANGE
        // Mock getEntries
        // Mock getPackages
    }

    public function xtestGetFilterBulkPackageWithoutFilter() {
        // ARRANGE
        // Mock getEntries
        // Mock getPackages
    }

    public function xtestGetFilterBulkRecords() {
        // Make sure size of dataProvider packages is the same as finalEntries count
    }

    /**
     * Verify that records returned below the limit of 500 should have partitionView flag to false
     */
    public function xtestGetFilterBulkRecordsBelowLimit() {

    }

    /**
     * Verify that records returned equal the limit of 500 should have partitionView flag to false
     */
    public function xtestGetFilterBulkRecordsEqualLimit() {

    }

    /**
     * Verify that records returned equal the limit of 500 should have partitionView flag to true
     * and the returned dataProvider should have length of 500
     */
    public function xtestGetFilterBulkRecordsAboveLimit() {

    }

    /**
     * Verify that the last value of entryNumber is updated
     * because records length > limit
     */
    public function testPartitionEntryNumberLastEntryChanged() {
        // ARRANGE
        $records = array();
        $record1 = [ 'entryNumber' => 10 ];
        $record2 = [ 'entryNumber' => 200 ];
        $records[] = $record2;
        $records[] = $record1;
        $records[] = $record1;
        $records[] = $record1;
        $records[] = $record2;
        $records[] = $record2;
        $records[] = $record1;
        $dataProvider = self::callMethod(
            $this->packageModel,
            'getDataProvider',
            array('testDataProviderId', $records)
        );

        $entryNumbers = [ 200 => 3, 10 => 4 ];
        $limit = 5;

        $expected = [ 10 => 4, 200 => 1 ];

        // ACT
        $actual = self::callMethod(
            $this->packageModel,
            'partition',
            array($dataProvider, $entryNumbers, $limit)
        );

        // ASSERT
        $this->assertEquals($actual['entryNumbers'], $expected);
    }

    /**
     * Verify that the last value of entryNumber is not updated
     * because records length < limit
     */
    public function testPartitionEntryNumberLastEntryNotChanged1() {
        // ARRANGE
        $records = array();
        $record1 = [ 'entryNumber' => 10 ];
        $record2 = [ 'entryNumber' => 200 ];
        $records[] = $record2;
        $records[] = $record1;
        $records[] = $record1;
        $records[] = $record1;

        $dataProvider = self::callMethod(
            $this->packageModel,
            'getDataProvider',
            array('testDataProviderId', $records)
        );

        $entryNumbers = [ 200 => 1, 10 => 3 ];
        $limit = 5;

        $expected = [ 10 => 3, 200 => 1 ];

        // ACT
        $actual = self::callMethod(
            $this->packageModel,
            'partition',
            array($dataProvider, $entryNumbers, $limit)
        );

        // ASSERT
        $this->assertEquals($actual['entryNumbers'], $expected);
    }

    /**
     * Verify that the last value of entryNumber is not updated
     * because records length == limit
     */
    public function testPartitionEntryNumberLastEntryNotChanged2() {
        // ARRANGE
        $records = array();
        $record1 = [ 'entryNumber' => 10 ];
        $record2 = [ 'entryNumber' => 200 ];
        $records[] = $record2;
        $records[] = $record1;
        $records[] = $record1;
        $records[] = $record1;
        $records[] = $record2;

        $dataProvider = self::callMethod(
            $this->packageModel,
            'getDataProvider',
            array('testDataProviderId', $records)
        );

        $entryNumbers = [ 200 => 2, 10 => 3 ];
        $limit = 5;

        $expected = [ 10 => 3, 200 => 2 ];

        // ACT
        $actual = self::callMethod(
            $this->packageModel,
            'partition',
            array($dataProvider, $entryNumbers, $limit)
        );

        // ASSERT
        $this->assertEquals($actual['entryNumbers'], $expected);
    }

    /**
     * Verify that the data provider value has been sliced up to the limit
     */
    public function testPartitionDataProviderChanged() {
        // ARRANGE
        $records = array();
        $record1 = [ 'entryNumber' => 10 ];
        $record2 = [ 'entryNumber' => 200 ];
        $records[] = $record2;
        $records[] = $record1;
        $records[] = $record1;
        $records[] = $record1;
        $records[] = $record2;
        $records[] = $record2;
        $records[] = $record1;
        $dataProvider = self::callMethod(
            $this->packageModel,
            'getDataProvider',
            array('testDataProviderId', $records)
        );

        $entryNumbers = [ 200 => 3, 10 => 4 ];
        $limit = 5;

        $expected = array();
        $expected[] = $record1;
        $expected[] = $record1;
        $expected[] = $record1;
        $expected[] = $record1;
        $expected[] = $record2;

        // ACT
        $actual = self::callMethod(
            $this->packageModel,
            'partition',
            array($dataProvider, $entryNumbers, $limit)
        );

        // ASSERT
        $this->assertEquals($actual['dataProvider']->allModels, $expected);
    }

    /**
     * Verify that the data provider value has not been sliced because limit
     * is less than the total number of data provider records
     */
    public function testPartitionDataProviderNotChanged() {
        // ARRANGE
        $records = array();
        $record1 = [ 'entryNumber' => 10 ];
        $record2 = [ 'entryNumber' => 200 ];
        $records[] = $record2;
        $records[] = $record1;
        $records[] = $record1;
        $records[] = $record1;

        $dataProvider = self::callMethod(
            $this->packageModel,
            'getDataProvider',
            array('testDataProviderId', $records)
        );

        $entryNumbers = [ 200 => 1, 10 => 3 ];
        $limit = 5;

        // ACT
        $actual = self::callMethod(
            $this->packageModel,
            'partition',
            array($dataProvider, $entryNumbers, $limit)
        );

        // ASSERT
        $this->assertEquals($actual['dataProvider']->allModels, $records);
    }

    public function testGetDataProvider() {
        // ARRANGE
        $id = 10;
        $records = [ 'record100', 200 ];

        $expected = new ArrayDataProvider([
            'allModels' => [ 'record100', 200 ],
            'key' => 'entryDbId',
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [ 'entryNumber' => SORT_ASC ],
                'attributes' => [
                    "entryNumber",
                    "packageQuantity",
                    "programName",
                    "seedManager",
                    "germplasmName",
                    "experimentName",
                    "experimentYear",
                    "experimentStageCode",
                    "experimentSeason",
                    "seedName",
                    "packageLabel",
                    "packageUnit",
                    "experimentType",
                ]
            ],
            'id' => 10
        ]);

        // ACT
        $actual = self::callMethod(
            $this->packageModel,
            'getDataProvider',
            array($id, $records)
        );

        // ASSERT
        $this->assertEquals($expected->allModels, $actual->allModels);
        $this->assertEquals($expected->key, $actual->key);
        $this->assertEquals($expected->pagination, $actual->pagination);
        $this->assertEquals($expected->sort, $actual->sort);
        $this->assertEquals($expected->id, $actual->id);
    }
}