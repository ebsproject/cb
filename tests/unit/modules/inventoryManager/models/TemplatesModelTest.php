<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\inventoryManager\models;

use Yii;

class TemplatesModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $templatesModel;
    
    protected function _before()
    {
        $this->templatesModel = Yii::$container->get('inventoryTemplatesModel');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * getCropCodeGivenProgramCode: Test 1
     */
    public function testGetCropCodeGivenProgramCode()
    {
        // ARRANGE
        // input
        $programCode = 'ABC';

        // mocks
        $this->templatesModel->program->setReturnValue(
            "searchAll",
            [
                "data" => [
                    [
                        "cropProgramDbId" => "123"
                    ]
                ]
            ]
        );

        $this->templatesModel->cropProgram->setReturnValue(
            "searchAll",
            [
                "data" => [
                    [
                        "cropCode" => "XY"
                    ]
                ]
            ]
        );

        // expected
        $expected = "XY";

        // ACT
        $actual = $this->templatesModel->getCropCodeGivenProgramCode($programCode);
        
        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getConfigurations: Test 1
     * The program has a specific configuration
     */
    public function testGetConfigurationsWhenProgramSpecificConfigExists() {
        // ASSERT
        // input
        $programCode = "ABC";
        $type = "seed-package";

        // mocks
        $this->templatesModel->program->setReturnValue(
            "searchAll",
            [
                "data" => [
                    [
                        "cropProgramDbId" => "123"
                    ]
                ]
            ]
        );

        $this->templatesModel->cropProgram->setReturnValue(
            "searchAll",
            [
                "data" => [
                    [
                        "cropCode" => "XY"
                    ]
                ]
            ]
        );

        $this->templatesModel->configurations->setReturnValue('getConfigByAbbrev', [
            'return_vals' => [
                [
                    "values" => [
                        "a", "b", "c"
                    ]
                ],
                [
                    "values" => [
                        "d", "e", "f"
                    ]
                ],
                [
                    "values" => [
                        "g", "h", "i"
                    ]
                ],
            ]
        ]);

        $this->templatesModel->user->setReturnValue(
            "isAdmin",
            false
        );


        // expected
        $expected = [
            "a", "b", "c",
            "d", "e", "f"
        ];
        
        // ACT
        $actual = $this->templatesModel->getConfigurations($programCode, $type);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getConfigurations: Test 2
     * The program does not have a specific configuration.
     * Should return default instead.
     */
    public function testGetConfigurationsWhenProgramSpecificConfigDoesNotExist() {
        // ASSERT
        // input
        $programCode = "ABC";
        $type = "seed-package";

        // mocks
        $this->templatesModel->program->setReturnValue(
            "searchAll",
            [
                "data" => [
                    [
                        "cropProgramDbId" => "123"
                    ]
                ]
            ]
        );

        $this->templatesModel->cropProgram->setReturnValue(
            "searchAll",
            [
                "data" => [
                    [
                        "cropCode" => "XY"
                    ]
                ]
            ]
        );
        
        $this->templatesModel->configurations->setReturnValue('getConfigByAbbrev', [
            'return_vals' => [
                [
                    "values" => [
                        "a", "b", "c"
                    ]
                ],
                [ ],
                [
                    "values" => [
                        "g", "h", "i"
                    ]
                ],
            ]
        ]);

        $this->templatesModel->user->setReturnValue(
            "isAdmin",
            false
        );

        // expected
        $expected = [
            "a", "b", "c",
            "g", "h", "i"
        ];
        
        // ACT
        $actual = $this->templatesModel->getConfigurations($programCode, $type);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /** 
     * extractTemplateHeaders: Test 1
     */
    public function testExtractTemplateHeaders() {
        // ARRANGE
        // input
        $config = [
            [
                "entity" => "package",
                "abbrev" => "VAR_A"
            ],
            [
                "entity" => "package",
                "abbrev" => "VAR_B"
            ],
            [
                "entity" => "package",
                "abbrev" => "VAR_C"
            ],
            [
                "entity" => "seed",
                "abbrev" => "VAR_D"
            ],
            [
                "entity" => "seed",
                "abbrev" => "VAR_E"
            ],
            [
                "entity" => "seed",
                "abbrev" => "VAR_F"
            ]
        ];

        // expected
        $expected = [
            "VAR_D",
            "VAR_E",
            "VAR_F",
            "VAR_A",
            "VAR_B",
            "VAR_C"
        ];

        // ACT
        $actual = $this->templatesModel->extractTemplateHeaders($config);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getListData: Test 1
     * When template type is seed-package
     */
    public function testGetListDataWhenTemplateTypeIsSeedAndPackage() {
        // ARRANGE
        // input
        $listDbId = 1234;
        $templateType = $this->templatesModel::SEED_PACKAGE_TEMPLATE;
        $headers = [
            "GERMPLASM_CODE",
            "DESIGNATION",
            "SEED_NAME",
            "PACKAGE_LABEL"
        ];

        // mocks
        $this->templatesModel->listsModel->setReturnValue(
            "searchAllMembers",
            [
                "data" => [
                    [
                        "germplasmCode" => "GE0001",
                        "designation" => "ABC123"
                    ],
                    [
                        "germplasmCode" => "GE0002",
                        "designation" => "DEF456"
                    ]
                ]
            ]
        );

        // expected
        $expected = [
            ["GE0001","ABC123","",""],
            ["GE0002","DEF456","",""]
        ];

        // ACT
        $actual = $this->templatesModel->getListData($listDbId, $templateType, $headers);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getListData: Test 2
     * When template type is package
     */
    public function testGetListDataWhenTemplateTypeIsPackageOnly() {
        // ARRANGE
        // input
        $listDbId = 1234;
        $templateType = $this->templatesModel::PACKAGE_TEMPLATE;
        $headers = [
            "SEED_CODE",
            "SEED_NAME",
            "PACKAGE_LABEL",
            "PACKAGE_STATUS",
            "PACKAGE_UNIT"
        ];

        // mocks
        $this->templatesModel->listsModel->setReturnValue(
            "searchAllMembers",
            [
                "data" => [
                    [
                        "seedCode" => "SEED0001",
                        "seedName" => "12345"
                    ],
                    [
                        "seedCode" => "SEED0002",
                        "seedName" => "56789"
                    ]
                ]
            ]
        );

        // expected
        $expected = [
            ["SEED0001","12345","","",""],
            ["SEED0002","56789","","",""]
        ];

        // ACT
        $actual = $this->templatesModel->getListData($listDbId, $templateType, $headers);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}