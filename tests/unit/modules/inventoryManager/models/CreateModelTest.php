<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\inventoryManager\models;

use Yii;

class CreateModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $createModel;
    
    protected function _before()
    {
        $this->createModel = Yii::$container->get('inventoryCreateModel');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * Verify KEY_VARIABLES constant is returned
     */
    public function testGetKeyVariables() {
        $expected = 'im-variables';
        $actual = $this->createModel->getKeyVariables();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify KEY_VARIABLES_REQUIRED constant is returned
     */ 
    public function testGetKeyVariablesRequired() {
        $expected = 'im-variables-required';
        $actual = $this->createModel->getKeyVariablesRequired();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify KEY_VARIABLES_OPTIONAL constant is returned
     */ 
    public function testGetKeyVariablesOptional() {
        $expected = 'im-variables-optional';
        $actual = $this->createModel->getKeyVariablesOptional();
        $this->assertEquals($expected, $actual);
    }
}