<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\inventoryManager\models;

use Yii;

class InventoryFileUploadTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $inventoryFileUpload;
    
    protected function _before()
    {
        $this->inventoryFileUpload = Yii::$container->get('inventoryFileUpload');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * Verify that prefix string value is returned
     */
    public function testGetPrefix()
    {
        $expected = 'im-';
        $actual = $this->inventoryFileUpload->getPrefix();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Verify that string model name is returned
     */
    public function testGetModelName()
    {
        $expected = 'InventoryFileUpload';
        $actual = $this->inventoryFileUpload->getModelName();
        $this->assertEquals($expected, $actual);
    }
}