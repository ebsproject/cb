<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\inventoryManager\models;

use Yii;

class SeedTransferModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $seedTransferModel;
    
    protected function _before()
    {
        $this->seedTransferModel = Yii::$container->get('inventorySeedTransferModel');
    }

    protected function _after()
    {
    }

    /**
     * getStatusOptions: Test 1
     */
    public function testGetStatusOptions()
    {
        // ARRANGE
        // mocks
        $this->seedTransferModel->variable->setReturnValue(
            "searchAll",
            [
                "data" => [
                    [
                        "variableDbId" => "123"
                    ]
                ]
            ]
        );

        $this->seedTransferModel->variable->setReturnValue(
            "getVariableScaleTags",
            [
                ["abc" => "ABC"]
            ]
        );

        $this->seedTransferModel->scaleValue->setReturnValue(
            "getVariableScaleValues",
            [
                [
                    "scaleValueStatus" => "show",
                    "value" => "abc",
                    "displayName" => "ABC"
                ],
                [
                    "scaleValueStatus" => null,
                    "value" => "xyz",
                    "displayName" => "XYZ"
                ]
            ]
        );

        // expected
        $expected = [
            ["abc" => "ABC"]
        ];

        // ACT
        $actual = $this->seedTransferModel->getStatusOptions();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSearchFilters: Test 1
     * Ascending sort
     */
    public function testGetSearchFiltersAscendingSort() {
        // ARRANGE
        // input
        $params = [
            "sort" => "abcd",
            "_pjax" => "#dynagrid-im-seed-transfer-pjax",
            "dynagrid-im-seed-transfer-page" => "25"
        ];

        // mocks
        $this->seedTransferModel->userDashboardConfig->setReturnValue(
            "getDefaultPageSizePreferences",
            100
        );

        // expected
        $expected = "limit=100&page=25&sort=abcd";

        // ACT
        $actual = $this->seedTransferModel->getSearchFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSearchFilters: Test 2
     * Descending sort
     */
    public function testGetSearchFiltersDescendingSort() {
        // ARRANGE
        // input
        $params = [
            "sort" => "-efgh",
            "_pjax" => "#dynagrid-im-seed-transfer-pjax",
            "dynagrid-im-seed-transfer-page" => "10"
        ];

        // mocks
        $this->seedTransferModel->userDashboardConfig->setReturnValue(
            "getDefaultPageSizePreferences",
            40
        );

        // expected
        $expected = "limit=40&page=10&sort=efgh:DESC";

        // ACT
        $actual = $this->seedTransferModel->getSearchFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSearchParams: Test 1
     * With filters
     */
    public function testGetSearchParamsWithFilters() {
        // ARRANGE
        // input
        $params = [
            "SeedTransferModel" => [
                "abcd" => "early bird",
                "efgh" => "worm%"
            ]
        ];
        $programIds = [123,456,789];

        // expected
        $expected = [
            "senderProgramDbId" => "[OR] 123|456|789",
            "receiverProgramDbId" => "[OR] 123|456|789",
            "abcd" => "equals early bird",
            "efgh" => "worm%"
        ];
        
        // ACT
        $actual = $this->seedTransferModel->getSearchParams($params, $programIds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getSearchParams: Test 2
     * Without filters
     */
    public function testGetSearchParamsWithoutFilters() {
        // ARRANGE
        // input
        $params = [
            "SeedTransferModel" => []
        ];
        $programIds = [123,456,789];

        // expected
        $expected = [
            "senderProgramDbId" => "[OR] 123|456|789",
            "receiverProgramDbId" => "[OR] 123|456|789"
        ];
        
        // ACT
        $actual = $this->seedTransferModel->getSearchParams($params, $programIds);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}