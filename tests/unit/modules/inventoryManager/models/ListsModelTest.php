<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\inventoryManager\models;

use Yii;
// Import data providers
use app\dataproviders\ArrayDataProvider;

class ListsModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $listsModel;
    
    protected function _before()
    {
        $this->listsModel = Yii::$container->get('inventoryListsModel');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * rules: Test 1
     */
    public function testRules()
    {
        // ARRANGE
        // expected
        $expected = [
            [['creatorDbId','entityId','listDbId','modifierDbId','shareCount'], 'integer'],
            [['creatorDbId','name','type'], 'required'],
            [['abbrev','creationTimestamp','creator','description','displayName','listUsage','memberCount','modificationTimestamp','modifier','name','notes','permission','remarks','status','stubType','type'], 'string'],
            [['creatorDbId','entityId','listDbId','modifierDbId','shareCount','abbrev','creationTimestamp','creator','description','displayName','listUsage','memberCount','modificationTimestamp','modifier','name','notes','permission','remarks','status','stubType','type'], 'safe'],
            [[], 'boolean']
        ];

        // ACT
        $actual = $this->listsModel->rules();

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getListAbbrev: Test 1
     */
    public function testGetListAbbrev() {
        // ARRANGE
        // input
        $listDbId = 1234;

        // mocks
        $userMock = \Yii::$container->get('userMock');
        $userDashboardConfigMock = Yii::$container->get('userDashboardConfigMock');
        $hmModelMock = \Yii::$container->get('harvestManagerModelMock');

        /**
         * Partially mock ListsModel.
         * Replace getOne's return value.
         */
        $this->listsModel = $this->construct($this->listsModel,
            [
                "user" => $userMock,
                'userDashboardConfig' => $userDashboardConfigMock,
                "hmModel" => $hmModelMock
            ], 
            [
            'getOne' => function () { 
                return [
                    "data" => [
                        "listDbId" => 1234,
                        "abbrev" => "ABC"
                    ]
                ]; 
            }
        ]);

        // expected
        $expected = "ABC";

        // ACT
        $actual = $this->listsModel->getListabbrev($listDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 1
     * Normal single value search
     */
    public function testAssembleBrowserFiltersWithPlainParameters() {
        // ARRANGE
        // input
        $params = [
            "ListsModel" => [
                "name" => "List A"
            ]
        ];

        // expected
        $expected = [
            "name" => "equals List A",
            "type" => "equals seed|equals package"
        ];

        // ACT
        $actual = $this->listsModel->assembleBrowserFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 2
     * With wildcard character
     */
    public function testAssembleBrowserFiltersWithWildcardCharacter() {
        // ARRANGE
        // input
        $params = [
            "ListsModel" => [
                "abbrev" => "L%S% A"
            ]
        ];

        // expected
        $expected = [
            "abbrev" => "L%S% A",
            "type" => "equals seed|equals package"
        ];

        // ACT
        $actual = $this->listsModel->assembleBrowserFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 3
     * With Timestamp filter
     */
    public function testAssembleBrowserFiltersWithTimestampFilter() {
        // ARRANGE
        // input
        $params = [
            "ListsModel" => [
                "creationTimestamp" => "2022-09-19"
            ]
        ];

        // expected
        $expected = [
            "creationTimestamp" => "2022-09-19%",
            "type" => "equals seed|equals package"
        ];

        // ACT
        $actual = $this->listsModel->assembleBrowserFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 4
     * With permission filter - OWNED
     */
    public function testAssembleBrowserFiltersWithPermissionFilterOwned() {
        // ARRANGE
        // input
        $params = [
            "ListsModel" => [
                "permission" => "owned"
            ]
        ];

        // mocks
        $this->listsModel->user->setReturnValue('getUserId', 123);

        // expected
        $expected = [
            "creatorDbId" => "equals 123",
            "type" => "equals seed|equals package"
        ];

        // ACT
        $actual = $this->listsModel->assembleBrowserFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 4
     * With permission filter - SHARED
     */
    public function testAssembleBrowserFiltersWithPermissionFilterShared() {
        // ARRANGE
        // input
        $params = [
            "ListsModel" => [
                "permission" => "shared"
            ]
        ];

        // mocks
        $this->listsModel->user->setReturnValue('getUserId', 123);

        // expected
        $expected = [
            "creatorDbId" => "not equals 123",
            "type" => "equals seed|equals package"
        ];

        // ACT
        $actual = $this->listsModel->assembleBrowserFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleBrowserFilters: Test 4
     * With type filter
     */
    public function testAssembleBrowserFiltersWithTypeFilter() {
        // ARRANGE
        // input
        $params = [
            "ListsModel" => [
                "type" => "germplasm"
            ]
        ];

        // expected
        $expected = [
            "type" => "equals germplasm"
        ];

        // ACT
        $actual = $this->listsModel->assembleBrowserFilters($params);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 1
     * Default sort applied, page not reset
     */
    public function testAssembleUrlParametersWhenDefaultSortingIsAppliedAndPageIsNotReset() {
        // ARRANGE
        // test variables 
        $gridId = 'im-template-lists-grid';

        // input
        $resetPage = false;

        // mocks
        $gridPageSize = 15;
        $gridCurrentPage = 4;
        $queryParams = [
            "page" => $gridCurrentPage
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "$gridId-pjax",
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->listsModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // expected
        $expected = 'limit=15&page=4&sort=creationTimestamp:DESC|listDbId:DESC';

        // ACT
        $actual = $this->listsModel->assembleUrlParameters($resetPage);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 2
     * Sort specified, page not reset
     */
    public function testAssembleUrlParametersWhenCustomSortingIsAppliedAndPageIsNotReset() {
        // ARRANGE
        // test variables 
        $gridId = 'dynagrid-im-template-lists-grid';

        // input
        $resetPage = false;

        // mocks
        $gridPageSize = 15;
        $gridCurrentPage = 4;
        $gridSort = 'attributeB|-attributeC';
        $queryParams = [
            "page" => $gridCurrentPage,
            "sort" => $gridSort
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "$gridId-pjax",
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->listsModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // expected
        $expected = 'limit=15&page=4&sort=attributeB:ASC|attributeC:DESC';

        // ACT
        $actual = $this->listsModel->assembleUrlParameters($resetPage);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 3
     * Default sort applied, page reset
     */
    public function testAssembleUrlParametersWhenDefaultSortingIsAppliedAndPageIsReset() {
        // ARRANGE
        // test variables 
        $gridId = 'im-template-lists-grid';

        // input
        $resetPage = true;

        // mocks
        $gridPageSize = 15;
        $gridCurrentPage = 4;
        $queryParams = [
            "page" => $gridCurrentPage
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "",
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->listsModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        // expected
        $expected = 'limit=15&page=1&sort=creationTimestamp:DESC|listDbId:DESC';
        $expectedPage = 1;

        // ACT
        $actual = $this->listsModel->assembleUrlParameters($resetPage);
        $actualGetParams = \Yii::$app->request->getQueryParams();
        $actualPage = $actualGetParams['page'];

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedPage, $actualPage);
    }

    /**
     * search: Test 1
     */
    public function testSearch() {
        // ARRANGE
        // test variables 
        $gridId = 'im-template-lists-grid';

        // input
        $params = ['program' => 'BW'];

        // mocks
        $listsSample = [];
        $listsSampleCount = count($listsSample);

        $userMock = \Yii::$container->get('userMock');
        $userDashboardConfigMock = Yii::$container->get('userDashboardConfigMock');
        $hmModelMock = \Yii::$container->get('harvestManagerModelMock');

        /**
         * Partially mock ListsModel.
         * Replace searchAll's return value.
         */
        $this->listsModel = $this->construct($this->listsModel,
            [
                "user" => $userMock,
                'userDashboardConfig' => $userDashboardConfigMock,
                "hmModel" => $hmModelMock
            ], 
            [
            'searchAll' => function () use ($listsSample, $listsSampleCount) { 
                return []; 
            }
        ]);

        $this->listsModel->user->setReturnValue('getUserId', 100);
        $this->listsModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', 15);
        $this->listsModel->hmModel->setReturnValue('browserReset', [
            "resetPage" => false,
            "resetFilters" => true
        ]);

        $gridCurrentPage = 4;
        $queryParams = [
            "page" => $gridCurrentPage
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_url' => "",
                '_queryParams' => $queryParams
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // expected
        $dataProvider = new ArrayDataProvider([
            'allModels' => $listsSample,
            'key' => 'listDbId',
            'restified' => true,
            'totalCount' => $listsSampleCount
        ]);
        $dataProvider->id = null;
        $dataProvider->sort = false;
        $expected = $dataProvider;

        // ACT
        $actual = $this->listsModel->search($params);
        $actual->id = null;
        $actual->sort = false;
 
        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}