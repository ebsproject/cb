<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\seedInventory\models;

use Yii;

use app\dataproviders\ArrayDataProvider;

class QueryParameterModelTest extends \Codeception\Test\Unit
{
/**
     * @var \UnitTester
     */
    protected $tester;
    protected $queryParameterModel;
    
    protected function _before()
    {
        $this->queryParameterModel = Yii::$container->get('queryParameterModel');
    }

    protected function _after()
    {
    }

    /**
     * Validate query parameter
     * @param mixed name type data
     * @return array api call return
     */
    public function testGetQueryParam(){
        // ARRANGE
        $expected = true;

        $name = "test params";
        $type = "filter";
        $data = '{"ids": "PROGRAM:101,EXPERIMENT_TYPE:Breeding Trial","texts": "PROGRAM:IRSEA,EXPERIMENT_TYPE:Breeding Trial"}';

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "result" => [
                    "data" => [
                        [
                            "data" => [
                                "ids"=>"PROGRAM:101,EXPERIMENT_TYPE:Breeding Trial",
                                "texts"=>"PROGRAM:IRSEA,EXPERIMENT_TYPE:Breeding Trial"
                            ],
                            "name"=>"test params",
                            "dataBrowserDbId"=>"find-seeds",
                            "type"=>"filter",
                            "configDbId"=>"123"
                        ]
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->queryParameterModel->getQueryParam($name,$type,$data);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Get empty query parameters
     *
     * @return Array $result Array of query parameters
     */
    public function testGetQueryParamsDataProviderEmptyResults(){
        // ARRANGE
        $userId = 470;
        $toolId = 'find seeds';

        $arrDataProvider = new ArrayDataProvider([
            'key'=>'configDbId',
            'allModels' => [],
            'sort' => [],
            'pagination' => false
        ]);
        $arrDataProvider->id = null;

        $expected = [
            'data' => $arrDataProvider, 
            'count' => 0
        ];
        
        // Create mock
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>0,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => []
                ]
            ]
        ];;

        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->queryParameterModel->getQueryParamsDataProvider($userId, $toolId);
        $actual["data"]->id = null;

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Get the query parameters
     *
     * @return Array $result Array of query parameters
     */
    public function testGetQueryParamsDataProviderWithResults(){
        // ARRANGE
        $userId = 470;
        $toolId = 'find seeds';

        $arrDataProvider = new ArrayDataProvider([
            'key'=>'configDbId',
            'allModels' => [
                ['configDbId'=>1, 'name'=>'test name', 'data'=>'testData']
            ],
            'sort' => [
                'attributes' => ['configDbId'],
                'defaultOrder' => [
                    'configDbId'=>SORT_DESC
                ]
            ],
            'pagination' => false
        ]);
        $arrDataProvider->id = null;

        $expected = [
            'data' => $arrDataProvider, 
            'count' => 1
        ];
        
        // Create mock
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        ['configDbId'=>1, 'name'=>'test name', 'data'=>'testData']
                    ]
                ]
            ]
        ];;

        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->queryParameterModel->getQueryParamsDataProvider($userId, $toolId);
        $actual["data"]->id = null;

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Save query parameters
     *
     * @return Array $result Array of query parameters
     */
    public function testSaveQueryParam(){
        // ARRANGE
        $expected = 200;

        $queryParamRecord = [
            "userDbId"=>470,
            "name"=>"test params",
            "data"=>[
                "ids"=>"PROGRAM:101,EXPERIMENT_TYPE:Breeding Trial",
                "texts"=>"PROGRAM:IRSEA,EXPERIMENT_TYPE:Breeding Trial"
            ],
            "dataBrowserDbId"=>"find-seeds",
            "type"=>"filter"
        ];

        $apiReturnValue = [
            "status" => 200,
            "message" => "Successfully saved data"
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->queryParameterModel->saveQueryParam($queryParamRecord);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Get the query parameters by id
     *
     * @return Array $result Array of query parameters
     */
    public function testGetQueryParamsById(){
        // ARRANGE
        $expected = '"PROGRAM:101,EXPERIMENT_TYPE:Breeding Trial"';

        $paramId = 123;
        $userId = 470;
        $appTool = 'find-seeds';

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "result" => [
                    "data" => [
                        [
                            "data"=>[
                                "ids"=>'PROGRAM:101,EXPERIMENT_TYPE:Breeding Trial',
                                "texts"=>'PROGRAM:IRSEA,EXPERIMENT_TYPE:Breeding Trial'
                            ],
                            "name"=>"test params",
                            "dataBrowserDbId"=>"find-seeds",
                            "type"=>"filter",
                            "configDbId"=>"123"
                        ]
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->queryParameterModel->getQueryParamsById($paramId,$userId,$appTool);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Get the query parameters by id
     *
     * @return Array $result Array of query parameters
     */
    public function testGetQueryParamsByIdEmptyResults(){
        // ARRANGE
        $paramId = 123;
        $userId = 470;
        $appTool = 'find-seeds';

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "result" => [
                    "data" => []
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->queryParameterModel->getQueryParamsById($paramId,$userId,$appTool);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals('',$actual);
    }

    /**
     * Get the query parameters by id
     *
     * @return Array $result Array of query parameters
     */
    public function testGetQueryParamsByIdNoData(){
        // ARRANGE
        $paramId = 123;
        $userId = 470;
        $appTool = 'find-seeds';

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "result" => [
                    "data" => [
                        [
                            "data"=>[],
                            "name"=>"test params",
                            "dataBrowserDbId"=>"find-seeds",
                            "type"=>"filter",
                            "configDbId"=>"123"
                        ]
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->queryParameterModel->getQueryParamsById($paramId,$userId,$appTool);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals('[]',$actual);
    }

}
?>