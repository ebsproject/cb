<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\seedInventory\models;

use Yii;

class VariableFilterComponentTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $variableFilterComponent;

    protected $seedSearchParamsConfig;
    
    protected function _before()
    {
        $this->variableFilterComponent = Yii::$container->get('variableFilterComponent');

        $this->seedSearchParamsConfig = [
            "name"=> "Find Seeds Query Parameters",
            "main_table"=> "germplasm.package",
            "main_table_primary_key"=> "id",
            "api_resource_method"=> "POST",
            "api_resource_endpoint"=> "packages-search",
            "search_sort_column"=> "id",
            "search_sort_order"=> "ASC",
            "input_field_type"=> [
                [ 
                    "id"=>"name", 
                    "label"=>"GERMPLASM NAME", 
                    "description"=>"Designation and Germplasm Names information",
                    "validation"=>"string",
                    "api_body_param" => "designation"
                ],
                [
                    "id"=>"seed", 
                    "label"=>"SEED NAME", 
                    "description"=>"Seed Name information",
                    "validation"=>"string",
                    "api_body_param" => "seedName"
                ],
                [
                    "id"=>"label", 
                    "label"=>"PACKAGE LABEL", 
                    "description"=>"Package Label information",
                    "validation"=>"string",
                    "api_body_param" => "label"
                ]
            ],
            "values"=> [
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "IRSEA,BW,KE",
                    "allowed_values"=> "",
                    "api_body_param"=> "programDbId",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "PROGRAM",
                    "reference_column"=> "programDbId",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "program_name",
                    "api_resource_endpoint"=> "seed-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "program"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "seasonDbId",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "SEASON",
                    "reference_column"=> "seasonDbId",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "season_name",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "season"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentYear",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "experiment_year",
                    "variable_abbrev"=> "EXPERIMENT_YEAR",
                    "reference_column"=> "experimentYear",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_year",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "experiment"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentType",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "experiment_type",
                    "variable_abbrev"=> "EXPERIMENT_TYPE",
                    "reference_column"=> "experimentDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_type",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "experiment"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "stageDbId",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "EVALUATION_STAGE",
                    "reference_column"=> "stageDbId",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "stage_name",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "stage"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentDbId",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "EXPERIMENT_NAME",
                    "reference_column"=> "experimentDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "id",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_name",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "experiment"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "occurrenceDbId",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "OCCURRENCE_NAME",
                    "reference_column"=> "occurrenceDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "occurrence_name",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "occurrence"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "facility_class=> equals structure | facility_type=> equals building,equals seed warehouse,equals farm house",
                    "api_body_param"=> "facilityDbId",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "FACILITY",
                    "reference_column"=> "facility",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "facility_code",
                    "api_resource_endpoint"=> "seed-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "facility"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "facility_class=> not equals structure | facility_type=> not equals building,not equals seed warehouse,not equals farm house",
                    "api_body_param"=> "subFacilityDbId",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "SUB_FACILITY",
                    "reference_column"=> "subFacility",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "facility_name",
                    "api_resource_endpoint"=> "seed-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "subFacility"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "locationDbId",
                    "basic_parameter"=> "false",
                    "variable_abbrev"=> "LOCATION",
                    "reference_column"=> "locationDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "id",
                    "api_resource_method"=> "POST",
                    "output_id_value"=> "id",
                    "output_display_value"=> "location_code",
                    "secondary_display_value"=> "location_name",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "location"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "locationDbId",
                    "basic_parameter"=> "false",
                    "variable_abbrev"=> "LOCATION_NAME",
                    "reference_column"=> "locationDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "id",
                    "api_resource_method"=> "POST",
                    "output_id_value"=> "id",
                    "output_display_value"=> "location_name",
                    "secondary_display_value"=> "location_code",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table" => "location"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "input field",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "plotNumber",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "PLOTNO",
                    "reference_column"=> "sourcePlotDbId",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "plot_number",
                    "api_resource_endpoint"=> "",
                    "secondary_resource_endpoint"=> "",
                    "secondary_resource_endpoint_method"=> "",
                    "target_table" => "location_occurrence_group"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "input field",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "plotCode",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "PLOT_CODE",
                    "reference_column"=> "",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "plot_code",
                    "api_resource_endpoint"=> "",
                    "secondary_resource_endpoint"=> "",
                    "secondary_resource_endpoint_method"=> "",
                    "target_table" => "plot"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "input field",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "entryNumber",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "ENTNO",
                    "reference_column"=> "sourceEntryDbId",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "entry_number",
                    "api_resource_endpoint"=> "",
                    "secondary_resource_endpoint"=> "",
                    "secondary_resource_endpoint_method"=> "",
                    "target_table" => "entry"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "input field",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "entryCode",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "ENTCODE",
                    "reference_column"=> "sourceEntryDbId",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "entry_code",
                    "api_resource_endpoint"=> "",
                    "secondary_resource_endpoint"=> "",
                    "secondary_resource_endpoint_method"=> "",
                    "target_table" => "entry"
                ],
                [
                    "disabled"=> "true",
                    "required"=> "false",
                    "input_type"=> "single",
                    "input_field"=> "input field",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "harvestDate",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "",
                    "variable_abbrev"=> "HVDATE_CONT",
                    "reference_column"=> "harvestDate",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "",
                    "output_display_value"=> "",
                    "api_resource_endpoint"=> "",
                    "secondary_resource_endpoint"=> "",
                    "secondary_resource_endpoint_method"=> "",
                    "target_table" => "seed"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "single",
                    "input_field"=> "selection",
                    "default_value"=> "et=>equals (=), gt=>greater than (>), lt=>less than (<), gte=>greater than or equal to (>=), lte=>less than or equal to (<=)",
                    "allowed_values"=> "",
                    "api_body_param"=> "quantity",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "package_volume",
                    "variable_abbrev"=> "VOLUME",
                    "reference_column"=> "",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "package_volume",
                    "api_resource_endpoint"=> "seed-packages-search",
                    "secondary_resource_endpoint"=> "",
                    "secondary_resource_endpoint_method"=> "",
                    "target_table" => "package"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "single",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "unit",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "package_unit",
                    "variable_abbrev"=> "UNIT",
                    "reference_column"=> "",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "package_unit",
                    "api_resource_endpoint"=> "seed-packages-search",
                    "secondary_resource_endpoint"=> "",
                    "secondary_resource_endpoint_method"=> "",
                    "target_table" => "package"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "input field",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "replication",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "REP",
                    "reference_column"=> "",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "rep",
                    "api_resource_endpoint"=> "",
                    "secondary_resource_endpoint"=> "",
                    "secondary_resource_endpoint_method"=> "",
                    "target_table" => "plot"
                ],
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "input field",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "names",
                    "basic_parameter"=> "",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "NAME",
                    "reference_column"=> "",
                    "search_sort_order"=> "ASC",
                    "search_sort_column"=> "",
                    "api_resource_method"=> "",
                    "output_display_value"=> "",
                    "api_resource_endpoint"=> "",
                    "secondary_resource_endpoint"=> "",
                    "secondary_resource_endpoint_method"=> "",
                    "target_table" => ""
                ]
            ]
        ];
    }

    protected function _after()
    {
    }

    /**
     * Retrieve tool config
     * @return string program code
     */
    public function testGetConfig()
    {
        $config = [
            "name" => "Find Seeds Query Parameters",
            "main_table" => "germplasm.package",
            "main_table_alias" => "package",
            "main_table_primary_key" => "id",
            "api_resource_method" => "POST",
            "api_resource_endpoint" => "packages-search",
            "search_sort_column" => "id",
            "search_sort_order" => "ASC",
            "input_field_type" => [],
            "values"=> [
                [
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentDbId",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "EXPERIMENT_NAME",
                    "reference_column"=> "experimentDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "id",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_name",
                    "api_resource_endpoint"=> "experiments-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table"=> "experiment"
                ]
            ]
        ];

        $variableInfo = [
                'description'=> "Desired name for this experiment.",
                'label'=> "EXPERIMENT",
                'id'=>2247,
                'abbrev'=> "EXPERIMENT_NAME",
                'display_name'=> "Experiment Name",
                'status'=> "active"
        ];

        $expected = [
            'records'=>[
                [
                    "field_description"=> "Desired name for this experiment.",
                    "field_label"=> "EXPERIMENT",
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentDbId",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "EXPERIMENT_NAME",
                    "reference_column"=> "experimentDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "id",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_name",
                    "api_resource_endpoint"=> "experiments-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table"=> "experiment"
                ]
            ],
            'mainTableInfo' => [
                'main_table'=>'germplasm.package',
                'main_table_alias'=>'package'
            ]
        ];

        $this->variableFilterComponent->configModel->setReturnValue('getConfigByAbbrev',$config);
        $this->variableFilterComponent->variableModel->setReturnValue('getVariableByAbbrev',$variableInfo);
        $actual = $this->variableFilterComponent->getConfig();

        $this->assertEquals($expected,$actual);
    }

    /**
     * Validate retrieved variables
     */
    public function testValidateConfigVariables()
    {
        
        $records = [
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "experimentDbId",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "EXPERIMENT_NAME",
                "reference_column"=> "experimentDbId",
                "search_sort_order"=> "DESC",
                "search_sort_column"=> "id",
                "api_resource_method"=> "POST",
                "output_display_value"=> "experiment_name",
                "api_resource_endpoint"=> "experiments-search",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table"=> "experiment"
            ]
        ];

        $variableInfo = [
            'description'=> "Desired name for this experiment.",
            'label'=> "EXPERIMENT",
            'id'=>2247,
            'abbrev'=> "EXPERIMENT_NAME",
            'display_name'=> "Experiment Name",
            'status'=> "active"
        ];

        $expected = [
            [
                "field_description"=> "Desired name for this experiment.",
                "field_label"=> "EXPERIMENT",
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "experimentDbId",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "EXPERIMENT_NAME",
                "reference_column"=> "experimentDbId",
                "search_sort_order"=> "DESC",
                "search_sort_column"=> "id",
                "api_resource_method"=> "POST",
                "output_display_value"=> "experiment_name",
                "api_resource_endpoint"=> "experiments-search",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table"=> "experiment"
            ]
        ];

        $this->variableFilterComponent->variableModel->setReturnValue('getVariableByAbbrev',$variableInfo);

        $actual = $this->variableFilterComponent->validateConfigVariables($records);

        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve selection options
     */
    public function testRetrieveInputListConfig(){
        $expected = [
            'label'=>[
                'description' => [
                    'data-toggle'=> "tooltip",
                    'title'=> "Package Label information"
                ],
                'text'=>"PACKAGE LABEL - Package Label information",
                'validation'=>"string",
                'apiBodyParam'=>"label"
            ],
            'name'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Designation and Germplasm Names information"
                ],
                'text'=>"GERMPLASM NAME - Designation and Germplasm Names information",
                'validation'=>"string",
                'apiBodyParam'=>"designation"
            ],
            'seed'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Seed Name information"
                ],
                'text'=>"SEED NAME - Seed Name information",
                'validation'=>"string",
                'apiBodyParam'=>"seedName"
                
            ]
        ];

        $this->variableFilterComponent->configModel->setReturnValue('getConfigByAbbrev',$this->seedSearchParamsConfig);

        $actual = $this->variableFilterComponent->retrieveInputListConfig();

        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve config file from db for options in variable filter selection
     */
    public function testRetrieveRequiredFilters(){

        $variableInfo = [
                [
                    'description'=> "Season",
                    'label'=> "SEASON",
                    'id'=>315,
                    'abbrev'=> "SEASON",
                    'display_name'=> "Season",
                    'status'=> "active"
                ],
                [
                    'description'=> "Year when the experiment was evaluated.",
                    'label'=> "EXPERIMENT_YEAR",
                    'id'=>2339,
                    'abbrev'=> "EXPERIMENT_YEAR",
                    'display_name'=> "Experiment Year",
                    'status'=> "active"
                ],
                [
                    'description'=> "Experiment type",
                    'label'=> "EXPERIMENT",
                    'id'=>2253,
                    'abbrev'=> "EXPERIMENT_TYPE",
                    'display_name'=> "Experiment Type",
                    'status'=> "active"
                ],
                [
                    'description'=> "Occurrence Name",
                    'label'=> "EXPERIMENT",
                    'id'=>2351,
                    'abbrev'=> "OCCURRENCE_NAME",
                    'display_name'=> "Occurrence Name",
                    'status'=> "active"
                ],
                [
                    'description'=> "Facility where seeds, materials, and packages are being kept for storage or processing",
                    'label'=> "FACILITY",
                    'id'=>2265,
                    'abbrev'=> "FACILITY",
                    'display_name'=> "Facility",
                    'status'=> "active"
                ],
                [
                    'description'=> "Facility within a parent facility where seeds and materials are being stored",
                    'label'=> "SUB_FACILITY",
                    'id'=>2301,
                    'abbrev'=> "SUB_FACILITY",
                    'display_name'=> "Experiment Name",
                    'status'=> "active"
                ]
        ];

        $expected = ['SEASON','EXPERIMENT_YEAR','EXPERIMENT_TYPE','OCCURRENCE_NAME','FACILITY','SUB_FACILITY'];
        $actual = [];

        $this->variableFilterComponent->configModel->setReturnValue('getConfigByAbbrev',$this->seedSearchParamsConfig);
        
        for($i=0;$i<count($variableInfo);$i++){
            $this->variableFilterComponent->variableModel->setReturnValue('getVariableByAbbrev',$variableInfo[$i]);
            array_push($actual,$this->variableFilterComponent->retrieveRequiredFilters()[$i]);
        }
        
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve config file from db for options in variable filter selection
     */
    public function testRetrieveSelectionOptions(){
        $expected = [
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "stageDbId",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "EVALUATION_STAGE",
                "reference_column"=> "stageDbId",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "POST",
                "output_display_value"=> "stage_name",
                "api_resource_endpoint"=> "experiment-packages-search",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table" => "stage",
                "field_description"=> "Evaluation stage",
                "field_label"=> "Evaluation Stage"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "experimentDbId",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "EXPERIMENT_NAME",
                "reference_column"=> "experimentDbId",
                "search_sort_order"=> "DESC",
                "search_sort_column"=> "id",
                "api_resource_method"=> "POST",
                "output_display_value"=> "experiment_name",
                "api_resource_endpoint"=> "experiment-packages-search",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table" => "experiment",
                "field_description"=> "Experiment Name",
                "field_label"=> "Experiment Name"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "locationDbId",
                "basic_parameter"=> "false",
                "variable_abbrev"=> "LOCATION",
                "reference_column"=> "locationDbId",
                "search_sort_order"=> "DESC",
                "search_sort_column"=> "id",
                "api_resource_method"=> "POST",
                "output_id_value"=> "id",
                "output_display_value"=> "location_code",
                "secondary_display_value"=> "location_name",
                "api_resource_endpoint"=> "experiment-packages-search",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table" => "location",
                "field_description"=> "Location Code",
                "field_label"=> "Location Code"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "locationDbId",
                "basic_parameter"=> "false",
                "variable_abbrev"=> "LOCATION_NAME",
                "reference_column"=> "locationDbId",
                "search_sort_order"=> "DESC",
                "search_sort_column"=> "id",
                "api_resource_method"=> "POST",
                "output_id_value"=> "id",
                "output_display_value"=> "location_name",
                "secondary_display_value"=> "location_code",
                "api_resource_endpoint"=> "experiment-packages-search",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table" => "location",
                "field_description"=> "Location Name",
                "field_label"=> "Location Name"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "input field",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "plotNumber",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "PLOTNO",
                "reference_column"=> "sourcePlotDbId",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "POST",
                "output_display_value"=> "plot_number",
                "api_resource_endpoint"=> "",
                "secondary_resource_endpoint"=> "",
                "secondary_resource_endpoint_method"=> "",
                "target_table" => "location_occurrence_group",
                "field_description"=> "Plot Number",
                "field_label"=> "Plot Number"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "input field",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "plotCode",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "PLOT_CODE",
                "reference_column"=> "",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "POST",
                "output_display_value"=> "plot_code",
                "api_resource_endpoint"=> "",
                "secondary_resource_endpoint"=> "",
                "secondary_resource_endpoint_method"=> "",
                "target_table" => "plot",
                "field_description"=> "Plot Code",
                "field_label"=> "Plot Code"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "input field",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "entryNumber",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "ENTNO",
                "reference_column"=> "sourceEntryDbId",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "POST",
                "output_display_value"=> "entry_number",
                "api_resource_endpoint"=> "",
                "secondary_resource_endpoint"=> "",
                "secondary_resource_endpoint_method"=> "",
                "target_table" => "entry",
                "field_description"=> "Entry Number",
                "field_label"=> "Entry Number"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "input field",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "entryCode",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "ENTCODE",
                "reference_column"=> "sourceEntryDbId",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "POST",
                "output_display_value"=> "entry_code",
                "api_resource_endpoint"=> "",
                "secondary_resource_endpoint"=> "",
                "secondary_resource_endpoint_method"=> "",
                "target_table" => "entry",
                "field_description"=> "Entry Code",
                "field_label"=> "Entry Code"
            ],
            [
                "disabled"=> "true",
                "required"=> "false",
                "input_type"=> "single",
                "input_field"=> "input field",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "harvestDate",
                "basic_parameter"=> "false",
                "output_id_value"=> "",
                "variable_abbrev"=> "HVDATE_CONT",
                "reference_column"=> "harvestDate",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "",
                "output_display_value"=> "",
                "api_resource_endpoint"=> "",
                "secondary_resource_endpoint"=> "",
                "secondary_resource_endpoint_method"=> "",
                "target_table" => "seed",
                "field_description"=> "Harvest Date",
                "field_label"=> "Harvest Date"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "single",
                "input_field"=> "selection",
                "default_value"=> "et=>equals (=), gt=>greater than (>), lt=>less than (<), gte=>greater than or equal to (>=), lte=>less than or equal to (<=)",
                "allowed_values"=> "",
                "api_body_param"=> "quantity",
                "basic_parameter"=> "false",
                "output_id_value"=> "package_volume",
                "variable_abbrev"=> "VOLUME",
                "reference_column"=> "",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "POST",
                "output_display_value"=> "package_volume",
                "api_resource_endpoint"=> "seed-packages-search",
                "secondary_resource_endpoint"=> "",
                "secondary_resource_endpoint_method"=> "",
                "target_table" => "package",
                "field_description"=> "Volume",
                "field_label"=> "Volume"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "single",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "unit",
                "basic_parameter"=> "false",
                "output_id_value"=> "package_unit",
                "variable_abbrev"=> "UNIT",
                "reference_column"=> "",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "POST",
                "output_display_value"=> "package_unit",
                "api_resource_endpoint"=> "seed-packages-search",
                "secondary_resource_endpoint"=> "",
                "secondary_resource_endpoint_method"=> "",
                "target_table" => "package",
                "field_description"=> "Unit",
                "field_label"=> "Unit"
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "input field",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "replication",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "REP",
                "reference_column"=> "",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "POST",
                "output_display_value"=> "rep",
                "api_resource_endpoint"=> "",
                "secondary_resource_endpoint"=> "",
                "secondary_resource_endpoint_method"=> "",
                "target_table" => "plot",
                "field_description"=> "Replication",
                "field_label"=> "Replication"
            ]
        ];

        $variableInfo = [
            [
                'description'=> "Evaluation stage",
                'label'=> "Evaluation Stage",
                'id'=>315,
                'abbrev'=> "EVALUATION_STAGE",
                'display_name'=> "Evaluation Stage",
                'status'=> "active"
            ],
            [
                'description'=> "Experiment Name",
                'label'=> "Experiment Name",
                'id'=>315,
                'abbrev'=> "EXPERIMENT_NAME",
                'display_name'=> "Experiment Name",
                'status'=> "active"
            ],
            [
                'description'=> "Location Code",
                'label'=> "Location Code",
                'id'=>315,
                'abbrev'=> "LOCATION",
                'display_name'=> "Location Code",
                'status'=> "active"
            ],
            [
                'description'=> "Location Name",
                'label'=> "Location Name",
                'id'=>315,
                'abbrev'=> "LOCATION_NAME",
                'display_name'=> "Location Name",
                'status'=> "active"
            ],
            [
                'description'=> "Plot Number",
                'label'=> "Plot Number",
                'id'=>315,
                'abbrev'=> "PLOTNO",
                'display_name'=> "Plot Number",
                'status'=> "active"
            ],
            [
                'description'=> "Plot Code",
                'label'=> "Plot Code",
                'id'=>315,
                'abbrev'=> "PLOT_CODE",
                'display_name'=> "Plot Code",
                'status'=> "active"
            ],
            [
                'description'=> "Entry Number",
                'label'=> "Entry Number",
                'id'=>315,
                'abbrev'=> "ENTNO",
                'display_name'=> "Entry Number",
                'status'=> "active"
            ],
            [
                'description'=> "Entry Code",
                'label'=> "Entry Code",
                'id'=>315,
                'abbrev'=> "ENTCODE",
                'display_name'=> "Entry Code",
                'status'=> "active"
            ],
            [
                'description'=> "Harvest Date",
                'label'=> "Harvest Date",
                'id'=>315,
                'abbrev'=> "HVDATE_CONT",
                'display_name'=> "Harvest Date",
                'status'=> "active"
            ],
            [
                'description'=> "Volume",
                'label'=> "Volume",
                'id'=>315,
                'abbrev'=> "VOLUME",
                'display_name'=> "Volume",
                'status'=> "active"
            ],
            [
                'description'=> "Unit",
                'label'=> "Unit",
                'id'=>315,
                'abbrev'=> "UNIT",
                'display_name'=> "Unit",
                'status'=> "active"
            ],
            [
                'description'=> "Replication",
                'label'=> "Replication",
                'id'=>315,
                'abbrev'=> "REP",
                'display_name'=> "Replication",
                'status'=> "active"
            ]
        ];

        $this->variableFilterComponent->configModel->setReturnValue('getConfigByAbbrev',$this->seedSearchParamsConfig);
        
        $actual = [];
        for($i=0;$i<count($variableInfo);$i++){
            $this->variableFilterComponent->variableModel->setReturnValue('getVariableByAbbrev',$variableInfo[$i]);
            array_push($actual,$this->variableFilterComponent->retrieveSelectionOptions()[$i]) ;
        }

        $this->assertEquals($expected,$actual);
    }

    /**
     * Generate filter field html for given variables
     */
    public function xtestGenerateInputFields(){

    }

    /**
     * Extract variable values and configuration information for existing filters
     */
    public function xtestExtractVariableFilters(){

        // ARRANGE
        $expected = [
            [
                "abbrev"=>"PROGRAM",
                "config_info"=>[
                    "allowed_values"=> "",
                    "api_body_param"=> "programDbId",
                    "api_resource_endpoint"=> "seed-packages-search",
                    "api_resource_method"=> "POST",
                    "basic_parameter"=> "true",
                    "default_value"=> "",
                    "disabled"=> "false",
                    "field_description"=> "Program name",
                    "field_label"=> "PROGRAM",
                    "input_field"=> "selection",
                    "input_type"=> "multiple",
                    "output_display_value"=> "program_name",
                    "output_id_value"=> "id",
                    "reference_column"=> "programDbId",
                    "required"=> "false",
                    "search_sort_column"=> "display_value",
                    "search_sort_order"=> "ASC",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table"=> "program",
                    "variable_abbrev"=> "PROGRAM"
                ],
                "values"=>"101"
            ]
        ];
        
        $existingFilters = [
            ["name"=>"filter[PROGRAM][]", "value"=>"101"]
        ];
        $configVar = [
            [
                "allowed_values"=> "",
                "api_body_param"=> "programDbId",
                "api_resource_endpoint"=> "seed-packages-search",
                "api_resource_method"=> "POST",
                "basic_parameter"=> "true",
                "default_value"=> "",
                "disabled"=> "false",
                "field_description"=> "Program name",
                "field_label"=> "PROGRAM",
                "input_field"=> "selection",
                "input_type"=> "multiple",
                "output_display_value"=> "program_name",
                "output_id_value"=> "id",
                "reference_column"=> "programDbId",
                "required"=> "false",
                "search_sort_column"=> "display_value",
                "search_sort_order"=> "ASC",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table"=> "program",
                "variable_abbrev"=> "PROGRAM"
            ]
        ];
        $id = "program-filter";

        // ACTUAL
        $actual = $this->variableFilterComponent->extractVariableFilters($existingFilters,$configVar,$id);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);

    }

    /**
     * Generate variable filter field html
     */
    public function testGenerateFilterFields(){
        // ARRANGE
        // inputs
        $variableFilters = [];

        $returnValue = [];
        $this->variableFilterComponent->configModel->setReturnValue('getConfigByAbbrev',$returnValue);

        // expected
        $expected = [];

        // ACT
        $actual = $this->variableFilterComponent->generateFilterFields(104, $variableFilters, true, []);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }


    /**
     * Retrieve filter initial values;
     */
    public function xtestGetInitFilterValues(){

    }

    /**
	 * Generates fields params from existing variable filter values for search filter
	 */
    public function testGenerateSearchFieldsParams(){
        
        $expected = [
            'fields' => 'program.id AS programDbId|experiment.experiment_type AS experimentType|"experiment".id AS id|"experiment".experiment_type AS text',
            'distinctOn' => 'id',
            'experimentType' => 'equals Breeding Trial',
            'programDbId' => 'equals 101'
        ];

        $programData = [
            'data' => [
                [
                    'programDbId' => '101',
                    'programCode' => 'IRSEA',
                    'programName' => 'Irrigated Southeast Asia',
                    'description' => 'IRSEA'
                ]
            ]
        ];

        $userId = 470;
        $existingFilters = [
            [
                'abbrev' => "PROGRAM",
                'values' => "101",
                'config_info' => $this->seedSearchParamsConfig["values"][0]
            ],
            [
                'abbrev' => "EXPERIMENT_TYPE",
                'values' => "Breeding Trial",
                'config_info' => $this->seedSearchParamsConfig["values"][3]
            ]
        ];
        $targetFilter = [
            'variable_abbrev' => 'EXPERIMENT_TYPE',
            'allowed_values' => '',
            'target_table' => 'experiment',
            'output_id_value' => 'id',
            'output_display_value' => 'experiment_type'
        ];
        $dependencyApplicable = true;
        $q = '';

        $this->variableFilterComponent->userModel->setReturnValue('getUserId',$userId);
        $this->variableFilterComponent->programModel->setReturnValue('searchAll',$programData);

        $actual = $this->variableFilterComponent->generateSearchFieldsParams($userId,$existingFilters,$targetFilter,$dependencyApplicable,$q);

        $this->assertEquals($expected,$actual);
    }

    /**
	 * Retrieves matching and filtered package-level values using generated fields params
	 */
    public function testExtractSearchFilterValues(){
        $expected = [
            'data'=>[
                ["id"=>'Breeding Trial',"text"=>'Breeding Trial'],
                ["id"=>'Intentional Cross Nursery',"text"=>'Intentional Cross Nursery'],
                ["id"=>'Generation Nursery',"text"=>'Generation Nursery']  
            ],
            'count'=>3
        ];

        // ARRANGE
        $searchParams = [
            "fields" => "experiment.id AS id|experiment.experiment_type AS text",
            "distinctOn" => "id"
        ];

        $targetFilter = $this->seedSearchParamsConfig["values"][3];
        $resourceEndpoint = "seed-packages-search";
        $resourceMethod = "POST";
        $limit = 5;
        $offset = 0;

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>3,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        ["id"=>'Breeding Trial',"text"=>'Breeding Trial'],
                        ["id"=>'Intentional Cross Nursery',"text"=>'Intentional Cross Nursery'],
                        ["id"=>'Generation Nursery',"text"=>'Generation Nursery']                      
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACTUAL
        $actual = $this->variableFilterComponent->extractSearchFilterValues($searchParams,$targetFilter,$resourceEndpoint, $resourceMethod,$limit,$offset);
        
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }
  
    /**
     * Extract and format query params for storage
     */
    public function testExtractQueryParamsValuesForStorage(){
        // ARRANGE
        $expected = [
            'displayValues'=>'ATTRIBUTE1:Attribute Value 1::UNIT:Attribute Value 1::',
            'queryParamValues'=>[
                ['ATTRIBUTE1' => 'Attribute Value 1'],
                ['UNIT' => 'Attribute Value 1']
            ],
            'queryParamIds' => [
                ['ATTRIBUTE1' => 'Attribute Value 1'],
                ['UNIT' => 'attribute1']
            ]
        ];

        // mocks
        $params = [
            [
                'name' => 'ATTRIBUTE1',
                'value' => 'Attribute Value 1'
            ],
            [
                'name' => 'UNIT',
                'value' => 'attribute1'
            ]
        ];
        $configVars = [
            [
                'variable_abbrev' => 'ATTRIBUTE1',
                'input_field' => 'input field',
                'allowed_values' => ''
            ],
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "single",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "unit",
                "basic_parameter"=> "false",
                "output_id_value"=> "package_unit",
                "variable_abbrev"=> "UNIT",
                "reference_column"=> "",
                "search_sort_order"=> "ASC",
                "search_sort_column"=> "display_value",
                "api_resource_method"=> "POST",
                "output_display_value"=> "package_unit",
                "api_resource_endpoint"=> "seed-packages-search",
                "secondary_resource_endpoint"=> "",
                "secondary_resource_endpoint_method"=> "",
                "target_table" => "package",
                "field_description"=> "Unit",
                "field_label"=> "Unit"
            ]
        ];
        $inputListType = ['attribute1'];

        // apiCallHander() mocks
        $programData = [
            'programDbId' => 123,
            'programCode' => 'ATTRIBUTE1',
            'programName' => 'attribute1',
            'description' => 'Attribute 1'
        ];

        $this->variableFilterComponent->userModel->setReturnValue('getUserId',470);
        $this->variableFilterComponent->programModel->setReturnValue('searchAll',$programData);

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>3,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        ["id"=>'attributeVal1',"text"=>'Attribute Value 1']       
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        $this->variableFilterComponent->variableModel->setReturnValue('getVariableByAbbrev',["variableDbId" => 1]);
        $this->variableFilterComponent->variableModel->setReturnValue(
            'getVariableScales',
            ["scaleValues" => [
                ['value' => "attribute1", "displayName" => "Attribute Value 1"]
            ]]);

        // ACT
        $actual = $this->variableFilterComponent->extractQueryParamsValuesForStorage(470,$params,$configVars,$inputListType);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Calls necessary api call to handle search
     */
    public function testApiCallHandler(){
        // ARRANGE
        $expected = [
            'data'=>[
                ["id"=>'Breeding Trial',"text"=>'Breeding Trial'],
                ["id"=>'Intentional Cross Nursery',"text"=>'Intentional Cross Nursery'],
                ["id"=>'Generation Nursery',"text"=>'Generation Nursery']  
            ],
            'count'=>3
        ];

        $userId = 470;
        $variableFilters = [
            [
                'abbrev' => "PROGRAM",
                'values' => "101",
                'config_info' => $this->seedSearchParamsConfig["values"][0]
            ]
        ];
        $variableInfo = $this->seedSearchParamsConfig["values"][3];
        
        $limit = 5;
        $offset = 0;
        $q = "";

        $programData = [
            'programDbId' => 101,
            'programCode' => 'IRSEA',
            'programName' => 'Irrigated Southeast Asia',
            'description' => 'IRSEA'
        ];

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>3,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        ["id"=>'Breeding Trial',"text"=>'Breeding Trial'],
                        ["id"=>'Intentional Cross Nursery',"text"=>'Intentional Cross Nursery'],
                        ["id"=>'Generation Nursery',"text"=>'Generation Nursery']                      
                    ]
                ]
            ]
        ];

        $this->variableFilterComponent->userModel->setReturnValue('getUserId',$userId);
        $this->variableFilterComponent->programModel->setReturnValue('searchAll',$programData);

        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACTUAL
        $actual = $this->variableFilterComponent->apiCallHandler($userId,$variableFilters,$variableInfo,$limit,$offset,$q);
        
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Calls necessary api call to handle search
     */
    public function testApiCallHandlerEmptyReturn(){
        // ARRANGE
        $expected = ['data'=>[],'count'=>0];

        // ACTUAL
        $actual = $this->variableFilterComponent->apiCallHandler(470,[],[],100,0,'');
        
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Calls necessary api call to handle search
     */
    public function testApiCallHandlerWithDefaultValues(){
        // ARRANGE
        $expected = [
            'data'=>[
                ["id"=>'1',"text"=>'Breeding Trial'],
                ["id"=>'2',"text"=>'Intentional Cross Nursery']
            ],
            'count'=>2
        ];

        $variableFilters = [
            [
                'abbrev' => "PROGRAM",
                'values' => "101",
                'config_info' => $this->seedSearchParamsConfig["values"][0]
            ]
        ];

        $variableInfo = [
            "disabled"=> "false",
            "required"=> "true",
            "input_type"=> "multiple",
            "input_field"=> "selection",
            "default_value"=> "1:Breeding Trial,2:Intentional Cross Nursery",
            "allowed_values"=> "",
            "api_body_param"=> "experimentType",
            "basic_parameter"=> "true",
            "output_id_value"=> "experiment_type",
            "variable_abbrev"=> "EXPERIMENT_TYPE",
            "reference_column"=> "experimentDbId",
            "search_sort_order"=> "DESC",
            "search_sort_column"=> "display_value",
            "api_resource_method"=> "POST",
            "output_display_value"=> "experiment_type",
            "api_resource_endpoint"=> "experiment-packages-search",
            "secondary_resource_endpoint"=> "packages-search",
            "secondary_resource_endpoint_method"=> "POST",
            "target_table" => "experiment"
        ];

        $programData = [
            'programDbId' => 101,
            'programCode' => 'IRSEA',
            'programName' => 'Irrigated Southeast Asia',
            'description' => 'IRSEA'
        ];

        $this->variableFilterComponent->userModel->setReturnValue('getUserId',470);
        $this->variableFilterComponent->programModel->setReturnValue('searchAll',$programData);

        // ACTUAL
        $actual = $this->variableFilterComponent->apiCallHandler(470,$variableFilters,$variableInfo,100,0,'');
        
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }
    
    /**
     * Extract variable values and configuration information for existing filters
     */
    public function testExtractVariableFiltersEmpty(){
        // ACTUAL
        $actual = $this->variableFilterComponent->extractVariableFilters([],[],470);
        
        // ASSERT
        $this->assertEquals([],$actual);
    }

    /**
     * Extract variable values and configuration information for existing filters
     */
    public function testExtractVariableFilters(){
        // ARRANGE
        $expected = [
            [
                'abbrev' => 'ATTRIBUTE1',
                'values' => 'attribute1 value',
                'config_info' => [
                    'variable_abbrev' => 'ATTRIBUTE1'
                ]
            ]
        ];

        // mocks
        $existingFilters = [
            [
                'name' => '[ATTRIBUTE1]',
                'value' => 'attribute1 value'
            ]
        ];
        $configVar = [
            [
                'variable_abbrev' => 'ATTRIBUTE1'
            ]
        ];

        // ACTUAL
        $actual = $this->variableFilterComponent->extractVariableFilters($existingFilters,$configVar,470);
        
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve program filter options
     */
    public function testGetProgramFilterOptionsNoPrograms(){
        // ARRANGE
        $expected = [
            'results' => [],
            'totalCount' => 0
        ];

        // mocks
        $this->variableFilterComponent->userModel->setReturnValue('getUserId',470);
        $this->variableFilterComponent->userModel->setReturnValue('getUserPrograms',[]);

        // ACTUAL
        $actual = $this->variableFilterComponent->getProgramFilterOptions([],100,0);
        
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve program filter options
     */
    public function testGetProgramFilterOptionsNoSearchParams(){
        // ARRANGE
        $expected = [
            'results' => [
                ['id' => 101, 'text' => 'Irrigated South East Asia']
            ],
            'totalCount' => 2
        ];

        // mocks
        $userPrograms = [
            ['id' => 101, 'text' => 'Irrigated South East Asia'],
            ['id' => 102, 'text' => 'Wheat Breeding']
        ];

        $this->variableFilterComponent->userModel->setReturnValue('getUserId',470);
        $this->variableFilterComponent->userModel->setReturnValue('getUserPrograms',$userPrograms);

        // ACTUAL
        $actual = $this->variableFilterComponent->getProgramFilterOptions([],1,0);
        
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve program filter options
     */
    public function testGetProgramFilterOptionsTest1(){
        // ARRANGE
        $expected = [
            'results' => [
                ['id' => 101, 'text' => 'Irrigated South East Asia'],
                ['id' => 102, 'text' => 'Wheat Breeding']
            ],
            'totalCount' => 2
        ];

        // mocks
        $userPrograms = [
            ['id' => 101, 'text' => 'Irrigated South East Asia'],
            ['id' => 102, 'text' => 'Wheat Breeding']
        ];

        $searchParams = [
            'programDbId' => 'equals 101|equals 102'
        ];

        $this->variableFilterComponent->userModel->setReturnValue('getUserId',470);
        $this->variableFilterComponent->userModel->setReturnValue('getUserPrograms',$userPrograms);

        // ACTUAL
        $actual = $this->variableFilterComponent->getProgramFilterOptions($searchParams,1,0);
        
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve program filter options
     */
    public function testGetProgramFilterOptionsTest2(){
        // ARRANGE
        $expected = [
            'results' => [
                ['id' => 101, 'text' => 'Irrigated South East Asia']
            ],
            'totalCount' => 1
        ];

        // mocks
        $userPrograms = [
            ['id' => 101, 'text' => 'Irrigated South East Asia'],
            ['id' => 102, 'text' => 'Wheat Breeding']
        ];

        $searchParams = [
            'text' => 'Ir%'
        ];

        $this->variableFilterComponent->userModel->setReturnValue('getUserId',470);
        $this->variableFilterComponent->userModel->setReturnValue('getUserPrograms',$userPrograms);

        // ACTUAL
        $actual = $this->variableFilterComponent->getProgramFilterOptions($searchParams,1,0);
        
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }
}