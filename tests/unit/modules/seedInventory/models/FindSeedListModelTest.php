<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

class FindSeedListModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $seedSearchListModel;
    
    protected function _before()
    {
        $this->seedSearchListModel = \Yii::$container->get('seedSearchListModel');
    }

    protected function _after()
    {
    }

    /**
     * Add items to platform.list_member
     * @param table name list id user id
     */
    public function testAddListItems(){
        // ARRANGE
        $tableName = 'Find Seed Working List 470';
        $listId = 1234;
        $userId = 470;
        
        // mocks
        $searchResults = [
            "data" => [
                [
                    'listDbId'=>1234,
                    'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                    'name'=>'Find Seed Working List 470',
                    'displayName'=>'Find Seed Working List 470',
                    'listType'=>'package'  
                ]                  
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResults);

        $searchAllMembersResults = [
            "data" => [
                ['id'=>'1','displayName'=>'1','packageDbId'=>'1','label'=>'PKG1'],
                ['id'=>'2','displayName'=>'2','packageDbId'=>'2','label'=>'PKG2'],
                ['id'=>'3','displayName'=>'3','packageDbId'=>'3','label'=>'PKG3']
            ],
            "totalCount" => 3,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchAllMembersResults);

        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) { return 500; }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        $apiReturnValue = [
            "status" => 200,
            "body" => [],
                "result"=>[
                    "data" => [
                        [ 'listDbId'=> "1234", 'dataDbId'=> "1" ],
                        [ 'listDbId'=> "1234", 'dataDbId'=> "2" ],
                        [ 'listDbId'=> "1234", 'dataDbId'=> "3" ]  
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->addListItems($tableName, $listId, $userId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals(true,$actual);
    }

    /**
     * Check if the temp table is existing
     *
     * @param string $tableName name of the temp table
     *
     * @return boolean
     */
    public function testCheckIfTableExistWithoutCol(){
        // ARRANGE
        $expected = true;

        $tableName = 'FIND_SEED_WORKING_LIST_470';
        $userId = 470;
        $searchResults = [
            "data" => [
                'listDbId'=>123,
                'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                'name'=>'Find Seed Working List 470',
                'displayName'=>'Find Seed Working List 470',
                'listType'=>'package'                    
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResults);

        // ACT
        $actual = $this->seedSearchListModel->checkIfTableExistWithoutCol($tableName,$userId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);

    }

    /**
     * Check seeds if in working list
     *
     * @param mixed selected ids user id
     */
    public function testValidateSeedIds(){
        // ARRANGE
        $expected = [
            'newListMembers'=>[6,7,8],
            'duplicateListMembers'=>[]
        ];

        $selectedIds = [6,7,8];
        $userId = 470;
        $existingWorkingList = [
            'data'=>[
                ['packageDbId'=>'1','label'=>'1'],
                ['packageDbId'=>'2','label'=>'2'],
                ['packageDbId'=>'3','label'=>'3'],
                ['packageDbId'=>'4','label'=>'4'],
                ['packageDbId'=>'5','label'=>'5']
            ],
            'totalCount'=>5
        ];

        // ACT
        $actual = $this->seedSearchListModel->validateSeedIds($selectedIds, $userId,$existingWorkingList);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve package label and id
     * @param Array list items
     * @return Array package data
     */
    public function testRetrievePackageDataById(){
        // ARRANGE
        $expected = [
            [ 'displayValue'=> "1", 'id'=> "1" ],
            [ 'displayValue'=> "2", 'id'=> "2" ],
            [ 'displayValue'=> "3", 'id'=> "3" ],
            [ 'displayValue'=> "4", 'id'=> "4" ],
            [ 'displayValue'=> "5", 'id'=> "5" ]  
        ];

        $newListItems = [1,2,3,4,5];
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>4,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'packageDbId'=> "1", 'label'=> "1" ],
                        [ 'packageDbId'=> "2", 'label'=> "2" ],
                        [ 'packageDbId'=> "3", 'label'=> "3" ],
                        [ 'packageDbId'=> "4", 'label'=> "4" ],
                        [ 'packageDbId'=> "5", 'label'=> "5" ]                    
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->retrievePackageDataById($newListItems);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Insert values to the temporary table created
     * @param Array list items
     * @param Integer list id
     * @return Boolean insert status
     */
    public function testInsertSelectedItemsToWorkingList(){
        // ARRANGE
        $expected = true;

        $newListItems = [1,2,3,4];
        $listId = 1;

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>4,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ 'packageDbId'=> "1", 'label'=> "1" ],
                        [ 'packageDbId'=> "2", 'label'=> "2" ],
                        [ 'packageDbId'=> "3", 'label'=> "3" ],
                        [ 'packageDbId'=> "4", 'label'=> "4" ]                      
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // Create mock
        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) { return 500; }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        // ACT
        $actual = $this->seedSearchListModel->insertSelectedItemsToWorkingList($newListItems,$listId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);

    }

    /**
     * Insert selected items using a background job
     * @param Integer list id
     * @param Array search parameters 
     */
    public function xtestInsertItemsInBackground(){

    }

    /**
     * Insert values to the temporary table created
     * @param Insert values to the temporary table created
     * @param text $selectedIds selected studies
     */
    public function xtestInsertTempValuesFromSeedList(){

    }

    /**
     * Builds the prerequisite parameter for the processor
     * @param array $data contains the request data for the endpoint
     * @param integer $listId ID of the list
     * @param integer $totalCount total count of the records to be inserted
     */
    public function xtestInsertProcessorWithPrereq(){

    }

    /**
     * processor implementation for inserting package records to list
     * @param boolean api call status
     */
    public function testInsertProcessor(){
        // ARRANGE
        $expected = true;

        $newItemRecords=[
            [ 'displayValue'=> "1", 'id'=> "1" ],
            [ 'displayValue'=> "2", 'id'=> "2" ],
            [ 'displayValue'=> "3", 'id'=> "3" ],
            [ 'displayValue'=> "4", 'id'=> "4" ]
        ];
        $listId=1234;
        $prereq=[];
        $totalCount=4;    
        
        // Create mock
        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) { return 500; }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        $apiReturnValue = [
            "status" => 200,
            "body" => [],
                "result"=>[
                    "data" => [
                        [ 'listDbId'=> "1234", 'dataDbId'=> "1" ],
                        [ 'listDbId'=> "1234", 'dataDbId'=> "2" ],
                        [ 'listDbId'=> "1234", 'dataDbId'=> "3" ],
                        [ 'listDbId'=> "1234", 'dataDbId'=> "4" ]  
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->insertProcessor($newItemRecords,$listId,$prereq,$totalCount);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertIsBool($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * processor implementation for inserting package records to list via processor test 2
     * @param boolean api call status
     */
    public function testInsertProcessorFailedRequest(){
        // ARRANGE
        $newItemRecords=[
            [ 'displayValue'=> "1", 'id'=> "1" ],
            [ 'displayValue'=> "2", 'id'=> "2" ],
            [ 'displayValue'=> "3", 'id'=> "3" ],
            [ 'displayValue'=> "4", 'id'=> "4" ]
        ];
        
        // Create mock
        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) { return 500; }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData, $filter, $retrieveAll) {
                    return [
                        "status" => 400,
                        "body" => [
                            "result"=>[]
                        ]
                    ];
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->insertProcessor($newItemRecords,1234,[],4);

        // ASSERT
        $this->assertIsBool($actual);
        $this->assertEquals(false,$actual);
    }

    /**
     * processor implementation for inserting package records to list via processor test 3
     * @param boolean api call status
     */
    public function testInsertProcessorBgProcess(){
        // ARRANGE
        $newItemRecords=[
            [ 'displayValue'=> "1", 'id'=> "1" ],
            [ 'displayValue'=> "2", 'id'=> "2" ],
            [ 'displayValue'=> "3", 'id'=> "3" ],
            [ 'displayValue'=> "4", 'id'=> "4" ]
        ];
        
        // Create mock
        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) { return 2; }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData, $filter, $retrieveAll) {
                    return [ "status" => 200 ];
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->insertProcessor($newItemRecords,1234,[],4);

        // ASSERT
        $this->assertIsBool($actual);
        $this->assertEquals(true,$actual);
    }

    /**
     * processor implementation for inserting package records to list via processor test 4
     * @param boolean api call status
     */
    public function testInsertProcessorBgProcessEmptyPreReq(){
        // ARRANGE
        $newItemRecords=[
            [ 'displayValue'=> "1", 'id'=> "1" ],
            [ 'displayValue'=> "2", 'id'=> "2" ],
            [ 'displayValue'=> "3", 'id'=> "3" ],
            [ 'displayValue'=> "4", 'id'=> "4" ]
        ];
        
        // Create mock
        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) { return 2; }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData, $filter, $retrieveAll) {
                    return [ "status" => 200 ];
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->insertProcessor($newItemRecords,1234,['lists-search'],4);

        // ASSERT
        $this->assertIsBool($actual);
        $this->assertEquals(true,$actual);
    }

    /**
     * Delete values to the temporary table created
     * @param array $selectedRemoveIds selected studies
     */
    public function testDeleteTempValuesFromSeedList(){
        // ARRANGE
        $expected = true;

        $selectedRemoveIds = [2,3,4];
        $userId = 470;
        $itemCount = 3;

        // Create mock
        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) { return 500; }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        $deleteReturnValue = [
            "status" => 200,
            "totalCount"=>3
        ];

        $this->seedSearchListModel->listMemberModel->setReturnValue('deleteMany',$deleteReturnValue);

        // ACT
        $actual = $this->seedSearchListModel->deleteTempValuesFromSeedList($selectedRemoveIds, $userId,$itemCount);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Get working list browser data provider
     * @return mixed
     */
    public function xtestGetListDataProvider(){

    }

    /**
    * Retrieve saved lists of a certain type for working list
    * @param mixed user id list type
    * @param array data provider items count
    **/
    public function xtestGetSavedSeedListDataProvider(){

    }

    /**
    * Retrieve saved list items by id
    * @param mixed list id
    * @param array list items
    **/
    public function testGetSavedListByIdNoList(){
        // ARRANGE
        $expected = [
            'success'=>false,
            'error'=>"Internal server error.",
            'result'=>[]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false){
                    return [
                        "status" => 400,
                        "body" => [
                            "metadata"=>[
                                'status' => [
                                    ['message' => "Internal server error."]
                                ]
                            ],
                            "result"=>[
                                "data" => []
                            ]  
                        ],
                    ];
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->getSavedListById(1234);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
    * Retrieve saved list items by id
    * @param mixed list id
    * @param array list items
    **/
    public function testGetSavedListByIdHasList(){
        // ARRANGE
        $expected = [
            'success'=>true,
            'error'=>"",
            'result'=>["testMember1","testMember2","testMember3"]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false){
                    return [
                        "status" => 200,
                        "body" => [
                            "metadata"=>[],
                            "result"=>[
                                "data" => [
                                    ["designation"=>"testMember1"],
                                    ["designation"=>"testMember2"],
                                    ["designation"=>"testMember3"]
                                ]
                            ]  
                        ],
                    ];
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->getSavedListById(1234);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
    * Retrieve saved list information
    * @param mixed list id
    * @param array list data
    **/
    public function testGetSavedListData(){
        // ARRANGE
        $expected = [
            'success'=>true,
            'error'=>'',
            'result'=>['IRRI 123']
        ];
        
        $id = 1234;
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[

                ],
                "result"=>[
                    "data" => [
                        ['designation'=>'IRRI 123'] 
                    ]
                ]  
            ],
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->getSavedListById($id);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
    * Retrieve saved list information
    * @param mixed list id
    * @param array list data
    **/
    public function testGetSavedListDataNoData(){
        // ARRANGE
        $expected = [
            'success'=>true,
            'error'=>'',
            'result'=>[]
        ];
        
        $id = 1234;
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[

                ],
                "result"=>[
                    "data" => []
                ]  
            ],
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->getSavedListById($id);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
    * Retrieve saved list information
    * @param mixed list id
    * @param array list data
    **/
    public function testGetSavedListDataFailedRetrieval(){
        // ARRANGE
        $expected = [];
        
        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false){
                    return [
                        "status" => 400,
                        "body" => [
                            "metadata"=>[
                                'status' => [
                                    ['message' => "Internal server error."]
                                ]
                            ],
                            "result"=>[
                                "data" => []
                            ]  
                        ],
                    ];
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->getSavedListData(1234);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
    * Retrieve saved list items by id
    * @param mixed list id
    * @param array list items
    **/
    public function testGetSavedSeedListByIdNoList(){
        // ARRANGE
        $expected = [
            'data'=>[],
            'totalCount'=>0,
            'listId'=>1234,
            'listAbbrev'=>''
        ];
        // mocks
        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',["data" => []]);

        // ACT
        $actual = $this->seedSearchListModel->getSavedSeedListById(1234);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
    * Retrieve saved list items by id
    * @param mixed list id
    * @param array list items
    **/
    public function testGetSavedSeedListByIdHasListNoMembers(){
        // ARRANGE
        $expected = [
            'data'=>[],
            'totalCount'=>0,
            'listId'=>1234,
            'listAbbrev'=>'TEST_LIST'
        ];
        // mocks
        $searchResult = [
            "data" => [
                ['listDbId' => 1234, 'abbrev'=>'TEST_LIST']
            ]
        ];
        
        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResult);

        $searchAllMembersResults = [
            "data" => [],
            "totalCount" => 0,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchAllMembersResults);

        // ACT
        $actual = $this->seedSearchListModel->getSavedSeedListById(1234);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
    * Retrieve saved list items by id
    * @param mixed list id
    * @param array list items
    **/
    public function testGetSavedSeedListByIdHasListWithMembers(){
        // ARRANGE
        $expected = [
            'data'=>[
                ['id'=>'1','displayName'=>'1'],
                ['id'=>'2','displayName'=>'2'],
                ['id'=>'3','displayName'=>'3']
            ],
            'totalCount'=>3,
            'listId'=>1234,
            'listAbbrev'=>'TEST_LIST'
        ];
        // mocks
        $searchResult = [
            "data" => [
                ['listDbId' => 1234, 'abbrev'=>'TEST_LIST']
            ]
        ];
        
        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResult);

        $searchAllMembersResults = [
            "data" => [
                ['id'=>'1','displayName'=>'1'],
                ['id'=>'2','displayName'=>'2'],
                ['id'=>'3','displayName'=>'3']
            ],
            "totalCount" => 3,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchAllMembersResults);

        // ACT
        $actual = $this->seedSearchListModel->getSavedSeedListById(1234);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Save list basic information
     * @param array list info
     * @return mixed api call return
     */
    public function testSaveListBasicInformation(){
        // ARRANGE
        $expected = [
            "status" => 200,
            "body" => [
                "metadata"=>[],
                "result"=>[
                    "data" => [
                        [ 'listDbId'=> "1234", 'dataDbId'=> "1" ]
                    ]
                ]
            ],
        ];

        $listInfo = [
                'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                'name'=> 'Find Seed Working List 470',
                'description'=>'Find Seed Working List 470',
                'displayName'=>'Find Seed Working List 470',
                'remarks'=>'working list for Seed Search tool',
                'type'=> 'package'
        ];
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[],
                "result"=>[
                        "data" => [
                            [ 'listDbId'=> "1234", 'dataDbId'=> "1" ]
                    ]
                ]
            ],
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual= $this->seedSearchListModel->saveListBasicInformation($listInfo);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Removes the package IDs in the array if existing already in working list
     * @param Integer $listDbId ID of the working list
     * @param Array $idArray contains the package db ids
     * @return Array array of the package Ids that have been filtered out
     */
    public function testRemoveDuplicatesNoDuplicates(){
        // ARRANGE
        $expected = [];

        $searchAllMembersResult = [
            "data" => [
                ['id'=>'1','displayName'=>'1','packageDbId'=>'1','label'=>'PKG1'],
                ['id'=>'2','displayName'=>'2','packageDbId'=>'2','label'=>'PKG2'],
                ['id'=>'3','displayName'=>'3','packageDbId'=>'3','label'=>'PKG3']
            ],
            "totalCount" => 3,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue(
                                                                'searchAllMembers',
                                                                $searchAllMembersResult
                                                            );

        // ACT
        $actual = $this->seedSearchListModel->removeDuplicates(12, [1,2,3]);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Removes the package IDs in the array if existing already in working list
     * @param Integer $listDbId ID of the working list
     * @param Array $idArray contains the package db ids
     * @return Array array of the package Ids that have been filtered out
     */
    public function testRemoveDuplicatesFailedToRetrieveMembers(){
        // ARRANGE
        $expected = [1,2,3];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',null);

        // ACT
        $actual = $this->seedSearchListModel->removeDuplicates(12, [1,2,3]);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Removes the package IDs in the array if existing already in working list
     * @param Integer $listDbId ID of the working list
     * @param Array $idArray contains the package db ids
     * @return Array array of the package Ids that have been filtered out
     */
    public function testRemoveDuplicatesNoMembers(){
        // ARRANGE
        $expected = [1,2,3];

        $searchAllMembersResult = [
            "data" => [],
            "totalCount" => 0,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue(
                                                                'searchAllMembers',
                                                                $searchAllMembersResult
                                                            );

        // ACT
        $actual = $this->seedSearchListModel->removeDuplicates(12, [1,2,3]);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve working list items
     * @param integer user id
     * @return array data total count
     */
    public function testRetrieveWorkingList(){
        // ARRANGE
        $expected = [
            'data'=>[
                ['id'=>'1','displayName'=>'1','packageDbId'=>'1','label'=>'PKG1'],
                ['id'=>'2','displayName'=>'2','packageDbId'=>'2','label'=>'PKG2'],
                ['id'=>'3','displayName'=>'3','packageDbId'=>'3','label'=>'PKG3']
            ],
            'totalCount'=>3,
            'listId'=>1234,
            'listAbbrev'=>'FIND_SEED_WORKING_LIST_470', 
            'totalPageCount' =>1
        ];

        $userId = 470;
        $searchResults = [
            "data" => [
                [
                    'listDbId'=>1234,
                    'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                    'name'=>'Find Seed Working List 470',
                    'displayName'=>'Find Seed Working List 470',
                    'listType'=>'package'  
                ]                  
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResults);

        $searchAllMembersResults = [
            "data" => [
                ['id'=>'1','displayName'=>'1','packageDbId'=>'1','label'=>'PKG1'],
                ['id'=>'2','displayName'=>'2','packageDbId'=>'2','label'=>'PKG2'],
                ['id'=>'3','displayName'=>'3','packageDbId'=>'3','label'=>'PKG3']
            ],
            "totalCount" => 3,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchAllMembersResults);

        // ACT
        $actual = $this->seedSearchListModel->retrieveWorkingList($userId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Clear working list items
     * @param integer user id
     * @return boolean status
     */
    public function xtestClearWorkingList(){

    }

    /**
     * update list record information
     * @param mixed id params
     * @return array mixed
     */
    public function testUpdateListRecord(){
        // ARRANGE
        $expected = [
            'success'=>true,
            'error'=>'',
            'newName'=>'nameValue',
            'newListDbId'=>'1234'
        ];

        $updateOneResult = [
            "data" => [
                ['listDbId'=>'1234'] 
            ],
            "status" => 200,
            "message" => "Successfully updated record."
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('updateOne',$updateOneResult);

        $listDbId = 1234;
        $params = [
            "name" => "nameValue"
        ];

        // ACT
        $actual = $this->seedSearchListModel->updateListRecord($listDbId,$params);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * update list record information
     * @param mixed id params
     * @return array mixed
     */
    public function testUpdateListRecordFailedUpdate(){
        // ARRANGE
        $expected = [
            'success'=>false,
            'error'=>'Internal server error.',
            'newName'=>'',
            'newListDbId'=>''
        ];

        $updateOneResult = [
            "data" => [],
            "status" => 400,
            "message" => "Internal server error."
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('updateOne',$updateOneResult);

        $listDbId = 1234;
        $params = [
            "name" => "nameValue"
        ];

        // ACT
        $actual = $this->seedSearchListModel->updateListRecord($listDbId,$params);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Create working list
     * @param integer user id
     * @return array working list
     */
    public function testCreateWorkingList(){
        // ARRANGE
        $expected = [
            'data'=>[
                ['id'=>'1','displayName'=>'1','packageDbId'=>'1','label'=>'PKG1'],
                ['id'=>'2','displayName'=>'2','packageDbId'=>'2','label'=>'PKG2'],
                ['id'=>'3','displayName'=>'3','packageDbId'=>'3','label'=>'PKG3']
            ],
            'totalCount'=>3,
            'listId'=>1234,
            'listAbbrev'=>'FIND_SEED_WORKING_LIST_470', 
            'totalPageCount' =>1
        ];

        // mocks

        // checkIfTableExistWithoutCol
        $searchResults = [
            "data" => [
                'listDbId'=>1234,
                'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                'name'=>'Find Seed Working List 470',
                'displayName'=>'Find Seed Working List 470',
                'listType'=>'package'                    
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResults);

        // retrieveWorkingList
        $searchResults = [
            "data" => [
                [
                    'listDbId'=>1234,
                    'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                    'name'=>'Find Seed Working List 470',
                    'displayName'=>'Find Seed Working List 470',
                    'listType'=>'package'  
                ]                  
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResults);

        $searchAllMembersResults = [
            "data" => [
                ['id'=>'1','displayName'=>'1','packageDbId'=>'1','label'=>'PKG1'],
                ['id'=>'2','displayName'=>'2','packageDbId'=>'2','label'=>'PKG2'],
                ['id'=>'3','displayName'=>'3','packageDbId'=>'3','label'=>'PKG3']
            ],
            "totalCount" => 3,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchAllMembersResults);

        // ACT
        $actual = $this->seedSearchListModel->createWorkingList(470);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Create working list
     * @param integer user id
     * @return array working list
     */
    public function testCreateWorkingListNoExistingList(){
        // ARRANGE
        $expected = [
            'data'=>[],
            'totalCount'=>0,
            'listId'=>12,
            'listAbbrev'=>'FIND_SEED_WORKING_LIST_470', 
            'totalPageCount' =>0
        ];

        // mocks
        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',[]);

        $createReturn = [ 
            "status" => 200,
            "body" => [
                "result" => [
                    "data" => [
                        ["listDbId" => 12]
                    ]
                ]
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('create',$createReturn);

        // retrieveWorkingList
        $searchResults = [
            "data" => [
                [
                    'listDbId'=>12,
                    'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                    'name'=>'Find Seed Working List 470',
                    'displayName'=>'Find Seed Working List 470',
                    'listType'=>'package'  
                ]                  
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResults);

        $searchAllMembersResults = [
            "data" => [],
            "totalCount" => 0,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchAllMembersResults);

        // ACT
        $actual = $this->seedSearchListModel->createWorkingList(470);

        // ASSERT
        $this->assertNotNull($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Saves the items from the working list to a final list
     * @param Array $data contains the request data with the list ID and list name
     * @return Array contains whether the operation is succesfull with a notification message
     */
    public function testSaveListItems(){
        // ARRANGE
        $expected = [
            'success' => true,
            'message' => 'TEST LIST has no items.'
        ];

        $data = [
            'listId' => '123',
            'listName' => 'TEST LIST'
        ];
        $userId = 470;

        $tableName = 'FIND_SEED_WORKING_LIST_470';
        $userId = 470;
        $searchResults = [
            "data" => [
                [
                    'listDbId'=>123,
                    'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                    'name'=>'Find Seed Working List 470',
                    'displayName'=>'Find Seed Working List 470',
                    'listType'=>'package'  
                ]                  
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResults);

        $searchAllMembersResults = [
            "data" => [],
            "totalCount" => 0,
            "totalPages" => 0
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchAllMembersResults);

        // ACT
        $actual = $this->seedSearchListModel->saveListItems($data, $userId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Saves the items from the working list to a final list
     * @param Array $data contains the request data with the list ID and list name
     * @return Array contains whether the operation is succesfull with a notification message
     */
    public function testSaveListItemsHasRecords(){
        // ARRANGE
        $expected = [
            'success' => true,
            'message' => 'Successfully created TEST LIST!'
        ];

        // mocks
        $userId = 470;
        $data = [
            'listId' => '123',
            'listName' => 'TEST LIST'
        ];
        
        $searchResults = [
            "data" => [
                [
                    'listDbId'=>1234,
                    'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                    'name'=>'Find Seed Working List 470',
                    'displayName'=>'Find Seed Working List 470',
                    'listType'=>'package'  
                ]                  
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResults);

        $searchAllMembersResults = [
            "data" => [
                ['id'=>'1','displayName'=>'1','packageDbId'=>'1','label'=>'PKG1'],
                ['id'=>'2','displayName'=>'2','packageDbId'=>'2','label'=>'PKG2'],
                ['id'=>'3','displayName'=>'3','packageDbId'=>'3','label'=>'PKG3']
            ],
            "totalCount" => 3,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchAllMembersResults);

        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) { return 500; }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        $apiReturnValue = [
            "status" => 200,
            "body" => [],
                "result"=>[
                    "data" => [
                        [ 'listDbId'=> "1234", 'dataDbId'=> "1" ],
                        [ 'listDbId'=> "1234", 'dataDbId'=> "2" ],
                        [ 'listDbId'=> "1234", 'dataDbId'=> "3" ]  
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->seedSearchListModel->saveListItems($data, $userId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieves the total count of the items in a working list
     * @return Integer total record count
     */
    public function testGetItemsCount(){
        // ARRANGE
        $tableName = 'FIND_SEED_WORKING_LIST_470';
        $userId = 470;
        $searchResults = [
            "data" => [
                [
                    'listDbId'=>123,
                    'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                    'name'=>'Find Seed Working List 470',
                    'displayName'=>'Find Seed Working List 470',
                    'listType'=>'package'  
                ]                  
            ]
        ];

        $this->seedSearchListModel->listsModel->setReturnValue('searchAll',$searchResults);

        $searchAllMembersResults = [
            "data" => [
                ['id'=>'1','displayName'=>'1'],
                ['id'=>'2','displayName'=>'2'],
                ['id'=>'3','displayName'=>'3']
            ],
            "totalCount" => 3,
            "totalPages" => 1
        ];

        $this->seedSearchListModel->platformListMemberModel->setReturnValue('searchAllMembers',$searchAllMembersResults);

        // ACT
        $actual = $this->seedSearchListModel->getItemsCount();

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals(3,$actual);

    }
}
?>