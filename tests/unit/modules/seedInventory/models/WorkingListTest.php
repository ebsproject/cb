<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\seedInventory\models;

use app\dataproviders\ArrayDataProvider;

use Yii;

class WorkingListTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $workingListModel;
    
    protected function _before()
    {
        $this->workingListModel = Yii::$container->get('workingListModel');
    }

    protected function _after()
    {
    }

    /**
     * Retrievs the working list for find seeds
     * @param $userId integer ID of the current user
     * @return array contains the attribute of the working list
     */
    public function testGetWorkingList(){
        // ARRANGE
        $expected = [
            'listDbId'=>123,
            'abbrev'=>'FIND_SEED_WORKING_LIST_470',
            'name'=>'Find Seed Working List 470',
            'displayName'=>'Find Seed Working List 470',
            'listType'=>'package' 
        ];

        $userId = 470;
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [
                            'listDbId'=>123,
                            'abbrev'=>'FIND_SEED_WORKING_LIST_470',
                            'name'=>'Find Seed Working List 470',
                            'displayName'=>'Find Seed Working List 470',
                            'listType'=>'package' 
                        ]                     
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->workingListModel->getWorkingList($userId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieves the working list for find seeds test 2
     * @param $userId integer ID of the current user
     * @return array contains the attribute of the working list
     */
    public function testGetWorkingListTest2(){
        // ARRANGE
        $userId = 470;
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => []
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->workingListModel->getWorkingList($userId);

        // ASSERT
        $this->assertEquals([],$actual);
    }

    /**
     * Retrievs the current working list member
     * @param integer integer ID of the current working list
     * @return array contains list member data
     */
    public function testGetWorkingListMembers(){
        // ARRANGE
        $expected = [
            [ "displayValue"=> "1", "id"=> "1", "orderNumber" => "1" ],
            [ "displayValue"=> "2", "id"=> "2", "orderNumber" => "2" ],
            [ "displayValue"=> "3", "id"=> "3", "orderNumber" => "3" ],
            [ "displayValue"=> "4", "id"=> "4", "orderNumber" => "4" ],
            [ "displayValue"=> "5", "id"=> "5", "orderNumber" => "5" ]                          
        ];

        $listDbId = 123;
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>5,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ "displayValue"=> "1", "id"=> "1", "orderNumber" => "1" ],
                        [ "displayValue"=> "2", "id"=> "2", "orderNumber" => "2" ],
                        [ "displayValue"=> "3", "id"=> "3", "orderNumber" => "3" ],
                        [ "displayValue"=> "4", "id"=> "4", "orderNumber" => "4" ],
                        [ "displayValue"=> "5", "id"=> "5", "orderNumber" => "5" ]
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->workingListModel->getWorkingListMembers($listDbId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrievs the current working list member test 2
     * @param integer integer ID of the current working list
     * @return array contains list member data
     */
    public function testGetWorkingListMembersTest2(){
        // ARRANGE
        $expected = [
            [ "displayValue"=> "1", "id"=> "1", "orderNumber" => "1" ],
            [ "displayValue"=> "2", "id"=> "2", "orderNumber" => "2" ],
            [ "displayValue"=> "3", "id"=> "3", "orderNumber" => "3" ],
            [ "displayValue"=> "4", "id"=> "4", "orderNumber" => "4" ],
            [ "displayValue"=> "5", "id"=> "5", "orderNumber" => "5" ],
            [ "displayValue"=> "1", "id"=> "1", "orderNumber" => "1" ],
            [ "displayValue"=> "2", "id"=> "2", "orderNumber" => "2" ],
            [ "displayValue"=> "3", "id"=> "3", "orderNumber" => "3" ],
            [ "displayValue"=> "4", "id"=> "4", "orderNumber" => "4" ],
            [ "displayValue"=> "5", "id"=> "5", "orderNumber" => "5" ]                          
        ];

        $listDbId = 123;
        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>5,
                        "totalPages"=>2
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ "displayValue"=> "1", "id"=> "1", "orderNumber" => "1" ],
                        [ "displayValue"=> "2", "id"=> "2", "orderNumber" => "2" ],
                        [ "displayValue"=> "3", "id"=> "3", "orderNumber" => "3" ],
                        [ "displayValue"=> "4", "id"=> "4", "orderNumber" => "4" ],
                        [ "displayValue"=> "5", "id"=> "5", "orderNumber" => "5" ]
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->workingListModel->getWorkingListMembers($listDbId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Search method for find seeds working list
     * @param $listDbId integer ID of the working list
     */
    public function testSearch(){
        // ARRANGE
        $expected = new ArrayDataProvider([
            'allModels' => [
                [ "displayValue"=> "1", "id"=> "1", "orderNumber" => "1" ],
                [ "displayValue"=> "2", "id"=> "2", "orderNumber" => "2" ],
                [ "displayValue"=> "3", "id"=> "3", "orderNumber" => "3" ],
                [ "displayValue"=> "4", "id"=> "4", "orderNumber" => "4" ],
                [ "displayValue"=> "5", "id"=> "5", "orderNumber" => "5" ]
            ],
            'key' => null,
            'restified' => true,
            'totalCount' => 5,
            'id' => 'workinglist-results-dp'
        ]);

        $listDbId = 123;
        
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                'get' => function () {
                    return [
                        'workinglist-results-dp-page' => '1',
                        'sort' => 'attribute1|-attribute2'
                    ];
                },
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('request',$request);

        // Mock page limit
        $gridPageSize = 10;
        $this->workingListModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>5,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [ "displayValue"=> "1", "id"=> "1", "orderNumber" => "1" ],
                        [ "displayValue"=> "2", "id"=> "2", "orderNumber" => "2" ],
                        [ "displayValue"=> "3", "id"=> "3", "orderNumber" => "3" ],
                        [ "displayValue"=> "4", "id"=> "4", "orderNumber" => "4" ],
                        [ "displayValue"=> "5", "id"=> "5", "orderNumber" => "5" ]
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->workingListModel->search($listDbId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Search method for find seeds working list
     * @param $listDbId integer ID of the working list
     */
    public function testSearchNoList(){
        // ARRANGE
        $expected = new \yii\data\ArrayDataProvider([
            'allModels' => null,
            'key' => null,
            'id' => 'dp-54'
        ]);

        // ACT
        $actual = $this->workingListModel->search(null);

        // ASSERT
        $this->assertNotEmpty($actual);
    }
}
?>