<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use app\dataproviders\ArrayDataProvider;

class FindSeedDataBrowserModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $findSeedDataBrowserModel;
    
    protected function _before()
    {
        $this->findSeedDataBrowserModel = \Yii::$container->get('findSeedDataBrowserModel');
        $this->variableFilterComponent = Yii::$container->get('variableFilterComponent');
    }

    protected function _after()
    {
    }

    /**
     * Retrieve program code
     * @param array program value
     * @return string program code
     */
    public function testGetProgramCode()
    {
        $mockData = [
            'params' => [101,102,103],
            'data'=>[
                ['programCode'=>'IRSEA','programDbId'=>101],
                ['programCode'=>'BW','programDbId'=>102],
                ['programCode'=>'KE','programDbId'=>103]
            ]
        ];
        $expectedPrograms = 'IRSEA|BW|KE';
        
        $this->findSeedDataBrowserModel->programModel->setReturnValue('searchAll',['data'=>$mockData['data']]);
        $actualPrograms = $this->findSeedDataBrowserModel->getProgramCode($mockData['params']);
        $this->assertEquals($expectedPrograms,$actualPrograms);
    }

    /**
     * Retrieve columns from configuration dataset
     * @return array Array of configuration
     */
    public function testGetColumns(){
        // ARRANGE
        $expected = [
            [
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "experimentDbId",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "EXPERIMENT_NAME",
                "reference_column"=> "experimentDbId",
                "search_sort_order"=> "DESC",
                "search_sort_column"=> "id",
                "api_resource_method"=> "POST",
                "output_display_value"=> "experiment_name",
                "api_resource_endpoint"=> "experiments-search",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table"=> "experiment",
                "field_description"=> "Desired name for this experiment.",
                "field_label"=> "EXPERIMENT"
            ]
        ];

        $config = [
            "name" => "Find Seeds Query Parameters",
            "main_table" => "germplasm.package",
            "main_table_alias" => "package",
            "main_table_primary_key" => "id",
            "api_resource_method" => "POST",
            "api_resource_endpoint" => "packages-search",
            "search_sort_column" => "id",
            "search_sort_order" => "ASC",
            "input_field_type" => [],
            "values"=> [
                [
                    "field_label"=> "EXPERIMENT",
                    "disabled"=> "false",
                    "required"=> "false",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentDbId",
                    "basic_parameter"=> "false",
                    "output_id_value"=> "id",
                    "variable_abbrev"=> "EXPERIMENT_NAME",
                    "reference_column"=> "experimentDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "id",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_name",
                    "api_resource_endpoint"=> "experiments-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "target_table"=> "experiment"
                ]
            ]
        ];
        $this->findSeedDataBrowserModel->configModel->setReturnValue('getConfigByAbbrev',$config);

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>3,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [
                            'description'=> "Desired name for this experiment.",
                            'label'=> "EXPERIMENT",
                            'id'=>2247,
                            'abbrev'=> "EXPERIMENT_NAME",
                            'display_name'=> "Experiment Name",
                            'status'=> "active"
                        ]                      
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->findSeedDataBrowserModel->getColumns();

        // ASSERT
        $this->assertEquals($expected,$actual);
       
    }

    /**
     * Check and sort variables with existing mariable information in the database
     * @param array config variables
     * @return array config variables
     */
    public function testValidateColumnConfigVariables(){
        // ARRANGE
        $records = [
            [
                "field_label"=> "EXPERIMENT",
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "experimentDbId",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "EXPERIMENT_NAME",
                "reference_column"=> "experimentDbId",
                "search_sort_order"=> "DESC",
                "search_sort_column"=> "id",
                "api_resource_method"=> "POST",
                "output_display_value"=> "experiment_name",
                "api_resource_endpoint"=> "experiments-search",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table"=> "experiment"
            ]
        ];

        $expected = [
            [
                "field_description"=> "Desired name for this experiment.",
                "field_label"=> "EXPERIMENT",
                "disabled"=> "false",
                "required"=> "false",
                "input_type"=> "multiple",
                "input_field"=> "selection",
                "default_value"=> "",
                "allowed_values"=> "",
                "api_body_param"=> "experimentDbId",
                "basic_parameter"=> "false",
                "output_id_value"=> "id",
                "variable_abbrev"=> "EXPERIMENT_NAME",
                "reference_column"=> "experimentDbId",
                "search_sort_order"=> "DESC",
                "search_sort_column"=> "id",
                "api_resource_method"=> "POST",
                "output_display_value"=> "experiment_name",
                "api_resource_endpoint"=> "experiments-search",
                "secondary_resource_endpoint"=> "packages-search",
                "secondary_resource_endpoint_method"=> "POST",
                "target_table"=> "experiment"
            ]
        ];

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>3,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [
                            'description'=> "Desired name for this experiment.",
                            'label'=> "EXPERIMENT",
                            'id'=>2247,
                            'abbrev'=> "EXPERIMENT_NAME",
                            'display_name'=> "Experiment Name",
                            'status'=> "active"
                        ]                      
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->findSeedDataBrowserModel->validateColumnConfigVariables($records);

        // ASSERT
        $this->assertEquals($expected,$actual);

    }

    /**
     * Process variable filter values
     * @param array passed filters
     * @return array variable filter
     */
    public function testProcessFilters(){
        // ARRANGE
        $expected = [
            [
                "abbrev"=>"EXPERIMENT_TYPE",
                "values"=>["Breeding Trial","Generation Nursery"],
                "operator"=>"in",
                "filter_type"=>"search"
            ],
            [
                "abbrev"=>"includeSeedlist",
                "values"=>"1",
                "operator"=>"in",
                "filter_type"=>"search"
            ],
            [
                "abbrev"=>"userId",
                "values"=>"470",
                "operator"=>"in",
                "filter_type"=>"search"
            ]
        ];

        $filters = [
            [
                "value"=>"Breeding Trial,Generation Nursery",
                "name"=>"EXPERIMENT_TYPE"
            ],
            [
                "value"=>"1",
                "name"=>"includeSeedlist"
            ],
            [
                "value"=>"470",
                "name"=>"userId"
            ]
        ];

        $inputListTypes = [
            'label'=>[
                'description' => [
                    'data-toggle'=> "tooltip",
                    'title'=> "Package Label information"
                ],
                'text'=>"PACKAGE LABEL - Package Label information",
                'validation'=>"string",
                'apiBodyParam'=>"label"
            ],
            'name'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Designation and Germplasm Names information"
                ],
                'text'=>"GERMPLASM NAME - Designation and Germplasm Names information",
                'validation'=>"string",
                'apiBodyParam'=>"designation"
            ],
            'seed'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Seed Name information"
                ],
                'text'=>"SEED NAME - Seed Name information",
                'validation'=>"string",
                'apiBodyParam'=>"seedName"
                
            ]
        ];
        $this->findSeedDataBrowserModel->variableFilterComponent->setReturnValue('retrieveInputListConfig',$inputListTypes);

        // ACT
        $actual = $this->findSeedDataBrowserModel->processFilters($filters);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Process variable filter values test 2
     * @param array passed filters
     * @return array variable filter
     */
    public function testProcessFiltersTest2(){
        // ARRANGE
        $expected = [
            [
                "abbrev"=>"EXPERIMENT_TYPE",
                "values"=>["Breeding Trial","Generation Nursery"],
                "operator"=>"in",
                "filter_type"=>"search"
            ],
            [
                "abbrev"=>"includeSeedlist",
                "values"=>"1",
                "operator"=>"in",
                "filter_type"=>"search"
            ],
            [
                "abbrev"=>"userId",
                "values"=>"470",
                "operator"=>"in",
                "filter_type"=>"search"
            ],
            [
                "abbrev"=>"SOURCE_HARV_YEAR",
                "values"=>[2022],
                "operator"=>"in",
                "filter_type"=>"search"
            ]
        ];

        $filters = [
            [
                "value"=>"Breeding Trial,Generation Nursery",
                "name"=>"EXPERIMENT_TYPE"
            ],
            [
                "value"=>"1",
                "name"=>"includeSeedlist"
            ],
            [
                "value"=>"470",
                "name"=>"userId"
            ],
            [
                "value"=>"2022",
                "name"=>"SOURCE_HARV_YEAR"
            ]
        ];

        $inputListTypes = [
            'label'=>[
                'description' => [
                    'data-toggle'=> "tooltip",
                    'title'=> "Package Label information"
                ],
                'text'=>"PACKAGE LABEL - Package Label information",
                'validation'=>"string",
                'apiBodyParam'=>"label"
            ],
            'name'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Designation and Germplasm Names information"
                ],
                'text'=>"GERMPLASM NAME - Designation and Germplasm Names information",
                'validation'=>"string",
                'apiBodyParam'=>"designation"
            ],
            'seed'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Seed Name information"
                ],
                'text'=>"SEED NAME - Seed Name information",
                'validation'=>"string",
                'apiBodyParam'=>"seedName"
                
            ]
        ];
        $this->findSeedDataBrowserModel->variableFilterComponent->setReturnValue('retrieveInputListConfig',$inputListTypes);

        // ACT
        $actual = $this->findSeedDataBrowserModel->processFilters($filters);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Test search with empty return
     * 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function testSearchNoBrowserFilters(){
        // ARRANGE
        $expected = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
            'totalCount' => 0,
            'sort' => false,
            'pagination' => false,
            'id' => 'dynagrid-seed-search-search-grid'
        ]);

        $params = [];
        $dataFilters = [];
        $inputListOrder = false;

        // ACT
        $actual = $this->findSeedDataBrowserModel->search($params, $dataFilters, $inputListOrder);
    
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve search data with browser filters
     * 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function testSearchWithBrowserFilters(){
        // ARRANGE
        $expected = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
            'totalCount' => 0,
            'sort' => false,
            'pagination' => false,
            'id' => 'dynagrid-seed-search-search-grid'
        ]);

        $params = [
            'FindSeedDataBrowserModel' => [
                'gid' => 'SEED1',
                'label' => 'PACKAGE1',
                'source_harv_year' => '2019'
            ]
        ];
        $dataFilters = [];
        $inputListOrder = false;

        // ACT
        $actual = $this->findSeedDataBrowserModel->search($params, $dataFilters, $inputListOrder);
    
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Retrieve search dquery parameters
     * needed in query builder 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function testGetSearchData(){
        // ARRANGE
        $expected = [
            [
                "abbrev"=>"FACILITY",
                "values"=>["PGF"],
                "operator"=>"ilike any",
                "filter_type"=>"column"
            ]
        ];

        $params = [
            "FindSeedDataBrowserModel"=>[
                "facility"=>"PGF"
            ]
        ];
        $dataFilters = [];
        $inputListOrder = true;

        // ACT
        $actual = $this->findSeedDataBrowserModel->getSearchData($params, $dataFilters, $inputListOrder);
    
        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Test getDataProvider with no conditions
     * @return mixed
     */
    public function testGetDataProviderNoConditions(){
        // ARRANGE
        $condition = [];
        $condStr = null;
        $columns = null;
        $inputListOrder = false;
        $browserFilters = [];
        $getAllResponse = false;
        $getParamsOnly = false;

        $arrDataProvider = new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
            'totalCount' => 0,
            'sort' => false,
            'pagination' => false,
            'id' => 'dynagrid-seed-search-search-grid'
        ]);

        $totalCount = 0;
        $gridPageSize = 10;

        // Mock page limit
        $this->findSeedDataBrowserModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $expected = [
            'data' => $arrDataProvider,
            'count' => $totalCount
        ];

        // ACT
        $actual = $this->findSeedDataBrowserModel->getDataProvider(
                                                            $condition, 
                                                            $condStr, 
                                                            $columns, 
                                                            $inputListOrder, 
                                                            $browserFilters, 
                                                            $getAllResponse, 
                                                            $getParamsOnly);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Test getDataProvider with conditions no browserFilters
     * @return mixed
     */
    public function testGetDataProviderNoBrowserFilters(){
        // ARRANGE
        $condition = [
            [
                'abbrev' => "EXPERIMENT_YEAR",
                'filter_type' => "search",
                'operator' => "in",
                'values' => [2019]
            ],
            [
                'abbrev' => "EXPERIMENT_TYPE",
                'filter_type' => "search",
                'operator' => "in",
                'values' => ['Generation Nursery','Intentional Crossing Nursery']
            ],
            [
                'abbrev' => "userId",
                'filter_type' => "search",
                'operator' => "in",
                'values' => "470"
            ]
        ];
        $condStr = null;
        $columns = null;
        $inputListOrder = false;
        $browserFilters = [];
        $getAllResponse = false;
        $getParamsOnly = false;

        $config = [
            "name" => "Find Seeds Query Parameters",
            "main_table" => "germplasm.package",
            "main_table_alias" => "package",
            "main_table_primary_key" => "id",
            "api_resource_method" => "POST",
            "api_resource_endpoint" => "packages-search",
            "search_sort_column" => "id",
            "search_sort_order" => "ASC",
            "input_field_type" => [],
            "values"=> [
                [
                    "disabled" => "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "target_table"=> "experiment",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentType",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "experiment_type",
                    "variable_abbrev"=> "EXPERIMENT_TYPE",
                    "reference_column"=> "experimentDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_type",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "hidden"=> "false",
                    "field_label"=> "Experiment Type",
                    "order_number"=> "11",
                    "target_table"=> "experiment.experiment",
                    "target_column"=> "experiment_type",
                    "api_body_param"=> "experimentType",
                    "variable_abbrev"=> "EXPERIMENT_TYPE",
                    "reference_column"=> "source_experiment_id",
                    "additional_columns"=> "s.id as source_occurrence_id",
                    "target_table_alias"=> "el",
                    "secondary_target_table"=> "",
                    "secondary_target_column"=> "",
                    "secondary_target_table_alias"=> ""
                ],
                [
                    "disabled"=> "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "target_table"=> "experiment",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentYear",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "experiment_year",
                    "variable_abbrev"=> "EXPERIMENT_YEAR",
                    "reference_column"=> "experimentYear",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_year",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "hidden"=> "false",
                    "field_label"=> "Experiment Year",
                    "order_number"=> "10",
                    "target_table"=> "experiment.location",
                    "target_column"=> "experiment_year",
                    "api_body_param"=> "experimentYear",
                    "variable_abbrev"=> "EXPERIMENT_YEAR",
                    "reference_column"=> "source_experiment_id",
                    "additional_columns"=> "el.experiment_year as source_experiment_year",
                    "target_table_alias"=> "el",
                    "secondary_target_table"=> "",
                    "secondary_target_column"=> "",
                    "secondary_target_table_alias"=> ""
                ]
            ]
        ];
        $this->findSeedDataBrowserModel->configModel->setReturnValue('getConfigByAbbrev',$config);

        $inputListTypes = [
            'label'=>[
                'description' => [
                    'data-toggle'=> "tooltip",
                    'title'=> "Package Label information"
                ],
                'text'=>"PACKAGE LABEL - Package Label information",
                'validation'=>"string",
                'apiBodyParam'=>"label"
            ],
            'name'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Designation and Germplasm Names information"
                ],
                'text'=>"GERMPLASM NAME - Designation and Germplasm Names information",
                'validation'=>"string",
                'apiBodyParam'=>"designation"
            ],
            'seed'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Seed Name information"
                ],
                'text'=>"SEED NAME - Seed Name information",
                'validation'=>"string",
                'apiBodyParam'=>"seedName"
                
            ]
        ];
        $this->findSeedDataBrowserModel->variableFilterComponent->setReturnValue('retrieveInputListConfig',$inputListTypes);

        $data = [
            ['packageDbId' => 1, 'packageCode' => '1'],
            ['packageDbId' => 2, 'packageCode' => '2'],
            ['packageDbId' => 3, 'packageCode' => '3']
        ];

        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($data) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1,
                                    'totalCount' => 3,
                                    'currentPage' => 1,
                                ]
                            ],
                            'result' => [
                                'data' => $data
                            ]
                        ],
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        // Mock page limit
        $gridPageSize = 10;
        $this->findSeedDataBrowserModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $arrDataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'totalCount' => count($data),
            'restified' => true,
            'disableDefaultSort' => true,
            'id' => 'dynagrid-seed-search-search-grid'
        ]);

        $expected = [
            'data' => $arrDataProvider,
            'count' => count($data)
        ];

        // ACT
        $actual = $this->findSeedDataBrowserModel->getDataProvider(
                                                            $condition, 
                                                            $condStr, 
                                                            $columns, 
                                                            $inputListOrder, 
                                                            $browserFilters, 
                                                            $getAllResponse, 
                                                            $getParamsOnly);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }
    
    /**
     * Test getDataProvider with conditions no browserFilters test 2
     * @return mixed
     */
    public function testGetDataProviderNoBrowserFiltersTest2(){
        // ARRANGE
        $condition = [
            [
                'abbrev' => "DESIGNATION",
                'filter_type' => "search",
                'operator' => "in",
                'values' => ["DESIGNATION1"]
            ],
            [
                'abbrev' => "packageDbId",
                'filter_type' => "search",
                'operator' => "in",
                'values' => ['123,456']
            ],
            [
                'abbrev' => "workingListDbId",
                'filter_type' => "search",
                'operator' => "in",
                'values' => 890
            ],
            [
                'abbrev' => "multipleSelect",
                'filter_type' => "search",
                'operator' => "in",
                'values' => ['123,456']
            ],
            [
                'abbrev' => "germplasmDbId",
                'filter_type' => "search",
                'operator' => "in",
                'values' => ['1234567']
            ],
            [
                'abbrev' => "userId",
                'filter_type' => "search",
                'operator' => "in",
                'values' => "470"
            ]
        ];
        $condStr = null;
        $columns = null;
        $inputListOrder = false;
        $browserFilters = [];
        $getAllResponse = false;
        $getParamsOnly = false;

        $config = [
            "name" => "Find Seeds Query Parameters",
            "main_table" => "germplasm.package",
            "main_table_alias" => "package",
            "main_table_primary_key" => "id",
            "api_resource_method" => "POST",
            "api_resource_endpoint" => "packages-search",
            "search_sort_column" => "id",
            "search_sort_order" => "ASC",
            "input_field_type" => [],
            "values"=> [
                [
                    "disabled" => "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "target_table"=> "experiment",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentType",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "experiment_type",
                    "variable_abbrev"=> "EXPERIMENT_TYPE",
                    "reference_column"=> "experimentDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_type",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "hidden"=> "false",
                    "field_label"=> "Experiment Type",
                    "order_number"=> "11",
                    "target_table"=> "experiment.experiment",
                    "target_column"=> "experiment_type",
                    "api_body_param"=> "experimentType",
                    "variable_abbrev"=> "EXPERIMENT_TYPE",
                    "reference_column"=> "source_experiment_id",
                    "additional_columns"=> "s.id as source_occurrence_id",
                    "target_table_alias"=> "el",
                    "secondary_target_table"=> "",
                    "secondary_target_column"=> "",
                    "secondary_target_table_alias"=> ""
                ],
                [
                    "disabled"=> "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "target_table"=> "experiment",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentYear",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "experiment_year",
                    "variable_abbrev"=> "EXPERIMENT_YEAR",
                    "reference_column"=> "experimentYear",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_year",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "hidden"=> "false",
                    "field_label"=> "Experiment Year",
                    "order_number"=> "10",
                    "target_table"=> "experiment.location",
                    "target_column"=> "experiment_year",
                    "api_body_param"=> "experimentYear",
                    "variable_abbrev"=> "EXPERIMENT_YEAR",
                    "reference_column"=> "source_experiment_id",
                    "additional_columns"=> "el.experiment_year as source_experiment_year",
                    "target_table_alias"=> "el",
                    "secondary_target_table"=> "",
                    "secondary_target_column"=> "",
                    "secondary_target_table_alias"=> ""
                ]
            ]
        ];
        $this->findSeedDataBrowserModel->configModel->setReturnValue('getConfigByAbbrev',$config);

        $inputListTypes = [
            'label'=>[
                'description' => [
                    'data-toggle'=> "tooltip",
                    'title'=> "Package Label information"
                ],
                'text'=>"PACKAGE LABEL - Package Label information",
                'validation'=>"string",
                'apiBodyParam'=>"label"
            ],
            'name'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Designation and Germplasm Names information"
                ],
                'text'=>"GERMPLASM NAME - Designation and Germplasm Names information",
                'validation'=>"string",
                'apiBodyParam'=>"designation"
            ],
            'seed'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Seed Name information"
                ],
                'text'=>"SEED NAME - Seed Name information",
                'validation'=>"string",
                'apiBodyParam'=>"seedName"
                
            ]
        ];
        $this->findSeedDataBrowserModel->variableFilterComponent->setReturnValue('retrieveInputListConfig',$inputListTypes);

        $data = [
            ['packageDbId' => 1, 'packageCode' => '1'],
            ['packageDbId' => 2, 'packageCode' => '2'],
            ['packageDbId' => 3, 'packageCode' => '3']
        ];

        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($data) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1,
                                    'totalCount' => 3,
                                    'currentPage' => 1,
                                ]
                            ],
                            'result' => [
                                'data' => $data
                            ]
                        ],
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        // Mock page limit
        $gridPageSize = 10;
        $this->findSeedDataBrowserModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $arrDataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'totalCount' => count($data),
            'restified' => true,
            'disableDefaultSort' => true,
            'id' => 'dynagrid-seed-search-search-grid'
        ]);

        $expected = [
            'data' => $arrDataProvider,
            'count' => count($data)
        ];

        // ACT
        $actual = $this->findSeedDataBrowserModel->getDataProvider(
                                                            $condition, 
                                                            $condStr, 
                                                            $columns, 
                                                            $inputListOrder, 
                                                            $browserFilters, 
                                                            $getAllResponse, 
                                                            $getParamsOnly);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
     * Test getDataProvider with conditions no browserFilters
     * @return mixed
     */
    public function testGetDataProviderWithBrowserFilters(){
        // ARRANGE
        $condition = [
            [
                'abbrev' => "EXPERIMENT_YEAR",
                'filter_type' => "search",
                'operator' => "in",
                'values' => [2019]
            ],
            [
                'abbrev' => "EXPERIMENT_TYPE",
                'filter_type' => "search",
                'operator' => "in",
                'values' => ['Generation Nursery','Intentional Crossing Nursery']
            ],
            [
                'abbrev' => "NAME",
                'filter_type' => "search",
                'operator' => "in",
                'values' => ['NAME1']
            ],
            [
                'abbrev' => "LABEL",
                'filter_type' => "search",
                'operator' => "in",
                'values' => ['LABEL1']
            ],
            [
                'abbrev' => "SEED",
                'filter_type' => "search",
                'operator' => "in",
                'values' => ['SEED1']
            ],
            [
                'abbrev' => "userId",
                'filter_type' => "search",
                'operator' => "in",
                'values' => "470"
            ]
        ];
        $condStr = null;
        $columns = null;
        $inputListOrder = false;
        $browserFilters = [
            [
                'abbrev' => "GID",
                'filter_type' => "column",
                'operator' => "ilike any",
                'values' => ['SEED1']
            ],
            [
                'abbrev' => "LABEL",
                'filter_type' => "column",
                'operator' => "ilike any",
                'values' => ['PACKAGE1']
            ],
            [
                'abbrev' => "SOURCE_HARV_YEAR",
                'filter_type' => "column",
                'operator' => "ilike any",
                'values' => ['2019']
            ]
        ];
        $getAllResponse = false;
        $getParamsOnly = false;

        $config = [
            "name" => "Find Seeds Query Parameters",
            "main_table" => "germplasm.package",
            "main_table_alias" => "package",
            "main_table_primary_key" => "id",
            "api_resource_method" => "POST",
            "api_resource_endpoint" => "packages-search",
            "search_sort_column" => "id",
            "search_sort_order" => "ASC",
            "input_field_type" => [],
            "values"=> [
                [
                    "disabled" => "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "target_table"=> "experiment",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentType",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "experiment_type",
                    "variable_abbrev"=> "EXPERIMENT_TYPE",
                    "reference_column"=> "experimentDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_type",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "hidden"=> "false",
                    "field_label"=> "Experiment Type",
                    "order_number"=> "11",
                    "target_table"=> "experiment.experiment",
                    "target_column"=> "experiment_type",
                    "api_body_param"=> "experimentType",
                    "variable_abbrev"=> "EXPERIMENT_TYPE",
                    "reference_column"=> "source_experiment_id",
                    "additional_columns"=> "s.id as source_occurrence_id",
                    "target_table_alias"=> "el",
                    "secondary_target_table"=> "",
                    "secondary_target_column"=> "",
                    "secondary_target_table_alias"=> ""
                ],
                [
                    "disabled"=> "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "target_table"=> "experiment",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentYear",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "experiment_year",
                    "variable_abbrev"=> "EXPERIMENT_YEAR",
                    "reference_column"=> "experimentYear",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_year",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "hidden"=> "false",
                    "field_label"=> "Experiment Year",
                    "order_number"=> "10",
                    "target_table"=> "experiment.location",
                    "target_column"=> "experiment_year",
                    "api_body_param"=> "experimentYear",
                    "variable_abbrev"=> "EXPERIMENT_YEAR",
                    "reference_column"=> "source_experiment_id",
                    "additional_columns"=> "el.experiment_year as source_experiment_year",
                    "target_table_alias"=> "el",
                    "secondary_target_table"=> "",
                    "secondary_target_column"=> "",
                    "secondary_target_table_alias"=> ""
                ],
                [
                    "hidden" => "false",
                    "field_label" => "LABEL",
                    "order_number" => "4",
                    "target_table" => "germplasm.package",
                    "target_column" => "label",
                    "api_body_param" => "label",
                    "variable_abbrev" => "LABEL",
                    "reference_column" => "",
                    "additional_columns" => "",
                    "target_table_alias" => "gp",
                    "secondary_target_table" => "",
                    "secondary_target_column" => "",
                    "secondary_target_table_alias" => "",
                    "api_resource_method" => "",
                    "output_display_value" => "",
                    "api_resource_endpoint" => "",
                ],
                [
                    "hidden" => "false",
                    "field_label" => "SEED",
                    "order_number" => "6",
                    "target_table" => "germplasm.seed",
                    "target_column" => "seed_code",
                    "api_body_param" => "seedCode",
                    "variable_abbrev" => "GID",
                    "reference_column" => "",
                    "additional_columns" => "",
                    "target_table_alias" => "gs",
                    "secondary_target_table" => "",
                    "secondary_target_column" => "",
                    "secondary_target_table_alias" => "",
                    "api_resource_method" => "",
                    "output_display_value" => "",
                    "api_resource_endpoint" => "",
                ],
                [
                    "disabled" => "false",
                    "required" => "false",
                    "input_type" => "multiple",
                    "input_field" => "input field",
                    "target_table" => "",
                    "default_value" => "",
                    "allowed_values" => "",
                    "api_body_param" => "names",
                    "basic_parameter" => "",
                    "output_id_value" => "id",
                    "variable_abbrev" => "NAME",
                    "reference_column" => "",
                    "search_sort_order" => "ASC",
                    "search_sort_column" => "",
                    "api_resource_method" => "",
                    "output_display_value" => "",
                    "api_resource_endpoint" => "",
                    "secondary_resource_endpoint" => "",
                    "secondary_resource_endpoint_method" => ""
                ],
                [
                    "hidden" => "false",
                    "field_label" => "SOURCE HARVEST YEAR",
                    "order_number" => "16",
                    "target_table" => "experiment.location",
                    "target_column" => "location_harvest_date",
                    "api_body_param" => "sourceHarvestYear",
                    "variable_abbrev" => "SOURCE_HARV_YEAR",
                    "reference_column" => "source_location_id",
                    "additional_columns" => "",
                    "target_table_alias" => "el",
                    "secondary_target_table" => "germplasm.seed",
                    "secondary_target_column" => "source_location_id",
                    "secondary_target_table_alias"=> "gs",
                    "api_resource_method" => "",
                    "output_display_value" => "",
                    "api_resource_endpoint" => "",
                ]
            ]
        ];
        $this->findSeedDataBrowserModel->configModel->setReturnValue('getConfigByAbbrev',$config);

        $inputListTypes = [
            'label'=>[
                'description' => [
                    'data-toggle'=> "tooltip",
                    'title'=> "Package Label information"
                ],
                'text'=>"PACKAGE LABEL - Package Label information",
                'validation'=>"string",
                'apiBodyParam'=>"label"
            ],
            'name'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Designation and Germplasm Names information"
                ],
                'text'=>"GERMPLASM NAME - Designation and Germplasm Names information",
                'validation'=>"string",
                'apiBodyParam'=>"designation"
            ],
            'seed'=>[
                'description'=>[
                    'data-toggle'=> "tooltip",
                    'title'=> "Seed Name information"
                ],
                'text'=>"SEED NAME - Seed Name information",
                'validation'=>"string",
                'apiBodyParam'=>"seedName"
                
            ]
        ];
        $this->findSeedDataBrowserModel->variableFilterComponent->setReturnValue('retrieveInputListConfig',$inputListTypes);

        $data = [
            ['packageDbId' => 1, 'packageCode' => '1'],
            ['packageDbId' => 2, 'packageCode' => '2'],
            ['packageDbId' => 3, 'packageCode' => '3']
        ];

        $request = $this->make('yii\web\Request',
            [
                'get' => function () {
                    return [
                        'page' => '1',
                        'dynagrid-seed-search-search-grid-page' => '1',
                        'sort' => 'attributeB|-attributeC',
                        'dp-1-sort' => 'attributeB|-attributeC',
                        'dynagrid-seed-search-search-grid-sort' => 'attributeB|-attributeC'
                    ];
                },
                '_cookies' => new \yii\web\CookieCollection([
                    "search-grid_" => (object) [
                        "name" => "search-grid_",
                        "value" => '{"grid":"{\"page\":\"1\",\"theme\":\"panel-default\",'.
                            '\"keys\":[\"55242506\",\"5905f20d\",\"9f3aba3f\",\"80078896\"],'
                            .'\"filter\":\"\",\"sort\":null}"}',
                        "domain" => "",
                        "expire" => null,
                        "path" => "/",
                        "secure" => false,
                        "httpOnly" => true,
                        "sameSite" => null
                        ]
                ])
            ]
        );

        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $gridPageSize = 10;
        $this->findSeedDataBrowserModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($data) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1,
                                    'totalCount' => 3,
                                    'currentPage' => 1,
                                ]
                            ],
                            'result' => [
                                'data' => $data
                            ]
                        ],
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        $arrDataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'totalCount' => count($data),
            'restified' => true,
            'disableDefaultSort' => true,
            'id' => 'dynagrid-seed-search-search-grid'
        ]);

        $expected = [
            'data' => $arrDataProvider,
            'count' => count($data)
        ];

        // ACT
        $actual = $this->findSeedDataBrowserModel->getDataProvider(
                                                            $condition, 
                                                            $condStr, 
                                                            $columns, 
                                                            $inputListOrder, 
                                                            $browserFilters, 
                                                            $getAllResponse, 
                                                            $getParamsOnly);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    /**
    *   Set dynamic columns
    * @param $config array 
    */
    public function testSetColumnsForFiltering(){
        // ARRANGE
        $config = [
            "name" => "Find Seeds Query Parameters",
            "main_table" => "germplasm.package",
            "main_table_alias" => "package",
            "main_table_primary_key" => "id",
            "api_resource_method" => "POST",
            "api_resource_endpoint" => "packages-search",
            "search_sort_column" => "id",
            "search_sort_order" => "ASC",
            "input_field_type" => [],
            "values"=> [
                [
                    "disabled" => "false",
                    "required"=> "true",
                    "input_type"=> "multiple",
                    "input_field"=> "selection",
                    "target_table"=> "experiment",
                    "default_value"=> "",
                    "allowed_values"=> "",
                    "api_body_param"=> "experimentType",
                    "basic_parameter"=> "true",
                    "output_id_value"=> "experiment_type",
                    "variable_abbrev"=> "EXPERIMENT_TYPE",
                    "reference_column"=> "experimentDbId",
                    "search_sort_order"=> "DESC",
                    "search_sort_column"=> "display_value",
                    "api_resource_method"=> "POST",
                    "output_display_value"=> "experiment_type",
                    "api_resource_endpoint"=> "experiment-packages-search",
                    "secondary_resource_endpoint"=> "packages-search",
                    "secondary_resource_endpoint_method"=> "POST",
                    "hidden"=> "false",
                    "field_label"=> "Experiment Type",
                    "order_number"=> "11",
                    "target_table"=> "experiment.experiment",
                    "target_column"=> "experiment_type",
                    "api_body_param"=> "experimentType",
                    "variable_abbrev"=> "EXPERIMENT_TYPE",
                    "reference_column"=> "source_experiment_id",
                    "additional_columns"=> "s.id as source_occurrence_id",
                    "target_table_alias"=> "el",
                    "secondary_target_table"=> "",
                    "secondary_target_column"=> "",
                    "secondary_target_table_alias"=> ""
                ]
            ]
        ];
        $this->findSeedDataBrowserModel->configModel->setReturnValue('getConfigByAbbrev',$config);

        $apiReturnValue = [
            "status" => 200,
            "body" => [
                "metadata"=>[
                    "pagination"=>[
                        "totalCount"=>1,
                        "totalPages"=>1
                    ]
                ],
                "result"=>[
                    "data" => [
                        [
                            'description'=> "Desired type for this experiment.",
                            'label'=> "EXPERIMENT TYPE",
                            'id'=>1234,
                            'abbrev'=> "EXPERIMENT_TYPE",
                            'display_name'=> "Experiment Type",
                            'status'=> "active"
                        ]                      
                    ]
                ]
            ]
        ];

        // Create mock
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) use ($apiReturnValue) {
                    return $apiReturnValue;
                }
            ]
        );
        // Inject user mock into Yii::$app
        Yii::$app->set('api',$api);

        // ACT
        $actual = $this->findSeedDataBrowserModel->setColumnsForFiltering(null);
    
        // ASSERT
        $this->assertTrue($actual);
    }

}