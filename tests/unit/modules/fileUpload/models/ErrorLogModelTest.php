<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\fileUpload\models;

use kartik\dynagrid\DynaGrid;
// Import models
use app\models\UserDashboardConfig;

class ErrorLogModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $errorLogModel;
    
    protected function _before()
    {
        $this->errorLogModel = \Yii::$container->get('fileUploadErrorLogModel');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * getFileUploadRecord: Test 1
     * 
     * The expected behavior is that an exception will be thrown
     * if the API retrieval fails (code not = 200)
     */
    public function testGetFileUploadRecordWhenApiRetrievalFails()
    {
        // ARRANGE
        $fileUploadDbId = 123;
        $this->errorLogModel->germplasmFileUpload->setReturnValue(
            'getOne',
            [
                "status" => 400
            ]
        );

        // ACT + ASSERT
        $this->assertThrows(\yii\base\ErrorException::class,
            function() use ($fileUploadDbId) {
                $actual = $this->errorLogModel->getFileUploadRecord($fileUploadDbId);
            }
        );
    }

    /**
     * getFileUploadRecord: Test 2
     * 
     * The expected behavior is that the "data" from the API result
     * will be returned.
     */
    public function testGetFileUploadRecordWhenApiRetrievalIsSuccessful()
    {
        // ARRANGE
        $fileUploadDbId = 123;
        $mockedReturnValue = [
            "status" => 200,
            "data" => [ "a" => "x", "b" => "y", "c" => "z" ]
        ];
        $this->errorLogModel->germplasmFileUpload->setReturnValue(
            'getOne',
            $mockedReturnValue
        );

        // ACT
        $actual = $this->errorLogModel->getFileUploadRecord($fileUploadDbId);
        
        // ASSERT
        $this->assertEquals($mockedReturnValue["data"], $actual);
    }

    /**
     * getColumns: Test1
     * 
     * Default use case
     */
    public function testGetColumns() {
        // ARRANGE
        $expected = $columns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'order' => DynaGrid::ORDER_FIX_LEFT,
                'header' => false
            ],
            [
                'attribute'=>'row_number',
                'label' => 'Row No',
                'format' => 'raw',
                'value' => function ($model){
                    $rowNum = $model['row_number'];
                    return '<span class="badge new red darken-2"><strong>'.$rowNum.'</strong></span>';
                },
            ],
            [
                'attribute'=>'entity',
                'label' => 'Entity',
                'content' => function($data) {
                    $entity = str_replace('_', ' ', $data['entity']);
                    return $entity;
                }
            ],
            [
                'attribute'=>'status_code',
                'label' => 'Status Code',
                'format' => 'raw',
                'value' => function ($model) {
                    $status = $model['status_code'] ?? '';

                    if($status == 'ERROR') {
                        $status = "<p class='red-text text-darken-2'>$status</p>";
                    }

                    return "<strong>$status</strong>";
                }
            ],
            [
                'attribute'=>'message',
                'label' => 'Error Log Message',
                'contentOptions' => [
                    "class" => "germplasm-name-col"
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    $message = $model['message'] ?? '';
                    if(gettype($message) == 'string') {
                        $messageParts = explode(':', $message);
                        $mainMessage = $messageParts[0] ?? '';
                        $messageParts[0] = "<strong>$mainMessage</strong>";
                        $message = implode(": ", $messageParts) ;
                    }
                    else $message = 'Error unknown.';

                    return $message;
                }
            ],
            [
                'attribute'=>'timestamp',
                'label' => 'Error Log Timestamp',
            ]
        ];

        $model = [
            "row_number" => 1,
            "entity" => "germplasm",
            "status_code" => "ERROR",
            "message" => "A:a"
        ];

        $expectedRowNumber = '<span class="badge new red darken-2"><strong>'.$model["row_number"].'</strong></span>';
        $expectedEntity = str_replace('_', ' ', $model['entity']);
        $expectedStatusCode = "<strong><p class='red-text text-darken-2'>" . $model["status_code"] . "</p></strong>";
        $message = $model['message'] ?? '';
        $messageParts = explode(':', $message);
        $mainMessage = $messageParts[0] ?? '';
        $messageParts[0] = "<strong>$mainMessage</strong>";
        $expectedMessageValid = implode(": ", $messageParts) ;
        $expectedMessageInvalid = "Error unknown.";

        // ACT
        $actual = $this->errorLogModel->getColumns();
        $actualRowNum = $actual[1]["value"]($model);
        $actualEntity = $actual[2]["content"]($model);
        $actualStatusCode = $actual[3]["value"]($model);
        $actualMessageValid = $actual[4]["value"]($model);
        $actualMessageInvalid = $actual[4]["value"](["message" => true]);
        
        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedRowNumber, $actualRowNum);
        $this->assertEquals($expectedEntity, $actualEntity);
        $this->assertEquals($expectedStatusCode, $actualStatusCode);
        $this->assertEquals($expectedMessageValid, $actualMessageValid);
        $this->assertEquals($expectedMessageInvalid, $actualMessageInvalid);
    }

    /**
     * paginate: Test 1
     * 
     * If page number does not exceed page count
     */
    public function testPaginateIfPageNumberIsWithinNumberOfPages() {
        // ARRANGE
        $fileData = [
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ]
        ];
        $queryParams = [
            'page' => '2'
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expected = [
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ]
        ];
        $expectedPage = $queryParams["page"];

        // ACT
        $actual = $this->errorLogModel->paginate($fileData);
        $actualQueryParams = \Yii::$app->request->getQueryParams();
        $actualPage = $actualQueryParams['page'];

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedPage, $actualPage);
    }

    /**
     * paginate: Test 2
     * 
     * If page number exceeds page count
     */
    public function testPaginateIfPageNumberExceedsNumberOfPages() {
        // ARRANGE
        $fileData = [
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ]
        ];
        $queryParams = [
            'page' => '200'
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expected = [
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], 
            [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ], [ "A" => "a" ]
        ];
        $expectedPage = 1;

        // ACT
        $actual = $this->errorLogModel->paginate($fileData);
        $actualQueryParams = \Yii::$app->request->getQueryParams();
        $actualPage = $actualQueryParams['page'];

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedPage, $actualPage);
    }

    public function testSearch() {
        // ARRANGE
        $fileUploadDbId = 123;
        $mockedReturnValue = [
            "status" => 200,
            "data" => [
                "fileName" => "File ABC",
                "fileStatus" => "Uploaded",
                "uploader" => "Doe, John",
                "uploaderTimestamp" => "2024-01-01 00:00:00",
                "errorLog" => [
                    [
                        "row_number" => 1,
                        "entity" => "germplasm",
                        "status_code" => "ERROR",
                        "message" => "A:a"
                    ]
                ]
            ]
        ];
        $this->errorLogModel->germplasmFileUpload->setReturnValue(
            'getOne',
            $mockedReturnValue
        );

        $queryParams = [
            'page' => '1'
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expectedFileName = $mockedReturnValue["data"]["fileName"];
        $expectedFileStatus = $mockedReturnValue["data"]["fileStatus"];
        $expectedUploader = $mockedReturnValue["data"]["uploader"];
        $expectedUploadTimestamp = $mockedReturnValue["data"]["uploaderTimestamp"];

        // ACT
        $actual = $this->errorLogModel->search($fileUploadDbId);
        $actualFileName = $actual["fileName"];
        $actualFileStatus = $actual["fileStatus"];
        $actualUploader = $actual["uploader"];
        $actualUploadTimestamp = $actual["uploadTimestamp"];

        // ASSERT
        $this->assertEquals($expectedFileName, $actualFileName);
        $this->assertEquals($expectedFileStatus, $actualFileStatus);
        $this->assertEquals($expectedUploader, $actualUploader);
        $this->assertEquals($expectedUploadTimestamp, $actualUploadTimestamp);

    }
}