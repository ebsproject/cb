<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\fileUpload\models;

class FileUploadHelperTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $fileUploadHelper;
    
    protected function _before()
    {
        $this->fileUploadHelper = \Yii::$container->get('fileUploadFileUploadHelper');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * getStatusClass: Test 1
     */
    public function testGetStatusClassWhenStatusIsInQueue()
    {
        // ARRANGE
        $fileUploadStatus = 'in queue';
        $expected = [
            "class" => "grey",
            "title" => "Queued for validation"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 2
     */
    public function testGetStatusClassWhenStatusIsCreated()
    {
        // ARRANGE
        $fileUploadStatus = 'created';
        $expected = [
            "class" => "grey",
            "title" => "Queued for validation"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 3
     */
    public function testGetStatusClassWhenStatusIsValidationInProgress()
    {
        // ARRANGE
        $fileUploadStatus = 'validation in progress';
        $expected = [
            "class" => "orange",
            "title" => "Validation in progress"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 4
     */
    public function testGetStatusClassWhenStatusIsResolvingConflict()
    {
        // ARRANGE
        $fileUploadStatus = 'resolving conflict';
        $expected = [
            "class" => "purple",
            "title" => "Conflict resolution in progress"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 5
     */
    public function testGetStatusClassWhenStatusIsValidated()
    {
        // ARRANGE
        $fileUploadStatus = 'validated';
        $expected = [
            "class" => "blue",
            "title" => "Transaction is ready to be completed"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 6
     */
    public function testGetStatusClassWhenStatusIsMergeReady()
    {
        // ARRANGE
        $fileUploadStatus = 'merge ready';
        $expected = [
            "class" => "blue",
            "title" => "Transaction is ready for merging"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 7
     */
    public function testGetStatusClassWhenStatusIsCreationInProgress()
    {
        // ARRANGE
        $fileUploadStatus = 'creation in progress';
        $expected = [
            "class" => "yellow",
            "title" => "Completion of transaction in progress"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 8
     */
    public function testGetStatusClassWhenStatusIsMergingInProgress()
    {
        // ARRANGE
        $fileUploadStatus = 'merging in progress';
        $expected = [
            "class" => "yellow",
            "title" => "Completion of transaction in progress"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 9
     */
    public function testGetStatusClassWhenStatusIsCompleted()
    {
        // ARRANGE
        $fileUploadStatus = 'completed';
        $expected = [
            "class" => "green",
            "title" => "Transaction has been completed"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 10
     */
    public function testGetStatusClassWhenStatusIsValidationError()
    {
        // ARRANGE
        $fileUploadStatus = 'validation error';
        $expected = [
            "class" => "red",
            "title" => "Transaction has failed"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 11
     */
    public function testGetStatusClassWhenStatusIsValidationFailed()
    {
        // ARRANGE
        $fileUploadStatus = 'validation failed';
        $expected = [
            "class" => "red",
            "title" => "Transaction has failed"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 12
     */
    public function testGetStatusClassWhenStatusIsCreationFailed()
    {
        // ARRANGE
        $fileUploadStatus = 'creation failed';
        $expected = [
            "class" => "red",
            "title" => "Transaction has failed"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 13
     */
    public function testGetStatusClassWhenStatusIsMergeFailed()
    {
        // ARRANGE
        $fileUploadStatus = 'merge failed';
        $expected = [
            "class" => "red",
            "title" => "Transaction has failed"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getStatusClass: Test 14
     */
    public function testGetStatusClassWhenStatusIsUnknown()
    {
        // ARRANGE
        $fileUploadStatus = 'core breeding';
        $expected = [
            "class" => "grey",
            "title" => "Unknown status"
        ];

        // ACT
        $actual = $this->fileUploadHelper->getStatusClass($fileUploadStatus);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getFileUploadInfo: Test 1
     */
    public function testGetFileUploadInfo() {
        // ARRANGE
        $fileDbId = 123;

        $returnValue = [
            "data" => [
                [
                    "fileName" => "File ABC",
                    "fileStatus" => "completed",
                    "uploader" => "Doe, John",
                    "uploadTimestamp" => "2020-10-20",
                    "fileData" => [],
                    "programCode" => "ABC",
                    "errorLog" => [],
                    "remarks" => "Test remarks",
                    "entity" => "germplasm",
                    "fileUploadAction" => "create",
                ]
            ]
        ];

        $this->fileUploadHelper->germplasmFileUpload->setReturnValue(
            'searchAll',
            $returnValue
        );

        $expected = [
            "fileName" => "File ABC",
            "fileStatus" => "Completed",
            "uploader" => "Doe, John",
            "uploadTimestamp" => "2020-10-20",
            "fileData" => [],
            "programCode" => "ABC",
            "errorLog" => [],
            "remarks" => "Test remarks",
            "entity" => "germplasm",
            "action" => "create",
        ];

        // ACT
        $actual = $this->fileUploadHelper->getFileUploadInfo($fileDbId);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * getConfigurations: Test 1
     */
    public function testGetConfigurations() {
        // ARRANGE
        $baseAbbrev = "ABC";
        $programCode = "XYZ";

        $this->fileUploadHelper->program->setReturnValue(
            'getCropCode',
            'CC'
        );

        $returnValues = [
            "return_vals" => [
                [],
                [
                    "values" => [ 1, 2, 3 ]
                ],
                [],
                [
                    "values" => [ 4, 5, 6 ]
                ]
            ]
        ];

        $this->fileUploadHelper->configurations->setReturnValue(
            'getConfigByAbbrev',
            $returnValues
        );

        $expected = [
            1, 2, 3, 4, 5, 6
        ];
        
        // ACT
        $actual = $this->fileUploadHelper->getConfigurations($baseAbbrev, $programCode);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
    
    /**
     * getConfigurations: Test 2
     */
    public function testGetConfigurationsWhenEntityIsGermplasm() {
        // ARRANGE
        $baseAbbrev = "ABC";
        $programCode = "XYZ";
        $entity = "germplasm";

        $this->fileUploadHelper->program->setReturnValue(
            'getCropCode',
            'CC'
        );

        $returnValues = [
            "return_vals" => [
                [],
                [
                    "values" => [ 1, 2, 3 ]
                ],
                [],
                [
                    "values" => [ 4, 5, 6 ]
                ]
            ]
        ];

        $this->fileUploadHelper->configurations->setReturnValue(
            'getConfigByAbbrev',
            $returnValues
        );

        $expected = [
            1, 2, 3, 4, 5, 6
        ];
        
        // ACT
        $actual = $this->fileUploadHelper->getConfigurations($baseAbbrev, $programCode, $entity);

        // ASSERT
        $this->assertEquals($expected, $actual);
    }
}