<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\fileUpload\models;

class SummaryModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $summaryModel;
    
    protected function _before()
    {
        $this->summaryModel = \Yii::$container->get('fileUploadSummaryModel');
    }

    protected function _after()
    {
    }

    // tests
    /**
     * assembleUrlParameters: Test 1
     */
    public function testAssembleUrlParametersNoResetWithGridId() 
    {
        // ARRANGE
        $resetPage = false;
        $entityIdName = "entityDbId";
        $entity = "entity";
        $gridId = 'dynagrid-file-upload-summary-' . $entity . '-grid';

        $queryParams = [
            "$gridId-page" => 3
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $expected = "limit=$mockedLimit&page=" . $queryParams["$gridId-page"] . "&sort=" . $entity . "DbId:ASC";

        // ACT
        $actual = $this->summaryModel->assembleUrlParameters(
            $resetPage,
            $entityIdName,
            $entity
        );

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 2
     */
    public function testAssembleUrlParametersNoResetWithDpPage() 
    {
        // ARRANGE
        $resetPage = false;
        $entityIdName = "entityDbId";
        $entity = "entity";
        $gridId = 'dp-1';

        $queryParams = [
            "$gridId-page" => 3
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $expected = "limit=$mockedLimit&page=" . $queryParams["$gridId-page"] . "&sort=" . $entity . "DbId:ASC";

        // ACT
        $actual = $this->summaryModel->assembleUrlParameters(
            $resetPage,
            $entityIdName,
            $entity
        );

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 3
     */
    public function testAssembleUrlParametersNoResetWithPage() 
    {
        // ARRANGE
        $resetPage = false;
        $entityIdName = "entityDbId";
        $entity = "entity";

        $queryParams = [
            "page" => 3
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $expected = "limit=$mockedLimit&page=" . $queryParams["page"] . "&sort=" . $entity . "DbId:ASC";

        // ACT
        $actual = $this->summaryModel->assembleUrlParameters(
            $resetPage,
            $entityIdName,
            $entity
        );

        // ASSERT
        $this->assertEquals($expected, $actual);
    }

    /**
     * assembleUrlParameters: Test 4
     */
    public function testAssembleUrlParametersWithResetWithGridId() 
    {
        // ARRANGE
        $resetPage = true;
        $entityIdName = "entityDbId";
        $entity = "entity";
        $gridId = 'dynagrid-file-upload-summary-' . $entity . '-grid';

        $queryParams = [
            "$gridId-page" => 3
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $expectedPage = 1;
        $expected = "limit=$mockedLimit&page=$expectedPage&sort=" . $entity . "DbId:ASC";

        // ACT
        $actual = $this->summaryModel->assembleUrlParameters(
            $resetPage,
            $entityIdName,
            $entity
        );
        $actualGetParams = \Yii::$app->request->getQueryParams();
        $actualPage = $actualGetParams["$gridId-page"];

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedPage, $actualPage);
    }

    /**
     * assembleUrlParameters: Test 5
     */
    public function testAssembleUrlParametersWithResetWithDpPage() 
    {
        // ARRANGE
        $resetPage = true;
        $entityIdName = "entityDbId";
        $entity = "entity";
        $gridId = 'dp-1';

        $queryParams = [
            "$gridId-page" => 3
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $expectedPage = 1;
        $expected = "limit=$mockedLimit&page=$expectedPage&sort=" . $entity . "DbId:ASC";

        // ACT
        $actual = $this->summaryModel->assembleUrlParameters(
            $resetPage,
            $entityIdName,
            $entity
        );
        $actualGetParams = \Yii::$app->request->getQueryParams();
        $actualPage = $actualGetParams["$gridId-page"];

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedPage, $actualPage);
    }

    /**
     * assembleUrlParameters: Test 6
     */
    public function testAssembleUrlParametersWithResetWithPage() 
    {
        // ARRANGE
        $resetPage = true;
        $entityIdName = "entityDbId";
        $entity = "entity";

        $queryParams = [
            "page" => 3
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $expectedPage = 1;
        $expected = "limit=$mockedLimit&page=$expectedPage&sort=" . $entity . "DbId:ASC";

        // ACT
        $actual = $this->summaryModel->assembleUrlParameters(
            $resetPage,
            $entityIdName,
            $entity
        );
        $actualGetParams = \Yii::$app->request->getQueryParams();
        $actualPage = $actualGetParams["page"];

        // ASSERT
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expectedPage, $actualPage);
    }

    /**
     * search: Test 1
     */
    public function testSearchNoReset() {
        // ARRANGE
        $fileDbId = 123;
        $entity = "germplasm";

        $returnValues = [
            "return_vals" => [
                [
                    "status" => 200,
                    "totalCount" => 1,
                    "data" => [
                        [
                            "germplasmDbId" => 987
                        ]
                    ]
                ]
            ]
        ];
        $this->summaryModel->api->setReturnValue(
            'getApiResults',
            $returnValues
        );

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $queryParams = [
            "page" => 30000
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expectedAllModels = [
            [
                "germplasmDbId" => 987
            ]
        ];
        $expectedTotalCount = count($expectedAllModels);

        // ACT
        $actual = $this->summaryModel->search($fileDbId, $entity);
        $actualAllModels = $actual->allModels;
        $actualTotalCount = $actual->totalCount;

        // ASSERT
        $this->assertEquals($expectedAllModels, $actualAllModels);
        $this->assertEquals($expectedTotalCount, $actualTotalCount);
    }

    /**
     * search: Test 2
     */
    public function testSearchWithReset() {
        // ARRANGE
        $fileDbId = 123;
        $entity = "germplasm";

        $returnValues = [
            "return_vals" => [
                [],
                [
                    "status" => 200,
                    "totalCount" => 1,
                    "data" => [
                        [
                            "germplasmDbId" => 987
                        ]
                    ]
                ]
            ]
        ];
        $this->summaryModel->api->setReturnValue(
            'getApiResults',
            $returnValues
        );

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $queryParams = [
            "page" => 30000
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expectedAllModels = [
            [
                "germplasmDbId" => 987
            ]
        ];
        $expectedTotalCount = count($expectedAllModels);

        // ACT
        $actual = $this->summaryModel->search($fileDbId, $entity);
        $actualAllModels = $actual->allModels;
        $actualTotalCount = $actual->totalCount;

        // ASSERT
        $this->assertEquals($expectedAllModels, $actualAllModels);
        $this->assertEquals($expectedTotalCount, $actualTotalCount);
    }

    /**
     * search: Test 3
     */
    public function testSearchNoResetGermplasmName() {
        // ARRANGE
        $fileDbId = 123;
        $entity = "germplasm_name";

        $returnValues = [
            "return_vals" => [
                [
                    "status" => 200,
                    "totalCount" => 1,
                    "data" => [
                        [
                            "germplasmNameDbId" => 987
                        ]
                    ]
                ]
            ]
        ];
        $this->summaryModel->api->setReturnValue(
            'getApiResults',
            $returnValues
        );

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $queryParams = [
            "page" => 30000
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expectedAllModels = [
            [
                "germplasmNameDbId" => 987
            ]
        ];
        $expectedTotalCount = count($expectedAllModels);

        // ACT
        $actual = $this->summaryModel->search($fileDbId, $entity);
        $actualAllModels = $actual->allModels;
        $actualTotalCount = $actual->totalCount;

        // ASSERT
        $this->assertEquals($expectedAllModels, $actualAllModels);
        $this->assertEquals($expectedTotalCount, $actualTotalCount);
    }

    /**
     * search: Test 4
     */
    public function testSearchNoResetGermplasmAttribute() {
        // ARRANGE
        $fileDbId = 123;
        $entity = "germplasm_attribute";

        $returnValues = [
            "return_vals" => [
                [
                    "status" => 200,
                    "totalCount" => 1,
                    "data" => [
                        [
                            "germplasmAttributeDbId" => 987
                        ]
                    ]
                ]
            ]
        ];
        $this->summaryModel->api->setReturnValue(
            'getApiResults',
            $returnValues
        );

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $queryParams = [
            "page" => 30000
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expectedAllModels = [
            [
                "germplasmAttributeDbId" => 987
            ]
        ];
        $expectedTotalCount = count($expectedAllModels);

        // ACT
        $actual = $this->summaryModel->search($fileDbId, $entity);
        $actualAllModels = $actual->allModels;
        $actualTotalCount = $actual->totalCount;

        // ASSERT
        $this->assertEquals($expectedAllModels, $actualAllModels);
        $this->assertEquals($expectedTotalCount, $actualTotalCount);
    }

    /**
     * search: Test 5
     */
    public function testSearchNoResetGermplasmRelation() {
        // ARRANGE
        $fileDbId = 123;
        $entity = "germplasm_relation";

        $returnValues = [
            "return_vals" => [
                [
                    "status" => 200,
                    "totalCount" => 1,
                    "data" => [
                        [
                            "germplasmRelationDbId" => 987
                        ]
                    ]
                ]
            ]
        ];
        $this->summaryModel->api->setReturnValue(
            'getApiResults',
            $returnValues
        );

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $queryParams = [
            "page" => 30000
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expectedAllModels = [
            [
                "germplasmRelationDbId" => 987
            ]
        ];
        $expectedTotalCount = count($expectedAllModels);

        // ACT
        $actual = $this->summaryModel->search($fileDbId, $entity);
        $actualAllModels = $actual->allModels;
        $actualTotalCount = $actual->totalCount;

        // ASSERT
        $this->assertEquals($expectedAllModels, $actualAllModels);
        $this->assertEquals($expectedTotalCount, $actualTotalCount);
    }

    /**
     * search: Test 6
     */
    public function testSearchNoResetSeed() {
        // ARRANGE
        $fileDbId = 123;
        $entity = "seed";

        $returnValues = [
            "return_vals" => [
                [
                    "status" => 200,
                    "totalCount" => 1,
                    "data" => [
                        [
                            "seedDbId" => 987
                        ]
                    ]
                ]
            ]
        ];
        $this->summaryModel->api->setReturnValue(
            'getApiResults',
            $returnValues
        );

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $queryParams = [
            "page" => 30000
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expectedAllModels = [
            [
                "seedDbId" => 987
            ]
        ];
        $expectedTotalCount = count($expectedAllModels);

        // ACT
        $actual = $this->summaryModel->search($fileDbId, $entity);
        $actualAllModels = $actual->allModels;
        $actualTotalCount = $actual->totalCount;

        // ASSERT
        $this->assertEquals($expectedAllModels, $actualAllModels);
        $this->assertEquals($expectedTotalCount, $actualTotalCount);
    }

    /**
     * search: Test 7
     */
    public function testSearchNoResetPackage() {
        // ARRANGE
        $fileDbId = 123;
        $entity = "package";

        $returnValues = [
            "return_vals" => [
                [
                    "status" => 200,
                    "totalCount" => 1,
                    "data" => [
                        [
                            "packageDbId" => 987
                        ]
                    ]
                ]
            ]
        ];
        $this->summaryModel->api->setReturnValue(
            'getApiResults',
            $returnValues
        );

        $mockedLimit = 50;
        $this->summaryModel->userDashboardConfig->setReturnValue(
            'getDefaultPageSizePreferences',
            $mockedLimit
        );

        $queryParams = [
            "page" => 30000
        ];
        // Create mock
        $request = $this->make('yii\web\Request',
            [
                '_queryParams' => $queryParams
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $expectedAllModels = [
            [
                "packageDbId" => 987
            ]
        ];
        $expectedTotalCount = count($expectedAllModels);

        // ACT
        $actual = $this->summaryModel->search($fileDbId, $entity);
        $actualAllModels = $actual->allModels;
        $actualTotalCount = $actual->totalCount;

        // ASSERT
        $this->assertEquals($expectedAllModels, $actualAllModels);
        $this->assertEquals($expectedTotalCount, $actualTotalCount);
    }

}