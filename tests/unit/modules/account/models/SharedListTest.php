<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\account\models;

use app\dataproviders\ArrayDataProvider;
use Yii;

class SharedListTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $sharedList;
    
    protected function _before()
    {
        $this->sharedList = Yii::$container->get('sharedList');
    }

    protected function _after()
    {
    }

    // tests
    public function testFormName()
    {
        //arrange
        $expected = "SharedList";

        //act
        $actual = $this->sharedList->formName();

        //assert
        $this->assertEquals($expected, $actual);

    }

    // tests search()
    public function testSearchValidInput()
    {
        //arrange
        $params = [
            'type' => 'package',
            'abbrev' => 'PACKAGE_LIST',
            'name' => 'package list',
            'displayName' => 'package list',
            'description' => 'package list'
        ];
        $filters = '';
        $pagination = true;
        $results = [
            'data' => [
                'listDbId' => 1,
                'type' => 'package',
                'abbrev' => 'PACKAGE_LIST',
                'name' => 'package list',
                'displayName' => 'package list',
                'description' => 'package list'
            ]
        ];

        $expected = true;

        $this->sharedList->lists->setReturnValue('searchAll',$results);

        //act
        $actual = !empty($this->sharedList->search($params, $filters, $pagination)) ? true : false ;

        //assert
        $this->assertEquals($expected, $actual);
    }

    // tests search()
    public function testSearchIncompleteParams()
    {
        //arrange
        $params = [
            'type' => 'package',
            'abbrev' => 'PACKAGE_LIST',
            'name' => 'package list'
        ];
        $filters = '';
        $pagination = true;
        $results = [
            'data' => [
                'listDbId' => 1,
                'type' => 'package',
                'abbrev' => 'PACKAGE_LIST',
                'name' => 'package list',
                'displayName' => 'package list',
                'description' => 'package list'
            ]
        ];

        $expected = true;

        $this->sharedList->lists->setReturnValue('searchAll',$results);

        //act
        $actual = !empty($this->sharedList->search($params, $filters, $pagination)) ? true : false;

        //assert
        $this->assertEquals($expected, $actual);

    }
}