<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\account\models;

use Yii;

class ListLocationSearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $listLocationSearchModel;
    
    protected function _before()
    {
        $this->sharedList = Yii::$container->get('listLocationSearchModel');
    }

    protected function _after()
    {
    }

    // tests
    public function testRules()
    {
        //arrange
        $expected = [
			[['abbrev','name','display_name','country', 'place_type', 'latitude', 'longitude', 'organization', 'remarks'], 'safe'],
			[['is_active'], 'boolean']
		];

        //act
        $actual = $this->sharedList->rules();

        //assert
        $this->assertEquals($expected, $actual);

    }
}
?>