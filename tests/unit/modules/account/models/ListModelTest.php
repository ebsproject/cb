<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\account\models;

use Yii;

use app\dataproviders\ArrayDataProvider;
use kartik\grid\GridView;
use kartik\date\DatePicker;

class ListModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $application;
    protected $user;

    protected $listModel;
    
    /**
     * Setup the test environment.
     * Instantiate objects, create mockups, etc.
     */
    protected function _before()
    {
        $this->listModel = Yii::$container->get('listModel');
    }

    /**
     * Tear down the test environment.
     * Deallocate memory, reset state, etc.
     */
    protected function _after()
    {
    }

    // tests
    public function testValidateSplitInvalidSplitCount()
    {
        $listId = 1;
        $listType = 'germplasm';
        $childListCount = 1;
        $splitOrder = 'desc';
        $listMemberCount = 5;

        $expected = ['valid'=>false,'message'=>'Invalid split count.','bgprocess'=>false];

        // Create mock
        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) {
                    return 200;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        $actual = $this->listModel->validateSplit($listId,$listType,$childListCount,$splitOrder,$listMemberCount);

        $this->assertEquals($expected,$actual);
    }

    // tests
    public function testValidateSplitInvalidListMemberCount()
    {
        $listId = 0;
        $listType = 'germplasm';
        $childListCount = 2;
        $splitOrder = 'desc';
        $listMemberCount = 1;

        $expected = ['valid'=>false,'message'=>'Insufficient number of list members for split.','bgprocess'=>false];

        // Create mock
        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) {
                    return 200;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        $actual = $this->listModel->validateSplit($listId,$listType,$childListCount,$splitOrder,$listMemberCount);

        $this->assertEquals($expected,$actual);
    }

    // tests
    public function testValidateSplitTooLargeSplitCount()
    {
        $listId = 1;
        $listType = 'germplasm';
        $childListCount = 6;
        $splitOrder = 'desc';
        $listMemberCount = 5;

        $expected = ['valid'=>false,'message'=>'Split count exceeds list member count.','bgprocess'=>false];

        // Create mock
        $config = $this->make('app\components\Config',
            [
                'getAppThreshold' => function($tool, $feature) {
                    return 200;
                }
            ]
        );

        // Inject user mock into Yii::$app
        Yii::$app->set('config',$config);

        $actual = $this->listModel->validateSplit($listId,$listType,$childListCount,$splitOrder,$listMemberCount);

        $this->assertEquals($expected,$actual);
    }

    // test update list info
    public function testUpdateListInfo(){
        // ARRANGE
        $expected = true;

        $listId = 123;
        $processRemarks = '';

        $getResultMock  = [
            "data" => [ 
                "listDbId" => 35557,
                "abbrev" => "HB2021WS001001_PLOT_01",
                "name" => "HB2021WS001001_plot_01",
                "displayName" => "HB2021WS001001_plot_01",
                "type" => "plot",
                "subType" => null,
                "entityId" => 3,
                "description" => "HB2021WS001001_plot_01",
                "remarks" => "HB2021WS001001_plot_01",
                "notes" => "Created by B4R API",
                "status" => "created",
                "isActive" => true,
                "creationTimestamp" => "2021-11-15T03:25:05.463Z",
                "creatorDbId" => 470,
                "creator" => "DelaRosa, Karen Mae",
                "modificationTimestamp" => null,
                "modifierDbId" => null,
                "modifier" => null,
                "accessData" => [
                    "user" => [
                        "470" => [
                            "addedBy" => 470,
                            "addedOn" => "2021-11-15T03:25:05.463Z",
                            "dataRoleId" => 25
                        ]
                    ]
                ],
                "listUsage" => "final list"
            ],
            "status" => 200
        ];
        $updateResultMock = [
            "metadata" => [
                "pagination" => [
                    "pageSize" => 100,
                    "totalCount" => 1,
                    "currentPage" => 1,
                    "totalPages" => 1
                ],
                "status" => [
                    [
                        "message" => "Request has been successfully completed.",
                        "messageType" => "INFO"
                    ]
                ],
                "datafiles" => []
            ],
            "result" => [
                "data" => [
                    [
                        "listDbId" => "35557",
                        "recordCount" => 1,
                        "href" => "https://cbapi.local/v3/lists/35557"
                    ]
                ]
            ]
        ];

        $this->listModel->lists->setReturnValue('getOne',$getResultMock);
        $this->listModel->lists->setReturnValue('updateOne',$updateResultMock);

        // ACT
        $actual = $this->listModel->updateListInfo($listId,$processRemarks);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test get users
    public function testGetUsers(){
        // ARRANGE
        $expected = [
            "470" => "DelaRosa, Karen Mae"
        ];

        $listId = 1234;
        $userId = 469;
        $personResult = [
            "data"=> [
                [
                    "personDbId"=> 470,
                    "email"=> "k.delarosa@irri.org",
                    "username"=> "k.delarosa",
                    "firstName"=> "Karen Mae",
                    "lastName"=> "DelaRosa",
                    "personName"=> "DelaRosa, Karen Mae",
                    "personType"=> "admin",
                    "personStatus"=> "active",
                    "personRoleId"=> 1,
                    "isActive"=> true,
                    "personDocument"=> "'activ':1C 'admin':1C 'delarosa':1A 'k.delarosa':1B 'k.delarosa@irri.org':1C 'karen':1A,2A 'mae':2A,3A",
                    "creationTimestamp"=> "2021-10-28T00:39:39.723Z",
                    "creatorDbId"=> 1,
                    "creator"=> "EBS, Admin",
                    "modificationTimestamp"=> "2021-10-28T00:41:47.073Z",
                    "modifierDbId"=> null,
                    "modifier"=> null
                ]
            ],
            "status" => 200
        ];

        $this->listModel->user->setReturnValue('getUserId',$userId);
        $this->listModel->person->setReturnValue('searchAll',$personResult);

        // ACT
        $actual = $this->listModel->getUsers($listId, $userId);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test get teams
    public function testGetTeams(){
        // ARRANGE
        $expected = [
            '101' => 'Irrigated Southeast Asia (IRSEA)'
        ];

        $teamResult = [
            "data" => [
                [
                    "teamDbId" => 101,
                    "teamCode" => "IRSEA",
                    "teamName" => "Irrigated Southeast Asia"
                ]
            ],
            "status" => 200
        ];

        $this->listModel->tenantTeam->setReturnValue('searchAll',$teamResult);

        // ACT
        $actual = $this->listModel->getTeams();

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test get teams
    public function testGetPrograms(){
        // ARRANGE
        $expected = [
            '101' => 'Irrigated Southeast Asia (IRSEA)'
        ];

        $teamResult = [
            "data" => [
                [
                    "programDbId" => 101,
                    "programCode" => "IRSEA",
                    "programName" => "Irrigated Southeast Asia"
                ]
            ],
            "status" => 200
        ];

        $this->listModel->tenantProgram->setReturnValue('getAll',$teamResult);

        // ACT
        $actual = $this->listModel->getPrograms();

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test validate designation input
    public function testValidateDesignationInput(){
        // ARRANGE
        $results = [
            'data' => [
                [
                    'germplasmDbId' => 1,
                    'germplasmNormalizedName' => "IRRI 123",
                    'inputProductName' => "IRRI 123",
                    'designation' => "IRRI 123",
                    'orderNumber' => 1
                ],
                [
                    
                    'germplasmDbId' => 2,
                    'germplasmNormalizedName' => "IRRI 154",
                    'inputProductName' => "IRRI 154",
                    'designation' => "IRRI 154",
                    'orderNumber' => 2
                ],
                [
                    'germplasmDbId' => 3,
                    'germplasmNormalizedName' => "IR 64",
                    'inputProductName' => "IR 64",
                    'designation' => "IR 64",
                    'orderNumber' => 3
                    
                ]
            ],
            'totalCount' => 3
        ];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $results['data'],
            'key' => 'germplasmDbId',
            'pagination' => false,
            'totalCount' => $results['totalCount'],
            'id' => 'dp-1'
        ]);
        $expected = [
            'dataProvider' => $dataProvider,
            'invalidItems' => [],
            'validItems' => $results['data'],
            'validCount' => $results['totalCount']
        ];

        $inputListSplitArr = ['IRRI 123','IRRI 154', 'IR 64'];
        $germplasmResult = [
            "data" => [
                [
                    'germplasmNormalizedName' => "IRRI 123",
                    'otherNames' => "IRRI 123",
                    'designation' => "IRRI 123",
                    'germplasmDbId' => 1
                ],
                [
                    'germplasmNormalizedName' => "IRRI 154",
                    'otherNames' => "IRRI 154",
                    'designation' => "IRRI 154",
                    'germplasmDbId' => 2
                ],
                [
                    'germplasmNormalizedName' => "IR 64",
                    'otherNames' => "IR 64",
                    'designation' => "IR 64",
                    'germplasmDbId' => 3
                ]
            ],
            "status" => 200,
            "totalCount" => 3
        ];
        
        $this->listModel->germplasm->setReturnValue('searchAll',$germplasmResult);

        // ACT
        $actual = $this->listModel->validateDesignationInput($inputListSplitArr);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($results['data'],$actual['dataProvider']->allModels);
        $this->assertEquals($results['totalCount'],$actual['validCount']);
        $this->assertEquals([],$actual['invalidItems']);
    }

    // test validate trait input
    public function testValidateTraitInput(){
        // ARRANGE
        $results = [
            'data' => [
                [
                    'name' => "Harvest Date",
                    'abbrev' => "HV_DATE",
                    'label' => "HV_DATE",
                    'name' => "Harvest Date",
                    'variableDbId' => 1,
                    'dataType' => 'date',
                    'inputTraitLabel' => 'HV_DATE',
                    'orderNumber' => 1
                ],
                [
                    'name' => "Seeding Date",
                    'abbrev' => "SEEDING_DATE",
                    'label' => "SEEDING_DATE",
                    'name' => "Seeding Date",
                    'variableDbId' => 2,
                    'dataType' => 'date',
                    'inputTraitLabel' => 'SEEDING_DATE',
                    'orderNumber' => 2
                ]
            ],
            'totalCount' => 2
        ];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $results['data'],
            'key' => 'variableDbId',
            'pagination' => false,
            'totalCount' => $results['totalCount'],
            'id' => 'dp-3'
        ]);
        $expected = [
            'dataProvider' => $dataProvider,
            'invalidItems' => [],
            'validItems' => $results['data'],
            'validCount' => $results['totalCount']
        ];

        $inputListSplitArr = ['HV_DATE','SEEDING_DATE'];
        $traitsResult = [
            "data" => [
                [
                    'name' => "Harvest Date",
                    'abbrev' => "HV_DATE",
                    'label' => "HV_DATE",
                    'variableDbId' => 1,
                    'dataType' => 'date'
                ],
                [
                    'name' => "Seeding Date",
                    'abbrev' => "SEEDING_DATE",
                    'label' => "SEEDING_DATE",
                    'variableDbId' => 2,
                    'dataType' => 'date'
                ]
            ],
            "status" => 200,
            "totalCount" => 2
        ];
        
        $this->listModel->traits->setReturnValue('searchAll',$traitsResult);

        // ACT
        $actual = $this->listModel->validateTraitInput($inputListSplitArr);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
        $this->assertEquals($results['data'],$actual['dataProvider']->allModels);
        $this->assertEquals($results['totalCount'],$actual['validCount']);
        $this->assertEquals([],$actual['invalidItems']);
    }

    // test validate input list with list type = trait
    public function testValidateInputListTraitListInput(){
        // ARRANGE
        $inputList = "HV_DATE,SEEDING_DATE";
        $id = 123;
        $listType = "trait";
        $program = "IRSEA";

        $traitsResults = [
            'data' => [
                [
                    'name' => "Harvest Date",
                    'abbrev' => "HV_DATE",
                    'label' => "HV_DATE",
                    'name' => "Harvest Date",
                    'variableDbId' => 1,
                    'dataType' => 'date',
                    'inputTraitLabel' => 'HV_DATE',
                    'orderNumber' => 1
                ],
                [
                    'name' => "Seeding Date",
                    'abbrev' => "SEEDING_DATE",
                    'label' => "SEEDING_DATE",
                    'name' => "Seeding Date",
                    'variableDbId' => 2,
                    'dataType' => 'date',
                    'inputTraitLabel' => 'SEEDING_DATE',
                    'orderNumber' => 2
                ]
            ],
            'totalCount' => 2
        ];

        $traitsResultMock = [
            "data" => [
                [
                    'name' => "Harvest Date",
                    'abbrev' => "HV_DATE",
                    'label' => "HV_DATE",
                    'name' => "Harvest Date",
                    'variableDbId' => 1,
                    'dataType' => 'date'
                ],
                [
                    'name' => "Seeding Date",
                    'abbrev' => "SEEDING_DATE",
                    'label' => "SEEDING_DATE",
                    'name' => "Seeding Date",
                    'variableDbId' => 2,
                    'dataType' => 'date'
                ]
            ],
            "status" => 200,
            "totalCount" => 2
        ];
        $this->listModel->traits->setReturnValue('searchAll',$traitsResultMock);

        // ACT
        $actual = $this->listModel->validateInputList($inputList, $id, $listType, $program);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($traitsResults['data'],$actual['returnArr']['dataProvider']->allModels);
        $this->assertEquals($traitsResults['totalCount'],$actual['returnArr']['validCount']);
        $this->assertEquals([],$actual['returnArr']['invalidItems']);
    }

    // test validate input list with list type = germplasm
    public function testValidateInputListGermplasmListInput(){
        // ARRANGE
        $inputList = "IRRI 123, IRRI 154, IR 64";
        $id = 123;
        $listType = "germplasm";
        $program = "IRSEA";

        $germplasmResults = [
            'data' => [
                [
                    'germplasmDbId' => 1,
                    'germplasmNormalizedName' => "IRRI 123",
                    'inputProductName' => "IRRI 123",
                    'designation' => "IRRI 123",
                    'orderNumber' => 1
                ],
                [
                    
                    'germplasmDbId' => 2,
                    'germplasmNormalizedName' => "IRRI 154",
                    'inputProductName' => "IRRI 154",
                    'designation' => "IRRI 154",
                    'orderNumber' => 2
                ],
                [
                    'germplasmDbId' => 3,
                    'germplasmNormalizedName' => "IR 64",
                    'inputProductName' => "IR 64",
                    'designation' => "IR 64",
                    'orderNumber' => 3
                    
                ]
            ],
            'totalCount' => 3
        ];

        $germplasmResultMock = [
            "data" => [
                [
                    'germplasmNormalizedName' => "IRRI 123",
                    'otherNames' => "IRRI 123",
                    'designation' => "IRRI 123",
                    'germplasmDbId' => 1
                ],
                [
                    'germplasmNormalizedName' => "IRRI 154",
                    'otherNames' => "IRRI 154",
                    'designation' => "IRRI 154",
                    'germplasmDbId' => 2
                ],
                [
                    'germplasmNormalizedName' => "IR 64",
                    'otherNames' => "IR 64",
                    'designation' => "IR 64",
                    'germplasmDbId' => 3
                ]
            ],
            "status" => 200,
            "totalCount" => 3
        ];
        $this->listModel->germplasm->setReturnValue('searchAll',$germplasmResultMock);

        // ACT
        $actual = $this->listModel->validateInputList($inputList, $id, $listType, $program);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($germplasmResults['data'],$actual['returnArr']['dataProvider']->allModels);
        $this->assertEquals($germplasmResults['totalCount'],$actual['returnArr']['validCount']);
        $this->assertEquals([],$actual['returnArr']['invalidItems']);
    }

    // test addMembersByInput
    public function testAddMembersByInputGermplasmList(){
        // ARRANGE
        $expected = 'Succesfully added 3 item/s to list.';

        $id = 35557;
        $listType = 'germplasm';

        $createMembersResultsMock = [
            "totalCount" => 3,
            "success" => true 
        ];
        $this->listModel->lists->setReturnValue('createMembers',$createMembersResultsMock);

        $getResultMock  = [
            "data" => [ 
                "listDbId" => 35557,
                "abbrev" => "HB2021WS001001_PLOT_01",
                "name" => "HB2021WS001001_plot_01",
                "displayName" => "HB2021WS001001_plot_01",
                "type" => "plot",
                "subType" => null,
                "entityId" => 3,
                "description" => "HB2021WS001001_plot_01",
                "remarks" => "HB2021WS001001_plot_01",
                "notes" => "Created by B4R API",
                "status" => "created",
                "isActive" => true,
                "creationTimestamp" => "2021-11-15T03:25:05.463Z",
                "creatorDbId" => 470,
                "creator" => "DelaRosa, Karen Mae",
                "modificationTimestamp" => null,
                "modifierDbId" => null,
                "modifier" => null,
                "accessData" => [
                    "user" => [
                        "470" => [
                            "addedBy" => 470,
                            "addedOn" => "2021-11-15T03:25:05.463Z",
                            "dataRoleId" => 25
                        ]
                    ]
                ],
                "listUsage" => "final list"
            ],
            "status" => 200
        ];
        $updateResultMock = [
            "metadata" => [
                "pagination" => [
                    "pageSize" => 100,
                    "totalCount" => 1,
                    "currentPage" => 1,
                    "totalPages" => 1
                ],
                "status" => [
                    [
                        "message" => "Request has been successfully completed.",
                        "messageType" => "INFO"
                    ]
                ],
                "datafiles" => []
            ],
            "result" => [
                "data" => [
                    [
                        "listDbId" => "35557",
                        "recordCount" => 1,
                        "href" => "https://cbapi.local/v3/lists/35557"
                    ]
                ]
            ]
        ];

        $this->listModel->lists->setReturnValue('getOne',$getResultMock);
        $this->listModel->lists->setReturnValue('updateOne',$updateResultMock);

        // Create mock
        $postParamsMock = [ 
            'productList' => '[
                {
                "germplasmDbId": 1,
                "designation": "IRRI 123"
                },
                {
                "germplasmDbId": 2,
                "designation": "IRRI 154"
                },
                {
                "germplasmDbId": 3,
                "designation": "IR 64"
                }
            ]'
        ];
        $request = $this->make('yii\web\Request',
            [
                'post' => function () use ($postParamsMock) {
                    return $postParamsMock;
                }
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // ACT
        $actual = $this->listModel->addMembersByInput($id, $listType);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test addMembersByInput
    public function testAddMembersByInputTraitList(){
        // ARRANGE
        $expected = 'Succesfully added 2 item/s to list.';

        $id = 35557;
        $listType = 'trait';

        $createMembersResultsMock = [
            "totalCount" => 2,
            "success" => true 
        ];
        $this->listModel->lists->setReturnValue('createMembers',$createMembersResultsMock);

        $getResultMock  = [
            "data" => [ 
                "listDbId" => 35557,
                "abbrev" => "HB2021WS001001_PLOT_01",
                "name" => "HB2021WS001001_plot_01",
                "displayName" => "HB2021WS001001_plot_01",
                "type" => "plot",
                "subType" => null,
                "entityId" => 3,
                "description" => "HB2021WS001001_plot_01",
                "remarks" => "HB2021WS001001_plot_01",
                "notes" => "Created by B4R API",
                "status" => "created",
                "isActive" => true,
                "creationTimestamp" => "2021-11-15T03:25:05.463Z",
                "creatorDbId" => 470,
                "creator" => "DelaRosa, Karen Mae",
                "modificationTimestamp" => null,
                "modifierDbId" => null,
                "modifier" => null,
                "accessData" => [
                    "user" => [
                        "470" => [
                            "addedBy" => 470,
                            "addedOn" => "2021-11-15T03:25:05.463Z",
                            "dataRoleId" => 25
                        ]
                    ]
                ],
                "listUsage" => "final list"
            ],
            "status" => 200
        ];
        $updateResultMock = [
            "metadata" => [
                "pagination" => [
                    "pageSize" => 100,
                    "totalCount" => 1,
                    "currentPage" => 1,
                    "totalPages" => 1
                ],
                "status" => [
                    [
                        "message" => "Request has been successfully completed.",
                        "messageType" => "INFO"
                    ]
                ],
                "datafiles" => []
            ],
            "result" => [
                "data" => [
                    [
                        "listDbId" => "35557",
                        "recordCount" => 1,
                        "href" => "https://cbapi.local/v3/lists/35557"
                    ]
                ]
            ]
        ];

        $this->listModel->lists->setReturnValue('getOne',$getResultMock);
        $this->listModel->lists->setReturnValue('updateOne',$updateResultMock);

        // Create mock
        $postParamsMock = [ 
            'productList' => '[
                {
                "variableDbId": 1,
                "label": "HV_DATE"
                },
                {
                "variableDbId": 2,
                "label": "SEEDING_DATE"
                }
            ]'
        ];
        $request = $this->make('yii\web\Request',
            [
                'post' => function () use ($postParamsMock) {
                    return $postParamsMock;
                }
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // ACT
        $actual = $this->listModel->addMembersByInput($id, $listType);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test getSeedListMemberColumns
    public function testGetSeedListMemberColumns(){
        // ARRANGE
        $model = [];
        $checkBoxId = 'boxId';
        $checkBoxClass = 'boxClass';
        $isPreview = false;
        $orderNumber = true;
        $isFinalList = true;

        $expected = [
            [
                'label'=> "checkbox",
                'visible'=> true,
                
                'header' => 'Reorder',

                'contentOptions' => ['style' => ['cursor' => 'pointer;',]],
                'content'=>function($model) {
                    return '
                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> true,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>

                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', "data-id"=>$data['id']];
                }
            ],
            [
                'attribute' => 'designation',
                'label' => 'Designation',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'label',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'gid',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'volume',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'source_study',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'source_year',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'container_label',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute'=>'creation_timestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];

        // ACT
        $actual = $this->listModel->getSeedListMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview, $orderNumber, $isFinalList);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test getLocationMemberColumns
    public function testGetLocationMemberColumns(){
        // ARRANGE
        $model = [];
        $checkBoxId = 'boxId';
        $checkBoxClass = 'boxClass';
        $isPreview = false;
        $orderNumber = true;
        $isFinalList = true;

        $expected = [

            [
                'label' => "checkbox",
                'visible' => true,
                'header' => 'Reorder',
                'contentOptions' => ['style' => ['cursor' => 'pointer;']],
                'content' => function($model) {
                    return '

                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'hiddenFromExport'=> true,
                'mergeHeader' => true,
                'width' => '50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> true,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', 'data-id' => $data['id']];
                }
            ],
            [
                'attribute' => 'abbrev',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'country',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'is_active',
                'label' => 'Status',
                'visible' => true,
                'header' => 'Status',
                'content' => function($data) {
                    return '<span class="badge '. (($data['is_active']) ? ' new">Active' : ' new badge amber darken-2">Draft') . '</span>';
                },
                'noWrap'=>false,
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>[
                    'true' =>'Active',
                    'false' =>'Draft',
                ], 
                'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true,'id'=>"location_active"],],
                'filterInputOptions'=>['placeholder'=>'status','id'=>"location_active"],
            ],
            [
                'attribute'=>'creation_timestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];

        // ACT
        $actual = $this->listModel->getLocationMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview, $orderNumber, $isFinalList);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test getPlotListMemberColumns
    public function testGetPlotListMemberColumns(){
        // ARRANGE
        $model = [];
        $checkBoxId = 'boxId';
        $checkBoxClass = 'boxClass';
        $isPreview = false;
        $orderNumber = true;
        $isFinalList = true;

        $expected = [
            [
                'label'=> "checkbox",
                'visible'=> true,
                'header' => 'Reorder',
                'contentOptions' => ['style' => ['cursor' => 'pointer;',]],
                'content'=>function($model) {
                    return '
                    
                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> true,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', "data-id"=>$data['id']];
                }
            ],
            [
                'attribute' => 'study',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'study_name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'product_name',
                'label' => 'Designation',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'code',
                'label' => 'Plot Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'plotno',
                'label' => 'Plot No.',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'entcode',
                'label' => 'Entry Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [   
                'attribute'=>'creation_timestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]

        ];

        // ACT
        $actual = $this->listModel->getPlotListMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview, $orderNumber, $isFinalList);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test getStudyMemberColumns
    public function testGetStudyMemberColumns(){
        // ARRANGE
        $model = [];
        $checkBoxId = 'boxId';
        $checkBoxClass = 'boxClass';
        $isPreview = false;
        $orderNumber = true;
        $isFinalList = true;

        $expected = [
            [
                'label'=> "checkbox",
                'visible'=> true,
                'header' => 'Reorder',
                'contentOptions' => ['style' => ['cursor' => 'pointer;',]],
                'content'=>function($model) {
                    return '
                    
                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> true,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', "data-id"=>$data['id']];
                }
            ],
            [
                'attribute' => 'program',
                'label' => 'Program',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'study_name',
                'label' => 'Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'study',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'place',
                'label' => 'Location',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'phase',
                'label' => 'Phase',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'year',
                'label' => 'Year',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'season',
                'label' => 'Season',
                'visible' => true,
                'format' => 'raw'
            ],
            [   
                'attribute'=>'creation_timestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];

        // ACT
        $actual = $this->listModel->getStudyMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview, $orderNumber, $isFinalList);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test getDesignationMemberColumns
    public function testGetDesignationMemberColumns(){
        // ARRANGE
        $model = [];
        $checkBoxId = 'boxId';
        $checkBoxClass = 'boxClass';
        $isPreview = false;
        $orderNumber = true;
        $isFinalList = true;

        $expected = [
            [
                'label' => 'checbox',
                'visible' => true,
                'header' => 'Reorder',
                'contentOptions' => ['style' => ['cursor' => 'pointer;']],
                'content' => function($model) {
                    return '
                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'hiddenFromExport' => true,
                'mergeHeader' => true,
                'width' => '50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> true,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', "data-id"=>$data['id']];
                }
            ],
            [
                'attribute' => 'designation',
                'label' => 'Designation',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'generation',
                'label' => 'Generation',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'other_names',
                'label' => 'Other Names',
                'visible' => true,
                'format' => 'raw',
                'value' => function ($data) {

                    if (strlen($data['other_names']) > 30) {
                        $displayNames = substr($data['other_names'],0,30);
                        $otherNames = $data['other_names'];

                        $display = '

                            <ul class="collapsible">
                                <li>
                                  <div class="collapsible-header" style="padding:0.5rem; color: #26a69a;">'.$displayNames.'</div>
                                  <div class="collapsible-body">'.$otherNames.'</div>
                                </li>
                              </ul>

                        ';

                    } else {
                        $display = $data['other_names'];
                    }

                    return $display;

                }
            ]
        ];

        // ACT
        $actual = $this->listModel->getDesignationMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview, $orderNumber, $isFinalList);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test addToPlotList
    public function testAddToPlotList(){
        // ARRANGE
        $expected = 3;

        $id = 1;
        $selectedStudies = ['1','2','3'];
        $selectedData = [
            '1' => "1,2,3",
            '2' => "1,2,3",
            '3' => "1,2,3"
        ];

        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1,
                                    'totalCount' => 3
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    ['plotDbId' => 1, 'code' => '1'],
                                    ['plotDbId' => 2, 'code' => '2'],
                                    ['plotDbId' => 3, 'code' => '3']
                                ]
                            ]
                        ],
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        // ACT
        $actual = $this->listModel->addToPlotList($id, $selectedStudies, $selectedData);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // test updateListStatus
    public function testUpdateListStatus(){
        // ARRANGE
        $expected = true;

        $selectedListIds = [1,2,3];
        $status = 'in use';

        $results = [
            'data' => [
                ['listDbId' => 1, 'abbrev' => 'LIST_1'],
                ['listDbId' => 2, 'abbrev' => 'LIST_2'],
                ['listDbId' => 3, 'abbrev' => 'LIST_3']
            ]
        ];
        $this->listModel->lists->setReturnValue('searchAll',$results);
        $this->listModel->lists->setReturnValue('updateOne',true);

        // ACT
        $actual = $this->listModel->updateListStatus($selectedListIds,$status);

        // ASSERT
        $this->assertNotEmpty($actual);
        $this->assertEquals($expected,$actual);
    }

    // tests rules()
    public function testRules()
    {
        //arrange
        $expected = [
            [['abbrev', 'name', 'country', 'status', 'generation', 'parentage', 'designation', 'label', 'gid', 'volume', 'study','study_name','studyName','place','phase', 'year', 'season', 'entry_count', 'plot_count','program', 'plotno', 'code', 'entcode', 'product_name','source_study', 'source_year', 'container_label', 'is_active', 'experiment', 'experimentOccurrence', 'seedName', 'seedSourcePlotCode',
            'quantity', 'unit', 'plotCode', 'plotNumber', 'entryCode', 'entryNumber', 'occurrenceName'
            ], 'safe'],
            [['is_void'], 'boolean'],
        ];

        //act
        $actual = $this->listModel->rules();

        //assert
        $this->assertEquals($expected, $actual);
    }

    // tests formName()
    public function testFormName()
    {
        //arrange
        $expected = "List";

        //act
        $actual = $this->listModel->formName();

        //assert
        $this->assertEquals($expected, $actual);

    }

    // tests validateCreateForm()
    public function testValidateCreateFormValidInput()
    {
        //arrange
        $expected = [
            'list_name' => 'list 1',
            'list_id' => 1,
            'success' => true,
            'message' => "List: <b>list 1</b> successfully updated."
        ];
        $id = '1';

        $result = [
            'status' => 200,
            'data' => [
                ['listDbId' => 1, 'abbrev' => 'LIST_1']
            ]
        ];

        $results = [
            'status' => 200,
            'data' => [
                ['listDbId' => 1, 'abbrev' => 'LIST_1']
            ]
        ];

        $postParams = [
            'Basic' => [
                'abbrev' => 'LIST_1',
                'name' => 'list 1',
                'display_name' => 'list 1',
                'type' => 'package',
                'subType' => 'source list',
                'description' => '',
                'remarks' => ''
            ]
        ];

        $request = $this->make('yii\web\Request',
            [
                'post' => function () use ($postParams) {
                    return $postParams;
                }
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        $this->listModel->lists->setReturnValue('updateOne',$result);
        $this->listModel->lists->setReturnValue('create',$results);

        //act
        $actual = $this->listModel->validateCreateForm($id);

        //assert
        $this->assertEquals($expected, $actual);
    }

    // tests validateCreateForm()
    public function testValidateCreateFormNullBasicInfo()
    {
        //arrange
        $expected =null;
        $id = '1';

        $request = $this->make('yii\web\Request',
            [
                'post' => function () {
                    return [];
                }
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        //act
        $actual = $this->listModel->validateCreateForm($id);

        //assert
        $this->assertEquals($expected, $actual);
    }
}