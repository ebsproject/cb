<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\account\models;

use app\dataproviders\ArrayDataProvider;
use phpDocumentor\Reflection\PseudoTypes\False_;
use Yii;

class ListAccessSearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $listAccessSearchModel;
    
    protected function _before()
    {
        $this->listAccessSearchModel = Yii::$container->get('listAccessSearchModel');
    }

    protected function _after()
    {
    }

    // tests getExistingAccessIds()
    public function testGetExistingAccessIds(){
        //ARRANGE
        $dataProvider = (object) [
            'allModels' => [
                ['userdbid' => 1, 'username' => 'doe, jane'],
                ['userdbid' => 2, 'username' => 'doe, john'],
                ['userdbid' => 3, 'username' => 'doe, jessica']
            ]
        ];

        $expected = [1,2,3]; 

        //ACT
        $actual = $this->listAccessSearchModel->getExistingAccessIds($dataProvider);

        //ASSERT
        $this->assertEquals($expected, $actual);
    }

    // tests search()
    public function testSearch(){
        // ARRANGE
        $expected = true;

        $params = [];
        $listId = 1;

        $this->listAccessSearchModel->user->setReturnValue('getUserId', 1);
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'result' => [
                                'data' => [
                                    ['entity' => 'user', 'entityDbId' => '1'],
                                    ['entity' => 'user', 'entityDbId' => '2'],
                                    ['entity' => 'user', 'entityDbId' => '3']
                                ]
                            ]
                        ],
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        //ACT
        $actual = !empty($this->listAccessSearchModel->search($params, $listId)) ? true : false;

        //ASSERT
        $this->assertEquals($expected, $actual);
    }
}
?>