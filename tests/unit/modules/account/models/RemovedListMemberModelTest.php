<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\account\models;

use Yii;

class RemovedListMemberModelTest extends \Codeception\Test\Unit
{
    use \Codeception\AssertThrows;
    /* @var \UnitTester
    */
   protected $tester;
   protected $removedListMemberModel;

   protected function _before()
   {
       $this->removedListMemberModel = Yii::$container->get('removedListMemberModel');
       $this->userDashboardConfigMock = \Yii::$container->get('userDashboardConfigMock');
   }

   protected function _after()
   {
   }

   // Tests
   public function testSearch() 
   {
        //arrange
        $type = '';
        $id = 1;
        $isVoid = null;
        $params = [];
        $pagination = false;
        $isView = false;
        $searchModel='List';

        $expected = true;

        // mocks
        $this->removedListMemberModel = $this->construct(
            $this->removedListMemberModel,
            [
                "userDashboardConfig" => $this->userDashboardConfigMock
            ]
        );

        $gridPageSize = 2;

        $request = $this->make('yii\web\Request',
            [
                'get' => function () {
                    return [
                        'dp-1-page' => '2'
                    ];
                },
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->removedListMemberModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1,
                                    'totalCount' => 3,
                                    'currentPage' => 1,
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    ['packageDbId' => 1, 'packageCode' => '1'],
                                    ['packageDbId' => 2, 'packageCode' => '2'],
                                    ['packageDbId' => 3, 'packageCode' => '3']
                                ]
                            ]
                        ],
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        //act
        $actual = !empty($this->removedListMemberModel->search($type, $id, $isVoid, $params, $pagination, $isView, $searchModel)) ? true : false;

        //assert
        $this->assertTrue($actual);
    }

    public function testSearchWithVoid() 
    {
        //arrange
        $type = '';
        $id = 1;
        $isVoid = false;
        $params = [];
        $pagination = false;
        $isView = false;
        $searchModel='List';

        $expected = true;
 
        // mocks
        $this->removedListMemberModel = $this->construct(
            $this->removedListMemberModel,
            [
                "userDashboardConfig" => $this->userDashboardConfigMock
            ]
        );

        $gridPageSize = 2;
 
        $request = $this->make('yii\web\Request',
            [
                'get' => function () {
                    return [
                        'dp-1-page' => '2'
                    ];
                },
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);
 
        // Mock page limit
        $this->removedListMemberModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);
 
        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1,
                                    'totalCount' => 3,
                                    'currentPage' => 1,
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    ['packageDbId' => 1, 'packageCode' => '1'],
                                    ['packageDbId' => 2, 'packageCode' => '2'],
                                    ['packageDbId' => 3, 'packageCode' => '3']
                                ]
                            ]
                        ],
                    ];
                }
            ]
        );
 
        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);
 
        //act
        $actual = !empty($this->removedListMemberModel->search($type, $id, $isVoid, $params, $pagination, $isView, $searchModel)) ? true : false;
 
        //assert
        $this->assertTrue($actual);
    }

}
