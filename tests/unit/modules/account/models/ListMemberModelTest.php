<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace modules\account\models;

use app\dataproviders\ArrayDataProvider;
use Yii;

class ListMemberModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $listMemberModel;
    protected $userDashboardConfigMock;
    protected $userModel;
    protected $backgroundJobModel;
    protected $variableModel;
    protected $configModel;
    protected $personModel;
    protected $programModel;
    protected $configuration;

    protected function _before()
    {
        $this->listMemberModel = Yii::$container->get('listMemberModel');
        $this->userDashboardConfigMock = \Yii::$container->get('userDashboardConfigMock');
        $this->userModel = Yii::$container->get('user');
        $this->backgroundJobModel = Yii::$container->get('backgroundJob');
        $this->variableModel = Yii::$container->get('variable');
        $this->configModel = Yii::$container->get('configModelMock');
        $this->personModel = Yii::$container->get('person');
        $this->programModel = Yii::$container->get('program');
        $this->configuration = Yii::$container->get('config');
    }

    protected function _after()
    {
    }

    // tests
    public function testPlotListMemberColumn()
    {
        //arrange
        $checkBoxId = "final-grid-select-all-id"; 
        $checkBoxClass = "final-grid_select";

        $params = [
            "creationTimestamp" => "2021-11-15T05:48:44.269Z",
            "creator" => "Panzo, Omner Neal",
            "creatorDbId" => 495, 
            "description" => "",
            "displayName" => "samp-plot",
            "entityId" => 3,
            "isActive" => true,
            "listDbId" => 35235,
            "listUsage" => "final list",
            "message" => "Request has been successfully completed.",
            "modificationTimestamp" => "2021-11-17T08:40:13.622Z",
            "modifier" => "Panzo, Omner Neal",
            "modifierDbId" => 495,
            "name" => "samp-plot",
            "notes" => "Created by B4R API",
            "remarks" => "",
            "status" => "created",
            "subType" => null,
            "success" => true,
            "type" => "plot"
        ];       

        $expected = true; 

        // mocks
        $this->listMemberModel = $this->construct(
            $this->listMemberModel,
            [
                "user" => $this->userModel,
                "backgroundJob" => $this->backgroundJobModel,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "variableModel" => $this->variableModel,
                "configModel" => $this->configModel,
                "personModel" => $this->personModel,
                "programModel" => $this->programModel,
                "configuration" => $this->configuration
            ]
        );

        //act
        $actual = ($this->listMemberModel->getPlotListMemberColumns($params,$checkBoxId, $checkBoxClass, $isPreview=true, $orderNumber=true,$isFinalist=true))? true : false;

        //assert
        $this->assertEquals($expected, $actual);
    }

    // tests getDesignationMemberColumns
    public function testGetDesignationMemberColumns()
    {
        //arrange
        $checkBoxId = "final-grid-select-all-id"; 
        $checkBoxClass = "final-grid_select";

        $model = [
            "creationTimestamp" => "2021-11-15T05:48:44.269Z",
            "creator" => "Doe, Jane",
            "creatorDbId" => 495, 
            "description" => "",
            "displayName" => "samp-designation",
            "entityId" => 3,
            "isActive" => true,
            "listDbId" => 35235,
            "listUsage" => "final list",
            "message" => "Request has been successfully completed.",
            "modificationTimestamp" => "2021-11-17T08:40:13.622Z",
            "modifier" => "Doe, Jane",
            "modifierDbId" => 495,
            "name" => "samp-designation",
            "notes" => "Created by B4R API",
            "remarks" => "",
            "status" => "created",
            "subType" => null,
            "success" => true,
            "type" => "germplasm"
        ];       

        $expected = true; 

        // mocks
        $this->listMemberModel = $this->construct(
            $this->listMemberModel,
            [
                "user" => $this->userModel,
                "backgroundJob" => $this->backgroundJobModel,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "variableModel" => $this->variableModel,
                "configModel" => $this->configModel,
                "personModel" => $this->personModel,
                "programModel" => $this->programModel,
                "configuration" => $this->configuration
            ]
        );

        //act
        $actual = ($this->listMemberModel->getDesignationMemberColumns($model,$checkBoxId,$checkBoxClass,false,true,true))? true : false;

        //assert
        $this->assertEquals($expected, $actual);
    }

    // tests search()
    public function testSearch(){
        //arrange
        $type = '';
        $id = 1;
        $isVoid = null;
        $params = [];
        $pagination = false;
        $isView = false;
        $searchModel='List';

        $expected = true;

        // mocks
        $this->listMemberModel = $this->construct(
            $this->listMemberModel,
            [
                "user" => $this->userModel,
                "backgroundJob" => $this->backgroundJobModel,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "variableModel" => $this->variableModel,
                "configModel" => $this->configModel,
                "personModel" => $this->personModel,
                "programModel" => $this->programModel,
                "configuration" => $this->configuration
            ]
        );

        $gridPageSize = 2;

        $request = $this->make('yii\web\Request',
            [
                'get' => function () {
                    return [
                        'dp-1-page' => '2'
                    ];
                },
            ]
        );
        // Inject request mock into Yii::$app
        \Yii::$app->set('request',$request);

        // Mock page limit
        $this->listMemberModel->userDashboardConfig->setReturnValue('getDefaultPageSizePreferences', $gridPageSize);

        $api = $this->make('app\components\Api',
            [
                'getResponse' => function($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
                    return [
                        'status' => 200,
                        'body' => [
                            'metadata' => [
                                'pagination' => [
                                    'totalPages' => 1,
                                    'totalCount' => 3,
                                    'currentPage' => 1,
                                ]
                            ],
                            'result' => [
                                'data' => [
                                    ['packageDbId' => 1, 'packageCode' => '1'],
                                    ['packageDbId' => 2, 'packageCode' => '2'],
                                    ['packageDbId' => 3, 'packageCode' => '3']
                                ]
                            ]
                        ],
                    ];
                }
            ]
        );

        // Inject user mock into Yii::$app
        \Yii::$app->set('api',$api);

        //act
        $actual = !empty($this->listMemberModel->search($type, $id, $isVoid, $params, $pagination, $isView, $searchModel)) ? true : false;

        //assert
        $this->assertTrue($actual);
    }

    public function testSeedListMemberColumns()
    {
        //arrange
        $checkBoxId = "final-grid-select-all-id"; 
        $checkBoxClass = "final-grid_select";

        $params = [
            "creationTimestamp" => "2021-11-15T05:48:44.269Z",
            "creator" => "Panzo, Omner Neal",
            "creatorDbId" => 495, 
            "description" => "",
            "displayName" => "samp-plot",
            "entityId" => 3,
            "isActive" => true,
            "listDbId" => 35235,
            "listUsage" => "final list",
            "message" => "Request has been successfully completed.",
            "modificationTimestamp" => "2021-11-17T08:40:13.622Z",
            "modifier" => "Panzo, Omner Neal",
            "modifierDbId" => 495,
            "name" => "samp-plot",
            "notes" => "Created by B4R API",
            "remarks" => "",
            "status" => "created",
            "subType" => null,
            "success" => true,
            "type" => "plot"
        ];       

        $expected = true; 

        // mocks
        $this->listMemberModel = $this->construct(
            $this->listMemberModel,
            [
                "user" => $this->userModel,
                "backgroundJob" => $this->backgroundJobModel,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "variableModel" => $this->variableModel,
                "configModel" => $this->configModel,
                "personModel" => $this->personModel,
                "programModel" => $this->programModel,
                "configuration" => $this->configuration
            ]
        );

        //act
        $actual = ($this->listMemberModel->getSeedListMemberColumns($params,$checkBoxId, $checkBoxClass, $isPreview=true, $orderNumber=true,$isFinalist=true))? true : false;

        //assert
        $this->assertEquals($expected, $actual);
    }

    public function testLocationMemberColumns()
    {
        //arrange
        $checkBoxId = "final-grid-select-all-id"; 
        $checkBoxClass = "final-grid_select";

        $params = [
            "creationTimestamp" => "2021-11-15T05:48:44.269Z",
            "creator" => "Panzo, Omner Neal",
            "creatorDbId" => 495, 
            "description" => "",
            "displayName" => "samp-plot",
            "entityId" => 3,
            "isActive" => true,
            "listDbId" => 35235,
            "listUsage" => "final list",
            "message" => "Request has been successfully completed.",
            "modificationTimestamp" => "2021-11-17T08:40:13.622Z",
            "modifier" => "Panzo, Omner Neal",
            "modifierDbId" => 495,
            "name" => "samp-plot",
            "notes" => "Created by B4R API",
            "remarks" => "",
            "status" => "created",
            "subType" => null,
            "success" => true,
            "type" => "plot"
        ];       

        $expected = true; 

        // mocks
        $this->listMemberModel = $this->construct(
            $this->listMemberModel,
            [
                "user" => $this->userModel,
                "backgroundJob" => $this->backgroundJobModel,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "variableModel" => $this->variableModel,
                "configModel" => $this->configModel,
                "personModel" => $this->personModel,
                "programModel" => $this->programModel,
                "configuration" => $this->configuration
            ]
        );

        //act
        $actual = ($this->listMemberModel->getLocationMemberColumns($params,$checkBoxId, $checkBoxClass, $isPreview=true, $orderNumber=true,$isFinalist=true))? true : false;

        //assert
        $this->assertEquals($expected, $actual);
    }

    public function testStudyMemberColumns()
    {
        //arrange
        $checkBoxId = "final-grid-select-all-id"; 
        $checkBoxClass = "final-grid_select";

        $params = [
            "creationTimestamp" => "2021-11-15T05:48:44.269Z",
            "creator" => "Panzo, Omner Neal",
            "creatorDbId" => 495, 
            "description" => "",
            "displayName" => "samp-plot",
            "entityId" => 3,
            "isActive" => true,
            "listDbId" => 35235,
            "listUsage" => "final list",
            "message" => "Request has been successfully completed.",
            "modificationTimestamp" => "2021-11-17T08:40:13.622Z",
            "modifier" => "Panzo, Omner Neal",
            "modifierDbId" => 495,
            "name" => "samp-plot",
            "notes" => "Created by B4R API",
            "remarks" => "",
            "status" => "created",
            "subType" => null,
            "success" => true,
            "type" => "plot"
        ];       

        $expected = true; 

        // mocks
        $this->listMemberModel = $this->construct(
            $this->listMemberModel,
            [
                "user" => $this->userModel,
                "backgroundJob" => $this->backgroundJobModel,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "variableModel" => $this->variableModel,
                "configModel" => $this->configModel,
                "personModel" => $this->personModel,
                "programModel" => $this->programModel,
                "configuration" => $this->configuration
            ]
        );

        //act
        $actual = ($this->listMemberModel->getStudyMemberColumns($params,$checkBoxId, $checkBoxClass, $isPreview=true, $orderNumber=true,$isFinalist=true))? true : false;

        //assert
        $this->assertEquals($expected, $actual);
    }

    public function testTraitMemberColumns()
    {
        //arrange
        $checkBoxId = "final-grid-select-all-id"; 
        $checkBoxClass = "final-grid_select";

        $params = [
            "creationTimestamp" => "2021-11-15T05:48:44.269Z",
            "creator" => "Panzo, Omner Neal",
            "creatorDbId" => 495, 
            "description" => "",
            "displayName" => "samp-plot",
            "entityId" => 3,
            "isActive" => true,
            "listDbId" => 35235,
            "listUsage" => "final list",
            "message" => "Request has been successfully completed.",
            "modificationTimestamp" => "2021-11-17T08:40:13.622Z",
            "modifier" => "Panzo, Omner Neal",
            "modifierDbId" => 495,
            "name" => "samp-plot",
            "notes" => "Created by B4R API",
            "remarks" => "",
            "status" => "created",
            "subType" => null,
            "success" => true,
            "type" => "plot"
        ];       

        $expected = true; 

        // mocks
        $this->listMemberModel = $this->construct(
            $this->listMemberModel,
            [
                "user" => $this->userModel,
                "backgroundJob" => $this->backgroundJobModel,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "variableModel" => $this->variableModel,
                "configModel" => $this->configModel,
                "personModel" => $this->personModel,
                "programModel" => $this->programModel,
                "configuration" => $this->configuration
            ]
        );

        //act
        $actual = ($this->listMemberModel->getTraitMemberColumns($params,$checkBoxId, $checkBoxClass, $isPreview=true, $orderNumber=true,$isFinalist=true))? true : false;

        //assert
        $this->assertEquals($expected, $actual);
    }
    public function testPackageMemberColumns()
    {
        //arrange
        $checkBoxId = "final-grid-select-all-id"; 
        $checkBoxClass = "final-grid_select";

        $params = [
            "creationTimestamp" => "2021-11-15T05:48:44.269Z",
            "creator" => "Panzo, Omner Neal",
            "creatorDbId" => 495, 
            "description" => "",
            "displayName" => "samp-plot",
            "entityId" => 3,
            "isActive" => true,
            "listDbId" => 35235,
            "listUsage" => "final list",
            "message" => "Request has been successfully completed.",
            "modificationTimestamp" => "2021-11-17T08:40:13.622Z",
            "modifier" => "Panzo, Omner Neal",
            "modifierDbId" => 495,
            "name" => "samp-plot",
            "notes" => "Created by B4R API",
            "remarks" => "",
            "status" => "created",
            "subType" => null,
            "success" => true,
            "type" => "plot"
        ];       

        $expected = true; 

        // mocks
        $this->listMemberModel = $this->construct(
            $this->listMemberModel,
            [
                "user" => $this->userModel,
                "backgroundJob" => $this->backgroundJobModel,
                "userDashboardConfig" => $this->userDashboardConfigMock,
                "variableModel" => $this->variableModel,
                "configModel" => $this->configModel,
                "personModel" => $this->personModel,
                "programModel" => $this->programModel,
                "configuration" => $this->configuration
            ]
        );

        //act
        $actual = ($this->listMemberModel->getPackageMemberColumns($params,$checkBoxId, $checkBoxClass, $isPreview=true, $orderNumber=true,$isFinalist=true))? true : false;

        //assert
        $this->assertEquals($expected, $actual);
    }

}