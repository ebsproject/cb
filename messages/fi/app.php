<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */

return [
'(not set)' =>'(ei valittu)',
'Account' =>'Tili',
'Add Access' =>'Lisää pääsy',
'Add new season' =>'Lisää uusi kausi',
'Add to team' =>'Lisää joukkueeseen',
'Add widget' =>'Lisää widget',
'Add widgets' =>'Lisää widgettiä',
'Additional information' =>'Lisäinformaatio',
'Always show left nav' =>'Näytä aina vasemmalle nav',
'Alt' =>'alt',
'Apply' =>'Käytä',
'Basic' =>'perustiedot',
'Basic and additional information of germplasm' =>'Perustiedot ja lisätiedot ituplasmasta',
'Basic and additional information of product' =>'Tuotteen perustiedot ja lisätiedot',
'Basic and additional information of seed lot' =>'Siemenerän perustiedot ja lisätiedot',
'Basic and additional information of study' =>'Perustiedot ja lisätietoja opiskelusta',
'Basic information' =>'Perustiedot',
'Below are the crosses and their progenies based from your dashboard filters.' =>'Alla ovat ristit ja niiden jälkeläiset, jotka perustuvat kojelaudan suodattimiin.',
'Below are the crosses where the product was used as a parent.' =>'Alla ovat ristit, joissa tuotetta käytettiin vanhempana.',
'Below are the derived observation data history of the product.' =>'Seuraavassa on tuotteen johdettu havainto-tietojen historia.',
'Below are the progenies of the cross' =>'Alla on ristin jälkeläisiä',
'Below are the studies where the product was used as an entry.' =>'Alla ovat tutkimukset, joissa tuotetta käytettiin merkinnöinä.',
'Below are the studies where the product was used as entry' =>'Alla ovat tutkimukset, joissa tuotetta käytettiin merkinnöissä',
'Below are the teams where you belong to. Click on a team to view team members.' =>'Alla ovat joukkueet, joihin kuulut. Napsauta tiimiä nähdäksesi tiimin jäsenet.',
'Below displays all the mandatory variables.' =>'Alla näytetään kaikki pakolliset muuttujat.',
'Below displays all the numeric observation variables.' =>'Alla näytetään kaikki numeeriset havainnointimuuttujat.',
'Below displays all the string observation variables.' =>'Alla näytetään kaikki merkkijonon havainnointimuuttujat.',
'Below displays basic and {source} information about the germplasm.' =>'Alla näytetään perus- ja {source} -tiedot itämerkeistä.',
'Below displays basic and {source} information about the product.' =>'Alla näytetään perustiedot ja {source} tietoja tuotteesta.',
'Below displays basic entry list information of the study.' =>'Alla näytetään tutkimuksen perusluettelotiedot.',
'Below displays basic information and additional metadata about the seed storage.' =>'Alla näytetään perustiedot ja lisää metatietoja siemenvarastosta.',
'Below displays basic information and additional metadata about the study.' =>'Alla näytetään perustiedot ja lisää metatietoja tutkimuksesta.',
'Below displays basic information of the person.' =>'Alla näytetään henkilön perustiedot.',
'Below displays basic plot list information of the study.' =>'Alla näytetään tutkimuksen perustiedot tonttiluettelosta.',
'Below displays basic, teams and roles information of the user.' =>'Alla näytetään käyttäjän perustiedot, tiimit ja roolit.',
'Below displays known names to product.' =>'Alla näytetään tunnetut nimet tuotteeseen.',
'Below displays more information about the shipment and lists the seed lots that belong to it.' =>'Alla näytetään lisätietoja lähetyksestä ja luetellaan siihen kuuluvat siemenerät.',
'Below displays observation data history of the seed lot record.' =>'Alla näytetään siemenmäärän tietueet.',
'Below displays passport data of product.' =>'Alla näytetään tuotteen passitiedot.',
'Below displays passport data of the line.' =>'Alla näytetään rivin passi.',
'Below displays pedigree tree of the line.' =>'Alla näytetään rivin rodun puu.',
'Below displays pedigree tree of the product.' =>'Alla näytetään tuotteen sukupuuhun.',
'Below displays usage history related to volume changes made to the seed lot record.' =>'Alla näytetään käyttökorvaus, joka liittyy siemenmäärätilaan tehtyihin tilavuuden muutoksiin.',
'Cancel' =>'Peruuttaa',
'Click to view more information' =>'Napsauta nähdäksesi lisätietoja',
'Click to view team members.' =>'Napsauta nähdäksesi tiimin jäsenet.',
'Close' =>'kiinni',
'Collapse' =>'Romahdus',
'Configure' =>'Määritä',
'Configure Dashboard' =>'Määritä hallintapaneeli',
'Configure dashboard' =>'Määritä hallintapaneeli',
'Configure dashboard' =>'Määritä hallintapaneeli',
'Confirm' =>'Vahvistaa',
'container name' =>'kontin nimi',
'Containers' =>'kontit',
'Containers' =>'kontit',
'Create another' =>'Luo toinen',
'Create fixed lines' =>'Luo kiinteät linjat',
'Create fixed lines' =>'Luo kiinteät linjat',
'Create seed lots from study' =>'Luo siemenosat tutkimuksesta',
'Create seed lots from study' =>'Luo siemenosat tutkimuksesta',
'creator display name' =>'luovan näytön nimen',
'Crosses' =>'Ristit',
'Crosses where the line was used as a parent' =>'Ristit, joissa linjaa käytettiin vanhempana',
'Crosses where the product was used as a parent' =>'Ristit, joissa tuotetta käytettiin vanhempana',
'Current filters' =>'Nykyiset suodattimet',
'Dashboard' =>'kojelauta',
'Data collection' =>'Tiedonkeruu',
'Data collection' =>'Tiedonkeruu',
'Data collection & QC' =>'Tietojen keruu ja QC',
'Data export' =>'Tietojen vienti',
'Data export' =>'Tietojen vienti',
'Data filters' =>'Tietosuodattimet',
'Data migration' =>'Tiedonsiirto',
'Data migration' =>'Tiedonsiirto',
'Data reports' =>'Tietoraportit',
'Data reports' =>'Tietoraportit',
'Data viewer' =>'Tietojen katseluohjelma',
'Data viewer' =>'Tietojen katseluohjelma',
'Date dispatched' =>'Lähetyspäivä',
'Delete filter' =>'Poista suodatin',
'Delete this filter.' =>'Poista tämä suodatin.',
'Description' =>'Kuvaus',
'design' =>'design',
'designation' =>'nimitys',
'Displays list of recently used applications' =>'Näyttää äskettäin käytetyt sovellukset',
'Don\'t ask me again.' =>'Älä kysy uudelleen.',
'Download' =>'ladata',
'Draft studies' =>'Luonnokset tutkimuksille',
'Draft studies' =>'Luonnokset tutkimuksille',
'Drag and drop to reorder items.' =>'Vedä ja pudota kohteita uudelleen järjestämiseksi.',
'Edit layout' =>'Muokkaa ulkoasua',
'Email' =>'Sähköposti',
'email' =>'sähköposti',
'Entity' =>'Entity',
'Entries' =>'merkinnät',
'Entries of study' =>'Opinnot',
'Entry' =>'merkintä',
'exactly \'foo bar\' word' =>'täsmälleen \ "foo bar \" sana',
'Examples' =>'esimerkit',
'Export all data to csv file' =>'Vie kaikki tiedot csv-tiedostoon',
'Facilities' =>'laitteisto',
'Facilities' =>'laitteisto',
'facility name' =>'laitoksen nimi',
'Families' =>'perheet',
'Favorites' =>'suosikit',
'field and then pressing the enter key or clicking the search icon. Below are the supported search syntax.' =>'kenttään ja paina Enter-näppäintä tai napsauttamalla hakukuvaketta. Alla on tuettu hakusyntaksi.',
'Filter Studies' =>'Suodatustutkimukset',
'Filter Study' =>'Suodatustutkimus',
'Filters' =>'Suodattimet',
'Filters Selected' =>'Suodattimet valittu',
'First' =>'Ensimmäinen',
'first name' =>'etunimi',
'Full name' =>'Koko nimi',
'Gearman' =>'Gearman',
'Gearman' =>'Gearman',
'General' =>'yleinen',
'generation' =>'sukupolvi',
'Germplasm' =>'germplasm',
'GID' =>'GID',
'Go back' =>'Mene takaisin',
'Go back to dashboard' =>'Palaa hallintapaneeliin',
'GRC Seed list' =>'GRC-siemenluettelo',
'GRC Seed list' =>'GRC-siemenluettelo',
'Help' =>'auta',
'Hide left nav' =>'Piilota vasen nav',
'Hide search' =>'Piilota haku',
'institute name or abbrev (person)' =>'instituutin nimi tai lyhyt (henkilö)',
'is a cloud-based breeding information management system. It provides applications for data management and knowledge work activities enabling a high-quality experience for decision-making processes.' =>'on pilviperusteinen jalostustietojen hallintajärjestelmä. Se tarjoaa sovelluksia tiedonhallintaan ja osaamistoimintaan, mikä mahdollistaa korkealaatuisen kokemuksen päätöksentekoprosesseista.',
'Known names to product' =>'Tunnetut tuotenimet',
'label' =>'etiketti',
'last name' =>'sukunimi',
'Layouts' =>'ulkoasuja',
'Layouts' =>'ulkoasuja',
'Location' =>'Sijainti',
'location name or abbrev' =>'sijainnin nimi tai lyhyt',
'Locations' =>'Toimipaikat',
'Locations' =>'Toimipaikat',
'Logout' =>'Kirjautua ulos',
'More actions' =>'Lisää toimintaa',
'My profile' =>'Profiilini',
'Name' =>'Nimi',
'name type' =>'nimen tyyppi',
'Names' =>'nimet',
'Next' =>'Seuraava',
'Not set' =>'Ei valittu',
'Number of members' =>'Jäsenmäärä',
'Observation' =>'havainto',
'Observation data history of the product' =>'Tuotteen havainnotiedot',
'Observation data of seed storage' =>'Siemennesteen havainnot',
'Observation Summary' =>'Havaintojen yhteenveto',
'Observations' =>'havaintoja',
'Ok' =>'kunnossa',
'Operational studies' =>'Operatiiviset tutkimukset',
'Operational studies' =>'Operatiiviset tutkimukset',
'OU Request No.' =>'OU-pyynnön nro',
'Passport' =>'Passi',
'Passport data of the germplasm' =>'Itämerplasmin passitiedot',
'Passport data of the product' =>'Tuotteen passitiedot',
'Pedigree' =>'Sukutaulu',
'Pedigree tree of the germplasm' =>'Sukujalan planeetan puu',
'Pedigree tree of the product' =>'Tuotteen sukutaulu',
'Person' =>'Henkilö',
'Phase' =>'vaihe',
'phase abbrev' =>'vaihe lyh',
'Phases' =>'vaiheissa',
'Phases' =>'vaiheissa',
'Planning' =>'Suunnittelu',
'Plans' =>'suunnitelmat',
'Plans' =>'suunnitelmat',
'Plot' =>'juoni',
'Plots of study' =>'Opintojaksot',
'Preferences' =>'Asetukset',
'Processes' =>'Prosessit',
'Processes' =>'Prosessit',
'Product' =>'Tuote',
'Product or line' =>'Tuote tai rivi',
'Product search' =>'Tuotehaku',
'Product search' =>'Tuotehaku',
'product type' =>'Tuotetyyppi',
'Products' =>'Tuotteet',
'Products' =>'Tuotteet',
'Profile' =>'Profiili',
'Profile' =>'Profiili',
'Program' =>'Ohjelmoida',
'program abbrev' =>'ohjelma lyhyt',
'program name or abbrev' =>'ohjelman nimi tai lyhyt',
'Programs' =>'ohjelmat',
'Programs' =>'ohjelmat',
'Quality control' =>'Laadunvalvonta',
'Quality control' =>'Laadunvalvonta',
'Receiver program' =>'Vastaanotinohjelma',
'Recently used' =>'Äskettäin käytetty',
'Recently used tools' =>'Äskettäin käytetyt työkalut',
'Recipient' =>'vastaanottaja',
'Recipient Address' =>'Vastaanottajan osoite',
'Recipient Country' =>'Vastaanottajavaltio',
'Recipient Institution' =>'Vastaanottajan laitos',
'Recipient type' =>'Vastaanottajan tyyppi',
'Refresh' =>'virkistää',
'Refresh data' =>'Päivitä tiedot',
'Remarks' =>'Huomautukset',
'remarks' =>'huomautukset',
'Remove application' =>'Poista sovellus',
'Reset grid' =>'Nollaa verkko',
'Revoke Access' =>'Peruuta käyttöoikeus',
'Save' =>'Tallentaa',
'Save data filters' =>'Tallenna datasuodattimet',
'Saved lists' =>'Tallennetut luettelot',
'Saved lists' =>'Tallennetut luettelot',
'Search' =>'Hae',
'Search' =>'Hae',
'Search' =>'Hae',
'Search entities' =>'Hakuyhteisöt',
'Search for product or line' =>'Etsi tuotetta tai riviä',
'Search for seed storage' =>'Etsi siemenvarastointia',
'Search for study' =>'Hae opintoja',
'Search for users and persons' =>'Etsi käyttäjiä ja henkilöitä',
'Search here' =>'Etsi täältä',
'Search syntax' =>'Etsi syntaksi',
'Season' =>'Kausi',
'season name or abbrev' =>'kauden nimi tai lyhyt',
'Seasons' =>'Vuodenajat',
'Seasons' =>'Vuodenajat',
'Seed' =>'siemen',
'Seed inventory' =>'Siemenvarasto',
'Seed inventory' =>'Siemenvarasto',
'Seed lot' =>'Siemenerä',
'Seed lot defaults' =>'Siemenerien oletusarvot',
'Seed lots' =>'Siemenpalat',
'seed manager' =>'siemenenhallinta',
'Seed processing' =>'Siemenjalostus',
'Seed processing' =>'Siemenjalostus',
'Seed storage' =>'Siemenvarasto',
'seed storage QR code' =>'siemenvarasto QR code',
'Seed storage search' =>'Siemenvarastohaku',
'Seed storage search' =>'Siemenvarastohaku',
'Seeds' =>'Siemenet',
'Select and apply filters for the study selection.' =>'Valitse ja ota käyttöön suodattimia tutkimuksen valinnalle.',
'Select Studies' =>'Valitse tutkimukset',
'Select the data level you want to export.' =>'Valitse tietotaso, jonka haluat viedä.',
'Select the study/studies you want to include in data export.' =>'Valitse tutkimukset / tutkimukset, jotka haluat sisällyttää tietojen vientiin.',
'Sender' =>'Lähettäjä',
'Service requests' =>'Palvelupyynnöt',
'Service requests' =>'Palvelupyynnöt',
'Services' =>'Palvelut',
'Services' =>'Palvelut',
'Shipments' =>'lähetykset',
'Shipments' =>'lähetykset',
'Shipper' =>'Lähettäjä',
'Shipper program' =>'Lähettäjäohjelma',
'short study name' =>'lyhyt nimi',
'Show left nav' =>'Näytä vasemmanpuoleinen nav',
'Sometimes, you want to search by just specifying a word or phrase and then get list of search results just like Google.' =>'Joskus haluat etsiä vain määrittämällä sana tai lause ja saat sitten luettelon hakutuloksista aivan kuten Google.',
'Status' =>'Status',
'Studies' =>'Opinnot',
'Studies where the product was used as an entry' =>'Tutkimukset, joissa tuotetta käytettiin merkinnöinä',
'Study' =>'tutkimus',
'Study Summary' =>'Tutkimuksen yhteenveto',
'study type' =>'tutkimustyyppi',
'Submit' =>'Lähetä',
'Syntax' =>'Syntaksi',
'system generated study name' =>'järjestelmän tuottaman tutkimuksen nimi',
'Tasks' =>'tehtävät',
'Tasks' =>'tehtävät',
'team name or abbrev (user)' =>'ryhmän nimi tai lyhyt (käyttäjä)',
'Teams' =>'joukkueet',
'Teams where user belongs to' =>'Joukkueet, joihin käyttäjä kuuluu',
'The EBS-Core Breeding search tool allows you to search specific EBS entities by typing what you are looking for in the search' =>'EBS-Core Breeding-hakutyökalun avulla voit etsiä tiettyjä EBS-kokonaisuuksia kirjoittamalla haun etsimäsi',
'There is already an existing data filter.' =>'Jo olemassa olevaa tietosuodinta on jo olemassa.',
'This action cannot be undone.' =>'Tätä toimintoa ei voi kumota.',
'This is the browser for all the products.' =>'Tämä on kaikkien tuotteiden selaimesi.',
'This is the browser for all the seasons available in the system. You may manage the seasons here.' =>'Tämä on kaikkien järjestelmässä käytettävissä olevien kausien selain. Voit hallita kausia täällä.',
'Transaction No.' =>'Tapahtuman numero',
'Unverified seeds' =>'Luvattomat siemenet',
'Unverified seeds' =>'Luvattomat siemenet',
'Update Access Rights' =>'Päivitä käyttöoikeudet',
'Upload products' =>'Lataa tuotteita',
'Upload products' =>'Lataa tuotteita',
'Upload seed lots' =>'Lataa siemenerät',
'Upload seed lots' =>'Lataa siemenerät',
'Usage' =>'Käyttö',
'Usage history related to volume shanges' =>'Käyttöhistoria, joka liittyy volyymikasveihin',
'Use existing saved filter' =>'Käytä olemassa olevaa tallennettua suodatinta',
'Use saved filter' =>'Käytä tallennettua suodatinta',
'User' =>'käyttäjä',
'user can search a product against product designation or name or and line against germplasm name.' =>'käyttäjä voi etsiä tuotetta tuotteen nimityk- sestä tai nimestä ja / tai linjasta germplasmin nimiä vastaan.',
'user can search a seed lot against label.' =>'käyttäjä voi etsiä siemenerän etikettiä vastaan.',
'user can search a study against short study name.' =>'käyttäjä voi etsiä tutkimusta lyhytaikaisen opiskelun nimen suhteen.',
'user can search a user or person against first name or last name.' =>'käyttäjä voi hakea käyttäjää tai henkilöä etunimi tai sukunimi vastaan.',
'user can search product against below attributes:' =>'käyttäjä voi etsiä tuotetta alla olevia attribuutteja vastaan:',
'user can search seed lot against below attributes:' =>'käyttäjä voi etsiä siemennerineitä alle määritteitä vastaan:',
'user can search user or person against below attributes:' =>'käyttäjä voi etsiä käyttäjää tai henkilöä alla olevia attribuutteja vastaan:',
'User or person' =>'Käyttäjä tai henkilö',
'Username' =>'Käyttäjätunnus',
'username (user)' =>'käyttäjätunnus (käyttäjä)',
'Users' =>'käyttäjät',
'Users' =>'käyttäjät',
'Valid end date' =>'Voimassaolopäivämäärä',
'Valid start date' =>'Voimassa aloituspäivä',
'Variables' =>'muuttujat',
'Variables' =>'muuttujat',
'View more information' =>'Katso lisää tietoja',
'View progenies' =>'Katso jälkeläisiä',
'View Study' =>'Katso tutkimus',
'View Summary' =>'Näytä yhteenveto',
'View Summary of selected study/studies' =>'Katso yhteenveto valituista opinnoista / tutkimuksista',
'volume and unit (eg: 20g, 100panicles)' =>'tilavuus ja yksikkö (esim. 20 g, 100 spekseja)',
'Welcome back' =>'Tervetuloa takaisin',
'Welcome to' =>'Tervetuloa',
'with word \'foo bar\'' =>'sanalla \'foo bar\'',
'with words \'foo\' and \'bar\'' =>'sanalla \'foo\' ja \'bar \'',
'with words that start with \'foo bar\'' =>'sanoilla, jotka alkavat \'foo bar\'',
'with words that start with \'foo bar\' and has one additional character' =>'sanoilla, jotka alkavat \'foo bar\' ja on yksi ylimääräinen merkki',
'Year' =>'vuosi',
'year' =>'vuoden',
'You do not belong to any teams yet.' =>'Ette vielä kuulu mihinkään tiimiin.',
'Breeding Process' =>'Jalostusprosessi',
'Metrics' =>'metrics',
'Favorites' =>'suosikit',
'Recently used' =>'Äskettäin käytetty',
'Nurseries' =>'taimitarhat',
'Trials' =>'Trials',
'Sites' =>'Sivustot',
'Shipments' =>'lähetykset',
'Tools' =>'Työkalut',
'Displays chart for breeding activities. Activities are clickable which serves as shortcut to applications.' =>'Näyttää jalostustoiminnan kaavion. Toimintoja voidaan napsauttaa, jotka toimivat pikakuvakkeina sovelluksiin.',
'Displays metrics for operational studies, entries families and observations.' =>'Näyttää operatiivisten tutkimusten, merkintäperheiden ja havaintojen tiedot.',
'Displays list of applications marked as favorite.' =>'Näyttää suosikeiksi merkityt hakemukset.',
'Displays list of recently used applications.' =>'Näyttää äskettäin käytetyt sovellukset.',
'Displays list of all nurseries studies.' =>'Näyttää luettelon kaikista taimitarhojen opinnoista.',
'Displays list of all trials studies.' =>'Näyttää luettelon kaikista kokeista.',
'Displays study locations on a map.' =>'Näyttää opiskelupaikkoja kartalla.',
'Displays list of all shipments.' =>'Näyttää luettelon kaikista lähetyksistä.',
'Displays list of added tools.' =>'Näyttää lisättyjen työkalujen luettelon.',
'Data management' =>'Tiedonhallinta',
'Administration' =>'antaminen',
'There are no applications marked as favorite yet.' =>'Hakemuksia ei ole vielä merkitty suosikkeihin.',
'Use {key} data filter' =>'Käytä {key} tietosuodinta',
'There are no recently used tools yet.' =>'Ei ole vielä äskettäin käytettyjä työkaluja.',
'Theme color' =>'Teeman väri',
'Left nav skin' =>'Vasen nav-iho',
'Left nav' =>'Vasen nav',
'Light' =>'valo',
'Dark' =>'Tumma',
'Font size' =>'Fonttikoko',
'Language' =>'Kieli',
'Always show' => 'Näytä aina',
'Create'=> 'Luoda',
'Remove widget' =>'Poista widget',
'There are no applications added yet.' =>'Sovelluksia ei ole vielä lisätty.',
'Select to add tool' =>'Lisää työkalu valitsemalla',
'Click to go to' =>'Napsauta siirtyäksesi',
'Drag and drop to reorder widgets.' =>'Vedä ja pudota widgetien uudelleenjärjestelyyn.',
'Unmark as favorite' =>'Poista suosike',
'Go to dashboard' =>'Siirry hallintapaneeliin',
'Below are the crosses and their progenies in your program.' =>'Alla ovat ristit ja niiden jälkeläiset ohjelmassasi.',
'Choose dashboard layout.' =>'Valitse hallintapaneelin asettelu.',
'You have not added any widgets in your dashboard. Go to the configuration page to add widgets.' =>'Et ole lisännyt widgettejä hallintapaneelissasi. Voit lisätä widgettejä määrityssivulta.',
'Click to add widget' =>'Napsauta lisätäksesi widgetin',
'Add' =>'Lisätä',
'Added' =>'lisätty',
'Filter' =>'Suodattaa',
'Member since' =>'Jäsen vuodesta',
'Meber until' =>'Meber till',
'Team' =>'Tiimi',
'Role' =>'Rooli',
'Select team' =>'Valitse joukkue',
'Select role' =>'Valitse rooli',
'Member until' =>'Jäsen till',
'Select tool' =>'Valitse työkalu',
'Parental selection and crosses' =>'Vanhempien valinta ja ristit',
'Analysis' =>'analyysi',
'Advancement' =>'edistäminen',
'There are no applications yet in this activity.' =>'Tässä toiminnossa ei vielä ole sovelluksia.',
'per design' =>'per suunnittelu',
'per phase' =>'per vaihe',
'per location' =>'sijainnin mukaan',
'per year' =>'vuodessa',
'View shipment' =>'Näytä lähetys',
'Team members' =>'Ryhmän jäsenet',
'Successfully added to the team.' =>'Joukkueelle on lisätty onnistuneesti.',
'You have successfully logged out!' =>'Olet onnistuneesti kirjautunut ulos!',
'Your session has expired. Please log in.' =>'Istuntosi on vanhentunut. Kirjaudu sisään, ole hyvä.',
'You have successfully logged in!' =>'Olet kirjautunut sisään!',
'We were not able to verify your account. Kindly contact your system administrator.' =>'Emme pystyneet vahvistamaan tiliäsi. Ota ystävällisesti yhteyttä järjestelmänvalvojaan.',
'We were not able to verify your account. Kindly contact your system administrator.' =>'Emme pystyneet vahvistamaan tiliäsi. Ota ystävällisesti yhteyttä järjestelmänvalvojaan.',
'EBS search tool allows you to search specific EBS entities by typing what you are looking for in the search field and then pressing the enter key or clicking the search icon. Below are the supported search syntax.' =>'EBS-hakutyökalulla voit hakea tiettyjä EBS-kokonaisuuksia kirjoittamalla etsimäsi hakukenttään ja painamalla Enter-näppäintä tai napsauttamalla hakukuvaketta. Alla on tuettu hakusyntaksi.',
'Crossing' =>'ylitys',
'Cross lists' =>'Ristiluettelot',
'Crosses' =>'Ristit',
'IRIS Crosses' =>'IRIS-ristit',
'Click to edit label' =>'Muokkaa tarra napsauttamalla tätä',
'Enter widget custom label' =>'Anna widgetin mukautettu tarra',
'To favorite an app, go to the application and click the star icon' =>'Jos haluat suositella sovellusta, siirry sovellukseen ja napsauta tähtikuvaketta',
'Summary' =>'Yhteenveto',
'Description' =>'Kuvaus',
'Reporter' =>'Reportteri',
'Attachments' =>'Liitteet',
'Request type' =>'Pyynnön tyyppi',
'Select Request type' =>'Valitse Kysymystyyppi',
'Send Feedback' =>'Lähetä palautetta',
'Submit' =>'Lähetä',
'Close' =>'kiinni',
'Attach one or more files' =>'Liitä yksi tai useampi tiedosto',
'Submitting feedback' =>'Palautteen lähettäminen',
'There was a problem while loading content' =>'Ongelma sisällön lataamisen aikana',
'Please specify the required fields.' =>'Määritä vaaditut kentät.',
'Receive seeds' =>'Saavat siemeniä',
'Repack seeds' =>'Repack siemenet',
'Deposit seeds' =>'Talleta siemeniä',
'Create progenies' => 'Luo jälkeläisiä',
];
