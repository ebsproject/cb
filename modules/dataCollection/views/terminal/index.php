<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\export\ExportMenu;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ArrayDataProvider;
use app\components\FavoritesWidget;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;
use app\models\Variable;

/**
 * View file for transactions browser
 */
Yii::$app->view->registerCss('
    .notification-badge {
        font-family: "Poppins", sans-serif;
        position: relative;
        right: 0px;
        top: -20px;
        color: #ffffff;
        background-color: #3f51b5;
        margin: 0 -.8em;
        border-radius: 50%;
        padding: 2px 5px;
    }

    #notifications-dropdown h5 {
        font-size: 1rem;
        text-transform: capitalize;
        font-weight: 500;
    }

    #notifications-dropdown li {
        padding: 8px 16px;
        font-size: 1rem;
    }

    #notifications-dropdown li > a {
        padding: 0;
        font-size: 1.1rem;
        font-weight: 300;
    }

    #notifications-dropdown li > a > span {
        display: inline-block;
        font-size: 1.2rem;
        position: relative;
        top: 4px;
        margin-right: 5px;
    }

    #notifications-dropdown li > time {
        font-size: 0.8rem;
        font-weight: 400;
        margin-left: 38px;
    }

    #notifications-dropdown li.divider {
        padding: 0;
    }
    .small {
        font-size: 1.0rem !important;
    }
    .icon-bg-circle {
        color: #fff;
        padding: .4rem;
        border-radius: 50%;
    }
    h6 {
        font-size: 1rem;
        line-height: 110%;
        margin: 0.5rem 0 0.4rem 0;
    }
    .round {
        display: inline-block;
        height: 30px;
        width: 30px;
        line-height: 30px;
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #222;    
        color: #FFF;
        text-align: center;  
    }
    body{
        overflow-x: hidden;
    }
');

$resetGridUrl = Url::toRoute(['/dataCollection/terminal/index', 'program' => $program]);
$newNotificationsUrl = Url::toRoute(['/dataCollection/terminal/new-notifications-count']);
$notificationsUrl = Url::toRoute(['/dataCollection/terminal/push-notifications']);
$getTransactionStatusUrl = Url::toRoute(['/dataCollection/terminal/get-transaction-status']);
$deleteUrl = Url::to(['/dataCollection/terminal/delete-transaction?program=' . $program]);

$params = Yii::$app->request->queryParams;
$filteredNotification = '';
if (isset($params['TerminalTransaction']['transactionDbId'], $dataProvider->getModels()[0])) {
    $filteredNotification = '
    <div class="alert ">
        <div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">
            <i class="fa fa-info"></i> 
            You are now seeing the transaction for <b>' . $dataProvider->getModels()[0]['locationName'] . '</b> is now <b>'
        . $dataProvider->getModels()[0]['status']
        . '</b>. Click the Reset Grid button to display all transactions.
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        </div>
    </div>';
}

$columns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'header' => false,
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => false,
        'noWrap' => true,
        'vAlign' => 'top',
        'template' => '{commit}{download}{delete}',
        'buttons' => [
            'commit' => function ($url, $model, $key) use ($program) {

                if (str_contains($model['status'], 'uploaded') || $model['status'] == 'trait calculation failed') {

                    $render = Yii::$app->access->renderAccess("QUALITY_CONTROL_DATA","QUALITY_CONTROL", $model['creatorDbId']);
                    
                    return !$render ? '':Html::a(
                        '<i class="material-icons">check_circle</i>',
                        false,
                        [
                            "title" => "QC",
                            'style' => 'cursor: pointer;',
                            'class' => 'check-validate-transaction ',
                            'data-transaction_id' => $model['transactionDbId'],
                            'data-transactionLocationName' => $model['locationName'],
                            'data-transaction_status' => $model['status'],
                            'data-url' => Url::to(['/dataCollection/transaction/view', "id" => $model['transactionDbId'], "program" => $program,]),
                        ]
                    );
                }
            },
            'delete' => function ($url, $model, $key) use ($program){
                if (
                    str_contains($model['status'], 'uploaded') ||
                    $model['status'] == 'in queue' ||
                    $model['status'] == 'uploading in progress' ||
                    $model['status'] == 'error in background process' ||
                    $model['status'] == 'trait calculation failed'
                ) {

                    $render = Yii::$app->access->renderAccess("DELETE_TRANSACTION","QUALITY_CONTROL", $model['creatorDbId']);
                    return !$render ? '':Html::a(
                        '<i class="material-icons">delete</i>',
                        '#',
                        [
                            'style' => 'cursor: pointer;',
                            'class' => 'delete-transaction',
                            'title' => Yii::t('app', 'Delete Transaction'),
                            'data-transaction_id' => $model['transactionDbId'],
                            'data-location-name' => $model['locationName']
                        ]

                    );
                }
            },
            'download' => function ($url, $model, $key) {
                $buttons = '';

                $render = Yii::$app->access->renderAccess("DOWNLOAD_TRANSACTION_FILE","QUALITY_CONTROL", $model['creatorDbId']);
                if ($model['status'] !== 'uploading in progress') {
                    $buttons .= ' ' . Html::a(
                        '<i class="material-icons">file_download</i>',
                        '#',
                        [
                            'class' => 'download-transaction '.$render,
                            'title' => 'Download files',
                            'data-transaction_id' => $model['transactionDbId'],
                            'data-location-name' => $model['locationName'],
                            'data-toggle' => "modal",
                            "data-target" => "#download-modal"
                        ]
                    );
                }

                return !$render ? '':$buttons;
            },
        ]
    ],
    [
        'attribute' => 'status',
        'width' => '20%',
        'noWrap' => false,
        'contentOptions' => ['class' => 'text-center'],
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
            'Open' => 'open',
            'in progress' => 'in progress',
            'committed' => 'committed',
            'uploaded' => 'uploaded',
            'uploaded: to be calculated' => 'uploaded: to be calculated',
            'trait calculation failed' => 'trait calculation failed',
            'in queue' => 'in queue',
            'to be committed' => 'to be committed',
            'to be suppressed' => 'to be suppressed',
            'to be unsuppressed' => 'to be unsuppressed',
            'error in background process' => 'failed',
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'class' => 'col-lg-2'],
        ],
        'filterInputOptions' => ['placeholder' => 'Select status'],
        'format' => 'raw',
        'value' => function ($model) {
            $status = $model['status'];
            if (str_contains($status, 'uploaded')) {
                $class = "blue darken-2";
            }elseif (str_contains($status, 'in progress')) {
                $class = "amber darken-2";
            }elseif ($status == 'error in background process') {
                $status = 'failed';
                $class = "red darken-2";
            }elseif ($status == 'trait calculation failed') {
                $class = "red darken-2";
            }elseif ($status == 'committed') {
                $class = "center green darken-2";
            }else {
                $class = "grey";
            }

            return '<span class="new badge '.$class.'"><strong>'.\Yii::t('app', strtoupper($status)) . '</strong></span>';
        }
    ],
    [
        'attribute' => 'locationName',
        'label' => 'Location Name',
        'format' => 'raw',
        'value' => function ($data) use ($program){
            return Html::a(
                $data["locationName"],
                Url::to(['/location/view/basic', "id" => $data["locationDbId"], "program" => $program]),
                [
                    'data-pjax' => "0",
                    'target' => '_blank',
                    "title" => "View location information",
                ]
            );
        }
    ],
    [
        'attribute' => 'occurrence',
        'label' => 'Occurrence Name',
        'vAlign' => 'top',
        'noWrap' => true,
        'width' => '10%',
        'format' => 'raw',
        'value' => function ($data) use ($program) {
            $finalValue = null;
            $hasValue = false;
            if (isset($data['occurrence']) && !empty($data['occurrence'])) {
                $finalValue = ' <ul class="collapsible" data-collapsible="accordion">';
                // loop through occurrences
                foreach ($data['occurrence'] as $occurrence) {
                    if (empty($occurrence['occurrenceName'])) {
                        continue;
                    }
                    $hasValue = true;
                    if ($occurrence['dataUnitCount'] > 1 && $occurrence['dataUnit'] == 'cross') {
                        $dataUnit = $occurrence['dataUnitCount'] . ' crosses';
                    } else {
                        $terminalBackgroundProcessModel = \Yii::$container->get('app\models\TerminalBackgroundProcess');
                        $dataUnit = $terminalBackgroundProcessModel->pluralize($occurrence['dataUnitCount'], $occurrence['dataUnit']);
                    }
                    $finalValue .= '<li>
                    <div class="collapsible-header" style="padding: .5rem;"><span class="secondary-content">'
                        . Html::a(
                            $occurrence["occurrenceName"],
                            Url::to(['/occurrence/view/index', "id" => $occurrence["occurrenceDbId"], "program" => $program]),
                            [
                                'data-pjax' => "0",
                                'target' => '_blank',
                                "title" => "View occurrence information",
                            ]
                        ) . '</span>' .
                        (!empty($occurrence['dataUnitCount']) ? 
                        '<span style="margin-top:-3px;margin-left:2px;" class="badge center green darken-2 white-text"><strong>'
                        . $dataUnit . '</strong></span>' : '').
                        '</div>
                    <div class="collapsible-body" style="padding:0px">';

                    // parse variableDbIds
                    $variableDbIds = $occurrence['variableDbIds'];
                    //TODO: Move data provider processing to Transaction model
                    $dataModel = [];
                    if (!empty($variableDbIds)) {
                        $variable = new Variable();
                        $variableList = $variable->searchAll([
                            'fields' => 'variable.label|variable.id AS variableDbId',
                            'variableDbId' => 'equals '.implode('|equals ', $variableDbIds)
                        ])['data'];
                        foreach ($variableList as $var) {
                            $dataModel[] = ["variable" => $var['label']];
                        }

                    }
                    $provider = new ArrayDataProvider([
                        'allModels' => $dataModel,
                        'pagination' => false
                    ]);
                    $finalValue .= \kartik\grid\GridView::widget([
                        'dataProvider' => $provider,
                        'layout' => '{items}',
                        'showHeader' => false,
                        'columns' => [
                            [
                                'attribute' => 'variable',
                                'format' => 'raw',
                                'label' => false,
                            ]
                        ],
                        'tableOptions' => ["style" => "margin-bottom:0px"]
                    ]);
                    $finalValue .= '</div></li>';
                }

                $finalValue = ($hasValue) ? $finalValue .= '</ul>' : null;
            }

            return $finalValue;
        }
    ],
    [
        'attribute' => 'programCode',
        'label' => 'Program'
    ],
    [
        'attribute' => 'creator',
        'label' => 'Uploader'
    ],
    [
        'attribute' => 'creationTimestamp',
        'label' => 'Uploaded Timestamp',
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [

            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'startDate'=> date("1970-01-01"),
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
    ],
    [
        'attribute' => 'committer',
    ],
    [
        'attribute' => 'committedTimestamp',
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [

            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'startDate'=> date("1970-01-01"),
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ],
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
    ],
    [
        'attribute' => 'action',
        'label' => 'Origin',
        'value' => function ($data) {
            return ucwords($data['action']);
        }
    ],
    [
        'attribute' => 'remarks',
        'value' => function ($data) {

            if ($data['remarks'] == 'null') {
                return null;
            }
            return $data['remarks'];
        }
    ],
    [
        'attribute' => 'modificationTimestamp',
        'visible' => false,
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [

            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'startDate'=> date("1970-01-01"),
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
    ],
    [
        'attribute' => 'modifier',
        'visible' => false
    ],
];
$export = ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'filename' =>  \Yii::t('app', 'Data Terminal Transactions'),
    'fontAwesome' => true,
    'exportConfig' => [
        ExportMenu::FORMAT_PDF => false
    ],
    'showColumnSelector' => false,
    'asDropdown' => true,
    'showConfirmAlert' => false
]);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings(['id' => 'transactions-grid-id']);

$renderUpload = Yii::$app->access->renderAccess("UPLOAD_DATA","QUALITY_CONTROL"); 
$renderPostHarvest = Yii::$app->access->renderAccess("DATA_COLLECTION_INPUT_POST_HARVEST_DATA","QUALITY_CONTROL");
$dynaGrid = DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => 'operational-studies-grid-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => 'transactions-grid-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsiveWrap' => false,
        'panel' => [
            'heading' => '<h3>' . \Yii::t('app', 'Data Collection') . FavoritesWidget::widget([
                'module' => 'dataCollection',
                'controller' => 'terminal'
            ]) .
                '</h3>' . $filteredNotification . '<br>',
            'before' => \Yii::t('app', 'This is the browser for all the Transactions in Data Terminal for Data Collection. You may manage the Transactions here. <b>Transactions displayed are filtered by applied dashboard program and transactions created by the user.</b>'),
            'after' => false,
        ],
        'toolbar' =>  [
            'postHarvest' => !$renderPostHarvest ? '':Html::a(
                \Yii::t('app','Post Harvest'),
                Url::toRoute(
                    [  
                        '/dataCollection/post-harvest',
                        'program' => $program
                    ]
                ),
                [
                    'id' => 'post-harvest-id',
                    'class' => 'btn btn-default',
                    'title' => \Yii::t('app', 'Post Harvest'),
                    'data-pjax' => true,
                    "style" => 'margin-right:5px;background-color: #5cb85c !important;',
                ]
            ),
            'upload' => !$renderUpload ? '':Html::a(
                \Yii::t('app','File Upload'),
                Url::toRoute(
                    [
                        '/dataCollection/upload',
                        'program' => $program
                    ]
                ),
                [
                    'id' => 'upload-file-id',
                    'class' => 'btn btn-default ',
                    'title' => \Yii::t('app', 'Upload file'),
                    'data-pjax' => true,
                    "style" => 'margin-right:5px;background-color: #5cb85c !important;',
                ]
            ),
            'notification' =>  Html::a(
                '<i class="material-icons large">notifications_none</i>
                <span id="notif-badge-id" class=""></span>',
                '#',
                [
                    'id' => 'notif-btn-id',
                    'class' => 'btn waves-effect waves-light terminal-notification-button tooltipped',
                    'data-pjax' => 0,
                    'style' => "margin-right:5px;overflow: visible !important;",
                    "data-position" => "top",
                    "data-tooltip" => \Yii::t('app', 'Notifications'),
                    "data-activates" => "notifications-dropdown"
                ]
            ),
            'exportConfig' => $export . '&emsp;',
            [
                'content' => "" .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', '#', ['id' => 'reset-transactions-grid-id', 'class' => 'btn btn-default', 'title' => \Yii::t('app', 'Reset grid'), 'data-pjax' => true])
                    . '{dynagridFilter}{dynagridSort}{dynagrid}',
            ]
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'resizableColumns' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options' => ['id' => 'transactions-grid-id'],
]);
DynaGrid::end();

// Delete transaction modal
Modal::begin([
    'id' => 'delete-transaction-modal',
    'header' => '<h4 id="delete-transaction-label"></h4>',
    'footer' =>
        // Temporarily remove confirmation button
        Html::a(\Yii::t('app','Cancel'),['#'],['data-dismiss'=>'modal']).'&nbsp;&nbsp'.
        Html::a(\Yii::t('app','Confirm'),['#'],['class'=>'btn btn-primary waves-effect waves-light delete-transaction-btn']).'&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo \Yii::t('app', '<h5> <i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete <span id="delete-transaction"></span>. This action will delete all records within the transaction. </h5>');
echo \Yii::t('app', "Please ensure that there's no synced Field Book data (uncommitted) before the deletion. Click confirm to proceed.");
echo '<div class = "row"><div id="mod-progress" class="hidden"></div><div id="mod-notif" class="hidden"></div></div>';
Modal::end();
?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>


<!-- Transaction files Modal -->
<div class="modal fade in" id="download-modal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <span id="loading-div"></span>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

                <div style="float:left">
                    <h4 id="transaction-study-name-label"></h4>
                </div>
            </div>

            <div class="modal-body download-body" style="max-height:100%;">

            </div>
            <div class="modal-footer">
                <?php echo Html::Button(\Yii::t('app', 'Close'), [
                    'class' => 'btn btn-flat waves-effect waves-light modal-close', "data-dismiss" => "modal",
                    'style' => "background-color:transparent !important;"
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
$transactionFileUrl = Url::toRoute(['/dataCollection/transaction-file']);

    // variables for change page size
    Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = 'transactions-grid-id',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);
    // js file for data browsers
    $this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
    ]);
$this->registerJs(
    <<<JS
    var resetGridUrl = "$resetGridUrl";
    var transactionFileUrl = "$transactionFileUrl";
    var newNotificationsUrl='$newNotificationsUrl';
    var notificationsUrl='$notificationsUrl';
    var getTransactionStatusUrl = '$getTransactionStatusUrl';
    var selectedTransactionId = null;

    renderBrowser();
    notifDropdown();
    renderNotifications();
    updateNotif();
    notificationInterval=setInterval(updateNotif, 10000);

    $(document).ready(function() {
    // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
    });


    $(document).on('ready pjax:success', function(e) {
        e.preventDefault();
        renderBrowser();
        $('#notif-btn-id').tooltip();
        notifDropdown();
        $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>');
        renderNotifications();
        updateNotif();
    });

    function renderBrowser() {
        var maxHeight = ($(window).height() - 260);
        $(".kv-grid-wrapper").css("height",maxHeight);

        $('.collapsible').collapsible();

        $('#reset-transactions-grid-id').on('click', function(e) {
            e.preventDefault();
            $.pjax.reload({
                container: '#transactions-grid-id-pjax',
                replace: true,
                url: resetGridUrl
            })
        });
    }

    // Delete a transaction
    $(document).on('click', '.delete-transaction', function() {
        var obj = $(this);
        var locationName = obj.data('location-name');
        var transactionId = obj.data('transaction_id');
        var deleteModal = '#delete-transaction-modal > .modal-dialog > .modal-content > .modal-body';
        selectedTransactionId = transactionId;

        $('#delete-transaction-label').html('<i class="fa fa-trash"></i> Delete ' + locationName);
        $('#delete-transaction').html(locationName);

        $('#delete-transaction-modal').modal('show');
    })

    $(document).on('click', '.delete-transaction-btn', function(e) {
        var url = '$deleteUrl';

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                id: selectedTransactionId
            },
            async: false,
            success: function(response) {
                $('#delete-transaction-modal').modal('hide');
                window.location = '$resetGridUrl';
            }
        });
    });

    $(document).on('click', '.download-transaction', function(e) {

        e.preventDefault();
        var obj = $(this);
        var locationName = obj.data('location-name');
        var transactionId = obj.data('transaction_id');

        $('#transaction-study-name-label').html('<i class="material-icons">file_download</i> ' + locationName);

        $(".download-body").html('');
        $('#loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        $.ajax({
            url: transactionFileUrl + "/" + transactionId,
            type: 'POST',
            data: {locationName: locationName},
            success: function(response) {
                if (response) {
                    $('#loading-div').html('');
                    $(".download-body").html(response);
                }
            },
            error: function() {
                $('.download-body').html('<div class="card-panel red"><span class="white-text">There seems to be a problem while retrieving files.</span></div>');
            }
        });
    });
    $(document).on('click', '.download-transaction-file', function(e) {
        $('#system-loading-indicator').hide();
    });

    /**
     * Initialize dropdown materialize  for notifications
     */
    function renderNotifications(){
        $('.terminal-notification-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            belowOrigin: true,
            alignment: 'right',
            stopPropagation: true
        });
    }
    /**
     * Shows new notifications
     */
    function updateNotif() {

        $.getJSON(newNotificationsUrl, 
        function(json){
            $('#notif-badge-id').html(json);
            if(parseInt(json)>0){

                $('#notif-badge-id').addClass('notification-badge red accent-2');
            }else{
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
            }
        });
    }
    /**
     * Displays the notification in dropdown UI
     */
    function notifDropdown(){

        $('.terminal-notification-button').on('click',function(e){
            $.ajax({
                url: notificationsUrl,
                type: 'get',
                async:false,
                success: function(data) {
                    renderNotifications();
                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                    $('#notifications-dropdown').html(data);

                    $('.bg_notif-btn').on('click', function(e){
                        e.preventDefault()

                        var obj = $(this);
                        var transaction_id = obj.data('transaction_id');
                        var process_name = obj.data('worker-name');

                        if (process_name == 'BuildCsvData') {
                            // Create temporary entry point to downloading CSV data
                            const a = document.createElement('a')

                            a.setAttribute('hidden', '')
                            a.setAttribute('href', `/index.php/dataCollection/terminal/download-csv-data`)
                            
                            // Temporarily insert element to document
                            document.body.appendChild(a)

                            // Trigger download
                            a.click()

                            // Remove element
                            document.body.removeChild(a)

                            $('#system-loading-indicator').html('')
                        } else {
                            window.location = '$resetGridUrl';
                        }
                    });
                },
                error: function(xhr, status, error) {
                    var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                        '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                        '<i class="material-icons red toast-close-btn">close</i></button>' +
                        '<ul class="collapsible toast-error grey lighten-2">' +
                        '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                        '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                        '</li> </ul>');
                    Materialize.toast(toastContent, 'x', 'red');
                    $('.toast-error').collapsible();
                }
            });
        });
    }

    // Redirect to QC tab
    $(document).on('click', '.check-validate-transaction',function(e){
        window.location.href = $(this).data('url');
    });

JS
);
