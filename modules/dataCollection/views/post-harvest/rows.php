<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Renders the rows for data collection post harvest
*/

// Import classes
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$urlSave = Url::to(['/dataCollection/post-harvest/save',
    'plotDbId' => $plotDbId,
    'occurrenceDbId' => $occurrenceDbId,
    'locationDbId' => $locationDbId,
    'transactionDbId' => $transactionDbId
]);

$script = <<< JS
    $(document).ready(function() {
        // Skip disabled inputboxes when pressing tab
        adjustInputIndexes();

        // Resize column width per row
        resizeRowColumns();
    })

    $("#save-post-harvest-btn").on('click', function() {
        var form = $("#update-post-harvest-data :input:not('.disabled')");
        var formData = form.serialize();
        var actionUrl = '$urlSave';

        $.ajax({
            type: "POST",
            datatype: "json",
            url: actionUrl,
            data: formData,
            success: function(response) {
                data = JSON.parse(response)
                
                // Set focus on QR Code Search after saving
                $("#qr-code-input").focus()
                
                notif = data.message;
                Materialize.toast(notif, 5000);

                $.pjax.reload({
                    container: '#post-harvest-view-pjax',
                    timeout: 0,
                })
            }
        })
    })

    $(window).resize(function() {
        resizeRowColumns();
    })

    function resizeRowColumns() {
        var sizes = [];

        // loop through each row to get widths
        $(".row-container").each(function(key, value) {
            // loop through each column
            $(this).children().each(function(key, value) {
                let columnWidth = $(this).width()

                // Set size limiters
                if (columnWidth <= 200) {
                    columnWidth = 200
                } else if (columnWidth >= 275) {
                    columnWidth = 275
                }
                
                // if key exists compare if wider
                if (sizes.hasOwnProperty(key)) {
                    existingWidth = sizes[key];
                    if (columnWidth > existingWidth) {
                        sizes[key] = columnWidth
                    }
                }

                // insert key if doesnt exist
                if (!sizes.hasOwnProperty(key)) {
                    sizes[key] = columnWidth
                }
            })
        })

        // resize per column
        $(".row-container").each(function(key, value) {
            // loop through each column
            $(this).children().each(function(key, value) {
                $(this).width(sizes[key])
                $(this).css("minWidth", sizes[key]+ "px")
                $(this).css("maxWidth", sizes[key]+ "px")
            })
        })
    }

    function adjustInputIndexes() {
        $("#update-post-harvest-data :input.disabled").each(function() {
            $(this).attr('tabindex', '-1')
        })
    }
JS;

$this->registerJs($script);

ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'update-post-harvest-data',
    'action' => '/dataCollection/post-harvest/save'
]);

?>

<div class="row row-container rows-1">
    <?php
    $countFields = count($row1);
    for($i = 0; $i < $countFields; $i++){
        echo $row1[$i]['value'];
    }
    ?>
</div>
<div class="row row-container rows-2">
    <?php
    $countFields = count($row2);
    for($i = 0; $i < $countFields; $i++){
        echo $row2[$i]['value'];
    }
    ?>
</div>
<div class="row row-container rows-3">
    <?php
    $countFields = count($row3);
    for($i = 0; $i < $countFields; $i++){
        echo $row3[$i]['value'];
    }
    ?>
</div>
<div class="row row-container">
    <div class="col-md-4">
        <button class="btn btn-sm btn-block" type="button" id="save-post-harvest-btn">Save</button>
    </div>
</div>

<?php
ActiveForm::end();

$this->registerJs(<<<JS

JS);
?>

<style>
    .row-container {
        display: flex;
        flex-wrap: nowrap;
        overflow-x: auto;
    }

    .control-label {
        display: block;
        flex: 0 1 auto;
        margin-right: 1%;
        margin-bottom: 1%;
        min-width: 300px;
    }
</style>