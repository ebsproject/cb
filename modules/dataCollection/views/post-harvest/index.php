<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<?php
/**
 * Renders Post Harvest Page
 */

// Import classes
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\Button;
use kartik\grid\GridView;

// Define URLs
$urlSearch = Url::to(['/dataCollection/post-harvest/search']);
$urlDownloadGeneratedReport = Url::toRoute(['/printouts/default/download-generated-report']);
$urlShowAllTransactions = Url::to(['/dataCollection/transaction/'.$transactionDbId, 'program' => $program]);

// Define Variables
$reportParametersJSON = json_encode($reportParameters);

// Build buttons based on retrieved printouts config
$buttonString = '';
$length = 30;
$dots = '...';
foreach($printouts as $item) {
    $buttons = Button::widget([
        'options' => [
            'class' => 'btn printouts-download-button tooltipped',
            'data-template-name' => $item['template_name'],
            'data-button-name' => $item['button_name'],
            'data-position' => 'top',
            'data-tooltip' => Yii::t('app', $item['button_name'])
        ],
        'label' => strlen($item['button_name']) > $length ? substr($item['button_name'], 0, $length - strlen($dots)) . $dots : $item['button_name']
    ]);

    $buttonString .= $buttons;
}

// Modal for device settings
Modal::begin([
    'id' => 'ph-device-settings-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">handyman</i> Device Settings</h4>',
    'footer' =>
        Html::a(\Yii::t('app','Cancel'), ['#'],
                [
                    'id'=>'ph-device-settings-modal-cancel-link',
                    'class' => 'modal-close',
                    'data-dismiss' => 'modal',
                ]
            ). '&emsp;' .
        Html::a(\Yii::t('app', 'Done'), '#',
            [
                'id' => 'ph-device-settings-modal-done-btn',
                'class' => 'btn btn-primary waves-effect waves-light modal-close',
                'title' => \Yii::t('app', 'Save device settings'),
                'data-dismiss' => 'modal',
            ]
        ). '&emsp;' ,
]);
echo '
<div id="ph-device-settings-modal-loading-bar" class="progress">
    <em>Fetching device settings configuration... </em>
    <div class="indeterminate"></div>
</div>
<div id="ph-device-settings-modal-body">
</div>';
Modal::end();


// Show All button for Grid View
$showAllButton = false;
if ($transactionDbId && $transactionDbId != 0) {
    $showAllButton = Html::a('Show All', 
        "$urlShowAllTransactions",
        [
            'class'=>'btn btn-primary waves-effect waves-light',
            'id'=>'show-all-transaction-btn',
        ]
    );
}

$script = <<< JS
// Define variables
var plotDbId = 0
var entityName = 'plot'
var inputTimer
var submitInterval = 2000 // 2 seconds
var deviceList = []
var useSearchButton = $useSearchButton
var csApiStatus = $csApiStatus

$(document).ready(function(e) {
    toggleSearchButton()
    $('.tooltipped').tooltip();

    // Set focus on QR Code / Plot ID on page load
    $("#qr-code-input").focus();
})

$(document).on("input", "#qr-code-input", function(e) {
    if (useSearchButton === false) {
        clearTimeout(inputTimer)

        inputTimer = setTimeout(function() {
            // Search plot after user input and submitInterval expires
            if ($("#qr-code-input").val() !== '') {
            $("#search-btn").click()
        }
        }, submitInterval)
        
    }
})

$(document).on("focus", "#qr-code-input", function() {
    // Clear QR Code / Plot when focused
    $("#qr-code-input").val("")

    // Clear Row 2-3
    $("#plot-data-values .rows-2").find(':input').val('')
    $("#plot-data-values .rows-3").find(':input').val('')
})

// Toggle search method
$("#searchMethodToggle-switch-id").on('click', function() {
    useSearchButton = $('#searchMethodToggle-switch-id').is(':checked');
    toggleSearchButton()
})

// Click Search Button
$(document).on("click", "#search-btn", function() {
    var qr_input = $("#qr-code-input")
    var qr_code = qr_input.val()

    // Disable inputs while search is ongoing
    qr_input.attr('disabled', true)

    $('.loader').removeClass('hidden');

    // reset values to default
    $("#plot-data-values").attr('hidden', true)
    $("#printout-buttons").attr('hidden', true)
    $("#grid-view").attr('hidden', true)
 
    if (qr_code) {
        $.ajax({    
            type: 'POST',
            dataType: 'json',
            data: {
                qr_code: qr_code
            },
            url: '$urlSearch',
            success: function(response) {

                if (response.success === true) {
                    $("#grid-view").removeAttr('hidden')
                    plotDbId = response.data.plotDbId ?? 0

                    // Show Printout Buttons when CS API is available
                    if (csApiStatus && csApiStatus !== 500) {
                        $("#printout-buttons").removeAttr('hidden')
                    }

                    // Render Plot Data
                    $("#plot-data-values").removeAttr('hidden')    
                    $("#plot-data-values").html(response.view)

                    // Enable Device Settings 
                    $("#ph-device-settings-btn").removeClass('disabled');
                    deviceList = response.deviceList

                    var focusedElement = response.focused
                    $("input[name*='"+focusedElement+"']").focus();

                    $.pjax.reload({
                        container: '#post-harvest-view-pjax',
                        timeout: 0,
                    })
                }

                $('.loader').addClass('hidden');
                qr_input.removeAttr('disabled')

                notif = response.message;
                Materialize.toast(notif, 5000);

            },
            error: function(xhr, error, status) {
                notif = '<i class="material-icons red-text left">close</i> An error occurred while searching QR code. Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        })
    } else {
        notif = '<i class="material-icons red-text left">close</i> QR Code field is empty. Please enter a value to search.';
        Materialize.toast(notif, 5000);
    }
})

// Download button click event
$('.printouts-download-button').off('click').on('click', function() {
    // Hide system loading indicator
    $('#system-loading-indicator').css('display','none');
    // Variables
    var dbIds = plotDbId;
    var template = $(this).data('template-name').toLowerCase();
    var buttonName = $(this).data('button-name').replaceAll('.','');
    var format = 'pdf';
    var fileName = template + " - " + buttonName + "." + format;

    var params = {
        'plotDbId': plotDbId.toString()
    }

    var requestParameters = params;
    var requestParamNames = Object.keys(params)
    var reportParameters = $reportParametersJSON;
    var currentTemplateSupported = reportParameters[template];
    var hasSupportedParam = false;

    // Check the param names in the request parameters
    for(var paramName of requestParamNames) {
        if(currentTemplateSupported.includes(paramName)) hasSupportedParam = true;
    }

    // If the template has no params, set flag to true
    if(currentTemplateSupported.length == 0) hasSupportedParam = true;

    // If the selected report template does not contain
    // any of the params in the request, display error message
    if(!hasSupportedParam) {
        var notif = "<i class='material-icons red-text'>close</i>&nbsp;The selected template does not support "+entityName+" records.";
        Materialize.toast(notif, 5000);
        return;
    }

    // Generate the report
    var notif = "<i class='material-icons blue-text'>info</i>&nbsp;Generating report...";
    Materialize.toast(notif, 2500);

    $('#system-loading-indicator').css('display','block');
    setTimeout(function() {
        $('#system-loading-indicator').css('display','none');
    },3000);

    // Build download url
    var downloadURL = window.location.origin
        + '$urlDownloadGeneratedReport?'
        + 'template=' + template + '&'
        + 'parameters=' + JSON.stringify(requestParameters) + '&'
        + 'format=' + format + '&'
        + 'fileName=' + fileName;
    
    // Download the file
    window.location.assign(downloadURL);
});

// Load device settings modal content
$(document).on("click", "#ph-device-settings-btn", function() {
    let htmlContent = ''

    if (deviceList.length === 0) {
        htmlContent = 'No devices found in the configuration. Please contact your data administrator.'
    } else {
        $.each(deviceList, function(index, element) {
            let inputClass = 'div[class*="field-' + element +'-observation"]'
            let inputId = $(inputClass+ ' input').attr('id')

            htmlContent += `
            <h5>`
                + $(inputClass).find('label').text() +
                `<button title="Connect to the device"
                    class="connect-device-btn btn btn-primary waves-effect waves-light pull-right"
                    data-id="` + inputId + `">
                    <i class="material-icons">power</i>
                </button>
            </h5>
            <label for="baud-rate">Baud Rate (For Serial Devices):</label>
            <input type="number" id="` + inputId + `-baud-rate" value="9600">
            <div id="` + inputId + `-connection-status">Choose a device to connect</div>
            <br/>
            `
        })
        htmlContent += `
            <div id="troubleshooting-guide">
                <a href="#" id="guide-toggle">
                    Having trouble accessing your device? Click here <span style="float: right;">+</span>
                </a>
                <ul id="guide-content" style="margin-top: 10px; padding-left: 20px; display: none;">
                    <li>• Ensure your device is connected properly.</li>
                    <li>• Check browser permissions for the device.</li>
                    <li>• On Windows, verify device access in Device Manager.</li>
                    <li>• On macOS, enable access in System Preferences → Security & Privacy → Privacy.</li>
                    <li>• Restart your browser and try again.</li>
                </ul>
            </div>
        `
    }

    $('#ph-device-settings-modal-loading-bar').addClass('hidden')
    $('#ph-device-settings-modal-body').html(htmlContent)
})

// Connect to the device
$(document).on("click", ".connect-device-btn", function() {
    let inputId = $(this).attr('data-id')

    if ('serial' in navigator) {
        try {
            connectToSerialPort(inputId);
        } catch (error) {
            console.log(error.message.includes('No port selected by the user'))
            console.warn("Serial connection failed:", error.message);
        }
    } else if ('hid' in navigator) {
        connectToHIDDevice(inputId)
    } else if ('usb' in navigator) {
        $('#' + inputId + '-connection-status').html(`
            <i class="material-icons orange-text">warning</i>
            USB support not implemented yet.
        `)
    } else {
        $('#' + inputId + '-connection-status').html(`
            <i class="material-icons orange-text">warning</i>
            No supported APIs found.
        `)
    }
})

$(document).on("click", '#guide-toggle', function() {
    const content = $('#guide-content')
    const toggleSymbol = $(this).find('span')

    if (content.is(':visible')) {
        content.slideUp()
        toggleSymbol.text('+')
    } else {
        content.slideDown()
        toggleSymbol.text('-')
    }
})

let serialPort, hidDevice, serialReader

// Function to connect to a Serial device
async function connectToSerialPort(inputId) {
    try {
        serialPort = await navigator.serial.requestPort()

        if (serialPort) {
            let baudRate = parseInt($('#' + inputId + '-baud-rate').val(), 10)
            await serialPort.open({ baudRate })
            $('#' + inputId + '-connection-status').html(`
                <p class="green-text" >Successfully connected to the device </p>
            `)

            console.log("Serial port connected at baud rate:", baudRate)
            readFromSerialPort(inputId) // Start reading data
        }
    } catch (error) {
        if(error.message.includes('No port selected by the user') && ('hid' in navigator)) {
            connectToHIDDevice(inputId)
        }else{
            console.error("Error connecting to Serial port:", error)
            $('#' + inputId + '-connection-status').text('Error: ' + error.message)
        }
    }
}

// Function to connect to an HID device
async function connectToHIDDevice(inputId) {
    try {
        let devices = await navigator.hid.requestDevice({ filters: [] })

        if (devices.length > 0) {
            hidDevice = devices[0]
            await hidDevice.open()

            $('#' + inputId + '-connection-status').html(`
                 <p class="green-text" >Successfully connected to the device: </p>` 
                 + hidDevice.productName
            )

            console.log("HID device connected:", hidDevice.productName)
            readFromHIDDevice(inputId) // Start reading data
        }
    } catch (error) {
        console.error("Error connecting to HID device:", error)
        $('#' + inputId + '-connection-status').text('Error: ' + error.message)
    }
}

// Read data from Serial port
async function readFromSerialPort(inputId) {
    const decoder = new TextDecoderStream()
    serialReader = serialPort.readable.pipeTo(decoder.writable)
    const inputStream = decoder.readable.getReader()

    try {
        let text = ''

        while (true) {
            const { value, done } = await inputStream.read()
            if (done) break
            text += value
            console.log("Raw Meter Data:", value)

            let formattedText = text.split(" ").reduce((acc, item) => {
                let trimmed = item.trim()
                if (trimmed) {
                    acc.push(trimmed)
                    if (acc.length > 2) acc.shift()
                }
                return acc
            }, [])[0]

            $('input#' + inputId).val(formattedText)
        }
    } catch (error) {
        console.error("Error reading from Serial port:", error)
    }
}

// Read data from HID device
async function readFromHIDDevice(inputId) {
    try {
        hidDevice.addEventListener("inputreport", event => {
            const dataArray = new Uint8Array(event.data.buffer)

            let scaleValue = dataArray[dataArray.length - 2] + (dataArray[dataArray.length - 1] * 256)
            $('input#' + inputId).val(scaleValue)
        })
    } catch (error) {
        console.error("Error reading from HID device:", error)
    }
}

function toggleSearchButton() {
    // Disable Search Button depending on toggle status
    if (useSearchButton === true) {
        $("#search-btn").removeAttr('disabled')
    } else {
        $("#search-btn").attr('disabled', true)
    }
}

JS;

$this->registerJs($script);

?>

<!-- Post Harvest -->
<div class="row">
    <h3 class=""><a href="<?php echo Url::toRoute(['/dataCollection/terminal', "program" => $program]) ?>"><?php echo \Yii::t('app', 'Data Collection'); ?></a><small> &raquo; <?php echo \Yii::t('app', 'Post Harvest'); ?></small>
    </h3>
</div>

<?php
    if ($csApiStatus && $csApiStatus === '500') {
        $printoutsMessage = Yii::t('app', '<i class="fa fa-exclamation-triangle"></i>&nbsp; Printouts server is unreachable at the moment. Printout buttons will not be shown.');
        echo Yii::$app->session->setFlash('warning', $printoutsMessage);
    }
?>

<div class="main-body">
    <div class="col col-lg" style="padding:2%">
        <div class="row">
            <div class="col-sm-4">
                <strong>
                    QR Code (Plot ID)
                    <div class="hidden preloader-wrapper loader small active" style="width:10px;height:10px;margin-left:5px;">
                        <div class="spinner-layer spinner-green-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="gap-patch">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </strong>
                <div class="value">
                    <input type="text" class="form-control" id="qr-code-input" placeholder="">
                    <h3><a id="search-btn" class="btn btn-primary waves-effect waves-light pull-right" style="margin-bottom:10px">Search</a></h3>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="toggle">
                    <div class='switch left' style='display:inline-block;margin-top: 30px;margin-left: 5%;'>
                        <label style='margin-bottom:0px;' title='Use search button to load the plot'>
                            <input type='checkbox' <?php echo ($useSearchButton === true ? 'checked' : ''); ?> id='searchMethodToggle-switch-id' data-pjax=0>
                            <span class='lever'></span>Use Search Button
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <?php
                    echo Html::a('<i class="material-icons">handyman</i>',
                        '#',
                        [
                            'class' => 'btn btn-primary waves-effect waves-light pull-right disabled',
                            'url' => '#',
                            'id' => 'ph-device-settings-btn',
                            'data-dismiss' => 'modal',
                            'data-toggle' => "modal",
                            'data-target' => "#ph-device-settings-modal",
                            "title" => \Yii::t('app',"Update device settings"),
                        ]
                    ) . '&emsp;';
                ?>
            </div>
        </div>
        <div id="plot-data-values" hidden>

        </div>
    </div> 
</div>
<div class="main-body" id="printout-buttons" style="padding:2%;" hidden>
    <div class="row">
        <div class="col-sm-6">
            <strong>Available Printouts</strong>
        </div>
    </div>
    <div class="button-container">
        <!-- Printout Buttons -->
        <?php echo $buttonString; ?>
    </div>
</div>
    
<div class="main-body" id="grid-view" style="padding:2%;" hidden>
    <!-- Grid View Container -->
        <?php
            echo $grid = GridView::widget([
                'dataProvider' => $dataProvider,
                'id' => "$browserId-id",
                'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
                'columns' => $columns,
                'export' => false, 
                'panel'=>[
                    'heading'=> false,
                    'before'=> \Yii::t('app', '{summary}'),
                    'after' => $showAllButton
                ],
                'toggleData'=>true,
                'toolbar' => [
                    ['content' => '&nbsp;']
                    ],
                'pjax' => true,
                'pjaxSettings' => [
                'options' => [
                        'id' => 'post-harvest-view-pjax'
                    ]
                ]
            ]);
        ?>
                        
</div>

<style type="text/css">
    .main-body{
        background-color: #fff;
        margin: 10px;
    }

    .value {
        display: flex;
        align-items: center;
    }

    .button-container {
        display: flex;
        flex-wrap: nowrap;
        overflow-x: auto;
    }
    
    .printouts-download-button {
        display: block;
        flex: 0 0 auto;
        margin-right: 1%;
        margin-bottom: 1%;
    }
    .switch label .lever {
        margin: 0 5px;
    }
</style>
