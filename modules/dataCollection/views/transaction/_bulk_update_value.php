<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use app\components\GenericFormWidget;
use yii\helpers\Url;

// Renders generic form for trait value input

echo GenericFormWidget::widget([
    'options' => $widgetOptions
]);

$saveDatasetUrl = Url::to(['/dataCollection/transaction/update-trait', 'transactionId' => $transactionId]);

$this->registerJs(<<<JS

    // Adjust trait value input field size
    $('.my-container, .editable-field').css('width', '525px');

    /**
     * Delays execution until a specified time period has passed since the last call
     */
    function debounce(func, delay) {
        let timeoutId;
        return function () {
            clearTimeout(timeoutId);
            timeoutId = setTimeout(() => {
            func.apply(this, arguments);
            }, delay);
        };
    }

    /**
     * Enable footer buttons upon trait value input or NA checkbox selection
     */

    function enableFooterButtons() {
        const valueInput = $('.editable-field').val();
        const noSelectedItems = $('#selected-items-div').children('b').eq(0).text() == '';
        const noItems = $('.summary').children('b').eq(2).text() == '';
        const naCheckbox = $('#bulk-update-na-checkbox').prop('checked');

        if (valueInput != '' || naCheckbox) {
            $('#plot-trait-bulk-update-selected-btn').toggleClass('disabled', noSelectedItems);
            $('#plot-trait-bulk-update-all-btn').toggleClass('disabled', noItems);
        } else{
            $('.plot-trait-bulk-update-btn').toggleClass('disabled', true);
        }

    }
    $(document).on('keypress keyup blur paste', '.editable-field', debounce(enableFooterButtons, 1));
    $(document).on('change', '.editable-field', enableFooterButtons);
    $(document).on('change', '#bulk-update-na-checkbox', enableFooterButtons);

    enableFooterButtons();

    /**
    * Save new/updated traits for SELECTED/ALL plots
    */
    $('.plot-trait-bulk-update-btn').one('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        const naValue = $('#bulk-update-na-checkbox').prop('checked');
        const traitVariableInfo = $('#plot-traits-dropdown-id').val().split('-') ;
        const traitVariableAbbrev = traitVariableInfo[0] ?? '';
        const traitVariableId = traitVariableInfo[2] ?? 0;
        const traitValue = naValue ? 'NA' : $('#' + $('#plot-traits-dropdown-id').val()).val();
        const browserId = '#' + $('#plot-browser-container-id').children("div:first").attr('id') + '-pjax';
        const completeBrowserId = '#complete-browser-pjax';
        const summaryBrowserId = '#data-collection-browser-pjax';
        const selection = $(this).attr('selection');
        let icon = '<i class="material-icons orange-text">warning</i>';
        let message = 'No updated traits.';
        let recordIds = [];

        // Display progress
        displayLoadingIndicator(true);

        // Retrieve selected plots
        $("input:checkbox.select-plot-row").each(function () {

            if ($(this).prop("checked") === true) {
                recordIds.push($(this).attr("id"));
            }
        });

        // Build POST data
        const data = {
            variableAbbrev: traitVariableAbbrev,
            variableId: traitVariableId,
            recordIds: JSON.stringify(recordIds),
            value: traitValue,
            selection: selection,
        };

        $.ajax({
            url: "$saveDatasetUrl",
            type: 'post',
            data: data,
            dataType: 'JSON',
            success: function (response) {
                const dataCount = response['count'];

                if(dataCount){
                    icon = "<i class='material-icons green-text'>check</i>"
                    message = 'The selected trait for ' + dataCount + ' plot(s) has been successfully updated';

                    // If successfully updated, reload plot trait and complete browser tables
                    $.pjax.reload({container: browserId}).done(function() {
                        $.pjax.reload({container: completeBrowserId}).done(function(){
                            $.pjax.reload({container: summaryBrowserId});
                        });
                    });
                }

                $('#plot-trait-bulk-update-modal').modal('hide');
                var hasToast = $('body').hasClass('toast');
                if (!hasToast) {
                    $('.toast').css('display','none');
                }
                Materialize.toast(icon + '&nbsp;' + message, 5000);

            },
            error: function (jqXHR, exception) {
                $('#plot-trait-bulk-update-modal').modal('hide');
                error = logError(jqXHR, exception);
                message = '<i class="material-icons red-text">close</i></button>' + 'Error updating the traits: ' + error;
                Materialize.toast(message, 5000);
            }
        });

    });

    /**
     * Show progress bar, hide close and disable footer buttons
     */
    function displayLoadingIndicator(toggleDisplay) {
        const closeButtonId = '#plot-trait-bulk-update-modal > div > div > div.modal-header > button';

        $('.bulk-update-progress').toggleClass('hidden', !toggleDisplay);
        $('.plot-trait-bulk-update-btn').toggleClass('disabled', toggleDisplay);

        if(!toggleDisplay) {
            $(closeButtonId + ', #plot-trait-bulk-update-modal-body, #plot-trait-bulk-update-modal-body').show();
        }else{
            $(closeButtonId + ', #plot-trait-bulk-update-modal-body, #plot-trait-bulk-update-modal-body').hide();
        }
    }

    function logError(jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connected. Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.' + jqXHR.responseText;
        }
        // logging error, please do not remove
        console.log(jqXHR.responseText)
        return msg;
    }
JS
);