<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have dataProvidereived a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<?php

use kartik\grid\GridView;

/**
 * Renders the summary of suppressing selected plot records
 */
$attributes = [

    [

        'label' => 'Trait',
        'format' => 'html',
        'value' => function ($dataProvider) {
            $isComputed = '';
            if ($dataProvider['isComputed']) {
                $isComputed = '&nbsp;<font class="badge grey">COMPUTED</font>';
            }
            return $dataProvider['variableLabel'] . $isComputed;
        },

    ],
    [
        'label' => 'No. of trait values',
        'format' => 'raw',
        'value' => function ($dataProvider) {
            return $dataProvider['suppressedCount'];
        },

    ],

];
?>

<div class="row">
    <?php
    $noData = true;
    $showUndo = false;
    $isComputed = '';

    foreach ($dataProvider->getModels() as $row) {
        if ($row["suppressedCount"] > 0) {
            $noData = false;
        }
    }
    if ($dataProvider->getCount() > 0) {

        if ($noData &&  $isSuppressed === "true") {

            echo '   <div class="alert error"><div class="card-panel">' . \Yii::t('app', 'There is no data to suppress') . '.</div></div>';
        } else if ($noData && $isSuppressed === "false") {

            echo '   <div class="alert error"><div class="card-panel">' . \Yii::t('app', 'There is no data to unsuppress') . '.</div></div>';
            $showUndo = true;
        } else if ($isSuppressed === "true") {

            echo '<div class="alert warning"><div class="card-panel">' . \Yii::t('app', 'The following records will be ') . '<strong>' . \Yii::t('app', 'suppressed ') . '</strong>' . \Yii::t('app', 'in this transaction.' . ' Click') . ' ' . \Yii::t('app', 'Apply ') . ' ' . \Yii::t('app', 'to confirm changes') . '.</div></div>';
        } else {

            echo '<div class="alert warning"><div class="card-panel">' . \Yii::t('app', 'The following records will be ') . '<strong>' . \Yii::t('app', 'unsuppressed ') . '</strong> ' . \Yii::t('app', 'in this transaction.') . '.</div></div>';

            $showUndo = true;
        }
    } else {

        if ($noData && $isSuppressed === "true") {

            echo '   <div class="alert error"><div class="card-panel">' . \Yii::t('app', 'There is no data to suppress') . '.</div></div>';
        } else {
            echo '   <div class="alert error"><div class="card-panel">' . \Yii::t('app', 'There is no data to unsuppress') . '. </div></div>';

            $showUndo = true;
        }
    }
    if ($dataProvider->getCount() > 0) {

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $attributes,
            'bordered' => true,
            'striped' => true,
            'condensed' => false,
            'responsive' => true,
            'hover' => true,
            'panel' => false,
            'id' => 'suppress-summary-browser',
            'toolbar' => false,
            'summary' => '',
        ]);
    }
    $totalCount = 0;
    foreach ($dataProvider->getModels() as $r) {
        $totalCount += $r["suppressedCount"];
    }
    
    if (isset($computedDataProvider) && $computedDataProvider->getCount() > 0) {
       
        foreach ($computedDataProvider->getModels() as $row) {
            if ($row["isComputed"] && $row["suppressedCount"] > 0) {
                $isComputed =  \Yii::t('app', 'The following traits that are computed will be updated.');
            }
            $totalCount += $r["suppressedCount"];
        }
        echo '<div class="alert warning"><div class="card-panel">' . $isComputed . '</div></div>';
        echo GridView::widget([
            'dataProvider' => $computedDataProvider,
            'columns' => $attributes,
            'bordered' => true,
            'striped' => true,
            'condensed' => false,
            'responsive' => true,
            'hover' => true,
            'panel' => false,
            'id' => 'suppress-summary-browser',
            'toolbar' => false,
            'summary' => '',
        ]);
    }
    ?>

        <?php
       

        ?>
</div>
<div>
    <div class="col s9">
        <?php if ($totalCount > $threshold) {
            echo '<span class="badge grey white-text" style="margin-left:">Note</span>&emsp;<strong><span class="black-text text-left">' . \Yii::t('app', 'Total no. of records greater than ' . $threshold . ' will be processed in the background.') . '</span></strong>&emsp;&emsp;';
        }
        ?>
    </div>
    <div class="col s3">
        <strong class=""><?php echo \Yii::t('app', 'Total') . ":"; ?> &emsp;<span id="<?php echo $dataLevel; ?>-suppress-total-count-id"><?php echo $totalCount; ?></span></strong>
    </div>
</div>

<?php

$js = <<<JS

if(showApply){
    $('.suppress-btn-id').addClass('hidden');
}else{
    $('.suppress-btn-id').removeClass('hidden');
}

JS;
if ($noData) {

    Yii::$app->view->registerJs("var showApply=true;" . $js,  \yii\web\View::POS_END);
} else {

    Yii::$app->view->registerJs("var showApply=false;" . $js,  \yii\web\View::POS_END);
}

if ($showUndo || $showUndo === 'true') {

    Yii::$app->view->registerJs("
    $('.undo-suppress-btn-id').removeClass('hidden'); 
    $('.suppress-btn-id').addClass('hidden');",  \yii\web\View::POS_END);
} else {

    Yii::$app->view->registerJs("
    $('.suppress-btn-id').removeClass('hidden'); 
    $('.undo-suppress-btn-id').addClass('hidden');
    ",  \yii\web\View::POS_END);
}
