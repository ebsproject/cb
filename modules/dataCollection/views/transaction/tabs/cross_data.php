<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;
use app\modules\dataCollection\models\CrossData;
use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;

/**
 * Renders Cross Data browser
 */
$crossDataModel = \Yii::$container->get('\app\modules\dataCollection\models\CrossData');
$filterChips = $crossDataModel->getFilterChips($transactionId);
$statusTags = $crossDataModel->getStatusTags($variables);

$params = Yii::$app->request->queryParams;
$resetParams = $params;

unset($resetParams['CrossData']);
unset($resetParams['cross-dp-' . $transactionId . '-page']);
unset($resetParams['cross-dp-' . $transactionId . '-per-page']);
$config = [
    "transactionId" => $transactionId,
    'variables' => $variables,
];

$searchModel = new CrossData($crossDataModel->transaction, $crossDataModel->terminalTransaction, $crossDataModel->userDashboardConfig, $config);

$dataProvider = $searchModel->search($params);
$pageCount = $dataProvider['pageCount'];
$page = $dataProvider['page'];
$dataProvider = $dataProvider['dataProvider'];

$dynaGridId = \Yii::$container->get('\app\models\TerminalTransaction')->getRecentFileId($transactionId);
$dynaGridId = 'cross-data-browser-' . $dynaGridId;
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$dynaGridId]);


$dynaGrid = DynaGrid::begin([
    'columns' => $searchModel->crossColumns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'id' => $dynaGridId,
            'neverTimeout' => true,
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'filterRowOptions' => [
            'class' => 'kartik-sheet-style'
        ],
        'tableOptions' => [
            'id' => 'cross-table-id'
        ],
        'rowOptions' => [
            "class" => "cross-row-record"
        ],
        'showPageSummary' => false,
        'responsiveWrap' => false,
        'panel' => [
            'heading' => $statusTags,
            'before' => $filterChips,
            'after' => false,
            'floatHeaderOptions' => [
                'position' => 'absolute',
            ],
        ],
        'toolbar' =>  [
            ['content' => '<div id="selected-items-div" style="margin-top:10px;">No selected items.</div>'],
            [
                'content' =>
                Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    '',
                    [
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app', 'Reset grid'),
                        'id' => 'cross-reset-grid-id',
                        'data-pjax' => true
                    ]
                ) . '{dynagridFilter}{dynagridSort}{dynagrid}',
            ],

        ],
        'floatOverflowContainer' => true,
        'resizableColumns' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $dynaGridId
    ],

]);
DynaGrid::end();

    // variables for change page size
    Yii::$app->view->registerJs("
        var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
        browserId = '".$dynaGridId."',
        currentDefaultPageSize = '".$currentDefaultPageSize."'
        ;",
        \yii\web\View::POS_HEAD);
    // js file for data browsers
    $this->registerJsFile("@web/js/data-browser.js", [
        'depends' => ['app\assets\AppAsset'],
        'position'=>\yii\web\View::POS_END
    ]);

    // For trait header css
    $this->registerCss('
        #cross-table-id > thead > tr:nth-child(1) > th.dataset-color-theme.darken-1.white-text.attrHeader.kv-nowrap > a{
            color: white !important;
        }
    ');

    $this->registerJs(
    " var varCrossResetUrl='" . Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $resetParams)) . "'; "
        . " var varPlotDataTotalCount=" . $dataProvider->totalCount . "; "
        . " var plotIdString=''; "
        . " var varLocationName='$locationName'; "
        .
        <<<JS
    renderCrossFilters();

    if($page > $pageCount){
        $('#cross-reset-grid-id').trigger('click');
    }

    $('#cross-table-id').on('reflowed', function (e) {

        $('.kv-thead-float').css('z-index', 997);    // fixed headers stay behind modals, sidenavs after pjax reload
    });
    $('.dataset-color-theme').addClass($('#search-bar-input-nav').attr('class').split('  ')[0]);
    
    $(document).ready(function() {
    // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
    });

    $(document).on('pjax:success', function (e) {

        renderCrossFilters();
        $('.dataset-color-theme').addClass($('#search-bar-input-nav').attr('class').split('  ')[0]);
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        }
        );
    });

    // Renders filter tags, legend, reset data browser
    function renderCrossFilters() {

        $(".close-chip").click(function (e) {

            $.pjax.reload({
                container: '#$dynaGridId-pjax',
                replace: false,
                url: $(this).data('url')
            });
        });

        $('#cross-remove-all-filters').on('click', function (e) {
            e.preventDefault();
            $.pjax.reload({
                container: '#$dynaGridId-pjax',
                replace: false,
                url: varCrossResetUrl
            });
        });

        $('#cross-reset-grid-id').on('click', function (e) {
            e.preventDefault();

            $.pjax.reload({
                container: '#$dynaGridId-pjax',
                replace: true,
                url: varCrossResetUrl
            });

        });

        $(".cross-status-btn").click(function (e) {
            $.pjax.reload({
                container: '#$dynaGridId-pjax',
                replace: false,
                url: varCrossResetUrl + '&CrossData[status]=' + $(this).attr("data-status")
            });
        });

    }
JS
);

?>