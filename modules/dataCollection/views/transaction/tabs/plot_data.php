<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<?php

use app\modules\dataCollection\models\PlotData;
use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;

use kartik\dynagrid\DynaGrid;
use kartik\widgets\Select2;

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

$renderTraitValueFieldUrl = Url::to(['/dataCollection/transaction/render-trait-value-field',
    'transactionId' => $transactionId]);

/**
 * Renders Plot Data browser
 */
$plotDataModel = \Yii::$container->get('app\modules\dataCollection\models\PlotData');
$filterChips = $plotDataModel->getFilterChips($transactionId);
$statusTags = $plotDataModel->getStatusTags($variables);

$params = Yii::$app->request->queryParams;
$resetParams = $params;

unset($resetParams['PlotData']);
unset($resetParams['terminal-dp-' . $transactionId . '-page']);
unset($resetParams['terminal-dp-' . $transactionId . '-per-page']);
$config = [
    "transactionId" => $transactionId,
    'variables' => $variables,
];

$searchModel = new PlotData(
    $plotDataModel->transaction,
    $plotDataModel->terminalTransaction,
    $config
);

$dataProvider = $searchModel->search($params);
$pageCount = $dataProvider['pageCount'];
$page = $dataProvider['page'];
$dataProvider = $dataProvider['dataProvider'];
    
// display occurrence data if used in the trait calculation
if ($occurrenceData) {
    echo '<ul class="collapsible">
        <li>
        <div class="collapsible-header"><i class="material-icons">info_outline</i>Occurrence Data</div>
        <div class="collapsible-body" style="overflow-x:auto;">
    ';
    foreach ($occurrenceData as $od) {
        $index = ucwords($od['label']);
        echo
            '<div class="col-md-4" style="padding-left:10px!important;">
                <dl>
                    <dt title="' . $index . '">' . $index . '</dt>
                    <dd><div id="'.$index.'">'.$od['value'].'</div></dd>
                </dl>
            </div>
            ';
    }
    echo '</div></li></ul>' ;
}

// Get transaction Number
$transactionNo = \Yii::$container->get('app\models\TerminalTransaction')->getRecentFileId($transactionId);

$dynaGridId = 'plot-data-browser-'.$transactionNo;
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$dynaGridId]);

$dynaGrid = DynaGrid::begin([
    'columns' => $searchModel->plotColumns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'id' => $dynaGridId,
            'neverTimeout' => true,
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'filterRowOptions' => [
            'class' => 'kartik-sheet-style'
        ],
        'tableOptions' => [
            'id' => 'plot-table-id'
        ],
        'rowOptions' => [
            "class" => "plot-row-record"
        ],
        'showPageSummary' => false,
        'responsiveWrap' => false,
        'panel' => [
            'heading' => $statusTags,
            'before' => $filterChips,
            'after' => false,
            'floatHeaderOptions' => [
                'position' => 'absolute',
            ],
        ],
        'toolbar' =>  [
            ['content' => '<div id="selected-items-div" style="margin-top:10px;">No selected items.</div>'],
            ['content' =>

                Html::a(
                    \Yii::t('app', 'Suppress'),
                    null,
                    [
                        'id' => 'suppress-btn-id',
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app', 'Suppress'),
                        'data-pjax' => true,
                        "style" => 'margin-right:5px;background-color: #5cb85c !important;'
                    ]
                ) .
                Html::a(
                    \Yii::t('app', 'Unsuppress'),
                    null,
                    [
                        'id' => 'unsuppress-btn-id',
                        'class' => 'btn btn-default grey',
                        'title' => \Yii::t('app', 'Unsuppress'),
                        'data-pjax' => true,
                        "style" => 'margin-right:5px;'
                    ]
                ) .
                Html::a(
                    '<i class="glyphicon glyphicon-edit"></i>',
                    null,
                    [
                        'id' => 'plot-trait-bulk-update-btn-id',
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app', 'Bulk update plot traits'),
                        'data-pjax' => true,
                        "style" => 'margin-right:5px;'
                    ]
                ) .
                Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    '',
                    [
                        'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid'),
                        'id' => 'plot-reset-grid-id',
                        'data-pjax' => true
                    ]
                ) . '{dynagridFilter}{dynagridSort}{dynagrid}'
            ],

        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'resizableColumns' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $dynaGridId
    ],

]);
DynaGrid::end();

?>
<!-- Selected Plot Suppress form modal -->
<div id="plot-suppress-record-modal" class="fade modal" tabindex="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <span class="plot-loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div style="float:left">
                    <h4 id="plot-suppress-modal-header-label" class="hidden"><?= \Yii::t('app', 'Suppress Plot Data') ?></h4>
                    <h4 id="plot-unsuppress-modal-header-label" class="hidden"><?= \Yii::t('app', 'Unsuppress Plot Data') ?></h4>
                </div>

            </div>
            <div class="modal-body" id="suppress-body-modal">
                <div class="row">
                    <div class="col  hidden">
                        <ul class="tabs plot-suppress-tabs">

                            <li class="tab col s6"><a class="active" href="#plot-condition-tab-id"><?php echo Yii::t('app', 'Add condition'); ?></a></li>
                            <li class="tab col s6 disabled" id="plot-supress-tab-1-id"><a href="#plot-suppress-summary-tab-id" id="suppress-summary-tab-link-id"><?php echo Yii::t('app', 'Summary'); ?></a></li>

                        </ul>
                    </div>
                    <div id="plot-suppress-summary-tab-id" class="col s12">
                        <div id="plot-suppress-summary-body-id">
                        </div>
                    </div>
                    <div id="plot-condition-tab-id">
                        <div id="plot-suppress-warning">
                            <?php echo \Yii::t('app', 'You have selected'); ?>
                            <span id="suppress-plot-count" class="bold"></span>.

                        </div>
                        <div class="" style="width:50%; padding:10px">
                            <input type="hidden" name="transactionId" value="<?php echo $transactionId; ?>">

                            <?php
                            $plotVarOptions = [];
                            $plotTraitOptions = [];
                            $plotVarData = [];
                            $plotTraitData = [];
                            foreach ($variables as $i => $v) {

                                $abbrev = $v['abbrev'];
                                $formattedVar = "$abbrev-observation-".$v["variableDbId"];
                                if ($v['dataUnit'] == 'plot') {
                                    $variableModel = new \app\models\Variable();
                                    $variable = $variableModel->getOne($v["variableDbId"]);
                                    $variable = $variable["data"];
                                    $plotTraitOptions[$formattedVar] = $plotVarOptions[$abbrev] = ucFirst($variable['name']) .
                                        ' (' . $variable["label"] . ')';
                                    if ($i == 0) {
                                        $suppressVarLabel = $variable["label"];
                                        $suppressVarAbbrev = $variable["abbrev"];
                                    }
                                    $plotTraitData[$formattedVar] = $plotVarData[$abbrev] = [
                                        'data-label' => $variable["label"],
                                        'data-id' => $variable["variableDbId"],
                                        'data-data_type' => $variable["dataType"],
                                        'data-data_level' => $v["dataUnit"],
                                        'data-abbrev' => $abbrev
                                    ];
                                }
                            }
                            echo Select2::widget([
                                'name' => 'suppress-plot-var',
                                'data' => $plotVarOptions,
                                'pluginOptions' => [
                                    'allowClear' => false,

                                ],
                                'options' => [
                                    'multiple' => false,
                                    'placeholder' => \Yii::t('app', 'Select trait'),
                                    "id" => "plot-suppress-record-select2-id",
                                    'options' => $plotVarData,
                                ]
                            ]);
                            ?>
                            <div class="plot-suppress-remarks-label hidden">
                                <label>
                                    <br><?php echo \Yii::t('app', 'Suppress Remarks'); ?>
                                    <span class="red-text"><em><?php echo \Yii::t('app', '*'); ?></em></span>
                                </label>
                            </div>
                            <div class="input-field plot-suppress-remarks-div ">

                                <?php echo Html::textArea('plot-suppress-remarks-name', "", ['id' => 'plot-suppress-remarks-id', "class" => "form-control hidden"]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <?php

                echo Html::a(Yii::t('app', 'Suppress'), '#', ['id' => 'plot-suppress-next-btn-id', 'style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'disabled pull-right btn waves-effect waves-light']);

                echo Html::a(\Yii::t('app', 'UnSuppress'), '#', ['id' => 'plot-unsuppress-next-btn-id', 'style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'disabled pull-right btn grey lighten-2 black-text  waves-effect waves-light']);

                echo Html::a(\Yii::t('app', 'Apply'), '#', ['style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'hidden waves-effect waves-light pull-right btn plot-suppress-btn-id plot-confirm-suppress-btn']);

                echo Html::a(\Yii::t('app', 'Apply'), '#', ['style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'hidden waves-effect waves-light pull-right btn plot-undo-suppress-btn-id plot-confirm-suppress-btn hidden']);

                echo Html::a(\Yii::t('app', 'Back'), '#condition-tab-id', ['style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'waves-effect waves-light pull-right btn btn-secondary grey lighten-2 z-depth-1 plot-suppress-back-btn-id black-text hidden']);
                ?>
            </div>
        </div>
    </div>
</div>

<!-- Suppress form modal -->
<div id="plot-suppress-rule-modal" class="fade modal" tabindex="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <span class="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div style="float:left">
                    <h4 id="suppress-modal-header-label" class="hidden"><?= \Yii::t('app', 'Suppress Plot Data') ?></h4>
                    <h4 id="unsuppress-modal-header-label" class="hidden"><?= \Yii::t('app', 'Unsuppress Plot Data') ?></h4>
                </div>

            </div>
            <div class="modal-body" id="factors-modal">

                <div class="row">
                    <div class="col s12 hidden">
                        <ul class="tabs suppress-tabs">

                            <li class="tab col s6"><a class="active" href="#condition-tab-id"><?php echo \Yii::t('app', 'Add condition'); ?></a></li>
                            <li class="tab col s6 disabled" id="supress-tab-1-id"><a href="#suppress-summary-tab-id" id="suppress-summary-tab-link-id"><?php echo \Yii::t('app', 'Summary'); ?></a></li>

                        </ul>
                    </div>
                    <div id="suppress-summary-tab-id" class="col s12 section scrollspy ">
                        <div id="suppress-summary-body-id">
                        </div>

                    </div>
                    <div id="condition-tab-id" class="col s12 section scrollspy">

                        <div class="" style="width:50%; padding:10px">
                            <input type="hidden" name="transactionId" value="<?php echo $transactionId; ?>">

                            <label><?php echo \Yii::t('app', 'Select trait'); ?></label>
                            <?php
                            echo Select2::widget([
                                'name' => 'suppress-var-name',
                                'data' => $plotVarOptions,
                                'pluginOptions' => [
                                    'allowClear' => false,

                                ],
                                'options' => [
                                    'multiple' => false,
                                    'placeholder' => 'Select trait',
                                    "id" => "plot-suppress-rule-select2-id",
                                    "class" => "filter-select-operator",
                                    'options' => $plotVarData,
                                ]
                            ]);
                            ?>

                        </div><br>
                        <div id="condition-id">

                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php

                echo Html::a(\Yii::t('app', 'Apply'), '#', ['style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'hidden waves-effect waves-light pull-right btn suppress-btn-id confirm-suppress-btn']);

                echo Html::a(\Yii::t('app', 'Apply'), '#', ['style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'hidden waves-effect waves-light pull-right btn undo-suppress-btn-id confirm-suppress-btn']);

                echo Html::a(\Yii::t('app', 'Back'), '#condition-tab-id', ['style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'waves-effect waves-light pull-right btn btn-secondary grey lighten-2 z-depth-1 suppress-back-btn-id black-text hidden']);

                echo Html::a(\Yii::t('app', 'Suppress'), '#', ['id' => 'suppress-next-btn-id', 'style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'hidden pull-right btn waves-effect waves-light']);

                echo Html::a(\Yii::t('app', 'UnSuppress'), '#', ['id' => 'unsuppress-next-btn-id', 'style' => 'margin-right:5px;', 'type' => 'button', 'class' => 'hidden pull-right btn grey lighten-2 black-text  waves-effect waves-light']);

                ?>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="suppress-modal" id="suppress-action-id" value="false"></input>
<?php

    // ---------------- BULK UPDATE ----------------

    // Modal for bulk update
    Modal::begin([
        'id' => 'plot-trait-bulk-update-modal',
        'header' => '<h4><i class="glyphicon glyphicon-edit"></i> Bulk Update</h4>',
        'footer' =>
            Html::a(
                \Yii::t('app', 'Apply to selected ('). '<strong id=selected-traits-count></strong>)',
                '#',
                [
                    'class' => 'btn btn-primary waves-effect waves-light modal-close plot-trait-bulk-update-btn disabled',
                    'id' => 'plot-trait-bulk-update-selected-btn',
                    'selection' => 'selected',
                    'title' => Yii::t('app', 'Update selected records')
                ]
            ) . '&emsp;'
            . Html::a(
                \Yii::t('app', 'Apply to all ('). '<strong id=all-traits-count></strong>)',
                '#',
                [
                    'class' => 'btn btn-primary waves-effect waves-light modal-close plot-trait-bulk-update-btn disabled',
                    'id' => 'plot-trait-bulk-update-all-btn',
                    'selection' => 'all',
                    'title' => Yii::t('app', 'Update all records')
                ]
            ) . '&emsp;',
        'size' =>'modal-md',
        'options' => [
            'data-backdrop' => 'static',
            'style' => 'z-index: 1040;'
        ]
    ]);

    echo '<i class="bulk-update-progress hidden">Updating...</i>';
    echo '<div class="progress hidden bulk-update-progress"><div class="indeterminate"></div></div>';
    echo '<div id="plot-trait-bulk-update-modal-body">';
    echo '<p>'.\Yii::t('app', 'Bulk update the following trait values for the plots.
        The changes will be reflected after closing the modal.').'</p>';
    echo '<div id="bulk-update-trait-field" style="padding-left:15px">';
    echo '<label class="control-label">';
    echo \Yii::t('app', 'Trait'.' '.'<span class="required">*</span>').'</label>';
    echo Select2::widget([
        'name' => 'plot-traits-dropdown',
        'data' => $plotTraitOptions,
        'pluginOptions' => [
            'allowClear' => false,
        ],
        'options' => [
            'multiple' => false,
            'placeholder' => Yii::t('app', 'Select a trait'),
            'id' => 'plot-traits-dropdown-id',
            'options' => $plotTraitData,
        ]
    ]);
    echo '</div>';
    echo '<div id="bulk-update-value-field" style="margin-top:10px"></div>';
    echo '<div id="bulk-update-na" style="margin-top:10px; padding-left:15px;">';
    echo '<input type="checkbox" class="filled-in" id="bulk-update-na-checkbox">';
    echo '<label for="bulk-update-na-checkbox">NA</label>';
    echo '</div>';
    echo '</div>';
    Modal::end();
    
    // For trait header css
    $this->registerCss('
        #plot-table-id > thead > tr:nth-child(1) > th.dataset-color-theme.darken-1.white-text.attrHeader.kv-nowrap > a{
            color: white !important;
        }
    ');

    // variables for change page size
    Yii::$app->view->registerJs("
        var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
        browserId = '".$dynaGridId."',
        currentDefaultPageSize = '".$currentDefaultPageSize."'
        ;",
        \yii\web\View::POS_HEAD);
    // js file for data browsers
    $this->registerJsFile("@web/js/data-browser.js", [
        'depends' => ['app\assets\AppAsset'],
        'position'=>\yii\web\View::POS_END
    ]);
    $this->registerJs(
    " var varPlotResetUrl='" . Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $resetParams)) . "'; "
        . " var varPlotDataTotalCount=" . $dataProvider->totalCount . "; "
        . " var plotIdString=''; "
        . " var varLocationName='$locationName'; "
        .
        <<<JS
    renderPlotFilters();
    renderCheckbox("plot");
    suppressPlot();
    
    $('#plot-suppress-record-select2-id').val('$suppressVarAbbrev').trigger('change');

    $('#plot-table-id').on('reflowed', function (e) {

        $('.kv-thead-float').css('z-index', 997);    // fixed headers stay behind modals, sidenavs after pjax reload
    });
    $('.dataset-color-theme').addClass($('#search-bar-input-nav').attr('class').split('  ')[0]);

    $(document).ready(function() {
    // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        
        if($page > $pageCount){
            $('#plot-reset-grid-id').trigger('click');
        }
    });

    $(document).on('pjax:success', function (e) {

        $('#plot-suppress-record-select2-id').val($('#plot-suppress-record-select2-id').val()).trigger('change');  
        renderPlotFilters();
        renderCheckbox("plot");
        $('.dataset-color-theme').addClass($('#search-bar-input-nav').attr('class').split('  ')[0]);
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        }
        );
    });
    /**
    * Show suppress modal
    * @param  element e Suppress button
    * @return null
    */
    $(document).on('click', '#suppress-btn-id', function (e) {
        e.preventDefault();
        $("#suppress-action-id").val("true")
        $('#plot-suppress-rule-select2-id').val('$suppressVarAbbrev').trigger('change');
        if (selectAll == '' || plotRows.length == 0) {
            $('#plot-suppress-rule-modal').modal('show');
        } else {
            $('#plot-unsuppress-next-btn-id').addClass('hidden');
            $('#plot-suppress-next-btn-id').removeClass('hidden');
            
            $(".plot-suppress-remarks-label").removeClass("hidden");
            $("#plot-suppress-remarks-id").removeClass("hidden");
            $('#plot-suppress-record-modal').modal('show');

            $('#plot-suppress-modal-header-label').removeClass('hidden');
            $('#plot-unsuppress-modal-header-label').addClass('hidden');
        }

    });

    /**
    * Show unsuppress modal
    * @param  element e Suppress button
    * @return null
    */
    $(document).on('click', '#unsuppress-btn-id', function (e) {
        e.preventDefault();
        $("#suppress-action-id").val("false")
        $('#plot-suppress-rule-select2-id').val('$suppressVarAbbrev').trigger('change');
        
        if (selectAll == '' || plotRows.length == 0) {
            $('#plot-suppress-rule-modal').modal('show');
        } else {
            $('#plot-suppress-next-btn-id').addClass('hidden');
            $('#plot-unsuppress-next-btn-id').removeClass('hidden');

            $(".plot-suppress-remarks-label").addClass("hidden");
            $("#plot-suppress-remarks-id").addClass("hidden");
            $('#plot-suppress-record-modal').modal('show');

            $('#plot-unsuppress-modal-header-label').removeClass('hidden');
            $('#plot-suppress-modal-header-label').addClass('hidden');
        }

    });

    /**
    * Show bulk update form modal
    * @param  element e bulk update button
    * @return null
    */
    $(document).on('click', '#plot-trait-bulk-update-btn-id', function (e) {
        e.preventDefault();
        const summary = $('.summary').children('b').eq(3).text();
        const selected = $('#selected-items-div').children('b').eq(0).text();
        const totalCount = summary == '' ? '0' : summary;
        const totalSelectedCount = selected == '' ? '0' : selected;
        displayLoadingIndicator(false);

        $('#plot-traits-dropdown-id').val('').trigger('change');
        $('#bulk-update-value-field').hide();
        $('#bulk-update-na').hide();
        $('.plot-trait-bulk-update-btn').toggleClass('disabled', true);

        $('#selected-traits-count').text(totalSelectedCount);
        $('#all-traits-count').text(totalCount);
        $('#plot-trait-bulk-update-modal').modal('show');
    });

    /**
     * Show progress bar, hide close and disable footer buttons
     */
    function displayLoadingIndicator(toggleDisplay) {
        const closeButtonId = '#plot-trait-bulk-update-modal > div > div > div.modal-header > button';

        $('.bulk-update-progress').toggleClass('hidden', !toggleDisplay);
        $('.plot-trait-bulk-update-btn').toggleClass('disabled', toggleDisplay);

        if(!toggleDisplay) {
            $(closeButtonId + ', #plot-trait-bulk-update-modal-body').show();
        }else{
            $(closeButtonId + ', #plot-trait-bulk-update-modal-body').hide();
        }
    }

    /**
    * Displays trait value input field based on the selected trait, and NA checkbox
    * @return null
    */
    $(document).on('change', '#plot-traits-dropdown-id', function () {
        const variableId = $('option:selected', this).attr('data-id');
        const variableAbbrev = $('option:selected', this).attr('data-abbrev');
        $('#bulk-update-na-checkbox').prop('checked', false);

        if(variableId !== undefined){
            $.ajax({
                url: "$renderTraitValueFieldUrl",
                type: 'post',
                data: {
                    variableDbId: variableId,
                    variableAbbrev: variableAbbrev,
                    transactionId: "$transactionId"
                },
                success: function (data) {
                    $('#bulk-update-value-field').html(data);
                    $('#bulk-update-value-field').show();
                    $('#bulk-update-na').show();
                },
                error: function (jqXHR, exception) {
                    msg = logError(jqXHR, exception);
                    const toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;
                    Materialize.toast(toastContent, 5000);
                }

            });

            $('#bulk-update-na-checkbox').on('click', function () {
                if ($(this).prop('checked')) {
                    $('.my-container, .editable-field').prop("disabled", true);
                    $('.editable-field').val('');
                } else {
                    $('.my-container, .editable-field').prop("disabled", false);
                }
            });
        }
    });

    /**
    * Checks all records
    * @param  element e Suppress button
    * @return null
    */
    $(document).on('click', '#check-all-id', function (e) {
        e.preventDefault();
        selectAll = 'all';
        if ($('#select-all-plot-id').prop("checked") === true) {
            $('#select-all-plot-id').trigger("click");
        }
        $('#select-all-plot-id').trigger("click");
        if (varPlotDataTotalCount == 1) {
            $('#selected-items-div').html('Selected <b>' + varPlotDataTotalCount + '</b> item.');
        } else {
            $('#selected-items-div').html('Selected <b>' + varPlotDataTotalCount + '</b> items.');
        }

    });

    /**
    * Checks all records in the current page
    * @param  element e Suppress button
    * @return null
    */
    $(document).on('click', '#check-page-id', function (e) {
        e.preventDefault();
        selectAll = 'page';
        $('#select-all-plot-id').attr('checked', 'checked');
        $('#select-all-plot-id').prop('checked', true);
        $('.select-plot-row').attr('checked', 'checked');
        $('.select-plot-row').prop('checked', true);
        $(".select-plot-row:checkbox").parent("td").parent("tr").addClass("grey lighten-4");

        plotRows = [];
        $("input:checkbox.select-plot-row").each(function () {

            if ($(this).prop("checked") === true) {
                plotRows.push($(this).attr("id"));
            }

        });
        if (plotRows.length == 1) {
            $('#selected-items-div').html('Selected <b>' + plotRows.length + '</b> item.');
        } else {
            $('#selected-items-div').html('Selected <b>' + plotRows.length + '</b> items.');
        }

    });

    // Renders filter tags, legend, reset data browser
    function renderPlotFilters() {

        $(".close-chip").click(function (e) {

            $.pjax.reload({
                container: '#$dynaGridId-pjax',
                replace: false,
                url: $(this).data('url')
            });
        });

        $('#plot-remove-all-filters').on('click', function (e) {
            e.preventDefault();
            $.pjax.reload({
                container: '#$dynaGridId-pjax',
                replace: false,
                url: varPlotResetUrl
            });
        });

        $('#plot-reset-grid-id').on('click', function (e) {
            e.preventDefault();

            $.pjax.reload({
                container: '#$dynaGridId-pjax',
                replace: true,
                url: varPlotResetUrl
            });

        });

        $(".plot-status-btn").click(function (e) {
            $.pjax.reload({
                container: '#$dynaGridId-pjax',
                replace: false,
                url: varPlotResetUrl + '&PlotData[status]=' + $(this).attr("data-status")
            });
        });

    }

    // Renders suppress buttons and other elements
    function suppressPlot() {
        /**
         * Show number of selected items on modal display
         */
        $('#plot-suppress-record-modal').on('show.bs.modal', function (e) {

            if (selectAll == 'all') {
                plotRows = [];
                if (varPlotDataTotalCount == 1) {
                    $('#suppress-plot-count').html(varPlotDataTotalCount + ' plot');
                } else {
                    $('#suppress-plot-count').html(varPlotDataTotalCount + ' plots');
                }
            } else {

                if (plotRows.length > -1) {
                    $('#suppress-plot-count').html(plotRows.length + '</b> plot');
                } else if (plotRows.length > 1) {
                    $('#suppress-plot-count').html(plotRows.length + '</b> plots');
                }
            }

        });

        // Show suppress remarks when a variable is selected
        $("#plot-suppress-record-select2-id").on('change', function (e) {

            $('#plot-unsuppress-next-btn-id').removeClass('disabled');

            if ($('#plot-suppress-remarks-id').val() !== '') {
                $('#plot-suppress-next-btn-id').removeClass('disabled');

            } else {
                $('#plot-suppress-next-btn-id').addClass('disabled');
            }

        });

        // Show suppress button when the remarks is supplied
        $('#plot-suppress-remarks-id').on('change keydown paste input', function () {

            if ($(this).val() !== '' && $("#plot-suppress-record-select2-id").val() !== '') {
                $('#plot-suppress-next-btn-id').removeClass('disabled');

            } else {
                $('#plot-suppress-next-btn-id').addClass('disabled');
            }
        });

        // SUPPRESS next -> Check if form is valid, if not, return error, if yes, show summary
        $('#plot-suppress-next-btn-id').on('click', function (e) {
            e.preventDefault();

            $('.plot-loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');

            $('#filter-error-summary-id').empty();
            $('#filter-error-summary-id').removeClass('error-summary');

            plotIdString = plotRows.join("|");
            $.ajax({
                url: "get-suppress-record-summary",
                type: 'post',
                data: {
                    condition: '' + plotIdString + '',
                    variableAbbrev: $("#plot-suppress-record-select2-id").val(),
                    remarks: $('#plot-suppress-remarks-id').val(),
                    transactionId: varTransactionId,
                    suppress: "true",
                    dataLevel: 'plot'
                },
                success: function (data) {

                    var data = $.parseJSON(data);
                    $('#plot-suppress-summary-body-id').empty();
                    $('#plot-supress-tab-1-id').removeClass('disabled');
                    $('#plot-suppress-summary-body-id').html(data);
                    $('ul.plot-suppress-tabs').tabs('select_tab', 'plot-suppress-summary-tab-id');

                    $('.plot-loading-div').html('');

                    $('#plot-suppress-next-btn-id').addClass('hidden');
                    $('#plot-unsuppress-next-btn-id').addClass('hidden');
                    $('.plot-suppress-back-btn-id').removeClass('hidden');
                    $('.plot-suppress-btn-id').removeClass('hidden');

                    $('.plot-undo-suppress-btn-id').addClass('hidden');

                    if (parseInt($('#plot-suppress-total-count-id').html()) == 0) {
                        $('.plot-confirm-suppress-btn').addClass('hidden');
                    }

                },
                error: function (jqXHR, exception) {
                    $('.plot-loading-div').html('');
                    msg = logError(jqXHR, exception);

                    var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                    Materialize.toast(toastContent, 5000);
                }

            });
        });

        // UNSUPPRESS next -> Check if form is valid, if not return error, if yes, show summary
        $('#plot-unsuppress-next-btn-id').on('click', function (e) {
            e.preventDefault();
            $('.plot-loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
            plotIdString = plotRows.join("|");
            $.ajax({
                url: "get-suppress-record-summary",
                type: 'post',
                data: {
                    condition: '' + plotIdString + '',
                    variableAbbrev: $("#plot-suppress-record-select2-id").val(),
                    remarks: $('#plot-suppress-remarks-id').val(),
                    transactionId: varTransactionId,
                    suppress: "false",
                    dataLevel: 'plot'
                },
                success: function (data) {

                    var data = $.parseJSON(data);
                    $('#plot-suppress-summary-body-id').empty();
                    $('#plot-supress-tab-1-id').removeClass('disabled');
                    $('#plot-suppress-summary-body-id').html(data);
                    $('ul.plot-suppress-tabs').tabs('select_tab', 'plot-suppress-summary-tab-id');

                    $('.plot-loading-div').html('');

                    $('#plot-suppress-next-btn-id').addClass('hidden');
                    $('#plot-unsuppress-next-btn-id').addClass('hidden');
                    $('.plot-suppress-back-btn-id').removeClass('hidden');

                    $('.plot-undo-suppress-btn-id').removeClass('hidden');
                    $('.plot-suppress-btn-id').addClass('hidden');

                    if (parseInt($('#plot-suppress-total-count-id').html()) == 0) {

                        $('.plot-confirm-suppress-btn').addClass('hidden');
                    }
                },
                error: function (jqXHR, exception) {
                    $('.plot-loading-div').html('');
                    msg = logError(jqXHR, exception);

                    var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                    Materialize.toast(toastContent, 5000);
                }

            });
        });

        // Triggers click of hidden first tab in the suppress modal
        $('.plot-suppress-back-btn-id').on('click', function (e) {
            e.preventDefault();
            $('ul.plot-suppress-tabs').tabs('select_tab', 'plot-condition-tab-id');
            var suppress = $("#suppress-action-id").val()
            if(suppress == 'true'){
                $('#plot-unsuppress-next-btn-id').addClass('hidden');
                $('#plot-suppress-next-btn-id').removeClass('hidden');
            }else{
                $('#plot-suppress-next-btn-id').addClass('hidden');
                $('#plot-unsuppress-next-btn-id').removeClass('hidden');
            }
            $('.plot-suppress-back-btn-id').addClass('hidden');
            $('.plot-suppress-btn-id').addClass('hidden');
            $('.plot-undo-suppress-btn-id').addClass('hidden');

        });

        // suppress action
        $('.plot-suppress-btn-id').on('click', function (e) {

            e.preventDefault();
            plotIdString = plotRows.join("|");

            $('.plot-loading-div').html('<div class="progress"><div class="indeterminate"></div></div>');

            if (parseInt($('#plot-suppress-total-count-id').html()) > $suppressRecordThreshold) {
                $('#plot-suppress-record-modal').modal('hide');
                krajeeDialog.confirm($("#plot-suppress-record-select2-id :selected").attr('data-label') + ' of ' + varLocationName + " will be suppressed. This will be sent to a background process.", function (result) {
                    if (result) {
                        $.ajax({
                            url: "suppress-record-background",
                            type: 'post',
                            data: {
                                entityDbId: '' + plotIdString + '',
                                variableDbId: $("#plot-suppress-record-select2-id :selected").attr('data-id'),
                                suppressRemarks: $('#plot-suppress-remarks-id').val(),
                                transactionId: varTransactionId,
                                suppress: "true",
                                dataLevel: 'plot'
                            },
                            error: function (jqXHR, exception) {

                                msg = logError(jqXHR, exception);

                                var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                                Materialize.toast(toastContent, 5000);

                            },
                            success: function (response) {

                                data = JSON.parse(response);

                                if (!data.success) {
                                    var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: '+ data.message;

                                    Materialize.toast(toastContent, 5000);

                                } else {

                                    window.location = browseTerminalUrl + "&TerminalTransaction[transactionDbId]=" + varTransactionId;

                                }
                            },
                            error: function (jqXHR, exception) {

                                $('.plot-loading-div').html('');
                                msg = logError(jqXHR, exception);

                                var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: '+ msg;

                                Materialize.toast(toastContent, 5000);

                            }
                        });
                    }
                });
            } else {
                $.ajax({
                    url: "suppress-record",
                    type: 'post',
                    data: {
                        condition: '' + plotIdString + '',
                        variableAbbrev: $("#plot-suppress-record-select2-id").val(),
                        remarks: $('#plot-suppress-remarks-id').val(),
                        transactionId: varTransactionId,
                        suppress: "true",
                        dataLevel: 'plot_data'
                    },
                    success: function (data) {
                        plotRows = [];
                        Materialize.toast('Successfully suppressed data!', 5000, 'green');

                        $.pjax.reload({
                            container: '#$dynaGridId-pjax',

                        });
                        $.pjax.xhr = null;
                        $.pjax.reload({ container: '#data-collection-browser-pjax' });
                        $.pjax.xhr = null;
                        $.pjax.reload({ container: '#complete-browser-pjax' });
                        $.pjax.xhr = null;
                        $('#plot-suppress-record-modal').modal('hide');

                        $('#plot-suppress-remarks-id').val('');
                        $('.plot-suppress-back-btn-id').trigger('click');

                        $('.plot-undo-suppress-btn-id').addClass('hidden');
                        $('.plot-suppress-btn-id').addClass('hidden');

                        $('#plot-suppress-next-btn-id').addClass('disabled');
                        $('#plot-unsuppress-next-btn-id').addClass('disabled');

                        $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
                        $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

                        $('.plot-loading-div').html('');

                    },
                    error: function (jqXHR, exception) {

                        $('.plot-loading-div').html('');
                        msg = logError(jqXHR, exception);
                        var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: '+ msg;

                        Materialize.toast(toastContent, 5000);
                    }
                });
            }
        });

        // undo suppress action

        $('.plot-undo-suppress-btn-id').on('click', function (e) {
            e.preventDefault();

            $('.plot-loading-div').html('<div class="progress"><div class="indeterminate"></div></div>');

            if (parseInt($('#plot-suppress-total-count-id').html()) > $suppressRecordThreshold) {
                $('#plot-suppress-record-modal').modal('hide');
                krajeeDialog.confirm($("#plot-suppress-record-select2-id :selected").attr('data-label') + ' of ' + varLocationName + " will be unsuppressed. This will be sent to a background process.", function (result) {
                    if (result) {
                        $.ajax({
                            url: "suppress-record-background",
                            type: 'post',
                            data: {
                                entityDbId: '' + plotIdString + '',
                                variableDbId: $("#plot-suppress-record-select2-id :selected").attr('data-id'),
                                suppressRemarks: $('#plot-suppress-remarks-id').val(),
                                transactionId: varTransactionId,
                                suppress: "false",
                                dataLevel: 'plot'
                            },
                            error: function (jqXHR, exception) {
                                
                                msg = logError(jqXHR, exception);

                                var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                                Materialize.toast(toastContent, 5000);

                            },
                            success: function (response) {

                                data = JSON.parse(response);
                                if (!data.success) {
                                    var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error';

                                    Materialize.toast(toastContent, 5000);
                                } else {
                                    if (data.message === "in queue") {

                                        var toastContent = varLocationName + " is now in queue for undo suppression.";
                                    } else {

                                        var toastContent = varLocationName + " is now being unsuppressed.";
                                    }

                                }
                                localStorage.setItem('commit_response', toastContent);

                                window.location = browseTerminalUrl + "&TerminalTransaction[transactionDbId]=" + varTransactionId;
                            },
                            error: function (jqXHR, exception) {

                                $('.plot-loading-div').html('');

                                msg = logError(jqXHR, exception);

                                var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                                Materialize.toast(toastContent, 5000);
                            }
                        });
                    }
                });
            } else {
                $.ajax({
                    url: "suppress-record",
                    dataType: 'html',
                    type: 'post',
                    data: {
                        condition: '' + plotIdString + '',
                        variableAbbrev: $("#plot-suppress-record-select2-id").val(),
                        remarks: $('#plot-suppress-remarks-id').val(),
                        transactionId: varTransactionId,
                        suppress: "false",
                        dataLevel: 'plot'
                    },
                    success: function (data) {
                        plotRows = [];
                        $('#plot-suppress-record-modal').modal('hide');
                        Materialize.toast('Undo suppression successful!', 5000, 'green');

                        $.pjax.reload({
                            container: '#$dynaGridId-pjax',

                        });
                        $.pjax.xhr = null;
                        $.pjax.reload({ container: '#data-collection-browser-pjax' });
                        $.pjax.xhr = null;
                        $.pjax.reload({ container: '#complete-browser-pjax' });
                        $.pjax.xhr = null;

                        $('#plot-suppress-remarks-id').val('');

                        $('.plot-suppress-back-btn-id').trigger('click');

                        $('#plot-suppress-next-btn-id').addClass('disabled');
                        $('#plot-unsuppress-next-btn-id').addClass('disabled');

                        $('.plot-undo-suppress-btn-id').addClass("hidden");
                        $('.plot-loading-div').html('');

                        $('.plot-undo-suppress-btn-id').addClass('hidden');
                        $('.plot-suppress-btn-id').addClass('hidden');

                        $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
                        $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

                    },
                    error: function (jqXHR, exception) {
                        $('.plot-loading-div').html('');

                        msg = logError(jqXHR, exception);

                        var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                        Materialize.toast(toastContent, 5000);

                    }
                });

            }

        });

        // if variable is selected, render all its conditions
        $("#plot-suppress-rule-select2-id").on('change', function (e) {

            $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');

            $.ajax({
                url: "get-suppression-rules",
                dataType: 'html',
                type: 'post',
                data: {
                    variableAbbrev: $(this).val(),
                    variableId: $('option:selected', this).attr('data-id'),
                    transactionId: varTransactionId
                },
                success: function (data) {

                    $("#condition-id").html(data);
                    $('.loading-div').html('');
                    var suppress = $("#suppress-action-id").val()
                    //show/hide suppress and unsuppress button
                    if ($("#filter-form-grid-id tbody > tr:last").text() == "No rules found") {
                        if(suppress == 'true'){
                            $(".add-rule-btn-id").removeClass("hidden");
                            $('#unsuppress-next-btn-id').addClass('hidden');
                            $('#suppress-next-btn-id').removeClass('hidden');
                            $('#suppress-next-btn-id').attr("disabled", false)

                            $('#suppress-modal-header-label').removeClass('hidden');
                            $('#unsuppress-modal-header-label').addClass('hidden');
                        }else{
                            $(".add-rule-btn-id").addClass("hidden");
                            $('#suppress-next-btn-id').addClass('hidden');
                            $('#unsuppress-next-btn-id').removeClass('hidden');
                            $('#unsuppress-next-btn-id').attr("disabled", true)

                            $('#unsuppress-modal-header-label').removeClass('hidden');
                            $('#suppress-modal-header-label').addClass('hidden');
                        }
                    } else {
                        
                        if(suppress == 'true'){
                            $(".add-rule-btn-id").removeClass("hidden");
                            $('#unsuppress-next-btn-id').addClass('hidden');
                            $('#suppress-next-btn-id').removeClass('hidden');
                            $('#suppress-next-btn-id').attr("disabled", false)

                            $('#suppress-modal-header-label').removeClass('hidden');
                            $('#unsuppress-modal-header-label').addClass('hidden');
                        }else{
                            $(".add-rule-btn-id").addClass("hidden");
                            $('#suppress-next-btn-id').addClass('hidden');
                            $('#unsuppress-next-btn-id').removeClass('hidden');
                            $('#unsuppress-next-btn-id').attr("disabled", false)

                            $('#unsuppress-modal-header-label').removeClass('hidden');
                            $('#suppress-modal-header-label').addClass('hidden');
                        }
                        
                    }

                },
                error: function (jqXHR, exception) {

                    msg = logError(jqXHR, exception);

                    var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                    Materialize.toast(toastContent, 5000);
                }
            });
            
            if ($("#supress-tab-1-id").hasClass("disabled") == false) {
                $("#supress-tab-1-id").addClass("disabled");
            }
        });

        var var_delete = false;
        var var_form_error = false;

        // Triggers the hiiden 2nd tab in the modal which is the summary
        $('#suppress-next-btn-id').on('click', function (e) {
            e.preventDefault();

            $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');

            $('#filter-error-summary-id').empty();
            $('#filter-error-summary-id').removeClass('error-summary');

            // save rule in db
            $('#filter-form-id').append('<input type="hidden" name="suppress" value="true" ></input>');
            $('#filter-form-id').append('<input type="hidden" name="dataLevel" value=' + $("#plot-suppress-rule-select2-id option:selected").attr('data-data_level') + ' ></input>');

            $.ajax({
                url: "get-suppression-rules-summary",
                type: 'post',
                data: $('#filter-form-id').serialize(),
                success: function (data) {
                    $('.loading-div').html('');
                    var data = $.parseJSON(data);

                    if (typeof data == 'object' && data.success == false) { //has error do not display summary of suppress action

                        var toastContent = '<i class="material-icons orange-text">warning</i></button>' + data.message;

                        Materialize.toast(toastContent, 5000);
                        var_form_error = true;
                        var suppress = $("#suppress-action-id").val()
                        if(suppress == 'true'){
                            $(".add-rule-btn-id").removeClass("hidden");
                            $('#unsuppress-next-btn-id').addClass('hidden');
                            $('#suppress-next-btn-id').removeClass('hidden');
                            $('#suppress-next-btn-id').attr("disabled", false)
                        }else{
                            $(".add-rule-btn-id").addClass("hidden");
                            $('#suppress-next-btn-id').addClass('hidden');
                            $('#unsuppress-next-btn-id').removeClass('hidden');
                            $('#unsuppress-next-btn-id').attr("disabled", false)
                        }

                        $('.suppress-back-btn-id').addClass("hidden");   //  back button
                        $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

                    } else {

                        $('#suppress-summary-body-id').empty();
                        $('#supress-tab-1-id').removeClass('disabled');
                        $('#suppress-summary-body-id').html(data.message);
                        $('ul.suppress-tabs').tabs('select_tab', 'suppress-summary-tab-id');

                        $('#unsuppress-next-btn-id').addClass("hidden");    // unsuppress button
                        $('#suppress-next-btn-id').addClass("hidden");     // suppress button

                        if (parseInt($('#plot-suppress-total-count-id').html()) == 0) {

                            $('.suppress-back-btn-id').removeClass("hidden");   //  back button
                            $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

                        } else {

                            $('.suppress-back-btn-id').removeClass("hidden");   //  back button
                            $('.suppress-btn-id').removeClass('hidden'); // suppress confirm btn

                        }

                    }

                    $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
                    $('#filter-form-id input[name=dataLevel]').remove();
                    $('#filter-form-id input[name=is_suppress]').remove();
                    $('.loading-div').html('');
                },
                error: function (jqXHR, exception) {
                    $('.loading-div').html('');
                    msg = logError(jqXHR, exception);

                    var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                    Materialize.toast(toastContent, 5000);
                }

            });
        });

        // undo suppress action
        $('#unsuppress-next-btn-id').on('click', function (e) {
            e.preventDefault();

            $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');

            $('#suppress-summary-body-id').empty();
            $('#filter-form-id').append('<input type="hidden" name="dataLevel" value=' + $("#plot-suppress-rule-select2-id option:selected").attr('data-data_level') + ' ></input>');
            $('#filter-form-id').append('<input type="hidden" name="suppress" value="false" ></input>');
            $.ajax({
                url: "get-suppression-rules-summary",
                type: 'post',
                data: $('#filter-form-id').serialize(),
                success: function (data) {

                    var data = $.parseJSON(data);

                    if (typeof data == 'object' && data.success == false) { //has error do not display summary of suppress action

                        var toastContent = '<i class="material-icons orange-text">warning</i></button>' + data.message;

                        Materialize.toast(toastContent, 5000);
                        var_form_error = true;
                        var suppress = $("#suppress-action-id").val()

                        if(suppress == 'true'){
                            $(".add-rule-btn-id").removeClass("hidden");
                            $('#unsuppress-next-btn-id').addClass('hidden');
                            $('#suppress-next-btn-id').removeClass('hidden');
                            $('#suppress-next-btn-id').attr("disabled", false)
                        }else{
                            $(".add-rule-btn-id").addClass("hidden");
                            $('#suppress-next-btn-id').addClass('hidden');
                            $('#unsuppress-next-btn-id').removeClass('hidden');
                            $('#unsuppress-next-btn-id').attr("disabled", false)
                        }

                        $('.suppress-back-btn-id').addClass("hidden");   //  back button
                        $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

                    } else {
                        $('#supress-tab-1-id').removeClass('disabled');
                        $('#suppress-summary-body-id').html(data.message);
                        $('ul.suppress-tabs').tabs('select_tab', 'suppress-summary-tab-id');
                        
                        $('#unsuppress-next-btn-id').addClass("hidden");    // unsuppress button
                        $('#suppress-next-btn-id').addClass("hidden");     // suppress button
                        
                        if (parseInt($('#plot-suppress-total-count-id').html()) == 0) {

                            $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
                            $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

                        } else {                          
                            $('.undo-suppress-btn-id').removeClass("hidden");   // unsuppress confirm button
                            $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn
                        }

                        $('.suppress-back-btn-id').removeClass("hidden");   //  back button

                        $('.loading-div').html('');
                    }
                },
                error: function (jqXHR, exception) {
                    $('.loading-div').html('');
                    msg = logError(jqXHR, exception);

                    var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                    Materialize.toast(toastContent, 5000);
                }
            });

        });

        // Trigger click hidden first tab in modal
        $('.suppress-back-btn-id').on('click', function (e) {
            e.preventDefault();
            $('#plot-suppress-rule-select2-id').val($('#plot-suppress-rule-select2-id').val()).trigger('change');
            $('.suppress-back-btn-id').addClass("hidden");   //  back button
            var suppress = $("#suppress-action-id").val()
            if(suppress == 'true'){
                $(".add-rule-btn-id").removeClass("hidden");
                $('#unsuppress-next-btn-id').addClass('hidden');
                $('#suppress-next-btn-id').removeClass('hidden');
                $('#suppress-next-btn-id').attr("disabled", false)
            }else{
                $(".add-rule-btn-id").addClass("hidden");
                $('#suppress-next-btn-id').addClass('hidden');
                $('#unsuppress-next-btn-id').removeClass('hidden');
                $('#unsuppress-next-btn-id').attr("disabled", false)
            }

            $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
            $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

            $('ul.suppress-tabs').tabs('select_tab', 'condition-tab-id');

        });

        //suppress action
        $('.suppress-btn-id').on('click', function (e) {
            e.preventDefault();
            $('.loading-div').html('<div class="progress"><div class="indeterminate"></div></div>');
            var var_delete = false;

            if (parseInt($('#plot-suppress-total-count-id').html()) > $suppressByRuleThreshold) {
                $('#plot-suppress-rule-modal').modal('hide');
                krajeeDialog.confirm($('#plot-suppress-rule-select2-id').val() + ' of ' + varLocationName + " will be suppressed. This will be sent to a background process.", function (result) {
                    if (result) {
                        $.ajax({
                            url: "suppress-by-rule-background",
                            type: 'post',
                            data: {
                                variableDbId: $("#plot-suppress-rule-select2-id :selected").attr('data-id'),
                                transactionId: varTransactionId,
                                suppress: "true",
                                dataLevel: 'plot'
                            },
                            error: function (jqXHR, exception) {
                                
                                msg = logError(jqXHR, exception);

                                var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                                Materialize.toast(toastContent, 5000);

                            },
                            success: function (response) {

                                data = JSON.parse(response);

                                if (!data.success) {
                                    var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error';

                                    Materialize.toast(toastContent, 5000);
                                } else {

                                    window.location = browseTerminalUrl + "&TerminalTransaction[transactionDbId]=" + varTransactionId;

                                }

                            }
                        });
                    }
                });
            } else {
                $.ajax({
                    url: "suppress-by-rules",
                    type: 'post',
                    data: {
                        transactionDbId: varTransactionId,
                        suppress: 'true',
                        variableDbId: $("#plot-suppress-rule-select2-id option:selected").attr('data-id'),
                        dataLevel: $("#plot-suppress-rule-select2-id option:selected").attr('data-data_level'),
                    },
                    success: function (data) {
                        var_delete = true;

                        Materialize.toast('Successfully suppressed data!', 5000, 'green');
                        $('.suppress-back-btn-id').addClass("hidden");   //  back button
                        
                        var suppress = $("#suppress-action-id").val()
                        if(suppress == 'true'){
                            $(".add-rule-btn-id").removeClass("hidden");
                            $('#unsuppress-next-btn-id').addClass('hidden');
                            $('#suppress-next-btn-id').removeClass('hidden');
                            $('#suppress-next-btn-id').attr("disabled", false)
                        }else{
                            $(".add-rule-btn-id").addClass("hidden");
                            $('#suppress-next-btn-id').addClass('hidden');
                            $('#unsuppress-next-btn-id').removeClass('hidden');
                            $('#unsuppress-next-btn-id').attr("disabled", false)
                        }
                        $('.suppress-back-btn-id').trigger('click');

                        $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
                        $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn
                        $('ul.suppress-tabs').tabs('select_tab', 'condition-tab-id');

                        $('.loading-div').html('');

                        $.pjax.reload({ container: '#$dynaGridId-pjax', });
                        $.pjax.xhr = null;

                        $.pjax.reload({ container: '#data-collection-browser-pjax' });
                        $.pjax.xhr = null;
                        $.pjax.reload({ container: '#complete-browser-pjax' });
                        $.pjax.xhr = null;

                    },
                    error: function (jqXHR, exception) {

                        $('.loading-div').html('');
                        msg = logError(jqXHR, exception);

                        var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: '+msg;

                        Materialize.toast(toastContent, 5000);
                    }
                });
            }
        });

        // undo suppress action
        $('.undo-suppress-btn-id').on('click', function (e) {
            e.preventDefault();
            $('.loading-div').html('<div class="progress"><div class="indeterminate"></div></div>');
            var var_delete = false;

            if (parseInt($('#plot-suppress-total-count-id').html()) > $suppressByRuleThreshold) {
                $('#plot-suppress-rule-modal').modal('hide');
                krajeeDialog.confirm($('#plot-suppress-rule-select2-id').val() + ' of ' + varLocationName + " will be unsuppressed. This will be sent to a background process.", function (result) {
                    if (result) {
                        $.ajax({
                            url: "suppress-by-rule-background",

                            type: 'post',

                            data: {
                                variableDbId: $("#plot-suppress-rule-select2-id :selected").attr('data-id'),
                                transactionId: varTransactionId,
                                suppress: "false",
                                dataLevel: 'plot'
                            },
                            error: function (jqXHR, exception) {

                                msg = logError(jqXHR, exception);

                                var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                                Materialize.toast(toastContent, 5000);

                            },
                            success: function (response) {

                                data = JSON.parse(response);
                                if (!data.success) {

                                    var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error';

                                    Materialize.toast(toastContent, 5000);
                                }

                                window.location = browseTerminalUrl + "&TerminalTransaction[transactionDbId]=" + varTransactionId;
                            }
                        });
                    }
                });
            } else {
                var var_delete = true;

                $.ajax({
                    url: "suppress-by-rules",
                    type: 'post',
                    data: {
                        transactionDbId: varTransactionId,
                        suppress: 'false',
                        variableDbId: $("#plot-suppress-rule-select2-id option:selected").attr('data-id'),
                        dataLevel: $("#plot-suppress-rule-select2-id option:selected").attr('data-data_level'),
                    },
                    success: function (data) {
                        var_delete = true;
                        Materialize.toast('Undo suppression successful!', 5000, 'green');
                        $('.loading-div').html('');
                        $('.suppress-back-btn-id').addClass("hidden");   //  back button

                        var suppress = $("#suppress-action-id").val()
                        if(suppress == 'true'){
                            $(".add-rule-btn-id").removeClass("hidden");
                            $('#unsuppress-next-btn-id').addClass('hidden');
                            $('#suppress-next-btn-id').removeClass('hidden');
                            $('#suppress-next-btn-id').attr("disabled", false)
                        }else{
                            $(".add-rule-btn-id").addClass("hidden");
                            $('#suppress-next-btn-id').addClass('hidden');
                            $('#unsuppress-next-btn-id').removeClass('hidden');
                            $('#unsuppress-next-btn-id').attr("disabled", false)
                        }
                        $('.suppress-back-btn-id').trigger('click');
                        $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
                        $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn
                        $('ul.suppress-tabs').tabs('select_tab', 'condition-tab-id');

                        $.pjax.reload({ container: '#$dynaGridId-pjax', });
                        $.pjax.xhr = null;
                        $.pjax.reload({ container: '#data-collection-browser-pjax' });
                        $.pjax.xhr = null;
                        $.pjax.reload({ container: '#complete-browser-pjax' });
                        $.pjax.xhr = null;

                    },
                    error: function (jqXHR, exception) {
                        $('.loading-div').html('');
                        msg = logError(jqXHR, exception);

                        var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                        Materialize.toast(toastContent, 5000);

                    }
                });
                if (var_delete) {
                    $(this).closest('tr').remove();
                }
            }

        });

    }

    function logError(jqXHR, exception) {
    var msg = '';
    if (jqXHR.status === 0) {
        msg = 'Not connected. Verify Network.';
    } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
    } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
    } else if (exception === 'parsererror') {
        msg = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        msg = 'Time out error.';
    } else if (exception === 'abort') {
        msg = 'Ajax request aborted.';
    } else {
        msg = 'Uncaught Error.' + jqXHR.responseText;
    }
    // logging error, please do not remove
    console.log(jqXHR.responseText)
    return msg;
}
JS
);

?>
