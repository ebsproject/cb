<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use app\models\SuppressionRule;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\data\ArrayDataProvider;

/**
 * Renders complete transaction view
 */

Yii::$app->view->registerCss('
    .complete-transaction-table > thead > tr > th, 
    .complete-transaction-table > tbody > tr > th, 
    .table > tfoot > tr > th, 
    .complete-transaction-table > thead > tr > td, 
    .complete-transaction-table > tbody > tr > td, 
    .complete-transaction-table > tfoot > tr > td {
        padding: 8px;
    }
');
?>
<div>

    <?php

    $columns = [
        [
            'attribute' => 'variableDbId',
            'header' => 'Variable',
            'vAlign' => 'middle',
            'filter' => false,
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $transactionModel = \Yii::$container->get('app\modules\dataCollection\models\Transaction');
                $variable = $transactionModel->getVariable($param);
                $isComputed = '';
                if ($model['computed']) {
                    $isComputed = '&nbsp;<font class="badge grey">COMPUTED</font>';
                }
                return Html::a(
                    $variable["label"],
                    false,
                    [
                        'style' => 'cursor: pointer;',
                        'class' => 'view-variable tooltipped',
                        'data-toggle' => "modal",
                        "data-target" => "#complete-variable-modal",
                        'data-pjax' => 0,

                        'data-variable_id' => $model["variableDbId"],
                        'data-variable_name' => $variable["abbrev"],
                        'data-position' => "right",
                        'data-tooltip' => "Click to view variable information",
                    ]
                ) . $isComputed;
            },

        ],

        [
            'attribute' => 'dataUnit',
            'vAlign' => 'middle',
            'filter' => false,
            "hAlign" => "center"
        ],
        [
            'attribute' => 'count',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'No. of observations',
            "hAlign" => "center"
        ],
        [
            'attribute' => 'remarksCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'No. of remarks',
            "hAlign" => "center"
        ],
        [
            'attribute' => 'invalidCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "danger"],
            "contentOptions" => ["class" => "red lighten-5"],
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) {
                if (!empty($model['invalidCount'])) {
                    return $model["invalidCount"];
                } else {
                    return '0';
                }
            }
        ],
        [
            'attribute' => 'missingCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "blue lighten-4"],
            "contentOptions" => ["class" => "blue lighten-4"],
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) {
                if (!empty($model['missingCount'])) {
                    return $model["missingCount"];
                } else {
                    return '0';
                }
            }
        ],
        [
            'attribute' => 'voidedCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "grey lighten-5"],
            "contentOptions" => ["class" => "grey lighten-5"],
            "hAlign" => "center",
            'visible' => true,
            "format" => 'raw',
        ],
        [
            'attribute' => 'minimum',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Min',
            "headerOptions" => ["class" => "success"],
            "hAlign" => "center"
        ],
        [
            'attribute' => 'maximum',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Max',
            "headerOptions" => ["class" => "success"],
            "hAlign" => "center"
        ],
        [
            'attribute' => 'average',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Avg',
            "headerOptions" => ["class" => "success"],
            "hAlign" => "center"
        ],
        [
            'attribute' => 'variance',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Variance',
            "headerOptions" => ["class" => "success"],
            "hAlign" => "center"
        ],
        [
            'attribute' => 'newCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "success"],
            "contentOptions" => ["class" => "green lighten-5"],
            "hAlign" => "center"
        ],
        [
            'attribute' => 'updatedCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "warning"],
            "contentOptions" => ["class" => "amber lighten-5"],
            "format" => 'raw',
            "hAlign" => "center"
        ],

        [
            'filter' => false,
            'vAlign' => 'middle',
            'header' => '<span class="col s6">Rule</span><span class="col s6">Remarks</span>',
            'format' => 'html',
            "headerOptions" => ["class" => "brown lighten-5"],
            "value" => function ($model) use ($transactionId) {
                $suppressedRecords = null;
                $params = [
                    "transactionDbId" => "equals $transactionId",
                    "targetVariableDbId" => "equals" . $model["variableDbId"],
                    "status" => "equals all|equals selected|equals complete"
                ];
                $suppressionModel = new SuppressionRule();
                $response = $suppressionModel->searchAll($params, '');
                if ($response['totalCount'] > 0) {
                    $provider = new ArrayDataProvider([
                        'allModels' => $response["data"],
                        'pagination' => false
                    ]);
                    $suppressedRecords .= \kartik\grid\GridView::widget([
                        'dataProvider' => $provider,
                        'layout' => '{items}',
                        'bordered' => false,
                        'hover' => true,
                        'striped' => false,
                        'showHeader' => false,
                        'showOnEmpty' => false,
                        'emptyText' => '<span class="not-set">(not set)</span>',
                        'columns' => [
                            [
                                'attribute' => 'rule',
                                'format' => 'raw',
                                'label' => false,
                                "options" => ["style" => "width:50%"],
                                "hAlign" => "center",
                                "value" => function ($model) {
                                    $value = null;
                                    if ($model["status"] == "complete") {
                                        $value = $model["factorVariableLabel"] . " " . $model["operator"] . " " . $model["value"];
                                    } else if ($model["status"] == "all") {
                                        $value = "All " . $model["factorVariableLabel"];
                                    }
                                    if ($model["status"] == "selected") {
                                        $value = "Select " . $model["factorVariableLabel"];
                                    }
                                    return $value;
                                }
                            ],
                            [
                                'attribute' => 'remarks',
                                'format' => 'raw',
                                'label' => false,
                                "options" => ["style" => "width:50%"],
                                "hAlign" => "center"
                            ]
                        ],
                        'tableOptions' => ["style" => "margin-bottom:0px"]
                    ]);
                }
                return $suppressedRecords;
            },
            "options" => ["style" => "width:30%"],
            "hAlign" => "center",

        ],

        [
            'attribute' => 'suppressedCount',
            'filter' => false,
            'vAlign' => 'middle',
            'header' => 'Count',
            "headerOptions" => ["class" => "brown lighten-5"],
            "contentOptions" => ["class" => "brown lighten-5"],

            "hAlign" => "center",
            'pageSummary' => 'Total <i class="tiny material-icons tooltipped grey-text" 
            id="bg-help-commit" data-position="left" data-delay="30" data-html=true '
                . "data-tooltip='"
                . \Yii::t('app', 'Total no. of records greater than ' . $commitThreshold . ' will be processed 
                in the background. <br>Processing in the background lets you do other tasks<br> 
                while waiting to get it done.')
                . "'>help</i>",
            'pageSummaryOptions' => ['class' => 'text-right '],

        ],
        [
            'attribute' => 'commitCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "info"],
            "contentOptions" => ["class" => "info"],
            "hAlign" => "center",
            'visible' => true,
            "format" => 'raw',
            'pageSummary' => true,
            'pageSummaryOptions' => ['class' => 'text-right ', 'id' => 'total_commit_count-id'],

        ],

    ];

    $renderCommit = !Yii::$app->access->renderAccess("COMMIT_TRANSACTION", "QUALITY_CONTROL", $creatorDbId)?'':Html::a(
        'Complete',
        '#',
        [
            'id' => 'commit-btn-id',
            'class' => 'pull-right btn btn-default waves-effect waves-light',
            'style' => 'margin-right:5px;', 'data-toggle' => "modal",
            "data-target" => "#confirm-commit"
        ]
    );

    $renderDownload = !Yii::$app->access->renderAccess("DOWNLOAD_TRANSACTION_FILE", "QUALITY_CONTROL", $creatorDbId)?'':Html::a(
        'Download Dataset',
        '#',
        [
            'data-pjax' => true,
            'id' => 'export-plots-btn',
            'style' => 'margin-right:5px;',
            'title' => \Yii::t('app', 'Download dataset'),
            'class' => 'black-text pull-right btn waves-effect waves-light grey lighten-2'
        ]
    );
    echo GridView::widget([
        'columns' => $columns,
        'options' => [
            'id' => 'complete-browser'
        ],
        'striped' => false,
        'export' => false,
        'hover' => true,
        'bordered' => false,
        'showPageSummary' => true,
        'pageSummaryRowOptions' => [
            'class' => 'kv-page-summary '
        ],
        'pjax' => true,
        'pjaxSettings' =>
        [
            'neverTimeout' => true,

            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'tableOptions' => [
            "class" => 'complete-transaction-table',
        ],
        'id' => 'grid',
        'dataProvider' => $datasetSummaryDataProvider,
        "toolbar" => false,
        "beforeHeader" => '
            <tr>
                <td data-complete-col-seq="0"></td>
                <td data-complete-col-seq="1"></td>
                <td data-complete-col-seq="2"></td>
                <td data-complete-col-seq="3"></td>
                <td data-complete-col-seq="4" class="red white-text kv-align-center"><b>' . \Yii::t('app', 'INVALID') . '</b></td>
                <td data-complete-col-seq="4" class="blue darken-2 white-text kv-align-center"><b>' . \Yii::t('app', 'MISSING') . '</b></td>
                <td data-complete-col-seq="5" class="grey white-text kv-align-center"><b>' . \Yii::t('app', 'VOIDED') . '</b></td>
                <td data-complete-col-seq="6" colspan="5" class="green white-text kv-align-center"><b>' . \Yii::t('app', 'NEW') . '</b></td>
                <td data-complete-col-seq="7" class="amber white-text kv-align-center"><b>' . \Yii::t('app', 'UPDATED') . '</b></td>
                <td data-complete-col-seq="8" colspan="2" class="brown lighten-2 white-text kv-align-center"><b>' . \Yii::t('app', 'SUPPRESSED') . '</b></td>
                <td class="blue white-text kv-align-center"><b>' . \Yii::t('app', 'COMMIT') . '</b></td>
            </tr>',

        'panel' => [
            'heading' => '<h5 class="panel-title">Summary of Transaction</h5>',
            'footer' => $renderCommit . $renderDownload,
            'before' => '<p style="padding:0 15px;">' . \Yii::t('app', 'The following will be committed to be operational.') . '</p>',
        ],
    ]);

    ?>

</div>
<!-- View Variable Information Modal-->
<div class="fade modal in" id="complete-variable-modal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <span id="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 id="complete-variable-header-id"></h4>
            </div>
            <div class="modal-body complete-variable-modal-content">

            </div>
            <div class="modal-footer">
                <a class="btn btn-flat waves-effect " data-dismiss="modal"><?php echo \Yii::t('app', 'Close'); ?></a>&emsp;

            </div>
        </div>
    </div>
</div>

<!-- Confirm Commit Modal-->
<div class="modal fade in" id="confirm-commit" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <span id="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title commit-title"><?php echo $locationName; ?></h4>
            </div>
            <div class="modal-body">
                <p id="commit-bg-body" class="hidden">This transaction will be committed in the experiment. This will be sent to a background process.
                    <a style="cursor: pointer;" id="bg-help" class="btn-link tooltipped" data-position="right" data-delay="50" data-tooltip="<p class=' white-text' style='font-size:smaller;'>Processing in the background lets you do other tasks<br> while waiting to get it done.</p>"><em class="tiny material-icons grey-text">help</em>
                    </a>
                </p>

                <p id="commit-body" class="hidden"><?php echo \Yii::t('app', 'This transaction will be committed in the experiment.'); ?></p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo \Yii::t('app', 'Cancel'); ?></button>
                <button type="button" class="btn btn-warning btn-ok amber" style="background-color: #f0ad4e !important;"><?php echo \Yii::t('app', 'Confirm'); ?></button>
            </div>
        </div>
    </div>
</div>

<?php
$viewVariable = Url::toRoute(['/variable/default/view-info']);
$exportDatasetUrl = Url::to(['/dataCollection/transaction/download-committed-dataset']);
$prepareDataForCsvDownloadUrl = Url::to(['/dataCollection/transaction/prepare-data-for-csv-download' . "?transactionId=$transactionId"]);
$this->registerJs(
    <<<JS
    const prepareDataForCsvDownloadUrl = '$prepareDataForCsvDownloadUrl';
    // Disable commit button if commit count = 0
    if(parseInt($('#total_commit_count-id').html())==0){
        $('#commit-btn-id').addClass('disabled');
    }else{
        $('#commit-btn-id').removeClass('disabled');
    }
    $(document).on('pjax:complete', function(e) {
        e.preventDefault();

        if(parseInt($('#total_commit_count-id').html())==0){
            $('#commit-btn-id').addClass('disabled');
        }else{
            $('#commit-btn-id').removeClass('disabled');
        }
        
    });
    // Show loading indicator on downloading dataset
    $('#export-plots-btn').off('click').on('click', function (e) {
        e.preventDefault()

        if ($count >= $downloadDatasetThreshold) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: prepareDataForCsvDownloadUrl,
                data: {id:varTransactionId},
                success: function(response) {}
            })
        } else {
            let exportDatasetUrl = '$exportDatasetUrl'
            let expDatasetUrl = window.location.origin + exportDatasetUrl + '?id=' + $transactionId

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
            $('.toast').css('display','none')
            Materialize.toast(message, 5000)

            window.location.replace(expDatasetUrl)

            $('#system-loading-indicator').html('')
        }
    })
    // Show modal for variable information
    $('#complete-variable-modal').on('show.bs.modal', function(e) {

        var obj = $(e.relatedTarget)
        var variableName = obj.data('variable_name');
        var variableId = obj.data('variable_id');

        $('#complete-variable-header-id').html('<i class="material-icons"></i> ' + variableName);

        $(".complete-variable-modal-content").html('');
        $('#loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');

        setTimeout(function(){
            $.ajax({
                url: '$viewVariable',
                data: {
                    id: variableId
                },
                type: 'POST',
                async: true,
                success: function(data){
                    $('.complete-variable-modal-content').html(data);
                    $('#loading-div').html('');
                },
                error: function(){
                    $('.complete-variable-modal-content').html('<i>There was a problem while loading record information.</i>');
                }
            });
        },300);  
    });

    $('#confirm-commit').on('show.bs.modal',function(e){
        $(".btn-ok").removeAttr("disabled");
        if(parseInt($('#total_commit_count-id').html())>$commitThreshold){
            
            $('#commit-bg-body').removeClass('hidden');
            $('#commit-body').addClass('hidden');
            $('#bg-help').tooltip({
                delay: 50,
                html: true,
            }).each(function () {
                $("#" + $(this).data('tooltip-id')).find(".backdrop").addClass('custom-backdrop');
            });
        }else{
            $('#commit-body').removeClass('hidden');
            $('#commit-bg-body').addClass('hidden');
        }

    });
    //Triggers the commit action
    
    $(document).on('click', '.btn-ok', function(e) {
        e.preventDefault();
        $(this).attr("disabled", "disabled");
        if(parseInt($('#total_commit_count-id').html())>$commitThreshold){
            
            $.ajax({
                url: 'commit-background',
                type: 'get',
                data:{id:varTransactionId},
                dataType: "json",
                success: function(data) {

                    if(!data.success){ 

                        var toastContent = $('<span>'+'There was an internal error with your request. <button class="btn-flat black-text toast-close-btn" style="background-color:transparent !important;"><i class="material-icons red toast-close-btn">close</i></button>'+
                            '<ul class="collapsible toast-error grey lighten-2">'+
                            '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>'+
                            '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + data.error+' </blockquote></span></div>'+
                            '</li> </ul>');

                        Materialize.toast(toastContent,'x','red');
                        $('.toast-error').collapsible();
                    }else{
                        window.location=browseTerminalUrl+"&TerminalTransaction[transactionDbId]="+varTransactionId;

                    }

                },
                error: function(xhr, status, error) {
                    $('#loading-div').html("");
                    var toastContent = $('<span>'+'There was an internal error with your request. <button class="btn-flat toast-action black-text toast-close-btn" style="background-color:transparent !important;"><i class="material-icons red toast-close-btn">close</i></button>'+
                        '<ul class="collapsible toast-error grey lighten-2">'+
                        '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>'+
                        '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText+' </blockquote></span></div>'+
                        '</li> </ul>');

                    Materialize.toast(toastContent,'x','red');
                    $('.toast-error').collapsible();
                }
            });
            
        }else{
            
            $('#loading-div').html('<div class="progress"><div class="indeterminate"></div></div>');
            $.ajax({
                url: 'commit',
                type: 'get',
                data:{id:varTransactionId},
                dataType: "json", 
                success: function(response) {
                    if(response.success){
                        $('#loading-div').html("");
                        
                        window.location=browseTerminalUrl+"&TerminalTransaction[status]=committed&TerminalTransaction[transactionDbId]="+varTransactionId;
                    }else{
                        $('#confirm-commit').modal('hide');
                        $('#loading-div').html("");
                    
                        var toastContent = $('<span>'+'There was an internal error with your request. <button class="btn-flat toast-action black-text" style="background-color:transparent !important;"><i class="material-icons red">close</i></button>'+
                            '<ul class="collapsible toast-error grey lighten-2">'+
                            '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>'+
                            '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + response.message+' </blockquote></span></div>'+
                            '</li> </ul>');

                        Materialize.toast(toastContent,'x','red');
                        $('.toast-error').collapsible();
                    }

                },
                error: function(xhr, status, error) {
                    $('#confirm-commit').modal('hide');
                    $('#loading-div').html("");
                    
                    var toastContent = $('<span>'+'There was an internal error with your request. <button class="btn-flat toast-action black-text" style="background-color:transparent !important;"><i class="material-icons red">close</i></button>'+
                        '<ul class="collapsible toast-error grey lighten-2">'+
                        '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>'+
                        '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText+' </blockquote></span></div>'+
                        '</li> </ul>');

                    Materialize.toast(toastContent,'x','red');
                    $('.toast-error').collapsible();
                }
            }); 
        }
    });
JS
); ?>