<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<?php

use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Renders Quality Control Validation Page
 */

// Url
$redirectToLocationUrl = Url::to(['/dataCollection/terminal', "program" => $program, 'TerminalTransaction[locationName]' => $locationName]);
$resultVariableDbIds = [];
foreach ($dataProvider->allModels as $model) {
    if (isset($model['resultVariableDbId'])) {
        $resultVariableDbIds[] = $model['resultVariableDbId'];
    }
}

?>
<!-- Quality Control -->

<div class="row">

    <h3 class=""><a href="<?php echo Url::toRoute(['/dataCollection/terminal', "program" => $program]) ?>"><?php echo \Yii::t('app', 'Quality Control'); ?></a><small> &raquo; <?php echo  Html::a($locationName, $redirectToLocationUrl) ?> </small>
    </h3>
    <div class="col col-md-12 row">
        <ul class="tabs">
            <li class="tab col s4 hidden" id="compute-tab">
                <a class="fixed-color" href="#compute-tab-id" >
                    <?php echo '<span class="glyphicon glyphicon-equalizer"></span>&nbsp;' . \Yii::t('app', 'Compute'); ?>
                </a>
            </li>
            <li class="tab col s4" id="suppress-tab">
                <a class="fixed-color" href="#suppress-tab-id" >
                    <?php echo '<span class="glyphicon glyphicon-list-alt"></span>&nbsp;' . \Yii::t('app', 'Review'); ?>
                </a>
            </li>
            <li class="tab col s4" id="complete-tab">
                <a class="fixed-color" href="#complete-tab-id" >
                    <span class="glyphicon glyphicon-ok"></span>&nbsp;<?php echo \Yii::t('app', 'Complete Transaction'); ?>
                </a>
            </li>
        </ul>
    </div>

    <!-- Compute Tab -->
    <div id="compute-tab-id" class="col s12 hidden">
        <?php
            echo \Yii::$app->controller->renderPartial('compute', [
                'formulaModel' => $formulaModel,
                'dataProvider' => $dataProvider,
                'program' => $program,
                'transactionId' => $transactionId,
                'selectedFormula' => $selectedFormula,
                'resultVariables' => $resultVariables,
                'status' => $status,
                'variables' => json_encode($transactionVariable),
                'formulaDbIds' => json_encode($transactionFormula),
            ]);
        ?>
    </div>

    <!-- Review Tab -->
    <div id="suppress-tab-id" class="col s12">
        <div class="" id="data-browser-container-id">
            <?php
            echo \Yii::$app->controller->renderPartial('dataset', [
                'variables' => $variables,
                'dataUnitVariables' => $dataUnitVariables,
                'transactionId' => $transactionId,
                'locationName' => $locationName,
                'locationId' => $locationId,
                'datasetSummaryDataProvider' => $datasetSummaryDataProvider,
                'suppressRecordThreshold' => $suppressRecordThreshold,
                'suppressByRuleThreshold' => $suppressByRuleThreshold,
                'program' => $program,
                'occurrenceData' => $occurrenceData,
            ]);
            ?>
        </div>
    </div>
    <!-- Complete Transaction Tab -->
    <div id="complete-tab-id" class="col s12 ">
        <?php
        echo \Yii::$app->controller->renderPartial('complete', [
            'transactionId' => $transactionId,
            'locationName' => $locationName,
            'locationId' => $locationId,
            'datasetSummaryDataProvider' => $datasetSummaryDataProvider,
            'commitThreshold' => $commitThreshold,
            'downloadDatasetThreshold' => $downloadDatasetThreshold,
            'count' => $count,
            'creatorDbId' => $creatorDbId
        ]);
        ?>
    </div>
</div>

<?php
$program = \Yii::$app->userprogram->get('abbrev');
Yii::$app->view->registerJs(
    "
    varLocationName='" . $locationName . "', 
    varlocationId='" . $locationId . "', 
    varTransactionId =$transactionId, 
    varResetGridUrl= '" . Url::toRoute(['/dataCollection/transaction/' . $transactionId, "program" => $program]) . "', 
    browseTerminalUrl= '" . Url::toRoute(['/dataCollection/terminal/index', "program" => $program]) . "';",
    \yii\web\View::POS_HEAD
);
?>

<?php
$this->registerJs(
    <<<JS
    function resetDataBrowser(){
        $('#reset-grid-id').on('click', function(e) {
            e.preventDefault();
            $.pjax.reload({
                container: '#suppress-browser-pjax',
                replace: true,
                url: varResetGridUrl
            });
        });
    }
    
    $(document).on('ready pjax:success', function(e) {
        e.preventDefault();
        resetDataBrowser();
    });
    
    resetDataBrowser();
JS
);

    if (isset($transactionFormula) && $transactionFormula !== []) {
        $this->registerJs(<<<JS
            // Hide the specified elements
            $(document).ready(function () {
                $("#compute-tab").removeClass("hidden");
                $("#compute-tab-id").removeClass("hidden");
            });
        JS
        );
    } else {
        // Trigger a click on the first tab when the condition is false
        $this->registerJs(<<<JS
            $(document).ready(function () {
                $("#suppress-tab a").click();
                $('#suppress-tab').removeClass('s4').addClass('s6');
                $('#complete-tab').removeClass('s4').addClass('s6');
            });
        JS
        );
    }
?>