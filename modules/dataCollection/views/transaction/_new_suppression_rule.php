<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

app\assets\AppAsset::register($this);

use yii\helpers\Html;
use kartik\select2\Select2;
?>

<?php
/**
 * Renders content of adding new condition in suppress form
 */

 $switchName = 'SuppressionRule[' . $rowNum . '][conjunction]';
?>
<div class="conjunct-field">
    <div class="switch" style='width:115px'>
        <label>
        OR
        <input class = "filter-conjunc" type="checkbox" value = "and" checked="true" name = "<?= $switchName?>">
        <span class="lever"></span>
        AND
        </label>
    </div>
</div>
<div id="var-field-id">
    <?php
    $terminalTransaction = \Yii::$container->get('\app\models\TerminalTransaction');
    $result = $terminalTransaction->getDatasetSummary($transactionId);
    $variables = $result['data'][0]['variables'] ?? [];
    $plotVarOptions = [];
    $plotVarData = [];
    foreach ($variables as $i => $v) {

        if ($v['dataUnit'] == 'plot') {
            $param = [
                "variableDbId" => "" . $v["variableDbId"]
            ];
            $transaction = \Yii::$container->get('\app\modules\dataCollection\models\Transaction');
            $variable = $transaction->getVariable($param);
            $plotVarOptions[$v["abbrev"]] = $variable["label"];

            $plotVarData[$v["abbrev"]] = [
                'data-label' => $variable["label"],
                'data-id' => $variable["variableDbId"],
                'data-data_type' => $variable["dataType"],
                'data-data_level' => $v["dataUnit"]
            ];
        }
    }
    echo kartik\widgets\Select2::widget([
        'name' => 'SuppressionRule[' . $rowNum . '][factorVariableAbbrev]',
        'initValueText' => $label,
        'value' => $abbrev,
        'data' => $plotVarOptions,
        'options' => [
            'multiple' => false,
            'placeholder' => '',
            "title" => $dataLevel,
            "class" => "filter-select-var",
            "id" => "filter-select-var-" . $rowNum,
            'options' => $plotVarData,
        ],

    ]);

    ?>
    <input type="hidden" name="<?php echo 'SuppressionRule[' . $rowNum . '][dataLevel]'; ?>" value="<php echo $dataLevel; ?>" class="filter-form-data-level">
</div>
<div id="op-field-id">
    <?php

    echo Select2::widget([
        'name' => 'SuppressionRule[' . $rowNum . '][operator]',
        'value' => '=',
        'data' => ["=" => "=", "<>" => "<>", "<" => "<", ">" => ">", "<=" => "<=", ">=" => ">="],
        'pluginOptions' => [
            'allowClear' => false,

        ],
        'options' => [
            'multiple' => false, 'placeholder' => "Select operator", "id" => "filter-select-operator-" . $rowNum,
            "class" => "filter-select-operator",
        ]
    ]);
    ?>
</div>
<div id="value-field-id">
    <?php

    use kartik\widgets\DatePicker;

    echo  DatePicker::widget([
        'name' => 'date-name',
        'options' => ['class' => 'suppress-datepicker hidden', 'placeholder' => ''],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => false,
            'autoclose' => true
        ]
    ]);

    if ($dataType === "integer") {
        echo  Html::input('number', '', "", ['class' => "form-control filter-form-value", "min" => 0]);
    } else if ($dataType === "float") {
        echo  Html::input('number', '', "", ['class' => "form-control filter-form-value", "step" => "any", "min" => 0]);
    } else {
        echo  Html::input('text', '', "", ['class' => "form-control filter-form-value"]);
    }

    ?>
    <?php
    echo Select2::widget([
        'name' => 'select2-name',
        'data' => ["=" => "=", "<>" => "<>", "<" => "<", ">" => ">", "<=" => "<=", ">=" => ">="],
        'pluginOptions' => [
            'allowClear' => false,
        ],
        'options' => [
            'multiple' => false, 'placeholder' => "Select value", "id" => "filter-select-value-" . $rowNum,
            "class" => "filter-select-value-select2", "disabled" => true
        ]
    ]);
    ?>
</div>

<div id="remarks-field-id">
    <?php echo Html::textArea('SuppressionRule[' . $rowNum . '][remarks]', "", ['id' => 'remarks-id', "class" => "form-control filter-form-remarks"]);
    ?>
</div>

<div id="status-field-id">

    <span class='badge new blue'><strong>new</strong></span>
</div>

<div id="action-field-id">
    <div class="skip-export kv-align-center kv-align-middle " style="width:80px;"><a href="" title="Remove" class="remove-filter-new" id="remove-filter-new-id-<?php echo $rowNum; ?>"><em class="material-icons">clear</em></a></div>
</div>

<?php

$js = <<<JS

$(".filter-conjunc").on('change', function () {
        if($(this).prop('checked')){
            $(this).val('and');
        }else{
            $(this).val('or');
        }
    });
JS;
$this->registerJs($js);
?>