<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\tabs\TabsX;
use yii\data\ArrayDataProvider;

use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;
?>

<?php
/**
 * Renders Dataset Tab
 */
Yii::$app->view->registerCss('
    .summary-browser > thead > tr > th, .summary-browser > tbody > tr > th, 
    .table > tfoot > tr > th, .summary-browser > thead > tr > td, 
    .summary-browser > tbody > tr > td, .summary-browser > tfoot > tr > td {
        padding: 8px;
    }

    .tab-content {
        background-color: white;
        padding: 0px;
    }

    .panel-default > .panel-heading {
        padding: 10px 15px; 
    }

    .panel > .panel-heading {
        background-color: white;
    }

    .kv-grid-wrapper { 
        height: calc(100vh - 365px);
    }
    
    .chips .chip.selected {
        background-color: #ffecb3;
    }

    .chip:focus {
        outline:none;
    }

    .plot-status-btn,.cross-status-btn{
        text-decoration: none; 
        color: #fff; 
        padding: 0px 9px; 
        height: 25px; 
        line-height: 25px; 
        text-transform: none;
    }

');
?>

<?php
// Get transaction Number
$transactionNo = \Yii::$container->get('app\models\TerminalTransaction')->getRecentFileId($transactionId);

// Set grid Id within the tabs
$plotGridId = 'plot-data-browser-'.$transactionNo;
$crossGridId = 'cross-data-browser-'.$transactionNo;

// URLs
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$plotGridId,$crossGridId]);

$dataLevels = [];
echo '<!-- Tabs -->';
foreach ($dataUnitVariables as $key => $dataLevel) {
    $dataLevels[] = ['unit' => $key];
}

$items[] = [
    'label' => 'Summary',
    'linkOptions' => ['id' => 'summary-tab-id',],
    'options' => ['id' => 'summary-tab-id']
];
foreach ($dataLevels as $unit) {
    $items[] = [
        'label' => '' . ucwords($unit['unit']),
        'linkOptions' => ['id' => $unit['unit'] . '-tab-id',],
        'options' => ['id' =>  $unit['unit'] . '-tab-id']
    ];
}

echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'fade' => true
]);

?>

<div class="" id="summary-container-id">
    <!-- Dataset Summary-->
    <?php

    $transactionModel = \Yii::$container->get('app\modules\dataCollection\models\Transaction');
    $columns = [
        [
            'attribute' => 'variableDbId',
            'header' => 'Variable',
            'vAlign' => 'middle',
            'filter' => false,
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) use ($transactionModel) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                $isComputed = '';
                if ($model['computed']) {
                    $isComputed = '&nbsp;<font class="badge grey">COMPUTED</font>';
                }
                return Html::a(
                    $variable["label"],
                    false,
                    [
                        'style' => 'cursor: pointer;',
                        'class' => 'view-variable tooltipped',
                        'data-toggle' => "modal",
                        "data-target" => "#summary-variable-modal",
                        'data-pjax' => 0,

                        'data-variable_id' => $model["variableDbId"],
                        'data-variable_name' => $variable["abbrev"],
                        'data-position' => "right",
                        'data-tooltip' => "Click to view variable information",
                    ]
                ) . $isComputed;
            },

        ],

        [
            'attribute' => 'dataUnit',
            'vAlign' => 'middle',
            'filter' => false,
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) {

                return Html::a(
                    $model["dataUnit"],
                    false,
                    [
                        'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                        'class' => 'btn-link data-level-id tooltipped ',
                        'data-dataUnit' => $model["dataUnit"],
                        'data-variable' => $model["abbrev"],
                        'data-variable_id' => $model["variableDbId"],
                        'data-position' => "right",
                        'data-tooltip' => "Click to view " . $model["dataUnit"] . " data browser"

                    ]
                );
            }
        ],
        [
            'attribute' => 'count',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'No. of observations',
            "hAlign" => "center"
        ],
        [
            'attribute' => 'remarksCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'No. of remarks',
            "headerOptions" => ["class" => ""],
            "contentOptions" => ["class" => ""],
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) use ($transactionId, $transactionModel) {
                $params = Yii::$app->request->queryParams;
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                unset($params[ucwords($model['dataUnit']) . 'Data']);
                $result = array();
                foreach ($params as $key => $entry) {

                    $result[$key] = $entry;
                }
                $result[ucwords($model['dataUnit']) . 'Data']['fields'] = $model['abbrev'];
                $result[ucwords($model['dataUnit']) . 'Data'][$model["abbrev"] . '_data']['status'] = 'remarks';

                if (!empty($model['remarksCount'])) {
                    return Html::a(
                        $model["remarksCount"],
                        false,
                        [
                            'data-dataUnit' => $model["dataUnit"],
                            'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                            'class' => 'btn-link  tooltipped filter-status-link',
                            'data-url' => Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $result)),
                            'data-position' => "right",
                            'data-tooltip' => "Click to view remarks of " . $variable["label"]

                        ]
                    );
                } else {
                    return '0';
                }
            }
        ],
        [
            'attribute' => 'invalidCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "danger"],
            "contentOptions" => ["class" => "red lighten-5"],
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) use ($transactionId, $transactionModel) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                $params = Yii::$app->request->queryParams;
                unset($params[ucwords($model['dataUnit']) . 'Data']);
                $result = array();
                foreach ($params as $key => $entry) {

                    $result[$key] = $entry;
                }
                $result[ucwords($model['dataUnit']) . 'Data']['fields'] = $model['abbrev'];
                $result[ucwords($model['dataUnit']) . 'Data'][$model["abbrev"] . '_data']['status'] = 'invalid%';

                if (!empty($model['invalidCount'])) {
                    return Html::a(
                        $model["invalidCount"],
                        false,
                        [
                            'data-dataUnit' => $model["dataUnit"],
                            'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                            'class' => 'btn-link  tooltipped filter-status-link',
                            'data-url' => Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $result)),
                            'data-position' => "right",
                            'data-tooltip' => "Click to view invalid " . $variable["label"]

                        ]
                    );
                } else {
                    return '0';
                }
            }
        ],
        [
            'attribute' => 'missingCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "blue lighten-4"],
            "contentOptions" => ["class" => "blue lighten-4"],
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) use ($transactionId, $transactionModel) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                $params = Yii::$app->request->queryParams;
                unset($params[ucwords($model['dataUnit']) . 'Data']);
                $result = array();
                foreach ($params as $key => $entry) {

                    $result[$key] = $entry;
                }
                $result[ucwords($model['dataUnit']) . 'Data']['fields'] = $model['abbrev'];
                $result[ucwords($model['dataUnit']) . 'Data'][$model["abbrev"] . '_data']['status'] = 'missing%';

                if (!empty($model['missingCount'])) {
                    return Html::a(
                        $model["missingCount"],
                        false,
                        [
                            'data-dataUnit' => $model["dataUnit"],
                            'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                            'class' => 'btn-link  tooltipped filter-status-link',
                            'data-url' => Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $result)),
                            'data-position' => "right",
                            'data-tooltip' => "Click to view missing " . $variable["label"]

                        ]
                    );
                } else {
                    return '0';
                }
            }
        ],
        [
            'attribute' => 'voidedCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "grey lighten-5"],
            "contentOptions" => ["class" => "grey lighten-5"],
            "hAlign" => "center",
            'visible' => true,
            "format" => 'raw',
            "value" => function ($model) use ($transactionId, $transactionModel) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                $params = Yii::$app->request->queryParams;
                unset($params[ucwords($model['dataUnit']) . 'Data']);
                $result = array();
                foreach ($params as $key => $entry) {

                    $result[$key] = $entry;
                }
                $result[ucwords($model['dataUnit']) . 'Data']['fields'] = $model['abbrev'];
                $result[ucwords($model['dataUnit']) . 'Data'][$model["abbrev"] . '_data']['isVoid'] = 'true';

                if (!empty($model['voidedCount'])) {
                    return Html::a(
                        $model["voidedCount"],
                        false,
                        [
                            'data-dataUnit' => $model["dataUnit"],
                            'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                            'class' => 'btn-link  tooltipped filter-status-link',
                            'data-url' => Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $result)),
                            'data-position' => "right",
                            'data-tooltip' => "Click to view voided " . $variable["label"]

                        ]
                    );
                } else {
                    return '0';
                }
            }
        ],
        [
            'attribute' => 'minimum',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Min',
            "headerOptions" => ["class" => "success"],
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) use ($transactionId, $transactionModel) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                $params = Yii::$app->request->queryParams;
                unset($params[ucwords($model['dataUnit']) . 'Data']);
                $result = array();
                foreach ($params as $key => $entry) {

                    $result[$key] = $entry;
                }
                $result[ucwords($model['dataUnit']) . 'Data']['fields'] = $model['abbrev'];
                $result[ucwords($model['dataUnit']) . 'Data'][$model["abbrev"]] = $model["minimum"];

                if (isset($model['minimum'])) {

                    return Html::a(
                        $model["minimum"],
                        false,
                        [
                            'data-dataUnit' => $model["dataUnit"],
                            'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                            'class' => 'btn-link  tooltipped filter-data-link',
                            'data-url' => Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $result)),
                            'data-position' => "right",
                            'data-tooltip' => "Click to view " . $variable["label"] . " record(s)"

                        ]
                    );
                }
            },
        ],
        [
            'attribute' => 'maximum',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Max',
            "headerOptions" => ["class" => "success"],
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) use ($transactionId, $transactionModel) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                $params = Yii::$app->request->queryParams;
                unset($params[ucwords($model['dataUnit']) . 'Data']);
                $result = array();

                foreach ($params as $key => $entry) {

                    $result[$key] = $entry;
                }
                $result[ucwords($model['dataUnit']) . 'Data']['fields'] = $model['abbrev'];
                $result[ucwords($model['dataUnit']) . 'Data'][$model["abbrev"]] = $model["maximum"];

                if (isset($model['maximum'])) {

                    return Html::a(
                        $model["maximum"],
                        false,
                        [
                            'data-dataUnit' => $model["dataUnit"],
                            'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                            'class' => 'btn-link  tooltipped filter-data-link',
                            'data-url' => Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $result)),
                            'data-position' => "right",
                            'data-tooltip' => "Click to view " . $variable["label"] . " record(s)"

                        ]
                    );
                }
            },
        ],
        [
            'attribute' => 'average',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Avg',
            "headerOptions" => ["class" => "success"],
            "hAlign" => "center"
        ],
        [
            'attribute' => 'variance',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Variance',
            "headerOptions" => ["class" => "success"],
            "hAlign" => "center"
        ],
        [
            'attribute' => 'newCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "success"],
            "contentOptions" => ["class" => "green lighten-5"],

            'subGroupOf' => 1,
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) use ($transactionId, $transactionModel) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                $params = Yii::$app->request->queryParams;
                unset($params[ucwords($model['dataUnit']) . 'Data']);
                $result = array();

                foreach ($params as $key => $entry) {

                    $result[$key] = $entry;
                }
                $result[ucwords($model['dataUnit']) . 'Data']['fields'] = $model['abbrev'];
                $result[ucwords($model['dataUnit']) . 'Data'][$model["abbrev"] . '_data']['status'] = 'new';
                if (!empty($model['newCount'])) {
                    return Html::a(
                        $model["newCount"],
                        false,
                        [
                            'data-dataUnit' => $model["dataUnit"],
                            'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                            'class' => 'btn-link  tooltipped filter-status-link',
                            'data-url' => Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $result)),
                            'data-position' => "right",
                            'data-tooltip' => "Click to view new " . $variable["label"]

                        ]
                    );
                } else {
                    return '0';
                }
            }
        ],
        [
            'attribute' => 'updatedCount',
            'vAlign' => 'middle',
            'filter' => false,
            'header' => 'Count',
            "headerOptions" => ["class" => "warning"],
            "contentOptions" => ["class" => "amber lighten-5"],
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) use ($transactionId, $transactionModel) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                $params = Yii::$app->request->queryParams;
                unset($params[ucwords($model['dataUnit']) . 'Data']);
                $result = array();

                foreach ($params as $key => $entry) {

                    $result[$key] = $entry;
                }
                $result[ucwords($model['dataUnit']) . 'Data']['fields'] = $model['abbrev'];
                $result[ucwords($model['dataUnit']) . 'Data'][$model["abbrev"] . '_data']['status'] = '%updated';

                if (!empty($model['updatedCount'])) {
                    return Html::a(
                        $model["updatedCount"],
                        false,
                        [
                            'data-dataUnit' => $model["dataUnit"],
                            'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                            'class' => 'btn-link  tooltipped filter-status-link',
                            'data-url' => Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $result)),
                            'data-position' => "right",
                            'data-tooltip' => "Click to view updated " . $variable["label"]

                        ]
                    );
                } else {
                    return '0';
                }
            }
        ],

        [

            'filter' => false,
            'vAlign' => 'middle',
            'header' => '<span class="col s6">Rule</span><span class="col s6">Remarks</span>',
            'format' => 'html',
            "headerOptions" => ["class" => "brown lighten-3 row"],
            "value" => function ($model) use ($transactionId) {
                $suppressedRecords = null;
                $params = [
                    "transactionDbId" => "equals $transactionId",
                    "targetVariableDbId" => "equals " . $model["variableDbId"],
                    "status" => "equals all|equals selected|equals complete"
                ];
                $response = \Yii::$container->get('\app\models\SuppressionRule')->searchALl($params, '');
                if ($response['totalCount'] > 0) {
                    $provider = new ArrayDataProvider([
                        'allModels' => $response["data"],
                        'pagination' => false
                    ]);
                    $suppressedRecords .= \kartik\grid\GridView::widget([
                        'dataProvider' => $provider,
                        'layout' => '{items}',
                        'bordered' => false,
                        'hover' => true,
                        'striped' => false,
                        'showHeader' => false,
                        'showOnEmpty' => false,
                        'emptyText' => '<span class="not-set">(not set)</span>',
                        'columns' => [
                            [
                                'attribute' => 'rule',
                                'format' => 'raw',
                                'label' => false,
                                "options" => ["style" => "width:50%"],
                                "hAlign" => "center",
                                "value" => function ($model) {
                                    $value = null;
                                    if ($model["status"] == "complete") {
                                        $value = $model["factorVariableLabel"] . " " . $model["operator"] . " " . $model["value"];
                                    } else if ($model["status"] == "all") {
                                        $value = "All " . $model["factorVariableLabel"];
                                    }
                                    if ($model["status"] == "selected") {
                                        $value = "Select " . $model["factorVariableLabel"];
                                    }
                                    return $value;
                                }
                            ],
                            [
                                'attribute' => 'remarks',
                                'format' => 'raw',
                                'label' => false,
                                "options" => ["style" => "width:50%"],
                                "hAlign" => "center"
                            ]
                        ],
                        'tableOptions' => ["style" => "margin-bottom:0px"]
                    ]);
                }
                return $suppressedRecords;
            },

            "options" => ["style" => "width:30%"],
            "hAlign" => "center"
        ],
        [
            'attribute' => 'suppressedCount',
            'filter' => false,
            'vAlign' => 'middle',
            'header' => 'Count',
            "headerOptions" => ["class" => "brown lighten-3"],
            "contentOptions" => ["class" => "brown lighten-5"],
            "hAlign" => "center",
            "format" => 'raw',
            "value" => function ($model) use ($transactionId, $transactionModel) {
                $param = [
                    "variableDbId" => "" . $model["variableDbId"]
                ];
                $variable = $transactionModel->getVariable($param);
                $params = Yii::$app->request->queryParams;
                unset($params[ucwords($model['dataUnit']) . 'Data']);
                $result = array();

                foreach ($params as $key => $entry) {

                    $result[$key] = $entry;
                }
                $result[ucwords($model['dataUnit']) . 'Data']['fields'] = $model['abbrev'];
                $result[ucwords($model['dataUnit']) . 'Data'][$model["abbrev"] . '_data']['isSuppressed'] = 'true';

                if (!empty($model['suppressedCount'])) {
                    return Html::a(
                        $model["suppressedCount"],
                        false,
                        [
                            'data-dataUnit' => $model["dataUnit"],
                            'style' => 'cursor: pointer;background-color:transparent; background:none!important; padding:0;',
                            'class' => 'btn-link  tooltipped filter-status-link',
                            'data-url' => Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $result)),
                            'data-position' => "right",
                            'data-tooltip' => "Click to view suppressed " . $variable["label"]

                        ]
                    );
                } else {
                    return '0';
                }
            }
        ],

    ];

    $beforeHeader = '
        <tr>
            <td data-summary-col-seq="0"></td>
            <td data-summary-col-seq="1"></td>
            <td data-summary-col-seq="2"></td>
            <td data-summary-col-seq="3"></td>
            <td data-summary-col-seq="4" class="red white-text kv-align-center"><b>' . \Yii::t('app', 'INVALID') . '</b></td>
            <td data-summary-col-seq="4" class="blue darken-2 white-text kv-align-center"><b>' . \Yii::t('app', 'MiSSING') . '</b></td>
            <td data-summary-col-seq="5" class="grey white-text kv-align-center"><b>' . \Yii::t('app', 'VOIDED') . '</b></td>
            <td data-summary-col-seq="6" colspan="5" class="green white-text kv-align-center"><b>' . \Yii::t('app', 'NEW') . '</b></td>
            <td data-summary-col-seq="7" class="amber white-text kv-align-center"><b>' . \Yii::t('app', 'UPDATED') . '</b></td>
            <td data-summary-col-seq="8" colspan="3" class="brown lighten-2 white-text kv-align-center"><b>' . \Yii::t('app', 'SUPPRESSED') . '</b></td>
        </tr>
    ';

    echo GridView::widget([

        'columns' => $columns,
        'options' => ['id' => 'data-collection-browser'],
        'export' => false,
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' =>
        [
            'neverTimeout' => true,
            'beforeGrid' => '',
            'afterGrid' => false
        ],
        'tableOptions' => ["class" => 'summary-browser',],

        'dataProvider' => $datasetSummaryDataProvider,
        "toolbar" => false,
        "beforeHeader" => $beforeHeader,
        'afterHeader' => '',
        'panel' => [
            'heading' => '<p style="padding:0 15px; font-size: 13px;">' . \Yii::t('app', 'This browser lists all uploaded and computed variables in this transaction.') . '</p>',
            'footer' => false,
            'before' => false,
            'after' => false
        ],

    ]);

    ?>

</div>

<!-- Plot browser -->

<div class="" id="plot-browser-container-id">
    <?php
    if (isset($dataUnitVariables["plot"])) {
        echo \Yii::$app->controller->renderPartial('tabs/plot_data', [
            'transactionId' => $transactionId,
            'locationName' => $locationName,
            'locationId' => $locationId,
            'variables' => $dataUnitVariables["plot"],
            'suppressRecordThreshold' => $suppressRecordThreshold,
            'suppressByRuleThreshold' => $suppressByRuleThreshold,
            'program' => $program,
            'occurrenceData' => $occurrenceData,
        ]);
    }
    ?>
</div>

<!-- Cross browser -->
<div class="" id="cross-browser-container-id">
    <?php
    if (isset($dataUnitVariables["cross"])) {
        echo \Yii::$app->controller->renderPartial('tabs/cross_data', [
            'transactionId' => $transactionId,
            'locationName' => $locationName,
            'locationId' => $locationId,
            'variables' => $dataUnitVariables["cross"],
            'suppressRecordThreshold' => $suppressRecordThreshold,
            'suppressByRuleThreshold' => $suppressByRuleThreshold,
            'program' => $program
        ]);
    }
    ?>
</div>

<!--  View Variable Information Modal-->
<div class="fade modal in" id="summary-variable-modal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <span id="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 id="summary-variable-header-id"></h4>
            </div>
            <div class="modal-body summary-variable-modal-content">

            </div>
            <div class="modal-footer">
                <a class="btn btn-flat waves-effect " data-dismiss="modal"><?php echo \Yii::t('app', 'Close'); ?></a>&emsp;

            </div>
        </div>
    </div>
</div>

<?php
$params = Yii::$app->request->queryParams;
$plotParams = [];
if (isset($params['PlotData'])) {
    $plotParams = $params['PlotData'];
    unset($params['PlotData']);
}
$crossParams = [];
if (isset($params['CrossData'])) {
    $crossParams = $params['CrossData'];
    unset($params['CrossData']);
}

$result = array();
foreach ($params as $key => $entry) {

    $result[$key] = $entry;
}
?>
<?php
$dynaGridId = \Yii::$container->get('app\models\TerminalTransaction')->getRecentFileId($transactionId);
$dynaGridId = '-data-browser-' . $dynaGridId;

$resetGridUrl = Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId, ["program" => $program]], $result));
$viewVariable = Url::toRoute(['/variable/default/view-info']);

// Page size change for 2 browser with tables in Quality Control
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ['".$plotGridId."','".$crossGridId."'],
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(
    <<<JS

    
    var selectAll = 'page';
    renderBrowser()
  
    var currentTab = sessionStorage.getItem('qc-current-tab-$transactionId')
    if(currentTab  == null){
        currentTab = sessionStorage.setItem('qc-current-tab-$transactionId', 'summary-tab-id')
    }
    if(currentTab == 'cross-tab-id'){
        $('#cross-tab-id').trigger("click")
    }
    
    function renderBrowser() {
        $("#plot-browser-container-id").hide();
        $("#cross-browser-container-id").hide();
        $('.tooltipped').tooltip();
        varResetGridUrl = '$resetGridUrl';

        $('#summary-variable-modal').on('show.bs.modal', function (e) {

            var obj = $(e.relatedTarget)
            var variableName = obj.data('variable_name');
            var variableId = obj.data('variable_id');

            $('#summary-variable-header-id').html('<i class="material-icons"></i> ' + variableName);

            $(".summary-variable-modal-content").html('');
            $('#loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');

            setTimeout(function () {
                $.ajax({
                    url: '$viewVariable',
                    data: {
                        id: variableId
                    },
                    type: 'POST',
                    async: true,
                    success: function (data) {
                        // $('.view-variable-modal').modal('show');
                        $('.summary-variable-modal-content').html(data);
                        $('#loading-div').html('');
                    },
                    error: function () {
                        $('.summary-variable-modal-content').html('<i>There was a problem while loading record information.</i>');
                    }
                });
            }, 300);
        });
        $(document).on('click', '#plot-tab-id', function (e) {
            if ($("#plot-browser-container-id").hide()) {
                $("#plot-browser-container-id").show();
                $("#summary-container-id").hide();
                $("#cross-browser-container-id").hide();
                sessionStorage.setItem('qc-current-tab-$transactionId', 'plot-tab-id');
            }
        });

        $(document).on('click', '#cross-tab-id', function (e) {
            if ($("#cross-browser-container-id").hide()) {
                $("#cross-browser-container-id").show();
                $("#summary-container-id").hide();
                $("#plot-browser-container-id").hide()
                sessionStorage.setItem('qc-current-tab-$transactionId', 'cross-tab-id');
            }
        });

        $(document).on('click', '#summary-tab-id', function (e) {
            if ($("#summary-container-id").hide()) {
                $("#summary-container-id").show();
                $("#plot-browser-container-id").hide();
                $("#cross-browser-container-id").hide();
                sessionStorage.setItem('qc-current-tab-$transactionId', 'summary-tab-id');
            }
        });

        $(document).on('click', '.data-level-id', function (e) {
            if ($(this).data('dataunit') == 'plot') {
                $("#plot-tab-id").trigger("click");
                $.pjax.reload({
                    container: '#' + $(this).data('dataunit') + '$dynaGridId-pjax',
                    replace: false,
                    url: varResetGridUrl + '&PlotData[fields]=' + $(this).attr("data-variable")
                });

            }

            if ($(this).data('dataunit') == 'cross') {
                $("#cross-tab-id").trigger("click");
                $.pjax.reload({
                    container: '#' + $(this).data('dataunit') + '$dynaGridId-pjax',
                    replace: false,
                    url: varResetGridUrl + '&CrossData[fields]=' + $(this).attr("data-variable")
                });

            }
        });

        $(document).on('click', '.filter-data-link', function (e) {
            e.preventDefault();
            $.pjax.reload({
                container: '#' + $(this).data('dataunit') + '$dynaGridId-pjax',
                replace: false,
                url: $(this).data('url')
            });
            $("#" + $(this).data('dataunit') + "-tab-id").trigger("click");
        });

        $(document).on('click', '.filter-status-link', function (e) {
            e.preventDefault();
            $.pjax.reload({
                container: '#' + $(this).data('dataunit') + '$dynaGridId-pjax',
                replace: false,
                url: $(this).data('url')
            });
            $("#" + $(this).data('dataunit') + "-tab-id").trigger("click");
        });

    }
    plotRows = [];
    // renders checkbox events 
    function renderCheckbox(dataLevel) {

        $("#" + dataLevel + "-table-id tbody tr").on('click', function (e) {

            this_row = $(this).find('input:checkbox')[0];
            selectAll = 'page';
            if (this_row.checked) {
                $("#" + this_row.id).prop("checked", false);
                this_row.checked = false;
                $(this).removeClass("grey lighten-4");
            } else {
                $("#" + this_row.id).prop("checked", true);
                $(this).addClass("grey lighten-4");
                this_row.checked = true;

            }
            if ($('#select-all-' + dataLevel + '-id').prop("checked") && !this_row.checked) {
                $('#select-all-' + dataLevel + '-id').prop("checked", false);
                $('#select-all-' + dataLevel + '-id').removeAttr('checked');
            }
            plotRows = [];
            $("input:checkbox.select-plot-row").each(function () {

                if ($(this).prop("checked") === true) {
                    plotRows.push($(this).attr("id"));
                }

            });

            if (plotRows.length == 1) {
                $('#selected-items-div').html('Selected <b>' + plotRows.length + '</b> item.');
            } else if (plotRows.length > 1) {
                $('#selected-items-div').html('Selected <b>' + plotRows.length + '</b> items.');
            } else {
                $('#selected-items-div').html('No selected items.');
            }

        });

        $('.select-' + dataLevel + '-row').on('click', function (e) {
            selectAll = 'page'
            if ($(this).prop("checked") === true) {
                $(this).attr('checked', 'checked');
                $(this).parent("td").parent("tr").addClass("grey lighten-4");
            } else {
                $(this).removeAttr('checked');
                $(this).parent("td").parent("tr").removeClass("grey lighten-4");
            }
            if ($('#select-all-' + dataLevel + '-id').prop("checked") && !this_row.checked) {
                $('#select-all-' + dataLevel + '-id').prop("checked") = false;
                $('#select-all-' + dataLevel + '-id').removeAttr('checked');
            }

            plotRows = [];
            $("input:checkbox.select-plot-row").each(function () {

                if ($(this).prop("checked") == true) {
                    plotRows.push($(this).attr("id"));
                }

            });
            if (plotRows.length == 1) {
                $('#selected-items-div').html('Selected <b>' + plotRows.length + '</b> item.');
            } else if (plotRows.length > 1) {
                $('#selected-items-div').html('Selected <b>' + plotRows.length + '</b> items.');
            } else {
                $('#selected-items-div').html('No selected items.');
            }

        });

        $('#select-all-' + dataLevel + '-id').on('click', function (e) {

            if ($(this).prop("checked") === true) {
                selectAll = 'selected'
                $(this).attr('checked', 'checked');
                $('.select-' + dataLevel + '-row').attr('checked', 'checked');
                $('.select-' + dataLevel + '-row').prop('checked', true);
                $(".select-" + dataLevel + "-row:checkbox").parent("td").parent("tr").addClass("grey lighten-4");

            } else {
                selectAll = ''
                $(this).removeAttr('checked');
                $('.select-' + dataLevel + '-row').prop('checked', false);
                $('.select-' + dataLevel + '-row').removeAttr('checked');
                $("input:checkbox.select-" + dataLevel + "-row").parent("td").parent("tr").removeClass("grey lighten-4");
                $('#selected-items-div').html('No selected items.');
            }

            if (selectAll == 'all') {

                if (varPlotDataTotalCount == 1) {
                    $('#selected-items-div').html('Selected <b>' + varPlotDataTotalCount + '</b> item.');
                } else if (varPlotDataTotalCount > 1) {
                    $('#selected-items-div').html('Selected <b>' + varPlotDataTotalCount + '</b> items.');
                }
            } else if (selectAll == 'selected') {
                plotRows = [];
                $("input:checkbox.select-plot-row").each(function () {

                    if ($(this).prop("checked") === true) {
                        plotRows.push($(this).attr("id"));
                    }

                });

                if (plotRows.length == 1) {
                    $('#selected-items-div').html('Selected <b>' + plotRows.length + '</b> item.');
                } else if (plotRows.length > 1) {
                    $('#selected-items-div').html('Selected <b>' + plotRows.length + '</b> items.');
                } else {
                    $('#selected-items-div').html('No selected items.');
                }
            }
        });
    }
JS
);

?>