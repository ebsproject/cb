<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\TabularForm;
use app\dataproviders\ArrayDataProvider;

/**
 * Renders the content of Suppress conditions of a variable
 */
$form = ActiveForm::begin([
    'id' => "filter-form-id",
]);

?>
<!-- Suppress Data data browser -->
<div id="mismatch-filter-error-container"></div>
<input type="hidden" name="transactionDbId" value="<?php echo $transactionId; ?>">
<input type="hidden" name="variableDbId" value="<?php echo $variableId; ?>">

<?php
$params = [
    "status" => "equals new|equals complete",
    "transactionDbId" => "equals $transactionId",
    "targetVariableDbId" => "equals $variableId"
];

$response = \Yii::$container->get('app\models\SuppressionRule')->searchAll($params);
$options = [
    'key' => 'suppressionRuleDbId',
    'allModels' => $response["data"],
];

$dataProvider = new ArrayDataProvider($options);
$rows = $response["data"];

if (!empty($rows)) {
    $param = [
        "variableDbId" => "" . $rows[0]["targetVariableDbId"]
    ];
    $variable =  \Yii::$container->get('\app\modules\dataCollection\models\Transaction')->getVariable($param);
    $dataType = $variable["dataType"];
} else {
    $dataType = 'text';
}

if ($dataType === "date") {
    $value = [
        'type' => TabularForm::INPUT_TEXT,
        'options' => ["class" => "datepicker filter-form-value", 'disabled' => 'disabled']
    ];
} else if ($dataType === "integer") {
    $value = [
        'type' => TabularForm::INPUT_TEXT,
        'options' => ["type" => "number", "min" => "0", "class" => "filter-form-value", 'disabled' => 'disabled']
    ];
} else if ($dataType === "float") {
    $value = [
        'type' => TabularForm::INPUT_TEXT,
        'options' => ["type" => "number", "min" => "0", "step" => "any", "class" => "filter-form-value", 'disabled' => 'disabled']
    ];
} else {
    $value = [
        'type' => TabularForm::INPUT_TEXT,
        'options' => ["class" => "filter-form-value", 'disabled' => 'disabled']
    ];
}
$value = [
    'type' => TabularForm::INPUT_STATIC,
    'options' => ["class" => "filter-form-value"]
];
echo "<div  id='filter-error-summary-id'></div>";

echo TabularForm::widget([

    'dataProvider' => $dataProvider,
    'formName' => 'SuppressionRule',
    'form' => $form,

    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,

    ],

    'attributes' => [
        'suppressionRuleDbId' => [
            'type' => TabularForm::INPUT_HIDDEN,
            'visible' => false,
        ],
        'conjunction' => [
            'label' => \Yii::t('app','Conjunction'),
            'value' => function ($model) {
                    return '
                        <div class="switch" style="width:115px">
                            <label>
                            OR
                            <input class="filter-conjunc-supressed" type="checkbox" value="'.$model["conjunction"].'"' . ($model["conjunction"] ? 'checked disabled' : '') . '>
                            <span class="lever"></span>
                            AND
                            </label>
                        </div>';
            },
            'type' => TabularForm::INPUT_STATIC,
            'columnOptions' => ['width' => '16%']
        ],
        'factorVariableLabel' =>
        [
            "label" => "Trait",
            'type' => TabularForm::INPUT_STATIC,
            'columnOptions' => ['width' => '25%']

        ],
        'operator' =>
        [
            'type' => TabularForm::INPUT_STATIC,
            'columnOptions' => ['width' => '10%']
        ],

        'value' => $value,
        'remarks' => [
            'label' => "Suppress remarks",
            'type' => TabularForm::INPUT_STATIC,
        ],
        'status' => [
            'label' => "Status",
            'value' => function ($model) {
                if ($model["status"] === 'new') {
                    return "<span class='badge new blue'><strong>new</strong></span>";
                } else {
                    return "<span class='badge new green white-text'><strong>suppressed</strong></span>";
                }
            },
            'type' => TabularForm::INPUT_STATIC,
        ],
    ],

    'serialColumn' => false,
    'checkboxColumn' => false,
    'actionColumn' =>
    [
        'class' => '\kartik\grid\ActionColumn',
        'template' => '{delete}{view}',
        'buttons' => [
            'delete' => function ($url, $model) {
            },
            'view' => function ($url, $model) {
                if ($model['status'] == 'complete') {
                } else {

                    return Html::a(
                        '<i class="material-icons">clear</i>',
                        false,
                        [
                            'class' => 'delete-suppression-rule',
                            'title' => 'Remove rule',
                            'data-id' => $model["suppressionRuleDbId"],
                            'data-operator' => $model["operator"],
                            'data-value' => $model["value"],
                            'data-variable_id' => $model["factorVariableDbId"],
                            'style' => "cursor:pointer;"
                        ]
                    );
                }
            },
        ],
    ],

    'gridSettings' => [
        'id' => 'filters-grid-id',
        'layout' => '{items}',
        'bordered' => true,
        'striped' => true,
        'hover' => true,
        "showPageSummary" => false,
        "summary" => "",
        'emptyText' => '<span class="not-set">No rules found</span>',
        'rowOptions' => function ($model, $key, $index, $grid) {
            return [
                'id' => $index
            ];
        },
        'tableOptions' => ["id" => 'filter-form-grid-id', 'class' => 'card'],

        'panel' => false
    ]

]);

echo Html::a('Add rule', '#', ['type' => 'button', 'class' => 'hidden btn pull-left add-rule-btn-id waves-effect waves-light']);

?>

<?php
ActiveForm::end();
$js = <<<JS
// change the switch button, this checks the switch button for each row
$('input.filter-conjunc-supressed:checkbox').each(function() {

    if ($(this).val() == "or") {
        $(this).prop("checked", false);
    }
});

// Change the operator and or values when a variable is selected in the filter form
$('#filter-form-grid-id tbody').on('change', '.filter-select-var', function(e) {
    
    e.preventDefault();
    var key = $(this).parent().parent().find('input').val();
    var v = $(this).val();
    var self = $(this);
    var tr = self.closest('tr');

    var filterDataLevel = $(this).attr('title') ;
    
    tr.find('.filter-form-data-level').val(filterDataLevel);

    var index = parseInt(tr.attr('id'));
    if (v !== '') {

        var variableScale;
        $.ajax({
            url: "get-variable-scale",
            dataType: 'json',
            type: 'get',
            async: false,
            data: {
                variableDbId: $('option:selected', this).attr('data-id')
            },
            success: function(data) {
                variableScale = data;
            },
            error: function(jqXHR, exception) {

                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect. Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.' + jqXHR.responseText;
                }
                alert(msg);
            }
        });

        tr.find('select.filter-select-operator').empty();
        tr.find('select.filter-select-value-select2').empty();
        tr.find('select.filter-select-value-select2').addClass('hidden');
        tr.find('.suppress-datepicker').addClass("hidden");
        tr.find('.filter-form-value').addClass("hidden");

        var selectOperator = variableScale.operator;

        var x;
        var option;
        for (x in selectOperator) {
            option = $('<option></option>').attr("value", x).text(selectOperator[x]);
            tr.find('.filter-select-operator').append(option);
        }

        if (variableScale.hasScale) {

            // hide the input field for values, show select2 
            var scales = variableScale.values
            var firstId;
            for (x in scales) {
                var i = Object.keys(scales).indexOf(x);
                if (i == 0) {
                    firstId = x;
                }
                option = $('<option></option>').attr("value", x).text(scales[x]);
                tr.find('.filter-select-value-select2').append(option);
            }

            tr.find('.filter-select-value-select2').attr("name", "SuppressionRule[" + index + "][value]");
            $('.filter-select-value-select2[name="SuppressionRule[' + index + '][value]"]').val(firstId).trigger('change');
            $('.filter-select-value-select2[name="SuppressionRule[' + index + '][value]"]').prop("disabled", false);
            var remarks = tr.find('.filter-select-var :selected').val() + " " + tr.find('.filter-select-operator :selected').val() + " " + firstId;

            tr.find('textarea').val(remarks);
            tr.find("input.filter-form-value[type=text]").attr("name", "");

        } else {

            tr.find('.filter-select-value-select2').attr('name','');
            
            tr.find('.filter-select-value-select2').addClass("hidden");
            tr.find('.filter-select-value-select2').next(".select2-container").hide();
            
            tr.find('.filter-form-value').removeClass("hidden");                
            tr.find('.filter-form-value').prop("disabled", false);

            if (variableScale.dataType == 'date') {

                tr.find('.filter-form-value').prop("disabled", true);
                tr.find('.suppress-datepicker').removeClass("hidden");
                tr.find('.suppress-datepicker').attr("name", "SuppressionRule[" + index + "][value]");
                tr.find('.filter-form-value').addClass("hidden");

            } else if (variableScale.dataType == 'integer' || variableScale.dataType == 'float' || variableScale.dataType == 'double precision') {

                tr.find(".input-group> .suppress-datepicker" ).attr("name", "");
            
                tr.find('.filter-form-value').replaceWith('<input type="number" class="filter-form-value">');
                tr.find('.filter-form-value').attr("value", "");
                tr.find('.filter-form-value').attr("name", "SuppressionRule[" + index + "][value]");
            } else {

                tr.find(".input-group> .suppress-datepicker" ).attr("name", "");
                tr.find('.filter-form-value').replaceWith('<input type="text" class="filter-form-value">');
                tr.find('.filter-form-value').attr("value", "");
                tr.find('.filter-form-value').attr("name", "SuppressionRule[" + index + "][value]");
            }
        }

    }
});

// remove the row that is not yet saved
$('#filter-form-grid-id tbody').on('click', '.remove-filter-new', function(e){ 
 
    // bind the new elements/row into remove row function 
    e.preventDefault();
 
    $(this).closest('tr').remove();
    $('#filter-error-summary-id').empty();
    $('#filter-error-summary-id').removeClass('error-summary');
 
    // show/hide suppress and unsuppress button
    if ($("#filter-form-grid-id tbody > tr:last").text() == '<span class="not-set">No rules found</span>') {

        $('#suppress-next-btn-id').addClass('hidden');
        $('#unsuppress-next-btn-id').addClass('hidden');
    } else {
        $('#suppress-next-btn-id').removeClass('hidden');
        $('#unsuppress-next-btn-id').removeClass('hidden');
    }
});

// remove the suppression rule record that is already saved with status of new
$('.delete-suppression-rule').on('click', function(e){  //bind the new elements/row into 
    //remove row function
    
    $.ajax({
        url: "delete-suppression-rule?id="+$(this).attr('data-id'),
        type: 'post',
        async: false,
        success: function(data) {

        },error:function(jqXHR, textStatus, errorThrown){
            $('#loading-div').html('');
        }
    });
    e.preventDefault();
    $(this).closest ('tr').remove();
    $('#filter-error-summary-id').empty();
    $('#filter-error-summary-id').removeClass('error-summary');
});

// dynamically adds row in suppress rule 
$('.add-rule-btn-id').on('click', function(e){
    e.preventDefault();

    $('#loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
    if ($("#filter-form-grid-id tbody > tr:last").text()=="No rules found") {
        $("#filter-form-grid-id tbody > tr:last").remove();
    }
    var table = $('#filter-form-grid-id >tbody:first'); 

    var rowNum = $('#filter-form-grid-id >tbody').children().length;
    
    var nextRow = parseInt($("#filter-form-grid-id tbody:first > tr:last").attr('data-key'));
    if (isNaN(nextRow)){
        nextRow= 0;    
    }
    nextRow+=1;
    
    if(rowNum==0){
        $("#filter-form-grid-id tbody:last-child").append("<div id='result-div' ></div>");
    }else{
        $("#filter-form-grid-id tbody:first > tr:last").after("<div id='result-div' ></div>");
    }
    
    $.ajax({
        url: "add-rule",
        type: 'post',
        async: false,
        data:{

            rowNum : nextRow, 
            dataType : $('option:selected', '#plot-suppress-rule-select2-id').attr('data-data_type'),
            firstAbbrev : $("#plot-suppress-rule-select2-id").val(),
            firstLabel : $('option:selected', '#plot-suppress-rule-select2-id').attr('data-label'), 
            dataLevel: $('option:selected', '#plot-suppress-rule-select2-id').attr('data-data_level'),
            transactionId:varTransactionId
        },
        success: function(data) {

            $("#result-div").html(data);
            $(".conjunct-field").wrapAll('<td class="kv-align-top" style="width:16%;" data-col-seq="0"><div class="form-control-static"></div></td>');
            $(".conjunct-field").replaceWith($(".conjunct-field").contents());

            if(nextRow==1){
                
                $('input[name="SuppressionRule[1][conjunction]"]' ).attr("disabled", true);
            }

            $("#var-field-id").wrapAll('<td class="kv-align-middle" style="width:25%;" data-col-seq="1"></td>');
            $("#var-field-id").replaceWith($("#var-field-id").contents());

            $("#op-field-id").wrapAll('<td class="kv-align-middle" style="width:10%;" data-col-seq="2"></td>');
            $("#op-field-id").replaceWith($("#op-field-id").contents());

            $("#value-field-id").wrapAll('<td class="kv-align-middle" data-col-seq="3"></td>');
            $("#value-field-id").replaceWith($("#value-field-id").contents());

            $("#remarks-field-id").wrapAll('<td class="kv-align-middle" data-col-seq="4"></td>');
            $("#remarks-field-id").replaceWith($("#remarks-field-id").contents());

            $("#status-field-id").wrapAll('<td class="kv-align-middle" data-col-seq="5"></td>');
            $("#status-field-id").replaceWith($("#status-field-id").contents());

            $("#action-field-id").wrapAll('<td class="kv-align-middle" data-col-seq="6"></td>');
            $("#action-field-id").replaceWith($("#action-field-id").contents());

            $("#action-col-field-id").wrapAll("<td></td>");
            $("#action-col-field-id").replaceWith($("#action-col-field-id").contents());

            $("#result-div").wrapAll("<tr id='"+ nextRow+ "' data-key='"+ nextRow+ "'></tr>");

            $("#filter-form-grid-id tbody:first > tr:last").after($("#result-div").replaceWith($("#result-div").contents()));

            $('#loading-div').html('');
            $('#suppress-next-btn-id').removeClass('hidden');
            $("#result-div").remove();

            // Stop user to press enter in textbox 
            $("input:text").keypress(function(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#filter-form-grid-id tbody:first > tr:last").find('.filter-select-var').val($("#plot-suppress-rule-select2-id").val()).trigger('change');
            changeValue();
           
        },error:function(e){
            $('#loading-div').html('');
        }
    });
});
function changeValue() {
    
    var isValueDropDown = false;
    // Change the operator and or values when a variable is selected in the filter form

    // Add remarks when a value is inputted
    // input field is text field 
    $('input.filter-form-value').change(function(e) {
        e.preventDefault();
        var key = $(this).parent().parent().find('textarea').val();
        var v = $(this).val();

        var self = $(this);
        var tr = self.closest('tr');

        var index = tr.index();

        var remarks = tr.find('.filter-select-var :selected').val() + " " + tr.find('.filter-select-operator :selected').val() + " " + v;

        $(this).parent().parent().find('textarea').val(remarks);

        isValueDropDown = false;
    });
    // input field is a dropdown 
    $('select.filter-select-value-select2').on('change', function(e) {
        e.preventDefault();

        var key = $(this).parent().parent().find('textarea').val();
        var v = $(this).val();

        var self = $(this);
        var tr = self.closest('tr');

        var index = tr.index();

        var remarks = tr.find('.filter-select-var :selected').val() + " " + tr.find('.filter-select-operator :selected').val() + " " + v;

        tr.find('textarea').val(remarks);

        isValueDropDown = true;
    });
    // input field is a dropdown 
    $('.suppress-datepicker').on('change', function(e) {
        e.preventDefault();
        var key = $(this).parent().parent().find('textarea').val();
        var v = $(this).val();

        var self = $(this);
        var tr = self.closest('tr');

        var index = tr.index();

        var remarks = tr.find('.filter-select-var :selected').val() + " " + tr.find('.filter-select-operator :selected').val() + " " + v;

        tr.find('textarea').val(remarks);

        isValueDropDown = true;
    });

    $('select.filter-select-operator').on('change', function(e) {
        e.preventDefault();
        var varDataType = $('option:selected', '#plot-suppress-rule-select2-id').attr('data-data_type');
        var key = $(this).parent().parent().find('textarea').val();
        var v = $(this).val();
        var self = $(this);
        var tr = self.closest('tr');

        var index = tr.index();

        var fldValue;

        if (isValueDropDown) {
            fldValue = tr.find('select.filter-select-value-select2').val();
        } else if (varDataType == 'date') {
            fldValue = tr.find('.suppress-datepicker').val();
        } else {
            fldValue = tr.find('.filter-form-value').val();

        }

        var remarks = tr.find('.filter-select-var').val() + " " + v + " " + fldValue;

        tr.find('textarea').val(remarks);
    });

    $('select.filter-select-var').on('change', function(e){
        e.preventDefault();
        var key = $(this).parent().parent().find('textarea').val();
        var v = $(this).val();
        var self = $(this);
        var tr = self.closest('tr');

        var selectTraitFilter = $('option:selected', '#plot-suppress-rule-select2-id').val();
        var mismatchFound = false;

        $('.filter-select-var').each(function() {
            if ($(this).val() !== selectTraitFilter) {
                mismatchFound = true;
                return false;
            }
        });

        if (mismatchFound) {
            var errorMessage = '<div class="alert error"><div class="card-panel">Trait filters do not match.</div></div>';
            $('#mismatch-filter-error-container').html(errorMessage);
        } else {
            $('#mismatch-filter-error-container').empty();
        }
    });

    $('input.filter-select-operator').change(function(e) {
        e.preventDefault();
        var key = $(this).parent().parent().find('textarea').val();
        var v = $(this).val();
        var self = $(this);
        var tr = self.closest('tr');

        var index = tr.index();

        var fldValue;

        if (isValueDropDown) {
            fldValue = tr.find('select.filter-form-value').val();
        } else {
            fldValue = tr.find('.filter-form-value').val();
        }

        var remarks = tr.find('.filter-select-var').val() + " " + v + " " + fldValue;

        tr.find('textarea').val(remarks);
    });

    $('input.filter-select-var').change(function(e) {
        
        e.preventDefault();
        var key = $(this).parent().parent().find('textarea').val();
        var v = $(this).val();

        var self = $(this);
        var tr = self.closest('tr');

        var index = tr.index();

        var filter_data_level = tr.find('.filter-select-var :selected').attr('data_level') ;

        $(this).parent().parent().find('.filter-form-data-level').val(filter_data_level);

        isValueDropDown = false;
    });

}
JS;
$this->registerJs($js);
// $this->registerJsFile("@web/js/data-terminal/suppress.js", ['depends' => ['miloschuman\highcharts\HighchartsAsset'], 'position' => \yii\web\View::POS_END]);
?>