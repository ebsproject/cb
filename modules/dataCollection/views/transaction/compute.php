<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\DataBrowserConfiguration;


//id of dc compute browser
$browserId = 'dynagrid-dc-compute-grid';
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

//set columns and attributes
$columns = [
    [
        'label'=> "Checkbox",
        'header'=>'
            <input type="checkbox" class="filled-in" id="formula-select-all" />
            <label for="formula-select-all"></label>
        ',
        'content'=>function($model) use ($selectedFormula, $resultVariables, $status) {
            if($status == 'uploaded') {
                $isChecked = in_array($model['formulaDbId'], $selectedFormula) ? 'checked' : '';
            }else if ($status == 'uploaded: to be calculated' || $status = 'trait calculation failed'){
                $isChecked = in_array($model['resultVariableDbId'], $resultVariables) ? 'checked' : '';
            }
            return '
                <input class="compute-grid-select filled-in" type="checkbox" id="'.$model['resultVariableDbId']. '-' . $model['formulaDbId'] . '" data-formula-db-id="' . $model['formulaDbId'] . '" data-result-variable-id="' . $model['resultVariableDbId'] . '" data-status="' . $status . '" '.$isChecked.' />
                <label for="'.$model['resultVariableDbId']. '-' . $model['formulaDbId'] . '"></label>      
            ';
        },        
        'hAlign'=>'center',
        'vAlign'=>'middle',
        'mergeHeader'=>true,
        'order'=> DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'class' => '\kartik\grid\SerialColumn',
        'header' => false,
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'vAlign' => 'top',
        'hAlign' => 'left',
    ],
    [
        'attribute' => 'resultVariableAbbrev',
        'label' => \Yii::t('app','Variable'),
    ],
    [
        'attribute' => 'resultVariableName',
        'label' => \Yii::t('app','Name'),
    ],
    [
        'attribute' => 'resultVariableLabel',
        'label' => \Yii::t('app','Label'),
    ],
    [
        'attribute' => 'formula',
    ]
    
];

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $formulaModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => '<h5 class="panel-title">Formula</h5>',
            'before' => '<p style="padding:0 15px;">This browser displays all the possible formulas specified for each variable (auto-selected for single formula). 
                Select preferred one for the variables with multiple formulas then click CALCULATE. <br> 
                The computation will be processed in the background and any formulas with missing parameter(s) will be excluded. 
                Once done, you may skip this step and go directly to the REVIEW tab.</p>',
        ],
        'toolbar' => 
            [
                ['content' => '<div id="selected-formula-div" style="margin-top:10px;">No selected items.</div>'],
                ['content' => 
                        Html::button('Calculate', 
                        [
                        'type'=>'button', 
                        'title'=>Yii::t('app', 'Calculate traits with selected formulas'), 
                        'class'=>'btn', 
                        'id'=>'calculate-trait-btn',
                    ])
                ],
                ['content' =>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                    ["/dataCollection/transaction/view?program=$program&id=$transactionId"], 
                    [
                        'id'=>'reset-'.$browserId,
                        'class' => 'btn btn-default', 
                        'title'=>\Yii::t('app','Reset formula browser'), 
                        'data-pjax' => true
                    ]).'{dynagridFilter}{dynagridSort}{dynagrid}'
                ]
            ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// Define URLs here
$computeUrl = Url::to(['transaction/compute', 'id' => $transactionId]);

$script = <<< JS

var selectedResultVariableDbIds = [];
var selectedFormulas = [];
resize();

// Resize the grid table upon loading
function resize(){
    var maxHeight = ($(window).height() - 420);
    $(".kv-grid-wrapper").css("height", maxHeight);
}

// Update select all checkbox state based on selected checkboxes
function checkAllSelected() {
    var totalCheckboxes = $(".compute-grid-select").length;
    var checkedCheckboxes = $(".compute-grid-select:checked").length;
    $("#formula-select-all").prop("checked", totalCheckboxes === checkedCheckboxes);
}

// Display of number of selected items
function updateSelectedItemsDiv() {
    var selectedCount = $(".compute-grid-select:checked").length;
    $("#selected-formula-div").html("Selected <b>" + selectedCount + "</b> item" + (selectedCount !== 1 ? "s" : ""));
}

// Update selected item count on page load
$(document).ready(function() {
    updateSelectedItemsDiv();

    // Handle select all checkbox functionality
    $(document).on('click', '#formula-select-all', function() {
        $(".compute-grid-select").prop('checked', $(this).prop('checked'));
        updateSelectedItemsDiv();
    });
});

var anyChecked = $(".compute-grid-select:checked").length > 0;

// Check if there are more than one resultVariableDbId
var uniqueResultVariableDbIds = new Set($(".compute-grid-select").map(function() {
    return $(this).data("result-variable-id");
}).get());

if (!anyChecked && uniqueResultVariableDbIds.size === 1) {
    $("#formula-select-all").prop("disabled", false);
}

// Uncheck all checkboxes with the same resultVariableDbId
$(".compute-grid-select").each(function() {
    var resultVariableDbId = $(this).data("result-variable-id");
    var status = $(this).data("status");
    var isChecked = $(this).is(":checked");
    var checkboxesWithSameId = $(".compute-grid-select[data-result-variable-id='" + resultVariableDbId + "']");
    if (checkboxesWithSameId.length > 1 && status == "uploaded: to be calculated") {
        checkboxesWithSameId.prop("checked", false);
    }
});

// Trigger change event for auto-selected checkboxes on page load
$(".compute-grid-select:checked").each(function() {
    var resultVariableDbId = $(this).data("result-variable-id");
    var formulaDbId = $(this).data("formula-db-id");
    
    selectedResultVariableDbIds.push(resultVariableDbId);
    selectedFormulas.push(resultVariableDbId + '-' + formulaDbId)

    $(this).trigger("change");
});

$(document).ready(function() {
    updateSelectedItemsDiv();
    checkAllSelected();

    // Handle select all checkbox functionality
    $(document).on('click', '#formula-select-all', function() {
        $(".compute-grid-select").prop('checked', $(this).prop('checked'));
        updateSelectedItemsDiv();
    });
});

// Handles selection/deselection of multiple formula
$(document).on("change", ".compute-grid-select", function() {
    var resultVariableDbId = $(this).data("result-variable-id");
    var isChecked = $(this).is(":checked");

    // Uncheck all other checkboxes with the same resultVariableDbId
    $(".compute-grid-select[data-result-variable-id='" + resultVariableDbId + "']").not(this).prop("checked", false);

    // Update selectedResultVariableDbIds array based on the selected checkboxes
    selectedResultVariableDbIds = $(".compute-grid-select:checked").map(function() {
        return $(this).data("result-variable-id");
    }).get();

    selectedFormulas = $(".compute-grid-select:checked").map(function() {
        return $(this).data("result-variable-id") + '-' + $(this).data("formula-db-id");
    }).get();

    updateSelectedItemsDiv();
    checkAllSelected();
});

// Retain selection when grid reloads
$(document).on("pjax:success", function() {
    updateSelectedItemsDiv();
    checkAllSelected();
});

// Triggers background trait calculation
$(document).on("click", "#calculate-trait-btn", function() {

    if(selectedFormulas.length == 0){
        Materialize.toast("<i class='material-icons orange-text'>warning</i> Please select at least (1) formula", 3000);
    }else{
        $('.loading-div').html('<div class="progress"><div class="indeterminate"></div></div>');
        $.ajax({
            url: '$computeUrl',
            type: 'POST',
            data: {
                formulas: selectedFormulas,
                formulaDbIds: $formulaDbIds,
                variables: $variables,
            },
            success: function(response) {
                data = JSON.parse(response);

                if (!data.success) {
                    var toastContent = '<i class="material-icons red-text">close</i></button>' +
                        'Error in trait calculation';

                    Materialize.toast(toastContent, 5000);
                } else {

                    window.location = browseTerminalUrl + "&TerminalTransaction[transactionDbId]=" + '$transactionId';

                }
            },
            error: function(xhr, status, error) {
                msg = logError(jqXHR, exception);

                var toastContent = '<i class="material-icons red-text">close</i></button>' + 'Error: ' + msg;

                Materialize.toast(toastContent, 5000);

            }
        });
    }
});
JS;
$this->registerJs($script);