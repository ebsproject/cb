<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders selected traits (formerly "variables") in export workbook
 */
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use app\widgets\VariableInfo;

Modal::begin([]);
Modal::end();

\yii\jui\JuiAsset::register($this);

//set entity
if(!empty($crossListId)){
    $entity = 'cross list';
}else if (!empty($occurrenceId)){
    $entity = 'occurrence';
}else{
    $entity = 'location';
}

// variables columns
$columns = [
    [
        'label'=> "checkbox",
        'header'=>'
            <input type="checkbox" class="filled-in" id="export_workbook-variables-grid-select-all-id" />
            <label for="export_workbook-variables-grid-select-all-id"></label>
        ',
        'contentOptions' => ['style' => ['max-width' => '100px;',]],
        'content'=>function($data) {
            return '
                <input class="final-grid_select filled-in" type="checkbox" id="'.$data["variableDbId"].'" />
                <label for="'.$data["variableDbId"].'"></label>
            
            ';
        },        
        'hAlign'=>'center',
        'vAlign'=>'middle',
        'hiddenFromExport'=>true,
        'mergeHeader'=>true,
        'width'=>'50px'
    ],
    [
        'attribute'=>'label',
        'format'=>'raw',
        'vAlign'=>'middle',
        'value'=> function($data){
            // render view variable info widget
            return VariableInfo::widget([
                'id' => $data['variableDbId']
            ]);


        },
        'contentOptions' => function($data) {
            return ['class' => 'order_tr', "data-varId" => $data['variableDbId']];
        },
        'headerOptions' => ['class' => 'sorter', 'style'=>'min-width:100px !important']
    ],
    [
        'attribute'=>'displayName',
        'format'=>'raw',
        'vAlign'=>'middle',
        'value'=> function($data){
            return !empty($data['displayName']) ? $data['displayName'] : '<span class="not-set">(not set)</span>';
        },
        'headerOptions' => ['class' => 'sorter']
    ],
    [
        'attribute'=>'scaleValue',
        'label' => 'scale',
        'format'=>'raw',
        'vAlign'=>'middle',
        'value'=> function($data){
            return !empty($data['scaleValue']) ? $data['scaleValue'] : '<span class="not-set">(not set)</span>';
        },
        'headerOptions' => ['class' => 'sorter']
    ],
];

$downloadFieldBookTraitUrl =  Url::to(['field-book/download-trait', 'fileName'=>$fileName, 'entity' => $entity]);
$downloadFieldBookLocationUrl =  Url::to([
    'field-book/download-location',
    'program' => $program,
    'locationId' => $locationId,
    'occurrenceId' => $occurrenceId,
    'crossListId' => $crossListId,
    'fileName' => $fileName,
]);

//actions
//temporarily disabled exporting of study and trt files
$actionExportTraitsForFieldbook = Html::a(
    '<i class="material-icons">folder_special</i>',
    '#!',
    [
        'data-pjax'=>0,
        'url'=>"#",
        'class' => 'btn blue-grey darken-2 waves-light waves-effect export-workbook-actions',
        'id' => 'export_trait-btn',
        'title'=>\Yii::t('app','Export traits for FieldBook App'),
    ]
);

$exportTitle = \Yii::t('app',"Export $entity for Field Book");
$exportForFieldBookWithTraitAbbrevs = \Yii::t('app','Export with trait headers');
$exportForFieldBookApp = \Yii::t('app','Export for FieldBook App');

$actionExportLocationForFieldbook = "
    <button 
        title='$exportTitle'
        id='export_study-btn'
        class='dropdown-button dropdown-button-pjax btn blue darken-3 waves-light waves-effect export-workbook-actions'
        href='#!'
        data-activates='export_study-dropdown-menu'
        data-beloworigin='true' 
        data-constrainwidth='false'
    >
        <span
            class='material-icons'
            style='
                vertical-align: -8px;
            '
        >
            phone_android
        </span> <span class='caret'></span>
    </button>
        <ul
            id='export_study-dropdown-menu'
            class='dropdown-content'
            data-beloworigin='false'
            style='width: 160px !important;'
        >
            <li
                id='export-for-field-book-with-trait-abbrevs-btn'
                class='hide-loading export-for-field-book export-for-field-book-btn'
                data-include-trait-abbrevs='true'
                title='$exportForFieldBookWithTraitAbbrevs'
            >" .
                Html::a(
                    '<p style="display: inline;">With trait abbreviations</p>',
                    '',
                    [
                        'target' => '_blank',
                        'rel' => 'noopener noreferrer'
                    ]
                ) .
            "</li>" .
            "<li
                id='export-for-field-book-without-trait-abbrevs-btn'
                class='hide-loading export-for-field-book export-for-field-book-btn'
                data-include-trait-abbrevs='false'
                title='$exportForFieldBookApp'
            >" .
                Html::a(
                    '<p style="display: inline;">For FieldBook App</p>',
                    '',
                    [
                        'target' => '_blank',
                        'rel' => 'noopener noreferrer'
                    ]
                ) .
            "</li>
        </ul>";
$actionSaveList = Html::a(
    '<i class="material-icons">add_shopping_cart</i>',
    '#!',
    [
        'class' => "btn light-green darken-3 waves-light waves-effect export-workbook-actions",
        'id' => 'export_workbook-save-new-list-btn',
        'title'=>\Yii::t('app','Save as a new Trait List')
    ]
);
$actionResetTable = Html::a(
    '<i class="material-icons">refresh</i>',
    '#!',
    [
        'class' => 'btn grey lighten-5 grey-text text-darken-4 waves-light waves-effect',
        'id' => 'export_workbook-reset_table-btn',
        'title'=>\Yii::t('app',"Reload the $entity trait protocol")
    ]
);
$actionDeleteList = Html::a(
    '<i class="material-icons">delete</i>',
    '#!',
    [
        'class' => 'btn red darken-3 waves-light waves-effect export-workbook-actions',
        'id' => 'export_workbook-discard-btn',
        'title'=>\Yii::t('app','Remove item(s) from the list')
    ]
);
$actionGoToDataCollectionUpload = Html::a(
    '<i class="material-icons">file_upload</i>',
    Url::to(
        [
            '/dataCollection/upload',
            'program'=>$_GET['program'],
        ]
    ),
    [
        'class' => 'btn grey lighten-2 grey-text text-darken-4 waves-light waves-effect',
        'id' => 'export_workbook-data_collection_upload_link-btn',
        'title'=>\Yii::t('app','Upload your data collection files here'),
    ]
);

$panel = [
    'heading' => '<i class="material-icons left">folder_special</i> '.\Yii::t('app', 'Selected Traits'),
    'before' => \Yii::t('app', 'You may drag the row across the table to reorder the traits.'),
    'footer' => false
];

// variables browser
DynaGrid::begin([
    'options'=>['id'=>'data_collection-selected-vars-grid'],
    'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'id' => 'dcsvg-grid-options',
        'dataProvider'=>$dataProvider,
        'pjax'=>true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => 'dcsvg-grid-options',
                'enablePushState' => false,
            ],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'panel'=> $panel,
        'responsiveWrap'=>false,
        'toolbar' =>  [
            'content' => Html::a('', ['', "program" => $program, "locationId" => $locationId, 'fileName' => $fileName, 'occurrenceId' => $occurrenceId, 'crossListId' => $crossListId], ['data-pjax'=>true, 'id'=>'reset-trait-grid' ,'class' => 'hidden btn btn-default', 'title'=>\Yii::t('app','Reset grid')])."$actionGoToDataCollectionUpload $actionResetTable $actionExportTraitsForFieldbook $actionExportLocationForFieldbook $actionSaveList $actionDeleteList",
        ],
        'floatHeader' =>true,
        'floatOverflowContainer'=> true
    ]
]);
DynaGrid::end();

// modal for saving as new list
Modal::begin([
    'id' => 'export_workbook-save-list-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">add</i>'.Yii::t('app','Save as a new list').'</h4>',
    'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
        .Html::a('Submit',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light disabled',
                'url' => '#',
                'id' => 'export_workbook-save-list-confirm-btn'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);


echo '<p id="export-workbook-save-list-preview-error"></p>';

$form = ActiveForm::begin([
    'enableClientValidation'=>false,
    'id'=>'create-saved-list-form',
    'action'=>['/dataCollection/export-workbook/index'],
    'fieldConfig' => function($model,$attribute){
        if(in_array($attribute, ['abbrev','display_name','name','type'])){
            return ['options' => ['class' => 'input-field form-group col-md-6']];
        }else{
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    }
]); 

$model = \Yii::$container->get('app\models\PlatformList');
$listType = [
    'trait' => 'Trait',
];
$listTypeOptions = [
    'placeholder' => Yii::t('app','Select type (required)'),
    'id' => 'workbook-list-type',
    'class' => 'text-field-var',
    'value' => 'trait',
];
echo 
    $form->field($model, 'name')->textInput([
        'maxlength' => true, 
        'id'=>'workbook-list-name',
        'title'=>Yii::t('app','Name identifier of the list'),
        'autofocus' => 'autofocus',
        'oninput' => 'js:
            $("#workbook-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());
            $("#workbook-list-display_name").val(this.value);
            Materialize.updateTextFields();',
    ]).
    $form->field($model, 'abbrev')->textInput([
        'maxlength' => true, 
        'id'=>'workbook-list-abbrev',
        'title'=>Yii::t('app','Short name identifier or abbreviation of the list'),
        'oninput' => 'js: $("#workbook-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());',
    ]).
    $form->field($model, 'display_name')->textInput([
        'maxlength' => true,
        'id'=>'workbook-list-display_name', 
        'title'=>Yii::t('app','Name of the list to show to users'),
    ]).
    $form->field($model, 'type')->widget(Select2::class, [
        'data' => $listType,
        'options' => $listTypeOptions
    ])->label(Yii::t('app','Type'),['class'=>'control-label active', 'style' => 'margin-top:-5px!important']).
    $form->field($model, 'description')->textarea([
        'class'=>'materialize-textarea',
        'id'=>'workbook-list-description',
        'title'=>Yii::t('app','Description about the list'),
    ]).
    $form->field($model, 'remarks')->textarea([
        'class'=>'materialize-textarea',
        'title'=>Yii::t('app','Additional details about the list'),
        'row'=>2,
        'id'=>'workbook-list-remarks',
    ]); 

ActiveForm::end();

echo '<div class="export-workbook-modal-loading"></div>';
echo '<div class="export-workbook-save-list-preview-variables"></div>';

Modal::end();



$exportWorkbookUrl =  Url::to(['export-workbook/download','program'=>$program, 'locationId'=> $locationId]);

// modal for exporting workbook preview
Modal::begin([
    'id' => 'export_workbook-download-preview-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">file_download</i>'.Yii::t('app','Export workbook').'</h4>',
    'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
        .Html::a('Confirm',
            $exportWorkbookUrl,
            [
                'class' => 'btn btn-primary waves-effect waves-light',
                'url' => '#',
                'id' => 'export_workbook-download-confirm-btn'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);
echo '<p>'.\Yii::t('app','Below is the list of traits that will be exported in workbook. Click "Confirm" to proceed.').'</p>';
echo '<div class="export-workbook-modal-loading"></div>';
echo '<div class="export-workbook-save-list-preview-variables"></div>';
Modal::end();

// restore traits modal
Modal::begin([
    'id' => 'restore-preloaded_traits-modal',
    'header' => '<h4 id="restore-preloaded_traits-label"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Cancel'),
        ['#'],
        ['data-dismiss'=>'modal']).
        '&nbsp;&nbsp'.
        Html::a(\Yii::t('app','Confirm'),
        ['#'],
        [
            'class'=>'btn btn-primary waves-effect waves-light restore-preloaded_traits-btn',
            'url'=>'#',
            'id'=>'restore-btn'
        ]).'&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You have chosen to reload the ' .$entity .' trait protocol. Please note that this will discard any changes you have made in the Selected Traits list. Press the <b>Confirm</b> button to proceed with the operation. Otherwise, click the <b>Cancel</b> button.';
Modal::end();

$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

$reorderUrl = Url::to(['export-workbook/reorder-variables', 'entity' => $entity]);
$removeVariablesUrl = Url::to(['export-workbook/remove-variables', 'entity' => $entity]);
$retrieveVarListPreviewUrl = Url::to(['export-workbook/retrieve-variables-preview', 'entity' => $entity]);
$confirmSaveVariableListUrl = Url::to(['export-workbook/save-variable-list', 'entity' => $entity]);
$getListTagsUrl = Url::to(['export-workbook/get-list-tags']);
$loadSavedVariablesUrl = Url::to(['export-workbook/load-saved-variables', 'program' => $program]);
$getTraitProtocolsUrl = Url::to(['export-workbook/get-trait-protocols', 'program' => $program]);

$this->registerJs(<<<JS
    const HEIGHT_SCALE = 0.65
    const loadingIndicatorHtml = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>'
    const entity = '$entity'

    $(document).ready(function () {
        $(window).on('resize', function () {
            calculateMaxHeight()
        })

        function calculateMaxHeight () {
            $(".kv-grid-wrapper").css("height", $(window).height() * HEIGHT_SCALE)
        }

        // if User moved from one Location to another within the same window/tab, clear "running" session item
        if (
            sessionStorage.getItem('locationId') !== getUrlParameter('locationId') ||
            sessionStorage.getItem('occurrenceId') !== getUrlParameter('occurrenceId')
        )
        sessionStorage.removeItem('running')
        sessionStorage.setItem('locationId', getUrlParameter('locationId'))
        sessionStorage.setItem('occurrenceId', getUrlParameter('occurrenceId'))

        $('#data_collection-selected-vars-grid').ready(function () {
            $('.ui-sortable').ready(function () {
                // check for saved trait protocols
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '$getTraitProtocolsUrl',
                    data: {
                        locationId: getUrlParameter('locationId'),
                        occurrenceId: getUrlParameter('occurrenceId'),
                        crossListId: getUrlParameter('crossListId'),
                    },
                    async: false,
                    success: function (response) {
                        let message

                        if (response['list'].length === 0 && !sessionStorage.getItem('running')) {
                            $('#export_workbook-reset_table-btn').addClass('disabled') // Disable reset button
                            message = "<i class='material-icons blue-text'>info</i>&emsp;The " + response['entity'] + " currently has no saved trait protocol to preload traits from."

                            // display notification
                            $('.toast').css('display','none');
                            Materialize.toast(message, 5000)
                        } else {
                            $('#export_workbook-reset_table-btn').removeClass('disabled') // Activate reset button

                            if (!!$('.empty').length && !sessionStorage.getItem('running')) {
                                // preload traits
                                $.ajax({
                                    type: 'POST',
                                    dataType: 'json',
                                    url: '$loadSavedVariablesUrl',
                                    data: {
                                        locationId: getUrlParameter('locationId'),
                                        occurrenceId: getUrlParameter('occurrenceId'),
                                        crossListId: getUrlParameter('crossListId'),
                                    },
                                    async: false,
                                    success: function (data) {
                                        let message

                                        if (data.statusCode === 0) { // successfully saved traits (formerly "variables") into temporary table
                                            message = "<i class='material-icons green-text'>done</i>&emsp;Successfully preloaded all traits from the " + response['entity'] + " trait protocol!"
                                          // display notification
                                          $('.toast').css('display','none');
                                          Materialize.toast(message, 5000)
                                        }

                                        // reload Selected Traits panel
                                        $('#reset-trait-grid').trigger('click');
                                        
                                    },
                                    error: function (data) {
                                    }
                                })
                            }
                        }
                    },
                    error: function (data) {
                        console.log(data)
                    }
                })

                // prevent repeated preloading upon detecting empty table
                sessionStorage.setItem('running', '1')

                refresh()
                actionsSetState()
            })
        })

        // for retrieving values of current URL parameters
        function getUrlParameter(param) {
            let pageURL = window.location.search.substring(1),
                urlVariables = pageURL.split('&'),
                parameterName,
                i

            for (i = 0; i < urlVariables.length; i++) {
                parameterName = urlVariables[i].split('=')

                if (parameterName[0] === param) {
                    return parameterName[1] === undefined ? true : decodeURIComponent(parameterName[1])
                }
            }
        };

        $(document).on('ready pjax:success', function(e) {
            refresh()
            actionsSetState()
        })

        $(document).on('click', '.sorter a', function(e) {
            e.preventDefault();
            if($('.summary').text() !== ''){
                $('.toast').css('display','none');
                let notif = "<i class='material-icons green-text'>done</i> Successfully reordered traits!";

                Materialize.toast(notif,5000);
            }
        })

        // set state of action buttons
        function actionsSetState(){
            var varCount = document.getElementsByClassName("final-grid_select");
            if(varCount.length == 0){
                $('.export-workbook-actions').addClass('disabled');
            } else{
                $('.export-workbook-actions').removeClass('disabled');
            }
        }

        // refresh gridview
        function refresh(){
            // render button dropdown
            $('.dropdown-button').dropdown()
            calculateMaxHeight()

            $("#data_collection-selected-vars-grid tbody").sortable({
                tolerance: 'pointer',
                cursor: 'move',
            });

            // reorder traits
            $('#data_collection-selected-vars-grid tbody').on('sortupdate',function(event,ui){
                var reorderUrl = '$reorderUrl'
                var list = []
                var order = $(this).find('.order_tr').each(function(){
                    var id = $(this).attr("data-varId")
                    list.push(parseInt(id))
                })

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: reorderUrl,
                    data: {ids: list},
                    async: false,
                    success: function(data) {
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully reordered traits!"

                        Materialize.toast(notif,5000)
                    }
                })
            })

            // check row in traits browser
            $("#data_collection-selected-vars-grid tbody tr").on('click',function(e){

                this_row=$(this).find('input:checkbox')[0];
                if(this_row.checked){
                    this_row.checked=false;  
                    $('#export_workbook-variables-grid-select-all-id').prop("checked", false);
                    $(this).removeClass("grey lighten-4");
                }else{
                    $(this).addClass("grey lighten-4");
                    this_row.checked=true;  
                    $("#"+this_row.id).prop("checked");  
                }

            });

            // select all traits
            $('#export_workbook-variables-grid-select-all-id ').on('click',function(e){

                if($(this).prop("checked") === true)  {
                    $(this).attr('checked','checked');
                    $('.final-grid_select').attr('checked','checked');
                    $('.final-grid_select').prop('checked',true);
                    $(".final-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");

                }else{

                    $(this).removeAttr('checked');
                    $('.final-grid_select').prop('checked',false);
                    $('.final-grid_select').removeAttr('checked');
                    $("input:checkbox.final-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");      
                }

            });

            // remove trait
            $(document).on('click', '#export_workbook-discard-btn', function(e) {
                e.preventDefault()
                e.stopImmediatePropagation()
                
                var ids=[];
                var removeVariablesUrl = '$removeVariablesUrl';
                var selected = false;

                $("input:checkbox.final-grid_select").each(function(){

                    if($(this).prop("checked") === true)  {
                        ids.push($(this).attr("id"));
                        selected = true;
                    }

                });

                if(!selected){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> No trait(s) selected.";
                    Materialize.toast(notif,5000);
                }else{
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: removeVariablesUrl,
                        data: {ids: ids},
                        async: false,
                        success: function(data) {
                            $('.toast').addClass('hidden');
                            var lenDiscard = ids.length;
                            var idsString = ids.join(",");
                        
                            if(idsString != ''){
                                $('#reset-trait-grid').trigger('click');
                                $('.toast').css('display','none');
                                var notif = "<i class='material-icons green-text'>done</i> Successfully removed&nbsp;<b> " + lenDiscard + "</b>&nbsp;trait(s)!";
                                Materialize.toast(notif,5000);
                            }

                            return false;
                        }
                    });
                }

            });

            // view save new list modal
            $('#export_workbook-save-new-list-btn').on('click', function(e){
                $('#export-workbook-save-list-preview-error').html('');
                $('#export_workbook-save-list-modal').modal('show');

                $('.export-workbook-modal-loading').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
                $('.export-workbook-save-list-preview-variables').html('');

                var varIdsSavedList=[];
                var retrieveVarListPreviewUrl = '$retrieveVarListPreviewUrl';

                $("input:checkbox.final-grid_select").each(function(){

                    if($(this).prop("checked") === true)  {
                        varIdsSavedList.push($(this).attr("id"));
                    }

                });

                $("#workbook-list-abbrev").trigger("change");
                setTimeout(function(){
                    $.ajax({
                        type: 'POST',
                        url: retrieveVarListPreviewUrl,
                        data: {
                            ids: varIdsSavedList
                        },
                        async: false,
                        dataType: 'json',
                        success: function(response) {
                
                            $('.export-workbook-save-list-preview-variables').html(response.htmlData);

                            $('.export-workbook-modal-loading').html('');

                        }
                    });

                }, 500);
            });

            // export workbook modal
            $('#export_workbook-download-btn').on('click', function(e){
                $('#export_workbook-download-preview-modal').modal('show');

                $('.export-workbook-modal-loading').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
                $('.export-workbook-save-list-preview-variables').html('');

                var varIdsSavedList=[];
                var retrieveVarListPreviewUrl = '$retrieveVarListPreviewUrl';

                $("input:checkbox.final-grid_select").each(function(){

                    if($(this).prop("checked") === true)  {
                        varIdsSavedList.push($(this).attr("id"));
                    }

                });

                setTimeout(function(){
                    $.ajax({
                        type: 'POST',
                        url: retrieveVarListPreviewUrl,
                        data: {
                            ids: varIdsSavedList,
                        },
                        async: false,
                        dataType: 'json',
                        success: function(response) {
                
                            $('.export-workbook-save-list-preview-variables').html(response.htmlData);

                            $('.export-workbook-modal-loading').html('');

                        }
                    });

                }, 500);
            });

            // click confirm download button
            $('#export_workbook-download-confirm-btn').on('click', function(e){
                $('#system-loading-indicator').css('display','none');
                $('#export_workbook-download-preview-modal').modal('hide');
            })

            // validate if all required fields are specified
            $('.form-control').bind("change keyup input",function() {
                var abbrev = $('#workbook-list-abbrev').val();
                var name = $('#workbook-list-name').val();
                var display_name = $('#workbook-list-display_name').val();

                if(abbrev != '' && name != '' && display_name != ''){
                    $('#export_workbook-save-list-confirm-btn').removeClass('disabled');
                }else{
                    $('#export_workbook-save-list-confirm-btn').addClass('disabled');
                }
            });

            // confirm save trait protocol
            $('#export_workbook-save-list-confirm-btn').on('click', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                var confirmSaveVariableListUrl = '$confirmSaveVariableListUrl';
                var getListTagsUrl = '$getListTagsUrl';
                var varIdsSavedList=[];

                var abbrev = $('#workbook-list-abbrev').val();
                var name = $('#workbook-list-name').val();
                var displayName = $('#workbook-list-display_name').val();
                var description = $('#workbook-list-description').val();
                var remarks = $('#workbook-list-remarks').val();


                $("input:checkbox.final-grid_select").each(function(){
                    if($(this).prop("checked") === true)  {
                        varIdsSavedList.push($(this).attr("id"));
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: confirmSaveVariableListUrl,
                    data: {
                        ids: varIdsSavedList,
                        abbrev: abbrev,
                        name: name,
                        displayName: displayName,
                        description: description,
                        remarks: remarks
                    },
                    dataType: 'json',
                    async: false,
                    success: function(response) {
                        $('.toast').css('display','none');
                        
                        // with error
                        if(response.msg != ''){
                            
                            var notif = "<i class='material-icons red-text'>close</i> " + response.msg;

                        }else{
                            
                            //reset fields
                            $('#workbook-list-abbrev, #workbook-list-display_name, #workbook-list-name, #workbook-list-description, #workbook-list-remarks').val('');
                            var notif = "<i class='material-icons green-text'>done</i> Successfully saved&nbsp;<b>"+ displayName +"</b>&nbsp;trait protocol!";
                            
                            //get list tags
                            $.ajax({
                                url: getListTagsUrl,
                                type: 'post',
                                dataType: 'json',
                                async: true,
                                success: function(response) {

                                    option = response;
                                    var lookup = {};
                                    var list = document.getElementById('export_workbook-select-list');
                                    var listCount = Object.keys(option).length;
                                    var count = 0;

                                    $.each(option, function(key, value) {

                                        if(count < (listCount-1)){

                                            list.options[count]['value'] = key;
                                            list.options[count].innerHTML = value;
                                            count++;
                                        }
                                        else{

                                            var newListOption = document.createElement("option");
                                            newListOption.value = key;
                                            newListOption.innerHTML = value;
                                            list.options.add(newListOption);
                                        }
                                    });

                                    $('#export_workbook-select-list').val(null);
                                    $('#export_workbook-select-list').trigger('change');
                                    $('#export_workbook-save-list-modal').modal('hide');

                                }
                            });
                        }

                        Materialize.toast(notif,5000);

                    },
                    error: function() {
                        var notif = "<i class='material-icons red-text'>close</i> There seems to be a problem while saving the list. ";

                        Materialize.toast(notif,5000);

                        $('#export_workbook-save-list-confirm-btn').addClass('disabled');
                    }
                });
            });

            // display confirmation modal for restoring traits
            $('#export_workbook-reset_table-btn').on('click', function(e) {
                $('#restore-preloaded_traits-label').html('<i class="material-icons">refresh</i> Reload the ' + entity + ' Trait Protocol');
                $('#restore-preloaded_traits-modal').modal('show');
            })

            // reset table to load consolidated traits
            $('#restore-btn').on('click', function(e) {
                e.preventDefault()
                e.stopImmediatePropagation()

                $('#restore-preloaded_traits-modal').modal('hide')

                // replace all table contents with saved traits
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '$loadSavedVariablesUrl',
                    data: {
                        locationId: getUrlParameter('locationId'),
                        occurrenceId: getUrlParameter('occurrenceId'),
                        crossListId: getUrlParameter('crossListId'),
                    },
                    async: false,
                    success: function (data) {
                        let message

                        if (data.statusCode === 0) {
                            message = "<i class='material-icons green-text'>done</i>&emsp;Successfully restored the " + entity + " trait protocol!"
                        } else if (data.statusCode == 2) {
                            message = "<i class='material-icons red-text'>close</i>&emsp;There seems to be a problem with restoring the " + entity + " trait protocol."
                        }

                        // reload Selected Traits panel
                        $('#reset-trait-grid').trigger('click');
                        // display notification
                        $('.toast').css('display','none');
                        Materialize.toast(message, 5000)
                    },
                    error: function (data) {
                    }
                })
            })

            // click download field book trait button
            $('#export_trait-btn').off('click').on('click', function (e) { // .off('click') prevents click event from firing more than once
                let variableIds = []

                // display notification
                $('.toast').css('display','none');
                Materialize.toast("<i class='material-icons blue-text'>info</i>&emsp; Exporting traits for field book. Please wait.", 5000)

                // display loading indicator
                $('#system-loading-indicator').html(loadingIndicatorHtml)

                // get selected trait IDs
                $("input:checkbox.final-grid_select").each( function () {
                    if ($(this).prop("checked") === true) {
                        variableIds.push($(this).attr("id"))
                    }
                })

                $.ajax({
                    type: 'POST',
                    url: '$downloadFieldBookTraitUrl',
                    async: true,
                    dataType: 'json',
                    data: {
                        variableIds: variableIds
                    },
                    success: function (data) {
                        if(data.success == true){
                            let csvRows = data.csvRows.map(e => e.join(',')).join(`\r\n`) // format data
                            let csvContent = 'data:text/csv;charset=utf-8,' + csvRows // prepare file for download

                            let encodedUri = encodeURI(csvContent)
                            let link = document.createElement('a')

                            link.setAttribute('href', encodedUri)
                            link.setAttribute('download', data.filename)
                            document.body.appendChild(link) // required for triggering click

                            link.click(); // download CSV file
                        }
                        // display notification
                        $('.toast').css('display','none');
                        Materialize.toast(data.message, 5000)

                        // remove loading indicator
                        $('#system-loading-indicator').html('')
                    },
                    error: function (data) {
                    }
                });
            })

            // click download field book study button
            $('.export-for-field-book-btn').off('click').on('click', function (e) { // .off('click') prevents click event from firing more than once
                let variableIds = []
                let includeTraitAbbrevs = $(this).data()['includeTraitAbbrevs']
                let data = {}

                if (includeTraitAbbrevs) {
                    // get selected trait IDs
                    $("input:checkbox.final-grid_select").each( function () {
                        if ($(this).prop("checked") === true) {
                            variableIds.push($(this).attr("id"))
                        }
                    })

                    // cancel procedure if there are no selected Traits
                    if (!variableIds.length) {
                        Materialize.toast("<i class='material-icons orange-text'>warning</i>&emsp; Please select at least 1 Trait before proceeding.", 5000)
                    
                        return
                    }

                    data = {
                        variableIds: variableIds
                    }
                }

                // display notification
                $('.toast').css('display','none');
                Materialize.toast(`<i class='material-icons blue-text'>info</i>&emsp; Exporting \${entity} for field book. Please wait.`, 5000)

                // display loading indicator
                $('#system-loading-indicator').html(loadingIndicatorHtml)

                $.ajax({
                    type: 'POST',
                    url: '$downloadFieldBookLocationUrl',
                    async: true,
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        if(data.success == true){
                            // If background processing was triggered
                            if (data === 1) return

                            let csvRows = data.csvRows.map(e => e.join(',')).join(`\r\n`) // format data

                            let encodedCsvContent = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvRows) // prepare file for download (%EF%BB%BF added to be able to encode special characters)
                            let link = document.createElement('a')
                            link.setAttribute('href', encodedCsvContent)
                            link.setAttribute('download', data.filename)
                            document.body.appendChild(link) // required for triggering click

                            link.click(); // download CSV file
                        }
                        // display notification
                        $('.toast').css('display','none');
                        Materialize.toast(data.message, 5000)

                        // remove loading indicator
                        $('#system-loading-indicator').html('')
                    },
                    error: function (data) {
                    }
                });
            })

            //focus on name field when create list modal is displayed
            $('#export_workbook-save-list-modal').on('shown.bs.modal', function () {
                $('#workbook-list-name').focus();
            })
        }
    });
JS
);

$this->registerCss('
    .disabled {
        pointer-events:none;
        opacity:0.6;
    }

    .dropdown-content {
        overflow-y: visible;
    }

    .dropdown-content .dropdown-content {
        margin-left: -100%;
    }

    .non-link {
        cursor: default
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;        
    }
');
?>

<style>
.ui-sortable-handle {
    cursor: move !important;
}
.panel-default > .panel-heading {
    padding: 1px 0px;
}
.box-body {
    padding: 0px 0px 15px 0px !important;
}
.box{
    margin: 0px;
}
.panel-title{
    margin-top:10px;
}
.panel-heading .summary {
    margin-top: 0px;
}
#data_collection-selected-vars-grid{
    margin: 20px;
}
</style>