<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders add traits (formerly "variables") using traits in export workbook
 */
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use yii\helpers\Url;

echo '<label class="control-label" style="margin-top:15px">'.\Yii::t('app','Trait') .'</label>';
echo Select2::widget([
    'data' => $variableTags,
    'name' => 'export_workbook-select-variable',
    'id' => 'export_workbook-select-variable',
    'maintainOrder' => true,
    'options' => [
      'placeholder' => \Yii::t('app','Select a trait here'),
      'tags' => true
    ]
]);

$addVariablesUrl = Url::to(['export-workbook/add-variables', 'entity' => $entity]);
$getVariableTags = Url::to(['export-workbook/get-variable-tags']);

// replace traits with trait protocol modal
Modal::begin([
	'id' => 'replace-with-trait-modal',
	'header' => '<h4><i class="material-icons">swap_horiz</i> ' . \Yii::t('app', 'Replace All Traits') . '</h4>',
	'footer' =>
		Html::a(\Yii::t('app','Cancel'),
		['#'],
		['data-dismiss'=>'modal']).
		'&nbsp;&nbsp'.
		Html::a(\Yii::t('app','Confirm'),
		['#'],
		[
			'class'=>'btn btn-primary waves-effect waves-light replace-with-trait-btn',
			'url'=>'#',
			'id'=>'replace-with-trait-btn'
		]).'&nbsp;&nbsp',
	'options' => ['data-backdrop'=>'static']
]);
echo \Yii::t('app', '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You have chosen to replace the selected traits with <span id="trait-name"></span>. Please note that this will replace <b>ALL</b> the current traits in this '.$entity.'. Click the <b>Confirm</b> button to proceed with the operation. Otherwise, click the <b>Cancel</b> button.');
Modal::end();
?>
<div style="margin-top:10px">
  <button class="btn waves-effect waves-light add-replace-vars-variable-btn" title="<?= \Yii::t('app','Add traits') ?>" id="export_workbook-add-var-btn" data-operation="add" disabled style="margin: 0px 7px 0px 0px;"><?= \Yii::t('app','Add') ?></button>

  <button class="btn waves-effect waves-light grey lighten-3 grey-text text-darken-2 add-replace-vars-variable-btn" id="export_workbook-replace-var-btn" data-operation="replace" disabled style="margin: 0px 10px 0px 0px;" title="<?= \Yii::t('app','Replace all') ?>"><em class="material-icons">swap_horiz</em></button>

  <div class="hidden preloader-wrapper small active" style="width:20px;height:20px;">
      <div class="spinner-layer spinner-green-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
              <div class="circle"></div>
          </div>
      </div>
  </div>

</div>

<?php

$this->registerJs(<<<JS

    $(document).ready(function(){
       
        // check if there is selected variable
        $('#export_workbook-select-variable').bind("change keyup input",function() {
          var selectedVar = $('#export_workbook-select-variable').val();

          if(selectedVar == ''){
              $('#export_workbook-add-var-btn').attr('disabled',true);
              $('#export_workbook-replace-var-btn').attr('disabled',true);
          }else{
              $('#export_workbook-add-var-btn').attr('disabled',false);
              $('#export_workbook-replace-var-btn').attr('disabled',false);
          }
        });

        // add trait
        $('#export_workbook-add-var-btn').on('click', function(){
            var obj = $(this);
            var selectedVar = $('#export_workbook-select-variable').val();

            $('.preloader-wrapper').removeClass('hidden');
            $('.toast').css('display','none');

            $.ajax({
                url: '$addVariablesUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    type: 'variable',
                    id: selectedVar,
                    operation: 'add'
                },
                success: function (response) {

                    Materialize.toast(response['message'],5000);

                    $('.preloader-wrapper').addClass('hidden');

                    $('#reset-trait-grid').trigger('click');

                },
                error: function (data) {
                  var notif = "<i class='material-icons red-text'>close</i> There seems to be a problem while adding the trait.";

                  Materialize.toast(notif,5000);
                }
            });
        });

        // display confirmation modal for replacing traits with trait
        $('#export_workbook-replace-var-btn').on('click', function (e) {
            e.preventDefault()
            e.stopImmediatePropagation()
            let traitLabel = $('#select2-export_workbook-select-variable-container').attr('title')

            $('#trait-name').html('<b>' + traitLabel + '</b>')
            $('#replace-with-trait-modal').modal('show')
        })

        // replace traits
        $('#replace-with-trait-btn').on('click', function(e){
            e.preventDefault()
            e.stopImmediatePropagation()
            $('#replace-with-trait-modal').modal('hide')

            var obj = $('#export_workbook-replace-var-btn');
            var selectedVar = $('#export_workbook-select-variable').val();

            $('.preloader-wrapper').removeClass('hidden');
            $('.toast').css('display','none');

            $.ajax({
                url: '$addVariablesUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    type: 'variable',
                    id: selectedVar,
                    operation: 'replace'
                },
                success: function (response) {

                    Materialize.toast(response['message'],5000);

                    $('.preloader-wrapper').addClass('hidden');

                    $('#reset-trait-grid').trigger('click');

                },
                error: function (data) {
                  var notif = "<i class='material-icons red-text'>close</i> There seems to be a problem while replacing traits.";

                  Materialize.toast(notif,5000);
                }
            });
        });
    });

JS
);

?>