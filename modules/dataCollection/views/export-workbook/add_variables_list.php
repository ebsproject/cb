<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders add traits (formerly "variables") using saved list in export workbook
 */
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use yii\helpers\Url;

echo '<br/><p class="grey-text text-darken-2"> A Trait List is a collection of traits. You may select one that you have access to.</p>';

echo '<label class="control-label">'.\Yii::t('app','Trait List') .'</label>';
echo Select2::widget([
    'data' => $listTags,
    'name' => 'export_workbook-select-list',
    'id' => 'export_workbook-select-list',
    'maintainOrder' => true,
    'options' => [
        'placeholder' => \Yii::t('app','Select a trait list'),
        'tags' => true,
    ]
]);

$addVariablesUrl = Url::to(['export-workbook/add-variables', 'entity' => $entity]);

// replace traits with trait protocol modal
Modal::begin([
	'id' => 'replace-with-protocol-modal',
	'header' => '<h4><i class="material-icons">swap_horiz</i> ' . \Yii::t('app', 'Replace All Traits') . '</h4>',
	'footer' =>
		Html::a(\Yii::t('app','Cancel'),
		['#'],
		['data-dismiss'=>'modal']).
		'&nbsp;&nbsp'.
		Html::a(\Yii::t('app','Confirm'),
		['#'],
		[
			'class'=>'btn btn-primary waves-effect waves-light replace-with-protocol-btn',
			'url'=>'#',
			'id'=>'replace-with-protocol-btn'
		]).'&nbsp;&nbsp',
	'options' => ['data-backdrop'=>'static']
]);
echo \Yii::t('app', '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You have chosen to replace the selected traits with <span id="trait-protocol-name"></span>. Please note that this will replace <b>ALL</b> the current traits in this location. Click the <b>Confirm</b> button to proceed with the operation. Otherwise, click the <b>Cancel</b> button.');
Modal::end();
?>
<div style="margin-top:10px">
  <button class="btn waves-effect waves-light add-replace-vars-list-btn" title="<?= \Yii::t('app','Add traits') ?>" id="export_workbook-add-list-btn" data-operation="add" disabled style="margin: 0px 7px 0px 0px;"><?= \Yii::t('app','Add') ?></button>

  <button class="btn waves-effect waves-light grey lighten-3 grey-text text-darken-2 add-replace-vars-list-btn" id="export_workbook-replace-list-btn" data-operation="replace" disabled style="margin: 0px 10px 0px 0px;" title="<?= \Yii::t('app','Replace all') ?>"><em class="material-icons">swap_horiz</em></button>

  <div class="hidden preloader-wrapper small active" style="width:20px;height:20px;">
      <div class="spinner-layer spinner-green-only">
          <div class="circle-clipper left">
              <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
              <div class="circle"></div>
          </div>
      </div>
  </div>

</div>

<?php

$this->registerJs(<<<JS
    $(document).ready(function(){
        // check if there is selected list
        $('#export_workbook-select-list').bind("change keyup input",function() {
          var selectedList = $('#export_workbook-select-list').val();

          if(selectedList == ''){
              $('#export_workbook-add-list-btn').attr('disabled',true);
              $('#export_workbook-replace-list-btn').attr('disabled',true);
          }else{
              $('#export_workbook-add-list-btn').attr('disabled',false);
              $('#export_workbook-replace-list-btn').attr('disabled',false);
          }
        });

        // add traits from list
        $('#export_workbook-add-list-btn').on('click', function(){
            var obj = $(this);
            var selectedList = $('#export_workbook-select-list').val();

            $('.preloader-wrapper').removeClass('hidden');
            $('.toast').css('display','none');

            $.ajax({
                url: '$addVariablesUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    type: 'list',
                    id: selectedList,
                    operation: 'add'
                },
                success: function (response) {
                    Materialize.toast(response['message'],5000);

                    $('.preloader-wrapper').addClass('hidden');

                    $('#reset-trait-grid').trigger('click');

                },
                error: function (data) {
                  var notif = "<i class='material-icons red-text'>close</i> There seems to be a problem while adding traits. ";

                  Materialize.toast(notif,5000);
                }
            });
        });

        // display confirmation modal for replacing traits with trait protocol
        $('#export_workbook-replace-list-btn').on('click', function (e) {
            e.preventDefault()
            e.stopImmediatePropagation()
            let traitProtocolLabel = $('#select2-export_workbook-select-list-container').attr('title')

            $('#trait-protocol-name').html('<b>' + traitProtocolLabel + '</b>')
            $('#replace-with-protocol-modal').modal('show')
        })

        $('#replace-with-protocol-btn').on('click', function(e){
            e.preventDefault()
            e.stopImmediatePropagation()
            $('#replace-with-protocol-modal').modal('hide')

            var obj = $(this);
            var selectedList = $('#export_workbook-select-list').val();

            $('.preloader-wrapper').removeClass('hidden');
            $('.toast').css('display','none');

            $.ajax({
                url: '$addVariablesUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    type: 'list',
                    id: selectedList,
                    operation: 'replace'
                },
                success: function (response) {
                    Materialize.toast(response['message'],5000);

                    $('.preloader-wrapper').addClass('hidden');
                    $('#reset-trait-grid').trigger('click');

                },
                error: function (data) {
                  var notif = "<i class='material-icons red-text'>close</i> There seems to be a problem while replacing traits.";

                  Materialize.toast(notif,5000);
                }
            });
        });
    });

JS
);

?>