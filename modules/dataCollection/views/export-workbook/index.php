<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders selected variables
 */
use marekpetras\yii2ajaxboxwidget\Box;
use yii\helpers\Html;
use yii\helpers\Url;

// Check if there's an entry point
$source = $_GET['source'] ?? '';
$tempLabel = $label ?? '';

// Conditional statement for return hyperlink on title
// If entry point comes from main experiment manager browser
if($source == "em-occ"){
	$redirectURL = Url::to(['/occurrence','program'=>$program, 'OccurrenceSearch[occurrenceCode]'=> $label]);
	$tempLabel = Html::a($label, $redirectURL);
}
// If entry point comes from location under experiment manager
else if($source == "em-loc"){
	$redirectURL = Url::to(['/occurrence','program'=>$program, 'OccurrenceSearch[locationCode]'=> $label]);
	$tempLabel = Html::a($label, $redirectURL);
}
// If entry point comes from main cross manager browser
else if($source == "cm-main"){
	$redirectURL = Url::to(['/crossManager','program'=>$program, 'CrossListModel[entryListName]'=> $label]);
	$tempLabel = Html::a($label, $redirectURL);
}

// Display Title
echo '<h3>'.\Yii::t('app','Download Data Collection Files').' <small>» ' . $tempLabel . '</small></h3>';

?>

<!-- Add variables panel -->
<div class="col col-md-5">
	<div class="card" style="padding: 15px;padding-bottom:30px;">
		<div class="row">
			<div class="col col-md-12">
				<ul class="tabs tabs-fixed-width">
				    <li class="tab"><a href="#export-workbook-variable-tab" class="active fixed-color"><em class="material-icons widget-icons">folder</em> Traits</a></li>
				    <li class="tab"><a href="#export-workbook-list-tab" class="fixed-color"><em class="material-icons widget-icons">shopping_cart</em> Trait Lists</a></li>
			  	</ul>
				<div id="export-workbook-variable-tab">
					<?= Yii::$app->controller->renderPartial('add_variables_variable.php',[
						'variableTags'=>$variableTags,
						'program'=>$program,
						'entity'=>$entity,
					]);?>
				</div>
				<div id="export-workbook-list-tab">
					<?= Yii::$app->controller->renderPartial('add_variables_list.php',[
						'listTags'=>$listTags,
						'program'=>$program,
						'entity'=>$entity,
					]);?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Selected traits -->
<div class="col col-md-7">
	<?php
		$toolsButtons = [
			'reload' => function(){
				return '
					&nbsp;&nbsp;<a href="#!" title="'. \Yii::t('app', 'Refresh data').'" class="text-muted fixed-color export-workbook-refresh-vars" onclick="$(&quot;#data_collection-selected-variables&quot;).box(&quot;reload&quot;);"><i class="material-icons widget-icons">loop</i></a>
					';
			}
		];

		$options = [
			'id'=>'data_collection-selected-variables',
			'bodyLoad'=> ['/dataCollection/export-workbook/load-variables?program='.$program.'&locationId='.$locationId.'&occurrenceId='.$occurrenceId.'&crossListId='.$crossListId],
			'toolsTemplate' => '{others} {reload}',
			'toolsButtons' => $toolsButtons
		];

	?>
	<?= Box::widget($options); ?>
</div>

<style>
.box {
	position: relative;
	background: #ffffff;
	-webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2); 
	box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2); 
	border-top:none;
	margin: .5rem 0 1rem 0;
	border-radius: 2px;
	width: 100%;
}
.box-header.with-border {
	display:none; 
}
.box-header > .box-tools {
	top: 10px;
}
.box-header {
	padding: 15px;
}
.box-body {
	padding: 0px 15px 10px 15px;
	max-height: 800px;
	overflow-y: hidden;
	overflow-x: hidden;
}
.panel-default {
	border-color: transparent;
}
.panel {
	margin-bottom: -10px;
	background-color: transparent;
	border: 1px solid transparent;
}
.panel-default > .panel-heading {
	margin-bottom: -15px;
	color: #333;
	background-color: transparent;
	border-color: transparent;
}
.panel-footer {
	padding: 10px 0px;
	background-color: transparent;
}
.table {
	margin-bottom: 8px;
}
.summary{
	margin-top: -5px;
}
.material-icons.widget-icons{
	vertical-align: sub;
}
.tabs .tab a {
	padding: 0 20px;
}
</style>