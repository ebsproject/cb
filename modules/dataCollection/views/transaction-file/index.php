<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;

/**
 * Renders browser and manage Transactions page
 */
?>

<div class="transaction-index">
    <!-- Browse Transactions -->
    <?php
    // action columns
    $actionColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ActionColumn',
            'header' => 'Actions',
            'noWrap' => true,
            'template' => '{download}',
            'buttons' => [
                'download' => function ($url, $model, $key) use ($locationName) {

                    $downloadUrl = '';

                    if (str_contains($model['status'], 'cross')) {
                        $downloadUrl = '/dataCollection/transaction-file/download-cross-data/';
                    } else {
                        $downloadUrl = '/dataCollection/transaction-file/download/';
                    }
                    return Html::a(
                        '<i class="material-icons">file_download</i>',
                        Url::to([
                            $downloadUrl,
                            "id" => $model['transactionFileDbId'],
                            "locationName" => $locationName
                        ]),
                        [
                            'class' => 'download-transaction-file',
                            'title' => 'Download ' . $model['status'] . ' file',
                            'data-transaction_id' => $model['transactionFileDbId'],
                            'data-file_status' => $model['status'],
                            'data-pjax' => 0,

                        ]
                    );
                },
            ]
        ],
    ];

    // all the columns that can be viewed in Transactions browser
    $columns = [
        [
            'attribute' => 'status',
            'vAlign' => 'middle',
            'width' => '8%',
            'noWrap' => false,
            'format' => 'html',
            'value' => function ($model) {
                if (str_contains($model['status'], 'committed')) {
                    return '<span class="new badge green">' . $model['status'] . '</span>';
                } else if (str_contains($model['status'], 'uploaded')) {
                    return '<span class="new badge blue">' . $model['status'] . '</span>';
                } else if (str_contains($model['status'], 'invalid')) {
                    return '<span class="new badge red">' . $model['status'] . '</span>';
                } else if ($model['status'] == 'duplicate') {
                    return '<span class="new badge red">' . $model['status'] . '</span>';
                } else if (str_contains($model['status'], 'voided')) {
                    return '<span class="new badge grey">' . $model['status'] . '</span>';
                } else {
                    return '<span class="new badge brown">' . $model['status'] . '</span>';
                }
            }
        ],

        [
            'attribute' => 'creator',
            'header' => 'Creator',
            'filter' => false,
        ],

        [
            'attribute' => 'creationTimestamp',
            'visible' => true,
        ],
        [
            'attribute' => 'remarks',
            'visible' => true,
            'value' => function ($model) {
                if ($model['remarks'] == 'null') {
                    return null;
                }
            }
        ],

    ];
    $gridColumns = array_merge($actionColumns, $columns);

    //dynagrid configuration
    $dynagrid = DynaGrid::begin([
        'columns' => $gridColumns,
        'theme' => 'panel-default',
        'showPersonalize' => false,
        'storage' => DynaGrid::TYPE_SESSION,
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions' => [
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'showPageSummary' => false,
            'pjax' => true,
            'responsiveWrap' => false,
            'panel' => false,
            'toolbar' =>  false,
            'layout' => '{items}',
        ],
        'submitButtonOptions' => [
            'icon' => 'glyphicon glyphicon-ok',
        ],
        'deleteButtonOptions' => [
            'icon' => 'glyphicon glyphicon-bin',
            'label' => 'Remove',
        ],
        'options' => ['id' => 'dynagrid-transaction'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }
    DynaGrid::end();
    ?>
</div>