<?php
/* 
* This file is part of EBS-Core Breeding. 
* 
* EBS-Core Breeding is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* 
* EBS-Core Breeding is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
* GNU General Public License for more details. 
* 
* You should have received a copy of the GNU General Public License 
* along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

/**
 * Renders file upload transaction modal content
 */

use kartik\widgets\FileInput;
use yii\helpers\Url;

$this->registerCss('
	.kv-fileinput-caption .file-caption-name{
		height:unset;
	}
	');
echo '<div>';
echo FileInput::widget([
    'name' => 'upload_file',
    'options' => [
        'multiple' => false
    ],

    'pluginOptions' => [
        'uploadAsync' => false,
        'uploadUrl' => Url::to(["/dataCollection/upload/get?program=$program&uploadType=$uploadType"]),
        'allowedFileExtensions' => ['csv'],
        'browseClass' => 'btn waves-effect waves-light',
        'uploadClass' => 'btn',
        'removeClass' => 'btn btn-danger',
        'cancelClass' => 'btn grey lighten-2 black-text',
        'dropZoneTitle' => '<i class="large material-icons">cloud_upload</i><br>' . \Yii::t('app', 'Drag and drop a file here or click Browse'),
        'elCaptionContainer' => 'file-caption-name-custom',
        'overwriteInitial' => true,
        'maxFileCount' => 1,
        'autoReplace' => true,
        'disabledPreviewTypes' => ['text'],
        'fileActionSettings' => [
            'showUpload' => false,
            'showZoom' => false,
            'removeIcon' => '<i class="fa fa-trash"></i>',
            'showRotate' => false,
            'indicatorNew' => '<i class="fa fa-plus-circle text-warning"></i>',
            'indicatorSuccess' => '<i class="fa fa-check-circle text-success"></i>',
            'indicatorError' => '<i class="fa fa-exclamation-circle text-danger"></i>',
            'indicatorLoading' => '<i class="fa fa-hourglass-half text-muted"></i>',
            'indicatorPaused' => '<i class="fa fa-pause-circle text-info"></i>',
        ],
        'theme' => 'fa',

    ],
    'options' => ['accept' => 'csv/*', 'id' => 'file-input-id']
]);
echo '</div>';

$this->registerJs(
    "var url = '" . Url::to(['/dataCollection/terminal', "program" => $program]) . "';" .
        <<<JS
		var temp_table;
		var inputList;
		var program = '<?php echo $program; ?>';
		$('#file-input-id').on('filebatchuploadsuccess', function(event, data) {

			var form = data.form, files = data.files, extra = data.extra,
			response = data.response, reader = data.reader;

			if(response.success){
				url=url+"&TerminalTransaction[transactionDbId]="+ response.transactionId ;

				window.location = url;
			}

        });

        // hide button icons
        $('.fileinput-remove-button > i.fa.fa-trash').hide();
        $('.fileinput-cancel-button > i.fa.fa-ban').hide();
        $('.btn-file > i.fa.fa-folder-open').hide();
JS
);

