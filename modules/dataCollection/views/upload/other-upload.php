<?php
/* 
* This file is part of EBS-Core Breeding. 
* 
* EBS-Core Breeding is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* 
* EBS-Core Breeding is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
* GNU General Public License for more details. 
* 
* You should have received a copy of the GNU General Public License 
* along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;

yii\bootstrap\BootstrapPluginAsset::register($this);
/**
 * 
 * Renders Data Collection Other File Upload View
 */

// Set base URL
$baseUrl = Url::base(); 


$this->title = \Yii::t('app', 'Data Collection Upload: Other Files');

echo '<h3>' . $this->title . '</h3>' . Html::a(
    \Yii::t('app','Back'),
    Url::toRoute(
        ['/dataCollection/upload',
        'program' => $program]

    ),
    [
        'id' => 'upload-file-id',
        'class' => 'btn btn-default right',
        'title' => \Yii::t('app', 'Back'),
        'data-pjax' => true,
        'style' => 'margin-top:-30px'
    ]
);

// File upload modal
Modal::begin([
    'id' => 'dc-file-upload-modal',
    'header' => '<h4 id="dc-file-upload-modal-header"></h4>',
    'footer' => Html::a('Cancel', [''], [
            'id' => 'dc-file-upload-cancel-btn',
            'class' => 'modal-close-button',
            'title' => \Yii::t('app', 'Cancel'),
            'data-dismiss' => 'modal'
        ]) . '&emsp;&nbsp;',
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin'
    ],
    'closeButton' => ['class'=>'hidden'],
]);
echo '<div class="parent-panel" id="dc-file-upload-modal-body"></div>';
Modal::end();
?>
    <div class="row">
    <div class="col s12 m5">
      
<?
foreach ($interfaceList as $key => $value) {
    # code...

?>
    <div class="card-panel">
    <span><a class="other-file-upload" upload-type="<?=$value?>" href="#"><i class="left material-icons">file_upload</i></a><?=$value?>
    </span>
    </div>

<?
}
?>
    </div>
</div>

<?php
$renderFileUploadFormUrl = Url::to(['/dataCollection/upload/render-file-upload-form', 'program'=>$program]);

$this->registerJs(<<<JS

     var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';

    // Upload modal entry point
    $(document).on('click', '.other-file-upload', function(e) {
        e.preventDefault();
        e.stopPropagation();
        let uploadType = $(this).attr('upload-type')
        // Set modal display
        var modalHeader = '<h4><i class="material-icons" style="vertical-align:bottom">file_upload</i>' + uploadType + ' File Upload</h4>'
        $('#dc-file-upload-modal-header').html(modalHeader);
        
        $('#dc-file-upload-modal').modal('show');

        var modalBodyId = '#dc-file-upload-modal-body';

        $(modalBodyId).html(loading);
        
            $.ajax({
                url: '$renderFileUploadFormUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    uploadType: uploadType
                },
                success: function(response) {
                    $(modalBodyId).html(response);
                },
                error: function() {
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBodyId).html(errorMessage);
                }
            });
    });

JS);
?>

