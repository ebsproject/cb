<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dataCollection\models;

use Yii;

use app\dataproviders\ArrayDataProvider;

use app\models\TerminalTransaction;
use app\models\UserDashboardConfig;
use app\models\Config;

use yii\helpers\Url;

use kartik\dynagrid\DynaGrid;


/**
 * Model class for dynamic Plot data
 */
class PlotData extends \app\models\Plot
{
    private $dynamicFields = []; //observation variables of transaction
    private $dynamicRules = []; //observation variables of transaction
    public $plotColumns = [];   //columns of observation variables of transaction
    public $abbreviations = [];
    public $status; // observation status
    public $variables; // variables in the transaction
    public $transactionDbId;
    public $entityDbId;
    public $locationCode;
    public $occurrenceCode;
    public $plotCode;
    public $plotNumber;
    public $plotType;
    public $rep;
    public $designX;
    public $designY;
    public $paX;
    public $paY;
    public $fieldX;
    public $fieldY;
    public $germplasmName;
    public $germplasmCode;
    public $parentage;
    public $entryNumber;
    public $entryType;
    public $entryCode;
    public $entryName;
    public $transactionId;
    public $occurrenceName;
    public $locationName;

    /**
     *   Set all observation variable headers
     *   This overrides construct
     * @param $config array Array of transactionId integer Transaction ID from the data terminal and/or variable abbrev
     */
    public function __construct(
        public Transaction $transaction,
        public TerminalTransaction $terminalTransaction,
        $config=[])
    {
        if(!empty($config)){
            extract($config);

            $this->transactionId = $transactionId;
            $variables = $variables ?? [];
            $abbreviations = [];

            foreach ($variables as $variable) {
                if (is_array($variable) && isset($variable['abbrev'])) {
                    $abbreviations[] = $variable['abbrev'];
                }
            }

            $abbreviations = array_values($abbreviations);
            $this->abbreviations = $abbreviations;
            $this->getPlotColumns($variables);
            parent::__construct($config);
        }
    }

    /**
     * Returns the validation rules for attributes.
     * This method is overridden
     *
     * @return array validation rules
     */
    public function rules()
    {
        return [
            [[
                'transactionDbId', 'entityDbId', 'locationCode', 'occurrenceCode',
                'plotCode', 'plotNumber', 'plotType', 'rep', 'designX', 'designY',
                'paX', 'paY', 'fieldX', 'fieldY', 'germplasmName', 'germplasmCode',
                'parentage', 'entryNumber', 'entryType', 'entryCode', 'entryName',
                'occurrenceName', 'locationName',
            ], 'safe'],
            [$this->dynamicRules, 'safe']
        ];
    }

    /**
     * Render all visible columns in data browser
     * @param array $variables List of variables in the transaction
     * @return array Mandatory and observation columns
     */
    public function getPlotColumns($variables)
    {
        $attributes = [];
        $configModel = new Config();
        $params = Yii::$app->request->queryParams;
        $program = Yii::$app->userprogram->get('abbrev');
        $modifier = "_$program";
        $personModel = Yii::$container->get('app\models\Person');
        $plotSettings = $configModel->getconfigByAbbrev("DOWNLOAD_DATA_COLLECTION_FILES$modifier");
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);

        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $plotSettings = $configModel->getconfigByAbbrev('DOWNLOAD_DATA_COLLECTION_FILES_COLLABORATOR');
        } else if (isset($program) && !empty($program) && !empty($plotSettings)) {
            $plotSettings;
        } else { 
            $plotSettings = $configModel->getconfigByAbbrev('DOWNLOAD_DATA_COLLECTION_FILES_DEFAULT');
        }
        
        if(isset($plotSettings['DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS'])) {
            $configValue = $plotSettings['DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS'];

            foreach ($configValue as $value) {
                if (in_array(
                    $value['attribute'],
                    ['blockNumber', 'rowBlockNumber', 'colBlockNumber']
                )) {
                    continue;
                }
                $attributes[] = $value['attribute'];
            }
        }
        //Checkbox
        $this->plotColumns[] =
            [
                'label' => "checkbox",
                'visible' => true,
                'header' => '<input class="filled-in" type="checkbox" data-id-string="" id="select-all-plot-id" />
                    <label for="select-all-plot-id"></label>',
                'contentOptions' => ['class' => ''],
                'content' => function ($model) {
                    return '
                    <input class="select-plot-row filled-in" type="checkbox" id="' . $model['entityDbId'] . '" />
                    <label for="' . $model['entityDbId'] . '"></label>
                    ';
                },
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'hiddenFromExport' => true,
                'mergeHeader' => true,
                'order' =>  DynaGrid::ORDER_FIX_LEFT
            ];

        // Static columns(non-traits)
        foreach ($attributes as $key) {
            if ($key == 'parentage'){
                $this->plotColumns[] = [
                    'attribute' => $key,
                    'format' => 'raw',
                    'value' => function ($model) {
                        return "<div
                            title='{$model['parentage']}'
                            class='truncated'
                        > {$model['parentage']} </div>";
                    },
                ];
            } else if(strpos($key, 'DbId') === false ){
                $this->plotColumns[] = [
                    'attribute' => $key,
                    'visible' => true,
                    'format' => 'raw',
                    'filter' => true,
                    'hidden' => false,
                    'noWrap' => false,
                ];
            }
        }

        foreach ($variables as $attribute) {
            $variableParams = [
                "variableDbId" => "equals " . $attribute["variableDbId"]
            ];
            $variable = $this->transaction->getVariable($variableParams);
            $headers = ['class' => 'dataset-color-theme darken-1 white-text attrHeader',];
            $this->dynamicFields[$attribute['abbrev']] = null;
            $this->dynamicRules[] = $attribute['abbrev'];
            if (
                (isset($params['PlotData']['fields']) && $params['PlotData']['fields'] == $attribute['abbrev'])
                || !isset($params['PlotData']['fields'])

            ) {
                $this->plotColumns[] = [
                    'label' => $variable['label'],
                    'attribute' => $attribute['abbrev'],
                    'visible' => true,
                    'format' => 'raw',
                    'filter' => true,
                    'hidden' => false,
                    'noWrap' => true,
                    'headerOptions' => $headers,
                    'value' => function ($model) use ($attribute) {
                        if (isset($model[$attribute['abbrev']])){
                            $status = $model[$attribute['abbrev']]['status'];
                            $value = $model[$attribute['abbrev']]['value'];
                            $isSuppressed = $model[$attribute['abbrev']]['isSuppressed'];
                            $isVoid = $model[$attribute['abbrev']]['isVoid'];
                            $plotDataValue = $model[$attribute['abbrev']]['dataValue'] ?? 'NA';
                            $plotDataQCCode = $model[$attribute['abbrev']]['dataQCCode'];
                            $statusColor = 'black'; // for traits with null status
                            if ($isVoid) {
                                $statusColor = 'black white-text';
                            } elseif ($isSuppressed) {
                                $statusColor = 'grey white-text';
                            } elseif ($status == 'new') {
                                $statusColor = 'green';
                            } elseif ($status == 'invalid') {
                                $statusColor = 'red';
                            } elseif ($status == 'missing') {
                                $statusColor = 'blue darken-2 white-text';
                            } elseif ($status == 'missing;updated') {
                                $statusColor = 'amber blue-text text-darken-2';
                            }elseif ($status == 'updated') {
                                $statusColor = 'amber grey-text text-darken-2';
                            }elseif ($status == 'invalid;updated') {
                                $statusColor = 'amber red-text text-darken-2';
                            }
                            if (str_contains($status, 'updated')) {
                                $plotData = '';
                                if ($plotDataQCCode == 'S') {
                                    $plotData = '<span class=" badge blue darken-4 text-center white-text"><strong>' . $plotDataValue . '</strong></span>';
                                } else {
                                    $plotData = '<span style="background-color: transparent !important;" class="  text-center black-text"><strong>' . $plotDataValue . '</strong></span>';
                                }
                                if ($isVoid) {
                                    $statusColor = 'black white-text';
                                }
                                if ($value == '') {
                                    $value = 'NULL';
                                }
                                return '<span class=" badge ' . $statusColor . ' text-center "><strong>' . $value . '</strong></span>' .
                                    ' &nbsp;' . $plotData;
                            } elseif ($value != '') {
                                return '<span class=" badge ' . $statusColor . ' text-center white-text"><strong>' . $value . '</strong></span>';
                            } elseif ($isVoid) {
                                if ($value == '') {
                                    $value = 'NULL';
                                }
                                $statusColor = 'black white-text';
                                return '<span class=" badge ' . $statusColor . ' text-center white-text"><strong>' . $value . '</strong></span>';
                            }
                        }
                        return '';
                    }
                ];
            }
        }
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes are the dynamic fields
     *
     * @param string $name property name
     * @return mixed property value
     * @see getAttribute()
     */
    public function __get($name)
    {

        if (array_key_exists($name, $this->dynamicFields))
            return $this->dynamicFields[$name];
        else
            return parent::__get($name);
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes are the dynamic fields
     *
     * @param string $name property name
     * @param mixed $value property value
     */
    public function __set($name, $value)
    {

        if (array_key_exists($name, $this->dynamicFields))
            $this->dynamicFields[$name] = $value;
        else
            parent::__set($name, $value);
    }

    /**
     * Search functionality for data browser
     * 
     * @param Array $params contains the filter parameters
     * @return ArrayDataProvider the data provider for the browser
     */
    public function search($params = null)
    {
        $dataProviderId = 'terminal-dp-' . $this->transactionId;
        $this->load($params);


        //build filters

        if (isset($params['PlotData'])) {
            $filters = [];
            foreach ($params['PlotData'] as $key => $value) {
                if (isset($value)) {
                    
                    if (
                        in_array($key, [
                            'locationCode', 'occurrenceCode',
                            'plotCode', 'plotType' , 'germplasmName',
                            'germplasmCode', 'parentage', 'entryType', 
                            'entryCode', 'entryName', 'occurrenceName', 
                            'locationName',
                        ])

                    ) {

                        if (trim($value) !== '') {
                            $filters[$key] = "%" . trim($value) . "%";
                        }
                    } elseif (
                        in_array($key, [
                            'plotNumber', 'rep', 'designX', 'designY',
                            'paX', 'paY', 'fieldX', 'fieldY', 'entryNumber',
                        ])

                    ) {
                        if (trim($value) !== '') {
                            $filters[$key] = "equals " . trim($value);
                        }
                    } elseif ($key == 'status') {
                        if ($value == 'isVoid') {
                            $filters['isVoid'] = "true";
                        } elseif ($value == 'suppressed') {
                            $filters['isSuppressed'] = "true";
                        } elseif ($value == 'invalid') {
                            $filters['status'] = 'invalid|invalid;updated';
                        } elseif ($value == 'missing') {
                            $filters['status'] = 'missing|missing;updated';
                        } elseif ($value == 'updated') {
                            $filters['status'] = 'updated|invalid;updated|missing;updated';
                        } else {
                            $filters[$key] = trim($value);
                        }
                    } else {

                        if (is_array($value)) {
                            foreach ($value as $recordKey => $recordValue) {
                                if ($recordKey == 'value') {
                                    $key = rtrim($key, '_data');
                                    foreach ($recordValue as $keyValue) {
                                        if ($keyValue !== '') {

                                            $this->{$key} = $keyValue;
                                        }
                                    }
                                    $filters[$key][$recordKey] = $recordValue;
                                } elseif (
                                    $recordKey == 'isSuppressed'
                                    || $recordKey == 'isVoid'
                                    || $recordKey == 'status'
                                ) {
                                    if ($recordValue !== '') {
                                        $key = rtrim($key, '_data');
                                        $filters[$key][$recordKey] = $recordValue;
                                        $this->{$key} = null;
                                    }
                                }
                            }
                        } elseif (trim($value) !== '' && $key !== 'fields') {
                            $filters[$key]['value'] = trim($value);
                        }
                    }
                }
            }
        }

        $paramSort = '';
        if (isset($_GET["$dataProviderId-sort"])) {
            $paramSort = '&sort=';
            $sortParams = $_GET["$dataProviderId-sort"];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
            $tempOrder = ['orderNumber' => SORT_ASC];
            $tempOrder = array_merge($tempOrder, $sortOrder);
        }
        
        $paramPage = '';
        $page = 1;
        if (isset($_GET['terminal-dp-' . $this->transactionId . '-page'])) {
            $paramPage = '&page=' . $_GET['terminal-dp-' . $this->transactionId . '-page'];
            $page = $_GET['terminal-dp-' . $this->transactionId . '-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        }

        $paramLimit = 'limit=' . UserDashboardConfig::getDefaultPageSizePreferences();

        $filters = empty($filters) ? null : $filters;
        $url = 'terminal-transactions/' . $this->transactionId . '/datasets-table-search?';

        $response = Yii::$app->api->getResponse('POST', $url . $paramLimit . $paramPage . $paramSort, json_encode($filters));

        $paramSortOriginal = $paramSort;
        $output = $response;
        $data = $output['body']['result']['data'];

        $sortAttributes = [
            'locationCode', 'occurrenceCode', 'plotCode', 'plotNumber', 'plotType',
            'rep', 'designX', 'designY', 'paX', 'paY', 'fieldX', 'fieldY', 'germplasmName',
            'germplasmCode', 'parentage', 'entryNumber', 'entryType', 'entryCode', 'entryName',
            'occurrenceName', 'locationName',
            'orderNumber' => [
                "asc" => ["orderNumber" => SORT_ASC],
                "desc" => ["orderNumber" => SORT_DESC]
            ]
        ];
        $sortAttributes = array_merge($sortAttributes, $this->abbreviations);
        $dataProvider = new ArrayDataProvider([
            'id' => 'terminal-dp-' . $this->transactionId,
            'allModels' => $data,
            'key' => 'entityDbId',
            'restified' => true,
            'totalCount' => $output['body']['metadata']['pagination']['totalCount'],
            'sort' => [
                'attributes' => $sortAttributes,
                'defaultOrder' => [
                    'orderNumber' => SORT_ASC
                ]
            ]
        ]);

        return [
            'dataProvider' => $dataProvider,
            'pageCount' => $output['body']['metadata']['pagination']['totalPages'],
            'page' => $page,
            'paramSortOriginal' => $paramSortOriginal
        ];
    }

    /**
     * Return filter tags in formatted HTML chips
     * @param integer $transactionId Transaction identifier
     * @return string filter HTML chips
     */
    public function getFilterChips($transactionId)
    {
        $params = Yii::$app->request->queryParams;
        $chips = '';

        if (isset($params['PlotData'])) {
            $chipTags = [];

            foreach ($params['PlotData'] as $key => $entry) {

                $label = '';
                $filter = $params;

                $color = 'amber';
                if ($key === 'fields' && trim($entry) !== '') {
                    $abbrev = strtoupper($filter['PlotData'][$key]);
                    $param = [
                        "abbrev" => $abbrev
                    ];
                    $variable = $this->transaction->getVariable($param);
                    $color = 'dataset-color-theme lighten-4';
                    $label = $variable['label'] ?? $abbrev;
                    unset($filter['PlotData']['fields']);
                } else if ($key === 'status') {
                    $label = 'Status = ' . ucwords($entry);
                    unset($filter['PlotData']['status']);
                } else {
                    $col = rtrim($key, '_data');

                    $param = [
                        "abbrev" => "" . strtoupper($col)
                    ];
                    $variable = $this->transaction->getVariable($param);

                    if (isset($filter['PlotData'][$key]['status'])) {
                        $label = $variable['label'] . ' = ' . $filter['PlotData'][$key]['status'];
                        unset($filter['PlotData'][$key]['status']);
                    } else if (isset($filter['PlotData'][$key]['isSuppressed'])) {
                        $label = $variable['label'] . ' = Suppressed';
                        unset($filter['PlotData'][$key]['isSuppressed']);
                    } else if (isset($filter['PlotData'][$key]['isVoid'])) {
                        $label = $variable['label'] . ' = Voided';
                        unset($filter['PlotData'][$key]['isVoid']);
                    } else if ($filter['PlotData'][$key] !== '') {
                        $keyLabel = $key;
                        if ($key == 'plotNumber') {
                            $keyLabel = 'Plot Number';
                        } else if ($key == 'occurrenceCode') {
                            $keyLabel = 'Occurrence Code';
                        } else if ($key == 'locationCode') {
                            $keyLabel = 'Location Code';
                        } else if ($key == 'designX') {
                            $keyLabel = 'Design X';
                        } else if ($key == 'designY') {
                            $keyLabel = 'Design Y';
                        } else if ($key == 'paX') {
                            $keyLabel = 'Pa X';
                        } else if ($key == 'paY') {
                            $keyLabel = 'Pa Y';
                        } else if ($key == 'fieldX') {
                            $keyLabel = 'Field X';
                        } else if ($key == 'fieldY') {
                            $keyLabel = 'Field Y';
                        } else if ($key == 'germplasmName') {
                            $keyLabel = 'Germplasm Name';
                        } else if ($key == 'germplasmCode') {
                            $keyLabel = 'Germplasm Code';
                        } else if ($key == 'parentage') {
                            $keyLabel = 'Parentage';
                        } else if ($key == 'entryNumber') {
                            $keyLabel = 'Entry Number';
                        } else if ($key == 'entryType') {
                            $keyLabel = 'Entry Type';
                        }  else if ($key == 'plotCode') {
                            $keyLabel = 'Plot Code';
                        } else if ($key == 'plotType') {
                            $keyLabel = 'Plot Type';
                        } else if ($key == 'entryCode') {
                            $keyLabel = 'Entry Code';
                        } else if ($key == 'entryName') {
                            $keyLabel = 'Entry Name';
                        } else if ($key == 'occurrenceName') {
                            $keyLabel = 'Occurrence Name';
                        } else if ($key == 'locationName') {
                            $keyLabel = 'Location Name';
                        }

                        if (is_array($filter['PlotData'][$key])) {
                            $value = $filter['PlotData'][$key]['value']['equals'];
                        } else {
                            $value = $filter['PlotData'][$key];
                        }
                        $label = (isset($variable['label']) ? $variable['label'] : $keyLabel) . ' = ' . $value;
                        unset($filter['PlotData'][$key]);
                    }
                }
                if ($label !== '') {
                    $dataUrl = Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $filter));
                    $chipTags[] = '<div class="chip ' . $color . '  lighten-4 z-depth-1" tabindex="0" >' . $label . '<i data-data_level="plot" data-variable_abbrev="' . $key . '" data-url="' . $dataUrl . '" class="close-chip material-icons close">close</i></div>';
                }
            }

            if (!empty($chipTags)) {

                $chips = '<a id="plot-remove-all-filters" class="waves-effect grey btn " style="text-decoration: none; color: #fff; padding: 0px 9px; height: 25px; line-height: 25px; text-transform: none; margin-left: 10px;">' . \Yii::t('app', 'Remove filters') . '</a>'
                    . '<div class="chips" style=" display: inline; border-bottom: unset;margin-left: 10px;">'
                    . implode('', $chipTags) . '</div>';
            }
        }
        return $chips;
    }

    /**
     * Return status tags in formatted HTML buttons
     * @param array $variables List of variables in the transaction
     * @return string filter HTML buttons
     */
    public function getStatusTags($variables)
    {
        $legend = '<a class="btn-flat " style="cursor: default;text-decoration: none; background-color: '
            . 'transparent !important; padding: 0px 9px; height: 20px; line-height: 20px; text-transform: none; '
            . 'margin-left: 10px;font-size: .8rem;font-weight: bold;    color: #9e9e9e;">' . \Yii::t('app', 'Status') . '</a>';
        $newCount = 0;
        $invalidCount = 0;
        $missingCount = 0;
        $updatedCount = 0;
        $voidedCount = 0;

        $operationalSuppressedCount = 0;
        $suppressedCount = 0;
        foreach ($variables as $record) {
            $newCount += intval($record["newCount"]);
            $updatedCount += intval($record["updatedCount"]);
            $voidedCount += intval($record["voidedCount"]);
            $invalidCount += intval($record["invalidCount"]);
            $missingCount += intval($record["missingCount"]);
            
            $suppressedCount += intval($record["suppressedCount"]);
            $operationalSuppressedCount += intval($record["operationalSuppressedCount"]);
        }

        $statusArr = [];
        if ($newCount > 0) {
            $statusArr[] = '<span data-status="new" style="cursor: pointer;" class=" plot-status-btn btn waves-effect btn-flat green darken-3  "' .
                ' data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'No value in the experiment') . '">' . \Yii::t('app', 'NEW') . '</span>';
        }
        if ($invalidCount > 0) {
            $statusArr[] = '<span data-status="invalid" style="cursor: pointer; background-color:#c62828 !important;" '
                . 'class=" plot-status-btn btn waves-effect grey btn-flat red darken-3 " data-placement="top" data-title="tooltip" title="Data is erroneous or not accepted">'
                . \Yii::t('app', 'INVALID') . '</span>';
        }
        if ($missingCount > 0) {
            $statusArr[] = '<span data-status="missing" style="cursor: pointer; background-color:#1976d2 !important;" '
                . 'class=" plot-status-btn btn waves-effect grey btn-flat blue darken-3 " data-placement="top" data-title="tooltip" title="Data is missing">'
                . \Yii::t('app', 'MISSING') . '</span>';
        }
        if ($updatedCount > 0) {
            $statusArr[] = '<span data-status="updated" style="cursor: pointer; background-color: #ffc107 !important;" '
                . 'class=" plot-status-btn btn waves-effect btn-flat grey-text text-darken-2" data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'Value that will update the experiment value') . '">' . \Yii::t('app', 'UPDATED') . '</span>';
        }

        if ($voidedCount > 0) {
            $statusArr[] = '<span data-status="isVoid" style="cursor: pointer; background-color:black !important;" class=" plot-status-btn btn waves-effect btn-flat  " '
                . 'data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'Experiment value that will be removed') . '">'
                . \Yii::t('app', 'VOIDED') . '</span>';
        }
        if ($suppressedCount > 0) {
            $statusArr[] = '<span data-status="suppressed" style="cursor: pointer;" '
                . 'class=" plot-status-btn btn waves-effect btn-flat grey" data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'Valid value but not useful in analysis') . '">' . \Yii::t('app', 'SUPPRESSED') . '</span>';
        }

        $experimentLegend = '';
        if ($updatedCount > 0) {

            $experimentLegend = '
            <span class="btn-flat" style="cursor: default;text-decoration: none; background-color: '
                . 'transparent !important; padding: 0px 9px; height: 20px; line-height: 20px; '
                . 'text-transform: none; margin-left: 10px;font-size: .8rem;font-weight: bold;color: #9e9e9e;">'
                . \Yii::t('app', 'Legend') . ':</span>
            <span style="cursor: default;text-decoration: none; color: black; padding: 0px 9px; '
                . 'height: 25px; line-height: 25px; text-transform: none; background-color: '
                . 'transparent !important;"
            class="btn-flat blue  " data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'Current experiment value') . '" >' . \Yii::t('app', 'EXPERIMENT VALUE') . '</span>';

            if ($operationalSuppressedCount > 0) {
                $experimentLegend = $experimentLegend . '<span class="btn-flat" style="background-color: #1565c0 !important;cursor: default;text-decoration: none; color: #fff; padding: 0px 9px; '
                    . 'height: 25px; line-height: 25px; text-transform: none; margin-left: 5px;" '
                    . ' data-placement="top" data-title="tooltip" title="'
                    . \Yii::t('app', 'Current experiment value but suppressed') . '">' . \Yii::t('app', 'SUPPRESSED EXPERIMENT VALUE') . '</span>';
            }
        }
        return  '
        <span class="pull-left">
            ' . $legend . '&nbsp;
        </span>
        <div class="legend-items">
         ' . implode(' 
         ', $statusArr) . $experimentLegend . '
        </div> ';
    }
}
