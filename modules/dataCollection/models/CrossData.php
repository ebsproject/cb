<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dataCollection\models;

use Yii;
use yii\data\Sort;
use app\dataproviders\ArrayDataProvider;
use app\models\TerminalTransaction;
use yii\helpers\Url;
use app\interfaces\models\IUserDashboardConfig;
use app\models\Config;

/**
 * Model class for dynamic Cross data
 */
class CrossData extends \app\models\Cross
{
    public $transactionDbId;

    public $transactionId, $crossDbId, $crossName, $crossMethod, $germplasmDesignation,
        $germplasmParentage, $germplasmGeneration, $crossFemaleParent,
        $femaleParentage, $femaleParentSeedSource, $crossMaleParent,
        $maleParentage, $maleParentSeedSource, $femaleSourceEntryDbId,
        $femaleSourceEntry, $femaleSourceSeedName, $femaleSourcePlotDbId,
        $femaleSourcePlot, $maleSourceEntryDbId, $maleSourceEntry,
        $maleSourceSeedName, $maleSourcePlotDbId, $maleSourcePlot,
        $femaleParentEntryNumber, $maleParentEntryNumber, $femaleOccurrenceName,
        $maleOccurrenceName, $femaleOccurrenceCode, $maleOccurrenceCode, $crossRemarks;


    private $dynamicFields = []; //observation variables of transaction
    private $dynamicRules = []; //observation variables of transaction
    public $crossColumns = [];   //columns of observation variables of transaction
    public $status; // observation status
    public $variables; // variables in the transaction

    /**
     *   Set all observation variable headers
     *   This overrides construct
     * @param $config array Array of transactionId integer Transaction ID from the data terminal and/or variable abbrev
     */
    public function __construct(
        public Transaction $transaction,
        public TerminalTransaction $terminalTransaction,
        public IUserDashboardConfig $userDashboardConfig,
        $config=[]
        )
    {
        if(!empty($config)){
            extract($config);

            $this->transactionId = $transactionId;
            $variables = $variables ?? [];
            $this->getCrossColumns($variables);
            parent::__construct($config);
        }
        
    }

    /**
     * Returns the validation rules for attributes.
     * This method is overridden
     * 
     * @return array validation rules
     */
    public function rules()
    {
        return [
            [[
                'transactionDbId', 'entityDbId', 'crossDbId', 'crossName', 'crossMethod', 'germplasmDesignation',
                'germplasmParentage', 'germplasmGeneration', 'crossFemaleParent',
                'femaleParentage', 'femaleParentSeedSource', 'crossMaleParent',
                'maleParentage', 'maleParentSeedSource', 'femaleSourceEntryDbId',
                'femaleSourceEntry', 'femaleSourceSeedName', 'femaleSourcePlotDbId',
                'femaleSourcePlot', 'maleSourceEntryDbId', 'maleSourceEntry',
                'maleSourceSeedName', 'maleSourcePlotDbId', 'maleSourcePlot',
                'femaleParentEntryNumber', 'maleParentEntryNumber', 'femaleOccurrenceName',
                'maleOccurrenceName', 'femaleOccurrenceCode', 'maleOccurrenceCode', 'crossRemarks'
            ], 'safe'],
            [$this->dynamicRules, 'safe']
        ];
    }

    /**
     * Render all visible columns in data browser
     * @param array $variables List of variables in the transaction
     * @return array Mandatory and observation columns
     */
    public function getCrossColumns($variables)
    {
        $crossAttributes = [];
        $configModel = new Config();

        $params = Yii::$app->request->queryParams;
        $program = Yii::$app->userprogram->get('abbrev');
        $modifier = "_$program";
        $abbrev = "_CROSS_SETTINGS";
        $crossSettings = $configModel->getconfigByAbbrev("DC$modifier$abbrev");
        $personModel = Yii::$container->get('app\models\Person');
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);

        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $crossSettings = $configModel->getconfigByAbbrev('DC_COLLABORATOR_CROSS_SETTINGS');
        } else if (isset($program) && !empty($program) && !empty($crossSettings)) {
            $crossSettings;
        } else { 
            $crossSettings = $configModel->getconfigByAbbrev('DC_GLOBAL_CROSS_SETTINGS');
        }

        if(isset($crossSettings['Export']) && !empty($crossSettings['Export'])){
            $export = $crossSettings['Export'];
            
            foreach ($export as $value) {
                if(isset($value['visible']) && $value['visible'] == true) {
                    $crossAttributes[] = $value['attribute'];
                }
            }       
        }
        foreach ($crossAttributes as $key) {
            if ($key == 'crossName') {
                $keyLabel = 'Cross Name';
            } else if ($key == 'crossMethod') {
                $keyLabel = 'Cross Method';
            } else if ($key == 'germplasmDesignation') {
                $keyLabel = 'Designation';
            } else if ($key == 'germplasmParentage') {
                $keyLabel = 'Parentage';
            } else if ($key == 'germplasmGeneration') {
                $keyLabel = 'Generation';
            } else if ($key == 'crossFemaleParent') {
                $keyLabel = 'Female Parent';
            } else if ($key == 'femaleParentage') {
                $keyLabel = 'Female Parentage';
            } else if ($key == 'femaleParentSeedSource') {
                $keyLabel = 'Female Parent Seed Source';
            } else if ($key == 'crossMaleParent') {
                $keyLabel = 'Male Parent';
            } else if ($key == 'maleParentage') {
                $keyLabel = 'Male Parentage';
            } else if ($key == 'maleParentSeedSource') {
                $keyLabel = 'Male Parent Seed Source';
            } else if ($key == 'femaleSourceEntryDbId') {
                $keyLabel = 'Female Parent Source Entry ID';
            } else if ($key == 'femaleSourceEntry') {
                $keyLabel = 'Female Source Entry';
            } else if ($key == 'femaleSourceSeedName') {
                $keyLabel = 'Female Source Seed Name';
            } else if ($key == 'femaleSourcePlotDbId') {
                $keyLabel = 'Female Source Plot ID';
            } else if ($key == 'femaleSourcePlot') {
                $keyLabel = 'Female Source Plot';
            } else if ($key == 'maleSourceEntryDbId') {
                $keyLabel = 'Male Source Entry ID';
            } else if ($key == 'maleSourceEntry') {
                $keyLabel = 'Male Source Entry';
            } else if ($key == 'maleSourceSeedName') {
                $keyLabel = 'Male Source Seed Name';
            } else if ($key == 'maleSourcePlotDbId') {
                $keyLabel = 'Male Source Plot ID';
            } else if ($key == 'maleSourcePlot') {
                $keyLabel = 'Male Source Plot';
            } else if ($key == 'femaleParentEntryNumber') {
                $keyLabel = 'Female Entry Number';
            } else if ($key == 'maleParentEntryNumber') {
                $keyLabel = 'Male Entry Number';
            } else if ($key == 'femaleOccurrenceName') {
                $keyLabel = 'Female Occurrence';
            } else if ($key == 'maleOccurrenceName') {
                $keyLabel = 'Male Occurrence';
            } else if ($key == 'femaleOccurrenceCode') {
                $keyLabel = 'Female Occurrence Code';
            } else if ($key == 'maleOccurrenceCode') {
                $keyLabel = 'Male Occurrence Code';
            } else if ($key == 'crossRemarks') {
                $keyLabel = 'Cross Remarks';
            } else {
                $keyLabel = $key;
            }
            if(strpos($keyLabel, 'DbId') === false) {
                $this->crossColumns[] = [
                    'label' => $keyLabel,
                    'attribute' => $key,
                    'visible' => true,
                    'format' => 'raw',
                    'filter' => true,
                    'hidden' => false,
                    'noWrap' => true,
                ];
            }
        }

        foreach ($variables as $attribute) {
            $variableParams = [
                "variableDbId" => "" . $attribute["variableDbId"]
            ];
            $variable = $this->transaction->getVariable($variableParams);
            $headers = ["class" => 'dataset-color-theme darken-1 white-text attrHeader',];
            $this->dynamicFields[$attribute['abbrev']] = null;
            $this->dynamicRules[] = $attribute['abbrev'];
            if (
                (isset($params['CrossData']['fields']) && $params['CrossData']['fields'] == $attribute['abbrev'])
                || !isset($params['CrossData']['fields'])

            ) {
                $this->crossColumns[] = [
                    'label' => $variable['label'],
                    'attribute' => $attribute['abbrev'],
                    'visible' => true,
                    'format' => 'raw',
                    'filter' => true,
                    'hidden' => false,
                    'noWrap' => true,
                    'headerOptions' => $headers,
                    'value' => function ($model) use ($attribute) {
                        if(isset($model[$attribute['abbrev']])){
                            $status = $model[$attribute['abbrev']]['status'];
                            $value = $model[$attribute['abbrev']]['value'];
                            $isSuppressed = $model[$attribute['abbrev']]['isSuppressed'];
                            $isVoid = $model[$attribute['abbrev']]['isVoid'];
                            $crossDataValue = $model[$attribute['abbrev']]['crossDataValue'];
                            $crossDataQCCode = $model[$attribute['abbrev']]['crossDataQcCode'];
                            if ($isVoid) {
                                $statusColor = 'black white-text';
                            } else if ($isSuppressed) {
                                $statusColor = 'grey white-text';
                            } else if ($status == 'new') {
                                $statusColor = 'green';
                            } else if ($status == 'invalid') {
                                $statusColor = 'red';
                            } elseif ($status == 'missing') {
                                $statusColor = 'blue darken-2 white-text';
                            } elseif ($status == 'missing;updated') {
                                $statusColor = 'amber blue-text text-darken-2';
                            }elseif ($status == 'updated') {
                                $statusColor = 'amber grey-text text-darken-2';
                            }elseif ($status == 'invalid;updated') {
                                $statusColor = 'amber red-text text-darken-2';
                            }
                            if (str_contains($status, 'updated')) {
                                $crossData = '';
                                if ($crossDataQCCode == 'S') {
                                    $crossData = '<span class=" badge blue darken-4 text-center white-text"><strong>' . $crossDataValue . '</strong></span>';
                                } else {
                                    $crossData = '<span style="background-color: transparent !important;" class="text-center black-text"><strong>' . $crossDataValue . '</strong></span>';
                                }
                                if ($isVoid) {
                                    $statusColor = 'black white-text';
                                }
                                if ($value == '') {
                                    $value = 'NULL';
                                }
                                return '<span class=" badge ' . $statusColor . ' text-center "><strong>' . $value . '</strong></span>' .
                                    ' &nbsp;' . $crossData;
                            } else if ($value != '') {
                                return '<span class=" badge ' . $statusColor . ' text-center white-text"><strong>' . $value . '</strong></span>';
                            } else if ($isVoid) {
                                if ($value == '') {
                                    $value = 'NULL';
                                }
                                $statusColor = 'black white-text';
                                return '<span class=" badge ' . $statusColor . ' text-center white-text"><strong>' . $value . '</strong></span>';
                            }
                        }
                        return '';
                    }
                ];
            }
        }
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes are the dynamic fields
     *
     * @param string $name property name
     * @return mixed property value
     * @see getAttribute()
     */
    public function __get($name)
    {

        if (array_key_exists($name, $this->dynamicFields))
            return $this->dynamicFields[$name];
        else
            return parent::__get($name);
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes are the dynamic fields
     *
     * @param string $name property name
     * @param mixed $value property value
     */
    public function __set($name, $value)
    {

        if (array_key_exists($name, $this->dynamicFields))
            $this->dynamicFields[$name] = $value;
        else
            parent::__set($name, $value);
    }

    /**
     * Search functionality for data browser
     * 
     * @param Array $params contains the filter parameters
     * @return ArrayDataProvider the data provider for the browser
     */
    public function search($params = null)
    {

        $this->load($params);


        //build filters

        if (isset($params['CrossData'])) {
            $filters = [];
            foreach ($params['CrossData'] as $key => $value) {
                if (isset($value)) {

                    if (
                        in_array($key, [
                            'crossName', 'crossMethod', 'germplasmDesignation',
                            'germplasmParentage', 'germplasmGeneration', 'crossFemaleParent',
                            'femaleParentage', 'femaleParentSeedSource', 'crossMaleParent',
                            'maleParentage', 'maleParentSeedSource',
                            'femaleSourceEntry', 'femaleSourceSeedName',
                            'femaleSourcePlot', 'maleSourceEntry',
                            'maleSourceSeedName', 'maleSourcePlot',
                            'femaleOccurrenceName', 'maleOccurrenceName',
                            'femaleOccurrenceCode', 'maleOccurrenceCode', 'crossRemarks'
                        ])

                    ) {

                        if (trim($value) !== '') {
                            $filters[$key] = "%" . trim($value) . "%";
                        }
                    } else if (
                        in_array($key, [
                            'femaleSourceEntryDbId', 'femaleSourcePlotDbId',
                            'maleSourceEntryDbId', 'maleSourcePlotDbId',
                            'femaleParentEntryNumber', 'maleParentEntryNumber',
                        ])

                    ) {
                        if (trim($value) !== '') {
                            $filters[$key] = "equals " . trim($value);
                        }
                    } else if ($key == 'status') {
                        if ($value == 'isVoid') {
                            $filters['isVoid'] = "true";
                        } else if ($value == 'suppressed') {
                            $filters['isSuppressed'] = "true";
                        } else {
                            $filters[$key] = trim($value);
                        }
                    } else {

                        if (is_array($value)) {
                            foreach ($value as $recordKey => $recordValue) {
                                if ($recordKey == 'value') {
                                    $key = rtrim($key, '_data');
                                    foreach ($recordValue as $keyValue) {
                                        if ($keyValue !== '') {

                                            $this->{$key} = $keyValue;
                                        }
                                    }
                                    $filters[$key][$recordKey] = $recordValue;
                                } else if (
                                    $recordKey == 'isSuppressed'
                                    || $recordKey == 'isVoid'
                                    || $recordKey == 'status'
                                ) {
                                    if ($recordValue !== '') {
                                        $key = rtrim($key, '_data');
                                        $filters[$key][$recordKey] = $recordValue;
                                        $this->{$key} = null;
                                    }
                                }
                            }
                        } else if (trim($value) !== '' && $key !== 'fields') {
                            $filters[$key]['value'] = trim($value);
                        }
                    }
                }
            }
        }

        if (isset($params['CrossData']['fields'])) {
            $crossColumns = [
                'entityDbId', 'crossName', 'crossMethod', 'germplasmDesignation',
                'germplasmParentage', 'germplasmGeneration', 'crossFemaleParent',
                'femaleParentage', 'femaleParentSeedSource', 'crossMaleParent',
                'maleParentage', 'maleParentSeedSource', 'femaleSourceEntryDbId',
                'femaleSourceEntry', 'femaleSourceSeedName', 'femaleSourcePlotDbId',
                'femaleSourcePlot', 'maleSourceEntryDbId', 'maleSourceEntry',
                'maleSourceSeedName', 'maleSourcePlotDbId', 'maleSourcePlot',
                'femaleParentEntryNumber', 'maleParentEntryNumber', 'femaleOccurrenceName',
                'maleOccurrenceName', 'femaleOccurrenceCode', 'maleOccurrenceCode', 'crossRemarks'
            ];
            $filters['fields'] = implode('|', $crossColumns) . '|' . $params['CrossData']['fields'];
        }
        $sort = new Sort();
        $sort->attributes = [
            'crossName', 'crossMethod', 'germplasmDesignation',
            'germplasmParentage', 'germplasmGeneration', 'crossFemaleParent',
            'femaleParentage', 'femaleParentSeedSource', 'crossMaleParent',
            'maleParentage', 'maleParentSeedSource', 'femaleSourceEntryDbId',
            'femaleSourceEntry', 'femaleSourceSeedName', 'femaleSourcePlotDbId',
            'femaleSourcePlot', 'maleSourceEntryDbId', 'maleSourceEntry',
            'maleSourceSeedName', 'maleSourcePlotDbId', 'maleSourcePlot',
            'femaleParentEntryNumber', 'maleParentEntryNumber', 'femaleOccurrenceName',
            'maleOccurrenceName', 'femaleOccurrenceCode', 'maleOccurrenceCode', 'crossRemarks'
        ];
        $sort->defaultOrder = [
            'orderNumber' => SORT_ASC
        ];

        $paramSort = '';
        if (isset($_GET['sort'])) {
            $paramSort = '&sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
            $tempOrder = ['orderNumber' => SORT_ASC];
            $tempOrder = array_merge($tempOrder, $sortOrder);
            $sort->setAttributeOrders($tempOrder);
            $sort->defaultOrder = $tempOrder;
        }
        
        $paramPage = '';
        $page = 1;
        if (isset($_GET['cross-dp-' . $this->transactionId . '-page'])) {
            $paramPage = '&page=' . $_GET['cross-dp-' . $this->transactionId . '-page'];
            $page = $_GET['cross-dp-' . $this->transactionId . '-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        }

        $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

        $filters = empty($filters) ? null : $filters;
        $url = 'terminal-transactions/' . $this->transactionId . '/cross-datasets-table-search?';

        $response = Yii::$app->api->getResponse('POST', $url . $paramLimit . $paramPage . $paramSort, json_encode($filters));

        $output = $response;
        $data = $output['body']['result']['data'];

        $dataProvider = new ArrayDataProvider([
            'id' => 'cross-dp-' . $this->transactionId,
            'allModels' => $data,
            'key' => 'entityDbId',
            'restified' => true,
            'totalCount' => $output['body']['metadata']['pagination']['totalCount'],
        ]);

        return [
            'dataProvider' => $dataProvider,
            'pageCount' => $output['body']['metadata']['pagination']['totalPages'],
            'page' => $page
        ];
    }

    /**
     * Return filter tags in formatted HTML chips
     * @param integer $transactionId Transaction identifier
     * @return string filter HTML chips
     */
    public function getFilterChips($transactionId)
    {
        $params = Yii::$app->request->queryParams;
        $chips = '';

        if (isset($params['CrossData'])) {
            $chipTags = [];

            foreach ($params['CrossData'] as $key => $entry) {

                $label = '';
                $filter = $params;

                $color = 'amber';
                if ($key === 'fields' && trim($entry) !== '') {
                    $abbrev = strtoupper($filter['CrossData'][$key]);
                    $param = [
                        "abbrev" => $abbrev
                    ];
                    $variable = $this->transaction->getVariable($param);
                    $color = 'dataset-color-theme lighten-4';
                    $label = $variable['label'] ?? $abbrev;
                    unset($filter['CrossData']['fields']);
                } else if ($key === 'status') {
                    $label = 'Status = ' . ucwords($entry);
                    unset($filter['CrossData']['status']);
                } else {
                    $col = rtrim($key, '_data');

                    $param = [
                        "abbrev" => "" . strtoupper($col)
                    ];
                    $variable = $this->transaction->getVariable($param);

                    if (isset($filter['CrossData'][$key]['status'])) {
                        $label = $variable['label'] . ' = ' . $filter['CrossData'][$key]['status'];
                        unset($filter['CrossData'][$key]['status']);
                    } else if (isset($filter['CrossData'][$key]['isSuppressed'])) {
                        $label = $variable['label'] . ' = Suppressed';
                        unset($filter['CrossData'][$key]['isSuppressed']);
                    } else if (isset($filter['CrossData'][$key]['isVoid'])) {
                        $label = $variable['label'] . ' = Voided';
                        unset($filter['CrossData'][$key]['isVoid']);
                    } else if ($filter['CrossData'][$key] !== '') {
                        $keyLabel = $key;
                        if ($key == 'plotNumber') {
                            $keyLabel = 'Plot Number';
                        } else if ($key == 'occurrenceCode') {
                            $keyLabel = 'Occurrence Code';
                        } else if ($key == 'locationCode') {
                            $keyLabel = 'Location Code';
                        } else if ($key == 'designX') {
                            $keyLabel = 'Design X';
                        } else if ($key == 'designY') {
                            $keyLabel = 'Design Y';
                        } else if ($key == 'paX') {
                            $keyLabel = 'Pa X';
                        } else if ($key == 'paY') {
                            $keyLabel = 'Pa Y';
                        } else if ($key == 'fieldX') {
                            $keyLabel = 'Field X';
                        } else if ($key == 'fieldY') {
                            $keyLabel = 'Field Y';
                        } else if ($key == 'plotCode') {
                            $keyLabel = 'Plot Code';
                        } else if ($key == 'plotType') {
                            $keyLabel = 'Plot Type';
                        }

                        if (is_array($filter['CrossData'][$key])) {
                            $value = $filter['CrossData'][$key]['value']['equals'];
                        } else {
                            $value = $filter['CrossData'][$key];
                        }
                        $label = (isset($variable['label']) ? $variable['label'] : $keyLabel) . ' = ' . $value;
                        unset($filter['CrossData'][$key]);
                    }
                }
                if ($label !== '') {
                    $dataUrl = Url::toRoute(array_merge(['/dataCollection/transaction/' . $transactionId], $filter));
                    $chipTags[] = '<div class="chip ' . $color . '  lighten-4 z-depth-1" tabindex="0" >' . $label . '<i data-data_level="cross" data-variable_abbrev="' . $key . '" data-url="' . $dataUrl . '" class="close-chip material-icons close">close</i></div>';
                }
            }

            if (!empty($chipTags)) {

                $chips = '<a id="cross-remove-all-filters" class="waves-effect grey btn " style="text-decoration: none; color: #fff; padding: 0px 9px; height: 25px; line-height: 25px; text-transform: none; margin-left: 10px;">' . \Yii::t('app', 'Remove filters') . '</a>'
                    . '<div class="chips" style=" display: inline; border-bottom: unset;margin-left: 10px;">'
                    . implode('', $chipTags) . '</div>';
            }
        }
        return $chips;
    }

    /**
     * Return status tags in formatted HTML buttons
     * @param array $variables List of variables in the transaction
     * @return string filter HTML buttons
     */
    public function getStatusTags($variables)
    {
        $legend = '<a class="btn-flat " style="cursor: default;text-decoration: none; background-color: '
            . 'transparent !important; padding: 0px 9px; height: 20px; line-height: 20px; text-transform: none; '
            . 'margin-left: 10px;font-size: .8rem;font-weight: bold;    color: #9e9e9e;">' . \Yii::t('app', 'Status') . '</a>';
        $newCount = 0;
        $invalidCount = 0;
        $updatedCount = 0;
        $voidedCount = 0;

        $operationalSuppressedCount = 0;
        $suppressedCount = 0;
        foreach ($variables as $record) {
            $newCount += intval($record["newCount"]);
            $updatedCount += intval($record["updatedCount"]);
            $voidedCount += intval($record["voidedCount"]);
            $invalidCount += intval($record["invalidCount"]);
            
            $suppressedCount += intval($record["suppressedCount"]);
            $operationalSuppressedCount += intval($record["operationalSuppressedCount"]);
        }

        $statusArr = [];
        if ($newCount > 0) {
            $statusArr[] = '<span data-status="new" style="cursor: pointer;" class=" cross-status-btn btn waves-effect btn-flat green darken-3  "' .
                ' data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'No value in the experiment') . '">' . \Yii::t('app', 'NEW') . '</span>';
        }
        if ($invalidCount > 0) {
            $statusArr[] = '<span data-status="invalid" style="cursor: pointer; background-color:#c62828 !important;" '
                . 'class=" cross-status-btn btn waves-effect grey btn-flat red darken-3 " data-placement="top" data-title="tooltip" title="Wrong data format">'
                . \Yii::t('app', 'INVALID') . '</span>';
        }
        if ($updatedCount > 0) {
            $statusArr[] = '<span data-status="updated" style="cursor: pointer; background-color: #ffc107 !important;" '
                . 'class=" cross-status-btn btn waves-effect btn-flat grey-text text-darken-2" data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'Valid value that will update the experiment value') . '">' . \Yii::t('app', 'UPDATED') . '</span>';
        }

        if ($voidedCount > 0) {
            $statusArr[] = '<span data-status="isVoid" style="cursor: pointer; background-color:black !important;" class=" cross-status-btn btn waves-effect btn-flat " '
                . 'data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'Experiment value that will be removed') . '">'
                . \Yii::t('app', 'VOIDED') . '</span>';
        }
        if ($suppressedCount > 0) {
            $statusArr[] = '<span data-status="suppressed" style="cursor: pointer;" '
                . 'class=" cross-status-btn btn waves-effect btn-flat grey" data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'Valid value but not useful in analysis') . '">' . \Yii::t('app', 'SUPPRESSED') . '</span>';
        }

        $experimentLegend = '';
        if ($updatedCount > 0) {

            $experimentLegend = '
            <span class="btn-flat" style="cursor: default;text-decoration: none; background-color: '
                . 'transparent !important; padding: 0px 9px; height: 20px; line-height: 20px; '
                . 'text-transform: none; margin-left: 10px;font-size: .8rem;font-weight: bold;color: #9e9e9e;">'
                . \Yii::t('app', 'Legend') . ':</span>
            <span style="cursor: default;text-decoration: none; color: black; padding: 0px 9px; '
                . 'height: 25px; line-height: 25px; text-transform: none; background-color: '
                . 'transparent !important;"
            class="btn-flat blue  " data-placement="top" data-title="tooltip" title="'
                . \Yii::t('app', 'Current cross data value') . '" >' . \Yii::t('app', 'CROSS DATA VALUE') . '</span>';

            if ($operationalSuppressedCount > 0) {
                $experimentLegend = $experimentLegend . '<span class="btn-flat" style="background-color: #1565c0 !important;cursor: default;text-decoration: none; color: #fff; padding: 0px 9px; '
                    . 'height: 25px; line-height: 25px; text-transform: none; margin-left: 5px;" '
                    . ' data-placement="top" data-title="tooltip" title="'
                    . \Yii::t('app', 'Current cross data value but suppressed') . '">' . \Yii::t('app', 'SUPPRESSED CROSS DATA VALUE') . '</span>';
            }
        }
        return  '
        <span class="pull-left">
            ' . $legend . '&nbsp;
        </span>
        <div class="legend-items">
         ' . implode(' 
         ', $statusArr) . $experimentLegend . '
        </div> ';
    }
}
