<?php
/* 
* This file is part of EBS-Core Breeding. 
* 
* EBS-Core Breeding is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* 
* EBS-Core Breeding is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
* GNU General Public License for more details. 
* 
* You should have received a copy of the GNU General Public License 
* along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

namespace app\modules\dataCollection\models;

use Yii;
use \app\models\Occurrence;
use \app\models\Person;
use \app\models\Plot;
use \app\models\User;
use app\interfaces\models\IVariable;
use app\models\Api;
use app\models\BackgroundJob;

/**
 * Upload model for data collection
 */
class Upload
{
    public function __construct (
        public Api $api,
        public Occurrence $occurrence,
        public Person $person,
        public Plot $plot,
        public User $user,
        public IVariable $variable,
        public BackgroundJob $backgroundJob,
    )
    {

    }
    /**
     * File reader
     * @param  string $filename File name
     * @param  string $uploadType file upload type
     * @param  string $program program code
     * @return array if success, includes header, temporary data table, otherwise error msg
     */
    public function reader($filename, $uploadType = '', $program = '')
    {
        $datasetUnit = '';
        $identifiers = ["PLOT_ID", "CROSS_ID"];
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if (!empty($uploadType)) {
            $configName = $program;
    
            if (strtoupper($role) == 'COLLABORATOR') {
                $configName = 'COLLABORATOR';
            }
    
            // Get other DC file names
            $dataCollectionOtherFiles = $this->api->getApiResults(
                'GET',
                'configurations?abbrev=DC_' . strtoupper($configName) . '_OTHER_FILE_SETTINGS&limit=1',
                retrieveAll: true
            );
            if($dataCollectionOtherFiles['totalCount'] == 0){
                $dataCollectionOtherFiles = $this->api->getApiResults(
                    'GET',
                    'configurations?abbrev=DC_GLOBAL_OTHER_FILE_SETTINGS&limit=1',
                    retrieveAll: true
                );
            }

            $configValue = $dataCollectionOtherFiles['data'][0]['configValue'] ?? [];
    
            $uploadConfig = $traits = $identifier = [];
            foreach  ($configValue[$uploadType]['Upload'] as $variable){

                if ($variable['isObservation'] || $variable['identifier']){
                    $uploadConfig[$uploadType][] = $variable;
                    if($variable['identifier']) {
                        $identifier[$variable['attribute']] = $variable['header'];
                    }
                    if($variable['isObservation']) {
                        $traits[$variable['abbrev']] = $variable['header'];
                    }
                }
            }
            return $this->otherFileUpload($traits, $identifier, $filename);
        }

        try {

            $uploadPath = realpath(Yii::$app->basePath) . '/uploads/';
            $file = \yii\web\UploadedFile::getInstanceByName($filename);
            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return array("success" => false, "error" => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.');
            }
            $newFileName = \Yii::$app->security->generateRandomString() . '.' . $file->extension;
            $newFileName = $uploadPath . '/' . $newFileName;

            if ($file->saveAs($newFileName)) {

                $csv = new \app\components\CsvReader($newFileName);
                $csv->setHasHeader(true);

                if (!empty($csv->getInvalids())) {

                    $error = [];
                    $errs = $csv->getInvalids();
                    foreach ($errs as $err) {
                        $error[] = $err . '<br>';
                    }

                    return array("success" => false, "error" => $error);
                }

                $plotList = '';
                foreach ($csv as $key => $line) {

                    if((isset($line['plot_id']) && $line['plot_id'] !== '') ||
                        (isset($line['cross_id']) && $line['cross_id'] !== '')){

                        $uploadedData[] = $line;

                        if(isset($uploadedData[$key-1]['plot_id'])) {
                            $plotList = $plotList == '' ? $uploadedData[$key-1]['plot_id'] :
                                $plotList . ',' . $uploadedData[$key-1]['plot_id'];
                        }
                    }
                }

                if(!$isAdmin && $plotList !== '') {
                    $plotListData = $this->plot->searchAll([
                        'plotDbId' => $plotList
                    ],'distinctOccurrences=true')['data'];

                    $occs = array_keys(array_flip(array_column($plotListData, 'occurrenceDbId')));

                    $permittedOccurrences = $this->occurrence->searchAll([
                        'fields' => 'occurrence.id AS occurrenceDbId',
                        'occurrenceDbId' => 'equals '.implode('|equals ', $occs)
                    ],
                    'permission=write'
                    )['totalCount'];

                    if ($permittedOccurrences == 0){
                        return [
                            "success" => false,
                            "error" => "You do not have permission to upload data collection file for occurrence(s) with ID " . implode(',' , $occs)
                        ];
                    }
                }

                $headers = $csv->getHeader();
                //  validate the measurement headers
                $measurementHeaders = [];
                foreach ($headers as $header) {
                    $header = trim($header);
                    if (
                        !empty($header) && !in_array($header, $identifiers)
                    ) {
                        $measurementHeaders[] = trim($header);
                    }
                }

                $variableAbbrev = implode("|equals", $measurementHeaders);

                //get the location and occurrence ID
                if (isset($uploadedData[0]['plot_id']) && !empty($uploadedData[0]['plot_id'])) {
                    $datasetUnit = 'plot';
                    $plotDbId = $uploadedData[0]['plot_id'];
                    $result = $this->getLocation($plotDbId);
                    if (!$result["success"]) {
                        return $result;
                    }
                    $locationDbId = $result["locationDbId"];

                    $occurrence = $this->getOccurrenceDbId($plotDbId);
                    if (!$occurrence["success"]) {
                        return $occurrence;
                    }
                    $occurrenceDbId = $occurrence["occurrenceDbId"];

                } elseif (isset($uploadedData[0]['cross_id']) && !empty($uploadedData[0]['cross_id'])) {
                    $datasetUnit = 'cross';
                    $crossDbId = $uploadedData[0]['cross_id'];

                    $result = $this->getLocationFromCross($crossDbId);

                    if (!$result["success"]) {
                        return $result;
                    }
                    $locationDbId = $result["locationDbId"];

                    $occurrence = $this->getOccurrenceDbIdFromCross($crossDbId);
                    if (!$occurrence["success"]) {
                        return $occurrence;
                    }
                    $occurrenceDbId = $occurrence["occurrenceDbId"];
                } else {
                    return ["success" => false, "error" => "Kindly make sure that PLOT_ID or CROSS_ID is in the file header and is not empty."];
                }

                if (empty($measurementHeaders)) {
                    return ["success" => false, "error" => "Kindly make sure that the file has plot or cross variables."];
                }
        
                $param = [
                    "abbrev" =>"equals $variableAbbrev",
                    "type" => 'equals observation'
                ];
                $variableList = $this->variable->searchAll($param);
                if ($variableList['totalCount'] == 0) {
                    return [
                        "success" => false,
                        "error" => "Missing trait variables. Kindly upload at least one(1) trait variable."
                    ];
                } else {
                    $measurementHeaders = array_column($variableList['data'], 'abbrev');
                }
                //checks if location has open transaction
                $result = $this->getTransaction($locationDbId, $occurrenceDbId);

                if (!$result["success"]) {
                    return $result;
                }
                $transactionId = $result["transactionId"];
                $oldTransaction = $result["oldTransaction"];
                // create datasets
                $result = $this->createDataset($transactionId, $oldTransaction, $measurementHeaders, $uploadedData);

                if (!$result["success"]) {
                    return $result;
                }

                // create the background job record
                $result = $this->createBackgroundJob($transactionId);

                if (!$result["success"]) {
                    return $result;
                }

                $backgroundJobDbId = $result["backgroundJobDbId"];

                $result["datasetUnit"] = $datasetUnit;
                $result["transactionId"] = $transactionId;
                $result["backgroundJobDbId"] = $backgroundJobDbId;
                $result["success"] = true;
                
                return $result;
            }
        } catch (\Exception $e) {
            $error = 'Error in uploading the file. Try uploading the file again. If error persists, submit a bug report.';
            $errorMessage = $e->getMessage();
            Yii::error(__METHOD__, $errorMessage);
            if (strpos($errorMessage, 'No delimiter found in file.') !== false) {
                $error = 'Missing required trait information. The file should consist of PLOT_ID/CROSS_ID and at least one(1) trait variable.';
            }elseif (strpos($errorMessage, 'Header count') !== false) {
                $error = "$errorMessage The file should consist of PLOT_ID/CROSS_ID and at least one(1) trait value.";

            }

            Yii::error($e, __METHOD__);
            return ["success" => false, "error" => \Yii::t('app',$error)];
        }
    }

    /**
     * Retrieves configuration for other file upload options
     * @param traits array list of configured traits
     * @param identifier array list of configured identifier
     * @param filename string configured file name
     */
    public function otherFileUpload($traits, $identifier, $filename) {
        try {

            $uploadPath = realpath(Yii::$app->basePath) . '/uploads/';
            $file = \yii\web\UploadedFile::getInstanceByName($filename);
            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return array("success" => false, "error" => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.');
            }
            $newFileName = \Yii::$app->security->generateRandomString() . '.' . $file->extension;
            $newFileName = $uploadPath . '/' . $newFileName;

            if ($file->saveAs($newFileName)) {

                $csv = new \app\components\CsvReader($newFileName);
                $csv->setHasHeader(true);

                // validate if has access to upload for collaborator role
                $program = Yii::$app->userprogram->get('abbrev');
                [
                    $role,
                    $isAdmin,
                ] = $this->person->getPersonRoleAndType($program);

                foreach ($csv as $key => $line) {

                    // if non-admin, collaborator role and file has plot_id column
                    if (!$isAdmin && strtoupper($role) == 'COLLABORATOR' && isset($line[strtolower('plot_id')]) && $key == 1){

                        $plotResult = $this->plot->getOne($line[strtolower('plot_id')]);

                        // if has no access in occurrence
                        if (!$plotResult['totalCount']) {
                            $error = 'You do not have permission in Plot with ID ' . $line[strtolower('plot_id')] . '.';
                            return array("success" => false, "error" => $error);
                        }
                    }

                    if (!empty(trim($key))) {
                        $uploadedData[] = $line;
                    }
                }

                if (!empty($csv->getInvalids())) {

                    $error = [];
                    $errs = $csv->getInvalids();
                    foreach ($errs as $err) {
                        $error[] = $err . '<br>';
                    }

                    return array("success" => false, "error" => $error);
                }

                $headers = $csv->getHeader();
                //  validate the measurement headers
                $measurementHeaders = [];

                foreach ($headers as $header) {
                    foreach ($traits as $key => $trait) {
                        $header = trim($header);
                        if (!empty($header) && $header == $trait) {
                            $measurementHeaders[] = $key;
                        }
                        
                    }
                }

                $variableAbbrev = implode("|equals", $measurementHeaders);

                if( $identifier !== 'PLOT_ID' && $identifier !== 'CROSS_ID'){
                    $filter = key($identifier);
                    if($filter  == 'plotDbId'){
                        foreach($uploadedData as $key => $value){
                            foreach($value as $a => $b){

                                if($a == strtolower($identifier['plotDbId'])){
                                    $uploadedData[$key]['plot_id'] =  $b;
                                }
                                foreach($traits as $tkey => $trait){
                                    if( $a == strtolower($trait)){
                                        $uploadedData[$key][strtolower($tkey)] = $b;

                                    }
                                }
                            }
                        }
                    }
                }

                //get the location ID
                if (isset($uploadedData[0]['plot_id']) && !empty($uploadedData[0]['plot_id'])) {
                    $datasetUnit = 'plot';
                    $plotDbId = $uploadedData[0]['plot_id'];
                    $result = $this->getLocation($plotDbId);
                    if (!$result["success"]) {
                        return $result;
                    }
                    $locationDbId = $result["locationDbId"];

                    $occurrence = $this->getOccurrenceDbId($plotDbId);
                    if (!$occurrence["success"]) {
                        return $occurrence;
                    }
                    $occurrenceDbId = $occurrence["occurrenceDbId"];

                } elseif (isset($uploadedData[0]['cross_id']) && !empty($uploadedData[0]['cross_id'])) {
                    $datasetUnit = 'cross';
                    $crossDbId = $uploadedData[0]['cross_id'];

                    $result = $this->getLocationFromCross($crossDbId);

                    if (!$result["success"]) {
                        return $result;
                    }
                    $locationDbId = $result["locationDbId"];

                    $occurrence = $this->getOccurrenceDbIdFromCross($crossDbId);
                    if (!$occurrence["success"]) {
                        return $occurrence;
                    }
                    $occurrenceDbId = $occurrence["occurrenceDbId"];
                } else {
                    return ["success" => false, "error" => "Kindly make sure that PLOT_ID or CROSS_ID is in the file header and is not empty."];
                }

                if (empty($measurementHeaders)) {
                    return ["success" => false, "error" => "Kindly make sure that the file has plot or cross variables."];
                }
        
                $param = [
                    "abbrev" =>"equals $variableAbbrev",
                    "type" => 'equals observation'
                ];
                $variableList = $this->variable->searchAll($param);
                if ($variableList['totalCount'] == 0) {
                    return [
                        "success" => false,
                        "error" => "Missing trait variables. Kindly upload at least one(1) trait variable."
                    ];
                } else {
                    $measurementHeaders = array_column($variableList['data'], 'abbrev');
                }
                //checks if location has open transaction
                $result = $this->getTransaction($locationDbId, $occurrenceDbId);

                if (!$result["success"]) {
                    return $result;
                }
                $transactionId = $result["transactionId"];
                $oldTransaction = $result["oldTransaction"];

                // create datasets
                $result = $this->createDataset($transactionId, $oldTransaction, $measurementHeaders, $uploadedData);

                if (!$result["success"]) {
                    return $result;
                }

                // create the background job record
                $result = $this->createBackgroundJob($transactionId);

                if (!$result["success"]) {
                    return $result;
                }

                $backgroundJobDbId = $result["backgroundJobDbId"];

                $result["datasetUnit"] = $datasetUnit;
                $result["transactionId"] = $transactionId;
                $result["backgroundJobDbId"] = $backgroundJobDbId;
                $result["success"] = true;
                return $result;
            }
        } catch (\Exception $e) {
            $error = 'Error in uploading the file. Try uploading the file again. If error persists, submit a bug report.';
            $errorMessage = $e->getMessage();

            if (strpos($errorMessage, 'No delimiter found in file.') !== false) {
                $error = 'Missing required trait information. The file should consist of PLOT_ID/CROSS_ID and at least one(1) trait variable.';
            }elseif (strpos($errorMessage, 'Header count') !== false) {
                $error = "$errorMessage The file should consist of PLOT_ID/CROSS_ID and at least one(1) trait value.";

            }

            Yii::error($e, __METHOD__);
            return ["success" => false, "error" => \Yii::t('app',$error)];
        }
    }

    /**
     * Validate and retrieve locationID using location code
     * @param string $locationCode Location Code
     * @return array success and error / success and locationDbId
     */
    public function validateLocation($locationCode)
    {
        $param = [
            'locationCode' => "equals $locationCode",
            'fields' => 'location.id AS locationDbId|location_code AS locationCode'
        ];

        $url = 'locations-search';
        $data = Yii::$app->api->getResponse('POST', $url, json_encode($param));

        if ($data["status"] != 200 && $data["status"] != 404) {
            return [
                "success" => false,
                "error" => "Error while retrieving locations. Kindly contact Administrator."
            ];
        }
        if (empty($data["body"]["result"]["data"])) {
            return [
                "success" => false,
                "error" => "Kindly check the LOCATION_CODE. Location <b>{$locationCode}</b> does not exist."
            ];
        }
        $locationDbId = "" . $data["body"]["result"]["data"][0]["locationDbId"];

        return [
            "success" => true,
            "locationDbId" => $locationDbId
        ];
    }

    /**
     * Retrieve locationID using plotDId
     * @param string $plotDbId Plot identifier
     * @return array success and error / success and locationDbId
     */
    public function getLocation($plotDbId)
    {

        $data = $this->plot->getOne($plotDbId);
        if ($data["status"] != 200 && $data["status"] != 404) {
            return [
                "success" => false,
                "error" => "Error while retrieving locations. Kindly contact Administrator."
            ];
        }
        if (empty($data["data"])) {
            return [
                "success" => false,
                "error" => "Kindly check the PLOT_ID. Plot <b>{$plotDbId}</b> does not exist."
            ];
        }else if (empty($data["data"]["locationDbId"])) {
            return [
                "success" => false,
                "error" => "Missing location details</b>. Please ensure that the experiment is MAPPED OR PLANTED before uploading."
            ];
        }
        $locationDbId = "" . $data["data"]["locationDbId"];

        return [
            "success" => true,
            "locationDbId" => $locationDbId
        ];
    }

    /**
     * Retrieve locationID using crossDbId
     * @param string $crossDbId Cross identifier
     * @return array success and error / success and locationDbId
     */
    public function getLocationFromCross($crossDbId)
    {
        $param = [
            "crossDbId" => "equals $crossDbId",
            'fields' => 'germplasmCross.id AS crossDbId|location.id AS locationDbId'
        ];

        $url = 'crosses-search';
        $data = Yii::$app->api->getResponse('POST', $url, json_encode($param));

        if ($data["status"] != 200 && $data["status"] != 404) {
            return [
                "success" => false,
                "error" => "Error while retrieving locations. Kindly contact Administrator."
            ];
        }
        if (empty($data["body"]["result"]["data"])) {
            return [
                "success" => false,
                "error" => "Kindly check the CROSS_ID. Cross <b>{$crossDbId}</b> does not exist."
            ];
        } else if (empty($data["body"]["result"]["data"][0]["locationDbId"])) {
            return [
                "success" => false,
                "error" => "Missing location details</b>. Please ensure that the experiment is PLANTED before uploading."
            ];
        }
        $locationDbId = "" . $data["body"]["result"]["data"][0]["locationDbId"];

        return [
            "success" => true,
            "locationDbId" => $locationDbId
        ];
    }

    /**
     * Retrieve occurrenceDbId using plotDbId
     * @param string $plotDbId Plot identifier
     * @return array success and error / success and occurrenceDbId
     */
    public function getOccurrenceDbId($plotDbId)
    {

        $data = $this->plot->getOne($plotDbId);
        if ($data["status"] != 200 && $data["status"] != 404) {
            return [
                "success" => false,
                "error" => "Error while retrieving locations. Kindly contact Administrator."
            ];
        }
        if (empty($data["data"])) {
            return [
                "success" => false,
                "error" => "Kindly check the PLOT_ID. Plot <b>{$plotDbId}</b> does not exist."
            ];
        }else if (empty($data["data"]["occurrenceDbId"])) {
            return [
                "success" => false,
                "error" => "Missing occurrence details</b>. Please ensure that the experiment is MAPPED OR PLANTED before uploading."
            ];
        }
        $occurrenceDbId = "" . $data["data"]["occurrenceDbId"];

        return [
            "success" => true,
            "occurrenceDbId" => $occurrenceDbId
        ];
    }

    /**
     * Retrieve occurrenceDbId using crossDbId
     * @param string $crossDbId Cross identifier
     * @return array success and error / success and occurrenceDbId
     */
    public function getOccurrenceDbIdFromCross($crossDbId)
    {

        $param = [
            "crossDbId" => "equals $crossDbId",
            'fields' => 'germplasmCross.id AS crossDbId|germplasmCross.occurrence_id AS occurrenceDbId'
        ];

        $url = 'crosses-search';
        $data = Yii::$app->api->getResponse('POST', $url, json_encode($param));

        if ($data["status"] != 200 && $data["status"] != 404) {
            return [
                "success" => false,
                "error" => "Error while retrieving occurrence. Kindly contact Administrator."
            ];
        }
        if (empty($data["body"]["result"]["data"])) {
            return [
                "success" => false,
                "error" => "Kindly check the CROSS_ID. Cross <b>{$crossDbId}</b> does not exist."
            ];
        } else if (empty($data["body"]["result"]["data"][0]["occurrenceDbId"])) {
            return [
                "success" => false,
                "error" => "Missing occurrence details</b>. Please ensure that the experiment is PLANTED before uploading."
            ];
        }
        $occurrenceDbId = "" . $data["body"]["result"]["data"][0]["occurrenceDbId"];

        return [
            "success" => true,
            "occurrenceDbId" => $occurrenceDbId
        ];
    }

    /**
     * Check if location has existing transaction
     * If no open transaction, create a transaction
     * @param integer $locationDbId Location identifier
     * @param integer $occurrenceDbId Occurrence identifier
     * @return array success and error / success ,transactionId, oldTransaction
     */
    public function getTransaction($locationDbId, $occurrenceDbId)
    {

        $userId = $this->user->getUserId();
        $param = [
            "locationDbId" => "equals $locationDbId",
            "status" => [
                "like" => "uploaded%|in queue|%progress|to be%"
            ],
            "creatorDbId" => "equals $userId",
            "action" => "equals file upload"
        ];
        $url = 'terminal-transactions-search';
        $data = Yii::$app->api->getResponse('POST', $url, json_encode($param));
        if ($data["status"] != 200) {
            return [
                "success" => false,
                "error" => "Error while retrieving transactions. Kindly contact Administrator."
            ];
        }

        $oldTransaction = true;
        $status = $data["body"]["result"]["data"][0]["status"] ?? '';
        if (empty($data["body"]["result"]["data"])) {   // no open transactions

            // create data terminal transaction
            $param = [
                "records" => [[
                    "locationDbId" => "" . $locationDbId,
                    "action" => "file upload",
                    "occurrence" => [["occurrenceDbId" => $occurrenceDbId]]
                ]]
            ];
            $url = 'terminal-transactions';
            $data = Yii::$app->api->getResponse('POST', $url, json_encode($param));

            if ($data["status"] != 200) {
                $error = $data["body"]["metadata"]["status"][0]["message"] ??
                    "while creating transaction. Kindly contact Administrator.";
                return [
                    "success" => false,
                    "error" => "Error $error",
                ];
            }
            $oldTransaction = false;
            $transactionId = $data["body"]["result"]["data"][0]["transactionDbId"];

        } elseif (str_contains($status, "progress")) {
            //if has open transaction and status is still in progress
            return [
                "success" => false,
                "error" => "The transaction for the location is still in progress. Wait until it finished, then upload your file."
            ];
        } elseif (str_contains($status, "to be") && !str_contains($status, "uploaded")) {
            //if has open transaction and status is still in queue
            return [
                "success" => false,
                "error" => "The transaction for the location is still in queue. Wait until it finished, then upload your file."
            ];
        } else {
            // use the open transaction
            $transactionId = $data["body"]["result"]["data"][0]["transactionDbId"];
            $oldTransaction = true;
        }
        return [
            "success" => true,
            "oldTransaction" => $oldTransaction,
            "transactionId" => $transactionId
        ];
    }

    /**
     * Creates background job record
     * @param integer $transactionId Transaction identifier
     * @return array success and error / success and backgroundJobDbId
     */
    public function createBackgroundJob($transactionId)
    {
        $userId = Yii::$app->session->get('user.id');

        $backgroundJobParams = [
            "workerName" => "DataCollectionUploader",
            "description" => "Upload Data Collection File",
            "entity" => "TERMINAL_TRANSACTION",
            "entityDbId" => $transactionId,
            "endpointEntity" => "TERMINAL_TRANSACTION",
            "application" => "QUALITY_CONTROL",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId,
        ];
        
        $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
        $backgroundJobDbId = $transaction["data"][0]["backgroundJobDbId"];
        
        if ($transaction["status"] != 200) {
            // delete the transaction if unsuccessful
            $url = 'terminal-transactions/' . $transactionId;
            Yii::$app->api->getResponse('DELETE', $url, json_encode($backgroundJobParams));
            return [
                "success" => false,
                "error" => "Error while creating background job record. Kindly contact Administrator."
            ];
        }
        return [
            "success" => true,
            "backgroundJobDbId" => $backgroundJobDbId,
        ];
    }

    /**
     * Creates records for transaction dataset
     * @param integer $transactionId Transaction identifier
     * @param string $oldTransaction Transaction identifier
     * @param array $measurementHeaders Array of variable abbrev
     * @param array $uploadedData Array of uploaded data
     * @return array success and error / success
     */
    public function createDataset($transactionId, $oldTransaction, $measurementHeaders, $uploadedData)
    {
        $param = [
            "measurementVariables" => $measurementHeaders,
            "records" => $uploadedData
        ];

        $url = 'terminal-transactions/' . $transactionId . "/dataset-table";
        $data = Yii::$app->api->getResponse('POST', $url, json_encode($param));

        if ($data["status"] == 400) {

            preg_match('/^(500|400:\s*)((\w*\s*[^|\s]*)*)(\s*.+)$/', $data["body"]["metadata"]["status"][0]["message"], $errorMsg);

            if (empty($errorMsg)) {
                $errorMsg = "There is at least one(1) cross or plot record that does not exist in the database.";
            } else {
                $errorMsg = $errorMsg[2];
            }
            // delete the transaction if unssuccessful
            $url = 'terminal-transactions/' . $transactionId;
            Yii::$app->api->getResponse('DELETE', $url, json_encode($param));
            return [
                "success" => false,
                "error" => "Error while uploading the file. " . $errorMsg
            ];
        } else if ($data["status"] != 200) {

            // delete the transaction if unssuccessful
            $url = 'terminal-transactions/' . $transactionId;
            $data = Yii::$app->api->getResponse('DELETE', $url, json_encode($param));
            return [
                "success" => false,
                "error" => "Error while uploading the file. Kindly contact Administrator."
            ];
        } else if ($data["body"]["result"]["data"][0]["count"] == 0) {

            if (!$oldTransaction) {
                // delete the transaction if unssuccessful
                $url = 'terminal-transactions/' . $transactionId;
                $data = Yii::$app->api->getResponse('DELETE', $url, json_encode($param));
            }

            return [
                "success" => false,
                "error" => "There are no data to be uploaded. Please check if the variables are not empty, the data in the file you uploaded is already in the experiment, or in an open transaction. "
            ];
        } else if ($data["body"]["result"]["data"][0]["count"] > 0) {
            // changed the status into in queue
            $param = [
                "status" => 'in queue',
            ];
            $url = 'terminal-transactions/' . $transactionId;
            $data = Yii::$app->api->getResponse('PUT', $url, json_encode($param));
            if ($data["status"] != 200) {
                // delete the transaction if unssuccessful
                $url = 'terminal-transactions/' . $transactionId;
                $data = Yii::$app->api->getResponse('DELETE', $url, json_encode($param));
                return [
                    "success" => false,
                    "error" => "Error while creating transaction dataset. Kindly contact Administrator."
                ];
            }
        }
        return [
            "success" => true
        ];
    }
}
