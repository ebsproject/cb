<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dataCollection\models;

use Yii;
use \app\models\Config;
use \app\models\FormulaParameters;
use app\models\OccurrenceData;
use \app\models\SuppressionRule;
use \app\models\TerminalTransaction;
use \app\models\TransactionDataset;
use \app\models\Variable;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\modules\dataCollection\models\ITransaction;

/**
 *  Model class for terminal transaction
 */
class Transaction implements ITransaction
{
    public function __construct (
        public Config $config,
        public FormulaParameters $formulaParameters,
        public OccurrenceData $occurrenceData,
        public SuppressionRule $suppressionRule,
        public TerminalTransaction $terminalTransaction,
        public TransactionDataset $transactionDataset,
        public Variable $variable,
    )
    {

    }

    /**
     * Retrieve variable given the variableDbId
     * @param array $param search parameters
     * @return array variable record
     */
    public function getVariable($param)
    {
        $response = $this->variable->searchAll($param, 'limit=1', false);
        return $response["data"][0] ?? [];
    }

    /**
     * Deletes dataset records
     * @param Integer $transactionDbId Transaction identifier
     * @return array Contains success and/or error message
     * 
     * TODO: to be deleted
     */
    function deleteDataset($transactionDbId)
    {
        $params = '';
        $response = $this->terminalTransaction->searchAllDatasetsTable($transactionDbId, $params, '');
        if ($response['success']) {
            $datasets = $response['data'];
            if (!empty($datasets)) {
                foreach ($datasets as $record) {
                    $datasetToBeDeleted[] = $record["datasetDbId"];
                }

                $params = [
                    "httpMethod" => "DELETE",
                    "endpoint" => 'v3/transaction-datasets',
                    "dbIds" => implode("|", $datasetToBeDeleted)
                ];
                $method = 'POST';
                $path = 'broker';
                $deleteResponse = Yii::$app->api->getParsedResponse($method, $path, json_encode($params));

                if (!$deleteResponse['success']) {
                    return [
                        "success" => false,
                        "error" => "There is an error while deleting transaction dataset records. "
                    ];
                }
                return [
                    "success" => true,
                ];
            }
            return [
                "success" => true,
            ];
        } else {
            return [
                "success" => false,
                "error" => "There is an error while retrieving transaction dataset records in deleting transaction dataset records. "
            ];
        }
    }

    /**
     * Creates file record
     * @param Integer $transactionDbId Transaction identifier
     * @param String $status Committed or invalid
     * @return array Contains success and/or error message
     */
    function createTransactionFile($transactionDbId, $status)
    {
        $params = [
            "status" => "equals $status",
            "isVoid" => "equals false"
        ];
        $response = $this->terminalTransaction->searchAllDatasets($transactionDbId, $params, '');
        if ($response['success']) {

            if (!empty($response['data'])) { // no file record is created if dataset is empty

                if ($status == 'new|updated' || $status == 'updated|new') {
                    $status = 'committed';
                }
                $params = [
                    "records" => [[
                        "transactionDbId" => $transactionDbId,
                        "status" => $status,
                        "dataset" => $response['data']
                    ]]
                ];

                $response = $this->terminalTransaction->createFiles($transactionDbId, $params);
                if ($response['success']) {
                    return [
                        "success" => true
                    ];
                } else {

                    return [
                        "success" => false,
                        "error" => "There is an error while creating file records. "
                    ];
                }
            }
            return [
                "success" => true,
            ];
        } else {
            return [
                "success" => false,
                "error" => "There is an error while retrieving transaction dataset records in creating file records. "
            ];
        }
    }

    /**
     * Commits transaction dataset records
     * @param Integer $transactionDbId Transaction identifier
     * @return array Contains success and/or error message
     */
    function commitPlotData($transactionDbId)
    {
        $response = Yii::$app->api->getParsedResponse('POST', 'terminal-transactions/' . $transactionDbId . '/commit-requests', '', $filters = '', true);
        if ($response['success']) {
            return [
                "success" => true,
            ];
        } else {
            return [
                "success" => false,
                "error" => "There is an error while committing plot data."
            ];
        }
    }

    /**
     * Suppress or unsuppress records
     * @param Array $params parameters for suppressing or unsuppressing records
     * @return Json Contains success and/or error message
     */
    public function suppressRecord($params)
    {
        extract($params);
        $param["abbrev"] = $varAbbrev;
        $suppressVariable = $this->getVariable($param);
        $result = $this->terminalTransaction->getDatasetSummary($transactionId, false);
        $datasetVariables = $result['data'][0]['variables'];
        $entityId = $_POST['condition'] ?? '';
        $datasetVariableDbId = [];
        foreach ($datasetVariables as $datasetVariable) {
            $datasetVariableDbId[] = $datasetVariable['variableDbId'];
        }
        if ($suppress == 'false') {
            $isSuppressed = 'true';
            $suppressRemarks = null;
        } else {
            $isSuppressed = 'false';
        }
        // get datasetDbIds
        $params = [
            "variableDbId" => "" . $suppressVariable["variableDbId"],
            "isSuppressed" => "equals $isSuppressed",
            "status" => "equals new|equals updated",
            "isVoid" => "equals false"
        ];

        if (!empty($condition)) {
            $params["entityDbId"] = $condition;
        }
        $datasetResponse = $this->terminalTransaction->searchAllDatasets($transactionId, $params, '');

        if ($datasetResponse['success'] && $datasetResponse['totalCount'] > 0) {

            // get dataset IDs
            $datasetDbId = [];
            foreach ($datasetResponse['data'] as $dataset) {
                if (isset($dataset["datasetDbId"])) {
                    $datasetDbId[] = $dataset["datasetDbId"];
                }
            }
            $params = [
                "isSuppressed" => $suppress,
                "suppressRemarks" => $suppressRemarks
            ];
            // update dataset records, isSuppressed=true/false
            $this->transactionDataset->updateMany($datasetDbId, $params);

            // recompute traits
            $params = [
                "paramVariableDbId" =>  "" . $suppressVariable["variableDbId"],
            ];
            $response = $this->formulaParameters->searchAll($params, '', true);

            if ($response['totalCount'] > 0) {
                $variableDbId = [];
                foreach ($response["data"] as $variable) {
                    if (in_array($variable["resultVariableDbId"], $datasetVariableDbId)) {
                        $variableDbId[] = $variable["resultVariableDbId"];
                    }
                }

                $formulaDbId = $response["data"][0]["formulaDbId"];
                $params = [
                    "variableDbId" => $variableDbId,
                    "formulaDbId" => [$formulaDbId]
                ];
                if (!empty($condition)) {
                    $params["entityDbId"] = explode('|', $condition);
                }

                // Exclude suppressed datasets from computations
                $excludedDatasets = $this->terminalTransaction->searchAllDatasets($transactionId, [
                    'isVoid' => 'equals false',
                    'variableDbId' => 'equals ' . $suppressVariable["variableDbId"],
                    'entityDbId' => $entityId,
                    'isSuppressed' => "equals true",
                ])['data'] ?? [];

                $excludedDatasetDbIds = array_unique(array_column($excludedDatasets, 'datasetDbId'));

                // Filter out suppressed datasetDbIds from params
                $params["datasetDbId"] = array_diff($params["datasetDbId"] ?? [], $excludedDatasetDbIds);

                $response = $this->terminalTransaction->variableComputations($transactionId, $params);
            }
            if ($suppress == 'false') {
                $isSuppressed = 'true';
                // get datasetDbIds
                $params = [
                    "variableDbId" => "" . $suppressVariable["variableDbId"],
                    "isSuppressed" => "equals $isSuppressed",
                    "status" => "equals new|equals updated",
                    "isVoid" => "equals false"
                ];

                $datasetResponse = $this->terminalTransaction->searchAllDatasets($transactionId, $params, '');

                if ($datasetResponse['success'] && $datasetResponse['totalCount'] == 0) {
                    //delete suppress rules
                    $params = [
                        "targetVariableDbId" => 'equals '.$suppressVariable["variableDbId"],
                        "status" => 'equals new|equals complete',
                        "transactionDbId" => "equals $transactionId"
                    ];
                    $suppressRulesRecords = $this->suppressionRule->searchAll($params);
                    $rulesDbId = [];
                    foreach ($suppressRulesRecords["data"] as $rules) {
                        if (isset($rules["suppressionRuleDbId"])) {
                            $rulesDbId[] = $rules["suppressionRuleDbId"];
                        }
                    }
                    if (!empty($rulesDbId)) {
                        $response = $this->suppressionRule->deleteMany($rulesDbId);
                    }
                }
            }


            return json_encode($response);
        } else {
            return json_encode($datasetResponse);
        }
    }

    /**
     * Renders summary of data on suppress/unsuppress action
     * @param Array $params parameters for suppressing or unsuppressing records
     * @return Html _suppress_summary view
     */
    public function getSuppressRecordSummary($params)
    {

        extract($params);

        $param["abbrev"] = $varAbbrev;
        $variable = $this->getVariable($param);
        $result = $this->terminalTransaction->getDatasetSummary($transactionId, false);
        $datasetVariables = $result['data'][0]['variables'];
        $computedData = [];
        foreach ($datasetVariables as $datasetVariable) {
            $datasetVariableDbId[] = $datasetVariable['variableDbId'];
        }
        if ($suppress == 'false') {
            $isSuppressed = 'true';
        } else {
            $isSuppressed = 'false';
        }
        $params = [
            "variableDbId" => "" . $variable["variableDbId"],
            "isSuppressed" => "equals $isSuppressed",
            "status" => "equals new|equals updated",
            "isVoid" => "equals false"
        ];
        if (!empty($condition)) {
            $params["entityDbId"] = $condition;
        }
        $response = $this->terminalTransaction->searchAllDatasets($transactionId, $params, 'sort=datasetDbId');

        if ($response['success']) {
            $data[] = [
                "variableDbId" => $variable["variableDbId"],
                "variableLabel" => $variable["label"],
                "isComputed" => false,
                "suppressedCount" => $response['totalCount']
            ];
            $datasetId = [];
            foreach ($response["data"] as $dataset) {
                $datasetId[] = $dataset["entityDbId"];
            }
            $params = [
                "paramVariableDbId" => implode(',', explode('|', $variable["variableDbId"])),
            ];
            $response = $this->formulaParameters->searchAll($params, '', true);

            // get related computed variables
            if ($response['totalCount'] > 0 && !empty($datasetId)) {

                $formulaArray = [];
                foreach ($response["data"] as $variable) {

                    // check if the formula has data in the transaction, else, computation is unnecessary
                    if (in_array($variable["resultVariableDbId"], $datasetVariableDbId)) {

                        $variableDbId[] = $variable["resultVariableDbId"];

                        if (!empty($condition)) {
                            $params = [
                                "variableDbId" => 'equals '. $variable["resultVariableDbId"],
                                "entityDbId" => 'equals '.implode('|equals ', $datasetId),
                                "status" => "equals new|equals updated",
                                "fields" => "dataset.variable_id AS variableDbId|dataset.entity_id AS entityDbId|dataset.status AS status"  
                            ];
                        } else {
                            $params = [
                                "variableDbId" => 'equals '. $variable["resultVariableDbId"],
                                "fields" => "dataset.variable_id AS variableDbId"
                            ];
                        }
                        $response = $this->terminalTransaction->searchAllDatasets($transactionId, $params, 'limit=1&sort=variableDbId:asc');
                        if ($response['success'] && $response['totalCount'] > 0) {
                            if (!in_array($variable["resultVariableDbId"], $formulaArray)) {
                                $formulaArray[] = $variable["resultVariableDbId"];
                                $computedData[] = [
                                    "variableDbId" => $variable["resultVariableDbId"],
                                    "variableLabel" => $variable["resultVariableLabel"],
                                    "isComputed" => true,
                                    "suppressedCount" => $response['totalCount']
                                ];
                            }
                        }
                    }
                }
            }

            $options = [
                'key' => 'variableDbId',
                'allModels' => $data,
            ];
            $computedOptions = [
                'key' => 'variableDbId',
                'allModels' => $computedData,
            ];

            $dataProvider = new ArrayDataProvider($options);
            $computedDataProvider = new ArrayDataProvider($computedOptions);
            if ($suppress == 'false') {
                $isSuppressed = 'true';
            } else {
                $isSuppressed = 'false';
            }
            $defaults = $this->config->getConfigByAbbrev('QUALITY_CONTROL_DEFAULTS');
            return json_encode(Yii::$app->controller->renderAjax('_suppress_summary', [
                'dataProvider' => $dataProvider,
                'computedDataProvider' => $computedDataProvider,
                'isSuppressed' => $suppress,
                'dataLevel' => $dataLevel,
                'threshold' => $defaults['defaults']['suppress_record_threshold']
            ]));
        } else {
            return json_encode([
                "success" => false
            ]);
        }
    }

    /**
     * Renders summary of data on suppress/unsuppress action for suppression rules
     * @param String $variableId target variable ID to be suppressed
     * @param Integer $transactionId transaction ID
     * @param String $suppress true or false
     * @param String $dataLevel plot
     * @return Html _suppress_summary view
     */
    public function getSuppressRuleSummary($variableId, $transactionId, $suppress, $dataLevel)
    {
        $result = $this->terminalTransaction->getDatasetSummary($transactionId, false);
        $datasetVariables = $result['data'][0]['variables'] ?? [];
        $datasetVariableDbId = [];
        $computedData = [];
        foreach ($datasetVariables as $datasetVariable) {
            $datasetVariableDbId[] = $datasetVariable['variableDbId'];
        }

        if ($suppress == 'false') {
            $isSuppressed = 'true';
        } else {
            $isSuppressed = 'false';
        }
        $params = [
            "status" => "equals new|equals complete",
            "transactionDbId" => "equals $transactionId",
            "targetVariableDbId" => "equals $variableId"
        ];

        $response = $this->suppressionRule->searchAll($params);
        $entityIdForAnd = [];
        $entityIdForOr = [];
        $variable = $this->variable->getOne($variableId);
        $variable = $variable['data'];

        if ($response['totalCount'] > 0) {
            foreach ($response['data'] as $rule) {

                if ($rule["operator"] == '=' || $rule["operator"] == 'equals') {
                    $operator = ['equals' => $rule["value"]];
                } else if ($rule["operator"] == '>=') {
                    $operator = ['greater than or equal to' => $rule["value"]];
                } else if ($rule["operator"] == '<=') {
                    $operator = ['less than or equal to' => $rule["value"]];
                } else if ($rule["operator"] == '<') {
                    $operator = ['less than' => $rule["value"]];
                } else if ($rule["operator"] == '>') {
                    $operator = ['greater than' => $rule["value"]];
                } else if ($rule["operator"] == '<>' || $rule["operator"] == 'not equals') {
                    $operator = ['not equals' => $rule["value"]];
                } else if ($rule["operator"] == 'contains' || $rule["operator"] == 'like') {
                    $operator = ['like' => '%' . $rule["value"] . '%'];
                } else if ($rule["operator"] == 'does not contain' || $rule["operator"] == 'not like') {
                    $operator = ['not like' => '%' . $rule["value"] . '%'];
                }
                if ($rule["status"] == "complete") {
                    $params = [
                        "variableDbId" => $rule["factorVariableDbId"],
                        "value" => $operator,
                        "status" => "equals new|equals updated"
                    ];
                } else {
                    $params = [
                        "variableDbId" => $rule["factorVariableDbId"],
                        "value" => $operator,
                        "isSuppressed" => "equals $isSuppressed",
                        "status" => "equals new|equals updated"
                    ];
                }

                $response = $this->terminalTransaction->searchAllDatasets($transactionId, $params, 'sort=datasetDbId');

                if ($response["success"]) {
                    foreach ($response["data"] as $dataset) {
                        if ($rule["conjunction"] == "and") {
                            $entityIdForAnd[$rule["suppressionRuleDbId"]][] = $dataset["entityDbId"];
                        } else {
                            $entityIdForOr[$rule["suppressionRuleDbId"]][] = $dataset["entityDbId"];
                        }
                    }
                    if ($response['totalCount'] == 0 && $rule["conjunction"] == "and") {
                        $entityIdForAnd[$rule["suppressionRuleDbId"]] = [];
                    }
                }
            }
            $resultAnd = [];
            $entityIdForAndCount = count($entityIdForAnd);
            if ($entityIdForAndCount > 1) {

                $resultAnd = call_user_func_array('array_intersect', $entityIdForAnd);
            } else if ($entityIdForAndCount == 1) {
                foreach ($entityIdForAnd as $record) {
                    $resultAnd = $record;
                }
            }
            $resultOr = [];
            $entityIdForOrCount = count($entityIdForOr);
            if ($entityIdForOrCount > 1) {

                $resultOr = call_user_func_array('array_merge', $entityIdForOr);
            } else if ($entityIdForOrCount == 1) {
                foreach ($entityIdForOr as $record) {
                    $resultOr = $record;
                }
            }
            $finalDataset = array_merge($resultAnd, $resultOr);

            if (count($finalDataset) > 0) {
                $params = [
                    "variableDbId" => "". $variableId,
                    "entityDbId" => 'equals '.implode('|equals ', $finalDataset),
                    "isSuppressed" => "equals $isSuppressed",
                    "status" => "equals new|equals updated"
                ];

                $response = $this->terminalTransaction->searchAllDatasets($transactionId, $params, '');
                $data[] = [
                    "variableDbId" => $variableId,
                    "variableLabel" => $variable["label"],
                    "isComputed" => false,
                    "suppressedCount" => $response["totalCount"]
                ];
                $params = [
                    "paramVariableDbId" => implode(',', explode('|', $variableId)),
                ];
                $response = $this->formulaParameters->searchAll($params, '', true);
                if ($response['success'] && $response['totalCount'] > 0) {
                    $formulaArray = [];
                    foreach ($response["data"] as $variable) {
                        if (in_array($variable["resultVariableDbId"], $datasetVariableDbId)) {
                            $params = [
                                "variableDbId" => "" . $variable["resultVariableDbId"],
                                "entityDbId" => "equals ".implode('|equals ', $finalDataset),
                                "status" => "equals new|equals updated"
                            ];

                            $response = $this->terminalTransaction->searchAllDatasets($transactionId, $params, '');
                            if ($response['success'] && !in_array($variable["resultVariableDbId"], $formulaArray)) {
                                $formulaArray[] = $variable["resultVariableDbId"];
                                $computedData[] = [
                                    "variableDbId" => $variable["resultVariableDbId"],
                                    "variableLabel" => $variable["resultVariableLabel"],
                                    "isComputed" => true,
                                    "suppressedCount" => $response['totalCount']
                                ];
                            }
                        }
                    }
                }
            } else {
                $data[] = [
                    "variableDbId" => $variableId,
                    "variableLabel" => $variable["label"],
                    "isComputed" => false,
                    "suppressedCount" => 0
                ];
            }
        } else {
            $data[] = [
                "variableDbId" => $variableId,
                "variableLabel" => $variable["label"] ?? '',
                "isComputed" => false,
                "suppressedCount" => 0
            ];
        }
        $options = [
            'key' => 'variableDbId',
            'allModels' => $data,
        ];

        $computedOptions = [
            'key' => 'variableDbId',
            'allModels' => $computedData,
        ];

        $dataProvider = new ArrayDataProvider($options);
        $computedDataProvider = new ArrayDataProvider($computedOptions);

        $defaults = $this->config->getConfigByAbbrev('QUALITY_CONTROL_DEFAULTS');

        return Yii::$app->controller->renderAjax('_suppress_summary', [
            'dataProvider' => $dataProvider,
            'computedDataProvider' => $computedDataProvider,
            'isSuppressed' => $suppress,
            'dataLevel' => $dataLevel,
            'threshold' => $defaults['defaults']['suppress_by_rule_threshold'] ?? 0

        ]);
    }

    /**
     * Suppress data by rules
     * @param String $variableId target variable ID to be suppressed
     * @param Integer $transactionId transaction ID
     * @param String $suppress true or false
     * @param String $dataLevel plot
     * @return Json success and/or error message
     */
    public function suppressByRule($variableId, $transactionId, $suppress, $dataLevel)
    {
        $result = $this->terminalTransaction->getDatasetSummary($transactionId, false);
        $datasetVariables = $result['data'][0]['variables'];
        $datasetVariableDbId = [];
        $finalDataset = [];
        foreach ($datasetVariables as $datasetVariable) {
            $datasetVariableDbId[] = $datasetVariable['variableDbId'];
        }
        if ($suppress == 'false') {
            $isSuppressed = 'true';
            $params = [
                "variableDbId" => "$variableId",
                "isSuppressed" => "equals $isSuppressed",
                "status" => "equals new|equals updated"
            ];

            $response = $this->terminalTransaction->searchAllDatasets($transactionId, $params, 'sort=datasetDbId');
            if ($response["success"]) {
                foreach ($response["data"] as $dataset) {
                    if (isset($dataset["entityDbId"])) {
                        $finalDataset[] = $dataset["entityDbId"];
                    }
                }
            }
        } else {
            $isSuppressed = 'false';

            $params = [
                "status" => "equals new|equals complete",
                "transactionDbId" => "equals $transactionId",
                "targetVariableDbId" => "equals $variableId"
            ];

            $response = $this->suppressionRule->searchAll($params);
            $entityIdForAnd = [];
            $entityIdForOr = [];
            $suppressRemarks = [];
            foreach ($response['data'] as $rule) {
                $suppressRemarks[] = $rule["remarks"];
                if ($rule["operator"] == '=' || $rule["operator"] == 'equals') {
                    $operator = ['equals' => $rule["value"]];
                } else if ($rule["operator"] == '>=') {
                    $operator = ['greater than or equal to' => $rule["value"]];
                } else if ($rule["operator"] == '<=') {
                    $operator = ['less than or equal to' => $rule["value"]];
                } else if ($rule["operator"] == '<') {
                    $operator = ['less than' => $rule["value"]];
                } else if ($rule["operator"] == '>') {
                    $operator = ['greater than' => $rule["value"]];
                } else if ($rule["operator"] == '<>' || $rule["operator"] == 'not equals') {
                    $operator = ['not equals' => $rule["value"]];
                } else if ($rule["operator"] == 'contains' || $rule["operator"] == 'like') {
                    $operator = ['like' => '%' . $rule["value"] . '%'];
                } else if ($rule["operator"] == 'does not contain' || $rule["operator"] == 'not like') {
                    $operator = ['not like' => '%' . $rule["value"] . '%'];
                }

                $params = [
                    "variableDbId" => $rule["factorVariableDbId"],
                    "value" => $operator,
                    "isSuppressed" => "equals $isSuppressed",
                    "status" => "equals new|equals updated"
                ];

                $response = $this->terminalTransaction->searchAllDatasets($transactionId, $params, 'sort=datasetDbId');

                if ($response["success"]) {
                    foreach ($response["data"] as $dataset) {
                        if ($rule["conjunction"] == "and") {
                            $entityIdForAnd[$rule["suppressionRuleDbId"]][] = $dataset["entityDbId"];
                        } else {
                            $entityIdForOr[$rule["suppressionRuleDbId"]][] = $dataset["entityDbId"];
                        }
                    }
                    if (count($response["data"]) == 0 && $rule["conjunction"] == "and") {
                        $entityIdForAnd[$rule["suppressionRuleDbId"]] = [];
                    }
                }
            }
            $resultAnd = [];

            if (count($entityIdForAnd) > 1) {

                $resultAnd = call_user_func_array('array_intersect', $entityIdForAnd);
            } else if (count($entityIdForAnd) == 1) {
                foreach ($entityIdForAnd as $record) {
                    $resultAnd = $record;
                }
            }
            $resultOr = [];
            if (count($entityIdForOr) > 1) {

                $resultOr = call_user_func_array('array_merge', $entityIdForOr);
            } else if (count($entityIdForOr) == 1) {
                foreach ($entityIdForOr as $record) {
                    $resultOr = $record;
                }
            }

            $finalDataset = array_merge($resultAnd, $resultOr);
        }
        $finalDataset = $finalDataset ?? '0';
        $params = [
            "variableDbId" => "$variableId",
            "entityDbId" => 'equals '.implode('|equals ', $finalDataset),
            "isSuppressed" => "equals $isSuppressed",
            "status" => "equals new|equals updated"
        ];
        $datasetResponse = $this->terminalTransaction->searchAllDatasets($transactionId, $params, 'sort=datasetDbId');
        $datasetDbId = [];
        foreach ($datasetResponse['data'] as $dataset) {
            if (isset($dataset["datasetDbId"])) {
                $datasetDbId[] = $dataset["datasetDbId"];
            }
        }
        $remarks = '';
        if ($suppress == 'true') {
            $remarks = implode(',', $suppressRemarks);
        }
        $params = [
            "isSuppressed" => $suppress,
            "suppressRemarks" => $remarks
        ];
        // update dataset records, isSuppressed=true/false
        $this->transactionDataset->updateMany($datasetDbId, $params);

        $params = [
            "paramVariableDbId" => implode(',', explode('|', $variableId)),
        ];
        $response = $this->formulaParameters->searchAll($params, '', true);
        if ($response['totalCount'] > 0) {
            $variableDbId = [];
            foreach ($response["data"] as $variable) {
                if (in_array($variable["resultVariableDbId"], $datasetVariableDbId)) {
                    $variableDbId[] = $variable["resultVariableDbId"];
                }
            }

            $formulaDbId = $response["data"][0]["formulaDbId"];
            $params = [
                "variableDbId" => [$variableDbId],
                "formulaDbId" => [$formulaDbId]
            ];
            if (!empty($condition)) {
                $params["entityDbId"] = $finalDataset;
            }
            $response = $this->terminalTransaction->variableComputations($transactionId, $params);
        }
        if ($suppress == 'false') {
            //delete suppress rules
            $params = [
                "targetVariableDbId" => "equals $variableId",
                "status" => 'equals new|equals complete',
                "transactionDbId" => "equals $transactionId"
            ];
            $suppressRulesRecords = $this->suppressionRule->searchAll($params);
            $rulesDbId = [];
            foreach ($suppressRulesRecords["data"] as $rules) {
                if (isset($rules["suppressionRuleDbId"])) {
                    $rulesDbId[] = $rules["suppressionRuleDbId"];
                }
            }
            if (!empty($rulesDbId)) {
                $response = $this->suppressionRule->deleteMany($rulesDbId);
            }
        } else {
            //change status into complete
            $params = [
                "targetVariableDbId" => "equals $variableId",
                "status" => 'equals new',
                "transactionDbId" => "equals $transactionId"
            ];
            $suppressRulesRecords = $this->suppressionRule->searchAll($params);
            $rulesDbId = [];
            foreach ($suppressRulesRecords["data"] as $rules) {
                if (isset($rules["suppressionRuleDbId"])) {
                    $rulesDbId[] = $rules["suppressionRuleDbId"];
                }
            }
            if (!empty($rulesDbId)) {
                $response = $this->suppressionRule->updateMany($rulesDbId, ["status" => 'complete']);
            }
        }

        return json_encode($response);
    }

    /**
     * Suppress data by rules
     * @param Integer $variableId target variable ID to be suppressed
     * @param Integer $transactionId transaction ID
     * @param String $dataLevel plot
     * @return Json success and/or error message 
     */
    public function createSuppressRules($variableDbId, $transactionDbId, $dataUnit)
    {
        $dataLevel = '';
        if ($dataUnit == 'plot') {
            $dataLevel = 'plot_data';
        }
        $params = [
            "transactionDbId" => "equals $transactionDbId",
            "targetVariableDbId" => "equals $variableDbId",
            "status" => 'equals new|equals complete',
        ];
        $suppressRulesTotal = $this->suppressionRule->searchAll($params, 'limit=1');
        $suppressRulesTotal = $suppressRulesTotal["totalCount"];

        foreach (Yii::$app->request->post('SuppressionRule') as $record) {
            if (
                empty($record["factorVariableAbbrev"])
                || empty($record["operator"])
                || empty($record["remarks"])

            ) {
                return json_encode(
                    array(
                        "success" => false,
                        "message" => "Please fill up all fields."
                    )
                );
            }
        }
        $first = false;
        foreach (Yii::$app->request->post('SuppressionRule') as $record) {
            $param = [
                "abbrev" => $record["factorVariableAbbrev"]
            ];
            $factorVariable = $this->getVariable($param);
            if (empty($record["conjunction"]) && $suppressRulesTotal == 0 && !$first) {
                $first = true;
                $record["conjunction"] = 'and';
            } else if (empty($record["conjunction"])) {
                $record["conjunction"] = 'or';
            }

            if (
                $factorVariable['dataType'] == 'string'
                || $factorVariable['dataType'] == 'text'
                || $factorVariable['dataType'] == 'character varying'
            ) {
                if ($record["operator"] == '=') {
                    $record["operator"] = 'equals';
                } else if ($record["operator"] == '<>') {
                    $record["operator"] = 'not equals';
                } else if ($record["operator"] == 'contains') {
                    $record["operator"] = 'like';
                } else if ($record["operator"] == 'does not contain') {
                    $record["operator"] = 'not like';
                }
            }

            $params = [
                "transactionDbId" => "equals $transactionDbId",
                "targetVariableDbId" => "equals $variableDbId",
                "factorVariableDbId" => 'equals '.$factorVariable["variableDbId"],
                "operator" => 'equals '.$record["operator"],
                "conjunction" => 'equals '.$record["conjunction"],
                "value" => 'equals '.$record["value"],
            ];

            $suppressRulesRecords = $this->suppressionRule->searchAll($params);

            if (count($suppressRulesRecords["data"]) == 0) {
                $params["records"] = [
                    [
                        "transactionDbId" => "" . $transactionDbId,
                        "targetVariableDbId" => "" . $variableDbId,
                        "factorVariableDbId" => "" . $factorVariable["variableDbId"],
                        "operator" => $record["operator"],
                        "conjunction" => $record["conjunction"],
                        "value" => $record["value"],
                        "remarks" => $record["remarks"],
                        "status" => 'new',
                        "entity" => $dataLevel,
                    ]

                ];
                $response = $this->suppressionRule->create($params);

                if (!$response["success"]) {
                    return json_encode(
                        array(
                            "success" => false,
                            "message" => "Error while saving suppression rule."
                        )
                    );
                }
            }
        }
    }

    /**
     * Get variable for each data unit
     * @param Array $datasetVariables List of all variables in the transaction
     * @return Array variables for each data unit
     */
    public function getDatasetVariables($datasetVariables)
    {
        $dataUnits = [];
        foreach ($datasetVariables as $row) {
            if (!in_array($row['dataUnit'], $dataUnits, true)) {
                array_push($dataUnits, $row['dataUnit']);
            }
        }
        $dataUnitVariables = [];
        foreach ($dataUnits as $dataUnit) {
            $variables = [];
            foreach ($datasetVariables as $row) {
                if ($dataUnit == $row["dataUnit"]) {
                    if (!in_array($row['variableDbId'], $variables, true)) {
                        array_push($variables, $row);
                    }
                }
            }
            $dataUnitVariables[$dataUnit] = $variables;
        }

        return $dataUnitVariables;
    }

    /**
     * Inserts datasets into the transaction in the data terminal
     * @param Integer transactionId transaction identifier
     * @param String variableAbbrev trait variable
     * @param String value trait value
     * @param Array recordIds array of record ids to update
     * @param String idType type of ids (plot/cross)
     * @return Boolean if creation of records is successful or not
     */
    public function insertDataset($transactionId, $variableAbbrev, $value, $recordIds, $idType = 'plot')
    {
        // Assemble prerequisites
        $dataset = [];
        $identifier = $idType . "_id";
        $requestBody = [
            "measurementVariables" => [$variableAbbrev],
            "records" => []
        ];

        // Loop through ids
        foreach ($recordIds as $recordId) {
            // If value is empty, delete existing dataset for the variable (if any)
            if ($value === '' || $value === null) {
                $variableId = $this->getVariable(['abbrev' => "equals $variableAbbrev"])['variableDbId'] ?? 0;

                $dataset = $this->terminalTransaction->searchAllDatasets($transactionId, [
                        "entityDbId" => "equals $recordId",
                        "variableDbId" => "equals $variableId",
                ]);

                if (isset($dataset['datasetDbId'])) {
                    $result = $this->transactionDataset->deleteOne($dataset['datasetDbId']);
                    Yii::debug('Deleted dataset ' . json_encode($result), __METHOD__);
                }
            }else {
                // If not empty or null, add to records
                $requestBody["records"][] = [
                    $identifier => $recordId,
                    strtolower($variableAbbrev) => $value
                ];
            }
        }
        // If records is not empty, add datasets
        if (!empty($requestBody['records'])) {
            // upload data
            $dataset = $this->terminalTransaction->createDataset($transactionId, $requestBody);
            Yii::debug('Created dataset ' . json_encode($dataset), __METHOD__);

            // validate data
            $validationResponse = $this->terminalTransaction->validateDataset($transactionId);
            Yii::debug('Uploaded dataset ' . json_encode($validationResponse), __METHOD__);

        }

        return isset($dataset[0]);
    }

    /**
     * Retrieve occurrence data used in trait calculation
     * @param Integer occurrenceDbId occurrence identifier
     * @param Array plotData uncommitted trait ids
     * @param Array formulaDbIdList occurrence formula ids
     */
    public function getOccurrenceData($occurrenceDbId, $plotData, $formulaDbIdList)
    {

        $occData = $this->occurrenceData->searchAll([
            "occurrenceDbId" => $occurrenceDbId,
            "dataQcCode" => "equals G|equals Q",
        ])['data'];
        $occurrenceData = [];

        $occVarList = array_column($occData, 'variableDbId');

        $formulaParam = [
            'paramVariableDbId' => implode(',', $occVarList),
            'fields' => '
                formula_parameter.param_variable_id AS paramVariableDbId|
                formula_parameter.result_variable_id AS resultVariableDbId|
                formula_parameter.formula_id AS formulaDbId
            ',
            'formulaDbId' => 'equals '.implode('|equals ', $formulaDbIdList),
        ];

        // Filter the list of occurrence data
        if($formulaDbIdList){
            $formulaParam = $this->formulaParameters->searchAll($formulaParam);
            $finalOccDataIds = array_column($formulaParam['data'], 'paramVariableDbId') ?? [];

            //get variable details
            foreach ($occData as $od){
                if (in_array($od['variableDbId'],$finalOccDataIds)) {
                    $occurrenceData[$od['variableDbId']] = [
                        'value' => $od['dataValue'],
                        'label' => $od['variableLabel']
                    ];
                }
            }
        }

        return $occurrenceData;
    }
    
    /**
     * Retrieve headers from configuration
     */
    public function getHeadersFromConfig() {
        $mainHeaders = [];
        $headers = [];
        $configModel = new Config();
        $program = Yii::$app->userprogram->get('abbrev');
        $modifier = "_$program";
        $personModel = Yii::$container->get('app\models\Person');
        $plotSettings = $configModel->getconfigByAbbrev("DOWNLOAD_DATA_COLLECTION_FILES$modifier");
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);

        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $plotSettings = $configModel->getconfigByAbbrev('DOWNLOAD_DATA_COLLECTION_FILES_COLLABORATOR');
        } else if (isset($program) && !empty($program) && !empty($plotSettings)) {
            $plotSettings;
        } else { 
            $plotSettings = $configModel->getconfigByAbbrev('DOWNLOAD_DATA_COLLECTION_FILES_DEFAULT');
        }

        if(isset($plotSettings['DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS'])) {
            $configValue = $plotSettings['DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS'];

            foreach ($configValue as $value) {
                if (in_array(
                    $value['attribute'],
                    ['blockNumber', 'rowBlockNumber', 'colBlockNumber']
                )) {
                    continue;
                }
                if (!strpos($value['attribute'], 'DbId')) {
                    $mainHeaders[] = $value['attribute'];
                }
                if (!strpos($value['abbrev'], '_ID')) {
                    $headers[] = $value['abbrev'];
                }
            }
        }
        
        return [
            'mainHeaders' => $mainHeaders,
            'headers' => $headers,
        ];
    }
}
