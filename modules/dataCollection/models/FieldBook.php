<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\dataCollection\models;

use Yii;
use app\interfaces\models\ICross;
use app\interfaces\models\IEntryList;
use app\interfaces\models\IVariable;

use app\models\Lists;
use app\modules\dataCollection\models\ExportWorkbook;
use app\modules\occurrence\models\FileExport;
use yii\base\Model;
use app\models\Config;

/**
 * Model for Fieldbook
 */
class FieldBook extends Model
{
    public function __construct (
        public ICross $cross,
        public IEntryList $entryList,
        public IVariable $variable,
        public ExportWorkbook $exportWorkbook,
        public Lists $lists,
        $config = []
    ) { parent::__construct($config); }

    /**
     * Retrieve trait info and scale values from working list for Field Book
     *
     * @param Array $traitDbIds list of trait identifiers
     * @param String $entity occurrence, location, or cross list level
     * @return Array $result list of trait abbrevs
     */
    public function getTraitInfoAndScaleValuesFromWorkingList($traitDbIds = [], $entity = 'occurrence'){
        $result = [];
        $addedPrefix = $entity == 'cross list' ?  '_cross' : '';

        // get working list ID
        $workingListDbId = $this->exportWorkbook->getWorkingListDbId($addedPrefix);

        // if input array is empty, get array of trait IDs
        $traitDbIds = empty($traitDbIds) ? $this->exportWorkbook->getTraitsFromList($workingListDbId) : $traitDbIds;

        // get order numbers
        $params = [
            'fields' => 'listMember.data_id AS id | listMember.order_number AS order_number',
        ];
        $listMemberResponse = $this->lists->searchAllMembers($workingListDbId, $params, '', true);
        $data = $listMemberResponse['data'];
        $orderNumbers = [];

        // consolidate order numbers into a dictionary
        foreach ($data as $datum) {
            $orderNumbers[$datum['id']] = $datum['order_number'];
        }

        foreach ($traitDbIds as $index => $traitDbId) {
            $response = Yii::$app->api->getParsedResponse('GET', 'variables/' . $traitDbId . '/scales', null, '', true);
            $traitData = [];

            if (
                $response['success']
                && isset($response['data'])
            )
                $traitData = $response['data'][0];

            //  determine data type
            $dataType = '';
            $numericList = [
                'float',
                'bigint',
                'smallint',
                'integer',
                'numeric',
            ];

            if (!empty($traitData['scales'][0]['scaleValues']))
                $dataType = 'categorical';
            else if (in_array($traitData['dataType'], $numericList))
                $dataType = 'numeric';
            else if ($traitData['dataType'] == 'timestamp'|| $traitData['dataType'] == 'date')
                $dataType = 'date';
            else if ($traitData['dataType'] == 'boolean')
                $dataType = $traitData['dataType'];
            else
                $dataType = 'text';

            // if applicable, format details an categories
            $details = [];
            $categories = [];

            if (!empty($traitData['scales'][0]['scaleValues'])) {
                $scaleValues = $traitData['scales'][0]['scaleValues'];

                foreach ($scaleValues as $scaleValue) {
                    $description = empty($scaleValue['description']) ? '' : '=' . $scaleValue['description'];
                    array_push($details, $scaleValue['value'] . $description);
                    array_push($categories, $scaleValue['value']);
                }

                $details = implode(', ', $details);
                $categories = implode('/', $categories);
            }

            // insert new row of trait info and scale values
            array_push($result,
                [
                    $traitData['abbrev'],
                    $dataType,
                    $traitData['defaultValue'],
                    $traitData['scales'][0]['minValue'],
                    $traitData['scales'][0]['maxValue'],
                    empty($details) ? '"'. $traitData['description'] .'"' : '"'. $traitData['description'] . ' ' . $details . '"',
                    empty($categories) ? '' : $categories,
                    'TRUE',
                    $orderNumbers[$traitDbId],
                ]
            );
        }

        return $result;
    }

    /**
     * Retrieve cross list file content
     * 
     * @param Integer $crossListId entry list identifier
     * @param Array $traitDbIds list of trait identifiers
     */
    public function getCrossListFileContent($crossListId, $traitDbIds){

        $crossAttributes = [];
        $configModel = new Config();

        $program = Yii::$app->userprogram->get('abbrev');
        $modifier = "_$program";
        $abbrev = "_CROSS_SETTINGS";
        $crossSettings = $configModel->getconfigByAbbrev("DC$modifier$abbrev");
        $personModel = Yii::$container->get('app\models\Person');
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);

        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $crossSettings = $configModel->getconfigByAbbrev('DC_COLLABORATOR_CROSS_SETTINGS');
        } else if (isset($program) && !empty($program) && !empty($crossSettings)) {
            $crossSettings;
        } else { 
            $crossSettings = $configModel->getconfigByAbbrev('DC_GLOBAL_CROSS_SETTINGS');
        }

        if(isset($crossSettings['Export']) && !empty($crossSettings['Export'])){
            $export = $crossSettings['Export'];
            
            foreach ($export as $value) {
                $header = $value['header'];
                $attribute = $value['attribute'];
                $crossAttributes[$attribute] = $header;
            }       
        }

        $crosses =  $this->cross->searchAll([
            'entryListDbId' => $crossListId,
            'includeParentSource' => true,
        ])['data'] ?? [];

        $csvContent = [[]];

        // Set headers: row[0]
        foreach ($crossAttributes as $value){
            array_push($csvContent[0], $value);
        }

        // Set succeeding rows starting row[1]
        $row = 1;
        foreach ($crosses as $key => $value) {

            $csvContent[$row] = [];
            foreach ($crossAttributes as $k => $v){
                array_push($csvContent[$row], '"'.$value[$k].'"');
            }
            $row += 1;

        }

        $withTraitAbbrevsMessage = '';
        if (!empty($traitDbIds)) {
            // Get trait info array
            $traitArray = $this->variable->searchAll([
                'fields' => 'variable.abbrev AS abbrev|variable.id AS variableDbId',
                'variableDbId' => 'equals '.implode('|equals ', $traitDbIds)
            ])['data'] ?? [];

            $traits = array_column($traitArray, 'abbrev');

            foreach ($traits as $trait){
                array_push($csvContent[0], $trait);
            }

            $withTraitAbbrevsMessage = ' including trait abbrevs';
        }

        $crossListData = $this->entryList->searchAll([
            'entryListDbId' => "equals $crossListId",
            'fields' => 'entryList.id AS entryListDbId|entryList.entry_list_name AS entryListName',
        ], 'limit=1', false);
        $toExport = new FileExport(FileExport::TRAIT_DATA_COLLECTION_CROSS_LIST, '', time(),
            crossList: $crossListData['data'][0]['entryListName'] ?? '');
        $filename = $toExport->getZippedFilename();

        return [
            'success' => true,
            'filename' => $filename,
            'csvRows' => $csvContent,
            'message' => "<i class='material-icons green-text'>done</i>&emsp; Successfully downloaded cross list for field book{$withTraitAbbrevsMessage}."
        ];
    }

}