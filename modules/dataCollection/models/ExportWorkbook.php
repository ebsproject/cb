<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\dataCollection\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\models\IConfig;
use app\models\Api;
use app\models\BaseModel;
use app\models\EntryListData;
use app\models\ListMember;
use app\models\Lists;
use app\models\PlatformListMember;
use app\models\Scale;
use app\models\ScaleValue;
use app\models\Variable;
use app\modules\crossManager\models\EntryListProtocol;

/**
 * Model for export workbook
 */
class ExportWorkbook extends BaseModel
{
	public function __construct (
        public Api $api,
        public ListMember $listMember,
        public Lists $lists,
        public Scale $scale,
        public ScaleValue $scaleValue,
        public IConfig $configuration,
        public Variable $variable,
        public EntryListData $entryListData,
        public EntryListProtocol $entryListProtocol,
        public PlatformListMember $platformListMember,
        $config = []
    ) { parent::__construct($config); }

	/**
	 * Load variables data provider
	 *
	 * @param Integer $listId working list identifier
	 * @param Array optional $ids list of trait/variable ids to be loaded
	 * @param String optional $pageParams sort parameters
	 * @return mixed array data provider
	 */
	public function loadVariablesProvider($listId, $idList = null, $pageParams = ''){
        
		$listId = !empty($listId) ? $listId : 0;

		if(!empty($idList)){
			$listMemberDbId = 'equals '.implode('|equals ', $idList);
			$listMemberInfo = $this->lists->searchAllMembers($listId, ["listMemberDbId"=>"$listMemberDbId"], $pageParams);
		}else{
			$listMemberInfo = $this->lists->searchAllMembers($listId, null, $pageParams);
		}

		$listMembers = isset($listMemberInfo['data'][0]['listMemberDbId']) ? $listMemberInfo['data'] : [];

		// reorder traits if sorting is enabled
		if(!empty($pageParams) && !empty($listMembers)){
			$variableDbIds = array_column($listMembers, 'variableDbId');
			$listMemberDbIds = array_column($listMembers, 'listMemberDbId');
			$this->listMember->deleteMany($listMemberDbIds);
			$traitDbIds = implode('|', $variableDbIds);
			$this->insertTraits($listId, $traitDbIds, $variableDbIds);

			$listMembers = $this->lists->searchAllMembers($listId, null, $pageParams)['data'];
		}

		$attributes = [
			"variableDbId",
			"orderNumber",
			"label",
			"displayName",
			"scaleValue"
		];

		return new ArrayDataProvider([
			'allModels' => $listMembers,
			'key' => 'orderNumber',
			'sort' => [
				'attributes' => $attributes,
				'defaultOrder' => [
					'orderNumber' => SORT_ASC
				]
			],
			'pagination' => false
		]);
	}

	/**
	 * Reorder traits
	 *
	 * @param String $traitDbIds reorder trait identifiers
	 * @param Array $traitDbIdsArray of trait identifiers
	 * @param String $entity occurrence, location, or cross list level
	 */
	public function reorderTraits($traitDbIds, $traitDbIdsArray, $entity){

		$addedPrefix = $entity == 'cross list' ? '_cross' : '';

		// get working list
		$workingListDbId = $this->getWorkingListDbId($addedPrefix);

		// if working list exists, clear it
		if ($workingListDbId != null) {
			$params = [
				'fields' => 'listMember.id AS listMemberDbId',
			];
			$response = $this->lists->searchAllMembers($workingListDbId, $params, '', true);
			$memberIds = [];

			if (
				$response['success']
				&& isset($response['data'])
			) {
				foreach ($response['data'] as $data) {
					array_push($memberIds, $data['listMemberDbId']);
				}

                if (!empty($memberIds)) $this->listMember->deleteMany($memberIds);
			}
		}

		$this->insertTraits($workingListDbId, $traitDbIds, $traitDbIdsArray);
	}

	/**
	 * Remove selected traits in the working list
	 *
	 * @param Array $traitDbIds list of trait identifiers
	 * @param String $entity occurrence, location, or cross list level
	 */
	public function removeSelectedTraits ($traitDbIds, $entity) {
		$addedPrefix = $entity == 'cross list' ? '_cross' : '';
		$params = [
			'fields' => 'listMember.id | listMember.data_id AS variableDbId',
			'variableDbId' => $traitDbIds,
		];
		$workingListDbId = $this->getWorkingListDbId($addedPrefix);

		Yii::debug('Removing traits '.json_encode(['traitDbIds'=>$traitDbIds, 'workingListDbId'=>$workingListDbId]), __METHOD__);

		$response = $this->lists->searchAllMembers($workingListDbId, $params);
		$data = [];
		$listMemberDbIds = [];

		if (
			isset($response['success'])
			&& $response['success']
			&& isset($response['data'])
		)
			$data = $response['data'];

		foreach ($data as $datum) {
			array_push($listMemberDbIds, $datum['id']);
		}

		if (!empty($listMemberDbIds)) $response = $this->listMember->deleteMany($listMemberDbIds);

		return $response['success'];
	}

	/**
	 * Retrieve saved list tags
	 *
	 * @return $listTags array list of saved list tags
	 */
	public function getListTags(){
		
		$listTags = []; // list tags
		//set api request
		$method = 'POST';
		$path = 'lists-search';
		$filter = 'sort=name&ownershipType=combined';
		$rawData = [
			'type' => 'trait',
			'memberCount' => 'not equals 0'
		];

		$results = Yii::$app->api->getResponse($method, $path, json_encode($rawData), $filter, true);

		if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){

			$results = $results['body']['result']['data'];

			foreach ($results as $result) {
				$listTags[$result['listDbId']] = $result['name'];
			}

		}

        return $listTags;
	}

	/**
	 * Retrieve saved trait protocol tags
	 * based on data level (Occurrence, Location or Cross List level)
	 *
	 * @param $entityId int|string unique entity identifier
	 * @param $entity string entity identifier
	 * @param $program string program code
	 * 
	 * @return $listTags array list of saved trait protocol tags
	 */
	public function getTraitProtocols ($entityId, $entity, $program = 'DEFAULT')
	{
		$listIds = [];

		if ($entity == 'location') {
			// Set api request
			$method = 'GET';
			$path = "locations/$entityId/protocol-data";
			$filter = 'type=trait';
			$rawData = null;

			$response = $this->api->getApiResults(
				$method,
				$path,
				json_encode($rawData),
				$filter,
				true
			);

			if ($response['success'] && !empty($response['data'])) {
				foreach ($response['data'] as $dataIndex => $data) {
					$listIds[$dataIndex] = $data['dataValue'];
				}
			}
		} else if ($entity == 'occurrence') {
			// Set api request
			$method = 'POST';
			$path = "occurrence-data-search";
			$rawData = [
				"fields" => "occurrence_data.occurrence_id AS occurrenceDbId | variable.abbrev AS variableAbbrev | occurrence_data.data_value AS dataValue",
				"occurrenceDbId" => "equals $entityId",
				"variableAbbrev" => "equals TRAIT_PROTOCOL_LIST_ID",
			];

			$response = $this->api->getApiResults(
				$method,
				$path,
				json_encode($rawData),
				'',
				true
			);

			if ($response['success'] && !empty($response['data'])) {
				foreach ($response['data'] as $dataIndex => $data) {
					$listIds[$dataIndex] = $data['dataValue'];
				}
			}
		} else if ($entity == 'cross list') {

			$workingListDbId = $this->getWorkingListDbId('_cross');
			$listIds[$workingListDbId] = $workingListDbId;
		}

        return $listIds;
	}

	/**
	 * Add new traits in the list
	 *
	 * @param int $id integer traits identifier
	 * @param string $type type of $id used for the function
	 * @param string $operation manner of inserting the $id value(s)
	 * @param string $entity occurrence, location or cross list level
	 */
	public function addVariables($id, $type, $operation, $entity = ''){
		// get working list
		$workingListDbId = $this->getWorkingListDbId($entity == 'cross list' ? '_cross' : '');

        // if working list exists, clear it
		if ($operation == 'replace' && $workingListDbId != null) {
            $params = [
                'fields' => 'listMember.id AS listMemberDbId'
            ];
            $response = $this->lists->searchAllMembers($workingListDbId, $params, '', true);
            $memberIds = [];

            if (
                $response['success']
                && isset($response['data'])
            ) {
                foreach ($response['data'] as $data) {
                    array_push($memberIds, $data['listMemberDbId']);
                }

                if (!empty($memberIds)) $this->listMember->deleteMany($memberIds);
            }
		}

		// get variables already added
		$excludedTraits = $this->savedTraitsInWorkingList($workingListDbId);

		$response = null;

		$traitDbIds = '';

		if($type == 'list'){
			// get trait ids from the list
			$idsArr = $this->getTraitsFromList($id, $excludedTraits);
			$traitDbIds = (empty($idsArr) ? '' : implode('|', $idsArr));
		}

		if($type == 'variable'){
			$idsArr = array_diff((array)$id, $excludedTraits);
			$traitDbIds = (empty($idsArr) ? '' : implode('|', $idsArr));
		}

		if (empty($traitDbIds)) {
			$message = "<i class='material-icons orange-text'>warning</i> Nothing to add. All traits are already in the list!";
		} else if ($operation == 'replace'){
			$this->insertTraits($workingListDbId, $traitDbIds, $idsArr);

			$message = "<i class='material-icons green-text'>done</i> Successfully replaced all traits!";
		} else {
			$this->insertTraits($workingListDbId, $traitDbIds, $idsArr);

			$message = "<i class='material-icons green-text'>done</i> Successfully added traits!";
		}

		return [
			'message' => $message
		];
	}

	/**
	 * Pre-load saved traits in the list
     * based on data level (Occurrence, Location, or Cross list level)
	 *
	 * @param $entityId int|string trait protocol identifier
	 * @param $entity string entity (occurrence, location, cross list)
	 * @param $program string program code
     * 
     * @return object object containing status code
	 */
	public function loadSavedVariables ($entityId, $entity, $program = '')
    {
		$addedPrefix = $entity == 'cross list' ? '_cross' : '';
		$statusCode = 0;

		// get working list
		$workingListDbId = $this->getWorkingListDbId($addedPrefix);

        // if working list exists, clear it
		if ($workingListDbId != null) {
			$params = [
				'fields' => 'listMember.id AS listMemberDbId'
			];
			$response = $this->lists->searchAllMembers($workingListDbId, $params, '', true);
			$memberIds = [];

			if (
				$response['success']
				&& isset($response['data'])
			) {
				foreach ($response['data'] as $data) {
					array_push($memberIds, $data['listMemberDbId']);
				}

				if (!empty($memberIds)) $this->listMember->deleteMany($memberIds);
			}
		}

		$listIds = $this->getTraitProtocols($entityId, $entity, $program);

		if(!empty($listIds) && $entity == 'cross list'){
			$this->insertMembersFromEntryListData($listIds[$workingListDbId], $entityId);
		}else{
			$traitDbIds = "";
			$ids = [];
	
			// get variable ids from the list
			foreach ($listIds as $id) {
				$idsArr = $this->getTraitsFromList($id);
				// consolidate trait ids
				$ids = array_merge($ids, $idsArr);
			}
	
			$traitDbIds = (empty($ids) ? '' : implode(' | ', $ids));

			if (!empty($traitDbIds)) {
				// insert traits from location-specific Trait Protocols
				// added $ids for DC UI to follow order based from EM
				$this->insertTraits($workingListDbId, $traitDbIds, $ids);
			} else if (empty($listIds) || empty($traitDbIds)) {
				$statusCode = 1;
			}
		}

		return [
			'statusCode' => $statusCode
		];
	}

	/**
	 * Get ID of the user's working list
	 * 
	 * @param string $addedPrefix optional prefix
	 * @return integer $workingListDbId
	 */
	public function getWorkingListDbId($addedPrefix = '') {
		$userId = Yii::$app->session->get('user.id');
		$abbrevSubstring = "%data_collection$addedPrefix".'_export_working_list_' . $userId . '%';
		$params = [
			'fields' => 'list.id AS listDbId | list.abbrev AS abbrev',
			'abbrev' => $abbrevSubstring,
		];
		$response = $this->lists->searchAll($params, 'showWorkingList=true', true);
		
		return $response['data'][0]['listDbId'] ?? null;
	}

	/**
	 * Insert traits into the working list given a string of trait IDs
	 *
	 * @param integer $workingListDbId working list identifier
	 * @param string $traitDbIds string of trait IDs
	 */
	private function insertTraits($workingListDbId, $traitDbIds, $traitDbIdsArr = null) {
		$params = [
			'fields' => 'variable.id | variable.label AS displayValue',
			'id' => $traitDbIds,
		];
		$response = $this->variable->searchAll($params, '', true);	
		$data = (isset($response['data']) && !empty($response['data'])) ? $response['data'] : [];

		// if traitDbIdsArr is not null, reorder retrieved trait data before insertion to working list
		if ($traitDbIdsArr !== null) {
			$reorderedData = [];

			foreach ($traitDbIdsArr as $traitIndex => $traitDbId) {
				foreach ($data as $variableData) {
					if (intval($traitDbId) === $variableData['id'])
						$reorderedData[$traitIndex] = $variableData;
				}
			}

			$data = $reorderedData;
		}

		if (
			$response['success']
			&& !empty($data)
		)
			$this->lists->createMembers($workingListDbId, $data);
	}

	/**
	 * Get traits from list
	 *
	 * @param integer $id list identifier
	 * @param array $excludedTraits list of excluded trait ids
	 */
	public function getTraitsFromList($id, $excludedTraits = []){

		$traitDbIds = [];
		$listMembers = $this->getListMembers($id);
		if(isset($listMembers) && !empty($listMembers)){
			$traitDbIds = array_diff(array_column($listMembers, 'variableDbId'), $excludedTraits);
		}

        return $traitDbIds;
	}

	/**
	 * Retrieve saved traits in working list
	 *
	 * @param integer $workingListDbId working list identifier
	 * @return array $traitDbIds array of trait identifiers
	 */
	public function savedTraitsInWorkingList($workingListDbId){
		$params = [
			'fields' => 'listMember.data_id AS traitDbId'
		];
		$response = $this->lists->searchAllMembers($workingListDbId, $params, '', true);
		$traitDbIds = [];

		if (
			$response['success']
			&& isset($response['data'])
		)
			foreach ($response['data'] as $data)
				array_push($traitDbIds, $data['traitDbId']);

        return $traitDbIds;
	}

	/**
	 * Retrieve variables tags
	 *
	 * @return $varTags array list of variable tags
	 */
	public function getVariablesTags(){

		$varTags = []; // variable tags
		//set api request
		$method = 'POST';
		$path = 'variables-search';
		$filter = 'sort=label:ASC';
		$rawData = [
			'status' => 'active',
			'type' => 'observation',
			'dataType' => 'not ilike %json%',
			'usage' => 'not ilike %application%',
			'dataLevel' => '%plot%',
		];

		$results = Yii::$app->api->getResponse($method, $path, json_encode($rawData), $filter, true);

		if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){

			$results = $results['body']['result']['data'];

			foreach ($results as $result) {
				$abbrev = $result['abbrev'];
				$abbrevIsValid = !($this->startsWith($abbrev, 'ACTUAL'))
					&& !($this->startsWith($abbrev, 'PLANNED'))
					&& !($this->endsWith($abbrev, '_ADJ_MEAN'))
					&& !($this->endsWith($abbrev, '_ADJ_MEAN_METH'))
					&& !($this->startsWith($abbrev, 'RANK'))
					&& !($this->startsWith($abbrev, 'SITE_MEAN_'))
				;			

				if($abbrevIsValid){

					$varTags[$result['variableDbId']] = $result['label'].' ('.$result['displayName'].')' ;
					
				}
			}

		}
		return $varTags;
	}

	/**
	 * Checks if a string(haystack) starts with a specific string(needle)
	 * 
	 * @param string haystack string to search in
	 * @param string needle string to check
	 * @reference https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
	 */
	public function startsWith($haystack, $needle) {
		return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
	}
	

	/**
	 * Checks if a string(haystack) ends with a specific string(needle)
	 * 
	 * @param string haystack string to search in
	 * @param string needle string to check
	 * @reference https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
	 */
	public function endsWith($haystack, $needle) {
		return substr_compare($haystack, $needle, -strlen($needle)) === 0;
	}

	/**
	 * Save as new variable list
	 *
	 * @param array $ids list of variable identifiers
	 * @param array $attributes list attributes
	 * @param string $entity occurrence, location, or cross list
	 * @return string $errorMessage error message
	 */
	public function saveVariableList($ids, $attributes, $entity){

		$errorMessage = '';
		$listInfo = $this->createList($attributes);

		if (!$listInfo['listDbId']) {
			$errorMessage = $listInfo['errorMessage'];
		} else {
			$workingListDbId = $this->getWorkingListDbId($entity == 'cross list' ? '_cross' : '');
			$ids = empty($ids) ? $this->getTraitsFromList($workingListDbId) : $ids;
			$listId = $listInfo['listDbId'];
			$params = [
				'fields' => 'variable.id | variable.label AS displayValue',
				'id' => 'equals '.implode('|equals ', $ids),
			];

			$response = $this->variable->searchAll($params, '', true);
			$data = $response['data'];
			if (
				$response['success']
				&& isset($data)
				&& !empty($data)	
			){
				// Rearrange variable order based on IdArray
				$reorderedData = [];
				foreach ($ids as $traitIndex => $traitDbId) {
					foreach ($data as $variableData) {
						if (intval($traitDbId) === $variableData['id'])
							$reorderedData[$traitIndex] = $variableData;
					}
				}
				$data = $reorderedData;

				$response = $this->lists->createMembers($listId, $data);
			}

			if(!$response['success']){
				$errorMessage = 'Cannot insert list members using API.';
			}

		}

		return $errorMessage;

	}

	/**
	 * Create a new record of variable list
	 * 
	 * @param $attributes array list attributes
	 * @return $listDbId integer list identifier
	 * 
	 */
	public function createList($attributes){

		extract($attributes);

		$listDbId = false;
		$errorMessage = '';
		//set api request
		$method = 'POST';
		$path = 'lists';
		$rawData = [
			'records' => [
				[
					'abbrev' => $abbrev,
					'name' => $name,
					'description' => $description,
					'displayName' => $displayName,
					'remarks' => $remarks,
					'type' => 'trait',
				]
			]
		];

		$results = Yii::$app->api->getResponse($method, $path, json_encode($rawData));

		if(isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){
			$listDbId = $results['body']['result']['data'][0]['listDbId'];
		}
		else{
			if(isset($results['status']) && $results['status'] == 400){
				$errorMessage =  'Name or Abbrev already exists. Please provide a new one.';
			}else{
				$errorMessage = $results['body']['metadata']['status'][0]['message'] ??  'There seems to be a problem in loading the page';
			}
		}

		return [
			'listDbId' => $listDbId,
			'errorMessage' => Yii::t('app', $errorMessage),
		];


	
	}

	/**
	 * Build study data in workbook
	 *
	 * @param $studyId integer study identifier
	 * @param $studyData array study information
	 * @return $studyDataArr array list of formatted study information
	 */
 	public function buildStudyData($studyId, $studyData){
		
		$study = isset($studyData['name']) ? $studyData['name'] : '';
		$title = isset($studyData['title']) ? $studyData['title'] : '';
		$studyType = isset($studyData['studyType']) ? $studyData['studyType'] : '';
		$shortStudy = isset($studyData['study']) && !empty($studyData['study']) ? $studyData['study'] : $study;

		return [
			'STUDY'=>$study,
			'TITLE'=>$title,
			'OBJECTIVE'=>'',
			'START DATE'=>'',
			'END DATE'=>'',
			'STUDY TYPE'=>$studyType,
			'SHORT STUDY'=>$shortStudy
		];


	}

	/**
	 * Retrieve variable information
	 * 
	 * @param $variables array list of variables
	 * @param $category text category identifier
	 * @return $result array list of variable information
	 */
	public function getVarsData($variables, $category){

		$result = [];
		// build variable string for ordering of variables to be displayed
		

		// build variable array string for getting variables using abbrev
		$varArrStr = implode("','", $variables);
		$varArrStr = "'".$varArrStr."'";

		$fields = ["fields" => "variable.id|variable.abbrev|variable.description|property.name AS property|method.description AS method|variable.label|variable.data_type AS dataType"];
		$variableList = $this->variable->getVariableByAbbrev($variables, $fields);
		
		//build variable data
		foreach ($variables as $value) {
			foreach($variableList as $var){
				if($value == $var['abbrev']){
					$scale = $this->getScales($var['id']);
					$result[] = [
						$category => $var['abbrev'],
						'description' => $var['description'],
						'scale' => $scale,
						'property' => $var['property'],
						'method' => $var['method'],
						'label' => $var['label'],
						'data_type' => $var['dataType'],
					];
				}
			}
		}

		return $result;
	}

	/**
	 * Retrieve and build scale information using variable id
	 * 
	 * @param $variableId integer variable identifier
	 */
	public function getScales($variableId){

		$scaleData = $this->scale->getVariableScale($variableId);
		$scaleValueList = isset($scaleData['scaleValues']) ? $scaleData['scaleValues'] : [];

		$scaleValue = [];
		foreach($scaleValueList as $val){
			$scaleValue[] = isset($val['value']) ? $val['value'] : '';
		}

		$scaleValue = implode(', ', $scaleValue);

		if(empty($scaleValue)){
			$scaleValue = (isset($scaleData['unit']) && !empty($scaleData['unit'])) ? '('.$scaleData['unit'].')' : '';
		}
		else{
			$scaleValue = (isset($scaleData['description']) && !empty($scaleData['description'])) ? $scaleData['description']." ($scaleValue)" : "($scaleValue)";
		}

		return $scaleValue;
	}

	/**
	 * Retrieve list members given a specific list id
	 * 
	 * @param $listId integer list identifier
	 */
	public function getListMembers($listId){

		$result = [];

		//set api request body
		$method = 'POST';
		$path = "lists/$listId/members-search";
		$filter = 'sort=orderNumber';


		$data = Yii::$app->api->getResponse($method, $path, null, $filter, true);

        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
			$result = $data['body']['result']['data'];
		}

		return $result;
	}

    /**
     * Retrieve occurrence data given a specific occurrence id
     *
     * @param integer $occurrenceDbId occurrence identifier
     */
    public function getOccurrence($occurrenceDbId) {
        $method = 'POST';
        $path = 'occurrences-search?limit=1';
        $occurrenceParams = [
            'fields' => 'occurrence.id AS id |'.
                'occurrence.occurrence_name AS occurrenceName |'.
                'occurrence.occurrence_number AS occurrenceNumber |'.
                'experiment.experiment_code AS experimentCode |'.
                'location.location_code AS locationCode ',
            	'id' => "equals $occurrenceDbId",
        ];

        $data = Yii::$app->api->getResponse($method, $path, json_encode($occurrenceParams), '', true);

        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
            $result = $data['body']['result']['data'];
        } else {
            $result = [];
        }

        return $result;
    }

    /**
     * Retrieve location data given a specific location id
     *
     * @param int $locationDbId location identifier
     */
    public function getLocation($locationDbId) {
        $method = 'POST';
        $path = 'locations-search?limit=1';
        $params = [
            'fields' => 'location.id as locationDbId |'.
                'location.location_code as locationCode |'.
                'location.location_name as locationName',
			'locationDbId' => "equals $locationDbId"
        ];

        $data = Yii::$app->api->getResponse($method, $path, json_encode($params), '', true);

        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
            $result = $data['body']['result']['data'];
        } else {
            $result = [];
        }

        return $result;
    }

	/**
	 * Insert list members from trait configuration
	 * 
	 * @param Integer $listId list identifier
	 * @param Integer $entryListDbId entry list identifier
	 */
	public function insertMembersFromEntryListData($listId, $entryListDbId){
		$entryListDataProtocolId = $this->entryListData->searchAll([
			'fields' => "entry_list_data.entry_list_id AS entryListDbId  | " .
				"variable.abbrev AS variableAbbrev | entry_list_data.data_value AS dataValue",
			'entryListDbId' => "equals $entryListDbId",
			'variableAbbrev' => "equals ". $this->entryListProtocol::PROTOCOL_TRAIT,
		])['data'][0]['dataValue'] ?? 0;

		$listMembers = $this->platformListMember->searchAllMembers($entryListDataProtocolId, params:[
			'fields' => 'variable.id | variable.label AS displayValue | variable.abbrev as abbrev'
		])['data'] ?? [];

		$insertedMembers = $this->lists->createMembers($listId, $listMembers);
		

	}
}