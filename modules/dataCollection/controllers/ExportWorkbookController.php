<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\dataCollection\controllers;

use app\modules\occurrence\models\FileExport;
use Yii;

use app\components\B4RController;
use app\controllers\BrowserController;
use app\interfaces\models\IEntryList;
use app\interfaces\models\IOccurrence;
use app\models\Lists;
use app\models\Study;
use app\models\Location;
use app\models\OccurrenceSearch;
use app\modules\dataCollection\models\ExportWorkbook;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
/**
 * Contains functions for data collection export workbook
 */
class ExportWorkbookController extends B4RController
{
    public $defaultFieldBookTraitColumnHeaders= ["trait","format","defaultValue","minimum","maximum","details","categories","isVisible","realPosition"];
    public $defaultVarHeaders = ['DESCRIPTION', 'PROPERTY', 'SCALE', 'METHOD', 'DATA TYPE', 'VALUE', 'LABEL'];
    public $factorVariables = ['ENTNO','GID','DESIGNATION','ENTRY_CODE','PARENTAGE','GENERATION','PLOTNO','PLOT_CODE','REP','FLDROW_CONT','FLDCOL_CONT','BLOCK_NO_CONT','QR_CODE','SOW_ORDER'];
    
    public function __construct(
        $id,
        $module,
        protected BrowserController $browserController,
        protected Location $location,
        protected Lists $lists,
        protected IOccurrence $occurrence,
        protected IEntryList $entryList,
        protected ExportWorkbook $exportWorkbook,
        protected OccurrenceSearch $occurrenceSearch,
        $config=[],
    )
    {
        parent::__construct($id, $module, $config);
    }
    /**
     * Renders data collection export workbook
     *
     * @param String $program current program code
     * @param Integer $locationId location record identifier
     * @param Integer $occurrenceId occurrence record identifier
     * @param Integer $crossListId entry list record identifier
     * 
     * @return Array default index view of export workbook
     */
    public function actionIndex($program, $locationId = null, $occurrenceId = null, $crossListId = null)
    {
        Yii::info('Rendering DC Export tool index page '.json_encode(['program'=>$program, 'occurrenceId'=>$occurrenceId, 'locationId' => $locationId, 'entryListId'=>$crossListId]), __METHOD__);

        $variableTags = [];
        $listTags = [];
        $label = '';
        $entityId = '';
        $addedPrefix = '';
        $entity = '';

        $isAdmin = Yii::$app->session->get('isAdmin');

        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess(
                'EXPERIMENT_MANAGER_DOWNLOAD_FILES',
                'OCCURRENCES',
                usage: 'url'
            );
        }

        // get user identifier
        $userId = Yii::$app->session->get('user.id');

        if(!empty($occurrenceId)){
            $occurrenceParams = [
                "occurrenceDbId" => "equals $occurrenceId",
                "fields" => 'occurrence.id AS occurrenceDbId|occurrence_code AS occurrenceCode'
            ];

            $occurrence = $this->occurrence->searchAll($occurrenceParams, 'limit=1', false);
            $label = $occurrence['data'][0]['occurrenceCode'] ?? '';
            $entity = 'occcurrence';
            $entityId = $occurrenceId;

        } else if (!empty($locationId)) {
            $locationData = $this->location->searchAll([
                'locationDbId' => "equals $locationId",
                'fields' => 'location.id AS locationDbId|location_code AS locationCode'
            ], 'limit=1', false);

            $label = $locationData['data'][0]['locationCode'] ?? '';
            $entity = 'location';
            $entityId = $locationId;

        }else if(!empty($crossListId)){
            $crossListData = $this->entryList->searchAll([
                'entryListDbId' => "equals $crossListId",
                'fields' => 'entryList.id AS entryListDbId|entryList.entry_list_name AS entryListName'
            ], 'limit=1', false);
            $label = $crossListData['data'][0]['entryListName'] ?? '';
            $entityId = $crossListId;
            $entity = 'cross list';
            $addedPrefix = '_CROSS';
        }

        // See if there's a working list for the User
        $abbrevSubstring = "%data_collection$addedPrefix"."_export_working_list_$userId%";
        $params = [
            'fields' => 'list.id AS listDbId | list.abbrev AS abbrev',
            'abbrev' => $abbrevSubstring,
        ];
        
        $response = $this->lists->searchAll($params, 'showWorkingList=true&limit=1', true);
        $data = [];

        if (
            $response['success']
            && isset($response['data'])
        ) {
            $data = $response['data'];
            $hasWorkingList = boolval($response['totalCount']);
        }
        else $hasWorkingList = false;

        $newAbbrev = "DATA_COLLECTION$addedPrefix" ."_EXPORT_WORKING_LIST_$userId" . '_' . $entityId;
        if (!$hasWorkingList || ($hasWorkingList && isset($data[0]) &&  $data[0]['abbrev'] !== $newAbbrev)) {
            // Replace existing working list, if applicable
            if ($hasWorkingList) $this->lists->deleteOne($data[0]['listDbId']);

            // Create a working list
            $requestData = [
                'records' => [
                    [
                        'abbrev' => $newAbbrev,
                        'name' => $newAbbrev,
                        'description' => "This is your working list. If you accessed a different $entity in Data Collection Export tool, a new working list bearing a different $entity ID will replace this list.",
                        'displayName' => $newAbbrev,
                        'remarks' => 'This is User #' . $userId . '\'s working list.',
                        'type' => 'trait',
                        'listUsage' => 'working list',
                    ]
                ]
            ];

            $insertedList = $this->lists->create($requestData);
            //load config if list member is empty
            $listId = $insertedList['data'][0]['listDbId'] ?? 0;
            if($listId){
                if(!empty($crossListId)){
                    $this->exportWorkbook->insertMembersFromEntryListData($listId, $crossListId);
                }
            }else{
                //log and throw if error is encountered
                $errorMessage = $insertedList['message'] ?? "Problem in saving working list: $abbrevSubstring";
                Yii::error($errorMessage, __METHOD__);
                throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
            }
        }

        // get all variables tags
        $variableTags = $this->exportWorkbook->getVariablesTags();

        // get all list tags
        $listTags = $this->exportWorkbook->getListTags();
        
        return $this->render('index',array(
            'variableTags' => $variableTags,
            'listTags' => $listTags,
            'label' => $label ?? '',
            'program' => $program,
            'locationId' => $locationId,
            'occurrenceId' => $occurrenceId,
            'crossListId' => $crossListId,
            'entity' => $entity,
        ));
    }

    /**
     * Load variables to be exported
     *
     * @param String $program program code
     * @param Integer $locationId location identifier
     * @param Integer $occurrenceId occurrence identifier
     * @param Integer $crossListId entry list identifier
     */
    public function actionLoadVariables($program, $locationId, $occurrenceId = null, $crossListId = null) {
        Yii::debug('Loading variables '.json_encode(['program'=>$program,'locationDbId'=>$locationId,'occurrenceDbId'=>$occurrenceId, 'entryListDbId'=>$crossListId]), __METHOD__);
        
        $workingListDbId = $this->exportWorkbook->getWorkingListDbId(!empty($crossListId) ? '_cross' : '');

        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';
        // get current sorting
        $paramSort = '';
        if(isset($_GET['sort'])) {
            $sort = $_GET['sort'][0] != '-' ? $_GET['sort'] : substr($_GET['sort'], 1) . ':desc';
            $paramSort = 'sort=' . $sort;
        } else if (isset($_GET['dp-1-sort'])) {
            $sort = $_GET['dp-1-sort'][0] != '-' ? $_GET['dp-1-sort'] : substr($_GET['dp-1-sort'], 1) . ':desc';
            $paramSort = 'sort=' . $sort;
        }

        $data = $this->exportWorkbook->loadVariablesProvider($workingListDbId, null, $paramSort);

        // Prepare filename for export
        try {
            if (!empty($crossListId)) {
                $crossListData = $this->entryList->searchAll([
                    'entryListDbId' => "equals $crossListId",
                    'fields' => 'entryList.id AS entryListDbId|entryList.entry_list_name AS entryListName'
                ], 'limit=1', false);
                $toExport = new FileExport(FileExport::TRAIT_DATA_COLLECTION_CROSS_LIST, '', time(),
                    crossList: $crossListData['data'][0]['entryListName'] ?? '');
                $filename = $toExport->getZippedFilename();
            } else if (!empty($occurrenceId)) {
                
                // get file name from config by occurrence ID
                $filename = $this->browserController->getFileNameFromConfigByEntityId($occurrenceId, 'TRAIT_DATA_COLLECTION_OCCURRENCE', FileExport::CSV);

                $filename = substr($filename, 0, -4);

            } else {

                // get file name from config by location ID
                $filename = $this->browserController->getFileNameFromConfigByEntityId($locationId, 'TRAIT_DATA_COLLECTION_LOCATION', FileExport::CSV, 'location');

                $filename = substr($filename, 0, -4);
            }
        } catch (yii\base\ErrorException $err) {
            Yii::error($err, __METHOD__);
            $filename = '';
        }

        return $this->render('variables',array(
            'program' => $program,
            'locationId' => $locationId,
            'occurrenceId' => $occurrenceId,
            'crossListId' => $crossListId,
            'fileName' => $filename,
            'dataProvider' => $data,
            'listTags' => $this->exportWorkbook->getListTags(),
        ));

    }

    /**
     * Retrieve list tags
     * 
     * @return json array of variables in the list
     */
    public function actionGetListTags() {

        $tags =  $this->exportWorkbook->getListTags();
        
        return json_encode($tags);
    }

    /**
     * Reorder variables in the list
     *
     * @param String $entity occurrence, location, or cross list level
     */
    public function actionReorderVariables($entity){
        $traitDbIdsArray = isset($_POST['ids']) ? $_POST['ids'] : [];

        $traitDbIds = implode('|', $traitDbIdsArray);

        $this->exportWorkbook->reorderTraits($traitDbIds, $traitDbIdsArray, $entity);

        return json_encode(['success'=>true]);
    }

    /**
     * Remove one or more variables in the list
     * 
     * @param String $entity occurrence, location, or cross list level
     */
    public function actionRemoveVariables($entity){
        $traitDbIds = isset($_POST['ids']) ? $_POST['ids'] : [];
        $traitDbIds = implode('|', $traitDbIds);

        $isSuccessful= false;

        if(isset($traitDbIds) && !empty($traitDbIds)){
            $isSuccessful = $this->exportWorkbook->removeSelectedTraits($traitDbIds, $entity);
        }

        return json_encode([ 'success' => $isSuccessful ]);
    }

    /**
     * Add new variables in the list
     * 
     * @param String $entity occurrence, location, or cross list level
     * @return String $message message for success/failed adding of variables
     */
    public function actionAddVariables($entity = ''){
        $id = isset($_POST['id']) ? $_POST['id'] : null;
        $type = isset($_POST['type']) ? $_POST['type'] : null;
        $operation = isset($_POST['operation']) ? $_POST['operation'] : null;

        $message = $this->exportWorkbook->addVariables($id, $type, $operation, $entity);

        return json_encode($message);
    }

    /**
     * Load saved variables in the lists
     * 
     * @param String $program program code
     * @return Integer $statusCode
     */
    public function actionLoadSavedVariables ($program = '')
    {
        $occurrenceId = $_POST['occurrenceId'] ?? null;
        $locationId = $_POST['locationId'] ?? null;
        $crossListId = $_POST['crossListId'] ?? null;

        if(!empty($crossListId)){
            $entityId = $crossListId;
            $entity = 'cross list';
        }else if(!empty($occurrenceId)){
            $entityId = $occurrenceId;
            $entity = 'occurrence';
        }else{
            $entityId = $locationId;
            $entity = 'location';
        }

        $statusCode = $this->exportWorkbook->loadSavedVariables($entityId, $entity, $program);

        return json_encode($statusCode);
    }

    /**
     * Get saved trait protocols
     *
     * @param String $program program code
     */
    public function actionGetTraitProtocols($program = 'DEFAULT')
    {
        $occurrenceId = $_POST['occurrenceId'] ?? null;
        $locationId = $_POST['locationId'] ?? null;
        $crossListId = $_POST['crossListId'] ?? null;

        if(!empty($crossListId)){
            $entityId = $crossListId;
            $entity = 'cross list';
        }else{
            $entityId = $occurrenceId ?? $locationId;
            $entity = $occurrenceId ? 'occurrence' : 'location';
        }

        $listIds = $this->exportWorkbook->getTraitProtocols($entityId, $entity, $program);

        return json_encode(['list' => $listIds, 'entity' => $entity]);
    }

    /**
     * Save as new list of type variable
     * 
     * @param String $entity occurrence, location, or cross list level
     */
    public function actionSaveVariableList($entity){
        $ids = isset($_POST['ids']) ? $_POST['ids'] : [];
        $attributes = [
            'abbrev' => isset($_POST['abbrev']) ? $_POST['abbrev'] : '',
            'name' => isset($_POST['name']) ? $_POST['name'] : '',
            'displayName' => isset($_POST['displayName']) ? $_POST['displayName'] : '',
            'description' => isset($_POST['description']) ? $_POST['description'] : '',
            'remarks' => isset($_POST['remarks']) ? $_POST['remarks'] : ''
        ];

        $data = $this->exportWorkbook->saveVariableList($ids, $attributes, $entity);

        return json_encode(['msg'=>$data]);
    }

    /**
     * Preview variables when saving new list
     * 
     * @param String $entity occurrence, location, or cross list level
     */
    public function actionRetrieveVariablesPreview($entity){
        $ids = isset($_POST['ids']) ? $_POST['ids'] : null;
        $addedPrefix = $entity == 'cross list' ? '_cross' : '';

        $workingListDbId = $this->exportWorkbook->getWorkingListDbId($addedPrefix);

        // there are variables selected
        if(!empty($ids)){
            Yii::$app->session->set('workbook-selected-vars',$ids);
        }else{
            Yii::$app->session->remove('workbook-selected-vars');
        }
        $data = $this->exportWorkbook->loadVariablesProvider($workingListDbId, $ids, '');

        $htmlData = $this->renderPartial('save_list_preview', ['dataProvider'=>$data]);

        return json_encode([
            'htmlData' => $htmlData
        ]);
    }

    /**
     * Download workbook
     *
     * @param $program text program identifier
     * @param $studyId integer study identifier
     * @param $tbl text temporary table where variables are stored
     */
    public function actionDownload($program, $studyId, $tbl = ''){

        // if there are selected variables, retrieve from session
        $variables = Yii::$app->session->get('workbook-selected-vars');
        // study basic information
        $studyData = Study::getBasicInfo($studyId)['data'];

        // create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // spreadsheet variables
        $firstSheetTitle = 'Study Description'; // first sheet title
        $secondSheetTitle = 'Observation'; // first sheet title

        // set worksheet title
        $spreadsheet->getActiveSheet()->setTitle($firstSheetTitle);

        // set active sheet index to the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();

        // get study basic information
        $studyDataArr = $this->exportWorkbook->buildStudyData($studyId, $studyData);

        // render study data
        $fileName = $this->renderStudy($spreadsheet, $sheet, $studyDataArr);

        // render condition data
        $this->renderCondition($spreadsheet, $sheet, $studyData);

        // render factor data
        $this->renderFactor($spreadsheet, $sheet);

        // render constant data
        $this->renderConstant($spreadsheet, $sheet);

        // render variate data
        $variateHeaders = $this->renderVariate($spreadsheet, $sheet, $tbl, $variables);

        // auto size width of columns
        $this->autoSizeColumns($spreadsheet, 8);

        // render observation sheet
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->setTitle($secondSheetTitle);

        // render factor headers
        $this->renderHeaders($this->factorVariables, '006600', $sheet, $spreadsheet, 1, 1);

        // render variate headers
        $currHighestCol = count($this->factorVariables);
        $this->renderHeaders($variateHeaders, '660066', $sheet, $spreadsheet, 1, $currHighestCol+1);

        $this->renderObservationData($spreadsheet, $sheet, $studyId, $currHighestCol);

        // auto size column
        $totalCols = count($variateHeaders) + $currHighestCol;
        $this->autoSizeColumns($spreadsheet, $totalCols);

        // set first sheet as active
        $spreadsheet->setActiveSheetIndex(0);
        
        // Redirect output to a client's web browser (xls)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'_WORKBOOK.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        exit;
    }

    /**
     * Auto resize column width
     *
     * @param $spreadsheet object spreadsheet
     * @param $totalCols integer total columns
     */
    public function autoSizeColumns($spreadsheet, $totalCols){
        for ($row = 1; $row <= $totalCols; $row++) {
            $spreadsheet->getActiveSheet()->getColumnDimension($this->getExcelColumnName($row))->setAutoSize(TRUE);
        }
    }


    /**
     * Render study basic information
     *
     * @param $spreadsheet object spreadsheet
     * @param $sheet object current sheet
     * @param $studyDataArr array study basic information
     * @return $fileName text file name
     */
    public function renderStudy($spreadsheet, $sheet, $studyDataArr){

        $sheet->getStyle('A1:A6')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('990000'); // maroon

        $sheet->getStyle('A1:A6')->applyFromArray(
            array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                )
            )
        );
        
        $fileName = '';
        $i = 1;
        foreach ($studyDataArr as $key => $value) {
            if($key == 'SHORT STUDY'){
                $fileName = $value;
            }else{
                $spreadsheet->getActiveSheet()->setCellValue('A'.$i,$key,TRUE);
                $spreadsheet->getActiveSheet()->setCellValue('B'.$i,$value,TRUE);
                $i++;
            }
        }

        return $fileName;
    }

    /**
     * Render condition data
     *
     * @param $spreadsheet object spreadsheet
     * @param $sheet object current sheet
     * @param $studyDataArr array study basic information
     */
    public function renderCondition($spreadsheet, $sheet, $studyData){
        
        // condition headers
        $conditionHeadArr = array_merge(['CONDITION'],$this->defaultVarHeaders);
        // render condition headers
        $startRow = $sheet->getHighestRow() + 2; // start row
        $this->renderHeaders($conditionHeadArr, '006600', $sheet, $spreadsheet, $startRow, 1);

        // get condition variables data
        $conditionVariables = ['YEAR','SEASON','ECOSYSTEM','PROGRAM','PLACE'];
        $conditionData = $this->exportWorkbook->getVarsData($conditionVariables, 'condition');

        // get current highest row
        $currHighestRow = $sheet->getHighestRow() ;

        // render condition row
        $conditionDataCount = count($conditionData);
        if ($conditionDataCount > 0) {
            for ($row = 0; $row < $conditionDataCount; $row++) {
                $this->renderRowData('condition', $row, $conditionData, $conditionHeadArr, $sheet, $spreadsheet, $currHighestRow, $studyData);
            }
        }
    }

    /**
     * Render headers
     *
     * @param $headerArr array list of headers
     * @param $headerBgColor text RGB header color
     * @param $sheet object current sheet
     * @param $spreadsheet object spreadsheet
     * @param $startRow integer start row
     * @param $startCol integer start column
     */
    public function renderHeaders($headerArr, $headerBgColor, $sheet, $spreadsheet, $startRow=1, $startCol=1){
        $columnName = $this->getExcelColumnName($startCol); // get column name

        // render headers
        foreach ($headerArr as $key => $value) {
            $key = $key+$startCol;
            $spreadsheet->getActiveSheet()->setCellValue($this->getExcelColumnName($key).$startRow,$value,TRUE);
        }

        $currHighestCol = $sheet->getHighestColumn();
        $currHighestRow = $sheet->getHighestRow() + 1;

        // set styling for header
        $sheet->getStyle($columnName . $startRow . ':' . $currHighestCol . $startRow)
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB($headerBgColor);

        $sheet->getStyle($columnName . $startRow . ':' . $currHighestCol . $startRow)->applyFromArray(
            array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                )
            )
        );

        // set font color for body
        $sheet->getStyle('A' . $currHighestRow . ':' . $currHighestCol . $currHighestRow)->applyFromArray(
            array(
                'font' => array(
                    'color' => array('rgb' => '00000'),
                )
            )
        );
    }

    /**
     * Render factor data
     *
     * @param $spreadsheet object spreadsheet
     * @param $sheet object current sheet
     * @param $studyDataArr array study basic information
     * @return $factorVariables array factor headers
     */
    public function renderFactor($spreadsheet, $sheet){
        
        // factor headers
        $factorHeadArr = array_merge(['FACTOR'],$this->defaultVarHeaders);
        // render factor headers
        $startRow = $sheet->getHighestRow() + 2; // start row
        $this->renderHeaders($factorHeadArr, '006600', $sheet, $spreadsheet, $startRow, 1);

        // get factor varibales data
        // $factorVariables = ['ENTNO','GID','DESIGNATION','ENTCODE','PARENTAGE','GENERATION','PLOTNO','PLOT_CODE','REP','FLDROW_CONT','FLDCOL_CONT','BLOCK_NO_CONT','QR_CODE','SOW_ORDER'];
        $factorData = $this->exportWorkbook->getVarsData($this->factorVariables,'factor');
        
        // get current highest row 
        $currHighestRow = $sheet->getHighestRow();

        // render factor row
        $factorDataCount = count($factorData);
        if ($factorDataCount > 0) {
            for ($row = 0; $row < $factorDataCount; $row++) {
                $this->renderRowData('factor', $row, $factorData, $factorHeadArr, $sheet, $spreadsheet, $currHighestRow, null);
            }
        }
    }

    /**
     * Render variate data
     *
     * @param $spreadsheet object spreadsheet
     * @param $sheet object current sheet
     * @param $tbl text temporary table identifier
     * @param $variables array list of variable identifier 
     * @return $variateVariables array list of variate headers
     */
    public function renderVariate($spreadsheet, $sheet, $tbl, $variables){
        
        // variate headers
        $variateHeadArr = array_merge(['VARIATE'],$this->defaultVarHeaders);
        // render factor headers
        $startRow = $sheet->getHighestRow() + 1; // start row
        $this->renderHeaders($variateHeadArr, '660066', $sheet, $spreadsheet, $startRow, 1);

        // get variate variables data
        $variateVariables = $this->exportWorkbook->getVarAbbrevsFromTempTable($tbl, $variables);
        $variateData = $this->exportWorkbook->getVarsData($variateVariables,'variate');
        
        // get current highest row 
        $currHighestRow = $sheet->getHighestRow();

        // render variate row
        $variateDataCount = count($variateData);
        if ($variateDataCount > 0) {
            for ($row = 0; $row < $variateDataCount; $row++) {
                $this->renderRowData('variate', $row, $variateData, $variateHeadArr, $sheet, $spreadsheet, $currHighestRow, null);
            }
        }

        return $variateVariables;
    }

    /**
     * Render constant data
     *
     * @param $spreadsheet object spreadsheet
     * @param $sheet object current sheet
     */
    public function renderConstant($spreadsheet, $sheet){
        
        // constant headers
        $cosntantHeadArr = array_merge(['CONSTANT'],$this->defaultVarHeaders);
        // render constant headers
        $startRow = $sheet->getHighestRow() + 2; // start row
        $this->renderHeaders($cosntantHeadArr, '660066', $sheet, $spreadsheet, $startRow, 1);
    }

    /**
     * Render observation data
     *
     * @param $spreadsheet object spreadsheet
     * @param $sheet object current sheet
     * @param $studyId integer study identifier
     * @param $maxCol integer maximum column
     */
    public function renderObservationData($spreadsheet, $sheet, $studyId, $maxCol){
        $plotData = $this->exportWorkbook->getPlotsOfStudy($studyId);

        // get current highest row 
        $currHighestRow = $sheet->getHighestRow() - 1;

        // render plot row
        $plotDataCount = count($plotData);
        if ($plotDataCount > 0) {
            for ($row = 0; $row < $plotDataCount; $row++) {
                $this->renderRowData('observation', $row, $plotData, $this->factorVariables, $sheet, $spreadsheet, $currHighestRow, null, $maxCol);
            }
        }
    }

    /**
     * Returns the coresponding excel column.(Abdul Rehman from yii forum)
     * 
     * @param $index integer column identifier
     * @return string excel column name
     */
    public function getExcelColumnName($index) {
        --$index;
        if ($index >= 0 && $index < 26)
            return chr(ord('A') + $index);
        else if ($index > 25)
            return ($this->getExcelColumnName($index / 26)) . ($this->getExcelColumnName($index % 26 + 1));
        else
            throw new Exception("Invalid column # " . ($index + 1));
    }

    /**
     * Render row data
     *
     * @param $category text category to be rendered
     * @param $row integer row count
     * @param $data array data to be rendered
     * @param $sheet object current sheet
     * @param $spreadsheet object spreadsheet
     * @param $currHighestRow integer current highest row
     * @param $studyData array study data
     * @param $maxCol integer maximum column
     */
    public function renderRowData($category, $row, $data, $header, $sheet, $spreadsheet, $currHighestRow, $studyData = null, $maxCol = 8){
        $a = 0;

        for ($i = 0; $i < $maxCol; $i++) {

            $column = strtolower(str_replace(' ', '_', $header[$i]));

            if($column == 'value'){ // if value, display data information
                $col = isset($data[$row][$category]) ? $data[$row][$category] : '';
                $value = isset($studyData[strtolower($col)]) ? $studyData[strtolower($col)] : '';
            }else{
                $value = isset($data[$row][$column]) && !empty($data[$row][$column]) ? $data[$row][$column] : 'not specified';
                if($maxCol != 8 && $value == 'not specified'){
                    $value = '';
                }

                if($column == 'data_type'){ // if data type, process display
                    if(in_array($value, ['float','integer','date'])){
                        $value = 'N'; // numeric
                    }else{
                        $value = 'C'; // character varying
                    }
                }
            }

            $a++;
            $spreadsheet->getActiveSheet()->setCellValue($this->getExcelColumnName($a) . ($row + $currHighestRow), $value, TRUE);
        }
    }
}