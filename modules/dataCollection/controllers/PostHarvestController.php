<?php
/* 
* This file is part of EBS-Core Breeding. 
* 
* EBS-Core Breeding is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* 
* EBS-Core Breeding is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
* GNU General Public License for more details. 
* 
* You should have received a copy of the GNU General Public License 
* along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

namespace app\modules\dataCollection\controllers;

use app\components\B4RController;

// Import models (alphabetical order)
use app\models\Api;
use app\models\Config;
use app\models\Occurrence;
use app\Models\OccurrenceData;
use app\models\Plot;
use app\models\Program;
use app\models\TerminalTransaction;
use app\models\User;
use app\models\Variable;

// From other modules
use app\modules\dataCollection\models\Upload;
use app\modules\printouts\models\PrintoutsModel;
use Yii;

use app\components\GenericFormWidget;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


/**
 * Controller for post harvest
 */
class PostHarvestController extends B4RController
{
    public function __construct($id, $module,
        public Plot $plot,
        public Occurrence $occurrence,
        public PrintoutsModel $printoutsModel,
        public Program $program,
        public Api $api,
        public Variable $variable,
        protected OccurrenceData $occurrenceData,
        protected GenericFormWidget $genericFormWidget,
        protected User $user,
        protected TerminalTransaction $terminalTransaction,
        protected Upload $upload,
        $config=[],
    )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     * @param string $program current program abbrev
     * @return html view
     */
    public function actionIndex($program)
    {
        if (Yii::$app->access->renderAccess("DATA_COLLECTION_INPUT_POST_HARVEST_DATA", "QUALITY_CONTROL", usage: 'url')) {
            $personModel = Yii::$container->get('app\models\Person');
            [
                $role,
                $isAdmin,
            ] = $personModel->getPersonRoleAndType($program);

            // retrieve available printout buttons
            $printouts = $this->retrievePrintoutsConfig($program, $role, $isAdmin);

            // Retrieve grid view configs
            $view = $this->retrieveViewConfig($program, $role, $isAdmin);
            $columns = $view['headers'];

            // Retrieve grid view data
            $gridViewData = Yii::$app->session->get('gridViewData', []);
            $transactionDbId = Yii::$app->session->get('transactionDbId', 0);
            // Map grid view data to column headers
            $mappedData = [];
            foreach ($gridViewData as $data) {
                $row = [];

                foreach ($columns as $column) {
                    if (isset($data[$column]) && is_array($data[$column])) {
                        $row[$column] = $data[$column]['value'];
                    }
                }

                // Format non-trait column headers
                foreach ($data as $key => $value) {
                    if (preg_match('/^[a-zA-Z0-9]*$/', $key)) {
                        $upperSnakeKey = strtoupper(preg_replace('/([a-z])([A-Z])/', '$1_$2', $key));
                        $row[$upperSnakeKey] = $value;
                    }
                }

                // Trait column headers
                foreach ($data as $key => $value) {
                    if (!is_array($value)) {
                        $row[$key] = $value;
                    }
                }

                $mappedData[] = $row;
            }

            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $mappedData,
            ]);

            // default status of search button toggle
            $useSearchButton = 'false';

            return $this->render('index', [
                "program" => $program,
                "printouts" => $printouts['availableTemplates'],
                "reportParameters" => $printouts['reportParameters'],
                "csApiStatus" => $printouts['csApiStatus'],
                "useSearchButton" => $useSearchButton,
                "columns" => $columns,
                "dataProvider" => $dataProvider,
                "browserId" => 'post-harvest-view',
                "transactionDbId" => $transactionDbId
            ]);
        }
    }

    /**
     * Search plots for record using the provided QR code and
     * checks if user has permission on the occurrence the plot is under
     * @return string JSON
     */
    public function actionSearch()
    {
        $plotDbId = $_POST['qr_code'] ?? '';

        // fetch plot records
        $plotParams = [
            'plotDbId' => "equals $plotDbId",
            'fields' => "
                plot.id AS plotDbId |
                plot.plot_code AS plotCode |
                plot.harvest_status AS harvestStatus |
                plot.occurrence_id AS occurrenceDbId |
                occurrence.occurrence_code as occurrenceCode |
                occurrence.experiment_id AS experimentDbId |
                entry.germplasm_id AS germplasmDbId |
                germplasm.designation AS designation |
                entry.id AS entryDbId |
                entry.entry_number AS entryNumber |
                plot.location_id AS locationDbId
            "
        ];        
        $plotRecords = $this->plot->searchAll($plotParams, 'limit=1');
        
        if (!$plotRecords['success'] || !isset($plotRecords['data']) || empty($plotRecords['data'])) {
            return json_encode([
                'success' => false,
                'message' => '<i class="material-icons red-text left">close</i> '. Yii::t('app', 'Plot record does not exist') 
            ]);
        } else {
            // if plot is found
            $data = $plotRecords['data'][0];

            // Check if plot is harvested or not
            $harvestStatus = $data['harvestStatus'] ?? '';
            $occurrenceDbId = $data['occurrenceDbId'] ?? 0;
            $locationDbId = $data['locationDbId'] ?? 0;
            $program = $program ?? Yii::$app->userprogram->get('abbrev');

            // check if user has read and write permission to occurrence this plot is under
            $occurrenceParams = [
                'fields' => '
                    occurrence.id AS occurrenceDbId |
                    occurrence.occurrence_name AS occurrenceName |
                    experiment.experiment_year AS experimentYear | 
                    program.id AS programDbId |
                    program.program_code AS programCode |
                    program.program_name AS program |
                    season.id AS SeasonDbId | 
                    season.season_code AS seasonCode | 
                    season.season_name AS season 
                ',
                'occurrenceDbId' => 'equals ' . $occurrenceDbId
            ];
            $occurrenceRecord = $this->occurrence->searchAll($occurrenceParams, 'permission=write')['data'][0] ?? [];
            $data = array_merge($occurrenceRecord, $data);

            if (empty($occurrenceRecord)) {
                return json_encode([
                    'success' => false,
                    'message' => '<i class="material-icons red-text left">close</i>' . Yii::t('app', 'You do not have the permission to view this plot.')
                ]);
            } elseif ($harvestStatus !== 'COMPLETED') {
                return json_encode([
                    'success' => false,
                    'message' => '<i class="material-icons red-text left">close</i>' . Yii::t('app', 'Failed to retrieve record. Plot is not yet harvested.')
                ]);
            }
            
            // Check plot-data for related metadata
            $plotDataParams = [
                'fields' => '
                    occurrence.id AS occurrenceDbId | 
                    plot.id AS plotDbId |  
                    plot_data.id AS plotDataDbId | 
                    plot_data.variable_id AS variableDbId | 
                    variable.abbrev AS variableAbbrev | 
                    plot_data.data_value AS dataValue
                ',  
                'plotDbId' => 'equals '.$data['plotDbId'],
                'occurrenceDbId' => 'equals '.$occurrenceDbId
            ];
            $searchPlotData = $this->api->getApiResults('POST', 'plot-data-search', json_encode($plotDataParams))['data'] ?? [];

            $plotData = [];
            // Process plot data and merge to data
            foreach($searchPlotData as $pData) {
                $plotData[$pData['variableAbbrev']] = $pData['dataValue'];
            }
            $data = array_merge($plotData, $data);

            // check Occurrence-Data for related metadata
            $occurrenceDataParams = [
                'fields' => '
                    occurrence_data.id AS occurrenceDataDbId | 
                    occurrence_data.occurrence_id AS occurrenceDbId |
                    variable.abbrev AS variableAbbrev | 
                    occurrence_data.data_value AS dataValue
                ',
                'occurrenceDbId' => 'equals '.$occurrenceDbId
            ];
            $searchOccurrenceData = $this->occurrenceData->searchAll($occurrenceDataParams)['data'] ?? [];
            
            $occurrenceData = [];
            // Process occurrence data and merge to data
            foreach($searchOccurrenceData as $oData) {
                $occurrenceData[$oData['variableAbbrev']] = $oData['dataValue'];
            }
            $data = array_merge($occurrenceData, $data);

            // Fetch Person info
            $personModel = Yii::$container->get('app\models\Person');
            [
                $role,
                $isAdmin,
            ] = $personModel->getPersonRoleAndType($program);

            $postHarvestRows = $this->retrieveRowConfigs($program, $role, $isAdmin);

            // Collect 'variable_abbrev' values where 'entity_data_type' is 'trait-data'
            $variableFilter = [];
            $focused = '';
            $deviceList = [];
            foreach ($postHarvestRows as $postHarvestRow) {
                foreach ($postHarvestRow as $attribute) {
                    if (!empty($attribute['entity_data_type']) && $attribute['entity_data_type'] === 'trait-data') {
                        $variableFilter[] = $attribute['variable_abbrev'];
                    }

                    // check which attribute will be focused after searching
                    if (isset($attribute['field_focus']) && $attribute['field_focus'] === true) {
                        $focused = $attribute['variable_abbrev'];
                    }

                    // fetch traits with devices
                    if (isset($attribute['withDevice']) && $attribute['withDevice'] === true){
                        $deviceList[] = $attribute['variable_abbrev'];
                    }
                }
            }

            // Search for variables by their abbreviations
            $variableDbIdFilter = $this->variable->searchAll([
                'abbrev' => 'equals ' . implode('|equals ', $variableFilter),
                'fields' => 'variable.id AS variableDbId|variable.abbrev',
            ])['data'] ?? [];

            $variableFilter = array_column($variableDbIdFilter, 'abbrev', 'variableDbId');

            // Retrieve transaction (if any)
            $transactionDbId = $this->terminalTransaction->searchAll([
                'creatorDbId' => 'equals ' . $this->user->getUserId(),
                'occurrenceDbId' => "equals $occurrenceDbId",
                "status" => [
                    "like" => "uploaded%|in queue|%progress|to be%"
                ],
            ], 'limit=1')['data'][0]['transactionDbId'] ?? 0;

            Yii::$app->session->set('transactionDbId', $transactionDbId);
            // Search datasets associated with the transaction
            $dataset = $this->terminalTransaction->searchAllDatasets($transactionDbId, [
                'variableDbId' => 'equals ' . implode('|equals ', array_keys($variableFilter)),
                'entityDbId' => "$plotDbId",
                'isVoid' => 'false'
            ])['data'] ?? [];

            // Map dataset values to the corresponding variable abbreviations
            $traitData = array_column($dataset, 'value', 'variableDbId');

            foreach ($traitData as $key => $value) {
                $data[$variableFilter[$key]] = $value;
            }

            // generate fields using Generic Form Widget
            $row1 = $this->generateRowFields($postHarvestRows['row1'], $data);
            $row2 = $this->generateRowFields($postHarvestRows['row2'], $data);
            $row3 = $this->generateRowFields($postHarvestRows['row3'], $data);
            
            // Stores grid view data in session
            $gridViewData = $this->retrieveGridData($transactionDbId);
            Yii::$app->session->set('gridViewData', $gridViewData);

            $view = $this->renderAjax('rows', 
                compact(
                    'plotDbId',
                    'occurrenceDbId',
                    'locationDbId',
                    'transactionDbId',
                    'row1',
                    'row2',
                    'row3',
                )
            );

            return json_encode([
                'success' => true,
                'message' => '<i class="material-icons green-text left">check</i>' . Yii::t('app', 'Successfully retrieved plot record.'),
                'data' => $data,
                'view' => $view,
                'focused' => $focused,
                'gridViewData' => $gridViewData,
                'transactionDbId' => $transactionDbId,
                'deviceList' => $deviceList,
            ]);
        }
    }

    /**
     * Retrieves available post harvest printouts for this program
     * @param string $program current program abbrev
     * @param string $role current user role
     * @param string $isAdmin if user is admin
     * @return string JSON
     */
    public function retrievePrintoutsConfig($program, $role, $isAdmin) {
        $configModel = new Config();

        // Define default printouts config
        $csApiStatus = '';
        $templatesCS = [];
        $printoutSettings = [];

        $tool = "DC";
        $modifier = "PROGRAM";
        $suffix = "POST_HARVEST_PRINTOUTS_SETTINGS";
        $configAbbrev = $tool.'_'.$modifier.'_'.$program.'_'.$suffix;
        $printoutSettings = $configModel->getConfigByAbbrev($configAbbrev);

        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $modifier = "ROLE";
            $printoutSettings = $configModel->getconfigByAbbrev($tool.'_'.$modifier.'_COLLABORATOR_'.$suffix);
        } elseif (isset($program) && !empty($program) && !empty($printoutSettings)) {
            $printoutSettings;
        } else { 
            $printoutSettings = $configModel->getconfigByAbbrev($tool.'_GLOBAL_'.$suffix);
        }

        try {
            // Retrieve printouts from CS-API
            $product = "DATA COLLECTION";
            $programInfo = $this->program->getProgramByProgramCode($program);
            $programName = $programInfo['programName'] ?? '';

            // Retrieve program in CS
            $programCS = $this->printoutsModel->getAllPrograms($programName);
            if (isset($programCS['status']) && $programCS['status'] !== 200)
                throw new \Exception(500);
            $programIdCS = $programCS[0]['id'] ?? 0;

            // Retrieve product in CS
            $productCS = $this->printoutsModel->getAllProducts($product);
            if (isset($productCS['status']) && $productCS['status'] !== 200)
                throw new \Exception(500);
            $productIdCS = $productCS[0]['id'] ?? 0;

            // Retrieve templates in CS
            $templatesCS = $this->printoutsModel->getAllTemplates($productIdCS, $programIdCS) ?? [];
            if (isset($templatesCS['status']) && $templatesCS['status'] !== 200)
                throw new \Exception(500);
        } catch(\Exception $e) {
            $csApiStatus = $e->getMessage();
        }

        $availableTemplates = [];
        $templateNames = [];

        // Compare config and templates retrieved from CS
        if (isset($printoutSettings) && array_key_exists('templates', $printoutSettings)) {
            foreach($printoutSettings['templates'] as $printout) {
                foreach($templatesCS as $template) {
                    if (strtolower($printout['template_name']) == strtolower($template['name'])) {
                        $availableTemplates[] = $printout;
                        $templateNames[$template['name']] = $template['name'];
                    }
                }
            }
        }

        // Get the parameters of the reports
        $reportParameters = $this->printoutsModel->getReportParameters(array_keys($templateNames));

        return array(
            'availableTemplates' => $availableTemplates ?? [],
            'reportParameters' => $reportParameters ?? [],
            'csApiStatus' => $csApiStatus ?? ''
        );
    }

    /**
     * Retrieves available post harvest row configs for this program
     * @param string $program current program abbrev
     * @param string $role current user role
     * @param string $isAdmin if user is admin
     * @return array rowConfigs
     */
    public function retrieveRowConfigs($program, $role, $isAdmin) {
        $configModel = new Config();

        // Define default rows config
        $tool = "DC";
        $modifier = "PROGRAM";
        $configAbbrev1 = $tool.'_'.$modifier.'_'.$program.'_POST_HARVEST_ROW_1_SETTINGS';
        $configAbbrev2 = $tool.'_'.$modifier.'_'.$program.'_POST_HARVEST_ROW_2_SETTINGS';
        $configAbbrev3 = $tool.'_'.$modifier.'_'.$program.'_POST_HARVEST_ROW_3_SETTINGS';

        $row1Config = $configModel->getConfigByAbbrev($configAbbrev1);
        $row2Config = $configModel->getConfigByAbbrev($configAbbrev2);
        $row3Config = $configModel->getConfigByAbbrev($configAbbrev3);

        // Retrieve configs based on program or role
        // Row 1
        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $modifier = "ROLE";
            $row1Config = $configModel->getConfigByAbbrev($tool.'_'.$modifier.'_COLLABORATOR_POST_HARVEST_ROW_1_SETTINGS');
        } elseif (isset($program) && !empty($program) && !empty($row1Config)) {
            $row1Config;
        } else { 
            $row1Config = $configModel->getconfigByAbbrev($tool.'_GLOBAL_POST_HARVEST_ROW_1_SETTINGS');
        }

        // Row 2
        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $modifier = "ROLE";
            $row2Config = $configModel->getConfigByAbbrev($tool.'_'.$modifier.'_COLLABORATOR_POST_HARVEST_ROW_2_SETTINGS');
        } elseif (isset($program) && !empty($program) && !empty($row2Config)) {
            $row2Config;
        } else { 
            $row2Config = $configModel->getconfigByAbbrev($tool.'_GLOBAL_POST_HARVEST_ROW_2_SETTINGS');
        }

        // Row 3
        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $modifier = "ROLE";
            $row3Config = $configModel->getConfigByAbbrev($tool.'_'.$modifier.'_COLLABORATOR_POST_HARVEST_ROW_3_SETTINGS');
        } elseif (isset($program) && !empty($program) && !empty($row3Config)) {
            $row3Config;
        } else { 
            $row3Config = $configModel->getconfigByAbbrev($tool.'_GLOBAL_POST_HARVEST_ROW_3_SETTINGS');
        }

        return array(
            "row1" => $row1Config['Values'] ?? [],
            "row2" => $row2Config['Values'] ?? [],
            "row3" => $row3Config['Values'] ?? []
        );
    }

    /**
     * Generate fields based on provided config
     * @param array $rowConfig config retrieved for rendering rows
     * @return string JSON
     */
    public function generateRowFields($rowConfig, $data) {
        $genericForm = $this->genericFormWidget;

        $widgetOptions = [];

        // Process the configs for default value searching
        foreach($rowConfig as $key => $val) {
            // Get values from config
            $targetValue = $val['target_value'] ?? '';

            // If entity_data_type exists in config, use abbrev instead of target_value
            if (array_key_exists('entity_data_type', $val)) {
                $targetValue = $val['variable_abbrev'];
            }

            // Check config default value and if is null or default doesnt exist
            if ((isset($val['default']) && $val['default'] === "") || !array_key_exists('default', $val)) {
                if (isset($data[$targetValue])) {
                    // Set retrieved value as default value
                    $rowConfig[$key]['default'] = $data[$targetValue];
                }
            } elseif (isset($data[$targetValue])) {
                $rowConfig[$key]['default'] = $data[$targetValue];
            }
        }

        $widgetOptions = array_merge($widgetOptions, [
            'config' => $rowConfig,
            'program' => $data['programCode'],
            'mainModel' => 'PlotData'
        ]);

        // Build required fields
        $genericForm->options = $widgetOptions;

        return $genericForm->generateFields()['requiredFields'];
    }

    /**
     * Saves input in post harvest rows
     * @param int plotDbId Plot Id
     * @param int occurrenceDbId Occurrence ID plot is under
     * @param int locationDbId location identifier
     * @param int transactionDbId transaction identifier
     */
    public function actionSave($plotDbId, $occurrenceDbId, $locationDbId, $transactionDbId) {
        // process params
        $postHarvestParams = $_POST['PlotData'];

        $processedParams = $this->processSaveParams($plotDbId, $occurrenceDbId, $locationDbId, $transactionDbId, $postHarvestParams);

        if ($transactionDbId == 0) {
            // Attempt to retrieve transaction after saving
            $transactionDbId = $this->terminalTransaction->searchAll([
                'creatorDbId' => 'equals ' . $this->user->getUserId(),
                'occurrenceDbId' => "equals $occurrenceDbId",
                "status" => [
                    "like" => "uploaded%|in queue|%progress|to be%"
                ],
            ], 'limit=1')['data'][0]['transactionDbId'] ?? 0;
        }
        
        // Stores grid view data in session
        $gridViewData = $this->retrieveGridData($transactionDbId);
        Yii::$app->session->set('gridViewData', $gridViewData);

        return json_encode($processedParams);
    }

    /**
     * Process input in post harvest rows
     * @param int plotDbId Plot Id
     * @param int occurrenceDbId Occurrence ID plot is under
     * @param int locationDbId location identifier
     * @param int transactionDbId transaction identifier
     * @param array $inputParams post harvest input fields
     */
    public function processSaveParams($plotDbId, $occurrenceDbId, $locationDbId, $transactionDbId, $inputParams) {
        // retrieve post harvest row configs
        $message = '';
        $status = '';
        $success = false;

        $row = $measurementHeaders = $updatedFields = [];
        $program = $program ?? Yii::$app->userprogram->get('abbrev');
        $personModel = Yii::$container->get('app\models\Person');
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);
        
        $postHarvestRows = $this->retrieveRowConfigs($program, $role, $isAdmin);
        array_push($row, $postHarvestRows['row1']) ?? [];
        array_push($row, $postHarvestRows['row2']) ?? [];
        array_push($row, $postHarvestRows['row3']) ?? [];
        
        $mergedRows = array_merge(...$row);

        try {
            // Build requests
            foreach($mergedRows as $row) {
                if (!array_key_exists('disabled', $row) || $row['disabled'] === false) {
                    $id = 0;
                    $dataParams = [];
                    $abbrev = $row['variable_abbrev'];
                    $var_type = $row['variable_type'];
                    $entity_data_type = $row['entity_data_type'];
                    $variableDbId = $this->variable->getAttributeByAbbrev($abbrev, 'variableDbId') ?? '';

                    // fetch value from input
                    $updateValue = $inputParams[$abbrev][$var_type][$variableDbId] ?? '';

                    if ($entity_data_type != 'trait-data') {
                        // if non-trait data: occurrence-data and plot-data
                        $searchEndpoint = $entity_data_type.'-search';

                        // Check plot-data for related metadata
                        if ($entity_data_type == 'plot-data') {
                            $id = $plotDbId;
                            $dataParams = [  
                                'plotDbId' => 'equals '.$id,
                                'variableDbId' => 'equals '.$variableDbId
                            ];
                        } elseif ($entity_data_type == 'occurrence-data') {
                            $id = $occurrenceDbId;
                            $dataParams = [  
                                'occurrenceDbId' => 'equals '.$id,
                                'variableDbId' => 'equals '.$variableDbId
                            ];
                        }
                        $searchRecord = $this->api->getApiResults('POST', $searchEndpoint, json_encode($dataParams), 'limit=1')['data'][0] ?? [];

                        // check if data exists and params were used to prevent accidental updating
                        if ($searchRecord && $dataParams) {
                            if ($entity_data_type == 'plot-data') {
                                $updateEndpoint = $entity_data_type. '/' . $searchRecord['plotDataDbId'];
                                $updateParams = [
                                    'dataValue' => $updateValue
                                ];
                            } elseif ($entity_data_type == 'occurrence-data') {
                                $updateEndpoint = $entity_data_type. '/' . $searchRecord['occurrenceDataDbId'];
                                $updateParams = [
                                    'dataValue' => $updateValue
                                ];
                            }
                            // update existing data record
                            $updateRecord = $this->api->getApiResults('PUT', $updateEndpoint, json_encode($updateParams)) ?? [];
                            
                            $status = $updateRecord['status'];
                            if ($status == 200) {
                                $success = true;
                                $message = "<i class='material-icons green-text left'>done</i>" . 
                                    Yii::t('app', 'Successfully updated record/s.');
                            } else {
                                $success = false;
                                $message = "<i class='fa-fw fa fa-warning txt-color-orangeDark'></i>&nbsp;&nbsp;" . 
                                    Yii::t('app','Updating the record failed. Please try again.');
                                break;
                            }
                        } else {
                            // create new record

                            // skip creation if empty value
                            if ($updateValue == '') {
                                continue;
                            }

                            if ($entity_data_type == 'plot-data') {
                                $createParams['records'][] = [
                                    'plotDbId' => "$id",
                                    'variableDbId' => "$variableDbId",
                                    'dataValue' => "$updateValue",
                                    'dataQCCode' => "G"
                                ];
                            } elseif ($entity_data_type == 'occurrence-data') {
                                $createParams['records'][] = [
                                    'occurrenceDbId' => "$id",
                                    'variableDbId' => "$variableDbId",
                                    'dataValue' => "$updateValue",
                                    'dataQCCode' => "G"
                                ];
                            }
                            // create new data record
                            $createRecord = $this->api->getApiResults('POST', $entity_data_type, json_encode($createParams)) ?? [];

                            $status = $createRecord['status'];
                            if ($status == 200) {
                                $success = true;
                                $message = "<i class='material-icons green-text left'>done</i>" .
                                    Yii::t('app', 'Successfully updated record/s.');
                            } else {
                                $success = false;
                                $message = "<i class='fa-fw fa fa-warning txt-color-orangeDark'></i>&nbsp;&nbsp;" .
                                    Yii::t('app', 'Record creation failed. Please try again.');
                                break;
                            }
                        }
                    } else {
                        $measurementHeaders[] = $abbrev;
                        $updatedFields[][strtolower($abbrev)] = $updateValue;
                    }
                }
            }

            if(!empty($measurementHeaders)){
                $oldTransaction = true;

                // If transaction does not exist, create a new one
                if($transactionDbId == 0){

                    $oldTransaction = false;
                    $requestBody = [
                        "records" => [[
                            "locationDbId" => "" . $locationDbId,
                            "action" => "post harvest",
                            "occurrence" => [["occurrenceDbId" => $occurrenceDbId]]
                        ]]
                    ];
                    $transactionDbId = $this->terminalTransaction->create($requestBody)['data'][0]['transactionDbId'] ?? 0;
                }

                // Add plot_id to each updated field
                array_walk($updatedFields, fn(&$field) => $field['plot_id'] = (string) $plotDbId);

                // Create datasets
                $result = $this->upload->createDataset($transactionDbId, $oldTransaction, $measurementHeaders, $updatedFields);

                if ($result["success"]) {
                    $success = true;
                    $message = "<i class='material-icons green-text left'>done</i>" . 
                        Yii::t('app', 'Successfully updated record/s.');
                    // Create the background job record
                    $bgJobResult = $this->upload->createBackgroundJob($transactionDbId);

                    if ($bgJobResult["success"]) {
                        $this->initBgProcess($transactionDbId, $bgJobResult["backgroundJobDbId"] ?? 0);
                    }
                }
            }

            return array(
                'status' => $status,
                'success' => $success,
                'message' => $message
            );
        } catch(\Exception $e) {
            return array(
                'status' => 500,
                'success' => false,
                'message' => $e->getMessage()
            );
        }
    }

    /**
     * Initialize transaction background processing
     * @param int $transactionDbId transaction identifier
     * @param int $backgroundJobDbId bg job identifier
     */
    public function initBgProcess($transactionDbId, $backgroundJobDbId){

        $host = getenv('CB_RABBITMQ_HOST');
        $port = getenv('CB_RABBITMQ_PORT');
        $user = getenv('CB_RABBITMQ_USER');
        $password = getenv('CB_RABBITMQ_PASSWORD');

        $connection = new AMQPStreamConnection($host, $port, $user, $password);

        $channel = $connection->channel();

        $channel->queue_declare(
            'DataCollectionUploader', //queue - Queue names may be up to 255 bytes of UTF-8 characters
            false,  //passive - can use this to check whether an exchange exists without modifying the server state
            true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
            false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
            false   //auto delete - queue is deleted when last consumer unsubscribes
        );

        $msg = new AMQPMessage(
            json_encode([
                "datasetUnit" => 'plot',
                "transactionDbId" => $transactionDbId,
                "backgroundJobDbId" => $backgroundJobDbId,
                "bearerToken" => Yii::$app->session->get('user.b4r_api_v3_token')
            ]),
            array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
        );

        $channel->basic_publish(
            $msg,   //message 
            '', //exchange
            'DataCollectionUploader'  //routing key (queue)
        );
        $channel->close();
        $connection->close();

    }

    /**
     * Retrieves available post harvest view configs for this program
     * @param string $program current program abbrev
     * @param string $role current user role
     * @param string $isAdmin if user is admin
     * @return array headers, number of preview items
     */
    public function retrieveViewConfig($program, $role, $isAdmin) {
        $headers = [];
        $configModel = new Config();

        $tool = "DC";
        $modifier = "PROGRAM";
        $suffix = "POST_HARVEST_VIEW_SETTINGS";
        $configAbbrev = $tool.'_'.$modifier.'_'.$program.'_'.$suffix;
        $viewSettings = $configModel->getConfigByAbbrev($configAbbrev);

        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $modifier = "ROLE";
            $viewSettings = $configModel->getconfigByAbbrev($tool.'_'.$modifier.'_COLLABORATOR_'.$suffix);
        } elseif (isset($program) && !empty($program) && !empty($viewSettings)) {
            $viewSettings;
        } else { 
            $viewSettings = $configModel->getconfigByAbbrev($tool.'_GLOBAL_'.$suffix);
        }

        if(isset($viewSettings['headers'])) {
            $configValue = $viewSettings['headers'];

            foreach ($configValue as $value) {
                $headers[] = $value['variable_abbrev'];
            }
        }

        if (isset($viewSettings['numberOfPreviewItems'])) {
            $numberOfPreviewItems = $viewSettings['numberOfPreviewItems'];
        }

        return[
            'headers' => $headers,
            'numberOfPreviewItems' => $numberOfPreviewItems
        ];
    }

    /**
     * Retrieves grid data by transactionDbId
     * @param string $program current program abbrev
     * @param int $transactionDbId transaction ID
     * @return array datasetRecords
     */
    public function retrieveGridData($transactionDbId) {
        $program = $program ?? Yii::$app->userprogram->get('abbrev');
        $params = '';
    
        // Retrieve the dataset
        $datasetRecords = $this->terminalTransaction->searchAllDatasetsTable($transactionDbId, $params, '')['data'] ?? [];

        // Sort the data by nested modificationTimestamp in descending order
        usort($datasetRecords, function($a, $b) {
            // Get the latest modification timestamps
            $timestampA = $this->getLatestTimestamp($a);
            $timestampB = $this->getLatestTimestamp($b);
            
            return ($timestampA < $timestampB) ? 1 : -1;
        });
        
        $personModel = Yii::$container->get('app\models\Person');
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);

        $view = $this->retrieveViewConfig($program, $role, $isAdmin);
        $numberOfPreviewItems = isset($view['numberOfPreviewItems']) ? $view['numberOfPreviewItems'] : 5;

        // Split array to get the first 5 records
        $filteredDatasetRecords = array_slice($datasetRecords, 0, $numberOfPreviewItems);
        
        return $filteredDatasetRecords;
    }

    /**
     * Function to find latest timestamp from the variables
     * @param array $item from datasetRecord
     * @return string $timestamps latest timestamp found from values
     */
    public function getLatestTimestamp($item) {
        $timestamps = [];
        foreach ($item as $key => $value) {
            if (is_array($value)) {
                $modificationTimestamp = $value['modificationTimestamp'] ?? null;
                $creationTimestamp = $value['creationTimestamp'];
                $timestamps[] = $modificationTimestamp ?? $creationTimestamp;
            }
        }
        // Return the latest timestamp
        return max($timestamps);
    }
}
