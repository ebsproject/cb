<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dataCollection\controllers;

use app\components\B4RController;
use app\modules\dataCollection\models\FieldBook;
use Yii;

/**
 * Controller for exporting plots on location
 */
class LocationController extends B4RController
{
    public $defaultColumnHeaders = ["ROW_ID", "OCCURENCE_CODE", "LOCATION_CODE", "PLOT_CODE", "PLOT_NUMBER", "PLOT_TYPE", "REP", "DESIGN_X", "DESIGN_Y", "PA_X", "PA_Y", "FIELD_X", "FIELD_Y"];

    /**
     * Exports location plots in CSV format
     *
     * @param $program text program identifier
     * @param $studyId integer study identifier
     * @param $tbl text temporary table where variables are stored
     */
    public function actionDownloadLocationPlots($locationId)
    {
        $locationData = FieldBook::getLocationPlots($locationId);
        if (!empty($locationData)) {

            $file = $locationData[0]['location_code'] . '.csv';
            $fileRoot = Yii::getAlias("@webroot") . '/files' . '/' . trim($file);
            $fp = fopen($fileRoot, 'w');

            fputcsv($fp, $this->defaultColumnHeaders, ',');

            foreach ($locationData as $line) {
                fputcsv($fp, $line, ',');
            }

            fclose($fp);

            if (file_exists($fileRoot)) {  // trigger successful download
                Yii::$app->response->sendFile($fileRoot);
                return Yii::$app->end();
            } else {    // file is not created/ does not exist
                Yii::$app->user->setFlash('error', '<i class="fa-fw fa fa-times"></i>There seems to be a problem with the file you were downloading.');
            }
        } else {
            Yii::$app->user->setFlash('error', '<i class="fa-fw fa fa-times"></i>There is no plot in the location.');
        }
    }
}
