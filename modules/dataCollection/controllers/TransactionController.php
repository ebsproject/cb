<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dataCollection\controllers;


use app\models\Config;
use app\models\SuppressionRule;
use app\models\TerminalBackgroundProcess;
use app\models\TerminalTransaction;
use app\models\Variable;
use app\models\BackgroundJob;
use app\models\Formula;
use app\modules\dataCollection\models\Transaction;
use app\components\B4RController;
use app\models\FormulaParameters;
use app\interfaces\models\IProgram;
use app\interfaces\models\IWorker;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

use Yii;
use yii\caching\TagDependency;

/**
 * Controller for Data Terminal Transaction
 * 
 */
class TransactionController extends B4RController
{

    public function __construct(
        $id,
        $module,
        //protected models in alphabetical order
        protected Config $configuration,
        protected FormulaParameters $formulaParameters,
        protected SuppressionRule $suppressionRule,
        protected TerminalBackgroundProcess $terminalBackgroundProcess,
        protected TerminalTransaction $terminalTransaction,
        protected Transaction $transaction,
        protected Variable $variable,
        protected BackgroundJob $backgroundJob,
        protected IProgram $program,
        protected Formula $formula,
        protected IWorker $worker,


        $config=[],
    )
    {
        parent::__construct($id, $module, $config);
    }
    /**
     * Displays the quality control pages of a transaction
     * @param Integer $id Transaction ID
     * @param String $program Program
     * @return Html
     */
    public function actionView($id, $program)
    {   
        $params = [
            'transactionDbId' => "equals $id",
            'fields' => 'transaction.id AS transactionDbId|location.location_name AS locationName|transaction.location_id AS locationDbId|transaction.status|transaction.creator_id as creatorDbId|occurrence',
        ];
        $transaction = $this->terminalTransaction->searchAll($params, '');

        if ($transaction['totalCount'] == 0) {
            Yii::$app->session->setFlash('error', "The transaction you are trying to access does not exist.");
            return $this->redirect(['/dataCollection/terminal', "program" => $_GET["program"]]);
        }
        if ($transaction['data'][0]['status'] == 'committed') {
            Yii::$app->session->setFlash('error', "The transaction you are trying to access is already committed.");
            return $this->redirect(['/dataCollection/terminal', "program" => $_GET["program"], "TerminalTransaction[transactionDbId]" => $id]);
        }

        if(Yii::$app->access->renderAccess("QUALITY_CONTROL_DATA","QUALITY_CONTROL", $transaction['data'][0]['creatorDbId'], usage:'url')){

            $result = $this->terminalTransaction->getDatasetSummary($id, false);
            $variableList = $result['data'][0]['variables'] ?? [];
            $variables = $this->transaction->getDatasetVariables($variableList);

            $datasetSummaryDataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $variableList,
                'pagination' => false
            ]);
            $defaults = $this->configuration->getConfigByAbbrev('QUALITY_CONTROL_DEFAULTS');
            //get occurrence Data variables
            $occurrenceDbId = $transaction['data'][0]['occurrence'][0]['occurrenceDbId'] ?? 0;
            $formulaParams = [];
            if(isset($_GET['Formula'])){
                $formulaParams = array_filter($_GET['Formula'], 'strlen');
            }
            $request = Yii::$app->request;
            $filters = $this->formula->getFilters($request->get());

            // filter formulas
            $transactionFormula = $transaction['data'][0]['occurrence'][0]['formulaDbId'] ?? [];
            if(!empty($transactionFormula)){
                $formulaParams['formulaDbId'] = 'equals '.implode('|equals ', $transactionFormula);
            }

            $dataProvider = $this->formula->search($formulaParams, $filters);
            $formulaModel = $this->formula;
            $selectedFormula = [];
            $resultVariables = [];
            $transactionResultVariable = $transaction['data'][0]['occurrence'][0]['resultVariable'] ?? [];
            $transactionVariable = $transaction['data'][0]['occurrence'][0]['variableDbIds'] ?? [];
            foreach ($transactionResultVariable as $value) {
                if (strpos($value, '-') !== false) {
                    $resultFormulaId = explode("-", $value);
                    $resultVariables[] = $resultFormulaId[0];
                    $selectedFormula[] = $resultFormulaId[1];
                } else {
                    $resultVariables[] = $value;
                }
            }

            $occurrenceData = $this->transaction->getOccurrenceData($occurrenceDbId, array_column($variableList, 'variableDbId'), $transactionFormula);

            return $this->render('index', [
                'variables' => $variableList,
                'dataUnitVariables' => $variables,
                'datasetSummaryDataProvider' => $datasetSummaryDataProvider,
                'transactionId' => $id,
                'locationName' => $transaction['data'][0]['locationName'] ?? '',
                'locationId' => $transaction['data'][0]['locationDbId'] ?? 0,
                'commitThreshold' => $defaults['defaults']['commit_threshold'] ?? 100,
                'suppressRecordThreshold' => $defaults['defaults']['suppress_record_threshold'] ?? 100,
                'suppressByRuleThreshold' => $defaults['defaults']['suppress_by_rule_threshold'] ?? 100,
                'downloadDatasetThreshold' => $defaults['defaults']['download_dataset_threshold'] ?? 100,
                'count' => $variableList[0]['count'] ?? 0,
                'creatorDbId' => $transaction['data'][0]['creatorDbId'] ?? 0,
                'program' => $program,
                'occurrenceData' => $occurrenceData,
                'transactionResultVariable' => $transactionResultVariable,
                'selectedFormula' => $selectedFormula ?? [],
                'resultVariables' => $resultVariables ?? [],
                'formulaModel' => $formulaModel,
                'dataProvider' => $dataProvider,
                'status' => $transaction['data'][0]['status'] ?? '',
                'transactionVariable' => $transactionVariable,
                'transactionFormula' => $transactionFormula,
            ]);
        }
    }

    /**
     * Deletes the transaction 
     * @param Integer $id transaction ID
     * @return Null     
     */
    public function actionDelete($id)
    {

        $transactionModel = new TerminalTransaction();
        $transactionModel->deleteOne($id);

        return;
    }

    /**
     * Download transaction dataset
     * @return File
     */
    public function actionDownloadCommittedDataset()
    {
        $id = $_GET["id"];
        $params = [
            'transactionDbId' => "equals $id",
            'fields' => 'transaction.id AS transactionDbId|location.location_name AS locationName'
        ];
        $transaction = $this->terminalTransaction->searchAll($params, '');

        if ($transaction['status'] !== 200) {
            Yii::$app->session->setFlash('error', $transaction['message']);
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        $locationCode = $transaction['data'][0]['locationName'] ?? '';
        $file = $locationCode . '-' . $id . '.csv';

        $fileRoot = Yii::$app->basePath . '/files' . '/' . trim($file);

        $params = [
            "status" => "new|updated",
            "isVoid" => "false"
        ];

        $response = $this->terminalTransaction->searchAllDatasetsTable($id, $params, '');

        if ($response['success']) {
            $json = $response['data'];
            $fp = fopen($fileRoot, 'w');

            $result = $this->terminalTransaction->getDatasetSummary($id, false);
            $excludeColumns = [
                'entityDbId',
                'entryCode',
                'entryName',
                'entryNumber',
                'entryType',
                'germplasmCode',
                'germplasmName',
                'orderNumber',
                'parentage',
                'totalCount',
                'transactionDbId',
            ];

            foreach ($result['data'][0]['variables'] as $variables) {
                if ($variables["commitCount"] > 0) {
                    $variableHeaders[] = $variables['abbrev'];
                } else {
                    $excludeColumns[] =  $variables['abbrev'];
                }
            }
            $headersData = $this->transaction->getHeadersFromConfig();
            $mainHeaders = $headersData['mainHeaders'];
            $headers = $headersData['headers'];
            $headers = array_merge($headers, $variableHeaders);
            $attributes = array_merge($mainHeaders, $variableHeaders);
            fputcsv($fp, $headers, ';');
            foreach ($json as $line) {
                $csvLine = [];
                foreach ($attributes as $key) {
                    if (in_array($key, $mainHeaders)) {
                        $csvLine[$key] = $line[$key] ?? null;
                    } elseif (!in_array($key, $excludeColumns)) {
                        $csvLine[$key] = isset($line[$key]['value']) ? $line[$key]['value'] : null;
                    }
                }

                fputcsv($fp, $csvLine, ';');
            }
            fclose($fp);

            if (file_exists($fileRoot)) {  // trigger successful download
                Yii::$app->response->sendFile($fileRoot);
                Yii::$app->end();
            } else {    // file is not created/ does not exist

                Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-times"></i>There seems to be a problem with the file you were downloading.');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        } else {

            Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-times"></i>There seems to be a problem with the file you were downloading.');
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
    }

    /**
     * Commits the transaction in the background
     * @param Integer $id transaction ID
     * @return Json success and/or error message
     */
    public function actionCommitBackground($id)
    {
        $userId = Yii::$app->session->get('user.id');

        $backgroundJobParams = [
            "workerName" => "QualityControlCommitter",
            "description" => "Commit records for transaction ID: {$id}",
            "entity" => "TERMINAL_TRANSACTION",
            "entityDbId" => $id,
            "endpointEntity" => "TERMINAL_TRANSACTION",
            "application" => "QUALITY_CONTROL",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId
        ];
        $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);

        // update the transaction status
        $this->terminalTransaction->updateOne($id, ['status' => "to be committed"]);

        if ($transaction["status"] != 200) {

            return json_encode([
                "success" => false,
                "error" => "Error while creating background process record. Kindly contact Administrator."
            ]);
        }
        $backgroundJobDbId = $transaction['data'][0]["backgroundJobDbId"];

        $host = getenv('CB_RABBITMQ_HOST');
        $port = getenv('CB_RABBITMQ_PORT');
        $user = getenv('CB_RABBITMQ_USER');
        $password = getenv('CB_RABBITMQ_PASSWORD');

        $connection = new AMQPStreamConnection($host, $port, $user, $password);

        $channel = $connection->channel();

        $channel->queue_declare(
            'QualityControlCommitter', //queue - Queue names may be up to 255 bytes of UTF-8 characters
            false,  //passive - can use this to check whether an exchange exists without modifying the server state
            true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
            false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
            false   //auto delete - queue is deleted when last consumer unsubscribes
        );

        $msg = new AMQPMessage(
            json_encode([
                "transactionDbId" => $id,
                "backgroundJobDbId" => $backgroundJobDbId,
                "accessToken" => Yii::$app->session->get('user.b4r_api_v3_token'),
            ]),
            array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
        );

        $channel->basic_publish(
            $msg,   //message 
            '', //exchange
            'QualityControlCommitter'  //routing key (queue)
        );
        $channel->close();
        $connection->close();

        // Invalidate related cached data that can be found in EM View Occurrence Plots
        TagDependency::invalidate(Yii::$app->cache, 'plot-data-table-records');

        return json_encode(array("success" => true, "transactionId" => $id));
    }

    /**
     * Commits the transaction 
     * @param Integer $id transaction ID
     * @return Json success and/or error message
     */
    public function actionCommit($id)
    {

        $response = $this->transaction->commitPlotData($id);
        if ($response["success"]) {
            // Invalidate related cached data that can be found in EM View Occurrence Plots
            TagDependency::invalidate(Yii::$app->cache, 'plot-data-table-records');

            $response = $this->terminalTransaction->updateOne($id, ["status" => "committed"]);
        }
        return json_encode($response);
    }
    /**
     * Renders summary of data on suppress/unsuppress action
     * @param Integer $filter_id transaction_filter ID
     * @return html
     */
    public function actionGetSuppressRecordSummary()
    {

        $transactionId = $_POST["transactionId"];
        $varAbbrev = $_POST["variableAbbrev"];
        $suppress = $_POST["suppress"];
        $condition = $_POST["condition"];
        $dataLevel = $_POST["dataLevel"];

        return $this->transaction->getSuppressRecordSummary([
            'transactionId' => $transactionId,
            'varAbbrev' => $varAbbrev,
            'suppress' => $suppress,
            'condition' => $condition,
            'dataLevel' => $dataLevel
        ]);
    }

    /**
     * Suppressing by record
     * @return Json success and/or error message
     */
    public function actionSuppressRecord()
    {
        $transactionId = $_POST["transactionId"];
        $varAbbrev = $_POST["variableAbbrev"];
        $suppress = $_POST["suppress"];
        $suppressRemarks = $_POST["remarks"];
        $condition = $_POST["condition"];
        $dataLevel = $_POST["dataLevel"];

        return $this->transaction->suppressRecord([
            'transactionId' => $transactionId,
            'varAbbrev' => $varAbbrev,
            'suppress' => $suppress,
            'suppressRemarks' => $suppressRemarks,
            'condition' => $condition,
            'dataLevel' => $dataLevel
        ]);
    }
    /**
     * Renders suppressed rules of a variable
     * @return view 
     */
    public function actionGetSuppressionRules()
    {
        $variableId = $_POST["variableId"];
        $variableAbbrev = $_POST["variableAbbrev"];
        $transactionId = $_POST["transactionId"];

        return $this->renderAjax('_suppress_rules', [
            "transactionId" => $transactionId,
            "variableAbbrev" => $variableAbbrev,
            "variableId" => $variableId
        ]);
    }

    /**
     * Retrieve variable scale
     * @return Json if has scale, scale values, operator, dataType
     */
    public function actionGetVariableScale()
    {
        $scaleValues = [];
        $hasScale = false;

        $variableDbId = $_GET["variableDbId"];
        $params["variableDbId"] = $variableDbId;
        $variable = $this->transaction->getVariable($params);
        $dataType = $variable["dataType"];
        if (!empty($variable['scaleDbId'])) {
            $response = $this->variable->getVariableScales($variableDbId);
            if (!empty($response)) {
                foreach ($response["scaleValues"] as $scale) {
                    $description = empty($scale['description']) ? '' : '=' . $scale['description'];
                    $scaleValues[$scale["value"]] = $scale["value"] . $description;
                    $hasScale = true;
                }
            }
        }
        if ($dataType == 'float' || $dataType == 'integer' || $dataType == 'double precision' || $dataType == 'date') {

            $operator = ["=" => "=", "<>" => "<>", "<" => "<", ">" => ">", "<=" => "<=", ">=" => ">="];
        } else if ($dataType == 'string' || $dataType == 'character varying' || $dataType == 'text') {

            $operator = ["equals" => "equals", "not equals" => "not equals"];
        }

        return json_encode([
            "hasScale" => $hasScale,
            "values" => $scaleValues,
            "operator" => $operator,
            "dataType" => $dataType,
            "variable" => $variable
        ]);
    }

    /**
     * Renders new row in suppress rule form
     * @return view 
     */
    public function actionAddRule()
    {
        if (isset($_POST["transactionId"]) && isset($_POST["rowNum"]) && isset($_POST["dataType"]) && isset($_POST["firstAbbrev"]) && isset($_POST["firstLabel"]) && isset($_POST["dataLevel"])) {

            $transactionId = $_POST["transactionId"];
            $rowNum = $_POST["rowNum"];
            $dataType = $_POST["dataType"];
            $firstAbbrev = $_POST["firstAbbrev"];
            $firstLabel = $_POST["firstLabel"];
            $dataLevel = $_POST["dataLevel"];

            return $this->renderAjax('_new_suppression_rule', [
                "transactionId" => $transactionId,
                "label" => $firstLabel,
                "abbrev" => $firstAbbrev,
                "dataType" => $dataType,
                "dataLevel" => $dataLevel,
                "rowNum" => $rowNum
            ]);
        } else {

            throw new \yii\web\HttpException(500, "Error in adding new condition");
        }
    }
    /**
     * Creates new TransactionFilter model
     * add rules in the suppress form
     * @return mixed
     */
    public function actionGetSuppressionRulesSummary()
    {
        $variableDbId = Yii::$app->request->post('variableDbId');
        $transactionDbId = Yii::$app->request->post('transactionDbId');
        $dataUnit = Yii::$app->request->post('dataLevel');
        $suppress = Yii::$app->request->post('suppress');
        if (!empty(Yii::$app->request->post('SuppressionRule'))) {
            $this->transaction->createSuppressRules($variableDbId, $transactionDbId, $dataUnit);
        }
        return json_encode(
            array(
                "success" => true,
                "message" => $this->transaction->GetSuppressRuleSummary(
                    $variableDbId,
                    $transactionDbId,
                    $suppress,
                    $dataUnit
                )
            )
        );
    }
    /**
     * Suppress data by rules
     * @param Integer $id transaction ID
     * @return Json success and/or error message
     */
    public function actionSuppressByRules()
    {
        $variableDbId = Yii::$app->request->post('variableDbId');
        $transactionDbId = Yii::$app->request->post('transactionDbId');
        $dataUnit = Yii::$app->request->post('dataLevel');
        $suppress = Yii::$app->request->post('suppress');
        return json_encode($this->transaction->suppressByRule($variableDbId, $transactionDbId, $suppress, $dataUnit));
    }

    /**
     * Deletes suppression rule
     * @param Integer $id suppressionRule ID
     * @return Json success and/or error message
     */
    public function actionDeleteSuppressionRule($id)
    {
        $response = $this->suppressionRule->deleteOne($id);
        return json_encode($response);
    }

    /**
     * Suppress/unsuppress record in the background
     * @param Integer $id transaction ID
     * @return Json success and/or error message
     */
    public function actionSuppressRecordBackground()
    {
        $variableDbId = Yii::$app->request->post('variableDbId');
        $transactionDbId = Yii::$app->request->post('transactionId');
        $suppress = Yii::$app->request->post('suppress');
        $entityDbId = $_POST["entityDbId"];
        $dataLevel = $_POST["dataLevel"];
        $suppressRemarks = $_POST["suppressRemarks"];
        $userId = Yii::$app->session->get('user.id');

        $backgroundJobParams = [
            "workerName" => "QualityControlSuppressor",
            "description" => "Suppression of records for transaction ID: {$transactionDbId}",
            "entity" => "TERMINAL_TRANSACTION",
            "entityDbId" => $transactionDbId,
            "endpointEntity" => "TERMINAL_TRANSACTION",
            "application" => "QUALITY_CONTROL",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId
        ];
        $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);

        // update the transaction status
        if ($suppress == "true"){
            $test = $this->terminalTransaction->updateOne($transactionDbId, ['status' => "to be suppressed"]);
        } else {
            $test = $this->terminalTransaction->updateOne($transactionDbId, ['status' => "to be unsuppressed"]);
        }

        if ($transaction["status"] != 200) {

            return json_encode([
                "success" => false,
                "error" => "Error while creating background process record. Kindly contact Administrator."
            ]);
        }
        $backgroundJobDbId = $transaction['data'][0]["backgroundJobDbId"];

        $host = getenv('CB_RABBITMQ_HOST');
        $port = getenv('CB_RABBITMQ_PORT');
        $user = getenv('CB_RABBITMQ_USER');
        $password = getenv('CB_RABBITMQ_PASSWORD');

        $connection = new AMQPStreamConnection($host, $port, $user, $password);

        $channel = $connection->channel();

        $channel->queue_declare(
            'QualityControlSuppressor', //queue - Queue names may be up to 255 bytes of UTF-8 characters
            false,  //passive - can use this to check whether an exchange exists without modifying the server state
            true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
            false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
            false   //auto delete - queue is deleted when last consumer unsubscribes
        );

        $msg = new AMQPMessage(
            json_encode([
                "transactionDbId" => $transactionDbId,
                "backgroundJobDbId" => $backgroundJobDbId,
                "accessToken" => Yii::$app->session->get('user.b4r_api_v3_token'),
                "action" => 'suppress-record',
                "params" => [
                    'transactionDbId' => $transactionDbId,
                    'variableDbId' => $variableDbId,
                    'suppress' => $suppress,
                    'suppressRemarks' => $suppressRemarks,
                    'entityDbId' => $entityDbId,
                    'dataLevel' => $dataLevel
                ]
            ]),
            array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
        );

        $channel->basic_publish(
            $msg,   //message 
            '', //exchange
            'QualityControlSuppressor'  //routing key (queue)
        );
        $channel->close();
        $connection->close();

        return json_encode(array("success" => true, "transactionId" => $transactionDbId));
    }

    /**
     * Suppress/unsuppress data by suppression rules in the background
     * @param Integer $id transaction ID
     * @return Json success and/or error message
     */
    public function actionSuppressByRuleBackground()
    {
        $variableDbId = Yii::$app->request->post('variableDbId');
        $transactionDbId = Yii::$app->request->post('transactionId');
        $suppress = Yii::$app->request->post('suppress');
        $dataLevel = $_POST["dataLevel"];
        $suppressRemarks = $_POST["suppressRemarks"] ?? '';
        $userId = Yii::$app->session->get('user.id');

        $backgroundJobParams = [
            "workerName" => "QualityControlSuppressor",
            "description" => "Suppression of records for transaction ID: {$transactionDbId}",
            "entity" => "TERMINAL_TRANSACTION",
            "entityDbId" => $transactionDbId,
            "endpointEntity" => "TERMINAL_TRANSACTION",
            "application" => "QUALITY_CONTROL",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId
        ];
        $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);

        // update the transaction status
        if ($suppress == "true"){
            $test = $this->terminalTransaction->updateOne($transactionDbId, ['status' => "to be suppressed"]);
        } else {
            $test = $this->terminalTransaction->updateOne($transactionDbId, ['status' => "to be unsuppressed"]);
        }

        if ($transaction["status"] != 200) {

            return json_encode([
                "success" => false,
                "error" => "Error while creating background process record. Kindly contact Administrator."
            ]);
        }
        $backgroundJobDbId = $transaction['data'][0]["backgroundJobDbId"];

        $host = getenv('CB_RABBITMQ_HOST');
        $port = getenv('CB_RABBITMQ_PORT');
        $user = getenv('CB_RABBITMQ_USER');
        $password = getenv('CB_RABBITMQ_PASSWORD');

        $connection = new AMQPStreamConnection($host, $port, $user, $password);

        $channel = $connection->channel();

        $channel->queue_declare(
            'QualityControlSuppressor', //queue - Queue names may be up to 255 bytes of UTF-8 characters
            false,  //passive - can use this to check whether an exchange exists without modifying the server state
            true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
            false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
            false   //auto delete - queue is deleted when last consumer unsubscribes
        );

        $msg = new AMQPMessage(
            json_encode([
                "transactionDbId" => $transactionDbId,
                "backgroundJobDbId" => $backgroundJobDbId,
                "accessToken" => Yii::$app->session->get('user.b4r_api_v3_token'),
                "action" => 'suppress-by-rule',
                "params" => [
                    'transactionDbId' => $transactionDbId,
                    'variableDbId' => $variableDbId,
                    'suppress' => $suppress,
                    'suppressRemarks' => $suppressRemarks,
                    'dataLevel' => $dataLevel
                ]
            ]),
            array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
        );

        $channel->basic_publish(
            $msg,   //message 
            '', //exchange
            'QualityControlSuppressor'  //routing key (queue)
        );
        $channel->close();
        $connection->close();

        return json_encode(array("success" => true, "transactionId" => $transactionDbId));
    }

    /**
     * Render trait value input field for bulk updating
     * @param Integer transactionId transaction record identifier
     * @return Object HTML page
     */
    public function actionRenderTraitValueField($transactionId) {
        $variableDbId = $_POST['variableDbId'] ?? 0;
        $variableAbbrev = $_POST['variableAbbrev'] ?? '';

        $config = [
            [
                "disabled" => false,
                "required" => true,
                "variable_abbrev" => $variableAbbrev,
            ]
        ];

        $widgetOptions = [
            'mainApiResourceMethod' => 'POST',
            'displayName' => 'Value',
            'mainApiResourceEndpoint' => 'variables-search',
            'mainApiSource' => 'identification',
            'mainApiResourceFilter' => ["variableDbId"=>"equals $variableDbId"],
            'id' => $variableDbId,
            'config' => $config,
        ];

        //render ajax
        return $this->renderAjax('_bulk_update_value.php', [
            'widgetOptions' => $widgetOptions,
            'transactionId' => $transactionId,
        ]);
    }

    /**
     *  Updates datasets in the data terminal
     *  @param Integer transactionId transaction record identifier
     */
    public function actionUpdateTrait($transactionId)
    {
        $variableAbbrev = $_POST['variableAbbrev'] ?? '';
        $variableDbId = $_POST['variableId'] ?? '';
        $value = $_POST['value'] ?? '';
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $idType = $_POST['idType'] ?? 'plot';
        $selection = $_POST['selection'] ?? '';

        if ($selection == 'all'){
            // Retrieve all record Ids based on dataUnit
            $dataset = $this->terminalTransaction->searchAllDatasets($transactionId, [
                'isVoid' => 'equals false',
                'dataUnit' => $idType
            ])['data'] ?? [];

            $recordIds = array_unique(array_column($dataset, 'entityDbId'));
        }

        $result = $this->transaction->insertDataset($transactionId, $variableAbbrev, $value, $recordIds, $idType);
        Yii::debug('Dataset insertion: ' . json_encode($result), __METHOD__);
        // recompute traits
        $resultVariableDbId = $this->formulaParameters->searchAll([
            'fields' => '
                formula_parameter.param_variable_id AS paramVariableDbId|
                formula_parameter.result_variable_id AS resultVariableDbId|
                formula_parameter.formula_id AS formulaDbId
            ',
            'paramVariableDbId' =>  "$variableDbId",
        ])['data'] ?? [];
        $formulaDbIds = [];
        foreach ($resultVariableDbId as $resultVar) {
            $formulaDbIds[] = $resultVar['formulaDbId'];
        }

        // Retrieve dataset status
        $dataset = $this->terminalTransaction->searchAllDatasets($transactionId, [
            'isVoid' => 'equals false',
            'dataUnit' => $idType,
            'variableDbId' => 'equals ' . $variableDbId,
            'entityDbId' => 'equals '.implode('|equals ', $recordIds)
        ])['data'] ?? [];
        
        $status = array_unique(array_column($dataset, 'status'));

        if(!empty($resultVariableDbId)){
            $selectedResultVariableDbId = $resultVariableDbId[0];
            
            $resultVariableDbId = $selectedResultVariableDbId['resultVariableDbId'];
            $formulaDbId = $selectedResultVariableDbId['formulaDbId'];
        
            // Retrieve selected formulaDbId
            $params = [
                'transactionDbId' => "equals $transactionId",
                'fields' => 'transaction.id AS transactionDbId|transaction.occurrence AS occurrence|transaction.status'
            ];
            $transaction = $this->terminalTransaction->searchAll($params, '');

            $occurrences = $transaction['data'][0]['occurrence'] ?? [];
            $resultVariable = [];
            $transactionStatus = $transaction['data'][0]['status'];

            foreach ($occurrences as $occurrence) {
                if (is_array($occurrence['resultVariable'])) {
                    foreach ($occurrence['resultVariable'] as $resultVar) {
                        if (strpos($resultVar, '-') !== false) {
                            $resultVar = explode('-', $resultVar);
                            $resultVarValue = end($resultVar);
                            $resultVariable[] = $resultVarValue;
                        }
                    }
                } 
            }

            if (in_array($formulaDbId, $resultVariable)) { // Update primary variable
                $result = $this->terminalTransaction->variableComputations($transactionId, [
                    'variableDbId' => [$resultVariableDbId],
                    'formulaDbId' => [$formulaDbId]
                ]);
                if ($result['totalCount'] > 0) $this->terminalTransaction->validateDataset($transactionId);
                Yii::debug('Variable computation: ' . json_encode($result), __METHOD__);

                $dataset = $this->terminalTransaction->searchAllDatasets($transactionId, [
                    'isVoid' => 'equals false',
                    'dataUnit' => $idType,
                    'variableDbId' => 'equals ' . $resultVariableDbId,
                    'entityDbId' => 'equals '.implode('|equals ', $recordIds)
                ])['data'] ?? [];
                
                $datasetDbIds = array_unique(array_column($dataset, 'datasetDbId'));
        
                if (($value == '0' && in_array('invalid', $status)) || $value == 'NA' && $transactionStatus == 'uploaded') {
                    foreach ($datasetDbIds as $datasetDbId) {
                        $url = 'transaction-datasets/' . $datasetDbId;
                        $result = Yii::$app->api->getResponse('DELETE', $url, json_encode(''));
                        Yii::debug('Dataset deletion: ' . json_encode($result), __METHOD__);
                    }
                }

                // Computation for derived formula
                if ($result) { 
                    $derivedFormula = $this->formulaParameters->searchAll([
                        'fields' => '
                            formula_parameter.param_variable_id AS paramVariableDbId|
                            formula_parameter.result_variable_id AS resultVariableDbId|
                            formula_parameter.formula_id AS formulaDbId
                        ',
                        'paramVariableDbId' =>  "$resultVariableDbId",
                    ])['data'] ?? [];

                    $derivedFormulaDbId = array_column($derivedFormula, 'formulaDbId');
                    $formulaDbId = array_intersect($derivedFormulaDbId, $resultVariable);
                    $formulaDbId = !empty($formulaDbId) ? reset($formulaDbId) : null;

                    $result = $this->terminalTransaction->variableComputations($transactionId, [
                        'variableDbId' => [$resultVariableDbId],
                        'formulaDbId' => [$formulaDbId]
                    ]);

                    if (isset($result['data'][0]['resultVariableDbId'])) {
                        $varDbId = $result['data'][0]['resultVariableDbId'];
                    } else {
                        $varDbId = $resultVariableDbId;
                    }
                    
                    if ($result['totalCount'] > 0) $this->terminalTransaction->validateDataset($transactionId);
                    Yii::debug('Variable computation: ' . json_encode($result), __METHOD__);

                    $dataset = $this->terminalTransaction->searchAllDatasets($transactionId, [
                        'isVoid' => 'equals false',
                        'dataUnit' => $idType,
                        'variableDbId' => 'equals ' . $varDbId ,
                        'entityDbId' => 'equals '.implode('|equals ', $recordIds)
                    ])['data'] ?? [];
                    
                    $datasetDbIds = array_unique(array_column($dataset, 'datasetDbId'));
            
                    if (($value == '0' && in_array('invalid', $status)) || $value == 'NA' && $transactionStatus == 'uploaded') {
                        foreach ($datasetDbIds as $datasetDbId) {
                            $url = 'transaction-datasets/' . $datasetDbId;
                            $result = Yii::$app->api->getResponse('DELETE', $url, json_encode(''));
                            Yii::debug('Dataset deletion: ' . json_encode($result), __METHOD__);
                        }
                    }
                }
            } else { // Update calculated variable
                $derivedFormulaDbId = array_intersect($formulaDbIds, $resultVariable);
                $formulaDbId = !empty($derivedFormulaDbId) ? reset($derivedFormulaDbId) : null;

                $result = $this->terminalTransaction->variableComputations($transactionId, [
                    'variableDbId' => [$variableDbId],
                    'formulaDbId' => [$formulaDbId]
                ]);

                if (isset($result['data'][0]['resultVariableDbId'])) {
                    $varDbId = $result['data'][0]['resultVariableDbId'];
                } else {
                    $varDbId = $variableDbId;
                }

                if ($result['totalCount'] > 0) $this->terminalTransaction->validateDataset($transactionId);
                Yii::debug('Variable computation without formulaDbId: ' . json_encode($result), __METHOD__);
                $dataset = $this->terminalTransaction->searchAllDatasets($transactionId, [
                    'isVoid' => 'equals false',
                    'dataUnit' => $idType,
                    'variableDbId' => 'equals ' . $varDbId,
                    'entityDbId' => 'equals '.implode('|equals ', $recordIds)
                ])['data'] ?? [];
                
                $datasetDbIds = array_unique(array_column($dataset, 'datasetDbId'));
                
                if (($value == '0' && in_array('invalid', $status)) || $value == 'NA' && $transactionStatus == 'uploaded') {
                    foreach ($datasetDbIds as $datasetDbId) {
                        $url = 'transaction-datasets/' . $datasetDbId;
                        $result = Yii::$app->api->getResponse('DELETE', $url, json_encode(''));
                        Yii::debug('Dataset deletion without formulaDbId: ' . json_encode($result), __METHOD__);
                    }
                }
            }
        }
        
        return json_encode(['count' => count($recordIds)]);
    }

    /**
     * Delegate building of CSV data to the background worker
     * @param Integer $transactionId transaction record identifier
     */
    public function actionPrepareDataForCsvDownload ($transactionId) {
        
        $userId = Yii::$app->session->get('user.id');
        $program = Yii::$app->userprogram->get('abbrev');
        $url = "terminal-transactions/$transactionId/datasets-table-search?";
        $requestBody = Yii::$app->session->get($transactionId);

        $params = [
            'transactionDbId' => "equals $transactionId",
            'fields' => 'transaction.id AS transactionDbId|location.location_name AS locationName'
        ];
        $transaction = $this->terminalTransaction->searchAll($params, '');

        $locationCode = $transaction['data'][0]['locationName'] ?? '';
        $file = $locationCode . '-' . $transactionId;

        $result = $this->terminalTransaction->getDatasetSummary($transactionId, false);
        foreach ($result['data'][0]['variables'] as $variables) {
            if ($variables["commitCount"] > 0) {
                $variableHeaders[] = $variables['abbrev'];
            }
        }
        $headersData = $this->transaction->getHeadersFromConfig();
        $mainHeaders = $headersData['mainHeaders'];
        $headers = $headersData['headers'];
        $csvHeaders = array_merge($headers, $variableHeaders);
        $csvAttributes = array_merge($mainHeaders, $variableHeaders);

        $backgroundJobParams = [
            "workerName" => "BuildCsvData",
            "description" => "Dataset download",
            "entity" => "TERMINAL_TRANSACTION",
            "entityDbId" => $transactionId,
            "endpointEntity" => "TERMINAL_TRANSACTION",
            "application" => "QUALITY_CONTROL",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId,
            'remarks' => $file,
            "message" => "Preparing dataset records for downloading"
        ];

        try {
            $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);         
        }

        try {
            if (isset($transaction['status']) && $transaction['status'] == 200) {
                $message = "The dataset records are being prepared in the background for download. You may proceed with other tasks. You will be notified in this page once done.";
                // set background processing info to session
                Yii::$app->session->set('dc-bg-processs-message', $message);
                Yii::$app->session->set('dc-bg-processs', true);

                $host = getenv('CB_RABBITMQ_HOST');
                $port = getenv('CB_RABBITMQ_PORT');
                $user = getenv('CB_RABBITMQ_USER');
                $password = getenv('CB_RABBITMQ_PASSWORD');

                $connection = new AMQPStreamConnection($host, $port, $user, $password);

                $channel = $connection->channel();

                $channel->queue_declare(
                    'BuildCsvData', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                    false,  //passive - can use this to check whether an exchange exists without modifying the server state
                    true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                    false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                    false   //auto delete - queue is deleted when last consumer unsubscribes
                );
                $msg = new AMQPMessage(
                    json_encode([
                        'tokenObject' => [
                            'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                            'refreshToken' => ''
                        ],
                        'backgroundJobId' => $transaction["data"][0]["backgroundJobDbId"],
                        'description' => 'Dataset download',
                        'transactionDbId' => $transactionId,
                        'processName' => "download-datasets",
                        'url' => $url,
                        'requestBody' => json_encode($requestBody),
                        'csvHeaders' => $csvHeaders,
                        'csvAttributes' => $csvAttributes,
                        'variableHeaders' => $variableHeaders,
                        'fileName' => $file
                    ]),
                    array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                );

                $channel->basic_publish(
                    $msg,   //message 
                    '', //exchange
                    'BuildCsvData'  //routing key (queue)
                );
                $channel->close();
                $connection->close();
                Yii::$app->session->set("bgp-export-filename", "$file");

                // redirect to DC browser
                // effectively exits this function (will trigger a (false) error response in the calling ajax)
                Yii::$app->response->redirect(['/dataCollection/terminal?program='.$program]);
            } else {
                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
            }
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $e);
        }
    }

    /**
     * Triggers trait calculation in the background
     * @param integer $id transaction identifier
     */
    public function actionCompute($id){

        $backgroundJob = $this->worker->invoke(
            "DataCollectionCalculator",
            "Calculate traits for transaction ID: {$id}",
            "TERMINAL_TRANSACTION",
            $id,
            "TERMINAL_TRANSACTION",
            "QUALITY_CONTROL",
            "POST",
            [
                'transactionDbId' => $id,
                'formulas' => $_POST['formulas'] ?? [],
                'formulaDbIds' => $_POST['formulaDbIds'] ?? [],
                'variableDbIds' => $_POST['variables'],
            ]
        );

        return json_encode(['success' => $backgroundJob['success'] ?? false]);
    }
}
