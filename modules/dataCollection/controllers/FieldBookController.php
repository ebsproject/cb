<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dataCollection\controllers;

use app\components\B4RController;
use app\controllers\BrowserController;
use app\models\Api;
use app\models\BackgroundJob;
use app\models\Location;
use app\modules\dataCollection\models\FieldBook;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Yii;

/**
 * Contains functions for Field Book Imports
 */
class FieldBookController extends B4RController
{
    public $defaultTraitColumnHeaders = ["trait", "format", "defaultValue", "minimum", "maximum", "details", "categories", "isVisible", "realPosition"];
    public $defaultLocationPlotColumnHeaders = ["PLOT_ID", "PLOT_CODE", "PLOT_NUMBER", "PLOT_REP", "ENTRY_NUMBER", "DESIGNATION", "PARENTAGE", "FIELD_X", "FIELD_Y", "OCCURRENCE_ID", "OCCURRENCE_NAME", "OCCURRENCE_CODE", "LOCATION_ID", "LOCATION_NAME", "LOCATION_CODE"];
    public $defaultLocationEntryColumnHeaders = ["ROW_ID", "ENTCODE", "ENTNO", "GID", "DESIGNATION", "LOCATION_NAME", "LOCATION"];

    public function __construct ($id, $module,
        protected Api $api,
        protected BrowserController $browserController,
        protected BackgroundJob $backgroundJob,
        protected Location $location,
        protected FieldBook $fieldBook,
        $config = []
    ) { 
        $this->api = $api;
        $this->browserController = $browserController;
        $this->backgroundJob = $backgroundJob;
        $this->location = $location;
        $this->fieldBook = $fieldBook;

        parent::__construct($id, $module, $config);
    }

    /**
     * Download traits for Field Book
     *
     * @param String $fileName File name of the downloaded file
     * @param String $entity occurrence, location or cross list level
     */
    public function actionDownloadTrait($fileName, $entity)
    {
        Yii::debug('Exporting traits for Field Book '.json_encode([ 'fileName' => $fileName ]), __METHOD__);

        $fileName = $fileName . '.trt';

        // if there are selected variables, retrieve from session
        $traitDbIds = $_POST['variableIds'] ?? [];
        Yii::$app->session->set('workbook-selected-vars', $traitDbIds);

        // prepare data set
        $variableArray = $this->fieldBook->getTraitInfoAndScaleValuesFromWorkingList($traitDbIds, $entity);

        $columnHeaders = $this->defaultTraitColumnHeaders;

        $csvRows = [];
        $csvRows = array_merge($csvRows, [$columnHeaders]);
        $csvRows = array_merge($csvRows, empty($variableArray) ? [] : $variableArray);

        return json_encode([
            'success' => true,
            'filename' => $fileName,
            'csvRows' => $csvRows,
            'message' => "<i class='material-icons green-text'>done</i>&emsp; Successfully downloaded traits for field book."
        ]);
    }

    /**
     * Download location with observation data for Field Book
     *
     * @param String $program program identifier
     * @param Integer $locationId Location identifier
     * @param Integer $occurrenceId Occurrence identifier
     * @param Integer $crossListId Cross List identifier
     * @param String $fileName File name of the downloaded file
     */
    public function actionDownloadLocation ($program, $locationId, $occurrenceId, $crossListId, $fileName)
    {
        Yii::debug('Exporting ' . ($locationId ? 'Location' : 'Occurrence') . ' for Field Book '.json_encode([ 'locationId' => $locationId, 'occurrenceId' => $occurrenceId, 'fileName' => $fileName, ]), __METHOD__);

        $entity = $occurrenceId ? 'occurrence' : 'location';
        $thresholdValue = Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportRecords');
        $path = '';
        $filter = '';
        $rawData = [];
        $excludeOccurrenceDbId = false;

        // if there are selected variables, retrieve from session
        // GET TRAITS
        $traitDbIds = $_POST['variableIds'] ?? [];
        $traitArray = [];

        if(!empty($crossListId)){
            $crossCsv = $this->fieldBook->getCrossListFileContent($crossListId, $traitDbIds);
            return json_encode($crossCsv);
        }

        if (!empty($traitDbIds)) {
            // Store trait DB IDs in PHP session
            Yii::$app->session->set('workbook-selected-vars', $traitDbIds);

            // Get trait info array
            $traitArray = $this->fieldBook->getTraitInfoAndScaleValuesFromWorkingList($traitDbIds);

            // Extract only the trait abbrevs
            foreach ($traitArray as $index => $traitInfo) {
                $traitArray[$index] = $traitInfo[0];
            }
        }

        [
            $csvAttributes,
            $csvHeaders,
        ] = $this->browserController->getColumns(
            $program,
            'DOWNLOAD_DATA_COLLECTION_FILES',
            'DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS',
            [],
            'download',
            'abbrev'
        );

        // Always include occurrenceDbId when retrieving either Occurrence- or Location-level plots
        if (!array_search('occurrenceDbId', $csvAttributes)) {
            $csvAttributes []= 'occurrenceDbId';
            $excludeOccurrenceDbId = true;
        }

        array_unshift($csvAttributes, 'plotDbId');
        array_unshift($csvHeaders, 'PLOT_ID');
        $csvHeaders = array_merge($csvHeaders, $traitArray);

        // If "GERMPLASM" is present, change it to "DESIGNATION"
        $index = array_search('GERMPLASM', $csvHeaders);
        if ($index)
            $csvHeaders[$index] = 'DESIGNATION';

        if ($locationId) {
            $method = 'POST';
            $path = "locations/$locationId/plots-search";

            // Add these sort parameters if corresponding csvAttributes exist
            $sortParams = [];
            if (array_search('occurrenceCode', $csvAttributes))
                $sortParams []= $csvAttributes[array_search('occurrenceCode', $csvAttributes)];
            if (array_search('plotNumber', $csvAttributes))
                $sortParams []= $csvAttributes[array_search('plotNumber', $csvAttributes)];
            $sortParams = implode('|', $sortParams);
            $filter = '';
            $filter = (empty($sortParams)) ? '' : "sort={$sortParams}";

            // Retrieve only plots of mapped/planted occurrences of a location
            $rawData['fields'] = $this->generateFields([ ...$csvAttributes, 'occurrenceStatus'], $path);
            if ($occurrenceId){
                $rawData['occurrenceDbId'] = "equals $occurrenceId";
            }
            $rawData['occurrenceStatus'] = 'mapped%|planted%';
        } elseif ($occurrenceId) {
            $method = 'POST';
            $path = "occurrences/$occurrenceId/plots-search";
            $filter = (array_search('plotNumber', $csvAttributes)) ? '' : "sort=plotNumber";
            $rawData = [
                'fields' => $this->generateFields($csvAttributes, $path),
            ];
        }

        // Exclude occurrenceDbId from being written in the CSV file...
        // ...if current config doesn't include occurrenceDbId
        if ($excludeOccurrenceDbId)
            unset($csvAttributes[array_search('occurrenceDbId', $csvAttributes)]);

        $response = $this->api->getApiResults(
            $method,
            $path,
            null,
            '&limit=1',
            false
        );

        // If number of plots meet the export threshold, trigger background processing
        if ($response['totalCount'] >= $thresholdValue) {
            // get user ID
            $userId = Yii::$app->session->get('user.id');
            // Not all Location IDs are identical to the Occurrence IDs
            $entityId = ($entity == 'location') ? $locationId : $occurrenceId;
            $description = "Preparing Field Book records for downloading";
            // Insert trait abbrevs
            $csvAttributes = array_merge($csvAttributes, $traitArray);

            $backgroundJobParams = [
                "workerName" => "BuildCsvData",
                "description" => $description,
                "entity" => strtoupper($entity),
                "entityDbId" => $entityId,
                "endpointEntity" => 'OCCURRENCE',
                "application" => 'OCCURRENCES',
                "method" => "POST",
                "jobStatus" => "IN_QUEUE",
                "startTime" => "NOW()",
                "creatorDbId" => $userId,
                'remarks' => $fileName,
            ];

            try {
                $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
            } catch (\Exception $e) {
                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);         
            }

            // if creating transaction is successful
            try {
                if (isset($transaction['status']) && $transaction['status'] == 200) {
                    $entityName = ($occurrenceId && str_contains($path, 'occurrences')) ? $response['data'][0]['occurrenceName'] : $response['data'][0]['locationName'];

                    $message = 'You are now seeing the transaction for <b>' . $entityName . '</b> is now <b>' . ucfirst($description) . ' in progress</b>. You will be notified once done. Click the Reset Grid button to display all records.';
                    // set background processing info to session
                    Yii::$app->session->set('em-bg-processs-message', $message);
                    Yii::$app->session->set('em-bg-processs', true);

                    $host = getenv('CB_RABBITMQ_HOST');
                    $port = getenv('CB_RABBITMQ_PORT');
                    $user = getenv('CB_RABBITMQ_USER');
                    $password = getenv('CB_RABBITMQ_PASSWORD');

                    $connection = new AMQPStreamConnection($host, $port, $user, $password);

                    $channel = $connection->channel();

                    $channel->queue_declare(
                        'BuildCsvData', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                        false,  //passive - can use this to check whether an exchange exists without modifying the server state
                        true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                        false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                        false   //auto delete - queue is deleted when last consumer unsubscribes
                    );
                    $msg = new AMQPMessage(
                        json_encode([
                            'tokenObject' => [
                                'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                                'refreshToken' => ''
                            ],
                            'backgroundJobId' => $transaction["data"][0]["backgroundJobDbId"],
                            'description' => $description,
                            'url' => "$path?$filter",
                            'requestBody' => json_encode($rawData),
                            'csvHeaders' => $csvHeaders,
                            'csvAttributes' => $csvAttributes,
                            'fileName' => $fileName,
                        ]),
                        array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                    );

                    $channel->basic_publish(
                        $msg,   //message 
                        '', //exchange
                        'BuildCsvData'  //routing key (queue)
                    );
                    $channel->close();
                    $connection->close();

                    // redirect to Occurrence browser
                    $program = Yii::$app->session->get('program');
                    // effectively exits this function (will trigger a (false) error response in the calling ajax)
                    Yii::$app->response->redirect(['/occurrence?program='.$program.'&OccurrenceSearch[occurrenceDbId]='.$entityId.'&filename='.$fileName.'&entity='.$entity.'&entityId='.$entityId]);
                } else {
                    $notif = 'error';
                    $icon = 'times';
                    $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                    Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
                }
            } catch (\Exception $e) {}

            return 1;
        }

        $response = $this->api->getApiResults(
            $method,
            $path,
            json_encode((object) $rawData),
            $filter,
            true
        );

        // Regular export
        if (
            !$response['success']
            || !isset($response['data'])
            || empty($response['data'])
        )
            return json_encode([
                'success' => false,
                'message' => "<i class='material-icons red-text'>close</i>&emsp; There seems to be a problem with downloading location for field book."
            ]);
        else {
            $retrievedData = $response['data'];

            // prepare data set
            $csvContent = [[]];
            $collatedData = [];

            // package arrays of retrieved data into one
            foreach ($retrievedData as $key => $value) {
                if ($locationId) {
                    $collatedValue = $value['plots'][0];

                    $collatedValue = array_map(function ($v) use ($value) {
                        $v['locationDbId'] = $value['locationDbId'];
                        $v['locationCode'] = $value['locationCode'];
                        $v['locationName'] = $value['locationName'];
                        $v['locationStatus'] = $value['locationStatus'];
                        $v['locationType'] = $value['locationType'];

                        return $v;
                    }, $collatedValue);
                } else if ($occurrenceId) {
                    $collatedValue = $value['plots'];

                    $collatedValue = array_map(function ($v) use ($value) {
                        $v['occurrenceDbId'] = $value['occurrenceDbId'];
                        $v['occurrenceCode'] = $value['occurrenceCode'];
                        $v['occurrenceName'] = $value['occurrenceName'];
                        $v['occurrenceNumber'] = $value['occurrenceNumber'];

                        return $v;
                    }, $collatedValue);
                }

                array_push($collatedData, ...$collatedValue);
            }

            $i = 0;

            if (!empty($collatedData) && isset($collatedData)) {
                foreach ($collatedData as $key => $value) {
                    $csvContent[$i] = [];
    
                    foreach ($csvAttributes as $k => $v){
                        $val = isset($value[$v]) ? $value[$v] : '';
                        array_push($csvContent[$i], '"' . $val. '"');
                    }

                    $i += 1;
                }
            }

            $csvRows = [];
            $csvRows = array_merge($csvRows, [$csvHeaders]);

            foreach ($csvContent as $line) {
                $line = array_values($line);

                foreach ($line as $i => $value) {
                    $line = array_values($line);

                    if (count($line) == $i+1) $line[$i] = $value;
                }

                $csvRows = array_merge($csvRows, [array_values($line)]);
            }

            $withTraitAbbrevsMessage = (empty($traitDbIds)) ? '' : ' including trait abbrevs';

            return json_encode([
                'success' => true,
                'filename' => $fileName,
                'csvRows' => $csvRows,
                'message' => "<i class='material-icons green-text'>done</i>&emsp; Successfully downloaded $entity for field book{$withTraitAbbrevsMessage}."
            ]);
        }

    }

    /**
     * Generate string of fields for API request body
     *
     * @param array $attributes list of specified attributes
     * @param string $endpoint API URL endpoint
     * 
     * @return string generated string of fields
     */
    public function generateFields ($attributes, $endpoint)
    {
        $locationsPlotsSearchAttributes = [
            'plotDbId' => 'plot.id AS plotDbId',
            'occurrenceDbId' => 'plot.occurrence_id AS occurrenceDbId',
            'occurrenceCode' => 'occurrence.occurrence_code AS occurrenceCode',
            'occurrenceName' => 'occurrence.occurrence_name AS occurrenceName',
            'occurrenceStatus' => 'occurrence.occurrence_status AS occurrenceStatus',
            'entryDbId' => 'plot.entry_id AS entryDbId',
            'entryCode' => 'pi.entry_code AS entryCode',
            'entryNumber' => 'pi.entry_number AS entryNumber',
            'entryName' => 'pi.entry_name AS entryName',
            'entryType' => 'pi.entry_type AS entryType',
            'entryRole' => 'pi.entry_role AS entryRole',
            'entryClass' => 'pi.entry_class AS entryClass',
            'entryStatus' => 'pi.entry_status AS entryStatus',
            'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
            'germplasmCode' => 'g.germplasm_code AS germplasmCode',
            'germplasmState' => 'g.germplasm_state AS germplasmState',
            'germplasmType' => 'g.germplasm_type AS germplasmType',
            'parentage' => 'g.parentage AS parentage',
            'seedDbId' => 'pi.seed_id AS seedDbId',
            'seedCode' => 'seed.seed_code AS seedCode',
            'seedName' => 'seed.seed_name AS seedName',
            'packageDbId' => 'pi.package_id AS packageDbId',
            'packageCode' => 'package.package_code AS packageCode',
            'packageLabel' => 'package.package_label AS packageLabel',
            'plotCode' => 'plot.plot_code AS plotCode',
            'plotNumber' => 'plot.plot_number AS plotNumber',
            'plotType' => 'plot.plot_type AS plotType',
            'rep' => 'plot.rep',
            'designX' => 'plot.design_x AS designX',
            'designY' => 'plot.design_y AS designY',
            'plotOrderNumber' => 'plot.plot_order_number AS plotOrderNumber',
            'paX' => 'plot.pa_x AS paX',
            'paY' => 'plot.pa_y AS paY',
            'fieldX' => 'plot.field_x AS fieldX',
            'fieldY' => 'plot.field_y AS fieldY',
            'blockNumber' => 'plot.block_number AS blockNumber',
            'rowBlockNumber' => 'descol1.block_value AS rowBlockNumber',
            'colBlockNumber' => 'descol2.block_value AS colBlockNumber',
            'harvestStatus' => 'plot.harvest_status AS harvestStatus',
            'plotStatus' => 'plot.plot_status AS plotStatus',
            'plotQcCode' => 'plot.plot_qc_code AS plotQcCode',
            'creationTimestamp' => 'pi.creation_timestamp AS creationTimestamp',
            'creatorDbId' => 'creator.id AS creatorDbId',
            'creator' => 'creator.person_name AS creator',
            'modificationTimestamp' => 'plot.modification_timestamp AS modificationTimestamp',
            'modifierDbId' => 'modifier.id AS modifierDbId',
            'modifier' => 'modifier.person_name AS modifier',
        ];

        $occurrencesPlotsSearchAttributes = [
            'plotDbId' => 'plot.id AS plotDbId',
            'entryDbId' => 'plot.entry_id AS entryDbId',
            'entryCode' => 'pi.entry_code AS entryCode',
            'entryNumber' => 'pi.entry_number AS entryNumber',
            'entryName' => 'pi.entry_name AS entryName',
            'entryType' => 'pi.entry_type AS entryType',
            'entryRole' => 'pi.entry_role AS entryRole',
            'entryClass' => 'pi.entry_class AS entryClass',
            'entryStatus' => 'pi.entry_status AS entryStatus',
            'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
            'germplasmCode' => 'g.germplasm_code AS germplasmCode',
            'germplasmState' => 'g.germplasm_state AS germplasmState',
            'germplasmType' => 'g.germplasm_type AS germplasmType',
            'parentage' => 'g.parentage AS parentage',
            'seedDbId' => 'pi.seed_id AS seedDbId',
            'seedCode' => 'seed.seed_code AS seedCode',
            'seedName' => 'seed.seed_name AS seedName',
            'packageDbId' => 'pi.package_id AS packageDbId',
            'packageCode' => 'package.package_code AS packageCode',
            'packageLabel' => 'package.package_label AS packageLabel',
            'plotCode' => 'plot_code AS plotCode',
            'plotNumber' => 'plot_number AS plotNumber',
            'plotType' => 'plot_type AS plotType',
            'rep' => 'rep',
            'designX' => 'design_x AS designX',
            'designY' => 'design_y AS designY',
            'plotOrderNumber' => 'plot_order_number AS plotOrderNumber',
            'paX' => 'pa_x AS paX',
            'paY' => 'pa_y AS paY',
            'fieldX' => 'field_x AS fieldX',
            'fieldY' => 'field_y AS fieldY',
            'blockNumber' => 'block_number AS blockNumber',
            'rowBlockNumber' => 'descol1.block_value AS rowBlockNumber',
            'colBlockNumber' => 'descol2.block_value AS colBlockNumber',
            'harvestStatus' => 'harvest_status AS harvestStatus',
            'plotStatus' => 'plot_status AS plotStatus',
            'plotQcCode' => 'plot_qc_code AS plotQcCode',
            'creationTimestamp' => 'pi.creation_timestamp AS creationTimestamp',
            'creatorDbId' => 'creator.id AS creatorDbId',
            'creator' => 'creator.person_name AS creator',
            'modificationTimestamp' => 'plot.modification_timestamp AS modificationTimestamp',
            'modifierDbId' => 'modifier.id AS modifierDbId',
            'modifier' => 'modifier.person_name AS modifier',
        ];

        $fields = '';

        if (preg_match('/locations\/.*\/plots-search/i', $endpoint)) {
            foreach ($attributes as $value) {
                if (!array_key_exists($value, $locationsPlotsSearchAttributes))
                    continue;

                $fields .= "{$locationsPlotsSearchAttributes[$value]} |";
            }
        } else if (preg_match('/occurrences\/.*\/plots-search/i', $endpoint)) {
            foreach ($attributes as $value) {
                if (!array_key_exists($value, $occurrencesPlotsSearchAttributes))
                    continue;

                $fields .= "{$occurrencesPlotsSearchAttributes[$value]} |";
            }
        }

        $fields = rtrim($fields, '|');

        return $fields;
    }
}
