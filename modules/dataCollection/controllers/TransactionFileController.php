<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dataCollection\controllers;

use app\models\TerminalTransaction;
use app\models\TerminalTransactionFile;
use app\dataproviders\ArrayDataProvider;
use Yii;

/**
 *  Controller for Transaction File 
 */
class TransactionFileController extends \yii\web\Controller
{

    /**
     * Renders view for related files in transaction
     * @param  integer $id transaction ID
     * @return html
     */
    public function actionView($id)
    {
        $locationName = isset($_POST['locationName']) ? $_POST['locationName'] : '';

        $terminalTransactionModel = new TerminalTransaction;
        $terminalTransactionModel->transactionDbId = $id;
        $files = $terminalTransactionModel->getFiles();
        $dataModels = $files['data'];
        $options = [
            'key' => 'transactionFileDbId',
            'allModels' => $dataModels,
            'pagination' => false
        ];

        $dataProvider = new ArrayDataProvider($options);
        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
            'locationName' => $locationName
        ]);
    }

    /**
     * Downloads plot datasets for uploaded, invalid, and committed
     * 
     * @param Integer $id ID of the terminal transaction
     * @param String $locationName name of the location for a given file
     */
    public function actionDownload($id, $locationName)
    {

        $fileData = TerminalTransactionFile::getData($id);
        $fileData = $fileData['data'];
        $file = $locationName . '-' . $id . '.csv';
        $fileRoot = Yii::getAlias("@webroot") . '/files' . '/' . trim($file);
        $fp = fopen($fileRoot, 'w');
        $headers = [];

        foreach ($fileData as $data) {

            if (empty($headers)) {
                $headers = ["PLOT_ID", "LOCATION_CODE", "OCCURRENCE_CODE", "PLOT_CODE", "PLOT_NUMBER", "PLOT_TYPE", "REP", "DESIGN_X", "DESIGN_Y", "PA_X", "PA_Y", "FIELD_X", "FIELD_Y"];
                $mainHeaders =  ["plotDbId", "designX", "designY", "fieldX", "fieldY", "locationCode", "occurrenceCode", "paX", "paY", "plotCode", "plotNumber", "plotType", "rep"];
                $csvHeaders = array_keys($data);
                foreach ($csvHeaders as $key) {
                    if (!in_array($key, $mainHeaders) && $key != "plotDbId") {
                        $headers[] = strtoupper($key);
                    }
                }
                fputcsv($fp, $headers, ',');
            }
            fputcsv($fp, $data, ',');
        }

        fclose($fp);
        if (file_exists($fileRoot)) {  // trigger successful download
            Yii::$app->response->sendFile($fileRoot);
            return Yii::$app->end();
        } else {    // file is not created/ does not exist
            Yii::$app->user->setFlash('error', '<i class="fa-fw fa fa-times"></i>There seems to be a problem with the file you were downloading.');
        }
    }

    /**
     * Downloads cross datasets for uploaded, invalid, and committed
     * 
     * @param Integer $id ID of the terminal transaction
     * @param String $locationName name of the location for a given file
     */
    public function actionDownloadCrossData($id, $locationName)
    {

        $fileData = TerminalTransactionFile::getCrossData($id);
        $fileData = $fileData['data'];
        $file = $locationName . '-' . $id . '.csv';
        $fileRoot = Yii::getAlias("@webroot") . '/files' . '/' . trim($file);
        $fp = fopen($fileRoot, 'w');
        $headers = [];

        foreach ($fileData as $data) {

            if (empty($headers)) {
                $headers = [
                    "CROSS_ID", "Cross Name", "Method", "Designation",
                    "Parentage", "Generation", "Female Parent",
                    "Female Parentage", "Female Parent Seed Source", "Male Parent",
                    "Male Parentage", "Male Parent Seed Source", "Female Source Entry ID",
                    "Female Source Entry", "Female Source Seed Name", "Female Source Plot ID",
                    "Female Source Plot", "Male Source Entry ID", "Male Source Entry",
                    "Male Source Seed Name", "Male Source Plot ID",  "Male Source Plot"
                ];
                $mainHeaders =  [
                    "crossDbId", "crossName", "crossMethod", "germplasmDesignation",
                    "germplasmParentage", "germplasmGeneration", "crossFemaleParent",
                    "femaleParentage", "femaleParentSeedSource", "crossMaleParent",
                    "maleParentage", "maleParentSeedSource", "femaleSourceEntryDbId",
                    "femaleSourceEntry", "femaleSourceSeedName", "femaleSourcePlotDbId",
                    "femaleSourcePlot", "maleSourceEntryDbId", "maleSourceEntry",
                    "maleSourceSeedName", "maleSourcePlotDbId", "maleSourcePlot"
                ];
                $csvHeaders = array_keys($data);
                foreach ($csvHeaders as $key) {
                    if (!in_array($key, $mainHeaders)) {
                        $headers[] = strtoupper($key);
                    }
                }
                fputcsv($fp, $headers, ',');
            }

            fputcsv($fp, $data, ',');
        }

        fclose($fp);
        if (file_exists($fileRoot)) {  // trigger successful download
            Yii::$app->response->sendFile($fileRoot);
            return Yii::$app->end();
        } else {    // file is not created/ does not exist
            Yii::$app->user->setFlash('error', '<i class="fa-fw fa fa-times"></i>There seems to be a problem with the file you were downloading.');
        }
    }
}
