<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dataCollection\controllers;

use app\interfaces\controllers\IDashboard;
use app\interfaces\models\IWorker;
use app\models\BackgroundJob;
use app\models\TerminalTransaction;
use app\models\TerminalBackgroundProcess;
use app\models\User;
use Yii;
use app\components\B4RController;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Controller for Data Terminal Browser
 */
class TerminalController extends B4RController
{

    public function __construct(
        $id,
        $module,
        protected IDashboard $dashboard,
        protected IWorker $worker,
        protected BackgroundJob $backgroundJob,
        protected TerminalTransaction $terminalTransaction,
        protected TerminalBackgroundProcess $terminalBackgroundProcess,
        $config=[],
    )
    {
        parent::__construct($id, $module, $config);
    }


    /**
     * Renders QC browser
     */

    public function actionIndex()
    {

        $dashboardFilters = (array) $this->dashboard->getFilters();
        $params = Yii::$app->request->queryParams;
        $terminalTransactionObj = new TerminalTransaction();

        $userId = (new User())->getUserId();

        if (!isset($params['TerminalTransaction']['status']) && !isset($params['TerminalTransaction']['transactionDbId'])) {
            $params['TerminalTransaction']['status'] = 'Open';
        }
        if(isset($dashboardFilters['program_id']) && !isset($params['TerminalTransaction']['transactionDbId'])){
            $params['TerminalTransaction']['programDbId'] = "equals ".$dashboardFilters['program_id'];
        }
        if(isset($dashboardFilters['owned']) && $dashboardFilters['owned'] == 'true'){
            $params['TerminalTransaction']['creatorDbId'] = "equals $userId";
        }

        $dataProvider = $terminalTransactionObj->search($params);
        $program = Yii::$app->userprogram->get('abbrev');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $terminalTransactionObj,
            'program' => $program
        ]);
    }

    /**
     * Delete the transaction and its records
     *
     * @param string $program name of the current program
     */
    public function actionDeleteTransaction($program) {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        
        // get user ID
        $userId = Yii::$app->session->get('user.id');

        $backgroundJobParams = [
            "workerName" => "DeleteTransactionRecords",
            "description" => "Delete records for transaction ID: {$id}",
            "entity" => "TERMINAL_TRANSACTION",
            "entityDbId" => $id,
            "endpointEntity" => "TERMINAL_TRANSACTION",
            "application" => "QUALITY_CONTROL",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId
        ];
        
        $redirectUrl = ['/dataCollection/terminal?program='.$program];

        //create background transactions
        try {
            $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);         
        }

        // if creating transaction is successful
        try {
            if (isset($transaction['status']) && $transaction['status'] == 200) {
                $message = "The records are being deleted in the background. You may proceed with other tasks. The transaction will not be visible in the browser once done. Please reload the page.";
                // set background processing info to session
                Yii::$app->session->set('dc-bg-processs-message', $message);
                Yii::$app->session->set('dc-bg-processs', true);
    
                $host = getenv('CB_RABBITMQ_HOST');
                $port = getenv('CB_RABBITMQ_PORT');
                $user = getenv('CB_RABBITMQ_USER');
                $password = getenv('CB_RABBITMQ_PASSWORD');

                $connection = new AMQPStreamConnection($host, $port, $user, $password);
    
                $channel = $connection->channel();
    
                $channel->queue_declare(
                    'DeleteTransactionRecords', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                    false,  //passive - can use this to check whether an exchange exists without modifying the server state
                    true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                    false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                    false   //auto delete - queue is deleted when last consumer unsubscribes
                );
    
                $msg = new AMQPMessage(
                    json_encode([
                        'tokenObject' => [
                            'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                            'refreshToken' => ''
                        ],
                        'backgroundJobId' => $transaction["data"][0]["backgroundJobDbId"],
                        'description' => 'Deletion of transaction related records',
                        'transactionDbId' => [$id],
                    ]),
                    array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                );
    
                $channel->basic_publish(
                    $msg,   //message 
                    '', //exchange
                    'DeleteTransactionRecords'  //routing key (queue)
                );
                $channel->close();
                $connection->close();

                // update the status to removing data in progress
                $requestData['status'] = 'removing data in progress'; 
                $this->terminalTransaction->updateOne($id, $requestData);

                // effectively exits this function (will trigger a (false) error response in the calling ajax)
                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> ' . $message);        
                Yii::$app->response->redirect($redirectUrl);
            } else {
                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
            }
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $e);
        }
    }

    /** 
     * Renders notifications from background processes 
     */
    public function actionPushNotifications()
    {
        $userModel = new User();
        $userId = $userModel->getUserId();

        //get new notifications
        $params = [
            "creatorDbId" => "equals $userId",
            "application" => 'QUALITY_CONTROL',
            "isSeen" => "equals false",
        ];
        $filter = 'sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);
        $newNotifications = $result["data"] ?? [];
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->backgroundJob->getGenericNotifications($newNotifications);

        //get old notifications
        $params = [
            'creatorDbId' => "equals $userId",
            'application' => 'QUALITY_CONTROL',
            'isSeen' => 'equals true'
        ];
        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);
        $oldNotifications = $result["data"] ?? [];
        $seenCount = count($oldNotifications);
        $seenNotifications = $this->backgroundJob->getGenericNotifications($oldNotifications);

        // update unseen notifications to seen
        $params = [
            "jobIsSeen" => 'true'
        ];
        $this->backgroundJob->update($newNotifications, $params);

        return $this->renderAjax(
            'notifications',
            [
                "unseenNotifications" => $unseenNotifications,
                "seenNotifications" => $seenNotifications,
                "unseenCount" => $unseenCount,
                "seenCount" => $seenCount
            ]
        );
    }

    /**
     * Returns new notifications from background processes
     */
    public function actionNewNotificationsCount()
    {
        $userModel = new User();
        $userId = $userModel->getUserId();

        $params = [
            "creatorDbId" => "equals $userId",
            "application" => 'equals QUALITY_CONTROL',
            "isSeen" => "equals false",
        ];
        $filter = 'sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter);
        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        }
        return json_encode($result["totalCount"]);
    }

    /**
     * Retrieve the status of the transaction
     * @return string transaction status 
     */
    public function actionGetTransactionStatus()
    {
        $url = 'terminal-transactions-search';
        $params = [
            'transactionDbId' => "equals " . $_POST['transactionId'],
            'fields' => 'transaction.id AS transactionDbId|transaction.status',
        ];
        $data = Yii::$app->api->getResponse('POST', $url, json_encode($params), 'limit=1&sort=transactionDbId:desc');
        if ($data['status'] == 200) {
            $data = $data['body'];
            return isset($data['result']['data']) ? $data['result']['data'][0]['status'] : '';
        }
    }

    /**
     * Download CSV data from filepath
     */
    public function actionDownloadCsvData () {
        
        $filename = Yii::$app->session->get("bgp-export-filename") . '.csv';
        $pathToFile = realpath(Yii::$app->basePath) . "/files/data_export/$filename";
        if (file_exists($pathToFile)) {
            // Set headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($pathToFile));

            // Flush system output buffer
            flush();

            // Read/Download CSV file
            readfile($pathToFile);
            
            die();
        }
    }
}
