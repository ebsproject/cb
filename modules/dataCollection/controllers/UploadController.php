<?php
/* 
* This file is part of EBS-Core Breeding. 
* 
* EBS-Core Breeding is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* 
* EBS-Core Breeding is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
* GNU General Public License for more details. 
* 
* You should have received a copy of the GNU General Public License 
* along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

namespace app\modules\dataCollection\controllers;

use app\components\B4RController;
use app\interfaces\models\IPerson;
use app\models\Api;
use app\models\User;
use \app\modules\dataCollection\models\Upload;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Yii;

/**
 * Controller for upload
 *
 * Uploading of files in CSV or Workbook format
 */
class UploadController extends B4RController
{
    public function __construct(
        $id,
        $module,
        protected Api $api,
        protected IPerson $person,
        protected Upload $importer,
        protected User $user,
        $config=[],
    )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     * @param  string $program current program abbrev
     * @return html view
     */
    public function actionIndex($program)
    {
        if (Yii::$app->access->renderAccess("UPLOAD_DATA", "QUALITY_CONTROL", usage: 'url')) {
            return $this->render('index', [
                "program" => $program,
            ]);
        }
    }

    /**
     * Renders the index view for the uploading other files
     * @param  string $program current program abbrev
     * @return html view
     */
    public function actionOtherUpload($program)
    {
        $configName = $program;

        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if (strtoupper($role) == 'COLLABORATOR') {
            $configName = 'COLLABORATOR';
        }

        // Get other DC file names
        $dataCollectionOtherFiles = $this->api->getApiResults(
            'GET',
            'configurations?abbrev=DC_' . strtoupper($configName) . '_OTHER_FILE_SETTINGS&limit=1',
            retrieveAll: true
        );
        // Fetch global settings if program/role-specific config is not available
        if ($dataCollectionOtherFiles['totalCount'] == 0) {
            $dataCollectionOtherFiles = $this->api->getApiResults(
                'GET',
                'configurations?abbrev=DC_GLOBAL_OTHER_FILE_SETTINGS&limit=1',
                retrieveAll: true
            );
        }

        $interfaceList = array_keys($dataCollectionOtherFiles['data'][0]['configValue'] ?? []);
        $configValue = $dataCollectionOtherFiles['data'][0]['configValue'] ?? [];

        $uploadConfig = [];
        foreach ($configValue as $key => $interface){
            foreach ($interface['Upload'] as $variable){

                if ($variable['isObservation'] || $variable['identifier']){
                    $uploadConfig[$key][] = $variable;
                }
            }
        }

        return $this->render('other-upload', [
            'program' => $program,
            'interfaceList' => $interfaceList
        ]);
    }

    /**
     * Renders file upload modal content
     *
     * @param string $program program code
     */
    public function actionRenderFileUploadForm($program)
    {
        extract($_POST);

        $htmlData = $this->renderAjax('_file_upload', [
            'program' => $program,
            'uploadType' => $uploadType ?? '',
        ]);

        return json_encode($htmlData);
    }

    /**
     * Upload files
     * accepts CSV or workbook files
     * @param  string $program current program abbrev
     * @param  string $uploadType upload type
     * @return json results, success or error notification
     */
    public function actionGet($program, $uploadType = '')
    {

        try {
            if (isset($_FILES['upload_file'])) {

                $result = $this->importer->reader('upload_file', $uploadType, $program);
                if ($result["success"]) {
                    $transactionId = $result["transactionId"];
                    $datasetUnit = $result["datasetUnit"];
                    $backgroundJobDbId = $result["backgroundJobDbId"];

                    $host = getenv('CB_RABBITMQ_HOST');
                    $port = getenv('CB_RABBITMQ_PORT');
                    $user = getenv('CB_RABBITMQ_USER');
                    $password = getenv('CB_RABBITMQ_PASSWORD');

                    $connection = new AMQPStreamConnection($host, $port, $user, $password);

                    $channel = $connection->channel();

                    $channel->queue_declare(
                        'DataCollectionUploader', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                        false,  //passive - can use this to check whether an exchange exists without modifying the server state
                        true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                        false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                        false   //auto delete - queue is deleted when last consumer unsubscribes
                    );

                    $msg = new AMQPMessage(
                        json_encode([
                            "datasetUnit" => $datasetUnit,
                            "transactionDbId" => $transactionId,
                            "backgroundJobDbId" => $backgroundJobDbId,
                            "bearerToken" => Yii::$app->session->get('user.b4r_api_v3_token')
                        ]),
                        array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                    );

                    $channel->basic_publish(
                        $msg,   //message 
                        '', //exchange
                        'DataCollectionUploader'  //routing key (queue)
                    );
                    $channel->close();
                    $connection->close();

                    return json_encode(array("success" => true, "message" => "uploading in progress", "transactionId" => $transactionId));
                } else {
                    return json_encode(["success" => false, "error" => $result['error']]);
                }
            } else {
                return json_encode(["success" => false, "error" => "No file uploaded."]);
            }
        } catch (\Exception $e) {
            Yii::error($e, __METHOD__);
            return json_encode(["success" => false, "error" => "Error in uploading file. Try uploading the file again. If error persists, submit a bug report."]);
        }
    }
}
