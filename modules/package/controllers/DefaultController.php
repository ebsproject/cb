<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\package\controllers;

use app\components\B4RController;
use app\models\Package;
use Yii;

class DefaultController extends B4RController
{
     /**
     * Renders view more information of a package in widget
     * @return mixed package information
     */
    public function actionViewInfo(){
        $id = isset($_POST['id']) ? $_POST['id'] : null;

        $data = Package::getPackageInfo($id);

        return Yii::$app->controller->renderAjax('@app/widgets/views/package/tabs.php',[
            'data' => $data,
            'id' => $id,
        ]);
    }

}