<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* Renders browser and manage seasons page
*/

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use app\components\FavoritesWidget;

use app\models\Season;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

$browserId = 'dynagrid-season';

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);
?>

<div class="season-index">
    <!-- Browse seasons -->
    <?php
        // action columns
        $actionColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
            ],
            [
                'class'=>'kartik\grid\ActionColumn',
                'header' => false,
                'noWrap' => true,
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {                        
                        return Html::a('<i class="material-icons">remove_red_eye</i>',
                            '#',
                            [
                                'class' => 'view-season',
                                'title' => 'View season',
                                'data-season_id' => $model['seasonDbId'],
                                'data-season_label' => isset($model['seasonName']) ? $model['seasonName'] : 'Season Name',
                            ]
                        );
                    },
                    'update' => function ($url, $model, $key) {
                        $used = Season::checkIfUsedInStudy($model['seasonDbId']);
                        if($used){return '';} 
                        return Html::a('<i class="material-icons">edit</i>',
                            '#',
                            [
                                'class' => 'update-season',
                                'title' => 'Update season',
                                'data-season_id' => $model['seasonDbId'],
                                'data-season_label' => isset($model['seasonName']) ? $model['seasonName'] : 'Season Name',
                            ]
                        );
                    },
                    'delete' => function ($url, $model, $key) {
                        $used = Season::checkIfUsedInStudy($model['seasonDbId']);
                        if($used){return '';}
                        return Html::a('<i class="material-icons">delete</i>',
                            '#',
                            [
                                'class'=>'delete-season',
                                'title' => 'Delete season',
                                'data-season_id' => $model['seasonDbId'],
                                'data-season_label' => isset($model['seasonName']) ? $model['seasonName'] : 'Season Name',
                            ]
                        );
                    },
                ],
                'visible' => true,
            ],
        ];
        // all the columns that can be viewed in seasons browser
        $columns = [
            [
                'attribute'=>'seasonCode',
                'label' => 'Code',
            ],
            [
                'attribute'=>'seasonName',
                'label' => 'Name',
            ],
            [
                'attribute'=>'description',
            ],
            [
                'class' => '\kartik\grid\BooleanColumn',
                'attribute'=>'isActive',
                'format' => 'raw',
                'label' => 'Is Active?',
                'trueLabel' => 'Yes',
                'falseLabel' => 'No',
                'hAlign'=>'center',
            ],
            [
                'attribute' => 'creator',
            ],
            [
                'attribute' => 'modifier',
                'visible' => false,
            ],
            [
                'attribute'=>'creationTimestamp',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => [
                    'autocomplete' => 'off'
                ],
                'filterWidgetOptions' => [
                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'clearBtn' => true,
                        'todayHighlight' => true,
                    ],
                    'pluginEvents' => [
                        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                    ],
                ],
                'visible' => false,
            ],
            [
                'attribute'=>'modificationTimestamp',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => [
                    'autocomplete' => 'off'
                ],
                'filterWidgetOptions' => [
                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'clearBtn' => true,
                        'todayHighlight' => true,
                    ],
                    'pluginEvents' => [
                        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                    ],
                ],
                'visible' => false,
            ]
        ];  

        $gridColumns = array_merge($actionColumns,$columns);

        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'filename' => 'Seasons',
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false,
            ],
            'showColumnSelector' => false,
            'showConfirmAlert' => false
        ]);

        $createButton = '<a class="btn light-green darken-3 waves-effect waves-light tooltipped" style="margin-right:5px;" id="add-season-btn" data-position="top" 
        data-toggle="modal"
        data-target="#add-season-modal" data-delay="50" data-tooltip="'.Yii::t('app','Create Seasons').'">'.\Yii::t('app','Create').'</a>';

        //dynagrid configuration
        $dynagrid = DynaGrid::begin([
            'columns' => $gridColumns,
            'theme'=>'simple-bordered',
            'showPersonalize'=>true,
            'storage' => DynaGrid::TYPE_SESSION,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'id' => 'grid',
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'showPageSummary'=>false,
                'pjax'=>true,
                'responsiveWrap'=>false,
                'panel'=>[
                    'heading'=>'<h3>'.\Yii::t('app', 'Seasons') .FavoritesWidget::widget([
                        'module' => 'season'
                        ]).'&nbsp;'.
                     '</h3>',
                    'before' => \Yii::t('app', 'This is the browser for all the seasons available in the system. You may manage the seasons here.'),
                    'after' => false,
                ],
                'toolbar' =>  [
                    [
                        'content'=> 
                            $createButton .
                            " " .
                            $export .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index','program'=>$program], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid')]).'{dynagridFilter}{dynagridSort}{dynagrid}',
                    ]
                ]
            ],
            'submitButtonOptions' => [
                'icon' => 'glyphicon glyphicon-ok',
            ],
            'deleteButtonOptions' => [
                'icon' => 'glyphicon glyphicon-delete',
                'label' => 'Remove',
            ],
            'options'=>['id'=> $browserId] // a unique identifier is important
        ]);
        DynaGrid::end();
    ?>    
</div>
<!-- Create a new season -->
<?php 
    $form = ActiveForm::begin([
        'enableClientValidation'=>false,
        'id'=>'create-season-form',
        'action'=>['/season/default/create', 'program' => $program],
        'fieldConfig' => function ($model,$attribute) {
            if (in_array($attribute, ['season_code', 'season_name'])) {
                return ['options' => ['class' => 'input-field form-group col-md-6']];
            } else if (in_array($attribute, ['description', 'isActive'])) {
                return ['options' => ['class' => 'input-field form-group col-md-12']];
            }
        }
    ]); 
?>
<?php
    Modal::begin([
        'id' => 'add-season-modal',
        'header' => '<h4><i class="material-icons">add</i> '.\Yii::t('app','Create').' '. \Yii::t('app','Season').'</h4>',
        'closeButton' => [
            'tag' => 'close-btn',
        ],
        'footer' =>
            Html::a(\Yii::t('app','Cancel'),
            ['#'],
            ['id'=>'cancel-save-btn','data-dismiss'=>'modal']).
            '&emsp;&nbsp;'. // Insert whitespaces
            '<input
                type="checkbox"
                id="season-create-another"
                class="filled-in"
                name="Form[create_another]"
                value="1"
                style="opacity:0"
            />'.
            '<label for="season-create-another">'.\Yii::t('app','Create another').'</label>&emsp;&nbsp;'.
            Html::a(\Yii::t('app','Submit'),
                ['#'],
                [
                    'class'=>'btn btn-primary waves-effect waves-light',
                    'url'=>'#',
                    'id'=>'save-btn'
                ]).'&emsp;',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static']
    ]);
?>
<?= 
    '<div class="modal-notif"></div>'. //modal notification field

    /*
        First row:
        -> season_code
        -> season_name
    */
    $form->field($model, 'season_code')->textInput([
        'maxlength' => true, 
        'id'=>'season-code-textfield',
        'title'=>'Short name identifier or code of the season',
        'oninput' => 'js: $("#season-code-textfield").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());',
        'placeholder' => 'ES',
        'spellcheck' => 'false',
    ])->label('Code').
    $form->field($model, 'season_name')->textInput([
        'maxlength' => true, 
        'id'=>'season-name-textfield',
        'title'=>'Name identifier of the season',
        'oninput' => 'js: $("#season-name-textfield").val(this.value.charAt(0).toUpperCase().concat(this.value.slice(1)));',
        'placeholder' => 'Example',
        'spellcheck' => 'false',
    ])->label('Name').

    /*
        Second row:
        -> description
    */
    $form->field($model, 'description')->textarea([
        'class'=>'materialize-textarea',
        'id'=>'season-description-textarea',
        'title'=>'Description about the season',
        'placeholder' => 'Example season',
    ]).
    
    // Is Active checkbox row
    '<input
        type="checkbox"
        id="season-is_active-chkbx"
        class="filled-in"
        title="Is season activated?"
        name="Season[is_active]"
        style="opacity:0"
    />
    <label for="season-is_active-chkbx">'.\Yii::t('app','Is Active?').'</label>&emsp;&nbsp;'; 
?>

<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>

<?php
    //view season modal
    Modal::begin([
        'id' => 'view-season-modal',
        'header' => '<h4 id="season-label"></h4>',
        'footer' =>
            Html::a(\Yii::t('app','Close'),
            '#',
            [
                'data-dismiss'=>'modal',
                'class'=>'modal-close',
            ]).
            '&emsp;',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static'],
    ]);
    Modal::end();

    //update season modal
    Modal::begin([
        'id' => 'update-season-modal',
        'header' => '<h4 id="update-season-label"></h4>',
        'footer' =>
            Html::a(\Yii::t('app','Cancel'),
            ['#'],
            [
                'id'=>'cancel-save-btn',
                'data-dismiss'=>'modal'
            ]).
            '&emsp;'.
            Html::a(\Yii::t('app','Submit'),
            ['#'],
            [
                'class'=>'btn btn-primary waves-effect waves-light',
                'url'=>'#',
                'id'=>'update-save-btn'
            ]).'&emsp;',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static'],
    ]);
    Modal::end();

    //delete season modal
    Modal::begin([
        'id' => 'delete-season-modal',
        'header' => '<h4 id="delete-season-label"></h4>',
        'footer' =>
            Html::a(
                \Yii::t('app','Cancel'),
                null,
                [
                    'data-dismiss'=>'modal'
                ]
            ).
            '&nbsp;&nbsp' .
            Html::a(
                \Yii::t('app','Confirm'),
                null,
            [
                'class'=>'btn btn-primary waves-effect waves-light delete-season-btn',
                'id'=>'delete-btn'
            ]) .
            '&nbsp;&nbsp',
        'options' => ['data-backdrop'=>'static']
    ]);
?>
<?= '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete <b><span id="season-delete"></span></b>. Click confirm to proceed.' ?>
<?php Modal::end(); ?>

<?php
$viewUrl = Url::to(['default/view']); //url for view season ajax
$updateUrl = Url::to(['default/update-season-form','program'=>$program]); //url for update season ajax
$checkSeasonCodeUrl = Url::to(['default/check-season-code']); // url for check email address ajax
$deleteUrl = Url::to(['default/delete','program'=>$program]); //url for delete season ajax

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    // Set "global" variable(s)
    let currentSeasonDbId = null
    let obj
    let season_label
    let season_id

    $(document).on('ready pjax:success', function(e) {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        
        // remove loading indicator upon export
        $('*[class^="export-full-"]').on('click',function(){
            $('#system-loading-indicator').css('display','none');
        });

        e.stopPropagation();
        e.preventDefault();
    });

    //create new season
    $('#add-season-modal').on('shown.bs.modal', function (e) { //set focus on first field
        $('#season-code-textfield').focus();
        $('.modal-notif').html("");
    });

    $("#save-btn").on('click', async function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        // Validate input fields BEGIN
        let seasonCode = $('#season-code-textfield').val()
        let seasonName = $('#season-name-textfield').val()

        let allValid = true

        // Validate seasonCode
        if (seasonCode == '') {
            $(".field-season-code-textfield").addClass("has-error")
            $(".field-season-code-textfield").find(".help-block").html('Please enter a code for the season')
            allValid = false
        } else if (await isSeasonCodeExisting(seasonCode, 'create', 'code-textfield')) {
            allValid = false
        } else {
            $(".field-season-code-textfield").removeClass("has-error")
            $(".field-season-code-textfield").find(".help-block").html('')
        }

        // Validate seasonName
        if (seasonName == '') {
            $(".field-season-name-textfield").addClass("has-error")
            $(".field-season-name-textfield").find(".help-block").html('Please enter a name for the season')
            allValid = false
        } else {
            $(".field-season-name-textfield").removeClass("has-error")
            $(".field-season-name-textfield").find(".help-block").html('')
        }

        if (allValid) $("#create-season-form").submit();
        else return

        $('.modal-notif').html('<div class="progress"><div class="indeterminate"></div></div>');

        $("#create-season-form").submit(function(e) {
            e.preventDefault()
            e.stopImmediatePropagation()
            $(".form-group").removeClass("has-error") //remove error class
            $(".help-block").html("") //remove existing error messages
            $('.modal-notif').html("")

            let form_data = $("#create-season-form").serialize()
            let action_url = $("#create-season-form").attr("action")

            $.ajax({
                type:"POST",
                dataType: 'json',
                url: action_url,
                data: form_data,
                success: function (data) {
                    $('.modal-notif').html('')
                    if (data.success == true){// data saved successfully
                        if (data.create_another == '1'){ // create another season
                            $('#create-season-form')[0].reset() // Reset input fields and checkbox
                            $('#season-create-another').prop('checked', true) // Retain its state

                            let ctgs = $(".field-season-code-textfield > label")
                            ctgs.addClass("active")

                            $('#season-code-textfield').focus() // Set current cursor here

                            $('.modal-notif').html('<div id="w1-success-0" class="alert success"><div class="card-panel"><i class="fa fa-check"></i> You successfully created <b>' + data.name + '</b>!<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button></div></div>')
                        } else {
                            location.reload()
                        }
                    } else { //validation errors occurred
                        $('.modal-notif').html('<div id="w1-error-0" class="alert error"><div class="card-panel"><i class="fa fa-times"></i> There seems to be a problem while creating a season. Please check that all required fields are correctly specified</div></div>');
                        $.each(data.error, function(fieldName, errorMessage) { // show errors to user
                            $(".field-user-" + fieldName).addClass("has-error")
                            $(".field-user-" + fieldName).find(".help-block").html(errorMessage)
                        });
                    }
                },
                error: function (data) {
                    $('.modal-notif').html('')
                    $('.modal-notif').html('<div id="w1-error-0" class="alert error"><div class="card-panel"><i class="fa fa-times"></i> There were problems while creating a season.</div></div>')
                }
            });            
            
            return false;
        });
    });

    $(document).keyup(function (e) {
        // Upon pressing Escape key, modal clears all input fields, checkboxes, and error messages
        if (e.keyCode === 27 && (!!document.getElementById('cancel-save-btn')))
            clearForm()
    })

    $('#cancel-save-btn').on('click', function(e) {
        clearForm()
    })

    $('close-btn').on('click', function (e) {
        clearForm()
    })

    function clearForm () {
        // Clear code error message
        $(".field-season-code-textfield").removeClass("has-error")
        $(".field-season-code-textfield").find(".help-block").html('')
        $(".field-season-code-textfield-update").removeClass("has-error")
        $(".field-season-code-textfield-update").find(".help-block").html('')

        // Clear name error message
        $(".field-season-name-textfield").removeClass("has-error")
        $(".field-season-name-textfield").find(".help-block").html('')
        $(".field-season-name-textfield-update").removeClass("has-error")
        $(".field-season-name-textfield-update").find(".help-block").html('')

        // Clear Create another
        $(`#season-create-another`).prop('checked', false)

        // Reset input fields and checkbox
        $('#create-season-form')[0].reset()
    }
    //end create new season

    //view season
    $(document).on('click', '.view-season', function() {
        let obj = $(this);
        let season_label = obj.data('season_label');
        let season_id = obj.data('season_id');
        let url = '$viewUrl';
        let view_modal = '#view-season-modal > .modal-dialog > .modal-content > .modal-body';

        $('#season-label').html('<i class="material-icons">remove_red_eye</i> ' + season_label);
        $('#view-season-modal').modal('show');

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
            season_id: obj.data('season_id')
            },
            success: function(response) {
                if (response.grid) {
                    $(view_modal).html(response.grid);
                }
            },
            error: function() {
                $(view_modal).html('<div class="card-panel red"><span class="white-text">There seems to be a problem while viewing the season.</span></div>');
            }
        });
    });
    //end view season 

    //update season
    $('#update-season-modal').on('shown.bs.modal', function() {
        $('#season-code-textfield-update').focus();
        $('.modal-notif').html("");
    });

    $(document).on('click', '.update-season', function() {
        let obj = $(this)
        let season_label = obj.data('season_label')
        let season_id = obj.data('season_id')
        let url = '$updateUrl'
        let update_modal = '#update-season-modal > .modal-dialog > .modal-content > .modal-body'

        $('#update-season-label').html('<i class="material-icons">edit</i> ' + season_label)
        $('#update-season-modal').modal('show')

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                season_id: obj.data('season_id')
            },
            success: function (response) {
                if (response) {
                    $(update_modal).html(response.htmlData)
                    currentSeasonDbId = season_id
                }
            },
            error: function () {
                $(update_modal).html('<div class="card-panel red"><span class="white-text">There seems to be a problem while updating the season.</span></div>')
            }
        });
    });

    $('#update-save-btn').on('click', async function (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        // Validate input fields BEGIN
        let seasonCode = $('#season-code-textfield-update').val()
        let seasonName = $('#season-name-textfield-update').val()

        let allValid = true

        // Validate seasonCode
        if (seasonCode == '') {
            $(".field-season-code-textfield-update").addClass("has-error")
            $(".field-season-code-textfield-update").find(".help-block").html('Please enter a code for the season')
            allValid = false
        } else if (await isSeasonCodeExisting(seasonCode, 'update', 'code-textfield-update', currentSeasonDbId)) {
            allValid = false
        } else {
            $(".field-season-code-textfield-update").removeClass("has-error")
            $(".field-season-code-textfield-update").find(".help-block").html('')
        }

        // Validate seasonName
        if (seasonName == '') {
            $(".field-season-name-textfield-update").addClass("has-error")
            $(".field-season-name-textfield-update").find(".help-block").html('Please enter a name for the season')
            allValid = false
        } else {
            $(".field-season-name-textfield-update").removeClass("has-error")
            $(".field-season-name-textfield-update").find(".help-block").html('')
        }

        if (allValid) $('#update-season-form').submit()
        else return

        $('.modal-notif').html('<div class="progress"><div class="indeterminate"></div></div>');

        $("#update-season-form").submit(async function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            $(".form-group").removeClass("has-error"); // remove error class
            $(".help-block").html(""); // remove existing error messages
            $('.modal-notif').html("");

            let form_data = $("#update-season-form").serialize();
            let action_url = $("#update-season-form").attr("action");

            await $.ajax({
                type:"PUT",
                dataType: 'json',
                url: action_url,
                data: form_data,
                success: function (data) {
                    $('.modal-notif').html('');
                    if (data.success == true) {// season's details updated successfully
                        location.reload()
                    } else { // validation errors occurred
                        $('.modal-notif').html('<div id="w1-error-0" class="alert error"><div class="card-panel"><i class="fa fa-times"></i> There seems to be a problem while updating the season. Please check that all required fields are correctly specified</div></div>');
                        $.each(data.error, function (fieldName, errorMessage) { // show errors to season
                            $('.field-season-' + fieldName).addClass("has-error");
                            $('.field-season-' + fieldName).find(".help-block").html(errorMessage);
                        });
                    }
                },
                error: function (data) {
                    $('.modal-notif').html('');
                    $('.modal-notif').html('<div id="w1-error-0" class="alert error"><div class="card-panel"><i class="fa fa-times"></i> There seems to be a problem while updating the season. Please check that all required fields are correctly specified</div></div>');
                }
            })

            $(".modal-notif").removeClass("progress")

            return false
        })
    });
    //end update season

    //delete season
    $(document).on('click', '.delete-season', function() {
        obj = $(this)
        season_label = obj.data('season_label')
        season_id = obj.data('season_id')

        $('#delete-season-label').html('<i class="material-icons">delete</i> Delete ' + season_label)
        $('#season-delete').html(season_label)
        document.getElementById("delete-btn").setAttribute("data-season_id",season_id)
        $('#delete-season-modal').modal('show')
    })

    $('#delete-btn').on('click', function(e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        let url = '$deleteUrl'

        $('#delete-btn').addClass('disabled')

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                season_id: obj.data('season_id')
            },
            async: true,
            success: function (res) {
                let message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i> There seems to be a problem with deleting ' + res.seasonName + '.'

                if (res.success) {
                    $.pjax.reload({
                        container: '#dynagrid-season-pjax'
                    })
                    message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i> Successfully deleted ' + res.seasonName + '!'
                }

                $('#delete-season-modal').modal('hide')
                $('#delete-btn').removeClass('disabled')

                Materialize.toast(message, 5000)
            },
            error: function (res) {
                let message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i> There seems to be a problem with deleting ' + res.seasonName + '.'

                $('#delete-season-modal').modal('hide')
                $('#delete-btn').removeClass('disabled')
                Materialize.toast(message, 5000)
            }
        })
    })
    //end delete season

    // update date input field labels
    $("[for=season-is_active-chkbx]").addClass('active');
    $("[for=season-is_active-chkbx-update]").addClass('active');

    /*
        Validators
    */

    async function isSeasonCodeExisting(seasonCode, action, fieldName, seasonDbId = null) {
        let checkSeasonCodeUrl =
            '$checkSeasonCodeUrl' +
            '?seasonCode=' + seasonCode +
            '&action=' + action +
            '&seasonDbId=' + seasonDbId
        let exists = false
        let obj = $(this)

        await $.ajax({
            url: checkSeasonCodeUrl,
            type: 'post',
            dataType: 'json',
            data: {
                season_id: obj.data('season_id')
            },
            success: function (data) {
                if (data.exists) {
                    $('.field-season-' + fieldName).addClass("has-error")
                    $('.field-season-' + fieldName).find(".help-block").html('Code already exists in the database. Please enter a different one')
                    exists = true
                }
            },
            error: function (data) {
                $('.field-season-' + fieldName).addClass("has-error")
                $('.field-season-' + fieldName).find(".help-block").html('There was a problem with processing the code')
                exists = true
            }
        });

        return exists
    }

JS
);
?>
