<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

    /**
     * Renders update web form
     */

    use yii\bootstrap\ActiveForm;
    use app\models\Season;

    //dependent injection
    $season = \Yii::$container->get('app\models\Season');
    
    /* @var $this yii\web\View */
    /* @var $model app\models\Season */

    $form = ActiveForm::begin([
        'enableClientValidation'=>false,
        'id'=>'update-season-form',
        'action'=>['/season/default/update','id'=>$seasonDbId,'program'=>$program],
        'fieldConfig' => function ($model, $attribute) {
            if (in_array($attribute, ['season_code', 'season_name'])){
                return ['options' => ['class' => 'input-field form-group col-md-6']];
            } else if (in_array($attribute, ['description', 'isActive'])) {
                return ['options' => ['class' => 'input-field form-group col-md-12']];
            }
        }
    ]);
?>
<?=
    '<div class="modal-notif"></div>'. //modal notification field

    /*
        First row:
        -> season_code
        -> season_name
    */
    $form->field($model, 'season_code')->textInput([
        'maxlength' => true, 
        'id'=>'season-code-textfield-update',
        'title'=>'Short name identifier or code of the season',
        'autofocus' => 'true',
        'oninput' => 'js: $("#season-code-textfield-update").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());',
        'placeholder' => 'ES',
        'spellcheck' => 'false',
    ])->label('Code').
    $form->field($model, 'season_name')->textInput([
        'maxlength' => true, 
        'id'=>'season-name-textfield-update',
        'title'=>'Name identifier of the season',
        'oninput' => 'js: $("#season-name-textfield-update").val(this.value.charAt(0).toUpperCase().concat(this.value.slice(1)));',
        'placeholder' => 'Example',
        'spellcheck' => 'false',
    ])->label('Name').

    /*
        Second row:
        -> description
    */
    $form->field($model, 'description')->textarea([
        'class'=>'materialize-textarea',
        'id'=>'season-description-textarea-update',
        'title'=>'Description about the season',
        'placeholder' => 'Example season',
    ]).

    // Is Active checkbox row
    '<input
        type="checkbox" ' .
        (($season->getSeason($seasonDbId)['isActive']) ? 'checked ' : '') .
        'id="season-is_active-chkbx-update"
        class="filled-in"
        title="Is season activated?"
        name="Season[is_active]"
        style="opacity:0"
    />
    <label for="season-is_active-chkbx-update">'.\Yii::t('app','Is Active?').'</label>&emsp;&nbsp;'; 
?>

<?php ActiveForm::end(); ?>

<style type="text/css">
    .control-label {
        transform: translateY(-14px) scale(0.8) !important;
        -webkit-transform-origin: 0 0 !important;
        transform-origin: 0 0 !important;
    }
</style>