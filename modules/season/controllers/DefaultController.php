<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Contains methods for managing seasons
 */

namespace app\modules\season\controllers;

use Yii;
use stdClass;
use app\models\Season;
use app\models\SeasonSearch;
use app\components\B4RController;
use yii\web\NotFoundHttpException;
use yii\widgets\DetailView;

class DefaultController extends B4RController
{
    protected $season;
    protected $seasonSearch;

    public function __construct (
        $id,
        $module,
        Season $season,
        SeasonSearch $seasonSearch,
        $config=[]
    )
    {
        $this->season = $season;
        $this->seasonSearch = $seasonSearch;

        parent::__construct($id, $module, $config);
    }

    /**
     * Lists all seasons
     * Manage seasons
     *
     * @param $userId integer user identifier
     * @param $program text current program abbrev of user
     * 
     * @return mixed
     */
    public function actionIndex (int $userId = null, $program)
    {
        $dataProvider = null;

        // Set user id to session
        if (isset($userId) && !empty($userId)) {
            $session = Yii::$app->session;
            $session->set('userId', $userId);
        }

        // Check if at least 1 field has a value
        // This is more reliable than empty($seasonSearchFilters)...
        // ...because once a user has used the search filters...
        // ...the latter function will always return false during the session
        $seasonSearchFilters = isset($_GET['SeasonSearch']) ?
            $_GET['SeasonSearch'] : [];
        $hasFilters = false;
        foreach ($seasonSearchFilters as $key => $value)
            if ($value !== "") $hasFilters = true;

        if ($hasFilters) {
            $searchParams = [];

            // Exclude any field that has empty value
            foreach ($seasonSearchFilters as $key => $value)
                if ($value != "") $searchParams[$key] = $value;

            // If searchParams is empty...
            // ...assign an empty JSON-ish Object to SeasonSearch
            $searchParams = [
                'SeasonSearch' => (count($searchParams) == 0) ?
                    new stdClass : $searchParams
            ];

            if (!empty($searchParams))
                $dataProvider = $this->seasonSearch->search($searchParams);
        } else {
            $dataProvider = $this->season->getSeasonsArrayDataProvider();
        }

        // View all seasons and renders season browser
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $this->season,
            'searchModel' => $this->seasonSearch,
            'program' => $program
        ]);
    }

    /**
     * Creates a Season.
     * If creation is successful...
     * ...the browser will be redirected to the 'browse' page.
     * 
     * @param string $program
     * 
     * @return mixed
     */
    public function actionCreate ($program)
    {
        if ($this->season->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $seasonParams = $post['Season'];

            $processedParams = $this->season->processSeasonParams(
                $seasonParams,
                'POST'
            );
            $seasonName = $processedParams['records'][0]['seasonName'];

            $result = $this->season->createSeason($processedParams);

            if (!empty($_POST['Form']) && isset($_POST['Form']) && $result) {
                // Proceed to create another Season
                return json_encode([
                    'success' => $result,
                    'name' => $seasonName,
                    'error' => [],
                    'create_another' => $_POST['Form']['create_another']
                ]);
            } else if ($result) {
                // Refresh page and notify user of successful creation
                Yii::$app->session->setFlash(
                    'success',
                    '<i class="fa fa-check"></i> You successfully created <b>' .
                    $seasonName .
                    '</b>!'
                );

                return json_encode([
                    'success' => $result,
                    'name' => $seasonName,
                    'error' => [],
                    'create_another' => "0"
                ]);
            } else {
                // Notify user of failed creation
                return json_encode([
                    'success' => false
                ]);
            }
        }

        return $this->redirect(['default/index', 'program' => $program]); 
    }

    /**
     * Displays a single Season.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView ()
    {
        $id = $_POST['season_id'];
        $data = $this->season->getSeason($id);

        $response['grid'] = '
            <div style="margin-bottom: 16px;">
                <span>
                    Below displays the basic and audit information about the season
                </span>
            </div>' .
            '<label>Basic Information</label>' .
            DetailView::widget([
                'model' => $data,
                'attributes' => [
                    [
                        'label' => 'Code',
                        'value' => function ($model) {
                            return $model['seasonCode'];
                        },
                    ],
                    [
                        'label' => 'Name',
                        'value' => function ($model) {
                            return $model['seasonName'];
                        },
                    ],
                    'description:ntext',
                    [
                        'label' => 'Is Active?',
                        'value' => function ($model) {
                            return ($model['isActive'] == 1) ? 'Yes' : 'No';
                        },
                    ],
                ],
            ]) .
            '<label>Audit Information</label>' .
            DetailView::widget([
                'model' => $data,
                'attributes' => [
                    [
                        'label' => 'Creator',
                        'value' => function ($model) {
                            return $model['creator'];
                        },
                    ],
                    'creationTimestamp:datetime',
                    [
                        'label' => 'Modifier',
                        'value' => function ($model) {
                            return $model['modifier'];
                        },
                    ],
                    'modificationTimestamp:datetime',

                ],
            ]);

        return json_encode($response);
    }

    /**
     * Renders update season form
     * 
     * @param integer $id
     * 
     * @return mixed
     */
    public function actionUpdateSeasonForm ($program)
    {
        $id = $_POST['season_id'];
        $model = $this->findModel($id);

        $htmlData = $this->renderAjax('update', [
            'seasonDbId' => $id,
            'model' => $model,
            'program' => $program
        ], true, false);

        return json_encode([
            'htmlData' => $htmlData
        ]);
    }

    /**
     * Updates an existing Season.
     * If update is successful, the browser will be redirected to the 'browse' page.
     * 
     * @param integer $id
     * @param $program text current program abbrev of user
     * 
     * @return mixed
     */
    public function actionUpdate ($id, $program)
    {
        $oldModel = $this->season->getSeason($id);

        if ($this->season->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $seasonParams = $post['Season'];

            // build PUT parameters
            $processedParams = $this->season->processSeasonParams($seasonParams);

            $result = $this->season->updateSeason($id, $processedParams);

            if ($result) {
                $updatedModel = $this->season->getSeason($id);

                Yii::$app->session->setFlash(
                    'success',
                    '<i class="fa fa-check"></i> You successfully updated <b>' .
                        $updatedModel['seasonName'] .
                    '</b>!');

                return json_encode([
                    'success' => $result,
                    'name' => $updatedModel['seasonName'],
                    'error' => []
                ]);
            } else {
                return json_encode([
                    'success' => false,
                    'name' => $oldModel['seasonName'],
                    'error' => []
                ]);
            }
        }

        return $this->redirect(['default/index', 'program' => $program]);
    }

    /**
     * Deletes an existing Season.
     * 
     * @param integer $id
     * 
     * @return mixed
     */
    public function actionDelete ()
    {
        $id = $_POST['season_id'];
        $model = $this->season->getSeason($id);
        $result = $this->season->deleteSeason($id);

        return json_encode([
            'success' => $result,
            'seasonName' => $model['seasonName'],
        ]);
    }

    /**
     * Checks if input season code already exists in the database.
     * If it already exists, this function returns a boolean value of true...
     * ...else, false
     * 
     * @param string $seasonCode
     * @param string $action
     * @param integer $seasonDbId
     * 
     * @return mixed
     */
    public function actionCheckSeasonCode ($seasonCode, $action, $seasonDbId = null)
    {
        $result = $this->season->isSeasonCodeExisting($seasonCode, $action, $seasonDbId);
        
        if ($result)
            return json_encode(['exists' => true]);
        else 
            return json_encode(['exists' => false]);
    }

    /**
     * Finds the Season model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @param integer $id
     * 
     * @return Season the loaded model
     * 
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        $model = $this->season;
        $seasonInstance = $this->season->getSeason($id);

        if ($seasonInstance !== null) {
            $model['seasonCode'] = $seasonInstance['seasonCode'];
            $model['season_code'] = $seasonInstance['seasonCode'];
            $model['seasonName'] = $seasonInstance['seasonName'];
            $model['season_name'] = $seasonInstance['seasonName'];
            $model['description'] = $seasonInstance['description'];
            $model['isActive'] = $seasonInstance['isActive'];
            $model['is_active'] = $seasonInstance['isActive'];

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
