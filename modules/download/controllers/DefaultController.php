<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\download\controllers;

use Yii;

use app\components\B4RController;
use app\controllers\BrowserController;
use app\interfaces\models\IApi;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IPerson;
use app\models\Program;
use app\models\Worker;
use app\modules\download\models\DownloadModel;
use yii\helpers\Url;

/**
 * Default controller for the `download` module
 */
class DefaultController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        protected IApi $api,
        protected BrowserController $browserController,
        protected IOccurrence $occurrence,
        protected IPerson $person,
        protected Program $program,
        protected DownloadModel $downloadModel,
        protected Worker $worker
    )
    { parent::__construct($id, $module); }

    /**
     * Renders the contents of the download modal
     *
     * @return html content of modal_body.php
     */
    public function actionRenderModalBody (
        $program,
        $entities,
        $isInDataBrowserPage,
        $returnUrl,
        $functionalityName
    )
    {
        // Retrieve selected occurrence IDs from Yii session
        $selectedIds = \Yii::$app->session->get('EMSelectedOccurrenceIds');
        $program = $program ?? throw new \Exception('Missing $program', 400);
        $entities = $entities ?? [];
        $isInDataBrowserPage = $isInDataBrowserPage ?? 'true';
        $functionalityName = $functionalityName ?? '';
        
        if(!is_array($selectedIds)){
            $selectedIds = json_decode($selectedIds) ?? throw new \Exception('Missing $selectedIds', 400);
        }

        if($isInDataBrowserPage != 'true'){
            $returnUrl = $returnUrl . "&id=" . $selectedIds[0];
        }

        if (!is_array($entities)) {
            $entities = explode(',', $entities);
        }

        // Retrieve occurrence information
        $response = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'occurrenceDbId' => 'equals ' . implode(' | equals ', $selectedIds),
            ]),
            'sort=occurrenceDbId:DESC',
            true
        );

        // Arrange retrieved occurrences
        $data = $response['data'];
        $occurrences = [];

        foreach ($data as $occurrence) {
            $occurrences[$occurrence['occurrenceName']] = Url::to([
                    '/occurrence/view',
                    'program' => $program,
                    'id' => $occurrence['occurrenceDbId'],
            ]);
        }

        // Retrieve User's current role and if they are an Admin
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);
        $configAbbrev = '';

        if (isset($role) && !empty($role) && strtoupper($role) == 'COLLABORATOR') {
            $configAbbrev = 'CB_ROLE_COLLABORATOR_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS';
        } elseif(isset($program) && !empty($program)) {
            $programUppercased = strtoupper($program);
            $configAbbrev = "CB_PROGRAM_{$programUppercased}_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS";
        } else {
            $configAbbrev = 'CB_GLOBAL_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS';
        }

        // Attempt to retrieve chosen config
        $config = $this->api->getApiResults(
            'GET',
            "configurations?abbrev=$configAbbrev&limit=1",
            retrieveAll: false
        );

        // If config does not exist, get global config
        if ($config['totalCount'] == 0) {
            $config = $this->api->getApiResults(
                'GET',
                "configurations?abbrev=CB_GLOBAL_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS&limit=1",
                retrieveAll: false
            );
        }

        $configValue = (isset($config['data'][0]['configValue'])) ? $config['data'][0]['configValue'] : [];

        if (empty($entities)) {
            $entities = array_keys($configValue);
        }

        // applicable only if METADATA_AND_PLOT_DATA
        if ($functionalityName == 'METADATA_AND_PLOT_DATA') {

            $entitiesToFilter = [
                'occurrence',
                'planting_protocol',
                'management_protocol',
                'trait',
                'germplasm',
            ];

            $variableAttributes = [];

            // Get entities with data
            foreach ($selectedIds as $occurrenceId) {
                $searchEntityData = $this->api->getApiResults(
                    'POST',
                    "occurrences/$occurrenceId/entity-data-search",
                    json_encode([
                        'entities' => implode('|', $entitiesToFilter)
                    ]),
                    'limit=1'
                );

                $entityDataRecord = $searchEntityData['data'][0];

                foreach (array_keys($entityDataRecord) as $key) {
                    // filter out metadata variableAttributes...
                    // ...that do not have AT LEAST 1 data value...
                    // ...e.g. LONGITUDE, OCCURRENCE_ECOSYSTEM, MC_CONT
                    if (
                        ($key != strtoupper($key)) || // variableAttributes such as occurrenceCode, entryNumber, etc are always displayed in the UI
                        ($key == strtoupper($key) && !str_contains($key, '_QC')) // i.e. this attribute should not be a QC code attribute
                    ) {
                        array_push($variableAttributes, $key);
                    }
                }
            }

            // Unset fields from config without data
            foreach ($entitiesToFilter as $entity) {
                // Re-assign to iterate
                $entityVariables = $configValue[$entity];

                foreach ($entityVariables as $key => $value) {
                    if (!in_array($value['attribute'], $variableAttributes)) {
                        unset($configValue[$entity][$key]);
                    }
                }
            }
        }

        // Render the download modal content with parameters
        return $this->render('modal_body', [
            'program' => $program,
            'occurrences' => $occurrences,
            'occurrencesJson' => json_encode(array_fill_keys(array_keys($occurrences), null)),
            'entities' => $entities,
            'configValue' => $configValue,
            'selectedIds' => json_encode($selectedIds),
            'isInDataBrowserPage' => $isInDataBrowserPage,
            'functionalityName' => $functionalityName,
            'returnUrl' => $returnUrl,
            'sourceTool' => 'Experiment Manager'
        ]);
    }

    /**
     * Invokes worker responsible for generating and downloading CSV files
     *
     */
    public function actionDownload ()
    {

        $selectedIds = isset($_POST['selectedIds']) ? json_decode($_POST['selectedIds']) : throw new \Exception('Missing $selectedIds', 400);
        $csvHeaders = isset($_POST['csvHeaders']) ? json_decode($_POST['csvHeaders']) : [];
        $csvAttributes = isset($_POST['csvAttributes']) ? json_decode($_POST['csvAttributes']) : [];
        $filenameAbbrev = $_POST['filenameAbbrev'] ?? '';
        $sourceUrl = $_POST['sourceUrl'] ?? '';
        $entities = isset($_POST['entities']) ? json_decode($_POST['entities']) : [];
        $program = $_POST['program'] ?? throw new \Exception('Missing $program', 400);

        $fileNameSuffix = '';
        $fileName = '';

        // Retrieve export settings
        $exportSettings = Yii::$app->session->get('em-metadata-export-options') ?? [];
        if(isset($exportSettings) && !empty($exportSettings)){
            $exportSettings = json_decode($exportSettings);
        }     

        if(!is_array($selectedIds)){
            $selectedIds = json_decode($selectedIds);
        }

        if (count($selectedIds) == 1) {
            // Generate file name for a single-occurrence download use case
            $fileName = $this->browserController->getFileNameFromConfigByEntityId(
                $selectedIds[0],
                $filenameAbbrev
            );

            $sourceUrl = ['/occurrence?program='.$program.'&OccurrenceSearch[occurrenceDbId]='.$selectedIds[0]];
        } else {
            // Generate file name for a multi-occurrence download use case
            [
                $fileNameAttribute,
                $fileNameSuffix
            ] = \Yii::$app->config->getFileNameAttributeAndSuffix($filenameAbbrev);

            $timestamp = date("Ymd_His", time());
            $fileName = "Multi-Occurrences_{$fileNameSuffix}_CSV_{$timestamp}.csv";

            $formattedOccurrenceId = implode('|', $selectedIds);
            $sourceUrl = ['/occurrence?program='.$program.'&OccurrenceSearch[occurrenceDbId]='.$formattedOccurrenceId];
        }

        // add DATA_QC_CODE columns
        $program = $_POST['program'] ?? throw new \Exception('Missing $program', 400);

        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);
        
        $configTraitVariables = $this->downloadModel->getConfigVariables('trait',$isAdmin,$role,$program);
        $configGermplasmDataVariables = $this->downloadModel->getConfigVariables('germplasm',$isAdmin,$role,$program);
        $configTraitVariables = array_merge($configTraitVariables,$configGermplasmDataVariables);

        if(!empty($configTraitVariables)){
            // retrieve the traits identified as column headers
            $identifiedTraits = array_filter(
                $configTraitVariables,
                function ($trait) use ($csvAttributes) {
                    return in_array($trait['abbrev'],$csvAttributes);
                }
            );

            // add QC column header for identified trait column header
            foreach($identifiedTraits as $trait){
                $dataQcCode = $trait['abbrev'] . '_QC';

                // Insert VAR_ABBREV_QC
                if (!in_array($dataQcCode, $csvAttributes)) {
                    // right after the matching trait attribute
                    array_splice($csvAttributes, array_search($trait['abbrev'],$csvAttributes)+1, 0, $dataQcCode);
                    
                    // right after matching trait header
                    array_splice($csvHeaders, array_search($trait['label'],$csvHeaders)+1, 0, $dataQcCode);
                }
            }
        }

        $fileNameReplaced = str_replace('_', ' ', $fileNameSuffix);
        $description = "Preparing $fileNameReplaced data for downloading";
        $httpMethod = 'POST';
        $url = "occurrences/:id/entity-data-search";
        $isTemplateUrl = true;

        $params = [ 'entities' => implode('|', $entities) ];

        // Add settings to params when needed
        if(isset($exportSettings) && !empty($exportSettings)){
            if(isset($exportSettings->replaceTraitValues) && !empty($exportSettings->replaceTraitValues)){
                $params['replaceTraitDataValue'] = $exportSettings->replaceTraitValues;
            }
        }

        $requestBody = json_encode($params);
        $idArray = $selectedIds;

        $this->worker->invoke(
            'BuildCsvData',
            $description,
            'OCCURRENCE',
            $selectedIds[0],
            'OCCURRENCE',
            'OCCURRENCES',
            'GET',
            compact(
                'description',
                'fileName',
                'url',
                'isTemplateUrl',
                'httpMethod',
                'requestBody',
                'idArray',
                'csvAttributes',
                'csvHeaders'
            ),
            [
                'remarks' => $fileName,
            ]
        );

        // Prepare setFlash notification content
        $message = "";
        if (count($selectedIds) == 1) {
            // Get occurrence name
            $occurrenceName = $this->api->getApiResults(
                'POST',
                'occurrences-search',
                json_encode([
                    'occurrenceDbId' => "equals {$selectedIds[0]}"
                ]),
                '?limit=1'
            )['data'][0]['occurrenceName'];

            // Get file name suffix...
            [
                $fileNameAttribute,
                $fileNameSuffix
            ] = \Yii::$app->config->getFileNameAttributeAndSuffix($filenameAbbrev);
            // ...Then convert to a readable functionality name
            $fileNameReplaced = str_replace('_', ' ', $fileNameSuffix);

            // Build message
            $message = "The $fileNameReplaced records for <strong>$occurrenceName</strong> are being prepared in the background for download.
                        You may proceed with other tasks. You will be notified in this page once done.";
        } else {
            $message = "The $fileNameReplaced records for <strong>multiple occurrences</strong> are being prepared in the background for download.
                        You may proceed with other tasks. You will be notified in this page once done.";
        }

        $content = "<i class='fa fa-info'></i> $message <button data-dismiss='alert' class='close' type='button'>×</button>";

        // Load flash notification content to be displayed upon redirection
        Yii::$app->session->setFlash('info', $content);

        Yii::$app->response->redirect($sourceUrl);
    }

    /**
     * Set selected occurrence IDs to session
     *
     * @param string $selectedOccurrenceIds string of selected occurrence IDs
     */
    public function actionSetSelectedOccurrenceIds () {
        $selectedOccurrenceIds = $_POST['selectedOccurrenceIds'] ?? '';

        \Yii::$app->session->set('EMSelectedOccurrenceIds', $selectedOccurrenceIds);
    }
}
