<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\download\controllers;

use Yii;

use app\components\B4RController;

/**
 * Controller for export management
 */
class ExportManagementController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module)
    { parent::__construct($id, $module); }

    /**
     * Set export options
     */
    public function actionSetExportOptions() {
        $postParams = $_POST;

        if(isset($postParams['exportSettings'])){
            Yii::$app->session->set('em-metadata-export-options', $postParams['exportSettings']);            
        }

        return json_encode(['success'=>true]);
    }
}