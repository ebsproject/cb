<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Displays list of selected occurrence records and provides a type filter...
 * ...to conveniently look for specific occurrence(s)
 */
?>

<!-- Display section title and helper text -->
<div class="panel panel-default">
    <div class="row" style="margin:10px;">
        <div>
            <h4>Selected Occurrences</h4>
            <p>
                Browse through your selected occurrences here.
                You may also <strong>search</strong> and <strong>click</strong>
                individual occurrences to navigate to the <strong>View Occurrence</strong> page.
            </p>
        </div>

        <!-- Display type filter -->
        <div class="input-field" style="width: 420px;">
            <input
                type="text"
                class="autocomplete tooltipped input-occurrence"
                data-position="top"
                data-tooltip="<?echo \Yii::t('app', 'Search for your selected occurrence here')?>"
                placeholder="<?echo \Yii::t('app', 'Search for your selected occurrence here')?>"
            >
        </div>

        <!-- Display list of selected occurrences -->
        <!-- Users can click an occurrence to navigate to View Occurrence -->
        <ul class="row scrollable occurrences collection">
            <?foreach ($occurrences as $occurrenceName => $href) {?>
                <a
                    class="collection-item occurrence-record waves-effect waves-light"
                    href="<?echo $href?>"
                    data-occurrence-name="<?echo $occurrenceName?>"
                >
                    <i class="material-icons left">remove_red_eye</i>
                    <p class="occurrence-name"><?echo $occurrenceName?></p>
                </a><?
            }?>
        </ul>
    </div>
</div>
<?

$this->registerJs(<<< JS
var occurrencesJson = $occurrencesJson
var autocompleteTimer = 0

// Initialize Materialize CSS classes
$('.tooltipped').tooltip()
$('input.autocomplete').autocomplete({
    data: occurrencesJson,
})

$(document).ready(function () {
    refreshOccurrences()
})

// reload grid on pjax
$(document).on('ready pjax:success', function(e) {
    e.stopPropagation();
    e.preventDefault();
    refreshOccurrences();
});

function refreshOccurrences(){
    $('.occurrence-record').on('click', function (e) {
        const occurrenceName = $(this).data('occurrence-name')
        const message = `Loading occurrence information for <strong>\${occurrenceName}</strong>. Please wait.`

        // Display helper text and loading indicator
        $(`download-widget-modal-body`).html(`
            <p>\${message}</p>
            <div class="progress"><div class="indeterminate"></div></div>
        `)

        // Disable download button by default
        $(`.download-widget-download-button`).addClass('disabled')
    })

    $('.input-occurrence').on('input change', function (e) {
        const value = $(this).val().toLowerCase()

        // cancel previously set timer
        if (autocompleteTimer) {
            clearTimeout(autocompleteTimer)
        }
        
        // Add delay
        autocompleteTimer = setTimeout(function() {
            if (!value) { // If input field is empty
                // Display all selected occurrences
                $('.occurrence-record').each(function (i) {
                    $(this).removeClass('hidden')
                })
            } else { // If input field has value
                // Temporarily hide occurrences that do not match with the input value
                $('.occurrence-record').each(function (i) {
                    if (value == $(this).children('.occurrence-name').text().toLowerCase()) {
                        $(this).removeClass('hidden')
                        return
                    }

                    $(this).addClass('hidden')
                })
            }
        }, 300)
    })
}

JS);

Yii::$app->view->registerCss('
    ul.row.scrollable {
        max-height: 160px;
        overflow-y: auto;
    }

    .occurrence-name {
        margin-bottom: 0px;
    }

    ul.autocomplete-content.dropdown-content {
        max-height: 210px;
        overflow-y: scroll !important;
        position: absolute;
        top: 66px;
        width: inherit;
    }
');