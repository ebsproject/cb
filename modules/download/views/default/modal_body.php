<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

Modal::begin([]);
Modal::end();
?>

<? 
    $baseUrl = Url::base(); 

    $functionName = str_replace('_'," ",$functionalityName);
    $functionalityNameLabel = ucwords(strtolower($functionName));
?>

<div class="progress hidden"><div class="indeterminate"></div></div>

<div class="row">
    <!-- Page Title -->            
    <div class="col-md-9">
        <div class="working-list-title pull-left" style="margin-left:10px;">
            <h3>
                <?  echo Html::a(Yii::t('app', $sourceTool), Url::to($baseUrl.'/index.php/'.$returnUrl))
                    . ' <small> » ' 
                    . Yii::t('app', 'Export ' . $functionalityNameLabel) 
                    . '</small>'
                ?>
            </h3><br/>
            <span>
                <h5><? 
                    echo Yii::t(
                        'app', 
                        "Use this page to choose what information you would like included in you exported CSV file"
                    )?>
                </h5>
            </span>
        </div>
    </div>

    <!-- Back Button -->
    <div class="col-md-3">
        <div class="working-list-title pull-right align=right">
            <?=
                Html::a(
                    Yii::t('app','Cancel'),
                    Url::to($baseUrl.'/index.php/'.$returnUrl),
                    [
                        'class' => 'btn',
                        'id' => 'data-export-btn',
                        'type' => 'button',
                        'style' => 'margin-bottom:5px;margin-top:5px',
                ])
                .Html::a(
                    \Yii::t('app', 'Export'),
                    '#',
                    [
                        'class' => '
                            download-widget-download-button
                            btn btn-default pull-right lighten-4 hide-loading
                        ',
                        'style' => 'margin:5px',
                        'data-source-url' => $returnUrl,
                        'data-entities' => $entities
                ]);
            ?>
        </div>
    </div>
</div>

<?php
// Render Selected Occurrences UI
if ($isInDataBrowserPage == 'true') {
    echo Yii::$app->controller->renderPartial('_selected_occurrences', [
        'program' => $program,
        'occurrences' => $occurrences,
        'occurrencesJson' => $occurrencesJson
    ]);
}
?>

<div class="panel panel-default">
<?php
// Render Selected Columns UI
echo Yii::$app->controller->renderPartial('_selected_columns', [
    'program' => $program,
    'entities' => $entities,
    'functionalityName' => $functionalityName,
    'configValue' => $configValue,
]);

$downloadUrl = Url::toRoute(['/download/default/download']);
$entities = json_encode($entities);

$this->registerJs(<<< JS

    var selectedIds = '$selectedIds'
    var entities = '$entities'
    var returnUrl = '$returnUrl'
    var program = '$program'
    var functionalityName = '$functionalityName'

    $(document).ready(function () {
        refreshDownload()
    })

    function refreshDownload(){
        // Trigger only the download processing
        // ALWAYS trigger the background worker for this download process
        $('.download-widget-download-button').off('click').on('click', function (e) {
            let csvHeaders = [ 'Occurrence ID' ] // system default CSV header
            let csvAttributes = [ 'occurrenceDbId' ] // system default CSV attribute

            $(`div.collapsible-body > * > ul.displayed-columns`).each(function (index, elem) {
                $(this).children().each(function (i, e) {
                    // Append this label as CSV Header
                    csvHeaders.push($(this).data('label'))
                    
                    // Append this as a CSV attribute based on whether to retrieve data value (if any) or not
                    if ($(this).data('should-retrieve-data-value') || functionalityName == 'METADATA_AND_PLOT_DATA') {
                        csvAttributes.push($(this).data('attribute'))
                    } else {
                        csvAttributes.push('_')
                    }
                })
            })

            $.ajax({
                url: '$downloadUrl',
                type: 'post',
                dataType: 'json',
                data:{
                    selectedIds: selectedIds,
                    csvHeaders: JSON.stringify(csvHeaders),
                    csvAttributes: JSON.stringify(csvAttributes),
                    filenameAbbrev: functionalityName,
                    entities: JSON.stringify($(this).data('entities')),
                    sourceUrl: returnUrl ?? '',
                    program: program ?? '',
                    functionalityName: functionalityName
                },
                success: function (response) {},
                error: function () {}
            });
        })
    }

JS);