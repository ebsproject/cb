<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Displays list of selected occurrence records and provides a type filter...
 * ...to conveniently look for specific occurrence(s)
 */

use kartik\sortable\Sortable;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<!-- Display section title and helper text -->
<div class="row" style="margin:10px;">
    <div class="col-md-9">
        <h4>Data</h4>
        <p>
            Select your preferred variables under the categories.
            Click the <strong>EXPORT</strong> button once you're satisfied with your selection.
        </p>
    </div>
    <div class="col-md-3">
        <?
            $replaceTraitValue = false;
            $exportOptions = "
                <ul id='dropdown-export-options' class='dropdown-content' style='width: 325px !important; margin-top:5px; margin-left:5px;' data-beloworigin='false'>
                    <li>
                        <div class='switch left' style='display:inline-block; margin-top: 10px; margin-left: 5px;'>
                            <label style='margin-bottom:0px;' 
                                title='Replace Bad (B), Supressed (S), and Missing (M) data with NA'>
                                <input type='checkbox' ".$replaceTraitValue." id='replace-trait-value-switch-id' data-pjax=0>
                                <span class='lever'></span>Replace Suppressed and Bad Data with NA
                            </label>
                        </div>
                    </li>
                </ul>
            ";

            if(isset($configValue['trait']) && !empty($configValue['trait'])){
                echo Html::a(
                    '<i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>', '#!',
                    [
                        'class' => 'waves-effect waves-light btn btn-primary dropdown-trigger dropdown-button-pjax pull-right',
                        'style' => 'margin-top:10px; margin-left: 5px;',
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                        'data-activates'=>'dropdown-export-options',
                        'title'=> \Yii::t('app', "Export options")
                    ]).'&emsp;'.$exportOptions;
            }
        ?>
    </div>    
</div>

<!-- Display subtitles -->
<div id="columns-subtitles" style="margin:10px;">
    <h5>Data Categories</h5>
    <h5>Related Data</h5>
</div>

<!-- Display collapsibles/accordions -->
<?
$entityAccordions = [];

foreach ($entities as $entity) {
    $variables = $configValue[$entity];
    $displayedColumns = [];
    $hiddenColumns = [];
    $chips = '';

    foreach ($variables as $variable) {
        if (
            $functionalityName == 'METADATA_AND_PLOT_DATA' ||
            (
                $functionalityName == 'OCCURRENCE_DATA_COLLECTION' &&
                $variable['is_uploadable'] == 'true'
            )
        ) {
            array_push($displayedColumns, [
                'content' =>
                    Html::tag(
                        'p',
                        $variable['label'],
                        [ 'class' => 'inline-text' ]
                    ) .
                    Html::tag(
                        'a',
                        ($variable['should_retrieve_data_value'] == 'true') ? 'add_circle' : 'add_circle_outline',
                        [
                            'class' => 'retrieve-data-value-flag material-icons tooltipped hidden',
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t(
                                'app',
                                'Toggle whether to leave the values for this column blank'
                            ),
                        ]
                    ),
                'options' => [
                    'data-label' => $variable['label'],
                    'data-attribute' => $variable['attribute'],
                    'data-should-retrieve-data-value' => $variable['should_retrieve_data_value'],
                    'data-entity' => ucwords(str_replace('_', ' ', $entity)),
                ]
            ]);

            $chips .= Html::tag(
                'div',
                Html::tag(
                    'p',
                    $variable['label'],
                    [ 'class' => 'inline-text variable' ]
                ) .
                Html::tag(
                    'i',
                    'close',
                    [ 'class' => 'close material-icons' ]
                ),
                [
                    'class' => 'chip grey white-text',
                    'data-label' => $variable['label'],
                    'data-attribute' => $variable['attribute'],
                    'data-should-retrieve-data-value' => $variable['should_retrieve_data_value'],
                    'data-entity' => ucwords(str_replace('_', ' ', $entity)),
                ]
            );
        }
    }

    array_push($entityAccordions, [
        'content' =>
        Html::tag(
            'div',
            Html::tag(
                'i',
                'navigate_next',
                [
                    'class' => 'material-icons collapsible-icon',
                ]
            ) .
            Html::tag(
                'i',
                'menu',
                [
                    'class' => 'material-icons',
                ]
            ) .
            Html::tag(
                'h5',
                ucwords(str_replace('_', ' ', $entity)),
                [
                    'class' => 'inline-text entity',
                    'data-entity' => $entity,
                ]
            ) .
            Html::tag(
                'div',
                $chips,
                [ 'class' => 'selected-columns-preview' ]
            ),
            [ 'class' => 'collapsible-header align-vertical', ]
        ) .
        Html::tag(
            'div',
            Html::tag(
                'div',
                Html::tag(
                    'h5',
                    'Displayed Columns'
                ) .
                Sortable::widget([
                    'connected' => true,
                    'itemOptions' => [ 'class' => 'sortable-variable' ],
                    'items' => $displayedColumns,
                    'options' => [
                        'class' => 'sortable-variable-list displayed-columns',
                        'data-entity' => ucwords(str_replace('_', ' ', $entity)),
                    ],
                ])
            ) .
            Html::tag(
                'div',
                Html::tag(
                    'h5',
                    'Hidden Columns'
                ) .
                Sortable::widget([
                    'connected' => true,
                    'itemOptions' => [ 'class' => 'sortable-variable' ],
                    'items' => $hiddenColumns,
                    'options' => [
                        'class' => 'sortable-variable-list hidden-columns',
                        'data-entity' => ucwords(str_replace('_', ' ', $entity)),
                    ],
                ])
            ),
            [ 'class' => 'collapsible-body', ]
        )
    ]);
}

echo Sortable::widget([
    'itemOptions' => [ 'class' => 'entity-accordion' ],
    'items' => $entityAccordions,
    'options' => ['class' => 'collapsible test'],
]);

?>

<?
    $setExportOptionsUrl = Url::to(['export-management/set-export-options']);

    $this->registerJs(<<< JS
    // Initialize Materialize CSS classes
    $('.tooltipped').tooltip()
    $('.collapsible').collapsible()
    $('#displayed-columns-experiment').autocomplete({})
    $('#hidden-columns-experiment').autocomplete({})

    var setExportOptionsUrl = '$setExportOptionsUrl';

    $(document).ready(function () {
        $('.dropdown-trigger').dropdown();
        refreshColumns()
        updateExportSettings();
    })

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
        refreshColumns();
    });

    $(document).on('click',"#replace-trait-value-switch-id", (e) => {
        updateExportSettings();
    })

    function refreshColumns(){
        // Remove chip and move variable to from Displayed Columns to Hidden Columns
        $('div.chip > .close').on('click', function (e) {
            // Remove chip
            $(this).parent().addClass('hidden')

            const variableLabel = $(this).siblings('p').html()
            const entity = $(`li[data-label|='\${variableLabel}']`).data('entity')

            // Move variable to Hidden Columns section
            $(`ul.hidden-columns[data-entity|='\${entity}']`).prepend($(`li[data-label|='\${variableLabel}']`))
        })

        // Trigger no events upon clicking a chip
        $('div.chip').on('click', function (e) {
            e.stopPropagation()
        })

        // Toggle icon upon User click
        $('.retrieve-data-value-flag').on('click', function (e) {
            if ($(this).html() == 'add_circle') {
                // Disable retrieval of data value for this variable
                $(this).html('add_circle_outline')
                $(this).parent().data('should-retrieve-data-value', false)
            } else {
                // Enable retrieval of data value for this variable
                $(this).html('add_circle')
                $(this).parent().data('should-retrieve-data-value', true)
            }
        })

        // Hide/Show chip depending on which column setion the variable is droppped
        $('.sortable-variable').on('dragend', function (e) {
            const dropSection = $(this).parent().siblings('h5').html()
            const variableLabel = $(this).data('label')

            if (dropSection == 'Hidden Columns') {
                // Hide chip
                $(`.chip[data-label|='\${variableLabel}']`).addClass('hidden')
            } else {
                // Show chip
                $(`.chip[data-label|='\${variableLabel}']`).removeClass('hidden')
            }
        })

        // If user clicks on a collapsible header, then toggle the collapsible icon's state
        $('.collapsible-header').on('click', function (e) {
            if ($(this).find('.material-icons.collapsible-icon').text() == 'navigate_next') {
                // Reset all icons to default state
                $('.material-icons.collapsible-icon').text('navigate_next')
                // Toggle this header's icon state
                $(this).find('.material-icons.collapsible-icon').text('expand_more')
            } else {
                $(this).find('.material-icons.collapsible-icon').text('navigate_next')
            }
        })
    }

    // Update data export settings 
    function updateExportSettings(){

        let replaceTraitValues = $('#replace-trait-value-switch-id').is(':checked');
        let exportSettings = JSON.stringify({'replaceTraitValues':replaceTraitValues});

        $.ajax({
            url: setExportOptionsUrl,
            type: 'post',
            dataType: 'json',
            data:{
                exportSettings: exportSettings
            },
            success: function (response) {}
        });
    }


JS);



Yii::$app->view->registerCss('
    #columns-subtitles {
        display: flex;
        align-items: center;
    }

    #columns-subtitles > * {
        width: 40%;
        margin-bottom: 0;
    }

    .inline-text {
        display: inline;
        margin: 0;
    }
    
    div.align-vertical {
        display: flex;
        align-items: center;
    }

    div.chip {
        cursor: default;
    }

    div.selected-columns-preview {
        max-width: 72%;
        margin-left: auto;
    }

    div.collapsible-body {
        display: flex;
        align-items: flex-start;
        justify-content: space-around;
    }

    .facet-list {
        margin: 0;
        padding: 0;
        margin-right: 10px;
        background: #eee;
        padding: 5px;
        width: 143px;
        min-height: 1.5em;
        font-size: 0.85em;
    }

    .facet-list li {
        margin: 5px;
        padding: 5px;
        font-size: 1.2em;
        width: 120px;
    }

    .facet-list li.placeholder {
        height: 1.2em
    }

    .facet {
        border: 1px solid #bbb;
        background-color: #fafafa;
        cursor: move;
    }

    .facet.ui-sortable-helper {
        opacity: 0.5;
    }

    .placeholder {
        border: 1px solid orange;
        background-color: blue;
    }

    .test {
        min-height: 8px;
        min-width: 120px;
    }
    
    li.entity-accordion {
        margin: 0;
        padding: 0;
        border: 0;
    }

    .retrieve-data-value-flag {
        margin-left: 48px;
    }

    .retrieve-data-value-flag:hover {
        cursor: pointer;
    }

    .sortable-variable {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    div.collapsible-body {
        cursor: default;
    }

    .sortable-variable-list {
        min-height: 40px;
        background-color: white;
    }

    .dropdown-content {
        overflow-y: visible;
    }
    .dropdown-content .dropdown-content {
        margin-left: -100%;
    }
');
