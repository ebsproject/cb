<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\download\models;

use app\models\BaseModel;
use app\interfaces\models\IApi;

/**
 * Model file for DownloadModel
 */
class DownloadModel extends BaseModel
{
    /**
     * Model constructor
     */
    public function __construct ($id, $module,
        protected IApi $api
    ) {}

    /**
     * Retrieve config variables
     * 
     * @param String $entity entity data level
     * @param String $role user role
     * @param String $program dashboard program
     * 
     * @return mixed
     */
    public function getConfigVariables($entity,$isAdmin,$role,$program){
        
        $configAbbrev = '';
        if (isset($role) && !empty($role) && strtoupper($role) == 'COLLABORATOR') {
            $configAbbrev = 'CB_ROLE_COLLABORATOR_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS';
        } elseif (isset($program) && !empty($program)) {
            $programUpper = strtoupper($program);
            $configAbbrev = "CB_PROGRAM_{$programUpper}_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS";
        } else {
            $configAbbrev = 'CB_GLOBAL_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS';
        }

        $config = $this->api->getApiResults(
            'GET',
            "configurations?abbrev=$configAbbrev&limit=1",
            retrieveAll: false
        );

        if (!isset($entity) || empty($entity)) {
            return [];
        }

        $configValue = (isset($config['data'][0]['configValue'])) ? $config['data'][0]['configValue'] : [];

        return $configValue[$entity] ?? [];
    }
}