<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\auth\models;

use Yii;

/**
 * This model class provides methods for managing B4R authentication with the API.
 *
 */
class APIAuthManager{

    /**
     * Request an access token from Service Gateway
     * @param $code text authorization code that was returned by the identity provider
     * @return mixed Return a valid access token if successful, else NULL
     */
    public function requestToken($code){
        $token = NULL;

        try{
            // Set variables
            $tokenUrl = getenv('CB_SG_TOKEN_URL');
            $authorizationUrl = getenv('CB_SG_AUTH_URL');
            $clientId = getenv('CB_SG_CLIENT_ID');
            $clientSecret = getenv('CB_SG_CLIENT_SECRET');
            $redirectUri = getenv('CB_SG_REDIRECT_URI');

            // Retrieve token from API v3
            $client = new \GuzzleHttp\Client(); 
            
            $response = $client->post($tokenUrl,
            [
                'form_params' => [
                    'code' => $code,
                    'client_id' => $clientId,
                    'client_secret' => $clientSecret,
                    'redirect_uri' => $redirectUri,
                    'grant_type' => 'authorization_code'
                ],
                'verify' => FALSE,
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ]
            ]);
        }
        catch (\GuzzleHttp\Exception\ClientException $e){

            $response = $e->getResponse();

        }
        catch (\GuzzleHttp\Exception\ServerException $e){

            $response = $e->getResponse();
        }
        
        // Parse access token if the call was completed successfully
        if($response->getStatusCode() == 200){
   
            $result = json_decode($response->getBody()->getContents(),true);
            
            if ($result!= null) {
                Yii::$app->session->set('user.refresh_token', $result['refresh_token']);
                $token = $result['id_token'];
            }
        }

        return $token;
    }
}
