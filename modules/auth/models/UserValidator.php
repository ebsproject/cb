<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\auth\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * This model class provides methods for validating authenticated users.
 *
 * Provides utility methods to interface with the data model in verifying the
 * access validity of authenticated users.
 *
 */
class UserValidator{
    
    /**
     * Verify user by email
     * @param $email text email identifier of user
     * @return boolean if valid or not
     */
    public function verifyUserByEmail($email){

        $user = User::getPersonInfoByEmail($email);

        if(!empty($user)){
            $session = Yii::$app->session;
            $session->set('user.id',$user['id']);
            $session->set('user.email',$user['email']);
            $session->set('user.type',$user['person_type']);
            $session->set('user.display_name',$user['person_name']);
            $session->set('user.username',$user['username']);
            $session->set('user.first_name',$user['first_name']);
            $session->set('user.last_name',$user['last_name']);

            return TRUE;    
        }
        else{
            return FALSE;
        }
        
    }
}
