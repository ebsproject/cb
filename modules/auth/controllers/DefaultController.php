<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\auth\controllers;

use app\components\B4RController;
use app\controllers\Constants;
use app\modules\auth\models\APIAuthManager;
use app\models\Config;
use app\models\Application;
use app\models\Person;
use app\models\DataBrowserConfiguration;
use Yii;
use yii\helpers\Url;

/**
 * Default controller for the `auth` module
 */
class DefaultController extends B4RController
{
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        
        return $this->render('index');
    }
    
    /**
     * 
     * Allow the user login using the appropriate identity provider.
     * 
     * This will display the login widget for oauth2 and redirect the user to the oauth2 site to initiate
     * the login process for the user.
     * 
     * @return NULL
     *
     */
    public function actionLogin()
    {
        // Get variables
        $authorizationUrl = getenv('CB_SG_AUTH_URL');
        $clientId = getenv('CB_SG_CLIENT_ID');
        $clientSecret = getenv('CB_SG_CLIENT_SECRET');
        $redirectUri = getenv('CB_SG_REDIRECT_URI');

        // Build URL
        $url = $authorizationUrl 
            . '?scope=openid&response_type=code'
            . '&client_id='.$clientId 
            . '&client_secret=' . $clientSecret 
            . '&redirect_uri=' . $redirectUri;

        $this->redirect($url, 301);
    }
    
    /**
     * 
     * Allow the user logout from the application.
     * 
     * This will exit the current user session and completely log out the user
     * from the application.
     * 
     * @return NULL
     *
     */
    public function actionLogout()
    {
        // Remove session variables
        \Yii::$app->cbSession->flushSession();

        Yii::$app->user->logout();
        
        $session = Yii::$app->session;
        
        $session->destroy(); // Destroy all application session data
        
        Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully logged out!'));
        
        // Go back to the homepage
        return $this->goHome();
    }
    
    /**
     * Verify that the authenticated user should be allowed access to EBS-CB.
     *
     * Cross-check the authenticated user using the email address with the EBS-CB database to ensure
     * that the user should be allowed access to EBS-CB.
     *
     * @return NULL
     *
     */
    public function actionVerify() {
        $errorMessage = '';
        try {
            $code = trim($_GET['code']);

            if ($code == '' || $code == null) {
                $errorMessage = 'Code to be parsed not found.';
                throw new \yii\base\ErrorException(Yii::t('app', $errorMessage));
            }

            // Request token
            $authManagerModel = new APIAuthManager();
            $generatedToken = $authManagerModel->requestToken($code);
            
            if ($generatedToken != null) {
                $tokenParts = explode('.', $generatedToken);
                $payload = json_decode(base64_decode($tokenParts[1]),true);
        
                $email = $payload['http://wso2.org/claims/emailaddress'] ?? $payload['email'] ?? '';

                if (empty($email)) {
                    $errorMessage = 'Email address not found.';
                    throw new \yii\base\ErrorException(Yii::t('app', $errorMessage));
                }

                $userProfile = $payload['http://wso2.org/claims/photourl'] ?? $payload['picture'] ?? null;

                $session = Yii::$app->session;
                
                // Set session variable for access token
                $session->set('user.b4r_api_v3_token', $generatedToken);

                $params['email'] = $email;
                $personModel = \Yii::$container->get('app\models\Person');
                $persons = $personModel->searchAll($params, 'limit=1', false);

                // Check if user has access
                if (isset($persons['status']) && $persons['status'] == 200 && !empty($persons['data'])) {
                    
                   

                    // Provide success message to the user
                    Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully logged in!'));
                    
                    // Set profile picture of the user
                    $session->set('user.picture', $userProfile);
                    
                    // Set session variable for login status
                    $session->set('user.is_logged_in', true);

                    // Set user session first name
                    $firstName = isset($persons['data'][0]['firstName']) ? $persons['data'][0]['firstName'] : 'User';
                    $session->set('user.first_name', $firstName);

                    // Set user session first name
                    $personId = isset($persons['data'][0]['personDbId']) ? $persons['data'][0]['personDbId'] : 0;
                    $session->set('user.id', $personId);
                    
                    // Set user session user type
                    $session->set('user.type', $persons['data'][0]['personType'] ?? 'user');

                    // Set user session if admin or not
                    $session->set('isAdmin', ($persons['data'][0]['personType'] == 'admin') ? true : false);

                    // verify if user belongs to a program
                    $program = Yii::$app->userprogram->verify();

                    // if no program, redirect to home page
                    if(empty($program)){
                        
                        Yii::$app->user->logout();
        
                        $session = Yii::$app->session;
                        
                        $session->destroy(); // Destroy all application session data
                        Yii::$app->session->setFlash('error', Yii::t('app', "You do not belong to any program yet. Kindly contact your data administrator."));
                        return $this->goHome();
                    }


                    // store processed user profile permissions to session
                    $this->saveUserProfilePermissionsToSession($personId);

                    // Load data browser settings in session
                    (new DataBrowserConfiguration())->loadDataBrowserSettings();

                    // Remove old session variables
                    \Yii::$app->cbSession->flushSession();

                    // Redirect to the dashboard
                    $this->redirect(Url::toRoute('/dashboard?program=' . $program));
                } else {
                    $errorMessage = 'User record not found.';
                    throw new \yii\base\ErrorException(Yii::t('app', $errorMessage));
                }
               
            } else {
                $errorMessage = 'Token not found.';
                throw new \yii\base\ErrorException(Yii::t('app', $errorMessage));
            }
        } catch(\yii\base\ErrorException $error) {
            Yii::error($error);
            Yii::$app->session->setFlash('error', Yii::t('app', "We were not able to verify your account. $errorMessage Kindly contact your system administrator."));
            
            // Go back to the homepage
            return $this->goHome();
        } catch (\GuzzleHttp\Exception\ConnectException $e){
            Yii::error($e);
            Yii::$app->session->setFlash('error', Yii::t('app', "Unable to connect to the server. Please check your internet or contact your system administrator if the issue continues."));

            // Go back to the homepage
            return $this->goHome();
        }
    }

    /**
     *  Save user profile permissions to session
     *
     * @param integer $personId Identifier of the currently logged in user
     */
    public function saveUserProfilePermissionsToSession($personId)
    {

        $productPermissions = null; // user permissions

        //check if RBAC is enabled in platform.config
        $rbacFlag = (new Config())->getConfigByAbbrev('ENABLE_RBAC_FLAG');
        $enableRbac = isset($rbacFlag) ? $rbacFlag : false;

        $userValidPrograms = Yii::$app->session->get('user_valid_programs') ?? [];
        $data = Yii::$app->api->getParsedResponse('GET','brapi/user/external/'.$personId.'?domainPrefix=cb', apiResource: 'cs');

        // check if user profile is not successfully retrieved, disable RBAC
        if(!$data['success']){
            $enableRbac = false;

            // store null product permissions to session
            Yii::$app->session->set('userPermissions', $productPermissions);
            // store enable RBAC flag to session
            Yii::$app->session->set('enableRbac', $enableRbac);
            Yii::error('Error in retrieving user profile',__METHOD__);
            return;
        }

        // get global role in user profile
        if(isset($data['data'][0]['permissions']['memberOf']['roles'])){
            $roles = $data['data'][0]['permissions']['memberOf']['roles'];


            // store CS ID
            $csUserId = $data['data'][0]['dbId'];
            Yii::$app->session->set('userCsId', $csUserId);
            
            // check if admin user, disable RBAC
            if(in_array(Constants::CB_ADMIN_ROLE, $roles) || in_array(Constants::EBS_ADMIN_ROLE, $roles) || Yii::$app->session->get('isAdmin')){

                $enableRbac = false;
            }
        }

        // store enable RBAC flag to session
        Yii::$app->session->set('enableRbac', $enableRbac);

        if($enableRbac){

            foreach ($userValidPrograms as $key => $value) {

                $data = Yii::$app->api->getParsedResponse('GET','brapi/user/external/'.$personId.'?domainPrefix=cb&programDbId='.$key, apiResource: 'cs');
                $programName = $data['data'][0]['permissions']['memberOf']['programs'][0]['name'] ?? '';
                $permissions = $enableRbac && isset($data['data'][0]['permissions']['applications'][0]) ?
                    $data['data'][0]['permissions']['applications'][0] : null;
                $productPermissions = []; // user permissions

                if(!empty($permissions['products'])){
                    $products = isset($permissions['products']) ? $permissions['products'] : null;

                    // process products and actions
                    if(!empty($products)){
                        foreach($products as $product){

                            $actions = array_merge($product['actions'][0][$programName] ?? [], $product['dataActions'][0][$programName] ?? []);

                            $productPermissions[] = [
                                'label' => isset($product['name']) ? $product['name'] : '',
                                'appAbbrev' => isset($product['abbrev']) ? $product['abbrev'] : '',
                                'name' => isset($product['name']) ? preg_replace('/\s+/', '-', strtolower($product['name'])) : '',
                                'permissions' => $actions,
                            ];
                        }
                    }
                }
                $userPermissions[$value] = $productPermissions;
            }
            
            // store product permissions to session
            Yii::$app->session->set('userPermissions', $userPermissions);
        }
    }
}
