<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\components\FavoritesWidget;
use \app\controllers\Constants;
use kartik\select2\Select2;
use app\widgets\PersonInfo;
use app\widgets\PackageInfo;
use app\widgets\GermplasmInfo;
use app\widgets\ExperimentInfo;
use app\widgets\VariableInfo;
use app\widgets\ApplicationInfo;

$program = Yii::$app->userprogram->get('abbrev');

/**
 * Renders search form and search results
 */
$enableNav = (isset($_GET['enableNav']) && !empty($_GET['enableNav'])) ? '1' : '';
?>
<h3>
<?=
    Yii::t('app', 'Search').
    FavoritesWidget::widget([
    'module' => 'search'
    ]);
?>
</h3>
<div class="search-container">
<?php 
$style='style="margin: 0px;width:100%"';
if(empty($dataProvider)){
    $style='';
?>
<h4 class="text-center header-search"><img src="<?= Yii::getAlias('@web/images/b4r-logo-circle.png')?>" style="width:60px;margin-bottom:10px;"><br/> <?= \Yii::t('app', 'Search ').Constants::APP_NAME ?> </h4>
<?php } 
if(!empty($dataProvider)){ ?>
<table class="search-table">
    <td class="search-td-logo">
        <a href="<?= Url::to(['/search','program'=>$program])?>"><img src="<?= Yii::getAlias('@web/images/b4r-logo-circle.png')?>" title="<?= \Yii::t('app', 'Search') ?>" style="width:60px;"></a>
    </td>
    <td>
    <?php } ?>
    <nav id="search-bar-nav" class="lighten-1 search-bar" <?= $style ?> >
        <div class="nav-wrapper row">
            <div class="col s6 m9" style="padding-right: 0 !important;">
                <input id="dashboard-search-bar-input" type="text" placeholder="Search here..." value= "<?= htmlentities($value) ?>" >
            </div>
            <div class="col s4 m2" style="padding: 0 !important;">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="input-field col s12" style="padding: 0 !important;">
                        <?php
                            echo Select2::widget([
                                'name' => 'dashboard-search-bar-entity-select',
                                'id' => 'dashboard-search-bar-entity-select',
                                'data' => ['germplasm','experiment','package','person','trait','tool'],
                                'theme' => Select2::THEME_MATERIAL,
                                'options' => [
                                ],
                                'pluginOptions' => [
                                    'minimumResultsForSearch' => -1,
                                ]
                            ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col s2 m1" style="padding: 0 !important;">
                <div id="dashboard-search-bar-btn-row" class="row" style="margin-bottom: 0px;">
                    <div class="col s6" style="padding: 0 !important;">
                        <i class="material-icons" id="submit-search-btn" title="<?= \Yii::t('app', 'Search') ?>">search</i>
                    </div>
                    <div class="col s6" style="padding: 0 !important;">
                        <i class="material-icons" id="help-search-btn" title="<?= \Yii::t('app', 'Help') ?>" data-target="#help-modal" data-toggle="modal">help</i>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <?php
        if(!empty($dataProvider)){ //display results
            $formatter = \Yii::$app->formatter;
            $dynagrid = DynaGrid::begin([
                'columns' => [
                    [
                        'attribute'=>'entityId',
                        'value'=>function($model){
                            if($model['entity'] == 'experiment'){
                                $icon = '<h6 class="header"><i class="fa fa-cogs" title="'. \Yii::t('app', 'Experiment').'""></i></h6>';
                            }else if($model['entity'] == 'germplasm'){
                                $icon = '<h6 class="header"><i class="fa fa-pagelines" title="'. \Yii::t('app', 'Germplasm').'""></i></h6>';
                            }else if($model['entity'] == 'package'){
                                $icon = '<h6 class="header"><i class="fa fa-leaf" title="'. \Yii::t('app', 'Package').'""></i></h6>';
                            }else if($model['entity'] == 'person'){
                                $icon = '<h6 class="header"><i class="fa fa-user-circle" title="'. \Yii::t('app', 'Person').'""></i></h6>';
                            }else if($model['entity'] == 'trait'){
                                $icon = '<h6 class="header"><i class="fa fa-pencil" title="'. \Yii::t('app', 'Trait').'""></i></h6>';
                            }else if($model['entity'] == 'tool'){
                                $icon = '<h6 class="header"><i class="fa fa-cogs" title="'. \Yii::t('app', 'Tool').'""></i></h6>';
                            }else{
                                $icon = '<h6 class="header"><i class="fa fa-cogs" title="'. \Yii::t('app', 'Tool').'""></i></h6>';
                            }
                            return $icon;
                        },
                        'format' => 'raw',
                        'options'=>['style'=>'width:10px;']
                    ],
                    [
                        'attribute'=>'entityDescription',
                        'value'=>function($model) use ($formatter){
                            $entity = $model['entity'] == 'trait' ? 'variable' : $model['entity'];
                            $entityName = (isset($model['entityName']) && !is_null($model['entityName']) && $model['entityName'] !== '') ? $model['entityName'] : '<span class="not-set">'. \Yii::t('app', 'Not set').'</span>';
                            $creationTimestamp = (isset($model['creationTimestamp'])) ? ' · <span title="Date when the record was created">'.$formatter->asDate($model['creationTimestamp'],'long') . '</span>' : '';
                            $creator = (isset($model['creator']) && !empty($model['creator'])) ? '  · <span class="meta-highlight" title="Creator of the record">'. $model['creator'] .'</span>' : '';

                            $entityClass = ['class' => 'blue-text text-darken-4 header-highlight'];
                            $viewMoreInfoClass = ['class' => 'fixed-color'];
                            $dataToggle = $model['entity'] == 'tool' ? '' : 'modal';
                            $title = $model['entity'] == 'tool' ? 'Go to the application' : 'Click to view more information';

                            $linkAttrsTemp = [
                                'title' => \Yii::t('app', $title),
                                'data-label' => $entityName,
                                'data-id' => $model['entityId'],
                                'data-target' => '#view-'.$entity.'-widget-modal',
                                'data-toggle' => $dataToggle
                            ];

                            $linkNameId = ['id' => 'view-'.$entity.'-widget-'.$model['entityId']];
                            $linkNameStyle = ['style' => ['font-size' => '1rem']];
                            $linkAttrsName = array_merge($linkNameId, $linkAttrsTemp, $linkNameStyle);
                            $linkAttrsMoreInfo = array_merge($linkNameId, $linkAttrsTemp);

                            if($model['entity'] == 'person') {
                                $widgetName = PersonInfo::widget([
                                    'id' => $model['entityId'],
                                    'label' => $entityName,
                                    'linkAttrs' => array_merge($entityClass,$linkAttrsName)
                                ]);

                                $widgetMoreInfo = PersonInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'label' => "View more information",
                                    'linkAttrs' => array_merge($viewMoreInfoClass,$linkAttrsMoreInfo)
                                ]);
                            } else if($model['entity'] == 'package') {
                                $widgetName = PackageInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'linkAttrs' => array_merge($entityClass,$linkAttrsName)
                                ]);

                                $widgetMoreInfo = PackageInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'label' => "View more information",
                                    'linkAttrs' => array_merge($viewMoreInfoClass,$linkAttrsMoreInfo)
                                ]);
                            }else if($model['entity'] == 'germplasm') {
                                $widgetName = GermplasmInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'linkAttrs' => array_merge($entityClass,$linkAttrsName)
                                ]);

                                $widgetMoreInfo = GermplasmInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'label' => "View more information",
                                    'linkAttrs' => array_merge($viewMoreInfoClass,$linkAttrsMoreInfo)
                                ]);
                            } else if($model['entity'] == 'experiment') {
                                $widgetName = ExperimentInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'linkAttrs' => array_merge($entityClass,$linkAttrsName)
                                ]);

                                $widgetMoreInfo = ExperimentInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'label' => "View more information",
                                    'linkAttrs' => array_merge($viewMoreInfoClass,$linkAttrsMoreInfo)
                                ]);
                            } else if($model['entity'] == 'trait') {
                                $widgetName = VariableInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'linkAttrs' => array_merge($entityClass,$linkAttrsName)
                                ]);

                                $widgetMoreInfo = VariableInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'label' => "View more information",
                                    'linkAttrs' => array_merge($viewMoreInfoClass,$linkAttrsMoreInfo)
                                ]);
                            } else if($model['entity'] == 'tool') {
                                // Get the URL of the tool
                                $id = $linkAttrsTemp['data-id'];

                                $dashboardConfigModel = \Yii::$container->get('app\models\UserDashboardConfig');
                                $appModel = \Yii::$container->get('app\models\Application');

                                $program = $dashboardConfigModel->getUserProgram();
                                $url = $appModel->geturlbyappid($id,$program);

                                $widgetName = ApplicationInfo::widget([
                                    'id' => $model['entityId'],
                                    'entityLabel' => $entityName,
                                    'url' => Url::to([$url]),
                                    'linkAttrs' => array_merge($entityClass,$linkAttrsName)
                                ]);

                                $widgetMoreInfo = ApplicationInfo::widget([
                                    'id' => $model['entityId'],
                                    'label' => "Go to the application",
                                    'url' => Url::to([$url]),
                                    'linkAttrs' => array_merge($viewMoreInfoClass,$linkAttrsMoreInfo)
                                ]);
                            }

                            return '<h6 class="header">'.
                            $widgetName.
                            '</h6>'.
                            '<span class="description description-highlight">'.$model['entityDescription'].'</span><br/>'.
                            '<span class="search-meta">'.
                                $widgetMoreInfo.
                                $creationTimestamp.
                                $creator.
                            '</span>';
                        },
                        'format' => 'raw',
                    ]
                ],
                'theme'=>'simple-default',
                'showPersonalize'=>false,
                'showFilter' => false,
                'showSort' => false,
                'allowFilterSetting' => false,
                'allowSortSetting' => false,
                'gridOptions'=>[
                    'id' => 'grid-search',
                    'dataProvider'=>$dataProvider,
                    'pjax'=>true,
                    'responsive'=>false,
                    'showHeader'=>false
                ],
                'options'=>['id'=>'dynagrid-search']
            ]);
            DynaGrid::end();
        }
    ?>
    </td>
</table>
</div>
<br/>

<?php
//help modal
Modal::begin([
    'id' => 'help-modal',
    'header' => '<h4 id="entity-label"><i class="fa fa-question-circle"></i> Search tool help</h4>',
    'footer' =>
        Html::a(\Yii::t('app','Close'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
]);
?>
<?= Yii::$app->controller->renderPartial('@app/modules/search/views/default/search_help.php'); ?>
<?php Modal::end();

//css
$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

//define app name
$appName = Constants::APP_NAME;

//javascript
$viewEntityUrl = Url::to(['/search/default/viewentity']);
$this->registerJs(<<<JS

    //display clear all icon on hover in input field
    $("#search-form").mouseover(function(){
        if($('#search').val() != ''){
            $('#clear-all-search-btn').css("opacity", "0.5");
        }
    });
    $("#search-form").mouseout(function(){
        $('#clear-all-search-btn').css("opacity", "0");
    });

    //update display of clear all upon changing value in search field
    $('#search').on('change keyup paste click', function () {
        if($('#search').val() != ''){
            $('#clear-all-search-btn').css("opacity", "0.5");
        }else{
            $('#clear-all-search-btn').css("opacity", "0");
        }
    });

    // fill select2
    var entity = '$firstEntity';
    var entityDropdown = 0;
    if(entity == 'germplasm') entityDropdown = 0;
    else if(entity == 'experiment') entityDropdown = 1;
    else if(entity == 'package') entityDropdown = 2;
    else if(entity == 'person') entityDropdown = 3;
    else if(entity == 'trait') entityDropdown = 4;
    else if(entity == 'tool') entityDropdown = 5;
    $('#dashboard-search-bar-entity-select').val(entityDropdown).trigger('change');

    // trigger search input event
    $('#dashboard-search-bar-input').on('input', function(e) {
        if(!$('#dashboard-search-bar-input').val().trim()) {
            $('#submit-search-btn').addClass('disabled');
        } else {
            $('#submit-search-btn').removeClass('disabled');
        }
    });

    $('#dashboard-search-bar-input').on('keypress', function(e) {
        var keyCode = (e.keyCode? e.keyCode : e.which);
        if(keyCode == 13 && $('#dashboard-search-bar-input').val().trim()) { // Enter key is pressed and input is not empty
            e.preventDefault();
            $('#submit-search-btn').trigger('click');
        }
    });

    $('#submit-search-btn').on('click', function(e) {
        if($('#dashboard-search-bar-input').val().trim()) {
            var searchValue = $('#dashboard-search-bar-input').val();
            var searchEntity = $('#dashboard-search-bar-entity-select').select2('data')[0].text;

            window.location = '/index.php/search/default/index?search=' + encodeURIComponent(searchValue) + '&entity=' + searchEntity;
        }
    });

    //export data
    $(document).on('click', '.export-data', function(e) {
        var id = this.id;
        var obj = $(this);
        var attr = obj.data('attr');
        var label = obj.data('label');
        window.location = 'export?entityId='+id+'&attr='+attr+'&label='+label;
        $(window).on('beforeunload', function(){
            $('#loading').html('');
            $('#system-loading-indicator').html('');
        });
    });
    
    //show loading indicator
    $(window).on('beforeunload', function(){
        //$('#loading').html('<br/><span class="teal-text text-darken-2"><i id="loading-text">Searching...</i></span><div class="progress"><div class="indeterminate"></div></div>');
    });

    //loading messages
    var text = [
        "Searching...",
        "We're testing your patience...",    
        "Waiting for approval from Bill Gates...",
        "Hang on a sec, we know your data is here somewhere...",
        "Searching for answer to life, the universe, and everything...",
        "Warming up the processors...",
        "Please don't move...",
        "Don't panic...",
        "Your time is important to us...",
        "Go get a cup of coffee or something...",
        "Drink and be merry for tomorrow this site may load...",
        "Buy more RAM...",
        "Waiting for magic to happen...",        
        "Don't press back, please...",
        "Deciphering your intentions...",
        "Looking for exact change...",
        "Calculating the odds..."
    ];
    var counter = 0;
    setInterval(change, 2000);

    function change() {
        $('#loading-text').html(text[counter]);
        counter++;
        if (counter >= text.length) {
            counter = 0;
        }
    }
    //end loading indicator

    $("document").ready(function(){
        highlight();

        //highlight search texts
        function highlight(){
            var terms = "$searchStr";
            var termsArr = terms.split(' & ');

            termsArr.forEach(function(term) {
                if(term != '-' && term != '/' && term != '.' && term != 'B'){
                    $(".header-highlight").each(function() {
                        try {
                            src_str = $( this ).html();
                            term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
                            var pattern = new RegExp("("+term+")", "gi");
                            src_str = src_str.replace(pattern, "<xx>$1</xx>");
                            src_str = src_str.replace(/(<xx>[^<>]*)((<[^>]+>)+)([^<>]*<\/xx>)/,"$1</xx>$2<xx>$4");
                            $(this).html(src_str);
                        } catch(err) {}
                    });

                    if(term != 'b'){
                        $(".description-highlight").each(function() {
                            try {
                                src_str = $( this ).html();
                                term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
                                var pattern = new RegExp("("+term+")", "gi");
                                src_str = src_str.replace(pattern, "<xx>$1</xx>");
                                src_str = src_str.replace(/(<xx>[^<>]*)((<[^>]+>)+)([^<>]*<\/xx>)/,"$1</xx>$2<xx>$4</xx>");
                                $(this).html(src_str);
                            } catch(err) {}
                        });
                    }
                    $(".meta-highlight").each(function() {
                        try {
                            src_str = $( this ).html();
                            term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
                            var pattern = new RegExp("("+term+")", "gi");
                            src_str = src_str.replace(pattern, "<xx>$1</xx>");
                            src_str = src_str.replace(/(<xx>[^<>]*)((<[^>]+>)+)([^<>]*<\/xx>)/,"$1</xx>$2<xx>$4");
                            $(this).html(src_str);
                        } catch(err) {}
                    });
                }
            });
        }

        //switching pages
        $("#dynagrid-search").on("pjax:end", function() {  
            var t = Math.random() / 100;
            var timeDiff = parseFloat($elapsedTime) - t;
            var time = Math.abs(timeDiff);
            $('.summary').append(' ('+time.toFixed(4)+' seconds)');
            highlight();
            $('html, body').animate({ scrollTop: 0 }, 'fast'); //got to top
        });

        $('.summary').append(' ('+$elapsedTime + ' seconds)');

        $(document).on('click', '.search-tabs', function(e) {
            window.location = $(this).attr('href');
        });

        //print pedigree tree
        $(document).on('click', '#print-tree', function(e) {
            var popUpAndPrint = function()
            {
                var pedigreeTree = $('#graph');
                var height = $(window).height() - 100;
                var width = $(window).width() - 100;
                var app_name = '$appName';
                
                var printWindow = window.open('', 'PrintMap',
                'width=' + width + ',height=' + height);
                printWindow.document.writeln('<style type="text/css" media="print"> @media print{ @page {size: portrait;} .card-panel.pedigree-tree{font-family: monospace;} .pull-right{float:right;}}</style>');
                printWindow.document.writeln('<p style="font-size:20px;">Pedigree tree<br/><span style="font-size:12px;font-weight:300">Generated from ' + app_name + '</span></p>');
                printWindow.document.writeln($(pedigreeTree).html());

                printWindow.document.close();
                printWindow.print();
                printWindow.close();
            };
            setTimeout(popUpAndPrint, 100);
        });

        //print passport data
        $(document).on('click', '#print-passport', function(e) {
            var popUpAndPrint = function()
            {
                var passport = $('#passport');
                var height = $(window).height() - 100;
                var width = $(window).width() - 100;

                var printWindow = window.open('', 'PrintMap',
                'width=' + width + ',height=' + height);
                printWindow.document.writeln('<style type="text/css" media="print"> @media print{ @page {size: portrait;} #passport-header{display:block;font-size:23px;text-align:center;} .table{width: 100%;} th{text-align:left;}}</style>');
                printWindow.document.writeln('');
                printWindow.document.writeln($(passport).html());

                printWindow.document.close();
                printWindow.print();
                printWindow.close();
            };
            setTimeout(popUpAndPrint, 100);
        });
    });

JS
);