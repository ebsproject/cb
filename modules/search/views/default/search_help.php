<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
* Renders search help content
*/

echo \Yii::t('app','Sometimes, you want to search by just specifying a word or phrase and then get list of search results just like Google.') . ' '.
\Yii::t('app','EBS search tool allows you to search specific EBS entities by typing what you are looking for in the search field and then pressing the enter key or clicking the search icon. Below are the supported search syntax.');
?>
<h5><?= \Yii::t('app','Search syntax') ?></h5>
<table class="table table-bordered white z-depth-3">
    <thead>
    <tr>
        <td> </td>
        <th title="Syntax"><?= \Yii::t('app','Syntax') ?></td>
        <th title="Description"><?= \Yii::t('app','Description') ?></th></tr>
    </thead>
    <tbody>
    <tr>
        <td style="text-align:center;width:5px;">1</td>
        <td>foo bar</td>
        <td><?= \Yii::t('app',"with words 'foo' and 'bar'") ?></td>
    </tr>
    <tr>
        <td style="text-align:center">2</td>
        <td>"foo bar"</td>
        <td><?= \Yii::t('app',"exactly 'foo bar' word") ?></td>
    </tr>
    <tr>
        <td style="text-align:center">3</td>
        <td>foo bar%</td>
        <td><?= \Yii::t('app',"with words that start with 'foo bar'") ?></td>
    </tr>
    <tr>
        <td style="text-align:center">4</td>
        <td>%foo bar%</td>
        <td><?= \Yii::t('app',"with word 'foo bar'") ?></td>
    </tr>
    <tr>
        <td style="text-align:center">5</td>
        <td>foo bar_</td>
        <td><?= \Yii::t('app',"with words that start with 'foo bar' and has one additional character") ?></td>
    </tr>
    <tr>
        <td style="text-align:center">6</td>
        <td>%foo bar</td>
        <td><?= \Yii::t('app',"with words that end with 'foo bar'") ?></td>
    </tr>
    </tbody>
</table>

<h5><?= \Yii::t('app','Search entities') ?></h5>
<p>Currently, supported entities are <b>Germplasm</b>, <b>Experiments</b>, <b>Packages</b>, <b>Persons</b>,  <b>Traits</b>, and <b>Tools</b>.</p>
<table class="table table-bordered white z-depth-3">
    <thead>
    <tr>
        <td> </td>
        <th title="Entity"><?= \Yii::t('app','Entity') ?></td>
        <th title="Description"><?= \Yii::t('app','Description') ?></th></tr>
    </thead>
    <tbody>    
    <tr>
        <td style="text-align:center">1</td>
        <td><?= \Yii::t('app','Germplasm') ?></td>
        <td>Using search <u>syntax #1</u>, <?= \Yii::t('app','user can search for a germplasm against the following attributes:') ?>
            <ul class="browser-default">
                <li><?= \Yii::t('app','designation') ?></li>
                <li><?= \Yii::t('app','parentage') ?></li>
                <li><?= \Yii::t('app','generation') ?></li>
                <li><?= \Yii::t('app','germplasm state') ?></li>
                <li><?= \Yii::t('app','germplasm name type') ?></li>
                <li><?= \Yii::t('app','germplasm normalized name') ?></li>
                <li><?= \Yii::t('app','other name') ?></li>
            </ul>
            <?= \Yii::t('app','Example')?>:
            <div class="card-panel grey lighten-4" style="padding:10px">
                    <b>f1 fixed line cultivar name</b> - germplasm with Generation F1 or State fixed line or Name Type cultivar name
            </div>
            Using search <u>syntax #2 to #6</u>, <?= \Yii::t('app','user can search a designation.') ?>
        </td>
    </tr>
    <tr>
        <td style="text-align:center;width:5px;">2</td>
        <td><?= \Yii::t('app','Experiments') ?></td>
        <td>Using search <u>syntax #1</u>, <?= \Yii::t('app','user can search for an experiment against the following attributes:') ?>
            <ul class="browser-default">
                <li><?= \Yii::t('app','experiment year') ?></li>
                <li><?= \Yii::t('app','planting season') ?></li>
                <li><?= \Yii::t('app','experiment code') ?></li>
                <li><?= \Yii::t('app','experiment name') ?></li>
                <li><?= \Yii::t('app','experiment type') ?></li>
                <li><?= \Yii::t('app','experiment sub type') ?></li>
                <li><?= \Yii::t('app','experiment sub sub type') ?></li>
                <li><?= \Yii::t('app','experiment design type') ?></li>
                <li><?= \Yii::t('app','experiment status') ?></li>
                <li><?= \Yii::t('app','season') ?></li>
                <li><?= \Yii::t('app','stage') ?></li>
                <li><?= \Yii::t('app','project') ?></li>
                <li><?= \Yii::t('app','steward') ?></li>
                <li><?= \Yii::t('app','creator') ?></li>
            </ul>
            <?= \Yii::t('app','Example') ?>:
            <div class="card-panel grey lighten-4" style="padding:10px">
                <b>nursery completed dry</b> - experiment with Type nursery or Status completed or Season dry
            </div> - 
            Using search <u>syntax #2 to #6</u>, <?= \Yii::t('app','user can search an experiment name.') ?>
        </td>
    </tr>
    <tr>
        <td style="text-align:center">3</td>
        <td><?= \Yii::t('app','Packages') ?></td>
        <td>Using search <u>syntax #1</u>, <?= \Yii::t('app','user can search for a package against the following attributes:') ?>
            <ul class="browser-default">
                <li><?= \Yii::t('app','package code') ?></li>
                <li><?= \Yii::t('app','package label') ?></li>
                <li><?= \Yii::t('app','package quantity') ?></li>
                <li><?= \Yii::t('app','package unit') ?></li>
                <li><?= \Yii::t('app','package status') ?></li>
                <li><?= \Yii::t('app','seed code') ?></li>
                <li><?= \Yii::t('app','seed name') ?></li>
                <li><?= \Yii::t('app','harvest date') ?></li>
                <li><?= \Yii::t('app','harvest method') ?></li>
            </ul>
            <?= \Yii::t('app','Example') ?>:
            <div class="card-panel grey lighten-4" style="padding:10px">
                <b>A12DSC-94</b> - package with label A12DSC-94
            </div>
            Using search <u>syntax #2 to #6</u>, <?= \Yii::t('app','user can search a label.') ?>
        </td>
    </tr>
    <tr>
        <td style="text-align:center">4</td>
        <td><?= \Yii::t('app','Persons') ?></td>
        <td>Using search <u>syntax #1</u>, <?= \Yii::t('app','user can search for a person against the following attributes:') ?>
        <ul class="browser-default">
            <li><?= \Yii::t('app','username (user)') ?></li>
            <li><?= \Yii::t('app','email') ?></li>
            <li><?= \Yii::t('app','first name') ?></li>
            <li><?= \Yii::t('app','last name') ?></li>
            <li><?= \Yii::t('app','person name') ?></li>
            <li><?= \Yii::t('app','person type') ?></li>
            <li><?= \Yii::t('app','person status') ?></li>
        </ul>
        <?= \Yii::t('app','Example') ?>:
        <div class="card-panel grey lighten-4" style="padding:10px">
            <b>Admin</b> - persons that are Admin
        </div>
        Using search <u>syntax #2 to #6</u>, <?= \Yii::t('app','user can search a person name and an email.') ?>
        </td>
    </tr>
    <tr>
        <td style="text-align:center">5</td>
        <td><?= \Yii::t('app','Traits') ?></td>
        <td>Using search <u>syntax #1</u>, <?= \Yii::t('app','user can search for a trait against the following attributes:') ?>
        <ul class="browser-default">
            <li><?= \Yii::t('app','abbrev') ?></li>
            <li><?= \Yii::t('app','label') ?></li>
            <li><?= \Yii::t('app','name') ?></li>
            <li><?= \Yii::t('app','property') ?></li>
            <li><?= \Yii::t('app','scale') ?></li>
            <li><?= \Yii::t('app','data type') ?></li>
            <li><?= \Yii::t('app','type') ?></li>
            <li><?= \Yii::t('app','data level') ?></li>
            <li><?= \Yii::t('app','status') ?></li>
        </ul>
        <?= \Yii::t('app','Example') ?>:
        <div class="card-panel grey lighten-4" style="padding:10px">
            <b>experiment, study</b> - trait with an Experiment/Study Data Level
        </div>
        Using search <u>syntax #2 to #6</u>, <?= \Yii::t('app','user can search a label.') ?>
        </td>
    </tr>
    <tr>
        <td style="text-align:center">6</td>
        <td><?= \Yii::t('app','Tools') ?></td>
        <td>Using search <u>syntax #1</u>, <?= \Yii::t('app','user can search for a tool against the following attributes:') ?>
        <ul class="browser-default">
            <li><?= \Yii::t('app','abbrev') ?></li>
            <li><?= \Yii::t('app','label') ?></li>
            <li><?= \Yii::t('app','action label') ?></li>
            <li><?= \Yii::t('app','description') ?></li>
        </ul>
        <?= \Yii::t('app','Example') ?>:
        <div class="card-panel grey lighten-4" style="padding:10px">
            <b>Find seeds</b> - tool for Find seeds
        </div>
        Using search <u>syntax #2 to #6</u>, <?= \Yii::t('app','user can search a label.') ?>
        </td>
    </tr>
    </tbody>
</table>