<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
namespace app\modules\search\controllers;

use app\components\B4RController;
use app\models\Search;
use app\modules\search\models\SearchEntity;
use app\modules\search\models\SearchGermplasm;
use app\modules\search\models\SearchProduct;
use app\modules\search\models\SearchStudy;
use Yii;
/**
 * Default controller for search tool
 */
class DefaultController extends B4RController
{
    public function __construct ($id, $module,
        public Search $searchModels,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the search form and search results
     * @return mixed
     */
    public function actionIndex()
    {
        $search = '';
        $elapsedTime = '';
        $dataProvider = [];
        $firstEntity = '';
        $entity = '';
        $searchStr = '';
        if(isset($_GET['search']) && isset($_GET['entity'])){
            $startTime = microtime(true);
            $search = $_GET['search'];
            $entity = $_GET['entity'];
            $data = $this->searchModels->getDataprovider($search, $entity);
            $dataProvider = $data['dataProvider'];
            $firstEntity = $data['firstEntity'];
            $searchStr = $data['searchStr'];
            $endTime = microtime(true);
            $elapsedTime = $endTime - $startTime;
            $elapsedTime = substr($elapsedTime, 0, 6);
        }

        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'firstEntity' => $firstEntity,
            'value' => $search,
            'elapsedTime' => $elapsedTime,
            'searchStr' => $searchStr,
            'entity' => $entity
        ]);
    }

    /**
     * Renders view more information of an entity
     */
    public function actionViewentity(){
        $entityId = isset($_POST['entity_id']) ? $_POST['entity_id'] : null;
        $entityType = isset($_POST['entity_type']) ? $_POST['entity_type'] : null;
        $entityLabel = isset($_POST['entity_label']) ? $_POST['entity_label'] : null;
        $program = isset($_POST['program']) ? $_POST['program'] : Yii::$app->userprogram->get('abbrev'); //current program

        if (empty($program)) {
            $defaultFilters = Dashboard::getFilters();
            $dashProgram = $defaultFilters->program_id;
        }

        $data = SearchEntity::getEntityData($entityId,$entityType);
        return Yii::$app->controller->renderAjax('@app/modules/search/views/'.$entityType.'/index.php',[
            'data' => $data,
            'entityLabel' => $entityLabel,
            'entityId' => $entityId,
            'program' => $program
        ]);
    }

    /**
     * Export entities for a Germplasm
     * @param $entityId integer Germplasm identifier
     * @param $label text Germplasm label
     * @param $attr text Attribute to be exported
     */
    public function actionExport($entityId,$attr,$label){
        $attr = !empty($attr) ? $attr : 'seeds';

        $entity = '';
        $header = "Header \r\n";
        
        if($attr == 'seeds')
        {
            $orderCond = Yii::$app->session->get('seedsOrderCond');
            $entity = 'Seeds';

            $fileData = SearchGermplasm::getGermplasmSeeds($entityId);
            $dataProvider = $fileData->allModels;
        }

        $label = preg_replace('/[^A-Za-z0-9\-()]/', '_', $label);        

        $file = $label .'-'.$entity. '.csv';
        $fileRoot = Yii::getAlias("@webroot") . '/files' . '/' . trim($file);
        $fp = fopen($fileRoot, 'w');
        $headers = [];

        foreach ($dataProvider as $data) {
            if (isset($data['seedDbId'])) {
                unset($data['seedDbId']);
            }
            if (empty($headers)) {
                $headers = array_keys($data);
                fputcsv($fp, $headers, ',');
            }
            fputcsv($fp, $data, ',');
        } 

        fclose($fp);

        if (file_exists($fileRoot)) {  // trigger successful download
          Yii::$app->response->sendFile($fileRoot);
          return Yii::$app->end();
        } 
        else {    // file is not created / does not exist
          Yii::$app->user->setFlash('error', '<i class="fa-fw fa fa-times"></i>There seems to be a problem with the file you were downloading.');
        }
    }
}
