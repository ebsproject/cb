<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for search entities
*/

namespace app\models;
namespace app\modules\search\models;

use Yii;
use app\models\Config;
use yii\data\SqlDataProvider;

class SearchEntity extends \yii\db\ActiveRecord
{

	/**
	 * Returns more information of entity
	 *
	 * @param $entityId integer entity identifier
	 * @param $entity text entity being viewed whether study, product or designation
	 *
	 * @return $data array list of entity information
	 */
	public static function getEntityData($entityId,$entity){
		$data = [];
		if ($entity == 'germplasm'){
			$data['basic'] = SearchGermplasm::getGermplasmBasicInfo($entityId);
			$data['pedigrees'] = SearchGermplasm::getGermplasmPedigrees($entityId);
		}

		return $data;
	}
}