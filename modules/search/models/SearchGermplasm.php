<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\search\models;

use Yii;
use yii\data\ArrayDataProvider;
use yii\widgets\DetailView;
use app\models\EventLog;
use app\models\BaseModel;

class SearchGermplasm extends BaseModel
{

    public static function apiEndPoint() {
        return 'germplasm';
    }

    /**
     * Returns basic information of the germplasm record
     *
     * @param $germplasmId integer germplasm identifier
     * @return mixed germplasm basic info
     */
    public static function getGermplasmBasicInfo($germplasmId){
        $response = Yii::$app->api->getResponse('GET', 'germplasm/'.$germplasmId);
        $data = isset($response['body']['result']['data'][0])? $response['body']['result']['data'][0] : [];

        if(!empty($data)) {
            $basicInfo = [
                'Germplasm' => $data['designation'],
                'Germplasm Code' => $data['germplasmCode'],
                'Parentage' => $data['parentage'],
                'Generation' => $data['generation'],
                'Germplasm Name Type' => $data['germplasmNameType'],
                'Germplasm Type' => $data['germplasmType'],
                'Germplasm Normalized Name' => $data['germplasmNormalizedName'],
                'Germplasm State' => $data['germplasmState'],
                'Family Name' => $data['familyName'],
                'Family Code' => $data['familyCode'],
                'Family Member Number' => $data['familyMemberNumber'],
                'Taxonomy' => $data['taxonomyName']
            ];

            $auditInfo = [
                'Creator' => $data['creator'],
                'Creation Timestamp' => date('M d, Y', strtotime($data['creationTimestamp'])),
                'Modifier' => $data['modifier'],
                'Modification Timestamp' => is_null($data['modifier']) ? null : date('M d, Y', strtotime($data['modificationTimestamp'])),
            ];
        } else {
            $basicInfo = [];
            $auditInfo = [];
        }

        return [
            'basic' => $basicInfo,
            'audit' => $auditInfo,
            'familyDbId' => $data['familyDbId'] ?? 0
        ];
    }

    /**
     * Retrieves list of germplasm names
     *
     * @param $germplasmId integer germplasm identifier
     * @return mixed arrayDataProvider of germplasm names
     */
    public static function getGermplasmNames($germplasmId){
        $params = ["germplasmDbId" => "equals $germplasmId"];
        $response = Yii::$app->api->getResponse('POST', 'germplasm-names-search', json_encode($params));
        $data = $response['body']['result']['data'];

        return new ArrayDataProvider([
            'allModels' => $data
        ]);
    }

    /**
     * Retrieves list of the seeds for a given germplasm
     *
     * @param $germplasmId integer germplasm identifier
     * @return mixed arrayDataProvider of germplasm seeds
     */
    public static function getGermplasmSeeds($germplasmId){
        $response = Yii::$app->api->getResponse('GET', 'germplasm/'.$germplasmId.'/seeds');
        $data = $response['body']['result']['data'];

        return new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);
    }

    /**
     * Retrieves list of the pedigrees for a given germplasm
     *
     * @param $germplasmId integer germplasm identifier
     * @return mixed arrayDataProvider of germplasm pedigrees
     */
    public static function getGermplasmPedigrees($germplasmId){
        // Uncomment once viewing pedigree is implemented
        // $response = Yii::$app->api->getResponse('POST', 'germplasm/'.$germplasmId.'/pedigrees-search',json_encode(['depth'=>"6"]));
        // $data = $response['body']['result']['data'];
        $data = [];

        return new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);
    }

    /**
     * Retrieves list of the attributes for a given germplasm
     *
     * @param $germplasmId integer germplasm identifier
     * @return mixed arrayDataProvider of germplasm attributes
     */
    public static function getGermplasmAttributes($germplasmId){
        $response = Yii::$app->api->getResponse('GET', 'germplasm/'.$germplasmId.'/attributes');
        $data = $response['body']['result']['data'][0]['attributes'];

        return new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);
    }

    /**
     * Retrieves all members of the given family ID
     * 
     * @param Integer $familyDbId family identifier
     * @return mixed family dataProvider and family information
     */
    public static function getFamily($familyDbId) {
        $response = Yii::$app->api->getResponse('GET', 'families/'.$familyDbId.'/members');
        $familyInfo = $response['body']['result']['data'][0] ?? [];
        $data = $response['body']['result']['data'][0]['familyMembers'] ?? [];
        $memberCount = $response['body']['metadata']['pagination']['totalCount'] ?? 0;
        $familyInfo['memberCount'] = $memberCount;
        
        return [
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $data,
                'totalCount' => $memberCount,
                'pagination' => false
            ]),
            'familyInfo' => $familyInfo
        ];
    }

    /**
     * Retrieves list of germplasm names
     *
     * @param $germplasmId integer germplasm identifier
     * @return mixed arrayDataProvider of germplasm event logs
     */
    public static function getGermplasmEventLogs($germplasmId){
        $response = Yii::$app->api->getResponse('GET', 'germplasm/'.$germplasmId.'/event-logs');
        $result = $response['body']['result']['data'];
        $commonFieldList = array(
            'place_id' => array('Place', 'abbrev'),
            'phase_id' => array('Phase', 'abbrev'),
            'study_id' => array('Study', 'name'),
            'variable_id' => array('Variable', 'abbrev'),
            'property_id' => array('Property', 'abbrev'),
            'scale_id' => array('Scale', 'type'),
            'method_id' => array('Method', 'description'),
            'variable_set_id' => array('VariableSet', 'abbrev'),
            'product_id' => array('Product', 'designation')
        );
        $commonFieldListKeys = array_keys($commonFieldList);
        // format row_data and new_data with DetailView
        $model = new EventLog();
        foreach($result as $key => $val) {
            $rowData = $result[$key]['row_data'];
            $rowDataAttr = array();
            if (!empty($rowData)) {
                foreach($rowData as $rowDataKey => $rowDataVal) {
                    if($rowDataKey == 'event_log') {
                        continue;
                    }

                    if(in_array($rowDataKey, $commonFieldListKeys)) {
                        if(isset($commonFieldList[$rowDataKey])) {
                            $model = $commonFieldList[$rowDataKey][0];
                            $column = $commonFieldList[$rowDataKey][1];
                            $modelObject = EventLog::loadSearchModel($model);
                            $object = $modelObject::findOne($rowDataVal);

                            $rowDataKey = $commonFieldList[$rowDataKey][0];
                            if(isset($object->$column)) {
                                $rowDataVal = $object->$column;
                            } else {
                                $rowDataVal = null;
                            }
                        }
                    }

                    array_push($rowDataAttr, array(
                        'attribute'=>$rowDataKey,
                        'value'=>$rowDataVal
                    ));
                }
            }

            $result[$key]['row_data'] = DetailView::widget([
                'model' => $model,
                'attributes' => $rowDataAttr
            ]);

            $newData = $result[$key]['new_data'];
            $newDataAttr = array();
            if (!empty($newData)) {
                foreach($newData as $newDataKey => $newDataVal) {
                    if($newDataKey == 'event_log') {
                        continue;
                    }

                    if(in_array($newDataKey, $commonFieldListKeys)) {
                        if(isset($commonFieldList[$newDataKey])) {
                            $model = $commonFieldList[$newDataKey][0];
                            $column = $commonFieldList[$newDataKey][1];
                            $modelObject = EventLog::loadSearchModel($model);
                            $object = $modelObject::findOne($newDataVal);

                            $newDataKey = $commonFieldList[$newDataKey][0];
                            if(isset($object->$column)) {
                                $newDataVal = $object->$column;
                            } else {
                                $newDataVal = null;
                            }
                        }
                    }

                    array_push($newDataAttr, array(
                        'attribute'=>$newDataKey,
                        'value'=>$newDataVal
                    ));
                }

                $result[$key]['new_data'] = DetailView::widget([
                    'model' => $model,
                    'attributes' => $newDataAttr
                ]);

                if(empty($rowDataAttr) && empty($newDataAttr)) {
                    unset($result[$key]);
                }
            }
        }

        return new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => false
        ]);
    }
}