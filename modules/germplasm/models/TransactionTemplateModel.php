<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\models;

// Import dependencies
use app\interfaces\models\IConfig;
use app\interfaces\models\IProgram;
use app\interfaces\models\ICropProgram;
use app\interfaces\models\IGermplasmFileUpload;
use app\interfaces\modules\inventoryManager\models\IListsModel;
use app\interfaces\modules\germplasmModule\models\ITransactionTemplateModel;

// Import models
use app\models\BaseModel;
use app\models\GermplasmFileUpload;

use app\modules\germplasm\models\FileExport;
use app\modules\inventoryManager\models\TemplatesModel;
use app\modules\inventoryManager\models\ListsModel;

/**
 * Model class for Templates for File Uploads
 */
class TransactionTemplateModel extends BaseModel implements ITransactionTemplateModel{

    // Define class constants
    CONST ENTITY_GERMPLASM = 'germplasm';
    CONST ENTITY_GERMPLASM_ATTRIBUTE = 'germplasm_attribute';

    CONST ACTION_UPDATE = 'update';

    CONST TEMPLATE_GERMPLASM = "germplasm";
    CONST TEMPLATE_GERMPLASM_ATTRIBUTE = "germplasm_attribute";

    // Define fields
    CONST GERMPLASM_FIELDS = "germplasm.germplasm_code AS germplasmCode|germplasm.designation AS designation";
    CONST GERMPLASM_DATA = [ "germplasmCode", "designation" ];

    // Define action fields
    CONST ACTION_UPDATE_CONFIG = [
        self::TEMPLATE_GERMPLASM => [
            "fields" => self::GERMPLASM_FIELDS,
            "data" => self::GERMPLASM_DATA
        ]
    ];

    public $germplasmFileUploadModel;
    public $listsModel;

    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        IGermplasmFileUpload $germplasmFileUploadModel,
        IListsModel $listsModel,
        $config = [])
    {

        $this->germplasmFileUploadModel = $germplasmFileUploadModel;
        $this->listsModel = $listsModel;

        parent::__construct($id, $module, $config);
    }

    // getter methods
    public function getEntityGermplasm() { return self::ENTITY_GERMPLASM; }
    public function getEntityGermplasmAttribute() { return self::ENTITY_GERMPLASM_ATTRIBUTE; }
    
    public function getGermplasmTemplate() { return self::TEMPLATE_GERMPLASM; }
    public function getGermplasmAttributeTemplate() { return self::TEMPLATE_GERMPLASM_ATTRIBUTE; }

    public function getActionUpdate() { return self::ACTION_UPDATE; }

    /**
     * Retrieve template type in system
     * 
     * @param String $type template type selected
     * 
     * @return String
     */
    public function getTypeString($type = null) {
        // Build type string
        if (isset($type) && !empty($type) && $type === self::TEMPLATE_GERMPLASM) {
            return 'GERMPLASM';
        }
        
        return '';
    }

    /**
     * Retrieve template type options configuration
     * 
     * @return mixed
     */
    public function getTypeOptionsConfig(){
        return [
            self::ENTITY_GERMPLASM => [
                self::ACTION_UPDATE => [
                    self::TEMPLATE_GERMPLASM,
                    self::TEMPLATE_GERMPLASM_ATTRIBUTE
                ]
            ]
        ];
    }

    /**
     * Retrieve template option contents
     * 
     * @param String $action germplasm module used
     * @param String $entity data level used
     * 
     * @return Array
     */
    public function getTemplateOptions($action,$entity = null){
        if(!isset($entity)){
            return [];
        }

        if($action == self::ACTION_UPDATE && $entity == self::ENTITY_GERMPLASM) {
            return [
                [
                    "display_text" => "Germplasm",
                    "type" => self::TEMPLATE_GERMPLASM
                ],
                [
                    "display_text" => "Germplasm Attribute",
                    "type" => self::TEMPLATE_GERMPLASM_ATTRIBUTE
                ]
            ];
        }

        return [];
    }

    /**
     * Retrieve template headers
     * 
     * @param String $entity data level
     * @param String $action germplasm module used
     * @param String $userRole user role
     * @param String $program dashboard program
     * @param String $cropCode crop code for dashboard program
     * @param String $templateType template type selected
     * 
     * @return mixed
     */
    public function getTemplateHeaders($entity,$action,$userRole,$program,$cropCode,$templateType){
        $configValues = $this->germplasmFileUploadModel->getConfigValues($entity,$action,$userRole,$program,$cropCode);
        
        // retrieve only basic variables
        $variables = ['GERMPLASM_CODE']; //required variable in export

        if( isset($configValues['configValues']) && !empty($configValues['configValues']) &&
            isset($configValues['configValues']['values']) && !empty($configValues['configValues']['values'])){

            $newVariables = array_filter(
                $configValues['configValues']['values'] ?? [],
                function($variable) use ($templateType){
                    if($templateType == 'germplasm'){
                        return  $variable['in_export'] == 'true' &&
                                $variable['entity'] == 'germplasm' && 
                                $variable['type'] == 'basic';
                    }
                    else{
                        return  $variable['in_export'] == 'true' &&
                                in_array($variable['entity'],['germplasm','germplasm_attribute']) && 
                                in_array($variable['type'],['basic','attribute']);
                    }
                }
            );

            $newVariables = array_column($newVariables,'abbrev');
            $variables = array_values(array_unique(array_merge($variables,$newVariables)));
        }

        return $variables;
    }

    /**
     * Retrieve items from selected saved list
     * 
     * @param String $listId uique list identifier
     * @param String $templateType selected template type
     * @param Array $headers extracted fiel headers
     * @param String $action germplasm module used
     * 
     * @return mixed
     */
    public function getListMemberData($listId, $templateType, $headers, $action){
        $fileData = [];

        // build parameters
        $fieldsConfigAction = $action == 'update' ? self::ACTION_UPDATE_CONFIG : self::ACTION_UPDATE_CONFIG;
        $fieldsConfigEntity = $fieldsConfigAction['germplasm'];
        
        $fields = "";
        $columnData = [];
        if(!empty($fieldsConfigEntity)){
            $fields = $fieldsConfigEntity['fields'];
            $columnData = $fieldsConfigEntity['data'];
        }
        
        // retrieve list data
        $result = $this->listsModel->searchAllMembers($listId,[
            "fields" => $fields,
            "isActive" => "equals true"
        ]);
        $listMembers = $result['data'] ?? [];
        
        // build data based on identified template data
        $headCount = count($headers) ?? 0;
        
        $colData = [];
        $rowData = [];

        $fileData = array_map(function($listMember) use ($headCount,$columnData,$rowData){

            for($i = 0;$i < $headCount; $i++){
                $rowData[] = isset($columnData[$i]) && !empty($columnData[$i]) ? 
                                $listMember[$columnData[$i]] : '';
            }

            return $rowData;

        },$listMembers);

        return $fileData;
    }

}
