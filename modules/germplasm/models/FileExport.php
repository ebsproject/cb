<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace app\modules\germplasm\models;

/**
 * File export model for Germplasm Manager tool
 */
class FileExport
{
    // File types
    const FILE_UPLOAD_TEMPLATE = 'File_Upload_Template';

    // File formats
    const CSV = 'csv';

    private string $fileType;
    private string $timestamp;
    private string $fileFormat;
    private array $fileContent;
    private string $fileName;

    public function __construct(string $fileType, string $fileFormat, int $time) {
        if (!in_array($fileType, $this->getFileTypes())) {
            throw new \TypeError('Unsupported file type: '.$fileType);
        }
        // Empty string file formats will return filenames with no extension
        if (!in_array($fileFormat, $this->getFileFormats()) && $fileFormat !== '') {
            throw new \TypeError('Unsupported file format: '.$fileFormat);
        }
        $this->fileType = $fileType;
        $this->fileFormat = $fileFormat;
        $this->timestamp = self::formatTimestamp($time);

        $this->fileContent = [];
        $this->fileName = '';
    }

    /**
     * Returns all valid file types for export
     *
     * @return string[] list of file types
     */
    public static function getFileTypes()
    {
        return [
            self::FILE_UPLOAD_TEMPLATE
        ];
    }

    /**
     * Returns all valid file formats for export
     *
     * @return string[] list of file formats
     */
    public static function getFileFormats()
    {
        return [
            self::CSV
        ];
    }

    /**
     * Returns a string-formatted timestamp
     *
     * @param int time Unix timestamp
     * @return string timestamp in YYYYMMDD_HHmmss format
     */
    public static function formatTimestamp($time)
    {
        return date("Ymd_His", $time);
    }


    /**
     * Adds content to file and assigns file name to export file
     * 
     * @param Array $content file content
     * @param String $name file name
     * 
     * @return Integer size of file content
     */
    public function addFileData(array $content,string $name){
        $this->fileContent = $content;
        $this->fileName = $name;

        return count($this->fileContent);
    }

    /**
     * Retrieve file content
     * 
     * @return Object file content
     */
    public function getUnzipped(){
        $unzippedContent = [ $this->fileName => $this->fileContent ];

        return $unzippedContent;
    }
}