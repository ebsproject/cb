<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\models;

use Yii;

use yii\web\UploadedFile;

// Import models
use app\models\BaseModel;
use app\modules\germplasm\models\ViewBrowserModel;

// Import interfaces
use app\interfaces\modules\germplasmModule\models\IMergeModel;
use app\interfaces\modules\germplasmModule\models\IConflictReportModel;
use app\interfaces\models\IUserDashboardConfig;

/**
 * Model for facilitating germplasm merge processes
 */
class ConflictReportModel extends BaseModel implements IConflictReportModel
{
    // merge conflict columns
    CONST CONFLICT_REPORT_COLUMNS = ['row_number','conflict_code','conflict_variable',
        'keep_germplasm_designation','merge_germplasm_designation','keep_germplasm_code','merge_germplasm_code',
        'keep_value','merge_value'];

    public $uploadedFile;
    public $userDashboardConfig;
    public $viewBrowserModel;

    public function __construct(
        UploadedFile $uploadedFile,
        IUserDashboardConfig $userDashboardConfig,
        ViewBrowserModel $viewBrowserModel
    )
    {
        $this->uploadedFile = $uploadedFile;
        $this->userDashboardConfig = $userDashboardConfig;
        $this->viewBrowserModel = $viewBrowserModel;
    }

    public function getConflictColumns() { return self::CONFLICT_REPORT_COLUMNS; }

    /**
     * Retrieve merge conflict for the transaction
     * 
     * @param Integer $fileId germplasm file upload identifier
     * @param String $conflictReportName conflict report file name
     * @param Integer $browserId browser identifier
     * 
     * @return mixed
     */
    public function search($fileId,$conflictReportName,$browserId){
        // retrieve uploaded file content

        $content = $this->extractConflictReportData($conflictReportName);

        if(!empty($content)){
            $paginatedData = $this->viewBrowserModel->paginate($content, $browserId);

            return [
                'data' => $paginatedData,
                'totalCount' => count($content),
                'success' => true
            ];
        }

        return [
            'data' => [],
            'totalCount' => 0,
            'success' => true
        ];
    }

    /**
     * Retrieve merge conflict report from generated data
     * 
     * @param String $conflictReportName report name
     * 
     * @return Array $content
     */
    public function extractConflictReportData($conflictReportName){
        try{
            $downloadPath = realpath(Yii::$app->basePath) . "/files/data_export";
            $pathToFile = $downloadPath . "/$conflictReportName";
            
            if (file_exists($pathToFile)) {
                $csv = new \app\components\CsvReader($pathToFile);
                $csv->setHasHeader(true);

                $columnHeaders = array_map('strtoupper',$csv->getHeader());
                $content = [];
                
                if(isset($csv->read()[0])){
                    
                    foreach ($csv as $line) {
                        array_push($content,$line);
                    }

                    return $content;
                }
            }

            return [];
        }
        catch(\Exception $e){
            return [];
        }
    }
}

?>