<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\models;

use Yii;
use yii\helpers\Html;

use kartik\select2\Select2;
use app\components\Config;

use app\interfaces\models\IVariable;

/**
 * Model for germplasm coding
 */
class CodingModel
{
    public function __construct (
        public IVariable $variableModel,
        public Config $configModel

    ) {
        $this->configModel = $configModel;
    }

    /**
     * Retrieve germplasm records based on source tool
     * @param sourceToolAbbrev tool abbrev where the request originates from
     */
    public function getGermplasmRecords($sourceToolAbbrev, $mode, $selectedIdsString = '')
    {
        $germplasmRecords = [
            "totalCount" => 0,
            "germplasmDbIds" => [],
            "apiFilters" => null
        ];
        if ($sourceToolAbbrev == 'GM') {
            if ($mode == 'selected') {
                $selectAllFlag = Yii::$app->session->get('germplasm-select_all_flag');

                if ($selectAllFlag == 'include mode') {
                    $germplasmRecords["germplasmDbIds"] = Yii::$app->session->get('germplasm-selected_ids') ?? [];
                    $germplasmRecords['totalCount'] = count($germplasmRecords["germplasmDbIds"]);
                } else {
                    $germplasmRecords['totalCount'] = Yii::$app->session->get('germplasm-total_results_count') ?? 0;
                    $gmFilters = Yii::$app->session->get('germplasm-api_filter');
                    $germplasmRecords['apiFilters'] = $gmFilters;
                    $germplasmRecords['apiMethod'] = "POST";
                    $germplasmRecords['apiEndpoint'] = "germplasm-search";
                }
            } else if ($mode == 'all') {
                // Get current browser filters
                $gmFilters = Yii::$app->session->get('germplasm-api_filter');
                $germplasmRecords['apiFilters'] = $gmFilters;
                $germplasmRecords['apiMethod'] = "POST";
                $germplasmRecords['apiEndpoint'] = "germplasm-search";

                // Get one germplasm and check total count from metadata
                $germplasmSearch = Yii::$app->api->getResponse(
                    "POST", 
                    "germplasm-search", 
                    json_encode($gmFilters), 
                    "limit=1", 
                    false
                );
                $germplasmRecords['totalCount'] = $germplasmSearch['body']['metadata']['pagination']['totalCount'];
            }
        } else if ($sourceToolAbbrev = 'HM') {
            $germplasmIds = explode("|", $selectedIdsString);
            $germplasmRecords['totalCount'] = count($germplasmIds);
            $germplasmRecords["germplasmDbIds"] = $germplasmIds;
        }

        return $germplasmRecords;
    }

    /**
     * Builds initiap inputs for germplasm coding.
     * Currently builds the following
     *  - germplasm name type
     *  - germplasm name status
     *  - coding option
     */
    public function buildDefaultInputs($germplasmNameTypeValues = null) {
        $inputFields = [];
        
        // Get germplasm name types
        // Return to default if empty or null the retrieved germplasm name type
        if(!$germplasmNameTypeValues){
            $result = $this->variableModel->getVariableByAbbrev("GERMPLASM_NAME_TYPE");
            $variableId = $result['variableDbId'] ?? 0;
            $result = $this->variableModel->getVariableScales($variableId);
            $germplasmNameTypeValues = $result['scaleValues'] ?? [];
        }

        $germplasmNameTypes = array_column($germplasmNameTypeValues, 'displayName', 'value');

        $germplasmNameStatus = [
            "active" => "active",
            "standard" => "standard"
        ];
        $codingOptions = [
            "free-text" => "FREE TEXT",
            "configured-code" => "CONFIGURED CODE"
        ];

        // BUILD SELECT2 FOR GERMPLASM NAME TYPE
        $inputFields["name-type"] = "<div id='ge-germplasm-coding-name-type-input-div' class='hidden'>" .
            '<label for="ge-germplasm-coding-name-type-input" class="active">Name Type <span style="color: red" ><b>*</b></span></label>'
            . Select2::widget([
                'name' => 'germplasm_name_type',
                'data' => $germplasmNameTypes,
                'value' => '',
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => "Select name type",
                    'class' => "ge-germplasm-coding-input",
                    'id' => "ge-germplasm-coding-name-type-input",
                    'title' => "Germplasm Name Type",
                    'disabled' => false,
                    'multiple' => false,
                    'allowClear' => false,
                ],
                'pluginOptions' => [
                    'allowClear' => false
                ]
            ]) . "</div>";

        // BUILD SELECT2 FOR GERMPLASM NAME STATUS
        $inputFields["name-status"] = "<div id='ge-germplasm-coding-name-status-input-div' class='hidden'>" .
            '<label for="ge-germplasm-coding-name-status-input" class="active">Name Status <span style="color: red"><b>*</b></span></label>   ' .
            Select2::widget([
                'name' => 'germplasm_name_status',
                'data' => $germplasmNameStatus,
                'value' => '',
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => "Select name status",
                    'class' => "ge-germplasm-coding-input",
                    'id' => "ge-germplasm-coding-name-status-input",
                    'title' => "Germplasm Name Status",
                    'disabled' => false,
                    'multiple' => false,
                    'allowClear' => false,
                ],
                'pluginOptions' => [
                    'allowClear' => false
                ]
            ]) . "</div>";

        // BUILD SELECT2 FOR GERMPLASM CODING OPTION
        $inputFields["coding-option"] = "<div id='ge-germplasm-coding-coding-option-input-div' class='active'>" .
            '<label for="ge-germplasm-coding-option-input" class="active">Coding Option <span style="color: red"><b>*</b></span></label>    ' .
            Select2::widget([
                'name' => 'germplasm_coding_option',
                'data' => $codingOptions,
                'value' => '',
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => "Select coding option",
                    'class' => "ge-germplasm-coding-input",
                    'id' => "ge-germplasm-coding-option-input",
                    'title' => "Coding Option",
                    'disabled' => false,
                    'multiple' => false,
                    'allowClear' => false,
                ],
                'pluginOptions' => [
                    'allowClear' => false
                ]
            ]) . "</div>";

        return $inputFields;
    }

    /**
     * Retrieves germplasm coding threshold
     * 
     */
    public function getGermplasmCodingThreshold() {
        // If no value was retrieved, set to default value of threshold to 5000
        $germplasmCodingThreshold = $this->configModel->getAppThreshold('GERMPLASM_MANAGER', 'germplasmCodingMax') ?? 5000;
        
        return $germplasmCodingThreshold;
    }

    /**
     * Build HTML field
     * 
     * @param String $type HTML field type
     * @param Object $info field information
     * @param Integer $sequenceId sequence identifier
     * @param Object $params additional field parameters
     * 
     * @return Object HTML field
     */
    public function buildHTMLField($type, $info, $sequenceId, $params, $fieldHasScaleValues)
    {
        $fieldId = "";
        $label = "";
        $presetValue = "";
        $htmlField = null;

        $isRequired = false;
        $disabled = false;

        if (isset($info) && !empty($info)) {
            $fieldName = $info['name'];
            $fieldTitle = $info['name'];
            $fieldId = 'gmcoding-configuredcode-' . $info['name'] . '-field';
            $fieldDivId = $fieldId;
            $fieldClass = 'gmcoding-configuredcode-field';
            $presetValue = $info['formula'];
            $isRequired = true; // default

            // If field is a sequence segment, preview sequence value
            if (
                $info['dataType'] == 'NUMBER'
                && !isset($info['segment_sequence_id'])
                && !empty($info["last"])
            ) {
                $presetValue = intval($info["last"]) + 1;
            }

            // Extract label from name
            $label = $info['name'];
            $label = str_replace('-', ' ', $info['name']);

            if ($type == 'selection' && $fieldHasScaleValues) {
                $label = $label . "-scale-values";
                $fieldName = "gmcoding-configuredcode-" . $fieldName . "-scaleval-field";
            }

            $label = '<label for="' . $fieldName . '">' . $label . '</label>';
            $disabled = $info['required'] == 'true' ? true : false;
        } else {
            return null;
        }

        if ($type == 'input') {
            $additionalParams = array_merge(
                [
                    'id' => $fieldId,
                    'class' => $fieldClass,
                    'title' => $fieldTitle,
                    'label' => $fieldTitle,
                    'value' => $presetValue,
                    'disabled' => $disabled
                ],
                $params
            );

            $htmlField = Html::input(
                'text',
                $fieldName,
                $presetValue,
                $additionalParams
            );
        } else if ($type == 'selection' && $fieldHasScaleValues) {
            $scaleValArr  = $info['scale'];

            $scaleValues = [];
            foreach ($scaleValArr as $scale) {
                $indx = strtolower($scale);
                $scaleValues["$indx"] = $scale;
            }

            $additionalOptions = array_merge($params, [
                'id' => "gmcoding-configuredcode-" . $info['name'] . "-scaleval-field-hidden",
                'class' => $fieldClass . ' hidden',
                'disabled' => true
            ]);

            $hiddenField = Html::input(
                'text',
                $fieldName . "hdn-field",
                null,
                $additionalOptions
            );

            $fieldId = "gmcoding-configuredcode-" . $info['name'] . "-scaleval-field";
            $htmlField = Select2::widget([
                'name' => $fieldName,
                'id' => $fieldId,
                'data' => $scaleValues,
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => "Select value",
                    'class' => $fieldClass . '-hidden scaleval-hdn-field',
                    'id' => $fieldId,
                    'title' => "Segment Options",
                    'disabled' => false,
                    'multiple' => false,
                    'allowClear' => false,
                ],
                'pluginOptions' => [
                    'allowClear' => false
                ]
            ]);

            $htmlField = $hiddenField . $htmlField;

            $fieldDivId = $fieldId . "-div";
        }

        $required = $isRequired ? '<span style="color: red" ><b>*</b></span>' : '';
        $htmlDiv = '<div id="' . $fieldDivId . '" >' . $label . ' ' . $required . '</label>' . $htmlField . "</div>";

        return $htmlDiv;
    }

    /**
     * Checks if user has access to Germplasm Coding capability
     */
    public function hasCodingAccess()
    {
        // Determine if user is ADMIN user
        $isAdmin = Yii::$app->session->get('isAdmin');
        // Determine if RBAC is in use
        $enableRbac = Yii::$app->session->get('enableRbac');

        // Check RBAC if has access
        $hasAccess = Yii::$app->access->renderAccess("GERMPLASM_CODING", "GERMPLASM_CATALOG");

        // if uses RBAC and has access to GERMPLASM_CODING 
        // OR not using RBAC, but is an Admin
        if (($enableRbac && $hasAccess) || (!$enableRbac && $isAdmin)) {
            return true;
        }
        // if uses RBAC but no access to GERMPLASM_CODING 
        // OR not using RBAC, and not an Admin
        else if (($enableRbac && !$hasAccess) || (!$enableRbac && !$isAdmin)) {
            return false;
        }

        return false;
    }
}
