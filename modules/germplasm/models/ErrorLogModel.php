<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\germplasm\models;

use app\models\GermplasmFileUpload;

// Import interfaces
use app\interfaces\models\IConfig;
use app\interfaces\models\ICrop;
use app\interfaces\models\IProgram;
use app\interfaces\models\IUserDashboardConfig;

// Import data providers
use app\dataproviders\ArrayDataProvider;

/**
 * Model for facilitating displaying the error log file upload data
 */
class ErrorLogModel extends GermplasmFileUpload
{
    public $config;
    public $crop;
    public $program;

    /**
     * Model constructor
     */
    public function __construct (
        IConfig $config,
        ICrop $crop,
        IProgram $program,
        public IUserDashboardConfig $userDashboardConfig
    )
    {
        $this->config = $config;
        $this->crop = $crop;
        $this->program = $program;
    }

    /**
     * Retrieves the file upload record given its ID
     * @param Integer $fileUploadDbId file upload identifier
     */
    public function getFileUploadRecord($fileUploadDbId) {
        // Get record
        $result = $this->getOne($fileUploadDbId);

        // If retrieval failed, throw error
        if($result['status'] != 200) {
            $errorMessage = 'There was a problem loading the information.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        return $result['data'] ?? [];
    }

    /**
     * Retrieves the grid columns
     */
    public function getColumns() {
        $columns = [
            [
                'attribute'=>'row_number',
                'label' => 'Row No',
            ],
            [
                'attribute'=>'entity',
                'label' => 'Entity',
                'content' => function($data) {
                    $entity = str_replace('_', ' ', $data['entity']);
                    return $entity;
                }
            ],
            [
                'attribute'=>'status_code',
                'label' => 'Status Code',
                'format' => 'raw',
                'value' => function ($model) {
                    $status = $model['status_code'] ?? '';

                    if($status == 'ERROR') {
                        $status = "<p class='red-text text-darken-2'>$status</p>";
                    }

                    return "<strong>$status</strong>";
                }
            ],
            [
                'attribute'=>'message',
                'label' => 'Error Log Message',
                'contentOptions' => [
                    "class" => "germplasm-name-col"
                ],
                'format' => 'raw',
                'value' => function ($model) {
                    $message = $model['message'] ?? '';
                    $messageParts = explode(':', $message);
                    $mainMessage = $messageParts[0] ?? '';
                    $messageParts[0] = "<strong>$mainMessage</strong>";
                    $message = implode(": ", $messageParts) ;

                    return $message;
                }
            ],
            [
                'attribute'=>'timestamp',
                'label' => 'Error Log Timestamp',
            ]
        ];

        return $columns;
    }

    /**
     * Perform pagination operation on the file data array
     * to account for the browser page size and current page number.
     * @param Array $fileData file data array
     */
    public function paginate($fileData) {
        $getParams = \Yii::$app->request->getQueryParams();

        // Pagination
        $page = 1;
        if (isset($getParams['page'])) {
            $page = intval($getParams['page']);
        }

        // Page limit
        $perPage = $this->userDashboardConfig->getDefaultPageSizePreferences();

        // If current page is over the total number of pages, return to page 1
        if($page > ceil(count($fileData)/$perPage)) {
            $page = 1;
            $_GET['page'] = 1;
        }

        return array_slice($fileData, ($perPage * $page - $perPage), $perPage);
    }

    /**
     * Retrieves the info for the file data error log
     * @param Integer $fileUploadDbId file upload identifier
     * @param Integer $programDbId program identifier
     */
    public function search($fileUploadDbId, $programDbId) {
        // Retrieve file upload record
        $fileUploadRecord = $this->getFileUploadRecord($fileUploadDbId);

        // Extract details
        $fileName = $fileUploadRecord['fileName'] ?? 'Unknown file';
        $fileStatus = !empty($fileUploadRecord['fileStatus']) ? ucfirst($fileUploadRecord['fileStatus']) : 'Unknown status';
        $uploader = $fileUploadRecord['uploader'] ?? 'Unknown uploader';
        $uploadTimestamp = $fileUploadRecord['uploaderTimestamp'] ?? 'Unknown timestamp';
        $fileData = $fileUploadRecord['errorLog'] ?? [];
        $totalCount = count($fileData);
        // Perform pagination
        $fileData = $this->paginate($fileData);
        // Build columns
        $columns = $this->getColumns();
        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'id' => 'gm-error-log-grid',
            'allModels' => $fileData,
            'key' => 'row_number',
            'restified' => true,
            'totalCount' => $totalCount
        ]);

        return [
            "fileName" => $fileName,
            "fileStatus" => $fileStatus,
            "uploader" => $uploader,
            "uploadTimestamp" => $uploadTimestamp,
            "dataProvider" => $dataProvider,
            "columns" => $columns,
        ];
    }
}