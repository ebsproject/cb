<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\germplasm\models;

use app\models\BaseModel;
use app\models\Germplasm;
use app\models\GermplasmName;

use app\interfaces\models\IGermplasm;
use app\interfaces\modules\germplasmModule\models\IDeleteGermplasmModel;

/**
 * Model for facilitating displaying the error log file upload data
 */
class DeleteGermplasmModel extends BaseModel implements IDeleteGermplasmModel
{
    public $germplasmModel;
    public $germplasmNameModel;

    /**
     * Model constructor
     */
    public function __construct (
        IGermplasm $germplasmModel,
        GermplasmName $germplasmNameModel
    )
    {
        $this->germplasmModel = $germplasmModel;
        $this->germplasmNameModel = $germplasmNameModel;
    }

    /**
     * Delete Germplasm record
     * 
     * @param Integer $germplasmId germplasm record identifier
     * 
     * @return Boolean $success deletion process status
     */
    public function deleteGermplasm($germplasmId){
        $success = true;

        $method = "DELETE";
        $endpoint= 'germplasm/'.intval($germplasmId);

        $result = \Yii::$app->api->getResponse($method, $endpoint, '');

        if(isset($result['status']) && $result['status'] != 200){
            $success = false;
        }

        if(!isset($result['status']) || empty($result['status'])){
            $success = false;
        }

        return $success;
    }

    /**
     * Delete Germplasm name record/s for a given germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * 
     * @return Boolean $success deletion process status
     */
    public function deleteGermplasmName($germplasmId){
        $success = true;

        $method = 'POST';
        $endpoint= 'germplasm/'.intval($germplasmId).'/delete-germplasm-names';

        $result = \Yii::$app->api->getResponse($method, $endpoint, '');

        if(!isset($result['status']) || empty($result['status'])){
            $success = false;
        }

        if(isset($result['status']) && $result['status'] != 200){
            $success = false;
        }

        return $success;
    }

    /**
     * Delete Germplasm relation record/s for a germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * 
     * @return Boolean $success deletion process status
     */
    public function deleteGermplasmRelation($germplasmId){
        $success = true;

        $method = 'POST';
        $endpoint = 'germplasm/'.intval($germplasmId).'/delete-germplasm-relations';

        $result = \Yii::$app->api->getResponse($method, $endpoint, '');

        if(!isset($result['status']) || empty($result['status'])){
            $success = false;
        }

        if(isset($result['status']) && $result['status'] != 200){
            $success = false;
        }

        return $success;
    }

    /**
     * Delete Germplasm attribute record/s for a germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * 
     * @return Boolean $success deletion process status
     */
    public function deleteGermplasmAttribute($germplasmId){
        $success = true;

        $method = 'POST';
        $endpoint= 'germplasm/'.intval($germplasmId).'/delete-germplasm-attributes';

        $result = \Yii::$app->api->getResponse($method, $endpoint, '');

        if(!isset($result['status']) || empty($result['status'])){
            $success = false;
        }

        if(isset($result['status']) && $result['status'] != 200){
            $success = false;
        }

        return $success;
    }

    /** 
     * delete transaction germplasm names
     * 
     * @param Array $germplasmIdsArr array of germplasm IDs
     * @param String $germplasmNameType germplasm name type
     * @param String $germplasmNameStatus germplasm name status
     * 
     */
    public function deleteTransactionGermplasmName($germplasmIdsArr, $germplasmNameType, $germplasmNameStatus)
    {
        foreach ($germplasmIdsArr as $germplasmId) {
            // retrieve germplasm name ID using germplasm ID and other parameters
            $method = 'POST';
            $endpoint = 'germplasm-names-search';
            $limits = 'limit=1&includeDraft=true';
            $params = [
                'germplasmDbId' => "equals " . $germplasmId,
                'germplasmNameType' => $germplasmNameType,
                'germplasmNameStatus' => $germplasmNameStatus
            ];

            $result = \Yii::$app->api->getResponse($method, $endpoint . '?' . $limits, json_encode($params));

            if ($result['status'] !== 200) {
                continue;
            }

            // void germplasm name records
            $germplasmNameRecord = $result['body']['result']['data'][0] ?? [];

            if (!empty($germplasmNameRecord)) {
                $recordId = $germplasmNameRecord['germplasmNameDbId'];
                $method = "DELETE";
                $endpoint = 'germplasm-names/' . $recordId;

                $result = \Yii::$app->api->getResponse($method, $endpoint . '?' . $limits, '');

                if ($result['status'] !== 200) {
                    continue;
                }
            }
        }

        return true;
    }
}
