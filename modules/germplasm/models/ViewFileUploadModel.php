<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\germplasm\models;

use Yii;
use app\models\GermplasmFileUpload;

// Import interfaces
use app\interfaces\models\IConfig;
use app\interfaces\models\ICrop;
use app\interfaces\models\IProgram;
use app\interfaces\models\IUserDashboardConfig;

// Import data providers
use app\dataproviders\ArrayDataProvider;

use app\widgets\GermplasmInfo;
use app\widgets\SeedInfo;

/**
 * Model for facilitating displaying the preview of the file upload data
 */
class ViewFileUploadModel extends GermplasmFileUpload
{
    public $config;
    public $crop;
    public $program;

    /**
     * Model constructor
     */
    public function __construct (
        IConfig $config,
        ICrop $crop,
        IProgram $program,
        public IUserDashboardConfig $userDashboardConfig
    )
    {
        $this->config = $config;
        $this->crop = $crop;
        $this->program = $program;
    }

    /**
     * Retrieves the file upload record given its ID
     * @param Integer $fileUploadDbId file upload identifier
     */
    public function getFileUploadRecord($fileUploadDbId) {
        // get all records
        $result = Yii::$app->api->getResponse( 
            'POST', 
            'germplasm-file-uploads/'.$fileUploadDbId.'/germplasm-search', 
            null, 
            '', 
            true);   

        // If retrieval failed, throw error
        if($result['status'] != 200 || !isset($result['body']['result']['data'][0])) {
            $errorMessage = 'There was a problem loading the information.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        return $result;
    }

    /**
     * Retrieves the configs and formats those into grid columns
     */
    public function getColumns() {
        $germplasmColumns = [
            [
                'attribute'=>'designation',
                'label' => 'Germplasm',
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ],
                'value'=>function($data) {
                    $germplasmDbId = $data['germplasmDbId'];
                    $designation = $data['designation'];

                    $linkAttrs = [
                        'id' => 'view-germplasm-widget-'.$germplasmDbId,
                        'class' => 'blue-text text-darken-4 header-highlight',
                        'title' => \Yii::t('app', 'Click to view more information'),
                        'data-label' => $designation,
                        'data-id' => $germplasmDbId,
                        'data-target' => '#view-germplasm-widget-modal',
                        'data-toggle' => 'modal'
                    ];

                    $widgetName = GermplasmInfo::widget([
                        'id' => $germplasmDbId,
                        'entityLabel' => $designation,
                        'linkAttrs' => $linkAttrs
                    ]);
                    return $widgetName;
                },
                'format'=>'raw',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'germplasmNameType',
                'label' => 'Germplasm Name Type',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'parentage',
                'contentOptions' => [
                    'class' => 'germplasm-name-col'
                ],
                'headerOptions' => [
                    'min-width' => '150px'
                ],
                'mergeHeader' => true
            ],
            [
                'attribute'=>'generation',
                'label' => 'Generation',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'germplasmState',
                'label' => 'Germplasm State',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'taxonomyName',
                'label' => 'Taxonomy',
                'mergeHeader' => true
            ]
        ];

        $seedColumns = [
            [
                'attribute'=>'designation',
                'label' => 'Germplasm',
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ],
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedName',
                'label' => 'Seed Name',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'programCode',
                'label' => 'Program',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'harvestDate',
                'label' => 'Harvest Date',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'harvestMethod',
                'label' => 'Harvest Method',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedMtaNumber',
                'label' => 'MTA Number',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedMtaStatus',
                'label' => 'MTA Status',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedIpStatus',
                'label' => 'IP Status',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedImport',
                'label' => 'Import',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedOrigin',
                'label' => 'Origin',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedSourceHarvestYear',
                'label' => 'Source Harvest Year',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedSourceOrganization',
                'label' => 'Source Organization',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'seedSourceStudy',
                'label' => 'Source Study',
                'mergeHeader' => true
            ]
        ];

        $packageColumns = [
            [
                'attribute'=>'designation',
                'label' => 'Germplasm',
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ],
                'mergeHeader' => true
            ],
            [
                'attribute'=>'packageLabel',
                'label' => 'Package Label',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'programCode',
                'label' => 'Program',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'packageStatus',
                'label' => 'Package Status',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'packageQuantity',
                'label' => 'Package Quantity',
                'mergeHeader' => true
            ],
            [
                'attribute'=>'packageUnit',
                'label' => 'Package Unit',
                'mergeHeader' => true
            ]
        ];

        $columns = [
            'germplasmColumns' => $germplasmColumns,
            'seedColumns' => $seedColumns,
            'packageColumns' => $packageColumns
        ];

        return $columns;
    }

    /**
     * Perform pagination operation on the file data array
     * to account for the browser page size and current page number.
     * @param Array $fileData file data array
     * @param String $entity data entity
     */
    public function paginate($fileData, $entity) {
        $getParams = \Yii::$app->request->getQueryParams();

        // Pagination
        $page = 1;
        $browserId = 'gm-file-upload-'.$entity.'-grid';

        if (isset($getParams['page'])){
            $page = intval($getParams['page']);
        }

        if(isset($getParams[$browserId.'-page'])){
            $page = intval($getParams[$browserId.'-page']);
        }

        // Page limit
        $perPage = $this->userDashboardConfig->getDefaultPageSizePreferences();

        // If current page is over the total number of pages, return to page 1
        if($page > ceil(count($fileData)/$perPage)) {
            $page = 1;
            $_GET['page'] = 1;
            $_GET[$browserId.'-page'] = 1;
        }

        return array_slice($fileData, ($perPage * $page - $perPage), $perPage);
    }

    /**
     * Retrieves the info for the file data preview
     * @param Integer $fileUploadDbId file upload identifier
     * @param Integer $programDbId program identifier
     */
    public function search($fileUploadDbId, $programDbId) {
        // Retrieve file upload record
        $record = $this->getFileUploadRecord($fileUploadDbId);
        $totalCount = $record['body']['metadata']['pagination']['totalCount'] ?? 0;
        $fileUploadRecord = $record['body']['result']['data'][0] ?? [];
        $data = [];
        foreach($record['body']['result']['data'] as $key => $recordData) {     // build data
            $data = array_merge($data, $recordData['germplasm']);
        }

        // Extract details
        $fileName = $fileUploadRecord['fileName'] ?? 'Unknown file';
        $fileStatus = !empty($fileUploadRecord['fileStatus']) ? ucfirst($fileUploadRecord['fileStatus']) : 'Unknown status';
        $uploader = $fileUploadRecord['uploader'] ?? 'Unknown uploader';
        $uploadTimestamp = $fileUploadRecord['uploaderTimestamp'] ?? 'Unknown timestamp';
        $fileData = $data;
        // Perform pagination
        $germplasmFileData = $this->paginate($fileData, 'germplasm');
        $seedFileData = $this->paginate($fileData, 'seed');
        $packageFileData = $this->paginate($fileData, 'package');
        // Build columns
        $columns = $this->getColumns();
        // Assemble data provider
        $dataProviders = [
            'germplasmDataProvider' => new ArrayDataProvider([
                'id' => 'gm-file-upload-germplasm-grid',
                'allModels' => $germplasmFileData,
                'restified' => true,
                'totalCount' => $totalCount
            ]),
            'seedDataProvider' => new ArrayDataProvider([
                'id' => 'gm-file-upload-seed-grid',
                'allModels' => $seedFileData,
                'restified' => true,
                'totalCount' => $totalCount
            ]),
            'packageDataProvider' => new ArrayDataProvider([
                'id' => 'gm-file-upload-package-grid',
                'allModels' => $packageFileData,
                'restified' => true,
                'totalCount' => $totalCount
            ])
        ];

        return [
            "fileName" => $fileName,
            "fileStatus" => $fileStatus,
            "uploader" => $uploader,
            "uploadTimestamp" => $uploadTimestamp,
            "dataProviders" => $dataProviders,
            "columns" => $columns
        ];
    }

    /**
     * Builds data provider for entity browser
     * 
     * @param Array $browserVariables variables used for as browser columns
     * @param String $entity entity to be viewed - germplasm, seed, package
     * @param Integer $fileUploadDbId germplasm file upload record ID
     */
    public function getDataProvider($browserVariables, $entity, $fileUploadDbId){

        $abbrevAttribCompat = [
            "hvdateCont" => "harvestDate",
            "hvMethDisc" => "harvestMethod",
            "volume" => "packageQuantity"
        ];

        if(!isset($browserVariables) || empty($browserVariables)){
            return [
                'data' => [],
                'columns' => [],
                'totalCount' => 0
            ];
        }

        // build columns
        $columns = [];
        foreach($browserVariables AS $column){
            $columnAttribStr = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($column['abbrev'])))));
            $columnAttribStr = isset($abbrevAttribCompat[$columnAttribStr]) ? $abbrevAttribCompat[$columnAttribStr] : $columnAttribStr;

            if($entity == 'germplasm' && $column['abbrev'] == 'DESIGNATION'){
                $browserColumn = [
                    'attribute' => $columnAttribStr,
                    'label' => $column['label'],
                    'contentOptions'=>[
                        'class' => 'germplasm-name-col'
                    ],    
                    'value' => function($data) {
                        $germplasmDbId = $data['germplasmDbId'];
                        $designation = $data['designation'];
                        
                        $linkAttrs = [
                            'id' => 'view-germplasm-widget-'.$germplasmDbId,
                            'class' => 'blue-text text-darken-4 header-highlight',
                            'title' => \Yii::t('app', 'Click to view more information'),
                            'data-label' => $designation,
                            'data-id' => $germplasmDbId,
                            'data-target' => '#view-germplasm-widget-modal',
                            'data-toggle' => 'modal'
                        ];
            
                        $widgetName = GermplasmInfo::widget([
                            'id' => $germplasmDbId,
                            'entityLabel' => $designation,
                            'linkAttrs' => $linkAttrs
                        ]);
                        return $widgetName;
                    },
                    'visible' => true,
                    'hidden' => false,
                    'format'=>'raw',
                    'mergeHeader' => true,
                    
                ];
            }
            else if($entity == 'seed' && $column['abbrev'] == 'SEED_NAME'){
                $browserColumn = [
                    'attribute'=>$columnAttribStr,
                    'label' => 'Seed Name',
                    'contentOptions'=>[
                        'class' => 'seed-name-col'
                    ],
                    'value'=>function($data) {
                        $seedDbId = $data['seedDbId'];
                        $seedName = $data['seedName'];
    
                        $linkAttrs = [
                            'id' => 'view-seed-widget-'.$seedDbId,
                            'class' => 'blue-text text-darken-4 header-highlight',
                            'title' => \Yii::t('app', 'Click to view more information'),
                            'data-label' => $seedName,
                            'data-id' => $seedDbId,
                            'data-target' => '#view-seed-widget-modal',
                            'data-toggle' => 'modal'
                        ];
    
                        $widgetName = SeedInfo::widget([
                            'id' => $seedDbId,
                            'entityLabel' => $seedName,
                            'label' => $seedName,
                            'linkAttrs' => $linkAttrs
                        ]);
                        return $widgetName;
                    },
                    'format'=>'raw',
                    'mergeHeader' => true
                ];
            }
            else{
                $browserColumn = [
                    'attribute' => $columnAttribStr,
                    'label' => $column['label'],
                    'visible' => true,
                    'hidden' => false,
                    'mergeHeader' => true
                ];
            }

            array_push($columns,$browserColumn);
        };

        // retrieve data
        $fileData = [];
        $endpointEntity = $entity != 'germplasm' ? $entity.'s' : $entity;

        $method = 'POST';
        $endpoint = 'germplasm-file-uploads/' . $fileUploadDbId . '/' . ($endpointEntity) . '-search';
        
        $result = Yii::$app->api->getResponse($method, $endpoint, null, '', true);

        // If retrieval failed, throw error
        if($result['status'] != 200 || !isset($result['body']['result']['data'][0])) {
            $errorMessage = 'There was a problem loading the information.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        $fileData = $result['body']['result']['data'];
        $totalCount = $result['body']['metadata']['pagination']['totalCount'];

        $germplasmFileData = $this->paginate($fileData, $entity);

        return [
            'data' => $germplasmFileData,
            'columns' => $columns,
            'totalCount' => $totalCount
        ];
    }
}