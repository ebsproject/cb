<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\germplasm\models;

use Yii;

use yii\web\UploadedFile;

use app\models\Program;
use app\models\Crop;
use app\models\Config;
use app\models\Variable;
use app\models\BaseModel;
use app\models\CropProgram;

use app\interfaces\models\IProgram;
use app\interfaces\models\ICrop;
use app\interfaces\models\ICropProgram;
use app\interfaces\models\IConfig;
use app\interfaces\models\IVariable;
use app\interfaces\modules\germplasmModule\models\ICreateModel;

use app\modules\germplasm\models\FileExport;

/**
 * Model for facilitating germplasm creation processes
 */
class CreateModel extends BaseModel implements ICreateModel
{
    // Reference keys to store data
    CONST KEY_VARIABLES = 'gefu-variables';
    CONST KEY_VARIABLES_REQUIRED = 'gefu-variables-required';
    CONST KEY_VARIABLES_OPTIONAL = 'gefu-variables-optional';

    public $programModel;
    public $cropModel;
    public $cropProgramModel;
    public $configModel;
    public $variableModel;
    
    public $uploadedFile;

    public function __construct (
        IProgram $programModel,
        ICrop $cropModel,
        ICropProgram $cropProgramModel,
        IConfig $configModel,
        IVariable $variableModel,
        UploadedFile $uploadedFile
    )
    { 

        $this->programModel = $programModel;
        $this->cropModel = $cropModel;
        $this->cropProgramModel = $cropProgramModel;
        $this->configModel = $configModel;
        $this->variableModel = $variableModel;

        $this->uploadedFile = $uploadedFile;
    }

    // Getter methods
    public function getKeyVariables() { return self::KEY_VARIABLES; }
    public function getKeyVariablesRequired() { return self::KEY_VARIABLES_REQUIRED; }
    public function getKeyVariablesOptional() { return self::KEY_VARIABLES_OPTIONAL; }

    /**
     * Facilitates the retrieval of variable configurations for a given program
     * 
     * @param Integer $programId id of current dashboard program
     * @param Boolean $systemDef if variables to be retrieved are system-defined
     * @param String $abbrev abbrev for variable config; default = empty
     * @return Array validated variables and corresponding data
     */
    public function getVariableConfig($programId,$systemDef = false, $abbrev = ''){

        if(!isset($programId) || empty($programId)){
            return [];
        }
        
        // Retrieve program code
        $programInfo = $this->programModel->getProgram($programId);

        if(!isset($programInfo) || empty($programInfo)){
            return [];
        }

        $cropCode = $this->programModel->getCropCode($programInfo['programCode']);

        if($cropCode == ''){
            return [];
        }

        $configVar = [];
        $identifier = $systemDef ? 'SYSTEM' : strtoupper($cropCode);
        
        // retrieve config
        $configAbbrev = $abbrev == '' ? 'GM_FILE_UPLOAD_VARIABLES_CONFIG_' : $abbrev;

        $config = $this->configModel->getConfigByAbbrev($configAbbrev . $identifier . '_DEFAULT');

        if(!isset($config) || empty($config) || !isset($config['values']) || empty($config['values'])){
            return [];
        }

        // validate config variables
        $validatedVars = $this->validateConfigVariables($config['values']);

        // return processed variables
        $configVar = $validatedVars;

        return $configVar;
    }

    /**
     * Facilitates the verification of variables and extraction of relevant information
     * 
     * @param Array $configVar array of variables extracted from configuration record
     * 
     * @return Array validated variables
     */
    public function validateConfigVariables($configVar){
        $validatedVars = [];
        
        if(isset($configVar) && !empty($configVar)){
            foreach($configVar as $key=>$record){
                if(!empty($record['abbrev'])){
                    $variableInfo = $this->variableModel->getVariableByAbbrev($record['abbrev']);
                    
                    if(isset($variableInfo) && !empty($variableInfo)){
                        $configVar[$key]['abbrev'] = $variableInfo['abbrev'];
                        $configVar[$key]['label'] = $variableInfo['label'];
                        $configVar[$key]['name'] = $variableInfo['name'];

                        $configVar[$key]['variable_info'] = $variableInfo;

                        $validatedVars[] = $configVar[$key];
                    }
                }
            }
        }

        return $validatedVars;
    }

    /**
     * Validates an uploaded file's headers based on config
     * 
     * @param Integer $programId program identifier
     * @param Array $headers column headers of uploaded file
     * @param Object $variableConfig variable configuration record
     * 
     * @return mixed
     */
    public function validateUploadedFile($programId,$headers,$variableConfig){
        $missingCols = [];
        $supportedEntities = ['germplasm', 'germplasm_relation', 'seed', 'package'];

        // build required variables
        $columnHeaders = [
            'required' => [],
            'optional' => []
        ];
        
        foreach($variableConfig as $key => $value){
            if($value['abbrev'] != ''){
                if(isset($columnHeaders[$value['usage']][$value['entity']]) && !empty($columnHeaders[$value['usage']][$value['entity']])){
                    array_push($columnHeaders[$value['usage']][$value['entity']],$value['abbrev']);
                }
                else{
                    $columnHeaders[$value['usage']][$value['entity']] = [$value['abbrev']];
                }
            }
        }

        Yii::$app->session->set($this->getKeyVariablesRequired(), $columnHeaders['required']);
        Yii::$app->session->set($this->getKeyVariablesOptional(), $columnHeaders['optional']);

        $uploadedFile = array_map('strval',$headers);  

        // Check for missing required columns
        foreach($supportedEntities as $entity) {
            if (
                isset($columnHeaders['required'][$entity])
                || isset($columnHeaders['optional'][$entity])
            ) {
                foreach($columnHeaders['required'][$entity] as $key => $val1){
                    if(!in_array($val1,$uploadedFile)){
                        array_push($missingCols,$val1);
                    }
                }
            }
        }

        if(empty($missingCols)){
            return [
                'success' => true,
                'error' => ''
            ];
        }
        else{
            $errorMsg = 'Missing required column header/s ' . implode(',',$missingCols) . '.';
            
            return [
                'success' => false,
                'error' => $errorMsg
            ];
        }        
    }

    /**
     * Facilitate data retrieval for uploaded file
     * 
     * @param String $fileReference reference name of uploaded file
     * 
     * @return Array array of file column headers
     */
    public function retrieveUploadedFile($fileReference){
        $success = true;
        $errorMsg = '';

        try{
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads/';
            $file = $this->uploadedFile->getInstanceByName($fileReference);

            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false,
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $tmpFileName = \Yii::$app->security->generateRandomString() . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $tmpFileName;

            $file->saveAs($tmpFileName);

            $csv = new \app\components\CsvReader($tmpFileName);
            $csv->setHasHeader(true);

            $fileName = $file->name;
            
            $columnHeaders = array_map('strtoupper',$csv->getHeader());
            $content = [];

            if(isset($csv->read()[0])){
                $columnHeaders = array_map('strtoupper',array_keys($csv->read()[0]));
                foreach ($csv as $line) {
                    array_push($content,$line);
                }
            }
            else{
                $success = false;
                $errorMsg = 'Invalid file upload. Please check if file has content data.';
            }

            return [
                'success' => $success,
                'headers' => $columnHeaders,
                'fileContent' => $content,
                'fileName' => $fileName,
                'error' => $errorMsg
            ];
        }
        catch(\Exception $e){
            $errorMsg = $e->getMessage();

            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $errorMsg = 'Please check if file is not empty.';
            }

            if( strpos($e->getMessage(),'The header must be with unique string values.') !== -1){
                $errorMsg .= '. Kindly use the CSV template as reference.';
            }

            return [
                'success' => false,
                'error' => Yii::t('app', $errorMsg)
            ];
        }
    }

    /**
     * Facilitates creation of new germplasm file upload record
     * 
     * @param Integer $programId program id
     * @param String $fileName name of uploaded file
     * @param Array $fileContent uploaded file content
     * @param String $origin abbrev of the application used to upload file
     * @param String $remarks remarks field value of the record to be added
     * @param String $action file upload action: create | update
     * 
     * @return mixed
     */
    public function saveUploadedFile($programId,$fileName,$fileContent, $origin='GERMPLASM_CATALOG', $remarks='', $action='create'){
        $sessReqVars = Yii::$app->session->get($this->getKeyVariablesRequired());
        $sessOptVars = Yii::$app->session->get($this->getKeyVariablesOptional());

        $supportedEntities = ['germplasm', 'germplasm_relation', 'seed', 'package', 'germplasm_attribute'];

        $fileUploadEntity = in_array($remarks,$supportedEntities) ? $remarks : ($remarks == 'seed-package' ? 'seed' : 'germplasm');
        // format file name
        $fileName = trim(str_replace('.csv','',$fileName));

        // format each record
        $fileData = null;
        foreach($fileContent as $key => $record){
            foreach($record as $column => $colVal){
                $column = strtoupper($column);
                // Remove white space (single) from start and end
                $colValTrimmed = trim($colVal);
                // Remove tab spaces, new lines, and carriage returns from start and end
                $colValTrimmed = preg_replace('/^(\\t|\\n|\\r)+|(\\t|\\n|\\r)+$/', '', $colValTrimmed);

                foreach($supportedEntities as $entity) {
                    if( isset($sessReqVars[$entity]) && in_array($column,$sessReqVars[$entity]) ||
                        isset($sessOptVars[$entity]) && in_array($column,$sessOptVars[$entity])){
                        $fileData[$key][$entity][strtolower($column)] = $colValTrimmed;
                    }
                }
            }
        }

        if(!isset($fileData)){
            $fileData = [];
        }

        // form insert parameters
        $params = [
            "records" => [
                [
                    "programDbId" => "$programId",
                    "fileName" => $fileName,
                    "fileStatus" => "in queue",
                    "fileData" => $fileData,
                    "fileUploadOrigin" => $origin,
                    "fileUploadAction" => $action,
                    "remarks" => $remarks,
                    "notes" => "",
                    "entity" => $fileUploadEntity

                ]
            ]
        ];

        // call API resource call
        $method = 'POST';
        $endpoint = 'germplasm-file-uploads';
        $requestBody = json_encode($params);

        $result = \Yii::$app->api->getResponse($method, $endpoint, $requestBody);

        if($result['status'] != 200){
            return [
                'success' => false,
                'error' => 'Error encountered creating file upload record.'
            ];
        }

        $fileUploadId = $result['body']['result']['data'][0]['germplasmFileUploadDbId'];
        
        return [
            'success' => true,
            'error' => '',
            'id' => $fileUploadId
        ];
    }

    /**
     * Facilitates the creation of CSV template file
     * 
     * @param Array $templateHeaders array of column headers for template file
     * @param String $cropCode crop code
     * @param String $programCode program code
     * @param String $fileName pre-defined file name
     */
    public function generateCSVTemplate($templateHeaders,$cropCode,$programCode,$fileName = ''){

        // add headers to template file content
        $templateContent = [
            $templateHeaders,
            []
        ];

        // initialize CSV
        $generateTemplate = new FileExport(FileExport::FILE_UPLOAD_TEMPLATE, FileExport::CSV, time());

        $identifier = !empty($cropCode) ? ( !empty($programCode) ? $cropCode . '_' . $programCode : $cropCode ) : '';
        $fileName = ($fileName == '' ? 'GM_CSV_UPLOAD_TEMPLATE_'. $identifier : $fileName) .'.csv';
        $content = $templateContent;

        $generateTemplate->addFileData($content,$fileName);
        $buffer = $generateTemplate->getUnzipped();
        $content = $buffer[$fileName];

        // Set headers
        header('Content-Type: application/csv');
        header("Content-Disposition: attachment; filename={$fileName};");
    
        // Write to output stream
        $fp = fopen('php://output', 'w');
        
        if (!$fp) {
            die('Failed to create CSV file.');
        }

        foreach ($templateContent as $field) {
            fputcsv($fp, $field);
        }

        exit;
    }

    /**
     * Retrieves the germplasm file upload record given its ID
     * @param Integer $germplasmFileUploadDbId file upload identifier
     */
    public function getFileUploadRecord($germplasmFileUploadDbId) {
        // Get record info
        $result = Yii::$app->api->getResponse('GET', 'germplasm-file-uploads/'.$germplasmFileUploadDbId, null, '', true);

        // If retrieval failed, throw error
        if($result['status'] != 200 || !isset($result['body']['result']['data'][0])) {
            $errorMessage = 'There was a problem retrieving the requested information.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        return $result;
    }
}

?>