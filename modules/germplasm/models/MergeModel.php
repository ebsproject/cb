<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\models;

use Yii;

use yii\web\UploadedFile;

// Import models
use app\models\BaseModel;

// Import interfaces
use app\interfaces\modules\germplasmModule\models\IMergeModel;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\models\IGermplasmFileUpload;

/**
 * Model for facilitating germplasm merge processes
 */
class MergeModel extends BaseModel implements IMergeModel
{
    CONST KEY_VARIABLES = 'ge-merge-variables';
    CONST KEY_VARIABLES_REQUIRED = 'ge-merge-variables-required';
    CONST KEY_VARIABLES_OPTIONAL = 'ge-merge-variables-optional';

    CONST FILE_UPLOAD_CONFIG_ABBREV = 'GM_MERGE_GERMPLASM_';
    CONST TEMPLATE_FILE_NAME = 'GM_MERGE_GERMPLASM_TEMPLATE_DEFAULT';

    public $uploadedFile;
    public $userDashboardConfig;
    public $germplasmFileUploadModel;

    public function __construct(
        UploadedFile $uploadedFile,
        IUserDashboardConfig $userDashboardConfig,
        IGermplasmFileUpload $germplasmFileUploadModel
    )
    {
        $this->uploadedFile = $uploadedFile;
        $this->userDashboardConfig = $userDashboardConfig;
        $this->germplasmFileUploadModel = $germplasmFileUploadModel;
    }

    // Getter methods
    public function getKeyVariables() { return self::KEY_VARIABLES; }
    public function getKeyVariablesRequired() { return self::KEY_VARIABLES_REQUIRED; }
    public function getKeyVariablesOptional() { return self::KEY_VARIABLES_OPTIONAL; }
    
    public function getFileUploadConfigAbbrev() { return self::FILE_UPLOAD_CONFIG_ABBREV; }
    public function getTemplateFileName() { return self::TEMPLATE_FILE_NAME; }

    /**
     * Facilitate data retrieval for uploaded file
     * 
     * @param String $fileReference reference name of uploaded file
     * 
     * @return Array array of file column headers
     */
    public function retrieveUploadedFile($fileReference){
        $success = true;
        $errorMsg = '';

        try{
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads/';
            $file = $this->uploadedFile->getInstanceByName($fileReference);

            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false,
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $tmpFileName = \Yii::$app->security->generateRandomString() . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $tmpFileName;

            $file->saveAs($tmpFileName);
            $csv = new \app\components\CsvReader($tmpFileName);
            $csv->setHasHeader(true);

            $fileName = $file->name;
            
            $columnHeaders = array_map('strtoupper',$csv->getHeader());
            $content = [];

            if(isset($csv->read()[0])){
                $columnHeaders = array_map('strtoupper',array_keys($csv->read()[0]));
                foreach ($csv as $line) {
                    array_push($content,$line);
                }
            }
            else{
                $success = false;
                $errorMsg = 'Invalid file upload. Please check if file has content data.';
            }

            return [
                'success' => $success,
                'headers' => $columnHeaders,
                'fileContent' => $content,
                'fileName' => $fileName,
                'error' => $errorMsg
            ];
        }
        catch(\Exception $e){
            $errorMsg = $e->getMessage();

            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $errorMsg = 'Please check if file is not empty.';
            }

            if( strpos($e->getMessage(),'The header must be with unique string values.') !== -1){
                $errorMsg .= '. Kindly use the CSV template as reference.';
            }

            return [
                'success' => false,
                'error' => Yii::t('app', $errorMsg)
            ];
        }
    }

    /**
     * Validates an uploaded file's headers based on config
     * 
     * @param Integer $programId program identifier
     * @param Array $headers column headers of uploaded file
     * @param Object $variableConfig variable configuration record
     * 
     * @return mixed
     */
    public function validateUploadedFile($programId,$headers,$variableConfig){
        $missingCols = [];

        // build required variables
        $columnHeaders = [
            'required' => [],
            'optional' => []
        ];
        
        foreach($variableConfig as $key => $value){
            if($value['abbrev'] != ''){
                if(isset($columnHeaders[$value['usage']][$value['entity']]) && !empty($columnHeaders[$value['usage']][$value['entity']])){
                    array_push($columnHeaders[$value['usage']][$value['entity']],$value['abbrev']);
                }
                else{
                    $columnHeaders[$value['usage']][$value['entity']] = [$value['abbrev']];
                }
            }
        }

        Yii::$app->session->set($this->getKeyVariablesRequired(), $columnHeaders['required']);
        Yii::$app->session->set($this->getKeyVariablesOptional(), $columnHeaders['optional']);

        $hasGermplasmVariables = isset($columnHeaders['required']['germplasm']) || isset($columnHeaders['optional']['germplasm']);
        $hasSeedVariables = isset($columnHeaders['required']['seed']) || isset($columnHeaders['optional']['seed']);
        $hasPackageVariables = isset($columnHeaders['required']['package']) || isset($columnHeaders['optional']['package']);

        $uploadedFile = array_map('strval',$headers);  

        // check if has all required germplasm columns
        if ($hasGermplasmVariables) {
            foreach($columnHeaders['required']['germplasm'] as $key => $val1){
                if(!in_array($val1,$uploadedFile)){
                    array_push($missingCols,$val1);
                }
            }
        }

        if($hasSeedVariables){
            // check if has all required seed columns
            foreach($columnHeaders['required']['seed'] as $key => $val2){
                if(!in_array($val2,$uploadedFile)){
                    array_push($missingCols,$val2);
                }
            }
        }

        if ($hasPackageVariables) {
            // check if has all required package columns
            foreach($columnHeaders['required']['package'] as $key => $val3){
                if(!in_array($val3,$uploadedFile)){
                    array_push($missingCols,$val3);
                }
            }
        }

        if(empty($missingCols)){
            return [
                'success' => true,
                'error' => ''
            ];
        }
        else{
            $errorMsg = 'Missing required column header/s ' . implode(',',$missingCols) . '.';
            
            return [
                'success' => false,
                'error' => $errorMsg
            ];
        }        
    }

    /**
     * Facilitates creation of new germplasm merge file upload record
     * 
     * @param Integer $programId program id
     * @param String $fileName name of uploaded file
     * @param Array $fileContent uploaded file content
     * @param String $origin abbrev of the application used to upload file
     * @param String $remarks remarks field value of the record to be added
     * @param String $action file upload action: create | update
     * 
     * @return mixed
     */
    public function saveUploadedFile($programId,$fileName,$fileContent, $origin='GERMPLASM_CATALOG', $remarks='', $action='merge'){
        $sessReqVars = Yii::$app->session->get($this->getKeyVariablesRequired());
        $sessOptVars = Yii::$app->session->get($this->getKeyVariablesOptional());

        // format file name
        $fileName = trim(str_replace('.csv','',$fileName));
        
        // format each record
        $fileData = [];

        foreach($fileContent as $key => $record){
            $fileData[$key] = [
                'keep' => [
                    'germplasm_code' => $record['germplasm_code_keep'],
                    'designation' => $record['designation_keep'],
                ],
                'merge' => [
                    'germplasm_code' => $record['germplasm_code_merge'],
                    'designation' => $record['designation_merge'],
                ]
            ];
        }

        if(!isset($fileData)){
            $fileData = [];
        }

        // form insert parameters
        $params = [
            "records" => [
                [
                    "programDbId" => "$programId",
                    "fileName" => $fileName,
                    "fileData" => $fileData,
                    "fileUploadOrigin" => $origin,
                    "fileUploadAction" => $action,
                    "remarks" => $remarks,
                    "notes" => "",
                    "fileStatus" => "created",
                    "entity" => "germplasm"
                ]
            ]
        ];

        // call API resource call
        $method = 'POST';
        $endpoint = 'germplasm-file-uploads';
        $requestBody = json_encode($params);

        $result = \Yii::$app->api->getResponse($method, $endpoint, $requestBody);

        if($result['status'] != 200){
            return [
                'success' => false,
                'error' => 'Error encountered creating file upload record.'
            ];
        }

        $fileUploadId = $result['body']['result']['data'][0]['germplasmFileUploadDbId'];
        
        return [
            'success' => true,
            'error' => '',
            'id' => $fileUploadId
        ];
    }

    /**
     * Add merge conflict resolutions to germplasm file upload transaction data
     * 
     * @param Integer $fileId file identifier
     * @param Array $matchingReports resolved conflict reports
     * 
     * @return mixed
     */
    public function addMergeConflictResolution($fileId,$matchingReports){
        $requestData = [];

        // retrieve file data
        $fileUploadInfo = $this->germplasmFileUploadModel->getOne($fileId);
        $fileData = $fileUploadInfo['data']['fileData'] ?? [];

        // add resolutions to transaction data
        foreach($fileData as $indx => $value){
            $fileData[$indx]['coflict'] = [ 'status' => 'resolved' ];

            if(isset($matchingReports[$indx+1]) && !empty($matchingReports[$indx+1])){
                $fileData[$indx]['coflict']['items'] = $matchingReports[$indx+1];
            }
        }

        // update file data
        return $this->germplasmFileUploadModel->updateOne(
            $fileId, 
            [ 'fileData' => $fileData, 'fileStatus' => 'merge ready' ]
        );
    }
}

?>