<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\models;

use Yii;

use yii\web\UploadedFile;

// Import models
use app\models\BaseModel;
use app\models\GermplasmFileUpload;

use app\interfaces\models\IGermplasmFileUpload;

// Import interfaces
use app\interfaces\modules\germplasmModule\models\IUpdateModel;

/**
 * Model for facilitating germplasm update processes
 */
class UpdateModel extends BaseModel implements IUpdateModel
{
    // global variables
    CONST KEY_VARIABLES = 'gm-global-update-variables';
    CONST KEY_VARIABLES_REQUIRED = 'gm-global-update-variables-required';
    CONST KEY_VARIABLES_OPTIONAL = 'gm-global-update-variables-optional';

    CONST LEVEL = 'GLOBAL';
    CONST ACTION = 'update';

    // config
    CONST TEMPLATE_FILE_NAME = 'GM_UPDATE_GERMPLASM_ATTRIBUTE_TEMPLATE';
    CONST EXCLUDE_VARIABLES = ['GERMPLASM_CODE','DESIGNATION'];

    public $uploadedFile;
    public $germplasmFileUpload;

    public function __construct(
        UploadedFile $uploadedFile,
        IGermplasmFileUpload $germplasmFileUpload
    )
    {
        $this->uploadedFile = $uploadedFile;
        $this->germplasmFileUpload = $germplasmFileUpload;
    }

    // Getter methods
    public function getKeyVariables() { return self::KEY_VARIABLES; }
    public function getKeyVariablesRequired() { return self::KEY_VARIABLES_REQUIRED; }
    public function getKeyVariablesOptional() { return self::KEY_VARIABLES_OPTIONAL; }
    public function getModelAction() { return self::ACTION; }
    public function getModelLevel() { return self::LEVEL; }
    public function getTemplateFileName() { return self::TEMPLATE_FILE_NAME; }
    public function getExcludeVariables() { return self::EXCLUDE_VARIABLES; }

    /**
     * Retrieve file data summary for file upload transaction record
     * 
     * @param Array $fileData file data content
     * @param String $action action in use
     * @param Array $configValues configuration values
     * 
     * @return mixed
     */
    public function getFileDataSummary($fileData,$action,$configValues = []){
        // retrieve config variable in summary
        $configVariables = array_column(array_filter($configValues,function($variable){
            return isset($variable['in_summary']) ? $variable['in_summary'] == 'true' : true; 
        }),'abbrev');

        // retrieve germplasm-level attributes from file data
        $headers = [];
        if(isset($fileData[0]['germplasm']) && !empty($fileData[0]['germplasm'])){
            // check if in excluded variables
            $headers = array_diff(
                array_map('strtoupper', array_unique(array_keys($fileData[0]['germplasm']))),
                self::EXCLUDE_VARIABLES) ?? [];

            // check if in identified in_summary
            $headers = array_filter($headers,function($header) use ($configVariables){
                return in_array($header,$configVariables) ; 
            });
        }
        
        // extract count values
        $attributeCount = count($headers) ?? 0;
        $rowCount = count($fileData) ?? 0;

        return [
            'attributes' => $headers,
            'attributeCount' => $attributeCount,
            'rowCount' => $rowCount
        ];
    }

    /**
     * Validates an uploaded file's headers based on config
     *
     * @param Integer $programId program identifier
     * @param Array $headers column headers of uploaded file
     * @param Object $variableConfig variable configuration record
     *
     * @return mixed
     */
    public function validateUploadedFile($programId,$headers,$variableConfig){
        $missingCols = [];

        // get required variables
        $columnHeaders = $this->germplasmFileUpload->getFileUploadVariables($variableConfig);

        $uploadedFile = array_map('strval',$headers);

        // check if has all required germplasm columns
        if ($columnHeaders) {
            foreach($columnHeaders['requiredVariables'] as $key => $val1){
                if(!in_array($val1,$uploadedFile)){
                    array_push($missingCols,$val1);
                }
            }
        }

        Yii::$app->session->set($this->getKeyVariablesRequired(), array_flip($columnHeaders['requiredVariables']));
        Yii::$app->session->set($this->getKeyVariablesOptional(), array_flip($columnHeaders['variables']));

        if(empty($missingCols)){
            return [
                'success' => true,
                'error' => ''
            ];
        }
        else{
            $errorMsg = 'Missing required column header/s ' . implode(',',$missingCols) . '.';

            return [
                'success' => false,
                'error' => $errorMsg
            ];
        }
    }

    /**
     * Facilitates creation of new germplasm merge file upload record
     *
     * @param Integer $programId program id
     * @param String $fileName name of uploaded file
     * @param Array $fileContent uploaded file content
     * @param String $origin abbrev of the application used to upload file
     * @param String $remarks remarks field value of the record to be added
     * @param String $action file upload action: create | update
     *
     * @return mixed
     */
    public function saveUploadedFile($programId,$fileName,$fileContent, $origin='GERMPLASM_CATALOG', $remarks='', $action='update'){
        $sessReqVars = Yii::$app->session->get($this->getKeyVariablesRequired());
        $sessOptVars = Yii::$app->session->get($this->getKeyVariablesOptional());

        // format file name
        $fileName = trim(str_replace('.csv','',$fileName));

        // format each record
        $fileData = [];

        foreach($fileContent as $key => $record){
            foreach($record as $column => $colVal){
                $column = strtoupper($column);
                // Remove white space (single) from start and end
                $colValTrimmed = trim($colVal);
                // Remove tab spaces, new lines, and carriage returns from start and end
                $colValTrimmed = preg_replace('/^(\\t|\\n|\\r)+|(\\t|\\n|\\r)+$/', '', $colValTrimmed);

                if(isset($sessReqVars[$column]) || isset($sessOptVars[$column])) {
                    $fileData[$key]['germplasm'][strtolower($column)] = $colValTrimmed;
                }
            }
        }

        if(!isset($fileData)){
            $fileData = [];
        }

        // Build param body
        $paramRequest = [
            "fileName" => $fileName,
            "fileData" => $fileData,
            "programDbId" => "$programId",
            "fileUploadOrigin" => $origin,
            "fileUploadAction" => $action,
            "fileStatus" => "in queue",
            "entity" => "germplasm_attribute",
            "remarks" => "germplasm_attribute"
        ];

        // create new file upload record
        $result = $this->germplasmFileUpload->saveUploadedFile($paramRequest);

        return $result;
    }
}

?>