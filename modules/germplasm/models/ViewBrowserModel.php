<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\models;

use Yii;

use app\models\GermplasmFileUpload;

// Import interfaces
use app\interfaces\models\IConfig;
use app\interfaces\models\ICrop;
use app\interfaces\models\IProgram;
use app\interfaces\models\IUserDashboardConfig;

// Import data providers
use app\dataproviders\ArrayDataProvider;

// Import models
use app\models\UserDashboardConfig;
use app\models\Person;

/**
 * Model for facilitating displaying the preview of the file upload data
 */
class ViewBrowserModel extends GermplasmFileUpload
{
    public $config;
    public $crop;
    public $program;
    public $person;
    public $germplasmFileUpload;

    const GERMPLASM_MGR_TYPE = ['germplasm', 'germplasm_relation'];

    /**
     * Model constructor
     */
    public function __construct(
        IConfig $config,
        ICrop $crop,
        IProgram $program,
        Person $person,
        GermplasmFileUpload $germplasmFileUpload,
        public IUserDashboardConfig $userDashboardConfig
    ) {
        $this->config = $config;
        $this->crop = $crop;
        $this->program = $program;
        $this->person = $person;
        $this->germplasmFileUpload = $germplasmFileUpload;
    }

    /**
     * Retrieves the file upload record given its ID
     * @param Integer $fileUploadDbId file upload identifier
     */
    public function getFileUploadRecord($fileUploadDbId)
    {
        // Get record
        $result = $this->searchAll([
            "germplasmFileUploadDbId" => "equals $fileUploadDbId"
        ]);

        // If retrieval failed, throw error
        if ($result['status'] != 200) {
            $errorMessage = 'There was a problem loading the information.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        return $result['data'] ?? [];
    }

    /**
     * Retrieves the configs and formats those into grid columns
     * @param Integer $programDbId program identifier
     * @param String $action file upload source action
     * @param String $type file upload type
     */
    public function getColumns($programDbId, $action = 'create', $type = 'germplasm')
    {
        $columns = [];
        $configs = [];

        // Get crop code
        $cropCode = $this->getCropCode($programDbId);

        $action = !isset($action) || empty($action) ? 'FILE_UPLOAD_VARIABLES' : strtoupper($action) . '_' . strtoupper($type);
        $sourceConfigAbbrev = 'GM_' . $action . '_CONFIG_SYSTEM_DEFAULT';

        if (strtolower($action) == 'merge_germplasm') {
            $sourceConfigAbbrev = 'GM_' . $action . '_SYSTEM_DEFAULT';
        }

        if (strtolower($action) == 'create_germplasm') {
            $sourceConfigAbbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT';
        }

        // Retrieve config values for update germplasm
        if (strtolower($action) == 'update_germplasm') {
            $program = $this->getProgramCode($programDbId);
            [$userRole, $isAdmin, $userId] = $this->person->getPersonRoleAndType($program);
            $abbrev = $this->germplasmFileUpload->getConfigValues('germplasm', 'update', $userRole, $program, $cropCode);
        }

        if ($type == 'germplasm_relation') {
            $sourceConfigAbbrev = 'GM_' . $action . '_SYSTEM_DEFAULT';
        }

        // Retrieve config
        $result = $this->config->getConfigByAbbrev($sourceConfigAbbrev);
        if (isset($result['values']) && !empty($result['values'])) {
            $configs[] = $result['values'] ?? [];
        }

        // Build config
        if ($cropCode !== '') {
            //Build config abbrev
            // CropProgAbbrev
            $cropProgramConfigAbbrev = 'GM_' . $action . '_CONFIG_' . strtoupper($cropCode) . '_DEFAULT';

            // Build config abbrev for germplasm relation types 
            if ($type == 'germplasm_relation') {
                $cropProgramConfigAbbrev = 'GM_' . $action . '_' . strtoupper($cropCode) . '_DEFAULT';
            }

            // Build config abbrev for update tab only
            if ($type == 'germplasm' && $action == 'UPDATE_GERMPLASM') {
                $cropProgramConfigAbbrev = $this->germplasmFileUpload->buildConfigAbbrev('germplasm', 'update', $abbrev['level'], $abbrev['levelValue']);
            }

            // Retrieve config values based on built config abbrevs
            $result = $this->config->getConfigByAbbrev($cropProgramConfigAbbrev);
            if (isset($result['values']) && !empty($result['values'])) {
                $configs[] = $result['values'] ?? [];
            }
        }

        if (empty($configs)) {
            // TEMPORARY! TODO: Refactor process
            $sourceConfigAbbrev = '';
            if (strtolower($action) == 'update_germplasm') {
                $sourceConfigAbbrev = 'GM_GLOBAL_GERMPLASM_UPDATE_CONFIG';
            }

            if (strtolower($action) == 'merge_germplasm') {
                $sourceConfigAbbrev = 'GM_MERGE_GERMPLASM_CONFIG_SYSTEM_DEFAULT';
            }

            $result = $this->config->getConfigByAbbrev($sourceConfigAbbrev);
            if (isset($result['values']) && !empty($result['values'])) {
                $configs[] = $result['values'];
            }
        }

        // Build columns
        foreach ($configs as $config) {
            foreach ($config as $item) {
                if ($item['abbrev'] == 'PROGRAM') {
                    $item['abbrev'] = 'PROGRAM_CODE_P';
                    $item['name'] = "Program (Package)";
                } else if ($item['abbrev'] == 'PROGRAM_CODE') {
                    $item['abbrev'] = 'PROGRAM_CODE_S';
                    $item['name'] = "Program (Seed)";
                } else if (strtolower($action) == 'merge_germplasm') {
                    $item['abbrev'] = $item['column_header'];
                }
                $columns[] = [
                    'attribute' => strtolower($item['abbrev']),
                    'label' => $item['name'] ?? $item['abbrev'],
                    'hAlign' => 'left',
                    'vAlign' => 'top',
                    'mergeHeader' => true
                ];
            }
        }

        return $columns;
    }

    /**
     * Retrieve crop code
     * 
     * @param Integer $programDbId current dashboard program id
     */
    public function getCropCode($programDbId)
    {
        // Retrieve program code
        $programInfo = $this->program->getProgram($programDbId);

        // Validation for program Info
        if (isset($programInfo) && !empty($programInfo)) {
            $cropCode = $this->program->getCropCode($programInfo['programCode']);

            return $cropCode ?? '';
        } else {
            return '';
        }
    }

    /**
     * Retrieve program code
     * 
     * @param Integer $programDbId current dashboard program id
     */
    public function getProgramCode($programDbId)
    {
        $programInfo = $this->program->getProgram($programDbId);
        return $programInfo['programCode'] ?? '';
    }

    /**
     * Formats the file data into an array
     * @param Object $originalFileData original value of the file data column
     */
    public function formatFileData($originalFileData)
    {
        $formatted = [];

        // Loop through rows
        foreach ($originalFileData as $row) {
            $rowValues = [];
            foreach ($row as $entity => $values) {
                // format values for keep and merge germplasm columns
                if (in_array($entity, ['keep', 'merge'])) {
                    foreach ($values as $key => $val) {
                        $values[$key . '_' . (strtolower($entity))] = $val;
                        unset($values[$key]);
                    }
                }

                $rowToAdd = is_array($values) ? $values : $row;
                $rowValues = array_merge($rowValues, $rowToAdd);
            }
            if (!empty($rowValues['program'])) {
                $rowValues['program_code_p'] = $rowValues['program'];
                unset($rowValues['program']);
            }
            if (!empty($rowValues['program_code'])) {
                $rowValues['program_code_s'] = $rowValues['program_code'];
                unset($rowValues['program_code']);
            }

            $formatted[] = $rowValues;
        }

        return $formatted;
    }

    /**
     * Perform pagination operation on the file data array
     * to account for the browser page size and current page number.
     * @param Array $fileData file data array
     * @param String $browserId browser grid id
     */
    public function paginate($fileData, $browserId)
    {
        $getParams = \Yii::$app->request->getQueryParams();

        // Pagination
        $page = 1;

        if (isset($getParams['page'])) {
            $page = intval($getParams['page']);
        }

        if (isset($getParams[$browserId . '-page'])) {
            $page = intval($getParams[$browserId . '-page']);
        }

        // Page limit
        $perPage = (int) $this->userDashboardConfig->getDefaultPageSizePreferences();

        // If current page is over the total number of pages, return to page 1
        if ($page > ceil(count($fileData) / $perPage)) {
            $page = 1;
            $_GET['page'] = 1;
        }

        return array_slice($fileData, ($perPage * $page - $perPage), $perPage);
    }

    /**
     * Retrieves the info for the file data preview
     * @param Integer $fileUploadDbId file upload identifier
     * @param Integer $programDbId program identifier
     * @param String $action file upload source action
     */
    public function search($fileUploadDbId, $programDbId, $action = '')
    {
        // Retrieve file upload record
        $result = $this->getFileUploadRecord($fileUploadDbId);
        $fileUploadRecord = $result[0] ?? [];

        // Extract details
        $fileName = $fileUploadRecord['fileName'] ?? 'Unknown file';
        $fileStatus = !empty($fileUploadRecord['fileStatus']) ? ucfirst($fileUploadRecord['fileStatus']) : 'Unknown status';
        $fileUploadAction = $fileUploadRecord['fileUploadAction'] ?? 'create';
        $uploader = $fileUploadRecord['uploader'] ?? 'Unknown uploader';
        $uploadTimestamp = $fileUploadRecord['uploadTimestamp'] ?? 'Unknown timestamp';
        $fileData = $fileUploadRecord['fileData'] ?? [];
        $remarks = $fileUploadRecord['remarks'] ?? '';
        $type = isset($remarks) && !empty($remarks) ? $remarks : 'germplasm';
        $totalCount = count($fileData);

        if ($action == 'coding') {
            // Retrieve germplasm.file_upload_germplasm records
            $fileData = $this->getFileUploadEntityData($fileUploadDbId, 'germplasm_name');
            $totalCount = count($fileData);
        }

        // TEMPORARY MEASURE!
        // if germplasm file upload remarks do not 
        // contain acceptable germplasm manager file type
        // default type to germplasm
        $type = in_array($type, self::GERMPLASM_MGR_TYPE) ? $type : 'germplasm';

        // Perform pagination
        $browserId = "dynagrid-gm-fu-preview-grid-$fileUploadAction-$remarks";
        $fileData = $this->paginate($fileData, $browserId);

        // Format file data
        $formattedFileData = $this->formatFileData($fileData);

        // Build columns
        if ($remarks == "germplasm") $fileUploadAction = '';
        $columns = $this->getColumns($programDbId, $fileUploadAction, $type);

        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'id' => $browserId,
            'allModels' => $formattedFileData,
            'restified' => true,
            'totalCount' => $totalCount
        ]);

        return [
            "fileName" => $fileName,
            "fileStatus" => $fileStatus,
            "uploader" => $uploader,
            "uploadTimestamp" => $uploadTimestamp,
            "dataProvider" => $dataProvider,
            "columns" => $columns,
            "browserId" => $browserId,
            "action" => $fileUploadRecord['fileUploadAction'] ?? 'create',
            "type" => $type,
        ];
    }

    /**
     * Retrieve file upload transaction data records
     * 
     * @param Integer $fileUploadDbId transaction ID
     * @param String $entity file upload data entity
     * @param String $dataLevel level of data to be search
     * 
     * @return mixed
     */
    public function getFileUploadEntityData($fileUploadDbId, $entity,  $dataLevel = 'data')
    {
        $endpoint = 'germplasm-file-uploads/' . $fileUploadDbId . '/' . $dataLevel . '-search';
        if (isset($entity) && !empty($entity)) {
            $endpoint .= '?entity=' . $entity;
        }

        $result = Yii::$app->api->getResponse(
            'POST',
            $endpoint,
            null,
            '',
            true
        );

        // If retrieval failed, throw error
        if ($result['status'] != 200 || !isset($result['body']['result']['data'][0])) {
			Yii::$app->session->setFlash('error', "There was a problem loading the information.");
        }

        $result = $result['body']['result']['data'] ?? [];

        return $result;
    }
}
