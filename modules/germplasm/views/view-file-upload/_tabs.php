<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders file upload data tabs
 */

use yii\helpers\Url;

use app\models\UserDashboardConfig;

// Initialize tab redirect url
$currentUrl = '/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
$recordsBrowserUrl = Url::to(['view-file-upload/view-records', 'germplasmFileUploadDbId' => $germplasmFileUploadDbId]);


// Initialize active tab status
$germplasmTabStatus = $entity == 'germplasm' ? 'active' : '';
$seedTabStatus = $entity == 'seed' ? 'active' : '';
$packageTabStatus = $entity == 'package' ? 'active' : '';

?>

<div class="row" style="margin-bottom:0px;">
    <!-- File Upload Records Tabs -->
    <ul id="tabs" class="tabs">
        <?php
            echo '  <li class="tab col step s3 hm-tool-tab" id="germplasm-tab" link="' . $recordsBrowserUrl . '" controller="germplasm">
                        <a class="germplasm-tab a-tab ' . $germplasmTabStatus . '" href="' . $recordsBrowserUrl . '&entity=germplasm">' . \Yii::t('app', 'Germplasm') . '</a>
                    </li>' . 
                 '  <li class="tab col step s3 hm-tool-tab" id="seed-tab" link="' . $recordsBrowserUrl . '" controller="seed">
                        <a class="seed-tab a-tab ' . $seedTabStatus . '" href="' . $recordsBrowserUrl . '&entity=seed">' . \Yii::t('app', 'Seed') . '</a>
                    </li>' .
                 '  <li class="tab col step s3 hm-tool-tab" id="package-tab" link="' . $recordsBrowserUrl . '" controller="package">
                        <a class="package-tab a-tab ' . $packageTabStatus . '" href="' . $recordsBrowserUrl . '&entity=package">' . \Yii::t('app', 'Package') . '</a>
                    </li>';
        ?>
    </ul>
</div>

<?php

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');

// Check if browser Id is empty (occurrence tab)
$browserId = empty($browserId) ? 'none' : $browserId;

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ". json_encode($browserId) .",
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

?>
