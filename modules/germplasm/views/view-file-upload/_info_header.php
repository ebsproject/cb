<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders file upload data information
 */

?>
<div class="col-md-12" align="right" style="margin: 0px; padding: 10px;">
    <button class="btn btn-default pull-right lighten-4" id="gfu-return-btn" type="button" style="margin-bottom:5px;">
        <?php echo \Yii::t('app', 'Return'); ?>
    </button>

    <!-- Header -->
    <div class="gm-browser-title pull-left">
        <h3 class="gm-browser-title"><?php echo \Yii::t('app', 'File Upload') . ' » ' . $fileUploadInfo['fileName']; ?></h3>
    </div>
</div>

    

<div class="row" style="margin-bottom:0px;">
    <!-- File upload summary -->
    <?php
        $fileStatus = strtoupper($fileUploadInfo['fileStatus']);
        $fileStatusDisplay = '';

        switch($fileStatus){
            case 'IN QUEUE':
                $fileStatusDisplay = '<span title="Queued for validation" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'IN QUEUE') . '</strong></span>';
                break;
            case 'VALIDATION IN PROGRESS':
                $fileStatusDisplay = '<span title="Ongoing validation of transaction" class="new badge orange darken-2"><strong>' . \Yii::t('app', 'VALIDATION IN PROGRESS') . '</strong></span>';
                break;
            case 'VALIDATED':
                $fileStatusDisplay = '<span title="Validation completed" class="new badge blue darken-2"><strong>' . \Yii::t('app', 'VALIDATED') . '</strong></span>';
                break;
            case 'VALIDATION FAILED':
                $fileStatusDisplay = '<span title="Validation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'VALIDATION FAILED') . '</strong></span>';
                break;
            case 'VALIDATION ERROR':
                $fileStatusDisplay = '<span title="Validation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'VALIDATION FAILED') . '</strong></span>';
                break;
            case 'CREATION IN PROGRESS':
                $fileStatusDisplay = '<span title="Ongoing creation of records" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'CREATION IN PROGRESS') . '</strong></span>';
                break;
            case 'CREATION FAILED':
                $fileStatusDisplay = '<span title="Creation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'CREATION FAILED') . '</strong></span>';
                break;
            case 'COMPLETED':
                $fileStatusDisplay =  '<span title="Creation completed" class="new badge green darken-2"><strong>' . \Yii::t('app', 'COMPLETED') . '</strong></span>';
                break;
            default:
                $fileStatusDisplay = '<span title="Status unknown" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
        }
        
        echo '<div id="gefu-detail-div" style="margin-left:20px;margin-bottom:0px" >
            <div class="col-md-4">
                <dl><dt title="File Status">File Status</dt>
                    <dd>' . $fileStatusDisplay . '</dd>
                </dl>
            </div>
            <div class="col-md-4">
                <dl>
                    <dt title="Uploader">Uploader</dt>
                    <dd>' . $fileUploadInfo['uploader'] . '</dd>
                </dl>
            </div>
            <div class="col-md-4">
                <dl>
                    <dt title="Upload Timestamp">Upload Timestamp</dt>
                    <dd>' . $fileUploadInfo['uploaderTimestamp'] . '</dd>
                </dl>
            </div>
        </div>';
    ?>
</div>
