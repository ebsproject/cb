<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders created germplasm records for the transaction
 */

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;

Yii::$app->view->registerCss('
    div.gm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.gm-browser-title {
        display: inline;
    }
');

/**
 * Renders Create tab for Germplasm
 */

echo Yii::$app->controller->renderPartial('@app/modules/germplasm/views/default/tabs.php',[
    'controller' => 'create',
    'program' => $program
]);

// Initialize tab redirect url
$currentUrl = '/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;

?>

<div class="panel panel-default" style="min-height: 500px; overflow-x: hidden;">
    <?php
        
        /**
         * Renders Germplasm File Upload Information
         */
        echo Yii::$app->controller->renderPartial('@app/modules/germplasm/views/view-file-upload/_info_header.php',[
            'fileUploadInfo' => $fileUploadInfo,
            'germplasmFileUploadDbId' => $germplasmFileUploadDbId
        ]);

        /**
         * Renders Germplasm File Upload Tabs
         */
        echo Yii::$app->controller->renderPartial('@app/modules/germplasm/views/view-file-upload/_tabs.php',[
            'entity' => 'germplasm',
            'germplasmFileUploadDbId' => $germplasmFileUploadDbId,
            'browserId' => $browserId
        ]);

        $columns = $columns ?? [];
        $dataProvider = $dataProvider ?? [];

        // Render germplasm grid
        echo '<div id="germplasm-browser-container" style="padding-left:15px; padding-right:15px;">';
        $dynagrid = DynaGrid::begin([
            'columns'=>$columns,
            'storage'=>DynaGrid::TYPE_SESSION,
            'theme'=>'simple-default',
            'showPersonalize' => true,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'tableOptions'=>['class'=>'gm-file-upload-germplasm-grid-table'],
                'options'=>['id'=>'gm-file-upload-germplasm-grid-table-con'],
                'id' => 'gm-file-upload-germplasm-grid-table-con',
                'striped'=>false,
                'responsive' => true,
                'responsiveWrap' => false,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                ],
                'hover' => true,
                'floatOverflowContainer' => true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options' => ['id' => 'gm-file-upload-germplasm-grid', 'enablePushState' => true,],
                    'beforeGrid'=>'',
                    'afterGrid'=>''
                ],
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
                'rowOptions'=> [],
                'panel'=> [
                    'heading'=> false,
                    'before' => 'Below displays the germplasm information of the file upload transaction. {summary}',
                    'after'=> false,
                ],
                'toolbar' =>  [
                    [
                        'content' => ''
                            . Html::a(
                                '<i class="glyphicon glyphicon-repeat"></i>',
                                [Yii::$app->controller->action->id . '?germplasmFileUploadDbId=' . $germplasmFileUploadDbId . '&entity=germplasm'],
                                [
                                    'class' => 'btn btn-default gm-tooltipped',
                                    'id' => 'gm-view-germplasm-reset-btn',
                                    'style' => "margin-left:10px;",
                                    'data-position' => 'top',
                                    'data-tooltip' => \Yii::t('app', 'Reset grid'),
                                    'data-pjax' => true
                                ]
                            )
                            . '{dynagridFilter}{dynagridSort}{dynagrid}'

                    ],
                ],
            ],
            'options'=>[
                'id'=>'gm-file-upload-germplasm-grid'
            ]
        ]);
        DynaGrid::end();
        echo '</div>';
    ?>
</div>

<?php

$urlRedirectToMainCreatePage = Url::to(['create/redirect-to-main-create-page']);

$script = <<<JS

    var germplasmFileUploadDbId = $germplasmFileUploadDbId;

    $(document).ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        /**
         * On return button click event
         */
        $(document).on('click', '#gfu-return-btn', function(e) {
            // Redirect
            $.ajax({
                url: '$urlRedirectToMainCreatePage',
                type: 'post',
                data:{},
                success: function(response){ }
            });
        });
    });
JS;

$this->registerJs($script);
?>
