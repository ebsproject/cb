<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Modal;

// import browser helpers
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

// import external models and components
use app\components\DownloadFileDataCSV;
use app\components\FileUploadSummaryWidget;
use app\components\FileUploadErrorLogsWidget;
use app\modules\fileUpload\models\FileUploadHelper;

$browserId = !isset($browserId) || empty($browserId) ? 'dynagrid-germplasm-gefu-grid' : $browserId;

// urls
$notificationsUrl = Url::to(['/germplasm/default/push-notifications','module' => $module]);
$newNotificationsUrl = Url::toRoute(['/germplasm/default/new-notifications-count','module' => $module]);
$resetGridUrl = Url::to(["/germplasm/".$action."/index", 'program'=>$program]);
$renderFileUploadFormUrl = Url::to(["/germplasm/default/render-file-upload"]);

// action button status variables
const VALID_COMPLETE_STATUS = ['merge ready', 'validated', 'coding ready'];
CONST VALID_DELETE_STATUS = ['created','validation error','validated','merge ready'];
CONST VALID_DOWNLOAD_STATUS = ['validation error','merge ready','completed'];
const VALID_ERROR_LOG_STATUS = ['validation error', 'creation failed', 'update failed', 'merge failed', 'coding failed'];

// action buttons
$defaultActionButtons = [
    'merge' => function ($url, $model) use ($action) {
        if($action == 'merge' && $model['fileStatus'] == 'created'){
            return Html::a('<i class="material-icons">merge</i>',
                '#',
                [
                    'id' => 'validate-gefu-merge',
                    'class' => 'validate-gefu-merge',
                    'title' => 'Validate Merge Transaction',
                    'data-entity_id' => $model['germplasmFileUploadDbId'],
                    'file_program_code' => $model['programCode']
                ]
            );
        } 
    },
    'complete' => function ($url, $model) use ($action){
        if(in_array($model['fileStatus'],VALID_COMPLETE_STATUS)){
            return Html::a('<i class="material-icons">check_circle</i>',
                '#',
                [
                    'class' => 'complete-gefu-'.strtolower($action),
                    'id' => 'complete-gefu-'.strtolower($action),
                    'title' => 'Complete '. ucfirst(strtolower($action)) .' Transaction',
                    'data-entity_id' => $model['germplasmFileUploadDbId'],
                    'data-entity_name' => $model['fileName'],
                    'file_program_code' => $model['programCode']
                ]
            );
        }                
    },
    'delete' => function ($url, $model) use ($action){
        if(in_array($model['fileStatus'],VALID_DELETE_STATUS)){
            return Html::a('<i class="material-icons">delete</i>',
                '#',
                [
                    'id' => 'delete-file-upload',
                    'class' => 'delete-file-upload',
                    'title' => 'Delete '. ucfirst(strtolower($action)) .' Transaction',
                    'data-entity_id' => $model['germplasmFileUploadDbId'],
                    'data-toggle' => "modal",
                    'data-target' => "#file-upload-confirm-delete-modal"
                ]
            );
        }        
    },
    'view' => function ($url, $model) use ($resetGridUrl, $abbrevPrefix, $action) {
        if ($model['fileStatus'] == 'completed' && $action != 'coding') {
            return FileUploadSummaryWidget::widget([
                "sourceToolName" => "Germplasm",
                "fileDbId" => $model['germplasmFileUploadDbId'],
                "returnUrl" => $resetGridUrl,
                "configAbbrevPrefix" => $abbrevPrefix,
                "entity" => "germplasm_attribute"
            ]);
        }
    },
    'errors' => function ($url, $model) use ($resetGridUrl, $action){
        if (in_array($model['fileStatus'], VALID_ERROR_LOG_STATUS)) {
            return FileUploadErrorLogsWidget::widget([
                "sourceToolName" => "Germplasm",
                "fileDbId" => $model['germplasmFileUploadDbId'],
                "returnUrl" => $resetGridUrl
            ]);
        }
    },
    'download' => function ($url, $model) use ($resetGridUrl, $action, $abbrevPrefix) {
        if(in_array($model['fileStatus'], VALID_DOWNLOAD_STATUS) && $action != "coding"){
            return DownloadFileDataCSV::widget([
                "sourceToolName" => "Germplasm",
                "fileDbId" => $model['germplasmFileUploadDbId'],
                "returnUrl" => "none",
                "configAbbrevPrefix" => $abbrevPrefix
            ]);
        }
    },
    // TEMPORARY! Preview is not yet supported. Will be updated next sprint.
    // 'preview' => function ($url, $model) use ($action) {
    //     if($action == 'coding' && $model['fileStatus'] == 'coding ready'){
    //         return Html::a('<i class="material-icons">pageview</i>',
    //             '#',
    //             [
    //                 'id' => 'preview-coding',
    //                 'class' => 'preview-coding',
    //                 'title' => 'Preview '. ucfirst(strtolower($action)) .' Transaction',
    //             ]
    //         );
    //     } 
    // },
];

$actionButtons = $defaultActionButtons;
if(isset($additionalActionButtons) && !empty($additionalActionButtons)){
    $actionButtons = array_merge($defaultActionButtons,$additionalActionButtons);
}

// initiate data browser columns
$defaultColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'template' => '{preview} {merge} {complete} {delete} {download} {view} {errors}',
        'buttons' => $actionButtons,
        'vAlign' => 'top',
        'header' => false,
        'noWrap' => true,
        'visible' => false
    ],
    [
        'attribute'=>'fileStatus',
        'label' => ($action=="coding") ? "Coding Status" : "File Status",
        'contentOptions'=>[
            'class' => 'file-upload-status-col'
        ],
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $fileUploadStatusArr,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'multiple'=>false, 
                'allowClear' => true
            ],
        ],
        'filterInputOptions' => [
            'placeholder' => 'Select status'
        ],
        'value' => function($model){
            return !empty($model['fileStatus']) ? $model['fileStatus'] : 'unknown';
        },
        'content' => function ($model) {
            $fileStatus = trim($model['fileStatus']);
            $fileStatusStyle = FileUploadHelper::getStatusClass($fileStatus);
            $statusClass = $fileStatusStyle['class'];
            $statusTitle = $fileStatusStyle['title'];

            return  '<span title="'.$fileStatus.'" class="new badge '.$statusClass.' darken-2"><strong>' 
                    . \Yii::t('app', strtoupper($fileStatus)) . '</strong></span>';
        },
        'headerOptions'=>[
            'style' => 'text-align:left'
        ],
        'visible' => false
    ],
    [
        'attribute'=>'fileName',
        'contentOptions'=>[
            'class' => 'file-upload-file-name-col'
        ],
        'value' => function($model){
            return !empty($model['fileName']) ? $model['fileName'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute'=>'uploader',
        'contentOptions'=>[
            'class' => 'gefu-uploader-col'
        ],
        'value' => function($model){
            return !empty($model['uploader']) ? $model['uploader'] : 0;
        },
        'content' => function($model){
            return !empty($model['uploader']) ? $model['uploader'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute'=>'uploadTimestamp',
        'contentOptions'=>[
            'class' => 'gefu-upload-ts-col'
        ],
        'value' => function($model){
            return !empty($model['uploadTimestamp']) ? $model['uploadTimestamp'] : 0;
        },
        'content' => function($model){
            return !empty($model['uploadTimestamp']) ? $model['uploadTimestamp'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
];

// Remove fileName column when tab is on coding
if($action == "coding"){
    unset($defaultColumns[3]);
}

$columns = $defaultColumns;
if(isset($additionalColumns) && !empty($additionalColumns)){
    $columns = array_merge($defaultColumns,$additionalColumns);
}

// toolbar buttons
$templateBtnOptions = '';
$templateMenuIcon = '';

if($action == 'update'){
    $templateMenuIcon = "&emsp;<span class='caret'></span>";

    $templateBtnOptions = "
    <ul id='gm-".strtolower($action)."-template-actions-list' class='dropdown-content'>
        <li
            id='gm-".strtolower($action)."-template-blank-btn' class = 'hide-loading'>" .
            Html::a( Yii::t('app', 'Blank Template') ) . "
        </li>
        <li
            id='gm-template-from-list-btn'>" .
            Html::a( Yii::t('app', 'Template from List') ) . "
        </li>
    </ul>";
}

$templateBtnId = 'download-'.strtolower($action).'-template-btn';
$templateBtn = ( $templateBtn ?? Html::a(
    \Yii::t('app','Template') . $templateMenuIcon, 
    '#!', 
    [
        'data-pjax'=>0, 
        'id' => $templateBtnId, 
        'class' => 'btn btn-default dropdown-trigger', 
        'style' => 'margin-left:10px;',
        'title'=>\Yii::t('app','Template'),
        'data-activates' => 'gm-'.strtolower($action).'-template-actions-list',
        'data-beloworigin' => 'true',
        'data-constrainwidth' => 'false',
    ]) ) . $templateBtnOptions;

$uploadBtnId = 'upload-'.strtolower($action).'-transaction-btn';
$uploadBtn = $uploadBtn ?? Html::a(
    \Yii::t('app','Upload'), 
    '#!', 
    [
        'data-pjax'=>0, 
        'id' => $uploadBtnId, 
        'class' => 'btn btn-default', 
        'style' => 'margin-left:5px;  margin-right:10px;',
        'title'=>\Yii::t('app','Upload')
    ]);

// If the tab is on coding, remove template and upload buttons
if($action == "coding"){
    $templateBtn = '';
    $uploadBtn = '';
}

// initiate data browser
$dynagrid = DynaGrid::begin([
    'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'export' => [
            'showConfirmAlert' => false,
        ],
        'exportConfig' => [],
        'id' => $browserId.'-table-con',
        'tableOptions'=>[
            'class'=>$browserId.'-table'
        ],
        'options'=>[
            'id'=>$browserId.'-table-con'
        ],
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>$browserId.'-id'
            ],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsive' => true,
        'responsiveWrap'=>false,
        'panel'=>[
            'heading' => false,
            'before' => '<br /><div>{summary}<p>'
                        .\Yii::t('app', 
                            'This is the browser for all uploaded '.strtolower($action).' germplasm transactions. 
                            <b>Transactions across all programs are displayed.</b>')
                        . '</p></div>' ,
            'after' => false,
        ],
        'toolbar' =>  [
            [
                'content'=> "" .
                    $templateBtn . '&nbsp;' . 
                    $uploadBtn . '&nbsp;' .
                    Html::a(
                        '<i class="material-icons large">notifications_none</i>
                        <span id="notif-badge-id" class=""></span>',
                        '#',
                        [
                            'id' => 'gefu-'.strtolower($action).'-notifs-btn',
                            'class' => 'btn waves-effect waves-light file-upload-notification-button hm-tooltipped',
                            'data-pjax' => 0,
                            'style' => "overflow: visible !important;",
                            "data-position" => "top",
                            "data-tooltip" => \Yii::t('app', 'Notifications'),
                            "data-activates" => "notifications-dropdown",
                            'disabled' => false,
                        ]
                    ) . 
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [$controllerActionId, 'program'=>$program],
                        [
                            'id' => 'reset-file-upload-grid', 
                            'class' => 'btn btn-default', 
                            'title'=>\Yii::t('app','Reset grid'),
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    ) . '{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
            ($action != "coding") ? '{export}' : ''
        ],
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'resizableColumns'=>true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options'=>[
        'id'=>$browserId
    ]
]);

DynaGrid::end();

echo '</div>';

echo '<!-- Notifications -->
    <div id="notif-container">
        <ul id="notifications-dropdown" class="dropdown-content" 
            style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
        </ul>
    </div>';

// Modal for confirming delete
Modal::begin([
    'id' => 'file-upload-confirm-delete-modal',
    'header' => '<h4>Delete File Upload Transaction</h4>',
    'footer' =>
    Html::a(\Yii::t('app','Cancel'), ['#'],
            [
                'id'=>'file-upload-delete-cancel-btn',
                'data-dismiss'=>'modal'
            ]
        ). '&emsp;'
    . Html::a(\Yii::t('app', 'Confirm'), '#', 
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close',
            'id' => 'file-upload-delete-confirm-btn',
            'title' => \Yii::t('app', 'Proceed')
        ]
    ) . '&emsp;',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="file-upload-confirm-delete-modal-container"></div>';
Modal::end();

$deleteTransactionUrl = Url::to(['/germplasm/default/delete-transaction']);
$downloadTemplateUrl = Url::to(['/germplasm/default/download-template', 'program' => $program]);
$deleteTransactionGermplasmNamesUrl = Url::to(['/germplasm/delete/delete-transaction-germplasm-names', 'program' => $program]);
$browserRefreshUrl = '/index.php/germplasm/'.strtolower($action).'/index?program='.$program;
$templateFromListUrl = Url::to(["/germplasm/transaction-template/select-list",'program'=>$program,'action'=>$module,'entity'=>'germplasm']);

$this->registerJs(<<<JS

    var selectedRecordId

    var resetGridUrl = '$browserRefreshUrl';
    var browserId = '$browserId';
    var templateBtnId = '$templateBtnId';
    var uploadBtnId = '$uploadBtnId';

    var program = '$program';
    var action = '$module';
    
    /**
     * Delete confirmation
     */
    $(document).on('click', '#delete-file-upload', function(e) {
        selectedRecordId = $(this).attr('data-entity_id');

        // Add message to modal body
        $("#file-upload-confirm-delete-modal-container").html('<p style="font-size:115%;">'+
            'This will delete the file upload and its contents. Click CONFIRM to proceed.</p>');

    });

    /**
     * Delete process
     */
    $(document).on('click','#file-upload-delete-confirm-btn',function (e){
        e.stopPropagation();
        e.preventDefault();

        if(selectedRecordId != null){
            // Disable CONFIRM button
            $(this).addClass('disabled');

            // Add progress bar
            $("#file-upload-confirm-delete-modal-container").html(
                '<div class="progress"><div class="indeterminate"></div></div>'
            );

            deleteSelectedTransaction(selectedRecordId);            
        }
    });

    /**
     * Redirect to saved lists page
     */
    $(document).on('click','#gm-template-from-list-btn',function (e){
        e.stopPropagation();
        e.preventDefault();

        window.location.href =  "$templateFromListUrl";
    });

    /**
     * On grid reset button click event
     */
    $(document).on('click', '#reset-file-upload-grid', function(e) {
        // refresh browser
        $.pjax.reload({
            container: '#'+browserId+'-pjax',
            url: resetGridUrl
        });
    });

    function deleteSelectedTransaction(selectedRecordId){

        if(action == 'coding'){
            deleteActionTransactionData(selectedRecordId,action)
        }
        // Perform delete
        $.ajax({
            url: '$deleteTransactionUrl',
            type: 'post',
            data:{
                fileUploadId: selectedRecordId
            },
            success: function(response){
                var res = JSON.parse(response)

                // enable delete buttons
                $('#gefu-delete-proceed-btn').removeClass('disabled');
                $('#file-upload-delete-confirm-btn').removeClass('disabled');

                if(!res.success){
                    $("#file-upload-confirm-delete-modal.in").modal('hide');

                    var notif = "<i class='material-icons red-text'>close</i>&nbsp;"+
                                "An error occurred. The file upload transaction was not deleted.";
                    Materialize.toast(notif, 3500);
                }
                else{
                    // Hide modal
                    $("#file-upload-confirm-delete-modal.in").modal('hide');

                    // Display toast message
                    var notif = "<i class='material-icons green-text'>check</i>&nbsp;"+
                                "The file upload transaction was successfully deleted.";
                    Materialize.toast(notif, 3500);

                    // refresh browser
                    $.pjax.reload({
                        container: '#'+browserId+'-pjax',
                        url: '$browserRefreshUrl'
                    });
                }
                
            },
            error: function(error){
                $("#file-upload-confirm-delete-modal.in").modal('hide');

                var notif = "<i class='material-icons red-text'>close</i>&nbsp;"+
                            "An error occurred. The file upload transaction was not deleted.";
                Materialize.toast(notif, 3500);
            }
        });
    };

    /**
     * Delete transaction data
     * 
     * @param Integer recordDbId record ID
     * @param String action action
     */
    function deleteActionTransactionData(recordDbId, action){
        if(action == 'coding'){
            $.ajax({
                url: '$deleteTransactionGermplasmNamesUrl',
                type: 'post',
                data:{
                    transactionDbId: ''+recordDbId
                },
            success: function(response){
                
            },
            error: function(error){
                
            }
        });
        }
    }

    function refresh(){
        var maxHeight = ($(window).height() - 280);
        $(".kv-grid-wrapper").css("height",maxHeight);

        $('.collapsible').collapsible();
        $('.dropdown-trigger').dropdown();

        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });
        
        // display and hide loading indicator
        $(document).on('click','.hide-loading',function(){
            $('#system-loading-indicator').css('display','block');
            setTimeout(function() {
                $('#system-loading-indicator').css('display','none');
            },3000);
        });
    }
JS
);
?>