<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Renders browser for germplasm
*/
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\date\DatePicker;
use yii\helpers\Html;
use app\components\FavoritesWidget;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\components\ManageSortsWidget;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use app\models\GermplasmList;
use app\components\QueryWidget;
use app\components\DataBrowser;
use app\widgets\GermplasmInfo;

$resetGridUrl = Url::toRoute(['/germplasm/default/index']);
$listUrl = Url::toRoute(['/account/list','program'=>Yii::$app->userprogram->get('abbrev')]);
$selectAllButtonChecked = ($selectAllMode == 'exclude mode' && empty($selectedIds))? 'checked': '';
$totalResultsCount = $dataProvider->totalCount;
$selectedItemsCount = 'No';
if($selectAllMode == 'include mode') {
    $selectedItemsCount = count($selectedIds) > 0? count($selectedIds): 'No';
} else {    // exclude mode
    $selectedItemsCount = count($selectedIds) > 0? $totalResultsCount - count($selectedIds): $totalResultsCount;
}

echo QueryWidget::widget([
    'configAbbrev' => 'GERMPLASM_QUERY_FIELDS',
    'sessionName' => $sessionName,
    'gridId' => 'germplasm-grid',
    'url' => Url::to(['/germplasm/search/index']),
    'totalCount' => $dataProvider->totalCount,
    'clearSelectionCb' => "true",
    'tool' => 'germplasm_browser'
]);

//all columns of germplasm catalog browser
$defaultColumns = [
    [
        'label'=> "Checkbox",
        'header'=>'
            <input type="checkbox" class="filled-in" id="germplasm-select-all-id" ' . $selectAllButtonChecked . '/>
            <label for="germplasm-select-all-id"></label>
        ',
        'content'=>function($model) use ($selectedIds, $selectAllMode) {
            $checked = '';
            if($selectAllMode == 'include mode') {
                if(in_array($model['germplasmDbId'], $selectedIds)) $checked = 'checked';
            } else {    // exclude mode
                if(!in_array($model['germplasmDbId'], $selectedIds)) $checked = 'checked';
            }
            return '
                <input class="germplasm-grid_select filled-in" type="checkbox" id="'.$model['germplasmDbId'].'" '.$checked.'/>
                <label for="'.$model['germplasmDbId'].'"></label>
            ';
        },
        'hAlign' => 'center',
        'vAlign' => 'top',
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => ' ',
        'template' => '{view} {edit}',
        'visibleButtons' => [
            'edit' => function ($model, $key, $index) {
                if(Yii::$app->session->get('isAdmin')) {
                    return true;
                }
                return false;
            }
        ],
        'buttons' => [
            'view' => function ($url, $model) {
                return GermplasmInfo::widget([
                    'id' => $model['germplasmDbId'],
                    'label' => '<i class="material-icons view-germplasm-btn">remove_red_eye</i>',
                    'entityLabel' => $model['designation'],
                    'linkAttrs' => [
                        'id' => 'view-germplasm-widget-'.$model['germplasmDbId'],
                        'class' => 'blue-text text-darken-4 header-highlight',
                        'title' => \Yii::t('app', 'Click to view more information'),
                        'data-label' => $model['designation'],
                        'data-id' => $model['germplasmDbId'],
                        'data-target' => '#view-germplasm-widget-modal',
                        'data-toggle' => 'modal',
                        'style' => ['font-size' => '1rem']
                    ]
                ]);
            },
            'edit' => function ($url, $model) {
                return Html::a('<i class="material-icons">edit</i>',
                    '#',
                    [
                        'class' => 'edit-germplasm',
                        'title' => 'Edit germplasm',
                        'data-entity_id' => $model['germplasmDbId']
                    ]
                );
            },
        ],
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'designation',
        'label' => 'Germplasm',
        'contentOptions'=>[
            'class' => 'germplasm-name-col'
        ]
    ],
    [
        'attribute'=>'otherNames',
        'contentOptions'=>[
            'class' => 'germplasm-name-col'
        ],
        'value' => function($data) {
            $otherNames = $data['otherNames'];
            $otherNames = str_replace(";", "<br/>", $otherNames);
            return $otherNames;
        },
        'format' => 'raw'
    ],
    [
        'attribute'=>'parentage',
        'contentOptions'=>[
            'class' => 'germplasm-name-col'
        ]
    ],
    [
        'attribute'=>'generation',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'germplasmNameType',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'germplasmType',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'germplasmState',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'germplasmNormalizedName',
        'contentOptions'=>[
            'class' => 'germplasm-name-col'
        ],
        'visible' => false
    ],
    [
        'attribute'=>'germplasmCode',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ],
        'visible' => false
    ],
];

$otherColumns = [
    [
        'attribute'=>'creator',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'creationTimestamp',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ],
		'visible' => false,
		'format' => 'dateTime',
		'filterType' => GridView::FILTER_DATE,
		'filterWidgetOptions' => [
			'pluginOptions'=> [
				'format' => 'yyyy-mm-dd',
				'autoclose' => true,
				'type'=>DatePicker::TYPE_COMPONENT_APPEND,
				'removeButton'=>true,
				'todayHighlight' => true,
				'showButtonPanel'=>true,
				'clearBtn' => true,
				'options'=>[
				'type'=>DatePicker::TYPE_COMPONENT_APPEND,
				'removeButton'=>true,
				]
			],
			'pluginEvents' => [
				"clearDate" => 'function (e) {$(e.target).find("input").change();}',
			],
		]
    ],
    [
        'attribute'=>'modifier',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'modificationTimestamp',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ],
		'visible' => false,
		'format' => 'dateTime',
		'filterType' => GridView::FILTER_DATE,
		'filterWidgetOptions' => [
			'pluginOptions'=> [
				'format' => 'yyyy-mm-dd',
				'autoclose' => true,
				'type'=>DatePicker::TYPE_COMPONENT_APPEND,
				'removeButton'=>true,
				'todayHighlight' => true,
				'showButtonPanel'=>true,
				'clearBtn' => true,
				'options'=>[
				'type'=>DatePicker::TYPE_COMPONENT_APPEND,
				'removeButton'=>true,
				]
			],
			'pluginEvents' => [
				"clearDate" => 'function (e) {$(e.target).find("input").change();}',
			],
		]
    ],
];

// set up columns for export and grid view
$otherGridColumns = [];
foreach ($otherColumns as $key => $value) {
    $visibleFalse = ['visible'=>false];

    $defaultOtherCols = array_merge($value,$visibleFalse);
    $otherGridColumns[] = $defaultOtherCols;
}

$gridColumns = array_merge($defaultColumns, $otherGridColumns);

//dynagrid configuration
$dynagrid = DynaGrid::begin([
    'columns' => $gridColumns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'export' => [
            'showConfirmAlert' => false,
        ],
        'exportConfig' => [
            GridView::CSV => [
                'label' => 'CSV',
                'icon' => '',
                'showHeader' => true,
                'showPageSummary' => false,
                'showFooter' => false,
                'showCaption' => false,
                'filename' => 'Germplasm',
                'config' => [
                    'colDelimiter' => ","
                ],
            ],
            GridView::EXCEL => [
                'label' => 'Excel',
                'icon' => '',
                'showHeader' => true,
                'showPageSummary' => false,
                'showFooter' => false,
                'showCaption' => false,
                'filename' => 'Germplasm',
            ],
        ],
        'id' => 'germplasm-grid-id',
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>['id'=>'germplasm-grid-id'],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            'heading'=>'<h3>'.\Yii::t('app', 'Germplasm') .FavoritesWidget::widget([
                'module' => 'germplasm',
    			'controller' => 'default'
                ]).'</h3>',
            'before' => '<br/>'.\Yii::t('app', 'This is the browser for all the germplasm. Click the eye icon to view germplasm information.') .
            '<p id="total-selected-text" class="pull-right" style="margin-right: 5px;"><b id = "selected-count">'.$selectedItemsCount.'</b> selected items.</p>',
            'after' => false,
        ],
        'toolbar' =>  [
            '{export}',
            [
                'content'=> ""
                    . Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'id' => 'reset-germplasm-grid', 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid')]).'{dynagridFilter}{dynagridSort}{dynagrid}'
                    . Html::a('<i class="material-icons widget-icons">add_shopping_cart</i>', '#!', [ 'class' => 'btn pull-right', 'id' => 'germplasm-save-new-list-btn', 'title'=>\Yii::t('app',' Save as new list')]).' '
                    . ManageSortsWidget::widget([
                        'dataBrowserId' => 'germplasm',
                        'gridId' => 'germplasm-grid',
                        'searchModel' => 'GermplasmSearch',
                        'btnClass' => 'sort-action-button',
                        'resetGridUrl' => $resetGridUrl.'?'
                    ])
            ],

        ],
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'resizableColumns'=>true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options'=>['id'=>'germplasm-grid']
]);

DynaGrid::end();

// modal for saving as new list
Modal::begin([
    'id' => 'germplasm-save-list-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">add</i> Save as new list</h4>',
    'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
        .Html::a('Submit' . '<i class="material-icons right">send</i>',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light disabled',
                'url' => '#',
                'id' => 'germplasm-save-list-confirm-btn'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);

$form = ActiveForm::begin([
    'enableClientValidation'=>false,
    'id'=>'create-saved-list-form',
    'fieldConfig' => function($model,$attribute){
        if(in_array($attribute, ['abbrev','display_name','name','type'])){
            return ['options' => ['class' => 'input-field form-group col-md-6']];
        }else{
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    }
]);

$model = new GermplasmList();
$listType = [
    'germplasm' => 'Germplasm'
];
$listTypeOptions = [
    'placeholder' => Yii::t('app','Select type'),
    'id' => 'type-fld',
    'class' => 'text-field-var',
    'required' => 'required',
    'data-name' => 'Type',
    'value' => 'germplasm'
];


echo '<div class="clearfix"></div><div class="germplasm-modal-loading-save"></div>';

echo $form->field($model, 'name')->textInput([
    'maxlength' => true,
    'id'=>'germplasm-list-name',
    'title'=>'Name identifier of the list',
    'autofocus' => 'autofocus',
    'onkeyup' => 'js: $("#germplasm-list-display-name").val(this.value.charAt(0).toUpperCase() + this.value.slice(1)); $(".field-germplasm-list-display-name > label").addClass("active");'
]).
$form->field($model, 'abbrev')->textInput([
    'maxlength' => true,
    'id'=>'germplasm-list-abbrev',
    'title'=>'Short name identifier or abbreviation of the list',
    'oninput' => 'js: $("#germplasm-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());'
]).
$form->field($model, 'display_name')->textInput(['maxlength' => true, 'title'=>'Name of the list to show to users', 'id'=>'germplasm-list-display_name',]).

$form->field($model, 'type')->widget(Select2::classname(), [
    'data' => $listType,
    'options' => $listTypeOptions
]).
$form->field($model, 'description')->textarea([ 'class'=>'materialize-textarea', 'title'=>'Description about the list', 'id'=>'germplasm-list-description']).
$form->field($model, 'remarks')->textarea(['class'=>'materialize-textarea', 'title'=>'Additional details about the list','row'=>2, 'id'=>'germplasm-list-remarks']);

ActiveForm::end();

echo '<div class="germplasm-save-list-preview-entries">Number of germplasm to be saved as a list: <b id = "germplasm-count"></b></div>';

Modal::end();
$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

$editEntityUrl = Url::to(['/germplasm/edit/index']);
$saveListUrl = Url::to(['/account/list/save-list']);
$selectRowUrl = Url::to(['/account/list/select-row']);
$toggleSelectAllUrl = Url::to(['/account/list/toggle-select-all']);
$exportGermplasmInfoInWidgetUrl = Url::toRoute(['/search/default/export']);

$this->registerJs(<<<JS
    var resetGridUrl='$resetGridUrl';
    var saveListUrl = "$saveListUrl";
    var selectRowUrl = "$selectRowUrl";
    var toggleSelectAllUrl = "$toggleSelectAllUrl";
    var totalResultsCount = "$totalResultsCount";
    var selectAllMode = "$selectAllMode";

    $(document).on('input', '#germplasm-list-name', function(e) {
        e.preventDefault();
        $('#germplasm-list-abbrev').val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());
        $('#germplasm-list-abbrev').trigger('change');
        $('#germplasm-list-display_name').val(this.value);
        $('#germplasm-list-display_name').trigger('change');
    });

    // export germplasm seed in germplasm widget
    $(document).on('click', '.export-data', function(e) {
        var id = this.id;
        var obj = $(this);
        var attr = obj.data('attr');
        var label = obj.data('label');
        window.location = '$exportGermplasmInfoInWidgetUrl'+'?entityId='+id+'&attr='+attr+'&label='+label;
        $(window).on('beforeunload', function(){
            $('#loading').html('');
            $('#system-loading-indicator').html('');
        });
    });

    // edit germplasm information
    $(document).on('click', '.edit-germplasm', function(e) {
        e.preventDefault();

        var obj = $(this);
        var entity_id = obj.data('entity_id');
        var url = '$editEntityUrl?id=' + entity_id;

        window.location = url;
    });

    // click row in browser
    $(document).on('click', '#germplasm-grid-id tbody tr:not(a)',function(e){
        e.stopPropagation();
        e.preventDefault();

        if($('#view-germplasm-widget-modal').css('display') != 'none') {
            // workaround because the view germplasm widget element is a descendant of the table row element
            return;
        }
        this_row=$(this).find('input:checkbox')[0];
        var id = this_row.id

            $.ajax({
                type: 'POST',
                url: selectRowUrl,
                data: {
                    id: id,
                    type: 'germplasm'
                },
                async: false,
                dataType: 'json',
                success: function(response) {
                    if(this_row.checked){
                        this_row.checked=false;
                        $('#germplasm-select-all-id').prop("checked", false);
                        $(this).removeClass("grey lighten-4");
                    } else {
                        $(this).addClass("grey lighten-4");
                        this_row.checked=true;
                        $("#"+this_row.id).prop("checked");
                    }

                    // get total selected
                    var count = 0
                    if(selectAllMode == 'include mode')
                        count = response > 0? response: 'No';
                    else    // exclude mode
                        count = response > 0? totalResultsCount - response: totalResultsCount;

                    // toggle select all button
                    if(totalResultsCount == count)
                        $('#germplasm-select-all-id').prop('checked',true);
                    else
                        $('#germplasm-select-all-id').prop('checked',false);

                    $('#selected-count').html(count);
                },
                error: function(){
                    var notif = "<i class='material-icons red-text'>close</i> There was a problem in selecting item.";
                    Materialize.toast(notif,5000);
                }
            });

    });

    // select all germplasm
    $(document).on('click', '#germplasm-select-all-id',function(e){
        var mode = 'include mode';
        var count = 'No';
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.germplasm-grid_select').attr('checked','checked');
            $('.germplasm-grid_select').prop('checked',true);
            $(".germplasm-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
            mode = 'exclude mode';
            count = totalResultsCount > 0? totalResultsCount : count;
        }else{
            $(this).removeAttr('checked');
            $('.germplasm-grid_select').prop('checked',false);
            $('.germplasm-grid_select').removeAttr('checked');
            $("input:checkbox.germplasm-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
            mode = 'include mode';
        }

        $.ajax({
            type: 'POST',
            url: toggleSelectAllUrl,
            data: {
                type: 'germplasm',
                mode: mode
            },
            async: false,
            dataType: 'json',
            success: function(response) {
                $('#selected-count').html(count);
                selectAllMode = mode;
            },
            error: function(){
                var notif = "<i class='material-icons red-text'>close</i> There was a problem in selecting all items.";
                Materialize.toast(notif,5000);
            }
        });
    });

    function refresh(){
        var total = $('#germplasm-grid-id').find('.summary').children().eq(1).html();
        totalResultsCount = total !== undefined? parseInt(total.replace(/,/g, '')): 0;
        var maxHeight = ($(window).height() - 280);
        $(".kv-grid-wrapper").css("height",maxHeight);

        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });

        $('#reset-germplasm-grid').on('click', function(e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: toggleSelectAllUrl,
                data: {
                    type: 'germplasm',
                    mode: 'include mode'
                },
                async: false,
                dataType: 'json',
                success: function(response) {
                    $('#selected-count').html('No');
                    selectAllMode = 'include mode'
                },
                error: function(){
                }
            });

            $.pjax.reload({
                container: '#germplasm-grid-pjax',
                replace: true,
                url: resetGridUrl
            });
        });

        $('#germplasm-save-new-list-btn').on('click', function(e) {
            e.preventDefault();
            var count = $('#selected-count').html().replace(/,/g, '');
            if(parseInt(count) > 0) {
                $('#germplasm-save-list-modal').modal('show');
                $('#germplasm-save-list-modal').on('shown.bs.modal', function () {
                    $('#germplasm-list-name').focus();
                })

                $('#germplasm-count').html(count);
            } else {
                var notif = "<i class='material-icons orange-text'>warning</i> Please select a germplasm.";
                Materialize.toast(notif,5000);
            }
        });

        // validate if all required fields are specified
        $('.form-control').bind("change keyup input",function() {
            var abbrev = $('#germplasm-list-abbrev').val();
            var name = $('#germplasm-list-name').val();
            var display_name = $('#germplasm-list-display_name').val();

            if(abbrev != '' && name != '' && display_name != ''){
                $('#germplasm-save-list-confirm-btn').removeClass('disabled');
            } else {
                $('#germplasm-save-list-confirm-btn').addClass('disabled');
            }
        });

        // confirm save germplasm list
        $('#germplasm-save-list-confirm-btn').on('click', function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            $('.germplasm-modal-loading-save').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');

            var abbrev = $('#germplasm-list-abbrev').val();
            var name = $('#germplasm-list-name').val();
            var displayName = $('#germplasm-list-display_name').val();
            var description = $('#germplasm-list-description').val();
            var remarks = $('#germplasm-list-remarks').val();

            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: saveListUrl,
                    data: {
                        abbrev: abbrev,
                        name: name,
                        displayName: displayName,
                        type: 'germplasm',
                        description: description,
                        remarks: remarks
                    },
                    dataType: 'json',
                    // async: false,
                    success: function(response) {

                        $('.germplasm-modal-loading-save').html('');
                        if(response['result']){
                            if(response['isBgProcess']) {
                                let exportEntriesUrl = '$listUrl'
                                let expEntUrl = window.location.origin + exportEntriesUrl
                                window.location.replace(expEntUrl)
                            }
                            var notif = "<i class='material-icons green-text'>done</i> " + response['message'];
                            $('#germplasm-save-list-modal').modal('hide');

                            $.pjax.reload({
                                container: '#germplasm-grid-pjax'
                            });

                            $.pjax.xhr = null;
                        } else {
                            var notif = "<i class='material-icons red-text'>close</i> "+ response['message'];
                        }
                        Materialize.toast(notif,5000);
                    },
                    error: function() {
                        $('.germplasm-modal-loading-save').html('');
                        var notif = "<i class='material-icons red-text'>close</i> There seems to be a problem while saving the list.";
                        Materialize.toast(notif,5000);
                        $('#germplasm-save-list-confirm-btn').addClass('disabled');
                    }
                });
            }, 500);
        });
    }

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
        refresh();
    });

    $(document).on('ready pjax:complete', function(e) {
        e.stopPropagation();
        e.preventDefault();

        refresh();
    });

    $(document).ready(function() {
    /**
    * Move the rows to prevent being covered by the select2 filter row
    * There is a weird behavior of dynagrid that uses select2 as a filter for columns where
    * the row of the select2 widgets cover the results
    */
        var height = $('#germplasm-grid-id-filters').parent().outerHeight();
        $('#germplasm-grid-id-container').find('thead').children().eq(0).height(height);
        refresh();
    });
JS
);


Yii::$app->view->registerCss('
    body{
        overflow-x: hidden;
    }
    .dropdown-content {
       overflow-y: visible;
    }
    .dropdown-content .dropdown-content {
       margin-left: -100%;
    }
    .grid-view td {
        white-space: nowrap;
    }
    .kv-align-center {
        text-align: left !important
    }
');

?>