<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

// Import components
use app\components\FavoritesWidget;

/**
 * Renders step tabs in Germplasm
 */

// Check if user is ADMIN
$isAdmin = Yii::$app->session->get('isAdmin');

$controllerID = Yii::$app->controller->id;

// Check RBAC if has access
$hasAccess = Yii::$app->access->renderAccess('GERMPLASM_'.strtoupper($controllerID),'GERMPLASM_CATALOG');
// Check allowed actions to determine tabs to display
$permissions = $isAdmin ? ['GERMPLASM_SEARCH', 'GERMPLASM_CREATE', 'GERMPLASM_MERGE', 'GERMPLASM_UPDATE', 'GERMPLASM_CODING'] : Yii::$app->session->get('currentPermissions')['permission'] ?? ['GERMPLASM_SEARCH'];

// Define URLs
$searchIndexUrl = Url::to(['search/index','program' => $program]);
$createIndexUrl = Url::to(['create/index','program' => $program]);
$mergeIndexUrl = Url::to(['merge/index','program' => $program]);
$updateIndexUrl = Url::to(['update/index','program' => $program]);
$codingIndexUrl = Url::to(['coding/index','program' => $program]);

// Determine active tabs
$searchTabStatus = strpos($controllerID,'search') !== false ? 'active' : '';
$createTabStatus = strpos($controllerID,'create') !== false ? 'active' : '';
$mergeTabStatus = strpos($controllerID,'merge') !== false ? 'active' : '';
$updateTabStatus = strpos($controllerID,'update') !== false ? 'active' : '';
$codingTabStatus = strpos($controllerID,'coding') !== false ? 'active' : '';

$disableTabs = '';

$searchTabHTML = $isAdmin || (!$isAdmin && in_array('GERMPLASM_SEARCH', $permissions)) ? '
    <li class="tab col step s2 hm-tool-tab'.$disableTabs.'" id="search-tab" link="' . $searchIndexUrl . '" controller="search">
    <a class="search-tab a-tab '.$disableTabs.' ' . $searchTabStatus . '" href="' . $searchIndexUrl . '">'
    . \Yii::t('app', 'Search')
    . '</a></li>' : '';

$createTabHTML = $isAdmin || (!$isAdmin && in_array('GERMPLASM_CREATE', $permissions)) ? '
    <li class="tab col step s2 hm-tool-tab'.$disableTabs.'" id="create-tab" link="' . $createIndexUrl . '" controller="create">
    <a class="create-tab a-tab '.$disableTabs.' ' . $createTabStatus . '" href="' . $createIndexUrl . '">'
    . \Yii::t('app', 'Create')
    . '</a></li>' : '';

$mergeTabHTML = $isAdmin || (!$isAdmin && in_array('GERMPLASM_MERGE', $permissions)) ? '
    <li class="tab col step s2 hm-tool-tab'.$disableTabs.'" id="ge-merge-tab" link="' . $mergeIndexUrl . '" controller="merge">
    <a class="merge-tab a-tab '.$disableTabs.' ' . $mergeTabStatus . '" href="' . $mergeIndexUrl . '">'
    . \Yii::t('app', 'Merge')
    . '</a></li>' : '';

$updateTabHTML = $isAdmin || (!$isAdmin && in_array('GERMPLASM_UPDATE', $permissions)) ? '
<li class="tab col step s2 hm-tool-tab'.$disableTabs.'" id="ge-update-tab" link="' . $updateIndexUrl . '" controller="update">
<a class="update-tab a-tab '.$disableTabs.' ' . $updateTabStatus . '" href="' . $updateIndexUrl . '">'
. \Yii::t('app', 'Update')
. '</a></li>' : '';

$codingTabHTML = $isAdmin || (!$isAdmin && in_array('GERMPLASM_CODING', $permissions)) ? '
<li class="tab col step s2 hm-tool-tab'.$disableTabs.'" id="ge-coding-tab" link="' . $codingIndexUrl . '" controller="coding">
<a class="coding-tab a-tab '.$disableTabs.' ' . $codingTabStatus . '" href="' . $codingIndexUrl . '">'
. \Yii::t('app', 'Coding')
. '</a></li>' : '';

// Build occurrence name display
$nameDisplay = Yii::t('app', 'Germplasm') .
FavoritesWidget::widget([
    'module' => 'germplasm',
    'controller' => $controllerID
]);

?>

<div class="col-md-9" style="margin:10px;">
    <h3><?= $nameDisplay ?></h3>
</div>

<!-- Germplasm Tabs here -->
<div class="row" style="margin-top:0px; margin-bottom:0px">
    <ul id="tabs" class="tabs">
        <?php
            echo $searchTabHTML . $createTabHTML . $mergeTabHTML . $updateTabHTML . $codingTabHTML;
        ?>
    </ul>
    <br />

</div>