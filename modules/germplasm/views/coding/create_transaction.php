<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Germplasm coding >> transaction creation page
 */

use yii\helpers\Url;

$urlGermplasmCoding = Url::to(['coding/index', 'program' => null]);
$urlRenderPatternSection = Url::toRoute(['/germplasm/coding/render-pattern-section']);
$urlStartGermplasmCoding = Url::toRoute(['/germplasm/coding/start-germplasm-coding']);
$urlGetConfiguredCodeNameType = Url::toRoute(['/germplasm/coding/get-configured-code-name-type']);
$germplasmRecordsJSON = json_encode($germplasmRecords);

?>

<div class="col-md-12" align="right" style="margin: 0px; padding: 10px;">
    <!-- Header -->
    <div class="col-md-12 row pull-left" style="padding: 0px; text-align: left;">
        <div class="col-md-10">
            <h3 class="germplasm-coding-create-transaction-title">
                <a href="<?= $urlGermplasmCoding ?>">Germplasm</a>
                <small>» Coding Germplasm </small>
            </h3>
        </div>
        <div class="col-md-2" style="padding: 0px;">
            <button class="btn btn-default pull-right lighten-4" id="germplasm-coding-back-btn" type="button" style="margin-bottom:5px;">BACK</button>
        </div>
    </div>
</div>


<div class="container">
    <!-- This is the helper text above the collapsible panels -->
    <!-- This contains the collapsible panels -->
    <div id="collapsible-section" class="row">
        <div class="col s6">
            <ul class="collapsible">
                <li class="active">
                    <div id="coding-options-collapsible" class="collapsible-header active item-theme-color white-text">
                        <span class="col s12">
                            CODING OPTION
                        </span>
                        <i class="material-icons">expand_more</i>
                    </div>
                    <div class="collapsible-body" style="display: block;">

                        <!-- INFO: Germplasm Count here -->
                        <div class="row">
                            <span class="col s12">You have selected <b> <?= $germplasmRecords['totalCount']; ?> germplasm</b>. <br><br>Your changes are <b>NOT</b> automatically saved. Navigating to another page will discard your current progress.</span>
                        </div>

                        <!-- SELECT2: Coding Option -->
                        <div class="row">
                            <span class="col s12">
                                <?= $inputFields['coding-option']; ?>
                            </span>
                        </div>

                        <!-- SELECT2: Germplasm Name Type -->
                        <div class="row">
                            <span class="col s12">    
                                <?= $inputFields['name-type']; ?>
                            </span>
                        </div>

                        <!-- SELECT2: Germplasm Name Status -->
                        <div class="row">
                            <span class="col s12"> 
                                <?= $inputFields['name-status']; ?>
                            </span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div id="collapsible-section" class="row">
        <div class="col s6">
            <ul class="collapsible">
                <li class="">
                    <div id='coding-pattern-collapsible' class="collapsible-header item-theme-color white-text">
                        <span class="col s12" id="pattern-definition-span">
                            PATTERN DEFINITION
                        </span>
                        <i class="material-icons">expand_more</i>
                    </div>
                    <div class="collapsible-body" style="display: block;">

                        <!-- PATTERN AREA: Contents based on coding option -->

                        <div class="row" id="ge-germplasm-coding-pattern-area">
                            <i>Please select a coding option.</i>
                        </div>

                        <div class="right-align">
                            <button type="button" id="ge-germplasm-coding-apply-btn" class="disabled waves-effect waves-light btn generate-code-pattern-entry" title="Create Coding Transaction" data-key="entry0" data-occurrence-count="1" data-entity="entry">
                            Apply
                            </button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php

$script = <<< JS

var codingOption = null;
var codingInfo = {};
var freeTextPrefixCount = 1;
var freeTextSuffixCount = 0;
var configuredCodeObj = {};
var germplasmRecords = $germplasmRecordsJSON;
var program = '$program';

$(document).ready(function() {

    // Coding Option
    $(document).on('change', "#ge-germplasm-coding-option-input", function() {
        codingOption = $(this).val();
        codingInfo['codingOption'] = codingOption;
        codingInfo['leadingZeros'] = 0;
        codingInfo['startingSequenceNumber'] = null;
        var inputListVal = $('ge-germplasm-coding-name-type-input').val();

        $.ajax({
            url: '$urlGetConfiguredCodeNameType',
            data: {
                codingOption: codingOption
            },
            type: 'POST',
            success: function(response) {
                var data = jQuery.parseJSON(response);

                // Add input list values based on the selected codingOption
                addNewSelect2Data('ge-germplasm-coding-name-type-input', data);
                $('#ge-germplasm-coding-name-type-input').val(inputListVal);
            
            }
        });

        $('#ge-germplasm-coding-name-type-input-div').removeClass('hidden');
    });
    
    // Name Type
    $(document).on('change', "#ge-germplasm-coding-name-type-input", function() {
        codingInfo['germplasmNameType'] = $(this).val();
        
        $('#ge-germplasm-coding-name-status-input-div').removeClass('hidden');
    });

    // Name Status
    $(document).on('change', "#ge-germplasm-coding-name-status-input", function() {
        codingInfo['germplasmNameStatus'] = $(this).val();  

        $.ajax({
            url: '$urlRenderPatternSection',
            data: {
                codingInfo: JSON.stringify(codingInfo)
            },
            type: 'POST',
            success: function(response) {
                $('#pattern-definition-span').html("PATTERN DEFINITION: " + codingOption);
                $('#ge-germplasm-coding-pattern-area').html(response);
                $('#coding-options-collapsible').click();
                $('#coding-pattern-collapsible').click();

                if(codingInfo['codingOption'] == 'configured-code'){
                    $('.gmcoding-configuredcode-field').trigger('change');
                }
            }
        });      
    });

    // Leading Zeroes
    $(document).on('change', "#leading-zeroes-field", function() {
        codingInfo['leadingZeros'] = parseInt($(this).val());
        updateFreeTextPreview();
    });

    // Starting Sequence Number
    $(document).on('change', "#starting-seq-field", function() {
        codingInfo['startingSequenceNumber'] = parseInt($(this).val());
        updateFreeTextPreview();
    });

    // Add prefix
    $(document).on('click', "#free-text-add-prefix-button", function() {
        // Update prefix count
        freeTextPrefixCount += 1;

        $("#free-text-code-prefixes").append(`
            <div class="row" id="free-text-code-prefix-div-` + freeTextPrefixCount + `">
                <div class="col s4 push-s1 card">
                    <input id="enter-free-text-` + freeTextPrefixCount + `" data-type="freeText" class="code-affix-entry free-text free-text-prefix-input">
                </div>
                <div class="col s1 push-s1">
                    <button type="button" class="remove-affix-btn remove-affix-entry free-text-remove-prefix-btn btn red" data-key="entry0" title="Remove free text" style="
                        padding-left:10px;
                        padding-right:10px;
                        ">
                    <i class="material-icons right" style="margin-left:0px;">remove</i>
                    </button>
                </div>
            </div>`);

        checkFreeTextInputCompleteness();
    });

    // Prefix input
    $(document).on('change', ".free-text-prefix-input", function() {
        updateAffixes('prefix');
    });

    // Remove prefix
    $(document).on('click', ".free-text-remove-prefix-btn", function() {
        var parentId = $(this).parent().parent().attr("id");
        $("#"+parentId).remove();
        updateAffixes('prefix');
    });

    // Add suffix
    $(document).on('click', "#free-text-add-suffix-button", function() {
        // Update suffix count
        freeTextSuffixCount += 1;

        $("#free-text-code-suffixes").append(`
            <div class="row" id="free-text-code-suffix-div-` + freeTextSuffixCount + `">
                <div class="col s4 push-s1 card">
                    <input id="enter-free-text-` + freeTextSuffixCount + `" data-type="freeText" class="code-affix-entry free-text free-text-suffix-input">
                </div>
                <div class="col s1 push-s1">
                    <button type="button" class="remove-affix-btn remove-affix-entry free-text-remove-suffix-btn btn red" data-key="entry0" title="Remove free text" style="
                        padding-left:10px;
                        padding-right:10px;
                        ">
                    <i class="material-icons right" style="margin-left:0px;">remove</i>
                    </button>
                </div>
            </div>`);

        checkFreeTextInputCompleteness();
    });

    // Suffix input
    $(document).on('change', ".free-text-suffix-input", function() {
        updateAffixes('suffix');
    });

    // Remove suffix
    $(document).on('click', ".free-text-remove-suffix-btn", function() {
        var parentId = $(this).parent().parent().attr("id");
        $("#"+parentId).remove();
        updateAffixes('suffix');
    });

    // Prefix input
    $(document).on('change', ".gmcoding-configuredcode-field", function() {
        updateSequenceValues();
        checkConfiguredCodeInputCompleteness();
    });

    // BACK BUTTON
    $(document).on('click', "#germplasm-coding-back-btn", function() {
        window.location.replace('$returnUrl');
    });

    // APPLY BUTTON
    $(document).on('click', "#ge-germplasm-coding-apply-btn", function() {
        $('#system-loading-indicator').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
        $('#system-loading-indicator').css('display','block');

        var codingData = {
            ...codingInfo,
            ...germplasmRecords,
            program: program
        };

        $.ajax({
            url: '$urlStartGermplasmCoding',
            data: {
                codingData: JSON.stringify(codingData)
            },
            type: 'POST',
            success: function(response) {

            }
        });
    });

    // Update SELECT2 value
    $(document).on('change', ".scaleval-hdn-field", function() {
        let attrFieldId = $(this).attr('id');
        let attrVal = $(this).val();

        attrFieldId = attrFieldId + "-hidden";
        var element = document.getElementById(attrFieldId);

        element.setAttribute('value',attrVal);
        $("#"+attrFieldId).trigger('change');
    });
});

/**
 * Update free text affixes
 * @param type type of affix (prefix or suffix) 
 */
function updateAffixes(type) {
    codingInfo[type] = '';
    $('.free-text-' + type + '-input').each(function(i, obj) {
        codingInfo[type] += $(this).val();
    });

    updateFreeTextPreview();
    checkFreeTextInputCompleteness();
}

/**
 * Updates free text code preview
 */
function updateFreeTextPreview() {
    if (codingInfo["prefix"] != '' && codingInfo["suffix"] != '') {
        var zeroPad = codingInfo["leadingZeros"] != null ? codingInfo["leadingZeros"] + 1 : 0;
        var sequenceNumber = String(1).padStart(zeroPad, '0');
        if(codingInfo["startingSequenceNumber"] != null) {
            var ssnDigits = codingInfo["startingSequenceNumber"].toString().length;
            zeroPad = codingInfo["leadingZeros"] != null ? codingInfo["leadingZeros"] + ssnDigits : 0;
            sequenceNumber = String(codingInfo["startingSequenceNumber"]).padStart(zeroPad, '0');
        }
        var previewString = 
            (codingInfo["prefix"] ?? '') 
            + "XXXX" // placeholder for sequence number
            + (codingInfo["suffix"] ?? '');
        
        var preview = `
            <span 
                title="Code Preview" 
                class="new badge center green darken-2"
            >
                <strong>
                    ` + previewString + `
                </strong>
            </span>
        `;

        $("#free-text-code-preview").html(preview);
    }
    else $("#free-text-code-preview").html("");
}

/**
 * Appends new data to the given select2 id
 * @param {string} select2Id - select2 element id
 * @param {array} dataArray - array containg ids and text
 */
function addNewSelect2Data(select2Id, dataArray) {
    var dataCount = dataArray.length;
    var newOption = null;
    var data = null;
    dataAppended = true;

    // Clear input list values to avoid redundancy
    $("#"+select2Id).empty();

    // Append values to input list
    for(var i = 0; i < dataCount; i++) {
        data = dataArray[i];
        newOption = new Option(data.text, data.id, false, false);
        $('#'+select2Id).append(newOption);
    }

    dataAppended = false;
}

/**
 * Check if inputs are complete for free text option
 * Once complete, enable "APPLY" button
 */
function checkFreeTextInputCompleteness() {
    // code-affix-entry
    var complete = true;

    $('.code-affix-entry').each(function(i, obj) {
        if($(this).val() == '') complete = false;
    });

    if(complete) $("#ge-germplasm-coding-apply-btn").removeClass("disabled");
    else $("#ge-germplasm-coding-apply-btn").addClass("disabled");
}

/**
 * Update sequence values for configured code fields
 */
function updateSequenceValues() {

    var obj;
    $('.gmcoding-configuredcode-field').each(function(i, obj) {
        obj = $(this);

        let attrFieldId = null;
        codingInfo['sequenceId'] = obj.attr("data-sequence_id");

        if( obj.val() !== null && obj.val() != undefined 
            && (obj.attr("data-requires_input") == 'true' || obj.attr("data-requires_input") == true)){

            // TEMPORARY! Input params not stored and retrieved;
            let inputParams = JSON.stringify({
                    "entity": "germplasm",
                    "field": "germplasmDbId"
            });

            let attrInputParams = $(this).attr("data-input_params") == '' ? $(this).attr("data-input_params") : inputParams;
            attrFieldId = $(this).attr("data-field_id");
            
            configuredCodeObj[''+ attrFieldId] = ''+attrInputParams;
        
            if( $(this).val() !== null && $(this).val() != undefined && $(this).val() != "" && $(this).attr("data-seqment_type") == 'selection'){            
                let attrFieldVal = $(this).val().toUpperCase() ?? "";
                attrFieldId = $(this).attr("data-field_id");

                if(attrFieldId && attrFieldVal !== null && attrFieldVal!= undefined && attrFieldVal != ""){
                    attrFieldVal = JSON.stringify(attrFieldVal);
                    configuredCodeObj[''+ attrFieldId] = attrFieldVal;
                }
            }
        }
        else if($(this).val() !== null && $(this).val() != undefined && $(this).attr("data-seqment_type") == 'selection'){
            let attrFieldVal = $(this).val().toUpperCase() ?? "";
                attrFieldId = $(this).attr("data-field_id");

                if(attrFieldId && attrFieldVal !== null && attrFieldVal!= undefined && attrFieldVal != ""){
                    attrFieldVal = JSON.stringify(attrFieldVal);
                    configuredCodeObj[''+ attrFieldId] = attrFieldVal;
                }
        }

        codingInfo['sequenceData'] = configuredCodeObj;
    });
}

/**
 * Verify whether configured code fields have complete required values
 */
function checkConfiguredCodeInputCompleteness() {
    var complete = true;
    $('.gmcoding-configuredcode-field').each(function(i, obj) {
        if( $(this).val() == '' || $(this).val() == null) complete = false;
    });

    if(complete) $("#ge-germplasm-coding-apply-btn").removeClass("disabled");
    else $("#ge-germplasm-coding-apply-btn").addClass("disabled");
}

JS;

$this->registerJs($script);

$this->registerCss('
    .default-dropdown {
        font-family: "Roboto", sans-serif;
        font-weight: bold;
        margin-left: -10px;
        border: none;
    }

    .remove-affix-btn {
        padding-left: 8px;
        padding-right: 8px;
        vertical-align: -32px;
    }
    
    .draggable:active {
        cursor: move;
    }

    .draggable.dragging {
        opacity: 0.4;
    }

    div.draggable.row {
        margin-bottom: 0px;
    }

    /* Margin for variable and free text cards */
    .col.s4.push-s1.card {
        margin-bottom: 8px;
    }

    /* Starting sequence label color */
    input.starting-sequence-field.has-error + label {
      color: red;
    }

    /* Starting sequence input underline color */
    input.starting-sequence-field.has-error {
      border-bottom: 1px solid red;
      box-shadow: 0 1px 0 0 red;
    }

    span.help-block-error, label.help-block-error {
        color: red;
    }
');

?>