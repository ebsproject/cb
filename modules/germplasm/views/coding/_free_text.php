<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Germplasm coding >> free text option
 */

?>

<div class="row center-align" style="margin-left: 32px;">
    <form id="generate-code-pattern-form">
        <div id="leading-zeroes-div" class="row">
            <div class="col s6 input-field ">
                <input id="leading-zeroes-field" type="number" class="leading-zeroes-field" data-key="entry0" title="Number of leading zeroes" value="0" min="0">
                <label for="leading-zeroes-field" class="active">Leading Zeroes</label>
            </div>
        </div>
        <div id="starting-seq-div" class="row">
            <div class="col s6 input-field ">
                <input id="starting-seq-field" type="number" class="starting-seq-field" data-key="entry0" title="Starting sequence number" value="0" min="0">
                <label for="starting-seq-field" class="active">Starting Sequence Number</label>
            </div>
        </div>
        <div id="code-pattern-div">
            <div class="row" style="margin-bottom: 5px;">
                <label class="col s12 left-align" title="Configure your code pattern here">Pattern</label>
                <span id="help-block-error" class="col s12 left-align"></span>
            </div>
            <div class="col s12 left-align fixed" id="free-text-code-preview" style="margin-bottom: 10px;"></div>
            <div class="row">
                <div class="col s12 left-align fixed">
                    <a 
                        id="free-text-add-prefix-button" 
                        class="waves-effect waves-light dropdown-button btn" 
                        title="Add code prefix">
                            Add prefix
                    </a>
                </div>
            </div>
            <div id="code-pattern-affixes" class="row">
                <!-- PREFIXES -->
                <div id="free-text-code-prefixes">
                    <div class="row" id="free-text-code-prefix-div-1">
                        <div class="col s4 push-s1 card">
                            <input id="enter-free-text-1" data-type="freeText" class="code-affix-entry free-text free-text-prefix-input">
                        </div>
                        <div class="col s1 push-s1">
                            <button type="button" class="remove-affix-btn remove-affix-entry free-text-remove-prefix-btn btn red" data-key="entry0" title="Remove free text" style="
                                padding-left:10px;
                                padding-right:10px;
                                ">
                            <i class="material-icons right" style="margin-left:0px;">remove</i>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- SEQUENCE -->
                <div class="row">
                    <div class="col s4 push-s1 card" style="
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        ">
                        <input type="hidden" class="code-affix-entry" data-type="sequence" value="sequence">
                        <span><b>SEQUENCE NUMBER</b></span>
                    </div>
                </div>
                <!-- SUFFIXES -->
                <div id="free-text-code-suffixes">
                    
                </div>
            </div>
            <div class="row">
                <div class="col s12 left-align">
                    <a 
                        id="free-text-add-suffix-button" 
                        class="waves-effect waves-light dropdown-button btn" 
                        title="Add code suffix">
                            Add suffix
                    </a>
                </div>
            </div>
        </div>
    </form>
</div>