<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View summary of germplasm coding transaction
 */

use kartik\dynagrid\DynaGrid;

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Modal;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

Yii::$app->view->registerCss('
    div.gm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.gm-browser-title {
        display: inline;
    }
');


// Define URLs
$returnUrl = Url::to(['/germplasm/coding', 'program' => $program]);
$resetUrl = Url::to(['/germplasm/coding/view-summary', 'program' => $program, 'fileDbId' => $fileDbId]);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

$fileName = $fileUploadInfo['fileName'] ?? '';

?>

<div id="aaaa" class="col-md-12 align= right" style="margin: 0px; padding: 10px;">
    <!-- Header -->
    <div class="col-md-12 row pull-left" style="padding: 0px; text-align: left;">
        <div class="col-md-10">
            <h3 class="file-upload-error-browser-title">
                <a href="<?= $returnUrl ?>"><?php echo \Yii::t('app', 'Germplasm'); ?></a>
                <small>» <?php echo \Yii::t('app', 'Germplasm Coding Summary'); ?> » <?php echo $fileUploadInfo['fileName']; ?></small>
            </h3>
        </div>
        <div class="col-md-2" style="padding: 0px;">
            <?php
            $completeButton = Html::a(
                '' . \Yii::t('app', 'COMPLETE'),
                '#',
                [
                    'class' => 'btn btn-default pull-right lighten-4 dropdown-trigger dropdown-button-pjax',
                    'id' => 'complete-coding-btn',
                    'title' => \Yii::t('app', 'Complete germplasm update transaction'),
                    'style' => 'margin-bottom:5px; margin-right:5px;',
                    'data-pjax' => true,
                    'data-entity_id' => '' . $fileDbId,
                    'data-entity_name' => '' . $fileUploadInfo['fileName'],
                    'data-action' => $action,
                    'data-toggle' => "modal",
                    'data-target' => "#file-upload-confirm-complete-modal",
                ]
            );
            $actionButton = $action == 'complete' ? $completeButton : '';

            echo '' . Html::a(
                '' . \Yii::t('app', 'BACK'),
                '#',
                [
                    'class' => 'btn btn-default pull-right lighten-4',
                    'id' => 'file-view-back-btn',
                    'style' => 'margin-bottom:5px;'
                ]
            )
                . $actionButton;
            ?>
        </div>
    </div>
</div>

<div class="panel panel-default" style="min-height: 500px; margin-top: 60px;">
    <?php
    // Add file info panel
    echo Yii::$app->controller->renderPartial('@app/modules/fileUpload/views/file-info.php', [
        "fileUploadInfo" => $fileUploadInfo
    ]);

    // Render data grid
    echo '<div style="padding-left:15px; padding-right:15px;">';

    $description = Yii::t('app', "View summary of germplasm coding transaction data here.");
    $dynagrid = DynaGrid::begin([
        'columns' => $columns,
        'storage' => DynaGrid::TYPE_SESSION,
        'theme' => 'simple-default',
        'showPersonalize' => true,
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => $browserId . '-table'],
            'options' => ['id' => $browserId . '-table-con'],
            'id' => $browserId . '-table-con',
            'striped' => false,
            'responsive' => true,
            'responsiveWrap' => false,
            'floatHeader' => true,
            'floatHeaderOptions' => [
                'scrollingTop' => '0',
                'position' => 'absolute',
            ],
            'hover' => true,
            'floatOverflowContainer' => true,
            'showPageSummary' => false,
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => ['id' => $browserId, 'enablePushState' => true,],
                'beforeGrid' => '',
                'afterGrid' => ''
            ],
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ],
            'rowOptions' => [],
            'panel' => [
                'heading' => false,
                'before' =>  $description . '{summary}',
                'after' => false,
            ],
            'toolbar' =>  [
                [
                    'content' =>
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [
                            Yii::$app->controller->action->id,
                            'germplasmFileUploadDbId' => $fileDbId
                        ],
                        [
                            'class' => 'btn btn-default',
                            'id' => 'ge-update-transaction-reset-summary-grid-btn',
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    )
                        . '{dynagridFilter}{dynagridSort}{dynagrid}'

                ],
            ],
        ],
        'options' => [
            'id' => $browserId
        ]
    ]);

    DynaGrid::end();
    echo '</div>';

    ?>
</div>

<?php
// Modal to confirm proceed to completion
Modal::begin([
    'id' => 'file-upload-confirm-complete-modal',
    'header' => '<h4>Complete Transaction</h4>',
    'footer' =>
    Html::a(
        \Yii::t('app', 'Cancel'),
        ['#'],
        [
            'id' => 'file-upload-delete-cancel-btn',
            'data-dismiss' => 'modal'
        ]
    ) . '&emsp;'
        . Html::a(
            \Yii::t('app', 'Confirm'),
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light modal-close',
                'id' => 'file-upload-complete-confirm-btn',
                'title' => \Yii::t('app', 'Proceed')
            ]
        ) . '&emsp;',
    'options' => [
        'data-backdrop' => 'static',
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="file-upload-confirm-complete-modal-container"></div>';
Modal::end();

$urlInvokeWorker = Url::to(['/germplasm/default/invoke-background-process']);
$urlUpdateFileStatus = Url::to(['/germplasm/default/update-status', 'fileId' => $fileDbId]);
$urlRedirectToMainPage = Url::to(['/germplasm/default/redirect-to-main-action-page', 'action' => 'coding']);

$this->registerJs(
    <<<JS
    var action = '$action';
    var fileDbId = '$fileDbId';
    var type = '';
        
        $("document").ready(function (){
            /**
             * On back button click event
             */
            $(document).on('click', '#file-view-back-btn', function(e) {
                // Redirect
                window.location.assign('$returnUrl');
            });

            /**
             * On COMPLETE button click event
             * Confirm action
             */
            $(document).on('click', '#complete-coding-btn', function(e) {
                var modalId = 'file-upload-confirm-complete-modal';
                var modalBody = 'file-upload-confirm-complete-modal-container';

                let html = '<p style="font-size:115%;">Are you sure you want to continue coding these germplasm records?'+
                      '</br></br>Click confirm to proceed.</p>';

                $('#'+modalBody).html(html).trigger('change');
                $('#'+modalId).modal('show');
            });

            /**
             * On CONFIRM of action
             */
            $(document).on('click','#file-upload-complete-confirm-btn',function(e) {    
                var modalId = 'file-upload-confirm-complete-modal';
                var modalBody = 'file-upload-confirm-complete-modal-container';

                invokeGermplasmWorker(
                    fileDbId,
                    'CodeGermplasmRecords',
                    'Create germplasm, seeds, and packages for gerplasm file upload ID: ' + fileDbId
                );
            });
        });

        /**
         * Generic function for invoking a background worker
         * @param Integer recordDbId germplasm file upload identifier
         * @param String workerName name of the worker to invoke
         * @param String description general description of what the worker does
         */
        function invokeGermplasmWorker(recordDbId, workerName, description) {

            // Invoke background worker
            $.ajax({
                url: '$urlInvokeWorker',
                type: 'post',
                data:{
                    fileUploadDbId: recordDbId,
                    workerName: workerName,
                    description: description,
                    action: 'coding',
                    additionalWorkerData: JSON.stringify({
                        action: action,
                        type: type
                    })
                },
                success: function(response){ 
                    if(!response){
                        updateCodingTransactionStatus('coding failed');
                    }
                    
                    redirectToMainActionPage();
                }
            });

        }
        
        /**
         * Update status of current transaction being viewed
         * 
         * @param String transationStatus new status
         */
        function updateCodingTransactionStatus(transactionStatus) {
            transactionStatus = transactionStatus ?? 'in queue';
            // Invoke background worker
            $.ajax({
                url: '$urlUpdateFileStatus',
                type: 'post',
                data:{
                    status: transactionStatus
                },
                success: function(response){ }
            });
        }

        /**
         * Redirect main to main coding page
         *
         */
        function redirectToMainActionPage(){
            $.ajax({
                url: '$urlRedirectToMainPage',
                type: 'post',
                data: { },
                success: function(response){ }
            });
        }

JS
);

$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');

?>