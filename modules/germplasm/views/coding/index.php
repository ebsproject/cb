<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Germplasm coding page
 */

// Import yii
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

// Import kartik classes
use kartik\date\DatePicker;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\widgets\Select2;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;
use app\modules\fileUpload\models\FileUploadHelper;

echo '<div id="gefu-update-bg-alert-div"></div>';

/**
 * Renders Update tab for Germplasm
 */

//  Render tabs
echo Yii::$app->controller->renderPartial(
    '@app/modules/germplasm/views/default/tabs.php',
    [
        'controller' => 'coding',
        'program' => $program
    ]
);

$controllerActionId = Yii::$app->controller->action->id;

$browserId = 'germplasm-coding';
$baseUrl = Url::base(); //set base URL for dynamic redirection of tools

// set columns
$additionalColumns = [
    [
        'attribute' => 'germplasmNameType',
        'label' => Yii::t('app', 'Coding germplasm name type'),
        'value' => function ($model) {
            return !empty($model['fileData'][0]['germplasmNameType']) ? $model['fileData'][0]['germplasmNameType'] : 'unknown';
        },
        'visible' => false
    ],
    [
        'attribute' => 'germplasmCount',
        'label' => Yii::t('app', 'Germplasm Count'),
        'visible' => false
    ]
];

$controllerActionId = Yii::$app->controller->action->id;

echo Yii::$app->controller->renderPartial(
    '@app/modules/germplasm/views/default/_tab_browser.php',
    [
        'module' => $module,
        'action' => 'coding',
        'program' => $program,
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'fileUploadStatusArr' => $statusValuesArr,
        'selectAllMode' => $selectAllMode,
        'browserId' => $browserId,
        'controllerActionId' => $controllerActionId,
        'additionalColumns' => $additionalColumns,
        'abbrevPrefix' => ''
    ]
);

$notificationsUrl = Url::to(['/germplasm/default/push-notifications', 'module' => $module]);
$newNotificationsUrl = Url::toRoute(['/germplasm/default/new-notifications-count', 'module' => $module]);
$viewCodingTransactionSummmaryUrl = Url::to(['/germplasm/coding/view-summary']);

// set variables for page size management
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
$pjaxReloadUrl = '/index.php/germplasm/coding/index?program=' . $program;

// variables for change page size
Yii::$app->view->registerJs(
    "
    var setDefaultPageSizeUrl = '" . $setDefaultPageSizeUrl . "',
    browserId = '" . $browserId . "',
    currentDefaultPageSize = '" . $currentDefaultPageSize . "'
    gmNewNotificationsUrl = '" . $newNotificationsUrl . "',
    gmNotificationsUrl = '" . $notificationsUrl . "',
    gmNotifsBtnId = 'gefu-coding-notifs-btn',
    gmBrowserId = '" . $browserId . "',
    gmPjaxReloadUrl = '" . $pjaxReloadUrl . "'
    ;",
    \yii\web\View::POS_HEAD
);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position' => \yii\web\View::POS_END
]);

// add global js for notifications
$this->registerJsFile("@web/js/germplasm-manager/notifications.js", [
    'depends' => ['app\assets\AppAsset'],
    'position' => \yii\web\View::POS_END
]);

// add global js for notifications
$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');


$this->registerJs(<<<JS

    var browserId = '$browserId';
    var programCode = '$program';

    adjustBrowserPageSize();

    $(document).ready(function() {
        adjustBrowserPageSize();

        notifDropdown();
        renderNotifications();
        updateNotif();

        // Check for new notifications every 10 seconds
        notificationInterval=setInterval(updateNotif, 10000);
    })

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();

        notifDropdown();

        $('#gefu-coding-notifs-btn').tooltip();
        
        $('#notif-container').html(
            '<ul id="notifications-dropdown" class="dropdown-content" '+
            'style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">'+
            '</ul>'
        );

        renderNotifications();
        updateNotif();

        refresh();
    });

    $(document).on('ready pjax:complete', function(e) {
        e.stopPropagation();
        e.preventDefault();

        refresh();
    });

    // Complete Transaction
    $(document).on('click','#complete-gefu-coding', function(e){
        e.stopPropagation();
        e.preventDefault();

        var obj = $(this);
        var recordId = obj.data('entity_id');

        // redirect to view germplasm coding summary page
        window.location.href =  "$viewCodingTransactionSummmaryUrl?program=" + programCode + 
                                "&germplasmFileUploadDbId=" + recordId;
    })
    
JS);

$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');
