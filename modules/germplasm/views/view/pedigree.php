<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders germplasm names information
 */

use yii\helpers\Url;
?>

<?php
    echo $this->renderFile('@app/modules/germplasm/views/view/tabs.php', [
        'entityId' => $entityId,
        'entityLabel' => $entityLabel,
        'program' => $program,
        'returnUrl' => $returnUrl,
        'currentTab' => 'pedigree'
    ]);
?>

<br>

<p><?= \Yii::t('app', 'Click the icon below to download the Germplasm pedigree in Helium format.') ?></p>
<a class="btn-floating waves-effect waves-light grey pull-left" id="export-pedigree-tree" title="Export pedigree tree."><i class="material-icons">download</i></a>
<p><?// \Yii::t('app', 'Below displays pedigree tree of the germplasm.') ?></p>
<?php
// Uncomment when pedigree is now viewable in widget
// if(!empty($data)){
//     echo '<div id="graph">';
//     echo '<div class="white z-depth-3 card-panel pedigree-tree">';
//     echo SearchProduct::printTree($data);
//     echo '</div></div>';
// }else{
//     echo '<i>No pedigree tree information.</i>';
// }

// Get germplasm code
$response = \Yii::$app->api->getResponse('GET', 'germplasm/'.$entityId);
$data = isset($response['body']['result']['data'][0])? $response['body']['result']['data'][0] : [];
$germplasmCode = $data['germplasmCode'];

$program = \Yii::$app->userprogram->get('abbrev');
$germplasmSearchUrl = Url::to(['/germplasm/search/index','program' => $program]);
$exportToHeliumUrl = Url::to(['/germplasm/search/export-to-helium','program' => $program]);

$this->registerJs(<<<JS
    var pedigreeDepth = '$pedigreeDepth'

    // Download germplasm pedigree
    $(document).on('click', '#export-pedigree-tree', function(e) {
        e.preventDefault();

        var entityDbId = '$entityId';
        var germplasmCode = '$germplasmCode';
        var filter = {
            germplasmDbId: String(entityDbId),
            depth:pedigreeDepth
        }
        // Invoke background worker
        $.ajax({
            url: '$exportToHeliumUrl',
            type: 'post',
            dataType: 'json',
            data: {
                description: 'Preparing the pedigree of Germplasm Code: ' + germplasmCode + ' for downloading',
                entity: 'GERMPLASM',
                entityDbId: entityDbId,
                endpointEntity: 'GERMPLASM',
                application: 'GERMPLASM_CATALOG',
                processName: 'export-pedigree',
                dataType: 'pedigree',
                endpoint: 'germplasm/:id/pedigrees-search',
                requestBody: JSON.stringify({filter:filter}),
                fileName: germplasmCode+'-Pedigree-Export'
            },
            success: function(response) {
                var success = response.success;
                var status = response.status;

                // Display message
                if (success) {
                    var notif = "<i class='material-icons green-text left'>check</i> The export file is being created in the background. You may proceed with other tasks. You will be notified in this page once done.";
                    Materialize.toast(notif,5000);

                    window.location = '$germplasmSearchUrl'
                } else {
                    // Failure message
                    var message = '<i class="material-icons red-text left">close</i> A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status !== 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if (status == null) message += ' (#002)';
                    Materialize.toast(message,5000);
                }
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There was a problem loading the content.";
                Materialize.toast(notif,5000);
            }
        });
    });
JS
);