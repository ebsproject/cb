<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders information about a germplasm
 */

use yii\helpers\Url;

$user = \Yii::$container->get('app\models\User');
$userType = $user->isAdmin();
?>

<div class="col-md-12" align="right" style="margin: 0px; padding: 10px;">
    <!-- Header -->
    <div class="col-md-12 pull-left" style="padding: 0px; text-align: left;">
        <div class="col-md-10">
            <h3 class="">
                <a href="<?= Url::to(['search/index','program' => $program]) ?>"><?= Yii::t('app', 'Germplasm') ?></a>
                <small>» <?= $entityLabel ?></small>
            </h3>
        </div>
        <?php if($returnUrl) { ?>
            <div class="col-md-2" style="padding: 0px;">
                <button class="btn btn-default pull-right lighten-4" id="view-germplasm-back-btn" type="button" style="margin-bottom:5px;">BACK</button>
            </div>
        <?php } ?>
    </div>
</div>

<ul id="view-germplasm-tabs" class="tabs">
    <li class="tab col " title="<?= \Yii::t('app', 'Basic and additional information of germplasm') ?>">
        <a href="#" id="basic-tab" class="active a-tab"><?= \Yii::t('app', 'Basic') ?></a>
    </li>
    <li class="tab col " title="<?= \Yii::t('app', 'Known names to germplasm') ?>">
        <a href="#" id="names-tab" class="a-tab"><?= \Yii::t('app', 'Names') ?></a>
    </li>
    <li class="tab col " title="<?= \Yii::t('app', 'Known seeds to germplasm') ?>">
        <a href="#" id="seeds-tab" class="a-tab" title="<?= \Yii::t('app', 'Available seeds of the germplasm') ?>"><?= \Yii::t('app', 'Seeds') ?></a>
    </li>
    <li class="tab col" title="<?= \Yii::t('app', 'Pedigree to germplasm')?>">
        <a href="#" id="pedigree-tab" class="a-tab"><?= \Yii::t('app', 'Pedigree') ?></a>
    </li>
    <li class="tab col " title="<?= \Yii::t('app', 'Known attributes to germplasm') ?>">
        <a href="#" id="attributes-tab" class="a-tab" title="<?= \Yii::t('app', 'Available attributes of the germplasm') ?>"><?= \Yii::t('app', 'Attributes') ?></a>
    </li>
    <li class="tab col " title="<?= \Yii::t('app', 'Known germplasm within the same family') ?>">
        <a href="#" id="family-tab" class="a-tab" title="<?= \Yii::t('app', 'Germplasm belonging to the same family') ?>"><?= \Yii::t('app', 'Family') ?></a>
    </li>

    <?php
        // show even logs only to admin user
        if($userType){
        ?>
        <li class="tab col " title="<?= \Yii::t('app', 'Known changes to germplasm') ?>">
            <a href="#" id="eventlogs-tab" class="a-tab"><?= \Yii::t('app', 'EventLogs') ?></a>
        </li>
    <?php } ?>
</ul>

<?php

$viewGermplasmUrl = Url::to(['/germplasm/view/index']);
$viewGermplasmNamesUrl = Url::to(['/germplasm/view/names']);
$viewGermplasmSeedsUrl = Url::to(['/germplasm/view/seeds']);
$viewGermplasmPedigreeUrl = Url::to(['/germplasm/view/pedigree']);
$viewGermplasmAttributesUrl = Url::to(['/germplasm/view/attributes']);
$viewGermplasmFamilyUrl = Url::to(['/germplasm/view/family']);
$viewGermplasmEventLogsUrl = Url::to(['/germplasm/view/event-logs']);
$eventLogUrl = Url::to(['/eventLog/default/index']);

$this->registerJs(<<<JS
    $(document).ready(function () {
        $('#basic-tab').removeClass('active')
        $('#$currentTab-tab').addClass('active')
    });

    // On back button click event
    $(document).on('click', '#view-germplasm-back-btn', function(e) {
        // Redirect
        window.location.assign('$returnUrl');
    });

    //tabs
    $(document).on('click', '#basic-tab', function(e) {
        e.preventDefault();
        goToTab('$viewGermplasmUrl');
    });

    $(document).on('click', '#names-tab', function(e) {
        e.preventDefault();
        goToTab('$viewGermplasmNamesUrl');
    });

    $(document).on('click', '#seeds-tab', function(e) {
        e.preventDefault();
        goToTab('$viewGermplasmSeedsUrl');
    });

    $(document).on('click', '#pedigree-tab', function(e) {
        e.preventDefault();
        goToTab('$viewGermplasmPedigreeUrl');
    });

    $(document).on('click', '#attributes-tab', function(e) {
        e.preventDefault();
        goToTab('$viewGermplasmAttributesUrl');
    });

    $(document).on('click', '#family-tab', function(e) {
        e.preventDefault();
        goToTab('$viewGermplasmFamilyUrl');
    });

    $(document).on('click', '#eventlogs-tab', function(e) {
        e.preventDefault();
        goToTab('$viewGermplasmEventLogsUrl');
    });
    //end tabs

    function goToTab(tabUrl) {
        let returnUrl = encodeURIComponent('$returnUrl');
        let url = tabUrl + '?id=$entityId&program=$program&returnUrl=' + returnUrl;
        window.location = url;
    }

JS
);

Yii::$app->view->registerCss('
    .indicator {
        display: none
    }
    #view-germplasm-tabs.tabs .tab a{
        padding: 0 20px !important;
    }
');

?>