<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders germplasm attributes information
 */

use yii\grid\GridView;
?>

<?php
    echo $this->renderFile('@app/modules/germplasm/views/view/tabs.php', [
        'entityId' => $entityId,
        'entityLabel' => $entityLabel,
        'program' => $program,
        'returnUrl' => $returnUrl,
        'currentTab' => 'attributes'
    ]);
?>

<br>

<p><?= \Yii::t('app', 'Below displays known attributes to germplasm.') ?></p>
<?= GridView::widget([
    'dataProvider' => $data,
    'layout' => '{items}',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => ''
        ],
        [
            'attribute' => 'variableDisplayName',
            'label' => 'Attribute'
        ],
        [
            'attribute' => 'attributeDataValue',
            'label' => 'Value'
        ]
    ]
]) ?>