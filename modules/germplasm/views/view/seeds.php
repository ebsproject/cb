<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders germplasm names information
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?php
    echo $this->renderFile('@app/modules/germplasm/views/view/tabs.php', [
        'entityId' => $entityId,
        'entityLabel' => $entityLabel,
        'program' => $program,
        'returnUrl' => $returnUrl,
        'currentTab' => 'seeds'
    ]);
?>

<br>

<?php echo Html::a('<i class="material-icons">file_download</i>','#',
    [
        'class' => 'btn-floating waves-effect waves-light grey pull-right export-data',
        'data-attr'=>'seeds',
        'data-label'=>$entityLabel,
        'id'=> $entityId,
        'title'=> 'Export to csv file',
        'style'=>"",
        'onclick' => '(function ( $event ) { $(\'#system-loading-indicator\').hide(); })();'
    ]);
?>
<?php echo Html::a('<i class="material-icons">search</i>','#',
    [
        'class' => 'btn-floating waves-effect waves-light grey pull-right',
        'id'=> 'find-seeds-btn',
        'title'=> 'View in Find Seeds tool',
        'style'=>"margin-right: 5px",
    ]);
?>

<p><?= \Yii::t('app', 'Below are the available seeds of the germplasm.') ?></p>
<?php
$browserId = 'view-germplasm-seeds-grid';

//dynagrid configuration
$dynagrid = DynaGrid::begin([
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => ''
        ],
        'seedCode',
        'seedName',
        'harvestDate',
        'harvestMethod',
        'programCode',
        'programName',
        'experimentName',
        'experimentYear',
        'experimentType',
    ],
    'theme'=>'simple-default',
    'showPersonalize'=>false,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'id' => $browserId,
        'dataProvider'=>$data,
        // 'filterModel'=>$searchModel,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>['id'=>$browserId],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            'heading'=> false,
            'before' => '{summary}',
            'after' => false,
        ],
        'toolbar' => [],
        'floatHeader' =>false,
        'floatOverflowContainer'=> false,
        'resizableColumns'=>true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options'=>['id'=>$browserId]
]);

DynaGrid::end();
?>

<?php
$findSeedsUrl = Url::to(['/seedInventory/find-seeds/find','program'=>$program]);
$addToSessionUrl = Url::to(['/germplasm/search/add-find-seeds-session']);
$exportGermplasmInfoInWidgetUrl = Url::toRoute(['/search/default/export']);
$this->registerJS(<<<JS
    var findSeedsUrl = "$findSeedsUrl";
    $('#find-seeds-btn').on('click', function() {
        $.ajax({
            url: "$addToSessionUrl",
            data: {designation: "$entityLabel"},
            type: 'POST',
            dataType: 'json',
            success: function() {
                // Update fieldSeedsUrl with designation
                findSeedsUrl = findSeedsUrl+`&FindSeedDataBrowserModel%5Bdesignation%5D=$entityLabel`

                window.location = findSeedsUrl;
            }
        })
    });

    // export germplasm seed in germplasm widget
    $(document).on('click', '.export-data', function(e) {
        var id = this.id;
        var obj = $(this);
        var attr = obj.data('attr');
        var label = obj.data('label');
        window.location = '$exportGermplasmInfoInWidgetUrl'+'?entityId='+id+'&attr='+attr+'&label='+label;
        $(window).on('beforeunload', function(){
            $('#loading').html('');
            $('#system-loading-indicator').html('');
        });
    });
JS);

?>