<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders germplasm family information
 */

use yii\grid\GridView;
?>

<?php
    echo $this->renderFile('@app/modules/germplasm/views/view/tabs.php', [
        'entityId' => $entityId,
        'entityLabel' => $entityLabel,
        'program' => $program,
        'returnUrl' => $returnUrl,
        'currentTab' => 'family'
    ]);
?>

<br>

<?php
    $hidden = [
        "familyMembers"
    ];
    $totalCount = $data['familyInfo']['memberCount'] ?? 0;

    // If family ID is null, display notice to user.
    // This means that the germplasm does not belong to any family.
    if(empty($data['familyInfo']['familyDbId'])) {
        echo "<p>" . \Yii::t('app', 'No family information available. This germplasm is currently not a member of any family.') . "</p>";
        return;
    }
?>

<p><?= \Yii::t('app', 'Below displays the family information and its known members.') ?></p>

<ul class="collapsible popout collapsible-accordion col col-md-6" data-collapsible="accordion" style="margin-top:-10px">
    <?php
    //family information
    $col1Str = '';
    $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';
    foreach ($data['familyInfo'] as $k => $value) {
        if(str_contains($k, 'DbId') || in_array($k, $hidden)) continue;
        $key = ucfirst(str_replace('_',' ', $k));
        $key = preg_replace('/(?<!\ )[A-Z]/', ' $0', $key);
        $value = ($value == '') ? $notSet : $value;

        $col1Str = $col1Str.'<dt title="'.$key.'" style="width: 170px !important;">'.$key.'</dt><dd style="margin-left: 190px !important;">'.$value.'</dd>';
    }
    ?>
    <li class="active">
        <div class="collapsible-header active"><?= \Yii::t('app', 'Family information') ?></div>
        <div class="collapsible-body" style="display: block;margin-top:15px">
            <dl class="dl-horizontal">
                <?= $col1Str ?>
            </dl>
        </div>
    </li>
</ul>

<ul class="collapsible popout collapsible-accordion col col-md-6" data-collapsible="accordion" style="margin-top:-10px">
    <li class="active">
        <div class="collapsible-header active"><?= \Yii::t('app', 'Family members') ?></div>
        <div class="collapsible-body" style="display: block;margin-top:15px;overflow-y:scroll; max-height:350px;">
        <p style="white-space: normal;"><?= \Yii::t('app', 'Below are the germplasm known to belong to this family. <b>Note</b>: This browser only displays a maximum of 100 members.') ?></p>
            <?=
                GridView::widget([
                    'dataProvider' => $data['dataProvider'],
                    'options' => ['id' => 'view-germplasm-tables-family'],
                    'summary' => 'Showing <b>{begin}-{end}</b> of <b>' . $totalCount .'</b> members.',
                    'summaryOptions' => ['class' => 'summary-modal pull-left'],
                    'layout' => '{summary}{items}',
                    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
                    'columns' => [
                        [
                            'attribute' => 'familyMemberNumber',
                            'label' => 'Family Member No.',
                            'headerOptions' => ['style' => 'vertical-align: top; width:20%;'],
                        ],
                        [
                            'attribute' => 'designation',
                            'label' => 'Germplasm',
                            'headerOptions' => ['style' => 'vertical-align: top;'],
                            'contentOptions' => [
                                "class" => "germplasm-name-col"
                            ],
                            'format' => 'raw',
                            'value' => function($model) use ($currentGermplasm) {
                                $designation = $model['designation'];
                                $class = "";

                                if($designation == $currentGermplasm) {
                                    $designation = "<b>$designation</b> (current germplasm)";
                                    $class = "current-germplasm";
                                }

                                return "<p class=" . $class . " >" . $designation . "</p>";
                            }
                        ]
                    ]
                ]);
            ?>
        </div>
    </li>
</ul>