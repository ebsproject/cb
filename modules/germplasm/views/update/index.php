<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Modal;
use yii\data\ArrayDataProvider;

// import browser helpers
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

// import external models and components
use app\models\UserDashboardConfig;

// import model
use app\modules\germplasm\models\UpdateModel;

echo '<div id="gefu-update-bg-alert-div"></div>';

/**
 * Renders Update tab for Germplasm
 */

//  Render tabs
echo Yii::$app->controller->renderPartial(
    '@app/modules/germplasm/views/default/tabs.php',
    [ 
        'controller' => 'update', 
        'program' => $program
    ]);

$controllerActionId = Yii::$app->controller->action->id;

// Render additional data browser columns
$additionalColumns = [
        [
            'attribute'=>'attributes',
            'contentOptions'=>[
                'class' => 'gefu-update-file-attributes-col'
            ],
            'noWrap' => true,
            'width' => '300px',
            'format' => 'html',
            'value' => function($model) use ($action,$searchModel,$updateModel,$configValues){
                if($model['fileData'] == null){
                    return '<span class="not-set">(not set)</span>';
                }
                else{
                    $summary = $updateModel->getFileDataSummary($model['fileData'],$action,$configValues);

                    $finalValue = '
                        <ul class="collapsible" data-collapsible="accordion" style="min-width: 158px; margin: 2px;">'.
                        '<li>
                            <div class="collapsible-header" style="padding: .5rem;">
                                <span class="secondary-content">'. 
                                    $summary['attributeCount']. ' attributes ( '.$summary['rowCount'].' rows )
                                </span>' .
                            '</div>
                            <div class="collapsible-body" style="padding:0px">';
                    
                    $dataModel = [];
                    if($summary['attributeCount'] != 0){
                        foreach ($summary['attributes'] as $attribute) {
                            $dataModel[] = ["attribute" => $attribute];
                        }
                    }

                    $provider = new ArrayDataProvider([
                        'allModels' => $dataModel,
                        'pagination' => false
                    ]);

                    $finalValue .= GridView::widget([
                        'dataProvider' => $provider,
                        'layout' => '{items}',
                        'showHeader' => false,
                        'columns' => [
                            [
                                'attribute' => 'attribute',
                                'format' => 'raw',
                                'label' => false,
                            ]
                        ],
                        'tableOptions' => ["style" => "margin-bottom:0px"]
                    ]);

                    $finalValue .= '</div></li></ul>';
    
                    return $finalValue;
                }
            },
            'visible' => false
        ]
    ];

echo Yii::$app->controller->renderPartial(
    '@app/modules/germplasm/views/default/_tab_browser.php',
    [ 
        'module' => $module,
        'action' => 'update',
        'program' => $program,
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'fileUploadStatusArr' => $statusValuesArr,
        'selectAllMode' => $selectAllMode,
        'browserId' => $browserId,
        'controllerActionId' => $controllerActionId,
        'additionalColumns' => $additionalColumns,
        'abbrevPrefix' => $abbrevPrefix
    ]);

// update germplasm file upload modal
Modal::begin([
    'id' => 'germplasm-update-file-upload-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">file_upload</i>Update Germplasm File Upload</h4>',
    'footer' => Html::a('Cancel', [''], [
                    'id' => 'cancel-update-germplasm-file-upload-btn',
                    'class' => 'modal-close-button',
                    'title' => \Yii::t('app', 'Cancel'),
                    'data-dismiss' => 'modal'
                ]) . '&emsp;&nbsp;',
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin'
    ],
    'closeButton' => ['class'=>'hidden'],
]);
echo '<div class="germplasm-update-file-upload-modal-body parent-panel"></div>';
Modal::end();

$notificationsUrl = Url::to(['/germplasm/default/push-notifications','module' => $module]);
$newNotificationsUrl = Url::toRoute(['/germplasm/default/new-notifications-count','module' => $module]);
$resetGridUrl = Url::to(['/germplasm/update/index', 'program'=>$program]);  
$downloadTemplateUrl = Url::to(['/germplasm/default/download-template', 'program'=>$program, 'action' => 'update']);
$viewUpdateTransactionUrl = Url::to(['/germplasm/update/preview']);

//Update upload URL
$renderFileUploadFormUrl = Url::to(['/germplasm/update/render-file-upload']); 

// set variables for page size management
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
$pjaxReloadUrl = '/index.php/germplasm/update/index?program=$program';

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = 'dynagrid-germplasm-gefu-update-grid',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    gmNewNotificationsUrl = '".$newNotificationsUrl."',
    gmNotificationsUrl = '".$notificationsUrl."',
    gmNotifsBtnId = 'gefu-update-notifs-btn',
    gmBrowserId = 'dynagrid-germplasm-gefu-update-grid',
    gmPjaxReloadUrl = '".$pjaxReloadUrl."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

// add global js for notifications
$this->registerJsFile("@web/js/germplasm-manager/notifications.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    
    var browserId = '$browserId';
    var programCode = '$program';

    var selectedRecordId = null;

    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';

    $(document).ready(function() {
        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();

        refresh();

        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        notifDropdown();
        renderNotifications();
        updateNotif();

        // Check for new notifications every 10 seconds
        notificationInterval=setInterval(updateNotif, 10000);
    });

    // Download transaction template
    $(document).on('click','#gm-update-template-blank-btn', function(e){
        e.stopPropagation();
        e.preventDefault();
        
        // Perform download
        let templateDownloadUrl = window.location.origin + '$downloadTemplateUrl' + '&redirectUrl=' + encodeURIComponent(window.location.href)
        window.location.assign(templateDownloadUrl)
    })

    // Upload transaction
    $(document).on('click','#upload-update-transaction-btn', function(e){
        e.stopPropagation();
        e.preventDefault();

        $('#germplasm-update-file-upload-modal').modal('show');
        
        var modalBody = '.germplasm-update-file-upload-modal-body';

        $(modalBody).html(loading);
        $.ajax({
            url: '$renderFileUploadFormUrl',
            type: 'post',
            dataType: 'json',
            data:{
                programCode: '$program'
            },
            success: function(response){
                $(modalBody).html(response);
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                $(modalBody).html(errorMessage);
                }
        });
    })

    // Complete Transaction
    $(document).on('click','#complete-gefu-update', function(e){
        e.stopPropagation();
        e.preventDefault();

        var obj = $(this);
        var recordId = obj.data('entity_id');

        // redirect to update germplasm transaction page
        window.location.href =  "$viewUpdateTransactionUrl?program=" + programCode + 
                                "&germplasmFileUploadDbId=" + recordId;
    })

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();

        notifDropdown();

        $('#gefu-update-notifs-btn').tooltip();
        
        $('#notif-container').html(
            '<ul id="notifications-dropdown" class="dropdown-content" '+
            'style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">'+
            '</ul>'
        );

        renderNotifications();
        updateNotif();

        refresh();
    });

    $(document).on('ready pjax:complete', function(e) {
        e.stopPropagation();
        e.preventDefault();

        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();

        refresh();
    });

    function refresh(){
        var maxHeight = ($(window).height() - 280);
        $(".kv-grid-wrapper").css("height",maxHeight);

        $('.collapsible').collapsible();
        $('.dropdown-trigger').dropdown();

        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });
        
        // display and hide loading indicator
        $(document).on('click','.hide-loading',function(){
            $('#system-loading-indicator').css('display','block');
            setTimeout(function() {
                $('#system-loading-indicator').css('display','none');
            },3000);
        });
    }

JS
);

$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');

?>

