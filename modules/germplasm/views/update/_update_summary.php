<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file for file upload summary
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

Yii::$app->view->registerCss('
    div.gm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.gm-browser-title {
        display: inline;
    }
');


// Define URLs
$returnUrl = Url::to(['/germplasm/update', 'program'=>$program]);
$resetUrl = Url::to(['/germplasm/update/preview', 'program'=>$program,'germplasmFileUploadDbId'=>$fileDbId]);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

$fileName = $fileUploadInfo['fileName'] ?? '';

?>
<div id="aaaa" class="col-md-12 align= right" style="margin: 0px; padding: 10px;">
    <!-- Header -->
    <div class="col-md-12 row pull-left" style="padding: 0px; text-align: left;">
        <div class="col-md-10">
            <h3 class="file-upload-error-browser-title">
                <a href="<?= $returnUrl ?>"><?php echo \Yii::t('app','Germplasm'); ?></a>
                <small>» <?php echo \Yii::t('app','File Upload Summary'); ?> » <?php echo $fileUploadInfo['fileName']; ?></small>
            </h3>
        </div>
        <div class="col-md-2" style="padding: 0px;">
            <?php
                $completeButton = Html::a(
                    ''.\Yii::t('app','COMPLETE'),
                    '#',
                    [
                        'class' => 'btn btn-default pull-right lighten-4 dropdown-trigger dropdown-button-pjax',
                        'id' => 'complete-update-btn',
                        'title' => \Yii::t('app','Complete germplasm update transaction'),
                        'style' => 'margin-bottom:5px; margin-right:5px;',
                        'data-pjax' => true,
                        'data-entity_id' => ''.$fileDbId,
                        'data-entity_name' => ''.$fileUploadInfo['fileName']
                    ]
                );
                $actionButton = $action == 'complete' ? $completeButton : '';

                echo ''.Html::a(
                    ''.\Yii::t('app','BACK'),
                    '#',
                    [
                        'class' => 'btn btn-default pull-right lighten-4',
                        'id' => 'file-view-back-btn',
                        'style' => 'margin-bottom:5px;'
                    ]
                )
                .$actionButton;
            ?>
        </div>
    </div>
</div>

<div class="panel panel-default" style="min-height: 500px; margin-top: 60px;">
    <?php
        // Add file info panel
        echo Yii::$app->controller->renderPartial('@app/modules/fileUpload/views/file-info.php',[
            "fileUploadInfo" => $fileUploadInfo
        ]);
        
        // Render data grid
        echo '<div style="padding-left:15px; padding-right:15px;">';

        $description = Yii::t('app', "Review update germplasm transaction data here.");
        $dynagrid = DynaGrid::begin([
            'columns'=>$columns,
            'storage' => DynaGrid::TYPE_SESSION,
            'theme'=>'simple-default',
            'showPersonalize' => true,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'tableOptions'=>['class'=>$browserId.'-table'],
                'options'=>['id'=>$browserId.'-table-con'],
                'id' => $browserId.'-table-con',
                'striped'=>false,
                'responsive' => true,
                'responsiveWrap' => false,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                ],
                'hover' => true,
                'floatOverflowContainer' => true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options' => ['id' => $browserId, 'enablePushState' => true,],
                    'beforeGrid'=>'',
                    'afterGrid'=>''
                ],
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
                'rowOptions'=> [],
                'panel'=> [
                    'heading'=> false,
                    'before' =>  $description . '{summary}',
                    'after'=> false,
                ],
                'toolbar' =>  [
                    [
                        'content' => 
                            Html::a(
                                '<i class="glyphicon glyphicon-repeat"></i>',
                                [
                                    Yii::$app->controller->action->id,
                                    'germplasmFileUploadDbId' => $fileDbId
                                ],
                                [
                                    'class' => 'btn btn-default',
                                    'id' => 'ge-update-transaction-reset-summary-grid-btn',
                                    'data-position' => 'top',
                                    'data-tooltip' => \Yii::t('app', 'Reset grid'),
                                    'data-pjax' => true
                                ]
                            )
                            . '{dynagridFilter}{dynagridSort}{dynagrid}'

                    ],
                ],
            ],
            'options'=> [
                'id' => $browserId
            ]
        ]);

        DynaGrid::end();
        echo '</div>';

    ?>
</div>

<?php

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$urlInvokeGermplasmWorker = Url::to(['update/invoke-germplasm-worker']);

$script = <<< JS
    var germplasmFileUploadId = '$fileDbId'
    var germplasmFileUploadName = '$fileName'

    $("document").ready(function() {
        
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();
        
        refresh();

        $(document).on('ready pjax:complete', function(e) {
            e.stopPropagation();
            e.preventDefault();

            $('.dropdown-trigger').dropdown();
            $('.dropdown-button').dropdown();

            // Adjust browser height
            $('#'+browserId+'-table-con-container').css('min-height','450px');
        });

        /**
         * On back button click event
         */
        $(document).on('click', '#file-view-back-btn', function(e) {
            // Redirect
            window.location.assign('$returnUrl');
        });

        /**
         * On grid reset button click event
         */
        $(document).on('click', '#ge-update-transaction-reset-summary-grid-btn', function(e) {
            // refresh browser 
            $.pjax.reload({
                container: '#'+browserId+'-pjax',
                url: '$resetUrl'
            });
        });

        /**
         * Facilitate completion of update germplasm transaction
         */
        $(document).on('click','#complete-update-btn',function(e){
            e.stopPropagation();
            e.preventDefault();

            // Invoke background worker
            $.ajax({
                url: '$urlInvokeGermplasmWorker',
                type: 'post',
                data:{
                    germplasmFileUploadDbId: germplasmFileUploadId,
                    workerName: 'CreateGermplasmRecords',
                    description: 'Create germplasm attributes for gerplasm file upload ID: ' + germplasmFileUploadId,
                    additionalWorkerData: JSON.stringify({
                        action: 'create',
                        type: 'germplasm_attribute'
                    })
                },
                success: function(response){ }
            });
        })
    });

    /**
    * Resize grid table upon loading 
    */
    function refresh() {
        var maxHeight = ($(window).height() - 350);
        $(".kv-grid-wrapper").css("height", maxHeight);

        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });
    }
JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    .dropdown-content {
       overflow-y: visible;
    }
    .dropdown-content .dropdown-content {
       margin-left: -100%;
    }
    .grid-view td {
        white-space: nowrap;
    }
    .kv-align-center {
        text-align: left !important
    }
');

?>