<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders germplasm file upload transaction modal content
 */
use kartik\widgets\FileInput;
use yii\helpers\Url;

$processFileUploadUrl = Url::to(['/germplasm/update/process-file-upload', 'programId'=>$program, 'programCode'=>$programCode, 'type'=>'germplasm_attribute']);
$validateFileUploadUrl = Url::to(['/germplasm/create/validate-data-bg-process']);

echo '<div class="col col-md-12 upload-panel" style="margin-top:10px">';

echo FileInput::widget([
    'name' => 'germplasm_file_upload',
    'pluginOptions' => [
        'uploadAsync' => false,
        'uploadUrl' => $processFileUploadUrl,
        'allowedFileExtensions' => ['csv'],
        'browseClass' => 'btn waves-effect waves-light browse-btn pull-right',
        'showCaption' => false,
        'showRemove' => false,
        'browseLabel' => 'Browse',
        'removeLabel' => '',
        'uploadLabel' => 'Validate',
        'uploadTitle' => 'Validate uploaded file',
        'uploadClass' => 'btn',
        'removeClass' => 'btn btn-danger',
        'cancelClass' => 'btn grey lighten-2 black-text',
        'dropZoneTitle' => \Yii::t('app', 'Drag and drop file here or<br/>click Browse<br/><small>Use the downloaded Germplasm Merge File Upload template in CSV format.</small>'),
        'overwriteInitial' => true,
        'maxFileCount' => 1,
        'autoReplace' => true,
        'disabledPreviewTypes' => ['text'],
        'fileActionSettings' => [
            'showUpload' => false,
            'showZoom' => false,
            'removeIcon' => '<i class="fa fa-trash"></i>',
            'showRotate' => false,
            'indicatorNew' => '<i class="fa fa-plus-circle text-warning"></i>',
            'indicatorSuccess' => '<i class="fa fa-check-circle text-success"></i>',
            'indicatorError' => '<i class="fa fa-exclamation-circle text-danger"></i>',
            'indicatorLoading' => '<i class="fa fa-hourglass-half text-muted"></i>',
            'indicatorPaused' => '<i class="fa fa-pause-circle text-info"></i>'
        ],
        'theme' => 'fa',
    ],
    'pluginEvents' => [
        'fileclear' => 'function() { validate("clear"); }',
        'filereset' => 'function() { validate("reset"); }',
        'fileloaded' => 'function(data) { validate("upload",data); }',
        'fileuploaderror' => 'function() { validate("error"); }',
    ],
    'options' => [
        'multiple' => false,
        'accept' => 'csv/*', 
        'id' => 'germplasm-update-create-file-upload'
    ]
]);

echo '<div>';

$this->registerJs(<<<JS
    var validationSuccess = false;
    var program = '$program';
    var programCode = '$programCode';

    // if file upload validation is successful
    $('#germplasm-update-create-file-upload').on('filebatchuploadsuccess', function(e, data) {
        validationSuccess = true;
        var response = data.response;
        
        // if success
        if(response.success){
            var fileName = data.files[0].name;
            var fileUploadId = response.id;
            $('#germplasm-update-file-upload-modal').modal('hide');

            // display notification
            var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>"+fileName+" has been successfully uploaded and sent for data validation!</span>";
            Materialize.toast(notif, 5000);

            // validate uploaded file
            $.ajax({
                url: '$validateFileUploadUrl',
                type: 'post',
                dataType: 'json',
                data:{
                    fileUploadId : fileUploadId,
                    type: 'germplasm_attribute'
                },
                success: function(response){
                    if(response.success) {
                        $('#gefu-bg-alert-div').html(
                            '<div id="w25-info-0" class="alert info">' +
                                '<div class="card-panel">' +
                                    '<i class="fa fa-info-circle hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                                    'Data validation of the uploaded file is being handled in the background. You may proceed with other tasks. You will be notified in this page once done.' +
                                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                '</div>' +
                            '</div>'
                        );
                    }
                    else {
                        $('#gefu-bg-alert-div').html(
                            '<div id="w25-warning-0" class="alert warning">' +
                                '<div class="card-panel">' +
                                    '<i class="fa fa-info-warning hm-bg-warning-icon"></i>&nbsp;&nbsp;' +
                                    'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.' +
                                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                '</div>' +
                            '</div>'
                        );
                    }

                    $.pjax.reload({
                        container: '#dynagrid-germplasm-gefu-update-grid-pjax',
                        url: '/index.php/germplasm/update/index?program=$programCode'
                    });
                },
                error: function(){
                    var errorMessage = "<i class='material-icons green-text left'>check</i> <span>There was a problem triggering the background process.</span>";
                    Materialize.toast(errorMessage, 5000);
                }
            });
        }
        
    });

    // validate if required fields specified upon file upload
    function validate(action,data){
        // if file is cleared
        if(action == 'clear'){
            validationSuccess = false;
        }
    }

JS
);
?>