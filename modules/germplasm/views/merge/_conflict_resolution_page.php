<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file for file upload summary
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

Yii::$app->view->registerCss('
    div.gm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.gm-browser-title {
        display: inline;
    }
');

$fileName = $fileUploadInfo['fileName'] ?? '';
$fileId = $fileUploadInfo['fileId'] ?? '';
$reportName = $fileUploadInfo['remarks'] ?? '';

$controllerId = Yii::$app->controller->action->id ?? '';
$browserId = $browserId ?? 'dynagrid-conflict-resolution-merge-grid';

// Define URLs
$mainGermplasmUrl = Url::to(['/germplasm/merge', 'program'=>$program]);
$returnUrl = Url::to(['/germplasm/merge/preview', 'program'=>$program,'germplasmFileUploadDbId'=>$fileId]);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

$resetUrl = Url::to(['/germplasm/merge/resolve-conflict', 'program'=>$program,'fileId'=>$fileId,'action'=>$action]);
$updateStatusUrl = Url::to(['/germplasm/default/update-status','fileId'=>$fileId]);
$submitResolutionsUrl = Url::to(['/germplasm/merge/submit-resolution','fileId'=>$fileId]);

// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// page action buttons
$submitOptionsHTML = "
    <ul id='dropdown-conflict-resolution-options' class='dropdown-content toolbar-btn-options' data-beloworigin='false'>
        <li><a href='#!' id='submit-all-resolution-btn' class='submit-resolution-options'>Submit All</a></li>
    </ul>
";

$submitButton = ''.Html::a(
    ''.\Yii::t('app','Submit'),
    '#',
    [
        'class' => 'btn btn-default pull-right lighten-4 dropdown-trigger dropdown-button-pjax',
        'id' => 'submit-resolution-btn',
        'style' => 'margin-bottom:5px;',
        'title' => \Yii::t('app','Submit resolution'),
        'data-pjax' => true,
        'data-beloworigin' => 'true',
        'data-constrainwidth' => 'false',
        'data-activates'=>'dropdown-conflict-resolution-options'
    ]
).$submitOptionsHTML;
?>

<div id="aaaa" class="col-md-12" align="right" style="margin: 0px; padding: 10px;">
    <!-- Header -->
    <div class="col-md-12 row pull-left" style="padding: 0px; text-align: left;">
        <div class="col-md-8">
            <h3 class="file-upload-error-browser-title">
                <a href="<?= $mainGermplasmUrl ?>"><?php echo \Yii::t('app','Germplasm'); ?></a>
                <small>
                    » <?php echo \Yii::t('app','Merge Conflict Resolution'); ?> 
                    » <?php echo $fileName; ?>
                </small>
            </h3>
        </div>
        <div class="col-md-4" style="padding: 0px;">
            <?php

                echo ''.Html::a(
                    ''.\Yii::t('app','BACK'),
                    '#',
                    [
                        'class' => 'btn btn-default pull-right lighten-4',
                        'id' => 'conflict-resolve-back-btn',
                        'style' => 'margin-left:5px;'
                    ]
                )
                .$submitButton;
            ?>
        </div>
    </div>
</div>

<div class="panel panel-default" style="min-height: 500px; margin-top: 60px;">
    <?php
        // Add file info panel
        echo Yii::$app->controller->renderPartial(
            '@app/modules/fileUpload/views/file-info.php',
            [ "fileUploadInfo" => $fileUploadInfo ]
        );

        // Render data grid
        $description = Yii::t('app', "Resolve merge conflict identified for the transaction data here.");
        echo '<div style="padding-left:15px; padding-right:15px;">';

        // add resolution column
        $columns[] = [
            'attribute' => 'Resolution',
            'label' => 'Conflict Resolution',
            'content' => function ($model) {
                return  '<div class = "switch pull=right">' . 
                        '<label>KEEP<input type = "checkbox" class = "decision-switch-option" id = ' . 
                        $model['transaction_id'] . '-' . $model['row_number'] . '-' . 
                        strtolower($model['conflict_code']) . '-' . strtolower($model['conflict_variable']) .
                        ' data-item_id = ' . $model['transaction_id'] . '-' . $model['row_number'] . '-' . 
                        strtolower($model['conflict_code']) . '-' . strtolower($model['conflict_variable']) . 
                        '>' . '<span class = "lever"></span></input>MERGE</label></div>';
            },
            'visible' => true,
            'hidden' => false,
            'order' => DynaGrid::ORDER_FIX_RIGHT
        ];

        $dynagrid = DynaGrid::begin([
            'columns'=>$columns,
            'storage' => DynaGrid::TYPE_SESSION,
            'theme'=>'simple-default',
            'showPersonalize' => true,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'tableOptions'=>['class'=>$browserId.'-table'],
                'options'=>['id'=>$browserId.'-table-con'],
                'id' => $browserId.'-table-con',
                'striped'=>false,
                'responsive' => true,
                'responsiveWrap' => true,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                ],
                'hover' => true,
                'floatOverflowContainer' => true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options' => [
                        'id' => $browserId, 
                        'enablePushState' => true
                    ],
                    'beforeGrid'=>'',
                    'afterGrid'=>''
                ],
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
                'rowOptions'=> [],
                'panel'=> [
                    'heading'=> false,
                    'before' =>  $description . '{summary}',
                    'after'=> false,
                ],
                'toolbar' =>  [
                    [
                        'content' => 
                            Html::a(
                                '<i class="glyphicon glyphicon-repeat"></i>',
                                [
                                    $controllerId,
                                    'program' => $program,
                                    'fileId' => $fileId,
                                    'action' => $action
                                ],
                                [
                                    'class' => 'btn btn-default',
                                    'id' => 'ge-merge-transaction-reset-summary-grid-btn',
                                    'data-position' => 'top',
                                    'data-tooltip' => \Yii::t('app', 'Reset grid'),
                                    'data-pjax' => true
                                ]
                            )
                            . '{dynagridFilter}{dynagridSort}{dynagrid}'

                    ],
                ],
            ],
            'options'=> [
                'id' => $browserId
            ]
        ]);

        DynaGrid::end();
        echo '</div>';

    ?>
</div>

<?php

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS
    var germplasmFileUploadId = '$fileId'
    var germplasmFileUploadName = '$fileName'
    var programCode = '$program'
    var fileName = '$fileName'
    var reportName = '$reportName'

    var resolutionSessionStorage = "conflict-res_toggle-storage_" + germplasmFileUploadId

    $("document").ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();

        refresh();

        loadSavedResolutions();

        $(document).on('ready pjax:complete', function(e) {
            e.stopPropagation();
            e.preventDefault();

            $('.dropdown-trigger').dropdown();
            $('.dropdown-button').dropdown();

            loadSavedResolutions();

            // Adjust browser height
            $('#'+browserId+'-table-con-container').css('min-height','450px');
        });

        /**
         * On back button click event
         */
        $(document).on('click', '#conflict-resolve-back-btn', function(e) {
            // update status to MERGE READY
            $.ajax({
                url: '$updateStatusUrl',
                type: 'POST',
                success: function(response) {
                    setSessionSavedResolutions(JSON.stringify([]))

                    // Redirect
                    window.location.assign('$returnUrl');
                },
                error: function(error){
                    notif = "<i class='material-icons red-text'>error</i>&nbsp;" +
                            "Error encountered in updating file upload status." +
                            error;
                    Materialize.toast(notif, 3500);
                }
            })
        });

        /**
         * On Submit All click event
         */
        $(document).on('click','#submit-all-resolution-btn', function(e) {
            // retrieve saved session resolutions
            var currResSessValues = getSessionSavedResolutions()
            var currentResArr = JSON.parse(currResSessValues)

            $.ajax({
                url: '$submitResolutionsUrl',
                type: 'POST',
                data:{
                    savedResolutionsArr: currentResArr,
                    fileName: reportName
                },
                success: function(response) {
                    var res = JSON.parse(response);

                    var notif = ''
                    if(res.success){
                        notif = "<i class='material-icons green-text'>check</i>&nbsp;" +
                                "The submitted resolutions have been saved.";
                        Materialize.toast(notif, 3500);
                    }
                    else{
                        notif = "<i class='material-icons red-text'>error</i>&nbsp;" +
                                "Error encountered in processing the submitted resolutions." + 
                                res.message;
                        Materialize.toast(notif, 3500);
                    }

                    setSessionSavedResolutions(JSON.stringify([]))

                    // Redirect
                    window.location.assign('$returnUrl');
                },
                error: function(error){
                    notif = "<i class='material-icons red-text'>error</i>&nbsp;" +
                            "Error encountered in processing the submitted resolutions." +
                            error;
                    Materialize.toast(notif, 3500);
                }
            })
        });

        // store resolution for conflict report item
        $(document).on('change','.decision-switch-option', function(e) {
            
            // retrieve ID
            var obj = $(this)
            var itemId = obj.data('item_id')

            var isChecked = false

            // check if checked
            if($(this).prop("checked") === true){
                isChecked = true
            }

            storeResolutionInSess(itemId,isChecked)
        });
    });

    // load saved resolutions
    function loadSavedResolutions(){
        var sessSavedRes = getSessionSavedResolutions()
        var currentResArr = sessSavedRes != null ? JSON.parse(sessSavedRes) : []

        var currSelectionIndx = -1
        var itemId

        $.each($('.decision-switch-option'), function(i, v) {
            itemId = this.id
            currSelectionIndx = currentResArr != null ? currentResArr.indexOf(itemId) : -1

            if (currSelectionIndx != -1){
                $("#"+itemId).prop("checked", true).trigger('change');
            }
        });
    }

    // get session saved resolutions
    function getSessionSavedResolutions(){
        return sessionStorage.getItem(resolutionSessionStorage)
    }

    // set session saved resolutions
    function setSessionSavedResolutions(data){
        sessionStorage.setItem(resolutionSessionStorage, data)
    }

    // store IDs of conflict item with resolution toggled to MERGE value
    function storeResolutionInSess(itemId,isChecked){
        var inSession = false
        
        // retrieve session data
        var sessSavedRes = getSessionSavedResolutions()
        var currentResArr = sessSavedRes != null ? JSON.parse(sessSavedRes) : []
        
        // check if ID in saved session resolution
        var currSelectionIndx = currentResArr != null ? currentResArr.indexOf(itemId) : -1

        if(currSelectionIndx !== -1){
            inSession = true
        }
        
        // if checked but not in session, then add to session
        if( isChecked && !inSession && 
            currentResArr != null && currentResArr.length == 0)
        {
            setSessionSavedResolutions(JSON.stringify([itemId]))                
        }

        if( isChecked && !inSession && 
            currentResArr != null && currentResArr.length > 0)
        {
            currentResArr.push(itemId)
            setSessionSavedResolutions(JSON.stringify(currentResArr))                
        }
        
        // if no longer checked but in session, then remove from session
        if(!isChecked && inSession){
            currentResArr.splice(currSelectionIndx,1);
            setSessionSavedResolutions(JSON.stringify(currentResArr))                
        }
    }
    
    /**
    * Resize grid table upon loading 
    */
    function refresh() {
        var maxHeight = ($(window).height() - 350);
        $(".kv-grid-wrapper").css("height", maxHeight);

        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });
    }
JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    .dropdown-content {
       overflow-y: visible;
    }
    .dropdown-content .dropdown-content {
       margin-left: -100%;
    }
    .grid-view td {
        white-space: nowrap;
    }
    .kv-align-center {
        text-align: left !important
    }
');

?>