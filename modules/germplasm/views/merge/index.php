<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Modal;

// import browser helpers
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

// import external models and components
use app\models\UserDashboardConfig;
use app\modules\fileUpload\models\FileUploadHelper;

use app\components\DownloadFileDataCSV;
use app\components\FileUploadSummaryWidget;
use app\components\FileUploadErrorLogsWidget;

use app\dataproviders\ArrayDataProvider;

echo '<div id="gefu-merge-bg-alert-div"></div>';

/**
 * Renders Merge tab for Germplasm
 */

//  Render tabs
echo Yii::$app->controller->renderPartial(
    '@app/modules/germplasm/views/default/tabs.php',
    [ 'controller' => 'merge', 'program' => $program]
);

// browser parameters
CONST GERMPLASM_MODULE = 'merge';

// urls
$notificationsUrl = Url::to(['/germplasm/default/push-notifications','module' => GERMPLASM_MODULE]);
$newNotificationsUrl = Url::toRoute(['/germplasm/default/new-notifications-count','module' => GERMPLASM_MODULE]);
$resetGridUrl = Url::to(['/germplasm/merge/index', 'program'=>$program]);
$renderFileUploadFormUrl = Url::to(['/germplasm/merge/render-file-upload']);

$selectAllButtonChecked = 'checked';

$browserId = !isset($browserId) || empty($browserId) ? 'dynagrid-germplasm-gefu-merge-grid' : $browserId;

$controllerActionId = Yii::$app->controller->action->id;

$additionalColumns = [
    [
        'attribute'=>'germplasmCount',
        'contentOptions'=>[
            'class' => 'gefu-merge-row-count-col'
        ],
        'value' => function($model){
            return !empty($model['germplasmCount']) ? $model['germplasmCount'] : 0;
        },
        'content' => function($model){
            return !empty($model['germplasmCount']) ? $model['germplasmCount'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
];

echo Yii::$app->controller->renderPartial(
    '@app/modules/germplasm/views/default/_tab_browser.php',
    [ 
        'module' => GERMPLASM_MODULE,
        'action' => GERMPLASM_MODULE,
        'program' => $program,
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'fileUploadStatusArr' => $statusValuesArr,
        'selectAllMode' => $selectAllMode,
        'browserId' => $browserId,
        'controllerActionId' => $controllerActionId,
        'additionalColumns' => $additionalColumns,
        'abbrevPrefix' => $abbrevPrefix
    ]);

echo '</div>';

echo '
    <!-- Notifications -->
    <div id="notif-container">
        <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
        </ul>
    </div>
';

?>

<?php

// set urls
$getSelectedFromSessionUrl = Url::to(['/account/list/get-selected-from-session']);
$saveListUrl = Url::to(['/account/list/save-list']);
$selectRowUrl = Url::to(['/account/list/select-row']);
$toggleSelectAllUrl = Url::to(['/account/list/toggle-select-all']);

$viewMergeTransactionUrl = Url::to(['/germplasm/merge/preview']);
$deleteTransactionUrl = Url::to(['/germplasm/merge/delete']);
$downloadTemplateUrl = Url::to(['/germplasm/merge/download-template', 'program'=>$program]);

// set variables for page size management
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
$pjaxReloadUrl = "/index.php/germplasm/merge/index?program=$program";

// Modal for confirming delete
Modal::begin([
    'id' => 'gefu-merge-confirm-delete-modal',
    'header' => '<h4>Delete File Upload Transaction</h4>',
    'footer' =>
    Html::a(\Yii::t('app','Cancel'), ['#'],
            [
                'id'=>'gfu-delete-cancel-btn',
                'data-dismiss'=>'modal'
            ]
        ). '&emsp;'
    . Html::a(\Yii::t('app', 'Confirm'), '#', 
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close',
            'id' => 'gefu-delete-proceed-btn',
            'title' => \Yii::t('app', 'Proceed')
        ]
    ) . '&emsp;',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="gefu-merge-confirm-delete-modal-container"></div>';
Modal::end();

// merge germplasm file upload modal
Modal::begin([
    'id' => 'germplasm-merge-file-upload-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">file_upload</i>Merge Germplasm File Upload</h4>',
    'footer' => Html::a('Cancel', [''], [
                    'id' => 'cancel-merge-germplasm-file-upload-btn',
                    'class' => 'modal-close-button',
                    'title' => \Yii::t('app', 'Cancel'),
                    'data-dismiss' => 'modal'
                ]) . '&emsp;&nbsp;',
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin'
    ],
    'closeButton' => ['class'=>'hidden'],
]);
echo '<div class="germplasm-merge-file-upload-modal-body parent-panel"></div>';
Modal::end();

// Check if browser Id is empty
$browserId = empty($browserId) ? 'none' : $browserId;

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = 'dynagrid-germplasm-gefu-merge-grid',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    gmNewNotificationsUrl = '".$newNotificationsUrl."',
    gmNotificationsUrl = '".$notificationsUrl."',
    gmNotifsBtnId = 'gefu-merge-notifs-btn',
    gmBrowserId = 'dynagrid-germplasm-gefu-merge-grid',
    gmPjaxReloadUrl = '".$pjaxReloadUrl."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

// add global js for notifications
$this->registerJsFile("@web/js/germplasm-manager/notifications.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    var resetGridUrl='$resetGridUrl';
    var saveListUrl = "$saveListUrl";
    var selectRowUrl = "$selectRowUrl";
    var toggleSelectAllUrl = "$toggleSelectAllUrl";
    var selectAllMode = "$selectAllMode";
    var getSelectedFromSessionUrl = '$getSelectedFromSessionUrl';

    // redirect urls
    var viewMergeTransactionUrl = '$viewMergeTransactionUrl';

    var notificationsUrl = '$notificationsUrl';
    var newNotificationsUrl = '$newNotificationsUrl';

    var browserId = '$browserId';
    var programCode = '$program';

    var selectedRecordId = null;

    $(document).ready(function() {
        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();

        refresh();

        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        notifDropdown();
        renderNotifications();
        updateNotif();

        // Check for new notifications every 7.5 seconds
        notificationInterval=setInterval(updateNotif, 7500);
    });

    /**
     * On grid reset button click event
     */
    $(document).on('click', '#reset-gefu-merge-grid', function(e) {
        // refresh browser
        $.pjax.reload({
            container: '#'+browserId+'-pjax',
            url: resetGridUrl
        });
    });

    /**
     * Delete confirmation
     */
    $(document).on('click', '#delete-gefu', function(e) {
        selectedRecordId = $(this).attr('data-entity_id');
        // Add message to modal body
        $("#gefu-merge-confirm-delete-modal-container").html('<p style="font-size:115%;">This will delete the file upload and its contents. Click CONFIRM to proceed.</p>');

    });

    /**
     * Delete process
     */
    $(document).on('click','#gefu-delete-proceed-btn',function (e){
        e.stopPropagation();
        e.preventDefault();

        if(selectedRecordId != null){
            // Disable CONFIRM button
            $(this).addClass('disabled');

            // Add progress bar
            $("#gefu-merge-confirm-delete-modal-container").html('<div class="progress"><div class="indeterminate"></div></div>');

            // Perform delete
            $.ajax({
                url: '$deleteTransactionUrl',
                type: 'post',
                data:{
                    germplasmFileUploadDbId: selectedRecordId
                },
                success: function(response){
                    // enable delete buttons
                    $('#gefu-delete-proceed-btn').removeClass('disabled');

                    // Hide modal
                    $("#gefu-merge-confirm-delete-modal.in").modal('hide');

                    // Display toast message
                    var notif = "<i class='material-icons green-text'>check</i>&nbsp;The file upload transaction was successfully deleted.";
                    Materialize.toast(notif, 3500);

                    // refresh browser
                    $.pjax.reload({
                        container: '#'+browserId+'-pjax',
                        url: '/index.php/germplasm/merge/index?program=$program'
                    });
                },
                error: function(error){
                    $("#gefu-merge-confirm-delete-modal.in").modal('hide');

                    var notif = "<i class='material-icons red-text'>close</i>&nbsp;An error occurred. The file upload transaction was not deleted.";
                    Materialize.toast(notif, 3500);
                }
            });
        }
    });

    // Download merge germplasm template
    $(document).on('click','#download-merge-template-btn', function(e){
        e.stopPropagation();
        e.preventDefault();

        // Perform download
        let templateDownloadUrl = window.location.origin + '$downloadTemplateUrl' + 
                                  '&redirectUrl=' + encodeURIComponent(window.location.href)

        window.location.assign(templateDownloadUrl)
    })

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();

        notifDropdown();

        $('#gefu-merge-notifs-btn').tooltip();
        $('#notif-container').html(
            '<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>'
        );

        renderNotifications();
        updateNotif();

        refresh();
    });

    $(document).on('ready pjax:complete', function(e) {
        e.stopPropagation();
        e.preventDefault();

        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();

        refresh();
    });

    // redirect to merge germplasm transaction preview
    $(document).on('click','#validate-gefu-merge', function (e){
        e.stopPropagation();
        e.preventDefault();

        var obj = $(this);
        var recordId = obj.data('entity_id');

        // redirect to merge germplasm transaction page
        window.location.href =  "$viewMergeTransactionUrl?program=" + programCode + 
                                "&germplasmFileUploadDbId=" + recordId + "&action=preview";
    });

    // redirect to merge germplasm transaction preview
    $(document).on('click','#complete-gefu-merge', function (e){
        e.stopPropagation();
        e.preventDefault();

        var obj = $(this);
        var recordId = obj.data('entity_id');

        // redirect to merge germplasm transaction page
        window.location.href =  "$viewMergeTransactionUrl?program=" + programCode + 
                                "&germplasmFileUploadDbId=" + recordId;
    });

    function refresh(){
        var total = $('#'+browserId+'-id').find('.summary').children().eq(1).html();
        totalResultsCount = total !== undefined? parseInt(total.replace(/,/g, '')): 0;

        var maxHeight = ($(window).height() - 280);
        $(".kv-grid-wrapper").css("height",maxHeight);

        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });

        // display and hide loading indicator
        $(document).on('click','.hide-loading',function(){
            $('#system-loading-indicator').css('display','block');
            setTimeout(function() {
                $('#system-loading-indicator').css('display','none');
            },3000);
        });
    }

    // enable merge germplasm file upload
    $(document).on('click', '#upload-merge-transaction-btn', function(e) {
        e.preventDefault();
        e.stopPropagation();

        $('#germplasm-merge-file-upload-modal').modal('show');

        var modalBody = '.germplasm-merge-file-upload-modal-body';

        $.ajax({
            url: '$renderFileUploadFormUrl',
            type: 'post',
            dataType: 'json',
            data:{
                programCode: '$program'
            },
            success: function(response){
                $(modalBody).html(response);
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                $(modalBody).html(errorMessage);
                }
        });
    });

JS
);

$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');

?>