<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Merge conflict report for germplasm merge
 */

use yii\helpers\Url;

// Set filename value
$fileName = $fileUploadInfo['remarks']; 

// Define URLs
$downloadConflictLog = Url::to(['/germplasm/merge/download-data', 'fileName' => $fileName]);

 ?>
 
 <div class="row orange lighten-4" style="margin-left: 20px; margin-bottom:0px; margin-right:20px; color:black; padding-bottom:5px;">
   <? echo "<i class='material-icons orange-text' style='padding-left:5px; margin-bottom:0px; margin-top: 5px;'>warning</i><span style='position:relative; top:-5px;'>&nbsp;&nbsp;&nbsp;Potential conflicts found in merge transaction. Kindly review the merge conflict report for further details. <a id='download-conflict-btn' class='hide-loading' style='cursor:pointer'>$fileName</a></span>";?>  
</div>

<?
$this->registerJs(<<<JS
  
  $(document).on('click', '#download-conflict-btn', function(e){
    window.location.href = '$downloadConflictLog'
  });

  // display and hide loading indicator
  $(document).on('click','.hide-loading',function(){
    $('#system-loading-indicator').css('display','block');
      setTimeout(function() {
        $('#system-loading-indicator').css('display','none');
    },3000);
  });

JS);
?>