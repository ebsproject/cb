<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file for file upload summary
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

Yii::$app->view->registerCss('
    div.gm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.gm-browser-title {
        display: inline;
    }
');


// Define URLs
$returnUrl = Url::to(['/germplasm/merge', 'program'=>$program]);
$resetUrl = Url::to(['/germplasm/merge/preview', 'program'=>$program,'germplasmFileUploadDbId'=>$fileDbId]);
$validateMergeGermplasmUrl = Url::to(['/germplasm/merge/validate-merge-transaction', 'program'=>$program,'germplasmFileUploadId'=>$fileDbId]);
$completeMergeGermplasmUrl = Url::to(['/germplasm/merge/complete-merge-transaction']);
$resolveConflictUrl = Url::to(['/germplasm/merge/resolve-conflict']);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

$fileName = $fileUploadInfo['fileName'] ?? '';

?>
<div id="aaaa" class="col-md-12" align="right" style="margin: 0px; padding: 10px;">
    <!-- Header -->
    <div class="col-md-12 row pull-left" style="padding: 0px; text-align: left;">
        <div class="col-md-8">
            <h3 class="file-upload-error-browser-title">
                <a href="<?= $returnUrl ?>"><?php echo \Yii::t('app','Germplasm'); ?></a>
                <small>» <?php echo \Yii::t('app','File Upload Summary'); ?> » <?php echo $fileUploadInfo['fileName']; ?></small>
            </h3>
        </div>
        <div class="col-md-4" style="padding: 0px;">
            <?php

                $mergeOptionsHTML = "
                    <ul id='dropdown-merge-ge-options' class='dropdown-content toolbar-btn-options'  data-beloworigin='false'>
                        <li><a href='#!' id='merge-germplasm-btn' class='merge-germplasm-options'>Merge All</a></li>
                    </ul>
                ";
                
                $mergeButton = Html::a(
                    ''.\Yii::t('app','MERGE'),
                    '#',
                    [
                        'class' => 'btn btn-default pull-right lighten-4 dropdown-trigger dropdown-button-pjax',
                        'id' => 'file-view-merge-btn',
                        'title' => \Yii::t('app','Merge germplasm records'),
                        'style' => 'margin-bottom:5px; margin-right:5px;',
                        'data-pjax' => true,
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                        'data-activates'=>'dropdown-merge-ge-options'
                    ]
                ).'&emsp;'.$mergeOptionsHTML;

                $completeButton = Html::a(
                    ''.\Yii::t('app','COMPLETE'),
                    '#',
                    [
                        'class' => 'btn btn-default pull-right lighten-4 dropdown-trigger dropdown-button-pjax',
                        'id' => 'complete-merge-btn',
                        'title' => \Yii::t('app','Complete germplasm merge transaction'),
                        'style' => 'margin-bottom:5px; margin-right:5px;',
                        'data-pjax' => true,
                        'data-entity_id' => ''.$fileDbId,
                        'data-entity_name' => ''.$fileUploadInfo['fileName']
                    ]
                );

                $resolveButton = ($fileUploadInfo['remarks'] != null) ? Html::a(
                    ''.\Yii::t('app','RESOLVE'),
                    '#',
                    [
                        'class' => 'btn btn-default pull-right lighten-4 dropdown-trigger dropdown-button-pjax',
                        'id' => 'resolve-conflict-btn',
                        'title' => \Yii::t('app','Resolve conflicts found in the transaction.'),
                        'style' => 'margin-bottom:5px; margin-right:5px;',
                        'data-pjax' => true,
                        'data-entity_id' => ''.$fileDbId,
                        'data-entity_name' => ''.$fileUploadInfo['fileName']
                    ]
                ) : '';

                $actionButton = $action == 'complete' ? $completeButton.' '.$resolveButton : $mergeButton;

                echo ''.Html::a(
                    ''.\Yii::t('app','BACK'),
                    '#',
                    [
                        'class' => 'btn btn-default pull-right lighten-4',
                        'id' => 'file-view-back-btn',
                        'style' => 'margin-bottom:5px;'
                    ]
                )
                .$actionButton;
            ?>
        </div>
    </div>
</div>

<div class="panel panel-default" style="min-height: 500px; margin-top: 60px;">
    <?php
        // Add file info panel
        echo Yii::$app->controller->renderPartial('@app/modules/fileUpload/views/file-info.php',[
            "fileUploadInfo" => $fileUploadInfo
        ]);

        // add condition if remarks has value for this conflict page
        if($fileUploadInfo['remarks'] != null){
            echo Yii::$app->controller->renderPartial('@app/modules/germplasm/views/merge/_merge_conflict_page.php',[
                "fileUploadInfo" => $fileUploadInfo
            ]);
        }
        
        // Render data grid
        echo '<div style="padding-left:15px; padding-right:15px;">';

        $description = Yii::t('app', "Review merge germplasm transaction data here.");
        $dynagrid = DynaGrid::begin([
            'columns'=>$columns,
            'storage' => DynaGrid::TYPE_SESSION,
            'theme'=>'simple-default',
            'showPersonalize' => true,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'tableOptions'=>['class'=>$browserId.'-table'],
                'options'=>['id'=>$browserId.'-table-con'],
                'id' => $browserId.'-table-con',
                'striped'=>false,
                'responsive' => true,
                'responsiveWrap' => false,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                ],
                'hover' => true,
                'floatOverflowContainer' => true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options' => ['id' => $browserId, 'enablePushState' => true,],
                    'beforeGrid'=>'',
                    'afterGrid'=>''
                ],
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
                'rowOptions'=> [],
                'panel'=> [
                    'heading'=> false,
                    'before' =>  $description . '{summary}',
                    'after'=> false,
                ],
                'toolbar' =>  [
                    [
                        'content' => 
                            Html::a(
                                '<i class="glyphicon glyphicon-repeat"></i>',
                                [
                                    Yii::$app->controller->action->id,
                                    'germplasmFileUploadDbId' => $fileDbId
                                ],
                                [
                                    'class' => 'btn btn-default',
                                    'id' => 'ge-merge-transaction-reset-summary-grid-btn',
                                    'data-position' => 'top',
                                    'data-tooltip' => \Yii::t('app', 'Reset grid'),
                                    'data-pjax' => true
                                ]
                            )
                            . '{dynagridFilter}{dynagridSort}{dynagrid}'

                    ],
                ],
            ],
            'options'=> [
                'id' => $browserId
            ]
        ]);

        DynaGrid::end();
        echo '</div>';

    ?>
</div>

<?php

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS
    var germplasmFileUploadId = '$fileDbId'
    var germplasmFileUploadName = '$fileName'
    var programCode = '$program'

    $("document").ready(function() {
        
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();
        
        refresh();

        $(document).on('ready pjax:complete', function(e) {
            e.stopPropagation();
            e.preventDefault();

            $('.dropdown-trigger').dropdown();
            $('.dropdown-button').dropdown();

            // Adjust browser height
            $('#'+browserId+'-table-con-container').css('min-height','450px');
        });

        /**
         * On back button click event
         */
        $(document).on('click', '#file-view-back-btn', function(e) {
            // Redirect
            window.location.assign('$returnUrl');
        });

        /**
         * On grid reset button click event
         */
        $(document).on('click', '#ge-merge-transaction-reset-summary-grid-btn', function(e) {
            // refresh browser 
            $.pjax.reload({
                container: '#'+browserId+'-pjax',
                url: '$resetUrl'
            });
        });

        /**
         * Trigger MergeGermplasm worker to validate transaction
         */
        $(document).on('click','#merge-germplasm-btn',function(e){
            e.stopPropagation();
            e.preventDefault();

            // pass on to trigger for merge germplasm worker
            $.ajax({
                url: '$validateMergeGermplasmUrl',
                type: 'post',
                data:{
                    fileUploadId: germplasmFileUploadId,
                    fileUploadName: germplasmFileUploadName
                },
                success: function(response){
                    var res = JSON.parse(response)

                    if(res.success){
                        // Display toast message
                        var notif = "<i class='material-icons green-text'>check</i>&nbsp;"
                                    +"The transaction has been sent for validation.";
                        Materialize.toast(notif, 3500);

                        // redirect to main merge page
                        window.location.assign('$returnUrl');
                    }
                    else{
                        // Display toast message
                        var notif = "<i class='material-icons red-text'>close</i>&nbsp;"
                                    +"Error encountered in sending the transaction for validation. Kindly try again.";
                        Materialize.toast(notif, 3500);
                    }
                    
                },
                error: function(error){
                    var notif = "<i class='material-icons red-text'>close</i>&nbsp;"
                                +"An error occurred. The file upload transaction was not sent for validation.";
                    Materialize.toast(notif, 3500);
                }
            });
        });

        /**
         * Facilitate completion of merge germplasm transaction
         */
        $(document).on('click','#complete-merge-btn',function(e){
            e.stopPropagation();
            e.preventDefault();

            var obj = $(this)
            var germplasmFileUploadId = obj.data('entity_id')
            var germplasmFileUploadName = obj.data('entity_name')

            // pass on to trigger for merge germplasm worker
            $.ajax({
                url: '$completeMergeGermplasmUrl',
                type: 'post',
                data:{
                    fileUploadId: germplasmFileUploadId,
                    fileUploadName: germplasmFileUploadName
                },
                success: function(response){
                    var res = JSON.parse(response)

                    if(res.success){
                        // Display toast message
                        var notif = "<i class='material-icons blue-text'>info</i>&nbsp;"
                                    +"The transaction is being completed...";
                        Materialize.toast(notif, 3500);

                        // redirect to main merge page
                        window.location.assign('$returnUrl');
                    }
                    else{
                        // Display toast message
                        var notif = "<i class='material-icons red-text'>close</i>&nbsp;"
                                    +"Error encountered in completing the merge transaction. Kindly try again.";
                        Materialize.toast(notif, 3500);
                    }
                    
                },
                error: function(error){
                    var notif = "<i class='material-icons red-text'>close</i>&nbsp;"
                                +"An error occurredin completing the merge transaction.";
                    Materialize.toast(notif, 3500);
                }
            });
        });

        $(document).on('click','#resolve-conflict-btn', function(e){
            e.stopPropagation();
            e.preventDefault();

            var obj = $(this);
            var recordId = obj.data('entity_id');

            // redirect to resolving merge transaction conflict
            window.location.href =  "$resolveConflictUrl?program=" + programCode + "&fileId=" + 
                                    recordId + "&action=resolve";
        });
    });

    /**
    * Resize grid table upon loading 
    */
    function refresh() {
        var maxHeight = ($(window).height() - 350);
        $(".kv-grid-wrapper").css("height", maxHeight);

        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });
    }
JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    .dropdown-content {
       overflow-y: visible;
    }
    .dropdown-content .dropdown-content {
       margin-left: -100%;
    }
    .grid-view td {
        white-space: nowrap;
    }
    .kv-align-center {
        text-align: left !important
    }
');

?>