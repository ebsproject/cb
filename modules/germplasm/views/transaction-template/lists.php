<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file for file upload summary
 */

use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

Yii::$app->view->registerCss('
    div.gm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.gm-browser-title {
        display: inline;
    }
');

// Define urls
$resetUrl = Url::to(['/germplasm/transaction-template/select-list','program'=>$program,'action'=>$action,'entity'=>$entity]);
$returnUrl = Url::to([$returnUrl, 'program'=>$program]);
?>

<div id="header" class="col-md-12" align="right" style="margin: 0px; padding: 10px;">
    <div class="col-md-12 row pull-left" style="padding: 0px; text-align: left;">
        <div class="col-md-8">
            <h3 class="file-upload-error-browser-title">
                <a href="<?= $returnUrl ?>">
                    <?php echo \Yii::t('app', ucfirst($sourceTool) ); ?>
                </a>
                <small> 
                    » <?php echo \Yii::t('app', ucfirst($action));?>
                    » <?php echo \Yii::t('app','Generate Template from '.ucfirst($entity ?? '').' List');?>
                </small>
            </h3>
        </div>
        <div class="col-md-4" style="padding: 0px;">
            <?php
                echo ''.Html::a(
                    ''.\Yii::t('app','BACK'),
                    '#',
                    [
                        'class' => 'btn btn-default pull-right lighten-4',
                        'id' => 'view-list-back-btn',
                        'style' => 'margin-left:5px;'
                    ]
                );
            ?>
        </div>
    </div>
</div>

<div class="panel panel-default" style="min-height: 500px; margin-top: 60px;">

<?php

$actionButtons = [
    'view' => function ($url, $model, $key) use ($action) {
        $styleOption = null;
        $inProgressString = '<'.$model['listDbId'].'-creating list member is in progress>';
        $isListInProgress = !is_null($model['remarks']) && substr($model['remarks'], 0, strlen($inProgressString)) === $inProgressString;
        
        if($isListInProgress) { // hide if creating list members is in progress
            $styleOption = 'display:none';
        }

        return  Html::a('<i class="material-icons">visibility</i> ',
            '#',
            [

                'title' => \Yii::t('app', 'View List'),
                'id'=> strtolower($action).'-view-list-btn',
                'data-toggle' => 'modal',
                'data-target' => '#view-list-members-modal',
                'data-id' => $model['listDbId'],
                'data-display_name' => $model['displayName'],
                'style' => $styleOption
            ]);
    },
    'download' => function ($url, $model, $key) use ($action, $templateOptions, $listTypeConfigs) {
        $id = $model['listDbId'];
        $listType = $model['type'];
        
        // Get config for list type
        $listTypeConfig = $listTypeConfigs[$listType] ?? [];
        $allowedTypes = $listTypeConfig[$action];

        // Build options
        $options = '';
        foreach($templateOptions as $to) {
            $displayText = $to['display_text'];
            $type = $to['type'];

            // Disable option if it is not supported
            $disableOption = !in_array($type, $allowedTypes);
            $optionStyle = $disableOption ? 'color:grey !important;' : '';
            $optionClass = $disableOption ? 'disabled-link' : '' ;
            $optionAdditionalTitle = $disableOption ? ' - not suported for ' . $listType . ' lists' : '' ;
            $spanClass =  $disableOption ? strtolower($action).'-wrapper' : '';

            // Build options element
            $options .= "
                <li title='$displayText $optionAdditionalTitle' class='$spanClass'>
                    <a
                        class='". strtolower($action) ."-download-template-btn hide-loading " . $optionClass . " download-from-list-btn'
                        style='$optionStyle'
                        data-id='" . $id . "'
                        data-type='". $type ."'
                    >
                        $displayText
                    </a>
                </li>
            ";
        }

        // Build dropdown element
        return
            "<span class='dropdown bootstrap-dropdown' title='Download template'>
                <a class='dropdown-toggle' type='button' data-toggle='dropdown'><i class='material-icons'>file_download</i></a>
                <ul class='dropdown-menu' style='z-index: 5040;'>
                   $options
                </ul>
            </span>";
    }
];

$defaultColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order' => kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => '',
        'noWrap' => true,
        'template' => '{view}{download}',
        'order'=>DynaGrid::ORDER_FIX_LEFT,
        'buttons' => $actionButtons
    ],
    [
        'attribute' => 'type',
        'visible' => true,
        'format' => 'raw',
        'noWrap'=>false,
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>$listTypeFilters ?? [], 
        'filterWidgetOptions'=>[
            'pluginOptions'=>[
                'allowClear'=>true,
                'id'=>"select-status-data-browser-id"
            ]
        ],
        'filterInputOptions'=>[
            'autocomplete' => 'new-password',
            'placeholder'=>'type',
            'id'=>"select-status-"
        ]
    ],
    [
        'attribute' => 'abbrev',
        'visible' => true,
        'format' => 'raw',
        'filterInputOptions' => ['autocomplete' => 'new-password']
    ],
    [
        'attribute' => 'name',
        'visible' => true,
        'format' => 'raw',
        'filterInputOptions' => ['autocomplete' => 'new-password']
    ],
    [
        'attribute' => 'displayName',
        'visible' => true,
        'format' => 'raw',
        'filterInputOptions' => ['autocomplete' => 'new-password']
    ],
    [
        'label' => 'Count',
        'visible' => true,
        'contentOptions' => ['class' => 'text-center'],
        'headerOptions' => ['class' => 'text-center'],
        'format' => 'raw',
        'value' => function ($model){
            $count = $model['memberCount'];
            $style = ($count > 0) ? '' : 'style="background-color:gray"';
            return  '<span class="badge new" '   .  
                    $style . '><strong>' . $count .
                    '</strong></span>';
        },
        'filterInputOptions' => ['autocomplete' => 'new-password']
    ],
    [
        'label' => 'Owned by',
        'attribute' => 'creator',
        'visible' => true,
        'filterInputOptions' => ['autocomplete' => 'new-password']
    ],
    [
        'label' => 'Creation Timestamp',
        'attribute'=>'creationTimestamp',
        'format' => 'date',
        'visible' => true,
        'filter' => true,
        'filterType' => GridView::FILTER_DATE,
        'filterInputOptions' => [
            'autocomplete' => 'new-password'
        ],
        'filterWidgetOptions' => [
            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'clearBtn' => true,
                'todayHighlight' => true,
            ],
            'pluginEvents' => [
                'clearDate' => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
    ],
    [
        'attribute' => 'permission',
        'label' => 'Access Type',
        'visible' => true,
        'format' => 'raw',
        'noWrap'=>false,
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>[
            'owned' => 'Owned',
            'shared' => 'Shared with me'
        ], 
        'filterWidgetOptions'=>[
            'pluginOptions'=>[
                'allowClear'=>true,
                'id'=>"select-permission-data-browser-id"
            ]
        ],
        'filterInputOptions'=>[
            'placeholder'=>'access',
            'id'=>"select-permission-"
        ],
        'value' => function ($model) use ($userId) {
            if ($userId == $model['creatorDbId']) {
                return '<span class="new badge blue darken-3">Owned</span>';
            } else {
                return '<span class="new badge yellow darken-3">Shared with me</span>';
            }
            
        }
    ]
];

// DynaGrid configuration
DynaGrid::begin([
    'columns' => $defaultColumns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'export' => [
            'showConfirmAlert' => false,
        ],
        'exportConfig' => [],
        'id' => strtolower($action).'-template-lists-grid-table-con',
        'tableOptions' => [
            'class' => strtolower($action).'-template-lists-grid-table'
        ],
        'options' => [
            'id' => strtolower($action).'-template-lists-grid-table-con'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id'=> $browserId,  
                'enablePushState' => false
            ],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => [
            'heading' => false,
            'before' => '<div>' .
                        \Yii::t(
                            'app', 
                            'This is the browser for available lists. ' .
                            'Select a list, and click the download action button to generate a ' .
                            'template based on the selected list.') .
                        '</div>',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' =>
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        $resetUrl,
                        [
                            'id' => 'reset-'.$action.'-template-lists',
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app','Reset grid'),
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    ) . '{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'resizableColumns' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options'=> [
        'id'=> $browserId,
    ]
]);

DynaGrid::end();

echo '</div>';

// modals

// view
Modal::begin([
    'id' => 'view-list-members-modal',
    'header' => '
        <h4>
            <i class="material-icons" style="vertical-align:bottom">format_list_numbered</i> View List
            <span id="listmgt-view-name" style="color: green;font-weight: bold;font-size:x-large;"></span>
        </h4>',
    'footer' => Html::a('Close', [''], [
                    'id' => 'close-view-list-members-btn',
                    'class' => 'modal-close-button',
                    'title' => \Yii::t('app', 'Close'),
                    'data-dismiss' => 'modal'
                ]) . '&emsp;&nbsp;',
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin fade modal in',
        'role' => 'dialog',
        'tabindex' =>"-1",
        'data-backdrop' => "static"
    ],
    'closeButton' => ['class'=>'hidden'],
]);

Modal::end();

// Define urls
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$viewListUrl = Yii::$app->getUrlManager()->createUrl(['account/list/view', 'program' => $program]);
$downloadTemplateUrl = Url::to([
    '/germplasm/transaction-template/download-template-from-list',
    'program' => $program, 
    'action' => $action, 
    'entity' => $entity
]);

// Browser configurations
$browserId = 'dynagrid-fu-template-lists-grid';
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
    
    var action = '$action';

    $(document).ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        refresh();

        flashTimeout();

        // Reload grid on pjax success
        $(document).on('ready pjax:success', function(e) {
            e.preventDefault();
            e.stopPropagation();
            
            refresh();
        });

        // Return to main browser click event
        $(document).on('click', '#view-list-back-btn', function(e) {
            returnUrl = '$returnUrl';
            window.location.assign(returnUrl);
        })

        // Implement logic for viewing list members
        $('#view-list-members-modal').on('show.bs.modal', function(event){
            $("#view-list-body").html(
                '<div class="progress"><div class="indeterminate"></div></div>'
            );

            var id = $(event.relatedTarget).data("id");
            var viewUrl = "$viewListUrl" +'&id='+id;
            var displayName = $(event.relatedTarget).data("display_name");
            
            $('#listmgt-view-name').html(displayName);

            setTimeout(function(){
                $.ajax({
                    type: 'GET',
                    url: viewUrl,
                    async: false,
                    success: function(data) {
                        $(".modal-body").html(data);
                    }
                });	
            }, 500);
        });

        // Export template from saved list
        $(document).on('click', '.download-from-list-btn', function(e) {
            var downloadTemplateUrl = '$downloadTemplateUrl'

            var listId = $(this).data("id")
            var templateType = $(this).data("type")

            window.location.href =  "$downloadTemplateUrl"+"&id="+ 
                                    listId+"&templateType="+templateType;
        })

        // display and hide loading indicator
        $(document).on('click','.hide-loading',function(){
            $('#system-loading-indicator').css('display','block');
            setTimeout(function() {
                $('#system-loading-indicator').css('display','none');
            },3000);
        });
    })

    /**
    * Resize grid table upon loading 
    */
    function refresh() {
        var maxHeight = ($(window).height() - 250);
        $(".kv-grid-wrapper").css("height", maxHeight);
        $('.dropdown-trigger').dropdown();
    }

    /**
     * Add a timeout for flash messages
     */
    function flashTimeout() {
        time = 7.5;
        if ($("#w7-info-0") || $("#w7-warning-0")){
            // remove flash message after the specified time in seconds (7.5 seconds by default)
            setTimeout(
                function() 
                {
                    $('#w7-info-0').fadeOut(250);
                    $('#w7-warning-0').fadeOut(250);
                }
            , 1000 * time);
        }
    }
JS);