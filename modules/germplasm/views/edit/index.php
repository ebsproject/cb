<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file for updating a germplasm given ID
 */
use yii\helpers\Url;
use app\models\Germplasm;
use kartik\dynagrid\DynaGrid;

$germplasm = new Germplasm();

$germplasmGeneration = $germplasm->getGermplasmGeneration();
$generationList = json_encode($germplasmGeneration);
$germplasmState = $germplasm->getGermplasmState();
$germplasmStateList = json_encode($germplasmState);
?>

<h3>
    <?=
        Yii::t('app', 'Update Germplasm') .
        ' <small>» ' . $data['designation'] . '</small>'
    ?>
</h3>

<p>Fields marked with <span class="required">*</span> are required. Greyed out fields are automatically provided by the system and cannot be modified.</p>

<div id="edit-germplasm-parent-container" class="container edit-germplasm-modal-container white">
    <form id="update-form">
        <div class="row edit-germplasm-modal-row">
            <div class="col-sm-6 form-column">
                <div class="row edit-germplasm-modal-row">
                    <div class="col-sm-12">
                        <label>Germplasm <span class="required">*</span></label>
                        <select id="designation-select" class="form-control" required></select>
                    </div>
                </div>
                <div class="row edit-germplasm-modal-row">
                    <div class="col-sm-12">
                        <label>Parentage <span class="required">*</span></label>
                        <input id="parentage-field" type="text" required>
                    </div>
                </div>
                <div class="row edit-germplasm-modal-row">
                    <div class="col-sm-12">
                        <label>Generation <span class="required">*</span></label>
                        <select id="generation-select" class="form-control" required></select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 form-column">
                <div class="row edit-germplasm-modal-row">
                    <div class="col-sm-12">
                        <label>Germplasm Name Type <span class="required">*</span></label>
                        <input id="germplasm_name_type-field" type="text" required>
                    </div>
                </div>
                <div class="row edit-germplasm-modal-row">
                    <div class="col-sm-12">
                        <label>Germplasm Normalized Name <span class="required">*</span></label>
                        <input id="germplasm_normalized_name-field" type="text" required>
                    </div>
                </div>
                <div class="row edit-germplasm-modal-row">
                    <div class="col-sm-12">
                        <label>Germplasm State <span class="required">*</span></label>
                        <select id="germplasm_state-select" class="form-control" required></select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row edit-germplasm-modal-row">
            <br>
            <div style="float: right;">
                <a class="close-cancel-dialog-btn" href="javascript:history.go(-1)">Cancel</a> &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-primary" value="Save">Save <i class="material-icons right">send</i></button>
            </div>
        </div>
    </form>
</div>

<?php
$data = json_encode($data);
$entityId = json_encode($entityId);
$UpdateGermplasmUrl = Url::to(['/germplasm/edit/update']);

$this->registerJs(<<<JS
    var data = $data;

    function populateInputFields() {
        $('#parentage-field').val(data['parentage']);
        $('#germplasm_name_type-field').val(data['germplasmNameType']).prop('disabled', true);
        $('#germplasm_normalized_name-field').val(data['germplasmNormalizedName']).prop('disabled', true);
    }

    function initializeSelect2Fields() {
        $('#designation-select').select2({
            data: [data['designation']],
            dropdownParent: $('#edit-germplasm-parent-container')
        }).val(data['designation']).trigger('change').prop('disabled', true); // disabled

        var generationList = JSON.parse('$generationList');
        var generationListSelectData = [];
        for(var generation of generationList) {
            generationListSelectData.push({
                id: generation['value'],
                text: generation['value']  // generation
            });
        }
        $('#generation-select').select2({
            data: generationListSelectData,
            dropdownParent: $('#edit-germplasm-parent-container')
        }).val(data['generation']).trigger('change');

        var germplasmStateList = JSON.parse('$germplasmStateList');
        var germplasmStateListSelectData = [];
        for(var germplasmState of germplasmStateList) {
            germplasmStateListSelectData.push({
                id: germplasmState['value'],
                text: germplasmState['value']  // germplasmState
            });
        }
        $('#germplasm_state-select').select2({
            data: germplasmStateListSelectData,
            dropdownParent: $('#edit-germplasm-parent-container')
        }).val(data['germplasmState']).trigger('change');
    }

    $('#update-form').on('submit', function(e) {
        e.preventDefault();
        var newValues = {};
        newValues['designation'] = $('#designation-select').select2('data')[0].id;
        newValues['parentage'] = $('#parentage-field').val();
        newValues['generation'] = $('#generation-select').select2('data')[0].id;
        newValues['germplasmState'] = $('#germplasm_state-select').select2('data')[0].id;

        setTimeout(function(){
            $.ajax({
                url: '$UpdateGermplasmUrl',
                data: {
                    id: $entityId,
                    data: newValues
                },
                type: 'POST',
                async: true,
                success: function(data) {
                    if(data == true) {  // Germplasm was updated successfully
                        var notif = '<i class="material-icons green-text">check</i> Successfully updated ' + newValues['designation'] + '.';
                        Materialize.toast(notif, 5000);
                        window.location = 'javascript:history.go(-1)';  // redirect
                    } else {    // Germplasm was not updated successfully
                        var notif = '<i class="material-icons red-text">close</i> There was a problem while updating germplasm. Please try again.';
                        Materialize.toast(notif, 5000);
                    }
                },
                error: function(){
                    var notif = '<i class="material-icons orange-text">warning</i> There was a problem while updating germplasm. Please try again.';
                    Materialize.toast(notif, 5000);
                }
            });
        }, 300);
    });

    $(document).ready(function(e) {
        populateInputFields();
        initializeSelect2Fields();
    });
JS
);

Yii::$app->view->registerCss('
    .edit-germplasm-modal-container {
        padding: 10px 15px 20px 15px !important;
    }
    .edit-germplasm-modal-row {
        margin-bottom: 5px;

    }
    .edit-germplasm-modal-row > .col {
        padding: 0 5px;

    }
    .form-column {
        padding-right: 75px;
    }

    .required {
        color:red;
    }

    label {
        margin-top: 15px;
        font-size: 1rem;
        color: #333333;
    }

    .select2-container {
        margin-top: 10px;
    }
    .select2-dropdown {
        margin-top: -10px;
    }
    select {
        padding-right: 15px;
    }
');

?>
<p><?= \Yii::t('app', 'List of historical events performed on a germplasm.') ?></p>
<?= DynaGrid::widget([
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'action_type',
            'value' => function ($data) {
                $actionType = $data['action_type'];
                $spanClass = 'default';

                switch ($actionType) {
                    case 'INSERT':
                        $spanClass = 'green';
                        break;
                    case 'UPDATE':
                        $spanClass = 'blue';
                        break;
                    case 'DELETE':
                        $spanClass = 'red';
                        break;
                    case 'VOID':
                        $spanClass = 'default';
                        break;
                    case 'RESTORE':
                        $spanClass = 'default';
                        break;
                }

                return '<span class="new badge '.$spanClass.'">'.$actionType.'</span>';
            },
            'format' => 'raw',
            'contentOptions'=>[
                'style'=>'min-width:100px;'
            ]
        ],
        'actor',
        'log_timestamp',
        [
            'attribute'=>'row_data',
            'format' => 'html'
        ],
        [
            'attribute'=>'new_data',
            'format' => 'html'
        ]
    ],
    'theme'=>'panel-default',
    'gridOptions'=>[
        'export'=>false,
        'id' => 'germplasm-audit-grid-id',
        'dataProvider'=>$eventLogDataProvider,
        'panel'=>[
            'heading'=>'',
            'before' => false,
            'after' => false
        ],
        'toolbar'=>[]
    ],
    'options'=>['id'=>'germplasm-audit-grid'],
]) ?>