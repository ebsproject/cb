<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

// import browser helpers
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

use app\components\FileUploadErrorLogsWidget;
use app\components\FileUploadSummaryWidget;
use app\components\DownloadFileDataCSV;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

echo '<div id="gefu-bg-alert-div"></div>';

/**
 * Renders Create tab for Germplasm
 */

echo Yii::$app->controller->renderPartial('@app/modules/germplasm/views/default/tabs.php',[
    'controller' => 'create',
    'program' => $program
]);

$totalResultsCount = $dataProvider->totalCount;
$module = 'create';
$notificationsUrl = Url::to(['/germplasm/default/push-notifications','module' => $module]);
$newNotificationsUrl = Url::toRoute(['/germplasm/default/new-notifications-count','module' => $module]);

$enableDeleteArr = ['in queue','validation error','validated','creation failed'];
$enableErrorLogsStatusArr = ['validation error','creation failed'];
$resetUrl = Url::to(['/germplasm/create', 'program'=>$program]);
            
// TEMPLATE BUTTON
$templateButtonTitleText = Yii::t('app', 'Template');
$optionTemplateForGermplasmText = Yii::t('app', 'Germplasm');
$optionTemplateForGermplasmRelationText = Yii::t('app', 'Germplasm relation');

$templateButton = "
    <a
        title='$templateButtonTitleText'
        id='gm-create-template-btn'
        class='dropdown-trigger btn'
        style='margin-left:5px;  margin-right:5px;'
        data-activates='gm-create-template-actions-list'
        data-beloworigin='true'
        data-constrainwidth='false'
    >
        ".$templateButtonTitleText."
        <span class='caret'></span>
    </a>
    
    <ul id='gm-create-template-actions-list' class='dropdown-content hide-loading'>
        <!-- Template for germplasm -->
        <li
            id='gm-create-template-germplasm-btn'>" .
            Html::a(
                $optionTemplateForGermplasmText
            ) . "
        </li>
        <!-- Template for germplasm relation -->
        <li
            id='gm-create-template-germplasm-relation-btn'>" .
            Html::a(
                $optionTemplateForGermplasmRelationText
            ) . "
        </li>
    </ul>
    
";

// UPLOAD BUTTON
$uploadButtonTitleText = Yii::t('app', 'Upload');
$optionUploadGermplasmText = Yii::t('app', 'Germplasm');
$optionUploadGermplasmRelationText = Yii::t('app', 'Germplasm relation');

$uploadButton = "
    <a
        title='$uploadButtonTitleText'
        id='gm-create-upload-btn'
        class='dropdown-trigger btn'
        style='margin-left:5px;  margin-right:5px;'
        data-activates='gm-create-upload-actions-list'
        data-beloworigin='true'
        data-constrainwidth='false'
    >
        ".$uploadButtonTitleText."
        <span class='caret'></span>
    </a>
    
    <ul id='gm-create-upload-actions-list' class='dropdown-content hide-loading'>
        <!-- Upload germplasm -->
        <li
            id='gm-create-upload-germplasm-btn'
            class='gm-create-upload-btn'
            type='germplasm'
            >" .
            Html::a(
                $optionUploadGermplasmText
            ) . "
        </li>
        <!-- Upload germplasm relation -->
        <li
            id='gm-create-upload-germplasm-relation-btn'
            class='gm-create-upload-btn'
            type='germplasm_relation'
            >" .
            Html::a(
                $optionUploadGermplasmRelationText
            ) . "
        </li>
    </ul>
    
";

// columns in germplasm file upload transaction browser
$defaultColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'template' => ' {complete} {delete} {view} {errors} {download}',
        'buttons' => [
            'complete' => function ($url, $model) {
                if($model['fileStatus'] == 'validated'){
                    return Html::a('<i class="material-icons">check_circle</i>',
                        '#',
                        [
                            'class' => 'complete-gefu',
                            'title' => 'Complete File Upload Transaction',
                            'data-entity_id' => $model['germplasmFileUploadDbId'],
                            'file_program_code' => $model['programCode']
                        ]
                    );
                }                
            },
            'delete' => function ($url, $model) use ($enableDeleteArr) {
                if(in_array($model['fileStatus'],$enableDeleteArr)){
                    return Html::a('<i class="material-icons">delete</i>',
                        '#',
                        [
                            'class' => 'delete-gefu',
                            'title' => 'Delete File Upload Transaction',
                            'data-entity_id' => $model['germplasmFileUploadDbId'],
                            'data-toggle' => "modal",
                            'data-target' => "#gfu-confirm-delete-modal",
                        ]
                    );
                }        
            },
            'view' => function ($url, $model) use ($resetUrl) {
                if($model['fileStatus'] == 'completed'){
                    $configAbbrevPrefix = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_';
                    $type = $model['remarks'] ?? 'germplasm';
                    $action = $model['fileUploadAction'] ?? 'create';
                    if($type !== 'germplasm') {
                        $configAbbrevPrefix = 'GM_' . strtoupper($action) . '_'
                            . strtoupper($type) . '_';
                    }

                    return FileUploadSummaryWidget::widget([
                        "sourceToolName" => "Germplasm",
                        "fileDbId" => $model['germplasmFileUploadDbId'],
                        "returnUrl" => $resetUrl,
                        "configAbbrevPrefix" => $configAbbrevPrefix
                    ]);
                }
            },
            'errors' => function ($url, $model) use ($enableErrorLogsStatusArr, $resetUrl){
                if(in_array($model['fileStatus'],$enableErrorLogsStatusArr)){
                    return FileUploadErrorLogsWidget::widget([
                        "sourceToolName" => "Germplasm",
                        "fileDbId" => $model['germplasmFileUploadDbId'],
                        "returnUrl" => $resetUrl
                    ]);
                }
            },
            'download' => function ($url, $model) use ($resetUrl) {
                if(in_array($model['fileStatus'], ['validation error','completed'])){
                    $configAbbrevPrefix = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_';
                    $type = $model['remarks'] ?? 'germplasm';
                    $action = $model['fileUploadAction'] ?? 'create';
                    if($type !== 'germplasm') {
                        $configAbbrevPrefix = 'GM_' . strtoupper($action) . '_'
                            . strtoupper($type) . '_';
                    }
                    
                    return DownloadFileDataCSV::widget([
                        "sourceToolName" => "Germplasm",
                        "fileDbId" => $model['germplasmFileUploadDbId'],
                        "returnUrl" => "none",
                        "configAbbrevPrefix" => $configAbbrevPrefix
                    ]);
                }
            },
        ],
        'vAlign' => 'top',
        'header' => false,
        'noWrap' => true,
        'visible' => false
    ],
    [
        'attribute'=>'fileStatus',
        'contentOptions'=>[
            'class' => 'gefu-status-col'
        ],
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
            'IN QUEUE' => 'queued for validation',
            'VALIDATION IN PROGRESS' => 'validation in progress',
            'VALIDATED' => 'successfully validated',
            'VALIDATION ERROR' => 'failed validation',
            'CREATION IN PROGRESS' => 'creation in progress',
            'CREATION FAILED' => 'failed creation of records',
            'COMPLETED' => 'completed creation of records'
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'multiple'=>false, 
                'allowClear' => true
            ],
        ],
        'filterInputOptions' => [
            'placeholder' => 'Select status'
        ],
        'value' => function($model){
            return !empty($model['fileStatus']) ? $model['fileStatus'] : 'unknown';
        },
        'content' => function ($model) {
            $fileStatus = strtoupper($model['fileStatus']);

            switch($fileStatus){
                case 'IN QUEUE':
                    return '<span title="Queued for validation" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'IN QUEUE') . '</strong></span>';
                case 'VALIDATION IN PROGRESS':
                    return '<span title="Ongoing validation of transaction" class="new badge orange darken-2"><strong>' . \Yii::t('app', 'VALIDATION IN PROGRESS') . '</strong></span>';
                case 'VALIDATED':
                    return '<span title="Validation completed" class="new badge blue darken-2"><strong>' . \Yii::t('app', 'VALIDATED') . '</strong></span>';
                case 'VALIDATION FAILED':
                    return '<span title="Validation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'VALIDATION FAILED') . '</strong></span>';
                case 'VALIDATION ERROR':
                    return '<span title="Validation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'VALIDATION FAILED') . '</strong></span>';
                case 'CREATION IN PROGRESS':
                    return '<span title="Ongoing creation of records" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'CREATION IN PROGRESS') . '</strong></span>';
                case 'CREATION FAILED':
                    return '<span title="Creation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'CREATION FAILED') . '</strong></span>';
                case 'COMPLETED':
                    return  '<span title="Creation completed" class="new badge green darken-2"><strong>' . \Yii::t('app', 'COMPLETED') . '</strong></span>';
                default:
                    return '<span title="Status unknown" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
            }
        },
        'headerOptions'=>[
            'style' => 'text-align:left'
        ],
        'visible' => false
    ],
    [
        'attribute'=>'fileName',
        'contentOptions'=>[
            'class' => 'gefu-file-name-col'
        ],
        'value' => function($model){
            return !empty($model['fileName']) ? $model['fileName'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute'=>'germplasmCount',
        'contentOptions'=>[
            'class' => 'gefu-germplasm-count-col'
        ],
        'value' => function($model){
            return !empty($model['germplasmCount']) ? $model['germplasmCount'] : 0;
        },
        'content' => function($model){
            return !empty($model['germplasmCount']) ? $model['germplasmCount'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute'=>'seedCount',
        'contentOptions'=>[
            'class' => 'gefu-seed-count-col'
        ],
        'value' => function($model){
            return !empty($model['seedCount']) ? $model['seedCount'] : 0;
        },
        'content' => function($model){
            return !empty($model['seedCount']) ? $model['seedCount'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute'=>'packageCount',
        'contentOptions'=>[
            'class' => 'gefu-package-count-col'
        ],
        'value' => function($model){
            return !empty($model['packageCount']) ? $model['packageCount'] : 0;
        },
        'content' => function($model){
            return !empty($model['packageCount']) ? $model['packageCount'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute'=>'uploader',
        'contentOptions'=>[
            'class' => 'gefu-uploader-col'
        ],
        'value' => function($model){
            return !empty($model['uploader']) ? $model['uploader'] : 0;
        },
        'content' => function($model){
            return !empty($model['uploader']) ? $model['uploader'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute'=>'uploadTimestamp',
        'contentOptions'=>[
            'class' => 'gefu-upload-ts-col'
        ],
        'value' => function($model){
            return !empty($model['uploadTimestamp']) ? $model['uploadTimestamp'] : 0;
        },
        'content' => function($model){
            return !empty($model['uploadTimestamp']) ? $model['uploadTimestamp'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
];

$browserId = 'dynagrid-germplasm-gefu-grid';

//dynagrid configuration
$dynagrid = DynaGrid::begin([
    'columns' => $defaultColumns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'export' => [
            'showConfirmAlert' => false,
        ],
        'exportConfig' => [],
        'id' => $browserId.'-table-con',
        'tableOptions'=>[
            'class'=>$browserId.'-table'
        ],
        'options'=>[
            'id'=>$browserId.'-table-con'
        ],
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>$browserId.'-id'
            ],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsive' => true,
        'responsiveWrap'=>false,
        'panel'=>[
            'heading' => false,
            'before' => '<br /><div>{summary}<p>'.\Yii::t('app', 'This is the browser for all uploaded germplasm creation transactions. <b>Transactions across all programs are displayed.</b>') . '</p></div>' ,
            'after' => false,
        ],
        'toolbar' =>  [
            [
                'content'=> "" .
                    $templateButton .
                    $uploadButton .
                    '&nbsp;' .
                    Html::a(
                        '<i class="material-icons large">notifications_none</i>
                        <span id="notif-badge-id" class=""></span>',
                        '#',
                        [
                            'id' => 'gefu-create-notifs-btn',
                            'class' => 'btn waves-effect waves-light harvest-manager-notification-button hm-tooltipped',
                            'data-pjax' => 0,
                            'style' => "overflow: visible !important;",
                            "data-position" => "top",
                            "data-tooltip" => \Yii::t('app', 'Notifications'),
                            "data-activates" => "notifications-dropdown",
                            'disabled' => false,
                        ]
                    ) . 
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [Yii::$app->controller->action->id, 'program'=>$program],
                        [
                            'id' => 'reset-gefu-grid', 
                            'class' => 'btn btn-default', 
                            'title'=>\Yii::t('app','Reset grid'),
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    ) . '{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
            '{export}'
        ],
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'resizableColumns'=>true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options'=>[
        'id'=>$browserId
    ]
]);

DynaGrid::end();

echo '</div>';

// germplasm file upload modal
Modal::begin([
    'id' => 'germplasm-file-upload-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">file_upload</i><span id="gm-create-upload-modal-title">Germplasm File Upload</span></h4>',
    'footer' => Html::a('Cancel', [''], [
                    'id' => 'cancel-germplasm-file-upload-btn',
                    'class' => 'modal-close-button',
                    'title' => \Yii::t('app', 'Cancel'),
                    'data-dismiss' => 'modal'
                ]) . '&emsp;&nbsp;',
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin'
    ],
    'closeButton' => ['class'=>'hidden'],
]);
echo '<div class="germplasm-file-upload-modal-body parent-panel"></div>';
Modal::end();

// Modal for confirming delete
Modal::begin([
    'id' => 'gfu-confirm-delete-modal',
    'header' => '<h4>Delete File Upload Transaction</h4>',
    'footer' =>
    Html::a(\Yii::t('app','Cancel'), ['#'],
            [
                'id'=>'gfu-delete-cancel-btn',
                'data-dismiss'=>'modal'
            ]
        ). '&emsp;'
    . Html::a(\Yii::t('app', 'Confirm'), '#', 
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close',
            'id' => 'gfu-delete-proceed-btn',
            'title' => \Yii::t('app', 'Proceed')
        ]
    ) . '&emsp;',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="germplasm-file-upload-confirm-delete-modal"></div>';
Modal::end();

$renderFileUploadFormUrl = Url::to(['/germplasm/create/render-file-upload']);
$urlRenderCreationSummary = Url::to(['create/render-creation-summary']);
$urlRenderFileUploadPreview = Yii::$app->getUrlManager()->createUrl(['germplasm/create/file-upload-preview']);
$urlRenderErrorLogPage = Yii::$app->getUrlManager()->createUrl(['germplasm/create/error-log-page']);
$urlRenderFileUploadPage = Url::to(['view-file-upload/view-records']);
$urlInvokeGermplasmWorker = Url::to(['create/invoke-germplasm-worker']);
$downloadTemplateProcessUrl = Url::to(['create/download-template-process','program'=>$programDbId]);
$templateDownloadUrl = Url::to(['create/download-template','program'=>$program,'action'=>'create']);
$deleteFileUploadRecord = Url::to(['/germplasm/create/delete-file-upload-record']);

(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<?php
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
$pjaxReloadUrl = "/index.php/germplasm/create/index?program=$program";
// Check if browser Id is empty (occurrence tab)
$browserId = empty($browserId) ? 'none' : $browserId;

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ". json_encode($browserId) .",
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    gmNewNotificationsUrl = '".$newNotificationsUrl."',
    gmNotificationsUrl = '".$notificationsUrl."',
    gmNotifsBtnId = 'gefu-create-notifs-btn',
    gmBrowserId = 'dynagrid-germplasm-gefu-grid',
    gmPjaxReloadUrl = '".$pjaxReloadUrl."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

// add global js for notifications
$this->registerJsFile("@web/js/germplasm-manager/notifications.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS

    var notificationsUrl = '$notificationsUrl';
    var newNotificationsUrl = '$newNotificationsUrl';

    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
    var selectedIdComplete = 0;
    var selectedIdDelete = 0;

    var browserId = '$browserId';

    $("document").ready(function(){
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        flashTimeout();

        // File upload button click event
        $(document).on('click', '.gm-create-upload-btn', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var modalBody = '.germplasm-file-upload-modal-body';

            // Add modal title based on upload type
            var type = $(this).attr('type');
            var typeParts = type.split('_')
            var typeTextArray = []
            for(part of typeParts) {
                typeTextArray.push(part[0].toUpperCase() + part.substring(1));
            }
            var typeText = typeTextArray.join(" ");
            $('#gm-create-upload-modal-title').html(typeText + ' File Upload');
            // Show modal
            $(modalBody).html(loading);
            $('#germplasm-file-upload-modal').modal('show');

            $.ajax({
                url: '$renderFileUploadFormUrl',
                type: 'post',
                dataType: 'json',
                data:{
                    programCode: '$program',
                    type: type
                },
                success: function(response){
                    $(modalBody).html(response);
                },
                error: function(){
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBody).html(errorMessage);
                }
            });
        });

        /**
         * On complete button click event
         */
        $(document).on('click', '.complete-gefu', function(e) {
            selectedIdComplete = $(this).attr('data-entity_id');
            programCode = $(this).attr('file_program_code');
            // Redirect to preview page
            window.location.href = "$urlRenderFileUploadPreview?program=" + programCode + "&germplasmFileUploadDbId=" + selectedIdComplete;
        });

        /**
         * On delete button click event
         */
        $(document).on('click', '.delete-gefu', function(e) {
            selectedIdDelete = $(this).attr('data-entity_id');
            // Add message to modal body
            $("#germplasm-file-upload-confirm-delete-modal").html('<p style="font-size:115%;">This will delete the file upload and its contents. Click confirm to proceed.</p>');
            // Enable proceed button
            $("#gfu-delete-proceed-btn").removeClass('disabled');
        });

        /**
         * On deletion proceed button click event
         */
        $(document).on('click', '#gfu-delete-proceed-btn', function(e) {
            // Disable proceed button
            $(this).addClass('disabled');
            // Add progress bar
            modalBody = "#germplasm-file-upload-confirm-delete-modal"
            $(modalBody).html('<div class="progress"><div class="indeterminate"></div></div>');

            // Perform delete
            $.ajax({
                url: '$deleteFileUploadRecord',
                type: 'post',
                data:{
                    germplasmFileUploadDbId: selectedIdDelete
                },
                success: function(response){
                    // Hide modal
                    $("#gfu-confirm-delete-modal.in").modal('hide');
                    // Display toast message
                    var notif = "<i class='material-icons green-text'>check</i>&nbsp;The file upload transaction was successfully deleted.";
                    Materialize.toast(notif, 3500);
                    // refresh browser
                    $.pjax.reload({
                        container: '#'+browserId+'-pjax',
                        url: '/index.php/germplasm/create/index?program=$program'
                    });
                },
                error: function(){
                    $("#gfu-confirm-delete-modal.in").modal('hide');
                    var notif = "<i class='material-icons red-text'>close</i>&nbsp;An error occurred. The file upload transaction was not deleted.";
                    Materialize.toast(notif, 3500);
                }
            });
        });

        /**
         * On grid reset button click event
         */
        $(document).on('click', '#reset-gefu-grid', function(e) {
            // refresh browser
            $.pjax.reload({
                container: '#'+browserId+'-pjax',
                url: '/index.php/germplasm/create/index?program=$program'
            });
        });

        /**
         * On error button click event
         */
        $(document).on('click', '.errors-gefu', function(e) {
            selectedIdComplete = $(this).attr('data-entity_id');
            // Redirect to error log page
            window.location.href = "$urlRenderErrorLogPage?germplasmFileUploadDbId=" + selectedIdComplete;
        });

        /**
         * On view button click event
         */
        $(document).on('click', '.view-gefu', function(e) {
            selectedIdComplete = $(this).attr('data-entity_id');
            programCode = $(this).attr('file_program_code');
            // Redirect to view file upload page
            window.location.href = "$urlRenderFileUploadPage?program=" + programCode + "&germplasmFileUploadDbId=" + selectedIdComplete + "&entity=germplasm";
        });

        // reload grid on pjax
        $(document).on('ready pjax:success', function(e) {
            e.preventDefault();
            e.stopPropagation();

            notifDropdown();
            
            $('#gefu-create-notifs').tooltip();
            $('#notif-container').html(
                '<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>'
                );

            renderNotifications();
            updateNotif();

            refresh();
        });

        // display and hide loading indicator
        $(document).on('click','.hide-loading',function(){
            $('#system-loading-indicator').css('display','block');
            setTimeout(function() {
                $('#system-loading-indicator').css('display','none');
            },3000);
        });

        notifDropdown();
        renderNotifications();
        updateNotif();

        // Check for new notifications every 7.5 seconds
        notificationInterval=setInterval(updateNotif, 7500);

        refresh();
        
        // generates and downloads the CSV Germplasm File Upload template
        $(document).on('click', '#gm-create-template-germplasm-btn', function(e) {
            let downloadTemplateProcessUrl = '$downloadTemplateProcessUrl'

            let templateFileUrl = window.location.origin + downloadTemplateProcessUrl +
            '&redirectUrl=' + encodeURIComponent(window.location.href)

            window.location.assign(templateFileUrl)
        });

        // Generate template for creating germplasm relations
        $(document).on('click', '#gm-create-template-germplasm-relation-btn', function(e) {
            let templateDownloadUrl = '$templateDownloadUrl'

            let templateFileUrl = window.location.origin + templateDownloadUrl +
            '&type=germplasm_relation' +
            '&redirectUrl=' + encodeURIComponent(window.location.href)

            window.location.assign(templateFileUrl)
        });
    });

    /**
     * Generic function for invoking a background worker
     * @param Integer germplasmFileUploadDbId germplasm file upload identifier
     * @param String workerName name of the worker to invoke
     * @param String description general description of what the worker does
     * @param String description general description of what the worker does
     * @param String modalID (optional) modal ID to be closed (if any)
     * @param String modalBody (optional) modal body to be used for displaying error messages
     */
    function invokeGermplasmWorker(germplasmFileUploadDbId, workerName, description, modalID = null, modalBody = null) {
        if(modalBody != null) $("#" + modalBody).html('<div class="progress"><div class="indeterminate"></div></div>');

        // Invoke background worker
        $.ajax({
            url: '$urlInvokeGermplasmWorker',
            type: 'post',
            data:{
                germplasmFileUploadDbId: germplasmFileUploadDbId,
                workerName: workerName,
                description: description
            },
            success: function(response){
                var result = JSON.parse(response);
                var success = result.success;
                var status = result.status;
                
                // Display flash message
                if(success) {
                    // Success message
                    displayCustomFlashMessage(
                        'info', 
                        'fa fa-info-circle',
                        'The germplasm records are being created in the background. You may proceed with other tasks. You will be notified in this page once done.',
                        'gefu-bg-alert-div',
                        10
                    );
                }
                else {
                    // Failure message
                    let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status != 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if(status == null) message += ' (#002)';

                    displayCustomFlashMessage(
                        'warning', 
                        'fa-fw fa fa-warning', 
                        message,
                        'gefu-bg-alert-div'
                    );
                }
                if(modalID != null) $("#" + modalID).modal('hide');
                if(modalBody != null) $("#" + modalBody).html('');

                // refresh browser
                $.pjax.reload({
                    container: '#'+browserId+'-pjax',
                    url: '/index.php/germplasm/create/index?program=$program'
                });
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                if(modalBody != null) $("#" + modalBody).html(errorMessage);
            }
        });
    }


    /**
     * Displays custom flash message
     * @param {string} status the status of the message (info, success, warning, error)
     * @param {string} iconClass icon of flash message
     * @param {string} message the message to display
     * @param {string} parentDivId id of the div where the flash message is to be inserted
     * @param {float} time time in seconds before the flash message is removed from the display. 7.5 seconds by default.
     */
    function displayCustomFlashMessage(status, iconClass, message, parentDivId, time = 7.5) {
        // Add flash message to parent div
        $('#' + parentDivId).html(
            '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
                '<div class="card-panel">' +
                    '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                    message +
                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                '</div>' +
            '</div>'
        );

        // remove flash message after the specified time in seconds (7.5 seconds by default)
        setTimeout(
            function() 
            {
                $('#w25-' + status + '-0').fadeOut(250);
                setTimeout(
                    function() 
                    {
                        $('#' + parentDivId).html('');
                    }
                , 500);
            }
        , 1000 * time);
    }

    /**
     * Updates notifications
     */
    function updateNotif() {
        $.getJSON(newNotificationsUrl, 
            {},
            function(json){
                $('#notif-badge-id').html(json);

                if(parseInt(json)>0){
                    $('#notif-badge-id').addClass('notification-badge red accent-2');
                }else{
                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                }
        })
        .fail( function(jqXHR, textStatus, errorThrown) {
            console.log("Message: "+textStatus+" : "+errorThrown);
        });
    }

    /**
     * Initialize dropdown materialize  for notifications
     */
    function renderNotifications(){
        $('#gefu-create-notifs-btn').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            belowOrigin: true,
            alignment: 'right',
            stopPropagation: true
        });
    }

    /**
     * Renders the notification in dropdown UI
     */
    function notifDropdown(){
        $('#gefu-create-notifs-btn').on('click',function(e){
            $.ajax({
                url: notificationsUrl,
                type: 'post',
                data: {},
                async:false,
                success: function(data) {

                    var pieces = data.split('||');
                    var height = pieces[1];

                    $('<style>#notifications-dropdown.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');

                    data = pieces[0];

                    renderNotifications();

                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                    $('#notifications-dropdown').html(data);

                    $('.bg_notif-btn').on('click', function(e){
                        var notifHTML = $(this).html();

                        // refresh browser
                        $.pjax.reload({
                            container: '#'+browserId+'-pjax',
                            url: '/index.php/germplasm/create/index?program=$program'
                        });
                    });
                },
                error: function(xhr, status, error) {
                    var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');

                    Materialize.toast(toastContent, 'x', 'red');
                    $('.toast-error').collapsible();
                }
            });
        });
    }

    function refresh(){
        var maxHeight = ($(window).height() - 350);
        $(".kv-grid-wrapper").css("height",maxHeight);
        $('.dropdown-trigger').dropdown();
    }

    /**
     * Add a timeout for flash messages
     */
    function flashTimeout() {
        time = 7.5;
        if ($("#w7-info-0") || $("#w7-warning-0")){
            // remove flash message after the specified time in seconds (7.5 seconds by default)
            setTimeout(
                function() 
                {
                    $('#w7-info-0').fadeOut(250);
                    $('#w7-warning-0').fadeOut(250);
                }
            , 1000 * time);
        }
    }
    
JS
);

$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');

Yii::$app->view->registerCss('
    #dynagrid-germplasm-gefu-grid .kv-grid-wrapper {
        min-height: 500px;
    }

    body{
        overflow-x: hidden;
    }
');

?>