<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders error log for the file data
 */


use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;
use yii\bootstrap\Modal;

use app\models\UserDashboardConfig;

Yii::$app->view->registerCss('
    div.gm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.gm-browser-title {
        display: inline;
    }
');

/**
 * Renders Create tab for Germplasm
 */

echo Yii::$app->controller->renderPartial('@app/modules/germplasm/views/default/tabs.php',[
    'controller' => 'create',
    'program' => $program
]);
?>

<div class="panel panel-default" style="min-height: 500px; overflow-x: hidden;">
    <div class="col-md-12" align="right" style="margin: 0px; padding: 10px;">
        <button class="btn btn-default pull-right lighten-4" id="ge-error-log-return-btn" type="button" style="margin-bottom:5px;">RETURN</button>

        <!-- Header -->
        <div class="gm-browser-title pull-left">
            <h3 class="gm-browser-title">Error Log » <?php echo $fileErrorLogInfo['fileName']; ?></h3>
        </div>
    </div>

    <div class="row" style="margin-bottom:0px;">
    <!-- File upload summary -->
    <?php
        $fileStatus = strtoupper($fileErrorLogInfo['fileStatus']);
        $fileStatusDisplay = '';

        switch($fileStatus){
            case 'IN QUEUE':
                $fileStatusDisplay = '<span title="Queued for validation" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'IN QUEUE') . '</strong></span>';
                break;
            case 'VALIDATION IN PROGRESS':
                $fileStatusDisplay = '<span title="Ongoing validation of transaction" class="new badge orange darken-2"><strong>' . \Yii::t('app', 'VALIDATION IN PROGRESS') . '</strong></span>';
                break;
            case 'VALIDATED':
                $fileStatusDisplay = '<span title="Validation completed" class="new badge blue darken-2"><strong>' . \Yii::t('app', 'VALIDATED') . '</strong></span>';
                break;
            case 'VALIDATION FAILED':
                $fileStatusDisplay = '<span title="Validation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'VALIDATION FAILED') . '</strong></span>';
                break;
            case 'VALIDATION ERROR':
                $fileStatusDisplay = '<span title="Validation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'VALIDATION FAILED') . '</strong></span>';
                break;
            case 'CREATION IN PROGRESS':
                $fileStatusDisplay = '<span title="Ongoing creation of records" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'CREATION IN PROGRESS') . '</strong></span>';
                break;
            case 'CREATION FAILED':
                $fileStatusDisplay = '<span title="Creation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'CREATION FAILED') . '</strong></span>';
                break;
            case 'COMPLETED':
                $fileStatusDisplay =  '<span title="Creation completed" class="new badge green darken-2"><strong>' . \Yii::t('app', 'COMPLETED') . '</strong></span>';
                break;
            default:
                $fileStatusDisplay = '<span title="Status unknown" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
        }
        echo '<div id="occurrence-detail-div" style="margin-left:20px;margin-bottom:0px" >
            <div class="col-md-4">
                <dl>
                    <dt title="File Status">File Status</dt>
                    <dd>' . $fileStatusDisplay . '</dd>
                </dl>
            </div>
            <div class="col-md-4">
                <dl>
                    <dt title="Uploader">Uploader</dt>
                    <dd>' . $fileErrorLogInfo['uploader'] . '</dd>
                </dl>
            </div>
            <div class="col-md-4">
                <dl>
                    <dt title="Upload Timestamp">Upload Timestamp</dt>
                    <dd>' . $fileErrorLogInfo['uploadTimestamp'] . '</dd>
                </dl>
            </div>
        </div>';
    ?>
    </div>


    <?php

    $columns = $fileErrorLogInfo['columns'] ?? [];
    $dataProvider = $fileErrorLogInfo['dataProvider'] ?? [];

    // Render grid
    echo '<div style="padding-left:15px; padding-right:15px;">';
    $dynagrid = DynaGrid::begin([
        'columns'=>$columns,
        'storage'=>DynaGrid::TYPE_SESSION,
        'theme'=>'simple-default',
        'showPersonalize' => true,
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions'=>[
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'tableOptions'=>['class'=>'gm-error-log-grid-table'],
            'options'=>['id'=>'gm-error-log-grid-table-con'],
            'id' => 'gm-error-log-grid-table-con',
            'striped'=>false,
            'responsive' => true,
            'responsiveWrap' => false,
            'floatHeader' => true,
            'floatHeaderOptions' => [
                'scrollingTop' => '0',
                'position' => 'absolute',
            ],
            'hover' => true,
            'floatOverflowContainer' => true,
            'showPageSummary' => false,
            'pjax' => true,
            'pjaxSettings'=>[
                'neverTimeout'=>true,
                'options' => ['id' => 'gm-error-log-grid', 'enablePushState' => true,],
                'beforeGrid'=>'',
                'afterGrid'=>''
            ],
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ],
            'rowOptions'=> [],
            'panel'=> [
                'heading'=> false,
                'before' => 'Review file upload data here. {summary}',
                'after'=> false,
            ],
            'toolbar' =>  [
                [
                    'content' => Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [Yii::$app->controller->action->id . '?germplasmFileUploadDbId=' . $germplasmFileUploadDbId],
                        [
                            'class' => 'btn btn-default hm-tooltipped',
                            'id' => 'hm-crt-reset-plot-btn',
                            'style' => "margin-left:10px;",
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    )
                    . '{dynagridFilter}{dynagridSort}{dynagrid}'

                ],
            ],
        ],
        'options'=>[
            'id'=>'gm-error-log-grid'
            ]
    ]);

    DynaGrid::end();
    echo '</div>';
    ?>

</div>

<?php

$urlRedirectToMainCreatePage = Url::to(['create/redirect-to-main-create-page']);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');

// Check if browser Id is empty (occurrence tab)
$browserId = empty($browserId) ? 'none' : $browserId;

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ". json_encode($browserId) .",
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS
    var germplasmFileUploadDbId = $germplasmFileUploadDbId;
    // Adjust browser height
    $('#gm-error-log-grid-table-con-container').css('min-height','450px');

    $("document").ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        // Perform after pjax completion of browser
        $(document).on('ready pjax:complete','#gm-error-log-grid-pjax', function(e) {
            // Adjust browser height
            $('#gm-error-log-grid-table-con-container').css('min-height','450px');
        });

        /**
         * On return button click event
         */
        $(document).on('click', '#ge-error-log-return-btn', function(e) {
            // Redirect
            $.ajax({
                url: '$urlRedirectToMainCreatePage',
                type: 'post',
                data:{},
                success: function(response){ }
            });
        });
    });
JS;

$this->registerJs($script);

?>