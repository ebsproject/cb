<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders view for creation summary
*/

use kartik\grid\GridView;

?>

<!-- Display information -->
<p style='font-size:115%;'>
    <?php
        echo \Yii::t('app', 'You are about to ' . $action . ' the following records:');
    ?>
</p>

<?php

// Define grid columns
$columns = [
    [
        'attribute' => 'entity',
        'label' =>  \Yii::t('app', 'Entity'),
        'format' => 'raw',
        'hAlign' => 'center',
    ],
    [
        'attribute' => 'count',
        'label' =>  \Yii::t('app', 'Count'),
        'format' => 'raw',
        'hAlign' => 'center',
        'value' => function ($model) {
            $count = $model['count'];
            return '
                <span class="new badge light-green darken-3">' .
                    '<strong>' . \Yii::t('app', $count) . '</strong>' .
                '</span>';
        }
    ]
];

// Render grid
echo $grid = GridView::widget([
    'pjax' => true,
    'dataProvider' => $dataProvider,
    'id' => 'gefu-creat-summary',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'striped'=>false,
    'columns' => $columns,
    'toggleData'=>true,
    'layout' => '{items}{pager}',
]);

?>

<p style='font-size:115%;'>
    <?php echo \Yii::t('app', 'Do you wish to proceed?'); ?>
</p>
