<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders preview for the file data
 */


use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;
use yii\bootstrap\Modal;

use app\models\UserDashboardConfig;

Yii::$app->view->registerCss('
    div.gm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.gm-browser-title {
        display: inline;
    }
');

$urlRedirectToMainCreatePage = Url::to(['create/redirect-to-main-create-page']);

?>

<div class="col-md-12" align="right" style="margin: 0px; padding: 10px;">
    <!-- Header -->
    <div class="col-md-12 row pull-left" style="padding: 0px; text-align: left;">
        <div class="col-md-8">
            <h3 class="file-upload-error-browser-title">
                <a href="<?= $urlRedirectToMainCreatePage ?>">Germplasm</a>
                <small>» File Upload Preview » <?= $fileDataPreviewInfo['fileName']; ?></small>
            </h3>
        </div>
        <div class="col-md-4" style="padding: 0px;">
            <button class="btn btn-default pull-right lighten-4" id="gefu-preview-back-btn" type="button" style="margin-bottom:5px;">BACK</button>    
            <button
                class="btn btn-default pull-right lighten-4"
                id="gefu-preview-complete-btn"
                type="button" 
                style="margin-right:5px; margin-bottom:5px;"
                data-toggle="modal"
                data-target="#gefu-complete-confirmation-modal">
                    COMPLETE
            </button>
        </div>
    </div>
</div>

<div class="panel panel-default" style="min-height: 500px; margin-top: 60px;">

    <div class="row" style="margin-bottom:0px;">
    <!-- File upload summary -->
    <?php
        $fileStatus = strtoupper($fileDataPreviewInfo['fileStatus']);
        $fileStatusDisplay = '';

        switch($fileStatus){
            case 'IN QUEUE':
                $fileStatusDisplay = '<span title="Queued for validation" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'IN QUEUE') . '</strong></span>';
                break;
            case 'VALIDATION IN PROGRESS':
                $fileStatusDisplay = '<span title="Ongoing validation of transaction" class="new badge orange darken-2"><strong>' . \Yii::t('app', 'VALIDATION IN PROGRESS') . '</strong></span>';
                break;
            case 'VALIDATED':
                $fileStatusDisplay = '<span title="Validation completed" class="new badge blue darken-2"><strong>' . \Yii::t('app', 'VALIDATED') . '</strong></span>';
                break;
            case 'VALIDATION FAILED':
                $fileStatusDisplay = '<span title="Validation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'VALIDATION FAILED') . '</strong></span>';
                break;
            case 'VALIDATION ERROR':
                $fileStatusDisplay = '<span title="Validation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'VALIDATION FAILED') . '</strong></span>';
                break;
            case 'CREATION IN PROGRESS':
                $fileStatusDisplay = '<span title="Ongoing creation of records" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'CREATION IN PROGRESS') . '</strong></span>';
                break;
            case 'CREATION FAILED':
                $fileStatusDisplay = '<span title="Creation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'CREATION FAILED') . '</strong></span>';
                break;
            case 'COMPLETED':
                $fileStatusDisplay =  '<span title="Creation completed" class="new badge green darken-2"><strong>' . \Yii::t('app', 'COMPLETED') . '</strong></span>';
                break;
            default:
                $fileStatusDisplay = '<span title="Status unknown" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
        }
        echo '<div id="occurrence-detail-div" style="margin-left:20px;margin-bottom:0px" >
            <div class="col-md-4">
                <dl>
                    <dt title="File Status">File Status</dt>
                    <dd>' . $fileStatusDisplay . '</dd>
                </dl>
            </div>
            <div class="col-md-4">
                <dl>
                    <dt title="Uploader">Uploader</dt>
                    <dd>' . $fileDataPreviewInfo['uploader'] . '</dd>
                </dl>
            </div>
            <div class="col-md-4">
                <dl>
                    <dt title="Upload Timestamp">Upload Timestamp</dt>
                    <dd>' . $fileDataPreviewInfo['uploadTimestamp'] . '</dd>
                </dl>
            </div>
        </div>';
    ?>
    </div>


    <?php

    // Define action columns
    $actionColumns = [
        [
            'class' => 'yii\grid\SerialColumn',
            'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
            'header' => false
        ]
    ];
    $columns = $fileDataPreviewInfo['columns'] ?? [];
    $columns = array_merge($actionColumns, $columns);
    $dataProvider = $fileDataPreviewInfo['dataProvider'] ?? [];

    // Render grid
    echo '<div style="padding-left:15px; padding-right:15px;">';
    $dynagrid = DynaGrid::begin([
        'columns'=>$columns,
        'storage'=>DynaGrid::TYPE_SESSION,
        'theme'=>'simple-default',
        'showPersonalize' => true,
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions'=>[
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'tableOptions'=>['class'=>$browserId.'-table'],
            'options'=>['id'=>$browserId.'-table-con'],
            'id' => $browserId.'-table-con',
            'striped'=>false,
            'responsive' => true,
            'responsiveWrap' => false,
            'floatHeader' => true,
            'floatHeaderOptions' => [
                'scrollingTop' => '0',
                'position' => 'absolute',
            ],
            'hover' => true,
            'floatOverflowContainer' => true,
            'showPageSummary' => false,
            'pjax' => true,
            'pjaxSettings'=>[
                'neverTimeout'=>true,
                'options' => ['id' => $browserId, 'enablePushState' => false,],
                'beforeGrid'=>'',
                'afterGrid'=>''
            ],
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ],
            'rowOptions'=> [],
            'panel'=> [
                'heading'=> false,
                'before' => 'Review file upload data here. {summary}',
                'after'=> false,
            ],
            'toolbar' =>  [
                [
                    'content' => Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [Yii::$app->controller->action->id . '?germplasmFileUploadDbId=' . $germplasmFileUploadDbId],
                        [
                            'class' => 'btn btn-default hm-tooltipped',
                            'id' => 'hm-crt-reset-plot-btn',
                            'style' => "margin-left:10px;",
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    )
                    . '{dynagridFilter}{dynagridSort}{dynagrid}'

                ],
            ],
        ],
        'options'=>[
            'id'=>$browserId
        ]
    ]);

    DynaGrid::end();
    echo '</div>';
    ?>

</div>

<?php

// Modal for confirming complete action
Modal::begin([
    'id' => 'gefu-complete-confirmation-modal',
    'header' => '<h4>Confirm Creation</h4>',
    'footer' =>
    Html::a(\Yii::t('app','Cancel'), ['#'], ['class'=>'gefu-modal-close-btn', 'id'=>'gefu-cancel-creation-btn', 'data-dismiss'=>'modal']) . '&emsp;' . 
    Html::a(\Yii::t('app', 'Proceed'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close gefu-modal-proceed-btn',
            'id' => 'gefu-modal-proceed-btn',
            'title' => \Yii::t('app', 'Proceed'),
            'action' => $fileDataPreviewInfo['action'] ?? 'create',
            'type' => $fileDataPreviewInfo['type'] ?? 'germplasm'
        ]
    ) . '&emsp;'
    . '&emsp;',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="gefu-complete-confirmation-modal-body"></div>';
Modal::end();

$urlRenderCreationSummary = Url::to(['create/render-creation-summary']);
$urlInvokeGermplasmWorker = Url::to(['create/invoke-germplasm-worker']);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');

// Check if browser Id is empty (occurrence tab)
$browserId = empty($browserId) ? 'none' : $browserId;

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ". json_encode($browserId) .",
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS
    var germplasmFileUploadDbId = $germplasmFileUploadDbId;
    var browserId = '$browserId';
    var action = '';
    var type = '';

    // Adjust browser height
    $('#' + browserId + 'table-con-container').css('min-height','450px');
    
    $("document").ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        refresh();

        // Perform after pjax completion of browser
        $(document).on('ready pjax:complete','#' + browserId + '-pjax', function(e) {
            refresh();
        });

        /**
         * On complete button click event
         */
        $(document).on('click', '#gefu-preview-complete-btn', function(e) {
            var modalBody = '#gefu-complete-confirmation-modal-body';

            $(modalBody).html('<div class="progress"><div class="indeterminate"></div></div>');
            
            // Render summary
            $.ajax({
                url: '$urlRenderCreationSummary',
                type: 'post',
                data:{
                    germplasmFileUploadDbId: germplasmFileUploadDbId
                },
                success: function(response){
                    $(modalBody).html(response);
                },
                error: function(){
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBody).html(errorMessage);
                }
            });
        });

        /**
         * On return button click event
         */
        $(document).on('click', '#gefu-preview-back-btn', function(e) {
            // Redirect
            $.ajax({
                url: '$urlRedirectToMainCreatePage',
                type: 'post',
                data:{},
                success: function(response){ }
            });
        });

        /**
         * On proceed button click event
         */
        $(document).on('click', '#gefu-modal-proceed-btn', function(e) {
            var modalID = 'gefu-complete-confirmation-modal';
            var modalBody = 'gefu-complete-confirmation-modal-body';
            action = $(this).attr('action');
            type = $(this).attr('type');

            invokeGermplasmWorker(
                germplasmFileUploadDbId,
                'CreateGermplasmRecords',
                'Create germplasm, seeds, and packages for gerplasm file upload ID: ' + germplasmFileUploadDbId,
                modalID,
                modalBody
            );
        });
    });

    /**
     * Generic function for invoking a background worker
     * @param Integer germplasmFileUploadDbId germplasm file upload identifier
     * @param String workerName name of the worker to invoke
     * @param String description general description of what the worker does
     * @param String description general description of what the worker does
     * @param String modalID (optional) modal ID to be closed (if any)
     * @param String modalBody (optional) modal body to be used for displaying error messages
     */
    function invokeGermplasmWorker(germplasmFileUploadDbId, workerName, description, modalID = null, modalBody = null) {
        if(modalBody != null) $("#" + modalBody).html('<div class="progress"><div class="indeterminate"></div></div>');

        // Invoke background worker
        $.ajax({
            url: '$urlInvokeGermplasmWorker',
            type: 'post',
            data:{
                germplasmFileUploadDbId: germplasmFileUploadDbId,
                workerName: workerName,
                description: description,
                additionalWorkerData: JSON.stringify({
                    action: action,
                    type: type
                })
            },
            success: function(response){ }
        });
    }

    // Adjust browser height after refresh
    function refresh() {
        // Adjust browser height
        $('#' + browserId + '-table-con-container').css('min-height','400px');
    }
JS;

$this->registerJs($script);

?>