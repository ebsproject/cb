<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders browser for germplasm
 */

use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\date\DatePicker;
use yii\helpers\Html;
use app\components\FavoritesWidget;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\components\ManageSortsWidget;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use app\models\GermplasmList;
use app\components\QueryWidget;
use app\components\DataBrowser;
use app\components\PrintoutsWidget2;
use app\widgets\GermplasmInfo;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

use app\models\Germplasm;

?>

<!-- Google Fonts style sheet -->
<!-- This is needed to properly render the buttons for Download of Pedigree -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />

<?php

$printoutsMaxCount = 1000;
if (
    isset($sessProcessThresh['GM_MAX_NO_OF_ROWS_FOR_PRINTOUT']) &&
    !empty($sessProcessThresh['GM_MAX_NO_OF_ROWS_FOR_PRINTOUT'])
) {
    $printoutsMaxCount = intval($sessProcessThresh['GM_MAX_NO_OF_ROWS_FOR_PRINTOUT']);
}

$printouts = PrintoutsWidget2::widget([
    "product" => "Germplasm",
    "program" => $program ?? "",
    "entityName" => "germplasm",
    "entityDisplayName" => "Germplasm",
    "entityType" => "germplasm",
    "entityDbIds" => Yii::$app->session['germplasm-selected_ids'] ?? [],
    "entityValueSource" => '/account/list/get-selected-from-session',
    "checkboxSessionStorage" => 'germplasm-selected_ids',
    "buttonStyle" => [
        "margin-left" => "10px"
    ]
]);

$resetGridUrl = Url::toRoute(['/germplasm/search/index']);
$getTotalCountUrl = Url::toRoute(['/germplasm/search/get-total-result-count']);
$listUrl = Url::toRoute(['/account/list', 'program' => $program]);
$codingGermplasmUrl = Url::toRoute(['/germplasm/coding/create-transaction', 'program' => $program, 'sourceToolAbbrev' => 'GM']);

$selectAllButtonChecked = ($selectAllMode == 'exclude mode' && empty($selectedIds)) ? 'checked' : '';
$totalResultsCount = $dataProvider->totalCount;

//Get config values
$getDepthThresh = Yii::$app->config->getAppThreshold('GERMPLASM_MANAGER', 'ancestryExportDepth');
$getSearchCharMin = Yii::$app->config->getAppThreshold('GERMPLASM_MANAGER', 'searchCharMin') ?? 3;
$exportDepthThresh = isset($getDepthThresh) && !empty($getDepthThresh) ? $getDepthThresh : 6;

$selectedItemsCount = 'No';

if ($selectAllMode == 'include mode') {
    $selectedItemsCount = count($selectedIds) > 0 ? count($selectedIds) : 'No';
} else {    // exclude mode
    $selectedItemsCount = count($selectedIds) > 0 ? $totalResultsCount - count($selectedIds) : $totalResultsCount;
}

echo Yii::$app->controller->renderPartial('@app/modules/germplasm/views/default/tabs.php', [
    'controller' => 'search',
    'program' => $program
]);

echo QueryWidget::widget([
    'configAbbrev' => 'GERMPLASM_QUERY_FIELDS',
    'sessionName' => $sessionName,
    'gridId' => 'dynagrid-germplasm-grid',
    'url' => Url::to(['/germplasm/search/index', 'program' => $program]),
    'totalCount' => $dataProvider->totalCount,
    'clearSelectionCb' => "true",
    'tool' => 'germplasm_browser',
    'searchCharMin' => $getSearchCharMin
]);

// Check access to breeding tools
$hasAccessToBreedingTools = true;
$hasAccessToCoding = $codingModel ? $codingModel->hasCodingAccess() : true;

// Germplasm tools button for the browser toolbar
$germplasmTools = "";
if ($hasAccessToBreedingTools && $hasAccessToCoding) {
    $germplasmTools = "
        <a
            title='Germplasm Tools'
            id='ge-internal-tools-btn'
            class='dropdown-trigger btn'
            data-activates='ge-internal-tools-list'
            data-beloworigin='true'
            data-constrainwidth='false'
        >
            <i class='glyphicon glyphicon-cog'></i>
            <span class='caret'></span>
        </a>
        <ul id='ge-internal-tools-list' class='dropdown-content' style='
                            max-width: 130px;
                            margin-left: -124px;
                        '>

            <!-- Germplasm Coding -->
            <li
                id='ge-germplasm-coding-btn'>" .
        Html::a(
            "Code Germplasm",
            null,
            [
                'class' => 'im-blank-template-btn dropdown-button dropdown-trigger dropdown-button-pjax',
                'data-activates' => 'ge-germplasm-coding-options',
                'data-alignment' => 'right',
                'data-hover' => 'hover',
            ]
        ) .
        "<ul
                    id='ge-germplasm-coding-options'
                    class='dropdown-content'
                    style='
                        max-width: 130px;
                        margin-left: -124px;
                    '
                >
                    <li 
                        id='ge-germplasm-coding-selected-btn'
                        class='ge-germplasm-coding-option-btn hide-loading'
                        title='Code selected'
                    >
                        <a
                            coding-type='selected'
                            class='ge-germplasm-coding-selected'
                        >Code Selected</a>
                    </li>
                    <li 
                        id='ge-germplasm-coding-all-btn'
                        class='ge-germplasm-coding-option-btn hide-loading'
                        title='Code all'
                    >
                        <a
                            coding-type='all'
                            class='ge-germplasm-coding-all'
                        >Code All</a>
                    </li>
                </ul>
            </li>
        </ul>";
}

//all columns of germplasm catalog browser
$defaultColumns = [
    [
        'label' => "Checkbox",
        'header' => '
            <input type="checkbox" class="filled-in" id="germplasm-select-all-id" ' . $selectAllButtonChecked . '/>
            <label for="germplasm-select-all-id"></label>
        ',
        'content' => function ($model) use ($selectedIds, $selectAllMode) {
            $checked = '';
            if ($selectAllMode == 'include mode') {
                if (in_array($model['germplasmDbId'], $selectedIds)) $checked = 'checked';
            } else {    // exclude mode
                if (!in_array($model['germplasmDbId'], $selectedIds)) $checked = 'checked';
            }
            return '
                <input class="dynagrid-germplasm-grid_select filled-in" type="checkbox" id="' . $model['germplasmDbId'] . '" ' . $checked . '/>
                <label for="' . $model['germplasmDbId'] . '"></label>
            ';
        },
        'hAlign' => 'center',
        'vAlign' => 'top',
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => ' ',
        'template' => '{view} {edit} {delete} {download}',
        'visibleButtons' => [
            'edit' => function ($model, $key, $index) {
                if (Yii::$app->session->get('isAdmin')) {
                    return true;
                }
                return false;
            },
            'delete' => function ($model, $key, $index) {
                if (Yii::$app->session->get('isAdmin')) {
                    return true;
                }
                return false;
            }
        ],
        'buttons' => [
            'view' => function ($url, $model) {
                return GermplasmInfo::widget([
                    'id' => $model['germplasmDbId'],
                    'label' => '<i class="material-icons view-germplasm-btn">remove_red_eye</i>',
                    'entityLabel' => $model['designation'],
                    'linkAttrs' => [
                        'id' => 'view-germplasm-widget-' . $model['germplasmDbId'],
                        'class' => 'blue-text text-darken-4 header-highlight',
                        'title' => \Yii::t('app', 'Click to view more information'),
                        'data-label' => $model['designation'],
                        'data-id' => $model['germplasmDbId'],
                        'data-target' => '#view-germplasm-widget-modal',
                        'data-toggle' => 'modal',
                        'style' => ['font-size' => '1rem']
                    ]
                ]);
            },
            'edit' => function ($url, $model) {
                return Html::a(
                    '<i class="material-icons">edit</i>',
                    '#',
                    [
                        'class' => 'edit-germplasm',
                        'title' => 'Edit germplasm',
                        'data-entity_id' => $model['germplasmDbId']
                    ]
                );
            },
            'delete' => function ($url, $model) {

                if ($model['entriesCount'] == 0 && $model['seedsCount'] == 0) {
                    return Html::a(
                        '<i class="material-icons">delete</i>',
                        '#',
                        [
                            'class' => 'delete-germplasm',
                            'id' => 'delete-germplasm-btn',
                            'title' => 'Delete germplasm',
                            'data-entity_id' => $model['germplasmDbId'],
                            'data-entity_entries-count' => $model['entriesCount'],
                            'data-entity_seeds-count' => $model['seedsCount'],
                            'data-entity_germplasm-name' => $model['designation']
                        ]
                    );
                }
            },
            'download' => function ($url, $model) {
                return Html::a(
                    '<span class="material-symbols-outlined">family_history</span>',
                    '#',
                    [
                        'class' => 'download-germplasm-pedigree',
                        'title' => 'Download pedigree',
                        'data-entity_id' => $model['germplasmDbId'],
                        'data-germplasm_code' => $model['germplasmCode']
                    ]
                );
            },
        ],
        'vAlign' => 'top'
    ],
    [
        'attribute' => 'designation',
        'label' => 'Germplasm Name',
        'contentOptions' => [
            'class' => 'germplasm-name-col'
        ]
    ],
    [
        'attribute' => 'otherNames',
        'contentOptions' => [
            'class' => 'germplasm-name-col'
        ],
        'value' => function ($data) {
            $otherNames = $data['otherNames'];
            $otherNames = str_replace(";", "<br/>", $otherNames);
            return $otherNames;
        },
        'format' => 'raw'
    ],
    [
        'attribute' => 'parentage',
        'contentOptions' => [
            'class' => 'germplasm-name-col'
        ]
    ],
    [
        'attribute' => 'generation',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ]
    ],
    [
        'attribute' => 'germplasmNameType',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ]
    ],
    [
        'attribute' => 'germplasmType',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ]
    ],
    [
        'attribute' => 'germplasmState',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ]
    ],
    [
        'attribute' => 'germplasmNormalizedName',
        'contentOptions' => [
            'class' => 'germplasm-name-col'
        ],
        'visible' => false
    ],
    [
        'attribute' => 'germplasmCode',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ],
        'visible' => false
    ],
    [
        'attribute' => 'taxonomyName',
        'label' => 'Taxonomy',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ],
        'visible' => false
    ],
    [
        'attribute' => 'cropCode',
        'label' => 'Crop',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ],
        'visible' => false
    ],
];

$otherColumns = [
    [
        'attribute' => 'creator',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ]
    ],
    [
        'attribute' => 'creationTimestamp',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ],
        'visible' => false,
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ]
    ],
    [
        'attribute' => 'modifier',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ]
    ],
    [
        'attribute' => 'modificationTimestamp',
        'contentOptions' => [
            'style' => 'min-width:100px;'
        ],
        'visible' => false,
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
        'value' => function ($data) {
            if (is_null($data['modifier'])) return null;
            return $data['modificationTimestamp'];
        },
    ],
];

// set up columns for export and grid view
$otherGridColumns = [];
foreach ($otherColumns as $key => $value) {
    $visibleFalse = ['visible' => false];

    $defaultOtherCols = array_merge($value, $visibleFalse);
    $otherGridColumns[] = $defaultOtherCols;
}

$gridColumns = array_merge($defaultColumns, $otherGridColumns);

$downloadOptionsHTML = "
    <ul id='dropdown-download-ge-options' class='dropdown-content toolbar-btn-options'  data-beloworigin='false'>
        <li><a href='#!' id='download-ge-germplasm-record-btn' class='download_germplasm_record'>Germplasm</a></li>
        <li><a href='#!' id='download-ge-pedigree-btn' class='download_pedigree'>Pedigree</a></li>
        <li><a href='#!' id='download-ge-relations-btn' class='download_relations'>Relations</a></li>
    </ul>
";

$downloadBtn = "" . Html::a(
    '<i class="material-icons widget-icons">download</i>',
    '#',
    [
        'class' => 'btn btn-default dropdown-trigger dropdown-button-pjax',
        'title' => \Yii::t('app', 'Download germplasm records'),
        'id' => 'download-ge-info-btn',
        'data-pjax' => true,
        'data-beloworigin' => 'true',
        'data-constrainwidth' => 'false',
        'data-activates' => 'dropdown-download-ge-options'
    ]
) . '&emsp;' . $downloadOptionsHTML;

//dynagrid configuration
$dynagrid = DynaGrid::begin([
    'columns' => $gridColumns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => 'dynagrid-germplasm-grid-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => 'dynagrid-germplasm-grid-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsiveWrap' => false,
        'panel' => [
            'heading' => '<br /><h6>' . \Yii::t('app', 'This is the browser for all the germplasm. Click the eye icon to view germplasm information. Please note that this displays only up to') . ' ' . number_format($totalCountLimit) . ' records.</h6>',
            'before' => '<br /><p id="total-selected-text" class="pull-right" style="margin-right: 5px;"><b id = "selected-count">' . $selectedItemsCount . '</b> selected items.</p>',
            'after' => false,
        ],
        'toolbar' =>  [
            $downloadBtn,
            [
                'content' => ""
                    . Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        ['index'],
                        [
                            'data-pjax' => 0,
                            'id' => 'reset-dynagrid-germplasm-grid',
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid')
                        ]
                    )
                    . '{dynagridFilter}{dynagridSort}{dynagrid}'
                    . Html::a(
                        '<i class="material-icons widget-icons">add_shopping_cart</i>',
                        '#!',
                        [
                            'class' => 'btn pull-right',
                            'id' => 'germplasm-save-new-list-btn',
                            'title' => \Yii::t('app', ' Save as new list')
                        ]
                    )
                    . '&emsp;'
                    . Html::a(
                        '<i class="material-icons large">notifications_none</i>
                        <span id="notif-badge-id" class=""></span>',
                        '#',
                        [
                            'id' => 'ge-search-notifs-btn',
                            'class' => 'btn waves-effect waves-light ge-search-notification-button ge-tooltipped',
                            'data-pjax' => 0,
                            'style' => "overflow: visible !important;",
                            "data-position" => "top",
                            "data-tooltip" => \Yii::t('app', 'Notifications'),
                            "data-activates" => "notifications-dropdown",
                            'disabled' => false,
                        ]
                    )
                    . $printouts
                    . $germplasmTools
                    . ManageSortsWidget::widget([
                        'dataBrowserId' => 'germplasm',
                        'gridId' => 'dynagrid-germplasm-grid',
                        'searchModel' => 'GermplasmSearch',
                        'btnClass' => 'sort-action-button',
                        'resetGridUrl' => $resetGridUrl . '?'
                    ])
            ],

        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'resizableColumns' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options' => ['id' => 'dynagrid-germplasm-grid']
]);

DynaGrid::end();

// modal for displaying excluded input list items
Modal::begin([
    'id' => 'germplasm-excluded-input-list-items-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">info</i> Excluded from search</h4>',
    'footer' =>
    Html::a(
        \Yii::t('app', 'Confirm'),
        '#',
        [
            'id' => 'confirmation-close-btn',
            'class' => 'btn btn-primary waves-effect waves-light modal-close confirmation-default-grp confirmation-close-btn',
            'data-dismiss' => 'modal'
        ]
    ) . '&emsp;</span>',
    'options' => [
        'data-backdrop' => 'static',
        'style' => 'z-index: 2000;'
    ]
]);
echo '  <div id="germplasm-excluded-input-list-items-modal-body"></div>';
Modal::end();

// modal for saving as new list
Modal::begin([
    'id' => 'germplasm-save-list-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">add</i> Save as new list</h4>',
    'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss' => 'modal']) . '&emsp;&emsp;'
        . Html::a(
            'Submit',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light disabled',
                'url' => '#',
                'id' => 'germplasm-save-list-confirm-btn'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);

$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'create-saved-list-form',
    'fieldConfig' => function ($model, $attribute) {
        if (in_array($attribute, ['abbrev', 'display_name', 'name', 'type'])) {
            return ['options' => ['class' => 'input-field form-group col-md-6 required']];
        } else {
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    }
]);

$model = new GermplasmList();
$listType = [
    'germplasm' => 'Germplasm'
];
$listTypeOptions = [
    'placeholder' => Yii::t('app', 'Select type'),
    'id' => 'type-fld',
    'class' => 'text-field-var',
    'required' => 'required',
    'data-name' => 'Type',
    'value' => 'germplasm'
];


echo '<div class="clearfix"></div><div class="germplasm-modal-loading-save"></div>';

echo $form->field($model, 'name')->textInput([
    'maxlength' => true,
    'id' => 'germplasm-list-name',
    'title' => 'Name identifier of the list',
    'autofocus' => 'autofocus',
    'onkeyup' => 'js: $("#germplasm-list-display-name").val(this.value.charAt(0).toUpperCase() + this.value.slice(1)); $(".field-germplasm-list-display-name > label").addClass("active");'
]) .
    $form->field($model, 'abbrev')->textInput([
        'maxlength' => true,
        'id' => 'germplasm-list-abbrev',
        'title' => 'Short name identifier or abbreviation of the list',
        'required' => true,
        'oninput' => 'js: $("#germplasm-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());'
    ]) .
    $form->field($model, 'display_name')->textInput([
        'maxlength' => true,
        'title' => 'Name of the list to show to users',
        'id' => 'germplasm-list-display_name'
    ]) .
    $form->field($model, 'type')->widget(Select2::classname(), [
        'data' => $listType,
        'options' => $listTypeOptions
    ])->label(
        Yii::t('app', 'Type'),
        ['class' => 'control-label', 'style' => 'top:-95%;']
    ) .
    $form->field($model, 'description')->textarea(['class' => 'materialize-textarea', 'title' => 'Description about the list', 'id' => 'germplasm-list-description']) .
    $form->field($model, 'remarks')->textarea(['class' => 'materialize-textarea', 'title' => 'Additional details about the list', 'row' => 2, 'id' => 'germplasm-list-remarks']);

ActiveForm::end();

echo '<div class="germplasm-save-list-preview-entries">Number of germplasm to be saved as a list: <b id = "germplasm-count"></b></div>';

Modal::end();

/** Modal for confirming deletion if germplasm record */
Modal::begin([
    'id' => 'germplasm-delete-confirmation-modal',
    'header' => '<h4>Delete Germplasm</h4>',
    'footer' =>
    Html::a(
        \Yii::t('app', 'Cancel'),
        ['#'],
        [
            'id' => 'confirmation-cancel-btn',
            'class' => 'modal-close confirmation-default-grp confirmation-cancel-btn',
            'data-dismiss' => 'modal'
        ]
    ) . '&emsp;' .
        Html::a(
            \Yii::t('app', 'Confirm'),
            '#',
            [
                'id' => 'confirmation-delete-btn',
                'class' => 'btn btn-primary waves-effect waves-light modal-close confirmation-default-grp confirmation-delete-btn',
                'data-dismiss' => 'modal'
            ]
        ) . '&emsp;</span>',
    'options' => [
        'data-backdrop' => 'static',
        'style' => 'z-index: 2000;'
    ]
]);
echo '  <div id="germplasm-delete-confirmation-modal-body"></div>';
Modal::end();

echo '
    <!-- Notifications -->
    <div id="notif-container">
        <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
        </ul>
    </div>
';

$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

$editEntityUrl = Url::to(['/germplasm/edit/index']);
$exportToHeliumUrl = Url::to(['/germplasm/search/export-to-helium']);
$exportToCSVUrl = Url::to(['/germplasm/search/export-to-csv']);
$buildGermplasmDataForExportUrl = Url::to(['/germplasm/search/build-germplasm-data-for-export']);
$getSelectedFromSessionUrl = Url::to(['/account/list/get-selected-from-session']);
$saveListUrl = Url::to(['/account/list/save-list']);
$selectRowUrl = Url::to(['/account/list/select-row']);
$retrieveDbIdsUrl = Url::to(['/germplasm/default/retrieve-db-ids', 'action' => 'search']);
$clearSelectedIdsUrl = Url::to(['/germplasm/default/clear-selected-ids']);
$toggleSelectAllUrl = Url::to(['/account/list/toggle-select-all']);
$exportGermplasmInfoInWidgetUrl = Url::toRoute(['/search/default/export']);
$deleteSelectedRecordUrl = Url::to(['/germplasm/delete/delete-germplasm']);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

$module = 'search';
$notificationsUrl = Url::to(['/germplasm/default/push-notifications', 'module' => $module]);
$newNotificationsUrl = Url::toRoute(['/germplasm/default/new-notifications-count', 'module' => $module]);

$pjaxReloadUrl = "/index.php/germplasm/search/index?program=$program";

$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings(['dynagrid-germplasm-grid']);    // saves current browser settings to db

// variables for change page size
Yii::$app->view->registerJs(
    "
    var setDefaultPageSizeUrl = '" . $setDefaultPageSizeUrl . "',
    browserId = 'dynagrid-germplasm-grid',
    currentDefaultPageSize = '" . $currentDefaultPageSize . "',
    gmNewNotificationsUrl = '" . $newNotificationsUrl . "',
    gmNotificationsUrl = '" . $notificationsUrl . "',
    gmNotifsBtnId = 'ge-search-notifs-btn',
    gmBrowserId = 'dynagrid-germplasm-grid',
    gmPjaxReloadUrl = '" . $pjaxReloadUrl . "'
    ;",
    \yii\web\View::POS_HEAD
);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position' => \yii\web\View::POS_END
]);

// add global js for notifications
$this->registerJsFile("@web/js/germplasm-manager/notifications.js", [
    'depends' => ['app\assets\AppAsset'],
    'position' => \yii\web\View::POS_END
]);

$this->registerJs(
    <<<JS
    var resetGridUrl='$resetGridUrl';
    var saveListUrl = "$saveListUrl";
    var selectRowUrl = "$selectRowUrl";
    var toggleSelectAllUrl = "$toggleSelectAllUrl";
    var totalResultsCount = "$totalResultsCount";
    var selectAllMode = "$selectAllMode";
    var getSelectedFromSessionUrl = '$getSelectedFromSessionUrl';
    var clearSelectedIdsUrl = '$clearSelectedIdsUrl';

    var buildGermplasmDataForExportUrl = '$buildGermplasmDataForExportUrl';
    var exportDepthThresh = '$exportDepthThresh';

    var selectedDeleteRecords = [];
    var selectedFromSession = null;
    var germplasmCodingThreshold = parseInt('$germplasmCodingThreshold');
    var maxPrintoutsRecords = parseInt('$printoutsMaxCount');

    $(document).on('input', '#germplasm-list-name', function(e) {
        e.preventDefault();
        $('#germplasm-list-abbrev').val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());
        $('#germplasm-list-abbrev').trigger('change');
        $('#germplasm-list-display_name').val(this.value);
        $('#germplasm-list-display_name').trigger('change');
    });

    // export germplasm seed in germplasm widget
    $(document).on('click', '.export-data', function(e) {
        var id = this.id;
        var obj = $(this);
        var attr = obj.data('attr');
        var label = obj.data('label');
        window.location = '$exportGermplasmInfoInWidgetUrl'+'?entityId='+id+'&attr='+attr+'&label='+label;
        $(window).on('beforeunload', function(){
            $('#loading').html('');
            $('#system-loading-indicator').html('');
        });
    });

    // edit germplasm information
    $(document).on('click', '.edit-germplasm', function(e) {
        e.preventDefault();

        var obj = $(this);
        var entity_id = obj.data('entity_id');
        var url = '$editEntityUrl?id=' + entity_id;

        window.location = url;
    });

    // Download germplasm pedigree
    $(document).on('click', '.download-germplasm-pedigree', function(e) {
        e.preventDefault();

        var obj = $(this);
        var entityDbId = obj.data('entity_id');
        var germplasmCode = obj.data('germplasm_code');

        var filter = {
            germplasmDbId: String(entityDbId),
            depth:exportDepthThresh
        }

        // Invoke background worker
        $.ajax({
            url: '$exportToHeliumUrl',
            type: 'post',
            dataType: 'json',
            data: {
                description: 'Preparing the pedigree of Germplasm Code: ' + germplasmCode + ' for downloading',
                entity: 'GERMPLASM',
                entityDbId: entityDbId,
                endpointEntity: 'GERMPLASM',
                application: 'GERMPLASM_CATALOG',
                processName: 'export-pedigree',
                dataType: 'pedigree',
                endpoint: 'germplasm/:id/pedigrees-search',
                requestBody: JSON.stringify({filter:filter}),
                fileName: germplasmCode+'-Pedigree-Export'
            },
            async: false,
            success: function(response) {
                var success = response.success;
                var status = response.status;

                // Display message
                if (success) {
                    var notif = "<i class='material-icons green-text left'>check</i> The export file is being created in the background. You may proceed with other tasks. You will be notified in this page once done.";
                    Materialize.toast(notif,5000);
                } else {
                    // Failure message
                    var message = '<i class="material-icons red-text left">close</i> A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status !== 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if (status == null) message += ' (#002)';
                    Materialize.toast(message,5000);
                }
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There was a problem loading the content.";
                Materialize.toast(notif,5000);
            }
        });
    });

    // click row in browser
    $(document).on('click', '#dynagrid-germplasm-grid-id tbody tr:not(a)',function(e){
        e.stopPropagation();
        e.preventDefault();

        if($('#view-germplasm-widget-modal').css('display') != 'none') {
            // workaround because the view germplasm widget element is a descendant of the table row element
            return;
        }
        this_row=$(this).find('input:checkbox')[0];
        var id = this_row.id

            $.ajax({
                type: 'POST',
                url: selectRowUrl,
                data: {
                    id: id,
                    type: 'germplasm'
                },
                async: false,
                dataType: 'json',
                success: function(response) {
                    if(this_row.checked){
                        this_row.checked=false;
                        $('#germplasm-select-all-id').prop("checked", false);
                        $(this).removeClass("grey lighten-4");

                    } else {
                        $(this).addClass("grey lighten-4");
                        this_row.checked=true;
                        $("#"+this_row.id).prop("checked");
                    }

                    // get total selected
                    var count = 0
                    if(selectAllMode == 'include mode')
                        count = response > 0? response: 'No';
                    else    // exclude mode
                        count = response > 0? totalResultsCount - response: totalResultsCount;

                    // toggle select all button
                    if(totalResultsCount == count)
                        $('#germplasm-select-all-id').prop('checked',true);
                    else
                        $('#germplasm-select-all-id').prop('checked',false);

                    $('#selected-count').html(count);
                },
                error: function(){
                    var notif = "<i class='material-icons red-text'>close</i> There was a problem in selecting item.";
                    Materialize.toast(notif,5000);
                }
            });

    });

    // select all germplasm
    $(document).on('click', '#germplasm-select-all-id',function(e){
        var mode = 'include mode';
        var count = 'No';
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.dynagrid-germplasm-grid_select').attr('checked','checked');
            $('.dynagrid-germplasm-grid_select').prop('checked',true);
            $(".dynagrid-germplasm-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
            mode = 'exclude mode';
            count = totalResultsCount > 0? totalResultsCount : count;

            // TEMPORARY! Disable printouts for SELECT ALL
            // $('.printouts-button').prop('disabled',true);
            // $('.printouts-button').addClass('disabled');
            var msg = 'The maximum number of records to be processed is ' + maxPrintoutsRecords + ' germplasm records.'
            var message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + msg;
            $('.toast').css('display','none')
            Materialize.toast(message, 5000)

            var selectedIdsArr = retrieveSelectedIds(true)
        }else{
            $(this).removeAttr('checked');
            $('.dynagrid-germplasm-grid_select').prop('checked',false);
            $('.dynagrid-germplasm-grid_select').removeAttr('checked');
            $("input:checkbox.dynagrid-germplasm-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
            mode = 'include mode';

            $('.printouts-button').prop('disabled',false);
            $('.printouts-button').removeClass('disabled');
        }

        $.ajax({
            type: 'POST',
            url: toggleSelectAllUrl,
            data: {
                type: 'germplasm',
                mode: mode
            },
            async: false,
            dataType: 'json',
            success: function(response) {
                $('#selected-count').html(count);
                selectAllMode = mode;
            },
            error: function(){
                var notif = "<i class='material-icons red-text'>close</i> There was a problem in selecting all items.";
                Materialize.toast(notif,5000);
            }
        });
    });

    // trigger delete confirmation modal
    $(document).on('click','#delete-germplasm-btn', function(e){
        e.stopPropagation();
        e.preventDefault();

        let data = $(this);
        let entityId = data.data('entity_id');
        let entityName = data.data('entity_germplasm-name');

        let message = '<p style="font-size:115%;">Are you sure you want to delete germplasm <strong>'+entityName+'</strong>?'+
                      '</br></br>This will delete the corresponding germplasm, germplasm name, attribute, and relation records from the system. '+
                      '</br></br>Click confirm to proceed.</p>';
        

        selectedDeleteRecords[0] = entityId;
        
        $('#germplasm-delete-confirmation-modal-body').html(message);
        $('#germplasm-delete-confirmation-modal').modal('show');
    });

    // delete germplasm record
    $(document).on('click','.confirmation-delete-btn', function(e){
        e.stopPropagation();
        e.preventDefault();

        $('.toast').css('display','none');

        var notif = '';
        if(selectedDeleteRecords.length > 0){
            $.ajax({
                type: 'POST',
                url: '$deleteSelectedRecordUrl',
                data: {
                    germplasmId: selectedDeleteRecords
                },
                async: false,
                dataType: 'json',
                success: function(response) {
                    if(!response.success){
                        notif = "<span class='white-text'><i class='material-icons orange-text'>warning</i>&nbsp;"+response.message+"</span>";
                        Materialize.toast(notif,5000);
                    }
                    else{
                        notif = "<span class='white-text'><i class='material-icons green-text left'>check</i>&nbsp;Successfully deleted germplasm record!</span>";
                        Materialize.toast(notif,5000);

                        $.pjax.reload({
                            container: '#dynagrid-germplasm-grid-pjax',
                            replace: true,
                            url: resetGridUrl
                        });
                    }

                },
                error: function(){
                    notif = "<i class='material-icons orange-text'>warning</i> <span class='white-text'>Error encountered in deleting germplasm record.</span>";
                    Materialize.toast(notif,5000);
                }
            });
        }
    });

    // download all germplasm records
    $(document).on('click','#download-ge-germplasm-record-btn', function(e){
        generateExportFile([],'csv','germplasm');
    })

    // download germplasm pedigree information
    $(document).on('click','#download-ge-pedigree-btn', function(e){
        var notif;

        // retrieve total selected records
        var count = $('#selected-count').html().replace(/,/g, '');
        count = parseInt(count);

        if(count > 0){
            // extract germplasm ID
            var germplasmId;
            var selectedFromSession;
            $.ajax({
                type: 'POST',
                url: getSelectedFromSessionUrl,
                data: {
                    type: 'germplasm'
                },
                async: false,
                dataType: 'json',
                success: function(response) {
                    selectedFromSession = JSON.parse(response);

                    generateExportFile(selectedFromSession,'helium','pedigree');
                },
                error: function(){
                    var notif = "<i class='material-icons red-text'>close</i> There was a problem while loading content.";
                    Materialize.toast(notif,5000);
                }
            });
        }
        // no record selected
        else{
            notif = "<i class='material-icons orange-text'>warning</i> Please select a germplasm.";
            Materialize.toast(notif,5000);
        }
    })

    // download germplasm relations information
    $(document).on('click','#download-ge-relations-btn', function(e){
        var notif;

        // retrieve total selected records
        var count = $('#selected-count').html().replace(/,/g, '');
        count = parseInt(count);

        // single record processing
        if(count > 0){
            // extract germplasm ID
            var germplasmId;
            var selectedFromSession;
            $.ajax({
                type: 'POST',
                url: getSelectedFromSessionUrl,
                data: {
                    type: 'germplasm'
                },
                async: false,
                dataType: 'json',
                success: function(response) {
                    selectedFromSession = JSON.parse(response);

                    generateExportFile(selectedFromSession,'csv','relations');
                },
                error: function(){
                    var notif = "<i class='material-icons red-text'>close</i> There was a problem while loading content.";
                    Materialize.toast(notif,5000);
                }
            });
        }
        // no record selected
        else{
            notif = "<i class='material-icons orange-text'>warning</i> Please select a germplasm.";
            Materialize.toast(notif,5000);
        }
    })

    // facilitate retrieval of germplasm record and generation of export file
    function generateExportFile(selectedFromSession,dataFormat,dataType){
        selectedFromSession = selectedFromSession ?? [];
        dataFormat = dataFormat ?? 'helium';

        if(dataType == 'germplasm') {
            exportToCSV([], '', dataType);
            return;
        }

        $.ajax({
            url: buildGermplasmDataForExportUrl,
            type: 'post',
            dataType: 'json',
            data: {
                'selectedFromSession': selectedFromSession,
                'exportFormat': dataFormat
            },
            success: function(response) {
                var success = response.success;
                var status = response.status;
                var data = response.data;
                var filter = response.filter;

                // Display message
                if (success && data != undefined) {
                    if(dataFormat == 'helium') {
                        exportToHelium(data, filter);
                    }

                    if(dataFormat == 'csv') {
                        exportToCSV(data, filter, dataType);
                    }
                } else {
                    // Failure message
                    var message =   '<i class="material-icons red-text left">close</i>'+
                                    ' A problem occurred while starting the background process or '+
                                    '<a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    Materialize.toast(message,5000);
                }
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There was a problem loading the content.";
                Materialize.toast(notif,5000);
            }
        });
    }

    // facilitate invoking of BuildCSV background process
    function exportToCSV(data, filter, dataType){
        var descMessage;
        var entityDbId;

        var namePrepend = '';
        var fileName = '';
        var processName;
        var dataType;
        var endpoint;

        var requestBody = null
        var reqBodyParams = null

        // process file name
        if(data.length == 1){
            descMessage = 'Germplasm Code: ' + data[0]['germplasmCode'];
            entityDbId = data[0]['germplasmDbId'];
            namePrepend = data[0]['germplasmCode']+'-';
        }
        else{
            descMessage = 'multiple germplasm records';
            entityDbId = data.map(germplasm => germplasm['germplasmDbId']).join('|');
        }

        if(dataType == 'germplasm') {
            endpoint = 'germplasm-search';
            fileName = 'Germplasm-';

            // build bg process parameters
            reqBodyParams = {};

            descMessage = 'the current filters'
        }else if(dataType == 'relations'){
            processName = 'export-relations';
            endpoint = 'germplasm/:id/relations-search';
            fileName = namePrepend+'Germplasm_Relation_For_COP-';

            // build bg process parameters
            reqBodyParams = {
                filter: filter,
                depth: exportDepthThresh
            };

        }

        requestBody = {
            description: 'Preparing the '+ dataType +' records of '+ descMessage +' for downloading',
            entity: 'GERMPLASM',
            entityDbId: entityDbId,
            endpointEntity: 'GERMPLASM',
            application: 'GERMPLASM_CATALOG',
            processName: processName,
            dataType: dataType,
            endpoint: endpoint,
            requestBody: JSON.stringify(reqBodyParams),
            fileName: fileName+'Export'
        }

        // Invoke background worker
        $.ajax({
            url: '$exportToCSVUrl',
            type: 'post',
            dataType: 'json',
            data: requestBody,
            success: function(response) {
                var success = response.success;
                var status = response.status;

                // Display message
                if (success) {
                    var notif = "<i class='material-icons green-text left'>check</i> The export file is being created in the background." + 
                                " You may proceed with other tasks. You will be notified in this page once done.";
                    Materialize.toast(notif,5000);
                } else {
                    // Failure message
                    var message = '<i class="material-icons red-text left">close</i> A problem occurred while starting the background process.' +
                                ' Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status !== 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if (status == null) message += ' (#002)';
                    Materialize.toast(message,5000);
                }
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There was a problem loading the content.";
                Materialize.toast(notif,5000);
            }
        });
    }

    // facilitate invoking of background process
    function exportToHelium(data, filter){
        var descMessage;
        var entityDbId;
        var namePrepend = '';

        if(data.length == 1){
            descMessage = 'Germplasm Code: ' + data[0]['germplasmCode'];
            entityDbId = data[0]['germplasmDbId'];
            namePrepend = data[0]['germplasmCode']+'-';
        }
        else{
            descMessage = 'multiple germplasm records';
            entityDbId = data.map(germplasm => germplasm['germplasmDbId']).join('|');
        }

        // add depth filter
        filter['depth'] = exportDepthThresh;
        
        // Invoke background worker
        $.ajax({
            url: '$exportToHeliumUrl',
            type: 'post',
            dataType: 'json',
            data: {
                description: 'Preparing the pedigree of '+ descMessage +' for downloading',
                entity: 'GERMPLASM',
                entityDbId: entityDbId,
                endpointEntity: 'GERMPLASM',
                application: 'GERMPLASM_CATALOG',
                processName: 'export-pedigree',
                dataType: 'pedigree',
                endpoint: 'germplasm/:id/pedigrees-search',
                requestBody: JSON.stringify({filter: filter}),
                fileName: namePrepend+'Pedigree-Export'
            },
            success: function(response) {
                var success = response.success;
                var status = response.status;

                // Display message
                if (success) {
                    var notif = "<i class='material-icons green-text left'>check</i> The export file is being created in the background. You may proceed with other tasks. You will be notified in this page once done.";
                    Materialize.toast(notif,5000);
                } else {
                    // Failure message
                    var message = '<i class="material-icons red-text left">close</i> A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status !== 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if (status == null) message += ' (#002)';
                    Materialize.toast(message,5000);
                }
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There was a problem loading the content.";
                Materialize.toast(notif,5000);
            }
        });
    }

    function retrieveSelectedIds(selectAll){
        if(selectAll){
            $.ajax({
                type: 'POST',
                dataTye: 'json',
                url: '$retrieveDbIdsUrl',
                data: {},
                async: true,
                success: function (response) {
                    values = JSON.parse(response)

                    if(values.count > maxPrintoutsRecords){
                        var msg = 'Selection has exceeded the maximum number of records! ' + 
                                  ' The maximum number of records to be processed is ' +maxPrintoutsRecords + '.'
                        var message = '<i class="material-icons red-text" style="margin-right: 8px;">error</i>' + msg;
                        $('.toast').css('display','custom')
                        Materialize.toast(message, 5000)

                        $('.printouts-button').prop('disabled',true);
                        $('.printouts-button').addClass('disabled');

                        clearSelectedIds()
                    }                   
                },
                error: function (error) {
                }
            })
        }
    }

    function clearSelectedIds(){
        $.ajax({
            type: 'POST',
            dataTye: 'json',
            url: '$clearSelectedIdsUrl',
            data: {},
            async: true,
            success: function (response) {
                
            },
            error: function (error) {
            }
        })
    }

    function refresh(){
        var total = $('#dynagrid-germplasm-grid-id').find('.summary').children().eq(1).html();
        totalResultsCount = total !== undefined? parseInt(total.replace(/,/g, '')): 0;
        var maxHeight = ($(window).height() - 280);
        $(".kv-grid-wrapper").css("height",maxHeight);

        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });

        $('#reset-dynagrid-germplasm-grid').on('click', function(e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: toggleSelectAllUrl,
                data: {
                    type: 'germplasm',
                    mode: 'include mode'
                },
                async: false,
                dataType: 'json',
                success: function(response) {
                    $('#selected-count').html('No');
                    selectAllMode = 'include mode'
                },
                error: function(){
                }
            });

            $.pjax.reload({
                container: '#dynagrid-germplasm-grid-pjax',
                replace: true,
                url: resetGridUrl
            });
        });

        // ge-germplasm-coding-selected
        $('.ge-germplasm-coding-selected').on('click', function(e) {
            var selected = $('#selected-count').html();
            
            if(isNaN(selected)) {
                Materialize.toast("<i class='material-icons orange-text'>warning</i> No germplasm selected. Please select at least one (1) germplasm and try again.");
                return;
            }

            if(parseInt(selected) > germplasmCodingThreshold) {
                Materialize.toast("<i class='material-icons orange-text'>warning</i> The maximum allowed germplasm per coding transaction is " + germplasmCodingThreshold + ".");
                return;
            }

            // Redirect
            var redirectUrl = '$codingGermplasmUrl' + '&' + 'mode=selected&returnUrl=' + encodeURIComponent($(location).attr("href"));
            window.location.replace(redirectUrl);
        });

        $('.ge-germplasm-coding-all').on('click', function(e) {
            $.ajax({
                type: 'POST',
                url: '$getTotalCountUrl',
                data: {},
                async: false,
                dataType: 'json',
                success: function(response) {
                    if(parseInt(response) > germplasmCodingThreshold) {
                        Materialize.toast("<i class='material-icons orange-text'>warning</i> The maximum allowed germplasm per coding transaction is " + germplasmCodingThreshold + ".");
                        return;
                    }
                    
                    // Redirect
                    var redirectUrl = '$codingGermplasmUrl' + '&' + 'mode=all&returnUrl=' + encodeURIComponent($(location).attr("href"));
                    window.location.replace(redirectUrl);
                },
                error: function(){
                }
            });
        });

        // Clear selected items upon clicking reset button on query widget
        $('#query-clear-all-btn').on('click', function(e) {
            e.preventDefault();

            $.ajax({
                    type: 'POST',
                    url: toggleSelectAllUrl,
                    data: {
                        type: 'germplasm',
                        mode: 'include mode'
                    },
                    async: false,
                    dataType: 'json',
                    success: function(response) {
                        $('#selected-count').html('No');
                        selectAllMode = 'include mode'
                    },
                    error: function(){
                    }
                });
        });

        $('#germplasm-save-new-list-btn').on('click', function(e) {
            e.preventDefault();
            var count = $('#selected-count').html().replace(/,/g, '');
            if(parseInt(count) > 0) {
                $('#germplasm-save-list-modal').modal('show');
                $('#germplasm-save-list-modal').on('shown.bs.modal', function () {
                    $('#germplasm-list-name').focus();
                })

                $('#germplasm-count').html(count);

                $.ajax({
                    type: 'POST',
                    url: getSelectedFromSessionUrl,
                    data: {
                        type: 'germplasm'
                    },
                    async: false,
                    dataType: 'json',
                    success: function(response) {
                        selectedFromSession = response;
                    },
                    error: function(){
                        var notif = "<i class='material-icons red-text'>close</i> There was a problem while loading content.";
                        Materialize.toast(notif,5000);
                    }
                });
            } else {
                var notif = "<i class='material-icons orange-text'>warning</i> Please select a germplasm.";
                Materialize.toast(notif,5000);
            }
        });

        // validate if all required fields are specified
        $('.form-control').bind("change keyup input",function() {
            var abbrev = $('#germplasm-list-abbrev').val();
            var name = $('#germplasm-list-name').val();
            var display_name = $('#germplasm-list-display_name').val();

            if(abbrev != '' && name != '' && display_name != ''){
                $('#germplasm-save-list-confirm-btn').removeClass('disabled');
            } else {
                $('#germplasm-save-list-confirm-btn').addClass('disabled');
            }
        });

        // confirm save germplasm list
        $('#germplasm-save-list-confirm-btn').on('click', function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            $('.germplasm-modal-loading-save').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');

            var abbrev = $('#germplasm-list-abbrev').val();
            var name = $('#germplasm-list-name').val();
            var displayName = $('#germplasm-list-display_name').val();
            var description = $('#germplasm-list-description').val();
            var remarks = $('#germplasm-list-remarks').val();

            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: saveListUrl,
                    data: {
                        abbrev: abbrev,
                        name: name,
                        displayName: displayName,
                        type: 'germplasm',
                        description: description,
                        remarks: remarks,
                        selectedFromSession: selectedFromSession
                    },
                    dataType: 'json',
                    // async: false,
                    success: function(response) {

                        $('.germplasm-modal-loading-save').html('');
                        if(response['result']){
                            if(response['isBgProcess']) {
                                let exportEntriesUrl = '$listUrl'
                                let expEntUrl = window.location.origin + exportEntriesUrl
                                window.location.replace(expEntUrl)
                            }
                            var notif = "<i class='material-icons green-text'>done</i> " + response['message'];
                            $('#germplasm-save-list-modal').modal('hide');

                            $.pjax.reload({
                                container: '#dynagrid-germplasm-grid-pjax'
                            });

                            $.pjax.xhr = null;
                        } else {
                            var notif = "<i class='material-icons red-text'>close</i> "+ response['message'];
                        }
                        Materialize.toast(notif,5000);
                    },
                    error: function() {
                        $('.germplasm-modal-loading-save').html('');
                        var notif = "<i class='material-icons red-text'>close</i> There seems to be a problem while saving the list.";
                        Materialize.toast(notif,5000);
                        $('#germplasm-save-list-confirm-btn').addClass('disabled');
                    }
                });
            }, 500);
        });

        // If modal that contains excluded search items are to be shown
        if(localStorage.getItem('showExemptedFromQuerySearch') === 'true') {
            $('#germplasm-excluded-input-list-items-modal-body').html('<div class="row">'
                +'<div class="col-md-12">'
                    + '<p>The following inputs were excluded from the current results:</p>'
                    + '<textarea class="form-control input-filter variable-filter" id="query-exempted-txt-area" rows="20" spellcheck="false" style="position: relative; background-color: white; line-height: 20px;"></textarea>'
                +'</div>'
                +'</div>');
            $('#query-exempted-txt-area').val(localStorage.getItem('exemptedFromQuerySearchValues'));
            $('#germplasm-excluded-input-list-items-modal').modal('show');
            localStorage.setItem('showExemptedFromQuerySearch', false);
        }
    }

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();

        notifDropdown();

        $('#ge-search-notifs-btn').tooltip();
        $('#notif-container').html(
            '<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>'
        );

        renderNotifications();
        updateNotif();

        refresh();
    });

    $(document).on('ready pjax:complete', function(e) {
        e.stopPropagation();
        e.preventDefault();

        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();

        refresh();
    });

    $(document).ready(function() {
        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();

    /**
    * Move the rows to prevent being covered by the select2 filter row
    * There is a weird behavior of dynagrid that uses select2 as a filter for columns where
    * the row of the select2 widgets cover the results
    */
        var height = $('#dynagrid-germplasm-grid-id-filters').parent().outerHeight();
        $('#dynagrid-germplasm-grid-id-container').find('thead').children().eq(0).height(height);
        refresh();

        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        notifDropdown();
        renderNotifications();
        updateNotif();

        // Check for new notifications every 7.5 seconds
        notificationInterval=setInterval(updateNotif, 7500);
    });

JS
);

$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');

Yii::$app->view->registerCss('
    body{
        overflow-x: hidden;
    }
    .dropdown-content {
       overflow-y: visible;
    }
    .dropdown-content .dropdown-content {
       margin-left: -100%;
    }
    .grid-view td {
        white-space: nowrap;
    }
    .kv-align-center {
        text-align: left !important
    }
    .material-symbols-outlined {
        font-variation-settings:
        "FILL" 0,
        "wght" 400,
        "GRAD" 0,
        "opsz" 48
    }
');

?>