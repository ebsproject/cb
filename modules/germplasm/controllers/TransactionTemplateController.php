<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use Yii;

use app\controllers\Dashboard;

// Import interfaces
use app\interfaces\models\IUser;
use app\interfaces\models\IWorker;
use app\interfaces\modules\inventoryManager\models\IListsModel;

// Import models
use app\models\Program;
use app\models\CropProgram;
use app\models\Person;
use app\models\GermplasmFileUpload;

use app\modules\germplasm\models\TransactionTemplateModel;
use app\modules\inventoryManager\models\TemplatesModel;

/**
 * Controller class for template-related operations for File Upload
 */
class TransactionTemplateController extends \yii\web\Controller
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public IUser $user,
        public IWorker $worker,
        public IListsModel $listsModel,
        public TransactionTemplateModel $templatesModel,
        public Person $person,
        public Program $programModel,
        public CropProgram $cropProgramModel,
        public Dashboard $dashboardModel,
        public GermplasmFileUpload $germplasmFileUploadModel,
        public TemplatesModel $imTemplatesModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Render page for generating template from saved list
     * 
     * @param String $program program dashboard
     * @param String $action germplasm module used
     * @param String $entity data level
     * 
     * @return mixed
     */
    public function actionSelectList($program, $action, $entity){
        // Get user ID
        $userId = $this->user->getUserId();

        // Retrieve lists browser data
        $params = \Yii::$app->request->getQueryParams();

        if(isset($params['entity']) && !empty($params['entity'])){
            $params['ListsModel']['type'] = $params['entity'];
        }

        $searchModel = $this->listsModel;
        $dataProvider = $this->listsModel->search($params, $action);

        // Retrieve template options
        $templateOptions = $this->templatesModel->getTemplateOptions($action,$entity);
        $listTypeConfigs = $this->templatesModel->getTypeOptionsConfig() ?? [];

        // Set type filter options based on entity and action
        $listTypeFilterOptions = [];
        $sourceTool = '';
        $returnUrl = '';

        if( $action == $this->templatesModel->getActionUpdate() && 
            ($entity == $this->templatesModel->getEntityGermplasm() || 
             $entity == $this->templatesModel->getEntityGermplasmAttribute())){
                
                $listTypeFilters = [
                    'germplasm' => 'Germplasm'
                ];

                $sourceTool = 'Germplasm';
                $returnUrl = "/germplasm/".$action."/index";
        }

        // Data browser ID
        $browserId = 'dynagrid-fu-template-lists-grid';

        // Render view file
        return $this->render('lists', [
            'program' => $program,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'userId' => $userId,
            'action' => $action,
            'templateOptions' => $templateOptions,
            'listTypeConfigs' => $listTypeConfigs,
            'sourceTool' => $sourceTool,
            'returnUrl' => $returnUrl,
            'entity' => $entity,
            'browserId' => $browserId
        ]);
    }

    /**
     * Facilitate downloading of template from selected saved list
     * 
     * @param String $program dashboard program
     * @param String $action germplasm module used
     * @param String $entity data level used
     * @param Integer $id list unique identifier
     * @param String $templateType template type selected
     * 
     */
    public function actionDownloadTemplateFromList($program, $action, $entity, $id, $templateType){
        
        $listId = $id ?? null;
        $templateType = $templateType ?? 'germplasm';

        $fileNamePrefix = 'GM_' . strtoupper($action);
        
        // retrieve configuration
        [$userRole,$isAdmin,$userId] = $this->person->getPersonRoleAndType($program);

        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programId = $dashboardFilters['program_id'] ?? '';
        $cropCode = $this->programModel->getCropCode($program);
        
        $headers = $this->templatesModel->getTemplateHeaders(
            $entity,
            $action,
            $userRole,
            $program,
            $cropCode,
            $templateType);
        
        // retrieve list members
        if(!empty($listId)){
            $listAbbrev = $this->listsModel->getListAbbrev($listId);
            $fileNamePrefix = 'GM_'.($listAbbrev == '' ? '' : $listAbbrev.'_').strtoupper($action);

            $data = $this->templatesModel->getListMemberData($listId,$templateType,$headers,$action);
        }
        
        // Build CSV
        if(!empty($headers)){
            $this->imTemplatesModel->buildCSVFile(
                headers: $headers,
                programCode: $program,
                type: $templateType,
                data: $data,
                fileNamePrefix: $fileNamePrefix
            );
        }
        else{
            return false;
        }
    }
}