<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use Yii;
use yii\helpers\Url;
use app\components\B4RController;
use app\controllers\Dashboard;
use app\models\GermplasmFileUpload;
use app\models\BackgroundJob;
use app\models\User;
use app\models\Worker;
use app\models\Program;
use app\models\Crop;
use app\models\CropProgram;
use app\interfaces\models\IConfig;

use app\modules\germplasm\models\CreateModel;
use app\modules\germplasm\models\ViewBrowserModel;
use app\modules\germplasm\models\ErrorLogModel;
use app\modules\germplasm\models\ViewFileUploadModel;
use app\modules\inventoryManager\models\TemplatesModel;
// Import data providers
use app\dataproviders\ArrayDataProvider;

/**
 * Controller for the `germplasm` module
 * Contains methods for managing creation of new germplasms
 */
class CreateController extends B4RController
{

    public $dashboardModel;
    public $germplasmFileUploadModel;
    public $createModel;
    public $viewBrowserModel;
    public $errorLogModel;
    public $viewFileUploadModel;
    public $programModel;
    public $cropModel;
    public $cropProgramModel;
    public $userModel;
    public $configurations;
    public $templatesModel;

    public $backgroundJob;
    public $worker;

    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        Dashboard $dashboardModel,
        GermplasmFileUpload $germplasmFileUploadModel,
        CreateModel $createModel,
        ViewBrowserModel $viewBrowserModel,
        ErrorLogModel $errorLogModel,
        ViewFileUploadModel $viewFileUploadModel,
        Worker $worker,
        Program $programModel,
        User $userModel,
        BackgroundJob $backgroundJob,
        Crop $cropModel,
        CropProgram $cropProgramModel,
        IConfig $configurations,
        TemplatesModel $templatesModel,
        $config = []
        )
    {
        
        $this->dashboardModel = $dashboardModel;
        $this->germplasmFileUploadModel = $germplasmFileUploadModel;
        $this->worker = $worker;
        $this->createModel = $createModel;
        $this->cropModel = $cropModel;
        $this->cropProgramModel = $cropProgramModel;
        $this->viewBrowserModel = $viewBrowserModel;
        $this->errorLogModel = $errorLogModel;
        $this->viewFileUploadModel = $viewFileUploadModel;
        $this->programModel = $programModel;
        $this->userModel = $userModel;
        $this->configurations = $configurations;
        $this->templatesModel = $templatesModel;

        $this->backgroundJob = $backgroundJob;
        $this->worker = $worker;

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the data browser germplasm file upload transactions
     * @param string program current program code
     * @return mixed
     */
	public function actionIndex($program = null) {
        // Determine if user is ADMIN user
        $isAdmin = Yii::$app->session->get('isAdmin');
        // Check RBAC if has access
        $hasAccess = Yii::$app->access->renderAccess('GERMPLASM_CREATE','GERMPLASM_CATALOG');
        // Check allowed actions to determine tabs to display
        $permissions = $isAdmin ? ['GERMPLASM_SEARCH', 'GERMPLASM_CREATE', 'GERMPLASM_MERGE', 'GERMPLASM_UPDATE'] : Yii::$app->session->get('currentPermissions')['permission'] ?? ['GERMPLASM_SEARCH'];
        // Redirect to proper page depending on access permissions
        if(!$isAdmin){
            if(!$hasAccess) {
                if(empty($permissions)) {
                    Yii::$app->session->setFlash('error', "You have no permission to the page you are trying to access. Please contact your data administrator.");
                    Yii::$app->response->redirect(Url::base().'/index.php');
                    return;
                } else {
                    return $this->redirect([
                        strtolower(substr($permissions[0], 10)).'/index',
                        'program' => $program
                    ]);
                }
            }
        }

        // initialize session variables
        Yii::$app->session->set($this->createModel->getKeyVariables(), null);
        Yii::$app->session->set($this->createModel->getKeyVariablesRequired(), null);
        Yii::$app->session->set($this->createModel->getKeyVariablesOptional(), null);

        $programDbId = Yii::$app->userprogram->get('id');

        // extract variable config
        $configVars = $this->createModel->getVariableConfig($programDbId);
        $sysDefVars = $this->createModel->getVariableConfig($programDbId,true);

        $configVars = array_merge($configVars, $sysDefVars);

        // save variables to session
        Yii::$app->session->set($this->createModel->getKeyVariables(), $configVars);

        $searchModel = $this->germplasmFileUploadModel;
        $dataProvider = $this->germplasmFileUploadModel->search(null,[
            "fileUploadOrigin" => "equals GERMPLASM_CATALOG",
            "fileUploadAction" => "create"
        ]);

        return $this->render('index',[
            'programDbId' => $programDbId,
            'program' => Yii::$app->userprogram->get('abbrev'),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'browserId' => 'dynagrid-germplasm-gefu-grid'
        ]);
    }

    /**
     * Facilitates rendering of file upload modal content
     * 
     * @return mixed
     */
    public function actionRenderFileUpload(){
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programDbId = $dashboardFilters['program_id'];
        $programCode = $_POST['programCode'] ?? '';
        $type = $_POST['type'] ?? '';

        $htmlData = $this->renderAjax('_file_upload', [
            'program' => $programDbId,
            'programCode' => $programCode,
            'type' => $type
        ]);

        return json_encode($htmlData);
    }

    /**
     * Facilitates processing of uploaded file
     * @param Integer programId - program identifier
     * @param String programCode - code of the program
     * @param String type - upload type (germplasm, germplasm_relation)
     * @return mixed
     */
    public function actionProcessFileUpload($programId, $programCode, $type){
        $success = true;
        $message = '';
        $origin = 'GERMPLASM_CATALOG';

        try{
            // retrieve uploaded file
            $uploadedFileInfo = $_FILES['germplasm_file_upload'];

            if(isset($uploadedFileInfo) && !empty($uploadedFileInfo)){
                $variableConfig = $this->getConfig($programCode, 'create', $type);

                // retrieve uploaded file content
                $uploadedFile = $this->createModel->retrieveUploadedFile('germplasm_file_upload');

                if($uploadedFile['success']){
                    // If row count exceed the limit, show error message.
                    if($type == 'germplasm_relation' && count($uploadedFile['fileContent']) > 500) {
                        $uploadLimitFormat = number_format(500);
                        $result = [
                            'success' => false,
                            'error' => "The file exceeds the maximum number of rows allowed. ($uploadLimitFormat)"
                        ];
                    }
                    else {
                        // validate file contents
                        $result = $this->createModel->validateUploadedFile($programId,$uploadedFile['headers'],$variableConfig);
                    }

                    if($result['success']){
                        // create new file upload record
                        $result = $this->createModel->saveUploadedFile($programId,$uploadedFile['fileName'],$uploadedFile['fileContent'],$origin,$type);
                    }

                    return json_encode($result);
                }
                else{
                    $success = false;
                    $error = $uploadedFile['error'];
                }
            }
        }
        catch(\Exception $e){
            $success = false;
            $error = 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.';
        }

        return json_encode([
            'success' => $success,
            'message' => $message,
            'error' => $error
        ]) ;
    }

    /**
     * Renders the preview of records to be created
     * @param Integer $germplasmFileUploadDbId file upload identifier
     */
    public function actionFileUploadPreview($germplasmFileUploadDbId = 0) {
        // Get program
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programDbId = $dashboardFilters['program_id'];
        
        // Get file upload info
        $fileDataPreviewInfo = $this->viewBrowserModel->search($germplasmFileUploadDbId, $programDbId);

        return $this->render('_file_data_preview',[
            'germplasmFileUploadDbId' => $germplasmFileUploadDbId,
            'fileDataPreviewInfo' => $fileDataPreviewInfo,
            'program' => $programDbId,
            'searchModel' => $this->viewBrowserModel,
            'browserId' => $fileDataPreviewInfo['browserId']
        ]);
    }

    /**
     * Renders the error log of records
     * @param Integer $germplasmFileUploadDbId file upload identifier
     */
    public function actionErrorLogPage($germplasmFileUploadDbId = 0) {
        // Get program
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programDbId = $dashboardFilters['program_id'];

        // Get file error log info
        $fileErrorLogInfo = $this->errorLogModel->search($germplasmFileUploadDbId, $programDbId);

        return $this->render('_error_log_page',[
            'germplasmFileUploadDbId' => $germplasmFileUploadDbId,
            'fileErrorLogInfo' => $fileErrorLogInfo,
            'program' => $programDbId,
            'searchModel' => $this->viewBrowserModel,
            'browserId' => 'gm-error-log-grid'
        ]);
    }

    /**
     * Renders the germplasm of records
     * @param Integer $germplasmFileUploadDbId file upload identifier
     */
    public function actionViewFileUploadPage($germplasmFileUploadDbId = 0) {
        $entity = isset($_POST['entity']) ? $_POST['entity'] : '';

        $sessionName = 'view-file-upload-entity';
        $entitySession = Yii::$app->session[$sessionName];

        if((is_null($entity) || $entity == '') && (is_null($entitySession) || $entitySession == '')) {  // entity and entity session has no value, default to germplasm
            $entity = 'germplasm';
        } else if((is_null($entity) || $entity == '') && !(is_null($entitySession) || $entitySession == '')) {  // entity has no value but entity session has value, use entity session
            $entity = $entitySession;
        }   // entity has value and entity session has no value OR entity has value and is not equal to entity session, use entity to set entity session

        Yii::$app->session->set($sessionName, $entity); // set new entity session value either way

        // Get program
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programDbId = $dashboardFilters['program_id'];

        // Get file error log info
        $fileUploadInfo = $this->viewFileUploadModel->search($germplasmFileUploadDbId, $programDbId);

        return $this->render('_view_file_upload_page',[
            'germplasmFileUploadDbId' => $germplasmFileUploadDbId,
            'fileUploadInfo' => $fileUploadInfo,
            'program' => $programDbId,
            'searchModel' => $this->viewBrowserModel,
            'entity' => $entity,
            'browserId' => [
                    'gm-file-upload-germplasm-grid', 
                    'gm-file-upload-seed-grid' , 
                    'gm-file-upload-package-grid'
                ]
        ]);
    }

    /**
     * Facilitates the generation of Germplasm File Upload CSV template
     * 
     * @param Integer $program program identifier
     */
    public function actionDownloadTemplateProcess($program){

        // retrieve template variables
        $cropVariablesConfig = $this->createModel->getVariableConfig($program);
        $sysVariableConfig = $this->createModel->getVariableConfig($program,true);
        
        // transform variable headers
        $entityOrderArr = ['germplasm','seed','package'];

        $templateHeaders = [];
        foreach($entityOrderArr as $entity){            
            $sysVarConfigArr = array_filter($sysVariableConfig,function($sysVarConfig) use ($entity){
                return $sysVarConfig['entity'] == $entity; 
            });
            
            $sysVarConfigArr = array_column($sysVarConfigArr,'abbrev');

            $templateHeaders = array_merge($templateHeaders,$sysVarConfigArr);

            if(!empty($cropVariablesConfig)){
                $cropVarConfigArr = array_filter($cropVariablesConfig,function($variableConfig) use ($entity){
                    return $variableConfig['entity'] == $entity; 
                });

                $cropVarConfigArr = array_column($cropVarConfigArr,'abbrev');

                $templateHeaders = array_merge($templateHeaders,$cropVarConfigArr);
            }
        }
        
        if(!empty($templateHeaders)){
            $programInfo = $this->programModel->getProgram($program);
            $cropInfo = $this->cropProgramModel->searchAll(
                ["cropProgramDbId" => "equals ".$programInfo['cropProgramDbId']],
                '',
                false
            );
            $cropInfo = $cropInfo['data'];
            
            $programCode = isset($programInfo) && !empty($programInfo) ? $programInfo['programCode'] : '';
            $cropCode = isset($cropInfo[0]) && !empty($cropInfo[0]) ? $cropInfo[0]['cropCode'] : '';

            // generateCSVTemplate
            $this->createModel->generateCSVTemplate($templateHeaders,$cropCode,$programCode);
        }
        else{
            return false;
        }
    }

    /**
     * Renders the creation summary in the confirmation modal
     */
    public function actionRenderCreationSummary() {
        $germplasmFileUploadDbId = isset($_POST['germplasmFileUploadDbId']) ? $_POST['germplasmFileUploadDbId'] : 0;
        $action = $_POST['action'] ?? 'create';
        $countArray = [];

        // Retrieve file upload transaction record
        $requestBody = [
            "germplasmFileUploadDbId" => "equals ".$germplasmFileUploadDbId
        ];
        $result = $this->germplasmFileUploadModel->searchAll($requestBody);
        $germplasmFileUpload = $result['data'][0] ?? [];
        $fileName = $germplasmFileUpload['fileName'];
        $fileData = $germplasmFileUpload['fileData'];

        // Determine count per entity based on the rows in the file data
        foreach($fileData as $row) {
            // Get entities included in row.
            $rowEntities = (array) array_keys($row);
            
            // Update count for each entity
            foreach($rowEntities as $entity) {
                // If count array does not contain
                // a count for the current entity,
                // initialize the count to zero (0).;
                if(!isset($countArray[$entity])) {
                    $countArray[$entity] = 0;
                }

                // Increment count
                $countArray[$entity] += 1;
            }
        }

        // Declare object for data provider
        $creationSummaryObject = [];
        // Loop through count array to render the count per entity
        foreach($countArray as $entity => $count) {
            // Build display name. Replaces all underscore instances with a space,
            // and capitalizes the first letter of the first word.
            $entityDisplayName = str_replace("_", " ",ucfirst($entity));

            // If count is greater than 0, add to the summary object.
            if($count > 0) {
                $creationSummaryObject[] = [
                    'summaryItemId' => 0,
                    'entity' => $entityDisplayName,
                    'count' => $count
                ];
            }
        }

        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'allModels' => $creationSummaryObject,
            'key' => 'summaryItemId',
            'restified' => true,
            'totalCount' => count($creationSummaryObject)
        ]);

        // Render summary
        return $this->renderAjax('_creation_summary', [
            "dataProvider" => $dataProvider,
            "fileName" => $fileName,
            "action" => $action,
        ]);
    }

    /**
     * Controller action for invoking workers. The germplasm file upload ID,
     * worker name, and description must be specified.
     */
    public function actionInvokeGermplasmWorker() {
        // Get germplasm file upload ID
        $germplasmFileUploadDbId = isset($_POST['germplasmFileUploadDbId']) ? $_POST['germplasmFileUploadDbId'] : 0;
        // Get worker name
        $workerName = isset($_POST['workerName']) ? $_POST['workerName'] : '';
        // description
        $description = isset($_POST['description']) ? $_POST['description'] : '';
        // additional worker data
        $additionalWorkerData = isset($_POST['additionalWorkerData']) ? (array) json_decode($_POST['additionalWorkerData']) : [];

        // Invoke worker
        $result = $this->worker->invoke(
            $workerName,                                    // Worker name
            $description,                                   // Description
            'GERMPLASM_FILE_UPLOAD',                        // Entity
            $germplasmFileUploadDbId,                       // Entity ID
            'GERMPLASM_FILE_UPLOAD',                        // Endpoint Entity
            'GERMPLASM_CATALOG',                            // Application
            'POST',                                         // Method
            [                                               // Worker data
                "germplasmFileUploadDbId" => $germplasmFileUploadDbId
            ],
            null,
            $additionalWorkerData
        );

        // Prepare flash message
        $type = 'info';
        $message = '';
        $iconClass = '';
        if($result['success']) {
            $type = 'info';
            $iconClass = 'fa fa-info-circle';
            $message = '<i class="' . $iconClass . ' hm-bg-info-icon"></i>&nbsp;&nbsp;The germplasm records are being created in the background. You may proceed with other tasks. You will be notified in this page once done.';
        }
        else {
            $type = 'warning';
            $iconClass = 'fa-fw fa fa-warning';
            $message = '<i class="' . $iconClass . ' hm-bg-info-icon"></i>&nbsp;&nbsp;A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
            // If status is not 200, the problem occurred while creating the background job record (code: #001)
            if($result['status'] != null && $result['status'] != 200) $message .= ' (#001)';
            // If status is 200, the problem occurred while connecting to the background worker (code: #002)
            else if($result['status'] == null) $message += ' (#002)';
        }
        // set flash
        \Yii::$app->session->setFlash($type, $message);

        // Redirect to the main create page
        return $this->actionRedirectToMainCreatePage();
    }

    /**
     * Redirects to the main create page (index)
     */
    public function actionRedirectToMainCreatePage() {
        // Get program in dashboard
        $defaultFilters = (array) $this->dashboardModel->getFilters();
        $programId = $defaultFilters['program_id'];
        $result = $this->programModel->getOne($programId);
        $programInfo = $result['data'] ?? [];
        $program = $programInfo['programCode'] ?? '';

        Yii::$app->session->set('view-file-upload-entity', null); // reset entity session for viewing of germplasm file upload records

        // Redirect to the main create page
        return $this->redirect(['create/index', 'program' => $program]);
    }
    
    /**
     * Triggers the background process for validating file upload record
     */
    public function actionValidateDataBgProcess(){
        
        $germplasmfileUploadDbId = isset($_POST['fileUploadId']) ? $_POST['fileUploadId'] : 0;
        $type = isset($_POST['type']) ? $_POST['type'] : 'germplasm';

        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programDbId = $dashboardFilters['program_id'];
        $programInfo = $this->programModel->getProgram($programDbId);

        $cropInfo = $this->cropProgramModel->searchAll(
            ["cropProgramDbId" => "equals ".$programInfo['cropProgramDbId']],
            '',
            false
        );
        $cropInfo = $cropInfo['data'];

        $cropCode = isset($cropInfo[0]) && !empty($cropInfo[0]) ? $cropInfo[0]['cropCode'] : '';

        // Invoke worker
        $result = $this->worker->invoke(
            'ValidateGermplasmFileUpload',                                              // Worker name
            'Validate data of file upload record : ' . $germplasmfileUploadDbId,        // Description
            'GERMPLASM_FILE_UPLOAD',                                                    // Entity
            $germplasmfileUploadDbId,                                                   // Entity ID
            'GERMPLASM_FILE_UPLOAD',                                                    // Endpoint Entity
            'GERMPLASM_CATALOG',                                                        // Application
            'POST',                                                                     // Method
            [                                                                           // Worker data
                "germplasmfileUploadDbId" => $germplasmfileUploadDbId,
                "programDbId" => $programDbId,
                "type" => $type
            ]
        );

        return json_encode($result);
    }

    /**
     * Facilitates the retrieval of push notifications
     */
    public function actionPushNotifications(){
        
        $workerName = "equals ValidateGermplasmFileUpload|equals CreateGermplasmRecords";

        $userId = $this->userModel->getUserId();

        $params = [
            "creatorDbId" => "equals $userId",
            "workerName" => $workerName,
            "isSeen" => "equals false"
        ];
        $result = $this->backgroundJob->searchAll($params, "sort=modificationTimestamp:desc");
        

        $newNotifications = $result["data"];
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->backgroundJob->getGenericNotifications($newNotifications);

        $params = [
            "creatorDbId" => "equals $userId",
            "workerName" => $workerName,
            "isSeen" => "equals true"
        ];
        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);

        $notifications = $result["data"];
        $seenCount = count($notifications);
        $seenNotifications = $this->backgroundJob->getGenericNotifications($notifications);

        // update unseen notifications to seen
        $params = [
            "jobIsSeen" => "true"
        ];
        $result = $this->backgroundJob->update($newNotifications, $params);

        return $this->renderAjax(
            '_notifications',
            [
                "unseenNotifications" => $unseenNotifications,
                "seenNotifications" => $seenNotifications,
                "unseenCount" => $unseenCount,
                "seenCount" => $seenCount,
            ]
        );
    }

    /**
     * Facilitates the retrieval of push notifications
     */
    public function actionNewNotificationsCount(){
        // Get worker name
        $workerName = "equals ValidateGermplasmFileUpload|equals CreateGermplasmRecords";
        // Get user id
        $userId = $this->userModel->getUserId();

        // Parameters for background jobs search
        $params = [
            "creatorDbId" => "equals $userId",
            "workerName" => $workerName,
            "isSeen" => "equals false"
        ];
        $filter = 'sort=modificationTimestamp:desc';   
        $result = $this->backgroundJob->searchAll($params, $filter);

        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        }

        return json_encode($result["totalCount"]);
    }

    /**
     * Delete file upload records
     */
    public function actionDeleteFileUploadRecord() {
        $germplasmFileUploadDbId = $_POST['germplasmFileUploadDbId'] ?? 0;

        $this->germplasmFileUploadModel->deleteOne($germplasmFileUploadDbId);
    }

    /**
     * Generates templates for germplasm manager
     * @param String program - program code value
     * @param String action - file upload action. Defaults to 'create'.
     * @param String type - type of data to handle eg. germplasm, germplasm_relation. Defaults to 'germplasm'.
     */
    public function actionDownloadTemplate($program, $action = "create", $type = "germplasm") {
        // Retrieve config
        $config = $this->getConfig($program, $action, $type);

        // Extract template headers based on config
        $headers = $this->templatesModel->extractTemplateHeaders($config, ['germplasm_relation']);

        $data = [[]];
        $fileNamePrefix = "GM_" . strtoupper($action);
        
        // Build CSV
        if(!empty($headers)){
            $this->templatesModel->buildCSVFile(
                headers: $headers,
                programCode: $program,
                type: $type,
                data: $data,
                fileNamePrefix: $fileNamePrefix
            );
        }
        else{
            return false;
        }
    }

    /**
     * Retrieves config given the program, action and type
     * 
     * @param String program - code of the program
     * @param String action - action to perform (create, update)
     * @param String type - type of upload (germplasm, germplasm_relation)
     */
    public function getConfig($program, $action, $type) {
        $configPrefix = 'GM_'
            . strtoupper($action) . '_' 
            . strtoupper($type) . '_';
        // TEMPORARY - old configs are to be renamed
        if($type == 'germplasm') {
            $configPrefix = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_';
        }

        // Get system config
        $systemConfig = $this->configurations->getConfigByAbbrev($configPrefix . 'SYSTEM_DEFAULT')['values'] ?? [];
        // Get program/crop specific config
        $cropCode = $this->templatesModel->getCropCodeGivenProgramCode($program);
        $programConfig = $this->configurations->getConfigByAbbrev($configPrefix . $cropCode . '_' . $program)['values'] ?? [];
        if(empty($programConfig)) {
            $programConfig = $this->configurations->getConfigByAbbrev($configPrefix . $cropCode . '_DEFAULT')['values'] ?? [];
        }
        // Merge configs
        return array_merge($systemConfig, $programConfig);
    }
}

?>