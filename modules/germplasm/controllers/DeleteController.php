<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use Yii;
use app\components\B4RController;
use app\controllers\Dashboard;

use app\models\Germplasm;
use app\models\GermplasmFileUpload;

use app\modules\search\models\SearchEntity;
use app\modules\germplasm\models\DeleteGermplasmModel;

/**
 * Controller for the `germplasm` module
 * Contains methods for managing deletion of germplasm
 */
class DeleteController extends B4RController
{
    public $dashboard;
    public $searchEntity;
    public $germplasmModel;
    public $deleteGermplasmModel;
    public $germplasmFileUploadModel;

    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        Dashboard $dashboard,
        SearchEntity $searchEntity,
        Germplasm $germplasmModel,
        DeleteGermplasmModel $deleteGermplasmModel,
        GermplasmFileUpload $germplasmFileUploadModel,
        $config = []
        )
    {
        $this->dashboard = $dashboard;
        $this->searchEntity = $searchEntity;
        $this->germplasmModel = $germplasmModel;
        $this->deleteGermplasmModel = $deleteGermplasmModel;
        $this->germplasmFileUploadModel = $germplasmFileUploadModel;

        parent::__construct($id, $module, $config);
    }

    /** 
     * Facilitate deletion of germplasm record and associated records 
     * 
     * @return mixed success status and process message
     */
    public function actionDeleteGermplasm(){
        $germplasmIdArr = $_POST['germplasmId'];

        $success = true;
        $message = '';

        if(isset($germplasmIdArr) && !empty($germplasmIdArr)){
            foreach($germplasmIdArr as $germplasmId){
                // delete germplasm relation record/s
                $delRelStatus = $this->deleteGermplasmModel->deleteGermplasmRelation($germplasmId);

                if($delRelStatus){
                    // delete germplasm attribute record/s
                    $delAttribStatus = $this->deleteGermplasmModel->deleteGermplasmAttribute($germplasmId);

                    if($delAttribStatus){
                        // delete germplasm name record/s
                        $delGermplasmNameStatus = $this->deleteGermplasmModel->deleteGermplasmName($germplasmId);

                        if($delGermplasmNameStatus){
                            // delete germpasm record
                            $delGermplasmStatus = $this->deleteGermplasmModel->deleteGermplasm($germplasmId);

                            if($delGermplasmStatus){
                                $success = true;
                                $message = 'Successfully deleted germplasm record!';
                            }
                            else{
                                $success = false;
                                $message .= ' Deletion of germplasm record failed!';
                            }
                        }
                        else{
                            $success = false;
                            $message .= ' Deletion of germplasm name record/s failed!';
                        }
                    }
                    else{
                        $success = false;
                        $message .= ' Deletion of germplasm attribute record/s failed!';
                    }
                }
                else{
                    $success = false;
                    $message = 'Deletion of germplasm relation record/s failed!';
                }
            }
        }
        else{
            $success = false;
            $message = 'Invalid germplasm ID provided for deletion.';
        }

        return json_encode(['success'=> $success, 'message'=> $message]);
    }



    /**
     * Delete draft germplasm name records associated with a trasaction 
     */
    function actionDeleteTransactionGermplasmNames($program)
    {
        $postParams = $_POST;
        $transactionDbId = $postParams['transactionDbId'];

        // retrieve file-upload record information
        $requestBody = [
            "germplasmFileUploadDbId" => "equals " . $transactionDbId
        ];
        $codingTrasactionRecord = $this->germplasmFileUploadModel->searchAll($requestBody);

        if (
            $codingTrasactionRecord['status'] != 200 ||
            !isset($codingTrasactionRecord['data']) ||
            empty($codingTrasactionRecord['data'])
        ) {
            // Redirect to the main update page
            return $this->redirect(['update/index', 'program' => $program]);
        }

        $transactionData = $codingTrasactionRecord['data'][0]['fileData'][0] ?? [];

        $germplasmIdsArr = [];
        $germplasmNameType = '';
        $germplasmNameStatus = 'draft';
        if (!empty($transactionData)) {
            $germplasmIdsArr = $transactionData['germplasmDbIds'] ?? [];
            $germplasmNameType = $transactionData['germplasmNameType'] ?? '';
            $germplasmNameStatus = 'draft';
        }

        $this->deleteGermplasmModel->deleteTransactionGermplasmName($germplasmIdsArr, $germplasmNameType, $germplasmNameStatus);

        return true;
    }
}
