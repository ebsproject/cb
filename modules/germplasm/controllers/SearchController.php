<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use Yii;
use yii\helpers\Url;
use app\components\B4RController;
use app\models\Config;
use app\models\GermplasmSearch;
use app\models\Germplasm;
use app\models\Program;
use app\models\Person;
use app\dataproviders\ArrayDataProvider;
use app\controllers\Dashboard;
use app\modules\search\models\SearchEntity;
use app\interfaces\models\IUser;
use app\interfaces\models\IWorker;

use app\modules\germplasm\models\CodingModel;

/**
 * Controller for the `germplasm` module
 * Contains methods for managing germplasms
 */
class SearchController extends B4RController
{
    public $configuration;
    public $dashboard;
    public $searchEntity;
    public $germplasmSearchModel;
    public $germplasmModel;
    public $programModel;
    public $personModel;
    public $user;
    public $worker;

    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        Config $configuration,
        Dashboard $dashboard,
        SearchEntity $searchEntity,
        GermplasmSearch $germplasmSearchModel,
        Germplasm $germplasmModel,
        Program $programModel,
        Person $personModel,
        IUser $user,
        IWorker $worker,
        public CodingModel $codingModel,
        $config = []
        )
    {
        $this->configuration = $configuration;
        $this->dashboard = $dashboard;
        $this->searchEntity = $searchEntity;
        $this->germplasmSearchModel = $germplasmSearchModel;
        $this->germplasmModel = $germplasmModel;
        $this->programModel = $programModel;
        $this->personModel = $personModel;
        $this->user = $user;
        $this->worker = $worker;

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the data browser for germplasm catalog
     * 
     * @param String $program Current dashboard program
     * 
     * @return mixed
     */
	public function actionIndex($program=null) {
        // Determine if user is ADMIN user
        $isAdmin = Yii::$app->session->get('isAdmin');
        // Check RBAC if has access
        $hasAccess = Yii::$app->access->renderAccess('GERMPLASM_SEARCH','GERMPLASM_CATALOG');
        // Check allowed actions to determine tabs to display
        $permissions = $isAdmin ? ['GERMPLASM_SEARCH', 'GERMPLASM_CREATE', 'GERMPLASM_MERGE', 'GERMPLASM_UPDATE'] : Yii::$app->session->get('currentPermissions')['permission'] ?? ['GERMPLASM_SEARCH'];
        // Redirect to proper page depending on access permissions
        if(!$isAdmin){
            if(!$hasAccess) {
                if(empty($permissions)) {
                    Yii::$app->session->setFlash('error', "You have no permission to the page you are trying to access. Please contact your data administrator.");
                    Yii::$app->response->redirect(Url::base().'/index.php');
                    return;
                } else {
                    return $this->redirect([
                        strtolower(substr($permissions[0], 10)).'/index',
                        'program' => $program
                    ]);
                }
            }
        }

        $searchModel = $model = $this->germplasmSearchModel;
        $params = Yii::$app->request->queryParams;

        $queryParams = [];
        $sessionName = 'query-tool-germplasm';

        if (isset($_POST['Query'])) {
            $queryParams = $_POST['Query'];
            Yii::$app->session->set($sessionName, $queryParams);

            // Initialize cache variables for Select All
            Yii::$app->session->set('germplasm-select_all_flag', 'include mode');   // (string: "exclude mode"/"include mode")
            Yii::$app->session->set('germplasm-selected_ids', []);                  // (string array of ids)
            Yii::$app->session->set('germplasm-total_results_count', 0);            // (integer)
            Yii::$app->session->set('germplasm-api_filter', []);                    // (json)

            Yii::$app->session->set('germplasm-search-query-params', []);
            Yii::$app->session->set('germplasm-search-browser-filters', []);
            Yii::$app->session->set('germplasm-process-thresholds', []);
        } else if (isset($_POST['reset'])) {
            if (isset(Yii::$app->session[$sessionName])) {
                unset(Yii::$app->session[$sessionName]);
            }
            // return programID
            return Yii::$app->userprogram->get('id');
        }else {
            if (isset(Yii::$app->session[$sessionName]) && !empty(Yii::$app->session[$sessionName])) {
                $queryParams = Yii::$app->session[$sessionName];
                $dataProvider = $searchModel->search($params, $queryParams);
            } else {
                // if no filters are set, return empty dataprovider
                $dataProvider = new ArrayDataProvider([
                    'allModels' => []
                ]);
            }

            $selectedIds = isset(Yii::$app->session['germplasm-selected_ids'])? Yii::$app->session['germplasm-selected_ids']: [];
            $selectAllMode = isset(Yii::$app->session['germplasm-select_all_flag'])? Yii::$app->session['germplasm-select_all_flag']: 'include mode';

            // get total count limit from config
            $totalCountLimit = $this->configuration->getConfigByAbbrev('CB_API_TOTAL_COUNT_LIMITS');

            // Total results
            $totalResultsCount = Yii::$app->session->get('germplasm-total_results_count');

            // Retrieve germplasm coding threshold
            $germplasmCodingThreshold = $this->codingModel->getGermplasmCodingThreshold();

            // Retrieve process thresholds
            $baseConfigAbbrevSuffix = '_PROCESS_THRESHOLD_CONFIG';
            $processThresholdValues = $this->getConfigValues(
                $program,
                'GM_GLOBAL' . $baseConfigAbbrevSuffix,
                $baseConfigAbbrevSuffix
            );
            Yii::$app->session->set('germplasm-process-thresholds', $processThresholdValues);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'searchModel' => $searchModel,
                'sessionName' => $sessionName,
                'selectedIds' => $selectedIds,
                'selectAllMode' => $selectAllMode,
                'totalResultsCount' => $totalResultsCount,
                'program' => Yii::$app->userprogram->get('abbrev'),
                'totalCountLimit' => empty($totalCountLimit) ? 500000 : $totalCountLimit,
                'germplasmCodingThreshold' => $germplasmCodingThreshold,
                'codingModel' => $this->codingModel,
                'sessProcessThresh' => $processThresholdValues
            ]);
        }
    }

    /**
     * Set the necessery sessions to search the germplasm in find seeds tool
     */
    public function actionAddFindSeedsSession() {

        $designation = $_POST['designation'];
        $params = [];
        $params['designation'] = $designation;
        Yii::$app->session->set('resultsFilters', $params);
        $dataFilters = [
            [
                'name'=> 'NAME',
                'value'=> $designation
            ]
        ];
        $filterAbbrev = [
            [
                'abbrev' => 'NAME',
                'filter_type' => 'search',
                'operator' => 'in',
                'values' => [
                    $designation
                ]
            ]   
        ];
        Yii::$app->session->set('dataFilters', $dataFilters);
        Yii::$app->session->set('filterAbbrevs', $filterAbbrev);

        return json_encode(['success'=> true]);
    }

    /**
     * Export germplasm pedigree to Helium format
     */
    public function actionExportToHelium() {
        Yii::debug(json_encode($_POST), __METHOD__);

        $description = $_POST['description'] ?? '';
        $entity = $_POST['entity'] ?? '';
        $entityDbId = $_POST['entityDbId'] ?? 0;
        $entityDbId = intval($entityDbId);
        $endpointEntity = $_POST['endpointEntity'] ?? '';
        $application = $_POST['application'] ?? '';

        // Build worker data
        $processName = $_POST['processName'] ?? '';
        $dataType = $_POST['dataType'] ?? '';
        $endpoint = $_POST['endpoint'] ?? '';
        $requestBody = $_POST['requestBody'] ?? '{}';
        $fileName = $_POST['fileName'] ?? '';

        $userDbId = $this->user->getUserId();

        $result = $this->worker->invoke(
            'BuildHeliumData',
            $description,
            $entity,
            $entityDbId,
            $endpointEntity,
            $application,
            'POST',
            [
                'processName' => $processName,
                'description' => $description,
                'dataType' => $dataType,
                'userId' => $userDbId,
                'url' => $endpoint,
                'requestBody' => $requestBody,
                'fileName' => $fileName.'_'.date('Ymd_His',time())
            ],
            [
                'remarks' => $fileName.'_'.date('Ymd_His',time()).'.helium'
            ]
        );

        return json_encode($result);
    }

    // initiate background process worker for generating export CSV data
    public function actionExportToCsv(){
        Yii::debug(json_encode($_POST), __METHOD__);

        $postParams = $_POST;

        $description = $postParams['description'] ?? '';
        $endpointEntity = $postParams['endpointEntity'] ?? '';
        $application = $postParams['application'] ?? '';
        $entity = $postParams['entity'] ?? '';
        $entityDbId = $postParams['entityDbId'] ?? 0;
        $entityDbId = intval($entityDbId);

        // Build worker data
        $processName = $postParams['processName'] ?? '';
        $fileName = $_POST['fileName'] ?? '';
        $dataType = $postParams['dataType'] ?? '';
        $endpoint = $postParams['endpoint'] ?? '';
        $requestBody = $postParams['requestBody'] ?? '{}';
        $isTemplateUrl = false;
        $idArray = [];
        $csvAttributes = [];
        $csvHeaders = [];

        if($dataType == 'germplasm') {
            $requestBody = json_encode(Yii::$app->session['germplasm-api_filter']);

            $columns = [
                'designation' => 'Germplasm Name',
                'otherNames' => 'Other Names',
                'parentage' => 'Parentage',
                'generation' => 'Generation',
                'germplasmNameType' => 'Germplasm Name Type',
                'germplasmType' => 'Germplasm Type',
                'germplasmState' => 'Germplasm State',
                'germplasmNormalizedName' => 'Germplasm Normalized Name',
                'germplasmCode' => 'Germplasm Code'
            ];

            $csvAttributes = array_keys($columns);
            $csvHeaders = array_values($columns);
        } else if($dataType == 'relations') {
            $isTemplateUrl = true;

            $columns = [
                'individualGermplasmCode' => 'Individual',
                'femaleParentGermplasmCode' => 'Female',
                'maleParentGermplasmCode' => 'Male'
            ];

            $csvAttributes = array_keys($columns);
            $csvHeaders = array_values($columns);
        }

        $userDbId = $this->user->getUserId();

        $result = $this->worker->invoke(
            'BuildCsvData',
            $description,
            $entity,
            $entityDbId,
            $endpointEntity,
            $application,
            'POST',
            [
                'processName' => $processName,
                'description' => $description,
                'dataType' => $dataType,
                'userId' => $userDbId,
                'url' => $endpoint,
                'isTemplateUrl' => $isTemplateUrl,
                'idArray' => $idArray,
                'requestBody' => $requestBody,
                'fileName' => $fileName.'_'.date('Ymd_His',time()),
                'csvAttributes' => $csvAttributes,
                'csvHeaders' => $csvHeaders
            ],
            [
                'remarks' => $fileName.'_'.date('Ymd_His',time()).'.csv'
            ]
        );

        return json_encode($result);
    }

    /**
     * Retrieve germplasm record given germplasm IDs
     *
     * @return mixed
     */
    public function actionBuildGermplasmDataForExport() {
        $postParams = $_POST;

        $selectedFromSession = $postParams['selectedFromSession'] ?? [];
        $exportFormat = $postParams['exportFormat'] ?? 'helium';

        $selectMode = $selectedFromSession['selectMode'];
        $currentSelectedIds = $selectedFromSession['currentSelectedIds'] ?? [];
        $filter = $selectedFromSession['filter'];
        $ids = null;

        if($selectMode == 'include mode') {
            if(!empty($currentSelectedIds)) $ids = 'equals '.implode(' | equals ', $currentSelectedIds);
        } else {
            if(!empty($currentSelectedIds)) $ids = 'not equals '.implode(' | not equals ', $currentSelectedIds);
        }
        if(!is_null($ids)) $filter['germplasmDbId'] = $ids;

        $records = $this->germplasmModel->searchAll($filter, 'limit=2&sort=germplasmDbId:ASC', false);

        $data = [];
        $success = false;
        $status = 'fail';

        if($records['status'] == 200){
            $data = $records['data'];

            $success = true;
            $status = 'success';
        }

        return json_encode(['success'=> $success, 'status' => $status, 'data' => $data, 'filter' => $filter]);
    }

    /**
     * Retrievescurrent total result count from session
     */
    public function actionGetTotalResultCount() {
        return Yii::$app->session->get('germplasm-total_results_count');
    }

    /**
     * Retrieves config values
     * 
     * @param String $program program code
     * @param String $globalConfigAbbrev global config abbrev
     * 
     * @return mixed
     */
    public function getConfigValues($program, $globalConfigAbbrev, $baseConfigAbbrevSuffix)
    {
        $cropConfig = [];
        $programConfig = [];
        $roleConfig = [];
        $config = [];

        $baseConfigAbbrevPrefix = 'GM_';

        // if program is provided
        if (isset($program)) {
            // get crop code
            $cropCode = $this->programModel->getCropCode($program);

            // get crop level config
            if (!empty($cropCode)) {
                $abbrev = $baseConfigAbbrevPrefix . 'CROP_' . (strtoupper($cropCode)) . $baseConfigAbbrevSuffix;
                $cropConfig = $this->configuration->getConfigByAbbrev($abbrev);

                if (!empty($cropConfig)) {
                    $config = $cropConfig;
                }
            }

            if (empty($cropConfig)) {
                // get program level config
                $abbrev = $baseConfigAbbrevPrefix . 'PROGRAM_' . (strtoupper($program)) . $baseConfigAbbrevSuffix;
                $programConfig = $this->configuration->getConfigByAbbrev($abbrev);

                if (!empty($programConfig)) {
                    $config = $programConfig;
                }
            }
        }

        // get role level config
        if (empty($cropConfig) && empty($programConfig)) {
            // get user role
            [$role, $isAdmin, $userId] = $this->personModel->getPersonRoleAndType($program);

            // get role config
            $abbrev = $baseConfigAbbrevPrefix . 'ROLE_' . (strtoupper($role)) . $baseConfigAbbrevSuffix;
            $roleConfig = $this->configuration->getConfigByAbbrev($abbrev);

            if (!empty($roleConfig)) {
                $config = $roleConfig;
            }
        }

        if (empty($cropConfig) && empty($programConfig) && empty($roleConfig)) {
            // get global config
            $globalConfig = $this->configuration->getConfigByAbbrev($globalConfigAbbrev);

            if (!empty($globalConfig)) {
                $config = $globalConfig;
            }
        }

        return $config;
    }
}
