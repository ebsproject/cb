<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use app\dataproviders\ArrayDataProvider;

use app\components\B4RController;

use app\models\Germplasm;
use app\modules\search\models\SearchGermplasm;

/**
 * Controller for the `product` module
 * Contains methods for managing products
 */
class ViewController extends B4RController {

    public function __construct(
        $id,
        $module,
        public Germplasm $germplasm,
        $config = []
        )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the germplasm basic information page
     * @param integer $id germplasm identifier
     */
    public function actionIndex($id, $program, $returnUrl = null) {
        $data = SearchGermplasm::getGermplasmBasicInfo($id);

        return $this->render('basic',[
            'data' => $data,
            'entityId' => $id,
            'entityLabel' => $data['basic']['Germplasm'] ?? null,
            'program' => $program,
            'returnUrl' => $returnUrl
        ]);
    }

    /**
     * Renders the germplasm names page
     * @param integer $id germplasm identifier
     */
    public function actionNames($id, $program, $returnUrl = null) {
        $germplasm = $this->germplasm->getOne($id);
        $designation = $germplasm['data']['designation'];

        $paramLimit = 'limit=' . getenv('CB_DATA_BROWSER_DEFAULT_PAGE_SIZE');
        $paramPage = '&page=1';
        if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        }

        $params = [
            'fields' => 'germplasm_name.germplasm_id AS germplasmDbId|germplasm_name.name_value AS nameValue|
                        germplasm_name.germplasm_name_type AS germplasmNameType|germplasm_name.germplasm_name_status AS germplasmNameStatus|
                        germplasm_name.germplasm_normalized_name AS germplasmNormalizedName',
            'germplasmDbId' => "equals $id"
        ];
        $response = \Yii::$app->api->getResponse('POST', 'germplasm-names-search?'.$paramLimit.$paramPage, json_encode($params));
        $data = $response['body']['result']['data'];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'restified' => true,
            'totalCount' => $response['body']['metadata']['pagination']['totalCount']
        ]);

        return $this->render('names',[
            'data' => $dataProvider,
            'entityId' => $id,
            'entityLabel' => $designation,
            'program' => $program,
            'returnUrl' => $returnUrl
        ]);
    }

    /**
     * Renders the germplasm seeds page
     * @param integer $id germplasm identifier
     */
    public function actionSeeds($id, $program, $returnUrl = null) {
        $germplasm = $this->germplasm->getOne($id);
        $designation = $germplasm['data']['designation'];

        $paramLimit = 'limit=' . getenv('CB_DATA_BROWSER_DEFAULT_PAGE_SIZE');
        $paramPage = '&page=1';
        if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        }

        $params = [
            'fields' => 'seed.germplasm_id AS germplasmDbId|seed.seed_code AS seedCode|seed.seed_name AS seedName|
                        seed.harvest_date AS harvestDate|seed.harvest_method AS harvestMethod|program.program_code AS programCode|
                        program.program_name AS programName|experiment.experiment_name AS experimentName|
                        experiment.experiment_year AS experimentYear|experiment.experiment_type AS experimentType',
            'germplasmDbId' => "equals $id"
        ];
        $response = \Yii::$app->api->getResponse('POST', 'seeds-search?'.$paramLimit.$paramPage, json_encode($params));
        $data = $response['body']['result']['data'];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'restified' => true,
            'totalCount' => $response['body']['metadata']['pagination']['totalCount']
        ]);

        return $this->render('seeds',[
            'data' => $dataProvider,
            'entityId' => $id,
            'entityLabel' => $designation,
            'program' => $program,
            'returnUrl' => $returnUrl
        ]);
    }

    /**
     * Renders the germplasm pedigree page
     * @param integer $id germplasm identifier
     */
    public function actionPedigree($id, $program, $returnUrl = null) {
        $germplasm = $this->germplasm->getOne($id);
        $designation = $germplasm['data']['designation'];
        $pedigreeDepth = 6;

        return $this->render('pedigree',[
            'entityId' => $id,
            'entityLabel' => $designation,
            'program' => $program,
            'returnUrl' => $returnUrl,
            'pedigreeDepth' => $pedigreeDepth
        ]);
    }

    /**
     * Renders the germplasm attributes page
     * @param integer $id germplasm identifier
     */
    public function actionAttributes($id, $program, $returnUrl = null) {
        $germplasm = $this->germplasm->getOne($id);
        $designation = $germplasm['data']['designation'];
        $data = SearchGermplasm::getGermplasmAttributes($id);

        return $this->render('attributes',[
            'data' => $data,
            'entityId' => $id,
            'entityLabel' => $designation,
            'program' => $program,
            'returnUrl' => $returnUrl
        ]);
    }

    /**
     * Renders the germplasm family page
     * @param integer $id germplasm identifier
     */
    public function actionFamily($id, $program, $returnUrl = null) {
        $germplasm = $this->germplasm->getOne($id);
        $designation = $germplasm['data']['designation'];
        $data = SearchGermplasm::getGermplasmBasicInfo($id);
        $data = SearchGermplasm::getFamily($data['familyDbId']);

        return $this->render('family',[
            'data' => $data,
            'entityId' => $id,
            'entityLabel' => $designation,
            'program' => $program,
            'returnUrl' => $returnUrl,
            'currentGermplasm' => $designation
        ]);
    }

    /**
     * Renders the germplasm event logs page
     * @param integer $id germplasm identifier
     */
    public function actionEventLogs($id, $program, $returnUrl = null) {
        $germplasm = $this->germplasm->getOne($id);
        $designation = $germplasm['data']['designation'];
        $data = SearchGermplasm::getGermplasmEventLogs($id);

        return $this->render('event-logs',[
            'data' => $data,
            'entityId' => $id,
            'entityLabel' => $designation,
            'program' => $program,
            'returnUrl' => $returnUrl
        ]);
    }
}