<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use Yii;
use app\components\B4RController;
use app\controllers\Dashboard;
use app\models\GermplasmFileUpload;
use app\models\BackgroundJob;
use app\models\User;
use app\models\Worker;
use app\models\Program;
use app\models\Crop;
use app\models\CropProgram;

use app\modules\germplasm\models\CreateModel;
use app\modules\germplasm\models\ViewBrowserModel;
use app\modules\germplasm\models\ErrorLogModel;
use app\modules\germplasm\models\ViewFileUploadModel;
// Import data providers
use app\dataproviders\ArrayDataProvider;

/**
 * Controller for the `germplasm` module
 * Contains methods for managing the viewing of created germplasm, seed, and package records
 */
class ViewFileUploadController extends B4RController
{

    public $dashboardModel;
    public $germplasmFileUploadModel;
    public $createModel;
    public $viewBrowserModel;
    public $errorLogModel;
    public $viewFileUploadModel;
    public $programModel;
    public $cropModel;
    public $cropProgramModel;
    public $userModel;

    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        Dashboard $dashboardModel,
        GermplasmFileUpload $germplasmFileUploadModel,
        CreateModel $createModel,
        ViewBrowserModel $viewBrowserModel,
        ErrorLogModel $errorLogModel,
        ViewFileUploadModel $viewFileUploadModel,
        Program $programModel,
        User $userModel,
        Crop $cropModel,
        CropProgram $cropProgramModel,
        $config = []
        )
    {
        $this->dashboardModel = $dashboardModel;
        $this->germplasmFileUploadModel = $germplasmFileUploadModel;
        $this->createModel = $createModel;
        $this->cropModel = $cropModel;
        $this->cropProgramModel = $cropProgramModel;
        $this->viewBrowserModel = $viewBrowserModel;
        $this->errorLogModel = $errorLogModel;
        $this->viewFileUploadModel = $viewFileUploadModel;
        $this->programModel = $programModel;
        $this->userModel = $userModel;

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the germplasm browser for viewing
     * 
     * @param Integer $germplasmFileUploadDbId germplasm file upload record identifier
     * @param String $entity entity being viewed - germplasm, seed, or package
     */
    public function actionViewRecords($germplasmFileUploadDbId,$entity){
        // Get program
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programDbId = $dashboardFilters['program_id'];

        // Get germplasm file upload transaction information
        $fileUploadInfoSearch = $this->createModel->getFileUploadRecord($germplasmFileUploadDbId);
        $fileUploadInfo = [];

        if(isset($fileUploadInfoSearch) && !empty($fileUploadInfoSearch['body']['result']['data'])){
            $fileUploadInfo = $fileUploadInfoSearch['body']['result']['data'][0];
        }

        // retrieve columns for browser
        $variableConfig = Yii::$app->session->get($this->createModel->getKeyVariables());
        $browserVariables = [];
        $columns = [];

        if($entity != 'germplasm'){
            array_push($browserVariables, [
                'abbrev' => 'DESIGNATION',
                'name' => 'Germplasm',
                'label' => 'Germplasm'
            ]);
        }

        foreach($variableConfig AS $variable){
            if( $variable['entity'] == $entity && $variable['type'] != 'attribute' && (
                $variable['entity'] != 'germplasm' || ($variable['entity'] == 'germplasm' && $variable['type'] != 'attribute'))){
                array_push($browserVariables, [
                    'abbrev' => $variable['abbrev'],
                    'name' => $variable['variable_info']['name'],
                    'label' => $variable['variable_info']['label']
                ]);
            }
        }

        // retrieve data
        $data = $this->viewFileUploadModel->getDataProvider($browserVariables,$entity,$germplasmFileUploadDbId);
        $totalCount = $data['totalCount'];

        $columns = $data['columns'];
        $dataProvider = new ArrayDataProvider([
            'id' => 'gm-file-upload-'.$entity.'-grid',
            'allModels' => $data['data'],
            'restified' => true,
            'totalCount' => $totalCount
        ]);

        return $this->render($entity.'_browser',[
            'germplasmFileUploadDbId' => $germplasmFileUploadDbId,
            'fileUploadInfo' => $fileUploadInfo,
            'program' => $programDbId,
            'entity' => $entity,
            'searchModel' => $this->viewFileUploadModel,
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'browserId' => 'gm-file-upload-'.$entity.'-grid'
        ]);
    }
}