<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

// Import Yii
use Yii;
use yii\helpers\Url;

// Import browser helpers
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

use app\controllers\Dashboard;

use app\models\GermplasmFileUpload;
use app\models\GermplasmSearch;
use app\models\Program;
use app\models\Person;
use app\models\Crop;
use app\models\Config;
use app\models\Variable;

use app\dataproviders\ArrayDataProvider;
use app\components\B4RController;
use app\interfaces\models\IProgram;
use app\interfaces\models\IUser;
use app\interfaces\models\IWorker;
use app\modules\germplasm\models\CodingModel;
use app\modules\germplasm\models\ViewBrowserModel;
use app\modules\germplasm\models\ViewFileUploadModel;

class CodingController extends B4RController
{

    public function __construct(
        $id,
        $module,
        public IProgram $programModel,
        public IUser $userModel,
        public IWorker $workerModel,
        public GermplasmFileUpload $germplasmFileUploadModel,
        public CodingModel $codingModel,
        public Dashboard $dashboardModel,
        public ViewFileUploadModel $viewFileUploadModel,
        public GermplasmSearch $germplasmSearchModel,
        public ViewBrowserModel $viewBrowserModel,
        public Person $person,
        public Crop $crop,
        public Config $configuration,
        public Variable $variable,
        $config = []
    ) {
        $this->germplasmFileUploadModel = $germplasmFileUploadModel;
        $this->viewBrowserModel = $viewBrowserModel;
        $this->viewFileUploadModel = $viewFileUploadModel;

        $this->dashboardModel = $dashboardModel;
        $this->programModel = $programModel;
        $this->person = $person;
        $this->configuration = $configuration;
        $this->variable = $variable;

        $this->germplasmSearchModel = $germplasmSearchModel;

        parent::__construct($id, $module, $config);
    }

    /**
     * Load index page
     */
    public function actionIndex($program = null)
    {
        // Determine if user is ADMIN
        $isAdmin = Yii::$app->session->get('isAdmin');

        // check if user has access to Coding tab
        $this->checkAccess($program, 'GERMPLASM_CODING');

        // Search parameters
        $params = [
            'entity' => 'GERMPLASM',
            'fileUploadAction' => 'coding'
        ];

        // Search model and data provider
        $searchModel = $model = $this->germplasmFileUploadModel;
        $dataProvider = $searchModel->search(null, $params);

        // retrieve germplasm file upload status
        $statusValuesArr = $this->germplasmFileUploadModel->getFileUploadStatusValues();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'searchModel' => $searchModel,
            'module' => 'coding',
            'program' => Yii::$app->userprogram->get('abbrev'),
            'fileUploadStatusArr' => $statusValuesArr,
            'statusValuesArr' => $statusValuesArr,
            'selectAllMode' => 'include mode',
            'program' => $program
        ]);
    }

    /**
     * Render page for creating germplasm coding transactions
     * @param program program code value
     * @param sourceToolAbbrev tool abbrev where the request originates from. Default = GM
     */
    public function actionCreateTransaction($program = null, $sourceToolAbbrev = 'GM', $mode, $selectedIdsString = '', $returnUrl)
    {
        // Get germplasm records
        $germplasmRecords = $this->codingModel->getGermplasmRecords($sourceToolAbbrev, $mode, $selectedIdsString);
        if(!empty($germplasmRecords['germplasmDbIds'])) {
            $ids = $germplasmRecords['germplasmDbIds'];
            $idsArray = (array) $ids;
            $germplasmRecords['germplasmDbIds'] = array_values($idsArray);
        }

        $germplasmNameType = $this->getGermplasmNameTypes();

        // Build select2 elements
        $inputFields = $this->codingModel->buildDefaultInputs($germplasmNameType);

        // Render page
        return $this->render('create_transaction', [
            "program" => $program,
            "germplasmRecords" => $germplasmRecords,
            "inputFields" => $inputFields,
            "returnUrl" => $returnUrl
        ]);
    }

    /**
     * Render summary of coding transaction
     */
    public function actionViewSummary($program = null, $germplasmFileUploadDbId = 0, $systemDef = true, $action = 'complete')
    {
        // retrieve dashboard program
        $programId = $this->getDashboardProgramId();

        // retrieve transaction record
        $requestBody = [
            "germplasmFileUploadDbId" => "equals " . $germplasmFileUploadDbId
        ];
        $codingTrasactionRecord = $this->germplasmFileUploadModel->searchAll($requestBody);

        if (
            $codingTrasactionRecord['status'] != 200 ||
            !isset($codingTrasactionRecord['data']) ||
            empty($codingTrasactionRecord['data'])
        ) {
            // Redirect to the main update page
            return $this->redirect(['update/index', 'program' => $program]);
        }

        $fileName = $codingTrasactionRecord['data'][0]['fileName'] ?? "";
        $fileStatus = $codingTrasactionRecord['data'][0]['fileStatus'] ?? "";
        $creator = $codingTrasactionRecord['data'][0]['uploader'] ?? "";
        $creationTimestamp = $codingTrasactionRecord['data'][0]['uploadTimestamp'] ?? "";
        $remarks = $codingTrasactionRecord['data'][0]['remarks'] ?? "";

        $transactionData = $this->viewBrowserModel->search($germplasmFileUploadDbId, $programId, 'coding');

        // Define browser information
        $browserId = 'dynagrid-germplasm-coding-transaction-summary-grid';
        $columns = $this->buildColumns();

        $dataProvider = new ArrayDataProvider([
            'id' => $browserId . '-grid',
            'allModels' => [],
            'restified' => true,
            'totalCount' => 0
        ]);

        if (!empty($transactionData['dataProvider'])) {
            $dataProvider = $transactionData['dataProvider'];
        }

        // render view update transaction page
        return $this->render('_coding_summary.php', [
            'browserId' => $browserId,
            'dataProvider' => $dataProvider,
            'fileDbId' => $germplasmFileUploadDbId,
            'fileUploadInfo' => [
                'fileName' => $fileName ?? '',
                'fileStatus' => $fileStatus ?? '',
                'uploader' => $creator,
                'uploadTimestamp' => $creationTimestamp,
                'remarks' => $remarks,
            ],
            'columns' => $columns,
            'searchModel' => $this->viewFileUploadModel,
            'program' => $program,
            'configAbbrevPrefix' => '',
            'action' => $action
        ]);
    }

    /**
     * Renders free text coding UI
     */
    public function actionRenderPatternSection()
    {
        $codingInfo = isset($_POST['codingInfo']) ? (array) json_decode($_POST['codingInfo']) : [];

        if ($codingInfo['codingOption'] == 'free-text') {
            return $this->renderPartial('_free_text', []);
        } else if ($codingInfo['codingOption'] == 'configured-code') {

            // get germplasm name type pattern
            $germplasmNameType = $codingInfo['germplasmNameType'] ?? '';
            $germplasmNameTypeArr = $this->getGermplasmNameTypes();

            $sequenceData = null;
            foreach ($germplasmNameTypeArr as $record) {
                if ($record['value'] == $germplasmNameType) {
                    $sequenceData = $record;
                    break;
                }
            }

            $sequenceId = $sequenceData['sequenceId'] ?? null;
            $sequenceName = $sequenceData['sequenceName'] ?? '';
            $sequencePattern = $sequenceData['sequenceRules'] ?? [];

            // build HTML elements
            $patternFields = $this->buildPatternHTMLFields($sequencePattern, $sequenceId);

            return $this->renderAjax('_configured_code', [
                'germplasmNameType' => "",
                'htmlFields' => $patternFields
            ]);
        }
    }

    /**
     * Trigger background worker for germplasm coding validation
     */
    public function actionStartGermplasmCoding() {
        // Unpack data
        $codingData = isset($_POST['codingData']) ? (array) json_decode($_POST['codingData']) : [];

        // GET PROGRAM DB ID
        $result = $this->programModel->searchAll([
            "programCode" => "equals " . $codingData["program"]
        ]);
        $programInfo = $result['data'][0] ?? [];
        $programDbId = $programInfo['programDbId'] ?? 0;
        // GET USER ID
        $userId = $this->userModel->getUserId();

        // CREATE TRANSACTION
        $requestBody = [
            "records" => [
                [
                    "programDbId" => "$programDbId",
                    "fileName" => "GERMPLASM_CODING_$userId" . "_" . date("Y-m-d h-i-s-u e"),
                    "fileUploadOrigin" => "GERMPLASM_CATALOG",
                    "entity" => "germplasm",
                    "fileUploadAction" => "coding",
                    "fileStatus" => "in queue",
                    "fileData" => [
                        $codingData
                    ]
                ]
            ]
        ];
        // CREATE GERMPLASM FILE UPLOAD RECORD
        $result = $this->germplasmFileUploadModel->create(
            $requestBody
        );
        $transactionDbId = $result['data'][0]['germplasmFileUploadDbId'] ?? 0;

        // TRIGGER BG WORKER
        $result = $this->workerModel->invoke(
            "GenerateGermplasmNames",
            "[IN QUEUE] Code Germplasm Records",
            "GERMPLASM",
            null,
            "GERMPLASM",
            "GERMPLASM_CATALOG",
            "POST",
            [
                "transactionDbId" => "$transactionDbId",
                "codingData" => $codingData
            ]
        );

        Yii::$app->session->setFlash('info', \Yii::t('app', 'Validation of germplasm coding transaction is ongoing.'));
        return $this->redirect(['coding/index', 'program' => $codingData["program"]]);
    }

    /**
     * Retrieval of dashboard program id
     */
    function getDashboardProgramId()
    {
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programId = $dashboardFilters['program_id'] ?? '';

        return $programId;
    }

    /**
     * Build columns for coding transaction preview page
     */
    function buildColumns()
    {
        $columns = [
            [
                'attribute' => 'germplasmNameType',
                'label' => 'Germplasm Name Type',
                'contentOptions' => [
                    'class' => ''
                ],
                'noWrap' => true,
                'width' => '300px',
                'format' => 'html',
                'visible' => false,
                'value' => function ($model) {
                    return $model['germplasmNameType'] ?? "";
                }
            ],
            [
                'attribute' => 'germplasmCode',
                'label' => 'Germplasm Code',
                'contentOptions' => [
                    'class' => ''
                ],
                'noWrap' => true,
                'width' => '300px',
                'format' => 'html',
                'visible' => false,
                'value' => function ($model) {
                    return $model['germplasmCode'] ?? "";
                }
            ],
            [
                'attribute' => 'designation',
                'label' => 'Germplasm Name',
                'contentOptions' => [
                    'class' => ''
                ],
                'noWrap' => true,
                'width' => '300px',
                'format' => 'html',
                'visible' => false,
                'value' => function ($model) {
                    return $model['designation'] ?? "";
                }
            ],
            [
                'attribute' => 'nameValue',
                'label' => 'Proposed Germplasm Name',
                'contentOptions' => [
                    'class' => ''
                ],
                'noWrap' => true,
                'width' => '300px',
                'format' => 'html',
                'visible' => false,
                'value' => function ($model) {
                    return $model['nameValue'];
                }
            ]
        ];

        return $columns;
    }

    /**
     * Retrieval of crop and program code based on program id
     * 
     * @param programId dashboard program id 
     */
    function getCropAndProgramCode($programId)
    {
        $cropCode = $this->crop->searchAll(['programDbId' => "equals $programId",
            'fields' => 'crop.id AS cropDbId|program.id AS programDbId|crop.crop_code AS cropCode|program.program_code AS programCode',
        ])['data'][0];

        return $cropCode;
    }

    /**
     * Retrieval of germplasm name types and sequence rules based on germplasm coding config and cs sequence rules
     */
    function getGermplasmNameTypes(){
        // Retrieve necessary variables
        $programId = $this -> getDashboardProgramId();
        $code = $this->getCropAndProgramCode($programId);
        $cropCode = $code['cropCode'] ?? '';
        $programCode = $code['programCode'] ?? '';

        $arrGermplasmNameTypeConfig = [];
        $arrGermplasmNameTypeScaleValue = [];

        // Set input list value based on the selected coding option
        // Build abbrev for config based on program
        $abbrev = $cropCode ? "GM_CROP_" . $cropCode . "_GERMPLASM_CODING_NAME_TYPE_CONFIG" : "GM_GLOBAL_GERMPLASM_CODING_NAME_TYPE_CONFIG";
        // Retrieve CB germplasm name type config 
        $germplasmNameTypeConfig = $this->configuration->getConfigByAbbrev($abbrev);
            
        // Extract germplasm name type values
        foreach ($germplasmNameTypeConfig as $temp) {
            foreach ($temp as $germplasmNameTypeVal) {
                // Store germplasm name type value
                array_push($arrGermplasmNameTypeConfig, $germplasmNameTypeVal);
            }
        }

        // Get abbrev for variable to retrieve scale values
        $variableAbbrev = strtoupper(key($germplasmNameTypeConfig));

        // Retrieve variable
        $result = $this->variable->getVariableByAbbrev($variableAbbrev);

        // Get variable id for scale values
        $variableId = $result['variableDbId'] ?? 0;

        // Retrieve scale values based on variable id
        $result = $this->variable->getVariableScales($variableId);
        $scaleValues = $result['scaleValues'] ?? [];

        // Extract the needed scale value
        foreach ($scaleValues as $temp => $scaleValue) {
            foreach ($arrGermplasmNameTypeConfig as $arr) {
                // Only get scale value based on the germplasm name type retrieved from config
                if (str_contains(strtolower($arr), $scaleValue['displayName'])) {
                    // Store scale value
                    array_push($arrGermplasmNameTypeScaleValue, $scaleValue);
                }
            }
        }

        // Get sequence rules from Core System
        $sequenceRules = Yii::$app->api->getResponse('GET', 'sequence', '', null, false, 'cs')['body']['content'] ?? [];

        // Extract sequence rules based on config abbrev
        foreach ($arrGermplasmNameTypeScaleValue as $key => $gmNameType) {
            foreach ($sequenceRules as $temp) {
                $prefix = "gmcoding-crop_" . strtolower($cropCode);
                $suffix = strtolower($gmNameType['displayName']);

                $sequenceNameProgram = $prefix . "-program_" . strtolower($programCode) . "-" . $suffix;
                $sequenceNameDefault = $prefix . "-program_all-" . $suffix;

                if (str_contains($temp['name'], $sequenceNameProgram) || str_contains($temp['name'], $sequenceNameDefault)) {
                    // Store sequence rules specifically on the germplasm name type under sequenceRules
                    $arrGermplasmNameTypeScaleValue[$key] += [
                        'sequenceRules' => $temp['segments'],
                        'sequenceId' => $temp['id'],
                        'sequenceName' => $temp['name']
                    ];
                }
            }
        }

        return $arrGermplasmNameTypeScaleValue;
    }

    /**
     * Process of germplasm name types based on coding option
     */
    function actionGetConfiguredCodeNameType()
    {
        $codingOption = isset($_POST['codingOption']) ? $_POST['codingOption'] : "free-text";
        $newGermplasmNameType = [];

        // Retrieve germplasm name type and sequence rules
        $germplasmNameType = $this->getGermplasmNameTypes();

        foreach ($germplasmNameType as $key) {
            $con = $key['sequenceRules'] ?? '';
            // If coding option is in free text, add all values; if not, add only those who have sequence rules
            if ($codingOption == 'free-text') {
                array_push($newGermplasmNameType, ["id" => $key['value'], "text" => $key['value']]);
            } else if ($con) {
                array_push($newGermplasmNameType, ["id" => $key['value'], "text" => $key['value']]);
            }
        }

        // Return value for append select2 input list
        return json_encode($newGermplasmNameType);
    }

    /**
     * Generate HTML fields for configured code pattern
     * 
     * @param Object $pattern sequence pattern
     * @param Integer $sequenceId sequence identifier
     * 
     * @return Array HTML fields
     */
    function buildPatternHTMLFields($pattern, $sequenceId)
    {
        $htmlFields = [];
        $additionalParams = [];
        $fieldHasScaleValues = false;

        // for each pattern segment
        foreach ($pattern as $segment) {
            $segmentType = 'input'; // default

            $sequenceDbId = $sequenceId ?? null;
            $fieldDataId = $segment['id'];

            $inputParams = "";
            $fieldRequiresInput = false;
            if ($segment['requiresInput'] || $segment['requiresInput'] == 'true') {
                $fieldRequiresInput = true;

                // TEMPORARY! inputParams will be updated;
                // to be derived from rule segment information
                $inputParams = json_encode([
                    "entity" => "germplasm",
                    "field" => "germplasmDbId"
                ]);
            }


            // check if field has scale values
            if (isset($segment['scale']) && !empty($segment['scale']) && $segment['scale'] !== null) {
                $segmentType = 'selection';
                $fieldHasScaleValues = true;
            }

            $additionalParams = [
                'data-field_id' => $fieldDataId,
                'data-sequence_id' => $sequenceDbId,
                'data-input_params' => strval($inputParams),
                'data-seqment_type' => $segmentType,
                'data-segment_data_type' => $segment['dataType'],
                'data-requires_input' => strval($fieldRequiresInput)
            ];

            // build html fields
            $htmlField = $this->codingModel->buildHTMLField($segmentType, $segment, $sequenceId, $additionalParams, $fieldHasScaleValues);

            array_push($htmlFields, $htmlField);
        }

        return $htmlFields;
    }

    /**
     * Check if user has access to Coding tab, else redirect to base page
     */
    function checkAccess($program, $action)
    {
        // Determine if user is ADMIN
        $isAdmin = Yii::$app->session->get('isAdmin');

        // Check RBAC if user has access to Coding
        $hasAccess = Yii::$app->access->renderAccess($action, 'GERMPLASM_CATALOG');

        // Check RBAC if user has access to Coding
        // Check allowed actions to determine tabs to display
        $gmPermissions = $this->germplasmSearchModel->getGermplasmManagerPermissions();
        $defPermissions = Yii::$app->session->get('currentPermissions')['permission'] ?? ['GERMPLASM_SEARCH'];
        $permissions = $isAdmin ? $gmPermissions : $defPermissions;

        // Redirect to proper page depending on access permissions
        if (!$isAdmin) {
            if (!$hasAccess) {
                if (empty($permissions)) {
                    Yii::$app->session->setFlash(
                        'error',
                        "You have no permission to the page you are trying to access."
                        . "Please contact your data administrator."
                    );

                    Yii::$app->response->redirect(Url::base() . '/index.php');
                    return;
                } else {
                    return $this->redirect([
                        strtolower(substr($permissions[0], 10)) . '/index',
                        'program' => $program
                    ]);
                }
            }
        }
    }
}
