<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use app\components\B4RController;
use app\models\Germplasm;
use app\models\GermplasmSearch;

use app\modules\search\models\SearchGermplasm;
use yii\helpers\Url;
use Yii;

/**
 * Controller for the `germplasm` module
 * Contains methods for managing germplasm
 */
class EditController extends B4RController
{
    public $germplasmSearchModel;

    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        GermplasmSearch $germplasmSearchModel,
        $config = []
        )
    {
        $this->germplasmSearchModel = $germplasmSearchModel;

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the specific germplasm information
     */
    public function actionIndex($id) {
        $searchModel = $this->germplasmSearchModel;
        $eventLogModel = new SearchGermplasm();
        $data = $searchModel->getGermplasmById($id);
        $eventLogDataProvider = $eventLogModel->getGermplasmEventLogs($id);
        $isAdmin = Yii::$app->session->get('isAdmin');

        if(!$isAdmin) {
            Yii::$app->session->setFlash('error', \Yii::t('app', 'Sorry, you are not allowed to access this page.'));
            return $this->redirect(Url::toRoute('/germplasm/default'));
        }

        if(empty($data)) {
            return $this->redirect(Url::toRoute('/germplasm/default'));
        }

        return $this->render('index',array(
            'entityId' => $id,
            'data' => $data,
            'eventLogDataProvider' => $eventLogDataProvider
        ));
    }

    /**
     * Updates germplasm on database.
     *
     * @return string $result Indicates if sql command executed successfully
     */
    public function actionUpdate() {
        $id = $_POST['id'];
        $data = $_POST['data'];
        $isAdmin = Yii::$app->session->get('isAdmin');
        $searchModel = new Germplasm();

        if(!$isAdmin) {
            Yii::$app->session->setFlash('error', \Yii::t('app', 'Sorry, you are not allowed to access this page.'));
            return $this->redirect(Url::toRoute('/germplasm/default'));
        }

        $result = $searchModel->updateGermplasm($id, $data);

        return $result;
    }
}