<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use Yii;
use app\components\B4RController;
use app\controllers\Dashboard;
use app\modules\search\models\SearchEntity;
use app\modules\search\models\SearchGermplasm;

// import models
use app\models\User;
use app\models\BackgroundJob;
use app\models\GermplasmFileUpload;
use app\models\Person;
use app\models\Program;
use app\models\Worker;

use app\modules\germplasm\models\UpdateModel;
use app\modules\germplasm\models\CreateModel;


/**
 * Controller for the `germplasm` module
 * Contains methods for managing germplasms
 */
class DefaultController extends B4RController
{
    const WORKER_NAME_REFERENCE = [
        'search' => 'equals BuildHeliumData|equals BuildCsvData',
        'create' => 'equals ValidateGermplasmFileUpload|equals CreateGermplasmRecords',
        'merge' => 'equals MergeGermplasm',
        'update' => 'equals ValidateGermplasmFileUpload|equals CreateGermplasmRecords',
        'coding' => 'equals CodeGermplasmRecords|equals GenerateGermplasmNames'
    ];

    public $dashboard;
    public $searchEntity;
    public $backgroundJob;
    public $userModel;
    public $updateModel;
    public $person;
    public $programModel;

    public $germplasmFileUploadModel;
    public $createModel;
    public $workerModel;

    /**
     * Controller constructor
     */
    public function __construct(
        $id,
        $module,
        Dashboard $dashboard,
        SearchEntity $searchEntity,
        User $userModel,
        BackgroundJob $backgroundJob,
        GermplasmFileUpload $germplasmFileUploadModel,
        UpdateModel $updateModel,
        Person $person,
        Program $programModel,
        CreateModel $createModel,
        Worker $workerModel,
        $config = []
    ) {
        $this->dashboard = $dashboard;
        $this->searchEntity = $searchEntity;
        $this->userModel = $userModel;
        $this->backgroundJob = $backgroundJob;
        $this->updateModel = $updateModel;
        $this->person = $person;
        $this->programModel = $programModel;
        $this->workerModel = $workerModel;

        $this->germplasmFileUploadModel = $germplasmFileUploadModel;
        $this->createModel = $createModel;

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the data browser for germplasm catalog
     * 
     * @param String $program Current dashboard program
     * 
     * @return mixed
     */
    public function actionIndex($program = null)
    {
        $dashboardFilters = (array) $this->dashboard->getFilters();
        $programDbId = $dashboardFilters['program_id'];

        return $this->redirect([
            'search/index',
            'program' => $program
        ]);
    }

    /**
     * Renders view more information of a germplasm in widget
     * @return mixed germplasm information
     */
    public function actionViewInfo()
    {
        $entityId = isset($_POST['entity_id']) ? $_POST['entity_id'] : null;
        $entityType = isset($_POST['entity_type']) ? $_POST['entity_type'] : null;
        $entityLabel = isset($_POST['entity_label']) ? $_POST['entity_label'] : null;
        $program = isset($_POST['program']) ? $_POST['program'] : Yii::$app->userprogram->get('abbrev'); //current program

        if (empty($program)) {
            $defaultFilters = $this->dashboard->getFilters();
            $dashProgram = $defaultFilters->program_id;
        }

        $getDepthThresh = Yii::$app->config->getAppThreshold('GERMPLASM_MANAGER', 'ancestryExportDepth');
        $depth = isset($getDepthThresh) && !empty($getDepthThresh) ? $getDepthThresh : 6;

        $data = $this->searchEntity->getEntityData($entityId, $entityType);
        return Yii::$app->controller->renderAjax('@app/widgets/views/germplasm/tabs.php', [
            'data' => $data,
            'entityLabel' => $entityLabel,
            'entityId' => $entityId,
            'program' => $program,
            'pedigreeDepth' => $depth
        ]);
    }

    /**
     * Renders names tab in germplasm widget
     * @param integer $id germplasm identifier
     *
     * @return mixed germplasm names information
     */
    public function actionViewNamesInfo($id)
    {
        $data = SearchGermplasm::getGermplasmNames($id);

        return Yii::$app->controller->renderAjax(
            '@app/widgets/views/germplasm/names.php',
            [
                'data' => $data
            ]
        );
    }

    /**
     * Retrieves push notifications for identified module
     * 
     * @param String $module name of source germplasm module where the notifications will be rendered
     */
    public function actionPushNotifications($module)
    {
        $userId = $this->userModel->getUserId();
        $workerName = self::WORKER_NAME_REFERENCE[$module] ?? '';

        // retrieve new and unseen notifications
        $params = [
            "creatorDbId" => "equals $userId",
            "application" => "GERMPLASM_CATALOG",
            "workerName" => $workerName,
            "isSeen" => "equals false"
        ];
        $result = $this->backgroundJob->searchAll($params, "sort=modificationTimestamp:desc");

        $newNotifications = $result["data"];
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->backgroundJob->getNotifications($newNotifications);

        // retrieve already viewed notifications
        $params = [
            "creatorDbId" => "equals $userId",
            "application" => "GERMPLASM_CATALOG",
            "workerName" => $workerName,
            "isSeen" => "equals true"
        ];
        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);

        $notifications = $result["data"];
        $seenCount = count($notifications);
        $seenNotifications = $this->backgroundJob->getNotifications($notifications);

        // update unseen notifications to seen
        $params = [
            "jobIsSeen" => "true"
        ];
        $result = $this->backgroundJob->update($newNotifications, $params);

        return $this->renderAjax(
            '_notifications',
            [
                "unseenNotifications" => $unseenNotifications,
                "seenNotifications" => $seenNotifications,
                "unseenCount" => $unseenCount,
                "seenCount" => $seenCount,
            ]
        );
    }

    /**
     * Retrieve number of unread notifications
     * 
     * @param String $module name of source germplasm module where the notifications will be rendered
     * @return $totalCount
     */
    public function actionNewNotificationsCount($module)
    {
        $userId = $this->userModel->getUserId();
        $workerName = self::WORKER_NAME_REFERENCE[$module] ?? '';

        // retrieve background jobs
        $params = [
            "creatorDbId" => "equals $userId",
            "application" => "GERMPLASM_CATALOG",
            "workerName" => $workerName,
            "isSeen" => "equals false"
        ];
        $filter = 'sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter);

        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        }

        return json_encode($result["totalCount"]);
    }

    /**
     * Facilitate downloading of template for given action in Germplasm tool
     * 
     * @param String $program for current dashboard program
     * @param String $action germplasm tool action (CREATE/MERGE/UPDATE)
     */
    public function actionDownloadTemplate($program, $action)
    {
        // Variables for getConfigValues
        $cropCode = $this->programModel->getCropCode($program);
        [$userRole, $isAdmin, $userId] = $this->person->getPersonRoleAndType($program);
        $action = $this->updateModel->getModelAction();
        $headers = [];

        // Retrieve template variables
        $abbrev = $this->germplasmFileUploadModel->getConfigValues('germplasm', $action, $userRole, $program, $cropCode);

        // Catch if abbrev return empty or undefined
        if (empty($abbrev['configValues']) || $abbrev['configValues'] == null) {
            return false;
        }

        // Determine the germplasm tool action
        if ($action = "update") {
            // Build headers
            foreach ($abbrev['configValues']['values'] as $key => $config) {
                if (isset($config['in_export']) && !empty($config['in_export']) && $config['in_export'] == 'true') {
                    array_push($headers, $config['abbrev']);
                }
            }

            // Get file name
            $fileName = $this->updateModel->getTemplateFileName() ?? 'GM_UPDATE_GERMPLASM_ATTRIBUTE_TEMPLATE';

            if (empty($headers)) {
                return false;
            }

            $this->createModel->generateCSVTemplate($headers, '', '', $fileName);
        }
    }

    /**
     * Facilitate processing of uploaded file
     * 
     * @param String $action germplasm tool action (CREATE/MERGE/UPDATE)
     */
    public function actionFileUpload($action)
    {
        $success = true;
        $message = '';

        // Get program id
        $dashboardFilters = (array) $this->dashboard->getFilters();
        $programId = $dashboardFilters['program_id'];

        [$userRole, $isAdmin, $userId] = $this->person->getPersonRoleAndType($programId);
        $cropCode = $this->programModel->getCropCode($programId);

        try {
            // retrieve uploaded file
            $uploadedFileInfo = $_FILES['germplasm_file_upload'];

            if (isset($uploadedFileInfo) && !empty($uploadedFileInfo)) {
                // Retrieve configuration values                    
                $variableConfig = $this->germplasmFileUploadModel->getConfigValues('germplasm', $action, $userRole, $programId, $cropCode);

                // Retrieve uploaded file content
                $uploadedFile = $this->germplasmFileUploadModel->retrieveUploadedFile('germplasm_file_upload');

                if ($uploadedFile['success']) {

                    // For update tab in germplasm
                    if ($action == "update") {

                        // validate file contents
                        $result = $this->updateModel->validateUploadedFile($programId, $uploadedFile['headers'], $variableConfig['configValues']);

                        if ($result['success']) {
                            // Format file name
                            $fileName = trim(str_replace('.csv', '', $uploadedFile['fileName']));

                            // format each record
                            $fileData = [];

                            // Build file data
                            foreach ($uploadedFile['fileContent'] as $key => $record) {
                                foreach ($record as $column => $colVal) {
                                    $fileData[$key]['germplasm'][$column] = $colVal;
                                }
                            }

                            if (!isset($fileData)) {
                                $fileData = [];
                            }

                            // Build param body
                            $paramRequest = [
                                "fileName" => $fileName,
                                "fileData" => $fileData,
                                "programDbId" => "$programId",
                                "fileUploadOrigin" => "GERMPLASM_CATALOG",
                                "fileUploadAction" => $action,
                                "fileStatus" => "in queue",
                                "entity" => "germplasm_attribute",
                                "remarks" => "germplasm_attribute"
                            ];

                            // create new file upload record
                            $result = $this->germplasmFileUploadModel->saveUploadedFile($paramRequest);
                        }

                        return json_encode($result);
                    }
                } else {
                    $success = false;
                    $error = $uploadedFile['error'];
                }
            }
        } catch (\Exception $e) {
            $success = false;
            $error = 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.';
        }


        return json_encode([
            'success' => $success,
            'message' => $message,
            'error' => $error
        ]);
    }

    /**
     * Delete selected file upload transaction record
     * 
     * @return mixed
     */
    public function actionDeleteTransaction()
    {
        $postParams = $_POST;
        $fileUploadId = $postParams['fileUploadId'];

        if (!isset($fileUploadId) || empty($fileUploadId) || $fileUploadId == null) {
            return json_encode(["success" => false]);
        }

        $this->germplasmFileUploadModel->deleteOne($fileUploadId);

        return json_encode(["success" => true]);
    }

    /**
     * Update file upload status
     * 
     * @param Integer $fileId file identifier
     * 
     * @return mixed
     */
    public function actionUpdateStatus($fileId)
    {
        $fileStatus = $_POST['status'] ?? 'merge ready';

        // update file data
        $result = $this->germplasmFileUploadModel->updateOne(
            $fileId,
            ['fileStatus' => $fileStatus]
        );

        return json_encode(['success' => $result['success'], 'message' => $result['message']]);
    }

    /**
     * Controller action for invoking workers. The file upload ID,
     * worker name, and description must be specified.
     */
    public function actionInvokeBackgroundProcess()
    {
        $postParams = $_POST;

        $fileUploadDbId = $postParams['fileUploadDbId'] ?? 0;
        $workerName = $postParams['workerName'] ?? '';
        $description = $postParams['description'] ?? '';
        $action = $postParams['action'] ?? 'search';

        $additionWorkerData = $postParams['additionalWorkerData'] ?? '';
        if (!empty($additionWorkerData)) {
            $additionWorkerData = json_decode($additionWorkerData);
        }

        // Invoke worker
        $result = $this->workerModel->invoke(
            $workerName,
            $description,
            'GERMPLASM',
            $fileUploadDbId,
            'GERMPLASM',
            'GERMPLASM_CATALOG',
            'POST',
            ["fileUploadDbId" => $fileUploadDbId],
            null,
            $additionWorkerData
        );

        // Prepare flash message
        $type = 'info';
        $message = '';
        $iconClass = '';
        if ($result['success']) {
            $type = 'info';
            $iconClass = 'fa fa-info-circle';
            $message = 'The ' . ucfirst($action) . ' background process is ongoing in the background. You may proceed with other tasks. '
                . 'You will be notified in this page once done.';
        } else {
            $type = 'warning';
            $iconClass = 'fa-fw fa fa-warning';
            $message = 'A problem occurred while starting the background process. Please try again at a later time or '
                . '<a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';

            // If status is not 200, the problem occurred while creating the background job record (code: #001)
            if ($result['status'] != null && $result['status'] != 200) $message .= ' (#001)';

            // If status is 200, the problem occurred while connecting to the background worker (code: #002)
            else if ($result['status'] == null) $message += ' (#002)';
        }

        // set flash
        $message = '<i class="' . $iconClass . ' hm-bg-info-icon"></i>&nbsp;&nbsp;' . $message;
        \Yii::$app->session->setFlash($type, $message);

        // Return bool success
        return $result['success'];
    }

    /**
     * Facilitate retrieval of dashboard program value
     * 
     * @return Integer $programId
     */
    function getDashboardProgramId()
    {
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programId = $dashboardFilters['program_id'] ?? '';

        return $programId;
    }

    /**
     * Redirects to the main action page (index)
     */
    public function actionRedirectToMainActionPage($action)
    {
        $defaultFilters = (array) $this->dashboard->getFilters();
        $programId = $defaultFilters['program_id'];
        $result = $this->programModel->getOne($programId);

        $programInfo = $result['data'] ?? [];
        $program = $programInfo['programCode'] ?? '';

        // Redirect to the main create page
        return $this->redirect([$action . '/index', 'program' => $program]);
    }

    /**
     * Retrieve IDs of selected records
     * 
     * @param String $action germplasm manager action
     * 
     * @return mixed
     */
    public function actionRetrieveDbIds($action)
    {
        $retrievedDbIdsArr = [];
        $totalRetrieved = 0;
        $totalCount = 0;

        $sessProcessThresh = Yii::$app->session->get('germplasm-process-thresholds');
        $selectionThresh = 1000;
        if (
            isset($sessProcessThresh['GM_MAX_NO_OF_ROWS_FOR_PRINTOUT']) &&
            !empty($sessProcessThresh['GM_MAX_NO_OF_ROWS_FOR_PRINTOUT'])
        ) {
            $selectionThresh = intval($sessProcessThresh['GM_MAX_NO_OF_ROWS_FOR_PRINTOUT']);
        }

        if ($action == 'search') {
            // Build search params
            $searchParams = $this->buildSearchParams();

            if (!isset($searchParams) || empty($searchParams)) {
                return json_encode($retrievedDbIdsArr);
            }

            $paramSort = 'germplasmDbId:ASC';
            $paramLimit = 'limit=100';
            $paramPage = 'page=1';
            $currPage = 1;

            // Retrieve data
            $endpoint = "germplasm-search?$paramPage&$paramLimit&$paramSort";

            do {
                $response = Yii::$app->api->getResponse('POST', $endpoint, json_encode($searchParams));

                $data = $response['body']['result']['data'];
                $totalRetrieved += count($data);

                $currPage += 1;
                $paramPage = "page=$currPage";
                $totalCount = $response['body']['metadata']['pagination']['totalCount'];
                $totalPages = $response['body']['metadata']['pagination']['totalPages'];

                $endpoint = "germplasm-search?$paramPage&$paramLimit&$paramSort";

                // Extract IDs
                $retrievedDbIdsArr = array_merge($retrievedDbIdsArr, array_column($data, 'germplasmDbId'));
            } while ($currPage <= $totalPages && $totalRetrieved < $selectionThresh);
        }

        Yii::$app->session->set('germplasm-selected_ids', $retrievedDbIdsArr);
        return json_encode(['count' => $totalCount, 'ids' => $retrievedDbIdsArr]);
    }

    /**
     * Clear session values for selected IDs
     */
    public function actionClearSelectedIds()
    {
        Yii::$app->session->set('germplasm-selected_ids', []);
    }

    /**
     * Buld search parameters
     * 
     * @return mixed
     */
    public function buildSearchParams()
    {
        $filters = [];

        $queryParams = Yii::$app->session->get('germplasm-search-query-params');
        $browserFilters = Yii::$app->session->get('germplasm-search-browser-filters');

        if (!empty($queryParams)) {
            if (isset($queryParams['names'])) {
                if (isset($queryParams['names']['equals'])) {
                    $queryParams['names'] = $queryParams['names']['equals'];
                } else {
                    unset($queryParams['names']);
                }
            }
            $filters = $queryParams;
        }

        $tempFilters = [];
        foreach ($browserFilters as $key => $value) {
            if (!empty($value) && isset($value) ** $key !== 'names') {
                if ($key == 'otherNames') {
                    $tempFilters[$key] = [
                        'like' => '%' . $value . '%'
                    ];
                } else {
                    $tempFilters[$key] = [
                        'like' => $value
                    ];
                }
            }
        }

        if (!empty($tempFilters)) {
            if (!empty($queryParams)) {
                $filters['__browser_filters__'] = $tempFilters;
            } else {
                $filters = $tempFilters;
            }
        }

        return $filters;
    }
}
