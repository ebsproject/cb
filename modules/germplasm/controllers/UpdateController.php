<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use Yii;
use yii\helpers\Url;
use app\components\B4RController;
use app\controllers\Dashboard;

// Import models
use app\models\GermplasmFileUpload;
use app\models\Program;
use app\models\CropProgram;
use app\models\Worker;
use app\models\Person;

use app\modules\germplasm\models\UpdateModel;
use app\modules\germplasm\models\ViewBrowserModel;
use app\modules\germplasm\models\ViewFileUploadModel;

// Import data providers
use app\dataproviders\ArrayDataProvider;

/**
 * Controller for the `germplasm` module
 * Contains methods for facilitating the UPDATE of existing germplasm
 */
class UpdateController extends B4RController
{

    public $dashboardModel;
    public $germplasmFileUploadModel;
    public $worker;
    public $person;
    public $programModel;
    public $cropProgramModel;
    public $updateModel;
    public $viewBrowserModel;
    public $viewFileUploadModel;

    public function __construct(
        $id, $module, 
        Dashboard $dashboardModel,
        GermplasmFileUpload $germplasmFileUploadModel,
        Program $programModel,
        CropProgram $cropProgramModel,
        Worker $worker,
        UpdateModel $updateModel,
        Person $person,
        ViewBrowserModel $viewBrowserModel,
        ViewFileUploadModel $viewFileUploadModel,
        $config = []
        )
    {
        $this->germplasmFileUploadModel = $germplasmFileUploadModel;
        $this->dashboardModel = $dashboardModel;
        $this->worker = $worker;
        $this->person = $person;
        $this->programModel = $programModel;
        $this->cropProgramModel = $cropProgramModel;
        $this->updateModel = $updateModel;
        $this->viewBrowserModel = $viewBrowserModel;
        $this->viewFileUploadModel = $viewFileUploadModel;
    
        parent::__construct($id, $module, $config);
    }

    /**
     * Facilitates the rendering of the main Update tab contents
     * @param string program current program code
     * @return mixed
     */
    public function actionIndex($program = null){
        // Determine if user is ADMIN user
        $isAdmin = Yii::$app->session->get('isAdmin');
        // Check RBAC if has access
        $hasAccess = Yii::$app->access->renderAccess('GERMPLASM_UPDATE','GERMPLASM_CATALOG');
        // Check allowed actions to determine tabs to display
        $permissions = $isAdmin ? ['GERMPLASM_SEARCH', 'GERMPLASM_CREATE', 'GERMPLASM_MERGE', 'GERMPLASM_UPDATE'] : Yii::$app->session->get('currentPermissions')['permission'] ?? ['GERMPLASM_SEARCH'];
        // Redirect to proper page depending on access permissions
        if(!$isAdmin){
            if(!$hasAccess) {
                if(empty($permissions)) {
                    Yii::$app->session->setFlash('error', "You have no permission to the page you are trying to access. Please contact your data administrator.");
                    Yii::$app->response->redirect(Url::base().'/index.php');
                    return;
                } else {
                    return $this->redirect([
                        strtolower(substr($permissions[0], 10)).'/index',
                        'program' => $program
                    ]);
                }
            }
        }

        // Retrieve crop code by program code
        $cropCode = $this->programModel->getCropCode($program);

        // Retrieve germplasm file upload status
        $statusValuesArr = $this->germplasmFileUploadModel->getFileUploadStatusValues();

        // Retrieve transactions data
        $updateModel = $this->updateModel;
        $searchModel = $this->germplasmFileUploadModel;
        $dataProvider = $this->germplasmFileUploadModel->search(
            null,
            [
                "fileUploadOrigin" => "equals GERMPLASM_CATALOG",
                "fileUploadAction" => "update",
                "entity" => "germplasm_attribute"
        ]);

        $action = $this->updateModel->getModelAction();
        $level = $this->updateModel->getModelLevel();

        [$userRole,$isAdmin,$userId] = $this->person->getPersonRoleAndType($program);

        // Retrieve file upload variables
        $configValues = $this->germplasmFileUploadModel->getConfigValues('germplasm',$action,$userRole,$program,$cropCode);
        $variables = $this->germplasmFileUploadModel->getFileUploadVariables($configValues['configValues']);

        $value = '';
        if(isset($configValues['level']) && !empty($configValues['level'])){
            $level = $configValues['level'];
            $value = $configValues['levelValue'];
        } 

        $variableLevel = ($level == 'GLOBAL' ? $level : $value);
        $prepend = 'gm-'. strtolower($variableLevel) .'-'. strtolower($action);

        // store variables in session
        Yii::$app->session->set('gm-update-prepend',$prepend);
        $sessVariables = Yii::$app->session->get($prepend .'-variables') ?? [];
        if(empty($sessVariables) && isset($variables['variables']) && !empty($variables['variables'])){
            Yii::$app->session->set($prepend .'-variables',$variables['variables']);
            Yii::$app->session->set($prepend .'-variables-required',$variables['requiredVariables']);
        }

        if(empty($sessVariables) && (!isset($variables['variables']) || empty($variables['variables']))){
            Yii::$app->session->set($prepend .'-variables',[]);
            Yii::$app->session->set($prepend .'-variables-required',[]);
        }

        // Build applicable config abbrev
        $abbrevPrefix = $this->germplasmFileUploadModel->buildConfigAbbrev(
            'germplasm',
            $action,
            $level,
            $value);

        return $this->render('index',[
            'program' => $program,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'statusValuesArr' => $statusValuesArr,
            'module' => $action,
            'action' => $action,
            'selectAllMode' => 'include mode',
            'browserId' => 'dynagrid-germplasm-gefu-update-grid',
            'abbrevPrefix' => $abbrevPrefix,
            'updateModel' => $updateModel,
            'configValues' => $configValues['configValues']['values'] ?? []
        ]);
    }

    /**
     * Facilitates rendering of update file upload modal content
     * 
     * @return mixed
     */
    public function actionRenderFileUpload(){
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programDbId = $dashboardFilters['program_id'];
        $programCode = $_POST['programCode'] ?? '';
        $htmlData = $this->renderAjax('_file_upload', [
            'program' => $programDbId,
            'programCode' => $programCode
        ]);
        return json_encode($htmlData);
    }

    /**
     * Facilitates processing of uploaded update file
     * @param Integer programId - program identifier
     * @param String programCode - code of the program
     * @param String type - upload type (germplasm, germplasm_attribute)
     * @return mixed
     */
    public function actionProcessFileUpload($programId, $programCode, $type){
        $success = true;
        $message = '';

        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programId = $dashboardFilters['program_id'];

        // Variables for getConfigValues
        $cropCode = $this->programModel->getCropCode($programCode);
        [$userRole,$isAdmin,$userId] = $this->person->getPersonRoleAndType($programId);

        try{
            // retrieve uploaded file
            $uploadedFileInfo = $_FILES['germplasm_file_upload']; 

            if(isset($uploadedFileInfo) && !empty($uploadedFileInfo)){
                $variableConfig = $this->germplasmFileUploadModel->getConfigValues('germplasm','UPDATE',$userRole,$programId,$cropCode);

                // retrieve uploaded file content
                $uploadedFile = $this->germplasmFileUploadModel->retrieveUploadedFile('germplasm_file_upload');

                if($uploadedFile['success']){
                    // validate file contents
                    $result = $this->updateModel->validateUploadedFile($programId,$uploadedFile['headers'],$variableConfig['configValues']);

                    if($result['success']){
                        // create new file upload record
                        $result = $this->updateModel->saveUploadedFile($programId,$uploadedFile['fileName'],$uploadedFile['fileContent']);
                    }
                    return json_encode($result);
                }
                else{
                    $success = false;
                    $error = $uploadedFile['error'];
                }
            }
        }
        catch(\Exception $e){
            $success = false;
            $error = 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.';
        }

        return json_encode([
            'success' => $success,
            'message' => $message,
            'error' => $error
        ]) ;
    }

    /**
     * Retrieval of dashboard program id
     */
    function getDashboardProgramId(){
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programId = $dashboardFilters['program_id'] ?? '';

        return $programId;
    }

    /**
     * Redirect user to update germplasm transaction preview page
     * @param String $program program code
     * @param Integer $germplasmFileUploadDbId germplasm file upload transaction identifier
     * @param Boolean $systemDef use system default; default = TRUE
     * @param String $action view transaction action indicator
     */
    public function actionPreview($program = null,$germplasmFileUploadDbId = 0, $systemDef = true, $action = 'complete'){
        // retrieve dashboard program
        $programId = $this->getDashboardProgramId();

        // retrieve transaction record
        $requestBody = [
            "germplasmFileUploadDbId" => "equals ".$germplasmFileUploadDbId
        ];
        $updateGermplasmTransactionRecord = $this->germplasmFileUploadModel->searchAll($requestBody);

        if( $updateGermplasmTransactionRecord['status'] != 200 || 
            !isset($updateGermplasmTransactionRecord['data']) || empty($updateGermplasmTransactionRecord['data'])){
            // Redirect to the main update page
            return $this->redirect(['update/index', 'program' => $program]);
        }
        $fileDbId = $germplasmFileUploadDbId;

        $transactionFileName = $updateGermplasmTransactionRecord['data'][0]['fileName'];
        $transactionFileStatus = $updateGermplasmTransactionRecord['data'][0]['fileStatus'];
        $creator = $updateGermplasmTransactionRecord['data'][0]['uploader'];
        $creationTimestamp = $updateGermplasmTransactionRecord['data'][0]['uploadTimestamp'];
        $remarks = $updateGermplasmTransactionRecord['data'][0]['remarks'];

        $browserVariables = [];
        $transactionInfo = $this->viewBrowserModel->search($fileDbId, $programId,'update');

        // Define browser information
        $browserId = 'dynagrid-file-upload-update-summary-grid';

        $columns = $transactionInfo['columns'] ?? [];
        $dataProvider = $transactionInfo['dataProvider'] ?? new ArrayDataProvider([
            'id' => $browserId.'-grid',
            'allModels' => [],
            'restified' => true,
            'totalCount' => 0
        ]);

        if(!empty($transactionInfo['columns'])){
            $columns = $transactionInfo['columns'];
        }

        if(!empty($transactionInfo['data'])){
            $dataProvider = $transactionInfo['data'];
        }

        // Variables for getConfigValues
        $cropCode = $this->programModel->getCropCode($program);
        [$userRole,$isAdmin,$userId] = $this->person->getPersonRoleAndType($program);

        $abbrev = $this->germplasmFileUploadModel->getConfigValues('germplasm',$action,$userRole,$program,$cropCode);

        // render view update transaction page
        return $this->render('_update_summary.php',[
            'browserId' => $browserId,
            'dataProvider' => $dataProvider,
            'fileDbId' => $fileDbId,
            'fileUploadInfo' => [
                'fileName' => $transactionFileName ?? '',
                'fileStatus' => $transactionFileStatus ?? '',
                'uploader' => $creator,
                'uploadTimestamp' => $creationTimestamp,
                'remarks' => $remarks,
            ],
            'columns' => $columns,
            'searchModel' => $this->viewFileUploadModel,
            'program' => $program,
            'configAbbrevPrefix' => $this->germplasmFileUploadModel->buildConfigAbbrev('germplasm', 'update',$abbrev['level'],$abbrev['levelValue']) ?? '',
            'action' => $action
        ]);
    }

    /**
     * Redirects to the main update page (index)
     */
    public function actionRedirectToMainUpdatePage() {
        // Get program in dashboard
        $defaultFilters = (array) $this->dashboardModel->getFilters();
        $programId = $defaultFilters['program_id'];
        $result = $this->programModel->getOne($programId);
        $programInfo = $result['data'] ?? [];
        $program = $programInfo['programCode'] ?? '';

        Yii::$app->session->set('view-file-upload-entity', null); // reset entity session for viewing of germplasm file upload records

        // Redirect to the main update page
        return $this->redirect(['update/index', 'program' => $program]);
    }

    /**
     * Controller action for invoking workers. The germplasm file upload ID,
     * worker name, and description must be specified.
     */
    public function actionInvokeGermplasmWorker() {
        // Get germplasm file upload ID
        $germplasmFileUploadDbId = isset($_POST['germplasmFileUploadDbId']) ? $_POST['germplasmFileUploadDbId'] : 0;
        // Get worker name
        $workerName = isset($_POST['workerName']) ? $_POST['workerName'] : '';
        // description
        $description = isset($_POST['description']) ? $_POST['description'] : '';
        // additional worker data
        $additionalWorkerData = isset($_POST['additionalWorkerData']) ? (array) json_decode($_POST['additionalWorkerData']) : [];

        // Invoke worker
        $result = $this->worker->invoke(
            $workerName,                                    // Worker name
            $description,                                   // Description
            'GERMPLASM_FILE_UPLOAD',                        // Entity
            $germplasmFileUploadDbId,                       // Entity ID
            'GERMPLASM_FILE_UPLOAD',                        // Endpoint Entity
            'GERMPLASM_CATALOG',                            // Application
            'POST',                                         // Method
            [                                               // Worker data
                "germplasmFileUploadDbId" => $germplasmFileUploadDbId
            ],
            null,
            $additionalWorkerData
        );

        // Prepare flash message
        $type = 'info';
        $message = '';
        $iconClass = '';
        if($result['success']) {
            $type = 'info';
            $iconClass = 'fa fa-info-circle';
            $message = '<i class="' . $iconClass . ' hm-bg-info-icon"></i>&nbsp;&nbsp;The germplasm records are being created in the background. You may proceed with other tasks. You will be notified in this page once done.';
        }
        else {
            $type = 'warning';
            $iconClass = 'fa-fw fa fa-warning';
            $message = '<i class="' . $iconClass . ' hm-bg-info-icon"></i>&nbsp;&nbsp;A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
            // If status is not 200, the problem occurred while creating the background job record (code: #001)
            if($result['status'] != null && $result['status'] != 200) $message .= ' (#001)';
            // If status is 200, the problem occurred while connecting to the background worker (code: #002)
            else if($result['status'] == null) $message += ' (#002)';
        }
        // set flash
        \Yii::$app->session->setFlash($type, $message);

        // Redirect to the main update page
        return $this->actionRedirectToMainUpdatePage();
    }
}
?>