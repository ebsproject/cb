<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\germplasm\controllers;

use Yii;
use yii\helpers\Url;
use app\components\B4RController;
use app\controllers\Dashboard;

// Import models
use app\models\GermplasmFileUpload;
use app\models\Program;
use app\models\CropProgram;
use app\models\Worker;

use app\modules\germplasm\models\CreateModel;
use app\modules\germplasm\models\MergeModel;
use app\modules\germplasm\models\ViewBrowserModel;
use app\modules\germplasm\models\ViewFileUploadModel;
use app\modules\germplasm\models\ConflictReportModel;

// Import data providers
use app\dataproviders\ArrayDataProvider;

/**
 * Controller for the `germplasm` module
 * Contains methods for managing merging of new germplasms
 */
class MergeController extends B4RController
{

    public $dashboardModel;
    public $germplasmFileUploadModel;
    public $worker;

    public $createModel;
    public $mergeModel;
    public $viewBrowserModel;
    public $viewFileUploadModel;
    public $programModel;
    public $cropProgramModel;
    public $conflictReportModel;

    public function __construct(
        $id, $module, 
        Dashboard $dashboardModel,
        GermplasmFileUpload $germplasmFileUploadModel,
        CreateModel $createModel,
        MergeModel $mergeModel,
        ViewBrowserModel $viewBrowserModel,
        ViewFileUploadModel $viewFileUploadModel,
        ConflictReportModel $conflictReportModel,
        Program $programModel,
        CropProgram $cropProgramModel,
        Worker $worker,
        $config = []
        )
    {
        $this->germplasmFileUploadModel = $germplasmFileUploadModel;
        $this->dashboardModel = $dashboardModel;
        $this->createModel = $createModel;
        $this->mergeModel = $mergeModel;
        $this->worker = $worker;

        $this->viewBrowserModel = $viewBrowserModel;
        $this->viewFileUploadModel = $viewFileUploadModel;
        $this->conflictReportModel = $conflictReportModel;
        $this->programModel = $programModel;
        $this->cropProgramModel = $cropProgramModel;
        $this->worker = $worker;
    
        parent::__construct($id, $module, $config);
    }

    /**
     * Facilitates the rendering of the main Merge tab contents
     * @param string program current program code
     * @return mixed
     */
    public function actionIndex($program = null){
        // Determine if user is ADMIN user
        $isAdmin = Yii::$app->session->get('isAdmin');
        // Check RBAC if has access
        $hasAccess = Yii::$app->access->renderAccess('GERMPLASM_MERGE','GERMPLASM_CATALOG');
        // Check allowed actions to determine tabs to display
        $permissions = $isAdmin ? ['GERMPLASM_SEARCH', 'GERMPLASM_CREATE', 'GERMPLASM_MERGE', 'GERMPLASM_UPDATE'] : Yii::$app->session->get('currentPermissions')['permission'] ?? ['GERMPLASM_SEARCH'];
        // Redirect to proper page depending on access permissions
        if(!$isAdmin){
            if(!$hasAccess) {
                if(empty($permissions)) {
                    Yii::$app->session->setFlash('error', "You have no permission to the page you are trying to access. Please contact your data administrator.");
                    Yii::$app->response->redirect(Url::base().'/index.php');
                    return;
                } else {
                    return $this->redirect([
                        strtolower(substr($permissions[0], 10)).'/index',
                        'program' => $program
                    ]);
                }
            }
        }

        // initialize MERGE session variables
        Yii::$app->session->set($this->mergeModel->getKeyVariables(), null);
        Yii::$app->session->set($this->mergeModel->getKeyVariablesRequired(), null);
        Yii::$app->session->set($this->mergeModel->getKeyVariablesOptional(), null);

        $programDbId = Yii::$app->userprogram->get('id');

        // retrieve variable config
        $configVars = $this->createModel->getVariableConfig($programDbId, true, 'GM_MERGE_GERMPLASM_');

        // retrieve germplasm file upload status
        $statusValuesArr = $this->germplasmFileUploadModel->getFileUploadStatusValues();

        // save to session
        Yii::$app->session->set($this->mergeModel->getKeyVariables(), $configVars);

        $searchModel = $this->germplasmFileUploadModel;
        $dataProvider = $this->germplasmFileUploadModel->search(
            null,
            [
                "fileUploadOrigin" => "equals GERMPLASM_CATALOG",
                "fileUploadAction" => "merge"
        ]);

        $statusValuesArr = $this->germplasmFileUploadModel->getFileUploadStatusValues();
        $fileUploadConfigAbbrev = $this->mergeModel->getFileUploadConfigAbbrev() ?? 'GM_MERGE_GERMPLASM_';

        return $this->render('index',[
            'programDbId' => $programDbId,
            'program' => Yii::$app->userprogram->get('abbrev'),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'fileUploadStatusArr' => $statusValuesArr,
            'selectAllMode' => 'include mode',
            'browserId' => 'dynagrid-germplasm-gefu-merge-grid',
            'fileUploadConfigAbbrev' => $fileUploadConfigAbbrev,
            'statusValuesArr' => $statusValuesArr,
            'selectAllMode' => 'include mode',
            'abbrevPrefix' => $fileUploadConfigAbbrev.'CONFIG_'
        ]);
    }

    /**
     * Redirect user to merge germplasm transaction preview page
     * @param String $program program code
     * @param Integer $germplasmFileUploadDbId germplasm file upload transaction identifier
     * @param Boolean $systemDef use system default; default = TRUE
     * @param String $action view transaction action indicator
     */
    public function actionPreview($program = null,$germplasmFileUploadDbId = 0, $systemDef = true, $action = 'complete'){
        // retrieve dashboard program
        $programId = $this->getDashboardProgramId();

        // retrieve transaction record
        $requestBody = [
            "germplasmFileUploadDbId" => "equals ".$germplasmFileUploadDbId
        ];
        $mergeGermplasmTransactionRecord = $this->germplasmFileUploadModel->searchAll($requestBody);

        if( $mergeGermplasmTransactionRecord['status'] != 200 || 
            !isset($mergeGermplasmTransactionRecord['data']) || empty($mergeGermplasmTransactionRecord['data'])){
            // Redirect to the main merge page
            return $this->redirect(['merge/index', 'program' => $program]);
        }
        $fileDbId = $germplasmFileUploadDbId;

        $transactionFileName = $mergeGermplasmTransactionRecord['data'][0]['fileName'];
        $transactionFileStatus = $mergeGermplasmTransactionRecord['data'][0]['fileStatus'];
        $creator = $mergeGermplasmTransactionRecord['data'][0]['uploader'];
        $creationTimestamp = $mergeGermplasmTransactionRecord['data'][0]['uploadTimestamp'];
        $remarks = $mergeGermplasmTransactionRecord['data'][0]['remarks'];

        $browserVariables = [];
        $transactionInfo = $this->viewBrowserModel->search($fileDbId, $programId,'MERGE_GERMPLASM');

        // Define browser information
        $browserId = 'dynagrid-file-upload-merge-summary-grid';
        
        $columns = $transactionInfo['columns'] ?? [];
        $dataProvider = $transactionInfo['dataProvider'] ?? new ArrayDataProvider([
            'id' => $browserId.'-grid',
            'allModels' => [],
            'restified' => true,
            'totalCount' => 0
        ]);

        if(!empty($transactionInfo['columns'])){
            $columns = $transactionInfo['columns'];
        }

        if(!empty($transactionInfo['data'])){
            $dataProvider = $transactionInfo['data'];
        }

        // render view merge transaction page
        return $this->render('_merge_summary',[
            'browserId' => $browserId,
            'dataProvider' => $dataProvider,
            'fileDbId' => $fileDbId,
            'fileUploadInfo' => [
                'fileName' => $transactionFileName ?? '',
                'fileStatus' => $transactionFileStatus ?? '',
                'uploader' => $creator,
                'uploadTimestamp' => $creationTimestamp,
                'remarks' => $remarks,
            ],
            'columns' => $columns,
            'searchModel' => $this->viewFileUploadModel,
            'program' => $program,
            'configAbbrevPrefix' => $this->mergeModel->getFileUploadConfigAbbrev() ?? '',
            'action' => $action
        ]);
    }

    /**
     * Facilitate deletion of germplasm file upload record
     */
    public function actionDelete(){
        $germplasmFileUploadDbId = $_POST['germplasmFileUploadDbId'] ?? 0;

        $this->germplasmFileUploadModel->deleteOne($germplasmFileUploadDbId);
    }

    /**
     * Facilitate trigger of MergeGermplasm worker to validate transaction
     * 
     * @param Integer $program program identifier
     * @param Integer $germplasmFileUploadId germplasm file upload transaction identifier
     * 
     * @return mixed
     */
    public function actionValidateMergeTransaction($program = null,$germplasmFileUploadId = 0){
        // retrieve transaction ID
        $postParams = $_POST;
        $mergeTransactionId = $postParams['fileUploadId'] ?? $germplasmFileUploadId;
        $fileUploadName = $postParams['fileUploadName'] ?? '';

        // get dashboard program ID
        $programId = $this->getDashboardProgramId() ?? 0;

        // invoke worker
        $result = $this->worker->invoke(
            'MergeGermplasm',                                                                           // Worker name
            'Validate merge germplasm transaction : (' . $mergeTransactionId . ') '.$fileUploadName,    // Description
            'GERMPLASM_FILE_UPLOAD',                                                                    // Entity
            $mergeTransactionId,                                                                        // Entity ID
            'GERMPLASM_FILE_UPLOAD',                                                                    // Endpoint Entity
            'GERMPLASM_CATALOG',                                                                        // Application
            'POST',                                                                                     // Method
            [                                                                                           // Worker data
                "germplasmFileUploadId" => $mergeTransactionId,
                "programDbId" => $programId,
                "processName" => 'validate-merge'
            ]
        );

        // return result invoking worker
        return json_encode($result);
    }

    /**
     * Facilitate download of template
     * 
     * @param String $program dashboard program
     */
    public function actionDownloadTemplate($program){
        // retrieve template variables
        $abbrev = ($this->mergeModel->getFileUploadConfigAbbrev() ?? 'GM_MERGE_GERMPLASM_') . 'CONFIG_';
        $fileName = $this->mergeModel->getTemplateFileName() ?? 'GM_MERGE_GERMPLASM_TEMPLATE_DEFAULT';
        
        $programId = $this->getDashboardProgramId();

        $cropVariablesConfig = $this->createModel->getVariableConfig($programId,false,$abbrev);
        $sysVariableConfig = $this->createModel->getVariableConfig($programId,true,$abbrev);

        $headers = array_unique(array_column(array_merge($cropVariablesConfig,$sysVariableConfig),'column_header'));
        
        if(empty($headers)){
            return false;
        }
        
        $this->createModel->generateCSVTemplate($headers,'','',$fileName);

    }

    /**
     * Facilitate completion of merge germplasm transaction
     * 
     * @param String $program program code
     * @param Integer $germplasmFileUploadId transaction ID
     * 
     * @return mixed
     */
    public function actionCompleteMergeTransaction($program = null,$germplasmFileUploadId = 0){
        // retrieve transaction ID
        $postParams = $_POST;
        $mergeTransactionId = $postParams['fileUploadId'] ?? $germplasmFileUploadId;
        $fileUploadName = $postParams['fileUploadName'] ?? '';

        // get dashboard program ID
        $programId = $this->getDashboardProgramId() ?? 0;

        // invoke worker
        $result = $this->worker->invoke(
            'MergeGermplasm',                                                                           // Worker name
            'Complete merge germplasm transaction : (' . $mergeTransactionId . ') '.$fileUploadName,    // Description
            'GERMPLASM_FILE_UPLOAD',                                                                    // Entity
            $mergeTransactionId,                                                                        // Entity ID
            'GERMPLASM_FILE_UPLOAD',                                                                    // Endpoint Entity
            'GERMPLASM_CATALOG',                                                                        // Application
            'POST',                                                                                     // Method
            [                                                                                           // Worker data
                "germplasmFileUploadId" => $mergeTransactionId,
                "programDbId" => $programId,
                "processName" => 'process-merge'
            ]
        );

        // return result invoking worker
        return json_encode($result);
    }

    function getDashboardProgramId(){
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programId = $dashboardFilters['program_id'] ?? '';

        return $programId;
    }

    /**
     * Facilitates rendering of merge file upload modal content
     * 
     * @return mixed
     */
    public function actionRenderFileUpload(){
        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programDbId = $dashboardFilters['program_id'];
        $programCode = $_POST['programCode'] ?? '';

        $htmlData = $this->renderAjax('_file_upload', [
            'program' => $programDbId,
            'programCode' => $programCode
        ]);

        return json_encode($htmlData);
    }

    /**
     * Facilitates processing of uploaded merge file
     * 
     * @return mixed
     */
    public function actionProcessFileUpload(){
        $success = true;
        $message = '';

        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programId = $dashboardFilters['program_id'];

        try{
            // retrieve uploaded file
            $uploadedFileInfo = $_FILES['germplasm_file_upload']; 

            if(isset($uploadedFileInfo) && !empty($uploadedFileInfo)){
                $variableConfig = Yii::$app->session->get($this->mergeModel->getKeyVariables());

                // retrieve uploaded file content
                $uploadedFile = $this->mergeModel->retrieveUploadedFile('germplasm_file_upload');

                if($uploadedFile['success']){
                    // validate file contents
                    $result = $this->mergeModel->validateUploadedFile($programId,$uploadedFile['headers'],$variableConfig);

                    if($result['success']){
                        // create new file upload record
                        $result = $this->mergeModel->saveUploadedFile($programId,$uploadedFile['fileName'],$uploadedFile['fileContent']);
                    }
                    return json_encode($result);
                }
                else{
                    $success = false;
                    $error = $uploadedFile['error'];
                }
            }
        }
        catch(\Exception $e){
            $success = false;
            $error = 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.';
        }

        return json_encode([
            'success' => $success,
            'message' => $message,
            'error' => $error
        ]) ;
    }

    /**
     * Download merge conflict reports from filepath 
     * 
     * @param String $fileName Name of file to be downloaded
     */
    public function actionDownloadData($fileName){
        $fileName = $_POST['fileName'] ?? $fileName;

        $pathToFile = realpath(Yii::$app->basePath) . "/files/data_export/$fileName";

        if (file_exists($pathToFile)) {
            // Set headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($pathToFile));

            // Flush system output buffer
            flush();

            // Read/Download CSV file
            readfile($pathToFile);

            die();
        }
    }

    /**
     * Facilitate the rendering of the conflict resolution page
     * 
     * @param String $program program code
     * @param Integer $fileId germplasm file upload identifier
     * @param String $action action (resolve)
     * 
     * @return mixed
     */
    public function actionResolveConflict($program,$fileId,$action){
        // retrieve dashboard program
        $programId = $this->getDashboardProgramId();

        // update germplasm file upload record
        $this->germplasmFileUploadModel->updateOne($fileId, ["fileStatus" => "resolving conflict"]);

        // retrieve transaction record
        $transactionRecord = $this->germplasmFileUploadModel->searchAll(
            [ "germplasmFileUploadDbId" => "equals ".$fileId ]
        );

        if( $transactionRecord['status'] != 200 || !isset($transactionRecord['data']) 
            || empty($transactionRecord['data'])){
            
            // Redirect to the main merge page
            return $this->redirect(['merge/preview', 'program' => $program]);
        }

        $transactionFileName = $transactionRecord['data'][0]['fileName'] ?? '';
        $transactionFileStatus = $transactionRecord['data'][0]['fileStatus'] ?? '';
        $creator = $transactionRecord['data'][0]['uploader'] ?? '';
        $creationTimestamp = $transactionRecord['data'][0]['uploadTimestamp'] ?? '';
        $remarks = $transactionRecord['data'][0]['remarks'] ?? '';
        
        // Define browser information
        $browserId = 'dynagrid-file-upload-merge-summary-grid';
        
        $browserVariables = $this->conflictReportModel->getConflictColumns() ?? [];
        $processedData = $this->conflictReportModel->search($fileId,$remarks,$browserId) ?? [];
        
        $data = $processedData['data'];
        $totalCount = $processedData['totalCount'];

        $keepColumnClass = 'green lighten-4';
        $mergeColumnClass = 'yellow lighten-4';

        $columns = array_map(function($header) use ($keepColumnClass, $mergeColumnClass){
            $classColor = strpos($header,'merge') > -1 ? $mergeColumnClass : $keepColumnClass;
            
            $contentOptions = [];
            $headerOptions = [
                'style' => [
                    'min-width' => '150px'
                ]
            ];
            
            if(strpos($header,'merge') > -1 || strpos($header,'keep') > -1){
                $headerOptions['class'] = $classColor;
                $contentOptions['class'] = $classColor;
            }

            return [
                'attribute' => strtolower($header),
                'label' => strtoupper(str_replace('_',' ',$header)),
                'headerOptions' => $headerOptions,
                'contentOptions' => $contentOptions
            ];
        }, $browserVariables);

        $dataProvider = new ArrayDataProvider([
            'id' => $browserId,
            'allModels' => $data,
            'restified' => true,
            'totalCount' => $totalCount
        ]);

        $searchModel = $this->conflictReportModel;

        // render conflict resolution page
        return $this->render('_conflict_resolution_page',[
            'program' => $program,
            'browserId' => $browserId,
            'columns' => $columns,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'action' => $action ?? 'resolve',
            'fileUploadInfo' => [
                'fileId' => $fileId,
                'fileName' => $transactionFileName,
                'fileStatus' => $transactionFileStatus,
                'uploader' => $creator,
                'uploadTimestamp' => $creationTimestamp,
                'remarks' => $remarks,
            ]
        ]);
    }

    /**
     * Facilitate processing of submitted conflict resolution
     * 
     * @param Integer $fileId file identifier
     * 
     * @return mixed
     */
    public function actionSubmitResolution($fileId){
        $postParams = $_POST;
        $savedResolutionsArr = $postParams['savedResolutionsArr'] ?? [];
        $reportFileName = $postParams['fileName'] ?? '';

        // remove duplicates
        if(is_array($savedResolutionsArr)){
            $savedResolutionsArr = count($savedResolutionsArr) == 0 ? [] : array_unique($savedResolutionsArr);
        }

        if(is_string($savedResolutionsArr)){
            $savedResolutionsArr = [];
        }

        // retrieve conflict report
        $conflictReportData = $this->conflictReportModel->extractConflictReportData($reportFileName);

        // format resolved values
        $matchingReports = [];
        foreach($savedResolutionsArr as $resolutionItem){
            $idArr = explode('-',$resolutionItem);

            $matchingReport = array_filter($conflictReportData,function ($report) use ($idArr){
                return $report['transaction_id'] == $idArr[0] && 
                       $report['row_number'] == $idArr[1] && 
                       strtolower($report['conflict_code']) == $idArr[2] && 
                       strtolower($report['conflict_variable']) == $idArr[3];
            });
            
            if(!empty($matchingReport)){
                if(!isset($matchingReports[$idArr[1]])){
                    $matchingReports[$idArr[1]] = [];
                }

                $matchingReport = array_values($matchingReport);
                $matchingReports[$idArr[1]][$matchingReport[0]['conflict_variable']] = $matchingReport[0]['merge_value'];
            }
            
        }

        $result = $this->mergeModel->addMergeConflictResolution($fileId,$matchingReports);        

        return json_encode(['success' => $result['success'], 'message' => $result['message']]);
    }
}
?>