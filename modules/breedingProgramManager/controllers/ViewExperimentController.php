<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\breedingProgramManager\controllers;

use app\components\B4RController;

use app\models\Experiment;
use app\modules\breedingProgramManager\models\Experiments;

class ViewExperimentController extends B4RController {

    public function __construct (
        $id,
        $module,
        public Experiment $experiment,
        public Experiments $experiments,
        $config=[]
    )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Render View Experiment Page
     *
     * @param $program text current program abbrev of user
     * @param $id int id of the experiment
     *
     * @return mixed
     */
    public function actionIndex($program, $id) {
        $columnsData1 = $this->experiments->getViewFieldsFromConfig();
        $fields = $columnsData1['fields'];
    
        $fieldNames = [];
        foreach ($fields as $field) {
            $field = strtolower($field);
            $field = str_replace(' ', '', ucwords(str_replace('_', ' ', $field)));
            $fieldNames[] = [
                'attribute' => $field
            ];
        }
    
        // Retrieve experiment data
        $experimentInfo = $this->experiment->searchAll([
            'experimentDbId' => "equals $id",
            'fields' => 'experiment.id AS experimentDbId | '.
                'experiment.experiment_status AS experimentStatus | '.
                'experiment.experiment_code AS experimentCode | '.
                'experiment.experiment_name AS experimentName | '.
                'experiment.experiment_type AS experimentType | '.
                'stage.stage_code AS stageCode | '.
                'experiment.experiment_year AS experimentYear | ' .
                'season.season_code AS seasonCode | '.
                'experiment.experiment_design_type AS experimentDesignType | '.
                'person.person_name AS experimentSteward | '.
                'pipeline.pipeline_code AS bpId | '.
                'pipeline.pipeline_name AS bpShortName '
        ]);
    
        $mappedData = [];
        foreach ($experimentInfo['data'][0] as $key => $value) {
            $formattedKey = ucfirst($key);
            foreach ($fieldNames as $field) {
                if ($field['attribute'] === $formattedKey) {
                    $mappedData[$field['attribute']] = $value;
                    break;
                }
            }
        }
        
        $experimentName = isset($experimentInfo['data'][0]['experimentName']) ? $experimentInfo['data'][0]['experimentName'] : '';

        return $this->render('index', [
            'fieldNames' => $fieldNames,
            'experimentInfo' => $experimentInfo,
            'mappedData' => $mappedData,
            'program' => $program,
            'experimentName' => $experimentName
        ]);
    }
}