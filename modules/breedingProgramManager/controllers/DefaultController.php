<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\breedingProgramManager\controllers;

use app\components\B4RController;
use app\controllers\Dashboard;
use app\modules\breedingProgramManager\models\Experiments;
use app\modules\experimentCreation\models\BrowserModel;
use Yii;

/**
 * Controller for the `breeding program manager` module
 * Contains methods for breeding program manager
 */
class DefaultController extends B4RController
{
    public function __construct(
        $id,
        $module,
        protected Dashboard $dashboard,
        protected Experiments $experiments,
        protected BrowserModel $browserModel,
        $config=[],
    )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the data browser for breeding program manager
     * 
     * @param String $program Current dashboard program
     * @return mixed
     */
    public function actionIndex($program = null)
    {
        $columnsData = $this->experiments->getBrowserColsFromConfig();
        $headers = $columnsData['headers'];
        
        $browserId = 'dynagrid-bpm-experiments';
        $searchModel = $this->experiments;
        $params = Yii::$app->request->queryParams;
        $params['params'] = $params;

        // Add dashboard filters to params
        $dashboardFilters = (array) $this->dashboard->getFilters();
        $p = [];
        foreach($dashboardFilters as $key => $value){
            $p[$key] = $value;
        }
        // if program is empty, add invalid program id value
        if($program == null){
            $dashboardFilters['program_id'] = "0";
        }
        $filters = isset($p) ? $this->browserModel->formatFilters($p) : [];
        foreach($filters as $key => $value){
            if(isset($params['params']['Experiments'][$key]) && !str_contains($value, $params['params']['Experiments'][$key])) $params['invalidFilter'] = true;
            if(isset($params['Experiments'][$key])) {
                if($params['Experiments'][$key] != '') $params['Experiments'][$key] = $params['Experiments'][$key] . '|';
                $params['Experiments'][$key] = $params['Experiments'][$key] . $value;
            } else {
                $params['Experiments'][$key] = $value;
            }
        }

        $output = $searchModel->search($params, $headers);
        $dataProvider = $output[0];

        $columns = [];
        foreach ($headers as $col) {
            $col = strtolower($col);
            $col = str_replace(' ', '', ucwords(str_replace('_', ' ', $col)));
            $col[0] = strtolower($col[0]);
            $columns[] = [
                'attribute' => $col
            ];
        }

        return $this->render('index', [
            'program' => $program,
            'columns' => $columns,
            'browserId' => $browserId,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'paramSort' => $output[2],
            'experimentDbId' => $output[0]
        ]);
    }
}
