<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\breedingProgramManager\controllers;

use Yii;
use app\components\B4RController;

use app\models\UserDashboardConfig;
use app\models\Crop;
use app\models\CropProgram;
use app\models\Experiment;
use app\models\ExperimentSearch;
use app\models\BreedingPortalData;


use app\dataproviders\ArrayDataProvider;

class UpdateExperimentController extends B4RController {

    public function __construct (
        $id,
        $module,
        public UserDashboardConfig $userDashboardConfig,
        public Crop $crop,
        public CropProgram $cropProgram,
        public Experiment $experiment,
        public ExperimentSearch $experimentSearch,
        public BreedingPortalData $breedingPortalData,
        $config=[]
    )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Render Update Experiment Page
     *
     * @param $program text current program abbrev of user
     *
     * @return mixed
     */
    public function actionIndex($program) {
        // Get selected experiment ids from Session
        $sessionName = 'bpm-selected-experiments-for-update';
        $experimentDbIds = [];

        if(isset(Yii::$app->session[$sessionName])) {
            $experimentDbIds = Yii::$app->session[$sessionName];
            unset(Yii::$app->session[$sessionName]);
        }

        // If no selected experiments then redirect back to BPM experiments
        if($experimentDbIds == []) {
            $this->redirect(['default/index?program='.$program]);
        }

        $dbIds = 'equals '.implode('|equals ', $experimentDbIds);

        $params = Yii::$app->request->queryParams;
        $params['ExperimentSearch']['experimentDbId'] = $dbIds;

        // get params for data browser
        $paramPage = '';
        $pageParams = '';
        $paramSort = '';

        // get current sorting
        if (isset($_GET['sort'])) {
            $paramSort = 'sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        } else {
            $paramSort = 'sort=experimentYear:desc|creationTimestamp:desc';
        }

        $paramLimit = '?limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();

        if (isset($_GET['dynagrid-experiment-browser-page'])){
            $paramPage = '&page=' . $_GET['dynagrid-experiment-browser-page'];
        }

        $currentPage = !empty($params) ? isset($_GET['dynagrid-experiment-browser-page']) ? $_GET['dynagrid-experiment-browser-page'] : null : null;

        $pageParams = $paramLimit.$paramPage;
        $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;

        $experimentRecords = $this->experiment->searchAll($params['ExperimentSearch'], $pageParams, false);

        $attributes = [
            'experimentDbId',
            'experimentYear',
            'stageDbId',
            'stageCode',
            'stageName',
            'seasonDbId',
            'seasonCode',
            'seasonName',
            'experimentName',
            'experimentType',
            'experimentStatus',
            'creationTimestamp',
            'creatorDbId',
            'creator',
            'modificationTimestamp',
            'modifierDbId',
            'modifier',
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $experimentRecords['data'],
            'key' => 'experimentDbId',
            'restified' => true,
            'id' => 'UpdateExperiment',
            'totalCount' => $experimentRecords['totalCount']
        ]);

        // Show applicable pipelines filtered by crop, institute(organisation) where the program is
        // The crop is included in the experiment records, use program to get the organisation
        $programs = array_column($experimentRecords['data'], 'programDbId');
        $programs = array_count_values($programs);
        $programs = array_keys($programs);
        $programs = implode('|', $programs);

        $crops = $this->crop->searchAll(['programDbId' => $programs],'',false);
        $cropDbIds = array_column($crops['data'], 'cropDbId');
        $cropDbIds = array_count_values($cropDbIds);
        $cropDbIds = array_keys($cropDbIds);
        $cropDbIds = implode('|', $cropDbIds);

        $cropCodes = array_column($crops['data'], 'cropCode');
        $cropCodes = array_count_values($cropCodes);
        $cropCodes = array_change_key_case($cropCodes, CASE_LOWER);

        $cropPrograms = $this->cropProgram->searchAll(['cropDbId' => $cropDbIds],'',false);
        $organizations = array_column($cropPrograms['data'], 'organizationCode');
        $organizations = array_count_values($organizations);
        $organizations = array_change_key_case($organizations, CASE_LOWER);

        // Get Breeding Portal Data
        $searchFilter = '';
        $filters = [
            'fields' => 'bp_data.id AS breedingPortalDataDbId|bp_data.entity_name AS entityName|bp_data.transformed_data AS transformedData|
                        bp_data.modification_timestamp AS modificationTimestamp',
            'entityName' => 'equals breeding_pipeline'
        ];
        $response = $this->breedingPortalData->searchAll($filters,$searchFilter,false);
        $data = $response['data'];
        $breedingPipelineData = [];
        foreach ($data as $value) {
            if($value['entityName'] === 'breeding_pipeline') $breedingPipelineData = $value;
        }

        // If transformed data doesn't exist yet, create and use it
        if(!isset($breedingPipelineData['transformedData'])) {
            $this->breedingPortalData->updateTransformedData();
            $response = $this->breedingPortalData->searchAll($filters,$searchFilter,false);
            $data = $response['data'];
            $breedingPipelineData = [];
            foreach ($data as $value) {
                if($value['entityName'] === 'breeding_pipeline') $breedingPipelineData = $value;
            }
        }

        $breedingPipelineOptions = [];
        foreach ($breedingPipelineData['transformedData'] as $breedingPipeline) {
            if(isset($cropCodes[strtolower($breedingPipeline['crop'])]) && isset($organizations[strtolower($breedingPipeline['organisation'])])) {
                $breedingPipelineOptions[] = [
                    'bpid' => $breedingPipeline['bpid'],
                    'shortName' => $breedingPipeline['shortName'],
                    'longName' => $breedingPipeline['longName'],
                    'description' => $breedingPipeline['description']
                ];
            }
        }

        return $this->render('/update-experiment/index',[
            'experimentDbIds' => $experimentDbIds,
            'dataProvider' => $dataProvider,
            'searchModel' => $this->experimentSearch,
            'program' => $program,
            'breedingPipelineOptions' => $breedingPipelineOptions
        ]);
    }

    /**
     * Save selected experiments to session
     */
    public function actionSaveExperimentSelection() {
        // Get selected experiment ids from POST
        $experimentDbIds = $_POST['experimentDbIds'] ?? '[]';
        $experimentDbIds = json_decode($experimentDbIds);

        Yii::$app->session->set('bpm-selected-experiments-for-update', $experimentDbIds);
    }

    /**
     * Save Pipeline to Experiments
     *
     * @return Boolean if success
     */
    public function actionUpdateExperimentPipeline() {
        $bpData = $_POST['bpData'] ?? null;
        $experimentDbIds = $_POST['experimentDbIds'] ?? null;
        $newPipelineDbId = null;

        if(!isset($bpData,$experimentDbIds)) {
            return false;
        }

        // Check if pipeline exists
        $pipelines = Yii::$app->api->getResponse('GET', 'pipelines');
        $pipelines = $pipelines['body']['result']['data'];
        foreach ($pipelines as $pip) {
            if($pip['pipelineCode'] == $bpData['bpId']) {
                $newPipelineDbId = $pip['pipelineDbId'];
                break;
            }
        }

        // If pipeline doesn't exist, create pipeline data in db
        if($newPipelineDbId == null) {
            $newPipeline = [
                'records' => [
                    [
                        'pipelineCode' => $bpData['bpId'],
                        'pipelineName' => $bpData['bpLongName'],
                        'description' => $bpData['description'],
                        'pipelineStatus' => 'active',
                        'notes' => json_encode($bpData)
                    ]
                ]
            ];
            $response = Yii::$app->api->getResponse('POST', 'pipelines', json_encode($newPipeline));

            if($response['status'] == 200 && isset($response['body']['result']['data'][0]['pipelineDbId'])){
                $newPipelineDbId = $response['body']['result']['data'][0]['pipelineDbId'];
            } else {
                return false;
            }
        }

        // Save pipeline db id to experiments
        foreach ($experimentDbIds as $expDbId) {
            $response = $this->experiment->updateOne($expDbId, ['pipelineDbId' => "$newPipelineDbId"]);
            if($response['status'] != 200){
                return false;
            }
        }

        return true;
    }
}