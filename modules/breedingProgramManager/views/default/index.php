<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

/**
 * Renders breeding program manager
 * landing page
 */

// Import components
use app\components\FavoritesWidget;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

Yii::$app->session->set('experiment-param-sort' . '', $paramSort);

/**
 * Render tabs in Breeding Program Manager
 */
// Define URLs
$experimentsUrl = Url::to(['/breedingProgramManager/default/index','program' => $program]);

$renderExperimentTab = Yii::$app->access->renderAccess("BPM_EXPERIMENTS_TAB","BREEDING_PROGRAM_MANAGER"); 

$experimentsTabHTML = !$renderExperimentTab ? "" : '
    <li class="tab col s3 bpm-tool-tab" id="experiments-tab" link="' . $experimentsUrl . '">
        <a class="experiments-tab a-tab active" href="' . $experimentsUrl . '">' . \Yii::t('app', 'Experiments') . '</a>
    </li>';

// Set tool name
$nameDisplay = Yii::t('app', 'Breeding Program Manager') .
FavoritesWidget::widget([
    'module' => 'breedingProgramManager'
]);
?>

<div class="col-md-9" style="margin:10px;">
    <h3><?= $nameDisplay ?></h3>
</div>

<!-- Tabs -->
<div class="row" style="margin-top:0px; margin-bottom:0px">
    <ul id="tabs" class="tabs">
        <?php
            echo $experimentsTabHTML;
        ?>
    </ul>
    <br />
</div>

<div>
    <?php

        if($renderExperimentTab){
            //check access for update experiments
            $renderUpdateBtn = Yii::$app->access->renderAccess("BPM_EXPERIMENTS_UPDATE","BREEDING_PROGRAM_MANAGER"); 
            $updateButton = Html::a('Update','#',[
                'type'=>'button',
                'title'=>\Yii::t('app','Update experiments'),
                'class'=>'tabular-btn btn waves-effect waves-light pull-right',
                'id'=>'update-experiments-btn'
            ]);
            $actionColumns = [
                [//checkbox
                    'label'=> '',
                    'header'=>'',
                    'content'=>function($data){
                        $experimentDbId = $data['experimentDbId'];

                        return '<input class="experiment-grid-select filled-in" type="checkbox" id="'.$experimentDbId.'" />
                            <label for="'.$experimentDbId.'"></label>
                        ';
                    },
                    'hAlign'=>'center',
                    'vAlign'=>'top',
                    'hiddenFromExport'=>true,
                    'mergeHeader'=>true,
                    'order'=> DynaGrid::ORDER_FIX_LEFT,
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'header' => false,
                    'noWrap' => true,
                    'vAlign' => 'top',
                    'order' => DynaGrid::ORDER_FIX_LEFT,
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($url, $model) use ($program) {
                            $experimentDbId = is_array($model) ? $model['experimentDbId'] : $model->experimentDbId;
                            
                            $url = Url::to([
                                '/breedingProgramManager/view-experiment',
                                'program' => $program,
                                'id' => $experimentDbId
                            ]);

                            return Html::a(
                                '<i class="material-icons">remove_red_eye</i>',
                                $url,
                                [
                                    'title' => Yii::t('app', 'View Experiment')
                                ]
                            );
                        }
                    ]
                ],
            ];

            $gridColumns = array_merge($actionColumns, $columns);

            DynaGrid::begin([
                'columns' => $gridColumns,
                'theme' => 'simple-default',
                'showPersonalize' => true,
                'storage' => DynaGrid::TYPE_SESSION,
                'showFilter' => false,
                'showSort' => false,
                'allowFilterSetting' => false,
                'allowSortSetting' => false,
                'gridOptions' => [
                    'id' => "$browserId-id",
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'showPageSummary' => false,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => ['id' => "$browserId-id"],
                        'beforeGrid' => '',
                        'afterGrid' => ''
                    ],
                    'responsiveWrap' => false,
                    'panel' => [
                        'heading' => false,
                        'before' =>
                            \Yii::t('app', 'This is the browser for the experiments.') .
                            '{summary}' .
                            '<br/>' .
                            '<p id = "selected-items" class = "pull-right" style = "margin:-1px"><b id = "selected-count">No</b> selected items.</p>'
                        ,
                        'after' => false,
                    ],
                    'toolbar' =>  [
                        ['content' => '&nbsp;'],
                        [
                            'content' =>
                            !$renderUpdateBtn ? "" : $updateButton
                        ],
                        [
                            'content' =>
                            '&nbsp;' .
                            Html::a(
                                '<i class="glyphicon glyphicon-repeat"></i>',
                                [
                                    '/breedingProgramManager/default/index',
                                    'program' => $program,
                                ],
                                [
                                    'data-pjax' => true,
                                    'id' => 'reset-experiment-grid',
                                    'class' => 'btn btn-default',
                                    'title' => \Yii::t('app', 'Reset grid')
                                ]
                            ) .
                            '{dynagridFilter}{dynagridSort}{dynagrid}'
                        ]
                    ],
                    'floatHeader' => true,
                    'floatOverflowContainer' => true,
                    'pager' => [
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last'
                    ],
                ],
                'submitButtonOptions' => [
                    'icon' => 'glyphicon glyphicon-ok',
                ],
                'deleteButtonOptions' => [
                    'icon' => 'glyphicon glyphicon-delete',
                    'label' => 'Remove',
                ],
                'options' => [
                    'id' => $browserId
                ]
            ]);
            DynaGrid::end();

        }
        $setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
        $currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
        (new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

        // variables for change page size
        Yii::$app->view->registerJs("
            var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
            browserId = '".$browserId."',
            currentDefaultPageSize = '".$currentDefaultPageSize."'
            ;",
        \yii\web\View::POS_HEAD);

        // js file for data browsers
        $this->registerJsFile("@web/js/data-browser.js", [
            'depends' => ['app\assets\AppAsset'],
            'position'=>\yii\web\View::POS_END
        ]);

        $updateExperimentUrl = Url::to(['/breedingProgramManager/update-experiment/index', 'program'=>$program]);
        $saveExperimentSelectionUrl = Url::to(['/breedingProgramManager/update-experiment/save-experiment-selection']);

        $script = <<< JS
            $(document).ready(function() {
                refresh()

                // Adjust browser page size, if the page size was changed in another browser
                adjustBrowserPageSize();
            })

            $(document).on('ready pjax:success', function(e) {
                refresh()
            });

            $(document).on('click', '#reset-experiment-grid', function(e) {
                localStorage.removeItem('bpm_experiments_ids');
                checkSelectedParentCounter(0);
            });

            //select grid
            $(document).on('click','.experiment-grid-select',function(e){
                e.stopImmediatePropagation();

                var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
                var unset;
                var experimentDbId = $(this).prop('id');

                if($(this).prop('checked')){
                    unset = false;
                }else{
                    unset = true;
                }

                var selected = localStorage.getItem('bpm_experiments_ids');
                selected = getSelectedExperiments(selected);
                selected = (selected == null) ? {} : selected;
                if(unset) { // remove selected
                    delete selected[experimentDbId];
                    selected = (Object.keys(selected).length === 0) ? null : selected;
                } else {  // add selected
                    selected[experimentDbId] = experimentDbId;
                }
                localStorage.setItem('bpm_experiments_ids', JSON.stringify(selected));  // save selected to localStorage

                var count = getSelectedCount(selected); // update count
                checkSelectedParentCounter(count);
            });

            $(document).on('click','#update-experiments-btn',function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                // get selected
                var selected = localStorage.getItem('bpm_experiments_ids');
                selected = getSelectedExperiments(selected);
                selected = (selected == null) ? {} : selected;

                experimentIdArray = Object.keys(selected);
                experimentIdCount = getSelectedCount(selected);
                // check if selected is null
                if(experimentIdCount > 0) {
                    // pass selected ids to update experiment page
                    $.ajax({
                        type: 'post',
                        url: '$saveExperimentSelectionUrl',
                        data: {
                            experimentDbIds: JSON.stringify(experimentIdArray)
                        },
                        success: function(response) {
                            // redirect to Update Experiment Page
                            window.location = '$updateExperimentUrl';
                        },
                        error: function(a) {
                            $('.toast').css('display','none');
                            var notif = '<i class="material-icons orange-text">warning</i> There was a problem while updating experiments. Please try again.';
                            Materialize.toast(notif, 5000);
                        }
                    });
                } else {
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please select at least one(1) experiment.</span>";
                        Materialize.toast(notif, 5000);
                    }
                }
            });

            function refresh () {
                // set dynamic height for the browser
                let maxHeight = ($(window).height() - 320)
                $('.kv-grid-wrapper').css('height', maxHeight)

                //Update selected items and count
                var selected = localStorage.getItem('bpm_experiments_ids');
                selected = getSelectedExperiments(selected);
                selected = (selected == null) ? {} : selected;

                $("input:checkbox.experiment-grid-select").each(function(){
                    var experimentDbId = $(this).parent("td").parent("tr").attr('data-key');
                    if(selected.hasOwnProperty(experimentDbId)) {
                        $(this).attr('checked','checked');
                        $(this).attr('checked','checked');
                        $(this).prop('checked',true);
                        $(this).parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5');
                    }
                });

                var count = getSelectedCount(selected); // update count
                checkSelectedParentCounter(count);
            }

            //set selected parent counter
            function checkSelectedParentCounter(response){
                count = parseInt(response).toLocaleString();
                if(response == 0) {
                    count = 'No';
                }
                $('#selected-count').html(count);
            }

            // count items inside a json object which contains selected items in a browser
            function getSelectedCount(jsonObj) {
                return (jsonObj == null) ? 0 : Object.keys(jsonObj).length;
            }

            // parse json string from localStorage and return a json object
            function getSelectedExperiments(jsonStringFromLocalStorage) {
                try {
                    var experimentsObj = JSON.parse(jsonStringFromLocalStorage);
                    if (experimentsObj && typeof experimentsObj === "object") {
                        return experimentsObj;
                    }
                }
                catch (e) { }

                return null;
            }
        JS;
        $this->registerJs($script);
    ?>
</div>

