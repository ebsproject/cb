<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders browser for experiment Creation
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\dynagrid\DynaGrid;

use marekpetras\yii2ajaxboxwidget\Box;

$params = Yii::$app->request->queryParams;

// Build dropdown options
$optionsId = '';
$optionsShortName = '';
$optionsLongName = '';
$optionsDescription = '';
foreach ($breedingPipelineOptions as $bpOptions) {
    $optionsId = $optionsId . '<option value="'.$bpOptions['bpid'].'">'.$bpOptions['bpid'].'</option>';
    $optionsShortName = $optionsShortName . '<option value="'.$bpOptions['bpid'].'">'.$bpOptions['shortName'].'</option>';
    $optionsLongName = $optionsLongName . '<option value="'.$bpOptions['bpid'].'">'.$bpOptions['longName'].'</option>';
    $optionsDescription = $optionsDescription . '<option value="'.$bpOptions['bpid'].'">'.$bpOptions['description'].'</option>';
}
?>

<div class="col col-sm-12 view-entity-content">
    <?php
        echo '
        <ul class="collapsible">
            <li>
                <div class="collapsible-header active">
                    <i class="material-icons">remove_red_eye</i> ' . \Yii::t('app', "This is the browser for the selected experiments for updating. You may use this for reference. Click this tab to toggle the display.") . '
                </div>
                <div class="collapsible-body">';

                    $browserId = 'dynagrid-bpm-update-experiment-browser';

                    // All the columns that can be viewed in experiment browser
                    $columns = [
                        [
                            'attribute'=>'experimentStatus',
                            'label' => 'Status',
                            'format' => 'raw',
                            'filter' => false,
                            'value' => function($data){
                                return \Yii::$container->get('app\models\Experiment')->formatExperimentStatus($data['experimentStatus']);
                            },
                            'contentOptions' => [
                                'style'=>'min-width: 150px;'
                            ],
                        ],
                        [
                            'attribute' => 'experimentName',
                            'label' => 'Experiment',
                            'filter' => false,
                            'contentOptions' => [
                                'style'=>'min-width: 150px;'
                            ],
                        ],
                        [
                            'attribute' => 'experimentType',
                            'label' => 'Type',
                            'filter' => false,
                            'contentOptions' => [
                                'style'=>'min-width: 150px;'
                            ],
                        ],
                        [
                            'attribute' => 'stageCode',
                            'label' => 'Stage',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'experimentYear',
                            'label' => 'Year',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'seasonName',
                            'label' => 'Season',
                            'filter' => false,
                        ]
                    ];

                    $gridColumns = array_merge($columns);

                    $dynagrid = DynaGrid::begin([
                        'columns' => $gridColumns,
                        'theme'=>'simple-default',
                        'showPersonalize'=>false,
                        'storage' => DynaGrid::TYPE_SESSION,
                        'showFilter' => false,
                        'showSort' => false,
                        'allowFilterSetting' => false,
                        'allowSortSetting' => false,
                        'gridOptions'=>[
                            'id' => 'grid-experiment-list',
                            'dataProvider'=>$dataProvider,
                            'showPageSummary'=>false,
                            'pjax'=>true,
                            'pjaxSettings' => [
                                'neverTimeout' => true,
                                'options' => ['id' =>'grid-experiment-list'],
                                'beforeGrid' => '',
                                'afterGrid' => ''
                            ],
                            'responsiveWrap'=>false,
                            'panel'=>[
                                'heading' => '<h3>'.
                                    Yii::t('app', 'Experiments'),
                                'before' => false,
                                'after' => false,
                            ],
                            'toolbar' => [],
                            'floatHeader' =>true,
                            'floatOverflowContainer'=> true,
                            'pager' => [
                                'firstPageLabel' => 'First',
                                'lastPageLabel' => 'Last'
                            ],
                        ],
                        'submitButtonOptions' => [
                            'icon' => 'glyphicon glyphicon-ok',
                        ],
                        'deleteButtonOptions' => [
                            'icon' => 'glyphicon glyphicon-remove',
                            'label' => Yii::t('app', 'Remove'),
                        ],
                        'options'=>['id'=>$browserId]
                    ]);
                    DynaGrid::end();

                    echo '
                </div>
            </li>
        </ul>';
    ?>
</div>


<div id="update-experiment-fields-container-id" class="col-md-6"  style="padding-right:20px;">
    <div class="col-md-12" style="padding-right:0px;">
        <div class="panel panel-default update-experiment-fields-div" style=" min-height:100px;">
            <div id="update-experiment-fields-id" class="row" style="margin-left:5px; margin-top:5px;padding-top:5px; padding-right:25px; padding-left:10px;">
            <div class="collapsible-header active" style="margin-bottom:10px; cursor:auto">
                <?= \Yii::t('app', 'Pipeline') ?>
                </div>
                <div style="margin-left:20px">
                    <p>Specify pipeline information to be applied to the selected experiments.</p>
                    <?php
                        echo '<div class="col col-md-12" style="padding:0px"><div class="col col-md-6" style="padding:0px"><label class="control-label" title="title1">BPID</label></div>';
                        echo '
                        <select id="bp_id-filter" class="select2-input-validate bp-select2" style="width: 100%; height: 1px; visibility: hidden;">
                            '.$optionsId.'
                        </select>&nbsp;';

                        echo '<div class="col col-md-12" style="padding:0px"><div class="col col-md-6" style="padding:0px"><label class="control-label" title="Breeding Pipeline Short Name">Short Name</label></div>';
                        echo '
                        <select id="bp_shortname-filter" class="select2-input-validate bp-select2" style="width: 100%; height: 1px; visibility: hidden;">
                            '.$optionsShortName.'
                        </select>&nbsp;';

                        echo '<div class="col col-md-12" style="padding:0px"><div class="col col-md-6" style="padding:0px"><label class="control-label" title="Breeding Pipeline Long Name">Long Name</label></div>';
                        echo '
                        <select id="bp_longname-filter" class="select2-input-validate bp-select2" style="width: 100%; height: 1px; visibility: hidden;">
                            '.$optionsLongName.'
                        </select>&nbsp;';

                        echo '
                        <select id="bp_description-filter" class="select2-input-validate bp-select2" style="width: 100%; height: 1px; visibility: hidden;">
                            '.$optionsDescription.'
                        </select>&nbsp;';
                    ?>
                </div>
                <?= Html::Button(\Yii::t('app', 'Save'),[
                        'class' => 'waves-effect waves-light btn btn-secondary pull-right teal hm-tooltipped',
                        'style' => 'margin-right:5px; margin-bottom:20px;',
                        'id' => 'bpm-update-save-btn',
                        'disabled' => false,
                        'data-pjax'=>true,
                        'data-position' => 'bottom',
                        'data-tooltip' => \Yii::t('app', 'Save changes to selected experiments'),
                    ]).
                    Html::Button(\Yii::t('app', 'Cancel'),[
                        'class' => 'waves-effect waves-light btn btn-secondary pull-right teal hm-tooltipped',
                        'style' => 'margin-right:5px; margin-bottom:20px;',
                        'id' => 'bpm-update-cancel-btn',
                        'disabled' => false,
                        'data-pjax'=>true,
                        'data-position' => 'bottom',
                        'data-tooltip' => \Yii::t('app', 'Cancel'),
                    ]);
                ?>
            </div>
        </div>

    </div>
</div>

<?php
// MODALS
?>

<?php
$updateTransformedDataUrl = Url::to(['/breedingProgramManager/update-experiment/update-transformed-data']);
$updateExperimentPipelineUrl = Url::to(['/breedingProgramManager/update-experiment/update-experiment-pipeline']);

$experimentDbIds = json_encode($experimentDbIds);

$this->registerJs(<<<JS
    var experimentDbIds = $experimentDbIds

    $(document).ready(function() {
        $('#bp_id-filter').select2()
        $('#bp_shortname-filter').select2()
        $('#bp_longname-filter').select2()
        $('#bp_description-filter').select2().next().css('display','none');
    });

    // When one dropdown on pipeline gets updated, then the other dropdown on pipeline also get updated
    $('.bp-select2').on('select2:select', function(e) {
        var selectedId = $(this).select2('data')[0].id

        $('.bp-select2').val(selectedId).trigger('change')
    });

    $('#bpm-update-cancel-btn').on('click', function (e) {
        // Redirect back to the Breeding Program Manager page
        window.location = '/index.php/breedingProgramManager/default/index?program=$program'
    });

    $('#bpm-update-save-btn').on('click', function (e) {
        var bpid = $('#bp_id-filter').select2('data')[0].text
        var bpshortname = $('#bp_shortname-filter').select2('data')[0].text
        var bplongname = $('#bp_longname-filter').select2('data')[0].text
        var description = $('#bp_description-filter').select2('data')[0].text

        if(bpid == null || bpshortname == null || bplongname == null || description == null) {
            var notif = '<i class="material-icons orange-text">warning</i> There was a problem while updating experiments. Please try again.'
            Materialize.toast(notif, 5000)
            true
        }

        var bpData = {
            bpId : bpid,
            bpShortName : bpshortname,
            bpLongName : bplongname,
            description : description
        }

        $.ajax({
            type: 'POST',
            url: '$updateExperimentPipelineUrl',
            data: {
                bpData: bpData,
                experimentDbIds: experimentDbIds
            },
            async: true,
            success: function(response) {
                if(response == false) {
                    var notif = '<i class="material-icons orange-text">warning</i> There was a problem while updating experiments. Please try again.';
                    Materialize.toast(notif, 5000);
                    return
                }

                var notif = '<i class="material-icons green-text">check_circle</i>Successfully updated experiments!';
                Materialize.toast(notif, 5000);

                // Redirect back to the Breeding Program Manager page
                window.location = '/index.php/breedingProgramManager/default/index?program=$program'
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while updating experiments. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });
    });
JS
);

/**
 * View file for experiment creation browser
 */
Yii::$app->view->registerCss('
    body{
        overflow-x: hidden;
    }
');
?>
