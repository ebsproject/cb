<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<?php
/**
 * Render experiment view page for
 * Breeding Program Manager
 */

// Import classes
use yii\helpers\Url;

// Register CSS
$this->registerCss('
    body {
        overflow-x: hidden;
    }
    .custom-row {
        margin: 0;
        padding: 0;
    }
    .custom-ul {
        list-style: none;
        padding: 0;
        margin: 0;
    }
    .custom-li {
        display: inline-block;
        width: 33%;
        box-sizing: border-box;
        padding: 5px;
    }
    .custom-label {
        font-weight: bold;
        color: black;
    }
');
?>

<?php
$experimentsTabHTML = '
    <li class="tab col bpm-tool-tab" id="experiments-tab">
        <a class="experiments-tab a-tab active left-align" href="">' . \Yii::t('app', 'Basic') . '</a>
    </li>';
?>

<div class="row">
    <h3 class="">
        <a href="<?php echo Url::toRoute(['/breedingProgramManager/default/index', "program" => $program]) ?>">
            <?php echo \Yii::t('app', 'Breeding Program Manager'); ?>
        </a>
        <small> &raquo;
            <a href="<?php echo Url::toRoute(['/breedingProgramManager/default/index', "program" => $program]) ?>">
                <?php echo \Yii::t('app', 'Experiment'); ?>
            </a> &raquo;
            <a href="<?php echo Url::toRoute(['/breedingProgramManager/default/index', "program" => $program, 'Experiments[experimentName]' => $experimentName]) ?>">
                <?php echo htmlspecialchars($experimentInfo['data'][0]['experimentName']); ?>
            </a>
        </small>
    </h3>
    <ul id="tabs" class="tabs">
        <?php
            echo $experimentsTabHTML;
        ?>
    </ul>
</div>

<div class="row custom-row">
    <ul class="custom-ul">
        <?php 
            $count = 0;
            foreach ($fieldNames as $fieldName) {
                $attribute = $fieldName['attribute'];
                $value = isset($mappedData[$attribute]) ? $mappedData[$attribute] : 'N/A';
                
                if ($count % 3 == 0) {
                    if ($count > 0) {
                        echo '</ul></div><div class="row custom-row"><ul class="custom-ul">';
                    } else {
                        echo '<div class="row custom-row"><ul class="custom-ul">';
                    }
                }
                ?>
                <li class="custom-li">
                    <div>
                        <b>
                            <?php
                                // Format attribute
                                $formattedAttribute = preg_replace('/(?<!^)(?<!\s)([A-Z])/', ' $1', $attribute);

                                // Convert attribute to uppercase if it contains ID
                                if (preg_match('/\b(Id|id|ID)\b/', $formattedAttribute)) {
                                    $formattedAttribute = strtoupper($formattedAttribute);
                                }

                                echo \Yii::t('app', $formattedAttribute);
                            ?>
                        </b>
                    </div>
                    <div>
                        <span><?php echo htmlspecialchars($value); ?></span>
                    </div>
                </li>
                <?php 
                $count++; 
            }
        ?>
    </ul>
</div>