<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\breedingProgramManager\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use \app\models\Config;
use \app\models\BaseModel;
use \app\models\UserDashboardConfig;

/**
 * Model class for Experiments browser
 * in Breeding Program Manager
 */
class Experiments extends BaseModel
{
    public $bpId;
    public $bpShortName;
    public $productDevelopmentStage;
    public $experimentStatus;
    public $experimentCode;
    public $experimentName;
    public $experimentType;
    public $stageCode;
    public $experimentYear;
    public $seasonCode;
    public $experimentDesignType;
    public $steward;

    /**
     * Set dependencies
     * This overrides construct
     */
    public function __construct (protected UserDashboardConfig $userDashboardConfig, $config = [])
    { parent::__construct($config); }

    /**
    * {@inheritdoc}
    */
    public function rules(){
        return [
            [[
                'bpId',
                'bpShortName',
                'productDevelopmentStage',
                'experimentStatus',
                'experimentCode',
                'experimentName',
                'experimentType',
                'stageCode',
                'experimentYear',
                'seasonCode',
                'experimentDesignType',
                'steward'
            ], 'string']
        ];
    }
    
    /**
     * Retrieve experiment browser columns 
     * from configuration
     * 
     * @return array column headers
     */
    public function getBrowserColsFromConfig() {
        $headers = [];
        $configModel = new Config();
        $program = Yii::$app->userprogram->get('abbrev');
        $modifier = "_$program";
        $personModel = Yii::$container->get('app\models\Person');
        $browserSettings = $configModel->getConfigByAbbrev("EXPERIMENTS_BROWSER_PROGRAM$modifier");
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);

        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $browserSettings = $configModel->getconfigByAbbrev('EXPERIMENTS_BROWSER_ROLE_COLLABORATOR');
        } else if (isset($program) && !empty($program) && !empty($browserSettings)) {
            $browserSettings;
        } else { 
            $browserSettings = $configModel->getconfigByAbbrev('EXPERIMENTS_BROWSER_GLOBAL');
        }

        if(isset($browserSettings['Values'])) {
            $configValue = $browserSettings['Values'];

            foreach ($configValue as $value) {
                $headers[] = $value['variable_abbrev'];
            }
        }
        
        return [
            'headers' => $headers,
        ];
    }

    /**
     * Retrieve dataprovider for bpm experiment browser
     *
     * @param array $params list of search parameters
     * @param array $attributes list of attributes to be retrieved from API 
     * 
     * @return array experiment dataprovider
     */
    public function search ($params, $attributes = []) {
        $filters = [];
        $dataProviderId = 'dynagrid-bpm-experiments';

        $paramLimit = '';
        $paramPage = '';
        $currPage = 1;
        $paramSort = '';
        $sortParams = '';

        if(
            isset($_GET["$dataProviderId-sort"]) &&
            !empty($_GET["$dataProviderId-sort"])
        ) {
            $paramSort = 'sort=';
            $sortParams = $_GET["$dataProviderId-sort"];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        } else { // set default order
            $paramSort = 'sort=experimentCode';
        }

        // get filters
        $isOwned = (isset($params['Experiments']['owned']) && $params['Experiments']['owned'] == 'true') ? '&isOwned' : '';
        unset($params['Experiments']['owned']);
        unset($params['Experiments']['siteDbId']);

        if (isset($params['Experiments'])) {
            foreach ($params['Experiments'] as $key => $value) {
                $val = trim($value);

                if ($val != '') {
                    $filters[$key] = (str_contains($val, '%') || str_contains($val, 'equals')) ? $val : "equals $val";
                }
            }
        }

        $filters = empty($filters) ? null : $filters;

        if (!empty($attributes)) {
            $filters['fields'] = $this->generateFields(array_unique([ ...$attributes,
                'experimentDbId', 'experimentStatus', 'experimentCode', 'experimentName','experimentType', 'experimentYear',
                'programDbId', 'stageDbId', 'stageCode', 'seasonDbId', 'seasonCode', 'experimentDesignType', 'steward', 'bpId', 'bpShortName'
            ]));
        }

        // get current page
        if (isset($_GET["$dataProviderId-page"])) {
            $currPage = intval($_GET["$dataProviderId-page"]);
            $paramPage = "&page=$currPage";
        }

        // Get page size from the preferences
        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $pagination = $paramLimit . $paramPage;

        $paramSortOriginal = $paramSort;

        if(!isset($params['invalidFilter'])) {
            // check if page is out of range
            $totalPagesArr = Yii::$app->api->getParsedResponse(
                'POST',
                "experiments-search?$pagination&$paramSort&$isOwned",
                json_encode($filters)
            );

            // if out of range, redirect to page 1
            if ($currPage != 1 && count($totalPagesArr['data']) == 0) {
                $pagination = "$paramLimit&page=1";
                $_GET["$dataProviderId-page"] = 1; // return browser to page 1
            }

            $output = Yii::$app->api->getParsedResponse(
                'POST',
                "experiments-search?$pagination&$paramSort&$isOwned",
                json_encode($filters)
            );
        } else {
            $output = [
                'data' => [],
                'totalCount' => 0
            ];
        }

        $dataProvider = new ArrayDataProvider([
            'id' => $dataProviderId,
            'allModels' => $output['data'],
            'key' => 'experimentDbId',
            'restified' => true,
            'totalCount' => $output['totalCount'],
            'sort' => [
                'attributes' => [
                    'bpId', 'bpShortName', 'productDevelopmentStage',
                    'experimentDbId', 'experimentStatus', 'experimentCode', 'experimentName','experimentType', 
                    'stageCode', 'experimentYear', 'seasonCode', 'experimentDesignType', 'steward'
                ],
                'defaultOrder' => [
                    'experimentDbId' => SORT_ASC
                ]
            ],
        ]);

        $this->load($params['params']);

        return [
            $dataProvider,
            $output['totalCount'],
            $paramSortOriginal,
            $filters,
        ];
    }

    /**
     * Generate string of fields
     *
     * @param array $attributes list of specified attributes
     * 
     * @return string generated string of fields
     */
    public function generateFields ($attributes)
    {
        $attributesMap = [
            'experimentDbId' => 'experiment.id AS experimentDbId',
            'experimentStatus' => 'experiment.experiment_status AS experimentStatus',
            'experimentCode' => 'experiment.experiment_code AS experimentCode',
            'experimentName' => 'experiment.experiment_name AS experimentName',
            'experimentType' => 'experiment.experiment_type AS experimentType',
            'programDbId' => 'program.id AS programDbId',
            'stageCode' => 'stage.stage_code AS stageCode',
            'stageDbId' => 'stage.id AS stageDbId',
            'experimentYear' => 'experiment.experiment_year AS experimentYear',
            'seasonCode' => 'season.season_code AS seasonCode',
            'seasonDbId' => 'season.id AS seasonDbId',
            'experimentDesignType' => 'experiment.experiment_design_type AS experimentDesignType',
            'steward' => 'person.person_name AS experimentSteward',
            'bpId' => 'pipeline.pipeline_code AS bpId',
            'bpShortName' => 'pipeline.pipeline_name AS bpShortName'
        ];

        $fields = '';

        foreach ($attributes as $value) {
            if (!array_key_exists($value, $attributesMap))
                continue;

            $fields .= "{$attributesMap[$value]} |";
        }

        $fields = rtrim($fields, '|');

        return $fields;
    }

    /**
     * Retrieve experiment view fields
     * from configuration
     * 
     * @return array fields
     */

    public function getViewFieldsFromConfig() {
        $fields = [];
        $configModel = new Config();
        $program = Yii::$app->userprogram->get('abbrev');
        $modifier = "_$program";
        $personModel = Yii::$container->get('app\models\Person');
        $browserSettings = $configModel->getConfigByAbbrev("EXPERIMENTS_VIEW_PROGRAM$modifier");
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);

        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $browserSettings = $configModel->getconfigByAbbrev('EXPERIMENTS_VIEW_ROLE_COLLABORATOR');
        } else if (isset($program) && !empty($program) && !empty($browserSettings)) {
            $browserSettings;
        } else { 
            $browserSettings = $configModel->getconfigByAbbrev('EXPERIMENTS_VIEW_GLOBAL');
        }

        if(isset($browserSettings['Values'])) {
            $configValue = $browserSettings['Values'];

            foreach ($configValue as $value) {
                $fields[] = $value['variable_abbrev'];
            }
        }
        
        return [
            'fields' => $fields
        ];
    }
}
