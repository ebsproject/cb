<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* Renders browser and manage filters page
*/
use yii\helpers\Url;
use kartik\select2\Select2;

?>

<div class="container filter-action-modal-container">
    <div class="row filter-modal-row">
        <div class="col s4">
            <div class="row">
                <h6>Filter Name</h6>
                <label>Select filters for this data browser.</label>
                <!-- Select2 widget -->
                <?php
                    echo Select2::widget([
                        'name' => 'filter_select',
                        'id' => 'filter_select',
                        'data' => [],
                        'options' => [
                            'placeholder' => 'Select Filter Configuration'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => new yii\web\JsExpression('$("#filter-action-modal")')
                        ]
                    ]);
                ?>
            </div>
            <!-- Buttons -->
            <div class="row btn-container">
                <button id="save-filter-btn" class="btn btn-default" style="display: none;"><span>Save</span> <i class="material-icons">save</i></button>
                <button id="delete-filter-btn" class="btn btn-danger" style="display: none;" data-toggle="modal" data-target="#deleteFilterModal"><span>Delete</span> <i class="material-icons">delete_forever</i></button>
            </div>
            <hr>
            <div class="row">
                <h6 class="guide-header text-muted">Advanced Filters Guide</h6>
                <div class="container filter-action-modal-container filter-view-container" style="height: 250px; position: relative; overflow-y: scroll">
                    <ul class="guide-list text-muted">
                        <li><span class="guide-condition">Equals</span>: <span class="guide-description">Searches for exact values to appear on the results.</span></li>
                        <li><span class="guide-condition">Not equals</span>: <span class="guide-description">Searches for exact values to be excluded on the results.</span></li>
                        <li><span class="guide-condition">Like</span>: <span class="guide-description">A case-sensitive filter where the value may contain a wildcard "%" to search for results.</span></li>
                        <li><span class="guide-condition">Not like</span>: <span class="guide-description">A case-sensitive filter where the results matching the value (with or without a wildcard "%") would be excluded on the results.</span></li>
                        <li><span class="guide-condition">Contains</span>: <span class="guide-description">A case-insensitive filter which searches for results that contain the value.</span></li>
                        <li><span class="guide-condition">Less than</span>: <span class="guide-description">Searches for values less than the input.</span></li>
                        <li><span class="guide-condition">Less than or equal to</span>: <span class="guide-description">Searches for values less than or equal to the input.</span></li>
                        <li><span class="guide-condition">Greater than</span>: <span class="guide-description">Searches for values greater than the input.</span></li>
                        <li><span class="guide-condition">Greater than or equal to</span>: <span class="guide-description">Searches for values greater than or equal to the input.</span></li>
                        <li><span class="guide-condition">Range (Number)</span>: <span class="guide-description">Searches for the results from the first number to the second number. Individual numbers may also be entered.</span></li>
                        <li><span class="guide-condition">Range (Date)</span>: <span class="guide-description">Searches for the results which are dated from the first date to the second date. Individual dates may also be entered.</span></li>
                        <li><span class="guide-condition">Is null</span>: <span class="guide-description">Searches for the results for null values.</span></li>
                        <li><span class="guide-condition">Is not null</span>: <span class="guide-description">Searches for the results for values that are not null.</span></li>
                        <li><span class="guide-condition">Additional Note:</span></li>
                        <li><span class="guide-description">&nbsp; Please follow the "N-N" format when entering number ranges.</span></li>
                        <li><span class="guide-description">&nbsp; Please follow the "YYYY-MM-DD" format when entering dates.</span></li>
                        <li><span class="guide-description">&nbsp; Please follow the "YYYY-MM-DD>YYYY-MM-DD" format when entering date ranges.</span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col s8">
            <div class="row filter-modal-row">
                <!-- Filter options container -->
                <div>
                    <h6>Filter options</h6>
                    <label>Displays the selected filter configuration.</label>
                    <div class="container filter-action-modal-container filter-view-container" style="background-color: white; height: 140px; position: relative; overflow-y: scroll">
                        <div style="max-height: 100%;">
                            <ul id="filter-view-list">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row filter-modal-row">
                <!-- Advanced Filters -->
                <div>
                    <h6>Advanced Filters</h6>
                    <label>Modify the operator and add condition for every column. Enclose strings in quotes ("") and use commas (,) to filter for multiple values.</label>
                    <div class="advanced-filter-container">
                        <div>
                            <!-- Select2 widget for Operator -->
                            <span>Operator: </span>
                            <?php
                                echo Select2::widget([
                                    'name' => 'advanced_filter_operator_select',
                                    'id' => 'advanced_filter_operator_select',
                                    'data' => ['AND', 'OR']
                                ]);
                            ?>
                        </div>
                        <div class="container filter-action-modal-container filter-view-container" style="background-color: white; height: 140px; position: relative; overflow-y: scroll">
                            <div style="max-height: 100%;">
                                <ul id="advanced-filter-condition-list">
                                </ul>
                            </div>
                        </div>
                        <button id="advanced-filter-add-condition-btn" class="btn btn-default"><span>Add condition</span></button>
                        <button id="advanced-filter-clear-all-condition-btn" class="btn btn-default" disabled><span>Clear all</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade custom-modal" id="deleteFilterModal" tabindex="-1" role="dialog" aria-labelledby="deleteFilterModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteFilterModalLabel">Delete <span id="deleteFilterModalTitle">Filter</span>?</h5>
            </div>
            <div class="modal-body">
                <p><span id="deleteFilterModalContent">Filter</span> will be deleted from your saved filter configurations. This action cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <a class="close-cancel-dialog-btn">Cancel</a> &nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-danger" id="delete-filter-final-btn">Delete</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade custom-modal" id="saveFilterModal" tabindex="-1" role="dialog" aria-labelledby="saveFilterModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="saveFilterModalLabel">Save filter</h5>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                    <input type="text" class="form-control" id="new-filter-name-input" placeholder="Enter name of filter">
                </form>
            </div>
            <div class="modal-footer">
                <a class="close-cancel-dialog-btn">Cancel</a> &nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-primary" id="save-filter-final-btn" disabled>Save</button>
            </div>
        </div>
    </div>
</div>


<?php
// Get the dataBrowserId and columnNames from the data browser
$dataBrowserId = json_encode($dataBrowserId);
$columnNames = json_encode($columnNames);
$columnNameAndDatatypes = json_encode($columnNameAndDatatypes);

$getFiltersUrl = Url::to(['/userFilter/data-browser-filter/get-filters']);
$saveFiltersUrl = Url::to(['/userFilter/data-browser-filter/save-filters']);
$deleteFiltersUrl = Url::to(['/userFilter/data-browser-filter/delete-filters']);


$this->registerJs(<<<JS
var savedUserFilterConfigs = [];
var currentUserFilterConfig = {};
var currentIsUnsaved = false;
var currentSelectedIndex = -1;
var columnNames = JSON.parse('$columnNames');
var columnNameAndDatatypes = JSON.parse('$columnNameAndDatatypes');
var advancedFilterSelection = {};
var advancedFilterSelected = {};
var advancedFilterConditionCurrentCount = 0;
var currentlySavingFilter = false;
var currentlyDeletingFilter = false;
var sortsInUrl = '';

/**
* Retrieves the current filter configurations based on the user's data browser.
*/
function retrieveCurrentConfig() {
    // Use url to get value and condition of each filter column
    url = window.location.search;
    // catch case where a space (' ') is replaced by a '+' by the browser
    url = url.replace(/\+/g, '%20');

    browserModel = '$browserModel';

    // Get sortsInUrl
    var sortsUrl = url.split('&');
    for(var val of sortsUrl) {
        if(val.indexOf('sort=') !== -1) {
            sortsInUrl += '&'+val;
        }
    }
    sortsInUrl = sortsInUrl.slice(1);

    url = decodeURIComponent(url);
    url = url.split('&');
    for(var val of url) {
        if(val.indexOf(browserModel) !== -1) {
            // split to browserModel+'['+columnName']' and value
            val = val.split('=');
            var modelAndColumnName = val[0];
            var value = val[1];
            // find currentColumn
            var currentColumn = '';
            var columnNameRegex = /\[(\w+)\]/g;
            var matches = modelAndColumnName.matchAll(columnNameRegex);
            for(var match of matches) {
                currentColumn = match[1];
                var isColNameFound = false;
                for(var colName of columnNames) {   // check if currentColumn is indeed a column name
                    if(currentColumn.indexOf(colName) !== -1) {
                        isColNameFound = true;
                    }
                }
                if(isColNameFound) {
                    break;
                }
            }

            if(modelAndColumnName.indexOf('__advanced_filters__') !== -1) {
                var currentAdvancedFilters = {};
                if(currentUserFilterConfig.hasOwnProperty('__advanced_filters__')) {
                    currentAdvancedFilters = currentUserFilterConfig['__advanced_filters__'];
                }
                if(currentColumn === '__operator__') {
                    currentAdvancedFilters['__operator__'] = value;
                } else {
                    currentAdvancedFilters[currentColumn] = value;
                }
                currentUserFilterConfig['__advanced_filters__'] = currentAdvancedFilters;
            } else {
                // do not add if value is empty
                if(value === '') {
                    continue;
                }
                var valueArr = []
                if(currentUserFilterConfig.hasOwnProperty(currentColumn)) {
                    valueArr = currentUserFilterConfig[currentColumn];
                }
                valueArr.push(value);
                currentUserFilterConfig[currentColumn] = valueArr;
            }
        }
    }

    // Remove values of columns with 'is null' and 'is not null' conditions
    // and initialize column value as empty array if it does not exist currentUserFilterConfig
    if(currentUserFilterConfig.hasOwnProperty('__advanced_filters__')) {
        var aFilters = currentUserFilterConfig['__advanced_filters__'];
        for(var colName in aFilters) {
            if(colName == '__operator__') {
                continue;
            }
            var condition = aFilters[colName];
            if((condition == 'is null' || condition == 'is not null')
              && currentUserFilterConfig.hasOwnProperty(colName)) {
                delete currentUserFilterConfig[colName];
            } else if((condition != 'is null' || condition != 'is not null')
              && !currentUserFilterConfig.hasOwnProperty(colName)) {
                currentUserFilterConfig[colName] = [];
            }
        }

        // if __advanced_filters__ become empty object after cleaning, delete
        if($.isEmptyObject(currentUserFilterConfig['__advanced_filters__'])) {
            delete currentUserFilterConfig['__advanced_filters__'];
        }
    }
}

/**
* Sets the Select2 widget's options to the filterNames parameter
*
* @param filterNames array containing the names of saved (and unsaved) filters
*/
function setFilterSelection(filterNames) {
    // Clear Select2 widget
    $('#filter_select').empty();

    // Populate Select2 by appending filterNames
    for(var index in filterNames) {
        $('#filter_select').append(new Option(filterNames[index], index));
    }

    // Apply Changes
    $('#filter_select').val('').trigger('change');
    currentSelectedIndex = $('#filter_select').val();
}

/**
* Sets the filter view based on the filterConfig passed.
*
* @param json filterConfig The object containing the data to display on the filter-view-list
*/
function setFilterView(filterConfig) {
    // Clear Filter View
    $('#filter-view-list').empty();

    // Check if filterConfig is an empty json object
    if($.isEmptyObject(filterConfig)) {
        $("#filter-view-list").append('<li>No filter configuration selected</li>');
    } else {
        // Add filter config to widget
        var filterKeys = Object.keys(filterConfig);
        var advancedFiltersAreSet = false;
        var advancedFilterConditions = {};
        var advancedFilterKeys = [];
        if(filterConfig.hasOwnProperty('__advanced_filters__')) {
            advancedFilterKeys = Object.keys(filterConfig['__advanced_filters__']);
            filterKeys = filterKeys.filter(e => e !== '__advanced_filters__');
            advancedFilterKeys = advancedFilterKeys.filter(e => e !== '__operator__');
            advancedFiltersAreSet = true;
            advancedFilterConditions = filterConfig['__advanced_filters__'];
        }
        var numOfKeys = [...new Set([...filterKeys, ...advancedFilterKeys])].length;

        for(var colName of columnNames) {
            if(!filterConfig.hasOwnProperty(colName) && !advancedFilterConditions.hasOwnProperty(colName)) {
                continue;
            }
            columnKey = colName.endsWith('Abbrev')? colName.slice(0, -6).toUpperCase(): colName.toUpperCase();

            var condition = 'equals';
            if(advancedFiltersAreSet && advancedFilterConditions.hasOwnProperty(colName)) {
                condition = advancedFilterConditions[colName];
            }
            if(condition.indexOf('equals') != -1 && filterConfig[colName].length > 1) {
                condition = condition.replace('equals', 'in');
            }
            var columnCondition = '<span class="column-condition">' + condition + '</span> ';
            var columnValue = '';

            if(condition != 'is null' && condition != 'is not null') {
                for(var value of filterConfig[colName]) {
                    columnValue += '<span class="column-value">' + value + '</span> ';
                }
                columnCondition += ' : ';
            }
            $("#filter-view-list").append('<li>'+
                columnKey + columnCondition + columnValue+
            '</li>');
            if(numOfKeys > 1) {
                var operator = 'AND';
                if(advancedFiltersAreSet && advancedFilterConditions.hasOwnProperty('__operator__')) {
                    operator = advancedFilterConditions['__operator__'];
                }
                $("#filter-view-list").append('<li>&nbsp&nbsp&nbsp<span class="column-operator">' + operator + '</span></li>');
            }
            numOfKeys -= 1;
        }
    }
}

/**
* Build advancedFilterSelection based on the datatype of each column.
*/
function buildAdvancedFilterSelection() {
    for(var colName of columnNames) {
        if(columnNameAndDatatypes.hasOwnProperty(colName)) {    // Use defined datatype
            advancedFilterSelection[colName] = columnNameAndDatatypes[colName];
        } else {    // Default to String
            advancedFilterSelection[colName] = 'String';
        }
    }
}

/**
* Initialize each column in advancedFilterSelected to false.
*/
function buildAdvancedFilterSelected() {
    for(var col of columnNames) {
        advancedFilterSelected[col] = false;
    }
}

/**
* Enables/disables the options of all advanced filter column name select widget.
*/
function updateDisabledAdvacedFilterNamesInSelect() {
    $('#advanced-filter-condition-list li').each(function(listIndex, listElement) {
        var selectElement = $(listElement).find('.advanced-filter-column-name-select');
        for(var colIndex in columnNames) {
            if(advancedFilterSelected[columnNames[colIndex]] == true) {
                selectElement.children().eq(colIndex).prop('disabled', true);
            } else {
                selectElement.children().eq(colIndex).prop('disabled', false);
            }
        }
    });
}

/**
* Adds a row to the advanced filter condition list.
*
* @param advancedFilterCondition object containing the properties (isNew, columnName, condition, value, element) of the advanced filter
*/
function addAdvancedFilterCondition(advancedFilterCondition) {
    advancedFilterConditionCurrentCount += 1;

    $('#advanced-filter-condition-list'). append('<li>'
        +'<div class="container filter-action-modal-container filter-view-container">'
            +'<div class="row filter-modal-row">'
                +'<div class="col s1">'
                    +'<button class="btn btn-danger remove-advanced-filter-condition-button"><i class="material-icons">remove</i></button>'
                +'</div>'
                +'<div class="col s3">'
                    +'<select class="advanced-filter-column-name-select">'
                    +'</select>'
                +'</div>'
            +'</div>'
        +'</div>'
    +'</li>');

    // Automatically selects the last element because it is newly appended
    var listElement = $('#advanced-filter-condition-list');
    var listItemIndex = listElement.children().length - 1;
    var selectElement = listElement.children().eq(listItemIndex).find('.advanced-filter-column-name-select');

    var selectData = [];
    for(var colName of columnNames) {
        selectData.push({
            id: colName,
            text: colName.endsWith('Abbrev')? colName.slice(0, -6).toUpperCase(): colName.toUpperCase()
        });
    }

    selectElement.select2({
        data: selectData,
        dropdownParent: $('#filter-action-modal')
    });

    if(advancedFilterCondition['isNew'] == true) {
        // Auto select first non-disabled option
        for(var colIndex in columnNames) {
            if(advancedFilterSelected[columnNames[colIndex]] == false) {
                selectElement.val(columnNames[colIndex]).trigger('change');
                advancedFilterSelected[columnNames[colIndex]] = true;
                break;
            }
        }

        // Disable other (already) selected options
        updateDisabledAdvacedFilterNamesInSelect();

        // Then add possible conditions (new or from saved filter configs)
        addAdvancedFilterConditionConditionSelect({
            'isNew': true,
            'element': selectElement
        });

    } else {
        var colName = advancedFilterCondition['columnName'];
        var colCondition = advancedFilterCondition['columnCondition'];
        var colValue = advancedFilterCondition['columnValue'];
        selectElement.val(colName).trigger('change');
        advancedFilterSelected[colName] = true;

        // Disable other (already) selected options
        updateDisabledAdvacedFilterNamesInSelect();

        // Then add possible conditions (new or from saved filter configs)
        addAdvancedFilterConditionConditionSelect({
            'isNew': false,
            'element': selectElement,
            'columnCondition': colCondition,
            'columnValue': colValue
        });
    }

    // Disable the add button if all column names are selected.
    if(advancedFilterConditionCurrentCount == columnNames.length) {
        $(advancedFilterCondition['element']).prop('disabled', true);
    }
    // enable clear all button
    $('#advanced-filter-clear-all-condition-btn').prop('disabled', false);

}

/**
* Adds an advanced filter condition select element to the advanced filter row.
*
* @param advancedFilterConditionColumnName object containing the properties (isNew, columnCondition, columnValue, element) of the advanced filter column name
*/
function addAdvancedFilterConditionConditionSelect(advancedFilterConditionColumnName) {
    var columnNameSelectElement = advancedFilterConditionColumnName['element'];

    // Remove condition and values to the right if they exist
    if($(columnNameSelectElement).parent().next().length) {
        $(columnNameSelectElement).parent().nextAll().remove();
    }

    var chosenColumnName = $(columnNameSelectElement).select2('data')[0].id;

    $(columnNameSelectElement).parents().eq(1).append('<div class="col s3">'
        +'<select class="advanced-filter-column-condition-select">'
        +'</select>'
    +'</div>');
    var listElement = $('#advanced-filter-condition-list');
    var listItemIndex = columnNameSelectElement.parents().eq(3).index();
    var columnConditionSelectElement = listElement.children().eq(listItemIndex).find('.advanced-filter-column-condition-select');

    var columnConditionChoices = [];
    // Use the currently selected column name to determine the datatype and add the respective condition select
    if(advancedFilterSelection[chosenColumnName] == 'String') {  // Datatype is String
        columnConditionChoices = [
            'equals',
            'not equals',
            'like',
            'not like',
            'is null',
            'is not null',
            'contains'
        ];
    } else if(advancedFilterSelection[chosenColumnName] == 'Number') {  // Datatype is Integer
        columnConditionChoices = [
            'equals',
            'not equals',
            'less than',
            'less than or equal to',
            'greater than',
            'greater than or equal to',
            'range',
            'is null',
            'is not null'
        ];
    } else {    // Datatype is Timestamp
        columnConditionChoices = [
            'equals',
            'not equals',
            'range'
        ];
    }

    var selectData = [];
    for(var colCondition of columnConditionChoices) {
        selectData.push({
            id: colCondition,
            text: colCondition.endsWith('equals')? colCondition+'/in': colCondition
        });
    }

    columnConditionSelectElement.select2({
        data: selectData,
        dropdownParent: $('#filter-action-modal')
    });
    columnConditionSelectElement.next().css('width', '100%');

    if(advancedFilterConditionColumnName['isNew'] == true) {
        if(currentUserFilterConfig.hasOwnProperty('__advanced_filters__')
          && currentUserFilterConfig['__advanced_filters__'].hasOwnProperty(chosenColumnName)) {
            // select the current condition according to currentUserFilterConfig
            columnConditionSelectElement.val(currentUserFilterConfig['__advanced_filters__'][chosenColumnName]).trigger('change');
        }

        addAdvancedFilterConditionValueField({
            'isNew': true,
            'element': columnConditionSelectElement,
            'columnName': chosenColumnName
        });
    } else {
        var colCondition = advancedFilterConditionColumnName['columnCondition'];
        var colValue = advancedFilterConditionColumnName['columnValue'];

        columnConditionSelectElement.val(colCondition).trigger('change');

        addAdvancedFilterConditionValueField({
            'isNew': false,
            'element': columnConditionSelectElement,
            'columnName': chosenColumnName,
            'columnValue': colValue
        });
    }
}

/**
* Adds the input field with placeholders that depend on the column's datatype at the advanced filter row.
*
* @param advancedFilterConditionColumnCondition object containing the properties (isNew, columnName, columnValue, element) of the advanced filter column condition
*/
function addAdvancedFilterConditionValueField(advancedFilterConditionColumnCondition) {
    var columnConditionSelectElement = advancedFilterConditionColumnCondition['element'];
    var columnName = advancedFilterConditionColumnCondition['columnName'];
    // Remove all input value to the right if they exist
    if($(columnConditionSelectElement).parent().next().length) {
        $(columnConditionSelectElement).parent().nextAll().remove();
    }

    var chosenColumnCondition = $(columnConditionSelectElement).select2('data')[0].id;

    // No input field: is null, is not null
    if(chosenColumnCondition != 'is null' && chosenColumnCondition != 'is not null') {  // Add a text input
        var placeholder = '';
        if(advancedFilterSelection[columnName] == 'String') {
            placeholder = 'Enter value/s enclosed in quotes (&quot;&quot;) and separated by a comma (,)';
        } else if(advancedFilterSelection[columnName] == 'Number') {
            if(chosenColumnCondition == 'range') {
                placeholder = 'Enter range in &quot;N-N&quot; format';
            } else {
                placeholder = 'Enter number/s separated by a comma (,)';
            }
        } else {    // Timestamp
            if(chosenColumnCondition == 'range') {
                placeholder = 'yyyy-mm-dd>yyyy-mm-dd';
            } else {
                placeholder = 'yyyy-mm-dd';
            }
        }

        $(columnConditionSelectElement).parents().eq(1).append('<div class="col s5">'
            +'<input id="'+columnName+'-input" class= "advanced-filter-column-value-input" placeholder="'+placeholder+'" type="text">'
        +'</div>');
        if(advancedFilterConditionColumnCondition['isNew'] == true) {
            // Put current value (from currentUserFilterConfig) in input field
            if(currentUserFilterConfig.hasOwnProperty(columnName)) {
                if(advancedFilterSelection[columnName] == 'String') {
                    $('#'+columnName+'-input').val('"'+currentUserFilterConfig[columnName].join('","')+'"');
                } else {
                    $('#'+columnName+'-input').val(currentUserFilterConfig[columnName].join(','));
                }
            }
            $('#'+columnName+'-input').focus();
        } else {
            var colValue = advancedFilterConditionColumnCondition['columnValue'];
            if(advancedFilterSelection[columnName] == 'String') {
                $('#'+columnName+'-input').val('"'+colValue.join('","')+'"');
            } else {
                $('#'+columnName+'-input').val(colValue.join(','));
            }
        }
    } else {    // if 'is null' or 'is not null', just remove the value and don't create input field
        if(currentUserFilterConfig.hasOwnProperty(columnName)) {
            delete currentUserFilterConfig[columnName];
        }
    }
}


/**
* Compares if two jsons are equal, given that all of the values in its key-value pair are arrays
*
* @param json curr The object containing the data of the current filter configuration
* @param json rowData The object containing the data of the current row the current filter configuration is being compared to
* @return boolean True or False Whether both objects are the same or not
*/
function filterDataIsEqual(curr, rowData) {
    // Compares if the two json are equal in length
    if(Object.keys(curr).length != Object.keys(rowData).length) {
        return false;
    }

    // Compares if the keys of both json are equal
    if(Object.keys(curr).sort().toString() != Object.keys(rowData).sort().toString()) {
        return false;
    }

    // Compares the values (array) of each key on both jsons
    for(key in curr) {
        if(key == '__advanced_filters__') {
            if(rowData.hasOwnProperty(key)) {
                for(advancedKey in curr[key]) {
                    if(!rowData[key].hasOwnProperty(advancedKey)) {
                        return false;
                    }
                    if(curr[key][advancedKey] != rowData[key][advancedKey]) {
                        return false;
                    }
                }
            }
            continue;
        }
        if(curr.hasOwnProperty(key) && rowData.hasOwnProperty(key)) {
            // Compares length of both arrays
            if(curr[key].length != rowData[key].length) {
                return false;
            }

            // Compares if the elements of both arrays are equal
            if(curr[key].sort().toString() != rowData[key].sort().toString()) {
                return false;
            }
        }
    }

    return true;
}

/**
* Finds the index of the current filter configuration in the user's saved filter configurations
*
* @param json currentConfig The object containing the current filter configuration
* @param array filterConfig The array containing the user's saved filter configurations
* @return integer The index of the current filter configuration in user's saved filter configurations, if not found then -1
*/
function findCurrentConfigInSavedConfigs(currentConfig, savedConfigs) {
    for(var index in savedConfigs) {
        // if found, return index
        if(filterDataIsEqual(currentConfig, savedConfigs[index]['data'])) {
            return index;
        }
    }

    // if not found, return -1
    return -1;
}

/**
* Sets the Select2 widget to the index of a selected filter
*
* @param integer index The index of the filter
*/
function updateFilterSelection(index) {
    // Set Select2 widget value to index and apply change
    $('#filter_select').val(index).trigger('change');
    currentSelectedIndex = $('#filter_select').val();
}

/**
* Add *unsaved at the filter selection if it does not exist and select if whenever a user modifies a filter configuration.
*/
function addUnsavedFilterSelectionForAdvancedFilters() {
    var index = $('#filter_select').val();
    if(currentIsUnsaved) {
        if(index == 0) {
            // Use currentUserFilterConfig
        } else {
            if(index == null) { // No filter is selected
                currentUserFilterConfig = {};
            } else {
                currentUserFilterConfig = {...savedUserFilterConfigs[index - 1]['data']};
            }
            // Select *unsaved in select2
            updateFilterSelection(0);
            $('#save-filter-btn').show();
            $('#delete-filter-btn').hide();
        }
    } else {
        if(index == null) { // No filter is selected
            currentUserFilterConfig = {};
        } else {
            currentUserFilterConfig = {...savedUserFilterConfigs[index]['data']};
        }
        // Get filter names of savedUserFilterConfigs
        var filterNames = [];
        for(var savedFilter of savedUserFilterConfigs) {
            filterNames.push(savedFilter['name'])
        }
        // Create *unsaved and select it in select2
        filterNames.unshift('*unsaved');
        setFilterSelection(filterNames);
        updateFilterSelection(0);
        $('#save-filter-btn').show();
        $('#delete-filter-btn').hide();
    }
    currentIsUnsaved = true;
}

/**
* Checks if all inputs in the advanced-filter-condition-list are filled correctly based on each column's datatype.
*/
function allInputsAreFilledCorrectly() {
    var inputsAreCorrect = true;
    for(var columnName in advancedFilterSelected) {
        if(advancedFilterSelected[columnName]) {    // column has advanced filters
            var columnCondition = 'equals';
            if(currentUserFilterConfig.hasOwnProperty('__advanced_filters__')
              && currentUserFilterConfig['__advanced_filters__'].hasOwnProperty(columnName)) {
                columnCondition = currentUserFilterConfig['__advanced_filters__'][columnName];
                // Catch conditions with null because they don't have an input field
                if(columnCondition == 'is null' || columnCondition == 'is not null') {
                    continue;
                }
            }
            var value = $('#'+columnName+'-input').val();

            var validString = true;
            if(advancedFilterSelection[columnName] == 'String') {
                value = value.trim();
                if(value.startsWith('"') && value.endsWith('"')) {
                    value = value.slice(1);
                    value = value.slice(0, -1);
                    value = value.split(/\"\s*\,\s*\"/);
                    value = value.map(e => e.trim()).filter(e => e !== '' && e !==',');
                } else {
                    validString = false;
                    value = [false];
                }
            } else {
                value = value.split(',');
                value = value.map(e => e.trim()).filter(e => e !== '');
            }
            // When the input is null or undefined
            if(!Array.isArray(value) || !value.length) {
                return false;
            }
            // Check if each value is valid
            var message = '';
            var allColumnInputIsValid = true;
            var columnDatatype = advancedFilterSelection[columnName];
            if(columnDatatype == 'String') {
                var newValue = '';
                if(validString) {
                    newValue = '"'+value.join('","')+'"';
                } else {
                    message = 'Please enclose each value in quotes ("") and separate by commas for the '+columnName+' input.';
                    allColumnInputIsValid = false;
                    inputsAreCorrect = false;
                }
                // Update the input value with the values that are enclosed in quotes and separated by commas
                if(allColumnInputIsValid) {
                    $('#'+columnName+'-input').val(newValue).trigger('input');
                }
            } else if(columnDatatype == 'Number') {
                var newValue = [];
                if(columnCondition == 'range') {
                    for(var val of value) {
                        if(val.indexOf('-') != -1) {    // range input
                            var rangeValueIsValid = true;
                            if((val.match(/\-/g) || []).length != 1) {  // Input should only be one range value
                                message = 'Please follow the "N-N" format for each of the '+columnName+' range input.';
                                rangeValueIsValid = false;
                            } else {
                                var range = val.split('-');
                                if(isNaN(range[0]) || isNaN(range[1])) {    // Both numbers should be valid
                                    message = 'Both values should be a valid number for each of the '+columnName+' range input.';
                                    rangeValueIsValid = false;
                                } else {
                                    var newRange1 = Number(range[0]);
                                    var newRange2 = Number(range[1]);
                                    if(newRange1 >= newRange2) {    // First value should only be less than the second value
                                        message = 'The first value should be less than the second value for each of the '+columnName+' range input.';
                                        rangeValueIsValid = false;
                                    } else {    // Range is valid
                                        newValue.push(newRange1.toString() + '-' + newRange2.toString());
                                    }
                                }
                            }

                            if(!rangeValueIsValid) {
                                allColumnInputIsValid = false;
                                inputsAreCorrect = false;
                            }
                        } else {    // number input
                            if(isNaN(val)) {
                                message = 'Only numbers are allowed for each of the '+columnName+' value/s.';
                                allColumnInputIsValid = false;
                                inputsAreCorrect = false;
                            } else {
                                // Convert value to Number
                                newValue.push(Number(val));
                            }
                        }
                    }
                } else {
                    for(var val of value) {
                        if(isNaN(val)) {
                            message = 'Only numbers are allowed for each of the '+columnName+' value/s.';
                            allColumnInputIsValid = false;
                            inputsAreCorrect = false;
                        } else {
                            // Convert value to Number
                            newValue.push(Number(val))
                        }
                    }
                }
                // Update the input value with the values that are converted to Number
                if(allColumnInputIsValid) {
                    $('#'+columnName+'-input').val(newValue.join(',')).trigger('input');
                }
            } else {    // Timestamp
                if(columnCondition == 'range') {
                    for(var val of value) {
                        if(val.indexOf('>') != -1) { // range input
                            if(!/^\d\d\d\d\-\d\d\-\d\d\>\d\d\d\d\-\d\d\-\d\d$/.test(val)) {
                                message = 'Please follow the "yyyy-mm-dd>yyyy-mm-dd" format for each of the '+columnName+' range input.';
                                allColumnInputIsValid = false;
                                inputsAreCorrect = false;
                                break;
                            }
                        } else {    // date input
                            if(!/^\d\d\d\d\-\d\d\-\d\d$/.test(val)) {
                                message = 'Please follow the "yyyy-mm-dd" format for each of the '+columnName+' date input.';
                                allColumnInputIsValid = false;
                                inputsAreCorrect = false;
                                break;
                            }
                        }
                    }
                } else {
                    for(var val of value) {
                        if(!/^\d\d\d\d\-\d\d\-\d\d$/.test(val)) {
                            message = 'Please follow the "yyyy-mm-dd" format for each of the '+columnName+' value/s.';
                            allColumnInputIsValid = false;
                            inputsAreCorrect = false;
                        }
                    }
                }
            }
            if(!allColumnInputIsValid) {
                var notif = '<i class="material-icons orange-text">warning</i>'+message;
                Materialize.toast(notif, 5000);
            }
        }
    }

    if(!inputsAreCorrect) {
        return false;
    }
    return true;
}

/**
* Checks the logic of each column condition and warns the user of any logical errors in the input.
*/
function checkColumnConditionLogic(filterConfigToApply) {
    if(filterConfigToApply) {   // if filterConfigToApply is not empty
        for(var colName in filterConfigToApply) {
            if(colName == '__advanced_filters__') {
                continue;
            }

            // Get the advanced filter condition of the column
            var colCondition = 'equals';
            if(filterConfigToApply.hasOwnProperty('__advanced_filters__')
              && filterConfigToApply['__advanced_filters__'].hasOwnProperty(colName)) {
                colCondition = filterConfigToApply['__advanced_filters__'][colName];
            }

            // Columns that should only have a single value, else there might be a logical error
            var singleValueOnlyColumns = [
                'less than',
                'less than or equal to',
                'greater than',
                'greater than or equal to',
                'range'
            ];
            if(colCondition == 'equals') {
                continue;
            } else if(singleValueOnlyColumns.indexOf(colCondition) !== -1) {
                // Column should only have 1 value, if it has more than 1 value then the result is prone to logical error
                if(filterConfigToApply[colName].length > 1) {
                    var message = 'Logical error found in '+colName+' column where it expects only 1 value. Results may not be as expected.';
                    var notif = '<i class="material-icons orange-text">warning</i>'+message;
                    Materialize.toast(notif, 5000);
                }
            }
        }
    }
}

/**
* Refreshes the Select2 widget and updates the filterView.
*/
function refreshFilterSelection() {
    setTimeout(function(){
        $.ajax({
            url: '$getFiltersUrl',
            data: {
                dataBrowserId: $dataBrowserId
            },
            type: 'POST',
            async: true,
            success: function(data) {
                data = JSON.parse(data);
                
                // Store parsed data to savedUserFilterConfigs
                savedUserFilterConfigs = data;

                // Get filter names of savedUserFilterConfigs
                var filterNames = [];
                for(var savedFilter of savedUserFilterConfigs) {
                    filterNames.push(savedFilter['name'])
                }

                // If currentUserFilterConfig is not empty
                if(!$.isEmptyObject(currentUserFilterConfig)) {
                    // Get index of currentUserFilterConfig in savedUserFilterConfigs if found
                    var index = findCurrentConfigInSavedConfigs(currentUserFilterConfig, savedUserFilterConfigs);
                    var selectedFilterConfig = {};
                    if(index == -1) {
                        currentIsUnsaved = true;
                        selectedFilterConfig = currentUserFilterConfig;
                        setFilterView(selectedFilterConfig);
                        filterNames.unshift('*unsaved');
                        index += 1
                        $('#save-filter-btn').show();
                    } else {
                        currentIsUnsaved = false;
                        selectedFilterConfig = {...savedUserFilterConfigs[index]['data']};
                        setFilterView(selectedFilterConfig);
                        $('#delete-filter-btn').show();
                    }
                    refreshAdvancedFilters(selectedFilterConfig);
                    setFilterSelection(filterNames);
                    updateFilterSelection(index);
                } else {
                    currentIsUnsaved = false;
                    setFilterSelection(filterNames);
                    updateFilterSelection('');
                }
                // enable Select2 widget
                $('#filter_select').prop('disabled', false);
                if(currentlyDeletingFilter) {
                    $('#filter_select').trigger('select2:unselect');
                    $('#filter_select').val(null).trigger('change');
                    $('#advanced-filter-add-condition-btn').prop('disabled', false);
                    currentlyDeletingFilter = false;
                }
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading filter configuration. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });
    }, 300);
}

/**
* This function is only called when a filter selection is selected. It builds the advanced filters.
*
* @param filterConfig object of the selected filter configuration
*/
function refreshAdvancedFilters(filterConfig) {
    // Reset advanced filters (remove from list)
    advancedFilterConditionCurrentCount = 0;
    $('#advanced-filter-condition-list').empty();
    // Reset selected column values
    buildAdvancedFilterSelected();

    var filterKeys = Object.keys(filterConfig);
    var advancedFilterConditions = {};
    var advancedFilterKeys = [];
    var operator = 'AND';
    if(filterConfig.hasOwnProperty('__advanced_filters__')) {
        advancedFilterKeys = Object.keys(filterConfig['__advanced_filters__']);
        filterKeys = filterKeys.filter(e => e !== '__advanced_filters__');
        advancedFilterKeys = advancedFilterKeys.filter(e => e !== '__operator__');
        if(filterConfig['__advanced_filters__'].hasOwnProperty('__operator__')) {
            operator = filterConfig['__advanced_filters__']['__operator__'];
        }
        advancedFilterConditions = filterConfig['__advanced_filters__'];
    }
    var keys = [...new Set([...filterKeys, ...advancedFilterKeys])];
    var operatorList = ['AND','OR'];

    $('#advanced_filter_operator_select').val(operatorList.indexOf(operator)).trigger('change');

    for(var colName of keys) {
        var colCondition = 'equals';
        if(advancedFilterConditions.hasOwnProperty(colName)) {
            colCondition = advancedFilterConditions[colName];
        }
        var colValue = [];
        if(filterConfig.hasOwnProperty(colName)) {
            colValue = filterConfig[colName];
        }
        addAdvancedFilterCondition({
            'isNew': false,
            'element': $('#advanced-filter-add-condition-btn'),
            'columnName': colName,
            'columnCondition': colCondition,
            'columnValue': colValue
        });
    }
}

/**
* When the user unselects the currently selected option in the Select2 widget,
* the currentSelectedIndex is set to -1, the buttons are hidden and the filter view is cleared.
*/
$('#filter_select').on('select2:unselect', function(e) {
    currentSelectedIndex = -1;
    buildAdvancedFilterSelected();
    $('#save-filter-btn').hide();
    $('#delete-filter-btn').hide();
    setFilterView({});
    // clearing the advanced filter conditions list
    $('#advanced_filter_operator_select').val(0).trigger('change');
    $('#advanced-filter-condition-list').empty();
    // disable operator select, add condition button, and clear all button
    $('#advanced-filter-clear-all-condition-btn').prop('disabled', true);
});

/**
* When the user selects an option in the Select2 widget, the filter view is updated and
* the save and delete buttons are shown or hidden based on the index of the selected option
* and if the current filter configuration is unsaved.
*/
$('#filter_select').on('select2:select', function(e) {
    $('#advanced-filter-add-condition-btn').prop('disabled', false);
    currentSelectedIndex = $('#filter_select').val();
    var index = $('#filter_select').val();
    var selectedFilterConfig = {};
    if(currentIsUnsaved) {
        if(index == 0) {
            selectedFilterConfig = currentUserFilterConfig;
            setFilterView(selectedFilterConfig);
            $('#save-filter-btn').show();
            $('#delete-filter-btn').hide();
        } else {
            selectedFilterConfig = {...savedUserFilterConfigs[index - 1]['data']};
            setFilterView(selectedFilterConfig);
            $('#save-filter-btn').hide();
            $('#delete-filter-btn').show();
        }
    } else {
        selectedFilterConfig = {...savedUserFilterConfigs[index]['data']};
        setFilterView(selectedFilterConfig);
        $('#save-filter-btn').hide();
        $('#delete-filter-btn').show();
    }
    refreshAdvancedFilters(selectedFilterConfig);
    $('#advanced-filter-clear-all-condition-btn').prop('disabled', false);
});

/**
* When the user selects an operator (AND, OR), the current user filter configuration is updated.
*/
$('#advanced_filter_operator_select').on('select2:select', function(e) {
    addUnsavedFilterSelectionForAdvancedFilters();
    if($('#advanced_filter_operator_select').select2('data')[0].text == 'AND'
      && currentUserFilterConfig.hasOwnProperty('__advanced_filters__')
      && currentUserFilterConfig['__advanced_filters__'].hasOwnProperty('__operator__')) {  // AND
        delete currentUserFilterConfig['__advanced_filters__']['__operator__'];
        // The advanced filters were cleared
        if($.isEmptyObject(currentUserFilterConfig['__advanced_filters__'])) {
            delete currentUserFilterConfig['__advanced_filters__'];
        }
        setFilterView(currentUserFilterConfig);
    } else {    // OR
        $('#advanced-filter-clear-all-condition-btn').prop('disabled', false);
        if(!currentUserFilterConfig.hasOwnProperty('__advanced_filters__')) {
            currentUserFilterConfig['__advanced_filters__'] = {};
        }
        currentUserFilterConfig['__advanced_filters__']['__operator__'] = ($('#advanced_filter_operator_select').select2('data')[0].text);
        setFilterView(currentUserFilterConfig);
    }
});

/**
* When the column name select is opened, enable selecting of currently selected column name.
*/
$('#advanced-filter-condition-list').on('select2:open', '.advanced-filter-column-name-select', function(e) {
    var currentlySelectedColumnName = $(this).select2('data')[0].id;
    $(this).children().eq(columnNames.indexOf(currentlySelectedColumnName)).prop('disabled', false);
});

/**
* Before the column name select event is fired, update the previously selected column name to false
* in the advancedFilterSelected array.
*/
$('#advanced-filter-condition-list').on('select2:selecting', '.advanced-filter-column-name-select', function(e) {
    var currentlySelectedColumnName = $(this).select2('data')[0].id;
    advancedFilterSelected[currentlySelectedColumnName] = false;
    updateDisabledAdvacedFilterNamesInSelect();
    if(currentUserFilterConfig.hasOwnProperty('__advanced_filters__')
        && currentUserFilterConfig['__advanced_filters__'].hasOwnProperty(currentlySelectedColumnName)) {
            delete currentUserFilterConfig['__advanced_filters__'][currentlySelectedColumnName];
        }
    if(currentUserFilterConfig.hasOwnProperty(currentlySelectedColumnName)) {
        delete currentUserFilterConfig[currentlySelectedColumnName];
    }
    setFilterView(currentUserFilterConfig);
});

/**
* When the user selects a column name, the column condition select widget is added by calling
* addAdvancedFilterConditionConditionSelect() and the selected advanced filter names are updated.
*/
$('#advanced-filter-condition-list').on('select2:select', '.advanced-filter-column-name-select', function(e) {
    var currentlySelectedColumnName = $(this).select2('data')[0].id;
    advancedFilterSelected[currentlySelectedColumnName] = true;
    addAdvancedFilterConditionConditionSelect({'isNew': true, 'element': $(this)});
    updateDisabledAdvacedFilterNamesInSelect();
});

/**
* Before the column condition select event is fired, remove past condition from currentUserFilterConfig.
*/
$('#advanced-filter-condition-list').on('select2:selecting', '.advanced-filter-column-condition-select', function(e) {
    var currentlySelectedCondition = $(this).select2('data')[0].id;
    var columnNameSelectElement = $(this).parent().prev().find('.advanced-filter-column-name-select');
    var columnName = columnNameSelectElement.select2('data')[0].id;

    // if currentlySelectedCondition is 'is null' or 'is not null', remove column value and condition from currentUserFilterConfig
    if(currentlySelectedCondition == 'is null' || currentlySelectedCondition == 'is not null') {
        if(currentUserFilterConfig.hasOwnProperty('__advanced_filters__')
            && currentUserFilterConfig['__advanced_filters__'].hasOwnProperty(columnName)) {
                delete currentUserFilterConfig['__advanced_filters__'][columnName];
            }
        if(currentUserFilterConfig.hasOwnProperty(columnName)) {
            delete currentUserFilterConfig[columnName];
        }
        setFilterView(currentUserFilterConfig);
    }
});

/**
* When the user selects a condition, the input element is added by calling the
* addAdvancedFilterConditionValueField() and the currentUserFilterConfig is updated.
*/
$('#advanced-filter-condition-list').on('select2:select', '.advanced-filter-column-condition-select', function(e) {
    addUnsavedFilterSelectionForAdvancedFilters();

    var columnNameSelectElement = $(this).parent().prev().find('.advanced-filter-column-name-select');
    var columnName = columnNameSelectElement.select2('data')[0].id;
    var columnCondition = $(this).select2('data')[0].id;

    addAdvancedFilterConditionValueField({
        'isNew': true,
        'element': $(this),
        'columnName': columnName
    });

    if(columnCondition == 'equals'
      && currentUserFilterConfig.hasOwnProperty('__advanced_filters__')
      && currentUserFilterConfig['__advanced_filters__'].hasOwnProperty(columnName)) {  // equals
        delete currentUserFilterConfig['__advanced_filters__'][columnName];
        // The advanced filters were cleared (or are using default behavior (AND, equals))
        if($.isEmptyObject(currentUserFilterConfig['__advanced_filters__'])) {
            delete currentUserFilterConfig['__advanced_filters__'];
        }
        setFilterView(currentUserFilterConfig);
    } else {    // other
        $('#advanced-filter-clear-all-condition-btn').prop('disabled', false);
        if(!currentUserFilterConfig.hasOwnProperty('__advanced_filters__')) {
            currentUserFilterConfig['__advanced_filters__'] = {};
        }
        currentUserFilterConfig['__advanced_filters__'][columnName] = columnCondition;
        if(!currentUserFilterConfig.hasOwnProperty(columnName)) {
            currentUserFilterConfig[columnName] = [];
        }
        setFilterView(currentUserFilterConfig);
    }
});

/**
* When the user types on the save filter input, the save button is enabled if the input
* is not empty and disabled if the input is empty.
*/
$('#new-filter-name-input').on('input', function(e) {
    if(!$('#new-filter-name-input').val().trim()) {
        $('#save-filter-final-btn').prop('disabled', true);
    } else {
        $('#save-filter-final-btn').prop('disabled', false);
    }
});

/**
* When the user presses the ENTER key on the save filter input, trigger the save button.
*/
$('#new-filter-name-input').on('keypress', function(e) {
    var keyCode = (e.keyCode? e.keyCode : e.which);
    if(keyCode == 13) { // Enter key is pressed
        e.preventDefault();
        if(!currentlySavingFilter) {
            $('#save-filter-final-btn').trigger('click');   // The save button is triggered
        }
    }
});

/**
* Whenever the user types in the advanced filter input field, check if it is not empty and update
* the currentUserFilterConfig and the filter view.
*/
$('#advanced-filter-condition-list').on('input', '.advanced-filter-column-value-input', function(e) {
    // catch if whitespace or blank (especially if the user inputs many comma (,) characters)
    var columnNameSelectElement = $(this).parent().prev().prev().find('.advanced-filter-column-name-select');
    var columnName = columnNameSelectElement.select2('data')[0].id;
    var value = $(this).val();
    var validInput = true;
    if(advancedFilterSelection[columnName] == 'String') {
        value = value.trim();
        if(value.startsWith('"') && value.endsWith('"')) {
            value = value.slice(1);
            value = value.slice(0, -1);
            value = value.split(/\"\s*\,\s*\"/);
            value = value.map(e => e.trim()).filter(e => e !== '' && e !==',');
        } else {
            validInput = false;
        }
    } else {
        value = value.split(',');
        value = value.map(e => e.trim()).filter(e => e !== '');
    }

    // Only applies change when the input is not null
    if(Array.isArray(value) && value.length && validInput) {
        addUnsavedFilterSelectionForAdvancedFilters();
        var columnName = $(this).parents().eq(1).find('.advanced-filter-column-name-select').select2('data')[0].id;
        currentUserFilterConfig[columnName] = value;
        setFilterView(currentUserFilterConfig);
    }
});

/**
* The close or cancel button hides the save or delete modal depending on which one
* is currently shown.
*/
$('.close-cancel-dialog-btn').on('click', function(e) {
    if($('#saveFilterModal').hasClass('in')) {
        $('#saveFilterModal').modal('hide');
    }

    if($('#deleteFilterModal').hasClass('in')) {
        $('#deleteFilterModal').modal('hide');
    }
});

/**
* When the save modal is shown, this function sets the focus on the input.
*/
$('#saveFilterModal').on('shown.bs.modal', function(e) {
    $(this).find('#new-filter-name-input').focus();
});

/**
* Shows the save filter modal when the save button is clicked if the currentUserFilterConfig
* is not empty.
*/
$('#save-filter-btn').on('click', function (e) {
    if(allInputsAreFilledCorrectly()) {
        if(!$.isEmptyObject(currentUserFilterConfig)) {
            $('#saveFilterModal').modal('show')
        } else {
            var notif = '<i class="material-icons orange-text">warning</i> There is no filter configuration to save. Please add a filter condition or change the operator.';
            Materialize.toast(notif, 5000);
        }
    } else {
        var notif = '<i class="material-icons orange-text">warning</i> Please correctly fill out all input fields.';
        Materialize.toast(notif, 5000);
    }
});

/**
* When the final save button is clicked, this function checks if the input is not empty
* and proceeds to save the current filters.
*/
$('#save-filter-final-btn').on('click', function(e) {
    // if input is not an empty string
    if($('#new-filter-name-input').val().trim() !== "" && !currentlySavingFilter) {
        currentlySavingFilter = true;
        $('#filter_select').prop('disabled', true);

        setTimeout(function(){
            $.ajax({
                url: '$saveFiltersUrl',
                data: {
                    name: $('#new-filter-name-input').val().trim(),
                    data: JSON.stringify(currentUserFilterConfig),
                    dataBrowserId: $dataBrowserId
                },
                type: 'POST',
                async: true,
                success: function(data) {
                    if(data == true) {  // Filter configuration was saved successfully
                        var notif = '<i class="material-icons green-text">check</i> Successfully saved ' + $('#new-filter-name-input').val().trim() + ' filter.';
                        Materialize.toast(notif, 5000);
                        $('#save-filter-btn').hide();
                        refreshFilterSelection();
                    } else {    // Filter configuration was not saved successfully
                        var notif = '<i class="material-icons red-text">close</i> There was a problem while saving filter configuration. Please try again.';
                        Materialize.toast(notif, 5000);
                        $('#filter_select').prop('disabled', false);
                    }
                    $('#saveFilterModal').modal('hide');
                    $('#new-filter-name-input').val('');
                    $('#save-filter-final-btn').prop('disabled', true);
                    currentlySavingFilter = false;
                },
                error: function(){
                    var notif = '<i class="material-icons orange-text">warning</i> There was a problem while saving filter configuration. Please try again.';
                    Materialize.toast(notif, 5000);
                    $('#filter_select').prop('disabled', false);
                    currentlySavingFilter = false;
                }
            });
        }, 300);
    }
});

/**
* When the delete button is clicked, this function gets the name and index of the
* currently selected filter and sets the title and content of the delete modal.
*/
$('#delete-filter-btn').on('click', function(e) {
    // find index of selected filter
    var index = $('#filter_select').val();
    if(currentIsUnsaved) {
        index -= 1;
    }

    $('#deleteFilterModalTitle').html('<b>'+savedUserFilterConfigs[index]['name']+'</b>');
    $('#deleteFilterModalContent').html('<b>'+savedUserFilterConfigs[index]['name']+'</b>');
});

/**
* When the final delete button is clicked, this function proceeds to delete the selected filter.
*/
$('#delete-filter-final-btn').on('click', function(e) {
    $('#filter_select').prop('disabled', true);

    // find index of selected filter
    var index = $('#filter_select').val();
    if(currentIsUnsaved) {
        index -= 1;
    }

    setTimeout(function(){
        $.ajax({
            url: '$deleteFiltersUrl',
            data: {
                filterId: savedUserFilterConfigs[index]['configDbId'],
            },
            type: 'POST',
            async: true,
            success: function(data) {
                if(data == true) {  // Filter configuration was deleted successfully
                    var notif = '<i class="material-icons green-text">check</i> Successfully deleted ' + savedUserFilterConfigs[index]['name'] + ' filter.';
                    Materialize.toast(notif, 5000);
                    currentlyDeletingFilter = true;
                    refreshFilterSelection();   // update filter view to currently selected
                } else {    // Filter configuration was not deleted successfully
                    var notif = '<i class="material-icons red-text">close</i> There was a problem while deleting filter configuration. Please try again.';
                    Materialize.toast(notif, 5000);
                    $('#filter_select').prop('disabled', false);
                }
                $('#deleteFilterModal').modal('hide');
                $('#deleteFilterModalTitle').text('Filter');  // reset the delete modal title text to 'Filter'
                $('#deleteFilterModalContent').text('Filter');    // reset the delete modal content text to 'Filter'
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while deleting filter configuration. Please try again.';
                Materialize.toast(notif, 5000);
                $('#filter_select').prop('disabled', false);
            }
        });
    }, 300);
});

/**
* When the add advanced filter condition button is clicked, add an advanced filter row.
*/
$('#advanced-filter-add-condition-btn').on('click', function(e) {
    addUnsavedFilterSelectionForAdvancedFilters();
    addAdvancedFilterCondition({'isNew': true, 'element': this});
    $('#advanced-filter-clear-all-condition-btn').prop('disabled', false);
});

/**
* Adds listener for all the remove-advanced-filter-condition-button descendants of
* advanced-filter-condition-list. When the user clicks the remove button on the
* advanced filter condition, this function searches for the index of the selected
* advanced filter and removes the list item.
*/
$('#advanced-filter-condition-list').on('click', '.remove-advanced-filter-condition-button', function(e) {
    addUnsavedFilterSelectionForAdvancedFilters();
    advancedFilterConditionCurrentCount -= 1;
    // enable option
    var columnName = $(this).parent().next().find('select').select2('data')[0].id;
    if(currentUserFilterConfig.hasOwnProperty(columnName)) {
        delete currentUserFilterConfig[columnName];
    }
    if(currentUserFilterConfig.hasOwnProperty('__advanced_filters__')
      && currentUserFilterConfig['__advanced_filters__'].hasOwnProperty(columnName)) {
        delete currentUserFilterConfig['__advanced_filters__'][columnName];
    }
    if(advancedFilterConditionCurrentCount == 0 && $.isEmptyObject(currentUserFilterConfig['__advanced_filters__'])) {
        delete currentUserFilterConfig['__advanced_filters__'];
        $('#advanced-filter-clear-all-condition-btn').prop('disabled', true);
    }
    $('#advanced-filter-add-condition-btn').prop('disabled', false);
    setFilterView(currentUserFilterConfig);
    advancedFilterSelected[columnName] = false;
    updateDisabledAdvacedFilterNamesInSelect();
    $('#advanced-filter-condition-list li').eq($(this).parents().eq(3).index()).remove();
});

/**
* When the user clicks the clear all advance filters button, this function clears the
* advanced-filter-condition-list list.
*/
$('#advanced-filter-clear-all-condition-btn').on('click', function(e) {
    $('#filter_select').val(null).trigger('change');
    $('#filter_select').trigger('select2:unselect');
    $('#advanced-filter-add-condition-btn').prop('disabled', false);
    advancedFilterConditionCurrentCount = 0;
});

/**
* When the user clicks the apply button, this function gets the selected filter
* configuration (except for the currently applied filters) and applies it.
*/
$('#filter-action-modal-apply-btn').off('click').on('click', function(e) {
    if(allInputsAreFilledCorrectly()) {
        $(this).prop('disabled', false);
        // find index of selected filter
        var index = currentSelectedIndex;
        var noSelectedFilter = false;

        if(index == -1 || index == null) {
            noSelectedFilter = true;
        }

        var browserUrl = '$browserUrl';
        if(!noSelectedFilter) {
            if(currentIsUnsaved) {
                index -= 1;
            }

            var filterConfigToApply = {};
            if(index > -1) {
                filterConfigToApply = savedUserFilterConfigs[index]['data'];
            } else {
                filterConfigToApply = currentUserFilterConfig;
            }

            checkColumnConditionLogic(filterConfigToApply);

            browserUrl += '&'+$.param({'$browserModel':filterConfigToApply});
        }
        if(sortsInUrl !== '') {
            browserUrl += '&'+sortsInUrl;
        }

        // Apply new filter values by reloading the data browser
        $.pjax.reload({
            container: '$browserPjaxId',
            replace: true,
            url: browserUrl
        });

        // close modal
        $('.filter-action-modal-body').empty();
    } else {
        var notif = '<i class="material-icons orange-text">warning</i> Please correctly fill out all input fields.';
        Materialize.toast(notif, 5000);
    }
});

/**
* When the user clicks the cancel button, this function just closes the modal.
*/
$('#filter-action-modal-cancel-btn').on('click', function(e) {
    $('.filter-action-modal-body').empty();
});

/**
* When DOM has loaded
*/
$(document).ready(function() {
    $('#filter-action-modal-apply-btn').prop('disabled', true);
    $('#filter_select').prop('disabled', true);
    $('#save-filter-btn').hide();
    $('#delete-filter-btn').hide();
    retrieveCurrentConfig();
    setFilterView(currentUserFilterConfig);

    $('#advanced_filter_operator_select').select2({
        minimumResultsForSearch: -1, // Removes Select2 search field
        dropdownParent: $('#filter-action-modal')
    });

    refreshFilterSelection();

    buildAdvancedFilterSelection();
    buildAdvancedFilterSelected();
});

JS
);

Yii::$app->view->registerCss('
    body{
        overflow-x: hidden;
    }
    .dropdown-content {
       overflow-y: visible;
    }
    .dropdown-content .dropdown-content {
       margin-left: -100%;
    }
    .grid-view td {
        white-space: nowrap;
    }
    #filter-action-modal {
        z-index: 1040 !important;
    }
    .filter-name-item-selected {
        background-color: silver;
    }
    .dialog-modal {
        position: fixed;
        z-index: 1050 !important;
    }
    #filter_select {
        width: 100%;
    }
    .filter-action-modal-container {
        padding: 10px 15px 20px 15px !important;
    }
    .btn-container {
        display: flex;
        justify-content: flex-end;
        height: 40px;
    }
    .material-icons {
        display: inline-flex;
        vertical-align: top;
    }
    .modal-body {
        padding: 10px 0px 0px 0px;
    }
    .form-group {
        padding: 0px 30px;
    }
    a:hover {
        cursor: pointer;
    }
    .modal-body > p {
        padding: 0px 30px;
    }
    .guide-header {
        font-size: 1.1em;
    }
    .guide-list {
        font-size: 0.9em;
    }
    .guide-condition {
        font-weight: bold;
    }
    .column-value {
        color: #555555;
        background: #f5f5f5;
        border: 1px solid #ccc;
        border-radius: 4px;
        cursor: default;
        margin: 2px 3px 2px 3px;
        padding: 1px 5px;
    }
    .column-operator {
        color: #555555;
        background: #4dffff;
        border: 1px solid #00e6e6;
        border-radius: 4px;
        cursor: default;
        margin: 2px 3px 2px 3px;
        padding: 1px 5px;
    }
    .column-condition {
        color: #555555;
        background: #67ff67;
        border: 1px solid #61ff61;
        border-radius: 4px;
        cursor: default;
        margin: 2px 3px 2px 3px;
        padding: 1px 5px;
    }
    #filter-view-list > li {
        margin: 5px 0 5px 0;
    }
    #advanced_filter_operator_select + .select2 {
        width: inherit !important;
    }
    .advanced-filter-container {
        padding: 0 0;
    }
    .advanced-filter-column-name-select {
        display: block;
    }
    .filter-view-container {
        padding: 5px 5px !important;
        margin: 5px 0;
    }
    .filter-modal-row {
        margin-bottom: 5px;
    }
    .filter-modal-row > .col {
        padding: 0 5px;
    }
    .filter-modal-row > .col > .select2 {
        margin: 3px 0;
    }
    .filter-modal-row > .col > .advanced-filter-column-value-input {
        margin: -8px 0;
    }
    .remove-advanced-filter-condition-button {
        padding: 0 10px;
    }
');
?>