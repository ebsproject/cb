<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Contains methods for managing data browser filter configurations
 */

namespace app\modules\userFilter\controllers;

use Yii;
use app\models\User;
use app\models\DataBrowserFilterConfig;
use app\components\B4RController;
use yii\helpers\Json;

class DataBrowserFilterController extends B4RController
{

    /**
     * Retrieves the user's filter configurations based on the user's data browser.
     *
     * @param integer $dataBrowserId The id of the current data browser
     * @return array $result The user's saved filter configurations
     */
    public function actionIndex() {
        $userModel = new User();
        $userId = $userModel->getUserId(); // get currently logged in user
        $dataBrowserId = $_POST['dataBrowserId'];
        $columnNames = $_POST['columnNames'];
        $columnNameAndDatatypes = $_POST['columnNameAndDatatypes'];
        $browserUrl = $_POST['browserUrl'];
        $browserModel = $_POST['browserModel'];
        $browserPjaxId = $_POST['browserPjaxId'];

        return $this->renderAjax('index', [
            'dataBrowserId' => $dataBrowserId,
            'columnNames' => $columnNames,
            'columnNameAndDatatypes' => $columnNameAndDatatypes,
            'browserUrl' => $browserUrl,
            'browserModel' => $browserModel,
            'browserPjaxId' => $browserPjaxId
        ], true);
    }

    /**
     * Retrieves the user's filter configurations based on the user's data browser.
     *
     * @param integer $dataBrowserId The id of the current data browser
     * @return array $result The user's saved filter configurations
     */
    public function actionGetFilters() {
        $userModel = new User();
        $userId = $userModel->getUserId(); // get currently logged in user
        $dataBrowserId = $_POST['dataBrowserId'];

        $searchModel = new DataBrowserFilterConfig();

        $result = $searchModel->getFilters($dataBrowserId);

        return Json::encode($result);
    }

    /**
     * Saves filter configuration to database.
     *
     * @param string $name The name of the filter configuration
     * @param json $data The filter configuration
     * @param string $dataBrowserId The id of the current data browser
     * @return string $result Indicates if sql command executed successfully
     */
    public function actionSaveFilters() {
        $userModel = new User();
        $userId = $userModel->getUserId(); // get currently logged in user
        $name = $_POST['name'];
        $data = $_POST['data'];
        $dataBrowserId = $_POST['dataBrowserId'];

        $searchModel = new DataBrowserFilterConfig();

        $result = $searchModel->saveFilter($name, $data, $dataBrowserId);

        return $result;
    }

    /**
     * Deletes filter configuration from database.
     *
     * @param integer $filterId The id of the filter configuration to be deleted
     * @return string $result Indicates if sql command executed successfully
     */
    public function actionDeleteFilters() {
        $filterId = $_POST['filterId'];

        $searchModel = new DataBrowserFilterConfig();

        $result = $searchModel->deleteFilter($filterId);

        return $result;
    }

}