<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import interfaces
use app\interfaces\models\IExperiment;
use app\interfaces\models\ILocation;
use app\interfaces\models\ILocationOccurrenceGroup;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IVariable;
use app\interfaces\modules\harvestManager\models\IApiHelper;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel;

/**
* Model class for Harvest Data
*/
class OccurrenceDetailsModel implements IOccurrenceDetailsModel {
    /**
     * Model constructor
     */
    public function __construct(
        public IExperiment $experiment,
        public ILocation $location,
        public ILocationOccurrenceGroup $locationOccurrenceGroup,
        public IOccurrence $occurrence,
        public IVariable $variable,
        public IApiHelper $apiHelper,
        public IHarvestManagerModel $harvestManagerModel
        )
    { }

    /**
     * Retrieve variable display name that matches the field name
     * @param string fieldName name of field from api
     * @return string displayName retrieved variable display name matching the field name. If no variable matched the field name, the field name is returned as display name
     */
    public function getDisplayName($fieldName) {
        // separate pieces of camelCase field name
        $camelCasePcs = preg_split('/(?=[A-Z])/',$fieldName);

        $abbrev = '';
        foreach($camelCasePcs as $pc) {
            $allCaps = strtoupper($pc);
            if($abbrev == '') $abbrev .= $allCaps;
            else $abbrev .= '_' . $allCaps;
        }

        $apiFields = ['fields'=>'variable.abbrev|variable.display_name'];

        // get variable info
        $variableInfo = $this->variable->getVariableByAbbrev($abbrev, $apiFields);
        $displayName = isset($variableInfo['display_name']) ? $variableInfo['display_name'] : '';
        
        // if not variable matched the field name
        // remove last word and check again
        while(empty($displayName)) {
            $abbrevPcs = explode('_',$abbrev);
            array_pop($abbrevPcs);
            if(count($abbrevPcs)==0) {
                $displayName = ucfirst(strtolower($abbrev));
                break;
            }
            $abbrev = implode("_",$abbrevPcs);
            $variableInfo = $this->variable->getVariableByAbbrev($abbrev, $apiFields);
            $displayName = isset($variableInfo['display_name']) ? $variableInfo['display_name'] : '';
        }

        return $displayName;
    }

    /**
     * Retrieves the location id associated with the occurrence
     * @param integer occurenceId occurrence identifier
     */
    public function getLocationIdOfOccurrence($occurrenceId) {
        // get location id of occurrence
        $requestBody = [
            "occurrenceDbId" => "equals $occurrenceId",
            "fields" => "location_id as locationDbId|occurrence_id as occurrenceDbId",
        ];
        $result = $this->locationOccurrenceGroup->searchAll($requestBody);
        $loGroup = isset($result['data'][0]) ? $result['data'][0] : [];
        return isset($loGroup['locationDbId']) ? $loGroup['locationDbId'] : 0;
    }

    /**
     * Retrieves the experiment id association with the occurrence
     * @param integer occurenceId occurrence identifier
     */
    public function getExperimentIdOfOccurrence($occurrenceId) {
        // get experiment id of occurrence
        $requestBody = [
            "occurrenceDbId" => "$occurrenceId"
        ];
        $result = $this->occurrence->searchAll($requestBody);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        return isset($occurrence['experimentDbId']) ? $occurrence['experimentDbId'] : 0;
    }
    
    /**
     * Get the harvest summary given the occurrence
     * @param integer $occurrenceId occurrence identifier
     */
    public function getHarvestSummary($occurrenceId) {
        $result = $this->apiHelper->getParsedResponse('GET', 'occurrences/'.$occurrenceId.'/harvest-summaries');
        return isset($result['data'][0]) ? $result['data'][0] : [];
    }

    /**
     * Retrieve occurrence details
     * @param integer occurrenceId occurrence identifier
     */
    public function getOccurrenceDetails($occurrenceId) {
        // get experiment and location of occurrence
        $experimentId = $this->getExperimentIdOfOccurrence($occurrenceId);
        $locationId = $this->getLocationIdOfOccurrence($occurrenceId);
        if($experimentId == 0 || $locationId == 0) {
            $errorMessage = 'Unable to retrieve record ids.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        $secondaryInfoFields = [
            'experimentType' => '',
            'experimentYear' => '',
            'stageCode' => '',
            'season' => '',
            'locationName' => '',
            'locationCode' => '',
            'experimentName' => '',
            'occurrenceName' => '',
            'occurrenceNumber' => '',
            'site' => '',
            'steward' => '',
        ];

        // get label names
        foreach($secondaryInfoFields as $fieldName => $label) {
            $varLabel = $this->getDisplayName($fieldName);
            $secondaryInfoFields[$fieldName] = $varLabel;
        }
        
        // experiment info
        $requestBody = [ "experimentDbId" => "$experimentId" ];
        $result = $this->experiment->searchAll($requestBody);
        $experiment = isset($result['data'][0]) ? $result['data'][0] : [];
        if(empty($experiment)) {
            $errorMessage = 'Unable to retrieve experiment information.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }
        $experimentType = strtolower($experiment['experimentType']);

        // occurrence info
        $requestBody = [ "occurrenceDbId" => "$occurrenceId" ];
        $result = $this->occurrence->searchAll($requestBody);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        if(empty($occurrence)) {
            $errorMessage = 'Unable to retrieve occurrence information.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        // location info
        $requestBody = [ "locationDbId" => "$locationId" ];
        $result = $this->location->searchAll($requestBody);
        $location = isset($result['data'][0]) ? $result['data'][0] : [];
        if(empty($location)) {
            $errorMessage = 'Unable to retrieve location information.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        // Check if CPN
        $cpnCheck = $this->harvestManagerModel->checkCPN($experimentId);
        // If CPN, add phase to experiment type
        if($cpnCheck['isCPN']) {
            $experiment['experimentType'] .= ' (Phase ' . $cpnCheck['phase'] . ')';
        }

        // build occurrence details for occurrence selection tab
        $occurrenceDetailsT1 = [];
        $occurrenceDetailsT1[$secondaryInfoFields['experimentType']] = $experiment['experimentType'];
        $occurrenceDetailsT1[$secondaryInfoFields['experimentYear']] = $experiment['experimentYear'];
        $occurrenceDetailsT1[$secondaryInfoFields['stageCode']] = $experiment['stageCode'];
        $occurrenceDetailsT1[$secondaryInfoFields['locationName']] = $location['locationName'];
        $occurrenceDetailsT1[$secondaryInfoFields['locationCode']] = $location['locationCode'];
        $occurrenceDetailsT1[$secondaryInfoFields['season']] = $experiment['seasonName'];
        $occurrenceDetailsT1['Occurrence Number'] = $occurrence['occurrenceNumber'];
        $occurrenceDetailsT1[$secondaryInfoFields['site']] = $occurrence['site'];
        $occurrenceDetailsT1[$secondaryInfoFields['steward']] = $location['steward'];

        // build occurrence details for other tabs
        $occurrenceDetailsOther = [];
        $occurrenceDetailsOther[$secondaryInfoFields['experimentName']] = $experiment['experimentName'];
        $occurrenceDetailsOther[$secondaryInfoFields['occurrenceName']] = $occurrence['occurrenceName'];
        $occurrenceDetailsOther[$secondaryInfoFields['locationName']] = $location['locationName'];
        $occurrenceDetailsOther[$secondaryInfoFields['stageCode']] = $experiment['stageCode'];
        $occurrenceDetailsOther[$secondaryInfoFields['season']] = $experiment['seasonName'];
        $occurrenceDetailsOther[$secondaryInfoFields['site']] = $occurrence['site'];

        // build harvest summary
        $harvestSummaryDetails = [];
        $harvestSummary = $this->getHarvestSummary($occurrenceId);
        if(empty($harvestSummary)) {
            $errorMessage = 'Unable to retrieve harvest summary.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        // Collate summary
        $harvestSummaryDetails['No. of entries'] = $harvestSummary['totalEntryCount'];
        $harvestSummaryDetails['No. of rep'] = $harvestSummary['totalRepCount'];
        $harvestSummaryDetails['No. of packages (Cross/Plot)'] = $harvestSummary['totalPackagesFromCrosses'] . "/" . $harvestSummary['totalPackagesFromPlots'];
        $harvestSummaryDetails['Harvested crosses'] = $harvestSummary['harvestedCrosses'] . "/" . $harvestSummary['totalCrossCount'];
        $harvestSummaryDetails['Harvested plots'] = $harvestSummary['harvestedPlots'] . "/" . $harvestSummary['totalPlotCount'];

        return array(
            'occurrenceDetailsTab1' => $occurrenceDetailsT1,
            'occurrenceDetailsOther' => $occurrenceDetailsOther,
            'harvestSummaryDetails' => $harvestSummaryDetails
        );
    }
}

?>