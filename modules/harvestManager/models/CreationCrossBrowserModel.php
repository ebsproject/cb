<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import yii
use yii\data\Sort;
// Import data providers
use app\dataproviders\ArrayDataProvider;
// Import interfaces
use app\interfaces\modules\harvestManager\models\ICreationModel;
use app\interfaces\modules\harvestManager\models\ICrossBrowserModel;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
// Import models
use app\models\Cross;

/**
* Model class for Cross browser
*/
class CreationCrossBrowserModel extends Cross
{    
    public $crossFemaleParent;
    public $crossMaleParent;
    public $crossingDate;
    public $crossDbId;
    public $crossMethod;
    public $crossMethodAbbrev;
    public $crossName;
    public $femaleGermplasmState;
    public $femaleParentage;
    public $femaleParentSeedSource;
    public $femaleSourceSeedEntry;
    public $femaleSourcePlot;
    public $harvestStatus;
    public $maleGermplasmState;
    public $maleParentage;
    public $maleParentSeedSource;
    public $maleSourceSeedEntry;
    public $maleSourcePlot;
    public $remarks;
    public $femaleEntryCode;
    public $femaleEntryNumber;
    public $maleEntryCode;
    public $maleEntryNumber;

    /**
     * Model constructor
     */
    public function __construct(
        public ICreationModel $creationModel, 
        public ICrossBrowserModel $crossBrowserModel, 
        public IHarvestManagerModel $harvestManagerModel)
    { }

    public function rules()
    {
        return [
            [['crossFemaleParent', 'crossMaleParent', 'crossName', 'crossDbId', 'femaleGermplasmState', 'maleGermplasmState'], 'required'],
            [['crossDbId'], 'number'],
            [['crossDbId'], 'integer'],
            [['crossFemaleParent', 'crossMaleParent', 'crossingDate', 'crossMethod', 'crossMethodAbbrev', 'crossName', 'femaleGermplasmState', 'femaleParentage', 'femaleParentSeedSource', 'femaleSourceSeedEntry', 'femaleSourcePlot', 'harvestStatus', 'maleGermplasmState', 'maleParentage', 'maleParentSeedSource', 'maleSourceSeedEntry', 'maleSourcePlot', 'remarks', 'femaleEntryCode', 'femaleEntryNumber', 'maleEntryCode', 'maleEntryNumber'], 'string'],
            [['crossDbId', 'crossFemaleParent', 'crossMaleParent', 'crossingDate', 'crossMethod', 'crossMethodAbbrev', 'crossName', 'femaleGermplasmState', 'femaleParentage', 'femaleParentSeedSource', 'femaleSourceSeedEntry', 'femaleSourcePlot', 'harvestStatus', 'maleGermplasmState', 'maleParentage', 'maleParentSeedSource', 'maleSourceSeedEntry', 'maleSourcePlot', 'remarks', 'femaleEntryCode', 'femaleEntryNumber', 'maleEntryCode', 'maleEntryNumber'], 'safe'],
            [[], 'boolean'],
            [[], 'unique']
        ];
    }

    /**
     * Retrieve plot records to display
     * @param object params POST parameters
     * @param integer experimentId experiment identifier
     * @param integer occurrenceId occurrence identifier
     * @param integer locationId location identifier
     * @param object browserConfig browser configurations
     * @param boolean loadData  whether or not to load data
     * @param boolean hidden states if the browser is hidden
     * @return object containing data provider and array of germplasm states
     */
    public function search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden) {
        // If data is not to be loaded or if browser is hidden, return empty array data provider
        if(!$loadData || $hidden) {
            return array (
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => [],
                    'key' => 'crossDbId',
                    'restified' => true,
                    'totalCount' => 0
                ])
            );
        }

        // Check if browser pagination and fitlers will be reset
        $reset = $this->harvestManagerModel->browserReset($occurrenceId, $params, 'CreationCrossBrowserModel');
        $resetPage = $reset['resetPage'];
        $resetFilters = $reset['resetFilters'];

        // If resetFilters = true, set params to null
        if($resetFilters) {
            $params = null;
        }
        // Load params into the browser search model
        $this->load($params);

        // Assemble URL parameters
        $urlParams = $this->harvestManagerModel->assembleUrlParameters(
            'dynagrid-harvest-manager-cross-crt-grid',
            'sort=crossDbId:ASC',
            $resetPage
        );

        // Assemble browser filters
        $columnFilters = $this->assembleBrowserFilters($experimentId);

        // Get crosses
        $result = $this->crossBrowserModel->getCrossRecords($occurrenceId, $columnFilters, $urlParams);
        $crosses = $result['data'];
        $totalCount = $result['count'];

        // If no crosses are retrieved and totalCount == 0, reset pagination.
        if(count($crosses) == 0 && $totalCount == 0){
            // Assemble URL parameters
            $urlParams = $this->harvestManagerModel->assembleUrlParameters(
                'dynagrid-harvest-manager-cross-crt-grid',
                'sort=crossDbId:ASC',
                true
            );

            // Get crosses
            $result = $this->crossBrowserModel->getCrossRecords($occurrenceId, $columnFilters, $urlParams);
            $crosses = $result['data'];
            $totalCount = $result['count'];
        }

        // get appropriate harvest data configs for plots
        $crosses = $this->harvestManagerModel->matchConfigs($crosses, $occurrenceId, 'cross');

        $crossesCount = count($crosses);
        for ($i = 0; $i < $crossesCount; $i++) {
            // validate crosses with complete harvest data and generate appropriate remarks
            $crosses[$i] =  $this->creationModel->buildHarvestRemarksColumn($crosses[$i]);
        }

        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'id' => 'dynagrid-harvest-manager-cross-crt-grid',
            'allModels' => $crosses,
            'key' => 'crossDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);
        // Initialize sort
        $sort = new Sort();
        $sort->attributes = [
            'crossDbId'=>[
                'asc' => ["crossDbId"=>SORT_ASC],
                'desc' => ["crossDbId"=>SORT_DESC]
            ],
            'crossName'=>[
                'asc' => ["crossName"=>SORT_ASC],
                'desc' => ["crossName"=>SORT_DESC]
            ],
            'crossFemaleParent'=>[
                'asc' => ["crossFemaleParent"=>SORT_ASC],
                'desc' => ["crossFemaleParent"=>SORT_DESC]
            ],
            'crossMaleParent'=>[
                'asc' => ["crossMaleParent"=>SORT_ASC],
                'desc' => ["crossMaleParent"=>SORT_DESC]
            ],
            'crossMethod'=>[
                'asc' => ["crossMethod"=>SORT_ASC],
                'desc' => ["crossMethod"=>SORT_DESC]
            ],
            'remarks'=>[
                'asc' => ["remarks"=>SORT_ASC],
                'desc' => ["remarks"=>SORT_DESC]
            ],
            'crossingDate'=>[
                'asc' => ["crossingDate"=>SORT_ASC],
                'desc' => ["crossingDate"=>SORT_DESC]
            ],
            'femaleSourceSeedEntry'=>[
                'asc' => ["femaleSourceSeedEntry"=>SORT_ASC],
                'desc' => ["femaleSourceSeedEntry"=>SORT_DESC]
            ],
            'maleSourceSeedEntry'=>[
                'asc' => ["maleSourceSeedEntry"=>SORT_ASC],
                'desc' => ["maleSourceSeedEntry"=>SORT_DESC]
            ],
            'femaleParentSeedSource'=>[
                'asc' => ["femaleParentSeedSource"=>SORT_ASC],
                'desc' => ["femaleParentSeedSource"=>SORT_DESC]
            ],
            'maleParentSeedSource'=>[
                'asc' => ["maleParentSeedSource"=>SORT_ASC],
                'desc' => ["maleParentSeedSource"=>SORT_DESC]
            ],
            'femaleSourcePlot'=>[
                'asc' => ["femaleSourcePlot"=>SORT_ASC],
                'desc' => ["femaleSourcePlot"=>SORT_DESC]
            ],
            'maleSourcePlot'=>[
                'asc' => ["maleSourcePlot"=>SORT_ASC],
                'desc' => ["maleSourcePlot"=>SORT_DESC]
            ],
            'femaleGermplasmState'=>[
                'asc' => ["femaleGermplasmState"=>SORT_ASC],
                'desc' => ["femaleGermplasmState"=>SORT_DESC]
            ],
            'maleGermplasmState'=>[
                'asc' => ["maleGermplasmState"=>SORT_ASC],
                'desc' => ["maleGermplasmState"=>SORT_DESC]
            ],
            'femaleParentage'=>[
                'asc' => ["femaleParentage"=>SORT_ASC],
                'desc' => ["femaleParentage"=>SORT_DESC]
            ],
            'maleParentage'=>[
                'asc' => ["maleParentage"=>SORT_ASC],
                'desc' => ["maleParentage"=>SORT_DESC]
            ],
            'femaleEntryCode'=>[
                'asc' => ["femaleEntryCode"=>SORT_ASC],
                'desc' => ["femaleEntryCode"=>SORT_DESC]
            ],
            'femaleEntryNumber'=>[
                'asc' => ["femaleEntryNumber"=>SORT_ASC],
                'desc' => ["femaleEntryNumber"=>SORT_DESC]
            ],
            'maleEntryCode'=>[
                'asc' => ["maleEntryCode"=>SORT_ASC],
                'desc' => ["maleEntryCode"=>SORT_DESC]
            ],
            'maleEntryNumber'=>[
                'asc' => ["maleEntryNumber"=>SORT_ASC],
                'desc' => ["maleEntryNumber"=>SORT_DESC]
            ]
        ];
        // Apply sort to data provider
        $dataProvider->setSort($sort);

        return array(
            'dataProvider' => $dataProvider,
        );
    }

    /**
     * Assemble browser filters
     * @param integer experimentId experiment identifier
     * @return object columnFilters browser filters in API request body format
     */
    public function assembleBrowserFilters($experimentId)
    {
        $browserColumns = [
            'experimentDbId' => "equals $experimentId",
            'femaleGermplasmState' => 'not equals unknown',
            'harvestStatus' => 'equals NO_HARVEST||INCOMPLETE%||CONFLICT%', // include crosses with incomplete harvest data
            'terminalHarvestMethod' => '[OR] not null',
            'harvestMethod' => '[OR] not null',
        ];

        $generatedBrowserColumns = $this->creationModel->generateBowserColumns();

        if (isset($generatedBrowserColumns) && !empty($generatedBrowserColumns)) {
            foreach ($generatedBrowserColumns as $column) {
                $browserColumns[$column] = "[OR] not null";
            };
        }

        return [
            'conditions' => [
                [
                    'experimentDbId' => "equals $experimentId",
                    'femaleGermplasmState' => 'not equals unknown',
                    'harvestStatus' => 'equals READY||equals QUEUED_FOR_HARVEST||equals BAD_QC_CODE||equals HARVEST_IN_PROGRESS'
                ],
                $browserColumns
            ],
            'sort' => [
                "attribute" => "harvestStatus",
                "sortValue" => "DONE|HARVEST_IN_PROGRESS|QUEUED_FOR_HARVEST|READY|NO_HARVEST|INCOMPLETE%"
            ]
        ];
    }
}