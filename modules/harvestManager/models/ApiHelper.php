<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import interface
use app\interfaces\modules\harvestManager\models\IApiHelper;

/**
 * Class for HM's own API Helper
 */
class ApiHelper implements IApiHelper {

    /**
     * Model constructor
     */
    public function __construct()
    {
        
    }

    /**
     * Sends a request to the API.
     * This is used for API calls that do not have a model class.
     * (eg. locations/:id/harvest-plot-data-search)
     * @param String $method HTTP method of the request
     * @param String $endpoint API endpoint 
     * @param Array $requestBody array containing body parameters
     * @param Array $queryParams array containing URL parameters
     * @param String $innerData field name of inner data in the case of search calls under an endpoint (eg. plots/:id/data-search)
     * @return Array the result of the request
     */
    public function sendRequest($method, $endpoint, $requestBody = null, $queryParams = null, $innerData = null) {
        if(isset($requestBody) && count($requestBody)>0) {
            $requestBody = json_encode($requestBody,JSON_UNESCAPED_SLASHES);
            $requestBody = str_replace("\\","\\\\",$requestBody);
        }
        else $requestBody = null;
        
        $queryParamString  = '';
        if($queryParams!=null) {
            $queryParamString .= '?' . implode('&',$queryParams);
        }
        
        $response = \Yii::$app->api->getResponse($method, $endpoint . $queryParamString, $requestBody);
        $data = null;

        if (
            isset($response['status'])
            && $response['status'] == 200
            && isset($response['body']['result']['data'])
        ) {
            if(!empty($innerData)) {
                $mainData = $response['body']['result']['data'];
                $data = $response['body']['result']['data'][0][$innerData];
            }
            else $data = $response['body']['result']['data'];

            $totalPages = $response['body']['metadata']['pagination']['totalPages'];
            
            if($totalPages>1) {
                $addlData = null;
                for($i=2;$i<=$totalPages;$i++) {
                    $queryParamString  = '?';
                    if($queryParams!=null) 
                        $queryParamString .= implode('&',$queryParams) . '&';
                    $queryParamString .= 'page=' . $i;

                    $response = \Yii::$app->api->getResponse($method, $endpoint . $queryParamString, $requestBody);

                    if (
                        isset($response['status'])
                        && $response['status'] == 200
                        && isset($response['body']['result']['data'])
                    ) {
                        if(!empty($innerData)) $addlData = $response['body']['result']['data'][0][$innerData];
                        else $addlData = $response['body']['result']['data'];
                    }
                    
                    if(!empty($innerData)) {
                        $data = array_merge($data,$addlData);
                        $mainData[0][$innerData] = $data;
                    }
                    else $data = array_merge($data,$addlData);
                }
            }
        } else {
            $data = null;
        }

        if(!empty($innerData)) $data = $mainData;

        return $data==null ? [] : $data;
    }

    /**
     * Sends a request to the API (single page).
     * This is used for API calls that do not
     * have a model class (eg. locations/:id/harvest-plot-data-search)
     * @param string method HTTP method of the request
     * @param string endpoint API endpoint 
     * @param array requestBody array containing body parameters
     * @param array queryParams array containing URL parameters
     * @return array the result of the request
     */
    public function sendRequestSinglePage($method, $endpoint, $requestBody = null, $queryParams = null) {
        if(isset($requestBody) && count($requestBody)>0) {
            $requestBody = json_encode($requestBody,JSON_UNESCAPED_SLASHES);
            $requestBody = str_replace("\\","\\\\",$requestBody);
        }
        else {
            $requestBody = null;
        }
        $queryParamString  = '?';
        if($queryParams!=null) {
            $queryParamString .= implode('&',$queryParams);
        }

        $response = \Yii::$app->api->getResponse($method, $endpoint . $queryParamString, $requestBody);

        $data = [];
        $totalCount = 0;
        $message = 'Error in fetching data';
        $status = 500;

        if(isset($response['status']) && isset($response['body']['result']['data'])){
            $status = $response['status'];
            $data = $response['body']['result']['data'];
            $totalCount = $response['body']['metadata']['pagination']['totalCount'];
            $message = $response['body']['metadata']['status'][0]['message'];

        }

        return ["status" => $status, "data" => $data, "count" => $totalCount, "message" => $message];
    }

    /**
    * Use HTTP requests to GET, PUT, POST and DELETE data
    * 
    * @param string method request method (POST, GET, PUT, etc)
    * @param string path url path request
    * @param array rawData (optional) request content
    * @param string filter (optional) sort, page or limit options 
    * @param boolean retrieveAll (optional) whether all of the data will be fetched or not
    *
    * @return array list of requested information
    */
    public function getParsedResponse($method, $path, $rawData = null, $filter = '', $retrieveAll = false) {
        return \Yii::$app->api->getParsedResponse($method, $path, $rawData, $filter, $retrieveAll);
    }

    /**
     * Function for bulk operations using processor
     * @param string httpMethod HTTP method to be used
     * @param string endpoint endpoint where bulk operation will be performed
     * @param array dbIds array containing database identifiers of records included in the bulk operation
     * @param string entity the entity where the record belongs (eg. germplasm)
     * @param integer entityDbId entity identifier
     * @param string endpointEntity the specific endpoint in the entity (eg. seed, package, etc.)
     * @param string application the application performing the bulk operation (eg. Harvest Manager)
     * @param string optional optional data for the bulk operations (eg. dependents, requestData)
     * @return object result of the API access
     */
    public function processorBuilder($httpMethod, $endpoint, $dbIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional = []) {
        if(!in_array($httpMethod,["PUT","DELETE"])) return null;
        // Build ids string
        $dbIds = implode("|",$dbIds);
        // Build processor request body
        $requestBody = array(
            "httpMethod" => $httpMethod,
            "endpoint" => $endpoint,
            "dbIds" => $dbIds,
            "description" => $description,
            "entity" => $entity,
            "entityDbId" => $entityDbId,
            "endpointEntity" => $endpointEntity,
            "application" => $application
        );
        // If method is PUT, check of request data exists
        if($httpMethod=="PUT" && isset($optional['requestData'])) {
            $requestBody['requestData'] = $optional['requestData'];
        }
        // If method is DELETE, check of dependents exist
        if($httpMethod=="DELETE" && isset($optional['dependents'])) {
            $requestBody['dependents'] = $optional['dependents'];
        }

        // Get token and refresh token from session
        $token = \Yii::$app->session->get('user.b4r_api_v3_token');
        $refreshToken = \Yii::$app->session->get('user.refresh_token');
        $requestBody["tokenObject"] = [
            "token" => $token,
            "refreshToken" => $refreshToken
        ];

        return $this->processorOperation($requestBody);
    }

    /**
     * Send request to the processor endpoint
     * @param object requestBody object containing request body parameters
     * @return object result of the API access
     */
    public function processorOperation($requestBody) { 
        $method  = "POST";
        $endpoint = "processor";
        
        return $this->sendRequest($method,$endpoint,$requestBody);
    }
}
