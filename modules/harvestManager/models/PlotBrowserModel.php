<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import yii
use yii\data\Sort;
// Import data providers
use app\dataproviders\ArrayDataProvider;
// Import interfaces
use app\interfaces\modules\harvestManager\models\IApiHelper;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\harvestManager\models\IPlotBrowserModel;
// Import models
use app\models\Plot;

/**
* Model class for Plot browser
*/
class PlotBrowserModel extends Plot implements IPlotBrowserModel
{
    public $designation;
    public $entryNumber;
    public $entryCode;
    public $harvestStatus;
    public $parentage;
    public $plotCode;
    public $plotDbId;
    public $plotNumber;
    public $rep;
    public $state;
    public $type;

    /**
     * Model constructor
     */
    public function __construct(
        public IApiHelper $apiHelper, 
        public IHarvestManagerModel $harvestManagerModel)
    { }

    public function rules()
    {
        return [
            [['designation', 'plotDbId', 'plotNumber', 'rep', 'state', 'type'], 'required'],
            [['plotDbId', 'plotNumber', 'entryNumber', 'entryCode', 'rep'], 'number'],
            [['plotDbId', 'plotNumber', 'entryNumber', 'entryCode', 'rep'], 'integer'],
            [['plotCode', 'designation', 'parentage', 'state', 'type'], 'string'],
            [['designation', 'entryNumber', 'entryCode', 'parentage', 'plotCode', 'plotDbId', 'plotNumber', 'rep', 'state', 'type'], 'safe'],
            [[], 'boolean'],
            [['plotDbId'], 'unique']
        ];
    }

    /**
     * Retrieve plot records to display
     * @param object params browser parameters
     * @param integer experimentId experiment identifier
     * @param integer  occurrenceId occurrence identifier
     * @param integer locationId location identifier
     * @return object containing data provider and array of germplasm states
     */
    public function search($params, $experimentId, $occurrenceId, $locationId) {
        // Check if browser pagination and fitlers will be reset
        $reset = $this->harvestManagerModel->browserReset($occurrenceId, $params, 'PlotBrowserModel');
        $resetPage = isset($reset['resetPage']) ? $reset['resetPage'] : false;
        $resetFilters = isset($reset['resetFilters'])  ? $reset['resetFilters'] : false;

        // If resetFilters = true, set params to null
        if($resetFilters) {
            $params = null;
        }
        // Load params into the browser search model
        $this->load($params);

        // Assemble URL parameters
        $urlParams = $this->harvestManagerModel->assembleUrlParameters(
            'dynagrid-harvest-manager-plot-hdata-grid',
            'sort=plotNumber:ASC',
            $resetPage
        );

        // Save url params to session for potential use in bulk operations -> apply to all
        $sessionSort = null;
        foreach($urlParams as $urlParam) {
            if(str_contains($urlParam, "sort=")) {
                $sessionSort = $urlParam;
                break;
            }
        }

        // Assemble browser filters
        $columnFilters = $this->assembleBrowserFilters($params, $occurrenceId);

        // Save sort and filter to session
        \Yii::$app->cbSession->set("plotBrowserParams-$occurrenceId", [
            "sort" => $sessionSort,
            "filter" => $columnFilters
        ]);

        // Get plots
        $result = $this->getPlotRecords($occurrenceId, $columnFilters, $urlParams);
        $plots = $result['data'];
        $totalCount = $result['count'];

        // If no plots are retrieved, reset the pagination.
        if(count($plots) == 0){
            // Assemble URL parameters
            $urlParams = $this->harvestManagerModel->assembleUrlParameters(
                'dynagrid-harvest-manager-plot-hdata-grid',
                'sort=plotNumber:ASC',
                true
            );

            // Get plots
            $result = $this->getPlotRecords($occurrenceId, $columnFilters, $urlParams);
            $plots = $result['data'];
            $totalCount = $result['count'];
        }

        // Get appropriate harvest data configs for the plots
        $plots = $this->harvestManagerModel->matchConfigs($plots, $occurrenceId, 'plot');

        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'allModels' => $plots,
            'key' => 'plotDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);

        // Initialize sort
        $sort = new Sort();
        $sort->attributes = [
            'plotNumber'=>[
                "asc" => ["plotNumber"=>SORT_ASC],
                'desc' => ["plotNumber"=>SORT_DESC]
            ],
            'rep'=>[
                "asc" => ["rep"=>SORT_ASC],
                'desc' => ["rep"=>SORT_DESC]
            ],
            'plotCode'=>[
                "asc" => ["plotCode"=>SORT_ASC],
                'desc' => ["plotCode"=>SORT_DESC]
            ],
            'entryNumber'=>[
                "asc" => ["entryNumber"=>SORT_ASC],
                'desc' => ["entryNumber"=>SORT_DESC]
            ],
            'entryCode'=>[
                "asc" => ["entryCode"=>SORT_ASC],
                'desc' => ["entryCode"=>SORT_DESC]
            ],
            'parentage'=>[
                "asc" => ["parentage"=>SORT_ASC],
                'desc' => ["parentage"=>SORT_DESC]
            ],
            'state'=>[
                "asc" => ["state"=>SORT_ASC],
                'desc' => ["state"=>SORT_DESC]
            ],
            'type'=>[
                "asc" => ["type"=>SORT_ASC],
                'desc' => ["type"=>SORT_DESC]
            ],
            'designation'=>[
                "asc" => ["designation"=>SORT_ASC],
                'desc' => ["designation"=>SORT_DESC]
            ]
        ];
        // Set default order
        $sort->defaultOrder = [
            'plotNumber' => SORT_ASC
        ];
        // Apply sort to data provider
        $dataProvider->setSort($sort);

        return array(
            'dataProvider' => $dataProvider,
            'germplasmStates' => []
        );
    }

    /**
     * Retrieves plots of the current occurrence in the given location
     * @param integer occurrenceId occurrence identifier
     * @param object requestBody column filters
     * @param array urlParams url params for sorting, pagination, and page size
     */
    public function getPlotRecords($occurrenceId, $requestBody, $urlParams) {
        $method = 'POST';
        $endpoint = "occurrences/$occurrenceId/harvest-plot-data-search";

        $result = $this->apiHelper->sendRequestSinglePage($method, $endpoint, $requestBody, $urlParams);
        // Trim values
        $result['data'] = $this->harvestManagerModel->trimValues($result['data']);
        return $result;
    }

    /**
     * Retrieve plots given the request body and url parameters
     * @param integer occurrenceId occurrence identifier
     * @param object requestBody column filters
     * @param array urlParams url params for sorting, pagination, and page size
     */
    public function searchAllPlots($occurrenceId, $requestBody = null, $urlParams = null) {
        $method = 'POST';
        $endpoint = "occurrences/$occurrenceId/harvest-plot-data-search";

        $plots = $this->apiHelper->sendRequest($method, $endpoint, $requestBody, $urlParams);
        // Return plots with trimmed values
        return $this->harvestManagerModel->trimValues($plots);
    }

    /**
     * Retrieves the distinct values for the specified field of the plots
     * given the occurrence id
     * @param integer occurrenceId occurrence identifier
     * @param string field name of the field for which distinct values will be retrieved
     */
    public function getDistinctField($occurrenceId, $field) {
        // Get distinct values
        $requestBody = [
           "distinctOn" => $field
       ];
       $result = $this->searchAllPlots($occurrenceId, $requestBody);
       
       // Loop through the plots
       $values = [];
       foreach($result as $plot) {
           $values[] = $plot[$field];
       }

       return $values;
    }

    /**
     * Assemble browser filters
     * @param object params parameters passed by data browser
     * @param integer occurrenceId occurrence identifier
     * @return object columnFilters browser filters in API request body format
     */
    public function assembleBrowserFilters($params, $occurrenceId) {
        $filters = isset($params['PlotBrowserModel']) ? $params['PlotBrowserModel'] : [];
        $columnFilters = [];
        $invalidStateFilter = '';
        $noHarvestFilter = '';

        // Retrieve supported states
        $stateScale = $this->harvestManagerModel->getScaleValues('GERMPLASM_STATE');
        // Remove 'unknown' state from scale values
        unset($stateScale['unknown']);
        // Get array of supported states from scale
        $supportedStates = array_values($stateScale);
        foreach($filters as $key => $value) {
            if($value!="") {
                $equalsString = '';
                if(!is_array($value) && !str_contains($value,'%')) $equalsString = 'equals ';
                if($key=='plotNumber' && str_contains($value,'-')){
                    $columnFilters[$key] = "range: ".$value;
                }
                else {
                    if(is_array($value)) {
                        $columnFilters[$key] = $equalsString . implode('|' . $equalsString,$value);
                    }
                    else {
                        $columnFilters[$key] = $equalsString . $value;
                    }
                }
            }   
        }


        // Check if observation data filters are set
        $sessionVar = \Yii::$app->cbSession->get("observation-filter-plot-".$occurrenceId);
        if(!empty($sessionVar)) {
            $observationFilter = isset($sessionVar) ? (array) json_decode($sessionVar) : [];
            $fields = $observationFilter['fields'];
            $withData = $observationFilter['withData'];
            $or = $withData ? '[OR] ' : '';

            foreach($fields as $field) {
                $terminalField = 'terminal' . ucfirst($field);
                $columnFilters[$field] = $or . ($withData ? ' not' : '') . " null";
                $columnFilters[$terminalField] = $or . ($withData ? ' not' : '') . " null";
            }
        }

        // Get post parameters
        $postParams = \Yii::$app->request->post();

        // Get harvest status filters from session
        $hsSession = \Yii::$app->cbSession->get("harvest-status-plot-" . $occurrenceId);
        // Set default status.
        // Defaults to NO_HARVEST (on initial page load).
        // If 'NO_HARVEST' is not in the distinct status values,
        // Select the first status available as default.
        $default = ['NO_HARVEST'];
        $distinctHarvestStatus = $params['distinctHarvestStatus'] ?? [];
        if (!empty($distinctHarvestStatus)) {
            if (!in_array('NO_HARVEST', $distinctHarvestStatus)) {
                $default = [$distinctHarvestStatus[0]];
            }
        }
        // If session variable is not yet set, use default value.
        if (!isset($hsSession) && !isset($postParams['harvestStatus'])) {
            $hsSession = $default;
            \Yii::$app->cbSession->set("harvest-status-plot-" . $occurrenceId, $hsSession);
        }
        // If post params contain filters for harvest status, update session variable.
        if (isset($postParams['harvestStatus'])) {
            $hsSession = json_decode($postParams['harvestStatus']);
            \Yii::$app->cbSession->set("harvest-status-plot-" . $occurrenceId, $hsSession);
        }
        // Build harvest status filter
        $harvestStatusFilters = [];
        foreach ($hsSession as $status) {
            if(in_array($status, ["INCOMPLETE","CONFLICT"])) {
                $harvestStatusFilters[] = $status . "%";
                continue;
            }
            else if($status == "INVALID_STATE") {
                $harvestStatusFilters[] = "[OR] equals ".$status;
                $invalidStateFilter = "equals unknown";
                continue;
            }
            else if($status == "NO_HARVEST") {
                $harvestStatusFilters[] = "equals ".$status;
                $noHarvestFilter = "equals " . implode("|equals ", $supportedStates);
                continue;
            }
            $harvestStatusFilters[] = "equals ".$status;
        }
        // Check if invalid state filter is set
        if(!empty($invalidStateFilter)) {
            if(!empty($columnFilters['state'])) {
                if(isset($columnFilters['state']) && $invalidStateFilter != $columnFilters['state']) {
                    $columnFilters['state'] = 'equals none';
                }
                else {
                    $columnFilters['state'] = '[OR] ' . $invalidStateFilter;
                }
            }
            else {
                $columnFilters['state'] = '[OR] ' . $invalidStateFilter;
            }
        }
        // Check if no_harvest is selected
        if(!empty($noHarvestFilter)) {
            if(empty($columnFilters['state'])) {
                $columnFilters['state'] = $noHarvestFilter;
            }
        }
        if (!empty($harvestStatusFilters)) {
            $columnFilters['harvestStatus'] = implode("|", $harvestStatusFilters);
        }

        // Get range filters from session
        $sessToolbarFilters = \Yii::$app->cbSession->get("toolbar-filter-plot-" . $occurrenceId);
        // If session variable is not yet set, use default value (empty array).
        if (!isset($sessToolbarFilters)) {
            $sessToolbarFilters = [];
            \Yii::$app->cbSession->set("toolbar-filter-plot-" . $occurrenceId, $sessToolbarFilters);
        }
        // Check if has toolbar filter
        if(isset($postParams['toolbarFilters']) && !empty($postParams['toolbarFilters'])){
            $sessToolbarFilters = $postParams['toolbarFilters'];
            \Yii::$app->cbSession->set("toolbar-filter-plot-" . $occurrenceId, $sessToolbarFilters);
        }

        // Reset range filter if removeRangeFilter is set to true
        if(!isset($postParams['toolbarFilters']) && isset($postParams['removeRangeFilter']) && strtolower($postParams['removeRangeFilter']) == 'true'){
            $sessToolbarFilters = [];
            \Yii::$app->cbSession->set("toolbar-filter-plot-" . $occurrenceId, $sessToolbarFilters);
        }

        // Get toolbar filters
        $toolbarFilters = [];
        if(!empty($sessToolbarFilters)){
            foreach ($sessToolbarFilters as $filter=>$values) {
                $toolbarFilters[$filter] = $values;
            }
        }

        // Build column filters from toolbar filters
        if(!empty($toolbarFilters)){
            foreach ($toolbarFilters as $ind=>$val) {
                $columnFilters[$ind] = $val;
            }
        }

        return $columnFilters;
    }
}