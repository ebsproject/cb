<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import Yii
use yii\helpers\Html;
// Import data providers
use app\dataproviders\ArrayDataProvider;
// Import interfaces
use app\interfaces\models\IOccurrence;
use app\interfaces\models\ITerminalTransaction;
use app\interfaces\models\IUser;
use app\interfaces\models\IVariable;
use app\interfaces\modules\dataCollection\models\ITransaction;
use app\interfaces\modules\harvestManager\models\ICreationModel;
use app\interfaces\modules\harvestManager\models\ICrossBrowserModel;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel;
use app\interfaces\modules\harvestManager\models\IPlotBrowserModel;

/**
 * Model class for Creation
 */
class CreationModel implements ICreationModel
{
    /**
     * Model constructor
     */
    public function __construct(
        public IOccurrence $occurrence,
        public ICrossBrowserModel $crossBrowserModel,
        public IHarvestManagerModel $harvestManagerModel,
        public IPlotBrowserModel $plotBrowserModel,
        public ITerminalTransaction $terminaltransaction,
        public IOccurrenceDetailsModel $occurrenceDetailsModel,
        public ITransaction $transaction,
        public IUser $userModel,
        public IVariable $variableModel
    ) {}

    /**
     * Generates buttons for the creation module actions (commit, create, notifs)
     * @param boolean twoBrowsers whether or not two browsers are to be rendered
     * @return string html button elements
     */
    public function getActionButtons($twoBrowsers = false, $occurrenceIsHarvestCompleted = false) {
        // Build harvest complete button text
        $completeHarvestButtonText = $occurrenceIsHarvestCompleted ? \Yii::t('app', 'Undo Complete Harvest') : \Yii::t('app', 'Complete Harvest');

        // Return button elements
        return Html::a(
            $completeHarvestButtonText,
            '#',
            [
                'class' => 'btn btn-default hm-tooltipped',
                'id' => 'hm-crt-complete-harvest-btn',
                'style' => "margin-right:5px;background-color: #5cb85c !important;margin-left:10px;",
                'data-position' => 'top',
                'data-tooltip' => $completeHarvestButtonText,
                'data-toggle' => 'modal',
                'data-target' => '#hm-complete-harvest-modal'
            ]
        )
        .Html::a(
            \Yii::t('app', 'Commit'),
            '#',
            [
                'class' => 'btn waves-effect waves-light white-text light-blue darken-3 hm-tooltipped',
                'id' => 'hm-crt-commit-data-btn',
                'data-position' => 'top',
                'data-tooltip' => \Yii::t('app', 'Commit harvest data'),
                'data-toggle' => 'modal',
                'data-target' => '#hm-harvest-data-commit-modal'
            ]
        )
            .
            Html::a(
                \Yii::t('app', 'Create'),
                '#',
                [
                    'class' => 'btn waves-effect waves-light white-text light-green darken-3 hm-tooltipped',
                    'id' => 'hm-crt-create-package-btn',
                    'data-position' => 'top',
                    'data-tooltip' => \Yii::t('app', 'Create  packages, seeds, and germplasm'),
                    'data-toggle' => 'modal',
                    'data-target' => '#hm-harvest-record-creation-modal'
                ]
            ) .
            Html::a(
                '<i class="material-icons large">notifications_none</i>
            <span id="creation-notif-badge-id" class=""></span><span id="notif-badge-id" class=""></span>',
                '#',
                [
                    'id' => 'hm-crt-notifs-btn',
                    'class' => 'btn waves-effect waves-light harvest-manager-notification-button-creation hm-tooltipped',
                    'data-pjax' => 0,
                    'style' => "overflow: visible !important;",
                    "data-position" => "top",
                    "data-tooltip" => \Yii::t('app', 'Notifications'),
                    "data-activates" => "notifications-dropdown"
                ]
            );
    }

    /**
     * Determines which browser data must be loaded
     * @param string pageUrl current page URL string
     * @param array postParams $_POST parameters of the request
     * @return object containing loadPlots and loadCrosses boolean values
     */
    public function determineDataToLoad($pageUrl, $postParams)
    {
        // Pjax ids
        $plotGridPjax = 'dynagrid-harvest-manager-plot-crt-grid-pjax';
        $crossGridPjax = 'dynagrid-harvest-manager-cross-crt-grid-pjax';

        // Get target from POST
        $targetBrowserPjax = isset($postParams['target']) ? $postParams['target'] : '';

        // Boolean values
        $urlContainsPlotPjax = strpos($pageUrl, $plotGridPjax) !== false;
        $targetEqualsPlotPjax = $targetBrowserPjax == $plotGridPjax;
        $urlContainsCrossPjax = strpos($pageUrl, $crossGridPjax) !== false;
        $targetEqualsCrossPjax = $targetBrowserPjax == $crossGridPjax;

        // If the target is not set and the url does not contain the view parameter,
        // load both plots and crosses
        if (empty($targetBrowserPjax) && (!str_contains($pageUrl, 'view'))) {
            return [
                "loadPlots" => true,
                "loadCrosses" => true
            ];
        }

        $loadPlots = true;
        $loadCrosses = true;
        // If url and target does not contain plot grid pjax, set loadPlots to FALSE.
        if (!$urlContainsPlotPjax && !$targetEqualsPlotPjax) {
            $loadPlots = false;
        }
        // If url and target does not contain cross grid pjax, set loadCrosses to FALSE.
        if (!$urlContainsCrossPjax && !$targetEqualsCrossPjax) {
            $loadCrosses = false;
        }
        // If url and target does not contain both plot and cross grid pjax
        // set both load variables to TRUE. This happens when the page is reloaded.
        // In this case, data for both browsers must be loaded.
        if (!$urlContainsPlotPjax && !$targetEqualsPlotPjax && !$urlContainsCrossPjax && !$targetEqualsCrossPjax) {
            $loadPlots = true;
            $loadCrosses = true;
        }

        return [
            "loadPlots" => $loadPlots,
            "loadCrosses" => $loadCrosses
        ];
    }

    /**
     * Renders harvest data columns (harvest date and harvest method)
     * in the creation tab browsers.
     *
     * This method is responsible for rendering the specified harvest data column
     * with the provided terminal and committed values. It also takes into account
     * the model object from the data provider and whether the column is required.
     *
     * @param string $columnName The name of the column to render.
     * @param string $terminal The terminal (uncommitted) value for the variable.
     * @param string $committed The committed value for the variable.
     * @param object $model The model object from the data provider.
     * @param bool $required Whether the column is required.
     * @return string The rendered HTML for the harvest data column.
     */
    public function renderHarvestDataColumn($columnName, $terminal, $committed, $model, $required)
    {
        // Produce properly capitalized column name
        $splitName = preg_split('/(?=[A-Z])/', $columnName);
        $splitName[0] = ucfirst($splitName[0]);
        $columnName = join(" ", $splitName);

        $htmlElement = [];

        // If committed has value, render in green
        if ($committed != null && $committed != "") {
            $htmlElement[] = "<span title='Committed $columnName' class='badge center light-green darken-4 white-text' style='margin: 0px 0px 5px 0px;'>" . $committed . "</span>";
        }
        // If terminal has value, render in yellow
        if ($terminal != null && $terminal != "") {
            $htmlElement[] = "<span title='Uncommitted $columnName' class='badge center amber grey-text text-darken-2' style='margin: 0px 0px 5px 0px;'>" . $terminal . "</span>";
        }

        // Return final element
        if (!empty($htmlElement)) {
            return join('<br>', $htmlElement);
        }

        // If harvest data is not required, mark value as "not required"
        if(!$required) {
            return "<span title='Input for $columnName is not required' class='badge center grey lighten-2 text-darken-3' style='margin: 0px 0px 5px 0px;'>NR</span>";
        }

        // If harvest data is valid, mark unset value as "not required"
        if ($model['harvestDataValid']) {
            return "<span title='Input for $columnName is not required' class='badge center grey lighten-2 text-darken-3' style='margin: 0px 0px 5px 0px;'>NR</span>";
        }

        // Else, render missing input
        return "<span title='Missing input for $columnName' class='badge center white red-text text-darken-3' style='margin: 0px 0px 5px 0px;'>?</span>";
    }

    /**
     * Renders the numeric variable column in the creation tab browsers.
     * @param string terminalMethod uncommitted harvest method
     * @param string committedMethod committed harvest method
     * @param object methodNumVarComat method numvar compatibility array
     * @param object numVars field names of each numeric variable abbrev
     * @param object methodNumVarCompatReq compatibility object for all harvest methods with a required numeric variable
     */
    public function renderNumericVariableColumn($terminalMethod, $committedMethod, $methodNumVarCompat, $numVars, $model, $methodNumVarCompatReq = [])
    {
        $htmlElement = [];
        $methodRequiringNumVar = array_keys($methodNumVarCompatReq);

        // If harvest method is not set, display NA
        if (empty($committedMethod) && empty($terminalMethod)) {
            return "<span title='Harvest method is not set' class='badge center grey lighten-2 text-darken-3' style='margin: 0px 0px 5px 0px;'>NA</span>";
        }

        // If committed harvest method is set, display the committed required numeric variable
        if (!empty($committedMethod)) {
            $requiredNumVar = $methodNumVarCompat[$committedMethod] ?? [];

            $updatedCommittedMethod = "";
            if (empty($requiredNumVar)) {
                $updatedCommittedMethod = strtoupper(str_replace(" ", "_", $committedMethod));
                $requiredNumVar = $methodNumVarCompat[$updatedCommittedMethod] ?? [];
            }

            // check variable scale value data
            if (empty($requiredNumVar)) {
                [$requiredNumVar, $numVars, $updatedCommittedMethod] = $this->extractFromScaleValues(
                    $committedMethod,
                    $model,
                    $numVars,
                    $methodNumVarCompat
                );
            }

            // check and update numVars if numeric variable is not included
            $updatedCommittedMethod = empty($updatedCommittedMethod) ? strtoupper(str_replace([" ", "-"], "_", $committedMethod)) : $updatedCommittedMethod;
            $numVars = $this->updateNumericVariables($requiredNumVar, $numVars, $model, $updatedCommittedMethod);

            // If method requires a numeric variable, display
            if (!empty($requiredNumVar)) {
                // If optional = false, required numeric var must not be empty
                // Otherwise, allow it to be empty. "Required" means that the
                // input for the numeric variable will be shown.
                $optional = false;
                // Check if optional. Set flag properly.
                if (
                    in_array("", $requiredNumVar)
                    || (!in_array($committedMethod, $methodRequiringNumVar)
                        && !in_array($updatedCommittedMethod, $methodRequiringNumVar))
                ) {
                    $optional = true;
                }
                foreach ($requiredNumVar as $req) {
                    if (empty($req)) continue;
                    $committedField = $numVars[$req];
                    $committedNumVar = $model[$committedField];
                    $terminalNumVar = $model['terminal'.ucfirst($committedField)];

                    // If committed value is null, set model copy validity to 'invalid'
                    $modelCopy = $model;
                    if (!$optional && empty($committedNumVar)) $modelCopy['harvestDataValid'] = false;
                    else if ($optional) $modelCopy['harvestDataValid'] = true;

                    // Render column
                    $htmlElement[] = $this->renderHarvestDataColumn($committedField, $terminalNumVar, $committedNumVar, $modelCopy);

                    // If the terminal method is not set, check if there is a terminal value for the required numeric variable
                    if (empty($terminalMethod)) {
                        $terminalField = $numVars[$req];
                        $terminalNumVar = $model['terminal' . ucfirst($terminalField)];

                        if (!empty($terminalNumVar)) {
                            // If committed value is null, set model copy validity to 'invalid'
                            $modelCopy = $model;
                            if (!$optional && !isset($terminalNumVar)) $modelCopy['harvestDataValid'] = false;
                            else if ($optional) $modelCopy['harvestDataValid'] = true;

                            // Render column
                            if (empty($committedNumVar)) $htmlElement = [];
                            $htmlElement[] = $this->renderHarvestDataColumn($terminalField, $terminalNumVar, null, $modelCopy);
                        }
                    }
                }
            }
        }

        // If terminal harvest method is set, display the terminal required numeric variable
        if (!empty($terminalMethod)) {
            $requiredNumVar = $methodNumVarCompat[$terminalMethod] ?? [];

            $updatedTerminalMethod = "";
            if (empty($requiredNumVar)) {
                $updatedTerminalMethod = strtoupper(str_replace([" ", "-"], "_", $terminalMethod));
                $requiredNumVar = $methodNumVarCompat[$updatedTerminalMethod] ?? [];
            }

            // check variable scale value data
            if (empty($requiredNumVar)) {
                [$requiredNumVar, $numVars, $updatedTerminalMethod] = $this->extractFromScaleValues(
                    $terminalMethod,
                    $model,
                    $numVars,
                    $methodNumVarCompat
                );
            } else {
                // check and update numVars if numeric variable is not included
                $updatedTerminalMethod = empty($updatedTerminalMethod) ? strtoupper(str_replace([" ", "-"], "_", $terminalMethod)) : $updatedTerminalMethod;
                $numVars = $this->updateNumericVariables($requiredNumVar, $numVars, $model, $updatedTerminalMethod);
            }
            // If method requires a numeric variable, display
            if (!empty($requiredNumVar)) {
                // If optional = false, required numeric var must not be empty
                // Otherwise, allow it to be empty. "Required" means that the
                // input for the numeric variable will be shown.
                $optional = false;
                // Check if optional. Set flag properly.
                if (
                    in_array("", $requiredNumVar)
                    || (!in_array($terminalMethod, $methodRequiringNumVar)
                        && !in_array($updatedTerminalMethod, $methodRequiringNumVar))
                ) {
                    $optional = true;
                }

                foreach ($requiredNumVar as $req) {
                    if (empty($req)) continue;
                    $terminalField = $numVars[$req] ?? "";
                    $terminalNumVar = $model['terminal' . ucfirst($terminalField)] ?? "";

                    // If committed value is null, set model copy validity to 'invalid'
                    $modelCopy = $model;
                    if (!$optional && empty($terminalNumVar)) $modelCopy['harvestDataValid'] = false;
                    else if ($optional) $modelCopy['harvestDataValid'] = true;

                    // Render column
                    $htmlElement[] = $this->renderHarvestDataColumn($terminalField, $terminalNumVar, null, $modelCopy);

                    // If the committed method is not set, check if there is a committed value for the required numeric variable
                    if (empty($committedMethod)) {
                        $committedField = $numVars[$req] ?? "";
                        $committedNumVar = $model[$committedField] ?? "";

                        if (!empty($committedNumVar)) {
                            // If committed value is null, set model copy validity to 'invalid'
                            $modelCopy = $model;
                            if (!$optional && !isset($committedNumVar)) $modelCopy['harvestDataValid'] = false;
                            else if ($optional) $modelCopy['harvestDataValid'] = true;

                            // Render column
                            if (empty($terminalNumVar)) $htmlElement = [];
                            $rendered = $this->renderHarvestDataColumn($committedField, null, $committedNumVar, $modelCopy);
                            $htmlElement = array_merge([$rendered], $htmlElement);
                        }
                    }
                }
            }
        }

        // Return final element
        if (!empty($htmlElement)) {
            return join('<br>', $htmlElement);
        }

        return "<span title='Not applicable' class='badge center grey lighten-2 text-darken-3' style='margin: 0px 0px 5px 0px;'>NA</span>";
    }

    /**
     * Checks if the record has terminal data
     * @param Array variables array containing variable field names
     * @param Object record record (plot/cross) information
     * @return Boolean whether or not the record has terminal data
     */
    public function hasTerminalData($variables, $record)
    {
        foreach ($variables as $var) {
            $terminalField = 'terminal' . ucfirst(($var));
            if (!empty($record[$terminalField])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determines if the plot or cross is valid depending on its available
     * harvest data (committed and uncommitted) and provides the appropriate remarks
     * based on the validity.
     * @param object browserConfig browser configurations
     * @param object record plot or cross record to be checked
     */
    public function harvestDataIsValid($browserConfig, $record)
    {
        // Define HTML elements of remarks
        $inQueue = '<span title="Queued for harvest" class="new badge grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'QUEUED FOR HARVEST') . '</strong></span>';
        $inProgress = '<span title="Harvest in progress" class="new badge orange darken-3"><strong>' . \Yii::t('app', 'HARVEST IN PROGRESS') . '</strong></span>';
        $forCreation = '<span title="Ready for package, seed and germplasm creation" class="new badge light-green darken-3"><strong>' . \Yii::t('app', 'FOR CREATION') . '</strong></span>';
        $toCommit = '<span title="Ready to commit" class="new badge light-blue darken-3"><strong>' . \Yii::t('app', 'TO COMMIT') . '</strong></span>';
        $missing = '<span class="new badge red darken-3" style="margin-bottom:3px"><strong>' . \Yii::t('app', 'MISSING VALUE') . '</strong></span>';
        $badQcCode = '<span class="new badge red darken-3" style="margin-bottom:3px"><strong>' . \Yii::t('app', 'BAD QC CODE') . '</strong></span>';
        $unsupportedCrossMethod = '<span class="new badge red darken-3"><strong>' . \Yii::t('app', 'UNSUPPORTED CROSS METHOD') . '</strong></span>';
        $missingCrossMethod = '<span class="new badge red darken-3"><strong>' . \Yii::t('app', 'MISSING CROSS METHOD') . '</strong></span>';

        // Check terminal data
        $inputCols = $browserConfig['input_columns'] ?? [];
        $numVarFields = $browserConfig['numeric_variables_field'] ?? [];
        $hasTerminalData = $this->hasTerminalData(
            array_merge($inputCols, $numVarFields),
            $record
        );

        // If cross method is not specified, mark as invalid
        if (empty($record['plotDbId']) && empty($record['crossMethodAbbrev'])) {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = $missingCrossMethod;
            return $record;
        }

        // If harvest status is 'QUEUED_FOR_HARVEST', mark as valid
        if ($record['harvestStatus'] == 'QUEUED_FOR_HARVEST') {
            $record['harvestDataValid'] = true;
            $record['harvestRemarks'] = $inQueue;
            return $record;
        }

        // If harvest status is 'HARVEST_IN_PROGRESS', mark as valid
        if ($record['harvestStatus'] == 'HARVEST_IN_PROGRESS') {
            $record['harvestDataValid'] = true;
            $record['harvestRemarks'] = $inProgress;
            return $record;
        }

        // If harvest status is 'CONFLICT', mark as invalid
        if (!$hasTerminalData && strpos($record['harvestStatus'], 'CONFLICT') !== false) {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = '<span class="new badge red darken-3"><strong>' . \Yii::t('app', strtoupper($record['harvestStatus'])) . '</strong></span>';
            return $record;
        }

        // If harvest status is 'BAD_QC_CODE', mark as valid
        if ($record['harvestStatus'] == 'BAD_QC_CODE') {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = $badQcCode;
            return $record;
        }

        // If harvest status is 'READY', mark as valid
        if ($record['harvestStatus'] == 'READY') {
            $record['harvestDataValid'] = true;
            $record['harvestRemarks'] = $forCreation;
            return $record;
        }

        // If cross method is unsupported, mark as invalid
        $crossMethodAbbrev = isset($record['crossMethodAbbrev']) ? $record['crossMethodAbbrev'] : 'CROSS_METHOD_SELFING';
        if (!isset($browserConfig['method_numvar_compat'][$crossMethodAbbrev])) {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = $unsupportedCrossMethod;
            return $record;
        }

        // Get numeric variables
        $numVars = array_combine($browserConfig['numeric_variables'], $browserConfig['numeric_variables_field']);

        // Germplasm state
        $state = isset($record['state']) ? $record['state'] : 'default';
        // Germplasm type
        $type = isset($record['type']) ? $record['type'] : 'default';
        $methodNumVarCompat = $browserConfig['method_numvar_requirement_compat'][$crossMethodAbbrev] ?? [];
        // Get compatibilty arrays
        $cmCompat = $browserConfig[$crossMethodAbbrev] ?? []; // Cross method config
        $stateCompat = isset($cmCompat[$state]) ? $cmCompat[$state] : ($cmCompat['default'] ?? []); // State config
        // If stage is TCV, use config for it
        $stage = empty($record['stageCode']) ? 'default' : $record['stageCode'];
        if ($stage == 'TCV') $stateCompat = isset($cmCompat[$stage]) ? $cmCompat[$stage] : [];
        $typeCompat = isset($stateCompat[$type]) ? $stateCompat[$type] : (isset($stateCompat['default']) ? $stateCompat['default'] : []); // Type config
        $inputColumns = $typeCompat['input_columns'] ?? []; // Type-compatible columns

        $inputColMissing = [];
        $inputColValid = true;
        // Loop through input columns
        foreach ($inputColumns as $column) {
            // If the column is required, check if the column has value
            if (isset($column['required']) && $column['required']) {
                $columnName = $column['column_name'];
                $terminalValue = $record['terminal' . ucfirst($columnName)];
                $committedValue = $record[$columnName];
                // If the column has no committed and terminal values, mark as invalid
                if (empty($terminalValue) && empty($committedValue)) {
                    // Add missing indicator
                    if (!in_array($missing, $inputColMissing)) {
                        $inputColMissing[] = $missing;
                    }
                    if ($inputColValid) $inputColValid = false;
                    // Add missing indicator for specific missing data
                    $inputColMissing[] = $this->buildMissingInfoDisplay($columnName);
                }
            }
        }

        // Check terminal harvest method's required numeric variable
        $numVarIsOptional = false;
        $hasRequiredNumVar = false;
        $terminalHasRequired = false;
        $terminalHarvestMethod = isset($record['terminalHarvestMethod']) ? $record['terminalHarvestMethod'] : '';
        $requiredNumVarT = isset($methodNumVarCompat[$terminalHarvestMethod]) ? $methodNumVarCompat[$terminalHarvestMethod] : [];
        $hasRequiredNumVar = !empty($requiredNumVarT);
        $terminalHasRequired = $hasRequiredNumVar;

        $terminalNumVar = null;
        $committedNumVar = null;

        // If required num vars for terminal method is not empty
        if ($requiredNumVarT != null && $requiredNumVarT != "") {
            // If "" is in the array, mark num var as optional input
            if (in_array("", $requiredNumVarT)) $numVarIsOptional = true;
            // Loop through each num var required for terminal method
            foreach ($requiredNumVarT as $requiredT) {
                // If num var is not an empty string, retrieve its value
                if (!empty($requiredT)) {
                    $terminalField = $numVars[$requiredT];
                    $terminalNumVar = $record['terminal' . ucfirst($terminalField)];

                    $committedNumVar = $record[$terminalField];
                }
            }
        }

        // Check committed harvest method's required numeric variable
        $committedHarvestMethod = isset($record['harvestMethod']) ? $record['harvestMethod'] : '';
        $requiredNumVarC = isset($methodNumVarCompat[$committedHarvestMethod]) ? $methodNumVarCompat[$committedHarvestMethod] : [];
        $hasRequiredNumVar = !$hasRequiredNumVar ? !empty($requiredNumVarC) : $hasRequiredNumVar;

        // If required num vars for committed method is not empty
        if ($requiredNumVarC != null && $requiredNumVarC != "") {
            // If "" is in the array, mark num var as optional input
            if (in_array("", $requiredNumVarC)) $numVarIsOptional = true;
            // Loop through each num var required for committed method
            foreach ($requiredNumVarC as $requiredC) {
                // If num var is not an empty string, retrieve its value
                if (!empty($requiredC)) {
                    $committedField = $numVars[$requiredC];
                    $committedNumVar = $record[$committedField];

                    $terminalNumVar = $record['terminal' . ucfirst($committedField)];
                }
            }
        }

        // If numeric variable is required and has no terminal or committed value, mark as invalid
        if (
            !(!empty($terminalHarvestMethod) && !$terminalHasRequired) &&
            $hasRequiredNumVar &&
            !$numVarIsOptional &&
            ($committedNumVar == null || $committedNumVar == "") &&
            ($terminalNumVar == null || $terminalNumVar == "")
        ) {
            if (!in_array($missing, $inputColMissing)) {
                $inputColMissing[] = $missing;
            }
            if ($inputColValid) $inputColValid = false;
            $inputColMissing[] = $this->buildMissingInfoDisplay('numericVariable');
        }

        // If there are missing inputs, set remarks
        if (!$inputColValid && !empty($inputColMissing)) {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = implode("<br>", $inputColMissing);
            return $record;
        }

        // has uncommitted data
        if ($hasTerminalData) {
            // if harvest method requires numeric variable and 
            // record has required variable and numeric value (terminal or committed), 
            // mark as valid
            if (
                !$numVarIsOptional &&
                (!empty($requiredNumVarT) || !empty($requiredNumVarC)) && // has required numeric variable, committed or not
                ((!isset($committedNumVar) && isset($terminalNumVar) ||   // numeric value is in terminal
                    (isset($committedNumVar) && !isset($terminalNumVar))))   // numeric value is committed
            ) {
                // If data is valid, mark as valid
                $record['harvestDataValid'] = true;
                $record['harvestRemarks'] = $toCommit;
                return $record;
            }
        }

        // Check if additional required variables are set
        $additionalRequired = $typeCompat['additional_required_variables'] ?? [];
        foreach ($additionalRequired as $additional) {
            // If required variable is not set, mark as invalid
            if (empty($record[$additional['field_name']])) {
                $missingInfo = $this->buildMissingInfoDisplay($additional['field_name']);
                $record['harvestDataValid'] = false;
                $record['harvestRemarks'] = $missing . '<br> ' . $missingInfo;
                return $record;
            }
        }

        // If data is valid, mark as valid
        $record['harvestDataValid'] = true;
        $record['harvestRemarks'] = $toCommit;
        return $record;
    }

    /**
     * Given a field name in lower camel case,
     * the field name is separated by capital case and
     * converted to all caps, then added to a span to
     * be used for indicating a missing harvest data.
     * @param String $field name field name in lower camel case
     * @return String span HTML element
     */
    public function buildMissingInfoDisplay($fieldName)
    {
        $pieces = preg_split('/(?=[A-Z])/', $fieldName);
        $pieces[0] = ucfirst($pieces[0]);
        $name = strtoupper(implode(" ", $pieces));

        return '<span class="new badge white red-text text-darken-3" style="margin-bottom:3px"><strong>' . \Yii::t('app', $name) . '</strong></span>';
    }

    /**
     * Retrieves the creation summary and returns a data provider
     * containing the summary for the creation summary modal.
     * @param integer occurrenceId occurrence identifier
     * @param boolean harvestCross whether or not crosses will be harvested as well
     */
    public function getSummaryDataProvider($occurrenceId, $harvestCross)
    {
        $summaryData = [];
        $readyPlots = 0;
        $readyCrosses = 0;

        // Count plots ready for harvest
        $requestBody = [
            "fields" => "plot.id AS plotDbId|plot.harvest_status AS harvestStatus",
            "harvestStatus" => "equals READY"
        ];
        $urlParam =  ["limit=1", "sort=plotDbId:asc"];
        $result = $this->plotBrowserModel->getPlotRecords($occurrenceId, $requestBody, $urlParam);
        $readyPlots = $result['count'];
        if ($readyPlots > 0) {
            $summaryData[] = [
                "summaryItemId" => 0,
                "dataLevel" => "Plot",
                "plural" => "Plots",
                "count" => $readyPlots
            ];
        }

        // If crosses are to be harvested, count crosses ready for harvest
        if ($harvestCross) {
            $requestBody['fields'] = "germplasmCross.id AS crossDbId|germplasmCross.harvest_status AS harvestStatus";
            $urlParam[1] = "sort=crossDbId:asc";
            $result = $this->crossBrowserModel->getCrossRecords($occurrenceId, $requestBody, $urlParam);
            $readyCrosses = $result['count'];
            if ($readyCrosses > 0) {
                $summaryData[] = [
                    "summaryItemId" => 1,
                    "dataLevel" => "Cross",
                    "plural" => "Crosses",
                    "count" => $readyCrosses
                ];
            }
        }

        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'allModels' => $summaryData,
            'key' => 'summaryItemId',
            'restified' => true,
            'totalCount' => count($summaryData)
        ]);

        return [
            "dataProvider" => $dataProvider,
            "readyPlots" => $readyPlots,
            "readyCrosses" => $readyCrosses
        ];
    }

    /**
     * Commits harvest data
     * @param integer $occurrenceId occurrence identifier
     * @return array $committedDataset commit response and dataset count check
     */
    public function commitHarvestData($occurrenceId)
    {
        // Retrieve user ID
        $userId = $this->userModel->getUserId();
        $locationDbId = $this->occurrenceDetailsModel->getLocationIdOfOccurrence($occurrenceId);
        // If location ID cannot be retrieved, throw an exception
        if ($locationDbId == 0) {
            $errorMessage = 'Unable to retrieve location ID.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }
        // Set default return array
        $committedDataset = array('response' => ['success' => false], "hasDataset" => false);

        // Retrieve transaction ids ready for commit
        $requestBody = array(
            'fields' => "transaction.id AS transactionDbId|transaction.location_id AS locationDbId|transaction.creator_id AS creatorDbId|transaction.status",
            'locationDbId'  => "equals $locationDbId",
            'creatorDbId' => "equals $userId",
            'status' => 'equals uploaded'
        );

        // Retrieve transactions of the user in the current location
        $transactions = $this->terminaltransaction->searchAll($requestBody)['data'];

        foreach ($transactions as $transaction) {
            $transactionId = $transaction['transactionDbId'];

            // Check dataset count
            $datasets = $this->terminaltransaction->searchAllDatasets(
                $transactionId,
                [
                    "fields" => "dataset.id as datasetDbId"
                ],
                'limit=1&sort=datasetDbId:asc',
                false
            );
            $datasetCount = $datasets['totalCount'];

            // If no datasets found, do not commit
            if ($datasetCount == 0) return $committedDataset;
            /**
             * TODO: Run data commit in the background(use TransactionController::actionCommitBackground) 
             *       if dataset count exceeded 6000(old HM commit threshold)
             */
            $committedDataset['hasDataset'] = true;

            // Commit harvest data
            $commitResponse = $this->transaction->commitPlotData($transactionId);

            if ($commitResponse["success"]) {
                $response = $this->terminaltransaction->updateOne($transactionId, ["status" => "committed"]);
                $committedDataset['response']['success'] = $response['success'];
            }
        }

        return $committedDataset;
    }

    /**
     * Build the creation tab columns based on inputted values
     * @param String $dataLevel - data level (plot or cross)
     * @param Integer $occurrenceId - occurrence identifier
     */
    public function buildCreationColumns($source, $occurrenceId) {
        $dataLevel = '';
        if ($source == 'plots') {
            $dataLevel = 'plot';
        } else if ($source = 'crosses') {
            $dataLevel = 'cross';
        }

        // Variable declarations
        $variableAbbrevs = [];
        $defaultColumns = [];
        $inputColumns = [];
        $supported = [
            "harvestMethods" => [],
            "numVars" => []
        ];
  
        $currentConfigs = \Yii::$app->cbSession->get("hmCurrentConfigs-$dataLevel-$occurrenceId");

        foreach($currentConfigs as $currentConfig) {
            if ($dataLevel == 'plot' && !empty($config) && $currentConfig['crossMethodAbbrev'] != 'SELFING') {
                continue;
            }

            // Get config value
            $config = $currentConfig['config'] ?? [];
            // Get harvest method config
            $harvestMethodsConfig = $config['harvestMethods'] ?? [];
            // Get harvest data config
            $harvestDataConfig = $config['harvestData'] ?? [];

            // Extract harvest methods and numeric variables
            foreach($harvestMethodsConfig as $abbrev => $numVars) {

                // Compile all harvest methods in the current configs
                if(!in_array($abbrev, $supported['harvestMethods'])) {
                    $supported['harvestMethods'][] = $abbrev;
                }

                // Compile all num vars in the current configs
                foreach($numVars as $numVar) {
                    $numVarAbbrev = $numVar['variableAbbrev'] ?? '';
                    
                    if(!in_array($numVarAbbrev, $supported['numVars'])) {
                        $supported['numVars'][] = $numVarAbbrev;
                    }
                }
            }

            // Loop through other harvest data
            foreach($harvestDataConfig as $hdata) {
                // Check variable abbrev
                $varAbb = $hdata['variableAbbrev'] ?? null;
                // If abbrev is null, or has been encountered already,
                // skip it.
                if(empty($varAbb) || in_array($varAbb, $variableAbbrevs)) continue;

                // Add abbrev to array for tracking
                $variableAbbrevs[] = $varAbb;

                // Get config info
                $apiFieldName = $hdata['apiFieldName'] ?? null;
                $required = $hdata['required'] ?? false;
                
                // Get variable display name
                $variable = $this->variableModel->getVariableByAbbrev($varAbb);
                $displayName = $variable['displayName'] ?? '';
                $hdata["displayName"] = $displayName;

                // Build new column
                $newColumn = [
                    'attribute' => $apiFieldName,
                    'label' => $displayName,
                    'mergeHeader' => true,
                    'format' => 'raw',
                    'visible' => true,
                    'order' => \kartik\dynagrid\DynaGrid::ORDER_FIX_RIGHT,
                    'encodeLabel' => false,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'value' => function ($model) use ($hdata, $required) {

                        $apiFieldName = $hdata['apiFieldName'] ?? null;
                        $terminal = isset($model["terminal" . (ucfirst($apiFieldName))]) ? $model["terminal" . (ucfirst($apiFieldName))] : null;
                        $committed = isset($model[$apiFieldName]) ? $model[$apiFieldName] : null;

                        return $this->renderHarvestDataColumn($apiFieldName, $terminal, $committed, $model, $required);
                    }
                ];

                // Add to input columns
                $inputColumns[] = $newColumn;
            }
        }

        // Build harvest method and numvars (if any)
        if (!empty($supported['harvestMethods'])) {
            // Build harvest method config
            $harvestMethodConfig = [
                "variableAbbrev" => "HV_METH_DISC",
                "apiFieldName" => "harvestMethod",
                "displayName" => "Harvest Method",
                "inputType" => "select2",
                "inputOptions" => []
            ];
            // Get harvest method scale values
            $harvestMethodScaleVals = $this->harvestManagerModel->getScaleValues(
                $harvestMethodConfig['variableAbbrev'],
                false,
                ["key" => "abbrev", "value" => "value"]
            );

            $apiFieldName = $harvestMethodConfig['apiFieldName'] ?? null;

            // Build new column
            $newColumn = [
                'attribute' => $apiFieldName,
                'label' => $harvestMethodConfig['displayName'],
                'mergeHeader' => true,
                'format' => 'raw',
                'visible' => true,
                'order' => \kartik\dynagrid\DynaGrid::ORDER_FIX_RIGHT,
                'encodeLabel' => false,
                'hAlign' => 'left',
                'vAlign' => 'middle',
                'value' => function ($model) use ($apiFieldName) {
                    $terminal = isset($model["terminal" . (ucfirst($apiFieldName))]) ? $model["terminal" . (ucfirst($apiFieldName))] : null;
                    $committed = isset($model[$apiFieldName]) ? $model[$apiFieldName] : null;

                    // check config if harvest method is required
                    $required = false;
                    $config = $model['harvestDataConfig']['config'];
                    $harvestMethods = $config['harvestMethods'] ?? [];
                    if(!empty($harvestMethods)) {
                        $required = true;
                    }

                    return $this->renderHarvestDataColumn($apiFieldName, $terminal, $committed, $model, $required);
                }
            ];

            // Add to input columns
            $defaultColumns[] = $newColumn;

            // Build harvest method and numvars (if any)
            if (!empty($supported['numVars'])) {
                // Map numvar abbrevs to the appropriate display names
                // Display names are retrieved from the `master.variable` table.

                $numVarDisplayNames = [];
                foreach ($supported['numVars'] as $numVarAbbrev) {
                    $variableRecord = $this->variableModel->getVariableByAbbrev($numVarAbbrev);
                    $displayName = $variableRecord['displayName'] ?? '';
                    if (empty($numVarDisplayNames[$numVarAbbrev])) {
                        $numVarDisplayNames[$numVarAbbrev] = $displayName;
                    }
                }

                $harvestMethodValues = \Yii::$app->session->get("harvestMethodValues");
                $harvestMethodValuesFlipped = array_flip($harvestMethodValues);
                $variableFieldNames = \Yii::$app->session->get("variableFieldNames");

                // Build new column
                $newColumn = [
                    'attribute' => 'numericVariable',
                    'label' => 'Numeric Variable',
                    'mergeHeader' => true,
                    'format' => 'raw',
                    'visible' => true,
                    'order' => \kartik\dynagrid\DynaGrid::ORDER_FIX_RIGHT,
                    'encodeLabel' => false,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'value' => function ($model) use ($supported, $harvestMethodValuesFlipped, $variableFieldNames) {
                        $browserConfig = $model['harvestDataConfig']['config'];
                        $terminalMethod = isset($model['terminalHarvestMethod']) ? $model['terminalHarvestMethod'] : null;
                        $committedMethod = isset($model['harvestMethod']) ? $model['harvestMethod'] : null;
                        $methodNumVarCompat = isset($browserConfig['harvestMethodNumVarCompat']) ? $browserConfig['harvestMethodNumVarCompat'] : [];
                        $methodNumVarCompatReq = isset($browserConfig['harvestMethodNumVarCompatRequired']) ? $browserConfig['harvestMethodNumVarCompatRequired'] : [];

                        // Determine the harvest method abbrev
                        $harvestMethodAbbrev = 
                            !empty($terminalMethod) ? 
                            $harvestMethodValuesFlipped[$terminalMethod]
                            :
                            (
                                !empty($committedMethod) ?
                                $harvestMethodValuesFlipped[$committedMethod]
                                :
                                null
                            );
                        
                        // Get supported num vars
                        $supportedNumVars = $methodNumVarCompat[$harvestMethodAbbrev] ?? [];

                        // If supportedNumVars is empty, mark numeric variable as not applicable
                        if(empty($supportedNumVars)) {
                            return "<span title='Not applicable' class='badge center grey lighten-2 text-darken-3' style='margin: 0px 0px 5px 0px;'>NA</span>";
                        }

                        // Get required num vars
                        $requiredNumVars = $methodNumVarCompatReq[$harvestMethodAbbrev];

                        // Render supported num vars (if any)
                        foreach($supportedNumVars as $numVarAbbrev) {
                            $apiFieldName = $variableFieldNames[$numVarAbbrev];
                            $terminal = $model["terminal" . ucfirst($apiFieldName)];
                            $committed = $model[$apiFieldName];
                            $required = in_array($numVarAbbrev, $requiredNumVars);

                            return $this->renderHarvestDataColumn($apiFieldName, $terminal, $committed, $model, $required);
                        }
                    },
                ];

                // Add to input columns
                $defaultColumns[] = $newColumn;
            }
        }

        // Combine default and other input columns
        $inputColumns = array_merge($defaultColumns, $inputColumns);

        return $inputColumns;
    }

    /** 
     * Build Harvest Data columns
     * 
     * @param string dataLevel data level of records to be processed ("plot", "cross")
     * @param integer occurrence ID occurrence record identifier
     * 
     * @return mixed harvest data columns
     */
    public function buildHarvestDataColumns($dataLevel, $occurrenceId)
    {
        // Retrieve session harvest data requirements config
        $harvestDataRequirementsConfig = \Yii::$app->cbSession->get("harvestDataRequirementsConfig");

        // Variable declarations
        $variableAbbrevs = [];
        $defaultColumns = [];
        $inputColumns = [];
        $supported = [
            "harvestMethods" => [],
            "numVars" => [],
            "plotInputColumns" => [],
            "numVarFields" => []
        ];

        // extract and build from harvestDataRequirementsConfig
        foreach ($harvestDataRequirementsConfig as $currentConfig) {

            if ($dataLevel == 'plot' && !empty($config) && $currentConfig['crossMethodAbbrev'] != 'SELFING') {
                continue;
            }

            // Get config value
            $config = $currentConfig['config'] ?? [];

            // Get harvest method config
            $harvestMethodsConfig = $config['harvestMethods'] ?? [];
            // Get harvest data config
            $harvestDataConfig = $config['harvestData'] ?? [];

            // Extract harvest methods and numeric variables
            foreach ($harvestMethodsConfig as $abbrev => $numVars) {
                // Compile all harvest methods in the current configs
                if (!in_array($abbrev, $supported['harvestMethods'])) {
                    $supported['harvestMethods'][] = $abbrev;
                }

                // Compile all num vars in the current configs
                foreach ($numVars as $numVar) {
                    $numVarAbbrev = $numVar['variableAbbrev'] ?? '';

                    if (!in_array($numVarAbbrev, $supported['numVars'])) {
                        $supported['numVars'][] = $numVarAbbrev;
                        $supported['numVarFields'][$numVarAbbrev] = $numVar['apiFieldName'];
                    }
                }
            }

            // Loop through other harvest data
            foreach ($harvestDataConfig as $hdata) {

                // Check variable abbrev
                $varAbb = $hdata['variableAbbrev'] ?? null;
                // If abbrev is null, or has been encountered already,
                // skip it.
                if (empty($varAbb) || in_array($varAbb, $variableAbbrevs)) continue;

                // Add abbrev to array for tracking
                $variableAbbrevs[] = $varAbb;
                // Get config info
                $apiFieldName = $hdata['apiFieldName'] ?? null;
                $required = $hdata['required'] ?? false;

                array_push($supported['plotInputColumns'], $apiFieldName);

                // Get variable display name
                $variable = $this->variableModel->getVariableByAbbrev($varAbb);
                $displayName = $variable['displayName'] ?? '';
                $hdata["displayName"] = $displayName;

                // Build new column
                $newColumn = [
                    'attribute' => $apiFieldName,
                    'label' => $displayName,
                    'mergeHeader' => true,
                    'format' => 'raw',
                    'visible' => true,
                    'order' => \kartik\dynagrid\DynaGrid::ORDER_FIX_RIGHT,
                    'encodeLabel' => false,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'visible' => in_array($apiFieldName, $supported['plotInputColumns']),
                    'value' => function ($model) use ($hdata, $required) {

                        $apiFieldName = $hdata['apiFieldName'] ?? null;
                        $terminal = isset($model["terminal" . (ucfirst($apiFieldName))]) ? $model["terminal" . (ucfirst($apiFieldName))] : null;
                        $committed = isset($model[$apiFieldName]) ? $model[$apiFieldName] : null;

                        return $this->renderHarvestDataColumn($apiFieldName, $terminal, $committed, $model, $required);
                    }
                ];

                // Add to input columns
                $inputColumns[] = $newColumn;
            }
        }

        // Build harvest method and numvars (if any)
        if (!empty($supported['harvestMethods'])) {
            // Build harvest method config
            $harvestMethodConfig = [
                "variableAbbrev" => "HV_METH_DISC",
                "apiFieldName" => "harvestMethod",
                "displayName" => "Harvest Method",
                "inputType" => "select2",
                "inputOptions" => []
            ];
            // Get harvest method scale values
            $harvestMethodScaleVals = $this->harvestManagerModel->getScaleValues(
                $harvestMethodConfig['variableAbbrev'],
                false,
                ["key" => "abbrev", "value" => "value"]
            );

            $apiFieldName = $harvestMethodConfig['apiFieldName'] ?? null;
            array_push($supported['plotInputColumns'], $apiFieldName);

            // Build new column
            $newColumn = [
                'attribute' => $apiFieldName,
                'label' => $harvestMethodConfig['displayName'],
                'mergeHeader' => true,
                'format' => 'raw',
                'visible' => true,
                'order' => \kartik\dynagrid\DynaGrid::ORDER_FIX_RIGHT,
                'encodeLabel' => false,
                'hAlign' => 'left',
                'vAlign' => 'middle',
                'visible' => in_array($apiFieldName, $supported['plotInputColumns']),
                'value' => function ($model) use ($apiFieldName) {
                    $terminal = isset($model["terminal" . (ucfirst($apiFieldName))]) ? $model["terminal" . (ucfirst($apiFieldName))] : null;
                    $committed = isset($model[$apiFieldName]) ? $model[$apiFieldName] : null;

                    // check config if harvest method is required
                    $required = false;
                    $config = $model['harvestDataConfig']['config'];
                    $harvestMethods = $config['harvestMethods'] ?? [];
                    if(!empty($harvestMethods)) {
                        $required = true;
                    }

                    return $this->renderHarvestDataColumn($apiFieldName, $terminal, $committed, $model, $required);
                }
            ];

            // Add to input columns
            $defaultColumns[] = $newColumn;

            // Build harvest method and numvars (if any)
            if (!empty($supported['numVars'])) {
                // Map numvar abbrevs to the appropriate display names
                // Display names are retrieved from the `master.variable` table.

                $numVarDisplayNames = [];
                foreach ($supported['numVars'] as $numVarAbbrev) {
                    $variableRecord = $this->variableModel->getVariableByAbbrev($numVarAbbrev);
                    $displayName = $variableRecord['displayName'] ?? '';
                    if (empty($numVarDisplayNames[$numVarAbbrev])) {
                        $numVarDisplayNames[$numVarAbbrev] = $displayName;
                    }
                }

                $supported['plotInputColumns'][] = "numericVariable";

                $harvestMethodValues = \Yii::$app->session->get("harvestMethodValues");
                $harvestMethodValuesFlipped = array_flip($harvestMethodValues);
                $variableFieldNames = \Yii::$app->session->get("variableFieldNames");

                // Build new column
                $newColumn = [
                    'attribute' => 'numericVariable',
                    'label' => 'Numeric Variable',
                    'mergeHeader' => true,
                    'format' => 'raw',
                    'visible' => true,
                    'order' => \kartik\dynagrid\DynaGrid::ORDER_FIX_RIGHT,
                    'encodeLabel' => false,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'visible' => in_array('numericVariable', $supported['plotInputColumns']),
                    'value' => function ($model) use ($supported, $harvestMethodValuesFlipped, $variableFieldNames) {
                        $browserConfig = $model['harvestDataConfig']['config'];
                        $terminalMethod = isset($model['terminalHarvestMethod']) ? $model['terminalHarvestMethod'] : null;
                        $committedMethod = isset($model['harvestMethod']) ? $model['harvestMethod'] : null;
                        $methodNumVarCompat = isset($browserConfig['harvestMethodNumVarCompat']) ? $browserConfig['harvestMethodNumVarCompat'] : [];
                        $methodNumVarCompatReq = isset($browserConfig['harvestMethodNumVarCompatRequired']) ? $browserConfig['harvestMethodNumVarCompatRequired'] : [];

                        // Determine the harvest method abbrev
                        $harvestMethodAbbrev = 
                            !empty($terminalMethod) ? 
                            $harvestMethodValuesFlipped[$terminalMethod]
                            :
                            (
                                !empty($committedMethod) ?
                                $harvestMethodValuesFlipped[$committedMethod]
                                :
                                null
                            );
                        
                        // Get supported num vars
                        $supportedNumVars = $methodNumVarCompat[$harvestMethodAbbrev] ?? [];

                        // If supportedNumVars is empty, mark numeric variable as not applicable
                        if(empty($supportedNumVars)) {
                            return "<span title='Not applicable' class='badge center grey lighten-2 text-darken-3' style='margin: 0px 0px 5px 0px;'>NA</span>";
                        }

                        // Get required num vars
                        $requiredNumVars = $methodNumVarCompatReq[$harvestMethodAbbrev];

                        // Render supported num vars (if any)
                        foreach($supportedNumVars as $numVarAbbrev) {
                            $apiFieldName = $variableFieldNames[$numVarAbbrev];
                            $terminal = $model["terminal" . ucfirst($apiFieldName)];
                            $committed = $model[$apiFieldName];
                            $required = in_array($numVarAbbrev, $requiredNumVars);

                            return $this->renderHarvestDataColumn($apiFieldName, $terminal, $committed, $model, $required);
                        }
                    },
                ];

                // Add to input columns
                $defaultColumns[] = $newColumn;
            }
        }

        // Combine default and other input columns
        $inputColumns = array_merge($defaultColumns, $inputColumns);

        return $inputColumns;
    }

    /**
     * Build Harvest Remarks column values for a given record
     * 
     * @param object record data record to be processed
     * 
     * @return object record with harvestRemarks values
     */
    public function buildHarvestRemarksColumn($record)
    {
        if (empty($record) || !is_array($record)) {
            return $record;
        }

        $harvestDataRequirementsConfig = \Yii::$app->cbSession->get("harvestDataRequirementsConfig");

        // Harvet remarks HTML element definition
        // Define HTML elements of remarks
        $inQueue = '<span title="Queued for harvest" class="new badge grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'QUEUED FOR HARVEST') . '</strong></span>';
        $inProgress = '<span title="Harvest in progress" class="new badge orange darken-3"><strong>' . \Yii::t('app', 'HARVEST IN PROGRESS') . '</strong></span>';
        $forCreation = '<span title="Ready for package, seed and germplasm creation" class="new badge light-green darken-3"><strong>' . \Yii::t('app', 'FOR CREATION') . '</strong></span>';
        $toCommit = '<span title="Ready to commit" class="new badge light-blue darken-3"><strong>' . \Yii::t('app', 'TO COMMIT') . '</strong></span>';
        $missing = '<span class="new badge red darken-3" style="margin-bottom:3px"><strong>' . \Yii::t('app', 'MISSING VALUE') . '</strong></span>';
        $badQcCode = '<span class="new badge red darken-3" style="margin-bottom:3px"><strong>' . \Yii::t('app', 'BAD QC CODE') . '</strong></span>';
        $unsupportedCrossMethod = '<span class="new badge red darken-3"><strong>' . \Yii::t('app', 'UNSUPPORTED CROSS METHOD') . '</strong></span>';
        $missingCrossMethod = '<span class="new badge red darken-3"><strong>' . \Yii::t('app', 'MISSING CROSS METHOD') . '</strong></span>';

        $harvestMethodValues = \Yii::$app->session->get("harvestMethodValues");
        $harvestMethodValuesFlipped = array_flip($harvestMethodValues);

        $supported = $this->getSupportedHarvestData($harvestDataRequirementsConfig, 'plot', $record);
        if (empty($supported)) {
            $supported = [
                "harvestMethods" => [],
                "numVars" => [],
                "crossMethods" => []
            ];
        }
        // Check terminal data
        $supported['inputColumns'] = [];
        foreach ($record['harvestDataConfig']['config']['harvestData'] as $inputFieldConfig) {
            array_push($supported['inputColumns'], $inputFieldConfig["apiFieldName"]);
        }

        $inputCols = $supported['inputColumns'] ?? [];
        $numVarFields = $supported['numericVariablesField'] ?? [];
        $hasTerminalData = $this->hasTerminalData(
            array_merge($inputCols, $numVarFields),
            $record
        );

        // If cross method is not specified, mark as invalid
        if (empty($record['plotDbId']) && empty($record['crossMethodAbbrev'])) {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = $missingCrossMethod;
            return $record;
        }
        // If harvest status is 'QUEUED_FOR_HARVEST', mark as valid
        if ($record['harvestStatus'] == 'QUEUED_FOR_HARVEST') {
            $record['harvestDataValid'] = true;
            $record['harvestRemarks'] = $inQueue;
            return $record;
        }

        // If harvest status is 'HARVEST_IN_PROGRESS', mark as valid
        if ($record['harvestStatus'] == 'HARVEST_IN_PROGRESS') {
            $record['harvestDataValid'] = true;
            $record['harvestRemarks'] = $inProgress;
            return $record;
        }
        // If harvest status is 'CONFLICT', mark as invalid
        if (!$hasTerminalData && strpos($record['harvestStatus'], 'CONFLICT') !== false) {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = '<span class="new badge red darken-3"><strong>' . \Yii::t('app', strtoupper($record['harvestStatus'])) . '</strong></span>';
            return $record;
        }
        // If harvest status is 'BAD_QC_CODE', mark as valid
        if ($record['harvestStatus'] == 'BAD_QC_CODE') {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = $badQcCode;
            return $record;
        }

        // If harvest status is 'READY', mark as valid
        if ($record['harvestStatus'] == 'READY') {
            $record['harvestDataValid'] = true;
            $record['harvestRemarks'] = $forCreation;
            return $record;
        }

        // If cross method is unsupported, mark as invalid
        $crossMethodAbbrev = isset($record['crossMethodAbbrev']) ? $record['crossMethodAbbrev'] : 'SELFING';
        $updatedCrossMethod = isset($record['crossMethodAbbrev']) ? "" . (strtoupper(str_replace(" ", "_", $record['crossMethod']))) : 'SELFING';
        if (
            isset($supported['crossMethods']) &&
            (!isset($supported['methodNumVarCompat'][$crossMethodAbbrev]) &&
                !isset($supported['methodNumVarCompat'][$updatedCrossMethod]))
        ) {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = $unsupportedCrossMethod;
            return $record;
        }
        $harvestDataConfig = $harvestDataRequirementsConfig;
        $harvestDataConfig = $record['harvestDataConfig'];
        $inputColumns = $harvestDataConfig['config']['harvestData'];

        if (!empty($harvestDataConfig['config']['harvestMethods'])) {
            // add HARV_METH_DISC column to inputColumns
            $inputColumns[] = [
                "apiFieldName" => "harvestMethod",
                "required" => true,
                "variableAbbrev" => "HV_METH_DISC"
            ];
        }

        $inputColMissing = [];
        $inputColValid = true;
        foreach ($inputColumns as $column) {
            $column = $column;
            // If the column is required, check if the column has value
            if (isset($column['required']) && $column['required']) {
                $columnName = $column['apiFieldName'];
                $terminalValue = $record['terminal' . ucfirst($columnName)];
                $committedValue = $record[$columnName];

                // If the column has no committed and terminal values, mark as invalid
                if (empty($terminalValue) && empty($committedValue)) {

                    // Add missing indicator
                    if (!in_array($missing, $inputColMissing)) {
                        $inputColMissing[] = $missing;
                    }
                    if ($inputColValid) $inputColValid = false;
                    // Add missing indicator for specific missing data
                    $inputColMissing[] = $this->buildMissingInfoDisplay($columnName);
                }
            }
        }

        // Check terminal harvest method's required numeric variable
        $methodNumVarCompat = $harvestDataConfig['config']['harvestMethodNumVarCompatRequired'];
        $numVars = $harvestDataConfig['config']['harvestMethodNumVarCompatRequired'];
        $harvestMethodsConfig = $harvestDataConfig['config']['harvestMethods'];

        $numVarIsOptional = false;
        $hasRequiredNumVar = false;
        $terminalHasRequired = false;

        $terminalHarvestMethod = isset($record['terminalHarvestMethod']) ? $record['terminalHarvestMethod'] : '';
        $terminalHarvestMethod = $harvestMethodValuesFlipped[$terminalHarvestMethod] ?? '';
        $requiredNumVarT = isset($methodNumVarCompat[$terminalHarvestMethod]) ? $methodNumVarCompat[$terminalHarvestMethod] : [];

        // Check harvest method in scale values
        if (empty($requiredNumVarT) && !empty($terminalHarvestMethod)) {
            [$requiredNumVarT, $numVars, $terminalHarvestMethod] = $this->extractFromScaleValues(
                $terminalHarvestMethod,
                $record,
                $numVars,
                $methodNumVarCompat
            );
        }

        $hasRequiredNumVar = !empty($requiredNumVarT);
        $terminalHasRequired = $hasRequiredNumVar;

        $terminalNumVar = null;
        $committedNumVar = null;

        // If required num vars for terminal method is not empty
        if ($requiredNumVarT != null && $requiredNumVarT != "") {
            // If "" is in the array, mark num var as optional input
            if (in_array("", $requiredNumVarT)) $numVarIsOptional = true;
            // Loop through each num var required for terminal method
            foreach ($requiredNumVarT as $requiredT) {
                // If num var is not an empty string, retrieve its value
                if (!empty($requiredT)) {
                    $terminalField = $requiredT;
                    foreach ($harvestMethodsConfig[$terminalHarvestMethod] as $methodConfig) {
                        if ($methodConfig['variableAbbrev'] == $requiredT) {
                            $terminalField = $methodConfig['apiFieldName'];
                            break;
                        }
                    }
                    $terminalNumVar = $record['terminal' . ucfirst($terminalField)];
                    $committedNumVar = $record[$terminalField];
                }
            }
        }

        // Check committed harvest method's required numeric variable
        $committedHarvestMethod = isset($record['harvestMethod']) ? $record['harvestMethod'] : '';
        $committedHarvestMethod = $harvestMethodValuesFlipped[$committedHarvestMethod] ?? '';
        $requiredNumVarC = isset($methodNumVarCompat[$committedHarvestMethod]) ? $methodNumVarCompat[$committedHarvestMethod] : [];

        // Check harvest method in scale values
        if (empty($requiredNumVarC) && !empty($committedHarvestMethod)) {
            [$requiredNumVarC, $numVars, $terminalHarvestMethod] = $this->extractFromScaleValues(
                $committedHarvestMethod,
                $record,
                $numVars,
                $methodNumVarCompat
            );
        }

        $hasRequiredNumVar = !$hasRequiredNumVar ? !empty($requiredNumVarC) : $hasRequiredNumVar;

        // If required num vars for committed method is not empty
        if ($requiredNumVarC != null && $requiredNumVarC != "") {
            // If "" is in the array, mark num var as optional input
            if (in_array("", $requiredNumVarC)) $numVarIsOptional = true;
            // Loop through each num var required for committed method
            foreach ($requiredNumVarC as $requiredC) {
                // If num var is not an empty string, retrieve its value
                if (!empty($requiredC)) {
                    $committedField = $requiredC;
                    foreach ($harvestMethodsConfig[$committedHarvestMethod] as $methodConfig) {
                        if ($methodConfig['variableAbbrev'] == $requiredC) {
                            $committedField = $methodConfig['apiFieldName'];
                            break;
                        }
                    }
                    $committedNumVar = $record[$committedField];
                    $terminalNumVar = $record['terminal' . ucfirst($committedField)];
                }
            }
        }

        // If numeric variable is required and has no terminal or committed value, mark as invalid
        if (
            (!empty($terminalHarvestMethod) || !empty($committedHarvestMethod)) // committed/terminal has harvest method
            && $hasRequiredNumVar // has required numerical variable
            && !$numVarIsOptional  // is not optional
            && ($committedNumVar == null || $committedNumVar == "") // committed has no required value
            && ($terminalNumVar == null || $terminalNumVar == "") // terminal has no required value
        ) {

            if (!in_array($missing, $inputColMissing)) {
                $inputColMissing[] = $missing;
            }

            if ($inputColValid) $inputColValid = false;
            $inputColMissing[] = $this->buildMissingInfoDisplay('numericVariable');
        }

        // If there are missing inputs, set remarks
        if (!$inputColValid && !empty($inputColMissing)) {
            $record['harvestDataValid'] = false;
            $record['harvestRemarks'] = implode("<br>", $inputColMissing);
            return $record;
        }

        // has uncommitted data
        if ($hasTerminalData) {
            // if harvest method requires numeric variable and 
            // record has required variable and numeric value (terminal or committed), 
            // mark as valid
            if (
                !$numVarIsOptional &&
                (!empty($requiredNumVarT) || !empty($requiredNumVarC)) && // has required numeric variable, committed or not
                ((!isset($committedNumVar) && isset($terminalNumVar) ||   // numeric value is in terminal
                    (isset($committedNumVar) && !isset($terminalNumVar))))   // numeric value is committed
            ) {
                // If data is valid, mark as valid
                $record['harvestDataValid'] = true;
                $record['harvestRemarks'] = $toCommit;
                return $record;
            }
        }

        // If data is valid, mark as valid
        $record['harvestDataValid'] = true;
        $record['harvestRemarks'] = $toCommit;

        return $record;
    }

    /**
     * Extract supported harvest data from required data config
     * 
     * @param object harvest data requirements config
     * 
     * @return array supported data information
     */
    public function getSupportedHarvestData($harvestReqConfig)
    {

        $supported = [
            "harvestMethods" => [], // supported harvest methods 
            "numVars" => [],    // numeric variable attributes
            "inputColumns" => [], // input columns with values
            "inputColumnsEntity" => [], //expected columns with value
            "numericVariablesField" => [], // numeric variable attributes
            "methodNumVarCompat" => [], // cross methods + compatible harvest method + numeric variable attr
            "methodNumVarRequirementCompat" => [], // same as methodNumvarCompat
            "crossMethods" => []
        ];

        $configCrossMethods = [];
        foreach ($harvestReqConfig as $harvestConfigRecord) {
            // Retrieve harvestMethods and numeric harvest data
            $harvestConfig = $harvestConfigRecord['config'];
            foreach ($harvestConfig['harvestMethods'] as $abbrev => $value) {
                // Compile all harvest methods in the current configs
                if (!in_array($abbrev, $supported['harvestMethods'])) {
                    $supported['harvestMethods'][] = $abbrev;
                }

                // Compile all num vars in the current configs
                foreach ($value as $numericVariable) {
                    $numVarAbbrev = $numericVariable['variableAbbrev'] ?? '';
                    $numVarField = $numericVariable['apiFieldName'] ?? '';
                    if (!in_array($numVarAbbrev, $supported['numVars'])) {
                        $supported['numVars'][] = $numVarAbbrev;
                        array_push($supported['numericVariablesField'], $numVarField);
                    }
                }
            }

            if (isset($harvestConfigRecord['crossMethodAbbrev'])) {
                if (is_array($harvestConfigRecord['crossMethodAbbrev'])) {
                    foreach ($harvestConfigRecord['crossMethodAbbrev'] as $cMethod) {
                        if (!in_array($cMethod, $supported['crossMethods'])) {
                            $crossMethodIndx = "" . (strtoupper(str_replace(" ", "_", $cMethod)));
                            $supported['methodNumVarCompat'][$crossMethodIndx] = $harvestConfigRecord['config']['harvestMethods'];
                            array_push($supported['crossMethods'], $cMethod);
                        }
                    }
                } else if (!in_array($harvestConfigRecord['crossMethodAbbrev'], $supported['crossMethods'])) {
                    $crossMethodIndx = "" . (strtoupper(str_replace(" ", "_", $harvestConfigRecord['crossMethodAbbrev'])));
                    $supported['methodNumVarCompat'][$crossMethodIndx] = $harvestConfigRecord['config']['harvestMethods'];
                    array_push($supported['crossMethods'], $harvestConfigRecord['crossMethodAbbrev']);
                }
            }
        }

        return $supported;
    }

    /**
     * Generate Creation tab browser columns based on harvest data requirements columns
     * 
     * @return array array of databrowser columns
     */
    public function generateBowserColumns()
    {
        $harvestDataRequirementsConfig = \Yii::$app->cbSession->get("harvestDataRequirementsConfig");

        $browserColumns = [];
        foreach ($harvestDataRequirementsConfig as $config) {
            $configHarvestData = $config['config']['harvestData'] ?? [];
            $configHarvestMethods = $config['config']['harvestMethods'] ?? [];

            // retrieve harvest data columns
            if (isset($configHarvestData) && !empty($configHarvestData)) {
                foreach ($configHarvestData as $harvestData) {
                    if (
                        isset($harvestData['apiFieldName']) && !empty($harvestData['apiFieldName']) &&
                        !in_array($harvestData['apiFieldName'], $browserColumns)
                    ) {
                        array_push($browserColumns, $harvestData['apiFieldName']);
                        array_push($browserColumns, "terminal" . ucfirst($harvestData['apiFieldName']));
                    };
                };
            }

            // retrieve harvest data numeric value columns
            if (isset($configHarvestMethods) && !empty($configHarvestMethods)) {
                foreach ($configHarvestMethods as $harvestMethod => $data) {
                    if (isset($data) && !empty($data) && is_array($data)) {
                        foreach ($data as $numVarColumn) {
                            if (
                                isset($numVarColumn['apiFieldName']) && !empty($numVarColumn['apiFieldName']) &&
                                !in_array($numVarColumn['apiFieldName'], $browserColumns)
                            ) {
                                array_push($browserColumns, $numVarColumn['apiFieldName']);
                                array_push($browserColumns, "terminal" . ucfirst($numVarColumn['apiFieldName']));
                            };
                        }
                    }
                }
            }
        };

        return $browserColumns;
    }

    /**
     * Retrieve matching HV_METH_DISC variable scale variable
     * 
     * @param String $method committed or terminal harvest method
     * @param Object $record record to be processed
     * @param Array $numVars numeric variables
     * @param Array $methodNumVarCompat method and numeric variable compatibility
     * @return mixed
     */
    public function extractFromScaleValues($method, $record, $numVars, $methodNumVarCompat)
    {
        $hvMethodInfo = $this->variableModel->getVariableByAbbrev('HV_METH_DISC');
        if (!empty($hvMethodInfo)) {
            $variableId = $hvMethodInfo['variableDbId'];

            $scaleValue = $this->variableModel->searchAllVariableScales(
                $variableId,
                ["value" => $method]
            );

            $scaleVal = array_values(
                array_filter(
                    $scaleValue['data'][0]['scaleValues'],
                    function ($x) use ($method) {
                        return strtolower($x['abbrev']) == strtolower("hv_meth_disc_" . $method);
                    }
                )
            );

            $extractedHarvestMethod = str_replace("HV_METH_DISC_", "", $scaleVal[0]['abbrev']);
            $requiredNumVar = $methodNumVarCompat[$extractedHarvestMethod] ?? [];

            // If not in numVars, add method and alias
            $numVars = $this->updateNumericVariables($requiredNumVar, $numVars, $record, $extractedHarvestMethod);
        }

        return [
            $requiredNumVar,
            $numVars,
            $extractedHarvestMethod
        ];
    }

    /**
     * Update numeric variables array
     * 
     * @param Array $requiredNumVar variables with required variables
     * @param Array $numVars numeric variables
     * @param Object $record record to be processed
     * @param String $method committed or terminal harvest method
     * 
     * @return mixed
     */
    public function updateNumericVariables($requiredNumVar, $numVars, $record, $method)
    {

        // If not in numVars, add method and alias
        foreach ($requiredNumVar as $reqNumVar) {
            if (!isset($numVar[$reqNumVar]) || empty($numVar[$reqNumVar])) {
                $extractedAlias = array_values(
                    array_filter(
                        $record['harvestDataConfig']['config']['harvestMethods'][$method],
                        function ($x) use ($reqNumVar) {
                            return $x['variableAbbrev'] == $reqNumVar;
                        }
                    )
                );
                if (isset($extractedAlias[0]['apiFieldName']) && !empty($extractedAlias[0]['apiFieldName'])) {
                    $numVars[$reqNumVar] = $extractedAlias[0]['apiFieldName'];
                };
            }
        }

        return $numVars;
    }
}
