<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import Yii
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
// Import interfaces
use app\interfaces\models\ICross;
use app\interfaces\models\IExperiment;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IPlot;
use app\interfaces\models\ITerminalTransaction;
use app\interfaces\models\ITransactionDataset;
use app\interfaces\models\IVariable;
use app\interfaces\models\IUser;
use app\interfaces\modules\harvestManager\models\IApiHelper;
use app\interfaces\modules\harvestManager\models\ICrossBrowserModel;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel;
use app\interfaces\modules\harvestManager\models\IPlotBrowserModel;

/**
* Model class for Harvest Data
*/
class HarvestDataModel {
    /**
     * Model constructor
     */
    public function __construct(
        public ICross $cross,
        public IExperiment $experiment,
        public IOccurrence $occurrence,
        public IPlot $plot,
        public ITerminalTransaction $terminalTransaction,
        public ITransactionDataset $transactionDataset,
        public IVariable $variable,
        public IUser $user,
        public IApiHelper $apiHelper,
        public ICrossBrowserModel $crossBrowserModel,
        public IHarvestManagerModel $harvestManagerModel,
        public IOccurrenceDetailsModel $occurrenceDetailsModel,
        public IPlotBrowserModel $plotBrowserModel
        )
    { }

    /**
     * Retrieves the experiment type of the experiment
     * where the occurrence belongs to
     * @param integer occurrenceId occurrence identifier
     * @return string experimentType the type of the experiment
     */
    public function getOccurrenceExperimentType($occurrenceId) {
        // get experiment id
        $experimentId = $this->occurrenceDetailsModel->getExperimentIdOfOccurrence($occurrenceId);
        // retrieve experiment record
        $requestBody = [ "experimentDbId" => "$experimentId" ];
        $result = $this->experiment->searchAll($requestBody);
        $exp = isset($result['data'][0]) ? $result['data'][0] : [];

        return isset($exp['experimentType']) ? $exp['experimentType'] : '';
    }

    /**
     * Retrieves the name of the occurrence
     * @param integer occurrenceId occurrence identifier
     * @return string occurrenceName name of the occurrence
     */
    public function getOccurrenceName($occurrenceId) {
        // retrieve occurrence record
        $requestBody = [ "occurrenceDbId" => "$occurrenceId" ];
        $result = $this->occurrence->searchAll($requestBody);
        $occ = isset($result['data'][0]) ? $result['data'][0] : [];

        return isset($occ['occurrenceName']) ? $occ['occurrenceName'] : '';
    }

    /**
     * Consume the given browser config.
     * Retrieves the harvest method values,
     * given the arrays of harvest method abbrevs
     * @param object config browser config
     * @param array states list of states to consume from the config
     * @param array types list of types to consume from the config
     */
    public function consumeConfig($config, $states, $types) {
        // Retrieve harvest method scale values
        $harvestMethodScaleValues = $this->harvestManagerModel->getScaleValues('HV_METH_DISC',true);

        // Define final config array
        $finalConfig = [
            'input_columns' => [],
            'numeric_variables' => [],
            'numeric_variables_field' => [],
            'method_numvar_compat' => [],
            'method_numvar_requirement_compat' => [],
            'bulk_update_config' => [
                'numeric_variables' => [],
                'harvest_methods' => [],
                'num_var_compat' => [],
                'cmet_hmet_compat' => [],
                'gstate_gtype_hmet_compat' => [],
                'variable_field_names' => [],
                'variable_placeholder' => [],
                'variable_field_placeholder_mapping' => [],
                'variable_scale_values' => []
            ],
            'bulk_delete_config' => [
                'all_variables' => [],
                'numeric_variables' => [],
                'numeric_variables_abbrevs' => [],
                'all_abbrevs' => [],
                'abbrev_field_name_array' => []
            ],
            'variable_compat' => []
        ];

        // Loop through each cross method in the config
        foreach($config as $crossMethodAbbrev => $crossMethodConfig) {
            // Define cross method config array
            $cmConfig = [];
            // Define method-numvar compat array
            $cmMethodNumVarCompat = [];
            $cmMethodNumVarCompatReq = [];
            // Initialize cmet-hmet compat
            if(!isset($finalConfig['bulk_update_config']['cmet_hmet_compat'][$crossMethodAbbrev])) {
                $finalConfig['bulk_update_config']['cmet_hmet_compat'][$crossMethodAbbrev] = [];
            }
            // Initialize variable_compat
            if(!isset($finalConfig['variable_compat'][$crossMethodAbbrev])) {
                $finalConfig['variable_compat'][$crossMethodAbbrev] = [];
            }

            // Loop through each state
            foreach($states as $state) {
                // Get config for current state. If non-existent, get default.
                $state = isset($crossMethodConfig[$state]) ? $state : 'default';

                // If state config is not set, skip the state.
                // This means that the state is unsupported for the current cross method
                if(!isset($crossMethodConfig[$state])) continue;

                // Initialize hmet-gstate compat
                if(!isset($finalConfig['bulk_update_config']['gstate_gtype_hmet_compat'][$state])) {
                    $finalConfig['bulk_update_config']['gstate_gtype_hmet_compat'][$state] = [];
                }
                // Initialize variable_compat
                if(!isset($finalConfig['variable_compat'][$crossMethodAbbrev][$state])) {
                    $finalConfig['variable_compat'][$crossMethodAbbrev][$state] = [];
                }

                $stateConfig = $crossMethodConfig[$state];

                // Loop through each type
                foreach($types as $type) {
                    // Get config for current type. If non-existent, get default.
                    $type = isset($stateConfig[$type]) ? $type : 'default';

                    // If type config is not set, skip the type.
                    // This means that the type is unsupported for the current state
                    if(!isset($stateConfig[$type])) continue;

                    // Initialize hmet-gstate-gtype compat
                    if(!isset($finalConfig['bulk_update_config']['gstate_gtype_hmet_compat'][$state][$type])) {
                        $finalConfig['bulk_update_config']['gstate_gtype_hmet_compat'][$state][$type] = [];
                    }
                    // Initialize variable_compat
                    if(!isset($finalConfig['variable_compat'][$crossMethodAbbrev][$state][$type])) {
                        $finalConfig['variable_compat'][$crossMethodAbbrev][$state][$type] = [];
                    }

                    $item = $stateConfig[$type];
                    $typeNumVars = [];
                    $typeNumVarFields = [];

                    // Get input columns
                    $inputColumns = $item['input_columns'];
                    // Loop through numeric variables
                    foreach($inputColumns as $idx => $column) {
                        $columnAbbrev = $column['abbrev'];
                        $columnName = $column['column_name'];
                        $columnPlaceholder = $column['placeholder'];

                        if($columnAbbrev != '<none>') {
                            // Add to variable field names array
                            if(!in_array($columnAbbrev,$finalConfig['bulk_update_config']['variable_field_names'])) {
                                $finalConfig['bulk_update_config']['variable_field_names'][$columnAbbrev] = $columnName;
                            }
                            // Add to variable placeholders array
                            if(!in_array($columnAbbrev,$finalConfig['bulk_update_config']['variable_placeholder'])) {
                                $finalConfig['bulk_update_config']['variable_placeholder'][$columnAbbrev] = $columnPlaceholder;
                            }
                            // Add to variable field-placholder array
                            if(!in_array($columnAbbrev,$finalConfig['bulk_update_config']['variable_field_placeholder_mapping'])) {
                                $finalConfig['bulk_update_config']['variable_field_placeholder_mapping'][$columnName] = $columnPlaceholder;
                            }
                            // Add to variable compatibility array
                            if(!in_array($columnName,$finalConfig['variable_compat'][$crossMethodAbbrev][$state][$type])) {
                                $finalConfig['variable_compat'][$crossMethodAbbrev][$state][$type][] = $columnName;
                            }
                        }
                    }

                    // Get numeric variables 
                    $numericVariables = $item['numeric_variables'];
                    $harvestMethods = [];
                    $variableScaleValues = [];
                    // Loop through numeric variables
                    foreach($numericVariables as $idx => $numVar) {
                        $numVarAbbrev = $numVar['abbrev'];
                        $numVarField = $numVar['field_name'];
                        $hMethods = $numVar['harvest_methods'];
                        $hMethodsOpt = $numVar['harvest_methods_optional'] ?? [];
                        if(!empty($hMethodsOpt)) {
                            $hMethods = array_merge($hMethods, $hMethodsOpt);
                        }
                        $hMethodsReq = array_diff($hMethods, $hMethodsOpt);
                        $numVarPlaceholder = $numVar['placeholder'];

                        // Add numeric variable to bulk update config
                        $numVarCopy = $numVar;
                        unset($numVarCopy['harvest_methods']);      // unset harvest_methods
                        if($numVarAbbrev != '<none>') {
                            // If not yet in numeric variables array, add
                            if(!in_array($numVarCopy, $finalConfig['bulk_update_config']['numeric_variables'])) {
                                $finalConfig['bulk_update_config']['numeric_variables'][] = $numVarCopy;
                            }
                            // If not yet in variable field names array, add numvar
                            if(!in_array($numVarField, $finalConfig['bulk_update_config']['variable_field_names'])) {
                                $finalConfig['bulk_update_config']['variable_field_names'][$numVarAbbrev] = $numVarField;
                            }
                            // If not yet in variable placeholder array, add numvar
                            if(!in_array($numVarField, $finalConfig['bulk_update_config']['variable_placeholder'])) {
                                $finalConfig['bulk_update_config']['variable_placeholder'][$numVarAbbrev] = $numVarPlaceholder;
                            }
                            // If not yet in variable field-placeholder mapping array, add numvar
                            if(!in_array($numVarField, $finalConfig['bulk_update_config']['variable_field_placeholder_mapping'])) {
                                $finalConfig['bulk_update_config']['variable_field_placeholder_mapping'][$numVarField] = $numVarPlaceholder;
                            }
                            // Add to variable compatibility array
                            if(!in_array($numVarField,$finalConfig['variable_compat'][$crossMethodAbbrev][$state][$type])) {
                                $finalConfig['variable_compat'][$crossMethodAbbrev][$state][$type][] = $numVarField;
                            }

                            // Add to bulk delete configs
                            $bdConfigItem = [
                                "field_name" => $numVarField,
                                "abbrev" => $numVarAbbrev,
                                "placeholder" => $numVarPlaceholder
                            ];
                            // If not yet in all variables, add
                            if(!in_array($bdConfigItem, $finalConfig['bulk_delete_config']['all_variables'])) {
                                $finalConfig['bulk_delete_config']['all_variables'][] = $bdConfigItem;
                            }
                            // If not yet in numeric variables, add
                            if(!in_array($bdConfigItem, $finalConfig['bulk_delete_config']['numeric_variables'])) {
                                $finalConfig['bulk_delete_config']['numeric_variables'][] = $bdConfigItem;
                            }
                            // If not yet in numeric variables abbrevs, add
                            if(!in_array($numVarAbbrev, $finalConfig['bulk_delete_config']['numeric_variables_abbrevs'])) {
                                $finalConfig['bulk_delete_config']['numeric_variables_abbrevs'][] = $numVarAbbrev;
                            }
                            // If abbrev not yet in all abbrevs array, add
                            if(!in_array($numVarAbbrev, $finalConfig['bulk_delete_config']['all_abbrevs'])) {
                                $finalConfig['bulk_delete_config']['all_abbrevs'][] = $numVarAbbrev;
                            }
                            // If abbrev not yet in abbrev-field name array, add
                            if(!in_array($numVarAbbrev, $finalConfig['bulk_delete_config']['abbrev_field_name_array'])) {
                                $finalConfig['bulk_delete_config']['abbrev_field_name_array'][$numVarAbbrev] = $numVarField;
                            }

                            // Add to num var compat
                            if(!isset($finalConfig['bulk_update_config']['num_var_compat'][$numVarAbbrev])) {
                                // If not yet set, initialize array for the numeric var
                                $finalConfig['bulk_update_config']['num_var_compat'][$numVarAbbrev] = [
                                    "harvestMethods" => [],
                                    "description" => $numVarPlaceholder,
                                    "crossMethods" => [ $crossMethodAbbrev ]
                                ];
                            }
                            else {
                                // If already set, add cross method abbrev to the cross methods
                                if(!in_array($crossMethodAbbrev, $finalConfig['bulk_update_config']['num_var_compat'][$numVarAbbrev]['crossMethods'])) {
                                    $finalConfig['bulk_update_config']['num_var_compat'][$numVarAbbrev]['crossMethods'][] = $crossMethodAbbrev;
                                }
                            }
                        }

                        // Loop through harvest methods that use the numeric varible
                        foreach($hMethods as $hmAbbrev) {
                            // If hmAbbrev is empty, skip it
                            if(empty($hmAbbrev)) continue;
                            // Add method to the master array of methods for this state
                            $hmItem = $harvestMethodScaleValues[$hmAbbrev];
                            $harvestMethods = array_merge($harvestMethods, $hmItem);
                            // Add harvest method to cross method compatibility
                            $finalConfig['bulk_update_config']['cmet_hmet_compat'][$crossMethodAbbrev] = array_unique(
                                array_merge(
                                    $finalConfig['bulk_update_config']['cmet_hmet_compat'][$crossMethodAbbrev],
                                    array_values($hmItem)
                                )
                            );
                            // Add harvest methods to bulk update config
                            $finalConfig['bulk_update_config']['harvest_methods'] = array_unique(array_merge($finalConfig['bulk_update_config']['harvest_methods'], $harvestMethods));
                            // Add harvest methods to hmet-gstate-gtype config
                            $finalConfig['bulk_update_config']['gstate_gtype_hmet_compat'][$state][$type] = array_unique(
                                array_merge(
                                    $finalConfig['bulk_update_config']['gstate_gtype_hmet_compat'][$state][$type], $harvestMethods
                                )
                            );
                            // Add num var abbrev to the array
                            if($numVarAbbrev != '<none>') {
                                $typeNumVars[] = $numVarAbbrev;
                                $typeNumVarFields[] = $numVarField;
                                // Add the harvest methods allowed for the current numeric variable
                                $finalConfig['bulk_update_config']['num_var_compat'][$numVarAbbrev]['harvestMethods'] = array_unique(
                                    array_merge(
                                        $finalConfig['bulk_update_config']['num_var_compat'][$numVarAbbrev]['harvestMethods'],
                                        array_values($hmItem)
                                    )
                                );
                            }
                            // Add numvar to method compatibility array
                            $hmValue = array_keys($hmItem)[0];
                            $cmMethodNumVarCompat[$hmValue] = isset($cmMethodNumVarCompat[$hmValue]) ? $cmMethodNumVarCompat[$hmValue] : [];
                            $insertAbbrev = $numVarAbbrev != '<none>' ? $numVarAbbrev : '';
                            $cmMethodNumVarCompat[$hmValue] = array_values(array_unique(array_merge($cmMethodNumVarCompat[$hmValue], [$insertAbbrev])));

                            // Check if the method has a required numeric variable
                            if(in_array($hmAbbrev, $hMethodsReq)) {
                                $cmMethodNumVarCompatReq[$hmValue] = $cmMethodNumVarCompat[$hmValue];
                            }
                        }

                        // Remove harvest methods from numeric variable config
                        unset($numericVariables[$idx]['harvest_methods']);
                    }

                    // Get input columns
                    $inputColumnConfig = $item['input_columns'];
                    $inputColumns = [];
                    foreach($inputColumnConfig as $inputCol) {
                        if(isset($inputCol['abbrev'])) {
                            $inputColumns[] = $inputCol['column_name'];
                        }
                        // Add to bulk delete config
                        if($inputCol['abbrev'] != '<none>') {
                            if(!in_array($inputCol['abbrev'], $finalConfig['bulk_delete_config']['all_abbrevs'])) {
                                $finalConfig['bulk_delete_config']['all_abbrevs'][] = $inputCol['abbrev'];

                                $newItem = [
                                    'abbrev' => $inputCol['abbrev'],
                                    'field_name' => $inputCol['column_name'],
                                    'placeholder' => $inputCol['placeholder']
                                ];

                                $finalConfig['bulk_delete_config']['all_variables'][] = $newItem;
                                $finalConfig['bulk_delete_config']['abbrev_field_name_array'][$inputCol['abbrev']] = $inputCol['column_name'];
                            }
                        }
                        // Retrieve scale values if needed
                        if(!empty($inputCol['retrieve_scale']) && $inputCol['retrieve_scale']) {
                            $variableScale = $this->harvestManagerModel->getScaleValues($inputCol['abbrev'], false, [
                                "key" => "value",
                                "value" => "value"
                            ]);
                            // Add scale value to config for current germplasm type
                            if(empty($variableScaleValues[$inputCol['column_name']])) {
                                $variableScaleValues[$inputCol['column_name']] = $variableScale;
                            }
                            // Add scale value to bulk update config
                            if(empty($finalConfig['bulk_update_config']['variable_scale_values'][$inputCol['column_name']])) {
                                $finalConfig['bulk_update_config']['variable_scale_values'][$inputCol['column_name']] = $variableScale;
                            }
                        }
                    }
                    
                    // Populate state configs
                    $cmConfig[$state][$type] = [
                        'additional_required_variables' => $item['additional_required_variables'],
                        'harvest_methods' => $harvestMethods,
                        'input_columns' => $item['input_columns'],
                        'numeric_variables' => $numericVariables,
                        'variable_scale_values' => $variableScaleValues
                    ];
                    // Update input columns in final config
                    $finalConfig['input_columns'] = array_unique(array_merge($finalConfig['input_columns'], $inputColumns));
                    // Update numeric vars in final config
                    $finalConfig['numeric_variables'] = array_values(array_unique(array_merge($finalConfig['numeric_variables'], $typeNumVars)));
                    $finalConfig['numeric_variables_field'] = array_values(array_unique(array_merge($finalConfig['numeric_variables_field'], $typeNumVarFields)));
                }

                // Add cross method config to final config
                $finalConfig[$crossMethodAbbrev] = $cmConfig;
                $finalConfig["method_numvar_compat"][$crossMethodAbbrev] = $cmMethodNumVarCompat;
                $finalConfig["method_numvar_requirement_compat"][$crossMethodAbbrev] = $cmMethodNumVarCompatReq;
            }
        }
        // Copy variable_compat into the bulk_update_config
        $finalConfig['bulk_update_config']['variable_compat'] = $finalConfig['variable_compat'];
        // Copy method_numvar_requirement_compat into the bulk_update_config
        $finalConfig['bulk_update_config']['method_numvar_requirement_compat'] = $finalConfig['method_numvar_requirement_compat'];

        return $finalConfig;
    }

    /**
     * Determines whether or not input will be disabled
     * based on the harvest status and/or state of the model
     * @param object model data model
     * @param string dataLevel data lavel (plot or cross)
     * @param object cmVarCompat variable compat configurations object
     * @param string fieldName name of the field
     */
    public function disableInput($model, $dataLevel, $cmVarCompat = [], $fieldName = '') {
        $harvestStatus = $model['harvestStatus'];
        $state = $model['state'];
        
        if($harvestStatus=='COMPLETED') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', "This $dataLevel has been harvested. Kindly delete the committed data first before you input new data on this $dataLevel.")
            ];
        }
        if($harvestStatus=='FAILED') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', "This harvest failed. Kindly delete the committed data first before you input new data on this $dataLevel.")
            ];
        }
        if($harvestStatus=='QUEUED_FOR_HARVEST' || $harvestStatus=='HARVEST_IN_PROGRESS' || $harvestStatus=='DONE') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', 'You cannot input harvest traits right now. Seed and package creation is in progress.')
            ];
        }
        if($harvestStatus=='DELETION_IN_PROGRESS') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', 'You cannot input harvest traits right now. Harvest trait and/or seed and package deletion is in progress.')
            ];
        }
        if($state == 'unknown') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', 'Input not allowed due to unknown state. Please request for data curation.')
            ];
        }
        
        // If fieldName is specified, check if input for the variable
        // is supported given the plot/cross model.
        if(!empty($fieldName)) {
            $variableCompat = $this->getVariableCompatConfig($model, $cmVarCompat);

            // If input is not supported, disable the input field
            if(!in_array($fieldName, $variableCompat)) {
                return [
                    'disable' => true,
                    'message' => \Yii::t('app', $fieldName . " input not supported for this $dataLevel.")
                ];
            }
        }

        return [
            'disable' => false,
            'message' => ''
        ];
    }

    /**
     * Builds harvest date HTML element
     * @param object cmVarCompat the variable compat config for the cross method
     * @param object model data model
     * @param string actionId id of the controller action
     */
    public function buildHarvestDateColumn($cmVarCompat, $model, $actionId) {
        // Define variables
        $fieldName = 'harvestDate';
        $dataLevel = $actionId == 'plots' ? 'plot' : 'cross';
        $class = 'hm-hdata-input hm-hdata-hdate-input';
        $idPrefix = 'hm-hdate-id';
        $idSuffix = ($dataLevel == 'cross') ? 'cross-' . $model['crossDbId'] : 'plot-' . $model['plotDbId'];
        $currentVal = !empty($model['harvestDate']) ? $model['harvestDate'] : '';
        $terminalVal = !empty($model['terminalHarvestDate']) ? $model['terminalHarvestDate'] : '';

        // Check if input will be disabled
        $disable = $this->disableInput($model, $dataLevel, $cmVarCompat, $fieldName);
        
        // Create datepciker widget
        // For unit testing purposes, building the datepciker moved to the sibling function "buildDatePicker"
        // to allow for partial mocking of the class.
        $value =  $this->buildDatePicker($terminalVal, $class, $idPrefix, $idSuffix, $disable);

        // Build final HTML element
        $currentHtmlElement = '<span title="Committed harvest date" class="badge center light-green darken-4 white-text">' . $currentVal . '</span>';
        if($currentVal != '') {
            return '<div class="harvest-date-committed" style="padding-top:12px">' . $currentHtmlElement . '</div><div class="harvest-date-input">' . $value . '<div>';
        }
        else {
            return '<div class="harvest-date-committed"></div><div class="harvest-date-input">' . $value . '<div>';
        }
    }

    /**
     * Builds date picker with the given parameters.
     * @param string terminalVal terminal value of variable
     * @param string class class name(s) to be applied to the date picker
     * @param string idPrefix prefix to the id, before the variable abbrev
     * @param string idSuffix suffix to the id, after the variable abbrev
     * @param object disable object containing instructions for disabling date picker
     */
    public function buildDatePicker($terminalVal, $class, $idPrefix, $idSuffix, $disable) {
        return DatePicker::widget([
            'name' => 'check_date',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => $terminalVal,
            'options' => [
                'class' => $class,
                'id' => $idPrefix . '--HVDATE_CONT--' . $idSuffix,
                'title' => $disable['message'],
                'disabled' => $disable['disable'],
                'autocomplete' => 'off',
                'oldValue' => $terminalVal,
            ],
            'pluginOptions' => [
                'todayHighlight' => true,
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'clearBtn' => true,
                'zIndexOffset' => '9000',
                'startDate'=> date("1970-01-01")
            ],
            'pluginEvents' => [
                'clearDate' => 'function (e) {$(e.target).find("input").change();}',
            ]

        ]);
    }
    
    /**
     * Builds harvest method HTML element
     * @param object crossMethodConfig the config applicable to the current data
     * @param object cmVarCompat the variable compat config for the cross method
     * @param object model data model
     * @param string actionId id of the controller action
     */
    public function buildHarvestMethodColumn($crossMethodConfig, $cmVarCompat, $model, $actionId) {
        // Define variables
        $fieldName = 'harvestMethod';
        $dataLevel = $actionId == 'plots' ? 'plot' : 'cross';
        $class = 'hm-hdata-input hm-hdata-hmethod-input';
        $idPrefix = 'hm-hmethod-id';
        $idSuffix = ($dataLevel == 'cross') ? 'cross-' . $model['crossDbId'] : 'plot-' . $model['plotDbId'];
        $currentVal = !empty($model['harvestMethod']) ? $model['harvestMethod'] : '';
        $terminalVal = !empty($model['terminalHarvestMethod']) ? $model['terminalHarvestMethod'] : '';
        $state = empty($model['state']) ? 'default' : $model['state'];
        $crossMethodAbbrev = !empty($model['crossMethodAbbrev']) ? $model['crossMethodAbbrev'] : "CROSS_METHOD_SELFING";
        // Get stage config
        $stateConfig = isset($crossMethodConfig[$state]) ? $crossMethodConfig[$state] : (isset($crossMethodConfig['default']) ? $crossMethodConfig['default'] : []);
        // If stage is TCV, use config for it
        $stage = empty($model['stageCode']) ? 'default' : $model['stageCode'];
        if($stage == 'TCV') $stateConfig = isset($crossMethodConfig[$stage]) ? $crossMethodConfig[$stage] : [];
        // Get type config
        $type = empty($model['type']) ? 'default' : $model['type'];
        $typeConfig = isset($stateConfig[$type]) ? $stateConfig[$type] : (isset($stateConfig['default']) ? $stateConfig['default'] : []);
        $allowedHarvestData = isset($typeConfig['harvest_methods']) ? $typeConfig['harvest_methods'] : [];

        // Check if input will be disabled
        $disable = $this->disableInput($model, $dataLevel, $cmVarCompat, $fieldName);
        // If test cross verification and allowed harvest data is empty, disable
        if($stage=='TCV' && empty($allowedHarvestData)) {
            $disable = [
                'disable' => true,
                'hidden' => 'hidden',
                'message' => \Yii::t('app', "Harvest method input is not allowed for this plot.")
            ];
        }

        // Create widget
        $placeholder = 'Select harvest method';
        $varAbbrev = 'HV_METH_DISC';
        $dataName = 'harvest-method';
        $value = $this->buildSelect2($allowedHarvestData, $terminalVal, $class, $idPrefix, $idSuffix, $disable, $crossMethodAbbrev, $placeholder, $varAbbrev, $dataName);
        
        // Build final HTML element
        $currentHtmlElement = '<span title="Committed harvest method" class="badge center light-green darken-4 white-text">' . $currentVal . '</span>';
        if($currentVal != '') {
            return '<div class="harvest-method-committed">' . $currentHtmlElement . '</div><div class="harvest-method-input" style="padding-top:8px">' . $value . '<div>';
        }
        else {
            return '<div class="harvest-method-committed"></div><div class="harvest-method-input" style="padding:5px 0px 5px 0px">' . $value . '<div>';
        }
    }

    /**
     * Builds grain color HTML element
     * @param object crossMethodConfig the config applicable to the current data
     * @param object cmVarCompat the variable compat config for the cross method
     * @param object model data model
     * @param string actionId id of the controller action
     */
    public function buildGrainColorColumn($crossMethodConfig, $cmVarCompat, $model, $actionId) {
        // Define variables
        $fieldName = 'grainColor';
        $dataLevel = $actionId == 'plots' ? 'plot' : 'cross';
        $class = 'hm-hdata-input hm-hdata-graincolor-input';
        $idPrefix = 'hm-graincolor-id';
        $idSuffix = ($dataLevel == 'cross') ? 'cross-' . $model['crossDbId'] : 'plot-' . $model['plotDbId'];
        $currentVal = !empty($model['grainColor']) ? $model['grainColor'] : '';
        $terminalVal = !empty($model['terminalGrainColor']) ? $model['terminalGrainColor'] : '';
        $state = empty($model['state']) ? 'default' : $model['state'];
        $crossMethodAbbrev = !empty($model['crossMethodAbbrev']) ? $model['crossMethodAbbrev'] : "CROSS_METHOD_SELFING";
        // Get stage config
        $stateConfig = isset($crossMethodConfig[$state]) ? $crossMethodConfig[$state] : (isset($crossMethodConfig['default']) ? $crossMethodConfig['default'] : []);
        // If stage is TCV, use config for it
        $stage = empty($model['stageCode']) ? 'default' : $model['stageCode'];
        if($stage == 'TCV') $stateConfig = isset($crossMethodConfig[$stage]) ? $crossMethodConfig[$stage] : [];
        // Get type config
        $type = empty($model['type']) ? 'default' : $model['type'];
        $typeConfig = isset($stateConfig[$type]) ? $stateConfig[$type] : (isset($stateConfig['default']) ? $stateConfig['default'] : []);
        // $allowedHarvestData = isset($typeConfig['harvest_methods']) ? $typeConfig['harvest_methods'] : [];
        $scale = !empty($typeConfig['variable_scale_values']) && !empty($typeConfig['variable_scale_values']['grainColor']) ? $typeConfig['variable_scale_values']['grainColor'] : [];

        // Check if input will be disabled
        $disable = $this->disableInput($model, $dataLevel, $cmVarCompat, $fieldName);
        // If test cross verification, disable
        if($stage=='TCV') {
            $disable = [
                'disable' => true,
                'hidden' => 'hidden',
                'message' => \Yii::t('app', "Grain color input is not allowed for $stage.")
            ];
        }

        // Create widget
        $placeholder = 'Select grain color';
        $varAbbrev = 'GRAIN_COLOR';
        $dataName = 'grain-color';
        $value = $this->buildSelect2($scale, $terminalVal, $class, $idPrefix, $idSuffix, $disable, $crossMethodAbbrev, $placeholder, $varAbbrev, $dataName);
        
        // Build final HTML element
        $currentHtmlElement = '<span title="Committed grain color" class="badge center light-green darken-4 white-text">' . $currentVal . '</span>';
        if($currentVal != '') {
            return '<div class="grain-color-committed">' . $currentHtmlElement . '</div><div class="grain-color-input" style="padding-top:8px">' . $value . '<div>';
        }
        else {
            return '<div class="harvest-method-committed"></div><div class="harvest-method-input" style="padding:5px 0px 5px 0px">' . $value . '<div>';
        }
    }

    /**
     * Builds select2 with the given parameters.
     * @param object data object containing data values of select2
     * @param string terminalVal terminal value of variable
     * @param string class class name(s) to be applied to the date picker
     * @param string idPrefix prefix to the id, before the variable abbrev
     * @param string idSuffix suffix to the id, after the variable abbrev
     * @param object disable object containint instructions for disabling date picker
     * @param string crossMethodAbbrev abbreviation of the record's cross method
     * @param string placeholder select2 placeholder text
     * @param string abbrev abbreviation of the variable
     * @param string dataName data name of the element
     */
    public function buildSelect2($data, $terminalVal, $class, $idPrefix, $idSuffix, $disable, $crossMethodAbbrev, $placeholder, $abbrev, $dataName) {
        return Select2::widget([
            'name' => 'input_type',
            'data' => $data,
            'value' => !empty($terminalVal) ? $terminalVal : '',
            'options' => [
                'autocomplete' => 'off',
                'placeholder' => $placeholder,
                'class' => $class,
                'id' => $idPrefix . '--' . $abbrev . '--' . $idSuffix,
                'title' => $disable['message'],
                'disabled' => $disable['disable'],
                'data-name' => $dataName,
                'cross-method-abbrev' => $crossMethodAbbrev,
                'data-label' => $dataName,
                'multiple' => false,
                'allowClear' => true,
                'oldValue' => $terminalVal,
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]);
    }

    /**
     * Builds numeric variable HTML elements
     * @param object crossMethodConfig the config applicable to the current data
     * @param object methodNumVarCompat array containing rules for harvest method-numeric variable correspondence
     * @param object model data model
     * @param string actionId id of the controller action
     */
    public function buildNumericVariableInput($crossMethodConfig, $methodNumVarCompat, $model, $actionId) {
        // Define variables
        $dataLevel = $actionId == 'plots' ? 'plot' : 'cross';
        $idPrefix = 'hm-numvar-id';
        $idSuffix = ($dataLevel == 'cross') ? 'cross-' . $model['crossDbId'] : 'plot-' . $model['plotDbId'];
        $class = 'hm-hdata-input hm-hdata-numvar-input hm-hdata-numvar-input--' . $idSuffix;
        $state = empty($model['state']) ? 'default' : $model['state'];
        if (!empty($model['stageCode']) && $model['stageCode'] == 'TCV') $state = 'TCV';
        $type = empty($model['type']) ? 'default' : $model['type'];
        $colorClass = 'light-grey darken-4';
        $title = '';
        $hoverTitle = '';
        $currentVal = '';
        $htmlElement = '';
        $currentHtmlElement = '';

        // Check if input will be disabled
        $disable = $this->disableInput($model, $dataLevel);
        // Get available harvest methods
        $committedMethod = isset($model['harvestMethod']) ? $model['harvestMethod'] : '';
        $uncommittedMethod = isset($model['terminalHarvestMethod']) ? $model['terminalHarvestMethod'] : '';
        // Based on the harvest method, determine the required numeric variables
        $committedRequiredNumVars = isset($methodNumVarCompat[$committedMethod]) ? $methodNumVarCompat[$committedMethod] : [];   
        $uncommittedRequiredNumVars = isset($methodNumVarCompat[$uncommittedMethod]) ? $methodNumVarCompat[$uncommittedMethod] : [];   
        $uncommittedNVCount = is_array($uncommittedRequiredNumVars) ? count($uncommittedRequiredNumVars) : 0;
        $committedNVCount = is_array($committedRequiredNumVars) ? count($committedRequiredNumVars) : 0;

        // Loop through each numeric variable config and build the html elements
        $stateConfig = isset($crossMethodConfig[$state]) ? $crossMethodConfig[$state] : (isset($crossMethodConfig['default']) ? $crossMethodConfig['default'] : []);
        $typeConfig = isset($stateConfig[$type]) ? $stateConfig[$type] : (isset($stateConfig['default']) ? $stateConfig['default'] : []);
        $numericVariables = isset($typeConfig['numeric_variables']) ? $typeConfig['numeric_variables'] : [];
        
        // If there are numeric variables in the config, build HTML elements
        if(!empty($numericVariables)) {
            foreach($numericVariables as $numericVariable) {
                $abbrev = $numericVariable['abbrev'];

                // Placeholder input
                if($abbrev == '<none>') {
                    // Get defaults
                    $id = $idPrefix . '--numeric-var-placeholder--' . $idSuffix;
                    $placeholder = $numericVariable['placeholder'];
                    // Determine wheter or not to hide the placeholder input.
                    $hidden = '';
                    if(!empty($uncommittedMethod) && $uncommittedNVCount == 1 && in_array("", $uncommittedRequiredNumVars)) $hidden = '';
                    else if(empty($uncommittedMethod) && !empty($committedMethod) && $committedNVCount == 1 && in_array("", $committedRequiredNumVars)) $hidden = '';
                    else if(empty($uncommittedMethod) && empty($committedMethod)) {
                        $hidden = '';
                    }
                    else $hidden = ' hidden';
                    
                    // Build the HTML element for the placeholder input
                    $htmlElement .= Html::input('number', $id, '', [
                        'id' => $id,
                        'class' => $class . $hidden,
                        'title' => empty($disable['message']) ? 'Numeric variable is not applicable.' : $disable['message'],
                        'disabled' => true,
                        'placeholder' => $placeholder,
                        'oninput' => "val = value||validity.valid||(value=val);",
                        'onkeypress' => null,
                        'min' => 1
                    ]);
                }
                // Actual numeric variables
                else {
                    // Get defaults
                    $id = $idPrefix . '--' . $abbrev . '--' . $idSuffix;
                    $placeholder = $numericVariable['placeholder'];
                    $type = $numericVariable['type'];
                    $subType = $numericVariable['sub_type'];
                    $minimum = isset($numericVariable['min']) ? $numericVariable['min'] : 0;
                    $onKeyPress = $subType == 'comma_sep_int' ? 'return event.charCode == 44 || (event.charCode >= 48 && event.charCode <= 57)' : null;
                    $committed = isset($model[$numericVariable['field_name']]) ? $model[$numericVariable['field_name']] : null;
                    $uncommitted = isset($model['terminal'.ucfirst($numericVariable['field_name'])]) ? $model['terminal'.ucfirst($numericVariable['field_name'])] : null;
                    // Determine wheter or not to hide the numeric var input
                    $hidden = '';
                    if(!empty($uncommittedMethod) && in_array($abbrev, $uncommittedRequiredNumVars)) $hidden = '';
                    else if(empty($uncommittedMethod) && !empty($committedMethod) && in_array($abbrev, $committedRequiredNumVars)) $hidden = '';
                    else $hidden = ' hidden';
                    // Committed numeric variable display logic
                    if($committed != null && $committed != "") {
                        $currentVal = $committed;
                        $title = $placeholder;
                        $hoverTitle = $title;
                        // If the numeric variable is required for the method, set color as green
                        if(in_array($abbrev, $committedRequiredNumVars)) {
                            $colorClass= 'light-green darken-4';
                        }
                        // If the numeric variable is not compatible with the committed method
                        else if(isset($committedRequiredNumVars[0]) && !empty($committedRequiredNumVars[0])) {
                            $colorClass = 'red lighten-2';
                            if(!empty($committedMethod)) $hoverTitle .= ' (Incompatible with committed method: ' . $committedMethod . ")";
                        }
                        // If the numeric variable is irrelevant to the committed method
                        else {
                            $colorClass= 'light-grey darken-4';
                            if(!empty($committedMethod)) $hoverTitle .= ' (Irrelevant to the committed method: ' . $committedMethod . ")";
                        }

                        $currentHtmlElement .= '<span title="Committed ' . $hoverTitle . '" style="margin-bottom:5px;" class="badge center ' . $colorClass . ' white-text">' . $title . ': ' . $currentVal . '</span>';
                    }

                    // Build the HTML element for the numeric variable input
                    $htmlElement .= Html::input($type, $id, $uncommitted, [
                        'id' => $id,
                        'class' => $class . $hidden,
                        'title' => empty($disable['message']) ? $disable['message'] : $disable['message'],
                        'disabled' => $disable['disable'],
                        'placeholder' => $placeholder,
                        'oninput' => "val = value||validity.valid||(value=val);",
                        'onkeypress' => $onKeyPress,
                        'min' => $minimum,
                        'subType' => $subType,
                        'oldValue' => $uncommitted,
                    ]);
                }
            }
        }
        // If there are no numeric variables in the config,
        // build default disabled input field
        else {
            // Get defaults
            $id = $idPrefix . '--numeric-var-placeholder--' . $idSuffix;
            $placeholder = 'Not applicable';
            // Determine wheter or not to hide the placeholder input.
            $hidden = '';
            
            // Build the HTML element for the placeholder input
            $htmlElement .= Html::input('number', $id, '', [
                'id' => $id,
                'class' => $class . $hidden,
                'title' => 'Numeric variable input is not supported.',
                'disabled' => true,
                'placeholder' => $placeholder,
                'oninput' => "val = value||validity.valid||(value=val);",
                'onkeypress' => null,
                'min' => 1
            ]);
        } 

        // Build final HTML element
        if($currentVal != '') {
            return '<div class="numeric-variable-committed" style="padding-top:12px">' . $currentHtmlElement . '</div><div class="numeric-variable">' . $htmlElement . '<div>';
        }
        else {
            return '<div class="numeric-variable-committed"></div><div class="numeric-variable-input">' . $htmlElement . '<div>';
        }
    }

    /**
     * Retrieves the appropriate variable compat object
     * given the plot/cross state and type
     * @param object model data model
     * @param object cmVarCompat the variable compat config for the cross method
     */
    public function getVariableCompatConfig($model, $cmVarCompat) {
        $state = $model['state'] ?? 'default';
        $type = $model['type'] ?? 'default';

        $stateVarCompat = $cmVarCompat[$state] ?? ($cmVarCompat['default'] ?? []);
        return $stateVarCompat[$type] ?? ($stateVarCompat['default'] ?? []);
    }

    /**
     * Retrieve transaction record for a location if existent.
     * Otherwise create new transaction.
     * @param integer locationId location identifier
     * @return integer transactionId the transaction id associated with the location
     */
    public function getTransactionOfLocation($locationId) {
        $userId = $this->user->getUserId();
        $requestBody = [
            "creatorDbId" => "$userId",
            "locationDbId" => "$locationId",
            "status"=>"not equals committed"
        ];
        $result = $this->terminalTransaction->searchAll($requestBody);
        $transactionId = isset($result['data'][0]['transactionDbId']) ? $result['data'][0]['transactionDbId'] : 0;
        
        // If there exists no non-committed transaction, create one 
        if($transactionId==0) {
            $requestBody = [
                "records"=>[
                    [
                        "locationDbId" => "$locationId",
                        "action" => "harvest manager"
                    ]
                ]
            ];
            $result = $this->terminalTransaction->create($requestBody);
            $transactionId = isset($result['data'][0]['transactionDbId']) ? $result['data'][0]['transactionDbId'] : 0;

            // If the transactionId is still 0 after creation, throw error excetion.
            if($transactionId == 0) {
                $errorMessage = 'Unable to create a transaction.';
                throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
            }
        }
        
        return $transactionId;
    }

    /**
     * Retrieves the dataset record given the entity id, variable id, and transaction
     * @param string entityId entity identifier
     * @param integer variableId variable identifier
     * @param integer transactionId transaction identifier
     * @return object dataset object
     */
    public function getDataset($entityId, $variableId, $transactionId) {
        // Build request body
        $requestBody = [
            "entityDbId" => "equals $entityId",
            "variableDbId" => "equals $variableId"
        ];

        // Fetch result
        $result = $this->apiHelper->sendRequest('POST','terminal-transactions/'.$transactionId.'/datasets-search',$requestBody);
        
        return isset($result[0]) ? $result[0] : [];
    }

    /**
     * Update the occurrence of the given transaction
     * based on its dataset summary.
     * @param integer transactionDbId transaction DB identifier
     * @return boolean whether the update was successful or not
     */
    public function updateOccurrenceOfTransaction($transactionId) {
        $result = $this->terminalTransaction->getDatasetSummary($transactionId);

        if (!empty($result['data'])) {
            // add the occurrence data of the transaction
            foreach ($result['data'] as $variableData) {
                $occurrences = $variableData["occurrences"];
                $newOccurrences = [];
                foreach ($occurrences as $occurrence) {
                    $newOccurrences[] = [
                        'variableDbIds' => $occurrence['variableDbIds'],
                        'occurrenceDbId' => $occurrence['occurrenceDbId'],
                        'dataUnit' => $occurrence['dataUnit'],
                        'dataUnitCount' => $occurrence['dataUnitCount'],
                    ];
                }
            }
            $params['occurrences'] = $newOccurrences;
            $method = 'PUT';
            $path = 'terminal-transactions/' . $transactionId;
            $updateResponse = $this->apiHelper->getParsedResponse($method, $path, json_encode($params));
            if (!$updateResponse['success']) {
                return false;
            }
            else return true;
        }

        return true;
    }

    /**
     * Inserts datasets into the transaction in the data terminal
     * @param integer locationId location identifier
     * @param array variableAbbrevs array containing variable abbrevs
     * @param array values array containing arrays for values
     * @param array recordIds array of record ids to update
     * @param string idType type of ids (plot/cross)
     */
    public function insertDataset($locationId, $variableAbbrevs, $values, $recordIds, $idType) {
        // Get transaction of location
        $transactionId = $this->getTransactionOfLocation($locationId);

        // Assemble prerequisites
        $variableAbbrev = "";
        $identifier = $idType . "_id";
        $requestBody = [
            "measurementVariables" => $variableAbbrevs,
            "records"=>[]
        ];

        // Loop through ids
        foreach($recordIds as $recordId) {
            // Loop through variable abbrevs
            foreach($variableAbbrevs as $key => $variableAbbrev) {
                $valueArray = $values[$key];
                // Loop through values
                foreach($valueArray as $value) {
                    // If value is empty, delete existing dataset for the variable (if any)
                    if(strlen($value)==0){
                        $value='null';
                        $variableId = $this->harvestManagerModel->getVariableId($variableAbbrev);
                        $dataset = $this->getDataset($recordId, $variableId, $transactionId);
                        if(isset($dataset['datasetDbId'])) {
                            $result = $this->transactionDataset->deleteOne($dataset['datasetDbId']);
                        }
                    }
                    // If not empty, add to records
                    else {
                        $requestBody["records"][] = [
                            $identifier => $recordId,
                            strtolower($variableAbbrev) => $value
                        ];
                    }
                }
            }
        }
        
        // If records is not empty, add datasets
        if(!empty($requestBody['records'])) {
            // uploads data
            $dataset = $this->apiHelper->sendRequest('POST','terminal-transactions/'.$transactionId.'/dataset-table',$requestBody);

            // validates data
            $validationResponse = $this->apiHelper->sendRequest('POST', 'terminal-transactions/' . $transactionId . '/validation-requests');

            // set transaction's status into uploaded
            $response = $this->terminalTransaction->updateOne($transactionId, ["status" => "uploaded"]);
        }

        // Check if transaction has any datasets
        $result = $this->apiHelper->sendRequest('POST','terminal-transactions/'.$transactionId.'/datasets-search');
        // If there are no datasets found, delete the transaction
        if(empty($result)) {
            $this->terminalTransaction->deleteOne($transactionId);
        }

        // update transaction of occurrence
        $occurrenceUpdated = $this->updateOccurrenceOfTransaction($transactionId);
        if(!$occurrenceUpdated) return $occurrenceUpdated;

        return isset($dataset[0]) ? true : false;
    }

    /**
     * Checks if the harvest status and state of the records are valid for bulk update
     * @param object records object containing the record information
     * @return array containing the count of completed, deletion, and invalid state records, and status and state lists 
     */
    public function checkHarvestStatusAndState($records) {
        $statusList = [];
        $completedCount = 0;
        $deletionCount = 0;
        $stateList = [];
        $invalidStateCount = 0;
        $totalUnupdateable = 0;
        $unsupportedStates = ['unknown'];

        foreach($records as $record) {
            $unableToUpdate = false;
            // If state is not supported, add to invalid state count
            if(isset($record['state']) && in_array($record['state'], $unsupportedStates)) {
                $invalidStateCount++;
                $unableToUpdate = true;
                if(!in_array($record['state'], $stateList)) $stateList[] = $record['state'];
            }
            // If harvest status is "COMPLETED", add to completed count
            if(isset($record['harvestStatus']) && $record['harvestStatus'] == 'COMPLETED') {
                $completedCount++;
                $unableToUpdate = true;
                if(!in_array($record['harvestStatus'], $statusList)) $statusList[] = $record['harvestStatus'];
            }
            // If harvest status is "DELETION_IN_PROGRESS", add to deletion count
            if(isset($record['harvestStatus']) && $record['harvestStatus'] == 'DELETION_IN_PROGRESS') {
                $deletionCount++;
                $unableToUpdate = true;
                if(!in_array($record['harvestStatus'], $statusList)) $statusList[] = $record['harvestStatus'];
            }

            // If unable to update record, add to unupdateable count
            if($unableToUpdate) $totalUnupdateable++;
        }

        return [
            'statusList' => $statusList,
            'completedCount' => $completedCount,
            'deletionCount' => $deletionCount,
            'stateList' => $stateList,
            'invalidStateCount' => $invalidStateCount,
            'totalUnupdateable' => $totalUnupdateable,
        ];
    }

    /**
     * Retrieves the records' harvest status and states. Supports plot level and cross level data.
     * @param integer occurrenceId occurence identifier
     * @param string dataLevel plot or cross
     * @param array recordIds database identifiers of the records
     */
    public function getRecordHarvestStatusAndState($occurrenceId, $dataLevel, $recordIds) {
        $requestBody = [];
        $result = [];

        // If record id array is empty, return empty result array
        if(empty($recordIds)) return $result;
        
        // Look up harvest status of selected items
        if($dataLevel == 'plot') {          // If data level is 'plot', retrieve plot data
            $ids = 'equals ' . (join("|equals ",$recordIds));
            $requestBody['fields'] = 'plot.id AS plotDbId|plot.harvest_status AS harvestStatus|germplasm.germplasm_state AS state';
            $requestBody['plotDbId'] = $ids;
            $result = $this->plotBrowserModel->searchAllPlots($occurrenceId, $requestBody);
        }
        else if($dataLevel == 'cross') {    // If data level is 'cross', retrieve cross data
            $ids = 'equals ' . (join("||equals ",$recordIds));
            $requestBody['fields'] = 'germplasmCross.id AS crossDbId|germplasmCross.harvest_status AS harvestStatus|crossFemaleAndMaleParent.germplasm_state AS state';
            $requestBody['crossDbId'] = $ids;
            $result = $this->crossBrowserModel->searchAllCrosses($occurrenceId, $requestBody);
        }

        return $result;
    }

    /**
     * Build numeric variable input HTML based on the config, id base, and class
     */
    public function buildNumVarInput($numVarConfig, $idBase, $class) {
        // Build element id
        $id = $idBase . '-' . $numVarConfig['abbrev'];
        // Determine onkeypress behavior
        $keyPress = '';
        if($numVarConfig['sub_type'] == 'single_int') $keyPress = 'return event.charCode >= 48 && event.charCode <= 57';
        if($numVarConfig['sub_type'] == 'comma_sep_int') $keyPress = 'return event.charCode == 44 || (event.charCode >= 48 && event.charCode <= 57)';
        // Return HTML element
        return '<div class="col-md-12 ' . $class . '-div" id="' . $id . '-div" style="font-size:14px">
                    <dl>
                        <dt title="' . $numVarConfig['placeholder'] . '" style="margin-bottom:5px">' . $numVarConfig['placeholder'] . '</dt>
                        <dd><input
                        id="' . $id . '" 
                        class="' . $class . '"
                        variableAbbrev="' . $numVarConfig['abbrev'] . '"
                        min="' . $numVarConfig['min'] . '"
                        subType="' . $numVarConfig['sub_type'] . '"
                        step="1"
                        placeholder="' . $numVarConfig['placeholder'] . '"
                        onkeypress="' . $keyPress . '" 
                        type="' . $numVarConfig['type'] . '"></dd>
                    </dl>
                </div>';
    }

    /**
     * Retrieves the total number of records in the browser,
     * given the current browser filters.
     * @param integer occurrenceId occurrence identifier
     * @param string dataLevel plot or cross
     * @return integer all records count
     */
    public function getAllRecordsCount($occurrenceId, $dataLevel) {
        // Retrieve filters from the session
        $browserParams = \Yii::$app->cbSession->get($dataLevel . "BrowserParams-$occurrenceId");
        $requestBody = $browserParams["filter"];
        $urlParams = [$browserParams["sort"],"limit=1"];
        $result = [
            "count" => 0
        ];
        // If data level is plot, retrieve plot count
        if($dataLevel == 'plot') {
            $result = $this->plotBrowserModel->getPlotRecords($occurrenceId, $requestBody, $urlParams);
        }
        // If data level is cross, retrieve cross count
        else if($dataLevel == 'cross') {
            $result = $this->crossBrowserModel->getCrossRecords($occurrenceId, $requestBody, $urlParams);
        }

        // Return the count from the result
        return isset($result["count"]) ? $result["count"] : 0;
    }

    /**
     * Retrieve record info of plots or crosses given the ids
     * @param array recordIds array containing plot or cross ids
     * @param integer occurrenceId occurrence identifier
     * @param string dataLevel plot or cross
     * @param integer limit max number of records to retrieve
     */
    public function getRecordInfo($recordIds, $occurrenceId, $dataLevel, $limit = null) {
        // Retrieve filters from the session
        $browserParams = \Yii::$app->cbSession->get($dataLevel . "BrowserParams-$occurrenceId");
        $requestBody = $browserParams["filter"];
        $urlParams = [$browserParams["sort"]];
        // If limit is set, add limit and page parameters
        if(isset($limit)) {
            $urlParams[] = 'limit=' . $limit;
            $urlParams[] = 'page=1';
        }

        // If data level is plot, retrieve plot info
        if($dataLevel == 'plot') {
            // If records is not empty, add ids to the request body
            if(!empty($recordIds)) {
                $requestBody = [
                    "plotDbId" => "equals " . join("|equals ", $recordIds)
                ];
            }
            // If request body is empty, set to null
            if(empty($requestBody)) $requestBody = null;
            // If limit is set, get 1st page of results
            if(isset($limit)) {
                $result = $this->plotBrowserModel->getPlotRecords($occurrenceId, $requestBody, $urlParams);
                $result = $result['data'] ?? [];
            }
            // Else, retrieve all
            else $result = $this->plotBrowserModel->searchAllPlots($occurrenceId, $requestBody, $urlParams);
            if(empty($result)) {
                $errorMessage = 'No plot records found.';
                throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
            }
            $result = $this->harvestManagerModel->matchConfigs($result, $occurrenceId, 'plot');
        }
        // If data level is cross, retrieve cross info
        else if($dataLevel == 'cross') {
            // If records is not empty, add ids to the request body
            if(!empty($recordIds)) {
                $requestBody = [
                    "crossDbId" => "equals " . join("||equals ", $recordIds)
                ];
            }
            // If request body is empty, set to null
            if(empty($requestBody)) $requestBody = null;
            // If limit is set, get 1st page of results
            if(isset($limit)) {
                $result = $this->crossBrowserModel->getCrossRecords($occurrenceId, $requestBody, $urlParams);
                $result = $result['data'] ?? [];
            }
            // Else, retrieve all
            else $result = $this->crossBrowserModel->searchAllCrosses($occurrenceId, $requestBody, $urlParams);
            if(empty($result)) {
                $errorMessage = 'No cross records found.';
                throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
            }
            $result = $this->harvestManagerModel->matchConfigs($result, $occurrenceId, 'cross');
        }

        return $result;
    }

    /**
     * Array format for records in bulk update and delete modals
     * @param record - record info array
     * @param dataLevel - current data level
     * @return array - bulk update plots format
     */
    public function bulkModalAllRecordsFormat($record, $dataLevel) {
        return [
            'id' => $record[$dataLevel.'DbId'].'',
            'state' => $record['state'],
            'committed' => [
                'harvestDate' => $record['harvestDate'].'',
                'harvestMethod' => $record['harvestMethod'].'',
                'noOfPlant' => $record['noOfPlant'].'',
                'noOfPanicle' => $record['noOfPanicle'].'',
                'specificPlantNo' => $record['specificPlantNo'].'',
                'noOfEar' => $record['noOfEar'].'',
                'noOfSeed' => (isset($record['noOfSeed']) ? $record['noOfSeed'] : '').'',
                'noOfBag' => (isset($record['noOfBag']) ? $record['noOfBag'] : '').'',
                'grainColor' => (isset($record['grainColor']) ? $record['grainColor'] : '').''
            ],
            'terminal' => [
                'harvestDate' => $record['terminalHarvestDate'].'',
                'harvestMethod' => $record['terminalHarvestMethod'].'',
                'noOfPlant' => $record['terminalNoOfPlant'].'',
                'noOfPanicle' => $record['terminalNoOfPanicle'].'',
                'specificPlantNo' => $record['terminalSpecificPlantNo'].'',
                'noOfEar' => $record['terminalNoOfEar'].'',
                'noOfSeed' => (isset($record['terminalNoOfSeed']) ? $record['terminalNoOfSeed'] : '').'',
                'noOfBag' => (isset($record['terminalNoOfBag']) ? $record['terminalNoOfBag'] : '').'',
                'grainColor' => (isset($record['terminalGrainColor']) ? $record['terminalGrainColor'] : '').''
            ],
            'harvestStatus' => $record['harvestStatus'],
            'crossMethodAbbrev' => (isset($record['crossMethodAbbrev']) ? $record['crossMethodAbbrev'] : (isset($record['plotDbId']) ? 'CROSS_METHOD_SELFING' : $record['crossMethodAbbrev'])).'',
            'stage' => $record['stageCode'] ?? '',
            'type' => $record['type'] ?? '',
            'notes' => $record['notes'] ?? '',
        ];
    }

    /**
     * Checks the committed and terminal values for the record.
     * If at least one record has a value for at least one harvest data variable,
     * return true. Otherwise, return false.
     * @param object recordInfo info object containing record data
     * @return boolean whether or not the records have harvest data
     */
    public function hasHarvestData($recordInfo, $harvestDataVarInfoArr)
    {
        $returnValue = [
            "hasData" => false,
            "hasCommitted" => false,
            "hasTerminal" => false
        ];

        $harvestDataVarArr = array_column($harvestDataVarInfoArr, 'apiFieldName') ?? [];
        if (!empty($harvestDataVarArr)) {
            foreach ($harvestDataVarArr as $column) {
                array_push($harvestDataVarArr, 'terminal' . ucfirst($column));
            }
        }

        $hasTerminalData = false;
        $hasCommittedData = false;
        $hasData = false;

        // Loop through each record
        foreach ($recordInfo as $record) {

            foreach ($harvestDataVarArr as $column) {
                $terminalCol = 'terminal' . ucfirst($column);
                if (
                    in_array($terminalCol, $harvestDataVarArr)
                    && isset($record[$terminalCol])
                    && !empty($record[$terminalCol])
                ) {
                    $hasTerminalData = true;
                    $hasData = true;
                }

                if (
                    in_array($column, $harvestDataVarArr)
                    && isset($record[$column])
                    && !empty($record[$column])
                ) {
                    $hasCommittedData = true;
                    $hasData = true;
                }

                $returnValue["hasTerminal"] = $hasTerminalData;
                $returnValue["hasCommitted"] = $hasCommittedData;
                $returnValue["hasData"] = $hasData;
            }
        }

        // If all values are empty, return false
        return $returnValue;
    }
    /**
     * Builds checkbox elements for the specified harvest data in the configs
     * @param array array containing config for each harvest data
     * @param array array containing numeric variable abbrevs
     * @return string checkbox HTML element string
     */
    public function buildHarvestDataCheckbox($harvestDataConfigs, $numVarAbbrevs)
    {
        $coreVariables = [];
        $numericVariables = [];
        $otherVariables = [];

        foreach ($harvestDataConfigs as $harvestData) {
            $abbrev = $harvestData['variableAbbrev'];
            $placeholder = $harvestData['displayName'] ?? "";
            if (empty($placeholder) || $placeholder == "") {
                $updatedStr = str_replace(['_', '-'], " ", $harvestData['variableAbbrev']);

                $updatedStr = explode(" ", $updatedStr);
                $updatedStrArr = array_map(function ($x) {
                    return ucfirst(strtolower($x));
                }, $updatedStr);
                $placeholder = implode(" ", $updatedStrArr);
            }

            $isNumVar = in_array($abbrev, $numVarAbbrevs);
            $numVarClass = $isNumVar ? ' hm-hd-bd-checkbox-item-num-var' : ' hm-hd-bd-checkbox-item-non-num-var';

            $currentHtmlElement = '<li><input type="checkbox" '
                . 'class="filled-in checkboxlist datacheck hm-hd-bd-checkbox hm-hd-bd-checkbox-item'
                . $numVarClass . '" data-id-string="" id="hm-hd-bd-checkbox-' . $abbrev
                . '" abbrev="' . $abbrev . '" /><label class="hm-hd-bd-checkbox' . $numVarClass
                . '" style="padding-left: 20px; font-weight: normal; color: black;" for="hm-hd-bd-checkbox-'
                . $abbrev . '">&nbsp;&nbsp; ' . $placeholder . '</label></li>';

            if ($abbrev == 'HVDATE_CONT') {
                $coreVariables[0] = $currentHtmlElement;
            } else if ($abbrev == 'HV_METH_DISC') {
                $coreVariables[1] = $currentHtmlElement;
            } else if ($isNumVar) {
                $numericVariables[] = $currentHtmlElement;
            } else {
                $otherVariables[] = $currentHtmlElement;
            }
        }

        return implode('', array_merge($otherVariables, $coreVariables, $numericVariables));
    }

    /**
     * Update the HARVEST_STATUS of the given records (plot or cross)
     * @param object recordIds object containing ids of records to update
     * @param string status new value for status
     * @param integer occurrenceId current occurrence id
     * @param string dataLevel plot or cross
     * @param boolean validateStatus whether or not harvest status process will be triggered, defaults to false
     * @return object backgroundJobInfo information on the created background job via processor
     */
    public function updateRecordsStatus($recordIds, $status, $occurrenceId, $dataLevel, $validateStatus = false) {
        // Build additional data for the background job record
        $additionalData = [
            "entity" => "OCCURRENCE",
            "entityDbId" => "$occurrenceId",
            "application" => "HARVEST_MANAGER",
            "endpointEntity" => $dataLevel == 'plot' ? "PLOT" : "CROSS",
            "description" => $dataLevel == 'plot' ? "Update of plots" : "Update of crosses"
        ];
        // Build request body for updateMany
        $requestBody = [
            "harvestStatus" => $status
        ];
        // If validate status is true, change request body
        if($validateStatus) {
            $requestBody = [
                "validateStatus" => "true"
            ];
        }
        // Perform updateMany
        if($dataLevel == 'plot') {              // If data level is plot, us plot model
            return $this->plot->updateMany(
                $recordIds,
                $requestBody,
                true,
                $additionalData
            );
        }
        else if($dataLevel == 'cross') {        // If data level is cross, us cross model
            return $this->cross->updateMany(
                $recordIds,
                $requestBody,
                true,
                $additionalData
            );
        }
    }

    /**
     * Revert the occurrence status to "planted"
     * when the occurrence has no plot data
     * @param Integer occurrenceId occurrence identifier
     */
    public function updateOccurrenceStatus($occurrenceId) {
        // Check if the occurrence has collected traits (plot data)
        $result = $this->apiHelper->sendRequest(
            'GET',
            "occurrences/$occurrenceId/plot-data-count"
        );

        $occurrence = $result[0] ?? [];
        $plotDataCount = $occurrence['plotDataCount'] ?? 0;

        // If occurrence has no plot data, update status to "planted"
        if($plotDataCount == 0) {
            $requestBody = [
                "occurrenceStatus" => "planted"
            ];
            $result = $this->apiHelper->sendRequest(
                'PUT',
                "occurrences/$occurrenceId",
                $requestBody
            );
        }
    }

    /**
     * Retrieves dataset ids given the record ids (plot or cross), the selected abbrevs,
     * and the location id with the transaction.
     * @param array recordIds array of plot or cross identifiers
     * @param array selectedAbbrevs array of selected variable abbrevs
     * @param integer locationId locaton identifier
     */
    public function getUncommittedTraitIds($recordIds, $selectedAbbrevs, $locationId) {
        $variableIds = [];
        $datasetIds = [];

        // Get user id
        $userId = $this->user->getUserId();
        // Get transaction id of location
        $transactionId = $this->getTransactionOfLocation($locationId);

        // Get variable ids
        foreach($selectedAbbrevs as $abbrev) {
            $requestBody = [
                "fields" => "variable.id AS variableDbId|variable.abbrev AS abbrev",
                "abbrev" => $abbrev
            ];
            $result = $this->variable->searchAll($requestBody);
            $data = isset($result['data']) ? $result['data'] : [];
            $variable = isset($data[0]) ? $data[0] : [];
            $variableIds[] = isset($variable['variableDbId']) ? $variable['variableDbId'] : 0;
        }

        // Get datasets
        foreach ($recordIds as $recordId) {
            if (is_object($recordId)) {
                $recordId = $recordId->plotDbId ?? $recordId->crossDbId;
            }

            $datasets = $this->getDatasets($recordId, $variableIds, $transactionId);

            foreach ($datasets as $dataset) {
                $datasetIds[] = $dataset['datasetDbId'];
            }
        }

        return $datasetIds;
    }

    /**
     * Perform retrieval of datasets given the record ids, variable ids, and transaction ids
     * @param array recordIds array containing plot or cross identifiers
     * @param array variableIds array containing variable identifiers
     * @param integer transactionId transaction identifier
     */
    public function getDatasets($recordIds, $variableIds, $transactionId) {
        $requestBody = [
            "entityDbId" => "equals ".$recordIds,
            "variableDbId" => "equals " . implode("|equals ",$variableIds)
        ];

        $result = $this->apiHelper->sendRequest('POST','terminal-transactions/'.$transactionId.'/datasets-search',$requestBody);

        return isset($result) ? $result : [];
    }

    /**
     * Delete transaction datasets via processor
     * @param array datasetIds array containing dataset ids to be deleted
     * @param integer locationId location identifier
     */
    public function deleteTransactionDatasetsViaProcessor($datasetIds, $locationId) {
        $entity = "LOCATION";
        $entityDbId = "".$locationId;
        $application = "HARVEST_MANAGER";
        $httpMethod = "DELETE";
        $endpoint = "v3/transaction-datasets";
        $description = "Deletion of datasets";
        $endpointEntity = "LOCATION";
        $deleteDataset = $this->apiHelper->processorBuilder($httpMethod, $endpoint, $datasetIds, $description, $entity, $entityDbId, $endpointEntity, $application);

        return isset($deleteDataset[0]) ? $deleteDataset[0] : null;
    }

    /**
     * Checks number of seeds created for the given record ids
     * @param array recordIds array containing plot or cross ids
     * @param string dataLevel plot or cross
     * @return integer seedCount the number of seeds found for the given plots/crosses
     */
    public function getSeedCount($recordIds, $dataLevel) {
        $count = 0;
        // Build request body
        $requestBody = [
            "fields" => "seed.harvest_source AS harvestSource",
            "harvestSource" => $dataLevel
        ];

        // Retrieve seed count based on data level
        foreach ($recordIds as $recordId) {
            if (is_object($recordId)) {
                $recordId = $recordId->plotDbId  ?? $recordId->crossDbId;
            }

            // If data level is plot, use plots/:id/seeds-search-lite
            if($dataLevel == 'plot') {
                $result = $this->apiHelper->sendRequestSinglePage(
                    'POST',
                    'plots/' . $recordId . '/seeds-search-lite',
                    $requestBody
                );
                $count += (isset($result['count']) ? $result['count'] : 0);
            }
            // If data level is cross, use crosses/:id/seeds-search-lite
            else if($dataLevel == 'cross') {
                $result = $this->apiHelper->sendRequestSinglePage(
                    'POST',
                    'crosses/' . $recordId . '/seeds-search-lite',
                    $requestBody
                );
                $count += (isset($result['count']) ? $result['count'] : 0);
            }
        }

        return $count;
    }
    
    /**
     * Checks if the given seeds have been used in entries
     * @param array recordIds - ids of records
     * @param string dataLevel - plot or cross
     * @return array - contains record ids segregated into:
     *                  1) records whose seeds have been used in entries
     *                  2) records whose seeds haven't been used in entries
     */
    public function checkSeedEntries($recordIds, $dataLevel) {
        $recordsToDelete = [];
        $recordsToDeleteCount = 0;
        $recordsUnableToDelete = [];
        $recordsUnableToDeleteCount = 0;
        $seedReference = '';
        // Build request body
        $requestBody = [
            "fields" => "seed.id AS seedDbId|seed.harvest_source AS harvestSource",
            "harvestSource" => $dataLevel
        ];

        // Retrieve seeds based on data level
        $seeds = [];
        foreach($recordIds as $recordId) {
            // If data level is plot, use plots/:id/seeds-search-lite
            if($dataLevel == 'plot') {
                $seedReference = 'sourcePlotDbId';
                $requestBody['fields'] .= "|seed.source_plot_id AS $seedReference";
                $result = $this->apiHelper->sendRequest(
                    'POST',
                    'plots/' . $recordId . '/seeds-search-lite',
                    $requestBody,
                    null,
                    'seeds'
                );
                // Get seeds array from result
                $plot = isset($result[0]) ? $result[0] : [];
                $plotSeeds = isset($plot['seeds']) ? $plot['seeds'] : [];
                $seeds = array_merge($seeds, $plotSeeds);
            }
            // If data level is cross, use crosses/:id/seeds-search-lite
            else if($dataLevel == 'cross') {
                $seedReference = 'crossDbId';
                $requestBody['fields'] .= "|seed.cross_id AS $seedReference";
                $result = $this->apiHelper->sendRequest(
                    'POST',
                    'crosses/' . $recordId . '/seeds-search-lite',
                    $requestBody,
                    null,
                    'seeds'
                );
                // Get seeds array from result
                $cross = isset($result[0]) ? $result[0] : [];
                $crossSeeds = isset($cross['seeds']) ? $cross['seeds'] : [];
                $seeds = array_merge($seeds, $crossSeeds);
            }
        }

        $seedDbIds = [];
        $seedSource = [];
        $recordsWithSeeds = [];
        // Store ids in arrays
        foreach($seeds as $seed) {
            $seedDbIds[] = $seed["seedDbId"];
            $seedSource[$seed["seedDbId"]] = $seed[$seedReference] . "";
            if(!in_array($seed[$seedReference], $recordsWithSeeds)) $recordsWithSeeds[] = $seed[$seedReference];
        }

        foreach($seedDbIds as $seedDbId) {
            // Retrieve entry count of the seed
            $result = $this->apiHelper->sendRequestSinglePage(
                'POST',
                'seeds/' . $seedDbId . '/entries-count',
                null
            );
            $entriesCount = isset($result['count']) ? $result['count'] : 0;
            
            // If seed has entries, add record to array for unable to delete
            if($entriesCount > 0 && !in_array($seedSource[$seedDbId], $recordsUnableToDelete)) {
                $recordsUnableToDelete[] = $seedSource[$seedDbId];
                $recordsUnableToDeleteCount += 1;

                // If record has been added previously to records to delete, remove
                if(in_array($seedSource[$seedDbId],$recordsToDelete)) {
                    $recordsToDelete = array_diff($recordsToDelete, [$seedSource[$seedDbId]]);
                    $recordsToDeleteCount -= 1;
                }
            }
            else if ($entriesCount == 0 
                && !in_array($seedSource[$seedDbId],$recordsToDelete) 
                && !in_array($seedSource[$seedDbId],$recordsUnableToDelete)
            )
            { // Else, add to delete array
                $recordsToDelete[] = $seedSource[$seedDbId];
                $recordsToDeleteCount += 1;
            }
        }

        // // Add ids of records without seeds to the records to delete array
        $recordsWithoutSeeds = array_diff(array_diff($recordIds, $recordsToDelete), $recordsUnableToDelete);
        $recordsToDelete = array_merge($recordsToDelete, $recordsWithoutSeeds);
        $recordsToDeleteCount = count($recordsToDelete);

        return [
            "recordsToDelete" => json_encode($recordsToDelete),
            "recordsToDeleteCount" => $recordsToDeleteCount,
            "recordsUnableToDelete" => json_encode($recordsUnableToDelete),
            "recordsUnableToDeleteCount" => $recordsUnableToDeleteCount
        ];
    }

    /**
     * Retrieve db ids to delete
     * @param array recordDbIds - object containing ids of records (plot or cross)
     * @param array selectedAbbrevs - object containing selected traits to delete
     * @param string dataLevel - plot or cross
     * @param boolean hasSeeds - boolean value. whether or not the records have seeds
     * @param boolean skipData - boolean value. whether or not to skip plot/cross data retrieval
     * @return array - database ids of packages, seeds, germplasm names,
     *  germplasm, and harvest data to delete
     */
    public function getDatabaseIdsToDelete($recordDbIds, $selectedAbbrevs, $dataLevel, $hasSeeds, $skipData = false) {
        $variableIds = [];
        $allSeedDbIds = [];
        $allGermplasmDbIds = [];
        $allGermplasmSeedRelation = [];
        $allHarvestDataDbIds = [];
        $seedsToDelete = [];

        // Retrieve variable ids
        foreach($selectedAbbrevs as $abbrev){
            $requestBody = [
                "fields" => "variable.id AS variableDbId|variable.abbrev AS abbrev",
                "abbrev" => $abbrev
            ];
            $result = $this->variable->searchAll($requestBody);
            $data = isset($result['data']) ? $result['data'] : [];
            $variable = isset($data[0]) ? $data[0] : [];
            $variableIds[] = isset($variable['variableDbId']) ? $variable['variableDbId'] : 0;
        }
        // Build variable ids string
        $variableIds = "equals " . implode("|equals ", $variableIds);

        // if records are Objects, extract plotDbIds or crossDbIds
        if (isset($recordDbIds[0]) && $recordDbIds[0] !== null && is_object($recordDbIds[0])) {
            $columnName = 'plotDbId';
            if (!isset($recordDbIds[0]->plotDbId) && isset($recordDbIds[0]->crossDbId)) {
                $columnName = 'crossDbId';
            }
            $recordDbIds = array_column($recordDbIds, $columnName);
        }

        // Loop through records
        foreach($recordDbIds as $recordDbId) {
            if(!$skipData) {
                $requestBody = [
                    "variableDbId" => $variableIds
                ];
                $harvestData = [];
                // Retrieve harvest data based on data level
                if($dataLevel == 'plot') {
                    $result = $this->apiHelper->sendRequest(
                        'POST',
                        'plots/' . $recordDbId . '/data-search',
                        $requestBody,
                        null,
                        'data'
                    );
                    // Get harvest data array
                    $plot = isset($result[0]) ? $result[0] : [];
                    $plotData = isset($plot['data']) ? $plot['data'] : [];
                    $harvestData = array_merge($harvestData, $plotData);
                }        
                else if($dataLevel == 'cross') {
                    $result = $this->apiHelper->sendRequest(
                        'POST',
                        'crosses/' . $recordDbId . '/data-search',
                        $requestBody,
                        null,
                        'data'
                    );
                    // Get harvest data array
                    $cross = isset($result[0]) ? $result[0] : [];
                    $crossData = isset($cross['data']) ? $cross['data'] : [];
                    $harvestData = array_merge($harvestData, $crossData);
                }
                // Store ids in array
                $harvestDataDbIds = [];
                foreach($harvestData as $hData) {
                    $harvestDataDbIds[] = $hData[$dataLevel.'DataDbId'];
                }
                $allHarvestDataDbIds = array_merge($allHarvestDataDbIds,$harvestDataDbIds);
            }

            // If records do not have seeds, skip search of
            // seeds, packages, and germplasm records
            if(!$hasSeeds) {
                continue;
            }

            // Search seeds
            // If data level is plot, use plots/:id/seeds-search-lite
            if($dataLevel == 'plot') {
                $result = $this->apiHelper->sendRequest(
                    'POST',
                    'plots/' . $recordDbId . '/seeds-search-lite',
                    null,
                    null,
                    'seeds'
                );
            }
            // If data level is cross, use crosses/:id/seeds-search-lite
            else if ($dataLevel == 'cross') {
                $result = $this->apiHelper->sendRequest(
                    'POST',
                    'crosses/' . $recordDbId . '/seeds-search-lite',
                    null,
                    null,
                    'seeds'
                );
            }
            // Store seed ids and germplasm ids
            $record = isset($result[0]) ? $result[0] : [];
            $seeds = isset($record['seeds']) ? $record['seeds'] : [];
            $seedDbIds = [];
            $germplasmDbIds = [];
            $germplasmSeedRelation = [];
            foreach($seeds as $seed) {
                $seedDbIds[] = $seed["seedDbId"];
                $germplasmDbIds[] = $seed["germplasmDbId"];
                $germplasmSeedRelation[$seed["germplasmDbId"]][] = $seed["seedDbId"];
                $allGermplasmSeedRelation[$seed["germplasmDbId"]][] = $seed["seedDbId"];
            }
            $allSeedDbIds = array_merge($allSeedDbIds,$seedDbIds);

            // Check if other seeds are using the germplasm
            $germplasmDbIdsTemp = [];
            foreach($germplasmDbIds as $germplasmDbId) {
                // Use germplasm/:id/seeds-search-lite
                $result = $this->apiHelper->sendRequest(
                    'POST',
                    'germplasm/' . $germplasmDbId . '/seeds-search-lite',
                );
                // Store ids in arrays
                $germplasm = isset($result[0]) ? $result[0] : [];
                $germplasmSeeds = isset($germplasm['seeds']) ? $germplasm['seeds'] : [];
                $germplasmSeedDbIds = [];
                foreach($germplasmSeeds as $gs) {
                    $germplasmSeedDbIds[] = $gs['seedDbId'];
                }

                // If the germplasm is not referrenced by other seeds,
                // add it to the array
                if(count(array_diff($germplasmSeedDbIds,$allSeedDbIds))==0) {
                    $germplasmDbIdsTemp[] = $germplasmDbId;

                    // Remove previous seeds linked to this germplasm
                    $seedsToDelete = array_diff($seedsToDelete, $allGermplasmSeedRelation[$germplasmDbId]);
                }
                else { // Otherwise, add the seed to the array
                    $seedsToDelete = array_merge($seedsToDelete, $germplasmSeedRelation[$germplasmDbId]);
                }
            }
            $allGermplasmDbIds = array_merge($allGermplasmDbIds,$germplasmDbIdsTemp);
        }

        return [
            "seedDbIds" => array_values(array_unique($seedsToDelete)),
            "germplasmDbIds" => array_values(array_unique($allGermplasmDbIds)),
            "harvestDataDbIds" => array_values(array_unique($allHarvestDataDbIds)),
            "recordDbIds" => array_values(array_unique($recordDbIds))
        ];
    }

    /**
     * Delete the given databaseIds via processor
     * @param array databaseIds - ids of records to be deleted
     * @param integer locationDbId - location identifier
     * @param boolean deleteTraits - whether or not to delete traits, defaults to true
     * @param string dataLevel - plot or cross, defaults to plot
     * @return - array containing background job records
     */
    public function performDeleteViaProcessor($databaseIds, $locationDbId, $deleteTraits=true, $dataLevel='plot') {
        $seedDbIds = $databaseIds['seedDbIds'];
        $germplasmDbIds = $databaseIds['germplasmDbIds'];
        $harvestDataDbIds = $databaseIds['harvestDataDbIds'];

        $entity = "LOCATION";
        $entityDbId = "".$locationDbId;
        $application = "HARVEST_MANAGER";

        if(count($seedDbIds) > 0) {
            // Delete seeds
            $httpMethod = "DELETE";
            $endpoint = "v3/seeds";
            $description = "Deletion of seeds";
            $endpointEntity = "SEED";
            $dependents = [
                "packages",
                "seed-relations",
                "seed-attributes"
            ];
            $optional = [ "dependents" => $dependents ];
            $deleteSeed = $this->apiHelper->processorBuilder($httpMethod, $endpoint, $seedDbIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional);
        }

        if(count($germplasmDbIds) > 0) {
            // Delete germplasm
            $httpMethod = "DELETE";
            $endpoint = "v3/germplasm";
            $description = "Deletion of germplasm";
            $endpointEntity = "GERMPLASM";
            $dependents = [
                "packages",
                "seed-relations",
                "seed-attributes",
                "seeds",
                "family-members",
                "germplasm-relations",
                "germplasm-attributes",
                "germplasm-names"
            ];
            $optional = [ "dependents" => $dependents ];
            $deleteGermplasm = $this->apiHelper->processorBuilder($httpMethod, $endpoint, $germplasmDbIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional);
        }

        // Delete harvest data and update records status
        if($deleteTraits) {
            // Delete harvest data
            $httpMethod = "DELETE";
            $endpoint = "v3/" . $dataLevel . "-data";
            $description = "Deletion of " . $dataLevel . " data";
            $endpointEntity = strtoupper($dataLevel)."_DATA";
            $deleteHarvestData = $this->apiHelper->processorBuilder($httpMethod, $endpoint, $harvestDataDbIds, $description, $entity, $entityDbId, $endpointEntity, $application);
        }

        return [
            "seed" => isset($deleteSeed[0]) ? $deleteSeed[0] : null,
            "germplasm" => isset($deleteGermplasm[0]) ? $deleteGermplasm[0] : null,
            "harvestData" => isset($deleteHarvestData[0]) ? $deleteHarvestData[0] : null
        ];
    }

    /**
     * Retrieve background job infos
     * @param array backgroundJobIds - array containing ids of background jobs to retrieve
     * @return array containing background jobs
     */
    public function getBackgroundJobs($backgroundJobIds) {
        $backgroundJobs = [];

        foreach($backgroundJobIds as $jobId) {
            $result = $this->apiHelper->sendRequest('GET','/v3/background-jobs/'.$jobId);
            $backgroundJobs[] = isset($result[0]) ? $result[0] : null;
        }

        return [
            'backgroundJobs' => $backgroundJobs
        ];
    }

    /**
     * Builds the parameters for the notification retrieval calls
     * @param String $workerName - worker names separated by pipe |
     * @param String $remars - worker remarks to specify for DeleteHarvestRecords worker
     * @param String $isSeen - string 'true' or 'false', whether or not the notif has been seen
     * @return Object containing the parameters
     */
    public function buildNotifRetrievalParams($workerName, $remarks, $isSeen) {
        // Get user Id
        $userId = $this->user->getUserId();

        // If DeleteHarvestRecords is included, set multiple condition groups
        if(str_contains($workerName, 'DeleteHarvestRecords')) {
            $pcs = explode('|',$workerName);
            $nonDeleteWorkers = [];
            foreach($pcs as $pc) {
                if($pc != 'DeleteHarvestRecords') $nonDeleteWorkers[] = $pc;
            }

            return [
                'conditions' => [
                    [
                        "creatorDbId" => "equals $userId",
                        "workerName" => "equals " . implode("|equals ", $nonDeleteWorkers),
                        "isSeen" => "equals $isSeen"
                    ],
                    [
                        "creatorDbId" => "equals $userId",
                        "workerName" => "equals DeleteHarvestRecords",
                        "jobRemarks" => $remarks,
                        "isSeen" => "equals $isSeen"
                    ]
                ]
            ];
        }
        else {
            return [
                "creatorDbId" => "equals $userId",
                "workerName" => "equals $workerName",
                "isSeen" => "equals $isSeen"
            ];
        }
    }

    /**
     * Generates chips for harvest status filters
     * @param Array statusArray array containing distinct 
     * @param Integer occurrenceId occurrence identifier
     * @param String dataLevel occurrence data level
     */
    public function generateHarvestStatusChips($statusArray, $occurrenceId, $dataLevel) {

        // Config
        $statusDisplay = [
            'NO_HARVEST' => [
                'colorClass' => 'grey darken-2 white-text',
                'text' => 'No harvest'
            ],
            'INCOMPLETE' => [
                'colorClass' => 'red darken-2 white-text',
                'text' => 'Incomplete data'
            ],
            'CONFLICT' => [
                'colorClass' => 'red darken-2 white-text',
                'text' => 'Data donflict'
            ],
            'FAILED' => [
                'colorClass' => 'red darken-2 white-text',
                'text' => 'Failed'
            ],
            'BAD_QC_CODE' => [
                'colorClass' => 'red darken-2 white-text',
                'text' => 'Bad QC Code'
            ],
            'INVALID_STATE' => [
                'colorClass' => 'red darken-2 white-text',
                'text' => 'Invalid State'
            ],
            'DELETION_IN_PROGRESS' => [
                'colorClass' => 'orange darken-2 white-text',
                'text' => 'Deletion in progress'
            ],
            'REVERT_IN_PROGRESS' => [
                'colorClass' => 'orange darken-2 white-text',
                'text' => 'Revert in progress'
            ],
            'HARVEST_IN_PROGRESS' => [
                'colorClass' => 'orange darken-2 white-text',
                'text' => 'Harvest in progress'
            ],
            'UPDATE_IN_PROGRESS' => [
                'colorClass' => 'orange darken-2 white-text',
                'text' => 'Update in progress'
            ],
            'READY' => [
                'colorClass' => 'blue darken-2 white-text',
                'text' => 'Ready'
            ],
            'COMPLETED' => [
                'colorClass' => 'green darken-2 white-text',
                'text' => 'Harvest complete'
            ],
            'QUEUED_FOR_HARVEST' => [
                'colorClass' => 'grey lighten-2 grey-text text-darken-3',
                'text' => 'Queued for harvest'
            ],
            'QUEUED_FOR_UPDATE' => [
                'colorClass' => 'grey lighten-2 grey-text text-darken-3',
                'text' => 'Queued for update'
            ]
        ];
        $defaultStatus = 'NO_HARVEST';
        $checkIcon = "<i class='material-icons hm-occurrence-info'>check</i>";
        $defaultHtml = '';
        $generalHtml = '';
        $checkedStatus = [];

        // Get applied status values
        $appliedStatus = \Yii::$app->cbSession->get("harvest-status-".$dataLevel."-".$occurrenceId) ?? [];

        // Loop through status values
        foreach($statusArray as $status) {
            $statusParts = explode(":",$status);
            $actualStatus = $statusParts[0] ?? '';

            // If the status has already been added, skip
            if(!in_array($actualStatus, $checkedStatus)) $checkedStatus[] = $actualStatus;
            else continue;

            // If the status is not known, skip
            if(!in_array($actualStatus,array_keys($statusDisplay))) continue;

            // Set colors and text
            $statusConfig = $statusDisplay[$actualStatus] ?? [];
            $colorClass = $statusConfig['colorClass'] ?? '';
            $text = $statusConfig['text'] ?? '';
            $selectedIcon = '';
            $selectedValue = 0;

            // If the status is applied, add check icon
            if(!empty($appliedStatus) && in_array($actualStatus, $appliedStatus)) {
                $selectedIcon = $checkIcon;
                $selectedValue = 1;
            }

            // If default status is detected, build specific html
            if ($actualStatus == $defaultStatus) {
                $defaultHtml = '<span is-selected="' . $selectedValue . '" title="' . $text . '" id="' . $actualStatus . '" class="hm-hd-harvest-status-chip badge center ' . $colorClass . '">' .$selectedIcon . ' ' . $text . '</span>';
            }
            else $generalHtml .= '<span is-selected="' . $selectedValue . '" title="' . $text . '" id="' . $actualStatus . '" class="hm-hd-harvest-status-chip badge center ' . $colorClass . '">' .$selectedIcon . ' ' . $text . '</span>';
        }

        $quickFilters = "<div id='hm-hd-quick-filters'><b>Quick filters: <a id='hm-hd-clear-quick-filters' href='#'>Clear all</a></b></div>";

        return $quickFilters . $defaultHtml . $generalHtml;
    }

    /**
     * Retrieves distinct values for the given field names.
     * @param String $dataLevel data to be retrieved (supports 'plot' and 'cross')
     * @param Array $fieldNames array of field names in string format
     * @param Integer $occurrenceId occurrence ID where plots or crosses are to be retrieved from
     * @return Array array containing distinct values for each field name
     */
    public function getDistinctValues($dataLevel, $fieldNames, $occurrenceId) {
        // Initialize result object
        $result = [];
        $distinctValuesArr = [];
        // Form distinct string
        $distinctString = implode("|", (array) $fieldNames);
        // Build request body
        $requestBody = [
            "distinctOn" => $distinctString
        ];

        // Retrieve plots
        if($dataLevel == 'plot') {
            $result = $this->plotBrowserModel->searchAllPlots($occurrenceId, $requestBody, [ 'isBasic=true' ]);
        }
        // Retrieve crosses
        else if($dataLevel == 'cross') {
            $result = $this->crossBrowserModel->searchAllCrosses($occurrenceId, $requestBody, [ 'isBasic=true' ] );
        }

        // Loop through results
        foreach($result as $record) {
            // Loop through field names
            foreach($fieldNames as $field) {
                $value = $record[$field];
                // If the field is not yet set in the distinct array, set to empty array
                if(!isset($distinctValuesArr[$field])) $distinctValuesArr[$field] = [];
                // If the value is not empty, and is not yet found in the array for the field, add to array
                if(!empty($value) && !in_array($value,$distinctValuesArr[$field])) $distinctValuesArr[$field][] = $record[$field];
            }
        }

        // Return distinct values array
        return $distinctValuesArr;
    }

    /**
     * Renders the contents of the bulk update modal.
     *
     * This method retrieves configuration items and harvest method values from the session,
     * builds HTML input elements for each configuration item, and returns the concatenated
     * HTML string.
     *
     * @return string The HTML content for the bulk update modal.
    */
    public function renderBulkUpdateModalContents() {
        $harvestMethodValues = \Yii::$app->session->get("harvestMethodValues");
        $numVarConfigItems = \Yii::$app->session->get("numVarConfigItems");
        $hDataConfigItems = \Yii::$app->session->get("hDataConfigItems");

        $hdataElements = '';
        $numVarElements = '';

        // Harvest Data
        foreach($hDataConfigItems as $hdata) {
            if($hdata['variableAbbrev'] == 'HV_METH_DISC') {
                $hdata['inputOptions'] = [];
                foreach($harvestMethodValues as $value) {
                    $hdata['inputOptions']['dataOptions'][$value] = $value;
                }
            }
            $hdataElements .= $this->buildHTMLInputElement($hdata);
        }

        // Numeric Variables
        foreach($numVarConfigItems as $numVar) {
            $numVarElements .= $this->buildHTMLInputElement($numVar, true);
        }

        return [
            "hdataElements" => $hdataElements,
            "numVarElements" => $numVarElements
        ];
    }

    /**
     * Builds an HTML input element based on the provided configuration item.
     *
     * This method generates an HTML input element (e.g., datepicker, select2) based on the
     * input type specified in the configuration item. It also sets various attributes and
     * options for the input element.
     *
     * @param array $configItem The configuration item containing details for the input element.
     *                          - 'variableAbbrev': string (required) The variable abbreviation.
     *                          - 'apiFieldName': string (required) The API field name.
     *                          - 'inputType': string (required) The type of input element ('datepicker', 'select2', 'number').
     *                          - 'inputOptions': array (optional) Additional options for the input element.
     *                              - 'useScaleValues': bool (optional) Whether to use scale values for select2.
     *                              - 'dataOptions': array (optional) Data options for select2.
     * @param bool $isNumVar Whether the input element is for a numeric variable.
     * @return string The HTML string for the input element.
    */
    public function buildHTMLInputElement($configItem, $isNumVar = false) {
        $variableAbbrev = $configItem['variableAbbrev'] ?? null;
        $inputType = $configItem['inputType'] ?? null;
        // Get variable display name
        $variable = $this->variable->getVariableByAbbrev($variableAbbrev);
        $displayName = $variable['displayName'] ?? '';

        $variableDisplayNames = \Yii::$app->session->get("variableDisplayNames") ?? [];
        $variableDisplayNames[$variableAbbrev] = $displayName;
        \Yii::$app->session->set("variableDisplayNames", $variableDisplayNames);

        // Define classes and ids for HTML elements.
        // To be used for jQuery event binding, and element visibility.
        $class = 'hm-hd-bu-input';
        $id = 'hm-hd-bu-input-'.$variableAbbrev;
        $divClass = 'hm-hd-bu-input-div hidden';
        $divId = 'hm-hd-bu-input-div-'.$variableAbbrev;
        $removeButtonClass = 'remove-hdata-input';
        $removeButtonId = 'remove-hdata-input-'.$variableAbbrev;
        // Add class for numeric variable inputs
        if($isNumVar) {
            $class .= ' hm-hd-bu-num-var-input';
            $divClass .= ' hm-hd-bu-num-var-input-div';
            $removeButtonClass .= ' hm-hd-bu-num-var-remove';
        }
        
        // Final input element
        $inputElement = '';
        
        // Build the HTML element based on the input type
        if($inputType == 'datepicker') {
            $inputElement = DatePicker::widget([
                'name' => $id,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'value' => '',
                'options' => [
                    'class' => $class,
                    'id' => $id,
                    'title' => $displayName,
                    'autocomplete' => 'off',
                    'variableAbbrev' => $variableAbbrev,
                ],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true,
                    'clearBtn' => true,
                    'zIndexOffset' => '9000',
                    'startDate'=> date("1970-01-01")
                ],
                'pluginEvents' => [
                    'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                ]
    
            ]);
        }
        else if($inputType == 'select2') {
            $data = [];
            $inputOptions = $configItem['inputOptions'] ?? [];
            $useScaleValues = $inputOptions['useScaleValues'] ?? false;
            $dataOptions = $inputOptions['dataOptions'] ?? [];

            // If useScaleValues is true, retrieve the scale values of the variable
            // and use that as data for the select2 element.
            if($useScaleValues) {
                $data = $this->harvestManagerModel->getScaleValues($variableAbbrev, false, [
                    "key" => "value",
                    "value" => "value"
                ]);
            }
            // If dataOptions is set, use that as select2 data.
            else if(!empty($dataOptions)) {
                $data = $dataOptions;
            }

            $inputElement = Select2::widget([
                'name' => $id,
                'id' => $id,
                'class' => $class,
                'data' => $data,
                'options' => [
                    'placeholder' => $displayName,
                    'variableAbbrev' => $variableAbbrev,
                    'class' => $class,
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]);
        }
        else if($inputType == 'number') {
            $inputOptions = $configItem['inputOptions'] ?? [];
            $minimumValue = $inputOptions['minimumValue'] ?? 1;
            $inputSubType = $inputOptions['inputSubType'] ?? 1;
            $onKeyPress = $inputSubType == 'comma_sep_int' ? 'return event.charCode == 44 || (event.charCode >= 48 && event.charCode <= 57)' : null;
            $type = $inputSubType == 'comma_sep_int' ? 'text' : 'number';

            // Build the HTML element for the numeric variable input
            $inputElement = Html::input($type, $id, null, [
                'id' => $id,
                'class' => $class,
                'title' => $displayName,
                'disabled' => false,
                'placeholder' => $displayName,
                'oninput' => "val = value||validity.valid||(value=val);",
                'onkeypress' => $onKeyPress,
                'min' => $minimumValue,
                'subType' => $inputSubType,
                'variableAbbrev' => $variableAbbrev
            ]);
        }

        // Return the HTML element
        return "
            <div class='harvest-data-input-div $divClass row' id='$divId' style='margin-bottom: 10px;'>
                <div class='col-md-1'>
                    <button class='btn btn-danger $removeButtonClass' title = 'Remove input field' id='$removeButtonId'>
                        <i class='material-icons'>remove</i>
                    </button>
                </div>
                <div class='col-md-11' style='font-size:14px; padding-left: 25px;'>
                    <dl>
                        <dt title='$variableAbbrev'>$displayName</dt>
                        <dd>$inputElement</dd>
                    </dl>
                </div>
            </div>
        ";
    }
}

?>