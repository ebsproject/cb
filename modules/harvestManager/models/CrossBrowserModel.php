<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import yii
use yii\data\Sort;
// Import data providers
use app\dataproviders\ArrayDataProvider;
// Import interfaces
use app\interfaces\modules\harvestManager\models\IApiHelper;
use app\interfaces\modules\harvestManager\models\ICrossBrowserModel;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
// Import models
use app\models\Cross;

/**
* Model class for Cross browser
*/
class CrossBrowserModel extends Cross implements ICrossBrowserModel
{    
    public $crossFemaleParent;
    public $crossMaleParent;
    public $crossingDate;
    public $crossDbId;
    public $crossMethod;
    public $crossMethodAbbrev;
    public $crossName;
    public $femaleGermplasmState;
    public $femaleParentage;
    public $femaleParentSeedSource;
    public $femaleSourceSeedEntry;
    public $femaleSourcePlot;
    public $harvestStatus;
    public $maleGermplasmState;
    public $maleParentage;
    public $maleParentSeedSource;
    public $maleSourceSeedEntry;
    public $maleSourcePlot;
    public $remarks;
    public $femaleEntryCode;
    public $femaleEntryNumber;
    public $maleEntryCode;
    public $maleEntryNumber;

    /**
     * Model constructor
     */
    public function __construct(
        public IApiHelper $apiHelper,
        public IHarvestManagerModel $harvestManagerModel)
    { }

    public function rules()
    {
        return [
            [['crossFemaleParent', 'crossMaleParent', 'crossName', 'crossDbId', 'femaleGermplasmState', 'maleGermplasmState'], 'required'],
            [['crossDbId'], 'number'],
            [['crossDbId'], 'integer'],
            [['crossFemaleParent', 'crossMaleParent', 'crossingDate', 'crossMethod', 'crossMethodAbbrev', 'crossName', 'femaleGermplasmState', 
                'femaleParentage', 'femaleParentSeedSource', 'femaleSourceSeedEntry', 'femaleSourcePlot', 'maleGermplasmState', 
                'maleParentage', 'maleParentSeedSource', 'maleSourceSeedEntry', 'maleSourcePlot', 'remarks', 'femaleEntryCode', 'femaleEntryNumber', 'maleEntryCode', 'maleEntryNumber'], 
                'string'],
            [['crossDbId', 'crossFemaleParent', 'crossMaleParent', 'crossingDate', 'crossMethod', 'crossMethodAbbrev', 'crossName', 
                'femaleGermplasmState', 'femaleParentage', 'femaleParentSeedSource', 'femaleSourceSeedEntry', 'femaleSourcePlot', 'maleGermplasmState', 
                'maleParentage', 'maleParentSeedSource', 'maleSourceSeedEntry', 'maleSourcePlot', 'remarks', 'femaleEntryCode', 'femaleEntryNumber', 'maleEntryCode', 'maleEntryNumber'], 
                'safe'],
            [[], 'boolean'],
            [[], 'unique']
        ];
    }

    /**
     * Retrieve cross records to display
     * @param object params browser parameters
     * @param integer experimentId experiment identifier
     * @param integer occurrenceId occurrence identifier
     * @param integer locationId location identifier
     * @return object containing data provider
     */
    public function search($params, $experimentId, $occurrenceId, $locationId) {
        // Check if browser pagination and fitlers will be reset
        $reset = $this->harvestManagerModel->browserReset($occurrenceId, $params, 'CrossBrowserModel');
        $resetPage = $reset['resetPage'];
        $resetFilters = $reset['resetFilters'];

        // If resetFilters = true, set params to null
        if($resetFilters) {
            $params = null;
        }
        // Load params into the browser search model
        $this->load($params);

        // Assemble URL parameters
        $urlParams = $this->harvestManagerModel->assembleUrlParameters(
            'dynagrid-harvest-manager-cross-hdata-grid',
            'sort=crossDbId:ASC',
            $resetPage
        );

        // Save url params to session for potential use in bulk operations -> apply to all
        $sessionSort = null;
        foreach($urlParams as $urlParam) {
            if(str_contains($urlParam, "sort=")) {
                $sessionSort = $urlParam;
                break;
            }
        }

        // Assemble browser filters
        $columnFilters = $this->assembleBrowserFilters($params, $occurrenceId);

        // Save sort and filter to session
        \Yii::$app->cbSession->set("crossBrowserParams-$occurrenceId", [
            "sort" => $sessionSort,
            "filter" => $columnFilters
        ]);

        // Get plots
        $result = $this->getCrossRecords($occurrenceId, $columnFilters, $urlParams);
        $crosses = $result['data'];
        $totalCount = $result['count'];

        // If no crosses are retrieved and totalCount == 0, reset the pagination.
        if(count($crosses) == 0 && $totalCount == 0){
            // Assemble URL parameters
            $urlParams = $this->harvestManagerModel->assembleUrlParameters(
            'dynagrid-harvest-manager-cross-hdata-grid',
            'sort=crossDbId:ASC',
            true
            );

            // Get crosses
            $result = $this->getCrossRecords($occurrenceId, $columnFilters, $urlParams);
            $crosses = $result['data'];
            $totalCount = $result['count'];    
        }

        // Get appropriate harvest data configs for the crosses
        $crosses = $this->harvestManagerModel->matchConfigs($crosses, $occurrenceId, 'cross');

        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'allModels' => $crosses,
            'key' => 'crossDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);
        // Initialize sort
        $sort = new Sort();
        $sort->attributes = [
            'crossDbId'=>[
                'asc' => ["crossDbId"=>SORT_ASC],
                'desc' => ["crossDbId"=>SORT_DESC]
            ],
            'crossName'=>[
                'asc' => ["crossName"=>SORT_ASC],
                'desc' => ["crossName"=>SORT_DESC]
            ],
            'crossFemaleParent'=>[
                'asc' => ["crossFemaleParent"=>SORT_ASC],
                'desc' => ["crossFemaleParent"=>SORT_DESC]
            ],
            'crossMaleParent'=>[
                'asc' => ["crossMaleParent"=>SORT_ASC],
                'desc' => ["crossMaleParent"=>SORT_DESC]
            ],
            'crossMethod'=>[
                'asc' => ["crossMethod"=>SORT_ASC],
                'desc' => ["crossMethod"=>SORT_DESC]
            ],
            'remarks'=>[
                'asc' => ["remarks"=>SORT_ASC],
                'desc' => ["remarks"=>SORT_DESC]
            ],
            'crossingDate'=>[
                'asc' => ["crossingDate"=>SORT_ASC],
                'desc' => ["crossingDate"=>SORT_DESC]
            ],
            'femaleSourceSeedEntry'=>[
                'asc' => ["femaleSourceSeedEntry"=>SORT_ASC],
                'desc' => ["femaleSourceSeedEntry"=>SORT_DESC]
            ],
            'maleSourceSeedEntry'=>[
                'asc' => ["maleSourceSeedEntry"=>SORT_ASC],
                'desc' => ["maleSourceSeedEntry"=>SORT_DESC]
            ],
            'femaleParentSeedSource'=>[
                'asc' => ["femaleParentSeedSource"=>SORT_ASC],
                'desc' => ["femaleParentSeedSource"=>SORT_DESC]
            ],
            'maleParentSeedSource'=>[
                'asc' => ["maleParentSeedSource"=>SORT_ASC],
                'desc' => ["maleParentSeedSource"=>SORT_DESC]
            ],
            'femaleSourcePlot'=>[
                'asc' => ["femaleSourcePlot"=>SORT_ASC],
                'desc' => ["femaleSourcePlot"=>SORT_DESC]
            ],
            'maleSourcePlot'=>[
                'asc' => ["maleSourcePlot"=>SORT_ASC],
                'desc' => ["maleSourcePlot"=>SORT_DESC]
            ],
            'femaleGermplasmState'=>[
                'asc' => ["femaleGermplasmState"=>SORT_ASC],
                'desc' => ["femaleGermplasmState"=>SORT_DESC]
            ],
            'maleGermplasmState'=>[
                'asc' => ["maleGermplasmState"=>SORT_ASC],
                'desc' => ["maleGermplasmState"=>SORT_DESC]
            ],
            'femaleParentage'=>[
                'asc' => ["femaleParentage"=>SORT_ASC],
                'desc' => ["femaleParentage"=>SORT_DESC]
            ],
            'maleParentage'=>[
                'asc' => ["maleParentage"=>SORT_ASC],
                'desc' => ["maleParentage"=>SORT_DESC]
            ],
            'femaleEntryCode'=>[
                'asc' => ["femaleEntryCode"=>SORT_ASC],
                'desc' => ["femaleEntryCode"=>SORT_DESC]
            ],
            'femaleEntryNumber'=>[
                'asc' => ["femaleEntryNumber"=>SORT_ASC],
                'desc' => ["femaleEntryNumber"=>SORT_DESC]
            ],
            'maleEntryCode'=>[
                'asc' => ["maleEntryCode"=>SORT_ASC],
                'desc' => ["maleEntryCode"=>SORT_DESC]
            ],
            'maleEntryNumber'=>[
                'asc' => ["maleEntryNumber"=>SORT_ASC],
                'desc' => ["maleEntryNumber"=>SORT_DESC]
            ]
        ];
        // Set default order
        $sort->defaultOrder = [
            'crossDbId' => SORT_DESC
        ];
        // Apply sort to data provider
        $dataProvider->setSort($sort);

        return array(
            'dataProvider' => $dataProvider,
        );
    }

    /**
     * Retrieves crosses of the current occurrence in the given location
     * @param integer occurrenceId occurrence identifier
     * @param object requestBody column filters
     * @param array urlParams url params for sorting, pagination, and page size
     */
    public function getCrossRecords($occurrenceId, $requestBody, $urlParams) {
        $method = 'POST';
        $endpoint = "occurrences/$occurrenceId/harvest-cross-data-search";

        // return $this->apiHelper->sendRequestSinglePage($method, $endpoint, $requestBody, $urlParams);
        $result = $this->apiHelper->sendRequestSinglePage($method, $endpoint, $requestBody, $urlParams);
        // Trim values
        $result['data'] = $this->harvestManagerModel->trimValues($result['data']);
        return $result;
    }

    /**
     * Retrieve crosses given the request body and url parameters
     * @param integer occurrenceId occurrence identifier
     * @param object requestBody column filters
     * @param array urlParams url params for sorting, pagination, and page size
     */
    public function searchAllCrosses($occurrenceId, $requestBody = null, $urlParams = null) {
        $method = 'POST';
        $endpoint = "occurrences/$occurrenceId/harvest-cross-data-search";

        // return $this->apiHelper->sendRequest($method, $endpoint, $requestBody, $urlParams);
        $crosses = $this->apiHelper->sendRequest($method, $endpoint, $requestBody, $urlParams);
        // Return crosses with trimmed values
        return $this->harvestManagerModel->trimValues($crosses);
    }

    /**
     * Retrieves the distinct values for the specified field of SELFING crosses
     * given the occurrence id
     * @param integer occurrenceId occurrence identifier
     * @param string field name of the field for which distinct values will be retrieved
     * @param boolean includeSelfing whether or not filter cross method abbrev with SELFING only
     */
    public function getDistinctField($occurrenceId, $field, $includeSelfing = true) {
        // Get distinct values for the field
        $requestBody = [
           "distinctOn" => $field
       ];
       if($includeSelfing) {
           $requestBody['crossMethodAbbrev'] = "equals CROSS_METHOD_SELFING";
       }
       $result = $this->searchAllCrosses($occurrenceId, $requestBody);
       
       // Loop through the SELFING crosses
       $values = [];
       foreach($result as $crosses) {
           $values[] = $crosses[$field];
       }

       return $values;
    }

    /**
     * Assemble browser filters
     * @param object params - parameters passed by data browser
     * @param integer occurrenceId - occurrenceIdentifier
     * @return object columnFilters - browser filters in API request body format
     */
    public function assembleBrowserFilters($params, $occurrenceId) {
        $filters = isset($params['CrossBrowserModel']) ? $params['CrossBrowserModel'] : [];
        $columnFilters = [];
        $invalidStateFilter = '';
        $noHarvestFilter = '';

        // Retrieve supported states
        $stateScale = $this->harvestManagerModel->getScaleValues('GERMPLASM_STATE');
        // Remove 'unknown' state from scale values
        unset($stateScale['unknown']);
        // Get array of supported states from scale
        $supportedStates = array_values($stateScale);

        foreach ($filters as $key => $value) {
            if ($value != "") {
                $equalsString = '';
                if (!is_array($value) && !str_contains($value, '%')) $equalsString = 'equals ';
                if (is_array($value)) {
                    $columnFilters[$key] = $equalsString . implode('||' . $equalsString, $value);
                } else if (str_contains($value, '||')) {
                    $columnFilters[$key] = $equalsString . implode("||" . $equalsString, explode("||", $value));
                } else {
                    $columnFilters[$key] = $equalsString . $value;
                }
            }
        }

        // Check if observation data filters are set
        $sessionVar = \Yii::$app->cbSession->get("observation-filter-cross-" . $occurrenceId);
        if (!empty($sessionVar)) {
            $observationFilter = isset($sessionVar) ? (array) json_decode($sessionVar) : [];
            $fields = $observationFilter['fields'];
            $withData = $observationFilter['withData'];
            $or = $withData ? '[OR] ' : '';

            foreach ($fields as $field) {
                $terminalField = 'terminal' . ucfirst($field);
                $columnFilters[$field] = $or . ($withData ? ' not' : '') . " null";
                $columnFilters[$terminalField] = $or . ($withData ? ' not' : '') . " null";
            }
        }

        // Get POST parameters
        $postParams = \Yii::$app->request->post();

        // Get harvest status filters from session
        $hsSession = \Yii::$app->cbSession->get("harvest-status-cross-" . $occurrenceId);
        // Set default status.
        // Defaults to NO_HARVEST (on initial page load).
        // If 'NO_HARVEST' is not in the distinct status values,
        // Select the first status available as default.
        $default = ['NO_HARVEST'];
        $distinctHarvestStatus = $params['distinctHarvestStatus'] ?? [];
        if (!empty($distinctHarvestStatus)) {
            if (!in_array('NO_HARVEST', $distinctHarvestStatus)) {
                $default = [$distinctHarvestStatus[0]];
            }
        }
        // If session variable is not yet set, use default value.
        if (!isset($hsSession) && !isset($postParams['harvestStatus'])) {
            $hsSession = $default;
            \Yii::$app->cbSession->set("harvest-status-cross-" . $occurrenceId, $hsSession);
        }
        // If post params contain filters for harvest status, update session variable.
        if (isset($postParams['harvestStatus'])) {
            $hsSession = json_decode($postParams['harvestStatus']);
            \Yii::$app->cbSession->set("harvest-status-cross-" . $occurrenceId, $hsSession);
        }

        // Build harvest status filter
        $harvestStatusFilters = [];
        foreach ($hsSession as $status) {
            if (in_array($status, ["INCOMPLETE", "CONFLICT"])) {
                $harvestStatusFilters[] = $status . "%";
                continue;
            } else if ($status == "INVALID_STATE") {
                $harvestStatusFilters[] = "[OR] equals " . $status;
                $invalidStateFilter = "equals unknown";
                continue;
            } else if ($status == "NO_HARVEST") {
                $harvestStatusFilters[] = "equals " . $status;
                $noHarvestFilter = "equals " . implode("||equals ", $supportedStates);
                continue;
            }
            $harvestStatusFilters[] = "equals " . $status;
        }
        if (!empty($harvestStatusFilters)) {
            $columnFilters['harvestStatus'] = implode("||", $harvestStatusFilters);
        }
        
        // Get session range filter
        $sessToolbarFilters = \Yii::$app->cbSession->get("toolbar-filter-cross-" . $occurrenceId);

        // If session variable is not yet set, use default value (empty array).
        if (!isset($sessToolbarFilters)) {
            $sessToolbarFilters = [];
            \Yii::$app->cbSession->set("toolbar-filter-cross-" . $occurrenceId, $sessToolbarFilters);
        }
        // Check if has toolbar filter
        if (isset($postParams['toolbarFilters']) && !empty($postParams['toolbarFilters'])) {
            $sessToolbarFilters = $postParams['toolbarFilters'];
            \Yii::$app->cbSession->set("toolbar-filter-cross-" . $occurrenceId, $sessToolbarFilters);
        }

        // Reset range filter if removeRangeFilter is set to true
        if (!isset($postParams['toolbarFilters']) && isset($postParams['removeRangeFilter']) && strtolower($postParams['removeRangeFilter']) == 'true') {
            $sessToolbarFilters = [];
            \Yii::$app->cbSession->set("toolbar-filter-cross-" . $occurrenceId, $sessToolbarFilters);
        }

        // Get toolbar filters
        $toolbarFilters = [];
        if (!empty($sessToolbarFilters)) {
            foreach ($sessToolbarFilters as $filter => $values) {
                $toolbarFilters[$filter] = $values;
            }
        }

        // Build column filters from toolbar filters
        if (!empty($toolbarFilters)) {
            foreach ($toolbarFilters as $ind => $val) {
                $columnFilters[$ind] = $val;
            }
        }

        $conditionSelfing = $columnFilters;
        $conditionNonSelfing = $columnFilters;

        // Check if invalid state filter is set
        if (!empty($invalidStateFilter)) {
            // Copy current column filters
            $conditionSelfing['crossMethodAbbrev'] = "equals CROSS_METHOD_SELFING";
            $conditionNonSelfing['crossMethodAbbrev'] = "not equals CROSS_METHOD_SELFING";

            if (!empty($columnFilters['femaleGermplasmState'])) {
                if (isset($columnFilters['femaleGermplasmState']) && $invalidStateFilter != $columnFilters['femaleGermplasmState']) {
                    $conditionSelfing['femaleGermplasmState'] = 'equals none';
                } else {
                    $conditionSelfing['femaleGermplasmState'] = '[OR] ' . $invalidStateFilter;
                }
            } else {
                $conditionSelfing['femaleGermplasmState'] = '[OR] ' . $invalidStateFilter;
            }

            // Assemble filter with mulitple conditions
            $columnFilters = [
                'conditions' => [
                    $conditionSelfing,
                    $conditionNonSelfing
                ]
            ];
        }

        // Check if no_harvest is selected
        if (!empty($noHarvestFilter)) {
            // Copy current column filters
            $conditionSelfing['crossMethodAbbrev'] = "equals CROSS_METHOD_SELFING";
            $conditionNonSelfing['crossMethodAbbrev'] = "not equals CROSS_METHOD_SELFING";

            if (empty($conditionSelfing['femaleGermplasmState'])) {
                $conditionSelfing['femaleGermplasmState'] = $noHarvestFilter;
            }

            // Assemble filter with mulitple conditions
            $columnFilters = [
                'conditions' => [
                    $conditionSelfing,
                    $conditionNonSelfing
                ]
            ];
        }

        return $columnFilters;
    }
}