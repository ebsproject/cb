<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import interfaces
use app\interfaces\controllers\IDashboard;
use app\interfaces\models\IExperiment;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IProgram;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\models\IVariable;
use app\interfaces\models\IWorker;
use app\interfaces\modules\harvestManager\models\IApiHelper;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;

/**
* General model class for the Harvest Manager module
*/
class HarvestManagerModel implements IHarvestManagerModel{
    /**
     * Model constructor
     */
    public function __construct(
        public IDashboard $dashboard,
        public IExperiment $experiment,
        public IOccurrence $occurrence,
        public IProgram $program,
        public IVariable $variable,
        public IWorker $worker,
        public IApiHelper $apiHelper,
        public IUserDashboardConfig $userDashboardConfig
        )
    { }

    /**
     * Verifies if the URL Parameters are complete and valid.
     * Returns an error and error message if conditions are not met.
     * @param string parameters URL parameters passed onto the controller action
     * @param string url the current page url
     * @return array array containing (1) error - boolean, if there is an error or none, and (2) errorMessage
     */
    public function validateUrlParameters($parameters, $url) {
        // Check if url came from a pjax request
        $fromPjax = str_contains($url, 'pjax');

        // If the request is NOT pjax, perform validation
        if(!$fromPjax) {
            // Check if at least one required URL Parameter is empty/unset
            $paramsEmpty = false;
            $missingParams = [];
            foreach($parameters as $pKey => $parameter) {
                $paramsEmpty = $paramsEmpty || empty($parameter);
                if(empty($parameter)) $missingParams[] = $pKey;
            }
            if($paramsEmpty) {
                return [
                    'error' => true,
                    'errorMessage' => \Yii::t('app', 'Required URL parameter' . (count($missingParams) == 1 ? '' : 's') . ' missing: ' . implode(', ', $missingParams))
                ];
            }

            // Check if source string is valid (plots or crosses)
            if(in_array('source', array_keys($parameters))) {
                $source = $parameters['source'];
                if($source != 'plots' && $source != 'crosses') {
                    return [
                        'error' => true,
                        'errorMessage' => \Yii::t('app', 'Invalid parameter: Source string must be "plots" or "crosses".')
                    ];
                }
            }

            // Check if occurrence id is an integer
            $occurrenceId = $parameters['occurrenceId'];
            if(!preg_match("/^\\d+$/i", $occurrenceId)) {
                return [
                    'error' => true,
                    'errorMessage' => \Yii::t('app', 'Invalid parameter: OccurrenceId must be an integer.')
                ];
            }

            // // REMOVED. For CORB-5719, this is removed to make way for the `renderAccess` function.
            // // This will be kept in the code in case it becomes needed again.
            // // Check if occurrence exists
            // $requestBody = [ "occurrenceDbId" => "$occurrenceId" ];
            // $result = $this->occurrence->searchAll($requestBody);
            // if(empty($result['data'])) {
            //     return [
            //         'error' => true,
            //         'errorMessage' => \Yii::t('app', 'Occurrence does not exist. Please select a valid occurrence.')
            //     ];
            // }
        }

        // No error
        return [
            'error' => false,
            'errorMessage' => \Yii::t('app', 'No errors found.')
        ];
    }

    /**
     * Retrieve program code, given the ID
     * @param integer programId program identifier
     * @return string programCode
     */
    public function getProgram($programId) {
        $requestBody = [ "programDbId" => "$programId" ];
        $result = $this->program->searchAll($requestBody);
        
        return isset($result['data'][0]['programCode']) ? $result['data'][0]['programCode'] : null;
    }

    /**
     * Validates the program in the URL parameters
     * @param string program program code string from the controller action
     * @param integer occurrenceId (optional) the occurrence id from the controller action
     * @return array array containing (1) redirect - boolean, if redirection will occur, and (2) the program to use
     */
    public function validateUrlProgram($program, $occurrenceId = null) {
        // Get program in dashboard
        $defaultFilters = (array) $this->dashboard->getFilters();
        $programId = $defaultFilters['program_id'];
        $dashboardProgram = $this->getProgram($programId);
        if(empty($program) && !empty($dashboardProgram)) {
            // If program is not set, redirect to this action with the program in the dashboard
            return [
                "redirect" => true,
                "program" => $dashboardProgram
            ];
        }
        else if(!empty($program) && !empty($occurrenceId)) {
            // Get occurence's program
            $requestBody = [ "occurrenceDbId" => "$occurrenceId" ];
            $result = $this->occurrence->searchAll($requestBody);
            $occurrenceProgram = $result['data'][0]['programCode'];
            // If URL program does not match the occurrence's program, 
            // redirect to this action with the occurrence's program
            if($program != $occurrenceProgram) {
                return [
                    "redirect" => true,
                    "program" => $occurrenceProgram
                ];
            }
        }

        // No redirection
        return [
            "redirect" => false,
            "program" => null
        ];
    }

    /**
     * Retrieves the variable id given the abbrev
     * @param string variableAbbrev the abbreviation of the variable
     * @return integer variable id
     */
    public function getVariableId($variableAbbrev) {
        // Search variable
        $requestBody = [
            "fields" => "variable.id AS variableDbId|variable.abbrev AS abbrev",
            "abbrev" => $variableAbbrev
        ];
        $result = $this->variable->searchAll($requestBody);
        $variable = isset($result['data'][0]) ? $result['data'][0] : [];
        return isset($variable['variableDbId']) ? $variable['variableDbId'] : 0;
    }

    /**
     * Retrieves the scale value of the given variable abbrev
     * @param string variableAbbrev the abbreviation of the variable
     * @param boolean scAbbrevMode whether or not to use scale value abbrev as key
     * @param array kvSettings default key and value to be used for the scale values
     * @return array scale value array where the key is the "value" and the value is the "displayName
     */
    public function getScaleValues($variableAbbrev, $scAbbrevMode = false, $kvSettings = ["key"=>"value","value"=>"displayName"]) {
        // Get variable id given the abbrev
        $variableId = $this->getVariableId($variableAbbrev);

        // Retrieve scale values
        $method = "POST";
        $endpoint = "variables/$variableId/scale-values-search";
        $result = $this->apiHelper->sendRequest($method, $endpoint);
        $scaleValues = isset($result[0]['scaleValues']) ? $result[0]['scaleValues'] : [];

        $scaleValArr = [];
        // Build scale value array
        if($scAbbrevMode) {
            // For harvest data browsers
            foreach($scaleValues as $scaleVal) {
                $scaleValArr[$scaleVal['abbrev']] = [$scaleVal[$kvSettings['key']] => $scaleVal[$kvSettings['value']]];
            }
        }
        else {
            // For general use
            foreach($scaleValues as $scaleVal) {
                $scaleValArr[$scaleVal[$kvSettings['key']]] = $scaleVal[$kvSettings['value']];
            }
        }
        

        return $scaleValArr;
    }

    /**
     * Check if the experiment is a Cross Parent Nursery.
     * If CPN, check status if 'planted' or 'planted;crossed'
     * @param integer experimentId experiment identifier
     * @return array array containing: (1) isCPN - boolean, whether or not experiment is CPN
     *                  (2) crossed - boolean, whether or not CPN has been crossed
     *                  (3) phase - string, CPN Phase (I or II)
     *                  (4) title - string, title to show on tab hover
     *                  (4) class - optional class name for the link
     */
    public function checkCPN($experimentId) {
        $info = [
            "isCPN" => false,
            "crossed" => false,
            "phase" => "",
            "title" => "",
            "class" => ""
        ];

        $result = $this->experiment->getOne($experimentId);
        $experiment = isset($result['data']) ? $result['data'] : [];
        // If no experiment found, return default info
        if(empty($experiment)) return $info;

        $experimentType = $experiment['experimentType'];
        $experimentStatus = $experiment['experimentStatus'];

        // If experiment is not CPN, return default info
        if($experimentType != "Cross Parent Nursery") {
            return $info;
        }
        // If experiment is CPN, check status
        else {
            // If CPN is crossed
            if(strpos($experimentStatus, 'planted') !== false && strpos($experimentStatus, 'crossed') !== false) {
                return [
                    "isCPN" => true,
                    "crossed" => true,
                    "phase" => "II",
                    "title" => "View crosses",
                    "class" => null
                ];
            }
            // If CPN is NOT crossed
            else {
                return [
                    "isCPN" => true,
                    "crossed" => false,
                    "phase" => "I",
                    "title" => "Experiment is not crossed yet",
                    "class" => "hm-unavailable"
                ];
            }
        }
    }

    /**
     * Determines the pagination key format for the browser grid
     * Formats: 'page', 'db-N-page'
     * @param object queryParams browser URL parameters
     * @return string paginationKey the pagination key for the browser grid
     */
    public function getPaginationKey($queryParams) {
        // Get array keys of queryParams
        $qpKeys = array_keys($queryParams);
        // Get dp-N-pages keys in queryParams
        $dpKey = array_values(preg_grep('/^dp-\\d+-page$/', array_keys($queryParams)));
        // If 'page' is found as a key in queryParams, set it as key
        if(in_array('page',$qpKeys)) {
            return 'page';
        }
        // If 'dp-N-page' is found as a key in queryParams, set it as key
        if(!empty($dpKey)) {
            return $dpKey[0];
        }

        return '';
    }

    /**
     * Determine if the pagination and filters will be reset
     * @param mixed uniqueIdentifier any unique identifier for the session (eg. occurenceId, userId, etc.)
     * @param object queryParams query parameters of the browser
     * @param string searchModelName name of search model
     */
    public function browserReset($uniqueIdentifier, $queryParams, $searchModelName) {
        // Get post parameters
        $postParams = \Yii::$app->request->post();

        // If bulkOperation flag is set to true, do not reset page and filters
        if(!empty($postParams['bulkOperation']) && boolval($postParams['bulkOperation'])) {
            return [
                'resetPage' => false,
                'resetFilters' => false
            ];
        }

        // Set pagination key
        $key = $this->getPaginationKey($queryParams);

        // Check post parameters
        if(!empty($postParams)) {
            // If forceReset = true (post parameter), reset pagination and filters
            if(!empty($postParams['forceReset']) && boolval($postParams['forceReset'])) {
                // Set current filter as empty array
                \Yii::$app->cbSession->set("$searchModelName-$uniqueIdentifier", []);
                // Reset query params
                $queryParams[$searchModelName] = [];
                $queryParams[$key] = "1";
                $queryParams['sort'] = null;
                \Yii::$app->request->setQueryParams($queryParams);

                return [
                    'resetPage' => true,
                    'resetFilters' => true
                ];
            }
        }

        // Check browser filters
        $previousParams = \Yii::$app->cbSession->get("$searchModelName-$uniqueIdentifier");
        $currentParams = isset($queryParams[$searchModelName]) ? $queryParams[$searchModelName] : [];
        // If change was detected, reset pagination
        if($previousParams != $currentParams) {
            // Store current filters
            \Yii::$app->cbSession->set("$searchModelName-$uniqueIdentifier", $currentParams);
            // Reset query params
            $queryParams[$searchModelName] = $currentParams;
            $queryParams[$key] = "1";
            $queryParams['sort'] = null;
            \Yii::$app->request->setQueryParams($queryParams);
            return [
                'resetPage' => true,
                'resetFilters' => false
            ];
        }
        else {
            // Store current filters
            \Yii::$app->cbSession->set("$searchModelName-$uniqueIdentifier", $currentParams);
        }

        return [
            'resetPage' => false,
            'resetFilters' => false
        ];
    }

    /**
     * Checks if the given string (haystack) contains
     * all the elements in the given array (needles)
     * @param String haystack the string to be checked
     * @param Array needles array of elements to look for in the haystack
     * @return Boolean whether or not the string contains all elements
     */
    public function stringContainsAll($haystack, array $needles) {
        foreach ($needles as $needle) {
            if (strpos($haystack, $needle) === false) {
                return false;
            }
        }
        return true;
    }

    /**
     * Assemble the URL parameters for 
     * pagination and sorting of the browser
     * @param string gridId grid identifier
     * @param string defaultSortString default sorting when no sort is set
     * @param boolean resetPage to reset pagination or not
     */
    public function assembleUrlParameters($gridId, $defaultSortString, $resetPage) {
        // Sorting
        $paramSort = '';
        $currentUrl = \Yii::$app->request->getUrl();
        $getParams = \Yii::$app->request->getQueryParams();

        // Validate if request is from creation tab
        $creationTabGridIds = [
            'dynagrid-harvest-manager-plot-crt-grid',
            'dynagrid-harvest-manager-cross-crt-grid'
        ];
        $crtPjax = $this->stringContainsAll($currentUrl, $creationTabGridIds);

        if(!$crtPjax && strpos($currentUrl,"$gridId-pjax")!==false) {
            if(isset($getParams['sort'])) {
                $paramSort = 'sort=';
                $sortParams = $getParams['sort'];
                $sortParams = explode('|', $sortParams);
                $countParam = count($sortParams);
                $currParam = 1;

                foreach($sortParams as $column) {
                    if($column[0] == '-') {
                        $paramSort = $paramSort . substr($column, 1) . ':DESC';
                    } else {
                        $paramSort = $paramSort . $column . ':ASC';
                    }
                    if($currParam < $countParam) {
                        $paramSort = $paramSort . '|';
                    }
                    $currParam += 1;
                }

            }
            else $paramSort = $defaultSortString;
        } else $paramSort = $defaultSortString;

        // Pagination
        $paramPage = '';
        if($resetPage) {
            $paramPage = 'page=1';
            if (isset($getParams[$gridId . '-page'])) {
                $getParams[$gridId . '-page'] = 1;
            }
            else if (isset($getParams['dp-1-page'])){
                $getParams['dp-1-page'] = 1;
            }
            else if (isset($getParams['page'])){
                $getParams['page'] = 1;
            }
            \Yii::$app->request->setQueryParams($getParams);
        }
        else if (isset($getParams[$gridId . '-page'])){
            $paramPage = 'page=' . $getParams[$gridId . '-page'];
        }
        else if (isset($getParams['dp-1-page'])){
            $paramPage = 'page=' . $getParams['dp-1-page'];
        } else if (isset($getParams['page'])) {
            $paramPage = 'page=' . $getParams['page'];
        }
        
        // Page limit
        $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

        $queryParams = [];
        if($paramLimit!='') $queryParams[] = $paramLimit;
        if($paramPage!='') $queryParams[] = $paramPage;
        if($paramSort!='') $queryParams[] = $paramSort;

        return $queryParams;
    }

    /**
     * Invokes the deletion worker
     * @param Integer $occurrenceId occurrence identifier
     * @param String $deletionMode mode of deletion (harvest data or package)
     * @param Object $parameters object containing parameters for the deletion
     */
    public function deleteInBackground($occurrenceId, $deletionMode, $parameters) {
        $desc = "Delete $deletionMode for occurrence ID: " . $occurrenceId;
        if($deletionMode == "revert") $desc = "Revert harvest for occurrence ID: " . $occurrenceId;
        // Invoke worker
        $result = $this->worker->invoke(
            'DeleteHarvestRecords',                 // Worker name
            $desc,                                  // Description
            'OCCURRENCE',                           // Entity
            $occurrenceId,                          // Entity ID
            'OCCURRENCE',                           // Endpoint Entity
            'HARVEST_MANAGER',                      // Application
            'POST',                                 // Method
            [                                       // Worker data
                "occurrenceId" => $occurrenceId,
                "deletionMode" => $deletionMode,
                "parameters" => $parameters
            ],
            [                                       // Additional data for the background job
                "remarks" => $deletionMode
            ]
        );

        return $result;
    }

    /**
     * Trim field values given an array of records.
     * To trim is to remove any whitespaces before and after a string
     * or a set of strings.
     * @param Array $records - array of record objects
     * @return Array $records
     */
    public function trimValues($records) {
        for($i = 0; $i < count($records); $i++) {
            $record = $records[$i];

            foreach($record as $key => $value) {
                $records[$i][$key] = trim($value);
            }
        }

        return $records;
    }
}

?>