<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import Yii
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
// Import interfaces
use app\interfaces\models\IConfig;
use app\interfaces\controllers\IDashboard;
use app\interfaces\models\IExperiment;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IProgram;
use app\interfaces\models\IScaleValue;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\models\IVariable;
use app\interfaces\models\IWorker;
use app\interfaces\modules\harvestManager\models\IApiHelper;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;

/**
* General model class for the Harvest Manager module
*/
class HarvestManagerModel implements IHarvestManagerModel{
    /**
     * Model constructor
     */
    public function __construct(
        public IConfig $configurations,
        public IDashboard $dashboard,
        public IExperiment $experiment,
        public IOccurrence $occurrence,
        public IProgram $program,
        public IScaleValue $scaleValue,
        public IVariable $variable,
        public IWorker $worker,
        public IApiHelper $apiHelper,
        public IUserDashboardConfig $userDashboardConfig
        )
    { }

    /**
     * Verifies if the URL Parameters are complete and valid.
     * Returns an error and error message if conditions are not met.
     * @param string parameters URL parameters passed onto the controller action
     * @param string url the current page url
     * @return array array containing (1) error - boolean, if there is an error or none, and (2) errorMessage
     */
    public function validateUrlParameters($parameters, $url) {
        // Check if url came from a pjax request
        $fromPjax = str_contains($url, 'pjax');

        // If the request is NOT pjax, perform validation
        if(!$fromPjax) {
            // Check if at least one required URL Parameter is empty/unset
            $paramsEmpty = false;
            $missingParams = [];
            foreach($parameters as $pKey => $parameter) {
                $paramsEmpty = $paramsEmpty || empty($parameter);
                if(empty($parameter)) $missingParams[] = $pKey;
            }
            if($paramsEmpty) {
                return [
                    'error' => true,
                    'errorMessage' => \Yii::t('app', 'Required URL parameter' . (count($missingParams) == 1 ? '' : 's') . ' missing: ' . implode(', ', $missingParams))
                ];
            }

            // Check if source string is valid (plots or crosses)
            if(in_array('source', array_keys($parameters))) {
                $source = $parameters['source'];
                if($source != 'plots' && $source != 'crosses') {
                    return [
                        'error' => true,
                        'errorMessage' => \Yii::t('app', 'Invalid parameter: Source string must be "plots" or "crosses".')
                    ];
                }
            }

            // Check if occurrence id is an integer
            $occurrenceId = $parameters['occurrenceId'];
            if(!preg_match("/^\\d+$/i", $occurrenceId)) {
                return [
                    'error' => true,
                    'errorMessage' => \Yii::t('app', 'Invalid parameter: OccurrenceId must be an integer.')
                ];
            }

            // // REMOVED. For CORB-5719, this is removed to make way for the `renderAccess` function.
            // // This will be kept in the code in case it becomes needed again.
            // // Check if occurrence exists
            // $requestBody = [ "occurrenceDbId" => "$occurrenceId" ];
            // $result = $this->occurrence->searchAll($requestBody);
            // if(empty($result['data'])) {
            //     return [
            //         'error' => true,
            //         'errorMessage' => \Yii::t('app', 'Occurrence does not exist. Please select a valid occurrence.')
            //     ];
            // }
        }

        // No error
        return [
            'error' => false,
            'errorMessage' => \Yii::t('app', 'No errors found.')
        ];
    }

    /**
     * Retrieve program code, given the ID
     * @param integer programId program identifier
     * @return string programCode
     */
    public function getProgram($programId) {
        $requestBody = [ "programDbId" => "$programId" ];
        $result = $this->program->searchAll($requestBody);
        
        return isset($result['data'][0]['programCode']) ? $result['data'][0]['programCode'] : null;
    }

    /**
     * Validates the program in the URL parameters
     * @param string program program code string from the controller action
     * @param integer occurrenceId (optional) the occurrence id from the controller action
     * @return array array containing (1) redirect - boolean, if redirection will occur, and (2) the program to use
     */
    public function validateUrlProgram($program, $occurrenceId = null) {
        // Get program in dashboard
        $defaultFilters = (array) $this->dashboard->getFilters();
        $programId = $defaultFilters['program_id'];
        $dashboardProgram = $this->getProgram($programId);
        if(empty($program) && !empty($dashboardProgram)) {
            // If program is not set, redirect to this action with the program in the dashboard
            return [
                "redirect" => true,
                "program" => $dashboardProgram
            ];
        }
        else if(!empty($program) && !empty($occurrenceId)) {
            // Get occurence's program
            $requestBody = [ "occurrenceDbId" => "$occurrenceId" ];
            $result = $this->occurrence->searchAll($requestBody);
            $occurrenceProgram = $result['data'][0]['programCode'];
            // If URL program does not match the occurrence's program, 
            // redirect to this action with the occurrence's program
            if($program != $occurrenceProgram) {
                return [
                    "redirect" => true,
                    "program" => $occurrenceProgram
                ];
            }
        }

        // No redirection
        return [
            "redirect" => false,
            "program" => null
        ];
    }

    /**
     * Retrieves the variable id given the abbrev
     * @param string variableAbbrev the abbreviation of the variable
     * @return integer variable id
     */
    public function getVariableId($variableAbbrev) {
        // Search variable
        $requestBody = [
            "fields" => "variable.id AS variableDbId|variable.abbrev AS abbrev",
            "abbrev" => $variableAbbrev
        ];
        $result = $this->variable->searchAll($requestBody);
        $variable = isset($result['data'][0]) ? $result['data'][0] : [];
        return isset($variable['variableDbId']) ? $variable['variableDbId'] : 0;
    }

    /**
     * Retrieves the scale value of the given variable abbrev
     * @param string variableAbbrev the abbreviation of the variable
     * @param boolean scAbbrevMode whether or not to use scale value abbrev as key
     * @param array kvSettings default key and value to be used for the scale values
     * @return array scale value array where the key is the "value" and the value is the "displayName
     */
    public function getScaleValues($variableAbbrev, $scAbbrevMode = false, $kvSettings = ["key"=>"value","value"=>"displayName"]) {
        // Get variable id given the abbrev
        $variableId = $this->getVariableId($variableAbbrev);

        // Retrieve scale values
        $method = "POST";
        $endpoint = "variables/$variableId/scale-values-search";
        $result = $this->apiHelper->sendRequest($method, $endpoint);
        $scaleValues = isset($result[0]['scaleValues']) ? $result[0]['scaleValues'] : [];

        $scaleValArr = [];
        // Build scale value array
        if($scAbbrevMode) {
            // For harvest data browsers
            foreach($scaleValues as $scaleVal) {
                $scaleValArr[$scaleVal['abbrev']] = [$scaleVal[$kvSettings['key']] => $scaleVal[$kvSettings['value']]];
            }
        }
        else {
            // For general use
            foreach($scaleValues as $scaleVal) {
                $scaleValArr[$scaleVal[$kvSettings['key']]] = $scaleVal[$kvSettings['value']];
            }
        }
        

        return $scaleValArr;
    }

    /**
     * Check if the experiment is a Cross Parent Nursery.
     * If CPN, check status if 'planted' or 'planted;crossed'
     * @param integer experimentId experiment identifier
     * @return array array containing: (1) isCPN - boolean, whether or not experiment is CPN
     *                  (2) crossed - boolean, whether or not CPN has been crossed
     *                  (3) phase - string, CPN Phase (I or II)
     *                  (4) title - string, title to show on tab hover
     *                  (4) class - optional class name for the link
     */
    public function checkCPN($experimentId) {
        $info = [
            "isCPN" => false,
            "crossed" => false,
            "phase" => "",
            "title" => "",
            "class" => ""
        ];

        $result = $this->experiment->getOne($experimentId);
        $experiment = isset($result['data']) ? $result['data'] : [];
        // If no experiment found, return default info
        if(empty($experiment)) return $info;

        $experimentType = $experiment['experimentType'];
        $experimentStatus = $experiment['experimentStatus'];

        // If experiment is not CPN, return default info
        if($experimentType != "Cross Parent Nursery") {
            return $info;
        }
        // If experiment is CPN, check status
        else {
            // If CPN is crossed
            if(strpos($experimentStatus, 'planted') !== false && strpos($experimentStatus, 'crossed') !== false) {
                return [
                    "isCPN" => true,
                    "crossed" => true,
                    "phase" => "II",
                    "title" => "View crosses",
                    "class" => null
                ];
            }
            // If CPN is NOT crossed
            else {
                return [
                    "isCPN" => true,
                    "crossed" => false,
                    "phase" => "I",
                    "title" => "Experiment is not crossed yet",
                    "class" => "hm-unavailable"
                ];
            }
        }
    }

    /**
     * Determines the pagination key format for the browser grid
     * Formats: 'page', 'db-N-page'
     * @param object queryParams browser URL parameters
     * @return string paginationKey the pagination key for the browser grid
     */
    public function getPaginationKey($queryParams) {
        // Get array keys of queryParams
        $qpKeys = array_keys($queryParams);
        // Get dp-N-pages keys in queryParams
        $dpKey = array_values(preg_grep('/^dp-\\d+-page$/', array_keys($queryParams)));
        // If 'page' is found as a key in queryParams, set it as key
        if(in_array('page',$qpKeys)) {
            return 'page';
        }
        // If 'dp-N-page' is found as a key in queryParams, set it as key
        if(!empty($dpKey)) {
            return $dpKey[0];
        }

        return '';
    }

    /**
     * Determine if the pagination and filters will be reset
     * @param mixed uniqueIdentifier any unique identifier for the session (eg. occurenceId, userId, etc.)
     * @param object queryParams query parameters of the browser
     * @param string searchModelName name of search model
     */
    public function browserReset($uniqueIdentifier, $queryParams, $searchModelName) {
        // Get post parameters
        $postParams = \Yii::$app->request->post();

        // If bulkOperation flag is set to true, do not reset page and filters
        if(!empty($postParams['bulkOperation']) && boolval($postParams['bulkOperation'])) {
            return [
                'resetPage' => false,
                'resetFilters' => false
            ];
        }

        // Set pagination key
        $key = $this->getPaginationKey($queryParams);

        // Check post parameters
        if(!empty($postParams)) {
            // If forceReset = true (post parameter), reset pagination and filters
            if(!empty($postParams['forceReset']) && boolval($postParams['forceReset'])) {
                // Set current filter as empty array
                \Yii::$app->cbSession->set("$searchModelName-$uniqueIdentifier", []);
                // Reset query params
                $queryParams[$searchModelName] = [];
                $queryParams[$key] = "1";
                $queryParams['sort'] = null;
                \Yii::$app->request->setQueryParams($queryParams);

                return [
                    'resetPage' => true,
                    'resetFilters' => true
                ];
            }
        }

        // Check browser filters
        $previousParams = \Yii::$app->cbSession->get("$searchModelName-$uniqueIdentifier");
        $currentParams = isset($queryParams[$searchModelName]) ? $queryParams[$searchModelName] : [];
        // If change was detected, reset pagination
        if($previousParams != $currentParams) {
            // Store current filters
            \Yii::$app->cbSession->set("$searchModelName-$uniqueIdentifier", $currentParams);
            // Reset query params
            $queryParams[$searchModelName] = $currentParams;
            $queryParams[$key] = "1";
            $queryParams['sort'] = null;
            \Yii::$app->request->setQueryParams($queryParams);
            return [
                'resetPage' => true,
                'resetFilters' => false
            ];
        }
        else {
            // Store current filters
            \Yii::$app->cbSession->set("$searchModelName-$uniqueIdentifier", $currentParams);
        }

        return [
            'resetPage' => false,
            'resetFilters' => false
        ];
    }

    /**
     * Checks if the given string (haystack) contains
     * all the elements in the given array (needles)
     * @param String haystack the string to be checked
     * @param Array needles array of elements to look for in the haystack
     * @return Boolean whether or not the string contains all elements
     */
    public function stringContainsAll($haystack, array $needles) {
        foreach ($needles as $needle) {
            if (strpos($haystack, $needle) === false) {
                return false;
            }
        }
        return true;
    }

    /**
     * Assemble the URL parameters for 
     * pagination and sorting of the browser
     * @param string gridId grid identifier
     * @param string defaultSortString default sorting when no sort is set
     * @param boolean resetPage to reset pagination or not
     */
    public function assembleUrlParameters($gridId, $defaultSortString, $resetPage) {
        // Sorting
        $paramSort = '';
        $currentUrl = \Yii::$app->request->getUrl();
        $getParams = \Yii::$app->request->getQueryParams();

        // Validate if request is from creation tab
        $creationTabGridIds = [
            'dynagrid-harvest-manager-plot-crt-grid',
            'dynagrid-harvest-manager-cross-crt-grid'
        ];
        $crtPjax = $this->stringContainsAll($currentUrl, $creationTabGridIds);

        if(!$crtPjax && strpos($currentUrl,"$gridId-pjax")!==false) {
            if(isset($getParams['sort'])) {
                $paramSort = 'sort=';
                $sortParams = $getParams['sort'];
                $sortParams = explode('|', $sortParams);
                $countParam = count($sortParams);
                $currParam = 1;

                foreach($sortParams as $column) {
                    if($column[0] == '-') {
                        $paramSort = $paramSort . substr($column, 1) . ':DESC';
                    } else {
                        $paramSort = $paramSort . $column . ':ASC';
                    }
                    if($currParam < $countParam) {
                        $paramSort = $paramSort . '|';
                    }
                    $currParam += 1;
                }

            }
            else $paramSort = $defaultSortString;
        } else $paramSort = $defaultSortString;

        // Pagination
        $paramPage = '';
        if($resetPage) {
            $paramPage = 'page=1';
            if (isset($getParams[$gridId . '-page'])) {
                $getParams[$gridId . '-page'] = 1;
            }
            else if (isset($getParams['dp-1-page'])){
                $getParams['dp-1-page'] = 1;
            }
            else if (isset($getParams['page'])){
                $getParams['page'] = 1;
            }
            \Yii::$app->request->setQueryParams($getParams);
        }
        else if (isset($getParams[$gridId . '-page'])){
            $paramPage = 'page=' . $getParams[$gridId . '-page'];
        }
        else if (isset($getParams['dp-1-page'])){
            $paramPage = 'page=' . $getParams['dp-1-page'];
        } else if (isset($getParams['page'])) {
            $paramPage = 'page=' . $getParams['page'];
        }
        
        // Page limit
        $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

        $queryParams = [];
        if($paramLimit!='') $queryParams[] = $paramLimit;
        if($paramPage!='') $queryParams[] = $paramPage;
        if($paramSort!='') $queryParams[] = $paramSort;

        return $queryParams;
    }

    /**
     * Invokes the deletion worker
     * @param Integer $occurrenceId occurrence identifier
     * @param String $deletionMode mode of deletion (harvest data or package)
     * @param Object $parameters object containing parameters for the deletion
     */
    public function deleteInBackground($occurrenceId, $deletionMode, $parameters) {
        $desc = "Delete $deletionMode for occurrence ID: " . $occurrenceId;
        if($deletionMode == "revert") $desc = "Revert harvest for occurrence ID: " . $occurrenceId;
        // Invoke worker
        $result = $this->worker->invoke(
            'DeleteHarvestRecords',                 // Worker name
            $desc,                                  // Description
            'OCCURRENCE',                           // Entity
            $occurrenceId,                          // Entity ID
            'OCCURRENCE',                           // Endpoint Entity
            'HARVEST_MANAGER',                      // Application
            'POST',                                 // Method
            [                                       // Worker data
                "occurrenceId" => $occurrenceId,
                "deletionMode" => $deletionMode,
                "parameters" => $parameters
            ],
            [                                       // Additional data for the background job
                "remarks" => $deletionMode
            ]
        );

        return $result;
    }

    /**
     * Trim field values given an array of records.
     * To trim is to remove any whitespaces before and after a string
     * or a set of strings.
     * @param Array $records - array of record objects
     * @return Array $records
     */
    public function trimValues($records) {
        for($i = 0; $i < count($records); $i++) {
            $record = $records[$i];

            foreach($record as $key => $value) {
                $records[$i][$key] = trim($value);
            }
        }

        return $records;
    }

    /**
     * Find the best match config for the given record
     * @param Object $configs - array object containing configs
     * @param Object $record - array object containing record (plot/cross information)
     */
    public function findBestMatchConfig($configs, $record) {
        /**
         * $pointsArray stores points to be awarded
         * for each supported field from the hierarchy configuration.
         * The array key is the field name, 
         * and the value is the corresponding points to be awared
         * should the record be a match.
         */
        $pointsArray = [];
        /**
         * $bestMatch corresponds to the configuration
         * that matches the record best, indicating that
         * it is the appropriate configuration to be used for the record.
         */
        $bestMatch = null;
        /**
         * $highestScore keeps track of the highest score 
         * among the configuration items. This holds the score
         * of the config set as $bestMatch.
         */
        $highestScore = -9999999999;
        /**
         * $configsTraversed is a flag that indicates whether or not
         * the configs have been previously traversed.
         */
        $configsTraversed = !empty(\Yii::$app->session->get("configsTraversed")) ? 
            \Yii::$app->session->get("configsTraversed") == "true"  : 
            false;
        /**
         * Array collecting all known harvest methods from the configs.
         * Checks first if the variable exists in session.
         * If not, initializes to an empty array.
         */
        $harvestMethodValues = \Yii::$app->session->get("harvestMethodValues") ?? [];
        /**
         * Harvest method variable (HV_METH_DISC) variable ID
         */
        $harvestMethodVariableDbId = \Yii::$app->session->get("harvestMethodVariableDbId");
        if(empty($harvestMethodVariableDbId)) {
            $harvestMethodVariableDbId = $this->getVariableId('HV_METH_DISC');
            \Yii::$app->session->set("harvestMethodVariableDbId", $harvestMethodVariableDbId);
        }
        /**
         * Arrays collecting harvest data and numeric variable config items.
         * These are used to store the config items for harvest data and numeric variables.
         */
        $hDataConfigItems = \Yii::$app->session->get("hDataConfigItems") ?? [];
        $numVarConfigItems = \Yii::$app->session->get("numVarConfigItems") ?? [];                                  
        $methodNumVarCompat = \Yii::$app->session->get("methodNumVarCompat") ?? [];
        $variableFieldNames = \Yii::$app->session->get("variableFieldNames") ?? ["HV_METH_DISC" => "harvestMethod"];

        // Retrieve HARVEST DATA REQUIREMENTS Config
        $hierarchy = \Yii::$app->cbSession->get("harvestManagerConfigHierarchy");
        // If not yet in session, retrieve via API
        if(empty($hierarchy)) {
            // Get config
            $hierarchy = $this->configurations->getConfigByAbbrev('HM_CONFIGS_HIERARCHICAL_STRUCTURE');
            // Store in session
            \Yii::$app->cbSession->set("harvestManagerConfigHierarchy", $hierarchy);
        }

        /**
         * For each item in the hierarchy config,
         * calculate the points to be awarded per field.
         * Store point values in $pointsArray.
         * 
         * The points is calculated by raising 10 to the level value.
         * Eg.  if level = 0, then 10 ^ 0 = 1 point
         *      if level = 1, then 10 ^ 1 = 10 points
         *      if level = 2, then 10 ^ 2 = 100 points
         *      if level = N, then 10 ^ N points
         */
        foreach($hierarchy['entities'] as $hItem) {
            $level = $hItem['level'] ?? 0;
            $points = pow(10,$level);
            $fields = $hItem['fields'] ?? [];

            // Loop through fields for each item in the hierarchy config
            foreach($fields as $field) {
                $pointsArray["$field"] = $points;
            }
        }

        // Initialize highest points
        $highestPoints = 0;

        // Loop through each available config
        foreach($configs as $config) {
            $configScore = 0;
            $configMaxPossibleScore = 0;

            // Go through each item in the current config
            foreach($config as $key => $value) {
                if($key == "config") {
                    $currentConfig = $config[$key];

                    // Check configs
                    $harvestMethods = $currentConfig['harvestMethods'] ?? [];
                    $harvestData = $currentConfig['harvestData'] ?? [];
                    // If configs haven't been checked yet, check methods
                    if(!$configsTraversed) {
                        // Loop through methods
                        foreach($harvestMethods as $abbrev => $numVars) {
                            // Method Numvar compat
                            if(!isset($methodNumVarCompat[$abbrev])) {
                                $methodNumVarCompat[$abbrev] = [];
                            }
                            // If harvest method abbrev not yet in array, retrieve scale value and add to array
                            if(empty($harvestMethodValues[$abbrev])) {
                                // Retrieve scale value for current harvest method
                                $result = $this->variable->searchAllVariableScales(
                                    $harvestMethodVariableDbId,
                                    [
                                        "abbrev" => "HV_METH_DISC_" . $abbrev
                                    ]
                                );
                                // Unpack results
                                $data = $result['data'] ?? [];
                                $harvestMethodVariable = $data[0] ?? [];
                                $harvestMethodScaleVals = $harvestMethodVariable['scaleValues'] ?? [];
                                $scaleValueRecord = $harvestMethodScaleVals[0] ?? [];
                                $currentScaleValue = $scaleValueRecord['value'] ?? '';
                                // If scale value is not empty, add to array with abbrev as key
                                if(!empty($currentScaleValue)) {
                                    $harvestMethodValues[$abbrev] = $currentScaleValue;
                                }
                            }

                            // Loop through numvars
                            foreach($numVars as $numVar) { 
                                $variableAbbrev = $numVar['variableAbbrev'] ?? '';
                                $apiFieldName = $numVar['apiFieldName'] ?? '';

                                if(empty($numVarConfigItems[$variableAbbrev])) {
                                    $numVarConfigItems[$variableAbbrev] = $numVar;
                                }
                                if(empty($variableFieldNames[$apiFieldName])) {
                                    $variableFieldNames[$variableAbbrev] = $apiFieldName;
                                }

                                // Method Numvar compat
                                if(!in_array($variableAbbrev, $methodNumVarCompat[$abbrev])) {
                                    $methodNumVarCompat[$abbrev][] = $variableAbbrev;
                                }
                            }
                        }

                        // Loop through harvest data
                        foreach($harvestData as $hd) {
                            $variableAbbrev = $hd['variableAbbrev'] ?? '';
                            $apiFieldName = $hd['apiFieldName'] ?? '';

                            if(empty($hDataConfigItems[$variableAbbrev])) {
                                $hDataConfigItems[$variableAbbrev] = $hd;
                            }
                            if(empty($variableFieldNames[$apiFieldName])) {
                                $variableFieldNames[$variableAbbrev] = $apiFieldName;
                            }
                        }
                    }

                    continue;
                }

                // Get the record value. 
                // Transform to all lowercase, and trim whitespaces.
                $recordValue = array_key_exists($key, (array) $record) ? trim(strtolower($record["$key"])) : null;

                // Get points for the current field from the points array.
                $currentPoints = $pointsArray["$key"];
                $configMaxPossibleScore += $currentPoints;
                if ($currentPoints > $highestPoints) {
                    $highestPoints = $currentPoints;
                }

                // Case 1: If the config value is an array, check if the record value is in the array.
                //          If found, add points to the current config.
                if (is_array($value)) {
                    $hasMatch = false;
                    foreach ($value as $val) {
                        // Check if the array value is a string and matches the record value (case-insensitive).
                        if (is_string($val) && (strtolower($val) == $recordValue)) {
                            $configScore += $currentPoints;
                            $hasMatch = true;
                            break;
                        }
                        // Check if the array value contains the keyword "contains ".
                        // If so, check if the record value contains the specified substring.
                        else if (is_string($val) && str_contains($val, "contains ")) {
                            $contains = str_replace("contains ", "", strtolower($val));
                            if (str_contains($recordValue, $contains)) {
                                $configScore += $currentPoints;
                                $hasMatch = true;
                                break;
                            }
                        }
                    }
                    // If no match was found, deduct points from the current config.
                    if(!$hasMatch) {
                        $configScore -= $currentPoints;
                    }
                }
                // Case 2: If the config value is a string value, check if the record value is equal to the config value.
                //          If equal, add points to the current config.
                else if (is_string($value) && (strtolower($value) == $recordValue)) {
                    $configScore += $currentPoints;
                }
                // Case 2.5: Check if the string value contains the keyword "contains ".
                // If so, check if the record value contains the specified substring.
                else if (is_string($value) && str_contains($value, "contains ")) {
                    $contains = str_replace("contains ", "", strtolower($value));
                    if (str_contains($recordValue, $contains)) {
                        $configScore += $currentPoints;
                    }
                }
                // Case 3: If config value is '*', award current points.
                //          This signifies that the current config should be applied to a record
                //          regardless of its value for this field.
                else if (is_string($value) && $value == '*') {
                    $configScore += $currentPoints;
                }
                // Default case: If record value does not match the config value, deduct points from the current config.
                else {
                    $configScore -= $currentPoints;
                }
            }

            /**
             * If the config score is equal to the max possible score
             * and the config score is greater than the highest score
             * and the config score is greater than or equal to the highest points,
             * set the current config as the best match.
             */
            if ($configScore == $configMaxPossibleScore && $configScore >= $highestScore && $configScore >= $highestPoints) {
                $bestMatch = $config;
                $highestScore = $configScore;
            }
        }

        // Set configs traversed flag to true
        $configsTraversed = true;
        if(empty($hDataConfigItems["HV_METH_DISC"])) {
            $hDataConfigItems["HV_METH_DISC"] = [
                "variableAbbrev" => "HV_METH_DISC",
                "apiFieldName" => "harvestMethod",
                "displayName" => "Harvest Method",
                "inputType" => "select2",
                "inputOptions" => []
            ];
        }

        // Store known harvest methods in cb session
        \Yii::$app->session->set("harvestMethodValues", $harvestMethodValues);
        \Yii::$app->session->set("configsTraversed", $configsTraversed);
        \Yii::$app->session->set("numVarConfigItems", $numVarConfigItems);
        \Yii::$app->session->set("hDataConfigItems", $hDataConfigItems);
        \Yii::$app->session->set("methodNumVarCompat", $methodNumVarCompat);
        \Yii::$app->session->set("variableFieldNames", $variableFieldNames);

        return $bestMatch;
    }

    /**
     * Given the records (plots/crosses),
     * match the configs (stored in session) for easy access.
     * @param Array $records - array object containing plots or crosses
     * @param Integer $occurrenceId - occurrence identifier
     * @param String $dataLevel - data level (plot or cross)
     */
    public function matchConfigs($records, $occurrenceId, $dataLevel){
        $currentConfigs = [];

        // Loop through records
        $recordsCount = count($records);
        for ($i = 0; $i < $recordsCount; $i++) {
            // Get harvest method abbrev
            $harvestMethodAbbrev = $this->getScaleValueAbbrev("HV_METH_DISC", $records[$i]["harvestMethod"]);
            $records[$i]["harvestMethodAbbrev"] = $harvestMethodAbbrev;
            // Get cross method abbrev
            $crossMethodAbbrev = $this->getScaleValueAbbrev("CROSS_METHOD", $records[$i]["crossMethod"]);
            $records[$i]["crossMethodAbbrev"] = $crossMethodAbbrev;

            $harvestDataConfig = $this->findBestMatchConfig(
                \Yii::$app->cbSession->get("harvestDataRequirementsConfig"),
                $records[$i]
            );

            // Processing
            $config = $harvestDataConfig['config'] ?? [];
            // Get harvest method config
            $harvestMethods = $config['harvestMethods'] ?? [];
            // Initialize arrays
            $harvestDataConfig['config']['harvestMethodNumVarCompat'] = [];
            $harvestDataConfig['config']['harvestMethodNumVarCompatRequired'] = [];
            $harvestDataConfig['config']['numVarHarvestMethodCompat'] = [];

            // Extract harvest methods and numeric variables
            foreach($harvestMethods as $abbrev => $numVars) {
                // Initialize harvest method-num var compatibility object
                $harvestDataConfig['config']['harvestMethodNumVarCompat'][$abbrev] = [];
                $harvestDataConfig['config']['harvestMethodNumVarCompatRequired'][$abbrev] = [];

                // Compile all num vars in the current configs
                foreach($numVars as $numVar) {
                    $numVarAbbrev = $numVar['variableAbbrev'] ?? '';
                    $required = $numVar['required'] ?? false;

                    // Initialize num var-harvest method compatibility object
                    if(!isset($harvestDataConfig['config']['numVarHarvestMethodCompat'][$numVarAbbrev])) {
                        $harvestDataConfig['config']['numVarHarvestMethodCompat'][$numVarAbbrev] = [];
                    }
                    // Add harvest method abbrev to supported harvest methods of the current numvar
                    if (!in_array($abbrev,$harvestDataConfig['config']['numVarHarvestMethodCompat'][$numVarAbbrev])) {
                        $harvestDataConfig['config']['numVarHarvestMethodCompat'][$numVarAbbrev][] = $abbrev;
                    }
                    // Add num var abbrev to supported num vars of the current harvest method
                    if (!in_array($numVarAbbrev,$harvestDataConfig['config']['harvestMethodNumVarCompat'][$abbrev])) {
                        $harvestDataConfig['config']['harvestMethodNumVarCompat'][$abbrev][] = $numVarAbbrev;
                    }
                    // Add num var abbrev to supported num vars of the current harvest method
                    if ($required && !in_array($numVarAbbrev,$harvestDataConfig['config']['harvestMethodNumVarCompatRequired'][$abbrev])) {
                        $harvestDataConfig['config']['harvestMethodNumVarCompatRequired'][$abbrev][] = $numVarAbbrev;
                    }
                }
            }

            // Add harest data config to the plot/cross record
            $records[$i]['harvestDataConfig'] = $harvestDataConfig;

            // Add to current configs array
            if(!in_array($harvestDataConfig, $currentConfigs)) {
                $currentConfigs[] = $harvestDataConfig;
            }
        }

        // Store current configs in cb session
        \Yii::$app->cbSession->set("hmCurrentConfigs-$dataLevel-$occurrenceId", $currentConfigs);

        return $records;
    }

    /**
     * Build the input columns for the data browsers
     * @param String $dataLevel - data level (plot or cross)
     * @param Integer $occurrenceId - occurrence identifier
     */
    public function buildInputColumns($dataLevel, $occurrenceId) {
        // Get current configs from session
        $currentConfigs = \Yii::$app->cbSession->get("hmCurrentConfigs-$dataLevel-$occurrenceId");
        // Variable declarations
        $variableAbbrevs = [];
        $defaultColumns = [];
        $inputColumns = [];
        $supported = [
            "harvestMethods" => [],
            "numVars" => []
        ];

        foreach($currentConfigs as $currentConfig) {
            // Get config value
            $config = $currentConfig['config'] ?? [];
            // Get harvest method config
            $harvestMethodsConfig = $config['harvestMethods'] ?? [];
            // Get harvest data config
            $harvestDataConfig = $config['harvestData'] ?? [];

            // Extract harvest methods and numeric variables
            foreach($harvestMethodsConfig as $abbrev => $numVars) {
                // Compile all harvest methods in the current configs
                if(!in_array($abbrev, $supported['harvestMethods'])) {
                    $supported['harvestMethods'][] = $abbrev;
                }

                // Compile all num vars in the current configs
                foreach($numVars as $numVar) {
                    $numVarAbbrev = $numVar['variableAbbrev'] ?? '';
                    
                    if(!in_array($numVarAbbrev, $supported['numVars'])) {
                        $supported['numVars'][] = $numVarAbbrev;
                    }
                }
            }

            // Loop through other harvest data
            foreach($harvestDataConfig as $hdata) {
                // Check variable abbrev
                $varAbb = $hdata['variableAbbrev'] ?? null;
                // If abbrev is null, or has been encountered already,
                // skip it.
                if(empty($varAbb) || in_array($varAbb, $variableAbbrevs)) continue;

                // Add abbrev to array for tracking
                $variableAbbrevs[] = $varAbb;
                // Get config info
                $apiFieldName = $hdata['apiFieldName'] ?? null;

                // Get variable display name
                $variable = $this->variable->getVariableByAbbrev($varAbb);
                $displayName = $variable['displayName'] ?? '';
                $hdata["displayName"] = $displayName;

                // Build new column
                $newColumn = [
                    'attribute' => $apiFieldName,
                    'label' => $displayName,
                    'mergeHeader' => true,
                    'format' => 'raw',
                    'visible' => true,
                    'order'=> \kartik\dynagrid\DynaGrid::ORDER_FIX_RIGHT,
                    'encodeLabel' => false,
                    'hAlign' => 'left',
                    'vAlign' => 'top',
                    'value' => function ($model) use ($dataLevel, $varAbb, $displayName, $hdata) {
                        $currentConfig = [];
                        $harvestDataConfig = $model['harvestDataConfig'] ?? [];
                        $config = $harvestDataConfig['config'] ?? [];
                        $harvestData = $config['harvestData'] ?? [];

                        // Get current config
                        foreach($harvestData as $hd) {
                            if($hd['variableAbbrev'] == $varAbb) {
                                $currentConfig = $hd;
                                $currentConfig['displayName'] = $displayName;
                                break;
                            }
                        }

                        // Build input element
                        return $this->buildInputElements($model, $dataLevel, $currentConfig, $hdata);
                    }
                ];

                // Add to input columns
                $inputColumns[] = $newColumn;
            }
        }

        // Build harvest method and numvars (if any)
        if(!empty($supported['harvestMethods'])) {
            // Build harvest method config
            $harvestMethodConfig = [
                "variableAbbrev" => "HV_METH_DISC",
                "apiFieldName" => "harvestMethod",
                "displayName" => "Harvest Method",
                "inputType" => "select2",
                "inputOptions" => []
            ];
            // Get harvest method scale values
            $harvestMethodScaleVals = $this->getScaleValues($harvestMethodConfig['variableAbbrev'], false, [
                "key" => "abbrev",
                "value" => "value"
            ]);

            // Build new column
            $newColumn = [
                'attribute' => $harvestMethodConfig['apiFieldName'],
                'label' => $harvestMethodConfig['displayName'],
                'mergeHeader' => true,
                'format' => 'raw',
                'visible' => true,
                'order'=> \kartik\dynagrid\DynaGrid::ORDER_FIX_RIGHT,
                'encodeLabel' => false,
                'hAlign' => 'left',
                'vAlign' => 'top',
                'value' => function ($model) use ($dataLevel, $harvestMethodConfig, $harvestMethodScaleVals, $supported) {
                    $dataOptions = [];
                    // Unpack configs
                    $harvestDataConfig = $model['harvestDataConfig'] ?? [];
                    $config = $harvestDataConfig['config'] ?? [];
                    $harvestMethods = $config['harvestMethods'] ?? [];
                    $harvestMethods = $config['harvestMethods'] ?? [];
                    
                    // Identify supported methods for the current record
                    foreach($harvestMethods as $abbrev => $value) {
                        $currentMethodAbbrev = $harvestMethodConfig['variableAbbrev'] . "_" . $abbrev;
                        $currentMethodValue = $harvestMethodScaleVals[$currentMethodAbbrev] ?? '';
                        if(empty($currentMethodValue)) continue;
                        // Update config's data options for rendering
                        $dataOptions[$currentMethodValue] = $currentMethodValue;
                    }

                    // Get harvest method (value)-numvars compatibility
                    // Maps the harvest method value, instead of the abbrev
                    // To the applicable numeric variable abbrevs.
                    $compatArray = [];
                    foreach($harvestMethods as $key => $value) {
                        $harvestMethodValue = $harvestMethodScaleVals['HV_METH_DISC_'.$key] ?? '';
                        if(empty($harvestMethodValue)) continue;

                        $hmNumVars = [];
                        foreach($value as $nv) {
                            $abb = $nv['variableAbbrev'] ?? '';

                            if(!in_array($abb, $hmNumVars)) $hmNumVars[] = $abb;
                        }

                        $compatArray[$harvestMethodValue] = $hmNumVars;
                    }

                    // Update current config
                    $harvestMethodConfig['inputOptions']['dataOptions'] = $dataOptions;
                    $harvestMethodConfig['inputOptions']['methodNumVarCompat'] = $compatArray;
                    $harvestMethodConfig['inputOptions']['numVars'] = $supported['numVars'];
                    $currentConfig = !empty($harvestMethods) ? $harvestMethodConfig : [];
                    if(!empty($currentConfig)) {
                        $model['harvestDataConfig']['config']['harvestData'][] = $currentConfig;
                    }

                    // Build input element
                    return $this->buildInputElements($model, $dataLevel, $currentConfig, $harvestMethodConfig);
                }
            ];

            // Add to input columns
            $defaultColumns[] = $newColumn;

            // Build harvest method and numvars (if any)
            if(!empty($supported['numVars'])) {
                // Map numvar abbrevs to the appropriate display names
                // Display names are retrieved from the `master.variable` table.
                $numVarDisplayNames = [];
                foreach($supported['numVars'] as $numVarAbbrev) {
                    $variableRecord = $this->variable->getVariableByAbbrev($numVarAbbrev);
                    $displayName = $variableRecord['displayName'] ?? '';
                    if (empty($numVarDisplayNames[$numVarAbbrev])) {
                        $numVarDisplayNames[$numVarAbbrev] = $displayName;
                    }
                }

                // Build new column
                $newColumn = [
                    'attribute' => 'numericVariable',
                    'label' => 'Numeric Variable',
                    'mergeHeader' => true,
                    'format' => 'raw',
                    'visible' => true,
                    'order'=> \kartik\dynagrid\DynaGrid::ORDER_FIX_RIGHT,
                    'encodeLabel' => false,
                    'hAlign' => 'left',
                    'vAlign' => 'top',
                    'value' => function ($model) use ($dataLevel, $numVarDisplayNames, $supported, $harvestMethodScaleVals) {
                        $recordDbId = $model[$dataLevel . 'DbId'] ?? 0;            
                        // Unpack configs
                        $harvestDataConfig = $model['harvestDataConfig'] ?? [];
                        $config = $harvestDataConfig['config'] ?? [];
                        $harvestMethodNumVarCompat = $config['harvestMethodNumVarCompat'] ?? [];
                        $harvestMethods = $config['harvestMethods'] ?? [];

                        // Get available harvest method values
                        $committedMethod = isset($model['harvestMethod']) ? $model['harvestMethod'] : '';
                        $uncommittedMethod = isset($model['terminalHarvestMethod']) ? $model['terminalHarvestMethod'] : '';
                        
                        
                        // Map harvest method valyes to the num var abbrevs.
                        // Switched to harvest method values from using harvest method abbrevs.
                        $methodNumVarCompat = [];
                        foreach($harvestMethodNumVarCompat as $method => $vars ) {
                            $hmeth = $harvestMethodScaleVals['HV_METH_DISC_'.$method] ?? '';
                            if(empty($hmeth)) continue;
                            $methodNumVarCompat[$hmeth] = $vars;
                        }

                        // Initialize variables for num var HTML element building
                        $numVarElements = '';
                        $currentHtmlElementArray = [];
                        $currentValCount = 0;
                        $inputElementArray = [];
                        $numVarsTraversed = [];

                        // Go through methods
                        foreach($harvestMethods as $abbrev => $numVars) {
                            // Combine numvars array to harvest data array for validation
                            $model['harvestDataConfig']['config']['harvestData'] = array_merge(
                                $model['harvestDataConfig']['config']['harvestData'],
                                $numVars
                            );

                            // Go through numvers for the cyrrent method
                            foreach($numVars as $numVar) {
                                $numVarAbbrev = $numVar['variableAbbrev'] ?? '';

                                // If the numvar has been encountered already, skip
                                if(in_array($numVarAbbrev,$numVarsTraversed)) continue;
                                $numVarsTraversed[] = $numVarAbbrev;

                                // Prepare essential variables/values
                                $numVarDisplayName = $numVarDisplayNames[$numVarAbbrev] ?? '';
                                $applicableHarvestMethods = $config['numVarHarvestMethodCompat'][$numVarAbbrev] ?? [];
                                $applicableHarvestMethods = array_map(function ($item) use ($harvestMethodScaleVals) {
                                    return $harvestMethodScaleVals['HV_METH_DISC_' . $item] ?? '';
                                }, $applicableHarvestMethods);
                                $numVar['displayName'] = $numVarDisplayName;
                                $numVar['inputOptions']['applicableHarvestMethods'] = $applicableHarvestMethods;

                                // Generate input element
                                $numVarHtml = $this->buildInputElements($model, $dataLevel, $numVar, $numVar, true);

                                // Update arrays
                                $currentHtmlElementArray[] = $numVarHtml['currentHtmlElement'] ?? '';
                                if ($numVarHtml['currentVal'] != '') $currentValCount++;
                                $inputElementArray[] = $numVarHtml['inputElement'] ?? '';
                            }
                        }

                        // Determine wheter or not to hide the placeholder input.
                        $hidden = '';
                        if(!empty($uncommittedMethod) && empty($methodNumVarCompat[$uncommittedMethod])) $hidden = '';
                        else if(empty($uncommittedMethod) && !empty($committedMethod) && empty($methodNumVarCompat[$committedMethod])) $hidden = '';
                        else if(empty($uncommittedMethod) && empty($committedMethod)) {
                            $hidden = '';
                        }
                        else $hidden = ' hidden';

                        // Declare placeholder element
                        $inputElementArray[] = Html::input('number', "NUMVAR_PLACEHOLDER-$dataLevel-$recordDbId", '', [
                            'id' => "NUMVAR_PLACEHOLDER-$dataLevel-$recordDbId",
                            'class' => 'hm-hdata-input hm-hdata-num-input hm-hdata-number-input-' . $recordDbId . $hidden,
                            'title' => 'Numeric variable is not applicable.',
                            'disabled' => true,
                            'placeholder' => 'Not applicable',
                            'oninput' => "val = value||validity.valid||(value=val);",
                            'onkeypress' => null,
                            'min' => 1
                        ]);

                        // Build final HTML element
                        if($currentValCount > 0) {
                            return '<div class="NUMVAR-committed">' . implode("",$currentHtmlElementArray) . '</div><div class="NUMVAR-input">' . implode("",$inputElementArray) . '<div>';
                        }
                        else {
                            return '<div class="NUMVAR-committed" ></div><div class="NUMVAR-input">' . implode("",$inputElementArray) . '<div>';
                        }

                        return $numVarElements;
                    }
                ];

                // Add to input columns
                $defaultColumns[] = $newColumn;
            }
        }

        // Combine default and other input columns
        $inputColumns = array_merge($defaultColumns, $inputColumns);

        return $inputColumns;
    }

    /**
     * Determines whether or not input will be disabled
     * based on the harvest status and/or state of the model
     * @param object model data model
     * @param string dataLevel data lavel (plot or cross)
     * @param string apiFieldName api field name of the input
     * @param string displayName display name of the input
     * @param object harvestDataConfig supported harvest data
     */
    public function disableInput($model, $dataLevel, $apiFieldName, $displayName, $harvestDataConfig) {
        $harvestStatus = $model['harvestStatus'];
        $state = $model['state'];
        
        if($harvestStatus=='COMPLETED') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', "This $dataLevel has been harvested. Kindly delete the committed data first before you input new data on this $dataLevel.")
            ];
        }
        if($harvestStatus=='FAILED') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', "This harvest failed. Kindly delete the committed data first before you input new data on this $dataLevel.")
            ];
        }
        if($harvestStatus=='QUEUED_FOR_HARVEST' || $harvestStatus=='HARVEST_IN_PROGRESS' || $harvestStatus=='DONE') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', 'You cannot input harvest traits right now. Seed and package creation is in progress.')
            ];
        }
        if($harvestStatus=='DELETION_IN_PROGRESS') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', 'You cannot input harvest traits right now. Harvest trait and/or seed and package deletion is in progress.')
            ];
        }
        if($state == 'unknown') {
            return [
                'disable' => true,
                'message' => \Yii::t('app', 'Input not allowed due to unknown state. Please request for data curation.')
            ];
        }

        $disable = true;
        $disableMessage = \Yii::t('app', $displayName . " input not supported for this $dataLevel.");

        foreach($harvestDataConfig as $hdata) {
            if($apiFieldName == $hdata['apiFieldName']) {
                $disable = false;
                $disableMessage = '';
                break;
            }
        }

        return [
            'disable' => $disable,
            'message' => $disableMessage
        ];
    }

    /**
     * Builds input elements for the supported harvest data
     * @param object model data model
     * @param string dataLevel data level (plot or cross)
     * @param object currentConfig harvest data config for the current plot/cross record
     * @param object templateConfig sample config for the column/input
     * @param boolean raw whether or not to enclose elements in divs
     */
    public function buildInputElements($model, $dataLevel, $currentConfig, $templateConfig, $raw = false) {
        // Get configs
        $harvestDataConfig = $model['harvestDataConfig'] ?? [];
        $config = $harvestDataConfig['config'] ?? [];
        $harvestData = $config['harvestData'] ?? [];
        // Declare variables
        $inputElement = null;
        $variableAbbrev = $templateConfig['variableAbbrev'] ?? null;
        $apiFieldName = $templateConfig['apiFieldName'] ?? null;
        $displayName = $templateConfig['displayName'] ?? null;
        $inputType = $templateConfig['inputType'] ?? null;
        $required = $templateConfig['required'] ?? null;
        $recordDbId = $model[$dataLevel."DbId"] ?? 0;
        $class = 'hm-hdata-input hm-hdata-'  . $inputType  . '-input'
            . ' hm-hdata-'  . $variableAbbrev . '-input'
            . ' hm-hdata-' . $inputType . '-input-' . $recordDbId;
        $id = "$variableAbbrev-$dataLevel-$recordDbId";
        $customStyle = ' ';
        $currentVal = isset($model[$apiFieldName]) ? $model[$apiFieldName] : '';
        $terminalVal = isset($model['terminal'.ucfirst($apiFieldName)]) ? $model['terminal'.ucfirst($apiFieldName)] : '';

        $currentHtmlElement = '<span title="Committed ' . $displayName . '" class="badge center light-green darken-4 white-text">' . $currentVal . '</span>';

        // Determine if input is to be disabled
        $disable = $this->disableInput($model, $dataLevel, $apiFieldName, $displayName, $harvestData);

        // Check input type
        if($inputType == "datepicker") {
            $inputElement = DatePicker::widget([
                'name' => 'check_date',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'value' => $terminalVal,
                'options' => [
                    'class' => $class,
                    'id' => $id,
                    'title' => $disable['message'],
                    'disabled' => $disable['disable'],
                    'autocomplete' => 'off',
                    'oldValue' => $terminalVal,
                ],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true,
                    'clearBtn' => true,
                    'zIndexOffset' => '9000',
                    'startDate'=> date("1970-01-01")
                ],
                'pluginEvents' => [
                    'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                ]
    
            ]);
        }
        else if($inputType == "select2") {
            $data = [];
            $inputOptions = $currentConfig['inputOptions'] ?? [];
            $useScaleValues = $inputOptions['useScaleValues'] ?? false;
            $dataOptions = $inputOptions['dataOptions'] ?? [];

            // If useScaleValues is true, retrieve the scale values of the variable
            // and use that as data for the select2 element.
            if($useScaleValues) {
                $data = $this->getScaleValues($variableAbbrev, false, [
                    "key" => "value",
                    "value" => "value"
                ]);
            }
            // If dataOptions is set, use that as select2 data.
            else if(!empty($dataOptions)) {
                $data = $dataOptions;
            }

            $inputElement = Select2::widget([
                'name' => 'input_type',
                'data' => $data,
                'value' => !empty($terminalVal) ? $terminalVal : '',
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => $displayName,
                    'class' => $class,
                    'id' => $id,
                    'title' => $disable['message'],
                    'disabled' => $disable['disable'],
                    'current-config' => $currentConfig,
                    'multiple' => false,
                    'allowClear' => true,
                    'oldValue' => $terminalVal,
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]);
        }
        else if($inputType == "number") {
            $inputOptions = $currentConfig['inputOptions'] ?? [];
            $applicableHarvestMethods = $inputOptions['applicableHarvestMethods'] ?? [];
            $minimumValue = $inputOptions['minimumValue'] ?? 1;
            $inputSubType = $inputOptions['inputSubType'] ?? 1;
            $onKeyPress = $inputSubType == 'comma_sep_int' ? 'return event.charCode == 44 || (event.charCode >= 48 && event.charCode <= 57)' : null;
            $type = $inputSubType == 'comma_sep_int' ? 'text' : 'number';
            // Get available harvest methods
            $committedMethod = isset($model['harvestMethod']) ? $model['harvestMethod'] : '';
            $uncommittedMethod = isset($model['terminalHarvestMethod']) ? $model['terminalHarvestMethod'] : '';
            // Determine wheter or not to hide the numeric var input
            $hidden = '';
            if(!empty($uncommittedMethod) && in_array($uncommittedMethod, $applicableHarvestMethods)) $hidden = '';
            else if(empty($uncommittedMethod) && !empty($committedMethod) && in_array($committedMethod, $applicableHarvestMethods)) $hidden = '';
            else $hidden = ' hidden';

            // Build the HTML element for the numeric variable input
            $inputElement = Html::input($type, $id, $terminalVal, [
                'id' => $id,
                'class' => $class . $hidden,
                'title' => empty($disable['message']) ? $disable['message'] : $disable['message'],
                'disabled' => $disable['disable'],
                'placeholder' => $displayName,
                'oninput' => "val = value||validity.valid||(value=val);",
                'onkeypress' => $onKeyPress,
                'min' => $minimumValue,
                'subType' => $inputSubType,
                'oldValue' => $terminalVal,
            ]);

            // Committed numeric variable display logic
            if($currentVal != null && $currentVal != "") {
                $title = $displayName;
                $hoverTitle = $title;
                // If the committed method supports then numva
                if(!empty($committedMethod) && in_array($committedMethod, $applicableHarvestMethods)) {
                    $colorClass= 'light-green darken-4';
                }
                // If the numeric variable is irrelevant to the committed method
                else {
                    $colorClass= 'light-grey darken-4';
                    if(!empty($committedMethod)) $hoverTitle .= ' (Irrelevant to the committed method: ' . $committedMethod . ")";
                }

                $currentHtmlElement = '<span title="Committed ' . $hoverTitle . '" class="badge center ' . $colorClass . ' white-text">' . $title . ': ' . $currentVal . '</span>';
            }
        }

        // If raw is set to true, return individual components.
        if($raw) {
            return [
                "currentHtmlElement" => $currentHtmlElement,
                "currentVal" => $currentVal,
                "inputElement" => $inputElement,
                "customStyle" => $currentHtmlElement
            ];
        }

        // Build final HTML element
        if($currentVal != '') {
            return '<div class="' . $variableAbbrev . '-committed"' . $customStyle . '>' . $currentHtmlElement . '</div><div class="' . $variableAbbrev . '-input">' . $inputElement . '<div>';
        }
        else {
            return '<div class="' . $variableAbbrev . '-committed" ></div><div class="' . $variableAbbrev . '-input">' . $inputElement . '<div>';
        }
    }

    /**
     * Retrieves the scale value abbreviation for a given variable and value.
     * 
     * @param string $variableAbbrev - The abbreviation of the variable to search for.
     * @param string $value - The value to search for.
     * 
     * @return string - The scale value abbreviation.
     */
    function getScaleValueAbbrev($variableAbbrev, $value) {
        // Get from session
        $currentAbbrevs = \Yii::$app->cbSession->get("scaleValueAbbrevs_$variableAbbrev") ?? [];
        // Check if the value has been encountered before.
        // If so, return the stored abbreviation.
        if (array_key_exists($value, $currentAbbrevs)) {
            return $currentAbbrevs[$value];
        }

        // Otherwise, search for the scale value abbreviation.
        $scaleValueSearch = $this->apiHelper->sendRequest(
            'POST',
            'scale-values-search',
            [
                "variableAbbrev" => "equals $variableAbbrev",
                "scaleValueAbbrev" => $variableAbbrev . "_%",
                "scaleValue" => "equals $value"
            ],
            [
                "limit=1"
            ]
        );
        // Extract the scale value abbreviation
        $scaleValueRecord = $scaleValueSearch[0] ?? [];
        $scaleValueAbbrev = $scaleValueRecord['scaleValueAbbrev'] ?? '';

        // Store value in session for future reference
        $currentAbbrevs[$value] = $scaleValueAbbrev;

        // Store in session
        return str_replace("{$variableAbbrev}_", '', $scaleValueAbbrev);
    }
}

?>