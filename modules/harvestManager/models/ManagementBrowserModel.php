<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import yii
use yii\data\Sort;
// Import data providers
use app\dataproviders\ArrayDataProvider;
// Import interfaces
use app\interfaces\modules\harvestManager\models\IApiHelper;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\models\ILists;
use app\interfaces\models\IWorker;
// Import models
use app\models\Package;

/**
* Model class for Management browser
*/
class ManagementBrowserModel extends Package
{
    public $designation;
    public $generation;
    public $germplasmState;
    public $seedCode;
    public $harvestDate;
    public $harvestMethod;
    public $harvestSource;
    public $label;
    public $packageCode;
    public $packageDbId;
    public $germplasmDbId;
    public $germplasmCode;
    public $seedDbId;
    public $parentage;
    public $quantity;
    public $replication;
    public $seedName;
    public $seedSourcePlotCode;
    public $seedSourcePlotNumber;
    public $sourceEntryCode;
    public $crossFemaleParent;
    public $femaleParentage;
    public $crossMaleParent;
    public $maleParentage;

    /**
     * Model constructor
     */
    public function __construct(
        public IApiHelper $apiHelper, 
        public IHarvestManagerModel $harvestManagerModel,
        public ILists $lists,
        public IWorker $worker,
        )
    { }

    public function rules()
    {
        return [
            [['designation', 'label', 'packageDbId', 'seedName'], 'required'],
            [['packageDbId', 'seedSourcePlotNumber', 'replication'], 'integer'],
            [['designation', 'generation', 'germplasmState', 'harvestDate', 'harvestMethod', 'harvestSource', 'label', 'packageCode', 'parentage', 'seedName','seedSourcePlotCode', 'sourceEntryCode', 'crossFemaleParent', 'femaleParentage', 'crossMaleParent', 'maleParentage'], 'string'],
            [['designation', 'generation', 'germplasmState', 'seedCode', 'germplasmCode', 'harvestDate', 'harvestMethod', 'harvestSource', 'label', 'packageCode', 'packageDbId', 'parentage', 'quantity', 'replication', 'seedName', 'seedSourcePlotCode', 'seedSourcePlotNumber', 'sourceEntryCode', 'crossFemaleParent', 'femaleParentage', 'crossMaleParent', 'maleParentage'], 'safe'],
            [[], 'boolean'],
            [['packageDbId'], 'unique']
        ];
    }

    /**
     * Retrieve package records to display
     * @param object params browsel filter object
     * @param integer experimentId experiment identifier
     * @param integer occurrenceId occurrence identifier
     * @param integer locationId location identifier
     * @return ArrayDataProvider data provider
     */
    public function search($params, $experimentId, $occurrenceId, $locationId) {
        // Check if browser pagination and fitlers will be reset
        $reset = $this->harvestManagerModel->browserReset($occurrenceId, $params, 'ManagementBrowserModel');
        $resetPage = $reset['resetPage'];
        $resetFilters = $reset['resetFilters'];

        // If resetFilters = true, set params to null
        if($resetFilters) {
            $params = null;
        }
        // Load params into the browser search model
        $this->load($params);

        // Assemble URL parameters
        $urlParams = $this->harvestManagerModel->assembleUrlParameters(
            'dynagrid-harvest-manager-package-mgmt-grid',
            'sort=seedSourcePlotNumber:ASC|seedCode:ASC',
            $resetPage
        );

        // Assemble browser filters
        $columnFilters = $this->assembleBrowserFilters($params, $occurrenceId);

        $result = $this->getPackageRecords($columnFilters, $urlParams, $occurrenceId);
        $packages = $result['data'];
        $totalCount = $result['count'];

        // If the package count and total count are both 0, reset the pagination.
        // This case catches when the last page is viewed, and a bulk delete operation took place,
        // resulting in no packages being left to display for the current page number.
        if(count($packages) == 0 && $totalCount == 0) {
            // Assemble URL parameters
            $urlParams = $this->harvestManagerModel->assembleUrlParameters(
                'dynagrid-harvest-manager-package-mgmt-grid',
                'sort=seedSourcePlotNumber:ASC|seedCode:ASC',
                true
            );

            $result = $this->getPackageRecords($columnFilters, $urlParams, $occurrenceId);
            $packages = $result['data'];
            $totalCount = $result['count'];
        }

        // Add sort to params
        if(empty($params['sort'])) $params['sort'] = 'seedSourcePlotNumber|seedCode';

        \Yii::$app->cbSession->set("packagesTotalCount-$occurrenceId", $totalCount);
        \Yii::$app->cbSession->set("packagesUrlParams-$occurrenceId", $urlParams);
        \Yii::$app->cbSession->set("packagesQueryParams-$occurrenceId", $params);

        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'allModels' => $packages,
            'key' => 'packageDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);
        // Initialize sort
        $sort = new Sort();
        $sort->attributes = [
            'designation'=>[
                "asc" => ["designation"=>SORT_ASC],
                'desc' => ["designation"=>SORT_DESC]
            ],
            'seedName'=>[
                "asc" => ["seedName"=>SORT_ASC],
                'desc' => ["seedName"=>SORT_DESC]
            ],
            'seedCode'=>[
                "asc" => ["seedCode"=>SORT_ASC],
                'desc' => ["seedCode"=>SORT_DESC]
            ],
            'femalePlotNumber'=>[
                "asc" => ["femalePlotNumber"=>SORT_ASC],
                'desc' => ["femalePlotNumber"=>SORT_DESC]
            ],
            'femalePlotCode'=>[
                "asc" => ["femalePlotCode"=>SORT_ASC],
                'desc' => ["femalePlotCode"=>SORT_DESC]
            ],
            'sourceEntryCode'=>[
                "asc" => ["sourceEntryCode"=>SORT_ASC],
                'desc' => ["sourceEntryCode"=>SORT_DESC]
            ],
            'seedSourcePlotCode'=>[
                "asc" => ["seedSourcePlotCode"=>SORT_ASC],
                'desc' => ["seedSourcePlotCode"=>SORT_DESC]
            ],
            'seedSourcePlotNumber'=>[
                "asc" => ["seedSourcePlotNumber"=>SORT_ASC],
                'desc' => ["seedSourcePlotNumber"=>SORT_DESC]
            ],
            'replication'=>[
                "asc" => ["replication"=>SORT_ASC],
                'desc' => ["replication"=>SORT_DESC]
            ],
            'harvestDate'=>[
                "asc" => ["harvestDate"=>SORT_ASC],
                'desc' => ["harvestDate"=>SORT_DESC]
            ],
            'harvestMethod'=>[
                "asc" => ["harvestMethod"=>SORT_ASC],
                'desc' => ["harvestMethod"=>SORT_DESC]
            ],
            'harvestSource'=>[
                "asc" => ["harvestMethod"=>SORT_ASC],
                'desc' => ["harvestMethod"=>SORT_DESC]
            ],
            'label'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
            'packageCode'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
            'quantity'=>[
                "asc" => ["quantity"=>SORT_ASC],
                'desc' => ["quantity"=>SORT_DESC]
            ],
            'generation'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
            'parentage'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
            'germplasmState'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
            'germplasmCode'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
            'crossFemaleParent'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
            'femaleParentage'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
            'crossMaleParent'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
            'maleParentage'=>[
                "asc" => ["label"=>SORT_ASC],
                'desc' => ["label"=>SORT_DESC]
            ],
        ];
        // // ⚠ ⚠ ⚠ DO NOT REMOVE YET ⚠ ⚠ ⚠
        // if($packageUpdated && isset($params['sort'])) {
        //     $columnName = $params['sort'];
        //     $sortOrder = SORT_ASC;
        //     if(substr($params['sort'], 0, 1) === '-') {
        //         $columnName = substr($params['sort'], 1, strlen($params['sort']));
        //         $sortOrder = SORT_DESC;
        //     }
        //     $sort->defaultOrder = [
        //         $columnName => $sortOrder
        //     ];
        // }
        // else {
        //     $sort->defaultOrder = [
        //         'seedSourcePlotNumber' => SORT_ASC
        //     ];
        // }
        
        // Set default order
        $sort->defaultOrder = [
            'seedSourcePlotNumber' => SORT_ASC
        ];
        // Apply sort to data provider
        $dataProvider->setSort($sort);

        return $dataProvider;
    }

    /**
     * Retrieves packages of the current occurrence
     * @param object requestBody column filters
     * @param array urlParams url params for sorting, pagination, and page size
     * @param integer occurrenceId occurrence identifier
     * @param boolean retrieveAll defaults to FALSE
     * @return array array containing packages
     */
    public function getPackageRecords($requestBody, $urlParams, $occurrenceId,$retrieveAll=false) {
        // Set fields
        $requestBody['fields'] = $this->getDefaultFields();
        // Set method and endpoint
        $method = 'POST';
        $endpoint = "occurrences/$occurrenceId/packages-search";

        // Store body and filters in session
        \Yii::$app->cbSession->set("dynagrid-harvest-manager-package-mgmt-grid-body",$requestBody);
        foreach ($urlParams as $uParam) {
            if (substr($uParam, 0, strlen('sort')) == 'sort') {
                \Yii::$app->cbSession->set("dynagrid-harvest-manager-package-mgmt-grid-filters", $uParam);
                break;
            }
        }

        // Retrieve packages
        if(!$retrieveAll){
            return $this->apiHelper->sendRequestSinglePage($method, $endpoint, $requestBody, $urlParams);
        }
        else{
            $urlParamStr = !empty($urlParams) ? trim(implode('&',$urlParams)) : '';

            return $this->apiHelper->getParsedResponse($method, $endpoint, json_encode($requestBody), $urlParamStr, true);
        }
    }

    /**
     * Returns default fields for the API call
     * for retrieving the package records.
     */
    public function getDefaultFields() {
        return "seed.id AS seedDbId" .
        "|seed.seed_code AS seedCode" .
        "|seed.seed_name AS seedName" .
        "|plot.plot_number AS seedSourcePlotNumber" . 
        "|plot.plot_code AS seedSourcePlotCode" .
        "|plot.rep AS replication" .
        "|germplasm.id AS germplasmDbId" .
        "|germplasm.germplasm_code AS germplasmCode" .
        "|germplasm.designation AS designation" .
        "|germplasm.germplasm_state AS germplasmState" .
        "|germplasm.generation AS generation" .
        "|germplasm.parentage AS parentage" .
        "|femaleGermplasm.id AS femaleGermplasmDbId" .
        "|femaleGermplasm.designation AS crossFemaleParent" .
        "|femaleGermplasm.parentage AS femaleParentage" .
        "|femalePlot.plotDbId AS femalePlotDbId" .
        "|femalePlot.plotCode AS femalePlotCode" .
        "|femalePlot.plotNumber AS femalePlotNumber" .
        "|maleGermplasm.id AS maleGermplasmDbId" .
        "|maleGermplasm.designation AS crossMaleParent" .
        "|maleGermplasm.parentage AS maleParentage" .
        "|package.id AS packageDbId" .
        "|package.package_code AS packageCode" .
        "|package.package_label AS label" .
        "|package.package_quantity AS quantity" .
        "|seed.harvest_date AS harvestDate" .
        "|seed.harvest_method AS harvestMethod" .
        "|seed.harvest_source AS harvestSource" .
        "|entry.entry_code AS sourceEntryCode" .
        "|package.package_unit AS unit".
        "|seed.program_id AS programDbId".
        "|program.program_code AS program".
        "|seed.source_occurrence_id AS occurrenceDbId".
        "|occurrence.occurrence_name AS occurrenceName";
    }

    /**
     * Assemble browser filters
     * @param object params parameters passed by data browser
     * @param integer occurrenceId occurrence identifier
     */
    public function assembleBrowserFilters($params, $occurrenceId) {
        $filters = isset($params['ManagementBrowserModel']) ? $params['ManagementBrowserModel'] : [];
        $columnFilters = [];

        foreach($filters as $key => $value) {
            if($value!="") {
                $equalsString = '';
                if(!str_contains($value,'%')) $equalsString = 'equals ';
                $searchVal = $value;
                if($key=='seedSourcePlotNumber') {
                    $valid = preg_match('/^\d+(-\d+)?(,\d+(-\d+)?)*$/i',$value);
                    if(!$valid) {
                        $columnFilters[$key] = $value;
                    }
                    else {
                        $cleanString = str_replace(" ","",$searchVal);
                        $pieces = explode(",",$cleanString);
                        $numbers = [];
                        foreach($pieces as $pc) {
                            if (strpos($pc, '-') !== false) {
                                $range = explode('-',$pc);
                                $max = 0;
                                $min = 0;
                                if($range[0]>$range[1]) {
                                    $max = $range[0];
                                    $min = $range[1];
                                }
                                else if($range[1]>$range[0]) {
                                    $max = $range[1];
                                    $min = $range[0];
                                }
                                else if($range[0]==$range[1]) {
                                    $max = $range[0];
                                    $min = $range[0];
                                }
                                for($i = $min; $i<=$max; $i++) {
                                    $numbers[] = $i;
                                }
                            }
                            else $numbers[] = $pc;
                        }
                        $newValue = implode("|",$numbers);
                        $columnFilters[$key] = $newValue;
                    }
                    continue;
                }
                if($key=='quantity') {
                    $valid = preg_match('/^(\d+(\.\d+)?)*\s*(g|kg|pan|seeds|vial)?$/i',$value);
                    if(!$valid) {
                        $this->quantity = null;
                    } else {
                        preg_match('/[0-9]*(.[0-9]+)?/',$value,$qtyMatches);
                        $max = -9999;
                        $qty = "";
                        foreach($qtyMatches as $index=>$match) {
                            $current = strlen($match);
                            if ($current > $max) {
                                $max = $current;
                                $qty = $qtyMatches[$index];
                            }
                        }
                        if ($qty != '' AND $qty != ' ') $columnFilters['quantity'] = $qty;

                        preg_match('/(g|kg|pan|seeds|vial)/i',$value,$unitMatches);
                        $unt = "";
                        foreach($unitMatches as $match) {
                            $unt = strtolower($match);
                            break;
                        }
                        if ($unt != '' AND $unt != ' ') $columnFilters['unit'] = $unt;
                    }
                    continue;
                }
                
                $delimeter = strpos($value,",") != FALSE ? ',' : '|';
                $inputs =  explode($delimeter,$value);
                $columnFilters[$key] = $equalsString . implode('|' . $equalsString,$inputs);
            }
        }

        return $columnFilters;
    }

    /**
     * Extract Management browser table column filters
     * @param Integer $occurrenceId occurrence id
     * 
     * @return Array browser filter
     */
    public function getBrowserFilters($occurrenceId){
        $sessionBrowserColumns = \Yii::$app->cbSession->get("ManagementBrowserModel-".$occurrenceId) ?? [];
        $nameColumns = ['designation','label','seedName'];
        $browserFilters = [];

        foreach($sessionBrowserColumns as $key=>$val){
            if($val != ''){ 
                $delimeter = strpos($val,",") != FALSE ? ',' : '|';
                $inputs =  explode($delimeter,$val);
                
                $browserFilters[$key] = in_array($key,$nameColumns) ? implode('|',$inputs) : 'equals '.implode('|equals ',$inputs);
            }
        }
        return $browserFilters;
    }

    /**
     * Retrieve list preview data provider
     * 
     * @param Integer $occurrenceId location identifier
     * @param Array $packageIdList list of packageDbIds
     * @param String $selectionMode selected/all
     * 
     * @return Array data provider and total count of packages
     */
    public function getListPreviewDataProvider($occurrenceId, $packageIdList, $selectionMode = 'selected') {
        $packagesUrlParams = \Yii::$app->cbSession->get("packagesUrlParams-$occurrenceId") ?? [];

        $requestBody = [];
        //define url parameters
        foreach($packagesUrlParams as $key => $value){
            // selected items
            if($selectionMode == 'selected' && strpos($value,'sort') === 0){
                $packagesUrlParams = [$value];
                break;
            }
            // all items
            if(strpos($value,'limit') === 0){
                unset($packagesUrlParams[$key]);
            }

            // remove page value
            if(strpos($value,'page') === 0){
                unset($packagesUrlParams[$key]);
            }
        }

        if($selectionMode == 'all'){
            $packages = $this->getPackageSeedGermplasmIds($occurrenceId, 'package', false);
            if(!$packages['success']){
                throw new \yii\base\ErrorException(\Yii::t('app', $packages['message']));
            }
            $packageIdList = $packages['data'];

            // retrieve management tab browser column filters
            $params = \Yii::$app->cbSession->get("packagesQueryParams-$occurrenceId");
            $requestBody = $this->assembleBrowserFilters($params, $occurrenceId);
            $requestBody["fields"] = $this->getDefaultFields();
        }

        if(!empty($packageIdList)){
            $requestBody['packageDbId'] = 'equals ' .implode('|equals ', $packageIdList);
        }

        $response = $this->getPackageRecords($requestBody, $packagesUrlParams, $occurrenceId);

        if($response['status'] !== 200){
            throw new \yii\base\ErrorException(\Yii::t('app', $response['message']));
        }
        $data = $response['data'];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'packageDbId',
            'totalCount' => $response['count'],
            'pagination' => false,
            'id' => 'hm-list-preview'
        ]);
        $totalCount = $selectionMode == 'all' ? $packages['totalCount'] : $response['count'];

        return ['dataProvider' => $dataProvider,'count' => $totalCount];
    }

    /**
     * Check if the list's abbrev and name are unique
     *
     * @param String $abbrev list abbrev
     * @param String $name list name
     * @return Array contains boolean for result and string for message
     */
    public function validateList($abbrev, $name) {

        // Check if list name or abbrev already exists
        $abbrevOrNameAlreadyExists = false;
        $response = $this->lists->searchAll(['abbrev' => $abbrev, 'fields' => 'list.id as listDbId|list.abbrev'], 'limit=1&sort=listDbId:asc', false);
        if(isset($response['data'][0])){
            $abbrevOrNameAlreadyExists = true;
        }
        $response = $this->lists->searchAll(['name' => $name, 'fields' => 'list.id as listDbId|list.name as name'], 'limit=1&sort=listDbId:asc', false);
        if(isset($response['data'][0])){
            $abbrevOrNameAlreadyExists = true;
        }
        if($abbrevOrNameAlreadyExists) {
            return ['result' => false, 'message' => 'Name or Abbrev already exists. Please provide a new one.'];
        }


        return ['result' => true];
    }

    /**
     * Save package/germplasm/seed list
     *
     * @param Integer $occurrenceId occurrence identifier
     * @param Array $listDetails list attributes
     * @param Array $ids list member ids
     * @param String $selectionMode: selected or all
     */
    public function saveList($occurrenceId, $listDetails, $ids = [], $selectionMode = 'selected') {
        // Initialize variables
        $success = true;
        $message = '';
        $background = false;
        $type = $listDetails['records'][0]['type'];

        // Get threshold
        $createListThreshold = \Yii::$app->config->getAppThreshold('HARVEST_MANAGER','createList');

        // Build filters
        $params = \Yii::$app->cbSession->get("packagesQueryParams-$occurrenceId");
        $filters = $this->assembleBrowserFilters($params, $occurrenceId);
        $filters["fields"] = $this->getDefaultFields();

        // add sorting
        $packagesUrlParams = \Yii::$app->cbSession->get("packagesUrlParams-$occurrenceId");
        $sortParam = array_values(array_filter(
            $packagesUrlParams,
            function($param){ return strpos($param,'sort=') > -1; })
        );
        $sortParam = $sortParam[0] ?? '';

        // Get total count
        $totalCount = 0;
        $thresholdExceeded = false;
        
        if($selectionMode == "all") {
            // Get total count
            $totalCount = \Yii::$app->cbSession->get("packagesTotalCount-$occurrenceId");
            // If total count is less than the threshold, retrieve db ids
            if($totalCount <= $createListThreshold) {
                $packagesData = $this->getPackageSeedGermplasmIds($occurrenceId, $type);
                $currentData = isset($packagesData) && isset($packagesData['data']) && !empty($packagesData['data']) ? $packagesData['data'] : [];
                $ids = $currentData;
            }
            
        }
        else {
            $totalCount = count($ids);
        }

        // Check if threshold was exceeded
        if($totalCount > $createListThreshold) {
            $thresholdExceeded = true;
        }

        // If count is 0, return an error
        if($totalCount == 0){
            $success = false;
            $message = 'No list members found. Please create packages first.';
            return ['success' => $success, 'message' => \Yii::t('app', $message), 'inBackground' => $background];
        }

        // Create list record
        $data = $this->lists->create($listDetails);

        if($data['status'] == 200) {
            $listId = $data['data'][0]['listDbId'];

            // If threshold was exceeded, send process to the background
            if($thresholdExceeded) {
                $background = true;
                $message = "The $type list is being created in the background. You may proceed with other tasks. You will be notified in this page once done.";
                $bgProcess = $this->createMembersInBackground($occurrenceId, $filters, $listId, $type, $ids, $sortParam);
                if($bgProcess['success'] == false){
                    $success = false;
                    $message = 'There seems to be a problem while saving the list members in the background.';
                    return ['success' => $success, 'message' => \Yii::t('app', $message), 'inBackground' => $background];
                }
            }
            else {
                // Save list members
                $isListMemberSaved = $this->lists->createMembers($listId, $ids, false, [], true);
                if(!$isListMemberSaved['success']) {  // if errors were encountered during saving of list members
                    $success = false;
                    $message = 'There seems to be a problem while saving the list members.';
                    return ['success' => $success, 'message' => \Yii::t('app', $message), 'inBackground' => $background];

                }
                $message = ucfirst($type).' List:&nbsp;<b>'.$listDetails['records'][0]['name'].'</b>&nbsp;successfully created.';
            }
        } else {
            $success = false;
            $message = 'There seems to be a problem while saving the list.';
            return ['success' => $success, 'message' => \Yii::t('app', $message), 'inBackground' => $background];

        }

        return ['success' => $success, 'message' => \Yii::t('app', $message), 'inBackground' => $background];
    }

    /**
     * Retrieve and save list members using CreateHarvestList worker
     * 
     * @param Integer $occurrenceId occurrence identifier
     * @param Array $filters filters currently applied in the browser
     * @param Integer $listId list identifier
     * @param String $listType list type: package/seed/gemplasm
     * @param Array $ids record ids to be added to the list
     */
    public function createMembersInBackground($occurrenceId, $filters, $listId, $listType, $ids, $sortParam){

        // Invoke worker
        $result = $this->worker->invoke(
            'CreateHarvestList',                         // Worker name
            "Create $listType list members for occurence ID: $occurrenceId",     // Description
            'OCCURRENCE',                                   // Entity
            $occurrenceId,                                  // Entity ID
            'OCCURRENCE',                                   // Endpoint Entity
            'HARVEST_MANAGER',                              // Application
            'POST',                                         // Method
            [                                               // Worker data
                "occurrenceId" => $occurrenceId,
                "filters" => $filters,
                "listId" => $listId,
                "listType" => $listType,
                "idList" => $ids,
                "sortConfig" => $sortParam
            ]
        );

        return $result;
    }


    /**
     * Retrieve all package, seed, and germplasm ids
     * 
     * @param Integer $occurrenceDbId occurrence identifier
     * @param String $type list type(package/germplasm/seed)
     * @param Boolean $retrieveAll true - retrieve all records, false - first page only
     */
    public function getPackageSeedGermplasmIds($occurrenceDbId, $type, $retrieveAll = true) {

        $id = $type.'DbId';
        
        // retrieve management tab browser column filters
        $params = \Yii::$app->cbSession->get("packagesQueryParams-$occurrenceDbId");
        $requestBody = $this->assembleBrowserFilters($params, $occurrenceDbId);
        $requestBody["fields"] = $this->getDefaultFields();

        $method = 'POST';
        $endpoint = "occurrences/$occurrenceDbId/packages-search";
        
        // add sorting
        $packagesUrlParams = \Yii::$app->cbSession->get("packagesUrlParams-$occurrenceDbId");
        $sortParam = array_values(array_filter(
            $packagesUrlParams,
            function($param){ return strpos($param,'sort=') > -1; })
        );
        $sortParam = $sortParam[0] ?? '';

        $data = [];
        $results = [];
        $totalCount = 0;
        if($retrieveAll){
            $results = $this->apiHelper->sendRequest($method,$endpoint,$requestBody,[$sortParam]);
            $data = $results;
        }
        else{
            $results = $this->apiHelper->getParsedResponse($method,$endpoint,json_encode($requestBody),$sortParam);
            $data = $results['data'];
        }

        $idList = [];
        $success = false;
        $message = 'Process failed! Error encountered in completing the process.';
        if(isset($results) && !empty($results)){
            $idList = array_column($data, $id);
            $totalCount = $retrieveAll ? count($data) : $results['totalCount'];

            $success = 200;
            $message = '';
        }       

        return ['data' => $idList, 'totalCount' => $totalCount, 'success' => $success, 'message' => $message];
    }

    /**
     * Checks and removes seeds already used in entries from the roster
     * @param array $seedIds array of seeds to check
     * @return object containing summary of entry check
     */
    public function checkSeedEntries($seedIds) {
        $seedIdsToDelete = [];
        $seedIdsWithEntries = [];
        $germplasmIdsToDelete = [];
        $germplasmSeedRelation = [];
        $seedsToDeleteCount = 0;
        $seedsWithEntriesCount = 0;

        foreach($seedIds as $seedId) {
            // Retrieve entry count of the seed
            $result = $this->apiHelper->sendRequestSinglePage(
                'POST',
                'seeds/' . $seedId . '/entries-count',
                null
            );
            if($result['status'] !== 200){
                throw new \yii\base\ErrorException(\Yii::t('app', $result['message']));
            }
            $data = isset($result['data']) ? $result['data'] : [];
            $entriesCount = isset($result['count']) ? $result['count'] : 0;

            // If the seed has no entries, add to the array
            if($entriesCount == 0) {
                $seed = isset($data[0]) ? $data[0] : [];
                $germplasmId = isset($seed['germplasmDbId']) ? $seed['germplasmDbId'] : 0;

                if(!in_array($seedId, $seedIdsToDelete)) $seedIdsToDelete[] = $seedId;
                if(!in_array($germplasmId, $germplasmIdsToDelete)) $germplasmIdsToDelete[] = $germplasmId;
                $germplasmSeedRelation[$germplasmId][] = $seedId;
                $seedsToDeleteCount++;
            }
            else {
                if(!in_array($seedId, $seedIdsWithEntries)) $seedIdsWithEntries[] = $seedId;
                $seedsWithEntriesCount++;
            }
        }

        return [
            "seedIdsToDelete" => $seedIdsToDelete,
            "seedIdsWithEntries" => $seedIdsWithEntries,
            "germplasmIdsToDelete" => $germplasmIdsToDelete,
            "germplasmSeedRelation" => $germplasmSeedRelation,
            "seedsToDeleteCount" => $seedsToDeleteCount,
            "seedsWithEntriesCount" => $seedsWithEntriesCount,
            "totalSeeds" => count($seedIds)
        ];
    }

    /**
     * Checks if the germplasm is used by seeds other than those included in the deletion.
     * If a germplasm is used by other seeds, the seed Id using it will be added in a separate array.
     * Else, the germplasm id is stored in the germplasm ids array.
     * @param array $seedIdsToDelete array of seed ids to delete
     * @param array $germplasmIdsToDelete array of germplasm ids to delete
     * @param object $germplasmSeedRelation array mapping of germplasm ids and the seeds that use them
     * @return object containing seed and germplasm id arrays
     */
    public function checkGermplasm($seedIdsToDelete, $germplasmIdsToDelete, $germplasmSeedRelation) {
        $allGermplasmDbIds = [];
        $seedsToDelete = [];

        // Check if other seeds are using the germplasm
        $germplasmIdsTemp = [];
        foreach($germplasmIdsToDelete as $germplasmId) {
            // Use germplasm/:id/seeds-search-lite
            $result = $this->apiHelper->sendRequest(
                'POST',
                'germplasm/' . $germplasmId . '/seeds-search-lite',
            );
            // Store ids in arrays
            $germplasm = isset($result[0]) ? $result[0] : [];
            $germplasmSeeds = isset($germplasm['seeds']) ? $germplasm['seeds'] : [];
            $germplasmSeedIds = [];
            foreach($germplasmSeeds as $gs) {
                $germplasmSeedIds[] = $gs['seedDbId'];
            }

            // If the germplasm is not referrenced by other seeds,
            // add it to the array
            if(count(array_diff($germplasmSeedIds,$seedIdsToDelete))==0) {
                $germplasmIdsTemp[] = $germplasmId;
            }
            else { // Otherwise, add the seed to the array
                $seedsToDelete = array_merge($seedsToDelete, $germplasmSeedRelation[$germplasmId]);
            }
        }
        $allGermplasmDbIds = array_merge($allGermplasmDbIds,$germplasmIdsTemp);

        return [
            "seedIds" => $seedsToDelete,
            "germplasmIds" => $allGermplasmDbIds
        ];
    }

    /**
     * Submit ids to delete to the processor API
     * @param array $seedIds array of seeds to delete
     * @param array $germplasmIds array of seeds to delete
     * @param integer $occurrenceId occurrence identifier
     * @return array $backgroundJobIds array containing background job ids
     */
    public function deleteViaProcessor($seedIds, $germplasmIds, $occurrenceId) {
        $backgroundJobIds = [];

        $entity = "OCCURRENCE";
        $entityDbId = "".$occurrenceId;
        $application = "HARVEST_MANAGER";

        if(!empty($seedIds)) {
            // Delete seeds if seed
            $httpMethod = "DELETE";
            $endpoint = "v3/seeds";
            $description = "Deletion of seeds";
            $endpointEntity = "SEED";
            $dependents = [
                "packages",
                "seed-relations",
                "seed-attributes"
            ];
            $optional = [ "dependents" => $dependents ];
            $result = $this->apiHelper->processorBuilder($httpMethod, $endpoint, $seedIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional);
            $job = isset($result[0]) ? $result[0] : [];
            if(isset($job['backgroundJobDbId'])) $backgroundJobIds[] = $job['backgroundJobDbId'];
        }

        if(!empty($germplasmIds)) {
            // Delete germplasm
            $httpMethod = "DELETE";
            $endpoint = "v3/germplasm";
            $description = "Deletion of germplasm";
            $endpointEntity = "GERMPLASM";
            $dependents = [
                "packages",
                "seed-relations",
                "seed-attributes",
                "seeds",
                "family-members",
                "germplasm-relations",
                "germplasm-attributes",
                "germplasm-names"
            ];
            $optional = [ "dependents" => $dependents ];
            $result = $this->apiHelper->processorBuilder($httpMethod, $endpoint, $germplasmIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional);
            $job = isset($result[0]) ? $result[0] : [];
            if(isset($job['backgroundJobDbId'])) $backgroundJobIds[] = $job['backgroundJobDbId'];
        }

        return $backgroundJobIds;
    }

    /**
     * Facilitates the generation of CSV file
     * @param Array $params package record search parameters
     * @param Array $urlParams package records search filters
     * @param Integer $occurrenceId occurrence unique identifier
     * @param String $occurrenceName name of occurrence to be used for the file name
     * @param Array $csvHeaders column header for the CSV file to be generated
     * @param Array $csvAttributes variable attributes
     */
    public function exportPackages($params,$urlParams,$occurrenceId,$occurrenceName,$csvHeaders,$csvAttributes){
        // build sort params
        $sortParams = [];
        if(isset($urlParams) && !empty($urlParams)){
            $sortParams[] = 'sort='.implode('|',$urlParams);
        }

        // retrieve all seeds and package records
        $records = $this->getPackageRecords($params,$sortParams,$occurrenceId,true);
        $records = $records['data'];

        $csvContent = [[]];
        $i = 1;

        if(!empty($records)){
            foreach ($csvHeaders as $value)
                array_push($csvContent[0], $value);
    
            foreach ($records as $value) {
                $csvContent[$i] = [];
                foreach ($csvAttributes as $v) {
                    if(isset($value[$v])){
                        array_push($csvContent[$i], $value[$v]);
                    }
                    else{
                        array_push($csvContent[$i], null);
                    }
                }
                $i += 1;
            }
        }
        else{
            // redirect
            exit;
        }

        $timeNow = date("Ymd_His", time());
        $occurrenceName = str_replace(' ','_',$occurrenceName);
        $filename = $occurrenceName . "_Packages_$timeNow".'.csv';
    
        header("Content-Disposition: attachment; filename={$filename}; Content-Type: application/csv;");
        $fp = fopen('php://output', 'wb');

        if ($fp === false) die('Failed to create CSV file.');

        foreach ($csvContent as $line) fputcsv($fp, $line);

        fclose($fp);

        exit;        
    }
}