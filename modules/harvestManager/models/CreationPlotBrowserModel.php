<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import yii
use yii\data\Sort;
// Import data providers
use app\dataproviders\ArrayDataProvider;
// Import interfaces
use app\interfaces\modules\harvestManager\models\ICreationModel;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\harvestManager\models\IPlotBrowserModel;
// Import models
use app\models\Plot;


/**
* Model class for Plot browser
*/
class CreationPlotBrowserModel extends Plot
{
    public $designation;
    public $entryNumber;
    public $harvestStatus;
    public $parentage;
    public $plotCode;
    public $plotDbId;
    public $plotNumber;
    public $rep;
    public $state;
    public $type;

    /**
     * Model constructor
     */
    public function __construct(
        public ICreationModel $creationModel, 
        public IHarvestManagerModel $harvestManagerModel, 
        public IPlotBrowserModel $plotBrowserModel)
    { }

    public function rules()
    {
        return [
            [['designation', 'plotDbId', 'plotNumber', 'rep', 'state', 'type'], 'required'],
            [['plotDbId', 'plotNumber', 'entryNumber', 'rep'], 'number'],
            [['plotDbId', 'plotNumber', 'entryNumber', 'rep'], 'integer'],
            [['plotCode', 'harvestStatus', 'designation', 'parentage', 'state', 'type'], 'string'],
            [['designation', 'entryNumber', 'harvestStatus', 'parentage', 'plotCode', 'plotDbId', 'plotNumber', 'rep', 'state', 'type'], 'safe'],
            [[], 'boolean'],
            [['plotDbId'], 'unique']
        ];
    }

    /**
     * Retrieve plot records to display
     * @param object params POST parameters
     * @param integer experimentId experiment identifier
     * @param integer occurrenceId occurrence identifier
     * @param integer locationId location identifier
     * @param object browserConfig browser configurations
     * @param boolean loadData  whether or not to load data
     * @param boolean hidden states if the browser is hidden
     * @return object containing data provider and array of germplasm states
     */
    public function search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadData, $hidden) {
        // If data is not to be loaded or if browser is hidden, return empty array data provider
        if(!$loadData || $hidden) {
            $dataProvider = new ArrayDataProvider([
                'allModels' => [],
                'key' => 'plotDbId',
                'restified' => true,
                'totalCount' => 0
            ]);
            return array(
                'dataProvider' => $dataProvider,
                'germplasmStates' => []
            );
        }

        // Check if browser pagination and fitlers will be reset
        $reset = $this->harvestManagerModel->browserReset($occurrenceId, $params, 'CreationPlotBrowserModel');
        $resetPage = $reset['resetPage'];
        $resetFilters = $reset['resetFilters'];

        // If resetFilters = true, set params to null
        if($resetFilters) {
            $params = null;
        }
        // Load params into the browser search model
        $this->load($params);

        // Assemble URL parameters
        $urlParams = $this->harvestManagerModel->assembleUrlParameters(
            'dynagrid-harvest-manager-plot-crt-grid',
            'sort=plotNumber:ASC',
            $resetPage
        );

        // Assemble browser filters
        $columnFilters = $this->assembleBrowserFilters($experimentId);

        // Get plots
        $result = $this->plotBrowserModel->getPlotRecords($occurrenceId, $columnFilters, $urlParams);
        $plots = $result['data'];
        $totalCount = $result['count'];

        // If no plots are retrieved, reset the pagination.
        if(count($plots) == 0){
            // Assemble URL parameters
            $urlParams = $this->harvestManagerModel->assembleUrlParameters(
                'dynagrid-harvest-manager-plot-crt-grid',
                'sort=plotNumber:ASC',
                true
            );

            // Get plots
            $result = $this->plotBrowserModel->getPlotRecords($occurrenceId, $columnFilters, $urlParams);
            $plots = $result['data'];
            $totalCount = $result['count'];
        }

        // get appropriate harvest data configs for plots
        $plots = $this->harvestManagerModel->matchConfigs($plots, $occurrenceId, 'plot');

        $plotsCount = count($plots);
        for ($i = 0; $i < $plotsCount; $i++) {
            // validate plor records and generate appropriate remarks
            $plots[$i] =  $this->creationModel->buildHarvestRemarksColumn($plots[$i]);
        }

        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'allModels' => $plots,
            'id' => 'dynagrid-harvest-manager-plot-crt-grid',
            'key' => 'plotDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);
        // Initialize sort
        $sort = new Sort();
        $sort->attributes = [
            'plotNumber'=>[
                "asc" => ["plotNumber"=>SORT_ASC],
                'desc' => ["plotNumber"=>SORT_DESC]
            ],
            'rep'=>[
                "asc" => ["rep"=>SORT_ASC],
                'desc' => ["rep"=>SORT_DESC]
            ],
            'plotCode'=>[
                "asc" => ["plotCode"=>SORT_ASC],
                'desc' => ["plotCode"=>SORT_DESC]
            ],
            'parentage'=>[
                "asc" => ["parentage"=>SORT_ASC],
                'desc' => ["parentage"=>SORT_DESC]
            ],
            'state'=>[
                "asc" => ["state"=>SORT_ASC],
                'desc' => ["state"=>SORT_DESC]
            ],
            'type'=>[
                "asc" => ["type"=>SORT_ASC],
                'desc' => ["type"=>SORT_DESC]
            ],
            'designation'=>[
                "asc" => ["designation"=>SORT_ASC],
                'desc' => ["designation"=>SORT_DESC]
            ]
        ];
        // Apply sort to data provider
        $dataProvider->setSort($sort);

        return array(
            'dataProvider' => $dataProvider,
            'germplasmStates' => []
        );
    }

    /**
     * Assemble browser filters
     * @param integer experimentId - experiment identifier
     * @return object columnFilters - browser filters in API request body format
     */
    public function assembleBrowserFilters($experimentId)
    {
        $browserColumns = [
            'experimentDbId' => "equals $experimentId",
            'harvestStatus' => 'equals NO_HARVEST||INCOMPLETE%',
            'state' => 'not equals unknown',
            'terminalHarvestMethod' => '[OR] not null',
            'harvestMethod' => '[OR] not null',
        ];

        $generatedBrowserColumns = $this->creationModel->generateBowserColumns();

        if (isset($generatedBrowserColumns) && !empty($generatedBrowserColumns)) {
            foreach ($generatedBrowserColumns as $column) {
                $browserColumns[$column] = "[OR] not null";
            };
        }

        return [
            'conditions' => [
                [
                    'experimentDbId' => "equals $experimentId",
                    'state' => 'not equals unknown',
                    'harvestStatus' => 'equals READY|equals QUEUED_FOR_HARVEST|equals HARVEST_IN_PROGRESS|equals BAD_QC_CODE|INCOMPLETE%|CONFLICT%'
                ],
                $browserColumns
            ],
            'sort' => [
                "attribute" => "harvestStatus",
                "sortValue" => "DONE|HARVEST_IN_PROGRESS|QUEUED_FOR_HARVEST|READY|NO_HARVEST|INCOMPLETE%"
            ]
        ];
    }
}