<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\models;

// Import Yii
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
// Import interfaces
use app\interfaces\models\IConfig;
use app\interfaces\models\IExperiment;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IProgram;
use app\interfaces\models\IUser;

/**
* Model class for Search Parameters in HM
*/
class SearchParametersModel {
    /**
     * Model constructor
     */
    public function __construct(
        public IConfig $config,
        public IExperiment $experiment,
        public IOccurrence $occurrence,
        public IProgram $program,
        public IUser $user)
    { }

    /**
     * Retrieve display text of parameter's initial value
     * @param object filter search filter 
     * @return string initVal intial value text
     */
    public function getFilterValuesById($filter) {
        $filterName = strtolower(explode("_",$filter['abbrev'])[0]);
        $filterId = $filter['values'];
        $initVal = array('ids'=>'','texts'=>'');
        
        if($filterName=='program') {
            $programInfo = $this->program->getProgram($filterId);
            $programName = $programInfo['programName'] ?? '';
            $programCode = $programInfo['programCode'] ?? '';
            $initVal['texts'] = $programName != '' && $programCode != '' ? $programName . ' (' . $programCode . ')' : '';
        }

        if($filterName=='occurrence') {
            $occurrenceInfo = $this->occurrence->get($filterId);
            $initVal['texts'] = $occurrenceInfo['occurrenceName'] ?? '';
        }

        if($initVal['texts']!='') {
            $initVal['ids'] = $filterId;
        }

        return $initVal;
    }

    /**
     * Generate Query Parameter Filter Fields
     * @param string dataUrl url for retrieving select2 data
     * @param object variableFilters filter information
     */
    public function generateFilterFields($dataUrl, $variableFilters) {
        $spConfig = $this->config->getConfigByAbbrev('HM_SEARCH_PARAMETERS');
        $configVariables = isset($spConfig['values']) ? $spConfig['values'] : [];
        $variableFields = [];
        $size = ' col-md-12';

        if (!empty($configVariables) && $configVariables != null) {

            foreach ($configVariables as $confVar) {

                $varArray = [];

                $abbrev = $confVar['variable_abbrev'];

                $varArray['label'] = isset($confVar['field_label']) ? $confVar['field_label'] : '';
                $varArray['order_number'] = $confVar['order_number'];
                $varArray['required'] = isset($confVar['required']) ? true : false;
                $varArray['default'] = 0;

                $reqStr = ($confVar['required'] == 'true' || $abbrev == 'PROGRAM' ? ' <span class="required">*</span>' : '');
                $label = Html::label(
                    ucwords(strtolower($varArray['label'])) . $reqStr,
                    $abbrev,
                    [
                        'data-label' => ucwords(strtolower($varArray['label'])),
                        'style' => 'margin-top:10px;',
                    ]
                );


                $name = ($abbrev == 'VOLUME' ? 'filter[' . $abbrev . '_COND]' : 'filter[' . $abbrev . ']');

                $initVal = null;
                if ($variableFilters != null && !empty($variableFilters)) {

                    $initVal = null;
                    foreach ($variableFilters as $varFilter) {
                        if ($varFilter['abbrev'] == $abbrev) {
                            $initVal = $this->getFilterValuesById($varFilter);
                            break;
                        }
                    }
                }

                if ($confVar['basic_parameter'] == "true") {

                    $varId = strtolower($abbrev) . '-filter';
                    $data = null;

                    if ($confVar['input_field'] == 'selection') {
                        $varArray['value'] = $this->generateHTMLElements($dataUrl, $name, $varId, $data, $varArray['default'], $reqStr, $label, $size, $initVal, $confVar['input_type']);
                    }

                    $variableFields[] = $varArray;
                }
            }
            return $variableFields;
        } else {
            return [];
        }
    }

    /**
     * Returns generated Select2 html fields
     * @param string dataUrl url to be accessed by the plugin
     * @param string name name of the plugin
     * @param integer varId variable identifier
     * @param object data default data object
     * @param string val default value of field
     * @param string reqStr string to display if required
     * @param string label element lable
     * @param integer size horizontal size of the element
     * @param string inputType type of input (number, text, etc)
     */
    public function generateHTMLElements($dataUrl, $name, $varId, $data, $val, $reqStr, $label, $size, $initVal, $inputType) {
        $pluginOptions = [
            'allowClear' => true,
            'minimumInputLength' => 0,
            'placeholder' => 'Search value',
            'language' => [
                'errorLoading' => new JSExpression("function(){ return 'Unable to fetch data at the moment.'; }"),
            ],'ajax' => [
                'url' => $dataUrl,
                'dataType' => 'json',
                'delay'=> 250,
                'data'=> new JSExpression(
                    'function (params) {

                        var filters = $(".harvest-filter").serializeArray();
                        var id = $(this).attr("id");

                        return {
                            q: params.term, // search term
                            id: id,
                            page: params.page,
                            filters: JSON.stringify(filters)
                        }
                    }

                    '
                ),
                'processResults' => new JSExpression(
                    'function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                            more: (params.page * 5) < data.totalCount
                            },
                        };
                    }'
                ),
                'cache' => true
            ],
            'templateResult' => new JSExpression(
                'function formatRepo (repo) {
                    if (repo.loading) {
                        return repo.text;
                    }

                    return repo.text;
                }'
            ),
            'templateSelection' => new JSExpression(
                'function formatRepoSelection (repo) {
                    return repo.text;
                }'
            ),
        ];

        if ($val == 0 || empty($val) || $val == null) {
            $val = null;
        };

        $data = null;
        $initValuesText = ($initVal == null ? null : $initVal['texts']);
        $initValues = ($initVal == null ? null : $initVal['ids']);
        $val = $initValues;

        // Get select 2
        // For unit testing purposes, building the select2 moved to the sibling function "buildSelect2"
        // to allow for partial mocking of the class.
        $value = $this->buildSelect2($name, $varId, $initValuesText, $initValues, $reqStr, $inputType, $pluginOptions, $data);


        $hidden = Html::input(
            'text',
            $name,
            !empty($val) ? $val : null,
            [
                'id' => $varId . '-hidden',
                'class' => 'select2-input-validate-hidden hidden variable-filter',
            ]
        );

        $label = Html::tag('div', $label, ['class' => 'row', 'style' => 'margin-bottom:5px;']);
        $select = Html::tag('div', $value . $hidden, ['class' => 'row', 'style' => 'margin-bottom:5px; padding-right:10px;']);
        
        if($name=='filter[OCCURRENCE_NAME]') {
            $warning = '
                <span>
                    <p class="hidden" id="occurrence-name-warning" style="color:#c62828">
                        Occurrence is required. Please select one.
                    </p>
                </span>
            ';

            $htmlElement = Html::tag('div', $label . $select . $warning, ['class' => 'control-label col-md-12' . $size, 'style' => 'margin-bottom:10px; padding-right:0px;']);
        }
        
        else {
            $htmlElement = Html::tag('div', $label . $select, ['class' => 'control-label col-md-12' . $size, 'style' => 'margin-bottom:10px; padding-right:0px;']);
        }

        return $htmlElement;
    }

    /**
     * Builds select2 with the given parameters
     * @param string name name of the select2
     * @param integer varId id of the select2
     * @param string initValuesText the display text of the initial values
     * @param array initValues ids of the initial values
     * @param string reqStr if the input is required or not
     * @param string inputType type of input (single or multiple)
     * @param object pluginOptions object containing options for the plugin
     * @param object data data to display
     */
    public function buildSelect2 ($name, $varId, $initValuesText, $initValues, $reqStr, $inputType, $pluginOptions, $data) {
        return Select2::widget([
            'name' => $name,
            'id' => $varId,
            'data' => [],
            'initValueText' => $initValuesText,
            'value' => $initValues,
            'options' => [
                'tags' => true,
                'class' => 'select2-input-validate' . $reqStr . ' variable-filter ',
                'multiple' => ($inputType === 'single' ? false : true),
                'title' => ($name=='filter[LOCATION_NAME]' ? 'Select location before loading plots' : '')
            ],
            'pluginOptions' => $pluginOptions,
            'pluginEvents' => [
                "change" => "
                        function() { 
                            $('#'+this.id+'-hidden').val($(this).val()); 
                            //  alert($(this).val());
                        }
                        ",
            ],
            'showToggleAll' => ($varId === 'study_name-filter' || $varId === 'experiment_name-filter' || empty($data) || $data == null ? false : true),
        ]);
    }

    /**
     * Retrieve variable filters
     * @param array existingFilters array currently existing filters
     * @param array configVar array variable information
     * @param integer id element id
     */
    public function extractVariableFilters($existingFilters, $configVar, $id)
    {
        $variableFilters = [];

        //for each existing filter values
        foreach ($existingFilters as $key => $value) {
            $duplicate = false;
            $configInfo = [];

            //extract name of filter
            $name = count(explode('[', $value['name'])) > 1 ? explode(']', explode('[', $value['name'])[1])[0] : $value['name'];

            // check for duplicate -- if filter has existing values
            foreach ($variableFilters as $filterValues) {
                $duplicate = ($filterValues['abbrev'] == $name ? true : false);
            }

            // extract config var of the existing variable
            foreach ($configVar as $key => $val) {
                if ($val['variable_abbrev'] == $name) {
                    $configInfo = $val;
                    break;
                }
            }

            // add variable anf config info of existing variables to variablefilters
            //condition - should apply to all variables
            if (isset($configInfo) && !empty($configInfo)) {
                if ($value['value'] != "" && $value['name'] != "_csrf" && $name != strtoupper(explode('-', $id)[0])) {
                    if ($duplicate == true) {
                        // if duplicate, replace with most current values
                        foreach ($variableFilters as $key => $val) {
                            if ($val['abbrev'] == $name) {
                                $variableFilters[$key]['values'] = $value['value'];
                                break;
                            }
                        }
                    } else {
                        $variableFilters[] = ['abbrev' => $name, 'values' => $value['value'], 'config_info' => $configInfo];
                    }
                }
            }
        }

        return $variableFilters;
    }

    /**
     * Retrieve data for variable filters
     *
     * @param string $searchTerm Select2 search text
     * @param integer $userId user current user identifier
     * @param integer $filterId filter element id
     * @param array $variableFilters list of variable filters
     * @param array $defaultFilters list of default search filters
     * @param integer $limit number of displayed options
     * @param integer $page page number
     * @return array $data list of filtered data
     */
    public function getVariableFilterData($searchTerm, $userId, $filterId, $variableFilters, $defaultFilters, $limit, $page) {
        $data = array('count'=>0,'data'=>[]);
        $searchTerm = !empty($searchTerm) ? strtolower($searchTerm) : "";
        $requestBody = [];
        $isAdmin = $this->user->isAdmin();

        // harvest manager filters
        foreach ($variableFilters as $filter) {
            $filterName = strtolower($filter['abbrev']);
            if(strpos($filterName,'plotno')!==false || strpos($filterName,'replication_number')!==false) continue;
            $filterVariable = explode('_', $filterName)[0] . "DbId";
            $filterValue = $filter['values'];
            $requestBody[$filterVariable] = $filterValue;
        }

        $defaultFiltersProcessed = [];
        foreach($defaultFilters as $key => $value) {
            $filterKey = $key;
            if ($filterKey=="owned") {
                if($value=="true") {
                    $defaultFiltersProcessed["stewardDbId"] = "[OR] ".$userId;
                    $defaultFiltersProcessed["creatorDbId"] = "[OR] ".$userId;
                }
                continue;
            }
            if(strpos($key,'_id')!==false) {
                $filterKey = explode('_', $key)[0] . "DbId";
            }
            if(!empty($value)) {
                if (is_array($value)) {
                    $value = implode('|equals ',$value);
                }
                $defaultFiltersProcessed[$filterKey] = $value;
            }
        }

        // Get program code of selected program
        $selectedProgramCode = null;
        if (isset($requestBody['programDbId'])) {
            $selectedProgram = $this->program->searchAll([
                "programDbId" => "equals " . $requestBody['programDbId'] ?? 0
            ]);

            $selectedProgramCode = $selectedProgram['data'][0]['programCode'] ?? null;
        }

        $requestBody = array_merge($requestBody,$defaultFiltersProcessed);
        if(!(strpos($filterId,'program')!==false) &&  !isset($requestBody['programDbId'])) return $data;

        $urlParam = "limit=$limit&page=$page";

        // search programs
        if(strpos($filterId,'program')!==false) {
            $result = $this->user->getUserPrograms($userId, $urlParam);
            $programs = $result == null ? [] : $result;
            $result = $this->user->getUserPrograms($userId);
            $all = $result == null ? [] : $result;
            $data['count'] = count($all);
            foreach ($programs as $program) {
                if (!empty($searchTerm)) {
                    if(strpos(strtolower($program['text']),$searchTerm)!==false) {
                        $data['data'][] = $program;
                    }
                } else {
                    $data['data'][] = $program;
                }
            }
        }
        // search experiments
        else if(strpos($filterId,'experiment')!==false) {
            $requestBodyExperiment = [
                'fields' => 'program.id AS programDbId|experiment.id AS experimentDbId|experiment.experiment_name AS experimentName|experiment.experiment_year AS experimentYear|experiment.experiment_status AS experimentStatus',
                'experimentStatus' => 'equals created|equals planted|equals planted;crossed'
            ];

            if(isset($requestBody['stageDbId'])) {
                $requestBodyExperiment['fields'] .= "|stage.id as stageDbId";
                $requestBodyExperiment['stageDbId'] = "equals " . $requestBody['stageDbId'];
            }
            if(isset($requestBody['seasonDbId'])) {
                $requestBodyExperiment['fields'] .= "|season.id as seasonDbId";
                $requestBodyExperiment['seasonDbId'] = "equals " . $requestBody['seasonDbId'];
            }
            if(isset($requestBody['year'])) {
                $requestBodyExperiment['experimentYear'] = "equals " . $requestBody['year'];
            }
            if(isset($requestBody['stewardDbId'])) {
                $requestBodyExperiment['fields'] .= "|person.id as stewardDbId";
                $requestBodyExperiment['stewardDbId'] = $requestBody['stewardDbId'];
            }
            if(isset($requestBody['creatorDbId'])) {
                $requestBodyExperiment['fields'] .= "|creator.id as creatorDbId";
                $requestBodyExperiment['creatorDbId'] = $requestBody['creatorDbId'];
            }
            if(!empty($searchTerm)) {
                $requestBodyExperiment['experimentName'] = "%$searchTerm%";
            }

            $urlParam = 'permission=write&sort=experimentYear:desc|experimentDbId:asc&' . $urlParam;
            if($isAdmin) {
                // If the user is an admin, add programDbId in request body.
                $requestBodyExperiment['programDbId'] = 'equals ' . $requestBody['programDbId'];
            }
            else {
                // If the user is NOT an admin, add collaboratorProgramCode in url params.
                $urlParam = 'collaboratorProgramCode=' . $selectedProgramCode . '&' . $urlParam;
            }
            $requestBodyExperiment = json_encode($requestBodyExperiment,JSON_UNESCAPED_SLASHES);

            $result = $this->experiment->getExperimentSelect2Data($requestBodyExperiment, $urlParam);

            $data['count'] = isset($result['totalCount']) ? $result['totalCount'] : 0;
            $data['data'] = isset($result['data']) ? $result['data'] : [];
        }
        // search occurrences
        else if(strpos($filterId,'occurrence')!==false) {
            $requestBodyOccurrence = [
                'fields' => "program.id as programDbId|occurrence.id as occurrenceDbId|occurrence.occurrence_name as occurrenceName|occurrence.occurrence_status AS occurrenceStatus|experiment.experiment_type AS experimentType|experiment.experiment_year AS experimentYear",
                'occurrenceStatus' => '%planted%',
            ];

            if(isset($requestBody['experimentDbId'])) {
                $requestBodyOccurrence['experimentDbId'] = "equals " . $requestBody['experimentDbId'];
                $requestBodyOccurrence['fields'] .= "|occurrence.experiment_id as experimentDbId";
            }
            if(isset($requestBody['stageDbId'])) {
                $requestBodyOccurrence['fields'] .= "|stage.id as stageDbId";
                $requestBodyOccurrence['stageDbId'] = "equals " . $requestBody['stageDbId'];
            }
            if(isset($requestBody['seasonDbId'])) {
                $requestBodyOccurrence['fields'] .= "|season.id as seasonDbId";
                $requestBodyOccurrence['seasonDbId'] = "equals " . $requestBody['seasonDbId'];
            }
            if(isset($requestBody['year'])) {
                $requestBodyOccurrence['year'] = "equals " . $requestBody['year'];
                $requestBodyOccurrence['fields'] .= "|experiment.experiment_year as year";
            }
            if(isset($requestBody['siteDbId'])) {
                $requestBodyOccurrence['fields'] .= "|site.id as siteDbId";  
                $requestBodyOccurrence['siteDbId'] = "equals " . $requestBody['siteDbId'];
            }
            if(!empty($searchTerm)) {
                $requestBodyOccurrence['occurrenceName'] = "%$searchTerm%";
            }

            $urlParam = 'permission=write&sort=experimentYear:desc|occurrenceDbId:asc&' . $urlParam;
            if($isAdmin) {
                // If the user is an admin, add programDbId in request body.
                $requestBodyOccurrence['programDbId'] = 'equals ' . $requestBody['programDbId'];
            }
            else {
                // If the user is NOT an admin, add collaboratorProgramCode in url params.
                $urlParam = 'collaboratorProgramCode=' . $selectedProgramCode . '&' . $urlParam;
            }
            $requestBodyOccurrence = json_encode($requestBodyOccurrence,JSON_UNESCAPED_SLASHES);

            $result = $this->occurrence->getOccurrenceSelect2Data($requestBodyOccurrence, $urlParam);
            
            $data['count'] = isset($result['totalCount']) ? $result['totalCount'] : 0;
            $data['data'] = isset($result['data']) ? $result['data'] : [];
        }

        return $data;
    }

    /**
     * Retrieves the occurrences that belong to the experiment
     * @param integer experimentId experiment identifier
     */
    public function getExperimentOccurrences($experimentId) {
        $data = [];

        $requestBody = [
            "experimentDbId" => "$experimentId",
            "occurrenceStatus" => "%planted%"
        ];

        $result = $this->occurrence->searchAll($requestBody,"permission=write");
        $occurrences = isset($result['data']) ? $result['data'] : [];

        foreach($occurrences as $occurrence) {
            $data[] = [
                'id' => $occurrence['occurrenceDbId'],
                'text' => $occurrence['occurrenceName']
            ];
        }

        return json_encode($data);
    }

    /**
     * Retrieves the experiment where the occurrence belongs to
     * @param integer occurrenceId occurrence identifier
     */
    public function getOccurrenceExperiment($occurrenceId) {
        $requestBody = [
            "occurrenceDbId" => "$occurrenceId",
            "occurrenceStatus" => "%planted%"
        ];

        $result = $this->occurrence->searchAll($requestBody,"permission=write");
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];

        return json_encode([
            "experimentId" => $occurrence["experimentDbId"],
            "data" => [
                [
                    "id" => $occurrence["experimentDbId"],
                    "text" => $occurrence["experiment"]
                ]
            ]
        ]);
    }
}
