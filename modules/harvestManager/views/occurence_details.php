<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\helpers\Url;

/**
 * Renders occurrence details in the occurrence selection step
 */

$this->registerCss('
    .no-occ-sel {
        font-weight: bold;
        font-size: 15px;
        text-align: center;
        color: #adadad;
    }
');

$controllerID = Yii::$app->controller->id;

$occurrenceDetails = isset($data['occurrenceDetailsTab1']) ? $data['occurrenceDetailsTab1'] : [];
if(isset($occurrenceTab) && !$occurrenceTab) $occurrenceDetails = isset($data['occurrenceDetailsOther']) ? $data['occurrenceDetailsOther'] : [];
$harvestSummaryDetails = isset($data['harvestSummaryDetails']) ? $data['harvestSummaryDetails'] : [];

if(!isset($data['occurrenceDetailsTab1']) && !isset($data['harvestSummaryDetails'])) {
    echo '<div class="collapsible-header active" style="margin-bottom:10px">' .
            '<i class="material-icons">info_outline</i> ' . \Yii::t('app', 'Occurrence Details') .
        '</div>' . 
        '<div id="occurrence-detail-div">' .
            '<span id="no-occurrence-text">' .
                '<p class="no-occ-sel">' .
                    'No occurrence selected'. 
                '</p><br>' .
            '</span>' .
            '<div id="occurrence-detail-content-div"></div>' .
        '</div>' . 
        '<div class="collapsible-header active" style="margin-bottom:10px">' .
            '<i class="material-icons">agriculture</i> ' . \Yii::t('app', 'Harvest Summary') .   
        '</div>' . 
        '<div id="harvest-summary-div">' .
            '<span id="no-occurrence-text">' .
                '<p class="no-occ-sel">' .
                    'No occurrence selected' .
                '</p>' .
            '</span>' .
            '<div id="harvest-summary-content-div"></div>' .
        '</div>';
}
else {
    echo '<div class="collapsible-header active col-md-12" style="margin-bottom:10px">' .
            '<i class="material-icons">info_outline</i> ' . \Yii::t('app', 'Occurrence Details') .
        '</div>' . 
        '<div id="occurrence-detail-div" style="margin-left:20px;margin-bottom:0px" >';
    foreach ($occurrenceDetails as $key => $value) {
        $label = $key;
        $value = ($value == '') ? $notSet : $value;
        $index = lcfirst(str_replace(' ','',$key));
        echo
            '<div class="col-md-' . $divSize . '" stye="margin-bottom:10px;">
                <dl>
                    <dt title="' . $label . '">' . $label . '</dt>
                    <dd><div id="'.$index.'">'.$value.'</div></dd>
                </dl>
            </div>
            ';
    }
    echo '</div>' . 
    '<div class="collapsible-header col-md-12 active" style="margin-bottom:10px">' .
        '<i class="material-icons">agriculture</i> ' . \Yii::t('app', 'Harvest Summary') .   
    '</div>' . 
    '<div id="harvest-summary-div" style="margin-left:20px;margin-bottom:0px" >';
    $iconName = 'help_outline';
    foreach ($harvestSummaryDetails as $key => $value) {
        $label = ucfirst($key);
        $value = ($value == '') ? 0 : $value;
        $icon = '';
        $title = $label;

        if($label == 'Plots with seeds') {
            $title = "$label/Total plot count";
        }
        else if($label == 'Seeds from plots') {
            $title = "$label/Total seeds from this location";
        }
        else if($label == 'Crosses with seeds') {
            $title = "$label/Total cross count";
        }
        else if($label == 'Seeds from crosses') {
            $title = "$label/Total seeds from this location";
        }

        echo
            '<div class="col-md-' . $divSize . '" stye="margin-bottom:10px;">
    			<dl>
    				<dt title="' . $title . '">' . $label . ' ' . $icon . '</dt>
    				<dd><div style="overflow-wrap: break-word;max-width:200px">'.$value.'</div></dd>
    			</dl>
    		</div>
    		';
    }
    echo '</div>';
}

?>