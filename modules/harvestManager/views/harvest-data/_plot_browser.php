<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use app\components\PrintoutsWidget;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\GermplasmInfo;

/**
 * Renders plot harvest data input step for Harvest Manager
 */

echo '<div id="hm-hdata-plot" class = "row col col-sm-12 col-md-12 col-lg-12" style = "margin-bottom:0px; padding:0px">';

// Add browser header
echo '<div class="hm-browser-title">' .
        '<h3 class="hm-browser-title">' . \Yii::t('app', 'Plots') . '</h3>' .
    '</div>';

// Define URLs
$urlRetrieveHarvestProtocol = Url::to(['harvest-data/retrieve-harvest-protocol']);
$urlSaveHarvestProtocol = Url::to(['harvest-data/save-harvest-protocol']);

// Unpack browser data
$dataProvider = $browserData['dataProvider'];
$plotGermplasmStates = isset($browserData['germplasmStates']) ? $browserData['germplasmStates'] : []; 
$harvestDataBrowserDescription = '';

$browserId = 'dynagrid-harvest-manager-plot-hdata-grid';

// Set action columns
$actionColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ],
    [
        'label' => "Checkbox",
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'visible' => true,
        'mergeHeader' => true,
        'vAlign' => 'top',
        'header' => '<input type="checkbox" class="filled-in" id="hm-browser-select-all-checkbox" table-con="' . $browserId . '-table-con"/><label style="padding-left: 20px;" for="hm-browser-select-all-checkbox"></label>',
        'content' => function ($model) use ($dataLevel, $browserId) {
            $checkboxId = $model['plotDbId'];
            $disabledCheckbox = '';
            $harvestStatus = $model['harvestStatus'];
            if(in_array($harvestStatus,['QUEUED_FOR_HARVEST','HARVEST_IN_PROGRESS'])) $disabledCheckbox = 'disabled';
            
            return '
            <input class="hm-browser-single-checkbox filled-in"' .
                'id="' . $checkboxId . '"' .
                'type="checkbox" ' .
                $disabledCheckbox . ' ' .
                'table-con="' . $browserId . '-table-con"' .
                'data_level="'. $dataLevel .'"/>' .
            '<label style="padding-left: 20px;" for="' . $checkboxId . '"></label>';
        },
        'width' => '1%',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT
    ],

];

// Set harvest status column
$harvestStatus = [
    'attribute' => 'harvestStatus',
    'label' => 'Harvest Status',
    'hAlign' => 'left',
    'vAlign' => 'top',
    'width' => 'auto',
    'noWrap' => true,
    'format' => 'raw',
    'visible' => true,
    'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
    // COMMENTED OUT TO SHIFT FOCUS TO HARVEST STATUS QUICK FILTER.
    // THE CODE WILL BE KEPT HERE IN CASE THERE IS A REQUEST TO RE-ADD.
    // 'filterType' => GridView::FILTER_SELECT2,
    // 'filter' => ['NO_HARVEST' => 'no harvest',
    //     'INCOMPLETE' => 'incomplete harvest data',
    //     'CONFLICT' => 'data conflict',
    //     'READY' => 'ready for seed creation',
    //     'IN_QUEUE' => 'in queue for creation',
    //     'IN_PROGRESS' => 'seed creation in progress',
    //     'COMPLETED' => 'harvest complete',
    //     'DELETION_IN_PROGRESS' => 'seed/trait deletion in progress',
    //     'REVERT_IN_PROGRESS' => 'revert harvest in progress',
    //     'INVALID_STATE' => 'invalid state',
    //     'FAILED' => 'seed creation failed'
    // ],
    // 'filterWidgetOptions' => [
    //     'pluginOptions' => ['multiple'=>false, 'allowClear' => true],
    // ],
    // 'filterInputOptions' => ['placeholder' => 'Select status'],
    'value' => function ($model) use ($dataLevel) {
        $harvestStatus = isset($model['harvestStatus']) ? $model['harvestStatus'] : 'NO_STATUS';
        $state = $model['state'];
        $crossMethod = $model['crossMethod'] ?? 'selfing';
        $dataLevel = $dataLevel == 'plots' ? 'plot' : 'cross';

        $hvSummaryInfoLink = Html::a(
            '<i class="material-icons blue-text" style="vertical-align:bottom">info</i>',
            '#',
            [
                'id' => 'hv-summary-btn',
                'data-tooltip' => \Yii::t('app', 'View harvest summary'),
                'class' => 'hm-tooltipped',
                'data-id' => $model['plotDbId'],
                'data-level' => 'plot',
                'data-target' => "#hm-hd-hv-summary-modal",
                'data-dismiss' => 'modal',
                'data-toggle' => "modal"
            ]
        );

        if (empty($crossMethod)) {
            return '<span title="Missing cross method value" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'MISSING CROSS METHOD') . '</strong></span>';
        }

        if($state == 'unknown') {
            return '<a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank"><span title="State is unknown. Click here to request data curation." class="new badge center red darken-2"><strong>' . \Yii::t('app', 'INVALID STATE') . '</strong></span></a>';
        }

        if($harvestStatus=='NO_HARVEST') {
            return '<span title="No seeds created yet" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'NO HARVEST') . '</strong></span>';
        } else if (strpos($harvestStatus,'INCOMPLETE')!==false) {
            return '<span title="Harvest data is incomplete" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'INCOMPLETE DATA') . '</strong></span>';
        } else if (strpos($harvestStatus,'CONFLICT')!==false) {
            $parts = explode(': ', $harvestStatus);
            $title = $parts[1] ?? 'Unknown conflict';
            return '<span title="' . $title . '" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'DATA CONFLICT') . '</strong></span>';
        } else if (str_contains($harvestStatus,'FAILED')) {
            return '<span><span title="Seed and package creation failed" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'FAILED') . '</strong></span>&nbsp;' .$hvSummaryInfoLink;
        } else if ($harvestStatus=='BAD_QC_CODE') {
            return '<span title="Harvest data with B (bad) QC code detected" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'BAD QC CODE') . '</strong></span>';
        } else if ($harvestStatus=='DELETION_IN_PROGRESS') {
            return '<span title="Deletion of traits and seeds/packages is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'DELETION IN PROGRESS') . '</strong></span>';
        } else if ($harvestStatus=='REVERT_IN_PROGRESS') {
            return '<span title="Reversion of failed harvest is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'REVERT IN PROGRESS') . '</strong></span>';
        } else if ($harvestStatus=='HARVEST_IN_PROGRESS') {
            return '<span title="Seed and package creation is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'HARVEST IN PROGRESS') . '</strong></span>';
        } else if ($harvestStatus=='UPDATE_IN_PROGRESS') {
            return '<span title="Update of harvest data is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'UPDATE IN PROGRESS') . '</strong></span>';
        } else if ($harvestStatus=='COMPLETED') {
            return '<span title="Seeds and packages have been created" class="new badge center green darken-2"><strong>' . \Yii::t('app', 'HARVEST COMPLETE') . '</strong></span>&nbsp;' . $hvSummaryInfoLink;
        } else if ($harvestStatus=='READY') {
            return '<span title="'.ucfirst($dataLevel).' is ready for seed and package creation" class="new badge center blue darken-2"><strong>' . \Yii::t('app', 'READY') . '</strong></span>';
        } else if ($harvestStatus=='QUEUED_FOR_HARVEST') {
            return '<span title="Queued for seed and package creation" class="new badge center grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'QUEUED FOR HARVEST') . '</strong></span>';
        } else if ($harvestStatus=='QUEUED_FOR_UPDATE') {
            return '<span title="Queued for harvest data update" class="new badge center grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'QUEUED FOR UPDATE') . '</strong></span>';
        } else {
            return '<span class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
        }
    },
    'headerOptions'=>[
        'style' => 'text-align:left'
    ],
];

// Set default browser columns
$columns = [
    $harvestStatus,
    [
        'attribute' => 'plotNumber',
        'label' => 'Plot No.',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'rep',
        'label' => 'Rep',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'headerOptions' => ['style' => 'width:5%'],   
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'entryNumber',
        'label' => 'Entry No.',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'entryCode',
        'label' => 'Entry Code',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'designation',
        'label' => 'Germplasm',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'format' => 'raw',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'contentOptions' => [
            "class" => "germplasm-name-col"
        ],
        'value' => function($model) {
            $germplasmDbId = $model['germplasmDbId'];
            $designation = $model['designation'];
            
            $linkAttrs = [
                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                'class' => 'blue-text text-darken-4 header-highlight',
                'title' => \Yii::t('app', 'Click to view more information'),
                'data-label' => $designation,
                'data-id' => $germplasmDbId,
                'data-target' => '#view-germplasm-widget-modal',
                'data-toggle' => 'modal'
            ];

            $widgetName = GermplasmInfo::widget([
                'id' => $germplasmDbId,
                'entityLabel' => $designation,
                'linkAttrs' => $linkAttrs
            ]);
            return $widgetName;
        }
    ],
    [
        'attribute' => 'plotCode',
        'label' => 'Plot Code',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'parentage',
        'label' => 'Parentage',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'format' => 'raw',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'contentOptions' => [
            "class" => "germplasm-name-col"
        ],
        'value' => function ($model) {
            return '<div style="overflow-wrap: break-word;max-width:150px">'.$model['parentage'].'</div>';
        }
    ],
    [
        'attribute' => 'state',
        'label' => 'Germplasm State',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => true,
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $germplasmStateScaleValues,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true,'class' => 'col-lg-2'],
        ],
        'filterInputOptions' => ['placeholder' => 'Select state'],
    ],
    [
        'attribute' => 'type',
        'label' => 'Germplasm Type',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => true,
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $germplasmTypeScaleValues,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true,'class' => 'col-lg-2'],
        ],
        'filterInputOptions' => ['placeholder' => 'Select type'],
    ],
];

// Merge columns
$gridColumns = array_merge($actionColumns, $columns, $inputColumns);

// Initialize printouts component
$printouts = PrintoutsWidget::widget([
    "product" => "Harvest Manager",
    "program" => $program,
    "occurrenceIds" => [ $occurrenceId ]
]);


// Render grid
$dynagrid = DynaGrid::begin([
    'columns'=>$gridColumns,
    'storage' => DynaGrid::TYPE_SESSION,
    'theme'=>'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'tableOptions'=>['class'=> $browserId . '-table'],
        'options'=>['id'=> $browserId . '-table-con'],
        'striped'=>false,
        'hover' => true,
        'floatHeader' => true,
        'responsive' => true,
        'floatOverflowContainer' => true,
        'showPageSummary' => false,
        'summaryOptions' => ['class' => 'pull-right'],
        'pjax' => true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => $browserId, 'enablePushState' => false,],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'id' => $browserId . '-table-con',
        'responsiveWrap' => false,
        'floatHeaderOptions' => [
			'position' => 'absolute',
            'top' => 63
		],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', "Manage your plot harvest data here.".$harvestDataBrowserDescription) . ' {summary}' .
                '<br/>
                <p id="total-selected-text" class = "pull-right" style = "margin: -1px;"></p>
                <div class="col-md-12" id="harvest-data-filter-div" style="padding-top:15px">'
                    .$observationDataFilters.            
                '</div>
                <div class="row">
                    <div class="col-md-4" id="harvest-status-filter-div" style="padding-top:15px">'
                    .$statusChips.            
                    '</div>
                    <div class="hm-range-filter col-md-6" style="padding-top:15px">'
                        .$toolbarFilters.
                    '</div>
                </div>',
            'after' => false,
        ],
        'toolbar' =>  [
            [
                'content' =>
                    Html::a('<i class="glyphicon glyphicon-edit"></i>' .
                        '', '#', [
                        'id' => 'hm-hdata-bulk-update-btn',
                        'class' => 'btn btn-default hm-tooltipped',
                        'style' => "margin-left:10px;",
                        'data-toggle' => 'modal',
                        'data-target' => '#hm-hd-bulk-update-modal',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Bulk update harvest traits'),
                        'disabled' => false,
                        'data-level' => 'plot',
                    ]).
                    Html::a('<i class="material-icons">delete_forever</i>' .
                        '', '#', [
                        'id' => 'hm-hdata-bulk-delete-btn',
                        'class' => 'btn btn-default red darken-3 hm-tooltipped',
                        'data-toggle' => 'modal',
                        'data-target' => '#hm-hd-bulk-delete-modal',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Bulk delete harvest traits'),
                        'disabled' => false,
                        'data-level' => 'plot',
                    ]).
                    Html::a('<i class="material-icons">undo</i>' .
                        '', '#', [
                        'id' => 'hm-hdata-revert-harvest-btn',
                        'class' => 'btn btn-default orange darken-3 hm-tooltipped',
                        'data-toggle' => 'modal',
                        'data-target' => '#hm-hd-bulk-delete-modal',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Revert failed harvest'),
                        'disabled' => false,
                        'data-level' => 'plot',
                    ]).
                    /**
                     * The ADDITIONAL HARVEST is TEMPOARILY DISABLED and HIDDEN.
                     * 
                     * Further develoment is needed to implement this feature in the refactored Harvest Manager version.
                     */
                    // Html::a('<i class="material-icons">add</i>' .
                    //     '', '#', [
                    //     'id' => 'hm-hdata-additional-harvest-btn',
                    //     'class' => 'btn btn-default green darken-3 hm-tooltipped',
                    //     'data-toggle' => 'modal',
                    //     'data-target' => '#hm-hd-addl-harvest-modal',
                    //     'data-position' => 'top',
                    //     'data-tooltip' => \Yii::t('app', 'Additional harvest'),
                    //     'disabled' => false,
                    //     'data-level' => 'plot',
                    // ]).
                    Html::a(
                        '<i class="material-icons large">notifications_none</i>
                        <span id="notif-badge-id" class=""></span>',
                        '#',
                        [
                            'id' => 'hm-hd-notifs-btn',
                            'class' => 'btn waves-effect waves-light harvest-manager-notification-button hm-tooltipped',
                            'data-pjax' => 0,
                            'style' => "overflow: visible !important;",
                            "data-position" => "top",
                            "data-tooltip" => \Yii::t('app', 'Notifications'),
                            "data-activates" => "notifications-dropdown",
                            'disabled' => false,
                        ]
                    )
            ],
            [
                'content' =>
                    $printouts.
                    /**
                     * The FILTER BY OBSERVATION DATA feature is TEMPOARILY DISABLED and HIDDEN.
                     * 
                     * Further develoment is needed to implement this feature in the refactored Harvest Manager version.
                     */
                    // Html::a(
                    //     '<i class="material-icons">visibility</i>',
                    //     '#',
                    //     [
                    //         'class' => 'btn btn-default lighten-4 hm-tooltipped',
                    //         'id' => 'hm-hd-observation-data-button-main',
                    //         'data-toggle' => 'modal',
                    //         'data-target' => '#hm-hd-observation-data-modal',
                    //         'data-position' => 'top',
                    //         'data-tooltip' => \Yii::t('app', 'Filter by observation data'),
                    //         'data-level' => 'plot',
                    //     ]
                    // ).
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [Yii::$app->controller->action->id, 'dataLevel' => $dataLevel],
                        [
                            'class' => 'btn btn-default hm-tooltipped',
                            'id' => 'hm-hd-reset-plot-btn',
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    ).
                    '{dynagridFilter}{dynagridSort}{dynagrid}',
            ]
        ],
    ],
    'options' => [ 'id' => $browserId ]
]);

DynaGrid::end();

echo '</div>';

//Modals

// Harvest method protocol usage
Modal::begin([
    'id' => 'hm-protocol-modal',
    'header' => '<h4><i class="material-icons left">content_copy</i>'.\Yii::t('app',' Harvest method protocol').'</h4>',
    'footer' =>
        '<div id="method-protocol-btn-group-1" class="method-protocol-btn-group">'
        . Html::a(\Yii::t('app',"No, don't use the default method"),['#'],['id'=>'no-default-hv','data-dismiss'=>'modal']).'&emsp;'
        . Html::a(\Yii::t('app','Confirm'). ' <i class="material-icons right">send</i>','#',['id' => 'confirm-default-hv-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal','data-dismiss'=>'modal', 'title' => \Yii::t('app','Yes, use default harvest method protocol')])
        . '&emsp;'
        . '</div>'
        . '<div id="method-protocol-btn-group-2" class="method-protocol-btn-group">'
        . Html::a(\Yii::t('app','Acknowledge'),'#',['id' => 'acknowledge-default-hv-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal','data-dismiss'=>'modal', 'title' => \Yii::t('app','Acknowledge')])
        . '&emsp;'
        . '</div>',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 1040;']
]);
echo '<div id="hm-protocol-modal-body"></div>';
Modal::end();

$script = <<< JS
// Set variables
var occurrenceId = $occurrenceId;
var gridId = '$browserId-table-con';
var selectionStorageId = occurrenceId + "_" + gridId;
var compatMapping = null;
// Restore checkbox selection
checkboxActionsHM(selectionStorageId, gridId);

$("document").ready(function(){
    checkHarvestProtocol();
    // Reset browser button click event
    $(document).on("click","#hm-hd-reset-plot-btn", function(e) {
        e.preventDefault();
        // Clear selection on grid reset
        clearSelectionHM(selectionStorageId);
        var pageurl = $('#$browserId li.active a').attr('href');
        $.pjax.reload(
            {
                container:'#$browserId-pjax',
                url: pageurl,
                replace: false,
                type: 'POST',
                data: {
                    forceReset:true
                }
            }
        );
    });

    //  User opt to apply harvest method protocol click event
    $(document).on('click','#confirm-default-hv-btn', function(e) {
        var confirmedMethodSessionName = 'confirmedMethodProtocol-' + occurrenceId;
        var harvestMethod = $('#default-harv-method').text();

        //Set method confirmation session to true
        localStorage.setItem(confirmedMethodSessionName, true);

        $.ajax({
            url: '$urlSaveHarvestProtocol',
            type: 'POST',
            data: {
                occurrenceDbId:"$occurrenceId",
                harvestMethod: harvestMethod,
                compatMapping: JSON.stringify(compatMapping)
            },
            success: function(updated){

                if(updated){
                    //Set success notification
                    var notif = "<i class='material-icons green-text left'>check</i>The default harvest method protocol was applied successfully!";
                    //Refresh plots' browser
                    $("#hm-hd-reset-plot-btn").click();
                }else{
                    var notif = "<i class='material-icons orange-text left'>warning</i>The default harvest method protocol is not applicable to all plots.";
                }

                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    Materialize.toast(notif, 3000);
                }
            },
            error: function(jqXHR, exception) {
            }
        });
    });

    // User opt not to apply default harvest method click event
    $(document).on('click','#no-default-hv', function(e) {
        var confirmedMethodSessionName = 'confirmedMethodProtocol-' + occurrenceId;
        localStorage.setItem(confirmedMethodSessionName, 'did not apply');
    });

    // User acknowledges the method is not applicable click event
    $(document).on('click','#acknowledge-default-hv-btn', function(e) {
        var confirmedMethodSessionName = 'confirmedMethodProtocol-' + occurrenceId;
        localStorage.setItem(confirmedMethodSessionName, 'did not apply');
    });

    // Perform after pjax completion of browser
    $(document).on('ready pjax:complete','#$browserId-pjax', function(e) {
        // Initialize tooltip
        initializeTooltips();
        // Adjust layout
        adjustLayout();
        // Restore checkbox selection
        checkboxActionsHM(selectionStorageId, gridId);
        // Set alignment of table headers to 'top'
        thAlignTop();
    });
});

/**
 * Checks if there is a specified harvest method protocol
 * and prompts the confirmation message
 */
function checkHarvestProtocol() {
    // hide button groups
    $('.method-protocol-btn-group').addClass('hidden');

    var occurrenceId = "$occurrenceId";
    
    //retrieve protocol sessions
    var defaultMethodSessionName = 'defaultMethodProtocol-' + occurrenceId;
    var confirmedMethodSessionName = 'confirmedMethodProtocol-' + occurrenceId;
    var defaultHarvMethSessionFlag = 'defaultHarvMethProtocolCheck-' + occurrenceId;

    var defaultMethod = localStorage.getItem(defaultMethodSessionName);
    var confirmedMethod = localStorage.getItem(confirmedMethodSessionName);
    var hasCheckedDefaultHarvMethProtocol = localStorage.getItem(defaultHarvMethSessionFlag);

    $.ajax({
        url: '$urlRetrieveHarvestProtocol',
        type: 'POST',
        data: {
            occurrenceDbId: occurrenceId
        },
        dataType: 'json',
        async: true,
        success: function(response){
            var defaultHarvMeth = response['harvestMethod'];
            compatMapping = response['compatMapping'];

            if(defaultHarvMeth !== ''){
                if(defaultHarvMeth == defaultMethod && (confirmedMethod == 'true' || confirmedMethod == 'did not apply')) {
                    return;
                }
                else if(response['methodApplicable'] === false){
                    // Show buttons
                    $('#method-protocol-btn-group-2').removeClass('hidden');
                    localStorage.setItem(defaultMethodSessionName, defaultHarvMeth);
                    localStorage.setItem(confirmedMethodSessionName, false);
                    // Add message in modal
                    var hvModalContent = 'The default harvest method <b id="default-harv-method">' + defaultHarvMeth + '</b> is not applicable to any plots in the current selection. '
                    + 'The method will not be used.';
                    $('#hm-protocol-modal-body').html(hvModalContent);
                    $('#hm-protocol-modal').modal('show');
                    return ;
                }

                // Show buttons
                $('#method-protocol-btn-group-1').removeClass('hidden');

                localStorage.setItem(defaultMethodSessionName, defaultHarvMeth);
                localStorage.setItem(confirmedMethodSessionName, false);

                // Build supported germplasm string
                var supported = '';
                for(var state in compatMapping) {
                    var types = compatMapping[state];
                    
                    supported += '<br>&nbsp; • germplasm state = <strong>' + state + '</strong> and germplasm type = <strong>' + types.join(', ') + '</strong>';
                }

                // Show confirmation message
                var germplasmState = defaultHarvMeth == 'Bulk' ? '' : ' with <b>not_fixed</b> germplasm state';
                var hvModalContent = 'The default harvest method <b id="default-harv-method">' + defaultHarvMeth + '</b> was specified in the experiment protocols. '
                    + 'Click confirm to apply this method to all plots with:' + supported;
                $('#hm-protocol-modal-body').html(hvModalContent);
                $('#hm-protocol-modal').modal('show');

            }else{
                //No specified method in EM/EC
                localStorage.removeItem(defaultMethodSessionName);
                localStorage.removeItem(confirmedMethodSessionName);

                if(!hasCheckedDefaultHarvMethProtocol){
                    // Flash no specified harvest method protocol in EM/EC
                    var notif = "<i class='material-icons blue-text left'>info</i>" + 'No default harvest method protocol selected.';
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        Materialize.toast(notif, 5000);
                    }
                }
            }

            localStorage.setItem(defaultHarvMethSessionFlag, true);
        },
        error: function(jqXHR, exception) {
        }
    });

}

JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    div.hm-browser-title {
        padding: 10px 0px 10px 10px;
    }
');

?>