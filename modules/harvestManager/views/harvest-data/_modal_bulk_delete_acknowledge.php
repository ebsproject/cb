<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Bulk Delete modal contents
 */

$toShow = $recordCount>10 ? 10 : $recordCount;

$headers = [];
$displayFields = [];
if($dataLevel == 'plot') {
    $headers = [
        'Plot Number',
        'Entry Number',
        'Germplasm Name',
        'Germplasm Type'
    ];
    $displayFields = [
        'plotNumber',
        'entryNumber',
        'designation',
        'state'
    ];
}
else if ($dataLevel == 'cross') {
    $headers = [
        'Cross Name',
        'Female Parent',
        'Male Parent'
    ];
    $displayFields = [
        'crossName',
        'crossFemaleParent',
        'crossMaleParent'
    ];
}

 ?>
<div id="acknowledge_plot_message_div" style="margin-bottom:20px">
    <p style="font-size:115%;">
        The following records have seeds that have been used in entries.
        The traits, seeds, and packages of these records will <b>NOT</b>
        be deleted.
    </p>
    <p>
        <b>Showing <?php echo $toShow; ?> of <?php echo $recordCount; ?> records.</b>
    </p>
</div>
<div id="acknowledge_plot_table_div" style="padding: 10px; max-height:300px; background-color: #ffffff;">
    <table class="centered responsive-table" style="background-color: #ffffff;">
    <thead>
        <tr>
            <?php
                foreach($headers as $header) {
                    echo '<th>' . $header . '</th>';
                }
            ?>
        </tr>
    </thead>

    <tbody>
        <?php
            foreach($recordInfo as $record) {
                echo '<tr>';
                foreach($displayFields as $field) {
                    echo '<td>' . $record[$field] . '</td>';
                }
                echo '</tr>';
            }

            if($recordCount>10) {
                echo '
                    <tr>
                    <td>' . ($recordCount-1) . ' more records</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>
                ';
            }
        ?>
    </tbody>
    </table>
</div>