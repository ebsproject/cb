<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Bulk update modal contents
 */
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\date\DatePicker;
\kartik\select2\Select2Asset::register($this);

// Define Urls
$urlRetrieveAllRecordInfo = Url::to(['harvest-data/retrieve-all-record-info']);
$urlUpdateHarvestData = Url::to(['harvest-data/update']);
$urlUpdateRecordsStatus = Url::to(['harvest-data/update-records-status']);
$urlUpdateHarvestDataInBG = Url::to(['harvest-data/update-harvest-data-in-background']);
// Set variables
$recordCount = count($recordIds);
$recordIdsJson = json_encode($recordIds);
$bulkUpdateConfigJSON = json_encode($bulkUpdateConfig);
$recordInfoJSON = json_encode($recordInfo);
$hiddenClass = '';
// Create date picker for harvest date input
$hvDatePicker = DatePicker::widget([
    'name' => 'hm-bulk-update-input-hdate',
    'id' => 'hm-bulk-update-input-hdate',
    'class' => 'hm-bulk-update-input-hdate hm-bulk-update-input',
    'value' => '',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true,
        'clearBtn' => true,
        'startDate'=> date("1970-01-01")
    ],
    'pluginEvents' => [
        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
    ],
    'options' => [
        'autocomplete' => 'off',
        'placeholder' => 'Select harvest date',
        'variableAbbrev' => 'HVDATE_CONT'
    ]
]);
// Create select2 for harvest method input
$hvMethodSelect = Select2::widget([
    'name' => 'hm-bulk-update-input-hmethod',
    'id' => 'hm-bulk-update-input-hmethod',
    'class' => 'hm-bulk-update-input-hmethod hm-bulk-update-input ' . $hiddenClass,
    'data' => $bulkUpdateConfig["harvest_methods"],
    'options' => [
        'placeholder' => 'Select method',
        'variableAbbrev' => 'HV_METH_DISC'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);
// Create select2 for grain color input
$variableScaleValues = (array) $bulkUpdateConfig['variable_scale_values'];
$grainColorScale = !empty($variableScaleValues['grainColor']) ? (array) $variableScaleValues['grainColor'] : [];
$showGrainColor = array_key_exists('GRAIN_COLOR',(array) $bulkUpdateConfig['variable_field_names']) ? '' : ' hidden';
$grainColorSelect = Select2::widget([
    'name' => 'hm-bulk-update-input-graincolor',
    'id' => 'hm-bulk-update-input-graincolor',
    'class' => 'hm-bulk-update-input-graincolor hm-bulk-update-input ' . $showGrainColor,
    'data' => $grainColorScale,
    'options' => [
        'placeholder' => 'Select grain color',
        'variableAbbrev' => 'GRAIN_COLOR'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);

?>

<?php
    // Display number of selected items
    $recordCount = count($recordIds);
    $countString = $recordCount == 0 ? 'No' : $recordCount;
    $itemType = '';
    if($dataLevel == 'plot') {
        if($recordCount != 1) $itemType = 'plots';
        else $itemType = $dataLevel;
    }
    else if($dataLevel == 'cross') {
        if($recordCount != 1) $itemType = 'crosses';
        else $itemType = $dataLevel;
    }
    $count = $countString." selected ".$itemType;
?>

<div class="panel panel-default" style=" min-height:100px; margin-top:10px">
    <div class="row" style="margin-left:5px; margin-top:5px;padding-top:5px; padding-right:25px; padding-left:10px;">
        <div class="collapsible-header active" style="margin-bottom:10px; font-size:14px">
            <div class="col-md-6">
                <i class="material-icons">list</i> <?= \Yii::t('app',"Harvest Traits") ?>
            </div>
            <div class="col-md-6" style="padding: 0px 0px 0px 0px;">
                <p class="float-right">
                    <?= \Yii::t('app',$count) ?>
                </p>
            </div>
        </div>
        <div style="margin-left:20px">
            <?php
                // Render input fields
                echo
                '<div class="harvest-data-input-div col-md-6" style="font-size:14px">
                    <dl>
                        <dt title="harvest-date">Harvest Date</dt>
                        <dd>'.$hvDatePicker.'</dd>
                    </dl>
                </div>
                <div class="harvest-data-input-div col-md-6 ' . $hiddenClass . '" style="font-size:14px">
                    <dl>
                        <dt title="harvest-method" style="margin-bottom:5px">Harvest Method</dt>
                        <dd>'.$hvMethodSelect.'</dd>
                    </dl>
                </div>' . $numVarHTML
                .
                '<div class="harvest-data-input-div col-md-12 ' . $showGrainColor . '" style="font-size:14px">
                    <dl>
                        <dt title="grain-color" style="margin-bottom:5px">Grain Color</dt>
                        <dd>'.$grainColorSelect.'</dd>
                    </dl>
                </div>'
                ;
            ?>
        </div>
    </div>
</div>

<?php

$script = <<< JS
// Set variables
var occurrenceId = $occurrenceId;
var locationId = $locationId;
var dataLevel = "$dataLevel";
var bulkUpdateConfig = $bulkUpdateConfigJSON;
var harvestDataUpdateThreshold = $harvestDataUpdateThreshold;
var hdutFormatted = numberWithCommas($harvestDataUpdateThreshold);
var selectedRecords = $recordInfoJSON;
var allRecords = {};
var selectedRecordsCount = $recordCount;
var allRecordsCount = $allRecordsCount;
var selectedRecordsCountFormatted = numberWithCommas($recordCount);
var allRecordsCountFormatted = numberWithCommas($allRecordsCount);
var crossMetHarvMetCompat = bulkUpdateConfig['cmet_hmet_compat'];
var gStateGTypeHMetCompat = bulkUpdateConfig['gstate_gtype_hmet_compat'];
var variableFieldNames = bulkUpdateConfig['variable_field_names'];
var variablePlaceholder = bulkUpdateConfig['variable_placeholder'];
var variableFPMapping = bulkUpdateConfig['variable_field_placeholder_mapping'];
var variableCompat = bulkUpdateConfig['variable_compat'];
var numVarCompat = bulkUpdateConfig['num_var_compat'];
var stateHMethodCompat = {};
var numVarCompatKeys = Object.keys(numVarCompat);
var kCount = numVarCompatKeys.length;
var variableAbbrevArray = [];
var values = [];
var hasInvalidNumVar = false;
var backgroundOverwrite = false;

// Variables for validation
var varAbb = [];
var vals = [];
var numVarAbbrev = '';
var completedRecords = [];
var deletionRecords = [];
var incompatibleRecords = [];
var overwriteRecords = [];
var missingCMethodRecords = [];
var unsupportedStateRecords = [];
var tcvRecords = [];
var variableIncompatObject = { variables: [] , recordIds: [] };
var methodStateIncompatObject = { states: [] , recordIds: [] };
var methodStateTypeIncompatObject = { types: [] , recordIds: [] };
var numVarMethodIncompatObject = { numvar: "", methods: [] , recordIds: [] };
var numVarCrossMethodIncompatObject = { numvar: "", crossMethods: [] , recordIds: [] };
var harvestMethodCrossMethodIncompatObject = { harvestMethod: "", crossMethods: [], recordIds: []};
var mode = '';

// Adjust button labels based on item count
$("#hm-hd-bulk-update-selected-btn").html('Apply to selected ('+selectedRecordsCountFormatted+')');
$("#hm-hd-bulk-update-all-btn").html('Apply to all ('+allRecordsCountFormatted+')');
// Reset modal
$('#hm-hd-bulk-update-modal-body').show();
$('#update-progress').html('');
buildStateHMethodCompat();

$(document).ready(function() {
    // Harvest date datepicker change event
    $(document).on('change', "#hm-bulk-update-input-hdate", function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        inputChange(value, variableAbbrev, index);
    });

    // Harvest method select2 change event
    $(document).on('change', "#hm-bulk-update-input-hmethod", function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        inputChange(value, variableAbbrev, index);

        // If the input value for harvest method is empty, show all numeric variable inputs
        if(value == '') {
            // If the number of numeric variables is more than 1, clear values for all numeric variables
            if(kCount > 1) $('.hm-bulk-update-input-numvar').val('').trigger('change');
            $('.hm-bulk-update-input-numvar').css({"background-color": ""});
            $('.hm-bulk-update-input-numvar-div').show();
            return;
        }
        // Hide all numeric variable inputs
        $('.hm-bulk-update-input-numvar-div').hide();
        // Check which numeric variable input to show (if applicable)
        for(var i = 0; i < kCount; i++) {
            var abbrev = numVarCompatKeys[i];
            var abbrevIndex = variableAbbrevArray.indexOf(abbrev);
            var compatObject = numVarCompat[abbrev];
            var allowedHarvestMethods = compatObject.harvestMethods;
            // Set element ids based on abbrev
            var inputId = '#hm-bulk-update-input-'+abbrev;
            var divId = inputId+'-div';

            // If the input value for harvest method
            // is included in the allowed harvest methods for the numeric variable,
            // then display the input field for that numeric variable
            if(allowedHarvestMethods.includes(value)) {
                $(divId).show();
            }
            // Else, clear any inputs for all numeric variables
            else {
                hasInvalidNumVar = false;
                numVarAbbrev = '';
                $(inputId).val('').trigger('change');
                $(inputId).css({"background-color": ""});
                inputChange("", abbrev, abbrevIndex);
            }
        }
    });

    // Numeric variable input event
    $(document).on('input', '.hm-bulk-update-input-numvar', function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        // If value is empty, show all numeric variable inputs
        if(value == '') {
            var hMethod = $("#hm-bulk-update-input-hmethod").val();
            if(hMethod == "") $('.hm-bulk-update-input-numvar-div').show();
            numVarAbbrev = '';
        }
        else {
            // Hide other numeric variable issues
            $('.hm-bulk-update-input-numvar-div').hide();
            $('#hm-bulk-update-input-' + variableAbbrev + '-div').show();
            numVarAbbrev = variableAbbrev;
        }

        // Validate input
        var subType = $(this).attr('subType');
        var minimum = $(this).attr('min');
        var placeholder = $(this).attr('placeholder');
        var regex = new RegExp('');
        // Format value based on sub type
        if(subType == 'single_int') {
            regex = new RegExp('^(' + minimum + '|[1-9][0-9]*)$');
            value = value != '' ? parseInt(value).toString() : '';
        }
        else if(subType == 'comma_sep_int') {
            value = cleanCommaSepInt(value);
            regex = new RegExp('^[1-9][0-9]*(,[1-9][0-9]*)*$');
        }
        // If input does not match regex, display error
        if(value != '' && !regex.test(value)) {
            $(this).css({"background-color": "#ffffcc"});
            hasInvalidNumVar = true;
            inputChange('', variableAbbrev, index);
            $('.hm-hd-bulk-update-btn').addClass("disabled");
            return;
        }
        // Remove background color
        $(this).css({"background-color": ""});
        hasInvalidNumVar = false;
        inputChange(value, variableAbbrev, index);
    });

    // Grain color select2 change event
    $(document).on('change', "#hm-bulk-update-input-graincolor", function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        inputChange(value, variableAbbrev, index);
    });

    // After focus event for numvar inputs
    $(".hm-bulk-update-input-numvar").focusout(function(){
        var value = $(this).val();
        // Validate input
        var subType = $(this).attr('subType');
        var minimum = $(this).attr('min');
        var placeholder = $(this).attr('placeholder');
        var regex = new RegExp('');
        // Format value based on sub type
        if(subType == 'single_int') {
            regex = new RegExp('^(' + minimum + '|[1-9][0-9]*)$');
            value = value != '' ? parseInt(value).toString() : '';
        }
        else if(subType == 'comma_sep_int') {
            value = cleanCommaSepInt(value);
            regex = new RegExp('^[1-9][0-9]*(,[1-9][0-9]*)*$');
        }
        // Triger change to update value in display
        $(this).val(value).trigger('change');
        // If input does not match regex, display toast
        if(value != '' && !regex.test(value)) {
            var notif = "<i class='material-icons orange-text left'>warning</i> Invalid input for " + placeholder;
            Materialize.toast(notif, 2000);
            return;
        }
    });

    // Bulk update selected btn click event
    $('#hm-hd-bulk-update-selected-btn').off('click').on('click', function() { 
        completedRecords = [];
        deletionRecords = [];
        incompatibleRecords = [];
        overwriteRecords = [];
        missingCMethodRecords = [];
        unsupportedStateRecords = [];
        variableIncompatObject = { variables: [] , recordIds: [] };
        methodStateIncompatObject = { states: [] , recordIds: [] };
        methodStateTypeIncompatObject = { types: [] , recordIds: [] };
        numVarMethodIncompatObject = { numvar: "", methods: [] , recordIds: [] };
        numVarCrossMethodIncompatObject = { numvar: "", crossMethods: [] , recordIds: [] };
        harvestMethodCrossMethodIncompatObject = { harvestMethod: "", crossMethods: [], recordIds: []};
        mode = 'selected';

        confirmModalBuildContent(selectedRecordsCount);
    });

    // Bulk update all btn click event
    $('#hm-hd-bulk-update-all-btn').off('click').on('click', function() { 
        completedRecords = [];
        deletionRecords = [];
        incompatibleRecords = [];
        overwriteRecords = [];
        missingCMethodRecords = [];
        unsupportedStateRecords = [];
        variableIncompatObject = { variables: [] , recordIds: [] };
        methodStateIncompatObject = { states: [] , recordIds: [] };
        methodStateTypeIncompatObject = { types: [] , recordIds: [] };
        numVarMethodIncompatObject = { numvar: "", methods: [] , recordIds: [] };
        numVarCrossMethodIncompatObject = { numvar: "", crossMethods: [] , recordIds: [] };
        harvestMethodCrossMethodIncompatObject = { harvestMethod: "", crossMethods: [], recordIds: []};
        mode = 'all';

        // Reset confirmation modal
        $('.hm-hd-bulk-update-overwrite-update-true').hide();
        $('.hm-hd-bulk-update-overwrite-update-false').hide();
        $('.hm-hd-bulk-update-proceed-bg-update').hide();
        $('#hm-hd-bulk-update-confirmation-modal-additional-main').hide();

        // If number of all records exceeds the limit, display notice
        if(allRecordsCount > harvestDataUpdateThreshold) {
            displayThresholdExceededMessage();
            return;
        }
        // Retrieve all records (formatted), then build confirmation modal contents
        $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>Loading info of '+allRecordsCount+' records...</i></p><div class="progress"><div class="indeterminate"></div></div>');
        $.ajax({
            url: '$urlRetrieveAllRecordInfo',
            data: {
                occurrenceId: occurrenceId,
                dataLevel: dataLevel
            },
            type: 'POST',
            async: true,
            success: function(data) {
                $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>Info of '+allRecordsCount+' records retrieved. Checking info...</i></p><div class="progress"><div class="indeterminate"></div></div>');
                allRecords = jQuery.parseJSON(data);
                confirmModalBuildContent(allRecordsCount)
            },
            error: function() {
                $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>There was a problem while loading record information.</i></p>');
            }
        });
    });

    // Bulk update confirmation button click event
    $('.hm-hd-bulk-update-confirm-btn-allow').off('click').on('click', function() {
        $('.hm-hd-bulk-update-btn').addClass("disabled");
        var id = $(this).prop('id');
        var updateRecords = [];
        if(mode=='selected') updateRecords = selectedRecords;
        else if(mode=='all') updateRecords = allRecords;

        var tempSelected = [];
        var removeRecords = incompatibleRecords;
        // If the skip button was clicked, add records to overwrite
        // to the array of records to be removed from the roster
        if(id.includes('skip')) {
            removeRecords = incompatibleRecords.concat(overwriteRecords);
        }

        // Collate all records to be updated
        for (var i = 0; i<updateRecords.length; i++) {
            if(!removeRecords.includes(updateRecords[i]['id']))
                tempSelected.push(updateRecords[i]);
        }
        
        updateRecords = tempSelected;

        // Perform bulk update
        performBulkUpdate(updateRecords);
    });

    // Overwrite data check
    $('#hm-hd-bu-bg-overwrite-chk').on('input', function (e) {
        var checked = $(this).prop("checked");
        if (checked) backgroundOverwrite = true;
        else backgroundOverwrite = false;
    });

    // Update harvest data in background
    $('#hm-hd-bulk-update-proceed-bg-btn').off('click').on('click', function() {
        // Add progress bar
        $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>Preparing bulk update of '+allRecordsCount+' records...</i></p><div class="progress"><div class="indeterminate"></div></div>');
        $('#hm-hd-bulk-update-proceed-bg-content').hide();

        // Build POST data
        var bgData = {
            locationId: $locationId,
            occurrenceId: $occurrenceId,
            dataLevel: dataLevel,
            overwrite: backgroundOverwrite,
            variableAbbrevs: JSON.stringify(variableAbbrevArray),
            values: JSON.stringify(values),
            bulkUpdateConfig: JSON.stringify(bulkUpdateConfig),
            others: JSON.stringify({
                numVarAbbrev: numVarAbbrev
            })
        };

        // Trigger background worker
        $.ajax({
            url: '$urlUpdateHarvestDataInBG',
            data: bgData,
            type: 'POST',
            async: true,
            success: function(data) {
                var result = JSON.parse(data);
                var success = result.success;
                var status = result.status;
                
                // Display flash message
                if(success) {
                    // Success message
                    displayCustomFlashMessage(
                        'info', 
                        'fa fa-info-circle',
                        'The harvest data is being updated in the background. You may proceed with other tasks. You will be notified in this page once done.',
                        'hm-bg-alert-div',
                        10
                    );
                }
                else {
                    // Failure message
                    let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status != 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if(status == null) message += ' (#002)';

                    displayCustomFlashMessage(
                        'warning', 
                        'fa-fw fa fa-warning', 
                        message,
                        'hm-bg-alert-div',
                        60
                    );
                }

                // Close the modal
                $("#hm-hd-bulk-update-confirmation-modal").modal('hide');
                // Reset the browser
                resetBrowser();
                return;
            },
            error: function() {
                $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>There was a problem while loading record information.</i></p>');
            }
        });
    });
});

/**
 * Builds the data array for bulk update, 
 * and sends the data to the data collection
 */
function performBulkUpdate(selected) {
    // Display progress bar
    $('#hm-hd-bulk-update-modal-body').hide();
    $('#update-progress').html('<p style="font-size:115%;"><i>Updating...</i></p><div class="progress"><div class="indeterminate"></div></div>');

    // Build record ids array
    var recordIds = [];
    for(var i = 0; i < selected.length; i++) {
        recordIds.push(selected[i]['id']);
    }

    // Build POST data
    var data = {
        variableAbbrevs: JSON.stringify(varAbb),
        recordIds: JSON.stringify(recordIds),
        locationId: $locationId,
        values: JSON.stringify(vals),
        idType: dataLevel
    };

    // Send data
    $.ajax({
        url: '$urlUpdateHarvestData',
        data: data,
        type: 'POST',
        async: true,
        success: function(data) {
            $('#update-progress').html('');
            $('#hm-hd-bulk-update-modal-body').show();
            var notif = "<i class='material-icons green-text'>check</i>&nbsp;Harvest data was successfully updated";
            Materialize.toast(notif, 2500);
            $("#hm-hd-bulk-update-modal.in").modal('hide');
            resetBrowser();
        },
        error: function() {
            $('#update-progress').html('');
            $('#hm-hd-bulk-update-modal-body').show();
            var notif = "<i class='material-icons red-text'>close</i>&nbsp;Bulk update was unsuccessful.";
            Materialize.toast(notif, 2500);
            $("#hm-hd-bulk-update-modal.in").modal('hide');
        }
    });
}

/**
 * Builds the contents of the bulk update confirmation modal
 */
function confirmModalBuildContent(totalCount) {
    varAbb = variableAbbrevArray;
    vals = values;
    var abbrevCount = varAbb.length;

    $('.hm-hd-bulk-update-overwrite-update-true').hide();
    $('.hm-hd-bulk-update-overwrite-update-false').hide();
    $('.hm-hd-bulk-update-proceed-bg-update').hide();
    $('#hm-hd-bulk-update-proceed-bg-content').hide();

    // VALIDATION START
    // Remove records with status 'COMPLETED' or 'DELETION_IN_PROGRESS' from the roster
    eliminateCompletedOrDeletionRecords();
    /**
     * TCV check disabled in support of CORB-6631.
     * The code will be kept here, commented,
     * in case the check needs to be re-enabled.
     */
    // // Catch TCV use case
    // catchTCVUseCase();
    // Take note of records with missing cross methods
    checkMissingCrossMethod();
    // Take note of records with an unsupported state
    checkUnsupportedState();
    // Take note of records that are incompatible with the variable(s) selected
    checkVariableIncompatibility();
    // Take note of records that are incompatible with the harvest method due to the state
    checkMethodStateIncompatibility();
    // Take not of records that are incompatible with the harvest method due to the type
    checkMethodStateTypeIncompatibility();
    // Take note of records that are incompatible with the numeric variable due to the harvest method
    checkNumVarMethodIncompatibility();
    // Take note of records that are incompatible with the numeric variable due to the cross method
    checkNumVarCrossMethodIncompatibility();
    // Take note of records that are incompatible with the harvest method due to the cross method
    checkHarvestMethodCrossMethodIncompatibility();
    // Take note of records to be overwritten with the new data
    countRecordsToOverwrite();
    // Find all harvest data names to be overwritten
    var overwriteTraitList = checkDataToOverwrite();

    // Set variables
    var harvestMethod = vals[varAbb.indexOf('HV_METH_DISC')];
    var currentNumVar = numVarMethodIncompatObject['numvar'];
    var currentNumVarCrossMethod = numVarCrossMethodIncompatObject['numvar'];
    var currentHarvestMethod = harvestMethodCrossMethodIncompatObject['harvestMethod'];
    var operationsAllowed = [];
    var pSize = '<p>';
    totalUpdatableCount = totalCount - incompatibleRecords.length;

    // If there are traits to be overwritten,
    // display a notice in the confirmation modal
    if(overwriteTraitList.length > 0 && overwriteRecords.length > 0) {
        var variablesStr = "";
        // Build the variable string to display
        for(var i = 0; i < overwriteTraitList.length; i++) {
            if (i > 0 && i < overwriteTraitList.length-1) variablesStr += ', ';
            else if (i > 0 && i == overwriteTraitList.length-1) variablesStr += ' and ';
            var varName = variableFPMapping[overwriteTraitList[i]];
            variablesStr += '<b>' + varName + '</b>';
        }
        // Build notice string
        var notice = "<br><p style='font-size:115%;'>You are updating the harvest data of <b>" + totalUpdatableCount + " record(s)</b>."
            + "<br><br><b>"+overwriteRecords.length+" record(s)</b> have existing values for the variables: "+ variablesStr 
            + "<br><br>Do you wish to overwrite these records?</p>";
        // Display the necessary buttons
        $('.hm-hd-bulk-update-overwrite-update-true').show();
        $('.hm-hd-bulk-update-overwrite-update-false').hide();
        // Add 'overwrite' to the allowed operations
        operationsAllowed.push('overwrite');
        // If all records for update are to be overwritten, hide the 'Skip' button
        if(totalUpdatableCount == overwriteRecords.length) {
            $('#hm-hd-bulk-update-skip-overwrite-btn').hide();
            $('#hm-hd-bulk-update-overwrite-update-true-space').addClass('hidden');
        }
        // If some records for update are to be overwritten, show the 'Skip' button
        else if(totalUpdatableCount > overwriteRecords.length) {
            // Add skip to the allowed operations
            operationsAllowed.push('skip');
            $('#hm-hd-bulk-update-skip-overwrite-btn').show();
            $('#hm-hd-bulk-update-overwrite-update-true-space').removeClass('hidden');
        }
    }
    // Else, display short summary and confirmatory prompt
    else {
        // Build notice string
        var notice = "<br><p style='font-size:115%;'>You are updating the harvest data of <b>" + totalUpdatableCount + " record(s)</b>."
            + "<br><br>Are you sure you want to apply these changes?</p>";
        // Display the necessary buttons
        $('.hm-hd-bulk-update-overwrite-update-true').hide();
        $('.hm-hd-bulk-update-overwrite-update-false').show();
        $('#hm-hd-bulk-update-overwrite-update-true-space').removeClass('hidden');
        // Add apply changes to allowed operations
        operationsAllowed.push('apply changes');
    }
    
    // If no records can be updated, display red notice in the modal
    if(totalUpdatableCount==0) {
        notice = '';
        pSize = "<p style='font-size:115%;'>";
        // Hide all buttons
        $('.hm-hd-bulk-update-overwrite-update-true').hide();
        $('.hm-hd-bulk-update-overwrite-update-false').hide();
    }
    // Display the notice in the modal body
    $('#hm-hd-bulk-update-confirmation-modal-body').html(notice);
    $('#hm-hd-bulk-update-confirmation-modal-additional-main').hide();
    // If incompatibilities are found, build summary of incompatibilities
    if(incompatibleRecords.length>0) {
        // Display notice if all records cannot be updated
        if(totalUpdatableCount==0) {
            notice = pSize + "Cannot perform bulk update for <b>" + incompatibleRecords.length + " record(s)</b>.</p>";
            $('#hm-hd-bulk-update-zero-update').show();     // Show spacing after button
        }
        else {
            notice = "";
            $('#hm-hd-bulk-update-zero-update').hide();     // Remove spacing after button
        }
        // Display notice for test cross verification
        if(tcvRecords.length>0) notice += pSize + " > Cannot update <b>" + tcvRecords.length + " record(s).</b> Only the harvest date may be specified for plots of experiments with <b>breeding stage = 'TCV'.</b></p>";
        // Display notice for records with missing cross methods
        if(missingCMethodRecords.length>0) notice += pSize + " > Cannot update <b>" + missingCMethodRecords.length + " record(s)</b> with <b>cross method = unknown</b>.</p>";
        // Display notice for records with unsupported states
        if(unsupportedStateRecords.length>0) notice += pSize + " > Cannot update <b>" + unsupportedStateRecords.length + " record(s)</b> with <b>state = unknown</b>. Kindly contact your data administrator for data curation.</p>";
        // Display notice for records with harvest status 'COMPLETED'
        if(completedRecords.length>0) notice += pSize + " > Cannot update <b>" + completedRecords.length + " record(s)</b> with <b>harvestStatus = COMPLETED</b>. Kindly <b>cancel</b> the transaction and ask your data administrator for assistance.</p>";
        // Display notice for records with harvest status 'DELETION_IN_PROGRESS'
        if(deletionRecords.length>0) notice += pSize + " > Cannot update <b>" + deletionRecords.length + " record(s)</b> with <b>harvestStatus = DELETION IN PROGRESS</b>.</p>";
        // Display notice for records where variables are not applicable to
        if(variableIncompatObject['recordIds'].length>0) notice += pSize + " > Cannot update <b>" + variableIncompatObject['recordIds'].length + " record(s)</b>. Variable(s) <b>" + variableIncompatObject['variables'].join(', ') + "</b> are not supported for the selected record(s).";
        // Display notice for records with harvest method and state incompatibility
        if(methodStateIncompatObject['recordIds'].length>0) notice += pSize + " > Harvest Method <b>" + harvestMethod + "</b> is not applicable to <b>" + methodStateIncompatObject['recordIds'].length + " record(s)</b> with state = <b>" + methodStateIncompatObject['states'].join(', ') + "</b>.</p>";
        // Display notice for records with harvest method and type incompatibility
        if(methodStateTypeIncompatObject['recordIds'].length>0) notice += pSize + " > Harvest Method <b>" + harvestMethod + "</b> is not applicable to <b>" + methodStateTypeIncompatObject['recordIds'].length + " record(s)</b> with type = <b>" + methodStateTypeIncompatObject['types'].join(', ') + "</b>.</p>";
        // Display notice for records with numeric variable and harvest method incompatibility
        if(numVarMethodIncompatObject['recordIds'].length>0) notice += pSize + " > Variable <b>" + variablePlaceholder[currentNumVar] + "</b> is not applicable to <b>" + numVarMethodIncompatObject['recordIds'].length + " record(s)</b> with method(s) = <b>" + numVarMethodIncompatObject['methods'].join(", ") + "</b>.</p>";
        // Display notice for records with numeric variable and cross method incompatibility
        if(numVarCrossMethodIncompatObject['recordIds'].length>0) notice += pSize + " > Variable <b>" + variablePlaceholder[currentNumVarCrossMethod] + "</b> is not applicable to <b>" + numVarCrossMethodIncompatObject['recordIds'].length + " record(s)</b> with cross method(s) = <b>" + numVarCrossMethodIncompatObject['crossMethods'].join(", ") + "</b>.</p>";
        // Display notice for records with harvest method and cross method incompatibility
        if(harvestMethodCrossMethodIncompatObject['recordIds'].length>0) notice += pSize + " > Harvest Method <b>" + currentHarvestMethod + "</b> is not applicable to <b>" + harvestMethodCrossMethodIncompatObject['recordIds'].length + " record(s)</b> with cross method(s) = <b>" + harvestMethodCrossMethodIncompatObject['crossMethods'].join(", ") + "</b>.</p>";
        // If some records can be updated, display message for continuing with the update operation
        if(totalUpdatableCount>0) notice += pSize + "If you wish to continue, click " + operationsAllowed.join(' or ') + ".</p>";
        // Display notice in the modal
        $('#hm-hd-bulk-update-confirmation-modal-additional-content').html(notice);
        $('#hm-hd-bulk-update-confirmation-modal-additional-main').show();
    }
}

/**
 * Displays info message when bulk update threshold has been exceeded
 */
function displayThresholdExceededMessage() {
    // Reset checkbox
    $('#hm-hd-bu-bg-overwrite-chk').prop('checked',false);
    $('#hm-hd-bu-bg-overwrite-chk').removeAttr('checked');
    backgroundOverwrite = false;

    // Build initial message
    var message = 'You are about to update <b>' + allRecordsCountFormatted + ' records</b>.' +
        ' The following harvest data will be applied to compatible records:';

    // Add selected data to the message
    for (var variableAbbrev of variableAbbrevArray) {
        var placeholder = variablePlaceholder[variableAbbrev];
        var varIndex = variableAbbrevArray.indexOf(variableAbbrev);
        var value = values[varIndex][0];

        message += '<br> • ' + placeholder + ' = <b>' + value + '</b>';
    }

    // Final additions to the message
    message += '<br><br>' + 'Any records incompatible with the selected data will be skipped.'
        +'<br>The process will be sent to a background worker. Do you wish to proceed?';

    // Display message in the confirmation modal
    $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;">' + message + '</p>');

    // Show and enable proceed button
    $('.hm-hd-bulk-update-proceed-bg-update').show();
    $('#hm-hd-bulk-update-overwrite-update-true-space').removeClass('hidden');

    // Show checkbox for overwriting existing data
    $('#hm-hd-bulk-update-proceed-bg-content').show();
}

/**
 * Remove records with harvest status 'COMPLETED' or 'DELETION_IN_PROGRESS' from the roster
 */
function eliminateCompletedOrDeletionRecords() {
    var rows = [];
    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var harvestStatus = row.harvestStatus;
        
        // If status is COMPLETED, add to array of completed records
        if(harvestStatus=='COMPLETED') {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!completedRecords.includes(id)) completedRecords.push(id);
        }

        // If status is DELETION_IN_PROGRESS, add to array of deletion records
        if(harvestStatus=='DELETION_IN_PROGRESS') {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!deletionRecords.includes(id)) deletionRecords.push(id);
        }
    }
}

/**
 * Catch TCV use case
 */
function catchTCVUseCase() {
    var rows = [];
    var disallow = true;
    if(varAbb.length == 1 && varAbb[0] == "HVDATE_CONT") {
        disallow = false;
    }

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var stage = row.stage;

        // If stage is test cross verification, disallow
        if(stage != null && stage == 'TCV' && disallow) {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!tcvRecords.includes(id)) tcvRecords.push(id);
        }
    }
}

/**
 * Take note of records with missing cross methods
 */
function checkMissingCrossMethod() {
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var crossMethodAbbrev = row.crossMethodAbbrev;

        // If cross method is not set, add to incompatible array
        if(crossMethodAbbrev == '') {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!missingCMethodRecords.includes(id)) missingCMethodRecords.push(id);
        }
    }
}

/**
 * Take note of records with unsupported state
 */
function checkUnsupportedState() {
    var supported = ['fixed', 'not_fixed'];
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var state = row.state;

        // If state is not supported, add record to unsupported state records array
        if(state != null && state != "" && !supported.includes(state)) {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!unsupportedStateRecords.includes(id)) unsupportedStateRecords.push(id);
        }
    }
}

/**
 * Take note of records that are incompatible with the variable(s) selected
 */
function checkVariableIncompatibility() {
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    // Loop through selected records
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var stage = row.stage
        // Get record info
        var crossMethodAbbrev = row.crossMethodAbbrev
        var state = row.state
        if (stage == 'TCV') state = stage
        var type = row.type
        // Get compat arrays
        var cmethCompat = variableCompat[crossMethodAbbrev] !== undefined ?
            variableCompat[crossMethodAbbrev] : (
                variableCompat['default'] !== undefined ?
                    variableCompat['default'] : []
            );
        var stateCompat = cmethCompat[state] !== undefined ?
            cmethCompat[state] : (
                cmethCompat['default'] !== undefined ?
                    cmethCompat['default'] : []
            );
        var typeCompat = stateCompat[type] !== undefined ?
            stateCompat[type] : (
                stateCompat['default'] !== undefined ?
                    stateCompat['default'] : []
            );

        // Check each abbrev selected
        for(var abbrev of varAbb) {
            var fieldName = variableFieldNames[abbrev];
            var placeholder = variablePlaceholder[abbrev];

            // If the type compatibility array does not include the field name
            // mark record as incompatible
            if(!typeCompat.includes(fieldName)) {
                if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
                if(!variableIncompatObject['recordIds'].includes(id)) variableIncompatObject['recordIds'].push(id);
                if(!variableIncompatObject['variables'].includes(placeholder)) variableIncompatObject['variables'].push(placeholder);
            }
        }
    }
}

/**
 * Take note of records that are incompatible with the harvest method due to the state
 */
function checkMethodStateIncompatibility() {
    var harvestMethodIncluded = varAbb.includes('HV_METH_DISC');
    var harvestMethod = vals[varAbb.indexOf('HV_METH_DISC')];
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;
    
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;

        /**
         * If stage is TCV, skip check.
         * (CORB-6631)
         */
        if(incompatibleRecords.includes(id) || unsupportedStateRecords.includes(id)
            || row.stage == 'TCV'
        ) {
            continue;
        }

        // If harvest method is part of the input, check state
        if(harvestMethodIncluded) {
            var state = row.state;
            if(stateHMethodCompat[state] === undefined) state = 'default';
            // If the state is fixed and the method is not Bulk, add to incompatibility object
            harvestMethod = Array.isArray(harvestMethod) ? harvestMethod[0] : harvestMethod
            if(!stateHMethodCompat[state].includes(harvestMethod)) {
                if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
                methodStateIncompatObject['recordIds'].push(id);
                if(!methodStateIncompatObject['states'].includes(state)) methodStateIncompatObject['states'].push(state);
            }
        }
    }
}

/**
 * Take not of records that are incompatible with the harvest method due to the type
 */
function checkMethodStateTypeIncompatibility() {
    var harvestMethodIncluded = varAbb.includes('HV_METH_DISC');
    var harvestMethod = vals[varAbb.indexOf('HV_METH_DISC')];
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;
    
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var stage = row.stage;

        if(incompatibleRecords.includes(id) || unsupportedStateRecords.includes(id)) continue;

        // If harvest method is part of the input, check state and type
        if(harvestMethodIncluded) {
            var state = row.state != undefined && row.state != "" ? row.state : 'default';
            /**
             * If stage is TCV, set state = stage
             * (CORB-6631)
             */
            if (stage == 'TCV') state = stage;
            var type = row.type != undefined && row.type != "" ? row.type : 'default';
            var allowedMethods =
                gStateGTypeHMetCompat[state][type] == undefined
                ?
                (
                    gStateGTypeHMetCompat[state]['default'] == undefined
                    ?
                    (
                        gStateGTypeHMetCompat['default']['default'] == undefined
                        ?
                        []
                        :
                        gStateGTypeHMetCompat['default']['default']
                    )
                    :
                    gStateGTypeHMetCompat[state]['default']
                )
                :
                gStateGTypeHMetCompat[state][type];

            harvestMethod = Array.isArray(harvestMethod) ? harvestMethod[0] : harvestMethod;

            // If method is not found in the allowed values, mark record as incompatible
            if(allowedMethods[harvestMethod] === undefined) {
                if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
                methodStateTypeIncompatObject['recordIds'].push(id);
                if(!methodStateTypeIncompatObject.types.includes(type)) methodStateTypeIncompatObject.types.push(type);
            }
        }
    }
}

/**
 * Take note of records that are incompatible with the numeric variable due to the harvest method
 */
function checkNumVarMethodIncompatibility() {
    var harvestMethodIncluded = varAbb.includes('HV_METH_DISC');
    var rows = [];

    if(harvestMethodIncluded || numVarAbbrev=='') return;

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    var compatibleMethods = numVarCompat[numVarAbbrev]['harvestMethods'];

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;

        if(incompatibleRecords.includes(id) || unsupportedStateRecords.includes(id)) continue;

        var method = (row.terminal.harvestMethod!='') ? (row.terminal.harvestMethod) : (row.committed.harvestMethod);

        // If the harvest method of the record is not compatible with the numeric variable,
        // then add to incompatibility object
        if(!compatibleMethods.includes(method)) {
            var mth = (method=='') ? ('[no_method]') : (method);
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!numVarMethodIncompatObject['methods'].includes(mth)) numVarMethodIncompatObject['methods'].push(mth);
            numVarMethodIncompatObject['recordIds'].push(id);
            numVarMethodIncompatObject['numvar'] = numVarAbbrev;
        }
    }
}

/**
 * Take note of records that are incompatible with the numeric variable due to the cross method
 */
function checkNumVarCrossMethodIncompatibility() {
    var rows = [];

    if(numVarAbbrev=='') return;

    var compatibleCrossMethods = numVarCompat[numVarAbbrev]['crossMethods'];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    var rowsLength = rows.length;
    for(var i=0; i<rowsLength; i++) {
        var row = rows[i];
        var id = row.id;

        if(incompatibleRecords.includes(id) || unsupportedStateRecords.includes(id)) continue;

        var crossMethodAbbrev = row.crossMethodAbbrev;

        // If the cross method of the record is not compatible with the numeric variable,
        // then add to incompatibility object
        if(!compatibleCrossMethods.includes(crossMethodAbbrev)) {
            // Get cross method name from abbrev
            var crossMethodName = crossMethodAbbrev.replace("CROSS_METHOD_","");
            crossMethodName = crossMethodName.replace("_"," ");
            var mth = crossMethodName.toLowerCase();
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!numVarCrossMethodIncompatObject['crossMethods'].includes(mth)) numVarCrossMethodIncompatObject['crossMethods'].push(mth);
            numVarCrossMethodIncompatObject['recordIds'].push(id);
            numVarCrossMethodIncompatObject['numvar'] = numVarAbbrev;
        }
    }
}

/**
 * Take note of records that are incompatible with the harvest method due to the cross method
 */
function checkHarvestMethodCrossMethodIncompatibility() {
    if(!varAbb.includes('HV_METH_DISC')) return;

    var selectedHarvestMethod = values[varAbb.indexOf('HV_METH_DISC')];
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    var rowsLength = rows.length
    for(var i=0; i<rowsLength; i++) {
        var row = rows[i];
        var id = row.id;

        if(incompatibleRecords.includes(id) || unsupportedStateRecords.includes(row[0])) continue;

        var crossMethodAbbrev = row.crossMethodAbbrev;
        var crossMetHarvMetCompatAbbrev = crossMetHarvMetCompat[crossMethodAbbrev];
        var compatibleHarvestMethods = {};

        if(crossMetHarvMetCompatAbbrev !== undefined){
            compatibleHarvestMethods = Object.values(crossMetHarvMetCompatAbbrev);
        }

        // Check if the harvest method is supported for the cross method
        var compatible = false;
        var compatibleHarvestMethodsLength = compatibleHarvestMethods.length;
        for(var j = 0; j < compatibleHarvestMethodsLength; j++) {
            if(selectedHarvestMethod==compatibleHarvestMethods[j]) {
                compatible = true;
                break;
            }
        }

        // If incompatible, then add to incompatibility object
        if(!compatible) {
            // Get cross method name from abbrev
            var crossMethodName = crossMethodAbbrev.replace("CROSS_METHOD_","");
            crossMethodName = crossMethodName.replace("_"," ");
            var mth = crossMethodName.toLowerCase();
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!harvestMethodCrossMethodIncompatObject['crossMethods'].includes(mth)) harvestMethodCrossMethodIncompatObject['crossMethods'].push(mth);
            harvestMethodCrossMethodIncompatObject['recordIds'].push(id);
            harvestMethodCrossMethodIncompatObject['harvestMethod'] = selectedHarvestMethod;
        }
    }
}

/**
 * Count the records to be overwritten based on the input
 */
function countRecordsToOverwrite() {
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;
    
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;

        if(incompatibleRecords.includes(id)) continue;

        var overwriteRow = false;
        for(var j=0; j<varAbb.length; j++) {
            var currentAbbrev = varAbb[j];
            var fieldName = variableFieldNames[currentAbbrev];
            var variableRowVal = (row.terminal[fieldName]!='') ? (row.terminal[fieldName]) : (row.committed[fieldName]);

            // If the value is not empty, set overwrite of record to true
            if(variableRowVal!='') {
                overwriteRow = true;
                break;
            }
        }

        // If overwriteRow is true, add record to the overwrite array
        if(overwriteRow) {
            if (!overwriteRecords.includes(id)) overwriteRecords.push(id);
        }
    }
}

/**
 * Take note of the data to be overwritten
 */
function checkDataToOverwrite() {
    var overwriteTraits = [];

    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    var varAbbLength = varAbb.length;
    var rowsLength = rows.length;
    for(var i = 0; i < varAbbLength; i++) {
        var currentAbbrev = varAbb[i];
        var fieldName = variableFieldNames[currentAbbrev];
        
        for(var j = 0; j < rowsLength; j++) {
            var row = rows[j];
            var id = row.id;

            if(incompatibleRecords.includes(id)) continue;

            var uncommitted = row.terminal[fieldName] == undefined ? '' : row.terminal[fieldName];
            var committed = row.committed[fieldName] == undefined ? '' : row.committed[fieldName];

            // If either of the committed or uncommitted values are not empty,
            // add the variable field name to the overwrite traits array
            if(uncommitted != '' || committed != '') {
                if(!overwriteTraits.includes(fieldName)) overwriteTraits.push(fieldName);
            }
        }
    }

    return overwriteTraits;
}

/**
 * Enable or disable buttons
 */
function adjustButtons() {
    // If variable abbev array is empty, disable buttons
    if(variableAbbrevArray.length == 0) {
        $('.hm-hd-bulk-update-btn').addClass("disabled");
    }
    // Else, enable buttons depending on the conditions below
    else if(!hasInvalidNumVar) {
        $('.hm-hd-bulk-update-btn').removeClass('disabled');
        // If selected record count is 0, disable bulk update selected btn
        if(selectedRecordsCount==0) {
            $('#hm-hd-bulk-update-selected-btn').addClass("disabled");
        }
    }
}

/**
 * Adjust abbrev array, value array, and buttons on input.
 * @param {string} value the value from the input field
 * @param {string} variableAbbrev the variable abbrev of the input
 * @param {integer} index the index of the variableAbbrev in the variableAbbrevArray
 */
function inputChange(value, variableAbbrev, index) {
    // If value is empty, remove the variableAbbrev from the variableAbbrevArray
    if(value=='') {
        if(index>-1) {                                  // If variableAbbrev is in variableArray, remove it
            variableAbbrevArray.splice(index,1);
            values.splice(index,1);
        }
        adjustButtons();
        return;
    }

    // If the variable abbrev is not yet included in the variable abbrev array,
    // then add it to the array, and then add the value to the values array
    if(index==-1) {
        variableAbbrevArray.push(variableAbbrev);
        values.push([value]);
    } else { // Else, add the value to the values array
        values[index] = [value];
    }

    adjustButtons();
}

/**
 * Formats numbers to comma-separated string per hundreds.
 * @param {string} num number to be formatted
 * @returns string number formatted with commas
 * Examples:
 * 4        -> 4
 * 100      -> 100
 * 1234     -> 1,234
 * 9876543  -> 9,876,543
 */
function numberWithCommas(num) {
    var pieces = num.toString().split(".");
    pieces[0] = pieces[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return pieces.join(".");
}

/**
 * Reset the browser and clear selection after bulk update
 */
function resetBrowser() {
    // Clear selection on grid reset
    var gridId = 'dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-table-con';
    var selectionStorageId = occurrenceId + "_" + gridId;
    clearSelectionHM(selectionStorageId);
    var pageurl = $('#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid li.active a').attr('href');
    $.pjax.reload(
        {
            container:'#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-pjax',
            url: pageurl,
            replace: false,
            type: 'POST',
            data: {
                forceReset:false,
                bulkOperation:true
            }
        }
    );
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} selectionStorageId unique string identifier holding the selected items
 * @param {boolean} all whether or not all checkbox selections are to be cleared
 */
function clearSelectionHM(selectionStorageId, all = false) {
    // If all flag is set to true, clear selection for all supported grids
    if (all) {
        // Loop through supported grids
        for(var i = 0; i < supportedCount; i++) {
            // Build selection identifier
            var currentSession = occurrenceId + "_" + supportedGrids[i];
            // Remove selection from storage
            sessionStorage.removeItem(currentSession);
        }
    }
    // If all flag is false, remove specified  selection id
    else sessionStorage.removeItem(selectionStorageId);
}

/**
 * Formats comma-separated integers. Removes duplicate integers,
 * excess commas, and sorts the integers in ascending order.
 * @param {string} value the comma-separated integer string
 * @returns {string} the formatted comma-separated integer
 */
function cleanCommaSepInt(value) {
    if(value == '' || value.length == 1) return value;
    // Replace 2 or more consecutive commas with just 1 comma
    value = value.replace(/,{2,}/g, ",");
    // If value starts with a comma, remove comma
    if(value.charAt( 0 ) === ',') value = value.slice(1);
    // If value ends with a comma, remove comma
    if(value.charAt( value.length - 1 ) === ',') value = value.slice(0, -1);
    // Remove duplicates
    var pieces = value.split(',');
    var finalPieces = [];
    $.each(pieces, function(idx, val){
        // Add to final pieces
        var integerVal = parseInt(val)
        if(integerVal > 0 && !finalPieces.includes(integerVal)) finalPieces.push(integerVal);
    });
    // Sort in ascending order
    finalPieces.sort( function(a,b) { return a - b; } );
    // Put value back together
    value = finalPieces.join(',')
    // Return final value conjoined with comma
    return value == '' ? '0' : value;
}

/**
 * Builds method-state compat array
 */
function buildStateHMethodCompat() {
    for(var state in gStateGTypeHMetCompat) {
        // if(state != 'TCV') {
        //     if(stateHMethodCompat[state] === undefined) stateHMethodCompat[state] = []
        //     var types = gStateGTypeHMetCompat[state];
        //     for(var type in types) {
        //         var methods = types[type];
        //         for(var method in methods) {
        //             if(!stateHMethodCompat[state].includes(method)) stateHMethodCompat[state].push(method); 
        //         }
        //     }
        // }

        if(stateHMethodCompat[state] === undefined) stateHMethodCompat[state] = []
        var types = gStateGTypeHMetCompat[state];
        for(var type in types) {
            var methods = types[type];
            for(var method in methods) {
                if(!stateHMethodCompat[state].includes(method)) stateHMethodCompat[state].push(method); 
            }
        }
    }
}

/**
 * Displays custom flash message
 * @param {string} status the status of the message (info, success, warning, error)
 * @param {string} iconClass icon of flash message
 * @param {string} message the message to display
 * @param {string} parentDivId id of the div where the flash message is to be inserted
 * @param {float} time time in seconds before the flash message is removed from the display. 7.5 seconds by default.
 */
function displayCustomFlashMessage(status, iconClass, message, parentDivId, time = 7.5) {
    // Add flash message to parent div
    $('#' + parentDivId).html(
        '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
            '<div class="card-panel">' +
                '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                message +
                '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
            '</div>' +
        '</div>'
    );

    // remove flash message after the specified time in seconds (7.5 seconds by default)
    setTimeout(
        function() 
        {
            $('#w25-' + status + '-0').fadeOut(250);
            setTimeout(
                function() 
                {
                    $('#' + parentDivId).html('');
                }
            , 500);
        }
    , 1000 * time);
}

JS;

$this->registerJs($script);

?>