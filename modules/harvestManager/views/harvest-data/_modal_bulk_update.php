<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Bulk update modal contents
 */
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\helpers\Html;
\kartik\select2\Select2Asset::register($this);

// Define Urls
$urlRetrieveAllRecordInfo = Url::to(['harvest-data/retrieve-all-record-info']);
$urlUpdateHarvestData = Url::to(['harvest-data/update']);
$urlUpdateRecordsStatus = Url::to(['harvest-data/update-records-status']);
$urlUpdateHarvestDataInBG = Url::to(['harvest-data/update-harvest-data-in-background']);
// Set variables
$recordCount = count($recordIds);
$recordIdsJson = json_encode($recordIds);
$bulkUpdateConfigJSON = json_encode($bulkUpdateConfig);
$recordInfoJSON = json_encode($recordInfo);
$harvestMethodValuesJSON = json_encode($harvestMethodValues);
$methodNumVarCompatJSON = json_encode($methodNumVarCompat);
$variableFieldNamesJSON = json_encode($variableFieldNames);
$variableDisplayNamesJSON = json_encode($variableDisplayNames);
$numVarAbbrevsJSON = json_encode($numVarAbbrevs);

// Set input elements
$inputFieldsSelect = "<label>Add harvest data field</label>" . Select2::widget([
    'name' => 'hm-bulk-update-input-fields-select2',
    'id' => 'hm-bulk-update-input-fields-select2',
    'class' => 'hm-bulk-update-input-fields-select2',
    'data' => $variableDisplayNames,
    'options' => [
        'placeholder' => 'Select harvest data to add',
        'multiple' => true
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
])
. Html::a(\Yii::t('app', 'Add Input Field'), '#',
[
    'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-update-add-harves-data-field-btn disabled',
    'id' => 'hm-hd-bulk-update-add-harves-data-field-btn',
    'style' => 'margin-top: 10px;',
    'title' => \Yii::t('app', 'Add Input Field')
]
);

?>

<?php
    // Display number of selected items
    $recordCount = count($recordIds);
    $countString = $recordCount == 0 ? 'No' : $recordCount;
    $itemType = '';
    if($dataLevel == 'plot') {
        if($recordCount != 1) $itemType = 'plots';
        else $itemType = $dataLevel;
    }
    else if($dataLevel == 'cross') {
        if($recordCount != 1) $itemType = 'crosses';
        else $itemType = $dataLevel;
    }
    $count = $countString." selected ".$itemType;
?>

<!-- Main modal content -->
<div class="hm-hd-bulk-update-modal-container">
    <div class="row">
        <div class="col s4">
            <!-- Display input for harvest data selection -->
            <?php
                echo $inputFieldsSelect;
            ?>
            <!-- Div for notice related to the bulk update process -->
            <div class="blue lighten-4 blue-text text-darken-2 hidden" id="hm-hd-bulk-update-notice-div" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; border-radius: 5px; margin-top: 10px;">
                
            </div>
        </div>
        <div class="panel panel-default col s8" style=" min-height:400px; margin-top:10px">
            <div class="collapsible-header active" style="margin-bottom:10px; font-size:14px">
                <div class="col-md-6">
                    <i class="material-icons">list</i> <?= \Yii::t('app',"Harvest Data") ?>
                </div>
                <!-- Display number of selected items -->
                <div class="col-md-6" style="padding: 0px 0px 0px 0px;">
                    <p class="float-right">
                        <?= \Yii::t('app',$count) ?>
                    </p>
                </div>
            </div>
            <div style="margin-left:20px">
                <!-- PLACEHOLDER - NONE SELECTED -->
                <div class='hm-hd-bu-input-div-placeholder col-md-12' id='hm-hd-bu-input-div-PLACEHOLDER_NONE' style='font-size:14px'>
                    <dl>
                        <dt title='HVDATE_CONT'>No harvest data selected yet.</dt>
                        <dd>
                            <em>
                                Please select at least one (1) harvest data to include in the bulk update.
                            </em>
                        </dd>
                    </dl>
                </div>
                <!-- INPUT FIELDS -->
                <?php
                    echo $inputElements["hdataElements"];
                    echo $inputElements["numVarElements"];
                ?>
            </div>
        </div>
    </div>
</div>

<?php

$script = <<< JS
// Set variables
var occurrenceId = $occurrenceId;
var locationId = $locationId;
var dataLevel = "$dataLevel";
var bulkUpdateConfig = $bulkUpdateConfigJSON;
var harvestDataUpdateThreshold = $harvestDataUpdateThreshold;
var hdutFormatted = numberWithCommas($harvestDataUpdateThreshold);
var harvestMethodValues = $harvestMethodValuesJSON;
var harvestMethodValuesInv = swap(harvestMethodValues);
var methodNumVarCompat = $methodNumVarCompatJSON;
var variableFieldNames = $variableFieldNamesJSON;
var variableDisplayNames = $variableDisplayNamesJSON;
var selectedRecords = $recordInfoJSON;
var allRecords = {};
var selectedRecordsCount = $recordCount;
var allRecordsCount = $allRecordsCount;
var selectedRecordsCountFormatted = numberWithCommas($recordCount);
var allRecordsCountFormatted = numberWithCommas($allRecordsCount);
var crossMetHarvMetCompat = bulkUpdateConfig['cmet_hmet_compat'];
var gStateGTypeHMetCompat = bulkUpdateConfig['gstate_gtype_hmet_compat'];
var variablePlaceholder = bulkUpdateConfig['variable_placeholder'];
var variableFPMapping = bulkUpdateConfig['variable_field_placeholder_mapping'];
var variableCompat = bulkUpdateConfig['variable_compat'];
var numVarCompat = bulkUpdateConfig['num_var_compat'];
var numVarAbbrevs = $numVarAbbrevsJSON;
var stateHMethodCompat = {};
var numVarCompatKeys = Object.keys(numVarCompat);
var kCount = $numVarCount;
var variableAbbrevArray = [];
var values = [];
var hasInvalidNumVar = false;
var backgroundOverwrite = false;
var displayedInputFields = [];

// Variables for validation
var varAbb = [];
var vals = [];
var numVarAbbrev = '';
var completedRecords = [];
var deletionRecords = [];
var incompatibleRecords = [];
var overwriteRecords = [];
var unsupportedStateRecords = [];
var tcvRecords = [];
var variableIncompatObject = { variables: {}, recordIds: [] };
var harvestMethodIncompatObject = { method: "", recordIds: [] };
var numVarMethodIncompatObject = { numvar: "", methods: {} , recordIds: [] };
var mode = '';

// Adjust button labels based on item count
$("#hm-hd-bulk-update-selected-btn").html('Apply to selected ('+selectedRecordsCountFormatted+')');
$("#hm-hd-bulk-update-all-btn").html('Apply to all ('+allRecordsCountFormatted+')');
// Reset modal
$('#hm-hd-bulk-update-modal-body').show();
$('#update-progress').html('');
buildStateHMethodCompat();

$(document).ready(function() {

    // NEW EVENT BINDS (START) ============================= 

    // Add harvest data field select2 change event
    $(document).on('change', '#hm-bulk-update-input-fields-select2', function() {
        var inputFieldsToShow = $(this).val();

        // If value is not empty, enable the add button
        if(inputFieldsToShow.length > 0) {
            $('#hm-hd-bulk-update-add-harves-data-field-btn').removeClass('disabled');
        }
        else {
            $('#hm-hd-bulk-update-add-harves-data-field-btn').addClass('disabled');
        }
    });

    // Add harvest data field button click event
    $(document).on('click', '#hm-hd-bulk-update-add-harves-data-field-btn', function() {
        var inputFieldsToShow = $('#hm-bulk-update-input-fields-select2').val();
        var skipNumericVar = false;

        // Hide placeholder
        $('#hm-hd-bu-input-div-PLACEHOLDER_NONE').addClass('hidden');

        for (var inputField of inputFieldsToShow) {
            // If input is a numeric var and flag is set to skip, skip the input
            if (skipNumericVar && numVarAbbrevs.includes(inputField)) {
                continue;
            }

            // Show the element
            showHideElement(inputField, true, false, true);

            // If harvest method is included,
            // do some logic on the numeric variables
            if (inputField == 'HV_METH_DISC') {
                skipNumericVar = true;
                // Loop through all numeric variables
                for (var numVarAbbrev of numVarAbbrevs) {
                    // Hide numeric variable input
                    showHideElement(numVarAbbrev, false, false, false);
                }

                // Disable remove buttons for numeric variables
                $('.hm-hd-bu-num-var-remove').addClass('disabled');

                // Add toast message informing user that numeric variable fields will automatically be shown based on the selected harvest method
                var message = "Numeric variable fields will be shown based on the selected harvest method."
                var notif = "<i class='material-icons left blue-text'>info</i> " + message;
                Materialize.toast(notif, 4000);
                // Add notif to notice div
                $('#hm-hd-bulk-update-notice-div').html('<p>ⓘ ' + message + '</p>');
                $('#hm-hd-bulk-update-notice-div').removeClass('hidden');
            }
        }

        // Clear the select2
        $('#hm-bulk-update-input-fields-select2').val(null).trigger('change');
    });

    // Add click event for remove field button (class = remove-hdata-input)
    $(document).on('click', '.remove-hdata-input', function() {
        // The variable abbrev is in the ID, after 'remove-hdata-input-'
        let variableAbbrev = $(this).attr('id').replace('remove-hdata-input-', '');

        // Hide the element
        showHideElement(variableAbbrev, false, true, true); 

        // If displayedInputFields is empty, show placeholder
        if(displayedInputFields.length == 0) {
            $('#hm-hd-bu-input-div-PLACEHOLDER_NONE').removeClass('hidden');
        }

        // If harvest method is removed, hide all numeric variables
        // and enable the selection and remove buttons
        if (variableAbbrev == 'HV_METH_DISC') {
            for (var numVarAbbrev of numVarAbbrevs) {
                showHideElement(numVarAbbrev, false, true, true);
            }

            // Remove notif from notice div
            $('#hm-hd-bulk-update-notice-div').html('');
            $('#hm-hd-bulk-update-notice-div').addClass('hidden');
        }

        // If the removed element is numeric variable,
        // check if there are numeric variables in the displayed input fields
        // that are hidden. If there are, show them
        if (numVarAbbrevs.includes(variableAbbrev)) {
            for (var numVarAbbrev of numVarAbbrevs) {
                if (displayedInputFields.includes(numVarAbbrev)) {
                    showHideElement(numVarAbbrev, true, null, null);
                }
            }
        }
    });

    // When focus is lost on the numeric variable input,
    // show or hide other numeric  variables.
    $(".hm-hd-bu-num-var-input").focusout(function() {
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');

        // If the value is not empty, hide other numeric variables that are shown
        if (value != '') {
            for (var nvAbb of numVarAbbrevs) {
                // If numVarAbbrev is in the displayed input fields, hide it,
                // but do not remove it from the array
                if (displayedInputFields.includes(nvAbb) && nvAbb != variableAbbrev) {
                    showHideElement(nvAbb, false, null, null, false);
                }
            }
        }
        else {
            // If the value is empty, show all numeric variable inputs
            // that are in the displayed input fields
            for (var nvAbb of numVarAbbrevs) {
                if (displayedInputFields.includes(nvAbb)) {
                    showHideElement(nvAbb, true, null, null, false);
                }
            }
        }
    });

    // Numeric variable input event
    $(document).on('input', '.hm-hd-bu-num-var-input', function() {
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        // Update the numVarAbbrev variable based on the input value
        if(value == '') {
            numVarAbbrev = '';
        }
        else {
            numVarAbbrev = variableAbbrev;
        }

        // Validate input
        var subType = $(this).attr('subType');
        var minimum = $(this).attr('min');
        var placeholder = $(this).attr('placeholder');
        var regex = new RegExp('');
        // Format value based on sub type
        if(subType == 'single_int') {
            regex = new RegExp('^(' + minimum + '|[1-9][0-9]*)$');
            value = value != '' ? parseInt(value).toString() : '';
        }
        else if(subType == 'comma_sep_int') {
            value = cleanCommaSepInt(value);
            regex = new RegExp('^[1-9][0-9]*(,[1-9][0-9]*)*$');
        }
        // If input does not match regex, display error
        if(value != '' && !regex.test(value)) {
            $(this).css({"background-color": "#ffffcc"});
            hasInvalidNumVar = true;
            inputChange('', variableAbbrev, index);
            $('.hm-hd-bulk-update-btn').addClass("disabled");
            return;
        }

        // Remove background color
        $(this).css({"background-color": ""});
        hasInvalidNumVar = false;
        inputChange(value, variableAbbrev, index);
    });

    // Bind change event for general inputs (non-numeric variables)
    $(document).on('change', ".hm-hd-bu-input", function() {
        // Get value and abbrev
        if(!$(this).hasClass("hm-hd-bu-num-var-input")) {
            var value = $(this).val();
            var variableAbbrev = $(this).attr('variableAbbrev');
            var index = variableAbbrevArray.indexOf(variableAbbrev);

            inputChange(value, variableAbbrev, index);
        }
    });

    // Bind change event for harvest method input
    // Hide or show numeric variables based on compatibility
    $(document).on('change', "#hm-hd-bu-input-HV_METH_DISC", function() {
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var methodAbbrev = harvestMethodValuesInv[value];
        var supportedNumVars = methodNumVarCompat[methodAbbrev];

        // If the value is empty, enable all numeric variable inputs
        // in the selection, and also enable their respective remove buttons
        if (value == '') {
            for (var numVarAbbrev of numVarAbbrevs) {
                // Hide element but enable it in selection,
                // and enable its remove button
                showHideElement(numVarAbbrev, false, true, true);
            }
        }
        else {
            for (var numVarAbbrev of numVarAbbrevs) {
                // If the numeric variable is supported by the selected harvest method,
                // show the input field
                if (supportedNumVars.includes(numVarAbbrev)) {
                    showHideElement(numVarAbbrev, true, false, false);
                }
                // Else, hide the input field
                else {
                    showHideElement(numVarAbbrev, false, false, false);
                }
            }
        }
    });

    // NEW EVENT BINDS (WIP END) =============================

    // Harvest date datepicker change event
    $(document).on('change', "#hm-bulk-update-input-hdate", function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        inputChange(value, variableAbbrev, index);
    });

    // Harvest method select2 change event
    $(document).on('change', "#hm-bulk-update-input-hmethod", function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        inputChange(value, variableAbbrev, index);

        // If the input value for harvest method is empty, show all numeric variable inputs
        if(value == '') {
            // If the number of numeric variables is more than 1, clear values for all numeric variables
            if(kCount > 1) $('.hm-bulk-update-input-numvar').val('').trigger('change');
            $('.hm-bulk-update-input-numvar').css({"background-color": ""});
            $('.hm-bulk-update-input-numvar-div').show();
            return;
        }
        // Hide all numeric variable inputs
        $('.hm-bulk-update-input-numvar-div').hide();
        // Check which numeric variable input to show (if applicable)
        for(var i = 0; i < kCount; i++) {
            var abbrev = numVarCompatKeys[i];
            var abbrevIndex = variableAbbrevArray.indexOf(abbrev);
            var compatObject = numVarCompat[abbrev];
            var allowedHarvestMethods = compatObject.harvestMethods;
            // Set element ids based on abbrev
            var inputId = '#hm-bulk-update-input-'+abbrev;
            var divId = inputId+'-div';

            // If the input value for harvest method
            // is included in the allowed harvest methods for the numeric variable,
            // then display the input field for that numeric variable
            if(allowedHarvestMethods.includes(value)) {
                $(divId).show();
            }
            // Else, clear any inputs for all numeric variables
            else {
                hasInvalidNumVar = false;
                numVarAbbrev = '';
                $(inputId).val('').trigger('change');
                $(inputId).css({"background-color": ""});
                inputChange("", abbrev, abbrevIndex);
            }
        }
    });

    // Numeric variable input event
    $(document).on('input', '.hm-bulk-update-input-numvar', function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        // If value is empty, show all numeric variable inputs
        if(value == '') {
            var hMethod = $("#hm-bulk-update-input-hmethod").val();
            if(hMethod == "") $('.hm-bulk-update-input-numvar-div').show();
            numVarAbbrev = '';
        }
        else {
            // Hide other numeric variable issues
            $('.hm-bulk-update-input-numvar-div').hide();
            $('#hm-bulk-update-input-' + variableAbbrev + '-div').show();
            numVarAbbrev = variableAbbrev;
        }

        // Validate input
        var subType = $(this).attr('subType');
        var minimum = $(this).attr('min');
        var placeholder = $(this).attr('placeholder');
        var regex = new RegExp('');
        // Format value based on sub type
        if(subType == 'single_int') {
            regex = new RegExp('^(' + minimum + '|[1-9][0-9]*)$');
            value = value != '' ? parseInt(value).toString() : '';
        }
        else if(subType == 'comma_sep_int') {
            value = cleanCommaSepInt(value);
            regex = new RegExp('^[1-9][0-9]*(,[1-9][0-9]*)*$');
        }
        // If input does not match regex, display error
        if(value != '' && !regex.test(value)) {
            $(this).css({"background-color": "#ffffcc"});
            hasInvalidNumVar = true;
            inputChange('', variableAbbrev, index);
            $('.hm-hd-bulk-update-btn').addClass("disabled");
            return;
        }
        // Remove background color
        $(this).css({"background-color": ""});
        hasInvalidNumVar = false;
        inputChange(value, variableAbbrev, index);
    });

    // Grain color select2 change event
    $(document).on('change', "#hm-bulk-update-input-graincolor", function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        inputChange(value, variableAbbrev, index);
    });

    // After focus event for numvar inputs
    $(".hm-bulk-update-input-numvar").focusout(function(){
        var value = $(this).val();
        // Validate input
        var subType = $(this).attr('subType');
        var minimum = $(this).attr('min');
        var placeholder = $(this).attr('placeholder');
        var regex = new RegExp('');
        // Format value based on sub type
        if(subType == 'single_int') {
            regex = new RegExp('^(' + minimum + '|[1-9][0-9]*)$');
            value = value != '' ? parseInt(value).toString() : '';
        }
        else if(subType == 'comma_sep_int') {
            value = cleanCommaSepInt(value);
            regex = new RegExp('^[1-9][0-9]*(,[1-9][0-9]*)*$');
        }
        // Triger change to update value in display
        $(this).val(value).trigger('change');
        // If input does not match regex, display toast
        if(value != '' && !regex.test(value)) {
            var notif = "<i class='material-icons orange-text left'>warning</i> Invalid input for " + placeholder;
            Materialize.toast(notif, 2000);
            return;
        }
    });

    // Bulk update selected btn click event
    $('#hm-hd-bulk-update-selected-btn').off('click').on('click', function() { 
        completedRecords = [];
        deletionRecords = [];
        incompatibleRecords = [];
        overwriteRecords = [];
        unsupportedStateRecords = [];
        variableIncompatObject = { variables: {}, recordIds: [] };
        harvestMethodIncompatObject = { method: "", recordIds: [] };
        numVarMethodIncompatObject = { numvar: "", methods: {} , recordIds: [] };
        mode = 'selected';

        confirmModalBuildContent(selectedRecordsCount);
    });

    // Bulk update all btn click event
    $('#hm-hd-bulk-update-all-btn').off('click').on('click', function() { 
        completedRecords = [];
        deletionRecords = [];
        incompatibleRecords = [];
        overwriteRecords = [];
        unsupportedStateRecords = [];
        variableIncompatObject = { variables: {}, recordIds: [] };
        harvestMethodIncompatObject = { method: "", recordIds: [] };
        numVarMethodIncompatObject = { numvar: "", methods: {} , recordIds: [] };
        mode = 'all';

        // Reset confirmation modal
        $('.hm-hd-bulk-update-overwrite-update-true').hide();
        $('.hm-hd-bulk-update-overwrite-update-false').hide();
        $('.hm-hd-bulk-update-proceed-bg-update').hide();
        $('#hm-hd-bulk-update-proceed-bg-content').hide();
        $('#hm-hd-bulk-update-confirmation-modal-additional-main').hide();

        // If number of all records exceeds the limit, display notice
        if(allRecordsCount > harvestDataUpdateThreshold) {
            displayThresholdExceededMessage();
            return;
        }
        // Retrieve all records (formatted), then build confirmation modal contents
        $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>Loading info of '+allRecordsCount+' records...</i></p><div class="progress"><div class="indeterminate"></div></div>');
        $.ajax({
            url: '$urlRetrieveAllRecordInfo',
            data: {
                occurrenceId: occurrenceId,
                dataLevel: dataLevel
            },
            type: 'POST',
            async: true,
            success: function(data) {
                $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>Info of '+allRecordsCount+' records retrieved. Checking info...</i></p><div class="progress"><div class="indeterminate"></div></div>');
                allRecords = jQuery.parseJSON(data);
                confirmModalBuildContent(allRecordsCount)
            },
            error: function() {
                $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>There was a problem while loading record information.</i></p>');
            }
        });
    });

    // Bulk update confirmation button click event
    $('.hm-hd-bulk-update-confirm-btn-allow').off('click').on('click', function() {
        $('.hm-hd-bulk-update-btn').addClass("disabled");
        var id = $(this).prop('id');
        var updateRecords = [];
        if(mode=='selected') updateRecords = selectedRecords;
        else if(mode=='all') updateRecords = allRecords;

        var tempSelected = [];
        var removeRecords = incompatibleRecords;
        // If the skip button was clicked, add records to overwrite
        // to the array of records to be removed from the roster
        if(id.includes('skip')) {
            removeRecords = incompatibleRecords.concat(overwriteRecords);
        }

        // Collate all records to be updated
        for (var i = 0; i<updateRecords.length; i++) {
            if(!removeRecords.includes(updateRecords[i][dataLevel+'DbId']))
                tempSelected.push(updateRecords[i]);
        }
        
        updateRecords = tempSelected;

        // Perform bulk update
        performBulkUpdate(updateRecords);
    });

    // Overwrite data check
    $('#hm-hd-bu-bg-overwrite-chk').on('input', function (e) {
        var checked = $(this).prop("checked");
        if (checked) backgroundOverwrite = true;
        else backgroundOverwrite = false;
    });

    // Update harvest data in background
    $('#hm-hd-bulk-update-proceed-bg-btn').off('click').on('click', function() {
        // Add progress bar
        $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>Preparing bulk update of '+allRecordsCount+' records...</i></p><div class="progress"><div class="indeterminate"></div></div>');
        $('#hm-hd-bulk-update-proceed-bg-content').hide();

        // Build POST data
        var bgData = {
            locationId: $locationId,
            occurrenceId: $occurrenceId,
            dataLevel: dataLevel,
            overwrite: backgroundOverwrite,
            variableAbbrevs: JSON.stringify(variableAbbrevArray),
            values: JSON.stringify(values),
            bulkUpdateConfig: JSON.stringify(bulkUpdateConfig),
            others: JSON.stringify({
                numVarAbbrev: numVarAbbrev
            })
        };

        // Trigger background worker
        $.ajax({
            url: '$urlUpdateHarvestDataInBG',
            data: bgData,
            type: 'POST',
            async: true,
            success: function(data) {
                var result = JSON.parse(data);
                var success = result.success;
                var status = result.status;
                
                // Display flash message
                if(success) {
                    // Success message
                    displayCustomFlashMessage(
                        'info', 
                        'fa fa-info-circle',
                        'The harvest data is being updated in the background. You may proceed with other tasks. You will be notified in this page once done.',
                        'hm-bg-alert-div',
                        10
                    );
                }
                else {
                    // Failure message
                    let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status != 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if(status == null) message += ' (#002)';

                    displayCustomFlashMessage(
                        'warning', 
                        'fa-fw fa fa-warning', 
                        message,
                        'hm-bg-alert-div',
                        60
                    );
                }

                // Close the modal
                $("#hm-hd-bulk-update-confirmation-modal").modal('hide');
                // Reset the browser
                resetBrowser();
                return;
            },
            error: function() {
                $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;"><i>There was a problem while loading record information.</i></p>');
            }
        });
    });
});

/**
 * Builds the data array for bulk update, 
 * and sends the data to the data collection
 */
function performBulkUpdate(selected) {
    // Display progress bar
    $('#hm-hd-bulk-update-modal-body').hide();
    $('#update-progress').html('<p style="font-size:115%;"><i>Updating...</i></p><div class="progress"><div class="indeterminate"></div></div>');

    // Build record ids array
    var recordIds = [];
    for(var i = 0; i < selected.length; i++) {
        recordIds.push(selected[i][dataLevel+'DbId']);
    }

    // Build POST data
    var data = {
        variableAbbrevs: JSON.stringify(varAbb),
        recordIds: JSON.stringify(recordIds),
        locationId: $locationId,
        values: JSON.stringify(vals),
        idType: dataLevel
    };

    // Send data
    $.ajax({
        url: '$urlUpdateHarvestData',
        data: data,
        type: 'POST',
        async: true,
        success: function(data) {
            $('#update-progress').html('');
            $('#hm-hd-bulk-update-modal-body').show();
            var notif = "<i class='material-icons green-text'>check</i>&nbsp;Harvest data was successfully updated";
            Materialize.toast(notif, 2500);
            $("#hm-hd-bulk-update-modal.in").modal('hide');
            resetBrowser();
        },
        error: function() {
            $('#update-progress').html('');
            $('#hm-hd-bulk-update-modal-body').show();
            var notif = "<i class='material-icons red-text'>close</i>&nbsp;Bulk update was unsuccessful.";
            Materialize.toast(notif, 2500);
            $("#hm-hd-bulk-update-modal.in").modal('hide');
        }
    });
}

/**
 * Builds the contents of the bulk update confirmation modal
 */
function confirmModalBuildContent(totalCount) {
    varAbb = variableAbbrevArray;
    vals = values;
    var abbrevCount = varAbb.length;

    $('.hm-hd-bulk-update-overwrite-update-true').hide();
    $('.hm-hd-bulk-update-overwrite-update-false').hide();
    $('.hm-hd-bulk-update-proceed-bg-update').hide();
    $('#hm-hd-bulk-update-proceed-bg-content').hide();

    // VALIDATION START
    // Remove records with status 'COMPLETED' or 'DELETION_IN_PROGRESS' from the roster
    eliminateCompletedOrDeletionRecords();
    // Take note of records with an unsupported state
    checkUnsupportedState();
    // Take note of records that are incompatible with the variable(s) selected
    checkVariableIncompatibility();
    // Take note of records that are incomplatible with the harvest method selected
    checkHarvestMethodIncompatibility();
    // Take note of records that are incompatible with the numeric variable due to the harvest method
    checkNumVarMethodIncompatibility();
    // Take note of records and traits to be overwritten with the new data
    var overwriteTraitList = countRecordsToOverwrite();

    // Set variables
    var harvestMethod = vals[varAbb.indexOf('HV_METH_DISC')];
    var currentNumVar = numVarMethodIncompatObject['numvar'];
    var operationsAllowed = [];
    var pSize = '<p>';
    totalUpdatableCount = totalCount - incompatibleRecords.length;

    // If there are traits to be overwritten,
    // display a notice in the confirmation modal
    if(overwriteTraitList.length > 0 && overwriteRecords.length > 0) {
        var variablesStr = "";
        // Build the variable string to display
        for(var i = 0; i < overwriteTraitList.length; i++) {
            if (i > 0 && i < overwriteTraitList.length-1) variablesStr += ', ';
            else if (i > 0 && i == overwriteTraitList.length-1) variablesStr += ' and ';
            var varName = variableDisplayNames[overwriteTraitList[i]];
            variablesStr += '<b>' + varName + '</b>';
        }
        // Build notice string
        var notice = "<br><p style='font-size:115%;'>You are updating the harvest data of <b>" + totalUpdatableCount + " record(s)</b>."
            + "<br><br><b>"+overwriteRecords.length+" record(s)</b> have existing values for the variables: "+ variablesStr 
            + "<br><br>Do you wish to overwrite these records?</p>";
        // Display the necessary buttons
        $('.hm-hd-bulk-update-overwrite-update-true').show();
        $('.hm-hd-bulk-update-overwrite-update-false').hide();
        // Add 'overwrite' to the allowed operations
        operationsAllowed.push('overwrite');
        // If all records for update are to be overwritten, hide the 'Skip' button
        if(totalUpdatableCount == overwriteRecords.length) {
            $('#hm-hd-bulk-update-skip-overwrite-btn').hide();
            $('#hm-hd-bulk-update-overwrite-update-true-space').addClass('hidden');
        }
        // If some records for update are to be overwritten, show the 'Skip' button
        else if(totalUpdatableCount > overwriteRecords.length) {
            // Add skip to the allowed operations
            operationsAllowed.push('skip');
            $('#hm-hd-bulk-update-skip-overwrite-btn').show();
            $('#hm-hd-bulk-update-overwrite-update-true-space').removeClass('hidden');
        }
    }
    // Else, display short summary and confirmatory prompt
    else {
        // Build notice string
        var notice = "<br><p style='font-size:115%;'>You are updating the harvest data of <b>" + totalUpdatableCount + " record(s)</b>."
            + "<br><br>Are you sure you want to apply these changes?</p>";
        // Display the necessary buttons
        $('.hm-hd-bulk-update-overwrite-update-true').hide();
        $('.hm-hd-bulk-update-overwrite-update-false').show();
        $('#hm-hd-bulk-update-overwrite-update-true-space').removeClass('hidden');
        // Add apply changes to allowed operations
        operationsAllowed.push('apply changes');
    }
    
    // If no records can be updated, display red notice in the modal
    if(totalUpdatableCount==0) {
        notice = '';
        pSize = "<p style='font-size:115%;'>";
        // Hide all buttons
        $('.hm-hd-bulk-update-overwrite-update-true').hide();
        $('.hm-hd-bulk-update-overwrite-update-false').hide();
    }
    // Display the notice in the modal body
    $('#hm-hd-bulk-update-confirmation-modal-body').html(notice);
    $('#hm-hd-bulk-update-confirmation-modal-additional-main').hide();
    // If incompatibilities are found, build summary of incompatibilities
    if(incompatibleRecords.length>0) {
        // Display notice if all records cannot be updated
        if(totalUpdatableCount==0) {
            notice = pSize + "Cannot perform bulk update for <b>" + incompatibleRecords.length + " record(s)</b>.</p>";
            $('#hm-hd-bulk-update-zero-update').show();     // Show spacing after button
        }
        else {
            notice = "";
            $('#hm-hd-bulk-update-zero-update').hide();     // Remove spacing after button
        }
        // Display notice for records with unsupported states
        if(unsupportedStateRecords.length>0) notice += pSize + " • Cannot update <b>" + unsupportedStateRecords.length + " record(s)</b> with <b>state = unknown</b>. Kindly contact your data administrator for data curation.</p>";
        // Display notice for records with harvest status 'COMPLETED'
        if(completedRecords.length>0) notice += pSize + " • Cannot update <b>" + completedRecords.length + " record(s)</b> with <b>harvest status = COMPLETED</b>. Kindly <b>cancel</b> the transaction and ask your data administrator for assistance.</p>";
        // Display notice for records with harvest status 'DELETION_IN_PROGRESS'
        if(deletionRecords.length>0) notice += pSize + " • Cannot update <b>" + deletionRecords.length + " record(s)</b> with <b>harvest status = DELETION IN PROGRESS</b>.</p>";
        // Display notice for records where variables are not applicable to
        if(variableIncompatObject['recordIds'].length>0) {
            var varNames = [];
            for (var abb of Object.keys(variableIncompatObject['variables'])) {
                varNames.push(variableDisplayNames[abb]);
            }
            notice += pSize + " • Cannot update <b>" + variableIncompatObject['recordIds'].length + " record(s)</b>. Variable(s) <b>" + varNames.join(', ') + "</b> are not supported for the selected record(s).";
        }
        // Display notice for records with harvest method incompatibility
        if(harvestMethodIncompatObject['recordIds'].length>0) {
            notice += pSize + " • Cannot update <b>" + harvestMethodIncompatObject['recordIds'].length + " record(s)</b> The selected harvest method, <b>" + harvestMethodValues[harvestMethodIncompatObject.method] + "</b>, is not applicable to these records.</p>";
        }
        // Display notice for records with numeric variable and harvest method incompatibility
        if(numVarMethodIncompatObject['recordIds'].length>0) {
            var methodNames = [];
            for (var abb of Object.keys(numVarMethodIncompatObject['methods'])) {
                var methodName = harvestMethodValues[abb]
                if (methodName == "" || methodName == null) methodName = "[no method set]";
                methodNames.push(methodName);
            }
            notice += pSize + " • Variable <b>" + variableDisplayNames[currentNumVar] + "</b> is not applicable to <b>" + numVarMethodIncompatObject['recordIds'].length + " record(s)</b> with method(s) = <b>" + methodNames.join(", ") + "</b>.</p>";
        }
        // If some records can be updated, display message for continuing with the update operation
        if(totalUpdatableCount>0) notice += pSize + "If you wish to continue, click " + operationsAllowed.join(' or ') + ".</p>";
        // Display notice in the modal
        $('#hm-hd-bulk-update-confirmation-modal-additional-content').html(notice);
        $('#hm-hd-bulk-update-confirmation-modal-additional-main').show();
    }
}

/**
 * Displays info message when bulk update threshold has been exceeded
 */
function displayThresholdExceededMessage() {
    // Reset checkbox
    $('#hm-hd-bu-bg-overwrite-chk').prop('checked',false);
    $('#hm-hd-bu-bg-overwrite-chk').removeAttr('checked');
    backgroundOverwrite = false;

    // Build initial message
    var message = 'You are about to update <b>' + allRecordsCountFormatted + ' records</b>.' +
        ' The following harvest data will be applied to compatible records:';

    // Add selected data to the message
    for (var variableAbbrev of variableAbbrevArray) {
        var placeholder = variablePlaceholder[variableAbbrev];
        var varIndex = variableAbbrevArray.indexOf(variableAbbrev);
        var value = values[varIndex][0];

        message += '<br> • ' + placeholder + ' = <b>' + value + '</b>';
    }

    // Final additions to the message
    message += '<br><br>' + 'Any records incompatible with the selected data will be skipped.'
        +'<br>The process will be sent to a background worker. Do you wish to proceed?';

    // Display message in the confirmation modal
    $('#hm-hd-bulk-update-confirmation-modal-body').html('<p style="font-size:115%;">' + message + '</p>');

    // Show and enable proceed button
    $('.hm-hd-bulk-update-proceed-bg-update').show();
    $('#hm-hd-bulk-update-overwrite-update-true-space').removeClass('hidden');

    // Show checkbox for overwriting existing data
    $('#hm-hd-bulk-update-proceed-bg-content').show();
}

/**
 * Remove records with harvest status 'COMPLETED' or 'DELETION_IN_PROGRESS' from the roster
 */
function eliminateCompletedOrDeletionRecords() {
    var rows = [];
    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row[dataLevel+"DbId"];
        var harvestStatus = row.harvestStatus;
        
        // If status is COMPLETED, add to array of completed records
        if(harvestStatus=='COMPLETED') {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!completedRecords.includes(id)) completedRecords.push(id);
        }

        // If status is DELETION_IN_PROGRESS, add to array of deletion records
        if(harvestStatus=='DELETION_IN_PROGRESS') {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!deletionRecords.includes(id)) deletionRecords.push(id);
        }
    }
}

/**
 * Take note of records with unsupported state
 */
function checkUnsupportedState() {
    var supported = ['fixed', 'not_fixed'];
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row[dataLevel+"DbId"];
        var state = row.state;

        // Skip row if marked incompatible from previous validation criteria
        if(incompatibleRecords.includes(id)) continue;

        // If state is not supported, add record to unsupported state records array
        if(state != null && state != "" && !supported.includes(state)) {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!unsupportedStateRecords.includes(id)) unsupportedStateRecords.push(id);
        }
    }
}

/**
 * Take note of records that are incompatible with the variable(s) selected
 */
function checkVariableIncompatibility() {
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    // Loop through selected records
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row[dataLevel+"DbId"];

        // Skip row if marked incompatible from previous validation criteria
        if(incompatibleRecords.includes(id)) continue;

        // Get supported harvest data for this row
        var harvestDataConfig = row.harvestDataConfig ?? {};
        var config = harvestDataConfig.config ?? {};
        var harvestData = config.harvestData ?? [];
        var harvestMethods = config.harvestMethods ?? {};
        var numVarHarvestMethodCompat = config.numVarHarvestMethodCompat ?? {};

        // Check harvest data
        let harvestDataAbbrevArray = harvestData.map(a => a.variableAbbrev);
        
        // Check harvest methods
        let harvestMethodsArray = Object.keys(harvestMethods);
        if(harvestMethodsArray.length > 0) harvestDataAbbrevArray.push('HV_METH_DISC');

        // Check numeric variables
        let numVarsArray = Object.keys(numVarHarvestMethodCompat);
        harvestDataAbbrevArray = harvestDataAbbrevArray.concat(numVarsArray);

        // Loop through selected variables
        for(var abbrev of varAbb) {
            // If the variable is not included, add id to incompatible records id array
            if(!harvestDataAbbrevArray.includes(abbrev)) {
                if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
                if(!variableIncompatObject.recordIds.includes(id)) variableIncompatObject.recordIds.push(id);

                // Initialize variable array
                if(variableIncompatObject.variables[abbrev] == null) variableIncompatObject.variables[abbrev] = []
                if(!variableIncompatObject.variables[abbrev].includes(id)) variableIncompatObject.variables[abbrev].push(id);

            }
        }
    }
}

/**
 * Take note of records that are incompatible with the harvest method selected
 */
function checkHarvestMethodIncompatibility() {
    var harvestMethodIncluded = varAbb.includes('HV_METH_DISC');
    var rows = [];
    var selectedMethod = values[variableAbbrevArray.indexOf('HV_METH_DISC')] ?? "";
    var selectedMethodAbbrev = harvestMethodValuesInv[selectedMethod]

    // If harvest method is not included in the bulk update, skip check
    if(!harvestMethodIncluded) return;

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    // Loop through selected records
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row[dataLevel+"DbId"];

        // Skip row if marked incompatible from previous validation criteria
        if(incompatibleRecords.includes(id)) continue;

        var harvestDataConfig = row.harvestDataConfig ?? {};
        var config = harvestDataConfig.config ?? {};
        var harvestMethods = config.harvestMethods ?? {};
        var supportedHarvestMethods = Object.keys(harvestMethods);

        if(!supportedHarvestMethods.includes(selectedMethodAbbrev)) {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!harvestMethodIncompatObject.recordIds.includes(id)) harvestMethodIncompatObject.recordIds.push(id);
            harvestMethodIncompatObject.method = selectedMethodAbbrev;
        }
    }
}

/**
 * Take note of records that are incompatible with the numeric variable due to the harvest method
 */
function checkNumVarMethodIncompatibility() {
    var harvestMethodIncluded = varAbb.includes('HV_METH_DISC');
    var rows = [];

    if(harvestMethodIncluded || numVarAbbrev=='') return;

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row[dataLevel+"DbId"];

        // Skip row if marked incompatible from previous validation criteria
        if(incompatibleRecords.includes(id)) continue;

        /**
         * Determine harvest method of row to be checked.
         * If both committed and uncommitted methods are set,
         * the uncommitted method will be considered.
         */
        var currentHarvestMethod = 
            row.terminalHarvestMethod != "" ? 
                row.terminalHarvestMethod 
            : 
                row.harvestMethod
        ;
        var hMethAbbrev = harvestMethodValuesInv[currentHarvestMethod] ?? null;

        // Get supported methods for the currently selected numeric variable
        var harvestDataConfig = row.harvestDataConfig ?? {};
        var config = harvestDataConfig.config ?? {};
        var numVarHarvestMethodCompat = config.numVarHarvestMethodCompat ?? {};
        var currentNumVarMethods = numVarHarvestMethodCompat[numVarAbbrev] ?? [];

        /**
         * If the selected numeric variable is not supported
         * for the current record, add to incompatibility arrays.
        */
        if (!currentNumVarMethods.includes(hMethAbbrev)) {
            var mth = (hMethAbbrev==null) ? ('NOT_SET') : (hMethAbbrev);
            // Initialize variable array
            if(numVarMethodIncompatObject.methods[mth] == null) numVarMethodIncompatObject.methods[mth] = []
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!numVarMethodIncompatObject.methods[mth].includes(id)) numVarMethodIncompatObject.methods[mth].push(id);
            numVarMethodIncompatObject.recordIds.push(id);
            numVarMethodIncompatObject.numvar = numVarAbbrev;
        }
    }
}

/**
 * Take note of records and traits to be overwritten with the new data
 */
function countRecordsToOverwrite() {
    var overwriteTraits = [];
    var rows = [];

    if(mode=='selected') rows = selectedRecords;
    else if(mode=='all') rows = allRecords;
    
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row[dataLevel+"DbId"];

        if(incompatibleRecords.includes(id)) continue;

        var overwriteRow = false;

        // Get supported methods for the currently selected numeric variable
        var harvestDataConfig = row.harvestDataConfig ?? {};
        var config = harvestDataConfig.config ?? {};

        for(var currentAbbrev of varAbb) {
            var fieldName = variableFieldNames[currentAbbrev];
            var fieldNameTerminal = 'terminal' + fieldName.charAt(0).toUpperCase() + fieldName.slice(1);
            var variableRowVal = 
                (row[fieldNameTerminal]!='') ? 
                    (row[fieldNameTerminal]) 
                    : 
                    (row[fieldName])
            ;

            // If the value is not empty, set overwrite of record to true
            if(variableRowVal!='') {
                // overwriteRow = true;
                // break;
                if (!overwriteRecords.includes(id)) overwriteRecords.push(id);
                if(!overwriteTraits.includes(currentAbbrev)) overwriteTraits.push(currentAbbrev);
            }
        }
    }

    return overwriteTraits;
}

/**
 * Enable or disable buttons
 */
function adjustButtons() {
    // If variable abbev array is empty, disable buttons
    if(variableAbbrevArray.length == 0) {
        $('.hm-hd-bulk-update-btn').addClass("disabled");
    }
    // Else, enable buttons depending on the conditions below
    else if(!hasInvalidNumVar) {
        $('.hm-hd-bulk-update-btn').removeClass('disabled');
        // If selected record count is 0, disable bulk update selected btn
        if(selectedRecordsCount==0) {
            $('#hm-hd-bulk-update-selected-btn').addClass("disabled");
        }
    }
}

/**
 * Adjust abbrev array, value array, and buttons on input.
 * @param {string} value the value from the input field
 * @param {string} variableAbbrev the variable abbrev of the input
 * @param {integer} index the index of the variableAbbrev in the variableAbbrevArray
 */
function inputChange(value, variableAbbrev, index) {
    // If value is empty, remove the variableAbbrev from the variableAbbrevArray
    if(value=='') {
        if(index>-1) {                                  // If variableAbbrev is in variableArray, remove it
            variableAbbrevArray.splice(index,1);
            values.splice(index,1);
        }
        adjustButtons();
        return;
    }

    // If the variable abbrev is not yet included in the variable abbrev array,
    // then add it to the array, and then add the value to the values array
    if(index==-1) {
        variableAbbrevArray.push(variableAbbrev);
        values.push([value]);
    } else { // Else, add the value to the values array
        values[index] = [value];
    }

    adjustButtons();
}

/**
 * Formats numbers to comma-separated string per hundreds.
 * @param {string} num number to be formatted
 * @returns string number formatted with commas
 * Examples:
 * 4        -> 4
 * 100      -> 100
 * 1234     -> 1,234
 * 9876543  -> 9,876,543
 */
function numberWithCommas(num) {
    var pieces = num.toString().split(".");
    pieces[0] = pieces[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return pieces.join(".");
}

/**
 * Reset the browser and clear selection after bulk update
 */
function resetBrowser() {
    // Clear selection on grid reset
    var gridId = 'dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-table-con';
    var selectionStorageId = occurrenceId + "_" + gridId;
    clearSelectionHM(selectionStorageId);
    var pageurl = $('#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid li.active a').attr('href');
    $.pjax.reload(
        {
            container:'#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-pjax',
            url: pageurl,
            replace: false,
            type: 'POST',
            data: {
                forceReset:false,
                bulkOperation:true
            }
        }
    );
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} selectionStorageId unique string identifier holding the selected items
 * @param {boolean} all whether or not all checkbox selections are to be cleared
 */
function clearSelectionHM(selectionStorageId, all = false) {
    // If all flag is set to true, clear selection for all supported grids
    if (all) {
        // Loop through supported grids
        for(var i = 0; i < supportedCount; i++) {
            // Build selection identifier
            var currentSession = occurrenceId + "_" + supportedGrids[i];
            // Remove selection from storage
            sessionStorage.removeItem(currentSession);
        }
    }
    // If all flag is false, remove specified  selection id
    else sessionStorage.removeItem(selectionStorageId);
}

/**
 * Formats comma-separated integers. Removes duplicate integers,
 * excess commas, and sorts the integers in ascending order.
 * @param {string} value the comma-separated integer string
 * @returns {string} the formatted comma-separated integer
 */
function cleanCommaSepInt(value) {
    if(value == '' || value.length == 1) return value;
    // Replace 2 or more consecutive commas with just 1 comma
    value = value.replace(/,{2,}/g, ",");
    // If value starts with a comma, remove comma
    if(value.charAt( 0 ) === ',') value = value.slice(1);
    // If value ends with a comma, remove comma
    if(value.charAt( value.length - 1 ) === ',') value = value.slice(0, -1);
    // Remove duplicates
    var pieces = value.split(',');
    var finalPieces = [];
    $.each(pieces, function(idx, val){
        // Add to final pieces
        var integerVal = parseInt(val)
        if(integerVal > 0 && !finalPieces.includes(integerVal)) finalPieces.push(integerVal);
    });
    // Sort in ascending order
    finalPieces.sort( function(a,b) { return a - b; } );
    // Put value back together
    value = finalPieces.join(',')
    // Return final value conjoined with comma
    return value == '' ? '0' : value;
}

/**
 * Builds method-state compat array
 */
function buildStateHMethodCompat() {
    for(var state in gStateGTypeHMetCompat) {
        // if(state != 'TCV') {
        //     if(stateHMethodCompat[state] === undefined) stateHMethodCompat[state] = []
        //     var types = gStateGTypeHMetCompat[state];
        //     for(var type in types) {
        //         var methods = types[type];
        //         for(var method in methods) {
        //             if(!stateHMethodCompat[state].includes(method)) stateHMethodCompat[state].push(method); 
        //         }
        //     }
        // }

        if(stateHMethodCompat[state] === undefined) stateHMethodCompat[state] = []
        var types = gStateGTypeHMetCompat[state];
        for(var type in types) {
            var methods = types[type];
            for(var method in methods) {
                if(!stateHMethodCompat[state].includes(method)) stateHMethodCompat[state].push(method); 
            }
        }
    }
}

/**
 * Shows or hides an element based on the provided abbreviation.
 *
 * This function controls the visibility of an element by adding or removing the 'hidden' class.
 * It also manages the state of the element in the displayedInputFields array, enables or disables
 * the selection of the element in the Select2 widget, and enables or disables the remove button.
 *
 * @param {string} abbrev - The abbreviation of the element to show or hide.
 * @param {boolean} [show=true] - Whether to show (true) or hide (false) the element.
 * @param {boolean} [enableSelection=true] - Whether to enable (true) or disable (false) the selection of the element in the Select2 widget.
 * @param {boolean} [enableRemoveBtn=true] - Whether to enable (true) or disable (false) the remove button for the element.
 * @param {boolean} [removeFromDisplayedArray=true] - Whether to remove the element from the displayedInputFields array when hiding it.
 */
function showHideElement(abbrev, show = true, enableSelection = true, enableRemoveBtn = true, removeFromDisplayedArray = true) {
    // Build IDs
    let divId = "hm-hd-bu-input-div-" + abbrev;
    let inputId = "hm-hd-bu-input-" + abbrev;
    let removeBtnId = "remove-hdata-input-" + abbrev;
    
    // Show/hide
    if (show) {
        // Show by removing the hidden class
        $('#'+divId).removeClass('hidden');
        // Add to displayedInputFields array
        if (!displayedInputFields.includes(abbrev)) {
            displayedInputFields.push(abbrev);
        }
    }
    else {
        // Hide by adding the hidden class
        $('#'+divId).addClass('hidden');
        // Remove from displayedInputFields array
        if (displayedInputFields.includes(abbrev) && removeFromDisplayedArray) {
            displayedInputFields.splice(displayedInputFields.indexOf(abbrev), 1);
        }
        // Clear the input field
        $('#'+inputId).val('').trigger('input');
        $('#'+inputId).val('').trigger('change');
    }

    // Enable/disable selection
    if (enableSelection == true) {
        $('#hm-bulk-update-input-fields-select2').find('option[value="' + abbrev + '"]').prop('disabled', false);
    }
    else if (enableSelection == false) {
        $('#hm-bulk-update-input-fields-select2').find('option[value="' + abbrev + '"]').prop('disabled', true);
    }

    // Enable/disable remove button
    if (enableRemoveBtn == true) {
        $('#'+removeBtnId).removeClass('disabled');
    }
    else if (enableRemoveBtn == false) {
        $('#'+removeBtnId).addClass('disabled');
    }
}

/**
 * Displays custom flash message
 * @param {string} status the status of the message (info, success, warning, error)
 * @param {string} iconClass icon of flash message
 * @param {string} message the message to display
 * @param {string} parentDivId id of the div where the flash message is to be inserted
 * @param {float} time time in seconds before the flash message is removed from the display. 7.5 seconds by default.
 */
function displayCustomFlashMessage(status, iconClass, message, parentDivId, time = 7.5) {
    // Add flash message to parent div
    $('#' + parentDivId).html(
        '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
            '<div class="card-panel">' +
                '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                message +
                '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
            '</div>' +
        '</div>'
    );

    // remove flash message after the specified time in seconds (7.5 seconds by default)
    setTimeout(
        function() 
        {
            $('#w25-' + status + '-0').fadeOut(250);
            setTimeout(
                function() 
                {
                    $('#' + parentDivId).html('');
                }
            , 500);
        }
    , 1000 * time);
}


/**
 * Swaps the keys and values of a given JSON object.
 *
 * This function takes a JSON object as input and returns a new object
 * where the keys and values are swapped. Each key in the input object
 * becomes a value in the output object, and each value in the input
 * object becomes a key in the output object.
 *
 * @param {Object} json - The JSON object to be swapped.
 * @returns {Object} The new JSON object with keys and values swapped.
*/
function swap(json){
    var ret = {};
    for(var key in json){
        ret[json[key]] = key;
    }
    return ret;
}


JS;

$this->registerJs($script);

?>