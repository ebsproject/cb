<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use app\components\PrintoutsWidget;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use app\widgets\GermplasmInfo;

/**
 * Renders cross harvest data input step for Harvest Manager
 */

echo '<div id="hm-hdata-cross" class = "row col col-sm-12 col-md-12 col-lg-12" style = "margin-bottom:0px; padding:0px">';

// Add browser header
echo '<div class="hm-browser-title">' .
        '<h3 class="hm-browser-title">' . \Yii::t('app', 'Crosses') . '</h3>' .
    '</div>';

// Unpack browser data
$dataProvider = $browserData['dataProvider'];
$plotGermplasmStates = isset($browserData['germplasmStates']) ? $browserData['germplasmStates'] : []; 
$harvestDataBrowserDescription = ' Please use double bar (<b>||</b>) as the <b>OR</b> operator when filtering the columns.';
$harvestDataFilters = '';

$browserId = 'dynagrid-harvest-manager-cross-hdata-grid';

// Set action columns
$actionColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ],
    [
        'label' => "Checkbox",
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'visible' => true,
        'vAlign' => 'top',
        'mergeHeader' => true,
        'header' => '<input type="checkbox" class="filled-in" id="hm-browser-select-all-checkbox" table-con="' . $browserId . '-table-con"/><label style="padding-left: 20px;" for="hm-browser-select-all-checkbox"></label>',
        'content' => function ($model) use ($dataLevel, $browserId) {
            $checkboxId = $model['crossDbId'];
            $disabledCheckbox = '';
            $harvestStatus = $model['harvestStatus'];
            if(in_array($harvestStatus,['QUEUED_FOR_HARVEST','HARVEST_IN_PROGRESS'])) $disabledCheckbox = 'disabled';
            
            return '
            <input class="hm-browser-single-checkbox filled-in"' .
                'id="' . $checkboxId . '"' .
                'type="checkbox" ' .
                $disabledCheckbox . ' ' .
                'table-con="' . $browserId . '-table-con"' .
                'data_level="'. $dataLevel .'"/>' .
            '<label style="padding-left: 20px;" for="' . $checkboxId . '"></label>';
        },
        'width' => '1%',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT
    ]

];
// Set harvest status column
$harvestStatus = [
    'attribute' => 'harvestStatus',
    'label' => 'Harvest Status',
    'hAlign' => 'left',
    'vAlign' => 'top',
    'width' => 'auto',
    'noWrap' => true,
    'format' => 'raw',
    'visible' => true,
    'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
    // TEMPORARILY COMMENTED OUT FOR HARVEST STATUS QUICK FILTER IMPLEMENTATION.
    // WILL RE-APPLY ONCE REQUESTED TO BE RETURNED.
    // 'filterType' => GridView::FILTER_SELECT2,
    // 'filter' => ['NO_HARVEST' => 'no harvest',
    //     'INCOMPLETE' => 'incomplete harvest data',
    //     'CONFLICT' => 'data conflict',
    //     'READY' => 'ready for seed creation',
    //     'IN_QUEUE' => 'in queue for creation',
    //     'IN_PROGRESS' => 'seed creation in progress',
    //     'COMPLETED' => 'harvest complete',
    //     'DELETION_IN_PROGRESS' => 'seed/trait deletion in progress',
    //     'REVERT_IN_PROGRESS' => 'revert harvest in progress',
    //     'INVALID_STATE' => 'invalid state',
    //     'FAILED' => 'seed creation failed'
    // ],
    // 'filterWidgetOptions' => [
    //     'pluginOptions' => ['multiple'=>false, 'allowClear' => true],
    // ],
    // 'filterInputOptions' => ['placeholder' => 'Select status'],
    'value' => function ($model) use ($dataLevel) {
        $harvestStatus = isset($model['harvestStatus']) ? $model['harvestStatus'] : 'NO_STATUS';
        $state = $model['state'];
        $crossMethodAbbrev = $model['crossMethodAbbrev'];

        $hvSummaryInfoLink = Html::a(
            '<i class="material-icons blue-text" style="vertical-align:bottom">info</i>',
            '#',
            [
                'id' => 'hv-summary-btn',
                'data-tooltip' => \Yii::t('app', 'View harvest summary'),
                'class' => 'hm-tooltipped',
                'data-id' => $model['crossDbId'],
                'data-level' => 'cross',
                'data-target' => "#hm-hd-hv-summary-modal",
                'data-dismiss' => 'modal',
                'data-toggle' => "modal"
            ]
        );

        if (empty($crossMethodAbbrev)) {
            return '<span title="Missing cross method value" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'MISSING CROSS METHOD') . '</strong></span>';
        }

        if($state == 'unknown') {
            return '<a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank"><span title="State is unknown. Click here to request data curation." class="new badge center red darken-2"><strong>' . \Yii::t('app', 'INVALID STATE') . '</strong></span></a>';
        }

        if($harvestStatus=='NO_HARVEST') {
            return '<span title="No seeds created yet" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'NO HARVEST') . '</strong></span>';
        } else if (strpos($harvestStatus,'INCOMPLETE')!==false) {
            return '<span title="Harvest data is incomplete" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'INCOMPLETE DATA') . '</strong></span>';
        } else if (strpos($harvestStatus,'CONFLICT')!==false) {
            $parts = explode(': ', $harvestStatus);
            $title = $parts[1] ?? 'Unknown conflict';
            return '<span title="' . $title . '" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'DATA CONFLICT') . '</strong></span>';
        } else if (str_contains($harvestStatus,'FAILED')) {
            return '<span><span title="Seed and package creation failed" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'FAILED') . '</strong></span>&nbsp;' . $hvSummaryInfoLink;
        } else if ($harvestStatus=='BAD_QC_CODE') {
            return '<span title="Harvest data with B (bad) QC code detected" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'BAD QC CODE') . '</strong></span>';
        } else if ($harvestStatus=='DELETION_IN_PROGRESS') {
            return '<span title="Deletion of traits and seeds/packages is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'DELETION IN PROGRESS') . '</strong></span>';
        } else if ($harvestStatus=='REVERT_IN_PROGRESS') {
            return '<span title="Reversion of failed harvest is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'REVERT IN PROGRESS') . '</strong></span>';
        } else if ($harvestStatus=='HARVEST_IN_PROGRESS') {
            return '<span title="Seed and package creation is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'HARVEST IN PROGRESS') . '</strong></span>';
        } else if ($harvestStatus=='UPDATE_IN_PROGRESS') {
            return '<span title="Update of harvest data is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'UPDATE IN PROGRESS') . '</strong></span>';
        } else if ($harvestStatus=='COMPLETED') {
            return '<span title="Seeds and packages have been created" class="new badge center green darken-2"><strong>' . \Yii::t('app', 'HARVEST COMPLETE') . '</strong></span>&nbsp;' . $hvSummaryInfoLink;
        } else if ($harvestStatus=='READY') {
            return '<span title="'.ucfirst($dataLevel).' is ready for seed and package creation" class="new badge center blue darken-2"><strong>' . \Yii::t('app', 'READY') . '</strong></span>';
        } else if ($harvestStatus=='QUEUED_FOR_HARVEST') {
            return '<span title="Queued for seed and package creation" class="new badge center grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'QUEUED FOR HARVEST') . '</strong></span>';
        } else if ($harvestStatus=='QUEUED_FOR_UPDATE') {
            return '<span title="Queued for harvest data update" class="new badge center grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'QUEUED FOR UPDATE') . '</strong></span>';
        } else {
            return '<span class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
        }
    },
    'headerOptions'=>[
        'style' => 'text-align:left'
    ],
];

// Set default browser columns
$columns = [
    $harvestStatus,
    [
        'attribute' => 'crossName',
        'format' => 'raw',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'contentOptions'=>[
            'class' => 'germplasm-name-col'
        ],
        'value' => function($model){
            return !empty($model['crossName']) ? $model['crossName'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossFemaleParent',
        'label' => '<span title="Female Parent"><i class="fa fa-venus"></i>'.Yii::t('app', ' Female Parent').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>[
            'class'=>'red lighten-4',
            'style'=>'min-width:130px !important',
        ],
        'contentOptions'=>[
            'class'=>'germplasm-name-col red lighten-5'
        ],
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model) {
            $germplasmDbId = $model['femaleGermplasmDbId'];
            $designation = $model['crossFemaleParent'];

            if(empty($germplasmDbId)) return '<span class="not-set">(not set)</span>';
            
            $linkAttrs = [
                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                'class' => 'blue-text text-darken-4 header-highlight',
                'title' => \Yii::t('app', 'Click to view more information'),
                'data-label' => $designation,
                'data-id' => $germplasmDbId,
                'data-target' => '#view-germplasm-widget-modal',
                'data-toggle' => 'modal'
            ];

            $widgetName = GermplasmInfo::widget([
                'id' => $germplasmDbId,
                'entityLabel' => $designation,
                'linkAttrs' => $linkAttrs
            ]);
            return $widgetName;
        }
    ],
    [
        'attribute' => 'femaleParentage',
        'label' => '<span title="Female Parentage"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Parentage').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>[
            'class'=>'germplasm-name-col red lighten-5'
        ],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['femaleParentage']) ? $model['femaleParentage'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossMaleParent',
        'label' => '<span title="Male Seed Source"><i class="fa fa-mars"></i>'.Yii::t('app', ' Male Parent').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>[
            'class'=>'blue lighten-4',
            'style'=>'min-width:120px !important',
        ],
        'contentOptions'=>[
            'class'=>'germplasm-name-col blue lighten-5'
        ],
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model) {
            $germplasmDbId = $model['maleGermplasmDbId'];
            $designation = $model['crossMaleParent'];

            if(empty($germplasmDbId)) return '<span class="not-set">(not set)</span>';
            
            $linkAttrs = [
                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                'class' => 'blue-text text-darken-4 header-highlight',
                'title' => \Yii::t('app', 'Click to view more information'),
                'data-label' => $designation,
                'data-id' => $germplasmDbId,
                'data-target' => '#view-germplasm-widget-modal',
                'data-toggle' => 'modal'
            ];

            $widgetName = GermplasmInfo::widget([
                'id' => $germplasmDbId,
                'entityLabel' => $designation,
                'linkAttrs' => $linkAttrs
            ]);
            return $widgetName;
        }
    ],
    [
        'attribute' => 'maleParentage',
        'label' => '<span title="Male Parentage"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Parentage').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>[
            'class'=>'germplasm-name-col blue lighten-5'
        ],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['maleParentage']) ? $model['maleParentage'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossMethod',
        'format' => 'raw',
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['crossMethod']) ? $model['crossMethod'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleEntryNumber',
        'label' => '<span title="Female Entry No."><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Entry No.').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible'=>false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['femaleEntryNumber']) ? $model['femaleEntryNumber'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleEntryCode',
        'label' => '<span title="Female Entry Code"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Entry Code').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible'=>false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['femaleEntryCode']) ? $model['femaleEntryCode'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleSourceSeedEntry',
        'label' => '<span title="Female Source Seed Entry"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Source Seed Entry').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['femaleSourceSeedEntry']) ? $model['femaleSourceSeedEntry'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleEntryNumber',
        'label' => '<span title="Male Entry No."><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Entry No.').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible'=>false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['maleEntryNumber']) ? $model['maleEntryNumber'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleEntryCode',
        'label' => '<span title="Male Entry Code"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Entry Code').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible'=>false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['maleEntryCode']) ? $model['maleEntryCode'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleSourceSeedEntry',
        'label' => '<span title="Male Source Seed Entry"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Source Seed Entry').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['maleSourceSeedEntry']) ? $model['maleSourceSeedEntry'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleParentSeedSource',
        'label' => '<span title="Female Seed Source"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Seed Source').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['femaleParentSeedSource']) ? $model['femaleParentSeedSource'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleParentSeedSource',
        'label' => '<span title="Male Seed Source"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Seed Source').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'encodeLabel' => false,
        'visible' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['maleParentSeedSource']) ? $model['maleParentSeedSource'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleSourcePlot',
        'label' => '<span title="Female Source Plot"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Source Plot').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['femaleSourcePlot']) ? $model['femaleSourcePlot'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleSourcePlot',
        'label' => '<span title="Male Source Plot"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Source Plot').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['maleSourcePlot']) ? $model['maleSourcePlot'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleGermplasmState',
        'label' => '<span title="Female Germplasm State"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Germplasm State').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'value' => function($model){
            return !empty($model['femaleGermplasmState']) ? $model['femaleGermplasmState'] : '<span class="not-set">(not set)</span>';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $germplasmStateScaleValues,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true,'class' => 'col-lg-2'],
        ],
        'filterInputOptions' => ['placeholder' => 'Select state'],
    ],
    [
        'attribute' => 'maleGermplasmState',
        'label' => '<span title="Male Germplasm State"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Germplasm State').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'value' => function($model){
            return !empty($model['maleGermplasmState']) ? $model['maleGermplasmState'] : '<span class="not-set">(not set)</span>';
        },   
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $germplasmStateScaleValues,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true,'class' => 'col-lg-2'],
        ],
        'filterInputOptions' => ['placeholder' => 'Select state'],
    ],
    [
        'attribute' => 'remarks',
        'label' => 'Cross Remarks',
        'format' => 'raw',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'contentOptions' => [
            "class" => "germplasm-name-col"
        ],
        'value' => function($model){
            return !empty($model['remarks']) ? $model['remarks'] : '<span class="not-set">(not set)</span>';
        }
    ],
]; 


// Merge columns
$gridColumns = array_merge($actionColumns, $columns, $inputColumns);


// Initialize printouts component
$printouts = PrintoutsWidget::widget([
    "product" => "Harvest Manager",
    "program" => $program,
    "occurrenceIds" => [ $occurrenceId ]
]);

// Render grid
$dynagrid = DynaGrid::begin([
    'columns'=>$gridColumns,
    'storage' => DynaGrid::TYPE_SESSION,
    'theme'=>'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'tableOptions'=>['class'=>$browserId.'-table'],
        'options'=>['id'=>$browserId.'-table-con'],
        'striped'=>false,
        'hover' => true,
        'floatHeader' => true,
        'responsive' => true,
        'floatOverflowContainer' => true,
        'showPageSummary' => false,
        'summaryOptions' => ['class' => 'pull-right'],
        'pjax' => true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => $browserId, 'enablePushState' => false,],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'id' => $browserId.'-table-con',
        'responsiveWrap' => false,
        'floatHeaderOptions' => [
			'position' => 'absolute',
            'top' => 63
		],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', "Manage your cross harvest data here.".$harvestDataBrowserDescription) . ' {summary}' .
                '<br/>
                <p id="total-selected-text" class = "pull-right" style = "margin: -1px;"></p>
                <div class="col-md-12" id="harvest-data-filter-div" style="padding-top:15px">'
                    .$observationDataFilters.            
                '</div>
                <div class = "row">
                    <div class="col-md-4" id="harvest-status-filter-div" style="padding-top:15px">'
                        .$statusChips.            
                    '</div>
                    <div class="hm-cross-range-filter col-md-6" style="padding-top:15px">'
                        .$toolbarFilters.
                    '</div>
                </div>',
            'after' => false,
        ],
        'toolbar' =>  [
            [
                'content' =>
                    Html::a('<i class="glyphicon glyphicon-edit"></i>' .
                        '', '#', [
                        'id' => 'hm-hdata-bulk-update-btn',
                        'class' => 'btn btn-default hm-tooltipped',
                        'style' => "margin-left:10px;",
                        'data-toggle' => 'modal',
                        'data-target' => '#hm-hd-bulk-update-modal',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Bulk update harvest traits'),
                        'disabled' => false,
                        'data-level' => 'cross',
                    ]).
                    Html::a('<i class="material-icons">delete_forever</i>' .
                        '', '#', [
                        'id' => 'hm-hdata-bulk-delete-btn',
                        'class' => 'btn btn-default red darken-3 hm-tooltipped',
                        'data-toggle' => 'modal',
                        'data-target' => '#hm-hd-bulk-delete-modal',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Bulk delete harvest traits'),
                        'disabled' => false,
                        'data-level' => 'cross',
                    ]).
                    Html::a('<i class="material-icons">undo</i>' .
                        '', '#', [
                        'id' => 'hm-hdata-revert-harvest-btn',
                        'class' => 'btn btn-default orange darken-3 hm-tooltipped',
                        'data-toggle' => 'modal',
                        'data-target' => '#hm-hd-bulk-delete-modal',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Revert failed harvest'),
                        'disabled' => false,
                        'data-level' => 'cross',
                    ]).
                    /**
                     * The ADDITIONAL HARVEST is TEMPOARILY DISABLED and HIDDEN.
                     * 
                     * Further develoment is needed to implement this feature in the refactored Harvest Manager version.
                     */
                    // Html::a('<i class="material-icons">add</i>' .
                    //     '', '#', [
                    //     'id' => 'hm-hdata-additional-harvest-btn',
                    //     'class' => 'btn btn-default green darken-3 hm-tooltipped',
                    //     'data-toggle' => 'modal',
                    //     'data-target' => '#hm-hd-addl-harvest-modal',
                    //     'data-position' => 'top',
                    //     'data-tooltip' => \Yii::t('app', 'Additional harvest'),
                    //     'disabled' => false,
                    //     'data-level' => 'cross',
                    // ]).
                    Html::a(
                        '<i class="material-icons large">notifications_none</i>
                        <span id="notif-badge-id" class=""></span>',
                        '#',
                        [
                            'id' => 'hm-hd-notifs-btn',
                            'class' => 'btn waves-effect waves-light harvest-manager-notification-button hm-tooltipped',
                            'data-pjax' => 0,
                            'style' => "overflow: visible !important;",
                            "data-position" => "top",
                            "data-tooltip" => \Yii::t('app', 'Notifications'),
                            "data-activates" => "notifications-dropdown",
                            'disabled' => false,
                        ]
                    )
            ],
            [
                'content' =>
                    $printouts.
                    /**
                     * The FILTER BY OBSERVATION DATA feature is TEMPOARILY DISABLED and HIDDEN.
                     * 
                     * Further develoment is needed to implement this feature in the refactored Harvest Manager version.
                     */
                    // Html::a(
                    //     '<i class="material-icons">visibility</i>',
                    //     '#',
                    //     [
                    //         'class' => 'btn btn-default lighten-4 hm-tooltipped',
                    //         'id' => 'hm-hd-observation-data-button-main',
                    //         'data-toggle' => 'modal',
                    //         'data-target' => '#hm-hd-observation-data-modal',
                    //         'data-position' => 'top',
                    //         'data-tooltip' => \Yii::t('app', 'Filter by observation data'),
                    //         'data-level' => 'cross',
                    //     ]
                    // ).
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [Yii::$app->controller->action->id, 'dataLevel' => $dataLevel],
                        [
                            'class' => 'btn btn-default hm-tooltipped',
                            'id' => 'hm-hd-reset-cross-btn',
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    ).
                    '{dynagridFilter}{dynagridSort}{dynagrid}',
            ]
        ],
    ],
    'options'=>['id'=>$browserId]
]);

DynaGrid::end();

echo '</div>';

$script = <<< JS
// Set variables
var occurrenceId = $occurrenceId;
var gridId = '$browserId-table-con';
var selectionStorageId = occurrenceId + "_" + gridId;
// Restore checkbox selection
checkboxActionsHM(selectionStorageId, gridId);

$("document").ready(function(){
    // Reset browser btn click event
    $(document).on("click","#hm-hd-reset-cross-btn", function(e) {
        e.preventDefault();
        // Clear selection on grid reset
        clearSelectionHM(selectionStorageId);
        var pageurl = $('#$browserId li.active a').attr('href');
        $.pjax.reload(
            {
                container:'#$browserId-pjax',
                url: pageurl,
                replace: false,
                type: 'POST',
                data: {
                    forceReset:true
                }
            }
        );
    });

    // Perform after pjax completion of browser
    $(document).on('ready pjax:complete','#$browserId-pjax', function(e) {
        // Initialize tooltip
        initializeTooltips();
        // Adjust layout
        adjustLayout();
        // Restore checkbox selection
        checkboxActionsHM(selectionStorageId, gridId);
        // Set alignment of table headers to 'top'
        thAlignTop();
    });
});

JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    div.hm-browser-title {
        padding: 10px 0px 10px 10px;
    }
');

?>