<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Bulk delete modal contents
 */

use yii\helpers\Url;

// Define URLs
$urlRetrieveAllRecordInfo = Url::to(['harvest-data/retrieve-all-record-info']);
$urlUpdateRecordsStatus = Url::to(['harvest-data/update-records-status']);
$urlGetBackgroundJob = Url::to(['harvest-data/get-background-job']);
$urlGetUncommittedTraitIds = Url::to(['harvest-data/get-uncommitted-trait-ids']);
$urlDeleteDatasets = Url::to(['harvest-data/delete-datasets']);
$urlGetSeedCount = Url::to(['harvest-data/get-seed-count']);
$urlCheckSeedEntries = Url::to(['harvest-data/check-seed-entries']);
$urlGetDatabaseIds = Url::to(['harvest-data/get-database-ids']);
$urlDeleteViaProcessor = Url::to(['harvest-data/delete-via-processor']);
$urlGetBackgroundJobs = Url::to(['harvest-data/get-background-jobs']);
$urlAcknowledgeRecords = Url::to(['harvest-data/acknowledge-records']);
$urlDeleteInBackground = Url::to(['harvest-data/delete-in-background']);
// Get arrays
$allAbbrevsJSON = json_encode(array_column($havestDataVarArr, 'variableAbbrev')) ?? [];
$numVarAbbrevsJSON = json_encode($numVarAbbrevs) ?? [];

$abbrevFieldNameArr = [];
foreach ($havestDataVarArr as $hData) {
    $abbrevFieldNameArr[$hData['variableAbbrev']] = $hData['apiFieldName'];
}
$abbrevFieldNameArrayJSON = isset($abbrevFieldNameArr) ? json_encode($abbrevFieldNameArr) : [];

$recordIdsJson = json_encode($recordIds);
$recordInfoJson = json_encode($recordInfo);
$traitsStatusJson = json_encode($traitsStatus);

/**
 * TCV check disabled in support of CORB-6631.
 * The code will be kept here, commented,
 * in case the check needs to be re-enabled.
 */
// $harvestDataCheckboxHTML = $stage == 'TCV' ?
//     '<li><input type="checkbox" class="filled-in checkboxlist datacheck hm-hd-bd-checkbox hm-hd-bd-checkbox-item hm-hd-bd-checkbox-item-non-num-var" data-id-string="" id="hm-hd-bd-checkbox-HVDATE_CONT" abbrev="HVDATE_CONT" />
//     <label class="hm-hd-bd-checkbox hm-hd-bd-checkbox-item-non-num-var" style="padding-left: 20px; font-weight: normal; color: black;" for="hm-hd-bd-checkbox-HVDATE_CONT">&nbsp;&nbsp; Harvest Date</label></li>'
//     : $harvestDataCheckboxHTML;

// Display number of selected items
$recordCount = count($recordIds);
$countString = $recordCount == 0 ? 'No' : $recordCount;
$itemType = '';
if($dataLevel == 'plot') {
    if($recordCount != 1) $itemType = 'plots';
    else $itemType = $dataLevel;
}
else if($dataLevel == 'cross') {
    if($recordCount != 1) $itemType = 'crosses';
    else $itemType = $dataLevel;
}
$count = $countString." selected ".$itemType;

?>

<div id="bulk-delete-modal">
    <div id="bulk-delete-main-body">
        <p style="font-size:115%;"> <?= \Yii::t('app',"Please select the type of traits to delete:") ?> </p>
        <div style="margin-bottom:50px;">
            <?php
                echo '
                <div id="delete-type-notification-div" style="margin-bottom:10px">
                </div>
                <div>
                <div class="col-md-6">
                    <input type="checkbox" class="filled-in hm-hd-bd-trait-type-chk" data-id-string="" id="hm-hd-bd-trait-type-chk-uncommitted"/>
                    <label style="padding-left: 20px; font-weight: normal; color: black;" for="hm-hd-bd-trait-type-chk-uncommitted">&nbsp;&nbsp; Uncommitted Traits</label>
                </div>
                <div class="col-md-6">
                    <input type="checkbox" class="filled-in hm-hd-bd-trait-type-chk" data-id-string="" id="hm-hd-bd-trait-type-chk-committed" />
                    <label style="padding-left: 20px; font-weight: normal; color: black;" for="hm-hd-bd-trait-type-chk-committed">&nbsp;&nbsp; Committed Traits</label>
                </div>
                </div>';   
                
            ?>
        </div>

        <div id="delete-type-remarks-div" style="margin-bottom:10px"></div>

        <div class="panel panel-default hm-hd-bd-trait-checkbox-div" style=" min-height:100px; margin-top:10px">
            <div class="row" style="margin-left:5px; margin-top:5px;padding-top:5px; padding-right:25px; padding-left:10px;">
                <div class="collapsible-header active" style="margin-bottom:10px; font-size:14px">
                    <div class="col-md-6">
                        <i class="material-icons">list</i> <?= \Yii::t('app',"Harvest Traits") ?>
                    </div>
                    <div class="col-md-6" style="padding: 0px 0px 0px 0px;">
                        <p class="float-right">
                            <?= \Yii::t('app',$count) ?>
                        </p>
                    </div>
                </div>
                <div style="margin-left:20px">
                
                    <?php
                        $checkboxes =  
                        '
                        <p style="font-size:115%;">Please select traits to be deleted: </p>
                        <ul style="display: inline-block">
                        <li><input type="checkbox" class="filled-in checkboxlist" data-id-string="" id="hm-hd-bd-checkbox-all"/>
                        <label style="padding-left: 20px; font-weight: normal; color: black;" for="hm-hd-bd-checkbox-all">&nbsp;&nbsp; Select All</label></li>' . $harvestDataCheckboxHTML
                        . '<div id="hm-hd-bd-trait-notification-div" style="margin-bottom:10px"></div>';
                        echo $checkboxes;
                    ?>
            
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$script = <<< JS
var uncommittedSelected = false;
var committedSelected = false;
var selectedTypes = [];
var selectedAbbrevs = [];
var occurrenceId = $occurrenceId;
var locationId = $locationId;
var dataLevel = "$dataLevel";
var recordCount = $recordCount;
var allRecordsCount = $allRecordsCount;
var allAbbrevs = $allAbbrevsJSON;
var numVarAbbrevs = $numVarAbbrevsJSON;
var abbrevFieldNameArray = $abbrevFieldNameArrayJSON;
var allAbbrevsCount = allAbbrevs.length;
var selectedRecords = $recordInfoJson;
var allRecords = {};
var traitsStatus = $traitsStatusJson;
var processStart = 0;
var hasSeeds = false;
var hasEntries = false;
var numberOfSeeds = 0;
var harvestDataCount = 0;
var harvestDataThreshold = $harvestDataThreshold;
var seedThreshold = $seedThreshold;
var recordDbIdsToDelete = [];
var lastKnownRecords = [];
var lastKnownRecordsToRevert = [];
var thresholdExceeded = false;
var noHarvestData = false;
var noHarvestDataTriggered = false;
var noUncommitted = false;
var withTerminalData = [];

// Hide harvest data checkboxes
$('.hm-hd-bd-trait-checkbox-div').hide();
$('#hm-hd-bulk-delete-cancel-btn').hide();
$('#hm-hd-bulk-delete-done-btn').hide();
$("#hm-hd-bd-delete-progress").hide();
$('.hm-hd-bd-confirmation-acknowledge-grp').hide();
$('.hm-hd-bd-confirmation-acknowledge-grp').addClass('disabled');
$('.hm-hd-bd-confirmation-default-grp').hide();
$('.hm-hd-bd-confirmation-default-grp').addClass('disabled');
disableCheckboxes();

$(document).ready(function() {
    // Uncommitted checkbox input event
    $('#hm-hd-bd-trait-type-chk-uncommitted').on('input', function (e) {
        if(uncommittedSelected) {
            uncommittedSelected = false;
        }
        else uncommittedSelected = true;

        displayTraitCheckboxes();
        adjustButtonDisplay();   
    });

    // Committed checkbox input event
    $('#hm-hd-bd-trait-type-chk-committed').on('input', function (e) {
        if(committedSelected) {
            committedSelected = false;
        }
        else committedSelected = true;

        displayTraitCheckboxes();
        adjustButtonDisplay();
    });

    // Select all traits checkbox input event
    $('#hm-hd-bd-checkbox-all').on('input', function (e) {
        var checkboxClass = "hm-hd-bd-checkbox-item";

        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.'+checkboxClass).attr('checked','checked');
            $('.'+checkboxClass).prop('checked',true);
            $("."+checkboxClass+":checkbox").parent("td").parent("tr").addClass("selected");
            selectedAbbrevs = JSON.parse(JSON.stringify(allAbbrevs));
        }else{
            $(this).removeAttr('checked');
            $('.'+checkboxClass).prop('checked',false);
            $('.'+checkboxClass).removeAttr('checked');
            $("input:checkbox."+checkboxClass).parent("td").parent("tr").removeClass("selected"); 
            selectedAbbrevs = [];
        }

        $('#hm-hd-bd-trait-notification-div').html('');

        adjustButtonDisplay();
    });

    // Specific trait checkbox input event
    $('.hm-hd-bd-checkbox-item').on('input', function (e) {
        var id = $(this).prop('id');
        var abbrev = $(this).attr('abbrev');
        var index = selectedAbbrevs.indexOf(abbrev);

        if($(this).prop("checked") === true)  {
            // If element not yet in the array, add
            if(index == -1) {
                selectedAbbrevs.push(abbrev);
            }
            $(this).parent("td").parent("tr").addClass("selected");
        } else{
            // If element is in the array, remove
            if(index > -1) {
                selectedAbbrevs.splice(index,1);
            }
            $(this).parent("td").parent("tr").removeClass("selected");
        }
        // Tick or untick the select all checkbox
        var selAll = tickSelectAll();
        $('#hm-hd-bd-checkbox-all').prop("checked", selAll);

        adjustButtonDisplay();

        // If the input is harvest method or numeric variable,
        // toggle checkboxes and check if numeric vars will be deleted
        if(abbrev == 'HV_METH_DISC' || numVarAbbrevs.includes(abbrev)) {
            checkHarvestMethodAndNumVar($(this), abbrev);
        }
    });

    // Delete button click event
    $('.hm-hd-bulk-delete-proceed-btn').off('click').on('click', function (e) {
        $('.hm-hd-bd-confirmation-acknowledge-grp').hide();
        $('.hm-hd-bd-confirmation-default-grp').show();
        $('.hm-hd-bd-confirmation-default-grp').removeClass('disabled');
        // Hide main div
        $("#hm-hd-bulk-delete-confirmation-modal-additional-main").hide();
    });

    // Delete selected records button click event
    $('#hm-hd-bulk-delete-selected-btn').off('click').on('click', function (e) {
        mode = 'selected';
        $("#hm-hd-bulk-delete-confirmation-modal-additional-main").hide();
        buildConfirmationModal(selectedRecords);
    });

    // Delete all records button click event
    $('#hm-hd-bulk-delete-all-btn').off('click').on('click', function (e) {
        mode = 'all';
        $("#hm-hd-bulk-delete-confirmation-modal-additional-main").hide();
        // Retrieve all records (formatted), then build confirmation modal contents
        $('#hm-hd-bulk-delete-confirmation-modal-body').html('<p style="font-size:115%;"><i>Loading info of '+allRecordsCount+' records...</i></p><div class="progress"><div class="indeterminate"></div></div>');
        $.ajax({
            url: '$urlRetrieveAllRecordInfo',
            data: {
                occurrenceId: occurrenceId,
                dataLevel: dataLevel
            },
            type: 'POST',
            async: true,
            success: function(data) {
                $('#hm-hd-bulk-delete-confirmation-modal-body').html('<p style="font-size:115%;"><i>Info of '+allRecordsCount+' records retrieved. Checking info...</i></p><div class="progress"><div class="indeterminate"></div></div>');
                allRecords = jQuery.parseJSON(data);
                buildConfirmationModal(allRecords);
            },
            error: function() {
                $('#hm-hd-bulk-delete-confirmation-modal-body').html('<p style="font-size:115%;"><i>There was a problem while loading record information.</i></p>');
            }
        });
    });

    // Acknowledge button click event
    $('#hm-hd-bd-confirmation-acknowledge-btn').off('click').on('click', function() {
        // Revert back records with seeds used in entries back to COMPLETED
        updateRecordsStatus(lastKnownRecordsToRevert, 'COMPLETED');
        // If no records can be updated, return
        if(lastKnownRecords.length == 0) return;

        // Check if there are still seeds for deletion
        $.ajax({
            url: '$urlGetSeedCount',
            data: {
                recordIds: JSON.stringify(lastKnownRecords),
                dataLevel: dataLevel
            },
            type: 'POST',
            async: true,
            success: function(seedCount) {
                seedCount = parseInt(seedCount);
                numberOfSeeds = seedCount;

                // If no seeds found, proceed to plot data deletion
                if(seedCount == 0) {
                    hasSeeds = false;
                    retrieveDatabaseIds(lastKnownRecords);
                    return;
                }

                // If seeds are found, retrieve database ids
                hasSeeds = true;
                retrieveDatabaseIds(recordDbIdsToDelete);
            },
            error: function() {
                ajaxGenericError();
            }
        });
    });

    // Confirm delete button click event
    $('#hm-hd-bd-confirmation-delete-btn').off('click').on('click', function() {
        // Hide buttons
        $('.hm-hd-bd-delete-btn').hide();
        $('.hm-hd-bd-done-btn').hide();
        // Hide modal body and display progress bar and progress field
        $("#hm-hd-bulk-delete-modal-body").hide();
        $('#hm-hd-bd-delete-progress').html('<p class="hm-hd-bd-progress-items" style="font-size:115%;"><i>Deleting...</i></p><div class="progress hm-hd-bd-progress-items"><div class="indeterminate hm-hd-bd-progress-items"></div></div>'
            +'<textarea id="delete-progress-textarea" name="delete-progress-display" style="background-color:#ffffff;min-height:150px;" rows="4" cols="50" readonly></textarea>');
        $("#hm-hd-bd-delete-progress").show();
        // Set rows based on mode
        var rows = [];
        if (mode == 'selected') {
            rows = selectedRecords;
        }
        if (mode == 'all') {
            rows = allRecords;
        }
        // Remove records without traits from the roster
        rows = eliminateRecordsWithoutTraits(rows);
        // Mark process start time
        processStart = new Date().getTime();
        // Update records status
        updateRecordsStatus(rows,'DELETION_IN_PROGRESS');
    });
});

/**
 * Toggle numeric variable checkboxes, if needed
 * @param {object} element HTML checkbox element
 * @param {string} abbrev variable abbreviation
 */
function checkHarvestMethodAndNumVar(element, abbrev) {
    // If harvest method, check all numeric variable checkbox (if not yet checked)
    if(abbrev == 'HV_METH_DISC') {
        if(element.prop("checked") === true)  {
            $('.hm-hd-bd-checkbox-item-num-var').attr('checked','checked');
            $('.hm-hd-bd-checkbox-item-num-var').prop('checked',true);
            $(".hm-hd-bd-checkbox-item-num-var:checkbox").parent("td").parent("tr").addClass("selected");
            // Add numeric variable abbrevs to the selected abbrevs array
            $.each(numVarAbbrevs, function(index, value){
                var idx = selectedAbbrevs.indexOf(value);
                if(idx == -1) {
                    selectedAbbrevs.push(value);
                }
            });
            if(numVarAbbrevs.length > 0) {
                // Display info that numeric variables are all deleted when harvest method is deleted
                $('#hm-hd-bd-trait-notification-div').html('<div class="blue lighten-5 blue-text text-darken-4" id="trait-notification-div-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 5px; border-radius: 5px"><p style="font-size:115%;">'
                + 'The numeric variable values will be deleted along with the harvest method values.'
                +'</p></div>');
            }
        }
        // If harvest method checkbox is deselected,
        // remove info panel
        else {
            $('#hm-hd-bd-trait-notification-div').html('');
        }
    }
    // If numeric variable, remove harvest method from selected abbrevs array
    else {
        var idx = selectedAbbrevs.indexOf('HV_METH_DISC');
        if(idx > -1) {
            selectedAbbrevs.splice(idx,1);
        }
        // Uncheck harvest method checkbox
        $('#hm-hd-bd-checkbox-HV_METH_DISC').prop('checked',false);
        $('#hm-hd-bd-checkbox-HV_METH_DISC').removeAttr('checked');
        $("input:checkbox#hm-hd-bd-checkbox-HV_METH_DISC").parent("td").parent("tr").removeClass("selected"); 
        // Remove info panel
        $('#hm-hd-bd-trait-notification-div').html('');
    }

    adjustButtonDisplay();
}

/**
 * Perform record harvest status update
 * @param {object} records object containing records to be updated
 * @param {string} status harvest status to be applied, defaults to null
 * @param {boolean} displayToast whether or not toast is to be displayed, defaults to true
 * @param {boolean} parseArray whether or not parse the records as array, defaults to true
 */
function updateRecordsStatus(records, status = null, displayToast = true, parseArray = true) {
    // Update text area
    if(!thresholdExceeded) updateTextArea(" > Updating status of records...\\n");
    // Get record id array
    var recordIds = records
    
    if(parseArray) recordIds = getRecordIdArray(records);

    if( recordIds !== undefined && recordIds!== null 
        && recordIds.length > 0 && typeof recordIds[0] === 'object'){
        let columnName = 'plotDbId';
        if(recordIds[0]['plotDbId'] == undefined 
        && recordIds[0]['crossDbId'] !== undefined){
            columnName = 'crossDbId';
        }

        let newRecordIds = recordIds.map(x => { return x[columnName] })
        recordIds = newRecordIds;
    }
    // Build data
    let data = {
        recordIds: JSON.stringify(recordIds),
        status: status,
        occurrenceId: occurrenceId,
        dataLevel: dataLevel
    }
    // If no status was provided, set validateStatus to true
    if(status == null) {
        data = {
            recordIds: JSON.stringify(recordIds),
            validateStatus: 'true',
            occurrenceId: occurrenceId,
            dataLevel: dataLevel
        }
    }
    // Send request to processor for status update
    $.ajax({
        url: '$urlUpdateRecordsStatus',
        data: data,
        type: 'POST',
        async: true,
        success: function(data) {
            // Parse data to get job id
            var result = jQuery.parseJSON(data);
            var job = result.data[0] != null ? result.data[0] : [];
            var jobId = job.backgroundJobDbId != null ? job.backgroundJobDbId : 0;

            // If jobId is 0, display error
            if(jobId == 0) {
                $('.hm-hd-bd-delete-btn').show();
                ajaxGenericError(false);
                return;
            }

            // Track record update progress
            getRecordUpdateProgress(jobId, records, status, displayToast);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Recursively track the progress of the record update given the job id
 * @param {integer} jobId object containing records to be updated
 * @param {object} recordIdsToDelete object containing records to be updated
 * @param {string} status harvest status to be applied
 * @param {boolean} displayToast whether or not toast is to be displayed, defaults to true
 */
function getRecordUpdateProgress(jobId, recordIdsToDelete, status, displayToast = true) {
    // Send request to retrieve background job info
    $.ajax({
        url: '$urlGetBackgroundJob',
        data: {
            jobId: jobId
        },
        type: 'POST',
        async: true,
        success: function(data) {
            // Parse data to get job id and job status
            var result = jQuery.parseJSON(data);
            var job = result.data != null ? result.data : [];
            var jobId = job.backgroundJobDbId != null ? job.backgroundJobDbId : 0;
            var jobStatus = job.jobStatus != null ? job.jobStatus : 0;

            // If jobId is 0, display error
            if(jobId == 0) {
                $('.hm-hd-bd-delete-btn').show();
                ajaxGenericError(false);
                return;
            }

            // If job status is DONE of FAILED, end recursive call
            if(jobStatus == 'DONE' || jobStatus == 'FAILED') {
                // Reset the browser
                resetBrowser();
                // Update the text area
                if(!thresholdExceeded) updateTextArea(" > STATUS UPDATED (" + jobStatus + ").\\n");

                // If the status is DELETION_IN_PROGRESS, proceed with deletion
                if(status == 'DELETION_IN_PROGRESS') {
                    if(uncommittedSelected) {
                        deleteUncomittedTraits(recordIdsToDelete);
                    }
                    else {
                        performDelete(recordIdsToDelete);
                    }
                }
                else {
                    if(noHarvestData && !noHarvestDataTriggered) {
                        noHarvestDataTriggered = true;
                        // Calculate total time consumed
                        var deleteExecutionTimeMillis = new Date().getTime() - processStart;
                        // Reset modal UI
                        $("#hm-hd-bulk-delete-modal-body").show();
                        $('#hm-hd-bd-delete-progress').html('');
                        // Display toast notification
                        var notif = "<i class='material-icons blue-text'>info</i>&nbsp;Bulk delete canceled. Found nothing to delete (" + (deleteExecutionTimeMillis*.001).toFixed(3) + "s)";
                        if(displayToast) Materialize.toast(notif, 2500);
                        // Close modal
                        $("#hm-hd-bulk-delete-modal.in").modal('hide');
                        // Reset the browser
                        resetBrowser();
                        return;
                    }
                    if(hasEntries) {
                        if(lastKnownRecords.length == 0) {
                            // Calculate total time consumed
                            var deleteExecutionTimeMillis = new Date().getTime() - processStart;
                            // Reset modal UI
                            $("#hm-hd-bulk-delete-modal-body").show();
                            $('#hm-hd-bd-delete-progress').html('');
                            // Display toast notification
                            var notif = "<i class='material-icons blue-text'>info</i>&nbsp;Deletion of harvest traits was canceled.";
                            if(displayToast) Materialize.toast(notif, 2500);
                            // Close modal
                            $("#hm-hd-bulk-delete-modal.in").modal('hide');
                            // Reset the browser
                            resetBrowser();
                        }
                        // if(status !== null) { // TEMPORARILY CHANGED
                        //     return;
                        // }
                        // TEMPORARY, check data level.
                        // If dataLevel is cross and status is not 'NO_HARVEST', return. TEMPORARY
                        if(dataLevel == 'cross' && status !== 'NO_HARVEST') {
                            return;
                        }
                        // If dataLevel is plot and status is not null, return. TEMPORARY
                        else if(dataLevel == 'plot' && status !== null) {
                            return;
                        }
                    }
                    if(thresholdExceeded) {
                        return;
                    }
                    // Calculate total time consumed
                    var deleteExecutionTimeMillis = new Date().getTime() - processStart;
                    // Reset modal UI
                    $("#hm-hd-bulk-delete-modal-body").show();
                    $('#hm-hd-bd-delete-progress').html('');
                    // Display toast notification
                    var notif = "<i class='material-icons green-text'>check</i>&nbsp;Harvest traits were successfully deleted (" + (deleteExecutionTimeMillis*.001).toFixed(3) + "s)";
                    if(displayToast) Materialize.toast(notif, 2500);
                    // Close modal
                    $("#hm-hd-bulk-delete-modal.in").modal('hide');
                    // Reset the browser
                    resetBrowser();
                }
                return;
            }

            // If job is not yet done, recursively call on the function to check again
            getRecordUpdateProgress(jobId, recordIdsToDelete, status);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Perform deletion of uncommitted traits via processor
 * @param {object} recordIdsToDelete object containing records to be updated
 */
function deleteUncomittedTraits(recordIdsToDelete) {
    // Store ids with terminal data
    getRecordsWithTerminalData(recordIdsToDelete);
    // Update text area
    updateTextArea(" > Retrieving uncommitted trait ids...\\n");
    // Get record ids array
    recordIdsArray = getRecordIdArray(recordIdsToDelete);
    // Mark start time of deletion
    var start = new Date().getTime();
    // Send request to retrieve trait ids to delete
    $.ajax({
        url: '$urlGetUncommittedTraitIds',
        data: {
            recordIds: JSON.stringify(recordIdsArray),
            selectedAbbrevs: JSON.stringify(selectedAbbrevs),
            locationId: locationId
        },
        type: 'POST',
        async: true,
        success: function(data) {
            // Parse data to get ids
            var datasetIds = jQuery.parseJSON(data);
            // Calculate time consumed
            var executionTimeMillis = new Date().getTime() - start;
            // If no traits found, proceed to the next step
            if(datasetIds.length == 0) {
                updateTextArea(" > 0 UNCOMMITTED TRAITS FOUND. (" + (executionTimeMillis*.001).toFixed(3) + "s)\\n");

                start = new Date().getTime();
                if(committedSelected) {
                    resetBrowser();
                    performDelete(recordIdsToDelete);
                }
                else {
                    noHarvestData = true;
                    // updateRecordsStatus(recordIdsArray); // TEMPORARILY DISABLED
                    // TEMPORARY CHANGE FOR CROSSES
                    // Update status to NO_HARVEST because
                    // validation can take time
                    if (dataLevel == 'cross') {
                        updateRecordsStatus(recordIdsArray, 'NO_HARVEST');
                    }
                    else {
                        updateRecordsStatus(recordIdsArray);
                    }
                }
                return;
            }
            // If traits found, proceed with deletion
            else {
                // Update text area
                updateTextArea(" > UNCOMMITTED TRAIT IDS RETRIEVED. (" + (executionTimeMillis*.001).toFixed(3) + "s)\\n");
                updateTextArea(" > Deleting uncommitted traits...\\n");
                // Mark start time
                start = new Date().getTime();
                // Send request to delete datasets via processor
                $.ajax({
                    url: '$urlDeleteDatasets',
                    data: {
                        datasetIds: JSON.stringify(datasetIds),
                        locationId: locationId
                    },
                    type: 'POST',
                    async: true,
                    success: function(data) {
                        // Parse data to get job id
                        var job = JSON.parse(data);
                        var jobId = job.backgroundJobDbId != null ? job.backgroundJobDbId : 0;

                        // If jobId is 0, display error
                        if(jobId == 0) {
                            $('.hm-hd-bd-delete-btn').show();
                            ajaxGenericError(false);
                            return;
                        }

                        // Track deletion progress
                        getDatasetDeleteProgress(jobId, recordIdsToDelete);
                    },
                    error: function() {
                        
                    }
                });
            }
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Track dataset deletion progress given the job id
 * @param {integer} jobId background job identifier
 * @param {object} recordIdsToDelete object containing records to be updated
 */
function getDatasetDeleteProgress(jobId, recordIdsToDelete) {
    // Send request to get background job info
    $.ajax({
        url: '$urlGetBackgroundJob',
        data: {
            jobId: jobId
        },
        type: 'POST',
        async: true,
        success: function(data) {
            // Parse data to get job id and job status
            var result = jQuery.parseJSON(data);
            var job = result.data != null ? result.data : [];
            var jobId = job.backgroundJobDbId != null ? job.backgroundJobDbId : 0;
            var jobStatus = job.jobStatus != null ? job.jobStatus : 0;

            // If jobId is 0, display error
            if(jobId == 0) {
                $('.hm-hd-bd-delete-btn').show();
                ajaxGenericError(false);
                return;
            }

            // If job status is DONE or FAILED
            if(jobStatus=='DONE' || jobStatus=='FAILED') {
                // Update text area
                updateTextArea(" > UNCOMMITTED TRAITS DELETED.\\n");
                // If committed flag is true, proceed to deletion of committed traits
                if(committedSelected) {
                    resetBrowser();
                    performDelete(recordIdsToDelete);
                    // updateRecordsStatus(withTerminalData, null, false); // TEMPORARILY DISABLED
                    // TEMPORARY CHANGE FOR CROSSES
                    // Update status to NO_HARVEST because
                    // validation can take time
                    if (dataLevel == 'cross') {
                        updateRecordsStatus(withTerminalData, 'NO_HARVEST', false);
                    }
                    else {
                        updateRecordsStatus(withTerminalData, null, false);
                    }
                }
                // Else, update status back to NO_HARVEST
                else {
                    noUncommitted = true;
                    // updateRecordsStatus(recordIdsToDelete); // TEMPORARILY DISABLED
                    // TEMPORARY CHANGE FOR CROSSES
                    // Update status to NO_HARVEST because
                    // validation can take time
                    if (dataLevel == 'cross') {
                        updateRecordsStatus(recordIdsToDelete, 'NO_HARVEST');
                    }
                    else {
                        updateRecordsStatus(recordIdsToDelete);
                    }
                }
                return;
            }

            // If job is not DONE, recursively call function to check again
            getDatasetDeleteProgress(jobId, recordIdsToDelete);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Perform deletion of harvest data, and harvest records (if any)
 * via the processor API.
 * @param {object} rows object containing record information
 */
function performDelete(rows) {
    updateTextArea(" > Retrieving seeds...\\n");
    hasSeeds = false;
    hasEntries = false;
    thresholdExceeded = false;
    noHarvestData = false;
    noHarvestDataTriggered = false;
    var start = new Date().getTime();
    numberOfSeeds = 0;

    // Check plot data count
    harvestDataCount = countHarvestData(rows);
    // If uncommitted switch is false, get record array
    rows = getRecordIdArray(rows);

    // 1. Check number of seeds
    $.ajax({
        url: '$urlGetSeedCount',
        data: {
            recordIds: JSON.stringify(rows),
            dataLevel: dataLevel
        },
        type: 'POST',
        async: true,
        success: function(seedCount) {
            seedCount = parseInt(seedCount);
            numberOfSeeds = seedCount;

            // Update progress message
            var executionTimeMillis = new Date().getTime() - start;
            var s = 's';
            if(seedCount == 1) s = '';
            updateTextArea(" > DONE. Found " + seedCount + " seed" + s + ". (" + (executionTimeMillis*0.001).toFixed(3) + "s)\\n");

            // If no seeds found, proceed to plot data deletion
            if(seedCount == 0) {
                retrieveDatabaseIds(rows);
                return;
            }
            // If seed count exceeds threshold, send process to background worker
            else if(harvestDataCount>harvestDataThreshold || numberOfSeeds>seedThreshold) {
                deleteInBackground(rows);
                return;
            }

            hasSeeds = true;
            
            updateTextArea(" > Checking if seeds have been used in entries...\\n");

            // 2. Check if seeds have been used in entries
            start = new Date().getTime();
            $.ajax({
                url: '$urlCheckSeedEntries',
                data: {
                    recordIds: JSON.stringify(rows),
                    dataLevel: dataLevel
                },
                type: 'POST',
                async: true,
                success: function(data) {
                    var data = jQuery.parseJSON(data);
                    recordDbIdsToDelete = jQuery.parseJSON(data.recordsToDelete);
                    
                    // If there are records with seeds already
                    // used in entries, display acknowledgement
                    if(data.recordsUnableToDeleteCount>0) {
                        hasEntries = true;
                        var executionTimeMillis = new Date().getTime() - start;
                        updateTextArea(" > DONE. Entries found for seeds of " + data.recordsUnableToDeleteCount + " record(s). (" + (executionTimeMillis*0.001).toFixed(3) + "s)\\n");
                        displayAcknowledgement(recordDbIdsToDelete, data.recordsUnableToDelete);
                        return;
                    }

                    var executionTimeMillis = new Date().getTime() - start;
                    updateTextArea(" > DONE. No entries found. (" + (executionTimeMillis*0.001).toFixed(3) + "s)\\n");

                    retrieveDatabaseIds(recordDbIdsToDelete);
                },
                error: function() {
                    ajaxGenericError();
                }
            });
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Retrieve ids to be deleted
 * @param {array} recordDbIdsToDelete record ids included in the deletion
 */
function retrieveDatabaseIds(recordDbIdsToDelete) {
    if(hasSeeds) updateTextArea(" > Retrieving database ids...\\n");
    else updateTextArea(" > Retrieving harvest data ids...\\n");

    var start = new Date().getTime();
    $.ajax({
        url: '$urlGetDatabaseIds',
        data: {
            recordDbIds: JSON.stringify(recordDbIdsToDelete),
            selectedAbbrevs: JSON.stringify(selectedAbbrevs),
            hasSeeds: hasSeeds,
            dataLevel: dataLevel
        },
        type: 'POST',
        async: true,
        success: function(databaseIdStr) {
            var executionTimeMillis = new Date().getTime() - start;
            var databaseIds = JSON.parse(databaseIdStr);
            var deleteIdCount = databaseIds.seedDbIds.length + 
                databaseIds.germplasmDbIds.length + databaseIds.harvestDataDbIds.length;

            // If there are records to delete, pass ids to processor
            if(deleteIdCount > 0)  {
                updateTextArea(" > DATABASE IDS RETRIEVED. (" + (executionTimeMillis*.001).toFixed(3) + "s)\\n");
                deleteViaProcessor(databaseIdStr);
                return;
            }
            if(databaseIds.harvestDataDbIds.length == 0) {
                updateTextArea(" > NO RECORDS FOUND. (" + (executionTimeMillis*.001).toFixed(3) + "s)\\n");
                if(uncommittedSelected && !noUncommitted) noHarvestData = false;
                else noHarvestData = true;
                // updateRecordsStatus(recordDbIdsToDelete); // TEMPORARILY DISABLED
                // TEMPORARY CHANGE FOR CROSSES
                // Update status to NO_HARVEST because
                // validation can take time
                if (dataLevel == 'cross') {
                    updateRecordsStatus(recordDbIdsToDelete, 'NO_HARVEST');
                }
                else {
                    updateRecordsStatus(recordDbIdsToDelete);
                }
            }
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Perform deletion of records via processor
 * @param {string} databaseIdStr string encoded record ids included in the deletion
 */
function deleteViaProcessor(databaseIdStr) {
    if(hasSeeds) updateTextArea(" > Deleting records...\\n");
    else updateTextArea(" > Deleting harvest data...\\n");

    $.ajax({
        url: '$urlDeleteViaProcessor',
        data: {
            databaseIdStr: databaseIdStr,
            locationId: locationId,
            dataLevel: dataLevel
        },
        type: 'POST',
        async: true,
        success: function(data) {
            var backgroundJobs = jQuery.parseJSON(data);
            var backgroundJobIds = [];
            // Get background job ids
            if(backgroundJobs.seed != null) backgroundJobIds.push(backgroundJobs.seed.backgroundJobDbId);
            if(backgroundJobs.germplasm != null) backgroundJobIds.push(backgroundJobs.germplasm.backgroundJobDbId);
            if(backgroundJobs.harvestData != null) backgroundJobIds.push(backgroundJobs.harvestData.backgroundJobDbId);

            var startDelete = new Date().getTime();

            getDeleteProgress(backgroundJobIds,startDelete,databaseIdStr);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Tracks the progress of the deletion of records
 * by checking the job status of the background jobs
 * @param {array} backgroundJobIds array containing background job ids
 * @param {integer} startDelete start time of the deletion
 */
function getDeleteProgress(backgroundJobIds, startDelete, databaseIdStr) {
    $.ajax({
        url: '$urlGetBackgroundJobs',
        data: {
            backgroundJobIds: JSON.stringify(backgroundJobIds),
        },
        type: 'POST',
        async: true,
        success: function(data) {
            var data = jQuery.parseJSON(data);
            var jobs = data.backgroundJobs;
            var jobsLength = jobs.length;
            var jobsDone = true;
            
            // Check if all jobs are done
            for(var i = 0; i < jobsLength; i++) {
                jobsDone = jobsDone && (jobs[i].jobStatus == 'DONE' || jobs[i].jobStatus == 'FAILED');
            }

            // If all jobs are done, proceed to record status update
            if(jobsDone) {
                var ids = JSON.parse(databaseIdStr);
                var records = ids.recordDbIds
                // updateRecordsStatus(records, null, true, false); // TEMPORARILY DISABLED
                // TEMPORARY CHANGE FOR CROSSES
                // Update status to NO_HARVEST because
                // validation can take time
                if (dataLevel == 'cross') {
                    updateRecordsStatus(records, 'NO_HARVEST', true, false);
                }
                else {
                    updateRecordsStatus(records, null, true, false);
                }
                return;
            }

            // If jobs are not all done yet, recusively call the function to check again
            getDeleteProgress(backgroundJobIds,startDelete,databaseIdStr);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Invoke background worker for deletion
 * @param {array} recordIds array containing record ids
 */
function deleteInBackground(recordIds) {
    updateTextArea(" > Sending data to background worker...\\n");

    $.ajax({
        url: '$urlDeleteInBackground',
        data: {
            occurrenceId: occurrenceId,
            recordIds: JSON.stringify(recordIds),
            selectedAbbrevs: JSON.stringify(selectedAbbrevs),
            dataLevel: dataLevel
        },
        type: 'POST',
        async: true,
        success: function(data) {
            var result = JSON.parse(data);
            var success = result.success;
            var status = result.status;
            
            // Display flash message
            if(success) {
                // Success message
                displayCustomFlashMessage(
                    'info', 
                    'fa fa-info-circle',
                    'The harvest data, packages, seeds, and germplasm are being deleted in the background. You may proceed with other tasks. You will be notified in this page once done.',
                    'hm-bg-alert-div',
                    10
                );
            }
            else {
                // Failure message
                let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                // If status is not 200, the problem occurred while creating the background job record (code: #001)
                if (status != null && status != 200) message += ' (#001)';
                // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                else if(status == null) message += ' (#002)';

                displayCustomFlashMessage(
                    'warning', 
                    'fa-fw fa fa-warning', 
                    message,
                    'hm-bg-alert-div'
                );
            }

            // Reset modal UI
            $("#hm-hd-bulk-delete-modal-body").show();
            $('#hm-hd-bd-delete-progress').html('');
            // Close the modal
            $("#hm-hd-bulk-delete-modal").modal('hide');
            // Reset the browser
            resetBrowser();
            return;
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

function displayAcknowledgement(recordDbIdsToDelete, recordsUnableToDelete) {
    // Get info of plots unable to delete
    // $("#delete_confirmation_modal_additional_main").hide();
    $("#hm-hd-bulk-delete-confirmation-modal-additional-main").hide();
    $.ajax({
        url: '$urlAcknowledgeRecords',
        data: {
            recordIds: recordsUnableToDelete,
            occurrenceId: occurrenceId,
            dataLevel: dataLevel
        },
        type: 'POST',
        async: true,
        success: function(data) {
            lastKnownRecords = recordDbIdsToDelete;
            lastKnownRecordsToRevert = jQuery.parseJSON(recordsUnableToDelete);
            $('.hm-hd-bd-confirmation-acknowledge-grp').removeClass('disabled');
            $('.hm-hd-bd-confirmation-acknowledge-grp').show();
            $('.hm-hd-bd-confirmation-default-grp').hide();
            $("#hm-hd-bulk-delete-modal").modal('hide');
            $("#hm-hd-bulk-delete-confirmation-modal-body").html(data);
            $("#hm-hd-bulk-delete-confirmation-modal").modal('show');
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Displays generic toast error message when an ajax call fails.
 * @param {boolean} reset whether or not to reset the browser after displaying the error. Defaults to true.
 */
function ajaxGenericError(reset = true) {
    $("#hm-hd-bulk-delete-modal-body").show();
    $('#hm-hd-bd-delete-progress').html('');
    var notif = "<i class='material-icons red-text'>close</i>&nbsp;An error occurred. Bulk delete unsuccessful.";
    Materialize.toast(notif, 3500);
    // Close modal
    $("#hm-hd-bulk-delete-modal.in").modal('hide');
    // Reset the browser
    if(reset) resetBrowser();
}

/**
 * Builds contents of the confirmation modal
 * @param {object} records object containing record information
 */
function buildConfirmationModal(records) {
    $("#hm-hd-bulk-delete-confirmation-modal-additional-main").hide();
    // Set variables
    var totalCount = records.length;
    var traitsToDelete = "";
    // Check which traits are to be deleted
    if(uncommittedSelected && committedSelected) traitsToDelete = "committed and uncommitted";
    else if(uncommittedSelected) traitsToDelete = "uncommitted";
    else if(committedSelected) traitsToDelete = "committed";
    // Build toDelete string
    var toDelete = confirmationMessageBuilder();
    var notice = "<br><p style='font-size:115%;'>You are deleting the " + toDelete + " of <b>" + totalCount + " record(s)</b>."
            + "<br><br>Are you sure you want to delete these?</p>";
    var recordsCopy = eliminateRecordsWithoutTraits(records);
    var deletableCount = recordsCopy.length;
    // If the total count is not equal to the deletable count, display notice
    if(totalCount != deletableCount) {
        // If there is nothing to delete, display notice
        if(deletableCount == 0) {
            var redNotice = "<p style='font-size:115%;'>Bulk delete cannot be performed because no records have " + toDelete + ".</p>";
            $("#hm-hd-bulk-delete-confirmation-modal-additional-content").html(redNotice);
            $("#hm-hd-bulk-delete-confirmation-modal-additional-main").show();
            $('#hm-hd-bulk-delete-confirmation-modal-body').html('');
            $('.hm-hd-bd-confirmation-default-grp').hide();
            $('#hm-hd-bd-confirmation-cancel-btn').show();
            $('#hm-hd-bd-confirmation-cancel-btn').removeClass('disabled');
            return;
        }
        notice = "<br><p style='font-size:115%;'>You are deleting the " + toDelete + " of <b>" + deletableCount + " record(s)</b>."
            + "<br><br>Are you sure you want to delete these?</p>";

        var redNotice = "<p><b>" + (totalCount - deletableCount) + " record(s)</b> do not have <b>" + traitsToDelete + "</b> harvest traits and will be skipped.";
        $("#hm-hd-bulk-delete-confirmation-modal-additional-content").html(redNotice);
        $("#hm-hd-bulk-delete-confirmation-modal-additional-main").show();
    }
    // Display main notice
    $('#hm-hd-bulk-delete-confirmation-modal-body').html(notice);
    $('.hm-hd-bd-confirmation-default-grp').show();
    $('.hm-hd-bd-confirmation-default-grp').removeClass('disabled');
}

/**
 * Builds the string based on which data will be deleted
 */
function confirmationMessageBuilder() {
    var message = '';
    var data = [];
    var types = [];

    // If committed checkbox is selected, add 'seeds' and 'packages' to data,
    // and add 'committed' to type
    if (committedSelected) {
        data.push('seeds');
        data.push('packages');
        types.push('committed');
    }
    // If uncommitted checkbox is selected, add 'uncommitted' to type
    if (uncommittedSelected) types.push('uncommitted');
    // join array items with "and"
    var dataToDelete = data.join(" and ");
    var typesToDelete = types.join(" and ");
    // Build final message
    message = "<b><u>" + typesToDelete + " harvest traits</u></b>";
    if(dataToDelete !== "") message = "<b><u>" + dataToDelete + "</u></b> and " + message;

    return message;
}

/**
 * Removes records without traits from the given rows
 */
function eliminateRecordsWithoutTraits(rows) {
    var newRows = [];

    for(var i = 0; i < rows.length; i++) {
        var row = rows[i];

        // If committed checkbox is selected,
        // only take records with committed data
        if(committedSelected) {
            var hasValue = false;
            for(var abbrev of selectedAbbrevs) {
                var fieldName = abbrevFieldNameArray[abbrev]

                if( row.fieldName !== '' ){
                    hasValue = true;
                    break;
                }
            }
            if(hasValue) {
                newRows.push(row);
                continue;
            }
        }

        // If uncommitted checkbox is selected,
        // only take records with uncommitted data
        if(uncommittedSelected) {
            var hasValue = false;
            for(var abbrev of selectedAbbrevs) {
                var fieldName = abbrevFieldNameArray[abbrev]

                var newFieldName = fieldName.charAt(0).toUpperCase() + fieldName.slice(1)
                var terminalFieldName = 'terminal' + newFieldName

                if( row.terminalFieldName !== ''){
                    hasValue = true;
                    break;
                }
            }
            if(hasValue) {
                newRows.push(row);
                continue;
            }
        }
    }

    return newRows;
}

/**
 * Disable checkbox based on what data is available
 */
function disableCheckboxes() {
    $("#hm-hd-bd-trait-type-chk-uncommitted").removeAttr("disabled");
    $("#hm-hd-bd-trait-type-chk-committed").removeAttr("disabled");
    if(!traitsStatus.hasCommitted && traitsStatus.hasTerminal) {
        uncommittedSelected = true;
        $('.hm-hd-bd-trait-checkbox-div').show();
        $("#hm-hd-bd-trait-type-chk-committed").attr("disabled", true);
        $("#hm-hd-bd-trait-type-chk-uncommitted").click();
        $("#hm-hd-bd-trait-type-chk-uncommitted").attr("disabled", true);
        $('#delete-type-remarks-div').html('<div class="grey lighten-3 grey-text text-darken-2" id="delete-type-remarks-div-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 5px; border-radius: 5px"><p style="font-size:115%;">Uncommitted Traits has been automatically selected. '
        +'The selected records only have uncommitted traits.'
        +'</p></div>');
    }
    else if(traitsStatus.hasCommitted && !traitsStatus.hasTerminal) {
        committedSelected = true;
        $('.hm-hd-bd-trait-checkbox-div').show();
        $("#hm-hd-bd-trait-type-chk-committed").click();
        $("#hm-hd-bd-trait-type-chk-uncommitted").attr("disabled", true);
        $("#hm-hd-bd-trait-type-chk-committed").attr("disabled", true);
        $('#delete-type-remarks-div').html('<div class="grey lighten-3 grey-text text-darken-2" id="delete-type-remarks-div-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 5px; border-radius: 5px"><p style="font-size:115%;">Committed Traits has been automatically selected. '
        +'The selected records only have committed traits.'
        +'</p></div>');
    }

    adjustButtonDisplay();
}

/**
 * Check if either one of the type checkboxes are selected.
 * If true, display the checkboxes for harvest data.
 * Else, hide checkboxes for harvest data.
 */
function displayTraitCheckboxes() {
    if(uncommittedSelected || committedSelected) {
        $(".hm-hd-bd-trait-checkbox-div").show();
    }
    else {
        $(".hm-hd-bd-trait-checkbox-div").hide();
        var checkboxClass = "hm-hd-bd-checkbox-item";
        $('.'+checkboxClass).prop('checked',false);
        $('.'+checkboxClass).removeAttr('checked');
        $('#hm-hd-bd-checkbox-all').prop('checked',false);
        $('#hm-hd-bd-checkbox-all').removeAttr('checked');
        selectedAbbrevs = [];
    }
}

/**
 * Check if all items in the current page are selected.
 * @return {boolean} whether or not to tick the select all checkbox element
 */
function tickSelectAll() {
    var checkboxClass = "hm-hd-bd-checkbox-item";
    var result = true;
    // Check each checkbox if checked
    $.each($('.'+checkboxClass), function(i, val) {
        result = result && $(this).prop("checked");
    });
    // Return result (true or false)
    return result;
}

/**
 * Adjust buttons on checkbox input
 */
function adjustButtonDisplay() {
    var selectedAbbrevCount = selectedAbbrevs.length;
    if((uncommittedSelected || committedSelected) && selectedAbbrevCount > 0) {
        $('#hm-hd-bulk-delete-all-btn').removeClass('disabled');
        if(recordCount > 0) {
            $('#hm-hd-bulk-delete-selected-btn').removeClass('disabled');
        }
    }
    else {
        $('#hm-hd-bulk-delete-all-btn').addClass('disabled');
        $('#hm-hd-bulk-delete-selected-btn').addClass('disabled');
    }
}

/**
 * Updates the deletion process text area with the given message
 * @param {string} message message to add to the text area
 */
function updateTextArea(message) {
    var textArea = $('#delete-progress-textarea');
    textArea.val(textArea.val() + message);
    if(typeof textArea[0] != 'undefined') textArea.scrollTop(textArea[0].scrollHeight);
}

/**
 * Returns an array of ids given the record info object
 * @param {object} rows object containing record information
 */
function getRecordIdArray(rows) {
    var ids = [];
    for(var i = 0; i<rows.length; i++) {
        var row = rows[i];

        if( row !== undefined 
            && row.plotDbId !== undefined 
            && row.crossDbId == undefined){
            ids.push(row.plotDbId);
        }
        else if( row !== undefined 
            && row.crossDbId !== undefined 
            && row.plotDbId == undefined){
            ids.push(row.crossDbId);
        }
        else return rows;
    }
    return ids;
}

/**
 * Remembers ids that have terminal data
 * @param {object} rows object containing record information
 */
function getRecordsWithTerminalData(rows) {
    withTerminalData = [];
    
    for(var i = 0; i < rows.length; i++) {
        var row  = rows[i];

        var terminalColumns = Object.keys(row).filter(function (x){
             return x.includes('terminal') 
        });

        for(column of terminalColumns){
            if( row[column] != undefined 
                && row[column] !== null 
                && row[column] !== ""){
                withTerminalData.push(row.plotDbId)
            }
        }
    }
}

/**
 * Counts the harvest data included in the rows
 * @param {object} rows object containing record information
 */
function countHarvestData(rows) {
    var count = 0;

    for(var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var terminalColumns = Object.keys(row).filter(function (x){
             return x.includes('terminal') 
        });

        for(column of terminalColumns){
            column = column.replaceAll('terminal','')

            if( row[column] != undefined 
                && row[column] !== null 
                && row[column] !== ""){
                count++;
            }
        }
    }

    return count;
}

/**
 * Reset the browser and clear selection after bulk update
 */
function resetBrowser() {
    // Clear selection on grid reset
    var gridId = 'dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-table-con';
    var selectionStorageId = occurrenceId + "_" + gridId;
    clearSelectionHM(selectionStorageId);
    var pageurl = $('#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid li.active a').attr('href');
    $.pjax.reload(
        {
            container:'#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-pjax',
            url: pageurl,
            replace: false,
            type: 'POST',
            data: {
                forceReset:false,
                bulkOperation:true
            }
        }
    );
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} selectionStorageId unique string identifier hoolding the selected items
 * @param {boolean} all whether or not all checkbox selections are to be cleared
 */
function clearSelectionHM(selectionStorageId, all = false) {
    // If all flag is set to true, clear selection for all supported grids
    if (all) {
        // Loop through supported grids
        for(var i = 0; i < supportedCount; i++) {
            // Build selection identifier
            var currentSession = occurrenceId + "_" + supportedGrids[i];
            // Remove selection from storage
            sessionStorage.removeItem(currentSession);
        }
    }
    // If all flag is false, remove specified  selection id
    else sessionStorage.removeItem(selectionStorageId);
}

/**
 * Displays a message to inform users when a threshold is exceeded
 * and the background processing is not available.
 * @param {integer} numberOfSeeds seed count
 * @param {integer} harvestDataCount harvest data count
 */
function backgroundUnavailable(numberOfSeeds, harvestDataCount) {
    // Enable button
    $('#hm-hd-bulk-delete-done-btn').removeClass('disabled');
    $('#hm-hd-bulk-delete-done-btn').show();

    // Build specific message based on which threshold exceeded
    var specificMessage = '';
    var count = '';
    if(harvestDataCount>harvestDataThreshold) {
        count = numberWithCommas(harvestDataCount);
        threshold = numberWithCommas(harvestDataThreshold);
        specificMessage = 'The harvest data count ( ' + count + ') exceeds the limit (' + threshold + ')';
    }
    else if(numberOfSeeds>seedThreshold) {
        count = numberWithCommas(numberOfSeeds);
        threshold = numberWithCommas(seedThreshold);
        specificMessage = 'The seed count (' + count + ') exceeds the limit (' + threshold + ')';
    }
    // Build final message
    var finalMessage = specificMessage + ". Background processing is not available at the moment. Kindly select records that do not exceed the limit and try again.";
    // Show notice
    var redNotice = "<p style='font-size:115%;'>" + finalMessage + "</p>";
    $('#hm-hd-bulk-delete-red-content').html(redNotice);
    $('#hm-hd-bulk-delete-red-content').show();
    $('#hm-hd-bd-delete-progress').hide();
    return;
}

/**
 * Formats numbers to comma-separated string per hundreds.
 * @param {string} num number to be formatted
 * @returns string number formatted with commas
 * Examples:
 * 4        -> 4
 * 100      -> 100
 * 1234     -> 1,234
 * 9876543  -> 9,876,543
 */
function numberWithCommas(num) {
    var pieces = num.toString().split(".");
    pieces[0] = pieces[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return pieces.join(".");
}

/**
 * Displays custom flash message
 * @param {string} status the status of the message (info, success, warning, error)
 * @param {string} iconClass icon of flash message
 * @param {string} message the message to display
 * @param {string} parentDivId id of the div where the flash message is to be inserted
 * @param {float} time time in seconds before the flash message is removed from the display. 7.5 seconds by default.
 */
function displayCustomFlashMessage(status, iconClass, message, parentDivId, time = 7.5) {
    // Add flash message to parent div
    $('#' + parentDivId).html(
        '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
            '<div class="card-panel">' +
                '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                message +
                '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
            '</div>' +
        '</div>'
    );

    // remove flash message after the specified time in seconds (7.5 seconds by default)
    setTimeout(
        function() 
        {
            $('#w25-' + status + '-0').fadeOut(250);
            setTimeout(
                function() 
                {
                    $('#' + parentDivId).html('');
                }
            , 500);
        }
    , 1000 * time);
}

JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    .hm-hd-bd-checkbox-item-non-num-var {
        margin-left:30px;
    }

    .hm-hd-bd-checkbox-item-num-var {
        margin-left:60px;
    }
');
?>