<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Additional harvest modal contents
 */
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\date\DatePicker;
\kartik\select2\Select2Asset::register($this);

// Define Urls
$urlRetrieveAllRecordInfo = Url::to(['harvest-data/retrieve-all-record-info']);
$urlInsertData = Url::to(['harvest-data/insert-data']);
$urlUpdateRecordsStatus = Url::to(['harvest-data/update-records-status']);
$urlGetBackgroundJob = Url::to(['harvest-data/get-background-job']);
$urlCreate = Url::to(['creation/create-packages']);

// Set variables
$recordCount = count($recordIds);
$recordIdsJson = json_encode($recordIds);
$bulkUpdateConfigJSON = json_encode($bulkUpdateConfig);
$recordInfoJSON = json_encode($recordInfo);
$hiddenClass = '';
// Create date picker for harvest date input
$hvDatePicker = DatePicker::widget([
    'name' => 'hm-addl-harvest-input-hdate',
    'id' => 'hm-addl-harvest-input-hdate',
    'class' => 'hm-addl-harvest-input-hdate hm-addl-harvest-input',
    'value' => '',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true,
        'clearBtn' => true,
        'startDate'=> date("1970-01-01")
    ],
    'pluginEvents' => [
        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
    ],
    'options' => [
        'autocomplete' => 'off',
        'placeholder' => 'Select harvest date',
        'variableAbbrev' => 'HVDATE_CONT'
    ]
]);
// Create select2 for harvest method input
$hvMethodSelect = Select2::widget([
    'name' => 'hm-addl-harvest-input-hmethod',
    'id' => 'hm-addl-harvest-input-hmethod',
    'class' => 'hm-addl-harvest-input-hmethod hm-addl-harvest-input ' . $hiddenClass,
    'data' => $bulkUpdateConfig["harvest_methods"],
    'options' => [
        'placeholder' => 'Select method',
        'variableAbbrev' => 'HV_METH_DISC'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);
// Create select2 for grain color input
$variableScaleValues = (array) $bulkUpdateConfig['variable_scale_values'];
$grainColorScale = !empty($variableScaleValues['grainColor']) ? (array) $variableScaleValues['grainColor'] : [];
$showGrainColor = array_key_exists('GRAIN_COLOR',(array) $bulkUpdateConfig['variable_field_names']) ? '' : ' hidden';
$grainColorSelect = Select2::widget([
    'name' => 'hm-addl-harvest-input-graincolor',
    'id' => 'hm-addl-harvest-input-graincolor',
    'class' => 'hm-addl-harvest-input-graincolor hm-addl-harvest-input ' . $showGrainColor,
    'data' => $grainColorScale,
    'options' => [
        'placeholder' => 'Select grain color',
        'variableAbbrev' => 'GRAIN_COLOR'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);

?>

<?php
    // Display number of selected items
    $recordCount = count($recordIds);
    $countString = $recordCount == 0 ? 'No' : $recordCount;
    $itemType = '';
    if($dataLevel == 'plot') {
        if($recordCount != 1) $itemType = 'plots';
        else $itemType = $dataLevel;
    }
    else if($dataLevel == 'cross') {
        if($recordCount != 1) $itemType = 'crosses';
        else $itemType = $dataLevel;
    }
    $count = $countString." selected ".$itemType;
?>

<div class="panel panel-default" style=" min-height:100px; margin-top:10px">
    <div class="row" style="margin-left:5px; margin-top:5px;padding-top:5px; padding-right:25px; padding-left:10px;">
        <div class="collapsible-header active" style="margin-bottom:10px; font-size:14px">
            <div class="col-md-6">
                <i class="material-icons">list</i> <?= \Yii::t('app',"Harvest Traits") ?>
            </div>
            <div class="col-md-6" style="padding: 0px 0px 0px 0px;">
                <p class="float-right">
                    <?= \Yii::t('app',$count) ?>
                </p>
            </div>
        </div>
        <div style="margin-left:20px">
            <?php
                // Render input fields
                echo
                '<div class="harvest-data-input-div col-md-6" style="font-size:14px">
                    <dl>
                        <dt title="harvest-date">Harvest Date</dt>
                        <dd>'.$hvDatePicker.'</dd>
                    </dl>
                </div>
                <div class="harvest-data-input-div col-md-6 ' . $hiddenClass . '" style="font-size:14px">
                    <dl>
                        <dt title="harvest-method" style="margin-bottom:5px">Harvest Method</dt>
                        <dd>'.$hvMethodSelect.'</dd>
                    </dl>
                </div>' . $numVarHTML
                .
                '<div class="harvest-data-input-div col-md-12 ' . $showGrainColor . '" style="font-size:14px">
                    <dl>
                        <dt title="grain-color" style="margin-bottom:5px">Grain Color</dt>
                        <dd>'.$grainColorSelect.'</dd>
                    </dl>
                </div>'
                ;
            ?>
        </div>
    </div>
</div>

<?php

$script = <<< JS
// Set variables
var occurrenceId = $occurrenceId;
var locationId = $locationId;
var dataLevel = "$dataLevel";
var bulkUpdateConfig = $bulkUpdateConfigJSON;
var selectedRecords = $recordInfoJSON;
var allRecords = {};
var selectedRecordsCount = $recordCount;
var allRecordsCount = $allRecordsCount;
var crossMetHarvMetCompat = bulkUpdateConfig['cmet_hmet_compat'];
var gStateGTypeHMetCompat = bulkUpdateConfig['gstate_gtype_hmet_compat'];
var variableFieldNames = bulkUpdateConfig['variable_field_names'];
var variablePlaceholder = bulkUpdateConfig['variable_placeholder'];
var variableFPMapping = bulkUpdateConfig['variable_field_placeholder_mapping'];
var variableCompat = bulkUpdateConfig['variable_compat'];
var numVarCompat = bulkUpdateConfig['num_var_compat'];
var stateHMethodCompat = {};
var numVarCompatKeys = Object.keys(numVarCompat);
var kCount = numVarCompatKeys.length;
var variableAbbrevArray = [];
var values = [];
var hasInvalidNumVar = false;

// Variables for validation
var varAbb = [];
var vals = [];
var numVarAbbrev = '';
var incompatibleRecords = [];
var overwriteRecords = [];
var missingCMethodRecords = [];
var unsupportedStateRecords = [];
var tcvRecords = [];
var hStatusIncompatObject = { statusVals: [], recordIds: [] };
var variableIncompatObject = { variables: [] , recordIds: [] };
var methodStateIncompatObject = { states: [] , recordIds: [] };
var methodStateTypeIncompatObject = { types: [] , recordIds: [] };
var numVarMethodIncompatObject = { numvar: "", methods: [] , recordIds: [] };
var numVarCrossMethodIncompatObject = { numvar: "", crossMethods: [] , recordIds: [] };
var harvestMethodCrossMethodIncompatObject = { harvestMethod: "", crossMethods: [], recordIds: []};
var mode = '';

// Reset modal
$('#hm-hd-addl-harvest-modal-body').show();
$('#update-progress').html('');
buildStateHMethodCompat();

$(document).ready(function() {
    // Harvest date datepicker change event
    $(document).on('change', "#hm-addl-harvest-input-hdate", function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        inputChange(value, variableAbbrev, index);
    });

    // Harvest method select2 change event
    $(document).on('change', "#hm-addl-harvest-input-hmethod", function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        inputChange(value, variableAbbrev, index);

        // If the input value for harvest method is empty, show all numeric variable inputs
        if(value == '') {
            // If the number of numeric variables is more than 1, clear values for all numeric variables
            if(kCount > 1) $('.hm-addl-harvest-input-numvar').val('').trigger('change');
            $('.hm-addl-harvest-input-numvar').css({"background-color": ""});
            $('.hm-addl-harvest-input-numvar-div').show();
            return;
        }
        // Hide all numeric variable inputs
        $('.hm-addl-harvest-input-numvar-div').hide();
        // Check which numeric variable input to show (if applicable)
        for(var i = 0; i < kCount; i++) {
            var abbrev = numVarCompatKeys[i];
            var abbrevIndex = variableAbbrevArray.indexOf(abbrev);
            var compatObject = numVarCompat[abbrev];
            var allowedHarvestMethods = compatObject.harvestMethods;
            // Set element ids based on abbrev
            var inputId = '#hm-addl-harvest-input-'+abbrev;
            var divId = inputId+'-div';

            // If the input value for harvest method
            // is included in the allowed harvest methods for the numeric variable,
            // then display the input field for that numeric variable
            if(allowedHarvestMethods.includes(value)) {
                $(divId).show();
            }
            // Else, clear any inputs for all numeric variables
            else {
                hasInvalidNumVar = false;
                numVarAbbrev = '';
                $(inputId).val('').trigger('change');
                $(inputId).css({"background-color": ""});
                inputChange("", abbrev, abbrevIndex);
            }
        }
    });

    // Numeric variable input event
    $(document).on('input', '.hm-addl-harvest-input-numvar', function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        // If value is empty, show all numeric variable inputs
        if(value == '') {
            var hMethod = $("#hm-addl-harvest-input-hmethod").val();
            if(hMethod == "") $('.hm-addl-harvest-input-numvar-div').show();
            numVarAbbrev = '';
        }
        else {
            // Hide other numeric variable issues
            $('.hm-addl-harvest-input-numvar-div').hide();
            $('#hm-addl-harvest-input-' + variableAbbrev + '-div').show();
            numVarAbbrev = variableAbbrev;
        }

        // Validate input
        var subType = $(this).attr('subType');
        var minimum = $(this).attr('min');
        var placeholder = $(this).attr('placeholder');
        var regex = new RegExp('');
        // Format value based on sub type
        if(subType == 'single_int') {
            regex = new RegExp('^(' + minimum + '|[1-9][0-9]*)$');
            value = value != '' ? parseInt(value).toString() : '';
        }
        else if(subType == 'comma_sep_int') {
            value = cleanCommaSepInt(value);
            regex = new RegExp('^[1-9][0-9]*(,[1-9][0-9]*)*$');
        }
        // If input does not match regex, display error
        if(value != '' && !regex.test(value)) {
            $(this).css({"background-color": "#ffffcc"});
            hasInvalidNumVar = true;
            inputChange('', variableAbbrev, index);
            $('.hm-hd-addl-harvest-btn').addClass("disabled");
            return;
        }
        // Remove background color
        $(this).css({"background-color": ""});
        hasInvalidNumVar = false;
        inputChange(value, variableAbbrev, index);
    });

    // Grain color select2 change event
    $(document).on('change', "#hm-addl-harvest-input-graincolor", function() {
        // Get value and abbrev
        var value = $(this).val();
        var variableAbbrev = $(this).attr('variableAbbrev');
        var index = variableAbbrevArray.indexOf(variableAbbrev);

        inputChange(value, variableAbbrev, index);
    });

    // Confirm button click event
    $('#hm-hd-addl-harvest-btn-confirm').off('click').on('click', function() { 
        // Disable proceed button
        $('.hm-hd-addl-harvest-confirm-btn').show();
        $('#hm-hd-addl-harvest-proceed-btn').addClass('disabled');
        // Add loading indicator
        $('#hm-hd-addl-harvest-confirmation-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        // Hide additional info div
        $('#hm-hd-addl-harvest-confirmation-modal-additional-main').hide();

        var notice = "";
        var pSize = "";
        hStatusIncompatObject = { statusVals: [], recordIds: [] };
        incompatibleRecords = [];
        overwriteRecords = [];
        missingCMethodRecords = [];
        unsupportedStateRecords = [];
        variableIncompatObject = { variables: [] , recordIds: [] };
        methodStateIncompatObject = { states: [] , recordIds: [] };
        methodStateTypeIncompatObject = { types: [] , recordIds: [] };
        numVarMethodIncompatObject = { numvar: "", methods: [] , recordIds: [] };
        numVarCrossMethodIncompatObject = { numvar: "", crossMethods: [] , recordIds: [] };
        harvestMethodCrossMethodIncompatObject = { harvestMethod: "", crossMethods: [], recordIds: []};

        // CHECK DATA VAILIDTY
        // Check harvest status
        checkUnsupportedHStatus();
        /**
         * TCV check disabled in support of CORB-6631.
         * The code will be kept here, commented,
         * in case the check needs to be re-enabled.
         */
        // // Catch TCV use case
        // catchTCVUseCase();
        // Take note of records with missing cross methods
        checkMissingCrossMethod();
        // Take note of records with an unsupported state
        checkUnsupportedState();
        // Take note of records that are incompatible with the variable(s) selected
        checkVariableIncompatibility();
        // Take note of records that are incompatible with the harvest method due to the state
        checkMethodStateIncompatibility();
        // Take not of records that are incompatible with the harvest method due to the type
        checkMethodStateTypeIncompatibility();
        // Take note of records that are incompatible with the numeric variable due to the cross method
        checkNumVarCrossMethodIncompatibility();
        // Take note of records that are incompatible with the harvest method due to the cross method
        checkHarvestMethodCrossMethodIncompatibility();
        
        // Set variables
        var totalUpdatableCount = selectedRecords.length - incompatibleRecords.length;
        var harvestMethod = vals[varAbb.indexOf('HV_METH_DISC')];
        var currentNumVar = numVarMethodIncompatObject['numvar'];
        var currentNumVarCrossMethod = numVarCrossMethodIncompatObject['numvar'];
        var currentHarvestMethod = harvestMethodCrossMethodIncompatObject['harvestMethod'];

        // Build notice string
        notice = "<br><p style='font-size:115%;'>You are about to perform additional harvest for <b>" + totalUpdatableCount + " record(s)</b>."
            + " This operation will commit the new harvest data and send the harvest process to the background worker."
            + " You will be redirected to the Creation tab where you may keep track of the harvest."
            + "<br><br>Do you wish to proceed with the operation?</p>";

        // If no records can be updated, display red notice in the modal
        if(totalUpdatableCount==0) {
            notice = '';
            pSize = "<p style='font-size:115%;'>";
        }

        // Display the notice in the modal body
        $('#hm-hd-addl-harvest-confirmation-modal-body').html(notice);
        $('#hm-hd-addl-harvest-confirmation-modal-additional-main').hide();
        // If incompatibilities are found, build summary of incompatibilities
        if(incompatibleRecords.length>0) {
            // Display notice if all records cannot be updated
            if(totalUpdatableCount==0) {
                notice = pSize + "Cannot perform bulk update for <b>" + incompatibleRecords.length + " record(s)</b>.</p>";
            }
            else {
                notice = "";
                $('#hm-hd-addl-harvest-proceed-btn').removeClass('disabled');
                $('#hm-hd-addl-harvest-proceed-btn').show();
            }

            var hStatusIncompatCount = hStatusIncompatObject.recordIds.length;
            if(hStatusIncompatCount > 0) notice += pSize + " • Cannot harvest <b>" + hStatusIncompatCount + " record(s).</b> The harvest status should be <b>HARVEST COMPLETE</b>. Found harvest status: <b>" + hStatusIncompatObject.statusVals.join(", ") + "</b>.</p>";
            // Display notice for test cross verification
            if(tcvRecords.length>0) notice += pSize + " • Cannot harvest <b>" + tcvRecords.length + " record(s).</b> Only the harvest date may be specified for plots of experiments with <b>breeding stage = 'TCV'.</b></p>";
            // Display notice for records with missing cross methods
            if(missingCMethodRecords.length>0) notice += pSize + " • Cannot harvest <b>" + missingCMethodRecords.length + " record(s)</b> with <b>cross method = unknown</b>.</p>";
            // Display notice for records with unsupported states
            if(unsupportedStateRecords.length>0) notice += pSize + " • Cannot harvest <b>" + unsupportedStateRecords.length + " record(s)</b> with <b>state = unknown</b>. Kindly contact your data administrator for data curation.</p>";
            // Display notice for records where variables are not applicable to
            if(variableIncompatObject['recordIds'].length>0) notice += pSize + " • Cannot harvest <b>" + variableIncompatObject['recordIds'].length + " record(s)</b>. Variable(s) <b>" + variableIncompatObject['variables'].join(', ') + "</b> are not supported for the selected record(s).";
            // Display notice for records with harvest method and state incompatibility
            if(methodStateIncompatObject['recordIds'].length>0) notice += pSize + " • Harvest Method <b>" + harvestMethod + "</b> is not applicable to <b>" + methodStateIncompatObject['recordIds'].length + " record(s)</b> with state = <b>" + methodStateIncompatObject['states'].join(', ') + "</b>.</p>";
            // Display notice for records with harvest method and type incompatibility
            if(methodStateTypeIncompatObject['recordIds'].length>0) notice += pSize + " • Harvest Method <b>" + harvestMethod + "</b> is not applicable to <b>" + methodStateTypeIncompatObject['recordIds'].length + " record(s)</b> with type = <b>" + methodStateTypeIncompatObject['types'].join(', ') + "</b>.</p>";
            // // Display notice for records with numeric variable and harvest method incompatibility
            // if(numVarMethodIncompatObject['recordIds'].length>0) notice += pSize + " • Variable <b>" + variablePlaceholder[currentNumVar] + "</b> is not applicable to <b>" + numVarMethodIncompatObject['recordIds'].length + " record(s)</b> with method(s) = <b>" + numVarMethodIncompatObject['methods'].join(", ") + "</b>.</p>";
            // Display notice for records with numeric variable and cross method incompatibility
            if(numVarCrossMethodIncompatObject['recordIds'].length>0) notice += pSize + " • Variable <b>" + variablePlaceholder[currentNumVarCrossMethod] + "</b> is not applicable to <b>" + numVarCrossMethodIncompatObject['recordIds'].length + " record(s)</b> with cross method(s) = <b>" + numVarCrossMethodIncompatObject['crossMethods'].join(", ") + "</b>.</p>";
            // Display notice for records with harvest method and cross method incompatibility
            if(harvestMethodCrossMethodIncompatObject['recordIds'].length>0) notice += pSize + " • Harvest Method <b>" + currentHarvestMethod + "</b> is not applicable to <b>" + harvestMethodCrossMethodIncompatObject['recordIds'].length + " record(s)</b> with cross method(s) = <b>" + harvestMethodCrossMethodIncompatObject['crossMethods'].join(", ") + "</b>.</p>";
            // If some records can be updated, display message for continuing with the update operation
            if(totalUpdatableCount>0) notice += pSize + "If you wish to continue, click \"proceed\".</p>";

            // Display notice in the modal
            $('#hm-hd-addl-harvest-confirmation-modal-additional-content').html(notice);
            $('#hm-hd-addl-harvest-confirmation-modal-additional-content').show();
            $('#hm-hd-addl-harvest-confirmation-modal-additional-main').show();
        }
        else {
            $('#hm-hd-addl-harvest-proceed-btn').removeClass('disabled');
            $('#hm-hd-addl-harvest-proceed-btn').show();
        }
        
    });

    // Proceed with harvest click event
    $('#hm-hd-addl-harvest-proceed-btn').off('click').on('click', function(e) {
        e.preventDefault();
        // Add progress bar
        $('#hm-hd-addl-harvest-confirmation-modal-body').html('<p style="font-size:115%;"><i>Starting harvest...</i></p><div class="progress"><div class="indeterminate"></div></div>');
        $('#hm-hd-addl-harvest-confirmation-modal-additional-content').hide();
        $('.hm-hd-addl-harvest-confirm-btn').hide();

        // Remove incompatible records
        var tempSelected = [];
        var removeRecords = incompatibleRecords;

        // Collate all records to be updated
        for (var i = 0; i<selectedRecords.length; i++) {
            if(!removeRecords.includes(selectedRecords[i]['id']))
                tempSelected.push(selectedRecords[i]['id']);
        }
        updateRecords = tempSelected;

        // build request data
        var insertData = {
            recordIds: JSON.stringify(updateRecords),
            variableAbbrevs: JSON.stringify(variableAbbrevArray),
            values: JSON.stringify(values),
            dataLevel: dataLevel
        }

        // Send request to processor for status update
        $.ajax({
            url: '$urlInsertData',
            data: insertData,
            type: 'POST',
            async: true,
            success: function(data) {
                var statusData = {
                    recordIds: JSON.stringify(updateRecords),
                    validateStatus: 'true',
                    occurrenceId: occurrenceId,
                    dataLevel: dataLevel
                }

                // Update harvest status
                $.ajax({
                    url: '$urlUpdateRecordsStatus',
                    data: statusData,
                    type: 'POST',
                    async: true,
                    success: function(data) {
                        // Parse data to get job id
                        var result = jQuery.parseJSON(data);
                        var job = result.data[0] != null ? result.data[0] : [];
                        var jobId = job.backgroundJobDbId != null ? job.backgroundJobDbId : 0;

                        // Track record update progress
                        getRecordUpdateProgress(jobId, updateRecords);
                    },
                    error: function() {
                        ajaxGenericError();
                    }
                });
            },
            error: function() {
                ajaxGenericError();
            }
        });
    });
});

/**
 * Recursively track the progress of the record update given the job id
 * @param {integer} jobId object containing records to be updated
 * @param {object} recordIdsToDelete object containing records to be updated
 * @param {boolean} displayToast whether or not toast is to be displayed, defaults to true
 */
function getRecordUpdateProgress(jobId, recordIdsToDelete, displayToast = true) {
    // Send request to retrieve background job info
    $.ajax({
        url: '$urlGetBackgroundJob',
        data: {
            jobId: jobId
        },
        type: 'POST',
        async: true,
        success: function(data) {
            // Parse data to get job id and job status
            var result = jQuery.parseJSON(data);
            var job = result.data != null ? result.data : [];
            var jobId = job.backgroundJobDbId != null ? job.backgroundJobDbId : 0;
            var jobStatus = job.jobStatus != null ? job.jobStatus : 0;

            // If job status is DONE of FAILED, end recursive call
            if(jobStatus == 'DONE' || jobStatus == 'FAILED') {
                // Trigger harvest worker
                $.ajax({
                    url: '$urlCreate',
                    type: 'post',
                    datatType: 'json',
                    cache: false,
                    data: {
                        occurrenceId: occurrenceId
                    },
                    success: function(response) {
                        var result = JSON.parse(response);
                        var success = result.success;
                        var status = result.status;
                        var flashMessage = {};
                        
                        // Display flash message
                        if(success) {
                            flashMessage = {
                                status: 'info',
                                iconClass: 'fa fa-info-circle',
                                message: 'The harvest records (packages, seeds, germplasm, and related records) are being created in the background. You may proceed with other tasks. You will be notified in this page once done.',
                                parentDivId: 'hm-bg-alert-div',
                                time: 10
                            }
                        }
                        else {
                            // Failure message
                            let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                            // If status is not 200, the problem occurred while creating the background job record (code: #001)
                            if (status != null && status != 200) message += ' (#001)';
                            // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                            else if(status == null) message += ' (#002)';

                            flashMessage = {
                                status: 'warning',
                                iconClass: 'fa-fw fa fa-warning',
                                message: message,
                                parentDivId: 'hm-bg-alert-div',
                                time: null
                            }
                        }
                        localStorage.setItem('hm-addl-harvest-' + occurrenceId, JSON.stringify(flashMessage));

                        // Redirect to Creation tab and display flash message
                        $('.creation-tab').click();
                    },
                    error: function() {
                        ajaxGenericError();
                    }
                });

                return;
            }

            // If job is not yet done, recursively call on the function to check again
            getRecordUpdateProgress(jobId, recordIdsToDelete);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Displays generic toast error message when an ajax call fails.
 * @param {boolean} reset whether or not to reset the browser after displaying the error. Defaults to true.
 */
function ajaxGenericError(reset = true) {
    $("#hm-hd-addl-harvest-modal-body").show();
    var notif = "<i class='material-icons red-text'>close</i>&nbsp;An error occurred. Additional harvest unsuccessful.";
    Materialize.toast(notif, 3500);
    // Close modal
    $("#hm-hd-addl-harvest-modal.in").modal('hide');
}

/**
 * Take note of records with status != COMPLETED
 */
function checkUnsupportedHStatus() {
    var supported = ['COMPLETED'];
    var rows = selectedRecords;

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var harvestStatus = row.harvestStatus;

        // If harvestStatus is not supported, add record to unsupported harvestStatus records array
        if(harvestStatus != null && harvestStatus != "" && !supported.includes(harvestStatus)) {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!hStatusIncompatObject['recordIds'].includes(id)) hStatusIncompatObject['recordIds'].push(id);
            if(!hStatusIncompatObject['statusVals'].includes(harvestStatus)) hStatusIncompatObject['statusVals'].push(harvestStatus.split(':')[0]);
        }
    }
}

/**
 * Catch TCV use case
 */
function catchTCVUseCase() {
    var rows = selectedRecords;
    var disallow = true;
    if(varAbb.length == 1 && varAbb[0] == "HVDATE_CONT") {
        disallow = false;
    }

    

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var stage = row.stage;

        // If stage is test cross verification, disallow
        if(stage != null && stage == 'TCV' && disallow) {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!tcvRecords.includes(id)) tcvRecords.push(id);
        }
    }
}

/**
 * Take note of records with missing cross methods
 */
function checkMissingCrossMethod() {
    var rows = selectedRecords;

    

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var crossMethodAbbrev = row.crossMethodAbbrev;

        // If cross method is not set, add to incompatible array
        if(crossMethodAbbrev == '') {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!missingCMethodRecords.includes(id)) missingCMethodRecords.push(id);
        }
    }
}

/**
 * Take note of records with unsupported state
 */
function checkUnsupportedState() {
    var supported = ['fixed', 'not_fixed'];
    var rows = selectedRecords;

    

    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        var state = row.state;

        // If state is not supported, add record to unsupported state records array
        if(state != null && state != "" && !supported.includes(state)) {
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!unsupportedStateRecords.includes(id)) unsupportedStateRecords.push(id);
        }
    }
}

/**
 * Take note of records that are incompatible with the variable(s) selected
 */
function checkVariableIncompatibility() {
    var rows = selectedRecords;

    

    // Loop through selected records
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;
        // Get record info
        var crossMethodAbbrev = row.crossMethodAbbrev
        var state = row.state
        var type = row.type
        // Get compat arrays
        var cmethCompat = variableCompat[crossMethodAbbrev] !== undefined ?
            variableCompat[crossMethodAbbrev] : (
                variableCompat['default'] !== undefined ?
                    variableCompat['default'] : []
            );
        var stateCompat = cmethCompat[state] !== undefined ?
            cmethCompat[state] : (
                cmethCompat['default'] !== undefined ?
                    cmethCompat['default'] : []
            );
        var typeCompat = stateCompat[type] !== undefined ?
            stateCompat[type] : (
                stateCompat['default'] !== undefined ?
                    stateCompat['default'] : []
            );

        // Check each abbrev selected
        for(var abbrev of varAbb) {
            var fieldName = variableFieldNames[abbrev];
            var placeholder = variablePlaceholder[abbrev];

            // If the type compatibility array does not include the field name
            // mark record as incompatible
            if(!typeCompat.includes(fieldName)) {
                if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
                if(!variableIncompatObject['recordIds'].includes(id)) variableIncompatObject['recordIds'].push(id);
                if(!variableIncompatObject['variables'].includes(placeholder)) variableIncompatObject['variables'].push(placeholder);
            }
        }
    }
}

/**
 * Take note of records that are incompatible with the harvest method due to the state
 */
function checkMethodStateIncompatibility() {
    var harvestMethodIncluded = varAbb.includes('HV_METH_DISC');
    var harvestMethod = vals[varAbb.indexOf('HV_METH_DISC')];
    var rows = selectedRecords;

    
    
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;

        if(incompatibleRecords.includes(id) || unsupportedStateRecords.includes(id)) continue;

        // If harvest method is part of the input, check state
        if(harvestMethodIncluded) {
            var state = row.state;
            if(stateHMethodCompat[state] === undefined) state = 'default';
            // If the state is fixed and the method is not Bulk, add to incompatibility object
            harvestMethod = Array.isArray(harvestMethod) ? harvestMethod[0] : harvestMethod
            if(!stateHMethodCompat[state].includes(harvestMethod)) {
                if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
                methodStateIncompatObject['recordIds'].push(id);
                if(!methodStateIncompatObject['states'].includes(state)) methodStateIncompatObject['states'].push(state);
            }
        }
    }
}

/**
 * Take not of records that are incompatible with the harvest method due to the type
 */
function checkMethodStateTypeIncompatibility() {
    var harvestMethodIncluded = varAbb.includes('HV_METH_DISC');
    var harvestMethod = vals[varAbb.indexOf('HV_METH_DISC')];
    var rows = selectedRecords;

    
    
    for(var i=0; i<rows.length; i++) {
        var row = rows[i];
        var id = row.id;

        if(incompatibleRecords.includes(id) || unsupportedStateRecords.includes(id)) continue;

        // If harvest method is part of the input, check state and type
        if(harvestMethodIncluded) {
            var state = row.state != undefined && row.state != "" ? row.state : 'default';
            var type = row.type != undefined && row.type != "" ? row.type : 'default';
            var allowedMethods =
                gStateGTypeHMetCompat[state][type] == undefined
                ?
                (
                    gStateGTypeHMetCompat[state]['default'] == undefined
                    ?
                    (
                        gStateGTypeHMetCompat['default']['default'] == undefined
                        ?
                        []
                        :
                        gStateGTypeHMetCompat['default']['default']
                    )
                    :
                    gStateGTypeHMetCompat[state]['default']
                )
                :
                gStateGTypeHMetCompat[state][type];

            harvestMethod = Array.isArray(harvestMethod) ? harvestMethod[0] : harvestMethod;

            // If method is not found in the allowed values, mark record as incompatible
            if(allowedMethods[harvestMethod] === undefined) {
                if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
                methodStateTypeIncompatObject['recordIds'].push(id);
                if(!methodStateTypeIncompatObject.types.includes(type)) methodStateTypeIncompatObject.types.push(type);
            }
        }
    }
}

/**
 * Take note of records that are incompatible with the numeric variable due to the cross method
 */
function checkNumVarCrossMethodIncompatibility() {
    var rows = selectedRecords;

    if(numVarAbbrev=='') return;

    var compatibleCrossMethods = numVarCompat[numVarAbbrev]['crossMethods'];

    var rowsLength = rows.length;
    for(var i=0; i<rowsLength; i++) {
        var row = rows[i];
        var id = row.id;

        if(incompatibleRecords.includes(id) || unsupportedStateRecords.includes(id)) continue;

        var crossMethodAbbrev = row.crossMethodAbbrev;

        // If the cross method of the record is not compatible with the numeric variable,
        // then add to incompatibility object
        if(!compatibleCrossMethods.includes(crossMethodAbbrev)) {
            // Get cross method name from abbrev
            var crossMethodName = crossMethodAbbrev.replace("CROSS_METHOD_","");
            crossMethodName = crossMethodName.replace("_"," ");
            var mth = crossMethodName.toLowerCase();
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!numVarCrossMethodIncompatObject['crossMethods'].includes(mth)) numVarCrossMethodIncompatObject['crossMethods'].push(mth);
            numVarCrossMethodIncompatObject['recordIds'].push(id);
            numVarCrossMethodIncompatObject['numvar'] = numVarAbbrev;
        }
    }
}

/**
 * Take note of records that are incompatible with the harvest method due to the cross method
 */
function checkHarvestMethodCrossMethodIncompatibility() {
    if(!varAbb.includes('HV_METH_DISC')) return;

    var selectedHarvestMethod = values[varAbb.indexOf('HV_METH_DISC')];
    var rows = selectedRecords;

    

    var rowsLength = rows.length
    for(var i=0; i<rowsLength; i++) {
        var row = rows[i];
        var id = row.id;

        if(incompatibleRecords.includes(id) || unsupportedStateRecords.includes(row[0])) continue;

        var crossMethodAbbrev = row.crossMethodAbbrev;
        var crossMetHarvMetCompatAbbrev = crossMetHarvMetCompat[crossMethodAbbrev];
        var compatibleHarvestMethods = {};

        if(crossMetHarvMetCompatAbbrev !== undefined){
            compatibleHarvestMethods = Object.values(crossMetHarvMetCompatAbbrev);
        }

        // Check if the harvest method is supported for the cross method
        var compatible = false;
        var compatibleHarvestMethodsLength = compatibleHarvestMethods.length;
        for(var j = 0; j < compatibleHarvestMethodsLength; j++) {
            if(selectedHarvestMethod==compatibleHarvestMethods[j]) {
                compatible = true;
                break;
            }
        }

        // If incompatible, then add to incompatibility object
        if(!compatible) {
            // Get cross method name from abbrev
            var crossMethodName = crossMethodAbbrev.replace("CROSS_METHOD_","");
            crossMethodName = crossMethodName.replace("_"," ");
            var mth = crossMethodName.toLowerCase();
            if(!incompatibleRecords.includes(id)) incompatibleRecords.push(id);
            if(!harvestMethodCrossMethodIncompatObject['crossMethods'].includes(mth)) harvestMethodCrossMethodIncompatObject['crossMethods'].push(mth);
            harvestMethodCrossMethodIncompatObject['recordIds'].push(id);
            harvestMethodCrossMethodIncompatObject['harvestMethod'] = selectedHarvestMethod;
        }
    }
}

/**
 * Adjust abbrev array, value array, and buttons on input.
 * @param {string} value the value from the input field
 * @param {string} variableAbbrev the variable abbrev of the input
 * @param {integer} index the index of the variableAbbrev in the variableAbbrevArray
 */
function inputChange(value, variableAbbrev, index) {
    // If value is empty, remove the variableAbbrev from the variableAbbrevArray
    if(value=='') {
        if(index>-1) {                                  // If variableAbbrev is in variableArray, remove it
            variableAbbrevArray.splice(index,1);
            values.splice(index,1);
        }
        checkDataCompleteness();
        return;
    }

    // If the variable abbrev is not yet included in the variable abbrev array,
    // then add it to the array, and then add the value to the values array
    if(index==-1) {
        variableAbbrevArray.push(variableAbbrev);
        values.push(value);
    } else { // Else, add the value to the values array
        values[index] = value;
    }

    checkDataCompleteness();
}

/**
 * Determines whether or not the data is complete.
 * If complete, the confirm button is enabled.
 * Otherwise, it is disabled.
 */
function checkDataCompleteness() {
    var hdate = $('#hm-addl-harvest-input-hdate').val();
    var hmethod = $('#hm-addl-harvest-input-hmethod').val();
    var numVarRequired = false;
    var numvar = '';

    if (hmethod !== '') {
        // Check which numeric variable
        for(var i = 0; i < kCount; i++) {
            var abbrev = numVarCompatKeys[i];
            var abbrevIndex = variableAbbrevArray.indexOf(abbrev);
            var compatObject = numVarCompat[abbrev];
            var allowedHarvestMethods = compatObject.harvestMethods;
            // Set element ids based on abbrev
            var inputId = '#hm-addl-harvest-input-'+abbrev;

            // If the input value for harvest method
            // is included in the allowed harvest methods for the numeric variable,
            // get its value
            if(allowedHarvestMethods.includes(hmethod)) {
                var numvar = $(inputId).val();
                break;
            }
        }

        var methodNumVarCompatConfig = bulkUpdateConfig.method_numvar_requirement_compat;
        var finalCompat = [];

        // Get config for harvest method
        for (compat in methodNumVarCompatConfig) {
            var compatVal = methodNumVarCompatConfig[compat];
            var compatValMethod = compatVal[hmethod] !== undefined ? compatVal[hmethod] : [];

            for (item of compatValMethod) {
                if (!finalCompat.includes(item)) finalCompat.push(item);
            }
        }
        // Check if num var is required
        if (finalCompat.length > 0 && !finalCompat.includes('')) numVarRequired = true; 
    }

    // If data is complete, enable confirm button
    if (hdate !== ''
        && hmethod !== ''
        && (numVarRequired == false || (numVarRequired == true && numvar !== ''))
    ) {
        $('#hm-hd-addl-harvest-btn-confirm').removeClass('disabled');
    }
    else $('#hm-hd-addl-harvest-btn-confirm').addClass('disabled');
}

/**
 * Builds method-state compat array
 */
function buildStateHMethodCompat() {
    for(var state in gStateGTypeHMetCompat) {
        /**
         * TCV check disabled in support of CORB-6631.
         * The code will be kept here, commented,
         * in case the check needs to be re-enabled.
         */
        // if(state != 'TCV') {
        //     if(stateHMethodCompat[state] === undefined) stateHMethodCompat[state] = []
        //     var types = gStateGTypeHMetCompat[state];
        //     for(var type in types) {
        //         var methods = types[type];
        //         for(var method in methods) {
        //             if(!stateHMethodCompat[state].includes(method)) stateHMethodCompat[state].push(method); 
        //         }
        //     }
        // }

        if(stateHMethodCompat[state] === undefined) stateHMethodCompat[state] = []
        var types = gStateGTypeHMetCompat[state];
        for(var type in types) {
            var methods = types[type];
            for(var method in methods) {
                if(!stateHMethodCompat[state].includes(method)) stateHMethodCompat[state].push(method); 
            }
        }
    }
}

JS;

$this->registerJs($script);

?>