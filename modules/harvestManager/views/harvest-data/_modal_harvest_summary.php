<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Harvest Summary modal contents
 */

use kartik\grid\GridView;
use app\dataproviders\ArrayDataProvider;

// Process recordInfo
$includedFields = [];
// Determine fields to include based on data type
if ($dataType == 'plot') {
    $includedFields = [
        'plotCode' => "Plot Code",
        'designation' => "Germplasm Name",
        'state' => "Germplasm State",
        'type' => "Germplasm Type",
        'occurrenceName' => "Occurrence Name",
        'programCode' => "Program"
    ];
}
else if ($dataType == 'cross') {
    $includedFields = [
        'crossName' => "Cross Name",
        'crossMethod' => "Cross Method",
        'femaleGermplasmName' => "Female Parent",
        'maleGermplasmName' => "Male Parent",
        'occurrenceName' => "Occurrence Name",
        'programCode' => "Program"
    ];
}
// Extract fields
$basicInfo = [];
foreach ($includedFields as $field => $label) {
    $basicInfo[] = [
        'fieldName' => $label,
        'fieldValue' => $recordInfo[$field]
    ];
}

// Build basic information table
$basicInfoColumns = [
    [
        'attribute' => 'fieldName',
        'label' =>  \Yii::t('app', 'Field Name'),
        'format' => 'raw',
        'hAlign' => 'center'
    ],
    [
        'attribute' => 'fieldValue',
        'label' =>  \Yii::t('app', 'Field Value'),
        'format' => 'raw',
        'hAlign' => 'center'
    ]
];

// Parse messageJSON
$messageMainArray = (array) $messageJSON;
$harvestSummary = (array) $messageMainArray['harvestSummary'] ?? [];
$harvestSummaryOutcome = $harvestSummary['outcome'] ?? '';
// Format outcome
switch ($harvestSummaryOutcome) {
    case 'FAILED': {
        $harvestSummaryOutcome = '<span class="new badge red darken-2"><strong>' . \Yii::t('app', $harvestSummaryOutcome) . '</strong></span>';
        break;
    }
    case 'SKIPPED': {
        $harvestSummaryOutcome .= '<span class="new badge orange darken-2"><strong>' . \Yii::t('app', $harvestSummaryOutcome) . '</strong></span>';
        break;
    }
    case 'SUCCESS': {
        $harvestSummaryOutcome = '<span class="new badge green darken-2"><strong>' . \Yii::t('app', $harvestSummaryOutcome) . '</strong></span>';
        break;
    }
    default: {
        $harvestSummaryOutcome = '';
        break;
    }
}

// Add outcome to basic info
$basicInfo[] = [
    'fieldName' => 'Outcome',
    'fieldValue' => $harvestSummaryOutcome
];

// Assemble data provider
$dataProvider = new ArrayDataProvider([
    'allModels' => $basicInfo,
    'key' => 'fieldName',
    'restified' => true,
    'totalCount' => count($basicInfo)
]);
// Build grid
$basicInfoGrid = GridView::widget([
    'pjax' => true,
    'dataProvider' => $dataProvider,
    'id' => 'hm-hd-harvest-summary-table',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'striped'=>false,
    'columns' => $basicInfoColumns,
    'toggleData'=>true,
    'layout' => '{items}{pager}',
]);


// Build harvest summary table
// Process messages
$harvestSummaryMessages = $harvestSummary['messages'] ?? [];
// Get messages and format the message type for display
$messagesArray = [];
foreach ($harvestSummaryMessages as $messageItem) {
    // Convert to array
    $messageItem = (array) $messageItem;
    // Extract message details
    $messageType = $messageItem["type"] ?? '';
    $messageLevel = $messageItem["level"] ?? '';
    $actualMessage = $messageItem["message"] ?? '';

    // Format message type
    switch ($messageType) {
        case 'ERROR': {
            $messageType = '<span class="new badge red darken-2"><strong>' . \Yii::t('app', $messageType) . '</strong></span>';
            break;
        }
        case 'WARNING': {
            $messageType = '<span class="new badge orange darken-2"><strong>' . \Yii::t('app', $messageType) . '</strong></span>';
            break;
        }
        case 'INFO': {
            $messageType = '<span class="new badge blue darken-2"><strong>' . \Yii::t('app', $messageType) . '</strong></span>';
            break;
        }
        default: {
            $messageType = '';
            break;
        }
    }

    // Add message to array
    $messagesArray[] = [
        'messageType' => $messageType,
        'message' => $actualMessage
    ];
}

// Build columns for harvest summary
$harvestSummaryColumns = [
    [
        'attribute' => 'messageType',
        'label' =>  \Yii::t('app', 'Type'),
        'format' => 'raw',
        'hAlign' => 'center',
    ],
    [
        'attribute' => 'message',
        'label' =>  \Yii::t('app', 'Message'),
        'format' => 'raw',
        'hAlign' => 'left'
    ]
];

// Assemble data provider
$dataProvider = new ArrayDataProvider([
    'allModels' => $messagesArray,
    'key' => 'messageType',
    'restified' => true,
    'totalCount' => count($messagesArray)
]);
// Build grid
$harvestSummaryGrid = GridView::widget([
    'pjax' => true,
    'dataProvider' => $dataProvider,
    'id' => 'hm-hd-harvest-summary-table',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'striped'=>false,
    'columns' => $harvestSummaryColumns,
    'toggleData'=>true,
    'layout' => '{items}{pager}',
]);

// Display basic information
echo "<p style='font-size:115%;'><strong>" . \Yii::t('app', 'Basic Information') . "</strong></p>";
echo $basicInfoGrid;

// Display harvest summary
echo "<p style='font-size:115%;'><strong>" . \Yii::t('app', 'Harvest Summary') . "</strong></p>";
echo $harvestSummaryGrid;
    
?>