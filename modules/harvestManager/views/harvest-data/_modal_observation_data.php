<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Imports
use yii\helpers\Url;
use kartik\select2\Select2;

// Define URLs
$urlSaveToSession = Url::to(['harvest-data/save-to-session']);

?>

<p>Select the observation data to use as filter.</p>

<?php
echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Observation data'.' '.'<span class="required">*</span>').'</label>';
echo Select2::widget([
    'name' => 'input_type',
    'data' => $select2Data,
    'value' => $currentValues,
    'options' => [
        'autocomplete' => 'off',
        'placeholder' => 'Select data',
        'class' => 'hm-hd-od-modal-input',
        'id' => 'hm-hd-od-modal-observation-data-input',
        'title' => 'Harvest Data',
        'disabled' => false,
        'data-name' => 'observation-data',
        'data-label' => 'observation-data',
        'allowClear' => true,
    ],
    'pluginOptions' => [
        'multiple' => true,
        'allowClear' => true
    ]
]);

?>

<div class="switch">
    <label class="pull-left">
        Without Observation Data
        <input type="checkbox" class= "hm-hd-od-modal-input" id="hm-hd-od-modal-visibility-input">
        <span class="lever" style="margin: 10px 7px;"></span>
        With Observation Data
    </label>
</div>


<?php

// set flags
$withData = $withData ? "true" : "false";
$filterIsSet = count($currentValues) == 0 ? "false" : "true";
$select2DataJSON = json_encode($select2Data);

$script = <<< JS
var occurrenceId = $occurrenceId;
var dataLevel = "$dataLevel";
var withData = $withData;
var filterIsSet = $filterIsSet;
var dataDisplayNames = $select2DataJSON;

// Disable slide
if(!filterIsSet) {
    $("#hm-hd-od-modal-visibility-input").attr('disabled', true);
    $('#hm-hd-observation-data-remove-btn').addClass('disabled');
}
else {
    $('#hm-hd-observation-data-remove-btn').removeClass('disabled');
}
// Toggle slide
if(withData) {
    $("#hm-hd-od-modal-visibility-input").prop('checked', true);
    $("#hm-hd-od-modal-visibility-input").addClass('isClicked');
    $("#hm-hd-od-modal-visibility-input").parent("td").parent("tr").addClass("selected");
}

$("document").ready(function(){

    // Visibility change event
    $(document).on('change', "#hm-hd-od-modal-visibility-input", function() {
        var dataValues = $("#hm-hd-od-modal-observation-data-input").val();

        // If data values are set, enable apply button
        if(dataValues != '') {
            $('#hm-hd-observation-data-apply-btn').removeClass('disabled');
        }
        else {
            $('#hm-hd-observation-data-apply-btn').addClass('disabled');
        }
    });

    // Observation data change event
    $(document).on('change', "#hm-hd-od-modal-observation-data-input", function() {
        var value = $(this).val();

        // If values are set, enable apply button
        if(value != '') {
            $("#hm-hd-od-modal-visibility-input").attr('disabled', false);
            $('#hm-hd-observation-data-apply-btn').removeClass('disabled');
        }
        else {
            $('#hm-hd-observation-data-apply-btn').addClass('disabled');
        }
    });

    // Apply filter button click event
    $('#hm-hd-observation-data-apply-btn').off('click').on('click', function() {
        let data = $("#hm-hd-od-modal-observation-data-input").val();
        let withData = $("#hm-hd-od-modal-visibility-input").prop('checked');

        //Assemble display names array
        var displayNames = [];
        for(var i = 0; i < data.length; i++) {
            displayNames.push(dataDisplayNames[data[i]]);
        }
        
        // Build session variable
        var sessionValue = JSON.stringify({
            fields: data,
            withData: withData,
            displayNames: displayNames,
            dataLevel: dataLevel
        });
        // Save to Yii session
        saveToSession("observation-filter-"+dataLevel+"-"+occurrenceId, sessionValue);
    });

    // Remove filter button click event
    $('#hm-hd-observation-data-remove-btn').off('click').on('click', function() {
        saveToSession("observation-filter-"+dataLevel+"-"+occurrenceId, '');
    });
});

/**
 * Saves the given variable name and value in the Yii session
 * @param {string} name name of the variable
 * @param {string} value variable value
 */
function saveToSession(name, value) {
    $.ajax({
        url: '$urlSaveToSession',
        type: 'post',
        data: {
            name: name,
            value: value
        },
        success: function(response) {
            $("#hm-hd-observation-data-modal.in").modal('hide');
            resetBrowser();
        },
        error: function(jqXHR, exception) {
            
        }
    });
}

/**
 * Reset the browser and clear selection after bulk update
 */
function resetBrowser() {
    // Clear selection on grid reset
    var gridId = 'dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-table-con';
    var selectionStorageId = occurrenceId + "_" + gridId;
    clearSelectionHM(selectionStorageId);
    var pageurl = $('#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid li.active a').attr('href');
    $.pjax.reload(
        {
            container:'#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-pjax',
            url: pageurl,
            replace: false,
            type: 'POST',
            data: {
                forceReset:true
            }
        }
    );
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} selectionStorageId unique string identifier holding the selected items
 * @param {boolean} all whether or not all checkbox selections are to be cleared
 */
function clearSelectionHM(selectionStorageId, all = false) {
    // If all flag is set to true, clear selection for all supported grids
    if (all) {
        // Loop through supported grids
        for(var i = 0; i < supportedCount; i++) {
            // Build selection identifier
            var currentSession = occurrenceId + "_" + supportedGrids[i];
            // Remove selection from storage
            sessionStorage.removeItem(currentSession);
        }
    }
    // If all flag is false, remove specified  selection id
    else sessionStorage.removeItem(selectionStorageId);
}

JS;

$this->registerJs($script);

Yii::$app->view->registerCss('

    .select2-container .select2-search__field {
        width: 100% !important;
    }

');