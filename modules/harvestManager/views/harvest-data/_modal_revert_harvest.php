<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Revert harvest modal contents
 */

use yii\helpers\Url;

// Define URLs
$urlUpdateRecordsStatus = Url::to(['harvest-data/update-records-status']);
$urlGetBackgroundJob = Url::to(['harvest-data/get-background-job']);
$urlGetSeedCount = Url::to(['harvest-data/get-seed-count']);
$urlCheckSeedEntries = Url::to(['harvest-data/check-seed-entries']);
$urlGetDatabaseIds = Url::to(['harvest-data/get-database-ids']);
$urlDeleteViaProcessor = Url::to(['harvest-data/delete-via-processor']);
$urlGetBackgroundJobs = Url::to(['harvest-data/get-background-jobs']);
$urlAcknowledgeRecords = Url::to(['harvest-data/acknowledge-records']);
$urlDeleteInBackground = Url::to(['harvest-data/delete-in-background']);

// Declare json encoded arrays
$recordIdsJson = json_encode($recordIds);
$recordInfoJson = json_encode($recordInfo);

// Display number of selected items
$recordCount = count($recordIds);
$countString = $recordCount == 0 ? 'No' : $recordCount;
$auxVerb = $recordCount == 1 ? 'is' : 'are';
$itemType = '';
$plural = '';
if($dataLevel == 'plot') {
    if($recordCount != 1) $itemType = 'plots';
    else $itemType = $dataLevel;
    $plural = 'plots';
}
else if($dataLevel == 'cross') {
    if($recordCount != 1) $itemType = 'crosses';
    else $itemType = $dataLevel;
    $plural = 'crosses';
}
$count = $countString." selected ".$itemType;

$failedPill = '<span title="Seed and package creation failed" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'FAILED') . '</strong></span>';

// If record count is 0, display info message to user
if($recordCount <= 0) {
    echo '<p style="font-size:115%;">'
    . '<em>There are no ' . $plural . ' with </em>'. $failedPill . '<em> harvest status.</em>'
    . '</p>';
    return;
}

// Status allowed for revert
$statusValues = [ $failedPill ];

// Build message
$modalMessage = "There $auxVerb <b>$recordCount $itemType</b> with "  . implode('&emsp;', $statusValues) . " harvest status."
. " Reverting the harvest will <b>delete germplasm, seeds, packages, and related records</b> created during the previous harvest,"
. " and <b>change the harvest status</b> to the appropriate value based on the available harvest data."
. " This operation <b>will not</b> delete any harvest data."
. "<br><br> If a given $dataLevel <b>has at least one seed that has been used as an entry in another experiment</b>,"
. " then the harvest for that $dataLevel <b>will not be reverted</b> and <b>no records will be deleted</b>."
. "<br><br> Do you wish to proceed?";

?>

<div id="revet-harvest-modal">
    <div id="revet-harvest-main-body">
        <p style="font-size:115%;"> <?= \Yii::t('app',$modalMessage) ?> </p>
    </div>
</div>

<?php 

$script = <<< JS
var selectedRecords = $recordInfoJson;
var occurrenceId = $occurrenceId;
var locationId = $locationId;
var dataLevel = "$dataLevel";
var hasSeeds = false;
var hasEntries = false;
var seedThreshold = $seedThreshold;
var thresholdExceeded = false;
var numberOfSeeds = 0;
var processStart = 0;
var lastKnownRecords = [];
var lastKnownRecordsToRevert = [];

$(document).ready(function() {

    // Proceed revert button click event
    $('#hm-hd-revert-harvest-proceed-btn').off('click').on('click', function() {
        $(this).addClass('disabled');
        $('.hm-hd-bd-cancel-btn').hide();
        // Hide modal body and display progress bar and progress field
        $("#hm-hd-bulk-delete-modal-body").hide();
        $('#hm-hd-bd-delete-progress').html('<p class="hm-hd-bd-progress-items" style="font-size:115%;"><i>Reverting...</i></p><div class="progress hm-hd-bd-progress-items"><div class="indeterminate hm-hd-bd-progress-items"></div></div>'
            +'<textarea id="delete-progress-textarea" name="delete-progress-display" style="background-color:#ffffff;min-height:150px;" rows="4" cols="50" readonly></textarea>');
        $("#hm-hd-bd-delete-progress").show();

        // Set rows based on mode
        var rows = selectedRecords;
        // Mark process start time
        processStart = new Date().getTime();
        // Update records status
        updateRecordsStatus(rows,'REVERT_IN_PROGRESS');
    });

    // Acknowledge button click event
    $('#hm-hd-bd-confirmation-acknowledge-btn').off('click').on('click', function() {
        // Revert back records with seeds used in entries back to FAILED
        updateRecordsStatus(lastKnownRecordsToRevert, 'FAILED');
        // If no records can be updated, return
        if(lastKnownRecords.length == 0) return;

        // Check if there are still seeds for deletion
        $.ajax({
            url: '$urlGetSeedCount',
            data: {
                recordIds: JSON.stringify(lastKnownRecords),
                dataLevel: dataLevel
            },
            type: 'POST',
            async: true,
            success: function(seedCount) {
                seedCount = parseInt(seedCount);
                numberOfSeeds = seedCount;

                // If no seeds found, proceed to plot data deletion
                if(seedCount == 0) {
                    hasSeeds = false;
                    retrieveDatabaseIds(lastKnownRecords);
                    return;
                }

                // If seeds are found, retrieve database ids
                hasSeeds = true;
                retrieveDatabaseIds(recordDbIdsToDelete);
            },
            error: function() {
                ajaxGenericError();
            }
        });
    });
});

/**
 * Perform record harvest status update
 * @param {object} records object containing records to be updated
 * @param {string} status harvest status to be applied, defaults to null
 * @param {boolean} displayToast whether or not toast is to be displayed, defaults to true
 * @param {boolean} parseArray whether or not parse the records as array, defaults to true
 */
function updateRecordsStatus(records, status = null, displayToast = true, parseArray = true) {
    // Update text area
    if(!thresholdExceeded) updateTextArea(" > Updating status of records...\\n");
    // Get record id array
    var recordIds = records
    if(parseArray) recordIds = getRecordIdArray(records);
    // Build data
    let data = {
        recordIds: JSON.stringify(recordIds),
        status: status,
        occurrenceId: occurrenceId,
        dataLevel: dataLevel
    }
    // If no status was provided, set validateStatus to true
    if(status == null) {
        data = {
            recordIds: JSON.stringify(recordIds),
            validateStatus: 'true',
            occurrenceId: occurrenceId,
            dataLevel: dataLevel
        }
    }
    // Send request to processor for status update
    $.ajax({
        url: '$urlUpdateRecordsStatus',
        data: data,
        type: 'POST',
        async: true,
        success: function(data) {
            // Parse data to get job id
            var result = jQuery.parseJSON(data);
            var job = result.data[0] != null ? result.data[0] : [];
            var jobId = job.backgroundJobDbId != null ? job.backgroundJobDbId : 0;

            // If jobId is 0, display error
            if(jobId == 0) {
                $('.hm-hd-bd-delete-btn').show();
                ajaxGenericError(false);
                return;
            }

            // Track record update progress
            getRecordUpdateProgress(jobId, records, status, displayToast);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Recursively track the progress of the record update given the job id
 * @param {integer} jobId object containing records to be updated
 * @param {object} recordIdsToDelete object containing records to be updated
 * @param {string} status harvest status to be applied
 * @param {boolean} displayToast whether or not toast is to be displayed, defaults to true
 */
function getRecordUpdateProgress(jobId, recordIdsToDelete, status, displayToast = true) {
    // Send request to retrieve background job info
    $.ajax({
        url: '$urlGetBackgroundJob',
        data: {
            jobId: jobId
        },
        type: 'POST',
        async: true,
        success: function(data) {
            // Parse data to get job id and job status
            var result = jQuery.parseJSON(data);
            var job = result.data != null ? result.data : [];
            var jobId = job.backgroundJobDbId != null ? job.backgroundJobDbId : 0;
            var jobStatus = job.jobStatus != null ? job.jobStatus : 0;

            // If jobId is 0, display error
            if(jobId == 0) {
                $('.hm-hd-bd-delete-btn').show();
                ajaxGenericError(false);
                return;
            }

            // If job status is DONE of FAILED, end recursive call
            if(jobStatus == 'DONE' || jobStatus == 'FAILED') {
                // Reset the browser
                resetBrowser();
                // Update the text area
                if(!thresholdExceeded) updateTextArea(" > STATUS UPDATED (" + jobStatus + ").\\n");

                // If the status is REVERT_IN_PROGRESS, proceed with revert
                if(status == 'REVERT_IN_PROGRESS') {
                    performDelete(recordIdsToDelete);
                }
                else {
                    if(hasEntries) {
                        if(lastKnownRecords.length == 0) {
                            // Calculate total time consumed
                            var deleteExecutionTimeMillis = new Date().getTime() - processStart;
                            // Reset modal UI
                            $("#hm-hd-bulk-delete-modal-body").show();
                            $('#hm-hd-bd-delete-progress').html('');
                            // Display toast notification
                            var notif = "<i class='material-icons blue-text'>info</i>&nbsp;Deletion of harvest traits was canceled.";
                            if(displayToast) Materialize.toast(notif, 2500);
                            // Close modal
                            $("#hm-hd-bulk-delete-modal.in").modal('hide');
                            // Reset the browser
                            resetBrowser();
                        }
                        if(status !== null) return;
                    }
                    if(thresholdExceeded) {
                        return;
                    }
                    // Calculate total time consumed
                    var deleteExecutionTimeMillis = new Date().getTime() - processStart;
                    // Reset modal UI
                    $("#hm-hd-bulk-delete-modal-body").show();
                    $('#hm-hd-bd-delete-progress').html('');
                    // Display toast notification
                    var notif = "<i class='material-icons green-text'>check</i>&nbsp;Harvest successfully reverted (" + (deleteExecutionTimeMillis*.001).toFixed(3) + "s)";
                    if(displayToast) Materialize.toast(notif, 2500);
                    // Close modal
                    $("#hm-hd-bulk-delete-modal.in").modal('hide');
                    // Reset the browser
                    resetBrowser();
                }
                return;
            }

            // If job is not yet done, recursively call on the function to check again
            getRecordUpdateProgress(jobId, recordIdsToDelete, status);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Perform deletion of harvest data, and harvest records (if any)
 * via the processor API.
 * @param {object} rows object containing record information
 */
function performDelete(rows) {
    updateTextArea(" > Retrieving seeds...\\n");
    hasSeeds = false;
    hasEntries = false;
    thresholdExceeded = false;
    var start = new Date().getTime();
    numberOfSeeds = 0;

    // If uncommitted switch is false, get record array
    rows = getRecordIdArray(rows);

    // 1. Check number of seeds
    $.ajax({
        url: '$urlGetSeedCount',
        data: {
            recordIds: JSON.stringify(rows),
            dataLevel: dataLevel
        },
        type: 'POST',
        async: true,
        success: function(seedCount) {
            seedCount = parseInt(seedCount);
            numberOfSeeds = seedCount;

            // Update progress message
            var executionTimeMillis = new Date().getTime() - start;
            var s = 's';
            if(seedCount == 1) s = '';
            updateTextArea(" > DONE. Found " + seedCount + " seed" + s + ". (" + (executionTimeMillis*0.001).toFixed(3) + "s)\\n");

            // If seed count exceeds threshold, send process to background worker
            if(numberOfSeeds>seedThreshold) {
                deleteInBackground(rows);
                return;
            }

            // If no seeds found, update harvest status
            if(numberOfSeeds === 0) {
                updateRecordsStatus(rows, null, true, false);
                return;
            }

            hasSeeds = true;
            
            updateTextArea(" > Checking if seeds have been used in entries...\\n");

            // 2. Check if seeds have been used in entries
            start = new Date().getTime();
            $.ajax({
                url: '$urlCheckSeedEntries',
                data: {
                    recordIds: JSON.stringify(rows),
                    dataLevel: dataLevel
                },
                type: 'POST',
                async: true,
                success: function(data) {
                    var data = jQuery.parseJSON(data);
                    recordDbIdsToDelete = jQuery.parseJSON(data.recordsToDelete);
                    
                    // If there are records with seeds already
                    // used in entries, display acknowledgement
                    if(data.recordsUnableToDeleteCount>0) {
                        hasEntries = true;
                        var executionTimeMillis = new Date().getTime() - start;
                        updateTextArea(" > DONE. Entries found for seeds of " + data.recordsUnableToDeleteCount + " record(s). (" + (executionTimeMillis*0.001).toFixed(3) + "s)\\n");
                        displayAcknowledgement(recordDbIdsToDelete, data.recordsUnableToDelete);
                        return;
                    }

                    var executionTimeMillis = new Date().getTime() - start;
                    updateTextArea(" > DONE. No entries found. (" + (executionTimeMillis*0.001).toFixed(3) + "s)\\n");

                    retrieveDatabaseIds(recordDbIdsToDelete);
                },
                error: function() {
                    ajaxGenericError();
                }
            });
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Retrieve ids to be deleted
 * @param {array} recordDbIdsToDelete record ids included in the deletion
 */
function retrieveDatabaseIds(recordDbIdsToDelete) {
    updateTextArea(" > Retrieving database ids...\\n");

    var start = new Date().getTime();
    $.ajax({
        url: '$urlGetDatabaseIds',
        data: {
            recordDbIds: JSON.stringify(recordDbIdsToDelete),
            hasSeeds: hasSeeds,
            skipData: true,
            dataLevel: dataLevel
        },
        type: 'POST',
        async: true,
        success: function(databaseIdStr) {
            var executionTimeMillis = new Date().getTime() - start;
            var databaseIds = JSON.parse(databaseIdStr);
            var deleteIdCount = databaseIds.seedDbIds.length + 
                databaseIds.germplasmDbIds.length + databaseIds.harvestDataDbIds.length;

            // If there are records to delete, pass ids to processor
            if(deleteIdCount > 0)  {
                updateTextArea(" > DATABASE IDS RETRIEVED. (" + (executionTimeMillis*.001).toFixed(3) + "s)\\n");
                deleteViaProcessor(databaseIdStr);
                return;
            }
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Perform deletion of records via processor
 * @param {string} databaseIdStr string encoded record ids included in the deletion
 */
function deleteViaProcessor(databaseIdStr) {
    updateTextArea(" > Deleting records...\\n");

    $.ajax({
        url: '$urlDeleteViaProcessor',
        data: {
            databaseIdStr: databaseIdStr,
            locationId: locationId,
            dataLevel: dataLevel,
            deleteTraits: false
        },
        type: 'POST',
        async: true,
        success: function(data) {
            var backgroundJobs = jQuery.parseJSON(data);
            var backgroundJobIds = [];
            // Get background job ids
            if(backgroundJobs.seed != null) backgroundJobIds.push(backgroundJobs.seed.backgroundJobDbId);
            if(backgroundJobs.germplasm != null) backgroundJobIds.push(backgroundJobs.germplasm.backgroundJobDbId);
            if(backgroundJobs.harvestData != null) backgroundJobIds.push(backgroundJobs.harvestData.backgroundJobDbId);

            var startDelete = new Date().getTime();

            getDeleteProgress(backgroundJobIds,startDelete,databaseIdStr);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Tracks the progress of the deletion of records
 * by checking the job status of the background jobs
 * @param {array} backgroundJobIds array containing background job ids
 * @param {integer} startDelete start time of the deletion
 */
function getDeleteProgress(backgroundJobIds, startDelete, databaseIdStr) {
    $.ajax({
        url: '$urlGetBackgroundJobs',
        data: {
            backgroundJobIds: JSON.stringify(backgroundJobIds),
        },
        type: 'POST',
        async: true,
        success: function(data) {
            var data = jQuery.parseJSON(data);
            var jobs = data.backgroundJobs;
            var jobsLength = jobs.length;
            var jobsDone = true;
            
            // Check if all jobs are done
            for(var i = 0; i < jobsLength; i++) {
                jobsDone = jobsDone && (jobs[i].jobStatus == 'DONE' || jobs[i].jobStatus == 'FAILED');
            }

            // If all jobs are done, proceed to record status update
            if(jobsDone) {
                var ids = JSON.parse(databaseIdStr);
                var records = ids.recordDbIds
                updateRecordsStatus(records, null, true, false);
                return;
            }

            // If jobs are not all done yet, recusively call the function to check again
            getDeleteProgress(backgroundJobIds,startDelete,databaseIdStr);
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Invoke background worker for deletion
 * @param {array} recordIds array containing record ids
 */
function deleteInBackground(recordIds) {
    updateTextArea(" > Sending data to background worker...\\n");

    $.ajax({
        url: '$urlDeleteInBackground',
        data: {
            occurrenceId: occurrenceId,
            recordIds: JSON.stringify(recordIds),
            dataLevel: dataLevel,
            deletionMode: 'revert'
        },
        type: 'POST',
        async: true,
        success: function(data) {
            var result = JSON.parse(data);
            var success = result.success;
            var status = result.status;
            
            // Display flash message
            if(success) {
                // Success message
                displayCustomFlashMessage(
                    'info', 
                    'fa fa-info-circle',
                    'The harvest is being reverted in the background. You may proceed with other tasks. You will be notified in this page once done.',
                    'hm-bg-alert-div',
                    10
                );
            }
            else {
                // Failure message
                let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                // If status is not 200, the problem occurred while creating the background job record (code: #001)
                if (status != null && status != 200) message += ' (#001)';
                // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                else if(status == null) message += ' (#002)';

                displayCustomFlashMessage(
                    'warning', 
                    'fa-fw fa fa-warning', 
                    message,
                    'hm-bg-alert-div'
                );
            }

            // Reset modal UI
            $("#hm-hd-bulk-delete-modal-body").show();
            $('#hm-hd-bd-delete-progress').html('');
            // Close the modal
            $("#hm-hd-bulk-delete-modal").modal('hide');
            // Reset the browser
            resetBrowser();
            return;
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Inform user that some records have seeds used as entries in another experiment.
 * @param {array} recordDbIdsToDelete array records that can be deleted
 * @param {array} recordsUnableToDelete array records that cannot be deleted
 */
function displayAcknowledgement(recordDbIdsToDelete, recordsUnableToDelete) {
    // Get info of plots unable to delete
    $("#hm-hd-bulk-delete-confirmation-modal-additional-main").hide();
    $.ajax({
        url: '$urlAcknowledgeRecords',
        data: {
            recordIds: recordsUnableToDelete,
            occurrenceId: occurrenceId,
            dataLevel: dataLevel
        },
        type: 'POST',
        async: true,
        success: function(data) {
            lastKnownRecords = recordDbIdsToDelete;
            lastKnownRecordsToRevert = jQuery.parseJSON(recordsUnableToDelete);
            $('.hm-hd-bd-confirmation-acknowledge-grp').removeClass('disabled');
            $('.hm-hd-bd-confirmation-acknowledge-grp').show();
            $('.hm-hd-bd-confirmation-default-grp').hide();
            $("#hm-hd-bulk-delete-modal").modal('hide');
            $("#hm-hd-bulk-delete-confirmation-modal-body").html(data);
            $("#hm-hd-bulk-delete-confirmation-modal").modal('show');
        },
        error: function() {
            ajaxGenericError();
        }
    });
}

/**
 * Displays generic toast error message when an ajax call fails.
 * @param {boolean} reset whether or not to reset the browser after displaying the error. Defaults to true.
 */
function ajaxGenericError(reset = true) {
    $("#hm-hd-bulk-delete-modal-body").show();
    $('#hm-hd-bd-delete-progress').html('');
    var notif = "<i class='material-icons red-text'>close</i>&nbsp;An error occurred. Bulk delete unsuccessful.";
    Materialize.toast(notif, 3500);
    // Close modal
    $("#hm-hd-bulk-delete-modal.in").modal('hide');
    // Reset the browser
    if(reset) resetBrowser();
}

/**
 * Reset the browser and clear selection after bulk update
 */
function resetBrowser() {
    // Clear selection on grid reset
    var gridId = 'dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-table-con';
    var selectionStorageId = occurrenceId + "_" + gridId;
    clearSelectionHM(selectionStorageId);
    var pageurl = $('#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid li.active a').attr('href');
    $.pjax.reload(
        {
            container:'#dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-pjax',
            url: pageurl,
            replace: false,
            type: 'POST',
            data: {
                forceReset:false,
                bulkOperation:true
            }
        }
    );
}

/**
 * Returns an array of ids given the record info object
 * @param {object} rows object containing record information
 */
function getRecordIdArray(rows) {
    var ids = [];
    for(var i = 0; i<rows.length; i++) {
        var row = rows[i];
        // If row.id is not null, push to ids array
        if(row.id != null) ids.push(row.id);
        // else, return rows (already formatted)
        else return rows;
    }
    return ids;
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} selectionStorageId unique string identifier hoolding the selected items
 * @param {boolean} all whether or not all checkbox selections are to be cleared
 */
function clearSelectionHM(selectionStorageId, all = false) {
    // If all flag is set to true, clear selection for all supported grids
    if (all) {
        // Loop through supported grids
        for(var i = 0; i < supportedCount; i++) {
            // Build selection identifier
            var currentSession = occurrenceId + "_" + supportedGrids[i];
            // Remove selection from storage
            sessionStorage.removeItem(currentSession);
        }
    }
    // If all flag is false, remove specified  selection id
    else sessionStorage.removeItem(selectionStorageId);
}

/**
 * Updates the deletion process text area with the given message
 * @param {string} message message to add to the text area
 */
function updateTextArea(message) {
    var textArea = $('#delete-progress-textarea');
    textArea.val(textArea.val() + message);
    if(typeof textArea[0] != 'undefined') textArea.scrollTop(textArea[0].scrollHeight);
}

/**
 * Displays custom flash message
 * @param {string} status the status of the message (info, success, warning, error)
 * @param {string} iconClass icon of flash message
 * @param {string} message the message to display
 * @param {string} parentDivId id of the div where the flash message is to be inserted
 * @param {float} time time in seconds before the flash message is removed from the display. 7.5 seconds by default.
 */
function displayCustomFlashMessage(status, iconClass, message, parentDivId, time = 7.5) {
    // Add flash message to parent div
    $('#' + parentDivId).html(
        '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
            '<div class="card-panel">' +
                '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                message +
                '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
            '</div>' +
        '</div>'
    );

    // remove flash message after the specified time in seconds (7.5 seconds by default)
    setTimeout(
        function() 
        {
            $('#w25-' + status + '-0').fadeOut(250);
            setTimeout(
                function() 
                {
                    $('#' + parentDivId).html('');
                }
            , 500);
        }
    , 1000 * time);
}

JS;

$this->registerJs($script);

?>