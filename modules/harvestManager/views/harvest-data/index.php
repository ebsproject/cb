<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\bootstrap\Modal;
use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Renders harvest data input step for Harvest Manager
 */

// Define URLs
$urlUpdateHarvestData = Url::to(['harvest-data/update']);
$urlCheckHarvestStatusAndState = Url::to(['harvest-data/check-harvest-status-and-state']);
$urlRenderBulkUpdateModal = Url::to(['harvest-data/render-bulk-update-modal']);
$urlRenderAdditionalHarvestModal = Url::to(['harvest-data/render-additional-harvest-modal']);
$urlCheckIfRecordsHaveHarvestData = Url::to(['harvest-data/check-if-records-have-harvest-data']);
$urlRenderBulkDeleteModal = Url::to(['harvest-data/render-bulk-delete-modal']);
$urlRenderRevertHarvestModal = Url::to(['harvest-data/render-revert-harvest-modal']);
$urlHarvestDataPlots = Url::to(['harvest-data/plots', 'program' => $program, 'occurrenceId' => $occurrenceId]);
$urlHarvestDataCrosses = Url::to(['harvest-data/crosses', 'program' => $program, 'occurrenceId' => $occurrenceId]);
$urlRenderObservationDataModal = Url::to(['harvest-data/render-observation-data-modal']);
$newNotificationsUrl = Url::toRoute(['/harvestManager/harvest-data/new-notifications-count']);
$notificationsUrl = Url::toRoute(['/harvestManager/harvest-data/push-notifications']);
$urlRemoveObservationVariable = Url::toRoute(['/harvestManager/harvest-data/remove-observation-variable']);
$urlSaveToSession = Url::toRoute(['/harvestManager/harvest-data/save-to-session']);
$urlToggleFilterMode = Url::toRoute(['/harvestManager/harvest-data/toggle-filter-mode']);
$urlGetFailedRecords = Url::to(['harvest-data/get-failed-records']);
$urlRenderHarvestSummaryModal = Url::to(['harvest-data/render-harvest-summary-modal']);

// Render harvest manager tabs
echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/tab_main.php',[
    'program' => $program,
    'occurrenceId' => $occurrenceId,
    'dataLevel' => $dataLevel,
    'experimentType' => $experimentType,
    'occurrenceName' => $occurrenceName,
    'cpnCheck' => $cpnCheck,
    'browserId' => $browserId,
]);

echo '<div id="hm-bg-alert-div"></div>';

$observationDataFilters = '';
if(!empty($observationFilter)) {
    // defaults
    $colorClass = "grey darken-3";
    $currentValues = (array) $observationFilter['fields'];
    $displayNames = (array) $observationFilter['displayNames'];
    $dataLevel = $observationFilter['dataLevel'];
    
    // with/without display
    $withData = $observationFilter['withData'];
    $withString = "WITHOUT";
    $switchString = "WITH";
    $withColor = "deep-orange darken-4";
    if($withData) {
        $withString = "WITH";
        $switchString = "WITHOUT";
        $withColor = "green darken-4";
    }
    $withElement = '<span data-position="top" data-tooltip="Switch to ' . $switchString . '" dataLevel="'.$dataLevel.'" style="cursor: pointer;" class="new badge ' . $withColor . ' hm-tooltipped" id="observation-data-switch-mode" title=""><strong>' . \Yii::t('app', $withString) . '</strong></span> ';

    $observationDataFilters .= '<dl>
            <dt style="margin-bottom:5px">Showing ' . $dataLevel . ' ' . $withElement . ' :</dt>';
    $observationDataFilters .= '<dd>';
    // Display each variable
    foreach($displayNames as $key => $value) {
        $observationDataFilters .= '<span title="Click to remove '.$value.'" dataLevel="'.$dataLevel.'" id="'.$currentValues[$key].'" class="observation-data-filter-badge observation-data-filter-selected badge center '.$colorClass.' white-text" style="padding-left:10px; margin:0px"><i class="material-icons right" style="margin:0px">clear</i>'.$value.'</span>&nbsp;';
    }
    // Remove all button
    $observationDataFilters .= '<span title="Remove all" dataLevel="'.$dataLevel.'" id="observation-data-filter-remove-all" class="observation-data-filter-badge observation-data-filter-remove badge center red lighten-1 white-text" style="padding-left:10px; margin:0px"><i class="material-icons right" style="margin:0px">clear</i>Remove all</span>&nbsp;';
    $observationDataFilters .= '</dd></dl>';
}

// If mode is crosses, display crosses tab
if($displayMode == 'crosses') {
    $items = [
        [
            'label' => '<strong>' . Yii::t('app', 'Crosses') . '</strong>',
            'active' => true,
            'content' => Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/harvest-data/_cross_browser.php',[
                'program' => $program,
                'occurrenceId' => $occurrenceId,
                'dataLevel' => 'crosses',
                'experimentType' => $experimentType,
                'occurrenceName' => $occurrenceName,
                'searchModel' => $searchModel,
                'browserData' => $browserData,
                'germplasmStateScaleValues' => $germplasmStateScaleValues,
                'germplasmTypeScaleValues' => $germplasmTypeScaleValues,
                'browserConfig' => $browserConfig,
                'controllerModel' => $controllerModel,
                'observationDataFilters' => $observationDataFilters,
                'statusChips' => $statusChips,
                'toolbarFilters' => $toolbarFilters,
                'inputColumns' => $inputColumns
            ]),
            'url' => $urlHarvestDataCrosses
        ],
        [
            'label' => Yii::t('app', ' Plots'),
            'active' => false,
            'url' => $urlHarvestDataPlots
        ]
    ];
}
// Else, display plots tab
else {
    $items = [
        [
            'label' => Yii::t('app', ' Crosses'),
            'active' => false,
            'url' => $urlHarvestDataCrosses
        ],
        [
            'label' => '<strong>' . Yii::t('app', 'Plots') . '</strong>',
            'active' => true,
            'content' => Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/harvest-data/_plot_browser.php',[
                'program' => $program,
                'occurrenceId' => $occurrenceId,
                'dataLevel' => 'plots',
                'experimentType' => $experimentType,
                'occurrenceName' => $occurrenceName,
                'searchModel' => $searchModel,
                'browserData' => $browserData,
                'germplasmStateScaleValues' => $germplasmStateScaleValues,
                'germplasmTypeScaleValues' => $germplasmTypeScaleValues,
                'browserConfig' => $browserConfig,
                'controllerModel' => $controllerModel,
                'observationDataFilters' => $observationDataFilters,
                'stage' => $stage,
                'toolbarFilters' => $toolbarFilters,
                'statusChips' => $statusChips,
                'inputColumns' => $inputColumns
            ]),
            'url' => $urlHarvestDataPlots
        ]
    ];
}

// Render tabs
echo '<div class="white tabs-space" style="padding:5px;">' .
    TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_LEFT,
    'sideways' => true,
    'encodeLabels' => false,
    'pluginOptions' => [
    'enableCache' => true
    ],
]).
'</div>';

// ---------------- BULK UPDATE ----------------

// Modal for bulk update
Modal::begin([
    'id' => 'hm-hd-bulk-update-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">edit</i> Bulk Update</h4>',
    'footer' =>
    Html::a(\Yii::t('app', 'Apply to selected (0)'), '#', 
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-update-btn',
            'id' => 'hm-hd-bulk-update-selected-btn', 
            'data-dismiss' => 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-bulk-update-confirmation-modal",
            'title' => \Yii::t('app', 'Update selected records')
        ]
    ) . '&emsp;'
    . Html::a(\Yii::t('app', 'Apply to all (0)'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-update-btn',
            'id' => 'hm-hd-bulk-update-all-btn',
            'data-dismiss' => 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-bulk-update-confirmation-modal"
        ]
    ) . '&emsp;',
    'size' =>'modal-lg',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 1040;'
    ]
]);
echo '<div id="update-progress"></div><div id="hm-hd-bulk-update-modal-body"></div>';
Modal::end();

// Modal for confirming bulk update
Modal::begin([
    'id' => 'hm-hd-bulk-update-confirmation-modal',
    'header' => '<h4>Confirm Bulk Update</h4>',
    'footer' =>
    Html::a(\Yii::t('app','Cancel'), ['#'],
            [
                'id'=>'hm-hd-bulk-update-cancel-btn',
                'data-dismiss'=>'modal',
                'data-toggle' => "modal",
                'data-target' => "#hm-hd-bulk-update-modal",
            ]
        ). '&emsp;'
    . '<span class="hidden" id="hm-hd-bulk-update-zero-update">&emsp;</span>'
    . '<span class="hm-hd-bulk-update-overwrite-update-true">' . '&emsp;'
    . Html::a(\Yii::t('app', 'Overwrite'), '#', 
        [
            'data-dismiss'=> 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-bulk-update-modal",
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-update-confirm-btn-allow',
            'id' => 'hm-hd-bulk-update-allow-overwrite-btn',
            'title' => \Yii::t('app', 'Overwrite existing values')
        ]
    ) . '&emsp;' 
    . Html::a(\Yii::t('app', 'Skip'), '#',
        [
            'data-dismiss'=> 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-bulk-update-modal",
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-update-confirm-btn-allow',
            'id' => 'hm-hd-bulk-update-skip-overwrite-btn',
            'title' => \Yii::t('app', 'Skip current operation')
        ]
    )
    .'</span>' 
    .'<span class="hm-hd-bulk-update-overwrite-update-false">' . '&emsp;'
    . Html::a(\Yii::t('app', 'Apply Changes'), '#',
        [
            'data-dismiss' => 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-bulk-update-modal", 
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-update-confirm-btn-allow',
            'id' => 'hm-hd-bulk-update-apply-changes-btn',
            'title' => \Yii::t('app', 'Save')
        ]
    )
    .'</span>'
    .'<span class="hm-hd-bulk-update-proceed-bg-update">' . '&emsp;'
    . Html::a(\Yii::t('app', 'Proceed'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-update-confirm-btn-proceed-bg',
            'id' => 'hm-hd-bulk-update-proceed-bg-btn',
            'title' => \Yii::t('app', 'Proceed')
        ]
    )
    .'</span>'
    . '<span class="hidden" id="hm-hd-bulk-update-overwrite-update-true-space">&emsp;</span>',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="hm-hd-bulk-update-confirmation-modal-body"></div><div id="hm-hd-bulk-update-confirmation-modal-additional-main"><div class="red lighten-4 red-text text-darken-2" id="hm-hd-bulk-update-confirmation-modal-additional-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; border-radius: 5px"></div></div>';
echo '<div class="blue lighten-4 blue-text text-darken-2" id="hm-hd-bulk-update-proceed-bg-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; border-radius: 5px">'
    . '<p style="font-size:115%;">Some records may already have harvest data. Tick the box below to overwrite existing harvest data. Otherwise, leave box unticked to skip.</p>'
    . '<input type="checkbox" class="filled-in hm-hd-bu-bg-overwrite-chk" data-id-string="" id="hm-hd-bu-bg-overwrite-chk"/>'
    . '<label style="padding-left: 20px; font-weight: normal; color: #1976D2;" for="hm-hd-bu-bg-overwrite-chk">&nbsp;&nbsp; Overwrite existing data <i>(optional)</i></label>'
. '</div>';
Modal::end();

// ---------------- BULK DELETE ----------------

// Modal for bulk delete
Modal::begin([
    'id' => 'hm-hd-bulk-delete-modal',
    'header' => '<h4 id="hm-hd-bulk-delete-modal-header"><i class="material-icons" style="vertical-align:bottom">delete</i> Bulk Delete</h4>',
    'footer' =>
    Html::a(\Yii::t('app', 'Cancel'), '#', 
        [
            'data-dismiss'=> 'modal', 
            'class' => 'modal-close hm-hd-bulk-delete-btn hm-hd-bd-cancel-btn disabled',
            'id' => 'hm-hd-bulk-delete-cancel-btn'
        ]
    ) . '&emsp;'.
    '<span id="hm-hd-bulk-delete-btn-group">' .
    Html::a(\Yii::t('app', 'Delete Selected'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-delete-btn hm-hd-bulk-delete-proceed-btn hm-hd-bd-delete-btn disabled',
            'id' => 'hm-hd-bulk-delete-selected-btn',
            'data-dismiss' => 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-bulk-delete-confirmation-modal"
        ]
    ) . '&emsp;'.
    Html::a(\Yii::t('app', 'Delete all'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-delete-btn hm-hd-bulk-delete-proceed-btn hm-hd-bd-delete-btn disabled',
            'id' => 'hm-hd-bulk-delete-all-btn',
            'data-dismiss' => 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-bulk-delete-confirmation-modal"
        ]
    ). '&emsp;'.
    '</span>' .
    '<span id="hm-hd-revert-harvest-btn-group">' .
    Html::a(\Yii::t('app', 'Proceed'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-revert-harvest-btn hm-hd-revert-harvest-proceed-btn hm-hd-bd-revert-btn disabled',
            'id' => 'hm-hd-revert-harvest-proceed-btn'
        ]
    ). '&emsp;'.
    '</span>' .
    Html::a(\Yii::t('app', 'Close'), '#',
        [
            'data-dismiss' => 'modal',
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bulk-delete-btn hm-hd-bd-done-btn disabled',
            'id' => 'hm-hd-bulk-delete-done-btn'
        ]
    ) . '&emsp;',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 1040;'
    ]
]);
echo '<div id="hm-hd-bd-delete-progress"></div><div id="hm-hd-bulk-delete-modal-body"></div>';
echo '<div class="red lighten-4 red-text text-darken-2" id="hm-hd-bulk-delete-red-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; border-radius: 5px"></div>';
Modal::end();

// Modal for confirming bulk delete
Modal::begin([
    'id' => 'hm-hd-bulk-delete-confirmation-modal',
    'header' => '<h4>Confirm Bulk Delete</h4>',
    'footer' =>
    Html::a(\Yii::t('app', 'Acknowledge'), '#',
        [
            'data-dismiss'=> 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-bulk-delete-modal",
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bd-confirmation-acknowledge-grp disabled',
            'id' => 'hm-hd-bd-confirmation-acknowledge-btn'
        ]
    ) . '&emsp;' .
    Html::a(\Yii::t('app','Cancel'), ['#'],
            [
                'id'=>'hm-hd-bd-confirmation-cancel-btn',
                'class' => 'modal-close hm-hd-bd-confirmation-default-grp disabled',
                'data-dismiss'=>'modal',
                'data-toggle' => "modal",
                'data-target' => "#hm-hd-bulk-delete-modal",
            ]
        ). '&emsp;' .
    Html::a(\Yii::t('app', 'Delete'), '#',
        [
            'data-dismiss'=> 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-bulk-delete-modal",
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-bd-confirmation-default-grp disabled',
            'id' => 'hm-hd-bd-confirmation-delete-btn'
        ]
    ) . '&emsp;</span>',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="hm-hd-bulk-delete-confirmation-modal-body"></div><div id="hm-hd-bulk-delete-confirmation-modal-additional-main"><div class="red lighten-4 red-text text-darken-2" id="hm-hd-bulk-delete-confirmation-modal-additional-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; border-radius: 5px"></div></div>';
Modal::end();

// ---------------- ADDITIONAL HARVEST ----------------

// Modal for additional harvest
Modal::begin([
    'id' => 'hm-hd-addl-harvest-modal',
    'header' => '<h4 id="hm-hd-addl-harvest-modal-header"><i class="material-icons" style="vertical-align:bottom">add</i> Additional Harvest</h4>',
    'footer' =>
    Html::a(\Yii::t('app', 'Cancel'), '#', 
        [
            'data-dismiss'=> 'modal', 
            'class' => 'modal-close hm-hd-addl-harvest-btn hm-hd-ah-cancel-btn',
            'id' => 'hm-hd-addl-harvest-cancel-btn'
        ]
    ) . '&emsp;'.
    Html::a(\Yii::t('app', 'Confirm'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-addl-harvest-btn disabled',
            'id' => 'hm-hd-addl-harvest-btn-confirm',
            'data-dismiss' => 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-addl-harvest-confirmation-modal"
        ]
    ) . '&emsp;',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 1040;'
    ]
]);
echo '<div id="update-progress"></div><div id="hm-hd-addl-harvest-modal-body"></div>';
Modal::end();

// Modal for confirming additional harvest
Modal::begin([
    'id' => 'hm-hd-addl-harvest-confirmation-modal',
    'header' => '<h4>Confirm Additional Harvest</h4>',
    'footer' =>
    Html::a(\Yii::t('app','Cancel'), ['#'],
        [
            'id'=>'hm-hd-addl-harvest-confirmation-cancel-btn',
            'class'=>'hm-hd-addl-harvest-confirm-btn',
            'data-dismiss'=>'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-hd-addl-harvest-modal",
        ]
    ) . '&emsp;'
    . Html::a(\Yii::t('app', 'Proceed'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-addl-harvest-confirm-btn hm-hd-addl-harvest-confirm-btn-allow disabled',
            'id' => 'hm-hd-addl-harvest-proceed-btn',
            'title' => \Yii::t('app', 'Proceed')
        ]
    ) . '&emsp;',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="hm-hd-addl-harvest-confirmation-modal-body"></div><div id="hm-hd-addl-harvest-confirmation-modal-additional-main"><div class="red lighten-4 red-text text-darken-2" id="hm-hd-addl-harvest-confirmation-modal-additional-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; border-radius: 5px"></div></div>';
Modal::end();

// ---------------- FILTER BY OBSERVATION DATA ----------------

// Modal for filter by observation data
Modal::begin([
    'id' => 'hm-hd-observation-data-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">visibility</i> Filter by Observation Data</h4>',
    'footer' =>
    Html::a(\Yii::t('app','Cancel'), '#',
            [
                'class'=>'hm-hd-observation-data-btn hm-hd-od-cancel-btn',
                'id'=>'hm-hd-observation-data-cancel-btn',
                'data-dismiss' => 'modal'
            ]
        ). '&emsp;' .
    Html::a(\Yii::t('app', 'Remove'), '#',
        [
            'class' => 'btn btn-danger waves-effect waves-light modal-close hm-hd-observation-data-btn hm-hd-od-remove-btn disabled',
            'id' => 'hm-hd-observation-data-remove-btn'
        ]
    ) . '&emsp;' .
    Html::a(\Yii::t('app', 'Apply'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-hd-observation-data-btn hm-hd-od-apply-btn disabled',
            'id' => 'hm-hd-observation-data-apply-btn'
        ]
    ) . '&emsp;',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 1040;'
    ]
]);
echo '<div id="hm-hd-observation-data-modal-body"></div>';
Modal::end();

// -------------------------- SHOW ERROR OF FAILED HARVEST -------------------------------
// Modal for showing failed harvest info
Modal::begin([
    'id' => 'hm-hd-failed-harvest-info-modal',
    'header' => '<h4>Failed Harvest Information</h4>',
    'footer' =>
    Html::a(\Yii::t('app', 'Close'), '#',
        [
            'data-dismiss'=> 'modal',
            'class' => 'btn btn-primary waves-effect waves-light modal-close',
            'id' => 'hm-hd-failed-harvest-info-close-btn'
        ]
    ) . '&emsp;</span>',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="hm-hd-failed-harvest-info-modal-body"></div>';
Modal::end();

// ------------------------- SHOW HARVEST SUMMARY NOTIFICATION ----------------------------
// Modal for showing harvest status notfication
$summaryDisplayMode = $displayMode == 'crosses' ? 'Cross' : 'Plot';

Modal::begin([
    'id' => 'hm-hd-hv-summary-modal',
    'header' => '<h4>
                    <i class="material-icons" style="vertical-align:bottom">assignment</i> ' . $summaryDisplayMode . ' Harvest Summary
                </h4>',
    'footer' =>
    Html::a(
        \Yii::t('app', 'Close'),
        '#',
        [
            'data-dismiss' => 'modal',
            'class' => 'btn btn-primary waves-effect waves-light modal-close',
            'id' => 'hm-hd-hv-summary-close-btn'
        ]
    ) . '&emsp;</span>',
    'options' => [
        'data-backdrop' => 'static',
        'style' => 'z-index: 2000;'
    ]
]);

echo '<div id="hm-hd-hv-summary-modal-body"></div>';
Modal::end();

?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<?php

// ---------------------------------------------

// Encode arrays to be used in jQuery
$methodNumVarCompatJSON = json_encode($browserConfig['method_numvar_compat']);
$numericVariablesJSON = json_encode(array_values($browserConfig['numeric_variables']));
$bulkUpdateConfigJSON = json_encode($browserConfig['bulk_update_config']);
$bulkDeleteConfigJSON = json_encode($browserConfig['bulk_delete_config']);

$script = <<< JS
var methodNumVarCompat = $methodNumVarCompatJSON;
var numericVariables = $numericVariablesJSON;
var bulkUpdateConfig = $bulkUpdateConfigJSON;
var bulkDeleteConfig = $bulkDeleteConfigJSON;
var dataLevel = '$dataLevel';
var occurrenceId = $occurrenceId;
var locationId = $locationId;
var harvestDataUpdateThreshold = $harvestDataUpdateThreshold;
var numVarAutoChange = false;
var newNotificationsUrl='$newNotificationsUrl';
var notificationsUrl='$notificationsUrl';
var errorOnUpdate = false;
var inputVal = null;

var harvestCompleteStatus = '<span title="Seeds and packages have been created" class="new badge center green darken-2"><strong>HARVEST COMPLETE</strong></span>';

notifDropdown();
renderNotifications();
updateNotif();

// Check for new notifications every 7.5 seconds
notificationInterval=setInterval(updateNotif, 7500);

$(document).on('ready pjax:success', function(e) {
    e.preventDefault();
    $('#notif-btn-id').tooltip();
    notifDropdown();
    $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>');
    renderNotifications();
    updateNotif();
    // Resize harvest data column values
    resizeHarvestDataColumnValues();
});

$("document").ready(function(){
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();
	// Adjust browser layout
    adjustLayout();
    // Resize harvest data column values
    resizeHarvestDataColumnValues();
    
	// On window resize event, adjust browser layout
    $(window).resize(function() {
        adjustLayout();
    });

    // Date picker input change event
    $(document).on("change",".hm-hdata-datepicker-input", function(e) {
        // Parse element id
        var elementId = $(this).attr('id');
        var eIdParts = elementId.split('-');
        var variableAbbrev = eIdParts[0];
        var idType = eIdParts[1];
        var dbId = eIdParts[2];
        // Get value
        var value = $(this).val();

        // Send data to data terminal
        performUpdate(elementId, dbId, idType, variableAbbrev, value);
    });

    // Select2 input change event
    $(document).on("change",".hm-hdata-select2-input", function(e) {
        // Parse element id
        var elementId = $(this).attr('id');
        var eIdParts = elementId.split('-');
        var variableAbbrev = eIdParts[0];
        var idType = eIdParts[1];
        var dbId = eIdParts[2];
        // Get value
        var value = $(this).val();

        // If variable is harvest method, skip.
        // A separate event handler will handle the update
        if(variableAbbrev == "HV_METH_DISC") return;

        // Send data to data terminal
        performUpdate(elementId, dbId, idType, variableAbbrev, value);
    });

    // Harvest method input change event
    $(document).on("change",".hm-hdata-HV_METH_DISC-input", function(e) {
        // Parse element id
        var elementId = $(this).attr('id');
        var eIdParts = elementId.split('-');
        var variableAbbrev = eIdParts[0];
        var idType = eIdParts[1];
        var dbId = eIdParts[2];
        // Get element attributes
        var value = $(this).val();
        // Get current config
        var currentConfig = JSON.parse($(this).attr('current-config'));
        // Unpack config
        var inputOptions = currentConfig.inputOptions != null ? currentConfig.inputOptions : {};
        var methodNumVarCompat = inputOptions.methodNumVarCompat != null ? inputOptions.methodNumVarCompat : [];
        var currentMethodNumVars = methodNumVarCompat[value] != null ? methodNumVarCompat[value] : [];
        var currentMethodNumVarsCount = currentMethodNumVars.length;
        var numVars = inputOptions.numVars != null ? inputOptions.numVars : [];
        var deleteNumVars = numVars.map((x) => x);

        // Hide all number inputs for the current tow
        $('.hm-hdata-number-input-'+dbId).addClass('hidden');

        // If value is empty, or if applicable numvars is emtpy,
        // Show placeholder
        if(value == '' || currentMethodNumVars.length == 0) {
            $('#NUMVAR_PLACEHOLDER-' + idType +'-'+dbId).removeClass('hidden');
        }
        else {
            // Show applicable numvars
            for(var i = 0; i < currentMethodNumVarsCount; i++) {
                var abbrev = currentMethodNumVars[i];
                // Show element
                $('#' + abbrev + '-' + idType +'-'+dbId).removeClass('hidden');
                // Remove numvar from array of numvars to delete
                var indexOfRequired = deleteNumVars.indexOf(abbrev);
                if(indexOfRequired > -1) deleteNumVars.splice(indexOfRequired, 1);
            }
        }

        // Send data to data terminal
        performUpdate(elementId, dbId, idType, variableAbbrev, value, {
            deleteNumVars: deleteNumVars,
            idType: idType,
            dbId: dbId
        });
    });

    // Numeric variable input change event
    $(document).on("change",".hm-hdata-number-input", function(e) {
        // If change came from within this function, exit
        if(numVarAutoChange) {
            numVarAutoChange = false;
            return;
        }

        // Parse element id
        var elementId = $(this).attr('id');
        var eIdParts = elementId.split('-');
        var variableAbbrev = eIdParts[0];
        var idType = eIdParts[1];
        var dbId = eIdParts[2];
        // Get value
        var value = $(this).val();
        // Validate input
        var subType = $(this).attr('subType');
        var minimum = $(this).attr('min');
        var placeholder = $(this).attr('placeholder');
        var regex = new RegExp('');
        // Format value based on sub type
        if(subType == 'single_int') {
            regex = new RegExp('^(' + minimum + '|[1-9][0-9]*)$');
            value = value != '' ? parseInt(value).toString() : '';
        }
        else if(subType == 'comma_sep_int') {
            value = cleanCommaSepInt(value);
            regex = new RegExp('^[1-9][0-9]*(,[1-9][0-9]*)*$');
        }
        // Triger change to update value in display
        numVarAutoChange = true;
        $(this).val(value).trigger('change');
        // If input does not match regex, display error
        if(value != '' && !regex.test(value)) {
            var notif = "<i class='material-icons orange-text left'>warning</i> Invalid input for " + placeholder;
            Materialize.toast(notif, 3500);
            $(this).css({"background-color": "#ffffcc"});
            return;
        }
        // Remove background color
        $(this).css({"background-color": ""});

        // Send data to data terminal
        performUpdate(elementId, dbId, idType, variableAbbrev, value);
    });

    // Observation data remove all click event
    $(document).on("click","#observation-data-filter-remove-all", function() {
        var dataLevel = $(this).attr('dataLevel');
        var name = "observation-filter-" + dataLevel + '-' + occurrenceId;

        // Clear session variable
        $.ajax({
            url: '$urlSaveToSession',
            type: 'post',
            data: {
                name: name,
                value: ''
            },
            success: function(response) {
                $("#hm-hd-reset-"+dataLevel+"-btn").click();
            }
        });
    });

    // Observation data click event
    $(document).on("click",".observation-data-filter-selected", function(e) {
        var id = $(this).attr('id');
        var dataLevel = $(this).attr('dataLevel');
        var sessionName = "observation-filter-" + dataLevel + '-' + occurrenceId;
        
        $.ajax({
            url: '$urlRemoveObservationVariable',
            type: 'post',
            data: {
                sessionName: sessionName,
                toRemove: id
            },
            success: function(response) {
                $("#hm-hd-reset-"+dataLevel+"-btn").click();
            }
        });
    });

    // Observation data switch mode click event
    $(document).on("click","#observation-data-switch-mode", function(e) {
        var dataLevel = $(this).attr('dataLevel');
        var sessionName = "observation-filter-" + dataLevel + '-' + occurrenceId;

        // Toggle mode in the session variable
        $.ajax({
            url: '$urlToggleFilterMode',
            type: 'post',
            data: {
                sessionName: sessionName
            },
            success: function(response) {
                $("#hm-hd-reset-"+dataLevel+"-btn").click();
            }
        });
    });

    // Additional harvest button click event
    $(document).on("click","#hm-hdata-additional-harvest-btn", function(e) {
        e.preventDefault();
        // Disable buttons
        $('#hm-hd-addl-harvest-btn-confirm').show();
        $('.hm-hd-addl-harvest-btn').addClass('disabled');
        // Add loading indicator
        $('#hm-hd-addl-harvest-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        var dataLevel = $(this).attr('data-level');
        var gridId = 'dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-table-con';
        var selectionStorageId = occurrenceId + "_" + gridId;
        var selected = getSelection(selectionStorageId);
        var selectedCount = selected.length;
        var addlHarvestThresh = 100; // TEMPORARILY HARD-CODED. TODO: MAKE CONFIGURABLE IN FUTURE SPRINTS.

        // If the selected count exceeds the update threshold, display notice
        if (selectedCount == 0) {
            var message = modalTextHM('No records have been selected. Please select records with harvest status = ' + harvestCompleteStatus + ' then try again.');
            $('#hm-hd-addl-harvest-modal-body').html(message);
            $('#hm-hd-addl-harvest-cancel-btn').removeClass('disabled');
            return;
        }
        else if(selectedCount > addlHarvestThresh) {
            var message = modalTextHM('The number of selected records (' + selectedCount + ') exceeds the maximum limit of <strong>' + addlHarvestThresh + '</strong> records. ' +
                'Kindly select fewer records and try again.');
            $('#hm-hd-addl-harvest-modal-body').html(message);
            $('#hm-hd-addl-harvest-cancel-btn').removeClass('disabled');
            return;
        }

        // Check if the none of the selected records have status = COMPLETED
        $.ajax({
            url: '$urlCheckHarvestStatusAndState',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId,
                recordIds: JSON.stringify(selected),
                dataLevel: dataLevel
            },
            success: function(response) {
                $('#hm-hd-addl-harvest-cancel-btn').removeClass('disabled');
                var statusSummary = JSON.parse(response);
                var completedCount = statusSummary.completedCount;

                if (completedCount == 0) {
                    // Display message in the modal body
                    var message = modalTextHM('Unable to proceed with additional harvest because none of the records selected have harvest status = ' + harvestCompleteStatus + '. Kindly select other records and try again.');
                    $('#hm-hd-addl-harvest-modal-body').html(message);
                    $('#hm-hd-addl-harvest-btn-confirm').hide();
                    return;
                }

                // Render modal contents
                $.ajax({
                    url: '$urlRenderAdditionalHarvestModal',
                    type: 'post',
                    datatType: 'json',
                    cache: false,
                    data: {
                        occurrenceId: occurrenceId,
                        locationId: locationId,
                        recordIds: JSON.stringify(selected),
                        dataLevel: dataLevel,
                        bulkUpdateConfig: JSON.stringify(bulkUpdateConfig)
                    },
                    success: function(response) {
                        // Add modal contents to the modal body
                        $('#hm-hd-addl-harvest-modal-body').html(response);
                    },
                    error: function(jqXHR, exception) {
                        var errorMessage = modalTextHM('There was a problem while loading record information.', false, true);
                        $('#hm-hd-bulk-update-modal-body').html(errorMessage);
                    }
                });
            },
            error: function(jqXHR, exception) {
                var errorMessage = modalTextHM('There was a problem while loading record information.', false, true);
                $('#hm-hd-addl-harvest-modal-body').html(errorMessage);
            }
        });
    });

    // Bulk update button click event
    $(document).on("click","#hm-hdata-bulk-update-btn", function(e) {
        e.preventDefault();
        // Disable buttons
        $('.hm-hd-bulk-update-btn').addClass('disabled');
        // Add loading indicator
        $('#hm-hd-bulk-update-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        // Set variables
        var dataLevel = $(this).attr('data-level');
        var gridId = 'dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-table-con';
        var selectionStorageId = occurrenceId + "_" + gridId;
        var selected = getSelection(selectionStorageId);
        var selectedCount = selected.length;

        // If the selected count exceeds the update threshold, display notice
        if(selectedCount > harvestDataUpdateThreshold) {
            var message = modalTextHM('The number of selected records (' + selectedCount + ') exceeds the maximum limit of <strong>' + harvestDataUpdateThreshold + '</strong> records. ' +
                'Kindly select fewer records and try again.');
            $('#hm-hd-bulk-update-modal-body').html(message);
            return;
        }

        // Check if the harvest status of all records are COMPLETED or DELETION_IN_PROGRSS
        $.ajax({
            url: '$urlCheckHarvestStatusAndState',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId,
                recordIds: JSON.stringify(selected),
                dataLevel: dataLevel
            },
            success: function(response) {
                var statusSummary = JSON.parse(response);

                // If all selected records are not valid to update, display message
                if(selectedCount > 0 && selectedCount == statusSummary.totalUnupdateable) {
                    var statusCount = statusSummary.completedCount + statusSummary.deletionCount;
                    var status = statusSummary.statusList;
                    var sStatus = statusCount != 1 ? 's' : '';
                    for(var i = 0; i < status.length; i++) {
                        status[i] = status[i].replace(/\_/g,' ');
                    }
                    var stateCount = statusSummary.invalidStateCount;
                    var state = statusSummary.stateList;
                    var sState = stateCount != 1 ? 's' : '';
                    for(var i = 0; i < state.length; i++) {
                        state[i] = state[i].replace(/\_/g,' ');
                    }
                    // Add details (if any)
                    var strItems = '';
                    if(statusCount > 0) strItems += '<br> • The harvest status of <strong>' + statusCount + ' record' + sStatus + '</strong> is <strong>' + status.join(" or ") + '</strong>. ';
                    if(stateCount > 0) strItems += '<br> • The state of <strong>' + stateCount + ' record' + sState + '</strong> is <strong>' + state.join(" or ") + '</strong>. ';
                    // Display message in the modal body
                    var message = modalTextHM('Bulk update cannot be performed due to the following reasons: <br>' + strItems
                        + "<br><br> Kindly select other records and try again.");
                    $('#hm-hd-bulk-update-modal-body').html(message);
                    return;
                }

                // Render modal contents
                $.ajax({
                    url: '$urlRenderBulkUpdateModal',
                    type: 'post',
                    datatType: 'json',
                    cache: false,
                    data: {
                        occurrenceId: occurrenceId,
                        locationId: locationId,
                        recordIds: JSON.stringify(selected),
                        dataLevel: dataLevel,
                        bulkUpdateConfig: JSON.stringify(bulkUpdateConfig)
                    },
                    success: function(response) {
                        // Add modal contents to the modal body
                        $('#hm-hd-bulk-update-modal-body').html(response);
                    },
                    error: function(jqXHR, exception) {
                        var errorMessage = modalTextHM('There was a problem while loading record information.', false, true);
                        $('#hm-hd-bulk-update-modal-body').html(errorMessage);
                    }
                });
            },
            error: function(jqXHR, exception) {
                var errorMessage = modalTextHM('There was a problem while loading record information.', false, true);
                $('#hm-hd-bulk-update-modal-body').html(errorMessage);
            }
        });
    });

    // Show/hide observation data button click event
    $(document).on("click","#hm-hd-observation-data-button-main", function(e) {
        // Disable apply button
        $('#hm-hd-observation-data-apply-btn').addClass('disabled');
        var dataLevel = $(this).attr('data-level');
        
        // render modal
        $.ajax({
            url: '$urlRenderObservationDataModal',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId,
                config: JSON.stringify(bulkDeleteConfig),
                dataLevel: dataLevel
            },
            success: function(response) {
                // Add modal contents to the modal body
                $('#hm-hd-observation-data-modal-body').html(response);
            },
            error: function(jqXHR, exception) {
                var errorMessage = modalTextHM('There was a problem while rendering. Please try again later.', false, true);
                $('#hm-hd-observation-data-modal-body').html(errorMessage);
            }
        });
    });

    // Bulk delete button click event
    $(document).on("click","#hm-hdata-bulk-delete-btn", function(e) {
        // Set modal display
        var modalHeader = '<i class="material-icons" style="vertical-align:bottom">delete</i> Bulk Delete';
        $('#hm-hd-bulk-delete-modal-header').html(modalHeader);
        // Button visibility
        $('#hm-hd-bulk-delete-btn-group').show();
        $('#hm-hd-revert-harvest-btn-group').hide();
        // Disable buttons
        $('.hm-hd-bulk-delete-btn').addClass('disabled');
        $('.hm-hd-bulk-delete-btn').show();
        // Add loading indicator
        $('#hm-hd-bulk-delete-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#hm-hd-bulk-delete-modal-body').show();
        $("#hm-hd-bd-delete-progress").hide();
        // Hide red div
        $('#hm-hd-bulk-delete-red-content').html('');
        $('#hm-hd-bulk-delete-red-content').hide();
        // Set variables
        var dataLevel = $(this).attr('data-level');
        var gridId = 'dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-table-con';
        var selectionStorageId = occurrenceId + "_" + gridId;
        var selected = getSelection(selectionStorageId);
        var selectedCount = selected.length;

        // Check if the harvest status of all records are COMPLETED or DELETION_IN_PROGRSS
        $.ajax({
            url: '$urlCheckIfRecordsHaveHarvestData',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId,
                recordIds: JSON.stringify(selected),
                dataLevel: dataLevel
            },
            success: function(response) {
                var hasHarvestData = JSON.parse(response);
                
                if(selectedCount > 0 && !hasHarvestData) {
                    // Display message in the modal body
                    var message = modalTextHM("The selected records do not have any harvest data. Kindly select other records and try again.");
                    // Hide progress bar div
                    $('#hm-hd-bd-delete-progress').html('');
                    $('#hm-hd-bd-delete-progress').hide();
                    // Show main modal body div
                    $('#hm-hd-bulk-delete-modal-body').html(message);
                    $('#hm-hd-bulk-delete-modal-body').show();
                    // Hide delete buttons
                    $('.hm-hd-bulk-delete-btn').hide();
                    // Show and enable done button
                    $('.hm-hd-bd-done-btn').show();
                    $('.hm-hd-bd-done-btn').removeClass('disabled');
                    return;
                }

                // Render modal contents
                $.ajax({
                    url: '$urlRenderBulkDeleteModal',
                    type: 'post',
                    datatType: 'json',
                    cache: false,
                    data: {
                        occurrenceId: occurrenceId,
                        locationId: locationId,
                        recordIds: JSON.stringify(selected),
                        dataLevel: dataLevel,
                        bulkDeleteConfig: JSON.stringify(bulkDeleteConfig)
                    },
                    success: function(response) {
                        // Hide progress bar div
                        $('#hm-hd-bd-update-progress').html('');
                        $('#hm-hd-bd-update-progress').hide();
                        // Add modal contents to the modal body
                        $('#hm-hd-bulk-delete-modal-body').html(response);
                        $('#hm-hd-bulk-delete-modal-body').show();
                    },
                    error: function(jqXHR, exception) {
                        var errorMessage = modalTextHM('There was a problem while loading record information.', false, true);
                        $('#hm-hd-bulk-delete-modal-body').html(errorMessage);
                    }
                });
            },
            error: function(jqXHR, exception) {
                var errorMessage = modalTextHM('There was a problem while loading record information.', false, true);
                $('#hm-hd-bulk-delete-modal-body').html(errorMessage);
            }
        });
    });

    // Harvest data chip click event
    $(document).on("click",".hm-hd-harvest-status-chip", function(e) {
        var id = $(this).attr('id');
        var title = $(this).attr('title');
        var isSelected = parseInt($(this).attr('is-selected'));
        var checkIcon = "<i class='material-icons hm-occurrence-info'>check</i>";

        // If current state is selected, deselect.
        if (isSelected) {
            $(this).html(title);
            $(this).attr('is-selected', '0');
        }
        // Else, select.
        else {
            $(this).html(checkIcon + " " + title);
            $(this).attr('is-selected', '1');
        }

        // Apply filter
        filterByHarvestStatus();
    });

    // Clear Harvest data click event
    $(document).on("click","#hm-hd-clear-quick-filters", function(e) {
        e.preventDefault();
        // Deselect all
        $.each($('.hm-hd-harvest-status-chip'), function(i, val) {
            var title = $(this).attr('title');
            $(this).html(title);
            $(this).attr('is-selected', '0');
        });

        // clear other filters
        clearInputFilters();

        // Apply filter
        filterByHarvestStatus(true);
    });

    // Revert harvest button click event
    $(document).on("click","#hm-hdata-revert-harvest-btn", function(e) {
        // Set modal display
        var modalHeader = '<i class="material-icons" style="vertical-align:bottom">undo</i> Revert Failed Harvest';
        $('#hm-hd-bulk-delete-modal-header').html(modalHeader);
        // Button visibility
        $('#hm-hd-bulk-delete-btn-group').hide();
        $('#hm-hd-revert-harvest-btn-group').show();
        // Disable buttons
        $('.hm-hd-bulk-delete-btn').addClass('disabled');
        $('.hm-hd-bulk-delete-btn').show();
        // Add loading indicator
        $('#hm-hd-bulk-delete-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#hm-hd-bulk-delete-modal-body').show();
        $("#hm-hd-bd-delete-progress").hide();
        // Hide red div
        $('#hm-hd-bulk-delete-red-content').html('');
        $('#hm-hd-bulk-delete-red-content').hide();
        // Set variables
        var dataLevel = $(this).attr('data-level');
        var gridId = 'dynagrid-harvest-manager-' + dataLevel + '-hdata-grid-table-con';
        var selectionStorageId = occurrenceId + "_" + gridId;
        var selected = getSelection(selectionStorageId);
        var selectedCount = selected.length;

        // Retrieve FAILED records
        $.ajax({
            url: '$urlGetFailedRecords',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId,
                dataLevel: dataLevel
            },
            success: function(response) {
                var records = JSON.parse(response);

                // Render modal contents
                $.ajax({
                    url: '$urlRenderRevertHarvestModal',
                    type: 'post',
                    datatType: 'json',
                    cache: false,
                    data: {
                        occurrenceId: occurrenceId,
                        locationId: locationId,
                        records: JSON.stringify(records),
                        dataLevel: dataLevel,
                        bulkDeleteConfig: JSON.stringify(bulkDeleteConfig)
                    },
                    success: function(response) {
                        var data = JSON.parse(response)
                        var html = data.html
                        var count = data.count
                        // Hide progress bar div
                        $('#hm-hd-bd-update-progress').html('');
                        $('#hm-hd-bd-update-progress').hide();
                        // Hide close button
                        $('.hm-hd-bd-done-btn').hide();
                        $('.hm-hd-bd-cancel-btn').removeClass('disabled');
                        $('.hm-hd-revert-harvest-proceed-btn').removeClass('disabled');
                        // Hide proceed button if no records were selected
                        if (count <= 0) $('#hm-hd-revert-harvest-btn-group').hide();
                        // Add modal contents to the modal body
                        $('#hm-hd-bulk-delete-modal-body').html(html);
                        $('#hm-hd-bulk-delete-modal-body').show();
                    },
                    error: function(jqXHR, exception) {
                        var errorMessage = modalTextHM('There was a problem while loading record information.', false, true);
                        $('#hm-hd-bulk-delete-modal-body').html(errorMessage);
                    }
                });
            },
            error: function(jqXHR, exception) {
                var errorMessage = modalTextHM('There was a problem while loading record information.', false, true);
                $('#hm-hd-bulk-delete-modal-body').html(errorMessage);
            }
        });
    });

    // Show failed harvest info click event
    $(document).on("click",".hm-harvest-status-failed", function(e) {
        // Add loading indicator
        $('#hm-hd-failed-harvest-info-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        // Parse errors
        var errors = $(this).attr('errors');
        var errorsArray = [];

        // Set default string message for when no error messages are found
        var strMsg = "<em>No information available.<em>";

        // If the errors variable is not empty, build new string message
        if (errors !== "") {
            // Split errors by semicolon
            errorsArray = errors.split(";");

            // Build new string message
            strMsg = "The harvest failed due to the following reasons: ";

            // Add each error to the string message
            for(var i = 0; i < errorsArray.length; i++) {
                strMsg += '<br> • ' + errorsArray[i];
            }
        }

        // Format the string message then display in the modal
        var message = modalTextHM(strMsg);
        $('#hm-hd-failed-harvest-info-modal-body').html(message);
    });

    // Show harvest summary information
    $(document).on("click","#hv-summary-btn", function(e){
        e.preventDefault();

        $('#hm-hd-hv-summary-modal-body').html(
            '<div class="progress"><div class="indeterminate"></div></div>'
        );

        var recordId = $(this).attr('data-id');
        var dataType = $(this).attr('data-level');

        $.ajax({
            url: '$urlRenderHarvestSummaryModal',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                recordId: recordId,
                dataType: dataType,
                occurrenceId: occurrenceId
            },
            success: function(response) {
                $('#hm-hd-hv-summary-modal-body').html(response);
                $('#hm-hd-hv-summary-modal-body').show();
            },
            error: function(jqXHR, exception) {

            }
        })
    })
});

// Applies harvest status quick filters
function filterByHarvestStatus(removeRangeFilter = false) {
    // Get selected status values from chips
    var selectedStatus = [];
    $.each($('.hm-hd-harvest-status-chip'), function(i, val) {
        var id = $(this).attr('id');
        // var title = $(this).attr('title');
        // $(this).html(title);
        var isSelected = $(this).attr('is-selected');
        if(parseInt(isSelected)) {
            selectedStatus.push(id);
        }
    });

    // Build strings
    var lvl = dataLevel == 'plots' ? 'plot' : 'cross';
    var gridId = 'dynagrid-harvest-manager-' + lvl + '-hdata-grid-table-con';
    var pageurl = $('#dynagrid-harvest-manager-' + lvl + '-hdata-grid li.active a').attr('href');

    // Include any other toolbar filters
    var toolbarFilters = retrieveToolbarFilters();

    // Perform PJAX reload
    $.pjax.reload(
        {
            container:'#dynagrid-harvest-manager-' + lvl + '-hdata-grid-pjax',
            url: pageurl,
            replace: false,
            type: 'POST',
            data: {
                forceReset:false,
                bulkOperation:false,
                harvestStatus:JSON.stringify(selectedStatus),
                removeRangeFilter:removeRangeFilter
            }
        }
    );
}

// Get toolbar filter values
function retrieveToolbarFilters(){
    var lvl = dataLevel == 'plots' ? 'plot' : 'cross';

    var obj;
    var filterId;
    var bodyParam;
    var filters = {};
    var values;

    $.each($('.toolbar-filter'), function(i, val) {
        obj = $(this);
        filterId = obj.attr('id');

        bodyParam = $('#'+filterId).data('api_body_param');
        values = $('#'+filterId).val();
        if(values != ''){ filters[bodyParam] = 'range: '+values }
    });

    return filters;
}

// Clear toolbar filter fields
function clearInputFilters(){
    var lvl = dataLevel == 'plots' ? 'plot' : 'cross';

    var obj;
    var filterId;
    var bodyParam;

    $.each($('.toolbar-filter'), function(i, val) {
        obj = $(this);
        filterId = obj.attr('id');

        bodyParam = $('#'+filterId).data('api_body_param');
        $('#'+filterId).val('').trigger('change');
    });

    // Perform PJAX reload
    var pageurl = $('#dynagrid-harvest-manager-'+ lvl +'-hdata-grid li.active a').attr('href');
    $.pjax.reload(
        {
            container:'#dynagrid-harvest-manager-' + lvl + '-hdata-grid-pjax',
            url: pageurl,
            replace: false,
            type: 'POST',
            data: {
                forceReset:false,
                bulkOperation:false,
            }
        }
    );
}

/**
 * Pass data onto data terminal
 * @param {integer} elementId the id of the html element performing the update
 * @param {integer} dbId record identifier
 * @param {string} idType type of dbId (plot or cross)
 * @param {array} variableAbbrev array containing variable abbrev to update
 * @param {string} value value to be applied to variable abbrev
 * @param {boolean} hMethodChange object provided when update request is for harvest method
 *                      - holds the data describing which unneeded numeric variables are to be deleted
 */
function performUpdate(elementId, dbId, idType, variableAbbrev, value, hMethodChange = null) {
    // If the trigger came from an error, skip update to avoid an infinite loop
    if (errorOnUpdate) {
        errorOnUpdate = false;
        return;
    }
    // Store input value in global variable
    inputVal = value;

    // Build POST data
    var data = {
        variableAbbrevs: JSON.stringify([variableAbbrev]),
        recordIds: JSON.stringify([dbId]),
        locationId: locationId,
        values: JSON.stringify([[value]]),
        idType: idType
    };

    // Send request via ajax
    $.ajax({
        url: '$urlUpdateHarvestData',
        type: 'post',
        datatType: 'json',
        cache: false,
        data: data,
        success: function(response) {
            // Update old value
            $('#'+elementId).attr('oldValue', inputVal);

            if(hMethodChange != null) {
                // Unpack variables
                var deleteNumVars = hMethodChange.deleteNumVars;
                var idType = hMethodChange.idType;
                var dbId = hMethodChange.dbId;

                // Empty out input field values and delete num vars
                var len = deleteNumVars.length;
                for(var i = 0; i < len; i += 1) {
                    var value = deleteNumVars[i];
                    var tempId = '#' + value + '-' + idType + '-' + dbId;
                    $(tempId).val('').trigger('change');
                }
            }
        },
        error: function() {
            // Set error on update flag to true
            errorOnUpdate = true;
            // Display error toast message
            var notif = "<i class='material-icons red-text'>close</i>&nbsp;Harvest data input was unsuccessful.";
            Materialize.toast(notif, 3500);
            // Revert old value of html input element
            var oldVal = $('#'+elementId).attr('oldValue');
            $('#'+elementId).val(oldVal).trigger('change');
            return;
        }
    });
}

/**
 * Adjust browser layout in relation with the side tabs
 */
function adjustLayout() {
    var windowHeight = $(window).height();
    $(".kv-grid-wrapper").css("height", windowHeight - 500);
    
    var plotBrowserHeight = $('#dynagrid-harvest-manager-plot-hdata-grid-table-con').height();
    var crossBrowserHeight = $('#dynagrid-harvest-manager-cross-hdata-grid-table-con').height();
    var browserHeight = plotBrowserHeight == undefined ? crossBrowserHeight : plotBrowserHeight;
    $('#hm-hdata-plot, #hm-hdata-cross').css('width', $('.tabs-space').width() - 70);
    $('.tabs-space').css('height', browserHeight + 80);
}

/**
 * Formats comma-separated integers. Removes duplicate integers,
 * excess commas, and sorts the integers in ascending order.
 * @param {string} value the comma-separated integer string
 * @returns {string} the formatted comma-separated integer
 */
function cleanCommaSepInt(value) {
    if(value == '' || value.length == 1) return value;
    // Replace 2 or more consecutive commas with just 1 comma
    value = value.replace(/,{2,}/g, ",");
    // If value starts with a comma, remove comma
    if(value.charAt( 0 ) === ',') value = value.slice(1);
    // If value ends with a comma, remove comma
    if(value.charAt( value.length - 1 ) === ',') value = value.slice(0, -1);
    // Remove duplicates
    var pieces = value.split(',');
    var finalPieces = [];
    $.each(pieces, function(idx, val){
        // Add to final pieces
        var integerVal = parseInt(val)
        if(integerVal > 0 && !finalPieces.includes(integerVal)) finalPieces.push(integerVal);
    });
    // Sort in ascending order
    finalPieces.sort( function(a,b) { return a - b; } );
    // Put value back together
    value = finalPieces.join(',')
    // Return final value conjoined with comma
    return value == '' ? '0' : value;
}

/**
 * Shows new notifications
 */
function updateNotif() {
    $.getJSON(newNotificationsUrl, 
    {
        workerName: 'DeleteHarvestRecords|UpdateHarvestData',
        remarks: 'harvest data|revert'
    },
    function(json){
        $('#notif-badge-id').html(json);
        if(parseInt(json)>0){
            $('#notif-badge-id').addClass('notification-badge red accent-2');
        }else{
            $('#notif-badge-id').html('');
            $('#notif-badge-id').removeClass('notification-badge red accent-2');
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Message: "+textStatus+" : "+errorThrown);
        
    });
}

/**
 * Initialize dropdown materialize  for notifications
 */
function renderNotifications(){
    $('#hm-hd-notifs-btn').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: false,
        gutter: 0,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });
}

/**
 * Displays the notification in dropdown UI
 */
function notifDropdown(){
    $('#hm-hd-notifs-btn').on('click',function(e){
        $.ajax({
            url: notificationsUrl,
            type: 'post',
            data: {
                workerName: 'DeleteHarvestRecords|UpdateHarvestData',
                remarks: 'harvest data|revert'
            },
            async:false,
            success: function(data) {
                var pieces = data.split('||');
                var height = pieces[1];
                $('<style>#notifications-dropdown.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');

                data = pieces[0];

                renderNotifications();
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
                $('#notifications-dropdown').html(data);

                $('.bg_notif-btn').on('click', function(e){
                    var lvl = dataLevel == 'plots' ? 'plot' : 'cross';
                    var pageurl = $('#hm-' + lvl + '-hdata-grid li.active a').attr('href');
                    $.pjax.reload(
                        {
                            container:'#dynagrid-harvest-manager-' + lvl + '-hdata-grid-pjax',
                            url: pageurl,
                            replace: false,
                            type: 'POST',
                            data: {
                                forceReset:true
                            }
                        }
                    );
                });
            },
            error: function(xhr, status, error) {
                var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');
                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();
            }
        });
    });
}

function resizeHarvestDataColumnValues() {
    // Adjust committed class heights
    var committedHeights = $("div[class$='-committed']").map(function() {
        return $(this).height();
    }).get();
    var committedMaxHeight = Math.max.apply(null, committedHeights);
    $("div[class$='-committed']").height(committedMaxHeight);

    // Adjust inputted class heights
    var inputtedHeights = $("div[class$='-input']").map(function() {
        return $(this).height();
    }).get();
    var inputtedMaxHeight = Math.max.apply(null, inputtedHeights);
    $("div[class$='-input']").height(inputtedMaxHeight);
}

JS;

$this->registerJs($script);

?>

<style type="text/css">
    div[class$="-committed"] {
        padding-top: 10px;
        padding-bottom: 15px;
    }
</style>