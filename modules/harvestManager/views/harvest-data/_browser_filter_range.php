<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<!-- Plot/Cross Browser Toolbar Filter  -->
<div id="toolbar-filter-container" class="row" style="margin-left:10px;">

    <!-- individual filter set container -->
    <?php
        foreach($filterVarArr as $filterVar){
            $value = isset($filters) &&  isset($filters[$filterFields[strtolower($filterVar)]['api_body_param']]) && 
                        !empty($filters[$filterFields[strtolower($filterVar)]['api_body_param']]) ? 
                            $filters[$filterFields[strtolower($filterVar)]['api_body_param']] : '';

            if($value != ''){
                $value = trim(str_replace('range:','',$value));
            }

            echo '
                <div id='. $filterFields[strtolower($filterVar)]['id'] .'-container'.' class="col-md-12">
                    <div class="col-md-2" style="padding-right: 2px; padding-top: 15px;">
                        <label>Filter by '. $filterFields[strtolower($filterVar)]['label'] .'</label>
                    </div>
                    <div class="col-md-6">'.
                        Html::input('text',
                            $filterFields[strtolower($filterVar)]['id'], 
                            $value ?? '', 
                            [
                                'id' => $filterFields[strtolower($filterVar)]['id'].'-input-fld',
                                'class' => "toolbar-filter",
                                'title' => $filterFields[strtolower($filterVar)]['tooltip'],
                                'placeholder' => 'Input range value (e.g. 1-40 or 80-100)',
                                'data-api_body_param' => $filterFields[strtolower($filterVar)]['api_body_param']
                            ]
                        ).
                    '</div>
                    <div class="col-md-3" style="padding-right: 0px; padding-top: 5px;">'.
                        Html::a(
                            '<span id="" class=""><i class="material-icons">filter_list</i></span>',
                            '',
                            [
                                'id' => $filterFields[strtolower($filterVar)]['id'].'-apply-filter-btn',
                                'class' => 'btn waves-effect waves-light hm-apply-filter-btn',
                                'style' => "",
                                'title' => "Apply filter to ".$dataSource." browser",
                                'data-filter' => $filterFields[strtolower($filterVar)]['id'],
                                'data-position' => 'top',
                                'data-tooltip' => \Yii::t('app','Apply'),
                                'data-abbrev' => $filterVar,
                                'data-filter_type' => $filterFields[strtolower($filterVar)]['filter_type'],
                                'data-input_type' => $filterFields[strtolower($filterVar)]['input_type'],
                                'data-id' => $filterFields[strtolower($filterVar)]['id']

                        ])
                    .'</div>
                </div>
            ';
        }
    ?>
</div>

<?php

$urlPlots = Url::to(['harvest-data/plots', 'program' => $program]);

$script = <<< JS

    var dataSource = '$dataSource'

    $(document).ready(function(){

        $(document).off('click','.hm-apply-filter-btn').on('click','.hm-apply-filter-btn',function(e){
            e.preventDefault();
            
            var obj = $(this);
            var abbrev = obj.data('abbrev');
            var filter = obj.data('filter');
            var filterType =  obj.data('filter_type');
            var inputType = obj.data('input_type');

            var filterId = obj.data('id');
            var inputFldId = filterId + '-input-fld';

            var values = $('#'+inputFldId).val();           
            var bodyParam = $('#'+inputFldId).data('api_body_param');

            var checkResults = validateFilterInputValid(bodyParam,filterType,inputType,values);
            var isValid = checkResults['status'];

            if(!isValid){
                showMessage(checkResults['message'],checkResults['type']);
            }
            else{
                filterResults(bodyParam,checkResults['values']);
            }

        });

        $(document).on('click','.hm-clear-filter-btn',function(e){
            e.preventDefault();

            var obj = $(this);
            var filterId = obj.data('id');
            var inputFldId = filterId + '-input-fld';
            var bodyParam = $('#'+inputFldId).data('api_body_param');

            $('#'+inputFldId).val('');

            filterResults(bodyParam,{[bodyParam]:''});
        });
    });

    // apply toolbar filter values
    function filterResults(bodyParam,values){
        var occurrenceId = '$occurrenceId';
        var urlPlots = '$urlPlots';
        var browserModel = '$browserModel';
        var dataLevel = dataSource == 'plots' ? 'plot' : 'cross'
        var filterStr = ''
        var filterArr

        var pageurl = $('#dynagrid-harvest-manager-'+dataLevel+'-hdata-grid li.active a').attr('href');

        $.pjax.reload(
            {
                container:'#dynagrid-harvest-manager-'+dataLevel+'-hdata-grid-pjax',
                url: pageurl,
                replace: false,
                type: 'POST',
                data: {
                    forceReset:false,
                    bulkOperation:false,
                    toolbarFilters:values 
                }
            }
        );
    }

    // validate toolbar filter input values
    function validateFilterInputValid(identifier,inputType,filterType,values){
        const specialChars = /[`!@#$%^&*()_+\=\[\]{};':"\\|.<>\/?~]/;
        var check = {'status':true, 'message':'', 'type':'success', 'values':{}}

        if(filterType == 'range'){
            // check for special character
            if(specialChars.test(values)){
                return {
                    'status':false, 
                    'message':'Invalid filter input. Range values required.', 
                    'type':'error' ,
                    'values':{}
                }
            }

            if(values.includes(',')){
                return {
                    'status':false, 
                    'message':'Invalid filter input. Multiple values not yet supported.', 
                    'type':'warning', 
                    'values':{}
                }
            }
            
            // check if proper range format
            var rangeValArr = values.includes('-') ? values.split('-') : [];

            if(rangeValArr.length < 2 || rangeValArr.length > 2){
                return {
                    'status':false, 
                    'message':'Invalid filter input. Valid range required.', 
                    'type':'error', 
                    'values':{}
                }
            }
            else if(!Number.isInteger(parseInt(rangeValArr[0])) || 
                    !Number.isInteger(parseInt(rangeValArr[1]))){
                return {
                    'status':false, 
                    'message':'Invalid filter input. Valid range required.', 
                    'type':'error', 
                    'values':{}
                }
            }
            else if((Number.isInteger(parseInt(rangeValArr[0])) && Number.isInteger(parseInt(rangeValArr[1])))
                        && (parseInt(rangeValArr[0]) > parseInt(rangeValArr[1]))){
                    return {
                        'status':false, 
                        'message':'Invalid filter input. Improper range format.', 
                        'type':'error', 
                        'values':{}
                    }
                }
            else{
                check['values'] = {
                    [identifier] : "range: "+rangeValArr.join('-')
                }
            }
        }

        return check;
    }

    // Renders Toast message
    function showMessage(message,type){
        var output = '';
        var icon = '';

        if(type == 'success'){
            icon = "<i class='material-icons green-text'>check</i>&nbsp; ";
        }
        else if(type == 'error'){
            icon = "<i class='material-icons red-text'>clear</i>&nbsp; ";
        }
        else if(type == 'warning'){
            icon = "<i class='material-icons yellow-text'>warning</i>&nbsp; ";
        }
        else if(type == 'message'){
            icon = "<i class='material-icons white-text'>info</i>&nbsp; ";
        }
        output = '<span class="left-align" style="text-align:left;">' +
                icon.concat(message) + '</span>';

        $('.toast').css('display','none');
        Materialize.toast(output, 5000);
    }

JS;

$this->registerJs($script);

Yii::$app->view->registerCss('');
?>