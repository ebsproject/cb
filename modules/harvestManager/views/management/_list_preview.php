<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Preview list of seeds/packages/germplasm
**/

use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'designation',
        'label' => \Yii::t('app','Germplasm'),
        'enableSorting' => false
    ],
    [
        'attribute' => 'seedName',
        'enableSorting' => false
    ],
    [
    	'attribute'=>'label',
    	'enableSorting' => false
    ],
    [
        'attribute' => 'quantity',
        'label' => \Yii::t('app','Package Qty.'),
    	'enableSorting' => false,
    ]
];

$dynaGrid = DynaGrid::begin([
	'columns' => $columns,
	'theme' => 'panel-default',
	'showPersonalize' => true,
	'storage' => DynaGrid::TYPE_SESSION,
	'showFilter' => false,
	'showSort' => false,
	'allowFilterSetting' => false,
	'allowSortSetting' => false,
	'gridOptions' => [
		'id' => 'hm-save-list-preview-grid',
		'dataProvider' => $dataProvider,
		'filterModel'=>false,
		'showPageSummary'=>false,
		'hover' => true,
		'pjax'=>true,
		'pjaxSettings'=>[
			'neverTimeout'=>true,
			'options'=>['id'=>'hm-save-list-preview-grid'],
			'beforeGrid'=>'',
			'afterGrid'=>''
		],
		'responsiveWrap'=>false,
		'panel'=>[
			'heading'=>false,
			'before' => '<p style = "margin-bottom:-20px">'.\Yii::t('app','Number of items to be saved in the list: ').'<b id="total-count-val">'.number_format($count).'</b></p>',
			'beforeOptions' => [
				'style' => 'height:50px'
			],
			'after' => false,
			'footer'=>false
		],
		'toolbar' =>  '',
	],
	'options' => [
        'id' => 'hm-save-list-preview-grid',
        'class' => 'table table-bordered white z-depth-3'
        ]
]);
DynaGrid::end();
?>