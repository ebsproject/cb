<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use kartik\date\DatePicker;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
// Import components
use app\components\B4RExportMenu;
use app\components\PrintoutsWidget;
use app\widgets\GermplasmInfo;

/**
 * Renders seed and package management step for Harvest Manager
 */

// Define URLs
$urlRetrieveListPreview = Url::to(['management/retrieve-list-preview']);
$urlCheckSeedEntries = Url::to(['management/check-seed-entries']);
$urlGetPackageCount = Url::to(['management/get-package-count']);
$urlCheckGermplasm = Url::to(['management/check-germplasm']);
$urlDeleteViaProcessor = Url::to(['management/delete-via-processor']);
$urlDeleteInBackground = Url::to(['management/delete-in-background']);
$urlGetBackgroundJobs = Url::to(['harvest-data/get-background-jobs']);
$urlGetPackageSeedIds = Url::to(['management/get-package-seed-ids']);
$urlSaveList = Url::to(['management/save-list']);
$newNotificationsUrl = Url::toRoute(['/harvestManager/harvest-data/new-notifications-count']);
$notificationsUrl = Url::toRoute(['/harvestManager/harvest-data/push-notifications']);
$urlManagementIndex = Url::to(['management/index', 'program' => $program, 'occurrenceId' => $occurrenceId, 'source' => $dataLevel]);
$exportPackageDataUrl = Url::to(['management/export-packages','occurrenceId' => $occurrenceId, 'source' => $dataLevel]);
$getBgProcessInfoUrl = Url::to(['harvest-data/get-background-process-info']);
$urlRenderBulkDeleteAck = Url::to(['management/render-bulk-delete-acknowledge-modal']);
$codingGermplasmUrl = Url::toRoute(['/germplasm/coding/create-transaction', 'program' => $program, 'sourceToolAbbrev' => 'HM']);

$browserId = 'dynagrid-harvest-manager-package-mgmt-grid';

// Render harvest manager tabs
echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/tab_main.php',[
    'program' => $program,
    'occurrenceId' => $occurrenceId,
    'dataLevel' => $dataLevel,
    'experimentType' => $experimentType,
    'occurrenceName' => $occurrenceName,
    'cpnCheck' => $cpnCheck,
    'browserId' => $browserId,
]);
echo '<div id="hm-bg-alert-div"></div>';
echo '<div id="hm-mgmt-package" class = "row col col-sm-12 col-md-12 col-lg-12" style = "margin-bottom:0px; padding:0px">';

// Add browser header
echo '<div class="hm-browser-title">' .
        '<h3 class="hm-browser-title">' . \Yii::t('app', 'Packages') . '</h3>' .
    '</div>';

$exportColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'attribute' => 'packageDbId',
        'label' => 'Package ID'
    ],
    [
        'attribute' => 'packageCode',
    ],
    [
        'attribute' => 'label',
        'label' => 'Package Label'
    ],
    [
        'attribute' => 'designation',
        'label' => 'Germplasm Name'
    ],
    [
        'attribute' => 'germplasmCode',
        'label' => 'Germplasm Code'
    ],
    [
        'attribute' => 'parentage',
        'label' => 'Parentage'
    ],
    [
        'attribute' => 'seedName',
        'label' => 'Seed Name'
    ],
    [
        'attribute' => 'seedCode',
        'label' => 'Seed Code'
    ],
    [
        'attribute' => 'sourceEntryCode',
        'label' => 'Source Entry Code'
    ],
    [
        'attribute' => 'femalePlotCode',
        'label' => 'Female Plot Code'
    ],
    [
        'attribute' => 'femalePlotNumber',
        'label' => 'Female Plot No.'
    ],
    [
        'attribute' => 'seedSourcePlotCode',
        'label' => 'Source Plot Code'
    ],
    [
        'attribute' => 'seedSourcePlotNumber',
        'label' => 'Source Plot No.'
    ],
    [
        'attribute' => 'replication',
        'label' => 'Rep'
    ],
    [
        'attribute' => 'harvestDate',
        'label' => 'Harvest Date'
    ],
    [
        'attribute' => 'harvestMethod',
        'label' => 'Harvest Method'
    ],
    [
        'attribute' => 'quantity',
        'label' => 'Package Quantity',
        'value' => function ($model) {
            $value = $model['quantity'] . " " . $model['unit'];
            return $value;
        }
    ],
    [
        'attribute' => 'crossFemaleParent',
        'label' => 'Female Parent',
    ],
    [
        'attribute' => 'femaleParentage',
        'label' => 'Female Parentage',
    ],
    [
        'attribute' => 'crossMaleParent',
        'label' => 'Male Parent',
    ],
    [
        'attribute' => 'maleParentage',
        'label' => 'Male Parentage',
    ],
];

$exportThreshConfig = Yii::$app->config->getAppThreshold('HARVEST_MANAGER_BG_PROCESSING_THRESHOLD', 'exportPackages');
$exportThreshold = isset($exportThreshConfig) ? intval($exportThreshConfig) : 2000;

// Build file name. Format: <occurrence_name>_Packages_<timestamp>
$timeNow = date("Ymd_His", time());
$fileName = $occurrenceName . "_Packages_$timeNow";

$export = B4RExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $exportColumns,
    'filename' => $fileName,
    'fontAwesome' => true,
    'exportConfig' => [
        ExportMenu::FORMAT_PDF => false
    ],
    'dropdownOptions' => [
        'id' => 'plot-browser-export-btn'
    ],
    'showColumnSelector' => false,
    'asDropdown' => true,
    'showConfirmAlert' => false,
    'clearBuffers' => true,
    'retrieveAll' => true,
    'apiEndpoint' => "occurrences/$occurrenceId/packages-search",
    'apiFilters' => \Yii::$app->cbSession->get($browserId . "-filters"),
    'apiRequestBody' => \Yii::$app->cbSession->get($browserId . "-body")
]);

// Check access to breeding tools
$hasAccessToBreedingTools = true;
$hasAccessToCoding = $codingModel ? $codingModel->hasCodingAccess() : true;

// Access breeding tools button for the browser toolbar
$accessBreedingTools = "";

if ($hasAccessToBreedingTools && $hasAccessToCoding) {
    $accessBreedingTools = "
        <a
            title='Access Breeding Tools'
            id='hm-access-breeding-tools-btn'
            class='dropdown-trigger btn'
            data-activates='hm-access-breeding-tools-list'
            data-beloworigin='true'
            data-constrainwidth='false'
        >
            <span class='material-icons' style='
                        font-size: 16px;
                        vertical-align: -4px;
                        margin-right: 4px;
                    '>
                    handyman
                </span>
            <span class='caret'></span>
        </a>
        <ul id='hm-access-breeding-tools-list' class='dropdown-content' style='
                            max-width: 130px;
                            margin-left: -124px;
                        '>

            <!-- Germplasm Coding -->
            <li
                id='ge-germplasm-coding-btn'>" .
        Html::a(
            "Code Germplasm",
            null,
            [
                'class' => 'dropdown-button dropdown-trigger dropdown-button-pjax',
                'id' => 'ge-germplasm-coding-btn',
                'data-alignment' => 'right',
            ]
        ) . "
            </li>
        </ul>";
}

// Define action columns
$actionColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ],
    [
        'label' => "Checkbox",
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'visible' => true,
        'mergeHeader' => true,
        'vAlign' => 'top',
        'header' => '<input type="checkbox" class="filled-in" id="hm-browser-select-all-checkbox" table-con="' . $browserId . '-table-con"/><label style="padding-left: 20px;" for="hm-browser-select-all-checkbox"></label>',
        'content' => function ($model) use ($browserId) {
            $checkboxId = $model['packageDbId'].'-'.$model['germplasmDbId'].'-'.$model['seedDbId'];
            
            return '
            <input class="hm-browser-single-checkbox filled-in"' .
                'id="' . $checkboxId . '"' .
                'type="checkbox"' .
                'table-con="' . $browserId . '-table-con"/>' .
            '<label style="padding-left: 20px;" for="' . $checkboxId . '"></label>';
        },
        'width' => '1%',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT
    ],
];

// Define browser columns
$columns = [
    [
        'attribute' => 'harvestSource',
        'label' => \Yii::t('app','Source'),
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>[
            'plot' => 'plot',
            'cross' => 'cross',
        ], 
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true,'id'=>"select-source-data-id"],],
        'filterInputOptions'=>['placeholder'=>'Source','id'=>"select-source-manage-seedlot-grid"],
        'headerOptions'=>[
            'style'=>'min-width:80px !important',
        ],
    ],
    [
        'attribute' => 'femalePlotNumber',
        'label' => \Yii::t('app','Plot No.'),
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'headerOptions'=>[
            'style'=>'min-width:90px !important',
        ],
        'value' => function($model) {
            if ($model['harvestSource'] == 'cross') {
                return $model['femalePlotNumber'];
            }
            else return $model['seedSourcePlotNumber'];
        }
    ],
    [
        'attribute' => 'femalePlotCode',
        'label' => 'Plot Code',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'headerOptions'=>[
            'style'=>'min-width:90px !important',
        ],
        'filterInputOptions' => ['autocomplete' => 'off'],
        'visible' => true,
        'value' => function($model) {
            if ($model['harvestSource'] == 'cross') {
                return $model['femalePlotCode'];
            }
            else return $model['seedSourcePlotCode'];
        }
    ],
    [
        'attribute' => 'designation',
        'label' => \Yii::t('app','Germplasm Name'),
        'hAlign' => 'left',
        'vAlign' => 'top',
        'format' => 'raw',
        'value' => function($model) {
            $germplasmDbId = $model['germplasmDbId'];
            $designation = $model['designation'];
            
            $linkAttrs = [
                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                'class' => 'blue-text text-darken-4 header-highlight',
                'title' => \Yii::t('app', 'Click to view more information'),
                'data-label' => $designation,
                'data-id' => $germplasmDbId,
                'data-target' => '#view-germplasm-widget-modal',
                'data-toggle' => 'modal'
            ];

            $widgetName = GermplasmInfo::widget([
                'id' => $germplasmDbId,
                'entityLabel' => $designation,
                'linkAttrs' => $linkAttrs
            ]);
            return $widgetName;
        },
        'filterInputOptions' => ['autocomplete' => 'off'],
        'contentOptions' => [
            'class' => 'germplasm-name-col'
        ],
    ],
    [
        'attribute' => 'germplasmCode',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => true,
        'headerOptions'=>[
            'style'=>'min-width:130px !important',
        ],
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'seedName',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'headerOptions'=>[
            'style'=>'min-width:150px !important',
        ],
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'seedCode',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'headerOptions'=>[
            'style'=>'min-width:100px !important',
        ],
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'label',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'format' => 'raw',
        'value' => function ($model) {
            return !empty($model['label']) && $model['label'] !== null ? '<div style="overflow-wrap: break-word;max-width:150px">'.$model['label'].'</div>' : '<span class="not-set">(not set)</span>';
        },
        'headerOptions'=>[
            'style'=>'min-width:130px !important',
        ],
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'packageCode',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'quantity',
        'label' => \Yii::t('app','Package Qty.'),
        'hAlign' => 'left',
        'vAlign' => 'top',
        'value' => function ($model) {
            return $model['quantity'] . " " . $model['unit'];
        },
        'filterInputOptions' => [
            'autocomplete' => 'off',
            'type' => 'text',
            'placeholder' => 'Hover for details...',
            'title' => 'Quantity format: <quantity><unit>
                        ex. 100g, 1.2Kg, 0.2kg, g',
            'onkeyup' => "
                var defaultTitle = 'Quantity format: <quantity><unit> ex. 100g, 1.2Kg, 0.2kg, g'
                var code = event.keyCode;
                var regexExp = /^(\d+(\.\d+)?)*\s*(g|kg|pan|seeds|vial)?$/i;
                var key = String.fromCharCode(event.which || event.charCode);
                var value = $(this).val()

                if(code==13) {
                    if(regexExp.test(value) === false) {
                        var notif = \"<i class='material-icons red-text'>close</i>&nbsp;Invalid format for quantity\";
                        Materialize.toast(notif, 7500);
                        return
                    }
                }

                if(regexExp.test(value) === false && value != '') {
                    $(this).attr('title','Invalid quantity format')
                    $(this).css({'background-color': '#ffd9da'});
                }
                else {
                    $(this).attr('title',defaultTitle)
                    $(this).css({'background-color': ''});
                }
            ",
        ],
        'headerOptions'=>[
            'style'=>'min-width:110px !important',
        ],
    ],
    [
        'attribute' => 'parentage',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'format' => 'raw',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'headerOptions'=>[
            'style'=>'min-width:200px !important',
        ],
        'value' => function ($model) {
            return !empty($model['parentage']) && $model['parentage'] !== null ? '<div style="overflow-wrap: break-word;max-width:200px">'.$model['parentage'].'</div>' : '<span class="not-set">(not set)</span>';
        },
        'contentOptions' => [
            'class' => 'germplasm-name-col'
        ],
    ],
    [
        'attribute' => 'generation',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'format' => 'raw',
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'germplasmState',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => true,
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $germplasmStateScaleValues,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true,'class' => 'col-lg-2'],
        ],
        'filterInputOptions' => ['placeholder' => 'Select state'],
    ],
    [
        'attribute' => 'harvestDate',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'headerOptions'=>[
            'style'=>'min-width:110px !important',
        ],
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [

            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'startDate' => date("1970-01-01"),
                'orientation' => 'bottom right',
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
    ],
    [
        'attribute' => 'harvestMethod',
        'label' => 'Harvest Method',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'headerOptions'=>[
            'style'=>'min-width:130px !important',
        ],
        'filterInputOptions' => ['autocomplete' => 'off'],
    ],
    [
        'attribute' => 'sourceEntryCode',
        'label' => 'Entry Code',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'headerOptions'=>[
            'style'=>'min-width:100px !important',
        ],
        'filterInputOptions' => ['autocomplete' => 'off'],
        'visible' => false,
    ],
    [
        'attribute' => 'replication',
        'label' => 'Rep',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'filterInputOptions' => ['autocomplete' => 'off'],
        'visible' => false,
    ],
    [
        'attribute' => 'crossFemaleParent',
        'label' => '<span title="Female Parent"><i class="fa fa-venus"></i>'.Yii::t('app', ' Female Parent').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>[
            'class'=>'red lighten-4',
            'style'=>'min-width:130px !important',
        ],
        'contentOptions'=>[
            'class'=>'germplasm-name-col red lighten-5'
        ],
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model) {
            $germplasmDbId = $model['femaleGermplasmDbId'];
            $designation = $model['crossFemaleParent'];

            if(empty($germplasmDbId)) return '<span class="not-set">(not set)</span>';
            
            $linkAttrs = [
                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                'class' => 'blue-text text-darken-4 header-highlight',
                'title' => \Yii::t('app', 'Click to view more information'),
                'data-label' => $designation,
                'data-id' => $germplasmDbId,
                'data-target' => '#view-germplasm-widget-modal',
                'data-toggle' => 'modal'
            ];

            $widgetName = GermplasmInfo::widget([
                'id' => $germplasmDbId,
                'entityLabel' => $designation,
                'linkAttrs' => $linkAttrs
            ]);
            return $widgetName;
        }
    ],
    [
        'attribute' => 'femaleParentage',
        'label' => '<span title="Female Parentage"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Parentage').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>[
            'class'=>'germplasm-name-col red lighten-5'
        ],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['femaleParentage']) ? $model['femaleParentage'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossMaleParent',
        'label' => '<span title="Male Seed Source"><i class="fa fa-mars"></i>'.Yii::t('app', ' Male Parent').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>[
            'class'=>'blue lighten-4',
            'style'=>'min-width:120px !important',
        ],
        'contentOptions'=>[
            'class'=>'germplasm-name-col blue lighten-5'
        ],
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model) {
            $germplasmDbId = $model['maleGermplasmDbId'];
            $designation = $model['crossMaleParent'];

            if(empty($germplasmDbId)) return '<span class="not-set">(not set)</span>';
            
            $linkAttrs = [
                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                'class' => 'blue-text text-darken-4 header-highlight',
                'title' => \Yii::t('app', 'Click to view more information'),
                'data-label' => $designation,
                'data-id' => $germplasmDbId,
                'data-target' => '#view-germplasm-widget-modal',
                'data-toggle' => 'modal'
            ];

            $widgetName = GermplasmInfo::widget([
                'id' => $germplasmDbId,
                'entityLabel' => $designation,
                'linkAttrs' => $linkAttrs
            ]);
            return $widgetName;
        }
    ],
    [
        'attribute' => 'maleParentage',
        'label' => '<span title="Male Parentage"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Parentage').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>[
            'class'=>'germplasm-name-col blue lighten-5'
        ],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible' => false,
        'filterInputOptions' => ['autocomplete' => 'off'],
        'value' => function($model){
            return !empty($model['maleParentage']) ? $model['maleParentage'] : '<span class="not-set">(not set)</span>';
        }
    ],
];

// Merge columns
$gridColumns = array_merge($actionColumns,$columns);

// Initialize printouts component
$printouts = PrintoutsWidget::widget([
    "product" => "Harvest Manager",
    "program" => $program,
    "occurrenceIds" => [ $occurrenceId ]
]);

// Render grid
$dynagrid = DynaGrid::begin([
    'columns'=>$gridColumns,
    'storage' => DynaGrid::TYPE_SESSION,
    'theme'=>'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'tableOptions'=>['class'=>$browserId.'-table'],
        'options'=>['id'=>$browserId.'-table-con'],
        'striped'=>false,
        'hover' => true,
        'floatHeader' => true,
        'responsive' => true,
        'floatOverflowContainer' => true,
        'showPageSummary' => false,
        'summaryOptions' => ['class' => 'pull-right'],
        'pjax' => true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => $browserId, 'enablePushState' => false,],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'id' => $browserId.'-table-con',
        'responsiveWrap' => false,
        'floatHeaderOptions' => [
			'position' => 'absolute',
            'top' => 63
		],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', "Manage your packages here.") . ' {summary}' .
                '<br/>
                <p id="total-selected-text" class = "pull-right" style = "margin: -1px;"></p><br>',
            'after' => false,
        ],
        'toolbar' =>  [
            [
                'content' =>   
                    Html::a(
                    '<i class="material-icons">delete_forever</i>', 
                    '', 
                    [
                        'class' => 'btn btn-default red darken-3 hm-tooltipped', 
                        'id' => 'hm-bulk-delete-packages-btn',
                        'style' => "margin-left:10px;",
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Delete Packages'),
                        'data-pjax' => true,
                        'disabled'=>false,
                        'data-toggle' => 'modal',
                        'data-target' => '#hm-bulk-delete-packages-modal'
                    ]).
                    Html::a('<i class="material-icons widget-icons">add_shopping_cart</i>', 
                    '#!', 
                    [
                        'class' => 'btn dropdown-trigger dropdown-button-pjax hm-tooltipped',
                        'id' => 'hm-save-new-list-btn',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Save to list'),
                        'data-activates' => 'hm-save-list-dropdown-actions',
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                    ]).
                    Html::a(
                        '<i class="material-icons large">notifications_none</i>
                        <span id="notif-badge-id" class=""></span>',
                        '#',
                        [
                            'id' => 'notif-btn-id',
                            'class' => 'btn waves-effect waves-light harvest-manager-notification-button-manage-tab hm-tooltipped',
                            'data-pjax' => 0,
                            'style' => "overflow: visible !important;",
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Notifications'),
                            'data-activates' => 'notifications-dropdown'
                        ]
                    )
            ],
            [
                'content' =>
                    $printouts.
                    $accessBreedingTools.
                    "<ul id='hm-save-list-dropdown-actions' class='dropdown-content' style='width: 160px !important'>".
                        "<li><a href='#!' id='selected-items'>".\Yii::t('app','Selected')."</a></li>".
                        "<li><a href='#!' id='all-items'>".\Yii::t('app','Select All')."</a></li>".
                    "</ul>".
                    // Data browser export
                    Html::a(
                    '<i class="material-icons right" style="margin-left:0px;">download</i>', 
                    null, 
                    [
                        'data-pjax' => true,
                        'class' => 'btn btn-default',
                        'style' => 'margin-right:5px;',
                        'id' => 'export-records-btn', 
                        'title' => \Yii::t('app','Export all data in browser'),
                    ]).
                    Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    [Yii::$app->controller->action->id, 'dataLevel' => $dataLevel],
                    [
                        'class' => 'btn btn-default hm-tooltipped',
                        'id' => 'hm-mgmt-reset-package-btn',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Reset grid'),
                        'data-pjax' => true
                    ]
                    ).
                    '{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
        ],
    ],
    'options'=>['id'=>$browserId]
]);

DynaGrid::end();

echo '</div>';

// Modals

// Saving a new list modal
Modal::begin([
    'id' => 'hm-save-list-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">add</i>'.\Yii::t('app','Save as new list').'</h4>',
    'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-hm-save-list', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
        .Html::a('Submit',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light disabled',
                'url' => '#',
                'id' => 'hm-save-list-confirm-btn'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);

echo '<div class="clearfix"></div><div class="hm-modal-loading-ids"></div>';
echo '<p id="hm-selection-mode" class="hidden">selected</p>';

echo '<div id="hm-save-list-modal-body">';
$form = ActiveForm::begin([
    'enableClientValidation'=>false,
    'id'=>'hm-create-saved-list-form',
    'fieldConfig' => function($model,$attribute){
        if(in_array($attribute, ['abbrev','display_name','name','type'])){
            return ['options' => ['class' => 'input-field form-group col-md-6']];
        }else{
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    }
]);

$model = \Yii::$container->get('app\models\PlatformList');
$listType = [
    'seed' => 'Seed',
    'package' => 'Package',
    'germplasm' => 'Germplasm',
];
$listTypeOptions = [
    'placeholder' => Yii::t('app','Select type (required)'),
    'id' => 'hm-type-fld',
    'class' => 'text-field-var',
    'required' => 'required',
    'data-name' => 'Type',
    'allowClear' => true,
];

echo '<div class="clearfix"></div><div class="hm-modal-loading-save"></div>';

echo $form->field($model, 'name')->textInput([
    'maxlength' => true,
    'id'=>'hm-list-name',
    'title'=> Yii::t('app','Name identifier of the list'),
    'autofocus' => 'autofocus',
    'onkeyup' => 'js: $("#hm-list-display-name").val(this.value.charAt(0).toUpperCase() + this.value.slice(1)); $(".field-hm-list-display-name > label").addClass("active");'
]).
$form->field($model, 'abbrev')->textInput([
    'maxlength' => true,
    'id'=>'hm-list-abbrev',
    'title'=> Yii::t('app','Short name identifier or abbreviation of the list'),
    'oninput' => 'js: $("#hm-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());'
]).
$form->field($model, 'display_name')->textInput([
    'maxlength' => true,
    'title'=> Yii::t('app','Name of the list to show to users'),
    'id'=>'hm-list-display_name',
]).
$form->field($model, 'type')->widget(Select2::class, [
    'data' => $listType,
    'options' => $listTypeOptions,
    'hideSearch' => true,
    ])->label(Yii::t('app','Type'),['class'=>'control-label', 'style' => 'top: -95%;']).
$form->field($model, 'description')->textarea([ 'class'=>'materialize-textarea', 'title'=>Yii::t('app','Description about the list'), 'id'=>'hm-list-description']).
$form->field($model, 'remarks')->textarea(['class'=>'materialize-textarea', 'title'=>Yii::t('app','Additional details about the list'),'row'=>2, 'id'=>'hm-list-remarks']);

ActiveForm::end();

echo '<div class="clearfix"></div><div class="hm-modal-loading"></div>';
echo '<div class="hm-save-list-preview-entries"></div>';
echo '</div>';

Modal::end();

// BULK DELETE MODAL

// Bulk delete package modal
Modal::begin([
    'id' => 'hm-bulk-delete-packages-modal',
    'header' => '<h4><i class="material-icons">warning</i> &nbsp Package Deletion</h4>',
    'footer' =>
        Html::a(\Yii::t('app','Cancel'), ['#'],
            [
                'class'=>'hm-mgmt-package-delete-modal-cancel hm-mgmt-package-delete-modal-btn',
                'id'=>'hm-mgmt-cancel-package-delete-modal',
                'data-dismiss'=>'modal'
            ]
        ). '&emsp;'
        . Html::a(\Yii::t('app', 'Delete Selected'), '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light hm-mgmt-delete-package-bulk-btn hm-mgmt-package-delete-modal-btn',
                'id' => 'hm-mgmt-package-delete-selected-btn'
            ]
        ). '&emsp;'
        . Html::a(\Yii::t('app', 'Delete All'), '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light hm-mgmt-delete-package-bulk-btn hm-mgmt-package-delete-modal-btn',
                'id' => 'hm-mgmt-package-delete-all-btn'
            ]
        ) .
        Html::a(\Yii::t('app', 'Close'), '#',
            [
                'data-dismiss' => 'modal',
                'class' => 'btn btn-primary waves-effect waves-light modal-close hm-mgmt-package-delete-modal-btn',
                'id' => 'hm-mgmt-close-package-delete-modal'
            ]
        ) . '&emsp;',
        'options' => [
            'data-backdrop' => 'static', 
            'style' => 'z-index: 1040;'
        ]
]);

echo '<div class="hm-mgmt-bd-pkg-count-progress" id="hm-mgmt-bd-pkg-count-progress"><div class="progress"><div class="indeterminate"></div></div></div>';
echo '<div class="hm-mgmt-bd-modal-body" id="hm-mgmt-bd-modal-body"></div>';
echo '<div class="hm-mgmt-bd-notice" id="package-delete-selected-count-div"></div>';
echo '<div class="hm-mgmt-bd-notice" id="package-delete-content-div"><p style="font-size:115%;">This operation will delete package, seed, and germplasm records.</p>';
echo '<br><p class="hm-mgmt-bd-notice" style="font-size:115%;">Are you sure you want to delete?</p></div>';
echo '<div class="hm-mgmt-package-delete-progress" id="hm-mgmt-package-delete-progress" hidden>'
. '<p class="hm-mgmt-bd-progress-items" style="font-size:115%;"><i>Deleting...</i></p><div class="progress hm-mgmt-bd-progress-items"><div class="indeterminate hm-mgmt-bd-progress-items"></div></div>'
. '<textarea id="delete-progress-textarea" name="delete-progress-display" style="background-color:#ffffff;min-height:150px;" rows="4" cols="50" readonly></textarea>'
.'</div>';
echo '<div class="red lighten-4 red-text text-darken-2" id="hm-mgmt-bulk-delete-red-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; border-radius: 5px"></div>';
Modal::end(); 

// Modal for confirming bulk delete
Modal::begin([
    'id' => 'hm-mgmt-bulk-delete-confirmation-modal',
    'header' => '<h4>Confirm Bulk Delete</h4>',
    'footer' =>
    Html::a(\Yii::t('app', 'Acknowledge'), '#',
        [
            'data-dismiss'=> 'modal',
            'data-toggle' => "modal",
            'data-target' => "#hm-bulk-delete-packages-modal",
            'class' => 'btn btn-primary waves-effect waves-light modal-close hm-mgmt-bd-confirmation-acknowledge-grp disabled',
            'id' => 'hm-mgmt-bd-confirmation-acknowledge-btn'
        ]
    ) . '&emsp;</span>',
    'size' =>'modal-lg',
    'options' => [
        'data-backdrop' => 'static', 
        'style' => 'z-index: 2000; padding: 10px 20px 10px 30px'
    ]
]);
echo '<div id="hm-mgmt-bulk-delete-confirmation-modal-body"></div><div id="hm-mgmt-bulk-delete-confirmation-modal-additional-main"><div class="red lighten-4 red-text text-darken-2" id="hm-mgmt-bulk-delete-confirmation-modal-additional-content" style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; border-radius: 5px"></div></div>';
Modal::end();


?>
<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<?php
$script = <<< JS
// Set variables
var occurrenceId = $occurrenceId;
var packagesDeleteThreshold = $packagesDeleteThreshold;
var gridId = '$browserId-table-con';
var selectionStorageId = occurrenceId + "_" + gridId;
var packageIds = [];
var seedIds = [];
var germplasmIds = [];
var newNotificationsUrl='$newNotificationsUrl';
var notificationsUrl='$notificationsUrl';
var urlManagementIndex = '$urlManagementIndex';
var processStart = 0;
var recordsObj = null;

var recordsThreshold = parseInt('$exportThreshold');
var germplasmCodingThreshold = parseInt('$germplasmCodingThreshold');

notifDropdown();
renderNotifications();
updateNotif();

// Check for new notifications every 7.5 seconds
notificationInterval=setInterval(updateNotif, 7500);

/**
 * Displays the notification in dropdown UI
 */
function notifDropdown(){
    $('#notif-btn-id').on('click',function(e){
        $.ajax({
            url: notificationsUrl,
            type: 'post',
            data: {
                workerName: 'CreateHarvestList|DeleteHarvestRecords|BuildCsvData',
                remarks: 'package'
            },
            async:false,
            success: function(data) {
                var pieces = data.split('||');
                var height = pieces[1];
                $('<style>#notifications-dropdown.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');

                data = pieces[0];

                renderNotifications();
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
                $('#notifications-dropdown').html(data);

                $('.bg_notif-btn').on('click', function(e){
                    window.location = urlManagementIndex;

                    let obj = $(this)
                    let worker = obj.data('worker-name')

                    // If notif is for BuildCsvWorker, retrieve file
                    if (worker === "BuildCsvData") {
                        let id = obj.data('process-id')

                        $.ajax({
                            type: 'POST',
                            url: "$getBgProcessInfoUrl",
                            data: {
                                bgProcessId: id
                            },
                            async: true,
                            dataType: 'json',
                            success: function(response) {
                                if(response.filename != ''){
                                    var filename = response.filename

                                    // Create temporary entry point to downloading CSV data
                                    const a = document.createElement('a')

                                    a.setAttribute('hidden', '')
                                    a.setAttribute('href', `/index.php/harvestManager/harvest-data/download-data?filename=\${filename}`)

                                    // Temporarily insert element to document
                                    document.body.appendChild(a)

                                    // Trigger download
                                    a.click()

                                    // Remove element
                                    document.body.removeChild(a)

                                    message = `<i class="material-icons blue-text" style="margin-right: 8px;">info</i> 
                                                Please wait for the download to finish.`
                                }
                                else{
                                    message = `<i class="material-icons blue-text" style="margin-right: 8px;">info</i> 
                                                Error encountered retrieving background process info.`
                                }

                                $('.toast').css('display','none')
                                Materialize.toast(message, 5000)

                                // remove loading indicator after 5 seconds
                                setTimeout( function(){
                                    $('.progress').css('display','none');
                                    $('.indeterminate').css('display','none');
                                }, 5000);
                            },
                            error: function(){
                                
                            }
                        });
                    }
                });
            },
            error: function(xhr, status, error) {
                var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');
                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();
            }
        });
    });
}

/**
 * Shows new notifications
 */
function updateNotif() {
    $.getJSON(newNotificationsUrl,
    {
        workerName: 'CreateHarvestList|DeleteHarvestRecords|BuildCsvData',
        remarks: 'package'
    },
    function(json){
        $('#notif-badge-id').html(json);
        if(parseInt(json)>0){
            $('#notif-badge-id').addClass('notification-badge red accent-2');
        }else{
            $('#notif-badge-id').html('');
            $('#notif-badge-id').removeClass('notification-badge red accent-2');
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Message: "+textStatus+" : "+errorThrown);

    });
}

/**
 * Initialize dropdown materialize  for notifications
 */
function renderNotifications(){
    $('#notif-btn-id').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: false,
        gutter: 0,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });
}

/**
 * Resize the package grid table upon loading
 */
function resize(){
    var maxHeight = ($(window).height() - 355);
    $(".kv-grid-wrapper").css("height",maxHeight);
}

$(document).on('ready pjax:success', function(e) {
    e.preventDefault();
    $('#notif-btn-id').tooltip();
    notifDropdown();
    $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>');
    renderNotifications();
    updateNotif();
});

// Restore checkbox selection
checkboxActionsHM(selectionStorageId, gridId);

// render button dropdown
$('.dropdown-trigger').dropdown();
resize();

$("document").ready(function(){
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();

    // Notifs button click event
    $(document).on("click","#notif-btn-id", function(e) {
        e.preventDefault();
    });

    // Remove export button title
    $("#plot-browser-export-btn").removeAttr("title");

    // Acknowledge entries
    $(document).on("click","#hm-mgmt-bd-confirmation-acknowledge-btn", function(e) {
        e.preventDefault();

        var seedIdsToDelete = recordsObj.seedIdsToDelete
        var germplasmIdsToDelete = recordsObj.germplasmIdsToDelete
        var germplasmSeedRelation = recordsObj.germplasmSeedRelation
        
        checkGermplasm(seedIdsToDelete, germplasmIdsToDelete, germplasmSeedRelation);
    });

    // Reset browser button click event
    $(document).on("click","#hm-mgmt-reset-package-btn", function(e) {
        e.preventDefault();
        // Clear selection on grid reset
        clearSelectionHM(selectionStorageId);
        var pageurl = $('#$browserId li.active a').attr('href');
        $.pjax.reload(
            {
                container:'#$browserId-pjax',
                url: pageurl,
                replace: false,
                type: 'POST',
                data: {
                    forceReset:true
                }
            }
        );
    });

    // Perform after pjax success of browser
    $(document).on('ready pjax:success','#$browserId-pjax', function(e) {
        $('.dropdown-trigger').dropdown();
        resize();
    });

    // Perform after pjax completion of browser
    $(document).on('ready pjax:complete','#$browserId-pjax', function(e) {
        // Initialize tooltip
        initializeTooltips();
        // Restore checkbox selection
        checkboxActionsHM(selectionStorageId, gridId);
        // Set alignment of table headers to 'top'
        thAlignTop();
        $('.dropdown-trigger').dropdown();
    });

    // Bulk delete button event
    $('body').on('click', '#hm-bulk-delete-packages-btn', function(e){
        e.preventDefault();
        $('.hm-mgmt-package-delete-progress').hide();
        $('#hm-mgmt-bulk-delete-red-content').html('');
        $('#hm-mgmt-bd-modal-body').html('');
        $('#hm-mgmt-bulk-delete-red-content').hide();
        $('.hm-mgmt-delete-package-bulk-btn').show();
        $('#hm-mgmt-close-package-delete-modal').hide();
        $('#hm-mgmt-cancel-package-delete-modal').show();
        $('.hm-mgmt-bd-progress-items').show();
        $('.hm-mgmt-delete-package-bulk-btn').addClass('disabled');
        $('#hm-mgmt-bd-pkg-count-progress').show();
        $('.hm-mgmt-bd-notice').hide();
        clearTextArea();

        // Retrieve total package count
        $.ajax({
            type: 'POST',
            url: "$urlGetPackageCount",
            data: {
                occurrenceId: occurrenceId
            },
            async: true,
            dataType: 'json',
            success: function(totalCount) {
                $('#hm-mgmt-bd-pkg-count-progress').hide();

                // If no packages were found, display notice
                if(totalCount == 0) {
                    var message = "No records found. Please create seeds and packages first.";
                    // Show notice
                    var redNotice = "<p style='font-size:115%;'>" + message + "</p>";
                    $('#hm-mgmt-bulk-delete-red-content').html(redNotice);
                    $('#hm-mgmt-bulk-delete-red-content').show();
                    $('#hm-mgmt-package-delete-progress').hide();
                    $('.hm-mgmt-delete-package-bulk-btn').hide();
                    $('#hm-mgmt-cancel-package-delete-modal').hide();
                    $('#hm-mgmt-close-package-delete-modal').show();
                    return;
                }

                $('.hm-mgmt-bd-notice').show();
                $('.hm-mgmt-delete-package-bulk-btn').removeClass('disabled');

                var currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));

                // If there are no items selected, disable "delete selected" btn
                if(currentSelection == null || currentSelection.length == 0){
                    $('#hm-mgmt-package-delete-selected-btn').addClass('disabled');
                    return;
                }
                $('#hm-mgmt-package-delete-selected-btn').removeClass('disabled');
            },
            error: function() {
                var errorMessage = modalTextHM('There was a problem while loading record information.');
                $('#hm-mgmt-bd-modal-body').html(errorMessage);
                $('#hm-mgmt-bd-pkg-count-progress').hide();
                $('.hm-mgmt-bd-notice').hide();
                $('.hm-mgmt-delete-package-bulk-btn').hide();
                $('#hm-mgmt-cancel-package-delete-modal').hide();
                $('#hm-mgmt-close-package-delete-modal').show();
            }
        });
    });

    // Delete selected packages button event
    $('#hm-mgmt-package-delete-selected-btn').on('click', function(e) {
        $('.hm-mgmt-package-delete-progress').show();
        $('.hm-mgmt-bd-progress-items').show();
        $('.hm-mgmt-delete-package-bulk-btn').hide();
        $('.hm-mgmt-bd-notice').hide();
        clearTextArea();

        processStart = new Date().getTime();
        packageIds = [];
        seedIds = [];
        germplasmIds = [];

        // Get values from storage
        var currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));
        $.each(currentSelection, function(index, value){
            id = value.split('-');
            packageIds.push(id[0]);
            germplasmIds.push(id[1]);
            seedIds.push(id[2]);
        });

        performDelete();
    });

    // Delete all packages button event
    $('#hm-mgmt-package-delete-all-btn').on('click', function(e) {
        $('.hm-mgmt-package-delete-progress').show();
        $('.hm-mgmt-bd-progress-items').show();
        $('.hm-mgmt-delete-package-bulk-btn').hide();
        $('.hm-mgmt-bd-notice').hide();
        clearTextArea();

        processStart = new Date().getTime();
        packageIds = [];
        seedIds = [];
        germplasmIds = [];

        // Retrieve total package count
        $.ajax({
            type: 'POST',
            url: "$urlGetPackageCount",
            data: {
                occurrenceId: occurrenceId
            },
            async: true,
            dataType: 'json',
            success: function(packageCount) {
                var ss = packageCount != 1 ? 'S' : '';
                updateTextArea(" > " + packageCount+" PACKAGE" + ss + " FOUND.\\n");

                // If package count exceeds threshold, send process to the background
                if(packageCount > packagesDeleteThreshold) {
                    deleteInBackground();
                    return;
                }

                updateTextArea(' > Retrieving package records...\\n');
                // Retrieve package seed ids
                $.ajax({
                    url: '$urlGetPackageSeedIds',
                    data: {
                        occurrenceId: occurrenceId
                    },
                    type: 'POST',
                    async: true,
                    success: function(data) {
                        data = JSON.parse(data);
                        $.each(data, function(index, value){
                            packageIds.push(value.packageDbId);
                            seedIds.push(value.seedDbId);
                            germplasmIds.push(value.germplasmDbId);
                        });

                        performDelete();
                    },
                    error: function() {
                        $('.hm-mgmt-package-delete-progress').hide();
                        $('.hm-mgmt-delete-package-bulk-btn').show();
                        $('.hm-mgmt-bd-progress-items').show();
                        $('.hm-mgmt-bd-notice').show();
                        ajaxError('An error occurred. Bulk delete unsuccessful.', 'hm-bulk-delete-packages-modal');
                    }
                });
            },
            error: function() {
                var errorMessage = modalTextHM('There was a problem while loading record information.');
                $('#hm-mgmt-bd-modal-body').html(errorMessage);
                $('#hm-mgmt-bd-pkg-count-progress').hide();
                $('.hm-mgmt-bd-notice').hide();
                $('.hm-mgmt-package-delete-progress').hide();
                $('.hm-mgmt-delete-package-bulk-btn').hide();
                $('#hm-mgmt-cancel-package-delete-modal').hide();
                $('#hm-mgmt-close-package-delete-modal').show();
            }
        });
    });

    // List creation: Format name, display name, and abbrev
    $(document).on('input', '#hm-list-name', function(e) {
        e.preventDefault();
        $('#hm-list-abbrev').val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());
        $('#hm-list-abbrev').trigger('change');
        $('#hm-list-display_name').val(this.value);
        $('#hm-list-display_name').trigger('change');
    });

    // List creation: Validate if all required fields are specified
    $('.form-control').bind("change keyup input",function() {
        var abbrev = $('#hm-list-abbrev').val();
        var name = $('#hm-list-name').val();
        var displayName = $('#hm-list-display_name').val();
        var listType = $('#hm-type-fld').val();

        if(abbrev != '' && name != '' && displayName != '' && listType != ''){
            $('#hm-save-list-confirm-btn').removeClass('disabled');
        } else {
            $('#hm-save-list-confirm-btn').addClass('disabled');
        }
    });

    // List creation: Save a new list for selected items
    $(document).on('click', "#selected-items", function(e) {
        enableListInputFields();
        $("#hm-selection-mode").html('selected');

        packageIds = [];
        seedIds = [];
        germplasmIds = [];

        var currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));

        if(currentSelection == null || currentSelection.length == 0){// no selected item(s)
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> Please select an item.";
                Materialize.toast(notif,2500);
            }
        }else{
            // Follows packageId[0]-germplasmId[1]-seedId[2] format
            $.each(currentSelection, function(index, value){
                id = value.split('-');
                packageIds.push(id[0]);
                germplasmIds.push(id[1]);
                seedIds.push(id[2]);
            });

            $("#hm-save-list-modal-body").show();
            $('.hm-modal-loading-ids').html('');
            $('#hm-save-list-modal').modal('show');
            $('#hm-save-list-modal').on('shown.bs.modal', function () {
                $('#hm-list-name').focus();
            })

            $('.hm-modal-loading').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
            $('.hm-save-list-preview-entries').html('');

            loadListPreview(packageIds, 'selected');
        }
    });

    // List creation: Save a new list for all items
    $(document).on('click', "#all-items", function(e) {
        enableListInputFields();
        var totalCount = $totalCount;
        $("#hm-selection-mode").html('all');
        $("#hm-save-list-modal-body").hide();
        $('#hm-save-list-modal').modal('show');
        $('#hm-save-list-modal').on('shown.bs.modal', function () {
            $('#hm-list-name').focus();
        })

        $('.hm-modal-loading-ids').html('<p style="font-size:115%;"><i>Loading list creation form and preview...</i></p><div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
        $('.hm-save-list-preview-entries').html('');

        if(totalCount > 0){
            $('.hm-modal-loading-ids').html('');
            $("#hm-save-list-modal-body").show();
            loadListPreview([], 'all');
        }else{
            $('#hm-save-list-modal').modal('hide');
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> No records found. Please create seeds and packages first.";
                Materialize.toast(notif,2500);
            }
        }
    })

    $('#ge-germplasm-coding-btn').on('click', function(e) {
        // Set selected Ids
        var selectionStorageId = occurrenceId + '_dynagrid-harvest-manager-package-mgmt-grid-table-con';
        var selectedIds = getSelection(selectionStorageId);
        
        // If selected items are empty, show warning message
        if(!selectedIds.length) {
            Materialize.toast("<i class='material-icons orange-text'>warning</i> No items selected. Please select at least one (1) item and try again.");
            return; 
        }

        // If ids for germplasm coding exceed threshold, show warning message        
        if(parseInt(selectedIds.length) > germplasmCodingThreshold) {
            Materialize.toast("<i class='material-icons orange-text'>warning</i> The maximum allowed germplasm per coding transaction is " + germplasmCodingThreshold + ".");
            return;
        }

        var selectedIdsGermplasm = [];

        for(var i = 0; i < selectedIds.length; i++) {
            var item = selectedIds[i];
            var itemParts = item.split('-');
            var germplasmId = itemParts[1];
            selectedIdsGermplasm.push(germplasmId);
        }

        var selectedIdsString = selectedIdsGermplasm.join("|");
        
        // Redirect
        var redirectUrl = '$codingGermplasmUrl' + '&' + 'mode=selected&selectedIdsString=' + selectedIdsString + '&returnUrl=' + encodeURIComponent($(location).attr("href"));
        window.location.replace(redirectUrl);
    });

    // List creation: Confirm saving of list
    $('#hm-save-list-confirm-btn').on('click', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var totalCount = parseInt($('#total-count-val').text());
        var listType = $('#hm-type-fld').val();
        var abbrev = $('#hm-list-abbrev').val();
        var name = $('#hm-list-name').val();
        var displayName = $('#hm-list-display_name').val();
        var description = $('#hm-list-description').val();
        var remarks = $('#hm-list-remarks').val();
        var ids = [];
        var selectionMode = $("#hm-selection-mode").text();
        var waitingMsg = totalCount > $createListThreshold ? 'preparing background process for list creation...' : 'saving the ' + name + ' list...';
        $('#hm-save-list-confirm-btn').addClass('disabled');
        disableListInputFields();
        $('.hm-modal-loading-ids').html('<p style="font-size:115%;"><i> All fields have been disabled. Please wait while ' + waitingMsg + '</i></p><div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div><br/><br/>');
        $('.hm-modal-loading-ids').show();

        if(listType == 'package'){
            ids = packageIds;
        } else if(listType == 'seed') {
            ids = seedIds;
        } else if(listType == 'germplasm') {
            ids = germplasmIds;
        }

        formattedIds = JSON.stringify(ids);
        $.ajax({
            type: 'POST',
            url: "$urlSaveList",
            data: {
                occurrenceId : occurrenceId,
                ids: formattedIds,
                abbrev: abbrev,
                name: name,
                displayName: displayName,
                description: description,
                remarks: remarks,
                type: listType,
                selectionMode: selectionMode
            },
            dataType: 'json',
            success: function(response) {
                if(response['success']){//successful processing
                    $('.hm-modal-loading-save').html('');
                    $('#hm-save-list-modal').modal('hide');
                    $('#hm-type-fld, #hm-list-abbrev, #hm-list-name, #hm-list-display_name, #hm-list-description,#hm-list-remarks').val('');
                    $('#hm-type-fld').val('').change();
                    if(response['inBackground']){
                        type = 'info';
                        icon = 'fa fa-info-circle';
                        displayCustomFlashMessage(
                            type, 
                            icon,
                            response['message'],
                            'hm-bg-alert-div',
                            10
                        );
                        updateNotif();
                    }else{
                        var notif = "<i class='material-icons green-text left'>done</i> " + response['message'] + ' (' + (response['totalTime']).toFixed(2) + ' s)';
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            Materialize.toast(notif,2500);
                        }
                    }
                } else {//unsuccessful processing
                    $('.hm-modal-loading-ids').hide();
                    enableListInputFields();
                    if(response['inBackground']){
                        type = 'warning';
                        icon = 'fa-fw fa fa-warning';
                        displayCustomFlashMessage(
                            type, 
                            icon,
                            response['message'],
                            'hm-bg-alert-div',
                            10
                        );
                        updateNotif();
                    }else{
                        var notif = "<i class='material-icons red-text' left>close</i> "+ response['message'];
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            Materialize.toast(notif,2500);
                        }
                    }
                }
            },
            error: function() {
                $('.hm-modal-loading-save').html('');
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while saving the list.";
                Materialize.toast(notif,2500);
                $('#hm-save-list-confirm-btn').addClass('disabled');
            }
        });
    });

    // trigger export of records in maangement tab browser
    $(document).on('click','#export-records-btn', function(e){
        e.preventDefault()
        e.stopPropagation()

        // show progress indicator
        $('#system-loading-indicator').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');

        $('.toast').css('display','none')

        // Get current package count
        $.ajax({
            url: '$urlGetPackageCount',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId
            },
            success: function(response) {
                var recordsCount = JSON.parse(response);
                
                var message = '';

                // if there are no records, display error toast message
                if(recordsCount == 0) {
                    // remove progress indicator
                    $('#system-loading-indicator').html('');

                    message = ` <i class="material-icons red-text" style="margin-right: 8px;">close</i> 
                                There are no records to export.`;
                    Materialize.toast(message, 5000);
                    return;
                }

                // notification for background job export processing
                if(recordsCount > recordsThreshold){
                    message = ` <i class="material-icons blue-text" style="margin-right: 8px;">info</i> 
                                Refreshing page. Background job for exporting seed and packages data is in progress.`;
                }
                else{
                    message = ` <i class="material-icons blue-text" style="margin-right: 8px;">info</i> 
                                Exporting all seed and packages data. Please wait.`;
                        
                }

                Materialize.toast(message, 5000);

                // Trigger Export working list
                window.location.assign(`\${window.location.origin}$exportPackageDataUrl&recordsCount=\${recordsCount}`);
                
                // remove loading indicator after 5 seconds
                setTimeout( function(){
                    $('.progress').css('display','none');
                    $('.indeterminate').css('display','none');
                }, 5000);
            },
            error: function(xhr, status, error) {
                var message = ` <i class="material-icons red-text" style="margin-right: 8px;">close</i> 
                                An error occurred while retrieving the package count.`;
                Materialize.toast(message, 5000);
                return;
            }
        });
    })

    // Disable list input fields
    function disableListInputFields(){
        $('#hm-type-fld, #hm-list-abbrev, #hm-list-name, #hm-list-display_name, #hm-list-description,#hm-list-remarks').addClass('disabled');
        $('#hm-type-fld').select2("enable",false);
    }

    // Enable list input fields
    function enableListInputFields(){
        $('#hm-type-fld, #hm-list-abbrev, #hm-list-name, #hm-list-display_name, #hm-list-description,#hm-list-remarks').removeClass('disabled');
        $('#hm-type-fld').select2("enable");
    }

    // Load preview of list items
    function loadListPreview(ids, selectionMode) {

        $.ajax({
            type: 'POST',
            url: "$urlRetrieveListPreview",
            data: {
                packageIdList: JSON.stringify(ids),
                occurrenceId: occurrenceId,
                selectionMode: selectionMode
            },
            async: true,
            dataType: 'json',
            success: function(response) {
                count = response.count;
                $('.hm-save-list-preview-entries').html(response.htmlData).trigger('change');
                $('.hm-modal-loading').html('');
                if(count > response.listPreviewThreshold) {
                    $('.hm-save-list-preview-entries').find('.summary').html('Showing <b>' + response.listPreviewThreshold + '</b> of <b>' + numberWithCommas(count) + '</b> items');
                }else{
                    $('.hm-save-list-preview-entries').find('.summary').html('<p style="color:#f1f4f5; margin-bottom:0px;">.</p>');
                }
            },
            error: function(){
                $('#hm-type-fld').select2("enable",false);
                $('.hm-save-list-preview-entries').html('<i>There was a problem while loading the save list form. Please try again later or file a report for assistance.</i>');
                $('.hm-modal-loading').html('');
            }
        });
    }

    /**
    * Updates the deletion process text area with the given message
    * @param {string} message message to add to the text area
    */
    function updateTextArea(message) {
        var textArea = $('#delete-progress-textarea');
        textArea.val(textArea.val() + message);
        if(typeof textArea[0] != 'undefined') textArea.scrollTop(textArea[0].scrollHeight);
    }

    /**
    * Empty out text area
    */
    function clearTextArea() {
        var textArea = $('#delete-progress-textarea');
        textArea.val('');
    }

    /**
     * Perform checks and deletion
     */
    function performDelete() {
        var packageCount = packageIds.length;
        var ss = packageCount != 1 ? 'S' : '';
        updateTextArea(" > " + packageCount+" PACKAGE" + ss + " SELECTED.\\n");

        // If package count exceeds threshold, send process to the background
        if(packageCount > packagesDeleteThreshold) {
            deleteInBackground();
            return;
        }

        checkSeedEntries();
    }

    /**
     * Check if seeds have been used in experiment entries
     */
    function checkSeedEntries() {
        updateTextArea(' > Checking if seeds have been used as entries...\\n');

        $.ajax({
            type: 'POST',
            url: "$urlCheckSeedEntries",
            data: {
                seedIds: JSON.stringify(seedIds)
            },
            async: true,
            dataType: 'json',
            success: function(response) {
                var seedsToDeleteCount = response.seedsToDeleteCount
                var seedsWithEntriesCount = response.seedsWithEntriesCount
                var totalSeeds = response.totalSeeds
                recordsObj = response

                // If there are seeds to be deleted, display appropriate message
                if(seedsToDeleteCount > 0){
                    if(seedsWithEntriesCount > 0) {
                        updateTextArea(" > " + seedsWithEntriesCount+" out of " + totalSeeds + " seeds have existing entries.\\n");
                        acknowledgeEntries(response);
                        return;
                    }

                    updateTextArea(" > No entries found.\\n");

                    var seedIdsToDelete = response.seedIdsToDelete
                    var germplasmIdsToDelete = response.germplasmIdsToDelete
                    var germplasmSeedRelation = response.germplasmSeedRelation
                    
                    checkGermplasm(seedIdsToDelete, germplasmIdsToDelete, germplasmSeedRelation);
                }
                // If there are no seeds to be deleted, close modal end display toast error message
                else {
                    updateTextArea(" > All the seeds of the select packages have existing entries.\\n");

                    acknowledgeEntries(response);
                    return;
                }
            },
            error: function() {
                $('.hm-mgmt-package-delete-progress').hide();
                $('.hm-mgmt-delete-package-bulk-btn').show();
                $('.hm-mgmt-bd-progress-items').show();
                $('.hm-mgmt-bd-notice').show();
                ajaxError('An error occurred. Bulk delete unsuccessful.', 'hm-bulk-delete-packages-modal', false);
            }
        });
    }

    /**
     * Renders the acknowledgement modal
     * @param response - previous response data
     */
    function acknowledgeEntries(response) {
        $("#hm-mgmt-bulk-delete-confirmation-modal-additional-main").hide();
        $("#hm-bulk-delete-packages-modal").modal('hide');
        $('#hm-mgmt-bulk-delete-confirmation-modal-body').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
        $("#hm-mgmt-bulk-delete-confirmation-modal").modal('show');

        // Render modal contents
        $.ajax({
            url: '$urlRenderBulkDeleteAck',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: { 
                occurrenceId: occurrenceId,
                seedIdsWithEntries: response.seedIdsWithEntries,
                seedsToDeleteCount: response.seedsToDeleteCount

            },
            success: function(html) {
                // Add modal contents to the modal body
                $('#hm-mgmt-bulk-delete-confirmation-modal-body').html(html);
                $('#hm-mgmt-bd-confirmation-acknowledge-btn').removeClass('disabled');
            },
            error: function(jqXHR, exception) {
                var errorMessage = modalTextHM('There was a problem while loading record information.', false, true);
                $('#hm-hd-bulk-update-modal-body').html(errorMessage);
            }
        });
    }

    /**
     * Checks if germplasm if used by seeds not included in the delete
     * @param {array} seedIdsToDelete array of seed ids
     * @param {array} germplasmIdsToDelete array of germplasm ids
     * @param {object} germplasmSeedRelation mapping of germplasm to seed
     */
    function checkGermplasm(seedIdsToDelete, germplasmIdsToDelete, germplasmSeedRelation) {
        updateTextArea(' > Checking germplasm records...\\n');

        $.ajax({
            type: 'POST',
            url: "$urlCheckGermplasm",
            data: {
                seedIdsToDelete: JSON.stringify(seedIdsToDelete),
                germplasmIdsToDelete: JSON.stringify(germplasmIdsToDelete),
                germplasmSeedRelation: JSON.stringify(germplasmSeedRelation)
            },
            async: true,
            dataType: 'json',
            success: function(response) {
                seedIds = response.seedIds;
                germplasmIds = response.germplasmIds;

                updateTextArea(" > DONE.\\n");
                deleteViaProcessor(seedIds, germplasmIds);
            },
            error: function() {
                $('.hm-mgmt-package-delete-progress').hide();
                $('.hm-mgmt-delete-package-bulk-btn').show();
                $('.hm-mgmt-bd-progress-items').show();
                $('.hm-mgmt-bd-notice').show();
                ajaxError('An error occurred. Bulk delete unsuccessful.', 'hm-bulk-delete-packages-modal');
            }
        });
    }

    /**
     * Send ids to the processor API for deletion
     * @param {array} seedIds array of seed ids
     * @param {array} germplasmIds array of germplasm ids
     */
    function deleteViaProcessor(seedIds, germplasmIds) {
        updateTextArea(' > Deleting packages, seeds, and germplasm...\\n');

        $.ajax({
            type: 'POST',
            url: "$urlDeleteViaProcessor",
            data: {
                seedIds: JSON.stringify(seedIds),
                germplasmIds: JSON.stringify(germplasmIds),
                occurrenceId: occurrenceId
            },
            async: true,
            dataType: 'json',
            success: function(response) {
                getDeleteProgress(response);
            },
            error: function() {
                $('.hm-mgmt-package-delete-progress').hide();
                $('.hm-mgmt-delete-package-bulk-btn').show();
                $('.hm-mgmt-bd-progress-items').show();
                $('.hm-mgmt-bd-notice').show();
                ajaxError('An error occurred. Bulk delete unsuccessful.', 'hm-bulk-delete-packages-modal');
            }
        });
    }

    /**
     * Track progress of deletion via background job ids
     * @param {array} backgroundJobIds array of background job ids
     */
    function getDeleteProgress(backgroundJobIds) {
        $.ajax({
            url: '$urlGetBackgroundJobs',
            data: {
                backgroundJobIds: JSON.stringify(backgroundJobIds),
            },
            type: 'POST',
            async: true,
            success: function(data) {
                var data = jQuery.parseJSON(data);
                var jobs = data.backgroundJobs;
                var jobsLength = jobs.length;
                var jobsDone = true;
                
                // Check if all jobs are done
                for(var i = 0; i < jobsLength; i++) {
                    jobsDone = jobsDone && (jobs[i].jobStatus == 'DONE' || jobs[i].jobStatus == 'FAILED');
                }

                // If all jobs are done, display success message
                if(jobsDone) {
                    var deleteExecutionTimeMillis = new Date().getTime() - processStart
                    clearSelectionHM(selectionStorageId);
                    $('.hm-mgmt-package-delete-progress').hide();
                    $('.hm-mgmt-delete-package-bulk-btn').show();
                    $('.hm-mgmt-bd-progress-items').show();
                    $('.hm-mgmt-bd-notice').show();
                    $("#hm-bulk-delete-packages-modal.in").modal('hide');
                    var notif = "<i class='material-icons green-text'>check</i>&nbsp; Packages were successfully deleted (" + (deleteExecutionTimeMillis*.001).toFixed(3) + "s)";
                    if (recordsObj.seedsToDeleteCount == 0) notif = "<i class='material-icons red-text'>close</i>&nbsp; Deletion failed: The seeds and packages have existing experiment entries.";
                    Materialize.toast(notif, 2500);
                    resetBrowser();
                    return;
                }

                // If not yet done, recursively call function to check again
                getDeleteProgress(backgroundJobIds)
            },
            error: function() {
                $('.hm-mgmt-package-delete-progress').hide();
                $('.hm-mgmt-delete-package-bulk-btn').show();
                $('.hm-mgmt-bd-progress-items').show();
                $('.hm-mgmt-bd-notice').show();
                ajaxError('An error occurred. Bulk delete unsuccessful.', 'hm-bulk-delete-packages-modal');
            }
        });
    }

    /**
     * Invoke background worker for deletion
     * @param {array} recordIds array containing record ids
     */
    function deleteInBackground() {
        updateTextArea(" > Sending data to background worker...\\n");

        $.ajax({
            url: '$urlDeleteInBackground',
            data: {
                occurrenceId: occurrenceId,
                seedIds: JSON.stringify(seedIds)
            },
            type: 'POST',
            async: true,
            success: function(data) {
                var result = JSON.parse(data);
                var success = result.success;
                var status = result.status;
                
                // Display flash message
                if(success) {
                    // Success message
                    displayCustomFlashMessage(
                        'info', 
                        'fa fa-info-circle',
                        'The packages, seeds, and germplasm are being deleted in the background. You may proceed with other tasks. You will be notified in this page once done.',
                        'hm-bg-alert-div',
                        10
                    );
                }
                else {
                    // Failure message
                    let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status != 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if(status == null) message += ' (#002)';

                    displayCustomFlashMessage(
                        'warning', 
                        'fa-fw fa fa-warning', 
                        message,
                        'hm-bg-alert-div'
                    );
                }

                clearSelectionHM(selectionStorageId);
                $('.hm-mgmt-package-delete-progress').hide();
                $('.hm-mgmt-delete-package-bulk-btn').show();
                $('.hm-mgmt-bd-progress-items').show();
                $('.hm-mgmt-bd-notice').show();
                $("#hm-bulk-delete-packages-modal.in").modal('hide');
                resetBrowser();

                return;
            },
            error: function() {
                ajaxError('An error occurred. Bulk delete unsuccessful.', 'hm-bulk-delete-packages-modal');
            }
        });
    }

    /**
     * Displays generic toast error message when an ajax call fails.
     * @param {string} message the message to display in the toast error message
     * @param {integer} modalId modal element id
     * @param {boolean} reset true/false reset browser
     */
    function ajaxError(message, modalId, reset = true) {
        $("#" + modalId).show();
        var notif = "<i class='material-icons red-text'>close</i>&nbsp;"+message;
        Materialize.toast(notif, 3500);
        // Close modal
        $("#" + modalId).modal('hide');
        // Reset the browser
        if(reset) resetBrowser();
    }


    /**
     * Displays a message to inform users when a threshold is exceeded
     * and the background processing is not available.
     * @param {integer} packageCount package count
     */
    function backgroundUnavailable(packageCount) {
        // Build message
        count = numberWithCommas(packageCount);
        threshold = numberWithCommas(packagesDeleteThreshold);
        var message = "The package count (" + count + ") exceeds the limit (" + threshold + "). Background processing is not available at the moment. Kindly select fewer packages and try again.";
        // Show notice
        var redNotice = "<p style='font-size:115%;'>" + message + "</p>";
        $('#hm-mgmt-bulk-delete-red-content').html(redNotice);
        $('#hm-mgmt-bulk-delete-red-content').show();
        $('#hm-mgmt-package-delete-progress').hide();
        return;
    }

    /**
     * Formats numbers to comma-separated string per hundreds.
     * @param {string} num number to be formatted
     * @returns string number formatted with commas
     * Examples:
     * 4        -> 4
     * 100      -> 100
     * 1234     -> 1,234
     * 9876543  -> 9,876,543
     */
    function numberWithCommas(num) {
        var pieces = num.toString().split(".");
        pieces[0] = pieces[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return pieces.join(".");
    }

    /**
     * Reset the browser and clear selection after bulk delete
     */
    function resetBrowser() {
        // Clear selection on grid reset
        var gridId = '$browserId-table-con';
        var selectionStorageId = occurrenceId + "_" + gridId;
        clearSelectionHM(selectionStorageId);
        var pageurl = $('#$browserId li.active a').attr('href');
        $.pjax.reload(
            {
                container:'#$browserId-pjax',
                url: pageurl,
                replace: false,
                type: 'POST',
                data: {
                    forceReset:false,
                    bulkOperation:true
                }
            }
        );
    }
});

JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    div.hm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    div#hm-mgmt-package {
        overflow: auto;
        overflow-x: hidden;
    }
');

?>