<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\helpers\Url;

/**
 * Renders information panels in Harvest manager's tabs
 */

?>

<!-- Collapsible panel for information -->
<div class="hm-info-panel-spacer-div" style="margin-bottom:0px">
    <div class="col-md-12" style="padding-right: 0px;-webkit-transition: .3s ease all;
    -o-transition: .3s ease all;
    -moz-transition: .3s ease all;
    transition: .3s ease all;
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    backface-visibility: hidden;
    margin-bottom: 10px;" ;>

        <ul class="collapsible collapsible-filter active">
            <li class="" id="filters">
                <div class="collapsible-header query-div"><i class="material-icons">info</i> Information</div>
                <div class="collapsible-body" style="padding-top:0px">
                    <div style="margin-top:5px;">

                        <div class="row" style="margin-bottom:0px;">
                            <div id="occurrence_info_id" class="col-md-12"  style="padding-right:0px;">   
                                <div class="col-md-12" style="padding-right:0px;">
                                    <div class="panel panel-default occurrence_details_div" style="min-height:10px;">
                                        <div id="occurrence_details_id" class="row" style="margin-left:5px; margin-top:5px;padding-top:5px; padding-right:25px; padding-left:10px;">
                                            <?php
                                                // Render occurrence details and harvest data
                                                echo $occurrenceDetails;
                                            ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </li>
        </ul>

    </div>
</div>

<!-- This div prevents the accordion from overlapping with the elements below it -->
<div class="row" style ="margin-bottom:0px;"></div>
<?php

$script = <<< JS

$("document").ready(function(){
    // ...
});

JS;

$this->registerJs($script);

?>