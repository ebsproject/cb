<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\helpers\Url;

/**
 * Renders search parameters in the occurrence selection step
 */

// Define URLs
$urlGetExperimentOccurrences = Url::to(['occurrence-selection/get-experiment-occurrences']);
$urlGetOccurrenceExperiment = Url::to(['occurrence-selection/get-occurrence-experiment']);
$urlGetOccurrenceDetails = Url::to(['occurrence-selection/get-occurrence-details']);
// Load button URLs
$urlPlots = Url::to(['harvest-data/plots', 'program' => $program]);
$urlCrosses = Url::to(['harvest-data/crosses', 'program' => $program]);

?>

<div class="collapsible-header active" style="margin-bottom:10px">
    <i class="material-icons">search</i> <?= \Yii::t('app', 'Search Parameters') ?>    
</div>

<div style="margin-left:20px">
    <p>Select an occurrence to be harvested, then load plots or crosses.</p>
    <?php
        foreach ($searchParameters as $field) {
            if (isset($field['value'])) {
                echo $field['value'];
            }
        }  
    ?>
</div>

<?php


$script = <<< JS
// Set loading indicator
var loading = '<div class="progress" style="margin:0; z-index:2000"><div class="indeterminate"></div></div>';
var dataAppended = false;

$("document").ready(function(){

    // Reset parameters click event
    $(document).on('click','#hm-reset-params-btn',function(e){
        $('#experiment_name-filter').val("").change();
    });

    // Load button click event
    $(document).on('click','.hm-load-btn',function(e){
        // Show loading indicator
        loadingIndicator(true);
    });


    // Program change event
    $(document).on('change','#program-filter',function(e){
        $('#experiment_name-filter').val("").change();
    });

    // Experiment name change event
    $(document).on('change','#experiment_name-filter',function(e){
        $('#hm-load-plots-btn, #hm-load-crosses-btn').attr('disabled',true);

        // If change was triggered by adding data to the select filter,
        // do not perform the rest of this code
        if(dataAppended) {
            return;
        }

        // Set opacity to 50%
        $("#harvest-summary-id").animate({"opacity":0.5}, 100);

        // Retrieve search field ids
        programId = $('#program-filter').val();
        experimentId = $('#experiment_name-filter').val();
        occurrenceId = $('#occurrence_name-filter').val();

        // If experiment is tempy, remove occurrence
        if(experimentId == '') {
            $('#occurrence_name-filter').val('').trigger("change");
            return;
        }
        // If program is empty, remove experiment
        if(programId == '') {
            $(this).val("").change();
            return;
        }

        // Show loading indicator
        loadingIndicator(true);

        // Get planted occurrences of experiment
        $.ajax({
            url: '$urlGetExperimentOccurrences',
            data: {
                programId: programId,
                experimentId: experimentId
            },
            type: 'POST',
            async: true,
            success: function(data) {
                occurrences = JSON.parse(data);

                // Append occurrence to select2 data
                addNewSelect2Data('occurrence_name-filter', occurrences);

                // If experiment has 1 occurrence, apply it to occurrence filter
                if(occurrences.length==1) {
                    var newOccurrenceId = JSON.stringify(occurrences[0].id);

                    // If the new occurrence is already equal to the current occurrence
                    // retrieve occurrence details
                    if(occurrenceId == newOccurrenceId) {
                        getOccurrenceDetails();
                        return;
                    }
                    // If not, trigger change and apply new occurrence
                    else {
                        $('#occurrence_name-filter').val(newOccurrenceId).trigger("change");
                    }
                }
                // If experiment has no occurrences, change value to empty and trigger change
                else if(occurrences.length==0) {
                    $('#occurrence_name-filter').val('').trigger("change");
                }
                // If experiment has multiple occurrences, clear occurrence filter
                else {
                    if(occurrenceId!='') {
                        for(var i = 0; i < occurrences.length; i++) {
                            var newOccurrenceId = JSON.stringify(occurrences[i].id);
                            if(occurrenceId==newOccurrenceId) {
                                getOccurrenceDetails();
                                return;
                            }
                        }
                        $('#occurrence_name-filter').val('').trigger("change");
                    }
                    else {
                        // Hide loading indicator
                        loadingIndicator(false);
                    }
                }
            },
            error: function(jqXHR, exception) {
            }
        });

        // Display warning if occurrence is empty
        occurrenceId = $('#occurrence_name-filter').val();
        if(occurrenceId=='') {
            $('#occurrence_name-filter').parent().parent().addClass('has-error');
            $('#occurrence-name-warning').removeClass("hidden");
        }
        else {
            $('#occurrence_name-filter').parent().parent().removeClass('has-error');
            $('#occurrence-name-warning').addClass("hidden");
        }

    });


    // Location name change event
    $(document).on('change','#occurrence_name-filter',function(e){
        $('#hm-load-plots-btn, #hm-load-crosses-btn').attr('disabled',true);

        // If change was triggered by adding data to the select filter,
        // do not perform the rest of this code
        if(dataAppended) {
            return;
        }

        // Set opacity to 50%
        $("#harvest-summary-id").animate({"opacity":0.5}, 100);

        // Disable buttons
        $('#hm-load-plots-btn, #hm-load-crosses-btn').attr('disabled',true);
        programId = $('#program-filter').val();

        // If the input is blank, display error message
        if($(this).val() == '') {
            getOccurrenceDetails();
            $('#hm-load-plots-btn, #hm-load-crosses-btn').attr('disabled',true);
            $('#occurrence_name-filter').parent().parent().addClass('has-error');
            $('#occurrence-name-warning').removeClass("hidden");
            return;
        }
        // If program is empty, clear value
        else if(programId == '') {
            $(this).val("").change();
            return;
        }
        // If occurrence is not empy, remove error message
        else {
            // Show loading indicator
            loadingIndicator(true)
            $('#occurrence_name-filter').parent().parent().removeClass('has-error');
            $('#occurrence-name-warning').addClass("hidden");
        }
        occurrenceId = $(this).val();

        // If experiment name field is empty, search for 
        var experimentId = $('#experiment_name-filter').val();
        if(experimentId=="") {
            $.ajax({
                url: '$urlGetOccurrenceExperiment',
                type: 'POST',
                data: {
                    occurrenceId:occurrenceId
                },
                async: true,
                success: function(response){
                    var expData = jQuery.parseJSON(response);
                    var experimentId = expData.experimentId;
                    var data = expData.data;

                    // Append data to experiment name filter
                    addNewSelect2Data('experiment_name-filter', data);
                    $('#experiment_name-filter').val(experimentId).trigger("change");
                },
                error: function(jqXHR, exception) {
                }
            });
        }
        else {
            // If experiment is not empty, load occurrence details
            getOccurrenceDetails();
        }
    });

    // Load plots btn click event
    $(document).on('click', '#hm-load-plots-btn', function(e) {
        e.preventDefault();
        var occurrenceId = $('#occurrence_name-filter').val();
        var url = '$urlPlots&occurrenceId=' + occurrenceId;
        window.location = url;
    });

    // Load crosses btn click event
    $(document).on('click', '#hm-load-crosses-btn', function(e) {
        e.preventDefault();
        var occurrenceId = $('#occurrence_name-filter').val();
        var url = '$urlCrosses&occurrenceId=' + occurrenceId;
        window.location = url;
    });

});

/**
 * Appends new data to the given select2 id
 * @param {string} select2Id - select2 element id
 * @param {array} dataArray - array containg ids and text
 */
function addNewSelect2Data(select2Id, dataArray) {
    var dataCount = dataArray.length;
    var newOption = null;
    var data = null;

    dataAppended = true;

    for(var i = 0; i < dataCount; i++) {
        data = dataArray[i];
        newOption = new Option(data.text, data.id, false, false);
        $('#'+select2Id).append(newOption).trigger('change');
    }

    dataAppended = false;
}

/**
 * Retrieves occurrence details and renders the panel
 */
function getOccurrenceDetails() {
    var occurrenceId = $('#occurrence_name-filter').val();

    $.ajax({
        url: '$urlGetOccurrenceDetails',
        type: 'POST',
        data: {
            occurrenceId:occurrenceId
        },
        async: true,
        success: function(response){
            data = JSON.parse(response);
            occurrenceDetails = data.occurrenceDetails;
            experimentType = data.experimentType;

            // Insert occurrence details to div
            $("#harvest-summary-id").html(occurrenceDetails);

            // Enable/disable buttons
            if(experimentType == "") {  
                $('#hm-load-plots-btn, #hm-load-crosses-btn').attr('disabled',true);
            }
            else {
                $('#hm-load-plots-btn, #hm-load-crosses-btn').attr('disabled',false);
            }
            
            // Hide loading indicator
            loadingIndicator(false);

            // Set opacity to 100%
            $("#harvest-summary-id").animate({"opacity":1.0}, 100);
        },
        error: function(jqXHR, exception) {
        }
    });
}

JS;

$this->registerJs($script);

?>