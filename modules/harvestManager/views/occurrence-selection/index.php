<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * Renders occurrence selection step for Harvest Manager
 */

echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/tab_main.php',[
    'controller' => 'occurrence-selection',
    'program' => $program,
    'occurrenceId' => $occurrenceId,
    'dataLevel' => $dataLevel
]);

$form = ActiveForm::begin([
    'action' => ['find'],
    'enableClientValidation' => false,
    'id' => 'harvest-filter',
    'options' => [
        'class' => 'harvest-filter'
    ],
]);

    
?>

<div id="search-parameters-div-id" class="col-md-5"  style="padding-right:20px;">   
    <div class="col-md-12" style="padding-right:0px;">
        <div class="panel panel-default search-parameters-div" style=" min-height:100px;">
            <div id="search-parameters-id" class="row" style="margin-left:5px; margin-top:5px;padding-top:5px; padding-right:25px; padding-left:10px;">
                <?php
                    echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/occurrence-selection/search_params.php',[
                        'searchParameters' => $searchParameters,
                        'occurrenceId' => $occurrenceId,
                        'program' => $program
                    ]);
                ?>
                <?= Html::submitButton(\Yii::t('app', 'Plots'), [
                        'class' => 'waves-effect waves-light btn btn-secondary pull-right light-blue darken-3 hm-load-btn hm-tooltipped',
                        'id' => 'hm-load-plots-btn',
                        'disabled' => true,
                        'data-pjax' => true,
                        'data-position' => 'bottom',
                        'data-tooltip' => \Yii::t('app', 'Load plots'),
                        'data-level' => 'plot',
                    ]).
                    Html::submitButton(\Yii::t('app', 'Crosses'), [
                        'class' => 'waves-effect waves-light btn btn-secondary pull-right light-green darken-3 hm-load-btn hm-tooltipped',
                        'style' => 'margin-right:5px; margin-bottom:20px;',
                        'id' => 'hm-load-crosses-btn',
                        'disabled' => true,
                        'data-pjax' => true,
                        'data-position' => 'bottom',
                        'data-tooltip' => \Yii::t('app', 'Load crosses'),
                        'data-level' => 'cross',
                    ]).
                    Html::Button(\Yii::t('app', 'Clear'),[
                        'class' => 'waves-effect waves-light btn btn-secondary pull-right teal hm-tooltipped',
                        'style' => 'margin-right:5px; margin-bottom:20px;',
                        'id' => 'hm-reset-params-btn',
                        'disabled' => false,
                        'data-pjax'=>true,
                        'data-position' => 'bottom',
                        'data-tooltip' => \Yii::t('app', 'Clear all search parameters'),
                    ]);
                ?>
            </div>
        </div>

    </div>
</div>

<div id="harvest-summary-div-id" class="col-md-7"  style="padding-right:0px;">   
    <div class="col-md-12" style="padding-right:0px;">
        <div class="panel panel-default harvest-summary-div" style=" min-height:250px;">
            <div id="harvest-summary-id" class="row" style="margin-left:5px; margin-top:5px;padding-top:5px; padding-right:25px; padding-left:10px;">
                <?php
                    echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/occurence_details.php',[
                    ]);
                ?>
            </div>
        </div>

    </div>
</div>

<?php

ActiveForm::end();


$script = <<< JS

$("document").ready(function(){
    // Load experiment and occurrence details
    if($occurrenceId) $('#occurrence_name-filter').trigger("change");
});

JS;

$this->registerJs($script);

?>