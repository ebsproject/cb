<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
// Import components
use app\components\FavoritesWidget;
// Import models
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

/**
 * Renders step tabs in Harvest Manager
 */

// Initializes nav bar dropdowns
Modal::begin([]);
Modal::end();

$currentUrl = '/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
$controllerID = Yii::$app->controller->id;

// Determine view and data level
if(empty($view)) {
    $view = isset($dataLevel) ? ($dataLevel == 'crosses' ? 'focus_cross' : 'focus_plot') : '';
}
else {
    $dataLevel = $view == 'focus_cross' ? 'crosses' : 'plots';
}

// Define URLs
$urlRenderOccurrenceDetails = Url::to(['occurrence-selection/render-occurrence-details']);
$urlViewInEm = Yii::$app->getUrlManager()->createUrl(['occurrence/view/index', 'program' => $program, 'id' => $occurrenceId]);
$urlOccurrenceSelectionIndex = Url::to(['occurrence-selection/index', 'program' => $program, 'occurrenceId' => $occurrenceId]);
$urlHarvestDataIndex = Url::to(['harvest-data/'.$dataLevel, 'program' => $program, 'occurrenceId' => $occurrenceId]);
$urlHarvestDataPlots = Url::to(['harvest-data/plots', 'program' => $program, 'occurrenceId' => $occurrenceId]);
$urlHarvestDataCrosses = Url::to(['harvest-data/crosses', 'program' => $program, 'occurrenceId' => $occurrenceId]);
$urlCreationIndex = Url::to(['creation/index', 'program' => $program, 'occurrenceId' => $occurrenceId, 'source' => $dataLevel, 'view' => $view]);
$urlManagementIndex = Url::to(['management/index', 'program' => $program, 'occurrenceId' => $occurrenceId, 'source' => $dataLevel]);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');

// Check if browser Id is empty (occurrence tab)
$browserId = empty($browserId) ? 'none' : $browserId;

// Save browser settings
// for all browsers in Harvest Manager
(new DataBrowserConfiguration())->saveDataBrowserSettings([
    'dynagrid-harvest-manager-plot-hdata-grid',
    'dynagrid-harvest-manager-cross-hdata-grid',
    'dynagrid-harvest-manager-plot-crt-grid',
    'dynagrid-harvest-manager-cross-crt-grid',
    'dynagrid-harvest-manager-package-mgmt-grid'
]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ". json_encode($browserId) .",
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

// Set tab activities
$tab1Active = strpos($controllerID, 'occurrence-selection') !== false ? ' active' : '';
$tabsDisabled = $tab1Active == ' active' ? ' disabled' : '';
$tab2Active = strpos($controllerID, 'harvest-data') !== false ? ' active' : '';
$tab3Active = strpos($controllerID, 'creation') !== false ? ' active' : '';
$tab4Active = strpos($controllerID, 'management') !== false ? ' active' : '';
$nextHidden = $tab1Active == ' active' || $tab4Active == ' active' ? ' hidden' : '';

// If CPN, add phase to experiment type
if(isset($cpnCheck) && $cpnCheck['isCPN']) {
    $experimentType .= ' (Phase ' . $cpnCheck['phase'] . ')';
}

// Build occurrence name display
$nameDisplay = Yii::t('app', 'Harvest Manager') .
FavoritesWidget::widget([
    'module' => 'harvestManager',
    'controller' => $controllerID
]);
if($controllerID != "occurrence-selection") {
    // Create achor element for the occurrence info
    $infoAnchor = Html::a(
        \Yii::t('app', $occurrenceName . "<i class='material-icons hm-occurrence-info'>info_outline</i>"),
        '#',
        [
            'class' => 'hm-occurrence-info-dropdown hm-tooltipped',
            'id' => 'hm-occurrence-info-anchor',
            "data-tooltip" => \Yii::t('app', 'Occurrence details'),
            'data-activates' => 'hm-occurrence-info-content'
        ]
    );

    $nameDisplay = '<a href="' . $urlOccurrenceSelectionIndex . '">' . 
        \Yii::t('app', 'Harvest Manager') . '</a>' .
        "<small> » $infoAnchor ($experimentType)". "</small>";
}

?>

<!-- Occurrence details dropdown -->
<div id="hm-occurrence-info-content" class="dropdown-content dropdown-menu dropdown-menu-right">
    <!-- Loading indicator for retrieval of details -->
    <span id="hm-occurrence-info-indicator-span">
        <div class="progress" id="hm-occurrence-info-indicator" style="margin:0px;">
            <div class="indeterminate">
            </div>
        </div>
    </span>
    <div id="hm-occurrence-info-content-inner">
        <!-- Div for occurrence details -->
        <div id="hm-occurrence-info-body">
        </div>
        <!-- Div for button to EM -->
        <div id="hm-occurrence-info-btns" class="pull-right">
            <?=
                Html::a(\Yii::t('app', ' View in Experiment Manager'),$urlViewInEm, [
                    'class' => 'btn btn-primary waves-effect waves-light disabled',
                    'id' => 'hm-occurrence-info-view-btn',
                    'target' => '_blank'
                ]);
            ?>
        </div>
    </div>
</div>

<!-- Display starts here -->
<div class="col-md-9">
    <h3>
        <?=
            $nameDisplay
        ?>
    </h3>
</div>
<!-- Next button -->
<div class="col-md-3" align="right" style="margin-top:5px; margin-bottom:5px; padding: 0px;">
    <?php
        echo '<button class="btn btn-default pull-right lighten-4 hm-tooltipped' . $nextHidden . '" id="harvest-manager-next-step-btn" type="button" data-tooltip="' . \Yii::t('app', ' Next step') . '" style="margin-right:5px; margin-bottom:5px;">Next</button>';
    ?>
</div>

<!-- HM tabs -->
<div class="row" style="margin-top:0px; margin-bottom:0px">
    <ul id="tabs" class="tabs">
        <?php
            echo '<li class="tab col step s3 hm-tool-tab" id="occurrence-selection-tab" link="' . $urlOccurrenceSelectionIndex . '" controller="occurrence-selection"><a class="occurrence-selection-tab a-tab' . $tab1Active . '" href="' . $urlOccurrenceSelectionIndex . '"><span class="badge-step" style="padding:4px 7px;">1</span>' . \Yii::t('app', 'Occurrence') . '</a></li>' .
                '<li class="tab col step s3 hm-tool-tab'.$tabsDisabled.'" id="harvest-data-tab" link="' . $urlHarvestDataIndex . '" controller="harvest-data"><a class="harvest-data-tab a-tab'.$tabsDisabled.'' . $tab2Active . '" href="' . $urlHarvestDataIndex . '"><span class="badge-step" style="padding:4px 7px;">2</span>' . \Yii::t('app', 'Harvest Data') . '</a></li>' .
                '<li class="tab col step s3 hm-tool-tab'.$tabsDisabled.'" id="creation-tab" link="' . $urlCreationIndex . '" controller="creation"><a class="creation-tab a-tab'.$tabsDisabled.'' . $tab3Active . '" href="' . $urlCreationIndex . '"><span class="badge-step" style="padding:4px 7px;">3</span>' . \Yii::t('app', 'Creation') . '</a></li>' .
                '<li class="tab col step s3 hm-tool-tab'.$tabsDisabled.'" id="management-tab" link="' . $urlManagementIndex . '" controller="management"><a class="management-tab a-tab'.$tabsDisabled.'' . $tab4Active . '" href="' . $urlManagementIndex . '"><span class="badge-step" style="padding:4px 7px;">4</span>' . \Yii::t('app', 'Management') . '</a></li>';
        ?>
    </ul>
    <br />

</div>

<?php
$script = <<< JS
// Set loading indicator
var loading = '<div class="progress" style="margin:0; z-index:2000"><div class="indeterminate"></div></div>';
// Set variables
var occurrenceId = $occurrenceId;
var supportedGrids = [
    'dynagrid-harvest-manager-plot-hdata-grid-table-con',
    'dynagrid-harvest-manager-cross-hdata-grid-table-con',
    'dynagrid-harvest-manager-package-mgmt-grid-table-con'
];
var supportedCount = supportedGrids.length;

// Set alignment of table headers to 'top'
thAlignTop();

// Initialize dropdown element
$('#hm-occurrence-info-anchor').dropdown({
    inDuration: 50,
    outDuration: 225,
    constrainWidth: false,
    hover: false,
    gutter: 0,
    belowOrigin: true,
    alignment: 'right',
    stopPropagation: true
});

$("document").ready(function(){
    initializeTooltips();

    // Hide tooltip on click event
    $('.hm-tooltipped').click(function () {
        $(this).tooltip("hide");
    });

    // Tab click event
    $('.hm-tool-tab').click(function () {
        var tabController = $(this).attr('controller');
        var currentController = "$controllerID";
        // If the controller of the clicked tab is different from the current tab's controller,
        // clear the checkbox selection
        if(tabController != currentController) {
            clearSelectionHM(null, true);
        }
        // Show loading indicator
        loadingIndicator(true);
        // Redirect to tab link
        var link = $(this).attr('link');
        window.location = link;
    });

    // If click occurred outside of the occurrence info div
    // enable hm tool tabs
    $(document).click(function(event) { 
        var target = $(event.target);
        if(!target.closest('#hm-occurrence-info-content').length
            && $('#hm-occurrence-info-content').is(":visible"))
        {
            // Enable tabs
            $('.hm-tool-tab').removeClass('disabled');
            $('#hm-occurrence-info-anchor').removeClass('disabled');
        }
        
    });

    // Adjust size and positioning, and retrieve occurrence details
    // on occurrence details anchor click event
    $('#hm-occurrence-info-anchor').click(function (e) {
        // temporarily disable tabs to avoid unintentional tab switch
        $('.hm-tool-tab').addClass('disabled');
        $(this).addClass('disabled');

        // Get divs
        var div = document.getElementById('hm-occurrence-info-content');
        var innerDiv = document.getElementById('hm-occurrence-info-body');

        // If left value is negative, set to 10px
        var left = parseInt(div.style.left.replace("px",""));
        var windowWidth = $(window).width();
        if((left < 0 && windowWidth < 992) || windowWidth < 992) div.style.left = '10px';
        else div.style.left = '237px';

        // Set max height
        innerDiv.style.maxHeight = '500px';

        // Retreive occurrence details
        renderOccurrenceDetails();
    });

    // Prevent occurrence info content from closing when clicked
    $('#hm-occurrence-info-content').on('click', function (event) {
        event.stopPropagation();
    });

    // On "next" button click, go to next tab
    $(document).on('click', '#harvest-manager-next-step-btn', function(e) {
        var url = '';
        if ('$tab2Active' == ' active') url = '$urlCreationIndex';
        if ('$tab3Active' == ' active') url = '$urlManagementIndex';

        window.location = url;
    });

    // Select all checkboxes in current page
    $(document).on('click', '#hm-browser-select-all-checkbox', function(e){
        var checkboxClass = "hm-browser-single-checkbox";
        var gridId = $(this).attr("table-con");

        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.'+checkboxClass).attr('checked','checked');
            $('.'+checkboxClass).prop('checked',true);
            $("."+checkboxClass+":checkbox").parent("td").parent("tr").addClass("selected");
            saveSelectionHM('selectAll', gridId);
        }else{
            $(this).removeAttr('checked');
            $('.'+checkboxClass).prop('checked',false);
            $('.'+checkboxClass).removeAttr('checked');
            $("input:checkbox."+checkboxClass).parent("td").parent("tr").removeClass("selected"); 
            saveSelectionHM('deselectAll', gridId);
        }
    });

    // Browser checkbox click event
    $(document).on('click', '.hm-browser-single-checkbox', function(e){
        var gridId = $(this).attr("table-con");

        if($(this).prop("checked") === true)  {
            $(this).parent("td").parent("tr").addClass("selected");
            var row = $(this);
            var value = row.prop("id");
            var selAll = tickSelectAll();
            $('#hm-browser-select-all-checkbox').prop("checked", selAll);
            saveSelectionHM('select', gridId, value);
        } else{
            $(this).parent("td").parent("tr").removeClass("selected");
            var row = $(this);
            var value = row.prop("id");
            saveSelectionHM('deselect', gridId, value);
            $('#hm-browser-select-all-checkbox').prop("checked", false);
        }
    });
});

/**
 * Set alignment of table headers to 'top'
 */
function thAlignTop() {
    $("th").removeClass("kv-align-middle").addClass("kv-align-top");
}

/**
 * Format text for HM modals.
 * @param {string} string the text to be formatted
 * @param {boolean} strong whether or not to add a strong tag <strong> around the text
 * @param {boolean} emphasis whether or not to add an emphasis tag <em> around the text
 * @return {boolean} whether or not to tick the select all checkbox element
 */
function modalTextHM(string, strong = false, emphasis = false) {
    if(strong) string = '<strong>' + string + '</strong>';
    if(emphasis) string = '<em>' + string + '</em>';
    return '<p style="font-size:115%;">' + string + '</p>';
}

/**
 * Check if all items in the current page are selected.
 * @return {boolean} whether or not to tick the select all checkbox element
 */
function tickSelectAll() {
    var checkboxClass = "hm-browser-single-checkbox";
    var result = true;
    // Check each checkbox if checked
    $.each($('.'+checkboxClass), function(i, val) {
        result = result && $(this).prop("checked");
    });
    // Return result (true or false)
    return result;
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} selectionStorageId unique string identifier holding the selected items
 * @param {boolean} all whether or not all checkbox selections are to be cleared
 */
function clearSelectionHM(selectionStorageId, all = false) {
    // If all flag is set to true, clear selection for all supported grids
    if (all) {
        // Loop through supported grids
        for(var i = 0; i < supportedCount; i++) {
            // Build selection identifier
            var currentSession = occurrenceId + "_" + supportedGrids[i];
            // Remove selection from storage
            sessionStorage.removeItem(currentSession);
        }
    }
    // If all flag is false, remove specified  selection id
    else sessionStorage.removeItem(selectionStorageId);
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} mode the mode of saving to be performed (select, deselect, selectAll, deselectAll)
 * @param {string} gridId unique string identifier of the grid 
 * @param {string} value the value of the selection to be saved
 */
function saveSelectionHM(mode, gridId, value = null) {
    var selectionStorageId = occurrenceId + "_" + gridId;
    var checkboxClass = "hm-browser-single-checkbox";
    // Retrieve current selection from storage
    var currentSelection = sessionStorage.getItem(selectionStorageId);
    if (currentSelection == null) {
        currentSelection = [];
    } else {
        currentSelection = JSON.parse(currentSelection);
    }

    // For selectAll mode, add all items in the current page to the storage
    if (mode == 'selectAll') {
        $('#'+gridId).find("input:checkbox."+checkboxClass).each(function() {
            if ($(this).prop("checked") === true) {

                var currentValue = $(this).attr('id');

                if ($.inArray(currentValue, currentSelection) === -1) {
                    currentSelection.push(currentValue);
                }
            }
        });

    }
    // For deselectAll mode, remove all items in the current page from the storage
    else if (mode == 'deselectAll') {
        $('#'+gridId).find("input:checkbox."+checkboxClass).each(function() {
            if ($(this).prop("checked") === false) {
                var currentValue = $(this).attr('id');
                var index = currentSelection.indexOf(currentValue);
                if (index !== -1) {
                    currentSelection.splice(index,1);
                }
            }
        });
    }
    // For select mode, add one item to the storage
    else if (mode == 'select') {
        if ($.inArray(value, currentSelection) === -1) {
            currentSelection.push(value);
        }
    }
    // For deselect mode, remove one item from the storage
    else if (mode == 'deselect') {
        var index = currentSelection.indexOf(value);
        if (index !== -1) {
            currentSelection.splice(index,1);
        }
    }
    // Display total count in the browser
    displayTotalCountHM(currentSelection);
    // Save the selection to the storage
    sessionStorage.setItem(selectionStorageId, JSON.stringify(currentSelection));
}

/**
 * Checks and restores the default state of the checkboxes based on the storage values
 * @param {string} selectionStorageId unique string identifier holding the selected items
 * @param {string} gridId unique string identifier of the grid 
 */
function checkboxActionsHM(selectionStorageId, gridId) {
    var selectedRows = sessionStorage.getItem(selectionStorageId);
    selectedRows = JSON.parse(selectedRows);
    // Display total count in the browser
    displayTotalCountHM(selectedRows);
    // Set variables
    var checkboxClass = "hm-browser-single-checkbox";
    var selAllId = "hm-browser-select-all-checkbox";
    var grid = $('#'+gridId);
    // Check selected items
    grid.find("."+checkboxClass).each(function() {
        var value = $(this).attr("id");
        if (selectedRows !== null) {
            if ($.inArray(value, selectedRows) !== -1) {
                $(this).prop('checked',true);
                $(this).addClass('isClicked');
                $(this).parent("td").parent("tr").addClass("selected");
            }
        }
    });
    // Tick select all if all items in page are selected
    if (grid.find("."+checkboxClass).length != 0) {
        var all = grid.find("."+checkboxClass).length == grid.find("."+checkboxClass + ":checked").length;
        $('#'+selAllId).prop('checked', all).change();
    }
}

/**
 * Displays the total count of selected items in the browser
 * @param {array} currentSelection array containing selected item values
 */
function displayTotalCountHM(currentSelection) {
    var noneSelected = '<b><span id="selected-items-count">No</span></b> <span id="selected-items-text"> selected items. &nbsp;</span>';

    // If current selection is null, display "No selected items"
    if (currentSelection === null) {
        $('#total-selected-text').html(noneSelected);
        return true;
    }
    // Get total count
    var totalCount = currentSelection.length;
    // If total count is greater than 0, display count
    if (totalCount > 0) {
        let s = totalCount == 1 ? '' : 's';
        $('#total-selected-text').html(
            '<b><span id="selected-items-count"> '+totalCount+
            '</span></b> <span id="selected-items-text"> selected item' + s + '. &nbsp;</span>'
        );
    }
    // if total count is less than or equal to 0, display "No selected items"
    else {
        $('#total-selected-text').html(noneSelected);
    }
}

/**
 * Retrieves the selection from the storage
 * @param {string} selectionStorageId unique string identifier holding the selected items
 * @return {array} array containing the selection
 */
function getSelection(selectionStorageId) {
    var currentSelection = sessionStorage.getItem(selectionStorageId);

    if (currentSelection == null) {
        return [];
    } else {
        return JSON.parse(currentSelection);
    }
}

/**
 * Retrieves the occurrence details and renders it in the dropdown panel
 */
function renderOccurrenceDetails() {
    // Animate opacity change
    $("#hm-occurrence-info-body").animate({"opacity":0.5}, 100);
    $("#hm-occurrence-info-indicator").animate({"opacity":1.0}, 100);
    

    $.ajax({
        url: '$urlRenderOccurrenceDetails',
        type: 'POST',
        data: {
            occurrenceId:"$occurrenceId"
        },
        async: true,
        success: function(response){
            data = JSON.parse(response);
            occurrenceDetails = data.occurrenceDetails;

            // Render details
            $("#hm-occurrence-info-body").html(occurrenceDetails);
            $("#hm-occurrence-info-view-btn").removeClass("disabled");
            // Animate opacity change
            $("#hm-occurrence-info-body").animate({"opacity":1.0}, 100);
            $("#hm-occurrence-info-indicator").animate({"opacity":0.0}, 200);
        },
        error: function(jqXHR, exception) {
            // Add error message to view
            $("#hm-occurrence-info-body").html("<br><br>Unable to fetch occurrence details and harvest summary at the moment.");
            $("#hm-occurrence-info-view-btn").addClass("disabled");
            // Animate opacity change
            $("#hm-occurrence-info-body").animate({"opacity":1.0}, 100);
            $("#hm-occurrence-info-indicator").animate({"opacity":0.0}, 200);
        }
    });
}

/**
 * Initializes all the tooltips in HM on page load
 */
function initializeTooltips() {
    // Add tooltip to the Personalize Grid Settings button
    $('button.btn-default').each(function(i, obj) {
        var title = $(this).attr('title');
        if(title == 'Personalize grid settings') {
            $(this).addClass('hm-tooltipped');
            $(this).attr('data-tooltip', title);
            $(this).attr('data-position', 'top');
            $(this).attr('title', '');
        }
    });

    // Initialize tooltip
    $('.hm-tooltipped').tooltip({
        trigger : 'hover'
    });
}

/**
 * Show or hide the page load indicator
 * @param {boolean} show set to true if loading indicator is to be displayed; otherwise, set to false
 */
function loadingIndicator(show) {
    if (show) {
        // Show loading indicator
        $("#system-loading-indicator").html(loading);
        $("#system-loading-indicator").show();
    }
    else {
        // Hide loading indicator
        $("#system-loading-indicator").html('');
        $("#system-loading-indicator").hide();
    }
}

/**
 * Displays custom flash message
 * @param {string} status the status of the message (info, success, warning, error)
 * @param {string} iconClass icon of flash message
 * @param {string} message the message to display
 * @param {string} parentDivId id of the div where the flash message is to be inserted
 * @param {float} time time in seconds before the flash message is removed from the display. 7.5 seconds by default.
 */
function displayCustomFlashMessage(status, iconClass, message, parentDivId, time = 7.5) {
    // Add flash message to parent div
    $('#' + parentDivId).html(
        '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
            '<div class="card-panel">' +
                '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                message +
                '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
            '</div>' +
        '</div>'
    );

    // remove flash message after the specified time in seconds (7.5 seconds by default)
    setTimeout(
        function() 
        {
            $('#w25-' + status + '-0').fadeOut(250);
            setTimeout(
                function() 
                {
                    $('#' + parentDivId).html('');
                }
            , 500);
        }
    , 1000 * time);
}

JS;

$this->registerJs($script);

// Register search_tool CSS
$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

Yii::$app->view->registerCss('
    .disabled{
        pointer-events: none;
    }

    .badge-step {
        background-color: #ee6e73;
        display: inline-block;
        font-size: 11.84px;
        font-weight: 700;
        margin-right: 5px;
        line-height: 14px;
        color: #fff;
        vertical-align: baseline;
        white-space: nowrap;
        text-shadow: 0 -1px 0 rgba(0,0,0,.25);
        -webkit-border-radius: 50%;
    }

    div#hm-occurrence-info-content {
        color: #333333;
        font-size: 12.4px;
        min-width: max-content !important;
        padding: 0px 0px 15px 0px;
        z-index:900;
        left: 237px;
    }

    div#hm-occurrence-info-content-inner {
        padding: 5px 15px 5px 15px;
    }

    div#hm-occurrence-info-body {
        overflow-y: auto;
    }

    div#hm-occurrence-info-btns {
        padding: 10px 0px 0px 0px;
    }

    div.hm-browser-title {
        background-color:white;
        padding-left:10px;
    }

    h3.hm-browser-title {
        margin:0px;
    }

    #dynagrid-harvest-manager-cross-crt-grid .kv-grid-wrapper {
        min-height: 350px;
    }

    #dynagrid-harvest-manager-plot-crt-grid .kv-grid-wrapper {
        min-height: 350px;
    }

    i.hm-occurrence-info {
        vertical-align: -18%;
        font-size: 18px;
    }

    i.hm-occurrence-info-btn {
        vertical-align: -13%;
        font-size: 18px;
    }

    i.hm-modal-icon {
        vertical-align: -20%;
    }

    i.hm-bg-info-icon {
        vertical-align: -10%;
    }

    .tab-content.printable {
        padding: 0px 0px 0px 10px;
    }

    #w8>li.active>a {
		background-color: #d9edf7 !important;
	}

    #w8>li>a.hm-unavailable {
		background-color: #dddddd !important;
	}
    .notification-badge {
        font-family: "Poppins", sans-serif;
        position: relative;
        right: 0px;
        top: -20px;
        color: #ffffff;
        background-color: #3f51b5;
        margin: 0 -.8em;
        border-radius: 50%;
        padding: 2px 5px;
    }

    #notifications-dropdown h5 {
        font-size: 1rem;
        text-transform: capitalize;
        font-weight: 500;
    }

    #notifications-dropdown li {
        padding: 8px 16px;
        font-size: 1rem;
    }

    #notifications-dropdown li > a {
        padding: 0;
        font-size: 1.1rem;
        font-weight: 300;
    }

    #notifications-dropdown li > a > span {
        display: inline-block;
        font-size: 1.2rem;
        position: relative;
        top: 4px;
        margin-right: 5px;
    }

    #notifications-dropdown li > time {
        font-size: 0.8rem;
        font-weight: 400;
        margin-left: 38px;
    }

    #notifications-dropdown li.divider {
        padding: 0;
    }
    .small {
        font-size: 1.0rem !important;
    }
    .icon-bg-circle {
        color: #fff;
        padding: .4rem;
        border-radius: 50%;
    }
    h6 {
        font-size: 1rem;
        line-height: 110%;
        margin: 0.5rem 0 0.4rem 0;
    }
    .round {
        display: inline-block;
        height: 30px;
        width: 30px;
        line-height: 30px;
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #222;    
        color: #FFF;
        text-align: center;  
    }
    body{
        overflow-x: hidden;
    }
    .observation-data-filter-badge {
        cursor: pointer;
    }

    .hm-hd-harvest-status-chip {
        margin-right: 5px;
        cursor: pointer;
    }

    .hm-harvest-status-failed {
        cursor: pointer;
    }
');

?>