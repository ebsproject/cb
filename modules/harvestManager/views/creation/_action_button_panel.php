<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders the action buttons in creation tab if two browsers are displayed
 */

?>

<div class="white hm-crt-action-btns-div row col-md-12">
    <div class=" action-btns-div">
        <div class="hm-panel-title row col-md-6">
            <h3 class="hm-panel-title">
                <?= \Yii::t('app', 'Package creation'); ?>
                <span style="cursor: default;text-decoration: none; background-color: transparent !important; padding: 0px 9px; height: 20px; line-height: 20px; text-transform: none; margin-left: 10px;font-size: .8rem;font-weight: bold;color: #9e9e9e;">Legend:</span>
                <span data-tooltip="Committed harvest data" style="cursor: pointer;" class="new badge light-green darken-4 hm-tooltipped" title=""><strong> <?= \Yii::t('app', 'COMMITTED') ?> </strong></span>
                <span data-tooltip="Uncommitted harvest data" style="cursor: pointer;" class="new badge amber grey-text text-darken-2 hm-tooltipped" title=""><strong> <?= \Yii::t('app', 'UNCOMMITTED') ?> </strong></span>
                <span data-tooltip="Missing required data" style="cursor: pointer;" class="new badge red lighten-5 red-text text-darken-3 hm-tooltipped" title=""><strong> <?= \Yii::t('app', 'MISSING') ?> </strong></span>
                <span data-tooltip="Not applicable or not required" style="cursor: pointer;" class="new badge grey lighten-2 grey-text text-darken-1 hm-tooltipped" title=""><strong> <?= \Yii::t('app', 'NA/NR') ?> </strong></span>
            </h3>
        </div>
        <div class="hm-panel-btns btn-toolbar kv-grid-toolbar toolbar-container">
        <?php
            echo $actionButtons;
        ?></div>
    </div>
</div>

<?php


$script = <<< JS

$("document").ready(function(){
    // ...
});

JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    div.hm-crt-action-btns-div {
        margin: 0px 0px 10px 0px;
        max-height: 50px;
    }

    div.action-btns-div {
        margin: 0px 0px 10px 0px;
    }

    div.hm-panel-title {
        padding: 10px 0px 10px 10px;
        float: left;
    }

    h3.hm-panel-title {
        margin: 0px 0px 0px 0px;
        display: inline;
    }

    div.hm-panel-btns {
        padding: 10px 0px 10px 10px;
        float: right;
    }

    div#hm-action-btns {
        float: right;
    }
');

?>