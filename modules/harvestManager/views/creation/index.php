<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders seed and package creation step for Harvest Manager
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;

// Define URLs
$urlSplitView = Url::to(['creation/index', 'program' => $program, 'occurrenceId' => $occurrenceId, 'source' => $dataLevel]);
$urlFocusPlotView = Url::to(['creation/index', 'program' => $program, 'occurrenceId' => $occurrenceId, 'source' => $dataLevel, 'view' => 'focus_plot']);
$urlFocusCrossView = Url::to(['creation/index', 'program' => $program, 'occurrenceId' => $occurrenceId, 'source' => $dataLevel, 'view' => 'focus_cross']);
$urlCheckHarvestJobs = Url::to(['creation/check-harvest-jobs']);
$urlGetOccurrenceStatus = Url::to(['creation/get-occurrence-status']);
$urlUpdateOccurrence = Url::to(['creation/update-occurrence']);
$urlCheckReadyRecords = Url::to(['creation/check-ready-records']);
$urlCreate = Url::to(['creation/create-packages']);
$urlCommitHarvestData = Url::to(['creation/commit-harvest-data']);
$newNotificationsUrl = Url::toRoute(['/harvestManager/harvest-data/new-notifications-count']);
$notificationsUrl = Url::toRoute(['/harvestManager/harvest-data/push-notifications']);
$urlManagementIndex = Url::to(['management/index', 'program' => $program, 'occurrenceId' => $occurrenceId, 'source' => $dataLevel]);
// Render harvest manager tabs
echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/tab_main.php',[
    'program' => $program,
    'occurrenceId' => $occurrenceId,
    'dataLevel' => $dataLevel,
    'experimentType' => $experimentType,
    'occurrenceName' => $occurrenceName,
    'cpnCheck' => $cpnCheck,
    'view' => $view,
    'browserId' => $browserId
]);

echo '<div id="hm-bg-alert-div"></div>';

// If two browsers are displayed, display action buttons panel
if($twoBrowsers) {
    // Render actions button panel
    echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/creation/_action_button_panel.php',[
        'actionButtons' => $actionButtons
    ]);
    // Render cross browser
    echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/creation/_cross_browser.php',[
        'occurrenceId' => $occurrenceId,
        'crossBrowserData' => $crossBrowserData,
        'crossSearchModel' => $crossSearchModel,
        'actionButtonsFocusMode' => $actionButtonsFocusMode,
        'browserDivSize' => $browserDivSize,
        'controllerModel' => $controllerModel,
        'twoBrowsers' => $twoBrowsers,
        'split' => $split,
        'focus' => $focus,
        'crossHidden' => $crossHidden,
        'browserConfig' => $browserConfig
    ]);
    // Render plot browser
    echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/creation/_plot_browser.php',[
        'plotBrowserData' => $plotBrowserData,
        'plotSearchModel' => $plotSearchModel,
        'twoBrowsers' => $twoBrowsers,
        'actionButtonsFocusMode' => $actionButtonsFocusMode,
        'browserDivSize' => $browserDivSize,
        'controllerModel' => $controllerModel,
        'split' => $split,
        'focus' => $focus,
        'plotHidden' => $plotHidden,
        'browserConfig' => $browserConfig,
        'stage' => $stage,
        'inputColumns' => $inputColumns
    ]);
}
else {
    if($plotHidden == '' && str_contains($crossHidden,'hidden')) {
        // Render plot browser
        echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/creation/_plot_browser.php',[
            'plotBrowserData' => $plotBrowserData,
            'plotSearchModel' => $plotSearchModel,
            'twoBrowsers' => $twoBrowsers,
            'actionButtonsFocusMode' => $actionButtonsFocusMode,
            'browserDivSize' => $browserDivSize,
            'controllerModel' => $controllerModel,
            'split' => $split,
            'focus' => $focus,
            'plotHidden' => $plotHidden,
            'browserConfig' => $browserConfig,
            'stage' => $stage,
            'inputColumns' => $inputColumns
        ]);
    }
    else if($crossHidden == '' && str_contains($plotHidden,'hidden')) {
        // Render cross browser
        echo Yii::$app->controller->renderPartial('@app/modules/harvestManager/views/creation/_cross_browser.php',[
            'occurrenceId' => $occurrenceId,
            'crossBrowserData' => $crossBrowserData,
            'crossSearchModel' => $crossSearchModel,
            'actionButtonsFocusMode' => $actionButtonsFocusMode,
            'browserDivSize' => $browserDivSize,
            'controllerModel' => $controllerModel,
            'twoBrowsers' => $twoBrowsers,
            'split' => $split,
            'focus' => $focus,
            'crossHidden' => $crossHidden,
            'browserConfig' => $browserConfig,
            'inputColumns' => $inputColumns
        ]);
    }
}

// MODALS
// Modal for harvest record creation
Modal::begin([
    'id' => 'hm-harvest-record-creation-modal',
    'header' => '<h4><i class="material-icons hm-modal-icon">info_outline</i> Create Harvest Records</h4>',
    'footer' =>
    Html::a(\Yii::t('app','Cancel'), ['#'], ['class'=>'hm-modal-close-btn', 'id'=>'hm-cancel-creation-btn', 'data-dismiss'=>'modal']). '&emsp;'
    . Html::a(\Yii::t('app', 'Proceed'), '#', ['class' => 'btn btn-primary waves-effect waves-light hm-modal-close-btn', 'id' => 'hm-proceed-creation-btn']). '&emsp;',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 1040;']
]);
echo '<div id="hm-harvest-record-creation-modal-body"></div>';
Modal::end();

//Modal for committing harvest data
Modal::begin([
    'id' => 'hm-harvest-data-commit-modal',
    'header' => '<h4><i class="material-icons hm-modal-icon">check</i>'. \Yii::t('app','Commit Harvest Data').'</h4>',
    'footer' => Html::a(\Yii::t('app','Close'), ['#'], ['class'=>'btn btn-primary waves-effect waves-light modal-close hm-modal-close-btn', 'id'=>'hm-cancel-commit-btn', 'data-dismiss'=>'modal']). '&emsp;',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 1040;']
]);
echo '<div id="hm-harvest-data-commit-modal-body"></div>';
Modal::end();

//Modal for completing harvest for the current occurrence
Modal::begin([
    'id' => 'hm-complete-harvest-modal',
    'header' => '<h4><i class="material-icons hm-modal-icon">check</i>'. \Yii::t('app','Complete Harvest').'</h4>',
    'footer' => Html::a(\Yii::t('app','Cancel'), ['#'],
        [
            'id'=>'hm-hd-bd-confirmation-cancel-btn',
            'class' => 'modal-close hm-hd-bd-confirmation-default-grp',
            'data-dismiss'=>'modal'
        ]
    ). '&emsp;' .
    Html::a(\Yii::t('app','Proceed'), ['#'], 
        [
            'class'=>'btn btn-primary waves-effect waves-light hm-modal-complete-btn disabled', 
            'id'=>'hm-crt-complete-harvest-proceed-btn'
            ]
        ). '&emsp;',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 1040;']
]);
echo  '<div id="hm-complete-harvest-modal-progress"><div class="progress"><div class="indeterminate"></div></div></div>'
. '<div id="hm-complete-harvest-modal-body" class="hidden">' 
. '<div id="hm-complete-harvest-modal-info-div"></div>'
. '<div id="hm-complete-harvest-modal-remarks-div">'
. '<label data-label="Occurrence remarks" style="margin-top:10px;" for="OCCURRENCE_REMARKS">Occurrence remarks <span class="required">*</span></label>'
. '<textarea id="hm-complete-harvest-modal-remarks-textarea" name="hm-complete-harvest-modal-remarks-textarea" style="background-color:#ffffff;min-height:150px;" rows="4" cols="50"></textarea>'
. '</div>'
. '</div>';
Modal::end();
?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<?php
// Convert boolean to string equivalent to allow for use in jQuery
$reloadPlots = $reloadPlots ? 'true' : 'false';
$reloadCrosses = $reloadCrosses ? 'true' : 'false';

$script = <<< JS
var occurrenceId = $occurrenceId;
var reloadPlots = $reloadPlots;
var reloadCrosses = $reloadCrosses;
var newNotificationsUrl='$newNotificationsUrl';
var notificationsUrl='$notificationsUrl';
var urlManagementIndex = '$urlManagementIndex';
var occurrenceStatus = null;
var occurrenceRemarks = null;

notifDropdown();
renderNotifications();
updateNotif();
displayFlashMessage();

// Check for new notifications every 7.5 seconds
notificationInterval=setInterval(updateNotif, 7500);

$(document).on('ready pjax:success', function(e) {
    e.preventDefault();
    $('#notif-btn-id').tooltip();
    notifDropdown();
    $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>');
    renderNotifications();
    updateNotif();
});

$("document").ready(function(){ 
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();

    // Adjust creation browser height
    adjustCreationBrowsers();
    resize();

    // Commit button click event
    $(document).on("click","#hm-crt-commit-data-btn", function(e) {
        e.preventDefault();
        processStart = new Date().getTime();

        // Add loading indicator
        $('#hm-cancel-commit-btn').hide();
        $('#hm-harvest-data-commit-modal-body').html('<p><i>Committing...</i></p><div class="progress"><div class="indeterminate"></div></div>');
        
        // Commit harvest data
        $.ajax({
            url: '$urlCommitHarvestData',
            data: {
                occurrenceId: occurrenceId
            },
            type: 'POST',
            success: function(data) {
                var dataArray = jQuery.parseJSON(data);
                if(dataArray.hasDataset == false) {
                    $('#hm-cancel-commit-btn').show();
                    $('#hm-harvest-data-commit-modal-body').html('<p style="font-size:115%;">There are no harvest data to commit.</p>');
                }
                else if(dataArray['response']['success']) {
                    // Calculate total time consumed
                    var commitExecutionTimeMillis = new Date().getTime() - processStart;
                    $("#hm-harvest-data-commit-modal").modal('hide');
                    var notif = "<i class='material-icons green-text left'>check</i>Harvest data was successfully committed. (" + (commitExecutionTimeMillis*.001).toFixed(3) + " s)";
                    Materialize.toast(notif, 3000);
                    reloadBrowsers();
                }
                else {
                    $('#hm-cancel-commit-btn').show();
                    $('#hm-harvest-data-commit-modal-body').html('<p style="font-size:115%;">There was a problem while committing the harvest data. Please try again.</p>');
                }
            },
            error: function(xhr, status, error) {
                $('#hm-cancel-commit-btn').show();
                $('#hm-harvest-data-commit-modal-body').html('<p style="font-size:115%;">Unable to commit harvest data at this time. Please try again later.</p>');
            }
        });

    });


    // Notifs button click event
    $(document).on("click","#hm-crt-notifs-btn", function(e) {
        e.preventDefault();
    });

    // Split view button click event
    $(document).on("click","#hm-crt-split-view-btn", function(e) {
        e.preventDefault();
        window.location = '$urlSplitView';
    });

    // Focus on crosses button click event
    $(document).on("click","#hm-crt-cross-focused-view-btn", function(e) {
        e.preventDefault();
        window.location = '$urlFocusCrossView';
    });

    // Focus on plots button click event
    $(document).on("click","#hm-crt-plot-focused-view-btn", function(e) {
        e.preventDefault();
        window.location = '$urlFocusPlotView';
    });

    // Complete harvest button
    $(document).on("click","#hm-crt-complete-harvest-btn", function(e) {
        // Hide/show modal contents
        $('#hm-complete-harvest-modal-progress').removeClass('hidden');
        $('#hm-complete-harvest-modal-body').addClass('hidden');
        $('#hm-crt-complete-harvest-proceed-btn').addClass('disabled');
        $("#hm-complete-harvest-modal-remarks-textarea").val('');
        // Check occurrence status, and update modal title and message
        $.ajax({
            url: '$urlGetOccurrenceStatus',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId
            },
            success: function(response) {
                occurrenceStatus = response;
                var hasHarvestCompletedStatus = occurrenceStatus.includes("harvest completed");

                var message = 'You are about to mark this occurrence with the "Harvest Completed" status. If you wish to proceed, please enter your remarks below and click "Proceed".';
                var modalTitle = 'Complete Harvest';
                var modalIcon = 'check';
                if (hasHarvestCompletedStatus) {
                    message = 'You are about to undo the "Harvest Completed" status of this occurrence. If you wish to proceed, please enter your remarks below and click "Proceed".';
                    modalTitle = 'Undo Complete Harvest';
                    modalIcon = 'undo';
                }

                // Modify the header of the modal
                $('#hm-complete-harvest-modal .modal-header h4').html('<h4><i class="material-icons hm-modal-icon">' + modalIcon + '</i> ' + modalTitle +'</h4>');
                // Modify modal info message
                $('#hm-complete-harvest-modal-info-div').html(message);
                // Hide/show modal contents
                $('#hm-complete-harvest-modal-progress').addClass('hidden');
                $('#hm-complete-harvest-modal-body').removeClass('hidden');
            }
        });
    });

    // Proceed complete harvest button
    $(document).on("click","#hm-crt-complete-harvest-proceed-btn", function(e) {
        e.preventDefault();
        // Hide/show modal contents
        $('#hm-complete-harvest-modal-progress').removeClass('hidden');
        $('#hm-complete-harvest-modal-body').addClass('hidden');
        $('#hm-crt-complete-harvest-proceed-btn').addClass('disabled');

        // Update occurrence status and remarks
        $.ajax({
            url: '$urlUpdateOccurrence',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId,
                occurrenceStatus: occurrenceStatus,
                occurrenceRemarks: occurrenceRemarks
            },
            success: function(response) {
                var notif = "<i class='material-icons green-text'>check</i>&nbsp;Occurrence status was successfully updated!";
                Materialize.toast(notif, 2500);
                $('#hm-crt-reset-plot-btn').click();
                // Close modal
                $("#hm-complete-harvest-modal.in").modal('hide');
            }
        });
    });

    
    // Complete harvest remarks input
    $(document).on("input","#hm-complete-harvest-modal-remarks-textarea", function(e) {
        var currentValue = $(this).val();

        if (currentValue != null && currentValue != "") {
            $('#hm-crt-complete-harvest-proceed-btn').removeClass("disabled");
        }
        else $('#hm-crt-complete-harvest-proceed-btn').addClass("disabled");

        occurrenceRemarks = currentValue;
    });

    // Create package button click event
    $(document).on("click","#hm-crt-create-package-btn", function(e) {
        e.preventDefault();

        // Disable button and add loading indicator
        $('#hm-proceed-creation-btn').addClass('disabled');
        $('#hm-harvest-record-creation-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        // Check if the occurrence has an ongoing harvest background job
        $.ajax({
            url: '$urlCheckHarvestJobs',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId
            },
            success: function(response) {
                // If response is null, warn user
                if (response == "") {
                    // handle error
                    $('#hm-proceed-creation-btn').addClass('disabled');
                    $('#hm-harvest-record-creation-modal-body').html('<p style="font-size:115%;">Unable to create harvest records at this time. Please try again later.</p>');
                    return
                }

                var jobCount = parseInt(response);
                // If jobCount is not 0, warn user that 
                // the occurrence has an ongoing job.
                if (jobCount > 0) {
                    var message = "This occurrence has an ongoing harvest job. Please wait for the current harvest to be finished before starting another harvest job. You may contact your system administrator if this is incorrect.";
                    $('#hm-proceed-creation-btn').addClass('disabled');
                    $('#hm-harvest-record-creation-modal-body').html('<p style="font-size:115%;">' + message + '</p>');
                    return
                }

                // Check for ready plots/crosses
                $.ajax({
                    url: '$urlCheckReadyRecords',
                    type: 'post',
                    datatType: 'json',
                    cache: false,
                    data: {
                        occurrenceId: occurrenceId
                    },
                    success: function(response) {
                        var data = JSON.parse(response);
                        // Render modal contents
                        $('#hm-harvest-record-creation-modal-body').html(data.html);
                        // Enable proceed button
                        if(data.totalReady > 0) $('#hm-proceed-creation-btn').removeClass('disabled');
                    },
                    error: function(xhr, status, error) {
                        $('#hm-proceed-creation-btn').addClass('disabled');
                        $('#hm-harvest-record-creation-modal-body').html('<p style="font-size:115%;">Unable to create harvest records at this time. Please try again later.</p>');
                    }
                });
            },
            error: function(xhr, status, error) {
                $('#hm-proceed-creation-btn').addClass('disabled');
                $('#hm-harvest-record-creation-modal-body').html('<p style="font-size:115%;">Unable to create harvest records at this time. Please try again later.</p>');
            }
        });
    });

    // Proceed with creation button click event
    $(document).on("click","#hm-proceed-creation-btn", function(e) {
        // Add loading indicator
        $('#hm-harvest-record-creation-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: '$urlCreate',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                occurrenceId: occurrenceId
            },
            success: function(response) {
                var result = JSON.parse(response);
                var success = result.success;
                var status = result.status;
                
                // Display flash message
                if(success) {
                    // Success message
                    displayCustomFlashMessage(
                        'info', 
                        'fa fa-info-circle',
                        'The harvest records (packages, seeds, germplasm, and related records) are being created in the background. You may proceed with other tasks. You will be notified in this page once done.',
                        'hm-bg-alert-div',
                        10
                    );
                }
                else {
                    // Failure message
                    let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status != 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if(status == null) message += ' (#002)';

                    displayCustomFlashMessage(
                        'warning', 
                        'fa-fw fa fa-warning', 
                        message,
                        'hm-bg-alert-div'
                    );
                }

                updateNotif();

                // Close the modal
                $("#hm-harvest-record-creation-modal.in").modal('hide');
                // Reload browsers
                reloadBrowsers()
                return;
            },
            error: function(xhr, status, error) {
                $('#hm-proceed-creation-btn').addClass('disabled');
                $('#hm-harvest-record-creation-modal-body').html('<p style="font-size:115%;">Unable to create harvest records at this time. Please try again later.</p>');
            }
        });
    });
});

/**
 * Perform browser refresh after commit/creation
 */
function reloadBrowsers() {
    // If two browsers are displayed, reload both browsers
    if(reloadCrosses && reloadPlots) {
        localStorage.setItem(occurrenceId+"_reload-plots", true)
        $('#hm-crt-reset-cross-btn').click()
    }
    // If cross browser is displayed, reload only the cross browser
    else if(reloadCrosses) {
        localStorage.setItem(occurrenceId+"_reload-plots", false)
        $('#hm-crt-reset-cross-btn').click()
    }
    // If plot browser is displayed, reload only the plot browser
    else if(reloadPlots) {
        $('#hm-crt-reset-plot-btn').click()
    }
}

/**
 * Shows new notifications
 */
function updateNotif() {
    $.getJSON(newNotificationsUrl, 
    {
        workerName: 'GenerateHarvestRecords'
    },
    function(json){
        $('#notif-badge-id').html(json);
        if(parseInt(json)>0){
            $('#notif-badge-id').addClass('notification-badge red accent-2');
        }else{
            $('#notif-badge-id').html('');
            $('#notif-badge-id').removeClass('notification-badge red accent-2');
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Message: "+textStatus+" : "+errorThrown);
        
    });
}

/**
 * Initialize dropdown materialize  for notifications
 */
function renderNotifications(){
    $('#hm-crt-notifs-btn').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: false,
        gutter: 0,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });
}

/**
 * Displays the notification in dropdown UI
 */
function notifDropdown(){
    $('#hm-crt-notifs-btn').on('click',function(e){
        $.ajax({
            url: notificationsUrl,
            type: 'post',
            data: {
                workerName: 'GenerateHarvestRecords'
            },
            async:false,
            success: function(data) {
                var pieces = data.split('||');
                var height = pieces[1];
                $('<style>#notifications-dropdown.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');

                data = pieces[0];

                renderNotifications();
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
                $('#notifications-dropdown').html(data);

                $('.bg_notif-btn').on('click', function(e){
                    var notifHTML = $(this).html();

                    // If background job has error, redirect to harvest data tab
                    if(notifHTML.includes('priority_high')) {
                        $('#harvest-data-tab').click();
                    }
                    // Else, redirect to the management tab
                    else {
                        window.location = urlManagementIndex; 
                    }
                });
            },
            error: function(xhr, status, error) {
                var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');
                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();
            }
        });
    });
}

/**
 * Set minimum height of the browser body container
 */
function adjustCreationBrowsers() {
    $('#dynagrid-harvest-manager-plot-crt-grid-table-con-container').css('min-height','450px');
    $('#dynagrid-harvest-manager-cross-crt-grid-table-con-container').css('min-height','450px');
}

/**
 * Resize the package grid table upon loading
 */
function resize(){
    var maxHeight = ($(window).height() - 360);
    $(".kv-grid-wrapper").css("height",maxHeight);
}

/**
 * Display flash message from additional harvest operation
 */
function displayFlashMessage() {
    var flashMessage = JSON.parse(localStorage.getItem('hm-addl-harvest-' + occurrenceId));

    if (flashMessage !== null && flashMessage !== undefined) {
        $('#hm-crt-notifs-btn').click();
        var status = flashMessage.status;
        var iconClass = flashMessage.iconClass;
        var message = flashMessage.message;
        var parentDivId = flashMessage.parentDivId;
        var time = flashMessage.time;
        // Display message
        displayCustomFlashMessage(
            status,
            iconClass,
            message,
            parentDivId,
            time
        );
        // Remove from local storage
        localStorage.setItem('hm-addl-harvest-' + occurrenceId, null);
    }
}

JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    div.hm-crt-cross-browser-div {
        min-height: 500px;
    }

    div.hm-crt-plot-browser-div {
        min-height: 500px;
    }
    
    .legend-btn{
        text-decoration: none; 
        color: #fff; 
        padding: 0px 9px; 
        height: 25px; 
        line-height: 25px; 
        text-transform: none;
    }
');

?>