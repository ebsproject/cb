<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /**
  * Renders view for creation summary
  */

use kartik\grid\GridView;

// If there are no records ready for harvest, inform user
if($totalReady == 0) {
    $records = $harvestCross ? "plots or crosses" : "plots";
    echo "<p style='font-size:115%;'><em>" . \Yii::t('app', "There are no $records ready for harvest. Please input and commit harvest data, and try again.</em>") . "</p>";
}
// If there are records ready for harvest, display the summary of materials to be harvested
else { 
    // Define grid columns
    $columns = [
        [
            'attribute' => 'dataLevel',
            'label' =>  \Yii::t('app', 'Level'),
            'format' => 'raw',
            'hAlign' => 'center',
        ],
        [
            'attribute' => 'count',
            'label' =>  \Yii::t('app', 'Ready for harvest'),
            'format' => 'raw',
            'hAlign' => 'center',
            'value' => function ($model) {
                $count = $model['count'];
                $dataLevel = $count != 1 ? $model['plural'] : $model['dataLevel'];
                $value = strtoupper("$count $dataLevel");
                return '
                    <span class="new badge light-green darken-3">' .
                        '<strong>' . \Yii::t('app', $value) . '</strong>' .
                    '</span>';
            }
        ]
    ];
    
    echo "<p style='font-size:115%;'>" . \Yii::t('app', 'Packages, seeds, germplasm, and related records will be created for the following:') . "</p>";

    // Render grid
    echo $grid = GridView::widget([
        'pjax' => true,
        'dataProvider' => $dataProvider,
        'id' => 'hm-harvest-record-creation-summary-table',
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
        'striped'=>false,
        'columns' => $columns,
        'toggleData'=>true,
        'layout' => '{items}{pager}',
    ]);

    echo "<p style='font-size:115%;'>" . \Yii::t('app', 'Do you wish to proceed with the creation?') . "</p>";
}