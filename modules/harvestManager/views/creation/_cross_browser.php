<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\GermplasmInfo;

/**
 * Renders the cross browser in the creation tab
 */

echo '<div class="hm-crt-cross-browser-div row col-md-' . $browserDivSize . $crossHidden . '" style="margin-bottom:0px;">' .
        '<div id="hm-crt-cross" class = "row col col-sm-12 col-md-12 col-lg-12" style = "margin-bottom:0px; padding:0px">';

$searchModel = $crossSearchModel;
$dataProvider = $crossBrowserData['dataProvider'];

$browserId = 'dynagrid-harvest-manager-cross-crt-grid';

// Set button style
$btnStyle = '';
if($twoBrowsers && $focus == '' && $split == '') {
    $btnStyle = 'margin-left:10px;';
}
if(!$twoBrowsers) {
    // Add browser header
    echo '<div class="hm-browser-title">' .
        '<h3 class="hm-browser-title">' . \Yii::t('app', 'Package creation') . ' ' .
            '<span style="cursor: default;text-decoration: none; background-color: transparent !important; padding: 0px 9px; height: 20px; line-height: 20px; text-transform: none; margin-left: 10px;font-size: .8rem;font-weight: bold;color: #9e9e9e;">Legend:</span> ' .
            '<span data-tooltip="Committed harvest data" style="cursor: pointer;" class="new badge light-green darken-4 hm-tooltipped" title=""><strong>' . \Yii::t('app', 'COMMITTED') . '</strong></span> ' . 
            '<span data-tooltip="Uncommitted harvest data" style="cursor: pointer;" class="new badge amber grey-text text-darken-2 hm-tooltipped" title=""><strong>'. \Yii::t('app', 'UNCOMMITTED') . '</strong></span> ' .
            '<span data-tooltip="Missing required data" style="cursor: pointer;" class="new badge red lighten-5 red-text text-darken-3 hm-tooltipped" title=""><strong>' . \Yii::t('app', 'MISSING') . '</strong></span> ' .
            '<span data-tooltip="Not applicable or not required" style="cursor: pointer;" class="new badge grey lighten-2 grey-text text-darken-1 hm-tooltipped" title=""><strong>' . \Yii::t('app', 'NA/NR') . '</strong></span>' .
        '</h3>' .
    '</div>';
}

// Browser panel configuration
$panel = [
    'heading'=>false,
    'before' => \Yii::t('app', 'Review cross harvest data here.') . ' {summary}<br/>',
    'after'=> false,
];

$floatHeader = true;

// Define action columns
$actionColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ]
];
$harvestRemarks = [
    'attribute' => 'harvestRemarks',
    'label' => 'Harvest Remarks',
    'hAlign' => 'left',
    'vAlign' => 'top',
    'format' => 'raw',
    'mergeHeader' => true,
    'headerOptions'=>[
        'style' => 'text-align:left'
    ],
];

$columns = [
    $harvestRemarks,
    [
        'attribute' => 'crossName',
        'format' => 'raw',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'contentOptions' => [
            "class" => "germplasm-name-col"
            
        ],
    ],
    [
        'attribute' => 'crossMethod',
        'format' => 'raw',
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'value' => function($model){
            return !empty($model['crossMethod']) ? $model['crossMethod'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossFemaleParent',
        'label' => '<span title="Female Parent"><i class="fa fa-venus"></i>'.Yii::t('app', ' Female Parent').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>[
            'class'=>'red lighten-4',
            'style'=>'min-width:130px !important',
        ],
        'contentOptions'=>['class'=>'germplasm-name-col red lighten-5'],
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'value' => function($model) {
            $germplasmDbId = $model['femaleGermplasmDbId'];
            $designation = $model['crossFemaleParent'];

            if(empty($germplasmDbId)) return '<span class="not-set">(not set)</span>';
            
            $linkAttrs = [
                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                'class' => 'blue-text text-darken-4 header-highlight',
                'title' => \Yii::t('app', 'Click to view more information'),
                'data-label' => $designation,
                'data-id' => $germplasmDbId,
                'data-target' => '#view-germplasm-widget-modal',
                'data-toggle' => 'modal'
            ];

            $widgetName = GermplasmInfo::widget([
                'id' => $germplasmDbId,
                'entityLabel' => $designation,
                'linkAttrs' => $linkAttrs
            ]);
            return $widgetName;
        }
    ],
    [
        'attribute' => 'femaleParentage',
        'label' => '<span title="Female Parentage"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Parentage').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'
        ],
        'contentOptions'=>['class'=>'germplasm-name-col red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'visible'=>false,
        'mergeHeader' => true,
        'value' => function($model){
            return !empty($model['femaleParentage']) ? '<div style="overflow-wrap: break-word;max-width:200px">'.$model['femaleParentage'].'</div>' : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossMaleParent',
        'label' => '<span title="Male Seed Source"><i class="fa fa-mars"></i>'.Yii::t('app', ' Male Parent').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>[
            'class'=>'blue lighten-4',
            'style'=>'min-width:120px !important',
        ],
        'contentOptions'=>['class'=>'germplasm-name-col blue lighten-5'],
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'value' => function($model) {
            $germplasmDbId = $model['maleGermplasmDbId'];
            $designation = $model['crossMaleParent'];

            if(empty($germplasmDbId)) return '<span class="not-set">(not set)</span>';
            
            $linkAttrs = [
                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                'class' => 'blue-text text-darken-4 header-highlight',
                'title' => \Yii::t('app', 'Click to view more information'),
                'data-label' => $designation,
                'data-id' => $germplasmDbId,
                'data-target' => '#view-germplasm-widget-modal',
                'data-toggle' => 'modal'
            ];

            $widgetName = GermplasmInfo::widget([
                'id' => $germplasmDbId,
                'entityLabel' => $designation,
                'linkAttrs' => $linkAttrs
            ]);
            return $widgetName;
        }
    ],
    [
        'attribute' => 'maleParentage',
        'label' => '<span title="Male Parentage"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Parentage').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['maleParentage']) ? '<div style="overflow-wrap: break-word;max-width:200px">'.$model['maleParentage'].'</div>' : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleEntryNumber',
        'label' => '<span title="Female Entry No."><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Entry No.').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['femaleEntryNumber']) ? $model['femaleEntryNumber'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleEntryCode',
        'label' => '<span title="Female Entry Code"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Entry Code').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['femaleEntryCode']) ? $model['femaleEntryCode'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleSourceSeedEntry',
        'label' => '<span title="Female Source Seed Entry"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Source Seed Entry').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['femaleSourceSeedEntry']) ? $model['femaleSourceSeedEntry'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleEntryNumber',
        'label' => '<span title="Male Entry No."><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Entry No.').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['maleEntryNumber']) ? $model['maleEntryNumber'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleEntryCode',
        'label' => '<span title="Male Entry Code"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Entry Code').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['maleEntryCode']) ? $model['maleEntryCode'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleSourceSeedEntry',
        'label' => '<span title="Male Source Seed Entry"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Source Seed Entry').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['maleSourceSeedEntry']) ? $model['maleSourceSeedEntry'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleParentSeedSource',
        'label' => '<span title="Female Seed Source"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Seed Source').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['femaleParentSeedSource']) ? $model['femaleParentSeedSource'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleParentSeedSource',
        'label' => '<span title="Male Seed Source"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Seed Source').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col blue lighten-5'],
        'encodeLabel' => false,
        'visible'=>false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'value' => function($model){
            return !empty($model['maleParentSeedSource']) ? $model['maleParentSeedSource'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleSourcePlot',
        'label' => '<span title="Female Source Plot"><i class="fa fa-venus"></i> '.Yii::t('app', 'Female Source Plot').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col red lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['femaleSourcePlot']) ? $model['femaleSourcePlot'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleSourcePlot',
        'label' => '<span title="Male Source Plot"><i class="fa fa-mars"></i> '.Yii::t('app', 'Male Source Plot').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'germplasm-name-col blue lighten-5'],
        'encodeLabel' => false,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible'=>false,
        'value' => function($model){
            return !empty($model['maleSourcePlot']) ? $model['maleSourcePlot'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'remarks',
        'label' => 'Cross Remarks',
        'format' => 'raw',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'contentOptions' => [
            'class' => 'germplasm-name-col'
        ],
        'visible'=>false,
        'value' => function($model){
            return !empty($model['remarks']) ? $model['remarks'] : '<span class="not-set">(not set)</span>';
        }
    ]
];

$variableColumns = [];

// Merge all column configurations
$gridColumns = array_merge($actionColumns, $columns, $variableColumns, $inputColumns);

// Render grid
$dynagrid = DynaGrid::begin([
    'columns'=>$gridColumns,
    'storage' => DynaGrid::TYPE_SESSION,
    'theme'=>'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'tableOptions'=>['class'=>$browserId.'-table'],
        'options'=>['id'=>$browserId.'-table-con'],
        'id' => $browserId.'-table-con',
        'striped'=>false,
        'responsive' => true,
        'responsiveWrap' => false,
        'floatHeader' => $floatHeader,
        'floatHeaderOptions' => [
            'scrollingTop' => '0',
            'position' => 'absolute',
        ],
        'hover' => true,
        'floatOverflowContainer' => true,
        'showPageSummary' => false,
        'summaryOptions' => ['class' => 'pull-right'],
        'pjax' => true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => $browserId, 'enablePushState' => false,],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'rowOptions'=>function($model){
            $valid = $model['harvestDataValid'];
            if($valid)
                return [];
            else
                return ['class' => 'danger'];
        },
        'panel'=> $panel,
        'toolbar' =>  [
            [
                'content' => $actionButtonsFocusMode
            ],
            [
                'content' =>
                    Html::a(
                        '<i class="material-icons">filter_center_focus</i>',
                        '#',
                        [
                            'class' => 'btn btn-default hm-tooltipped' . $focus,
                            'id' => 'hm-crt-cross-focused-view-btn',
                            'style' => "margin-left:10px;",
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Focus on crosses')
                        ]
                    )
                    .Html::a(
                        '<i class="material-icons">swap_horiz</i>',
                        '#',
                        [
                            'class' => 'btn btn-default hm-tooltipped' . $split,
                            'id' => 'hm-crt-plot-focused-view-btn',
                            'style' => "margin-left:10px;",
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Switch focus to plots'),
                        ]
                    )
                    .Html::a(
                        '<i class="material-icons" style="transform: rotate(90deg);">splitscreen</i>',
                        '#',
                        [
                            'class' => 'btn btn-default hm-tooltipped' . $split,
                            'id' => 'hm-crt-split-view-btn',
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Split view')
                        ]
                    )
                    .Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [Yii::$app->controller->action->id],
                        [
                            'class' => 'btn btn-default hm-tooltipped',
                            'id' => 'hm-crt-reset-cross-btn',
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    )
                    .'{dynagridFilter}{dynagridSort}{dynagrid}'

            ],
        ],
    ],
    'options'=>['id'=>$browserId]
]);

DynaGrid::end();

echo '</div></div>';

$script = <<< JS
var occurrenceId = $occurrenceId;
var counter = 0;

$("document").ready(function(){
    // Reset browser button click event
    $(document).on("click","#hm-crt-reset-cross-btn", function(e) {
        e.preventDefault();
        var pageurl = $('#$browserId li.active a').attr('href');
        $.pjax.reload(
            {
                container:'#$browserId-pjax',
                url: pageurl,
                replace: false,
                type: 'POST',
                data: {
                    forceReset:true,
                    target:'$browserId-pjax'
                }
            }
        );
    });

    // Perform after pjax completion of browser
    $(document).on('ready pjax:complete','#$browserId-pjax', function(e) {
        // Initialize tooltip
        initializeTooltips();
        // Adjust creation browser height
        adjustCreationBrowsers();
        resize();

        // Check if plot browser will be reloaded
        var reloadPlots = localStorage.getItem(occurrenceId+"_reload-plots");
        if(reloadPlots == 'true') {
            // If counter is 0, do not refresh. Increment by 1.
            if (counter == 1) counter ++;
            // If counter is 1, reset plot browser. Set counter back to 0.
            else {
                counter = 0
                localStorage.setItem(occurrenceId+"_reload-plots", false)
                $('#hm-crt-reset-plot-btn').click();
            }
        }

    });
});

JS;

$this->registerJs($script);

// Base css
$css = '
    div.hm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.hm-browser-title {
        display: inline;
    }
';

// If only one browser is displayed,
// remove padding from right side.
if(!$twoBrowsers) {
    $css .= '
        div.hm-crt-cross-browser-div {
            padding-right:0px
        }
    ';
}

Yii::$app->view->registerCss($css);

?>