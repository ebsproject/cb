<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use app\widgets\GermplasmInfo;

/**
 * Renders the plot browser in the creation tab
 */

echo '<div class="hm-crt-plot-browser-div row col-md-' . $browserDivSize . $plotHidden . '" style="margin-bottom:0px;">' . 
        '<div id="hm-crt-plot" class = "row col col-sm-12 col-md-12 col-lg-12" style = "margin-bottom:0px; padding:0px">';

// Unpack browser data
$searchModel = $plotSearchModel;
$dataProvider = $plotBrowserData['dataProvider'];

$browserId = 'dynagrid-harvest-manager-plot-crt-grid';

// Set button style
$btnStyle = '';
if($twoBrowsers && $focus == '' && $split == '') {
    $btnStyle = 'margin-left:10px;';
}
if(!$twoBrowsers) {
    // Add browser header
    echo '<div class="hm-browser-title">' .
        '<h3 class="hm-browser-title">' . \Yii::t('app', 'Package creation') . ' ' .
            '<span style="cursor: default;text-decoration: none; background-color: transparent !important; padding: 0px 9px; height: 20px; line-height: 20px; text-transform: none; margin-left: 10px;font-size: .8rem;font-weight: bold;color: #9e9e9e;">Legend:</span> ' .
            '<span data-tooltip="Committed harvest data" style="cursor: pointer;" class="new badge light-green darken-4 hm-tooltipped" title=""><strong>' . \Yii::t('app', 'COMMITTED') . '</strong></span> ' . 
            '<span data-tooltip="Uncommitted harvest data" style="cursor: pointer;" class="new badge amber grey-text text-darken-2 hm-tooltipped" title=""><strong>'. \Yii::t('app', 'UNCOMMITTED') . '</strong></span> ' .
            '<span data-tooltip="Missing required data" style="cursor: pointer;" class="new badge red lighten-5 red-text text-darken-3 hm-tooltipped" title=""><strong>' . \Yii::t('app', 'MISSING') . '</strong></span> ' .
            '<span data-tooltip="Not applicable or not required" style="cursor: pointer;" class="new badge grey lighten-2 grey-text text-darken-1 hm-tooltipped" title=""><strong>' . \Yii::t('app', 'NA/NR') . '</strong></span>' .
        '</h3>' .
    '</div>';
}

$floatHeader = true;

// Browser panel configuration
$panel = [
    'heading'=>false,
    'before' => \Yii::t('app', 'Review plot harvest data here.') . ' {summary}<br/>',
    'after'=> false,
];

// Define action columns
$actionColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ]
];
$harvestRemarks = [
    'attribute' => 'harvestRemarks',
    'label' => 'Harvest Remarks',
    'hAlign' => 'left',
    'vAlign' => 'top',
    'format' => 'raw',
    'mergeHeader' => true,
    'headerOptions'=>[
        'style' => 'text-align:left'
    ],
];

$columns = [
    $harvestRemarks,
    [
        'attribute' => 'plotNumber',
        'label' => 'Plot No.',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
    ],
    [
        'attribute' => 'designation',
        'label' => 'Germplasm',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'format' => 'raw',
        'contentOptions' => [
            "class" => "germplasm-name-col"
        ],
        'value' => function($model) {
            $germplasmDbId = $model['germplasmDbId'];
            $designation = $model['designation'];
            
            $linkAttrs = [
                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                'class' => 'blue-text text-darken-4 header-highlight',
                'title' => \Yii::t('app', 'Click to view more information'),
                'data-label' => $designation,
                'data-id' => $germplasmDbId,
                'data-target' => '#view-germplasm-widget-modal',
                'data-toggle' => 'modal'
            ];

            $widgetName = GermplasmInfo::widget([
                'id' => $germplasmDbId,
                'entityLabel' => $designation,
                'linkAttrs' => $linkAttrs
            ]);
            return $widgetName;
        }
    ],
    [
        'attribute' => 'entryNumber',
        'label' => 'Entry Number',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
    ],
    [
        'attribute' => 'rep',
        'label' => 'Rep',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
    ],
    [
        'attribute' => 'state',
        'label' => 'Germplasm State',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'visible' => true
    ],
    [
        'attribute' => 'parentage',
        'label' => 'Parentage',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'format' => 'raw',
        'contentOptions' => [
            "class" => "germplasm-name-col"
        ],
    ],
    [
        'attribute' => 'plotCode',
        'label' => 'Plot Code',
        'hAlign' => 'left',
        'vAlign' => 'top',
        'mergeHeader' => true,
    ],
];

$variableColumns = [];

// Merge all column configurations
$gridColumns = array_merge($actionColumns, $columns, $variableColumns, $inputColumns);

// Render grid
$dynagrid = DynaGrid::begin([
    'columns'=>$gridColumns,
    'storage' => DynaGrid::TYPE_SESSION,
    'theme'=>'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'tableOptions'=>['class'=>$browserId.'-table'],
        'options'=>['id'=>$browserId.'-table-con'],
        'id' => $browserId.'-table-con',
        'striped'=>false,
        'responsive' => true,
        'responsiveWrap' => false,
        'floatHeader' => $floatHeader,
        'floatHeaderOptions' => [
            'scrollingTop' => '0',
            'position' => 'absolute',
        ],
        'hover' => true,
        'floatOverflowContainer' => true,
        'showPageSummary' => false,
        'summaryOptions' => ['class' => 'pull-right'],
        'pjax' => true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => $browserId, 'enablePushState' => false,],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'rowOptions'=>function($model){
            $valid = $model['harvestDataValid'];
            if($valid)
                return [];
            else
                return ['class' => 'danger'];
        },
        'panel'=> $panel,
        'toolbar' =>  [
            [
                'content' => $actionButtonsFocusMode
            ],
            [
                'content' =>
                    Html::a(
                        '<i class="material-icons">filter_center_focus</i>',
                        '#',
                        [
                            'class' => 'btn btn-default hm-tooltipped' . $focus,
                            'id' => 'hm-crt-plot-focused-view-btn',
                            'style' => "margin-left:10px;",
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Focus on plots'),
                        ]
                    )
                    .Html::a(
                        '<i class="material-icons">swap_horiz</i>',
                        '#',
                        [
                            'class' => 'btn btn-default hm-tooltipped' . $split,
                            'id' => 'hm-crt-cross-focused-view-btn',
                            'style' => "margin-left:10px;",
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Switch focus to crosses'),
                        ]
                    )
                    .Html::a(
                        '<i class="material-icons" style="transform: rotate(90deg);">splitscreen</i>',
                        '#',
                        [
                            'class' => 'btn btn-default hm-tooltipped' . $split,
                            'id' => 'hm-crt-split-view-btn',
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Split view')
                        ]
                    )
                    .Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [Yii::$app->controller->action->id],
                        [
                            'class' => 'btn btn-default hm-tooltipped',
                            'id' => 'hm-crt-reset-plot-btn',
                            'style' => $btnStyle,
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    )
                    .'{dynagridFilter}{dynagridSort}{dynagrid}'

            ],
        ],
    ],
    'options'=>['id'=>$browserId]
]);

DynaGrid::end();

echo '</div></div>';


$script = <<< JS

$("document").ready(function(){
    // Reset browser button click event
    $(document).on("click","#hm-crt-reset-plot-btn", function(e) {
        e.preventDefault();
        var pageurl = $('#$browserId li.active a').attr('href');
        $.pjax.reload(
            {
                container:'#$browserId-pjax',
                url: pageurl,
                replace: false,
                type: 'POST',
                data: {
                    forceReset:true,
                    target:'$browserId-pjax'
                }
            }
        );
    });

    // Perform after pjax completion of browser
    $(document).on('ready pjax:complete','#$browserId-pjax', function(e) {
        // Initialize tooltip
        initializeTooltips();
        // Adjust creation browser height
        adjustCreationBrowsers();
        resize();
    });
});

JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    div.hm-browser-title {
        padding: 10px 0px 10px 10px;
    }

    h3.hm-browser-title {
        display: inline;
    }

    div.hm-crt-plot-browser-div {
        padding-right:0px
    }
');

?>