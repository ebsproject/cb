<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\controllers;

// Import Yii
use Yii;
// Import components
use app\components\B4RController;
// Import interfaces
use app\interfaces\models\IBackgroundJob;
use app\interfaces\models\IConfig;
use app\interfaces\models\IExperiment;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IUser;
use app\interfaces\models\IVariable;
use app\interfaces\models\IWorker;
use app\interfaces\modules\harvestManager\models\ICrossBrowserModel;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel;
use app\interfaces\modules\harvestManager\models\IPlotBrowserModel;
// Import models
use app\modules\harvestManager\models\HarvestDataModel;
use app\models\Crop;
use app\models\CrossData;
use app\models\OccurrenceData;
use app\models\PlotData;

/**
 * Harvest data controller for the `harvestManager` module
 */
class HarvestDataController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        protected IBackgroundJob $backgroundJob,
        protected IConfig $configurations,
        protected CrossData $crossDataModal,
        protected IExperiment $experiment,
        protected IOccurrence $occurrence,
        protected PlotData $plotDataModal,
        protected IUser $user,
        protected IVariable $variableModel,
        protected IWorker $worker,
        protected IHarvestManagerModel $harvestManagerModel,
        protected ICrossBrowserModel $crossBrowserModel,
        protected IOccurrenceDetailsModel $occurenceDetailsModel,
        protected IPlotBrowserModel $plotBrowserModel,
        protected HarvestDataModel $harvestDataModel,
        protected Crop $crop,
        protected OccurrenceData $occurrenceData,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Controller action for displaying harvest data of plots
     * @param string program current program code
     * @param string occurrenceId occurrence identifier
     * @return string
     */
    public function actionPlots($program = null, $occurrenceId = null) {
        // Get url
        $url = \Yii::$app->request->getUrl();
        // Validate URL parameters
        $validation = $this->harvestManagerModel->validateUrlParameters(
            [
                'occurrenceId' => $occurrenceId
            ],
            $url
        );
        // If there is an error, set flash message and redirect to HM landing page
        if($validation['error']) {
            Yii::$app->session->setFlash('error', $validation['errorMessage']);
            return $this->redirect(['occurrence-selection/index', 'program' => $program]);
        }
        
        // Get occurrence details
        $result = $this->occurrence->searchAll(["occurrenceDbId"=>"equals $occurrenceId"]);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        $occurrenceName = isset($occurrence['occurrenceName']) ? $occurrence['occurrenceName'] : '';
        $accessData = isset($occurrence['accessData']) ? $occurrence['accessData'] : null;
        $creatorDbId = isset($occurrence['creatorDbId']) ? $occurrence['creatorDbId'] : 0;
        // Check if user has access to the occurrence
        $isAdmin = $this->user->isAdmin();
        if ($isAdmin || Yii::$app->access->renderAccess("HARVEST_OCCURRENCE","HARVEST_MANAGER", $creatorDbId, $accessData, 'write', 'url', true))  {

            // Retrieve HARVEST DATA REQUIREMENTS Config
            $harvestDataRequirementsConfig = \Yii::$app->cbSession->get("harvestDataRequirementsConfig");
            // If not yet in session, retrieve via API
            if(empty($harvestDataRequirementsConfig)) {
                // Get config
                $harvestDataRequirementsConfig = $this->configurations->getConfigByAbbrev('HM_HARVEST_DATA_REQUIREMENTS_CONFIG');
                // Store in session
                \Yii::$app->cbSession->set("harvestDataRequirementsConfig", $harvestDataRequirementsConfig);
            }

            // Get experiment details
            $experimentId = isset($occurrence['experimentDbId']) ? $occurrence['experimentDbId'] : '';
            $result = $this->experiment->searchAll(["experimentDbId"=>"equals $experimentId"]);
            $experiment = isset($result['data'][0]) ? $result['data'][0] : [];
            $experimentType = isset($experiment['experimentType']) ? $experiment['experimentType'] : '';
            $stage = isset($experiment['stageCode']) ? $experiment['stageCode'] : '';
            $cropCode = isset($experiment['cropCode']) ? $experiment['cropCode'] : '';
            
            // Get location id
            $locationId = $this->occurenceDetailsModel->getLocationIdOfOccurrence($occurrenceId);

            // Check if CPN
            $cpnCheck = $this->harvestManagerModel->checkCPN($experimentId);

            $distinctValues = $this->harvestDataModel->getDistinctValues(
                'plot',                                 // data level
                [                                       // distinct fields
                    "harvestStatus",
                    "state",
                    "type"
                ],
                $occurrenceId                           // occurrence ID
            );
            // Save distinct values to the session
            \Yii::$app->cbSession->set("hm-distinct-values-plot-".$occurrenceId, $distinctValues);

            // Retrieve plot browser data
            $params = Yii::$app->request->getQueryParams();

            $distinctHarvestStatus = $distinctValues['harvestStatus'] ?? [];
            $params['distinctHarvestStatus'] = $distinctHarvestStatus;
            $searchModel = $this->plotBrowserModel;
            $browserData = $searchModel->search($params, $experimentId, $occurrenceId, $locationId);
            // Get distinct states
            $states = $distinctValues['state'] ?? [];
            // If no states found, set states to an array with 'default' as the only content
            $states = empty($states) ? ['default'] : $states;
            // Add 'TCV' state
            $states[] = 'TCV';

            // Get distinct types
            $types = $distinctValues['type'] ?? [];
            // Add 'default' type config
            $types[] = 'default';

            // Check if unknown states exist
            $statesArray = $distinctValues['state'] ?? [];
            $unknownStates = array_values(array_diff($statesArray, ['fixed', 'not_fixed']));
            if(!empty($unknownStates) && !in_array('INVALID_STATE', $distinctHarvestStatus)) {
                $distinctHarvestStatus[] = 'INVALID_STATE';
            }

            // Retrieve scale values
            $germplasmStateScaleValues = $this->harvestManagerModel->getScaleValues('GERMPLASM_STATE');
            $germplasmTypeScaleValues = $this->harvestManagerModel->getScaleValues('GERMPLASM_TYPE');

            // Retrieve data browser config 
            $browserConfig = $this->configurations->getConfigByAbbrev('HM_DATA_BROWSER_CONFIG_'.$cropCode);

            // Get selfing config
            $selfingConfig = $browserConfig['CROSS_METHOD_SELFING'];
            $browserConfig = [
                'CROSS_METHOD_SELFING' => empty($selfingConfig) ? [] : $selfingConfig
            ];
            // Consume config
            $browserConfig = $this->harvestDataModel->consumeConfig($browserConfig, $states, $types);

            // Get bulk update threshold
            $harvestDataUpdateThreshold = Yii::$app->config->getAppThreshold('HARVEST_MANAGER','updateHarvestData');

            // Retrieve current filter values from session
            $sessionVar = \Yii::$app->cbSession->get("observation-filter-plot-$occurrenceId");
            $observationFilter = [];
            if(!empty($sessionVar)) {
                $observationFilter = isset($sessionVar) ? (array) json_decode($sessionVar) : [];
            }

            // retrieve toolbar filters
            $filterFields = [
                'order_number' => [
                    'id' => 'filter-order_number-fld',
                    'name' => 'Range Filter',
                    'label' => 'Range',
                    'tooltip' => 'Input range value (e.g. 1-40 or 80-100)',
                    'filter_type' => 'integer',
                    'input_type' => 'range',
                    'api_body_param' => 'orderNumber'
                ]
            ];
            $toolbarFilterSess = \Yii::$app->cbSession->get("toolbar-filter-plot-" . $occurrenceId);
            $toolbarFilters = \Yii::$app->controller->renderPartial(
                '_browser_filter_range.php',
                [
                    'filterFields' => $filterFields,
                    'filterVarArr' => ['ORDER_NUMBER'],
                    'dataSource' => 'plots',
                    'program' => $program,
                    'occurrenceId' => $occurrenceId,
                    'filters' => $toolbarFilterSess,
                    'browserModel' => 'PlotBrowserModel'

                ]);

            // Generate chips html
            $statusChips = $this->harvestDataModel->generateHarvestStatusChips($distinctHarvestStatus, $occurrenceId,'plot');

            // Build input columns for dynagrid
            $inputColumns = $this->harvestManagerModel->buildInputColumns('plot', $occurrenceId);

            return $this->render('index', [
                'displayMode' => 'plots',
                'program' => $program,
                'occurrenceId' => $occurrenceId,
                'locationId' => $locationId,
                'dataLevel' => 'plots',
                'experimentType' => $experimentType,
                'occurrenceName' => $occurrenceName,
                'searchModel' => $searchModel,
                'browserData' => $browserData,
                'germplasmStateScaleValues' => $germplasmStateScaleValues,
                'germplasmTypeScaleValues' => $germplasmTypeScaleValues,
                'cpnCheck' => $cpnCheck,
                'browserConfig' => $browserConfig,
                'controllerModel' => $this->harvestDataModel,
                'harvestDataUpdateThreshold' => $harvestDataUpdateThreshold,
                'observationFilter' => $observationFilter,
                'stage' => $stage,
                'browserId' => 'dynagrid-harvest-manager-plot-hdata-grid',
                'toolbarFilters' => $toolbarFilters,
                'statusChips' => $statusChips,
                'inputColumns' => $inputColumns
            ]);
        }
        
    }

    /**
     * Controller action for retrieving the harvest method protocol occurrence data
     */
    public function actionRetrieveHarvestProtocol(){
        $occurrenceId = $_POST['occurrenceDbId'] ?? 0;
        $methodApplicable = true;
        $occurrenceData = $this->occurrenceData->searchAll([
            'occurrenceDbId' => "equals $occurrenceId",
            'variableAbbrev' => 'HV_METH_DISC'
        ]);
        $harvestMethod = $occurrenceData['data'][0]['dataValue'] ?? '';
        $distinctValues = \Yii::$app->cbSession->get("hm-distinct-values-plot-".$occurrenceId);

        // Get program and crop details
        $programId = $this->occurrence->searchAll(['occurrenceDbId' => "equals $occurrenceId",
            'fields' => 'occurrence.id AS occurrenceDbId|program.id AS programDbId',
        ])['data'][0]['programDbId'];
        $cropCode = $this->crop->searchAll(['programDbId' => "equals $programId",
            'fields' => 'crop.id AS cropDbId|program.id AS programDbId|crop.crop_code AS cropCode',
        ])['data'][0]['cropCode'];

        // Retrieve data browser config
        $config = $this->configurations->getConfigByAbbrev('HM_DATA_BROWSER_CONFIG_'.$cropCode);

        // Get distinct states
        $states = $distinctValues['state'] ?? [];
        // If no states found, set states to an array with 'default' as the only content
        $states = empty($states) ? ['default'] : $states;

        // Get distinct types
        $types = $distinctValues['type'] ?? [];
        // If no types found, set types to an array with 'default' as the only content
        $types = empty($types) ? ['default'] : $types;

        // Consume config
        $browserConfig = $this->harvestDataModel->consumeConfig($config, $states, $types);
        $harvestMethodList = array_keys($browserConfig['method_numvar_compat']['CROSS_METHOD_SELFING'] ?? []);

        // Get state-type compatibility of the harvest method from the protocols.
        $compatMapping = [];
        $stateTypeMethodCompat = $browserConfig['bulk_update_config']['gstate_gtype_hmet_compat'] ?? [];

        // Save state-type compatibility for the selected method
        foreach($stateTypeMethodCompat as $state => $typeMethodCompat) {
            // If state is "default", skip.
            if($state == 'default') continue;

            foreach($typeMethodCompat as $type => $methods) {
                // If type is "default", set value as "any type".
                if($type == 'default') $type = 'any type';

                // If the method from protocol is in the methods array,
                // add state and type to compat mapping array.
                if(in_array($harvestMethod, $methods)) {
                    if(!isset($compatMapping[$state])) {
                        $compatMapping[$state] = [];
                    }

                    // If type = 'any type', set compat map for state as
                    // an array containing 'any type'.
                    if ($type == 'any type') {
                        $compatMapping[$state] = [$type];
                    }
                    // If state's compat map does not contain 'any type',
                    // add current type into the compat map.
                    else if (!in_array('any type', $compatMapping[$state])) {
                        $compatMapping[$state][] = $type;
                    }
                }
            }
        }

        // Check if harvest method is not yet supported
        if(!in_array($harvestMethod,$harvestMethodList)){
            $methodApplicable = false;
        }

        return json_encode([
            'harvestMethod' => $harvestMethod,
            'methodApplicable' => $methodApplicable,
            'compatMapping' => $compatMapping,
        ]);
    }

    /**
     * Controller action for populating transaction dataset based on specified harvest method protocol
     */
    public function actionSaveHarvestProtocol(){
        $recordIds = [];
        $occurrenceId = $_POST['occurrenceDbId'] ?? 0;
        $harvestMethod = $_POST['harvestMethod'] ?? '';
        $compatMapping = isset($_POST['compatMapping']) ? (array) json_decode($_POST['compatMapping']) : [];
        $locationId = $this->occurenceDetailsModel->getLocationIdOfOccurrence($occurrenceId);

        // Set base condition
        $baseConditions = [
            'harvestStatus' => 'not equals COMPLETED|not equals QUEUED_FOR_HARVEST|not equals HARVEST_IN_PROGRESS|not equals DONE', 
        ];

        // Set request body
        $requestBody = [
            'fields' => 'plot.id AS plotDbId|germplasm.designation AS designation|germplasm.germplasm_state AS state|germplasm.germplasm_type AS type|plot.harvest_status AS harvestStatus',
            "conditions" => []
        ];

        // Loop through compat mapping array
        foreach($compatMapping as $state => $types) {
            $currentCondition = [
                "state" => "equals $state"
            ];

            // If the types array does not contain 'any type',
            // add type condition.
            if(!in_array('any type', $types)) {
                $currentCondition['type'] = "equals " . implode("|equals ", $types);
            }

            // Add new conditions array to the request body
            $requestBody['conditions'][] = array_merge($baseConditions, $currentCondition);
        }

        // Retreive data
        $harvestData = $this->plotBrowserModel->searchAllPlots($occurrenceId, $requestBody);
        if(!empty($harvestData)){
            $recordIds = array_column($harvestData, 'plotDbId');
        }
        
        // Perform update
        return $this->harvestDataModel->insertDataset($locationId, ['HV_METH_DISC'], [[$harvestMethod]], $recordIds, 'plot');
    }

    /**
     * Controller action for displaying harvest data of crosses
     * @param string program current program code
     * @param string occurrenceId occurrence identifier
     * @return string
     */
    public function actionCrosses($program = null, $occurrenceId = null) {
        // Get url
        $url = \Yii::$app->request->getUrl();
        // Validate URL parameters
        $validation = $this->harvestManagerModel->validateUrlParameters(
            [
                'occurrenceId' => $occurrenceId
            ],
            $url
        );
        // If there is an error, set flash message and redirect to HM landing page
        if($validation['error']) {
            Yii::$app->session->setFlash('error', $validation['errorMessage']);
            return $this->redirect(['occurrence-selection/index', 'program' => $program]);
        }
        
        // Get occurrence details
        $result = $this->occurrence->searchAll(["occurrenceDbId"=>"equals $occurrenceId"]);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        $occurrenceName = isset($occurrence['occurrenceName']) ? $occurrence['occurrenceName'] : '';
        $accessData = isset($occurrence['accessData']) ? $occurrence['accessData'] : null;
        $creatorDbId = isset($occurrence['creatorDbId']) ? $occurrence['creatorDbId'] : 0;
        // Check if user has access to the occurrence
        $isAdmin = $this->user->isAdmin();
        if ($isAdmin || Yii::$app->access->renderAccess("HARVEST_OCCURRENCE","HARVEST_MANAGER", $creatorDbId, $accessData, 'write', 'url', true))  {
            // Retrieve HARVEST DATA REQUIREMENTS Config
            $harvestDataRequirementsConfig = \Yii::$app->cbSession->get("harvestDataRequirementsConfig");
            // If not yet in session, retrieve via API
            if(empty($harvestDataRequirementsConfig)) {
                // Get config
                $harvestDataRequirementsConfig = $this->configurations->getConfigByAbbrev('HM_HARVEST_DATA_REQUIREMENTS_CONFIG');
                // Store in session
                \Yii::$app->cbSession->set("harvestDataRequirementsConfig", $harvestDataRequirementsConfig);
            }

            // Get experiment details
            $experimentId = isset($occurrence['experimentDbId']) ? $occurrence['experimentDbId'] : '';
            $result = $this->experiment->searchAll(["experimentDbId"=>"equals $experimentId"]);
            $experiment = isset($result['data'][0]) ? $result['data'][0] : [];
            $experimentType = isset($experiment['experimentType']) ? $experiment['experimentType'] : '';
            $cropCode = isset($experiment['cropCode']) ? $experiment['cropCode'] : '';
            // Get location id
            $locationId = $this->occurenceDetailsModel->getLocationIdOfOccurrence($occurrenceId);

            // Check if CPN
            $cpnCheck = $this->harvestManagerModel->checkCPN($experimentId);

            $distinctValues = $this->harvestDataModel->getDistinctValues(
                'cross',                                // data level
                [                                       // distinct fields
                    "harvestStatus",
                    "state",
                    "type",
                    "crossMethod"
                ],
                $occurrenceId                           // occurrence ID
            );
            // Save distinct values to the session
            \Yii::$app->cbSession->set("hm-distinct-values-cross-".$occurrenceId, $distinctValues);

            // Retrieve plot browser data
            $params = Yii::$app->request->getQueryParams();
            $distinctHarvestStatus = $distinctValues['harvestStatus'] ?? [];
            $params['distinctHarvestStatus'] = $distinctHarvestStatus;
            $searchModel = $this->crossBrowserModel;

            $browserData = $searchModel->search($params, $experimentId, $occurrenceId, $locationId);

            // Get distinct states (selfing, if any)
            $states = $distinctValues['state'] ?? [];
            // If no states found, set states to an array with 'default' as the only content
            $states = empty($states) ? ['default'] : $states;

            // Get distinct types (selfing, if any)
            $types = $distinctValues['type'] ?? [];
            // If no types found, set types to an array with 'default' as the only content
            $types = empty($types) ? ['default'] : $types;

            // Get distinct cross methods (selfing, if any)
            $crossMethods = $distinctValues['crossMethod'] ?? [];

            // Check if unknown states exist
            $statesArray = $distinctValues['state'] ?? [];
            $unknownStates = array_values(array_diff($statesArray, ['fixed', 'not_fixed']));
            if(!empty($unknownStates) && !in_array('INVALID_STATE', $distinctHarvestStatus)) {
                $distinctHarvestStatus[] = 'INVALID_STATE';
            }

            // Retrieve scale values
            $germplasmStateScaleValues = $this->harvestManagerModel->getScaleValues('GERMPLASM_STATE');
            $germplasmTypeScaleValues = $this->harvestManagerModel->getScaleValues('GERMPLASM_TYPE');

            // Retrieve data browser config 
            $browserConfig = $this->configurations->getConfigByAbbrev('HM_DATA_BROWSER_CONFIG_'.$cropCode);
            // If no selfing crosses, remove selfing config
            if(!in_array('selfing', $crossMethods)) unset($browserConfig['CROSS_METHOD_SELFING']);
            // Consume config
            $browserConfig = $this->harvestDataModel->consumeConfig($browserConfig, $states, $types);

            // Get bulk update threshold
            $harvestDataUpdateThreshold = Yii::$app->config->getAppThreshold('HARVEST_MANAGER','updateHarvestData');

            // Retrieve current filter values from session
            $sessionVar = \Yii::$app->cbSession->get("observation-filter-cross-$occurrenceId");
            $observationFilter = [];
            if(!empty($sessionVar)) {
                $observationFilter = isset($sessionVar) ? (array) json_decode($sessionVar) : [];
            }

            // retrieve toolbar filters
            $filterFields = [
                'order_number' => [
                    'id' => 'cross-filter-order_number-fld',
                    'name' => 'Range Filter',
                    'label' => 'Range',
                    'tooltip' => 'Input range value (e.g. 1-40 or 80-100)',
                    'filter_type' => 'integer',
                    'input_type' => 'range',
                    'api_body_param' => 'orderNumber'
                ]
            ];
            
            $toolbarFilterSess = \Yii::$app->cbSession->get("toolbar-filter-cross-" . $occurrenceId);

            $toolbarFilters = \Yii::$app->controller->renderPartial(
                '_browser_filter_range.php',
                [
                    'filterFields' => $filterFields,
                    'filterVarArr' => ['ORDER_NUMBER'],
                    'dataSource' => 'cross',
                    'program' => $program,
                    'occurrenceId' => $occurrenceId,
                    'filters' => $toolbarFilterSess,
                    'browserModel' => 'CrossBrowserModel'

                ]);

            // Generate chips html
            $statusChips = $this->harvestDataModel->generateHarvestStatusChips($distinctHarvestStatus, $occurrenceId,'cross');

            // Build input columns for dynagrid
            $inputColumns = $this->harvestManagerModel->buildInputColumns('cross', $occurrenceId);

            return $this->render('index', [
                'displayMode' => 'crosses',
                'program' => $program,
                'occurrenceId' => $occurrenceId,
                'locationId' => $locationId,
                'dataLevel' => 'crosses',
                'experimentType' => $experimentType,
                'occurrenceName' => $occurrenceName,
                'searchModel' => $searchModel,
                'browserData' => $browserData,
                'germplasmStateScaleValues' => $germplasmStateScaleValues,
                'germplasmTypeScaleValues' => $germplasmTypeScaleValues,
                'cpnCheck' => $cpnCheck,
                'browserConfig' => $browserConfig,
                'controllerModel' => $this->harvestDataModel,
                'harvestDataUpdateThreshold' => $harvestDataUpdateThreshold,
                'observationFilter' => $observationFilter,
                'browserId' => 'dynagrid-harvest-manager-cross-hdata-grid',
                'statusChips' => $statusChips,
                'toolbarFilters' => $toolbarFilters,
                'inputColumns' => $inputColumns
            ]);
        }
    }

    /**
     * Updates datasets in the data terminal
     */
    public function actionUpdate()
    {
        // Unpack $_POST parameters
        $locationId = isset($_POST['locationId']) ? $_POST['locationId'] : 0;
        $variableAbbrevs = isset($_POST['variableAbbrevs']) ? json_decode($_POST['variableAbbrevs']) : [];
        $values = isset($_POST['values']) ? json_decode($_POST['values']) : [];
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $idType = isset($_POST['idType']) ? $_POST['idType'] : '';

        // Perform update
        return $this->harvestDataModel->insertDataset($locationId, $variableAbbrevs, $values, $recordIds, $idType);
    }

    /**
     * Returns new notifications from background processes
     * @param String workerName the name of the worker to retrieve notifications for
     * @param String remarks remarks to specify for the retrieval
     */
    public function actionNewNotificationsCount($workerName = '', $remarks = '')
    {
        // Parameters for background jobs search
        $params = $this->harvestDataModel->buildNotifRetrievalParams($workerName, $remarks, 'false');
        $filter = 'sort=modificationTimestamp:desc';   
        $result = $this->backgroundJob->searchAll($params, $filter);

        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        }

        return json_encode($result["totalCount"]);
    }

    /** 
     * Renders notifications from background processes 
     */
    public function actionPushNotifications()
    {
        // Get worker name
        $workerName = isset($_POST['workerName']) ? $_POST['workerName'] : '';
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : '';
        
        $params = $this->harvestDataModel->buildNotifRetrievalParams($workerName, $remarks, 'false');
        $result = $this->backgroundJob->searchAll($params, "sort=modificationTimestamp:desc");
        
        if(isset($result['data']) && !empty($result['data'])){
            foreach($result['data'] as $key=>$bgJob){
                if($bgJob['workerName'] == "BuildCsvData"){
                    $result['data'][$key]['message'] = "Preparing seeds and packages CSV for downloading ".strtolower($bgJob['jobStatus']."!");
                }
            }
        }
        
        $newNotifications = $result["data"];
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->backgroundJob->getGenericNotifications($newNotifications);

        $params = $this->harvestDataModel->buildNotifRetrievalParams($workerName, $remarks, 'true');
        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);

        if(isset($result['data']) && !empty($result['data'])){
            foreach($result['data'] as $key=>$bgJob){
                if($bgJob['workerName'] == "BuildCsvData"){
                    $result['data'][$key]['message'] = "Preparing seeds and packages CSV for downloading ".strtolower($bgJob['jobStatus']."!");
                }
            }
        }
        
        $notifications = $result["data"];
        $seenCount = count($notifications);
        $seenNotifications = $this->backgroundJob->getGenericNotifications($notifications);
        
        // update unseen notifications to seen
        $params = [
            "jobIsSeen" => "true"
        ];
        $result = $this->backgroundJob->update($newNotifications, $params);
        
        return $this->renderAjax(
            '_notifications',
            [
                "unseenNotifications" => $unseenNotifications,
                "seenNotifications" => $seenNotifications,
                "unseenCount" => $unseenCount,
                "seenCount" => $seenCount,
            ]
        );
    }

    /**
     * Checks if the harvest status and state of the records are valid for bulk update
     */
    public function actionCheckHarvestStatusAndState() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : null;

        $recordInfo = $this->harvestDataModel->getRecordHarvestStatusAndState($occurrenceId, $dataLevel, $recordIds);

        return json_encode($this->harvestDataModel->checkHarvestStatusAndState($recordInfo));
    }

    /**
     * Renders the bulk update modal content
     */
    public function actionRenderBulkUpdateModal() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $locationId = isset($_POST['locationId']) ? $_POST['locationId'] : 0;
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : null;
        $bulkUpdateConfig = isset($_POST['bulkUpdateConfig']) ? (array) json_decode($_POST['bulkUpdateConfig']) : [];
        
        // For each numeric variable, build the HTML input element
        $numVarHTML = "";
        foreach($bulkUpdateConfig['numeric_variables'] as $numVarConfig) {
            $numVarHTML .= $this->harvestDataModel->buildNumVarInput(
                (array) $numVarConfig,
                'hm-bulk-update-input',
                'hm-bulk-update-input hm-bulk-update-input-numvar'
            );
        }

        // Retrieve all records count (browser filters applied)
        $allRecordsCount = $this->harvestDataModel->getAllRecordsCount($occurrenceId, $dataLevel);

        // Retrieve info of selected records
        $recordInfo = [];
        if(!empty($recordIds)) {
            $recordInfo = $this->harvestDataModel->getRecordInfo($recordIds, $occurrenceId, $dataLevel);
            if(empty($recordInfo)) {
                $errorMessage = 'No records found.';
                throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
            }
        }

        // Get bulk update threshold
        $harvestDataUpdateThreshold = Yii::$app->config->getAppThreshold('HARVEST_MANAGER','updateHarvestData');
        if(!isset($harvestDataUpdateThreshold)) {
            $errorMessage = 'Unable to retrieve app threshold.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        // Get occurrence details
        $result = $this->occurrence->searchAll(["occurrenceDbId"=>"equals $occurrenceId"]);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        // Get experiment details
        $experimentId = isset($occurrence['experimentDbId']) ? $occurrence['experimentDbId'] : '';
        $result = $this->experiment->searchAll(["experimentDbId"=>"equals $experimentId"]);
        $experiment = isset($result['data'][0]) ? $result['data'][0] : [];
        $stage = isset($experiment['stageCode']) ? $experiment['stageCode'] : '';

        // Render input elements
        $inputElements = $this->harvestDataModel->renderBulkUpdateModalContents();

        // Get harvest method values
        $harvestMethodValues = \Yii::$app->session->get("harvestMethodValues") ?? [];
        $methodNumVarCompat = \Yii::$app->session->get("methodNumVarCompat") ?? [];
        $numVarConfigItems = \Yii::$app->session->get("numVarConfigItems") ?? [];
        $numVarAbbrevs = array_column($numVarConfigItems, 'variableAbbrev');
        $numVarCount = count($numVarConfigItems);
        $variableFieldNames = \Yii::$app->session->get("variableFieldNames") ?? [];
        $variableDisplayNames = \Yii::$app->session->get("variableDisplayNames") ?? [];

        // Render the bulk update modal content
        return $this->renderAjax('_modal_bulk_update', [
            'occurrenceId' => $occurrenceId,
            'locationId' => $locationId,
            'recordIds' => $recordIds,
            'recordInfo' => $recordInfo,
            'harvestMethodValues' => $harvestMethodValues,
            'methodNumVarCompat' => $methodNumVarCompat,
            'allRecordsCount' => $allRecordsCount,
            'dataLevel' => $dataLevel,
            'numVarHTML' => $numVarHTML,
            'bulkUpdateConfig' => $bulkUpdateConfig,
            'harvestDataUpdateThreshold' => $harvestDataUpdateThreshold,
            'stage' => $stage,
            'inputElements' => $inputElements,
            'numVarCount' => $numVarCount,
            'variableFieldNames' => $variableFieldNames,
            'variableDisplayNames' => $variableDisplayNames,
            'numVarAbbrevs' => $numVarAbbrevs
        ]);
    }

    /**
     * Renders the additional harvest modal content
     */
    public function actionRenderAdditionalHarvestModal() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $locationId = isset($_POST['locationId']) ? $_POST['locationId'] : 0;
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : null;
        $bulkUpdateConfig = isset($_POST['bulkUpdateConfig']) ? (array) json_decode($_POST['bulkUpdateConfig']) : [];
        
        // For each numeric variable, build the HTML input element
        $numVarHTML = "";
        foreach($bulkUpdateConfig['numeric_variables'] as $numVarConfig) {
            $numVarHTML .= $this->harvestDataModel->buildNumVarInput(
                (array) $numVarConfig,
                'hm-addl-harvest-input',
                'hm-addl-harvest-input hm-addl-harvest-input-numvar'
            );
        }

        // Retrieve all records count (browser filters applied)
        $allRecordsCount = $this->harvestDataModel->getAllRecordsCount($occurrenceId, $dataLevel);

        // Retrieve info of selected records
        $recordInfo = [];
        if(!empty($recordIds)) {
            $recordInfo = $this->harvestDataModel->getRecordInfo($recordIds, $occurrenceId, $dataLevel);
            if(empty($recordInfo)) {
                $errorMessage = 'No records found.';
                throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
            }
        }

        // Get occurrence details
        $result = $this->occurrence->searchAll(["occurrenceDbId"=>"equals $occurrenceId"]);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        // Get experiment details
        $experimentId = isset($occurrence['experimentDbId']) ? $occurrence['experimentDbId'] : '';
        $result = $this->experiment->searchAll(["experimentDbId"=>"equals $experimentId"]);
        $experiment = isset($result['data'][0]) ? $result['data'][0] : [];
        $stage = isset($experiment['stageCode']) ? $experiment['stageCode'] : '';

        // Render the bulk update modal content
        return $this->renderAjax('_modal_additional_harvest', [
            'occurrenceId' => $occurrenceId,
            'locationId' => $locationId,
            'recordIds' => $recordIds,
            'recordInfo' => $recordInfo,
            'allRecordsCount' => $allRecordsCount,
            'dataLevel' => $dataLevel,
            'numVarHTML' => $numVarHTML,
            'bulkUpdateConfig' => $bulkUpdateConfig,
            'stage' => $stage,
        ]);
    }

    /**
     * Retrieves all record information for validation in bulk operations
     */
    public function actionRetrieveAllRecordInfo() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : null;
        $limit = isset($_POST['limit']) ? $_POST['limit'] : null;

        // Retrieve info of selected records
        return json_encode($this->harvestDataModel->getRecordInfo([], $occurrenceId, $dataLevel, $limit));
    }

    /**
     * Retrieve session variables
     * 
     * @return mixed
     */
    public function retrieveSessionVariables()
    {
        $sessNumVarConfigItems = \Yii::$app->session->get('numVarConfigItems');
        $sessHDataConfigItems = \Yii::$app->session->get('hDataConfigItems');

        $configItems = array_merge($sessNumVarConfigItems, $sessHDataConfigItems);

        $harvestDataVarInfoArr = [];
        foreach ($configItems as $item) {
            if (!isset($item['displayName']) || empty($item['displayName']) || $item['displayName'] == null) {
                // Retrieve displayName values for variables
                $variableInfo = $this->variableModel->getVariableByAbbrev($item['variableAbbrev']);
                $item['displayName'] = $variableInfo['displayName'] ?? "";
            }

            if (isset($item['apiFieldName']) && !empty($item['apiFieldName'])) {
                array_push($harvestDataVarInfoArr, $item);
            }
        }

        return $harvestDataVarInfoArr;
    }

    /**
     * Check if the selected records have harvest data (committed and/or terminal)
     */
    public function actionCheckIfRecordsHaveHarvestData() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : null;

        // Retrieve info of selected records
        $recordInfo = [];
        if(!empty($recordIds)) {
            $recordInfo = $this->harvestDataModel->getRecordInfo($recordIds, $occurrenceId, $dataLevel);

            // Check if records have harvest data
            $harvestDataVarInfoArr = $this->retrieveSessionVariables();
            $result = $this->harvestDataModel->hasHarvestData($recordInfo, $harvestDataVarInfoArr);
            return (isset($result["hasData"]) && $result["hasData"]) ? 'true' : 'false';
        }

        return 'false';
    }

    /**
     * Renders the bulk delete modal content
     */
    public function actionRenderBulkDeleteModal() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $locationId = isset($_POST['locationId']) ? $_POST['locationId'] : 0;
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : null;
        $bulkDeleteConfig = isset($_POST['bulkDeleteConfig']) ? (array) json_decode($_POST['bulkDeleteConfig']) : [];

        $harvestDataVarInfoArr = $this->retrieveSessionVariables();

        // Build checkboxes
        $numVarAbbrevs = array_column(array_filter($harvestDataVarInfoArr, function ($record) {
            return $record['inputType'] == 'number';
        }), 'variableAbbrev');

        $harvestDataCheckboxHTML = $this->harvestDataModel->buildHarvestDataCheckbox($harvestDataVarInfoArr, $numVarAbbrevs);

        // Retrieve threshold values
        $harvestDataThreshold = Yii::$app->config->getAppThreshold('HARVEST_MANAGER','deleteHarvestData');
        if(!isset($harvestDataThreshold)) {
            $errorMessage = 'Unable to retrieve harvest data deletion threshold.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }
        $seedThreshold = Yii::$app->config->getAppThreshold('HARVEST_MANAGER','deleteSeeds');
        if(!isset($seedThreshold)) {
            $errorMessage = 'Unable to retrieve seed deletion threshold.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        // Retrieve all records count (browser filters applied)
        $allRecordsCount = $this->harvestDataModel->getAllRecordsCount($occurrenceId, $dataLevel);

        // Retrieve info of selected records
        $recordInfo = [];
        if(!empty($recordIds)) {
            $recordInfo = $this->harvestDataModel->getRecordInfo($recordIds, $occurrenceId, $dataLevel);
            if(empty($recordInfo)) {
                $errorMessage = 'No records found.';
                throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
            }
        }

        // Check if records have harvest data
        $traitsStatus = $this->harvestDataModel->hasHarvestData($recordInfo, $harvestDataVarInfoArr);

        // Get occurrence details
        $result = $this->occurrence->searchAll(["occurrenceDbId"=>"equals $occurrenceId"]);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        // Get experiment details
        $experimentId = isset($occurrence['experimentDbId']) ? $occurrence['experimentDbId'] : '';
        $result = $this->experiment->searchAll(["experimentDbId"=>"equals $experimentId"]);
        $experiment = isset($result['data'][0]) ? $result['data'][0] : [];
        $stage = isset($experiment['stageCode']) ? $experiment['stageCode'] : '';

        // Render the bulk delete modal content
        return $this->renderAjax('_modal_bulk_delete', [
            'occurrenceId' => $occurrenceId,
            'locationId' => $locationId,
            'recordIds' => $recordIds,
            'recordInfo' => $recordInfo,
            'allRecordsCount' => $allRecordsCount,
            'dataLevel' => $dataLevel,
            'traitsStatus' => $traitsStatus,
            'harvestDataCheckboxHTML' => $harvestDataCheckboxHTML,
            'harvestDataThreshold' => $harvestDataThreshold,
            'seedThreshold' => $seedThreshold,
            'bulkDeleteConfig' => $bulkDeleteConfig,
            'havestDataVarArr' => $harvestDataVarInfoArr,
            'numVarAbbrevs' => $numVarAbbrevs,
            'stage' => $stage,
        ]);
    }

    /**
     * Renders the revert harvest modal content
     */
    public function actionRenderRevertHarvestModal() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $locationId = isset($_POST['locationId']) ? $_POST['locationId'] : 0;
        $recordInfo = isset($_POST['records']) ? json_decode($_POST['records']) : [];
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : null;
        $bulkDeleteConfig = isset($_POST['bulkDeleteConfig']) ? (array) json_decode($_POST['bulkDeleteConfig']) : [];

        // Retrieve threshold value
        $seedThreshold = Yii::$app->config->getAppThreshold('HARVEST_MANAGER','deleteSeeds');
        if(!isset($seedThreshold)) {
            $errorMessage = 'Unable to retrieve seed deletion threshold.';
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        // Render the bulk delete modal content
        return json_encode([
            "html" => $this->renderAjax('_modal_revert_harvest', [
                'occurrenceId' => $occurrenceId,
                'locationId' => $locationId,
                'recordIds' => $recordInfo,
                'recordInfo' => $recordInfo,
                'dataLevel' => $dataLevel,
                'seedThreshold' => $seedThreshold,
                'bulkDeleteConfig' => $bulkDeleteConfig,
            ]),
            "count" => count($recordInfo)
        ]);
    }

    /**
     * Update harvest status of plots or crosses
     */
    public function actionUpdateRecordsStatus() {
        $recordIds = isset($_POST['recordIds']) ? (array) json_decode($_POST['recordIds']) : [];
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $validateStatus = isset($_POST['validateStatus']) ? boolval($_POST['validateStatus']) : false;
        $occurrenceId = isset($_POST['occurrenceId']) ? intval($_POST['occurrenceId']) : 0;
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : '';

        // Update occurrence status
        $this->harvestDataModel->updateOccurrenceStatus($occurrenceId);

        // Update records status
        return json_encode($this->harvestDataModel->updateRecordsStatus($recordIds, $status, $occurrenceId, $dataLevel, $validateStatus));
    }

    /**
     * Retrieve background job information
     */
    public function actionGetBackgroundJob() {
        $jobId = isset($_POST['jobId']) ? $_POST['jobId'] : 0;

        return json_encode($this->backgroundJob->getOne($jobId));
    }

    /**
     * Retrieve datased ids to be deleted
     */
    public function actionGetUncommittedTraitIds() {
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $selectedAbbrevs = isset($_POST['selectedAbbrevs']) ? json_decode($_POST['selectedAbbrevs']) : null;
        $locationId = isset($_POST['locationId']) ? $_POST['locationId'] : null;

        return json_encode($this->harvestDataModel->getUncommittedTraitIds($recordIds, $selectedAbbrevs, $locationId));
    }

    /**
     * Delete dataset records via processor
     */
    public function actionDeleteDatasets() {
        $datasetIds = isset($_POST['datasetIds']) ? (array) json_decode($_POST['datasetIds']) : [];
        $locationId = isset($_POST['locationId']) ? intval($_POST['locationId']) : 0;

        return json_encode($this->harvestDataModel->deleteTransactionDatasetsViaProcessor($datasetIds, $locationId));
    }

    /**
     * Checks number of seeds created for the given records
     * @return integer the number of seeds found for the given plots/crosses
     */
    public function actionGetSeedCount() {
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : 'plot';

        return $this->harvestDataModel->getSeedCount($recordIds, $dataLevel);
    }

    /**
     * Checks if the given seeds have been used in entries
     * @return array - contains plots ids segregated into:
     *                  1) plots whose seeds have been used in entries
     *                  2) plots whose seeds haven't been used in entries
     */
    public function actionCheckSeedEntries() {
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : 'plot';

        return json_encode($this->harvestDataModel->checkSeedEntries($recordIds,$dataLevel));
    }

    /**
     * Retrieve db ids to delete
     * @return array - database ids of packages, seeds, germplasm names,
     *  germplasm, and plot data to delete
     */
    public function actionGetDatabaseIds() {
        $recordDbIds = isset($_POST['recordDbIds']) ? json_decode($_POST['recordDbIds']) : [];
        $selectedAbbrevs = isset($_POST['selectedAbbrevs']) ? json_decode($_POST['selectedAbbrevs']) : [];
        $hasSeeds = isset($_POST['hasSeeds']) ? ($_POST['hasSeeds'] == 'true' ? true : false) : null;
        $skipData = isset($_POST['skipData']) ? ($_POST['skipData'] == 'true' ? true : false) : false;
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : 'plot';

        return json_encode($this->harvestDataModel->getDatabaseIdsToDelete($recordDbIds,$selectedAbbrevs,$dataLevel,$hasSeeds,$skipData));
    }

    /**
     * Delete records via processor
     */
    public function actionDeleteViaProcessor() {
        $databaseIds = isset($_POST['databaseIdStr']) ? (array) json_decode($_POST['databaseIdStr']) : [];
        $locationDbId = isset($_POST['locationId']) ? intval($_POST['locationId']) : 0;
        $deleteTraits = isset($_POST['deleteTraits']) ? ($_POST['deleteTraits'] === 'true' ? true: false) : true;
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : 'plot';
        
        return json_encode($this->harvestDataModel->performDeleteViaProcessor($databaseIds,$locationDbId,$deleteTraits,$dataLevel));
    }

    /**
     * Get background job info
     */
    public function actionGetBackgroundJobs() {
        $backgroundJobIds = isset($_POST['backgroundJobIds']) ? (array) json_decode($_POST['backgroundJobIds']) : [];

        return json_encode($this->harvestDataModel->getBackgroundJobs($backgroundJobIds));
    }

    /**
     * Render acknowledement message to show
     * plots that have seeds used in entries
     */
    public function actionAcknowledgeRecords() {
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : null;
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : 'plot';
        $fieldName = $dataLevel . 'DbId';
        
        $recordInfo = [];
        $recordCount = 0;
        if($dataLevel == 'plot') {
            $requestBody = [
                $fieldName => "equals " . implode("|equals ",$recordIds)
            ];
            $recordInfo = $this->plotBrowserModel->searchAllPlots(
                $occurrenceId,
                $requestBody,
                ['sort=plotNumber:asc','limit=10']
            );
        }
        else if ($dataLevel == 'cross') {
            $requestBody = [
                $fieldName => "equals " .implode("||equals ",$recordIds)
            ];
            $recordInfo = $this->crossBrowserModel->searchAllCrosses(
                $occurrenceId,
                $requestBody,
                ['crossDbId:desc','limit=10']
            );
        }
        $recordCount = count($recordInfo);

        return $this->renderAjax('_modal_bulk_delete_acknowledge', [
            'recordInfo' => $recordInfo,
            'recordCount' => $recordCount,
            'dataLevel' => $dataLevel
        ]);
    }

    /**
     * Perform deletion of harvest data in the background
     */
    public function actionDeleteInBackground() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $deletionMode = isset($_POST['deletionMode']) ? $_POST['deletionMode'] : 'harvest data';
        $parameters = [
            "recordIds" => isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [],
            "selectedAbbrevs" => isset($_POST['selectedAbbrevs']) ? json_decode($_POST['selectedAbbrevs']) : [],
            "dataLevel" => isset($_POST['dataLevel']) ? $_POST['dataLevel'] : 'plot'
        ];

        return json_encode($this->harvestManagerModel->deleteInBackground($occurrenceId, $deletionMode, $parameters));
        
    }

    /**
     * Renders the bulk delete modal content
     */
    public function actionRenderObservationDataModal() {
        $config = isset($_POST['config']) ? (array) json_decode($_POST['config']) : [];
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : null;

        // Build checkboxes
        $configVariables = isset($config['all_variables']) ? (array) $config['all_variables'] : [];
        // Build select2 data
        $select2Data = [];
        foreach($configVariables as $variable) {
            $field_name = $variable->field_name;
            $placeholder = $variable->placeholder;
            $select2Data[$field_name] = $placeholder;
        }

        // Retrieve current filter values from session
        $sessionVar = \Yii::$app->cbSession->get("observation-filter-$dataLevel-$occurrenceId");
        $currentValues = [];
        $withData = false;
        if(!empty($sessionVar)) {
            $observationFilter = isset($sessionVar) ? (array) json_decode($sessionVar) : [];
            $currentValues = (array) $observationFilter['fields'];
            $withData = $observationFilter['withData'];
        }

        // Render the bulk delete modal content
        return $this->renderAjax('_modal_observation_data', [
            'select2Data' => $select2Data,
            'occurrenceId' => $occurrenceId,
            'dataLevel' => $dataLevel,
            'currentValues' => $currentValues,
            'withData' => $withData,
        ]);
    }

    /**
     * Saves the specified session variable into the Yii app session
     */
    public function actionSaveToSession() {
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $value = isset($_POST['value']) ? $_POST['value'] : '';
        \Yii::$app->cbSession->set($name, $value);
    }

    /**
     * Remove one variable from the currently applied filters
     */
    public function actionRemoveObservationVariable() {
        $sessionName = isset($_POST['sessionName']) ? $_POST['sessionName'] : '';
        $toRemove = isset($_POST['toRemove']) ? $_POST['toRemove'] : '';

        $sessionVar = \Yii::$app->cbSession->get($sessionName);
        $observationFilter = [];
        if(!empty($sessionVar)) {
            $observationFilter = isset($sessionVar) ? (array) json_decode($sessionVar) : [];
            $currentValues = (array) $observationFilter['fields'];
            $displayNames = (array) $observationFilter['displayNames'];

            // Remove from arrays
            $key = array_search($toRemove, $currentValues);
            unset($currentValues[$key]);
            unset($displayNames[$key]);

            // If arrays are empty, set session variable to empty
            if(empty($currentValues) && empty($displayNames)) {
                \Yii::$app->cbSession->set($sessionName, '');
                return;
            }

            // Else, update session variable
            $observationFilter['fields'] = $currentValues;
            $observationFilter['displayNames'] = $displayNames;
            \Yii::$app->cbSession->set($sessionName, json_encode($observationFilter));
        }
    }

    /**
     * Toggles the current view mode of the observation data filters in session
     */
    public function actionToggleFilterMode() {
        $sessionName = isset($_POST['sessionName']) ? $_POST['sessionName'] : '';
        $sessionVar = \Yii::$app->cbSession->get($sessionName);
        $observationFilter = [];
        if(!empty($sessionVar)) {
            $observationFilter = isset($sessionVar) ? (array) json_decode($sessionVar) : [];
            $withData = $observationFilter['withData'];

            // If withData is true, change to false
            if($withData) $observationFilter['withData'] = false;
            // If withData is false, change to true
            else $observationFilter['withData'] = true;
            // Update session variable
            \Yii::$app->cbSession->set($sessionName, json_encode($observationFilter));
        }
    }

    /**
     * Retrieve background process information given the ID
     */
    public function actionGetBackgroundProcessInfo(){
        $bgProcessId = isset($_POST['bgProcessId']) && !empty($_POST['bgProcessId']) ? $_POST['bgProcessId'] : null;

        if(isset($bgProcessId)){
            $params = [
                'backgroundJobDbId' => 'equals '.$bgProcessId,
                'workerName' => 'equals BuildCsvData'
            ];
            $result = $this->backgroundJob->searchAll($params, "sort=modificationTimestamp:desc");

            if($result['status'] == 200 && isset($result['data']) && !empty($result['data'])){
                $filename = $result['data'][0]['jobRemarks'].".csv";

                return json_encode(['filename' => $filename]);
            }
        }
        else {
            return json_encode(['filename' => '']);
        }
    }

    /**
     * Download data from filepath 
     * 
     * @param String $filename Name of file to be downloaded
     */
    public function actionDownloadData($filename){
        $filename = $_POST['filename'] ?? $filename;
        $pathToFile = realpath(Yii::$app->basePath) . "/files/data_export/$filename";

        if (file_exists($pathToFile)) {
            // Set headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($pathToFile));

            // Flush system output buffer
            flush();

            // Read/Download CSV file
            readfile($pathToFile);

            die();
        }
    }

    /**
     * Retrieves all records with FAILED harvest status
     */
    public function actionGetFailedRecords() {
        $occurrenceId = $_POST['occurrenceId'] ?? 0;
        $dataLevel = $_POST['dataLevel'] ?? '';

        // Retrieve records
        $records = [];

        if($dataLevel == 'plot') {
            $requestBody = [
                "fields" => "plot.id AS id|plot.harvest_status AS harvestStatus",
                "harvestStatus" => "FAILED%"
            ];

            $records = $this->plotBrowserModel->searchAllPlots(
                $occurrenceId,
                $requestBody
            );
        }
        else if($dataLevel == 'cross') {
            $requestBody = [
                "fields" => "germplasmCross.id AS id|germplasmCross.harvest_status AS harvestStatus",
                "harvestStatus" => "FAILED%"
            ];

            $records = $this->crossBrowserModel->searchAllCrosses(
                $occurrenceId,
                $requestBody
            );
        }

        return json_encode($records);
    }

    /**
     * Inserts plot or cross data
     */
    public function actionInsertData() {
        $recordIds = isset($_POST['recordIds']) ? json_decode($_POST['recordIds']) : [];
        $variableAbbrevs = isset($_POST['variableAbbrevs']) ? json_decode($_POST['variableAbbrevs']) : [];
        $values = isset($_POST['values']) ? json_decode($_POST['values']) : [];
        $dataLevel = $_POST['dataLevel'] ?? 'plot';
        $variableAbbrevId = [];

        // Get variable ids
        $result = $this->variableModel->searchAll(
            [
                "abbrev" => "equals " . implode("|equals ",$variableAbbrevs)
            ],
            '',
            true
        );
        // Get variable info
        $data = $result['data'] ?? [];
        foreach($data as $d) {
            $variableAbbrevId[$d['abbrev']] = $d['variableDbId'];
        }

        // Build request body
        $requestBody = [
            "records" => []
        ];
        foreach($recordIds as $id) {
            foreach($variableAbbrevs as $abbrev) {
                $recordItem = [
                    "dataQCCode" => "Q"
                ];
                $recordItem[$dataLevel."DbId"] = "$id";
                $recordItem["variableDbId"] = "$variableAbbrevId[$abbrev]";
                $recordItem["dataValue"] = $values[array_search($abbrev, $variableAbbrevs)];

                $requestBody['records'][] = $recordItem;
            }
        }

        // Insert Data
        if ($dataLevel == 'plot') {
            $result = $this->plotDataModal->create(
                $requestBody,
                false,
                [],
                'allowMultiple=true'
            );
        } else if ($dataLevel == 'cross') {
            $result = $this->crossDataModal->create(
                $requestBody,
                false,
                [],
                'allowMultiple=true'
            );
        }
    }

    /**
     * Trigger background worker for bulk updating harvest data
     */
    public function actionUpdateHarvestDataInBackground() {
        // Unpack $_POST parameters
        $locationId = isset($_POST['locationId']) ? $_POST['locationId'] : 0;
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $variableAbbrevs = isset($_POST['variableAbbrevs']) ? json_decode($_POST['variableAbbrevs']) : [];
        $values = isset($_POST['values']) ? json_decode($_POST['values']) : [];
        $dataLevel = isset($_POST['dataLevel']) ? $_POST['dataLevel'] : '';
        $bulkUpdateConfig = isset($_POST['bulkUpdateConfig']) ? json_decode($_POST['bulkUpdateConfig']) : [];
        $overwrite = isset($_POST['overwrite']) ? $_POST['overwrite'] : 'false';
        $others = isset($_POST['others']) ? json_decode($_POST['others']) : [];

        // Retrieve filters from the session
        $browserParams = \Yii::$app->cbSession->get($dataLevel . "BrowserParams-$occurrenceId");
        // Set description for backgroundJob
        $backgroundJobDescription = "Update harvest data for occurrence ID: " . $occurrenceId;

        // Invoke worker
        $result = $this->worker->invoke(
            'UpdateHarvestData',                    // Worker name
            $backgroundJobDescription,              // Description
            'OCCURRENCE',                           // Entity
            $occurrenceId,                          // Entity ID
            'OCCURRENCE',                           // Endpoint Entity
            'HARVEST_MANAGER',                      // Application
            'POST',                                 // Method
            [                                       // Worker data
                "occurrenceId" => $occurrenceId,
                "locationId" => $locationId,
                "variableAbbrevs" => $variableAbbrevs,
                "values" => $values,
                "dataLevel" => $dataLevel,
                "bulkUpdateConfig" => $bulkUpdateConfig,
                "overwrite" => $overwrite,
                "browserParams" => $browserParams,
                "others" => $others
            ]
        );

        return json_encode($result);
    }

    /**
     * Trigger the rendering of the harvest summary modal
     */
    public function actionRenderHarvestSummaryModal()
    {
        $dataType = $_POST['dataType'] ?? 'plot';
        $occurrenceId = $_POST['occurrenceId'] ?? 0;
        $recordId = $_POST['recordId'] ?? 0;
        $messageJSON = "";

        // Retrieve record
        $recordInfo = $this->harvestDataModel->getRecordInfo([$recordId], $occurrenceId, $dataType);

        $recordData = isset($recordInfo) && !empty($recordInfo) ? $recordInfo[0] : [];
        $messageJSON = isset($recordData['notes']) ? json_decode($recordData['notes']) : "";

        // Render the harvest summary modal content
        return $this->renderAjax('_modal_harvest_summary', [
            'recordId' => $recordId,
            'recordInfo' => $recordData,
            'messageJSON' => $messageJSON,
            'dataType' => $dataType
        ]);
    }
}
