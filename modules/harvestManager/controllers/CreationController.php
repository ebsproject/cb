<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\controllers;

// Import Yii
use Yii;
// Import components
use app\components\B4RController;
// Import interfaces
use app\interfaces\models\IBackgroundJob;
use app\interfaces\models\IConfig;
use app\interfaces\models\IExperiment;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IUser;
use app\interfaces\models\IWorker;
use app\interfaces\modules\harvestManager\models\ICreationModel;
use app\interfaces\modules\harvestManager\models\ICrossBrowserModel;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel;
use app\interfaces\modules\harvestManager\models\IPlotBrowserModel;
// Import models
use app\modules\harvestManager\models\CreationCrossBrowserModel;
use app\modules\harvestManager\models\CreationPlotBrowserModel;
use app\modules\harvestManager\models\HarvestDataModel;

/**
 * Creation controller for the `harvestManager` module
 */
class CreationController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public IBackgroundJob $backgroundJob,
        public IConfig $configurations,
        public IExperiment $experiment,
        public IOccurrence $occurrence,
        public IUser $user,
        public IWorker $worker,
        public ICreationModel $creationModel,
        public ICrossBrowserModel $crossBrowserModel,
        public IHarvestManagerModel $harvestManagerModel,
        public IOccurrenceDetailsModel $occurrenceDetailsModel,
        public IPlotBrowserModel $plotBrowserModel,
        public CreationCrossBrowserModel $creationCrossBrowserModel,
        public CreationPlotBrowserModel $creationPlotBrowserModel,
        public HarvestDataModel $harvestDataModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Index action of harvest manager's seed and package creation feature
     * @param string program current program code
     * @param string occurrenceId occurrence identifier
     * @param string source previously loaded data level (plot or cross)
     * @param string view type of view (empty for split view, focus_plot/focus_cross for focused view)
     * @return string
     */
    public function actionIndex($program = null, $occurrenceId = null, $source = null, $view = null)
    {
        // Get url
        $url = \Yii::$app->request->getUrl();
        // Validate URL parameters
        $validation = $this->harvestManagerModel->validateUrlParameters(
            [
                'occurrenceId' => $occurrenceId,
                'source' => $source
            ],
            $url
        );
        // If there is an error, set flash message and redirect to HM landing page
        if($validation['error']) {
            Yii::$app->session->setFlash('error', $validation['errorMessage']);
            return $this->redirect(['occurrence-selection/index', 'program' => $program]);
        }

        // get harvest data requirements config
        $harvestDataRequirementsConfig = \Yii::$app->cbSession->get("harvestDataRequirementsConfig");

        // Get occurrence details
        $result = $this->occurrence->searchAll(["occurrenceDbId"=>"equals $occurrenceId"]);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        $occurrenceName = isset($occurrence['occurrenceName']) ? $occurrence['occurrenceName'] : '';
        $occurrenceStatus = isset($occurrence['occurrenceStatus']) ? $occurrence['occurrenceStatus'] : '';
        $occurrenceIsHarvestCompleted = str_contains(strtolower($occurrenceStatus),"harvest completed");
        $accessData = isset($occurrence['accessData']) ? $occurrence['accessData'] : null;
        $creatorDbId = isset($occurrence['creatorDbId']) ? $occurrence['creatorDbId'] : 0;
        // Check if user has access to the occurrence
        $isAdmin = $this->user->isAdmin();
        if ($isAdmin || Yii::$app->access->renderAccess("HARVEST_OCCURRENCE","HARVEST_MANAGER", $creatorDbId, $accessData, 'write', 'url', true))  {
            // Get experiment details
            $experimentId = isset($occurrence['experimentDbId']) ? $occurrence['experimentDbId'] : '';
            $result = $this->experiment->searchAll(["experimentDbId"=>"equals $experimentId"]);
            $experiment = isset($result['data'][0]) ? $result['data'][0] : [];
            $experimentType = isset($experiment['experimentType']) ? $experiment['experimentType'] : '';
            $stage = isset($experiment['stageCode']) ? $experiment['stageCode'] : '';
            $cropCode = isset($experiment['cropCode']) ? $experiment['cropCode'] : '';
            // Get location id
            $locationId = $this->occurrenceDetailsModel->getLocationIdOfOccurrence($occurrenceId);

            // Check if CPN
            $cpnCheck = $this->harvestManagerModel->checkCPN($experimentId);

            // Verify view parameter
            // If invalid view was provided, redirect to default view.
            if(!in_array($view,[null, '', 'focus_plot', 'focus_cross'])) {
                Yii::$app->session->setFlash('error', \Yii::t('app', 'Invalid parameter: View string must be focus_plot, focus_cross, or an empty string.'));
                return $this->redirect([
                    'creation/index',
                    'program' => $program,
                    'occurrenceId' => $occurrenceId,
                    'source' => $source
                ]);
            }


            // Get plot distinct values
            $distinctValuesPlot = $this->harvestDataModel->getDistinctValues(
                'plot',                                 // data level
                [                                       // distinct fields
                    "harvestStatus",
                    "state",
                    "type"
                ],
                $occurrenceId                           // occurrence ID
            );
            // Save distinct values to the session
            \Yii::$app->cbSession->set("hm-distinct-values-plot-".$occurrenceId, $distinctValuesPlot);

            // Get cross distinct values
            $distinctValuesCross = $this->harvestDataModel->getDistinctValues(
                'cross',                                // data level
                [                                       // distinct fields
                    "harvestStatus",
                    "state",
                    "type",
                    "crossMethod"
                ],
                $occurrenceId                           // occurrence ID
            );
            // Save distinct values to the session
            \Yii::$app->cbSession->set("hm-distinct-values-cross-".$occurrenceId, $distinctValuesCross);

            // Get distinct plot states
            $plotStates = $distinctValuesPlot['state'] ?? [];
            // If no states found, set states to an array with 'default' as the only content
            $plotStates = empty($plotStates) ? ['default'] : $plotStates;
            // Get distinct cross states (selfing, if any)
            $crossStates = $distinctValuesCross['state'] ?? [];
            // If no states found, set states to an array with 'default' as the only content
            $crossStates = empty($crossStates) ? ['default'] : $crossStates;
            $states = array_values(array_unique(array_merge($plotStates, $crossStates)));
            $states[] = 'TCV';

            // Get distinct plot types
            $plotTypes = $distinctValuesPlot['type'] ?? [];
            // If no types found, set types to an array with 'default' as the only content
            $plotTypes = empty($plotTypes) ? ['default'] : $plotTypes;
            // Get distinct cross types (selfing, if any)
            $crossTypes = $distinctValuesCross['type'] ?? [];
            // If no types found, set types to an array with 'default' as the only content
            $crossTypes = empty($crossTypes) ? ['default'] : $crossTypes;
            $types = array_values(array_unique(array_merge($plotTypes, $crossTypes)));

            // Get distinct cross methods (selfing, if any)
            $crossMethods = $distinctValuesCross['crossMethod'] ?? [];

            // Retrieve data browser config 
            $config = $this->configurations->getConfigByAbbrev('HM_DATA_BROWSER_CONFIG_'.$cropCode);
            // Get plot browser specific input columns
            $selfingConfig = $config['CROSS_METHOD_SELFING'];
            $configPlot = [
                'CROSS_METHOD_SELFING' => empty($selfingConfig) ? [] : $selfingConfig
            ];
            $pbConfig = $this->harvestDataModel->consumeConfig($configPlot, $states, $types);
            // If no selfing crosses, remove selfing config
            if(!in_array('selfing', $crossMethods)) unset($config['CROSS_METHOD_SELFING']);
            // Consume config
            $browserConfig = $this->harvestDataModel->consumeConfig($config, $states, $types);
            // Copy needed configs from plot browser config
            $browserConfig['input_columns_plot'] = $pbConfig['input_columns'];
            $browserConfig['method_numvar_compat'] = array_merge($browserConfig['method_numvar_compat'],$pbConfig['method_numvar_compat']);
            $browserConfig['method_numvar_requirement_compat'] = array_merge($browserConfig['method_numvar_requirement_compat'],$pbConfig['method_numvar_requirement_compat']);
            $browserConfig['numeric_variables'] = array_merge($browserConfig['numeric_variables'],$pbConfig['numeric_variables']);
            $browserConfig['numeric_variables_field'] = array_merge($browserConfig['numeric_variables_field'],$pbConfig['numeric_variables_field']);
            $browserConfig['CROSS_METHOD_SELFING'] = $pbConfig['CROSS_METHOD_SELFING'];

            // Determine what data needs to be loaded
            $pageUrl = \Yii::$app->request->getUrl();
            $postParams = \Yii::$app->request->post();
            $load = $this->creationModel->determineDataToLoad($pageUrl, $postParams);
            $loadPlots = $load['loadPlots'];
            $loadCrosses = $load['loadCrosses'];

            // Set default visibility of split and focus buttons
            $split = ' hidden';
            $focus = '';

            // Check if either the plot or cross browser is to be hidden (for focused view)
            $plotHidden = '';
            $crossHidden = '';
            if(isset($view)) {
                $plotHidden = $view == 'focus_plot' ? '' : ' hidden';
                $crossHidden = $view == 'focus_cross' ? '' : ' hidden';
            }

            // Retrieve browser data
            $params = Yii::$app->request->getQueryParams();
            // Plots
            $plotSearchModel = $this->creationPlotBrowserModel;
            $plotBrowserData = $plotSearchModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadPlots, $plotHidden == ' hidden');
            $reloadPlots = $loadPlots && $plotHidden != ' hidden';
            // Crosses
            $crossSearchModel = $this->creationCrossBrowserModel;
            $crossBrowserData = $crossSearchModel->search($params, $experimentId, $occurrenceId, $locationId, $browserConfig, $loadCrosses, $crossHidden == ' hidden');
            $reloadCrosses = $loadCrosses && $crossHidden != ' hidden';
            // Set default browser size
            $browserDivSize = 12;
            $twoBrowsers = false;
            if($view == null) $twoBrowsers = true;

            // Final modifications on browser div size and hiding additional buttons
            // If one of the browsers are hidden, set browser div size to 12
            // for the visible browser, and hide the focus button
            if($plotHidden != $crossHidden) {
                $browserDivSize = 12;
                $split = '';
                $focus = ' hidden';
            }
            // If two browsers are in display,
            // set browser div size to 6 and hide the split button
            if($twoBrowsers) {
                $browserDivSize = 6;
                $split = ' hidden';
                $focus = '';
            }

            // Get creation action buttons
            $actionButtons = $this->creationModel->getActionButtons($twoBrowsers, $occurrenceIsHarvestCompleted);
            $actionButtonsFocusMode = $actionButtons;
            if ($twoBrowsers) $actionButtonsFocusMode = '';

            // Generate harvest data columns
            $inputColumns = $this->creationModel->buildCreationColumns($source, $occurrenceId) ?? [];
            
            // Full rendering of the index view file under the 'creation' sub-directory
            return $this->render('index', [
                'program' => $program,
                'occurrenceId' => $occurrenceId,
                'dataLevel' => $source,
                'experimentType' => $experimentType,
                'occurrenceName' => $occurrenceName,
                'plotSearchModel' => $plotSearchModel,
                'plotBrowserData' => $plotBrowserData,
                'crossSearchModel' => $crossSearchModel,
                'crossBrowserData' => $crossBrowserData,
                'browserDivSize' => $browserDivSize,
                'twoBrowsers' => $twoBrowsers,
                'actionButtons' => $actionButtons,
                'actionButtonsFocusMode' => $actionButtonsFocusMode,
                'cpnCheck' => $cpnCheck,
                'controllerModel' => $this->creationModel,
                'split' => $split,
                'focus' => $focus,
                'crossHidden' => $crossHidden,
                'plotHidden' => $plotHidden,
                'reloadCrosses' => $reloadCrosses,
                'reloadPlots' => $reloadPlots,
                'browserConfig' => $browserConfig,
                'view' => $view,
                'stage' => $view,
                'browserId' => ['dynagrid-harvest-manager-plot-crt-grid', 'dynagrid-harvest-manager-cross-crt-grid'],
                'inputColumns' => $inputColumns
            ]);
        }
    }

    /**
     * Action for retrieving the number of plots (and crosses, if applicable)
     * that are ready for harvest record creation.
     */
    public function actionCheckReadyRecords() {
        // Get occurrence ID
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        // Retrieve data
        $result = $this->creationModel->getSummaryDataProvider($occurrenceId, true);
        $dataProvider = isset($result['dataProvider']) ? $result['dataProvider'] : null;
        $readyPlots = isset($result['readyPlots']) ? $result['readyPlots'] : 0;
        $readyCrosses = isset($result['readyCrosses']) ? $result['readyCrosses'] : 0;

        return json_encode([
            "html" => $this->renderAjax('_modal_creation_summary', [
                "harvestCross" => true,
                "dataProvider" => $dataProvider,
                "totalReady" => ($readyPlots + $readyCrosses)
            ]),
            "totalReady" => ($readyPlots + $readyCrosses)
        ]);
    }

    /**
     * Controller action for creating packages, seeds, germplasm,
     * and other harvest-related data. The occurrenceId and harvestCross variables
     * are sent to and consumed by the GenerateHarvestRecords background worker.
     */
    public function actionCreatePackages() {
        // Get occurrence ID
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;

        // Invoke worker
        $result = $this->worker->invoke(
            'GenerateHarvestRecords',                       // Worker name
            'Create packages, seeds, and germplasm for occurrence ID: ' . $occurrenceId,     // Description
            'OCCURRENCE',                                   // Entity
            $occurrenceId,                                  // Entity ID
            'OCCURRENCE',                                   // Endpoint Entity
            'HARVEST_MANAGER',                              // Application
            'POST',                                         // Method
            [                                               // Worker data
                "occurrenceId" => $occurrenceId,
                "harvestCross" => true
            ]
        );

        return json_encode($result);
    }
    
    /**
     * Controller action for committing harvest data
     */
    public function actionCommitHarvestData() {
        $occurrenceId = $_POST['occurrenceId'] ?? 0;

        $datasetValidated = $this->creationModel->commitHarvestData($occurrenceId);

        return json_encode($datasetValidated);
    }

    /**
     * Looks up the background job table for
     * IN QUEUE or IN PROGRESS jobs related to
     * the given occurrence id.
     */
    public function actionCheckHarvestJobs() {
        $occurrenceId = $_POST['occurrenceId'] ?? 0;

        $requestBody = [
            "workerName" => "equals GenerateHarvestRecords",
            "entity" => "equals OCCURRENCE",
            "entityDbId" => "equals $occurrenceId",
            "jobStatus" => "equals IN_QUEUE|equals IN_PROGRESS",
        ];
        // Retrieve jobs
        $result = $this->backgroundJob->searchAll($requestBody, "limit=1&sort=backgroundJobDbId:asc", false);
        // Return total count
        return $result['totalCount'] ?? null;
    }

    /**
     * Retrieves the status of the current occurrence
     */
    public function actionGetOccurrenceStatus() {
        $occurrenceId = $_POST['occurrenceId'] ?? 0;

        // Get occurrence details
        $result = $this->occurrence->searchAll(["occurrenceDbId"=>"equals $occurrenceId"]);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        $occurrenceStatus = isset($occurrence['occurrenceStatus']) ? $occurrence['occurrenceStatus'] : '';

        return $occurrenceStatus;
    }

    /**
     * Updates the occurrence status and occurrence remarks
     */
    public function actionUpdateOccurrence() {
        $occurrenceId = $_POST['occurrenceId'] ?? 0;
        $occurrenceStatus = $_POST['occurrenceStatus'] ?? '';
        $occurrenceRemarks = $_POST['occurrenceRemarks'] ?? '';

        // Identify new status
        $newStatus = '';
        $hasHarvestCompletedStatus = str_contains($occurrenceStatus, 'harvest completed');
        if($hasHarvestCompletedStatus) {
            $statusParts = explode(";", $occurrenceStatus);
            $newStatusParts = [];
            foreach($statusParts as $status) {
                if($status != 'harvest completed') $newStatusParts[] = $status;
            }
            $newStatus = implode(";", $newStatusParts);
        }
        else $newStatus = $occurrenceStatus . ';harvest completed';
        // Build request
        $requestBody = [
            "occurrenceStatus" => $newStatus,
            "remarks" => $occurrenceRemarks . " (" . date('Y-m-d H:i:s O',time()) . ")"
        ];
        // Update occurrence
        return json_encode($this->occurrence->updateOne($occurrenceId, $requestBody));
    }
}
