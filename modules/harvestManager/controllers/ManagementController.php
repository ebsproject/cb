<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\controllers;

// Import Yii
use Yii;
// Import components
use app\components\B4RController;

use app\models\Worker;

// Import interfaces
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IUser;
use app\interfaces\modules\harvestManager\models\IApiHelper;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel;
// Import models
use app\modules\harvestManager\models\HarvestDataModel;
use app\modules\harvestManager\models\ManagementBrowserModel;
use app\modules\germplasm\models\CodingModel;

/**
 * Management controller for the `harvestManager` module
 */
class ManagementController extends B4RController
{
    /**
     * Controller constructor
     */

    public function __construct($id, $module,
        public IOccurrence $occurrence,
        public IUser $user,
        public IApiHelper $apiHelper,
        public IHarvestManagerModel $harvestManagerModel,
        public IOccurrenceDetailsModel $occurenceDetailsModel,
        public HarvestDataModel $harvestDataModel,
        public ManagementBrowserModel $managementBrowserModel,
        public Worker $worker,
        public CodingModel $codingModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Index action of harvest manager's seed and package management feature
     * @param string program current program code
     * @param string occurrenceId occurrence identifier
     * @param string source previously loaded data level (plot or cross)
     * @return string
     */
    public function actionIndex($program = null, $occurrenceId = null, $source = null)
    {
        // Get url
        $url = \Yii::$app->request->getUrl();
        // Validate URL parameters
        $validation = $this->harvestManagerModel->validateUrlParameters(
            [
                'occurrenceId' => $occurrenceId,
                'source' => $source
            ],
            $url
        );
        // If there is an error, set flash message and redirect to HM landing page
        if($validation['error']) {
            Yii::$app->session->setFlash('error', $validation['errorMessage']);
            return $this->redirect(['occurrence-selection/index', 'program' => $program]);
        }

        // Get occurrence details
        $result = $this->occurrence->searchAll(["occurrenceDbId"=>"equals $occurrenceId"]);
        $occurrence = isset($result['data'][0]) ? $result['data'][0] : [];
        $occurrenceName = isset($occurrence['occurrenceName']) ? $occurrence['occurrenceName'] : '';
        $accessData = isset($occurrence['accessData']) ? $occurrence['accessData'] : null;
        $creatorDbId = isset($occurrence['creatorDbId']) ? $occurrence['creatorDbId'] : 0;
        // Check if user has access to the occurrence
        $isAdmin = $this->user->isAdmin();
        if ($isAdmin || Yii::$app->access->renderAccess("HARVEST_OCCURRENCE","HARVEST_MANAGER", $creatorDbId, $accessData, 'write', 'url', true))  {
            // Get experiment details
            $experimentType = (string) $this->harvestDataModel->getOccurrenceExperimentType($occurrenceId);
            $experimentId = $this->occurenceDetailsModel->getExperimentIdOfOccurrence($occurrenceId);
            // Get location id
            $locationId = $this->occurenceDetailsModel->getLocationIdOfOccurrence($occurrenceId);

            // Check if CPN
            $cpnCheck = $this->harvestManagerModel->checkCPN($experimentId);

            // Retrieve data provider
            $params = Yii::$app->request->getQueryParams();
            $searchModel = $this->managementBrowserModel;
            $dataProvider = $searchModel->search($params, $experimentId, $occurrenceId, $locationId);

            // Retrieve scale values
            $germplasmStateScaleValues = $this->harvestManagerModel->getScaleValues('GERMPLASM_STATE');

            // Get bulk delete threshold
            $packagesDeleteThreshold = Yii::$app->config->getAppThreshold('HARVEST_MANAGER','deletePackages');

            // Save program
            Yii::$app->cbSession->set('hm-current-program', $program);

            // Retrieve germplasm coding threshold
            $germplasmCodingThreshold = $this->codingModel->getGermplasmCodingThreshold();

            return $this->render('index', [
                'program' => $program,
                'occurrenceId' => $occurrenceId,
                'dataLevel' => $source,
                'experimentType' => $experimentType,
                'occurrenceName' => $occurrenceName,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'germplasmStateScaleValues' => $germplasmStateScaleValues,
                'cpnCheck' => $cpnCheck,
                'packagesDeleteThreshold' => $packagesDeleteThreshold,
                'createListThreshold' => \Yii::$app->config->getAppThreshold('HARVEST_MANAGER','createList'),
                'totalCount' => \Yii::$app->cbSession->get("packagesTotalCount-$occurrenceId"),
                'browserId' => 'dynagrid-harvest-manager-package-mgmt-grid',
                'germplasmCodingThreshold' => $germplasmCodingThreshold,
                'codingModel' => $this->codingModel
            ]);
        }
        
    }

    /**
     * Preview members when saving new list
     */
    public function actionRetrieveListPreview() {

        $occurrenceId = $_POST['occurrenceId'] ?? 0;
        $packageIdList = isset($_POST['packageIdList']) ? json_decode($_POST['packageIdList'], true) : [];
        $selectionMode = $_POST['selectionMode'] ?? '';
        $data = $this->managementBrowserModel->getListPreviewDataProvider($occurrenceId, $packageIdList, $selectionMode);
        $count = $data['count'];

        $listPreviewThreshold = Yii::$app->config->getAppThreshold('HARVEST_MANAGER','previewList');
        $htmlData = $this->renderAjax('_list_preview', [
            'dataProvider'=> $data['dataProvider'],
            'count' => $count,
        ]);
        
        return json_encode([
            'count' => $count,
            'listPreviewThreshold' => $listPreviewThreshold,
            'htmlData' => $htmlData,
        ]);
    }


    /**
     * Create a new list and its members
     * 
     * @return Object contains the boolean result of creation and string message
     */
    public function actionSaveList() {
        $timeStart = microtime(true);

        $occurrenceId = $_POST['occurrenceId'] ?? 0;
        $ids = isset($_POST['ids']) ? json_decode($_POST['ids'], true) : [];
        $abbrev = isset($_POST['abbrev']) ? trim($_POST['abbrev']) : '';
        $name = isset($_POST['name']) ? trim($_POST['name']) : '';
        $displayName = isset($_POST['displayName']) ? $_POST['displayName'] : '';
        $type = isset($_POST['type']) ? $_POST['type'] : '';
        $description = isset($_POST['description']) ? $_POST['description'] : '';
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : '';
        $selectionMode = isset($_POST['selectionMode']) ? $_POST['selectionMode'] : '';

        $model = $this-> managementBrowserModel;
        $isListValid = $model->validateList($abbrev, $name);
        
        if(!$isListValid['result']) {	// if list is invalid because of existing values
            return json_encode($isListValid);
        }

        $listValues['records'][] = [
            'abbrev' => $abbrev,
            'name' => $name,
            'displayName' => $displayName,
            'type' => $type,
            'description' => $description,
            'remarks' => $remarks,
        ];
        $result = $model->saveList($occurrenceId, $listValues, $ids, $selectionMode);
        $result['totalTime'] = microtime(true) - $timeStart;

        return json_encode($result);
    }

    /**
     * Checks and removes seeds already used in entries from the roster
     */
    public function actionCheckSeedEntries() {
        $seedIds = isset($_POST['seedIds']) ? json_decode($_POST['seedIds'], true) : [];

        return json_encode($this->managementBrowserModel->checkSeedEntries($seedIds));
    }

    /**
     * Checks if the germplasm is used by seeds other than those included in the deletion
     */
    public function actionCheckGermplasm() {
        $seedIdsToDelete = isset($_POST['seedIdsToDelete']) ? json_decode($_POST['seedIdsToDelete'], true) : [];
        $germplasmIdsToDelete = isset($_POST['germplasmIdsToDelete']) ? json_decode($_POST['germplasmIdsToDelete'], true) : [];
        $germplasmSeedRelation = isset($_POST['germplasmSeedRelation']) ? json_decode($_POST['germplasmSeedRelation'], true) : [];

        return json_encode($this->managementBrowserModel->checkGermplasm($seedIdsToDelete, $germplasmIdsToDelete, $germplasmSeedRelation));
    }

    /**
     * Submit ids to delete to the processor API
     */
    public function actionDeleteViaProcessor() {
        $seedIds = isset($_POST['seedIds']) ? json_decode($_POST['seedIds'], true) : [];
        $germplasmIds = isset($_POST['germplasmIds']) ? json_decode($_POST['germplasmIds'], true) : [];
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;

        return json_encode($this->managementBrowserModel->deleteViaProcessor($seedIds, $germplasmIds, $occurrenceId));
    }

    /**
     * Retrieves seed ids harvested from the occurence given its id
     */
    public function actionGetPackageSeedIds() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;

        $requestBody = [
            "fields" => "package.id AS packageDbId|"
                . "seed.id AS seedDbId|"
                . "seed.germplasm_id AS germplasmDbId|"
                . "seed.source_occurrence_id AS sourceOccurrenceId",
            "sourceOccurrenceId" => "equals $occurrenceId"
        ];
        // Set method and endpoint
        $method = 'POST';
        $endpoint = "packages-search";

        // Retrieve packages
        return json_encode($this->apiHelper->sendRequest($method, $endpoint, $requestBody));
        
    }

    /**
     * Retrieves total package count for the occurrence given its ID
     */
    public function actionGetPackageCount() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;

        $requestBody = [
            "fields" => "package.id AS packageDbId"
        ];

        $method = 'POST';
        $endpoint = 'occurrences/' . $occurrenceId . '/packages-search';
        $urlParams = [ 'limit=1' ];

        $results = $this->apiHelper->sendRequestSinglePage($method, $endpoint, $requestBody, $urlParams);

        if($results['status'] !== 200){
            throw new \yii\base\ErrorException(Yii::t('app', $results['message']));
        }

        return isset($results['count']) ? $results['count'] : 0;
    }

    /**
     * Perform deletion of harvest data in the background
     */
    public function actionDeleteInBackground() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;
        $seedIds = isset($_POST['seedIds']) ? json_decode($_POST['seedIds']) : [];
        $deletionMode = 'package';
        $parameters = [
            "seedIds" => $seedIds,
            "columnFilters" => [],
        ];

        return json_encode($this->harvestManagerModel->deleteInBackground($occurrenceId, $deletionMode, $parameters));
        
    }

    /**
     * Facilitate exporting of all seeds and packages in management tab browser
     * 
     * @param Integer $occurrenceId occurrence unique identifier
     * @param String $source data level source - cross/plot
     * @param Integer $recordsCount total number of records to be exported
     */
    public function actionExportPackages($occurrenceId,$source,$recordsCount){
        $exportThreshConfig = Yii::$app->config->getAppThreshold('HARVEST_MANAGER_BG_PROCESSING_THRESHOLD', 'exportPackages');
        $exportThreshold = isset($exportThreshConfig) ? intval($exportThreshConfig) : 2000;

        // build csv columns
        $csvHeaders = [
            'OCURRENCE NAME',
            'PROGRAM',
            'HARVEST SOURCE',
            'PLOT NUMBER',
            'PLOT CODE',
            'DESIGNATION',
            'GERMPLASM CODE',
            'SEED NAME',
            'SEED CODE',
            'LABEL',
            'PACKAGE CODE',
            'QUANTITY',
            'PARENTAGE',
            'GENERATION',
            'GERMPLASM STATE',
            'HARVEST DATE',
            'HARVEST METHOD',
            'ENTRY CODE',
            'REPLICATION',
            'CROSS FEMALE PARENT',
            'FEMALE PLOT NUMBER',
            'FEMALE PLOT CODE',
            'FEMALE PARENTAGE',
            'CROSS MALE PARENT',
            'MALE PARENTAGE'
        ];

        $csvAttributes = [
            'occurrenceName',
            'program',
            'harvestSource',
            'seedSourcePlotNumber',
            'seedSourcePlotCode',
            'designation',
            'germplasmCode',
            'seedName',
            'seedCode',
            'label',
            'packageCode',
            'quantity',
            'parentage',
            'generation',
            'germplasmState',
            'harvestDate',
            'harvestMethod',
            'sourceEntryCode',
            'replication',
            'crossFemaleParent',
            'femalePlotNumber',
            'femalePlotCode',
            'femaleParentage',
            'crossMaleParent',
            'maleParentage'
        ];
        
        $occurrenceInfo = $this->occurrence->get($occurrenceId);
        $occurrenceName = isset($occurrenceInfo['occurrenceCode']) && !empty($occurrenceInfo['occurrenceCode']) ? $occurrenceInfo['occurrenceCode'] : $this->harvestDataModel->getOccurrenceName($occurrenceId);
        $params = \Yii::$app->cbSession->get("dynagrid-harvest-manager-package-mgmt-grid-body");
        
        // extract url params
        $queryParams = \Yii::$app->cbSession->get("packagesQueryParams-$occurrenceId");
        $sortParams = isset($queryParams['sort']) && !empty($queryParams['sort']) ? $queryParams['sort'] : '';

        // build sort parameters
        $tmpStrArr = [];
        if(isset($sortParams) && !empty($sortParams)){
            $tmpStrArr = explode('|',$sortParams);   
            $tmpStrArr = array_map(function ($sortOrder){
                if($sortOrder[0] == '-'){
                    return substr($sortOrder, 1) . ':DESC';
                }
                else{
                    return  $sortOrder . ':ASC';
                }
            },$tmpStrArr);
        }

        if($recordsCount > $exportThreshold){
            $description = 'Preparing seeds and packages CSV for downloading';

            $occurrenceName = str_replace(' ','_',$occurrenceName); // replace spaces with underscore
            $occurrenceName = preg_replace('/[^A-Za-z0-9\-]/', '', $occurrenceName); // remove all special characters
            $timeNow = date("Ymd_His", time());
            $filename = $occurrenceName."_Packages_$timeNow";
            
            $reqBody = [];
            foreach($params as $key => $value){
                $reqBody[$key] = $value;
            };

            $sort = isset($tmpStrArr) && !empty($tmpStrArr) ? '?sort='.implode('|',$tmpStrArr) : '';
            $url = "occurrences/$occurrenceId/packages-search".$sort;

            $this->worker->invoke(
                'BuildCsvData',
                $description,
                'OCCURRENCE',
                "$occurrenceId",
                'OCCURRENCE',
                'HARVEST_MANAGER',
                'POST',
                [
                    'url' => $url,
                    'requestBody' => json_encode($reqBody),
                    'description' => $description,
                    'csvHeaders' => $csvHeaders,
                    'csvAttributes' => $csvAttributes,
                    'fileName' => $filename,
                    'httpMethod' => 'POST',
                ],
                [
                    'remarks' => $filename
                ]
            );

            // Reload page
            Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i>&nbsp; The background process for exporting the data is in progress. Click on the notification to download the file once the process has been completed.');
            
            $program = Yii::$app->cbSession->get('hm-current-program');
            return $this->redirect([
                'management/index',
                'program' => $program,
                'occurrenceId' => $occurrenceId,
                'source' => $source
            ]);
        }
        else{
            $this->managementBrowserModel->exportPackages(
                $params,
                $tmpStrArr,
                $occurrenceId,
                $occurrenceName,
                $csvHeaders,
                $csvAttributes
            );
        }

        exit;

    }

    /**
     * Render acknowledgement modal when there are seed and packages that cannot be deleted
     */
    public function actionRenderBulkDeleteAcknowledgeModal() {
        // Get variables
        $occurrenceId = $_POST['occurrenceId'] ?? 0;
        $seedIdsWithEntries = $_POST['seedIdsWithEntries'] ?? [];
        $seedsToDeleteCount = $_POST['seedsToDeleteCount'] ?? 0;

        // Get seed and package info
        $result = $this->managementBrowserModel->getPackageRecords(
            [
                "seedDbId" => "equals " . implode("|equals ", $seedIdsWithEntries)
            ],
            [],
            $occurrenceId
        );
        $records = $result['data'] ?? [];
        $recordCount = $result['count'] ?? 0;

        // Render modal content
        return $this->renderAjax('_modal_bulk_delete_acknowledge', [
            "records" => $records,
            "recordCount" => $recordCount,
            "seedsToDeleteCount" => $seedsToDeleteCount
        ]);

    }
}
