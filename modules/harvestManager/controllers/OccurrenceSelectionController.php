<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\harvestManager\controllers;

use Yii;
// Import components
use app\components\B4RController;
use app\controllers\Dashboard;
// Import interfaces
use app\interfaces\models\IConfig;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IUser;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel;
// Import models
use app\modules\harvestManager\models\SearchParametersModel;

/**
 * Occurrence selection controller for the `harvestManager` module
 */
class OccurrenceSelectionController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public Dashboard $dashboard,    
        public IConfig $configurations,
        public IOccurrence $occurrence,
        public IUser $user,
        public IHarvestManagerModel $harvestManagerModel,
        public IOccurrenceDetailsModel $occurrenceDetailsModel,
        public SearchParametersModel $searchParametersModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Index action of harvest manager's occurrence selection view
     * @param string program current program code
     * @return string
     */
    public function actionIndex($program = null)
    {
        // Get dashboard filters
        $dashboardFilters = (array) $this->dashboard->getFilters();
        $programDbId = $dashboardFilters['program_id'];
        $programCode = $this->harvestManagerModel->getProgram($programDbId);
        // Get occurrence id
        $occurrenceDbId = $_GET['occurrenceId'] ?? 0;
        $isAdmin = $this->user->isAdmin();

        // Check if program in dashboard matches current occurrence program
        $requestBody = [
            "occurrenceDbId" => "equals $occurrenceDbId",
        ];
        $result = $this->occurrence->searchAll($requestBody);
        $occurrenceInfo = isset($result['data'][0]) ? $result['data'][0] : [];
        $occurrenceProgramId = $occurrenceInfo['programDbId'] ?? 0;
        // If the occurrence's program id does not match the dashboard program id,
        // redirect to index with only the dashboard program
        if($isAdmin && $occurrenceDbId != 0 && $programDbId != $occurrenceProgramId) {
            return $this->redirect([
                'occurrence-selection/index',
                'program' => $programCode
            ]);
        }
        // Check access
        $accessData = $occurrenceInfo['accessData'] ?? null;
        $creatorDbId = $occurrenceInfo['creatorDbId'] ?? 0;
        if ($isAdmin || $occurrenceDbId == 0 || Yii::$app->access->renderAccess("HARVEST_OCCURRENCE","HARVEST_MANAGER", $creatorDbId, $accessData, 'write', 'url', true))  {
            // set filter values
            $filterValues = [
                [
                    'abbrev' => 'PROGRAM',
                    'values' => $programDbId.''
                ],
                [
                    'abbrev' => 'EXPERIMENT_NAME',
                    'values' => '0'
                ],
                [
                    'abbrev' => 'OCCURRENCE_NAME', 
                    'values' => $occurrenceDbId.''
                ]
            ];

            // Generate search parameter select2
            $dataUrl = \yii\helpers\Url::to(['occurrence-selection/get-filter-data']);
            $searchParameters = $this->searchParametersModel->generateFilterFields($dataUrl, $filterValues);
            
            // render the index view file
            return $this->render(
                'index',
                [
                    'searchParameters' => $searchParameters, 
                    'program' => $program,
                    'occurrenceId' => $occurrenceDbId,
                    'dataLevel' => ''
                ]
            );
        }
    }

    /**
     * Get select2 options
     * @param string q - search value
     * @param string id - element id
     * @param integer page - pagination
     * @param array filters - search param filter configs
     */
    public function actionGetFilterData($q = null, $id = null, $page = null, $filters = null)
    {
        $configSearch = $this->configurations->getConfigByAbbrev('HM_SEARCH_PARAMETERS');
        $configVar = $configSearch['values'];
        $userId = $this->user->getUserId();

        $existingFilters = json_decode($filters, true);
        
        $defaultFilters = (array) $this->dashboard->getFilters();
        unset($defaultFilters['program_id']);

        $variableFilters = $this->searchParametersModel->extractVariableFilters($existingFilters, $configVar, $id);

        $limit = 5;
        $page = ($page==null ? 1 : $page);

        $data = $this->searchParametersModel->getVariableFilterData($q, $userId, $id, $variableFilters, $defaultFilters, $limit, $page);

        return json_encode(['totalCount' => $data['count'], 'items' => $data['data']]);
    }

    /**
     * Retrieves occurrences that belong to the experiment
     */
    public function actionGetExperimentOccurrences() {
        $experimentId = isset($_POST['experimentId']) ? $_POST['experimentId'] : 0;

        return $this->searchParametersModel->getExperimentOccurrences($experimentId);
    }

    /**
     * Retrieves the experiment where the occurrence belongs to
     */
    public function actionGetOccurrenceExperiment() {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;

        return $this->searchParametersModel->getOccurrenceExperiment($occurrenceId);
    }

    /**
     * Renders the occurrence details in occurrence selection tab
     */
    public function actionGetOccurrenceDetails() {
        // Get occurrence details html
        $result = $this->actionRenderOccurrenceDetails(true);
        $occurrenceDetails = $result['occurrenceDetails'];
        $data = $result['data'];

        // Get experiment details
        $experimentType = isset($data['occurrenceDetailsTab1']['Experiment Type']) ? $data['occurrenceDetailsTab1']['Experiment Type'] : '';

        return json_encode(
            array(
                'occurrenceDetails' => $occurrenceDetails,
                'experimentType' => $experimentType
            )
        );
    }

    /**
     * Renders the occurrence details in other tabs
     * @param boolean fromController whether or not the request is from this controller or not
     */
    public function actionRenderOccurrenceDetails($fromController = false) {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : 0;

        // retrieve occurrence details
        if($occurrenceId != '') {
            $data = $this->occurrenceDetailsModel->getOccurrenceDetails($occurrenceId);
        }
        else $data = [];

        // If request is from controller, return non-encoded array
        if($fromController) {
            return array(
                "occurrenceDetails" => $this->renderAjax('../occurence_details', [
                    'data' => $data,
                    'divSize' => '4',
                    'occurrenceTab' => true
                ]),
                "data" => $data
            );
        }

        // Return encoded array to view file
        return json_encode(
            array(
                "occurrenceDetails" => $this->renderAjax('../occurence_details', [
                    'data' => $data,
                    'divSize' => '4',
                    'occurrenceTab' => false
                ]),
                "data" => $data
            )
        );
    }
}
