<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.	If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Add Crosses tab
 */
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\GermplasmInfo;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

// render tabs
echo Yii::$app->controller->renderPartial('/tabs', [
    'entryListId' => $entryListId,
    'entryListName' => $entryListName,
    'program' => $program,
    'active' => 'add-crosses',
    'experimentDbId' => $experimentDbId,
    'entryListStatus' => $entryListStatus
]);
?>

<?php
$maxCrossCountDisplay = number_format($maxCrossCount);
$selectedFemaleCount = count($selectedFemaleItems) > 0 ? number_format(count($selectedFemaleItems)) : 'No';
$selectedMaleCount = count($selectedMaleItems) > 0 ? number_format(count($selectedMaleItems)) : 'No';
$notSet = '<span class="not-set">(not set)</span>';

$addCrossesUrl = Url::to(['/crossManager/add-crosses/load', 'program'=>$program, 'id'=>$entryListId]);
$manageCrossesUrl = Url::to(['/crossManager/manage-crosses/load', 'program'=>$program, 'id'=>$entryListId]);
$selfCrossesUrl = Url::to(['/crossManager/add-crosses/self-crosses', 'program'=>$program, 'id'=>$entryListId]);
$saveCrossesUrl = Url::to(['/crossManager/add-crosses/save-crosses', 'program'=>$program, 'id'=>$entryListId]);
$pairParentsUrl = Url::to(['/crossManager/add-crosses/pair-parents', 'program'=>$program, 'id'=>$entryListId]);
$storeSessionItemsUrl = Url::to(['/crossManager/add-crosses/store-session-items']);
$getSelectedItemsCountUrl = Url::to(['/crossManager/add-crosses/get-user-selection-count', 'id'=>$entryListId]);
$resetFemaleBrowserUrl = url::to(['/crossManager/add-crosses/load', 'program'=>$program, 'id'=>$entryListId, 'reset'=>'female_hard_reset']);
$resetMaleBrowserUrl = url::to(['/crossManager/add-crosses/load', 'program'=>$program, 'id'=>$entryListId, 'reset'=>'male_hard_reset']);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$urlCheckRequiredFields = Url::to(['/crossManager/add-crosses/check-required-fields/', 'program' => $program,'id' => $entryListId]);
$urlExportGermplasmAttr = Url::to(['/crossManager/add-crosses/export-germplasm-attr/', 
    'program' => $program,'id' => $entryListId]);

$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings(['dynagrid-cm-male-grid','dynagrid-cm-female-grid']);
?>

<div id = "cm-parent-list" class = "row col col-sm-12 col-md-12 col-lg-12" style = "margin:0px; padding-right:0px;background-color:white;">
    <p style='margin:10px 10px 0px 10px;'><?= \Yii::t('app' , 'Pair parents by selecting from the female and male browsers. Click "Add" to add pairings to the cross list. Max of <strong>'.$maxCrossCountDisplay.'</strong> crosses per validation.');?></p>

    <!-- female data browser -->
    <div class="col col-sm-4 col-md-4 col-lg-4 parent-box" style="padding-left: 10px !important;">
        <?php
            $femaleHeaderColor = 'red lighten-4';
            $femaleContentColor = 'red lighten-5';

            $columns = [
                [
                    'label'=> "Checkbox",
                    'header' => '
                      <input type="checkbox" class="filled-in" id="dynagrid-cm-female-grid-select-all" style="margin:0px; padding:0px;"/>
                      <label for="dynagrid-cm-female-grid-select-all" style="margin:0px; padding:0px 20px 0px 0px;"></label>',
                    'content' => function($data) use ($experimentDbId, $selectedFemaleItems) {
                        $femaleId = empty($experimentDbId) ? $data['parentDbId'] : $data['entryDbId'];
                        $gridId = "cm_female_$femaleId";
                        if (isset($selectedFemaleItems[$femaleId])) {
                            $checkbox = '<input class="dynagrid-cm-female-grid-select filled-in" type="checkbox" checked="checked" id="'.$gridId.'" value="'.$data["occurrenceCode"].':'.$data["entryCode"].'"/>';
                        } else {
                            $checkbox = '<input class="dynagrid-cm-female-grid-select filled-in" type="checkbox" id="'.$gridId.'" value="'.$data["occurrenceCode"].':'.$data["entryCode"].'"/>';
                        }
                        return $checkbox.'<label for="'.$gridId.'" style="margin:0px; padding:0px 20px 0px 0px;"></label>';
                    },
                    'contentOptions'=>['class' => $femaleContentColor],
                    'hAlign' => 'center',
                    'vAlign' => 'top',
                    'mergeHeader'=>true,
                    'order'=> DynaGrid::ORDER_FIX_LEFT,
                ],
                [
                    'attribute' => 'entryNumber',
                    'label' => '<span title="Sequential numbering of the entries">'.Yii::t('app', 'Entry No.').'</span>',
                    'headerOptions'=>['class' => $femaleHeaderColor],
                    'contentOptions'=>['class' => $femaleContentColor],
                    'format' => 'raw',
                    'encodeLabel'=> false,
                    'order' => DynaGrid::ORDER_FIX_LEFT
                ],
                [
                    'attribute' => 'entryCode',
                    'headerOptions' => ['class' => $femaleHeaderColor],
                    'contentOptions' => ['class' => $femaleContentColor],
                    'order' => DynaGrid::ORDER_FIX_LEFT
                ],
                [
                    'attribute' => 'designation',
                    'label' => '<span title="Germplasm">'.Yii::t('app', 'Germplasm Name').'</span>',
                    'headerOptions' => ['class' => $femaleHeaderColor],
                    'contentOptions' => ['class' => $femaleContentColor],
                    'format' => 'raw',
                    'encodeLabel' => false,
                    'value' => function($model){
                        $linkAttrsTemp = [
                            'title' => \Yii::t('app', 'View Germplasm information'),
                            'data-label' => $model['designation'],
                            'data-id' => $model['germplasmDbId'],
                            'data-target' => '#view-germplasm-widget-modal',
                            'data-toggle' => 'modal'
                        ];
                        $linkNameId = ['id' => 'view-germplasm-widget-'.$model['germplasmDbId']];
                        $linkAttrsName = array_merge($linkNameId, $linkAttrsTemp);
            
                        return GermplasmInfo::widget([
                            'id' => $model['germplasmDbId'],
                            'linkAttrs' => $linkAttrsName,
                            'entityLabel' => $model['designation']
                        ]);
                    }
                ],
                [
                    'attribute'=>'femaleCount',
                    'label' => '<span title="'.Yii::t('app', 'Number of crosses this parent was used as a female.').'"># <b style="display:none">♀</b><i class="fa fa-venus" style="font-size:120%"></i></span>',
                    'headerOptions'=>['class'=>'red lighten-4'],
                    'contentOptions'=>['class'=>'red lighten-5'],
                    'format' => 'raw',
                    'vAlign'=>'middle',
                    'encodeLabel'=>false,
                ],
                [
                    'attribute'=>'maleCount',
                    'label' => '<span title="'.Yii::t('app', 'Number of crosses this parent was used as a male.').'"># <b style="display:none">♂</b><i class="fa fa-mars" style="font-size:120%"></i></span>',
                    'headerOptions'=>['class'=>'red lighten-4'],
                    'contentOptions'=>['class'=>'red lighten-5'],
                    'format' => 'raw',
                    'vAlign'=>'middle',
                    'encodeLabel'=>false,
                ],
                [
                    'attribute' => 'experimentName',
                    'headerOptions'=>['class' => $femaleHeaderColor],
                    'contentOptions'=>['class' => $femaleContentColor],
                    'visible' => false
                ],
                [
                    'attribute' => 'experimentCode',
                    'headerOptions'=>['class' => $femaleHeaderColor],
                    'contentOptions'=>['class' => $femaleContentColor],
                    'visible' => false
                ],
                [
                    'attribute' => 'occurrenceName',
                    'headerOptions'=>['class' => $femaleHeaderColor],
                    'contentOptions'=>['class' => $femaleContentColor],
                    'visible' => false,
                    'filterInputOptions' => [
                        'disabled' => !empty($experimentDbId)
                    ],
                ],
                [
                    'attribute' => 'occurrenceCode',
                    'headerOptions'=>['class' => $femaleHeaderColor],
                    'contentOptions'=>['class' => $femaleContentColor],
                    'filterInputOptions' => [
                        'disabled' => !empty($experimentDbId)
                    ],
                ],
                [
                    'attribute' => 'parentRole',
                    'headerOptions'=>['class' => $femaleHeaderColor],
                    'contentOptions'=>['class' => $femaleContentColor],
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter'=>['' => '--Select--',
                      'female' => 'female',
                      'female-and-male' => 'female-and-male'],
                    'visible' => false
                ],
                [
                    'attribute' => 'parentage',
                    'label' => '<span title="Pedigree Information">'.Yii::t('app', 'Parentage').'</span>',
                    'headerOptions'=>['class' => $femaleHeaderColor],
                    'contentOptions'=>['class' => $femaleContentColor],
                    'format' => 'raw',
                    'encodeLabel'=>false,
                    'visible' => false
                ],
                [
                    'attribute' => 'generation',
                    'headerOptions'=>['class' => $femaleHeaderColor],
                    'contentOptions'=>['class' => $femaleContentColor],
                    'visible' => false
                ],
                [
                    'attribute' => 'germplasmState',
                    'headerOptions'=>['class' => $femaleHeaderColor],
                    'contentOptions'=>['class' => $femaleContentColor],
                    'visible' => false
                ],
            ];

        $selfButton = '<a data-type="female" class="self-btn light-green darken-3 btn btn-success" style="margin-right:5px;" title="'
            .Yii::t('app','Create self crosses for Female parents').'"><b>'.\Yii::t('app','Self').'</b></a>';

            DynaGrid::begin([
                'columns' => $columns,
                'theme' => 'simple-default',
                'showPersonalize' => true,
                'storage' => DynaGrid::TYPE_SESSION,
                'showFilter' => false,
                'showSort' => false,
                'allowFilterSetting' => false,
                'allowSortSetting' => false,
                'gridOptions' => [
                    'id' => 'dynagrid-cm-female-grid-id',
                    'filterModel'=> $parentModel,
                    'dataProvider' => $femaleDataProvider,
                    'showPageSummary' => false,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'dynagrid-cm-female-grid-id',
                            'enablePushState' => false
                        ],
                        'beforeGrid' => '',
                        'afterGrid' => ''
                    ],
                    'responsive' => false,
                    'panel' => [
                        'heading' =>  false,
                        'before' => '
                            <strong class="larger-text" style="margin-left:-10px;">'.\Yii::t('app',' Female').'&emsp;'.'</strong>{summary}<br/>
                            <span id="selected-items" class="pull-right" style="margin-top:-3px">
                                <div class="hidden preloader-wrapper female-loader small active" style="width:10px;height:10px;">
                                    <div class="spinner-layer spinner-green-only">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="gap-patch">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                <b id="selectedFemaleCount">'.$selectedFemaleCount.'</b><span id="selectedFemaleText"> selected items</span>.
                            </span>
                        ',
                        'after' => false,
                    ],
                    'toolbar' => [
                        [
                            'content' =>
                                $selfButton.
                                Html::a('<i class="glyphicon glyphicon-repeat"></i>',
                                    $resetFemaleBrowserUrl,
                                    [
                                        'id' => 'reset-dynagrid-cm-female-grid',
                                        'class' => 'btn btn-default', 
                                        'title' => \Yii::t('app','Reset female data browser'),
                                        'data-pjax' => true
                                    ]
                                ).'{dynagrid}'
                                .Html::a(
                                    '<i class="material-icons inline-material-icon">file_download</i>',
                                    '',
                                    [
                                        'data-pjax' => true,
                                        'id' => 'export-female-germplasm-attr-btn',
                                        'class' => 'btn btn-default',
                                        'style' => 'margin-left: 5px;',
                                        'title' => \Yii::t('app', 'Export female germplasm attributes in CSV format')
                                    ]
                                )
                        ],
                    ],
                    'floatHeader' => true,
                    'floatOverflowContainer' =>  true,
                    'resizableColumns' => true,
                    'pager' => [
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last'
                    ]
                ],
                'submitButtonOptions' => [
                    'icon' => 'glyphicon glyphicon-ok',
                ],
                'deleteButtonOptions' => [
                    'icon' => 'glyphicon glyphicon-remove',
                    'label' => Yii::t('app', 'Remove'),
                ],
                'options' => [
                    'id' => 'dynagrid-cm-female-grid'
                ]
            ]);
            DynaGrid::end();
        ?>
    </div>

    <!-- cross input text area -->
    <div class="cross-input col-sm-4 col-md-4 col-lg-4" style="padding:5px 5px 10px 5px; background-color:white;">
        <strong class='larger-text'><?=\Yii::t('app',' Crosses');?></strong><strong id="cross-count" class="pull-right">0</strong><strong class="pull-right" title="Combinations of selected parents and input crosses"><?=\Yii::t('app','Total:');?></strong>
        <?= Html::textArea('crosses-textarea',"",[
            'id'=>'crosses-textarea',
            'style'=>'background-color:white; resize: none; !important;',
            'placeholder'=>"Parent format: ENTRY_CODE or OCCURRENCE_CODE:ENTRY_CODE\n\nUse pipe ('|') to separate female parent and male parent \nlike the following format:\nENTRY_CODE1|ENTRY_CODE2\nENTRY_CODE3|ENTRY_CODE4\nOCCURRENCE_CODE1:ENTRY_CODE5|OCCURRENCE_CODE2:ENTRY_CODE6"
        ]); ?>
        <button id="pair-btn" disabled class="btn waves-effect waves-light teal" title = "Match female and male parents" style="margin: 5px 0px 0px 0px;">
            <strong><?=\Yii::t('app','Pair');?></strong>
        </button>
        <button id="clear-cross-btn" disabled class="btn waves-effect waves-light black-text grey lighten-2" title="Clear Input List" style="margin: 5px 0px 0px 5px;">
            <strong><?=\Yii::t('app','Clear');?></strong>
        </button>
        <button id="add-cross-btn" disabled class="btn waves-effect waves-light teal pull-right" title = "Add Crosses" style="margin: 5px 0px 0px 5px;">
            <strong><?=\Yii::t('app','Add');?></strong>
        </button>
        <?php
            //Modal for validation messages
            Modal::begin([
                'id' => 'validation-modal',
                'header' => '<h4 id= "validation-header"></h4>',
                'footer' => Html::a(\Yii::t('app', ' No '), '#', ['id' => 'no-btn', 'data-dismiss' => 'modal', 'class' => 'btn btn-primary waves-effect waves-light modal-close black-text grey lighten-2', 'style' => 'display:none'])
                    . '&nbsp;' . Html::a(\Yii::t('app', ' Yes '), '#', ['id' => 'yes-btn', 'class' => 'btn btn-primary waves-effect waves-light modal-close teal', 'style' => 'display:none'])
                    . Html::a(\Yii::t('app', ' Back '), '#', ['id' => 'back-btn', 'data-dismiss' => 'modal', 'class' => 'btn btn-primary waves-effect waves-light modal-close black-text grey lighten-2', 'style' => 'display:none']) . '&nbsp;'
                    . Html::a(\Yii::t('app', 'Ok'), '#', ['id' => 'ok-btn', 'data-dismiss' => 'modal', 'class' => 'btn btn-primary waves-effect waves-light modal-close', 'style' => 'display:none'])
                    . '&nbsp;' . Html::a(\Yii::t('app', 'Proceed'), '#', ['id' => 'clear-btn', 'class' => 'btn btn-primary waves-effect waves-light modal-close teal', 'style' => 'display:none'])
                    . '&nbsp;' . Html::a(\Yii::t('app', 'Add more'), '#', ['id' => 'add-more-btn', 'data-dismiss' => 'modal', 'class' => 'btn black-text grey lighten-2 waves-effect waves-light modal-close', 'style' => 'display:none'])
                    . '&nbsp;&nbsp;' . Html::a(\Yii::t('app', 'Manage crosses'), $manageCrossesUrl, ['id' => 'manage-crosses-btn', 'class' => 'btn btn-primary waves-effect waves-light modal-close', 'style' => 'display:none']),
                'size' => 'modal-md',
                'options' => ['data-backdrop' => 'static'],
                'closeButton' => ['class' => 'hidden'],
            ]);
            echo '<div id="validation-body"></div>';
            Modal::end();
        ?>
    </div>

    <!-- male data browser -->
    <div class="col col-sm-4 col-md-4 col-lg-4 parent-box" style="padding-left: 10px !important;">
    <?php
            $maleHeaderColor = 'blue lighten-4';
            $maleContentColor = 'blue lighten-5';

            $columns = [
                [
                    'label'=> "Checkbox",
                    'header' => '
                      <input type="checkbox" class="filled-in" id="dynagrid-cm-male-grid-select-all" style="margin:0px; padding:0px;"/>
                      <label for="dynagrid-cm-male-grid-select-all" style="margin:0px; padding:0px 20px 0px 0px;"></label>',
                    'content'=> function($data) use ($experimentDbId, $selectedMaleItems) {
                        $maleId = empty($experimentDbId) ? $data['parentDbId'] : $data['entryDbId'];
                        $gridId = "cm_male_$maleId";
                        if (isset($selectedMaleItems[$maleId])) {
                            $checkbox = '<input class="dynagrid-cm-male-grid-select filled-in" type="checkbox" checked="checked" id="'.$gridId.'" value="'.$data["occurrenceCode"].':'.$data["entryCode"].'"/>';
                        } else {
                            $checkbox = '<input class="dynagrid-cm-male-grid-select filled-in" type="checkbox" id="'.$gridId.'" value="'.$data["occurrenceCode"].':'.$data["entryCode"].'"/>';
                        }
                        return $checkbox.'<label for="'.$gridId.'" style="margin:0px; padding:0px 20px 0px 0px;"></label>';
                    },
                    'contentOptions'=>['class' => $maleContentColor],
                    'hAlign' => 'center',
                    'vAlign' => 'top',
                    'mergeHeader'=>true,
                    'order'=> DynaGrid::ORDER_FIX_LEFT,
                ],
                [
                    'attribute' => 'entryNumber',
                    'label' => '<span title="Sequential numbering of the entries">'.Yii::t('app', 'Entry No.').'</span>',
                    'headerOptions'=>['class' => $maleHeaderColor],
                    'contentOptions'=>['class' => $maleContentColor],
                    'format' => 'raw',
                    'encodeLabel'=> false,
                    'order' => DynaGrid::ORDER_FIX_LEFT
                ],
                [
                    'attribute' => 'entryCode',
                    'headerOptions' => ['class' => $maleHeaderColor],
                    'contentOptions' => ['class' => $maleContentColor],
                    'order' => DynaGrid::ORDER_FIX_LEFT
                ],
                [
                    'attribute' => 'designation',
                    'label' => '<span title="Germplasm">'.Yii::t('app', 'Germplasm Name').'</span>',
                    'headerOptions' => ['class' => $maleHeaderColor],
                    'contentOptions' => ['class' => $maleContentColor],
                    'format' => 'raw',
                    'encodeLabel' => false,
                    'value' => function($model){
                        $linkAttrsTemp = [
                            'title' => \Yii::t('app', 'View Germplasm information'),
                            'data-label' => $model['designation'],
                            'data-id' => $model['germplasmDbId'],
                            'data-target' => '#view-germplasm-widget-modal',
                            'data-toggle' => 'modal'
                        ];
                        $linkNameId = ['id' => 'view-germplasm-widget-'.$model['germplasmDbId']];
                        $linkAttrsName = array_merge($linkNameId, $linkAttrsTemp);
            
                        return GermplasmInfo::widget([
                            'id' => $model['germplasmDbId'],
                            'linkAttrs' => $linkAttrsName,
                            'entityLabel' => $model['designation']
                        ]);
                    }
                ],
                [
                    'attribute'=>'femaleCount',
                    'label' => '<span title="'.Yii::t('app', 'Number of crosses this parent was used as a female.').'"># <b style="display:none">♀</b><i class="fa fa-venus" style="font-size:120%"></i></span>',
                    'headerOptions'=>['class'=>'blue lighten-4'],
                    'contentOptions'=>['class'=>'blue lighten-5'],
                    'format' => 'raw',
                    'vAlign'=>'middle',
                    'encodeLabel'=>false,
                ],
                [
                    'attribute'=>'maleCount',
                    'label' => '<span title="'.Yii::t('app', 'Number of crosses this parent was used as a male.').'"># <b style="display:none">♂</b><i class="fa fa-mars" style="font-size:120%"></i></span>',
                    'headerOptions'=>['class'=>'blue lighten-4'],
                    'contentOptions'=>['class'=>'blue lighten-5'],
                    'format' => 'raw',
                    'vAlign'=>'middle',
                    'encodeLabel'=>false,
                ],
                [
                    'attribute' => 'experimentName',
                    'headerOptions'=>['class' => $maleHeaderColor],
                    'contentOptions'=>['class' => $maleContentColor],
                    'visible' => false
                ],
                [
                    'attribute' => 'experimentCode',
                    'headerOptions'=>['class' => $maleHeaderColor],
                    'contentOptions'=>['class' => $maleContentColor],
                    'visible' => false
                ],
                [
                    'attribute' => 'occurrenceName',
                    'headerOptions'=>['class' => $maleHeaderColor],
                    'contentOptions'=>['class' => $maleContentColor],
                    'visible' => false,
                    'filterInputOptions' => [
                        'disabled' => !empty($experimentDbId)
                    ],
                ],
                [
                    'attribute' => 'occurrenceCode',
                    'headerOptions'=>['class' => $maleHeaderColor],
                    'contentOptions'=>['class' => $maleContentColor],
                    'filterInputOptions' => [
                        'disabled' => !empty($experimentDbId)
                    ],
                ],
                [
                    'attribute' => 'parentRole',
                    'headerOptions'=>['class' => $maleHeaderColor],
                    'contentOptions'=>['class' => $maleContentColor],
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter'=>['' => '--Select--',
                      'male' => 'male',
                      'female-and-male' => 'female-and-male'],
                    'visible' => false
                ],
                [
                    'attribute' => 'parentage',
                    'label' => '<span title="Pedigree Information">'.Yii::t('app', 'Parentage').'</span>',
                    'headerOptions'=>['class' => $maleHeaderColor],
                    'contentOptions'=>['class' => $maleContentColor],
                    'format' => 'raw',
                    'encodeLabel'=>false,
                    'visible' => false
                ],
                [
                    'attribute' => 'generation',
                    'headerOptions'=>['class' => $maleHeaderColor],
                    'contentOptions'=>['class' => $maleContentColor],
                    'visible' => false
                ],
                [
                    'attribute' => 'germplasmState',
                    'headerOptions'=>['class' => $maleHeaderColor],
                    'contentOptions'=>['class' => $maleContentColor],
                    'visible' => false
                ],
            ];

            $selfButton = '<a data-type="male" class="self-btn light-green darken-3 btn btn-success" style="margin-right:5px;" title="'
                .Yii::t('app','Create self crosses for Male parents').'"><b>'.\Yii::t('app','Self').'</b></a>';

            DynaGrid::begin([
                'columns' => $columns,
                'theme' => 'simple-default',
                'showPersonalize' => true,
                'storage' => DynaGrid::TYPE_SESSION,
                'showFilter' => false,
                'showSort' => false,
                'allowFilterSetting' => false,
                'allowSortSetting' => false,
                'gridOptions' => [
                    'id' => 'dynagrid-cm-male-grid-id',
                    'filterModel'=> $maleModel,
                    'dataProvider' => $maleDataProvider,
                    'showPageSummary' => false,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'dynagrid-cm-male-grid-id',
                            'enablePushState' => false
                        ],
                        'beforeGrid' => '',
                        'afterGrid' => ''
                    ],
                    'responsive' => false,
                    'panel' => [
                        'heading' =>  false,
                        'before' =>'
                            <strong class="larger-text" style="margin-left:-10px;">'.\Yii::t('app',' Male').'&emsp;'.'</strong>{summary}<br/>
                            <span id="selected-items" class="pull-right" style="margin-top:-3px">
                                <div class="hidden preloader-wrapper male-loader small active" style="width:10px;height:10px;">
                                    <div class="spinner-layer spinner-green-only">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="gap-patch">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                <b id="selectedMaleCount">'.$selectedMaleCount.'</b><span id="selectedMaleText"> selected items</span>.
                            </span>
                        ',
                        'after' => false,
                    ],
                    'toolbar' => [
                        [
                            'content' =>
                                $selfButton.
                                Html::a('<i class="glyphicon glyphicon-repeat"></i>',
                                    $resetMaleBrowserUrl,
                                    [
                                        'id' => 'reset-dynagrid-cm-male-grid',
                                        'class' => 'btn btn-default', 
                                        'title' => \Yii::t('app','Reset male data browser'),
                                        'data-pjax' => true
                                    ]
                                ).'{dynagrid}'
                                .Html::a(
                                    '<i class="material-icons inline-material-icon">file_download</i>',
                                    '',
                                    [
                                        'data-pjax' => true,
                                        'id' => 'export-male-germplasm-attr-btn',
                                        'class' => 'btn btn-default',
                                        'style' => 'margin-left: 5px;',
                                        'title' => \Yii::t('app', 'Export male germplasm attributes in CSV format')
                                    ]
                                )
                        ],
                    ],
                    'floatHeader' => true,
                    'floatOverflowContainer' =>  true,
                    'resizableColumns' => true,
                    'pager' => [
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last'
                    ]
                ],
                'submitButtonOptions' => [
                    'icon' => 'glyphicon glyphicon-ok',
                ],
                'deleteButtonOptions' => [
                    'icon' => 'glyphicon glyphicon-remove',
                    'label' => Yii::t('app', 'Remove'),
                ],
                'options' => [
                    'id' => 'dynagrid-cm-male-grid'
                ]
            ]);
            DynaGrid::end();
        ?>
    </div>

</div>

<?php

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ['dynagrid-cm-male-grid','dynagrid-cm-female-grid'],
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS
resize();
disableNextButton();


$(window).resize(function() {
    resize();
});

// Reload grid, after pjax success of browser
$(document).on('ready pjax:success', '#dynagrid-cm-female-grid-pjax, #dynagrid-cm-male-grid-pjax', function(e) {
    e.stopPropagation();
    e.preventDefault();
    resize();
    disableNextButton();

});

// Update female and male count, after pjax completion of browser
$(document).on('ready pjax:complete', '#dynagrid-cm-female-grid-pjax, #dynagrid-cm-male-grid-pjax', function(e) {
    e.stopPropagation();
    e.preventDefault();
    disableNextButton();
    
    let femaleCount = getSessionUserSelection('female_entry_ids');
    setSelectedParentCounter(femaleCount, 'selectedFemaleCount', 'selectedFemaleText');
    // TODO: Persist select all state if (total female == selected female)
    
    let maleCount = getSessionUserSelection('male_entry_ids');
    setSelectedParentCounter(maleCount, 'selectedMaleCount', 'selectedMaleText');
    // TODO: Persist select all state if (total male == selected male)
});

//retrieve and update data browser
$(document).ready(function() {
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();
    disableNextButton();
});

// Selfing
$(document).on('click', '.self-btn', function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    let notif = '';
    
    let dataType = $(this).attr('data-type');
    let counter = dataType === 'male' ? '#selectedMaleCount' : '#selectedFemaleCount';
    let selectedParentCount = parseInt($(counter).html());
    if (selectedParentCount > 0) {
        showSelfModal();
        setTimeout(function() { 
            $.ajax({
                url: '$selfCrossesUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    sessionName: dataType+'_entry_ids',
                    experimentDbId: '$experimentDbId'
                },
                success: function(response) {
                    $('#validation-modal').modal('hide');
                    if (response > 0) {
                        notif = "<i class='material-icons green-text left'>done</i>Successfully added self crosses!";
                        if (response !== selectedParentCount) {
                            notif = notif + '<br/>Some of the parents have already been used in selfing.';
                        }
                        $.pjax.reload({container: '#dynagrid-cm-female-grid-pjax'}).done(function() {
                            $.pjax.reload({container: '#dynagrid-cm-male-grid-pjax'});
                        });
                    } else {
                        notif = "<i class='material-icons red-text left'>close</i>Selected parent/s have already been used in selfing!";
                    }
                    
                    $('.toast').css('display','none');
                    Materialize.toast(notif, 5000);
                },
                error: function(e) {
                    showRefreshModal();
                }
            });
        }, 300);
    } else {
        notif = '<i class="material-icons orange-text left">warning</i>No selected '+dataType+' parents.';
        $('.toast').css('display','none');
        Materialize.toast(notif, 5000);
    }
});

// Clear crosses action
$(document).on('click', '#clear-cross-btn', function(e) {
    var header = "<h4><i class='material-icons left'>delete_sweep</i>Clear input crosses</h4>";
    var message = '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> This will erase <b>ALL</b> the crosses in the text area.'
    
    // Set footer options
    $('#clear-btn, #back-btn').css('display','');
    $('#ok-btn, #no-btn, #add-more-btn, #manage-crosses-btn, #yes-btn').css('display','none');
    
    // Set modal display
    $('#validation-header').html(header);
    $('#validation-body').html(message);
    $('#validation-modal').modal('show');
});

// Reset text area confirmation
$(document).on('click', '#clear-btn', function(e) {
    $('#validation-modal').modal('hide');
    resetTextArea();
});

// Add cross action
$(document).on('click', '#add-cross-btn', function() {
    let crossCount = $('#cross-count').html();
    
    if (crossCount > $maxCrossCount) {
        showLimitModal();
    } else if (crossCount > 0) {
        let textAreaVal = $('#crosses-textarea').val();
        showAddModal();
        
        setTimeout(function() {
            $.ajax({
                url: '$saveCrossesUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    crosses: textAreaVal,
                    experimentDbId: '$experimentDbId'
                },
                success: function(data) {
                    resetTextArea();
                    
                    if (data['invalidCount'] > 0) {
                        let crossInput = '';
                        $.each(data['crosses'], function(key, value) {
                            if (value['status'] === 'invalid') {
                                cnt = (typeof cnt == 'undefined') ? 1 : cnt + 1;
                                if (crossInput === '') {
                                    crossInput = value['crossInput'];
                                } else {
                                    crossInput = crossInput+'\\n' + value['crossInput'];
                                }
                                $('#row'+ cnt).html("<div title = '" + value['remarks']  + "'class='lineno lineselect'><i class='smaller fa fa-question-circle'></i> " + cnt + "</div>");
                            }
                        });
                        var cnt = 0;
                        $("#crosses-textarea").val(crossInput);
                        $('#cross-count').html(data['invalidCount']);
                        toggleTextAreaActionButtons();
                    } else {
                        resetSelection();
                    }
                    
                    let header = '<i class="glyphicon glyphicon-list-alt"></i> Crosses Creation Summary';

                    // Set footer options
                    $('#add-more-btn').css('display','');
                    $('#manage-crosses-btn').css('display','');
                    $('#clear-btn, #ok-btn, #back-btn, #yes-btn, #no-btn').css('display','none');
                    
                    $('#validation-header').html(header);
                    $('#validation-body').html(data['message']);
                },
                error: function(e) {
                    showRefreshModal();
                }
            });
        }, 300);
    } else {
        // crossCount should not be negative
    }
});

//refresh male and female browser upon adding
$(document).on('click', '#add-more-btn', function(e) {
    $.pjax.reload({container: '#dynagrid-cm-female-grid-pjax'}).done(function () {
        $.pjax.reload({container: '#dynagrid-cm-male-grid-pjax'});
    });
});

// Select/unselect all female parents
$(document).on('click', '#dynagrid-cm-female-grid-select-all', function(e) {
    let unset = false;
    
    if ($(this).prop("checked") === true) {
        $(this).attr('checked','checked');
        $('.dynagrid-cm-female-grid-select').attr('checked', 'checked');
        $('.dynagrid-cm-female-grid-select').prop('checked', true);
        $('.dynagrid-cm-female-grid-select:checkbox').parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5');
    } else {
        unset = true;
        $(this).removeAttr('checked');
        $('.dynagrid-cm-female-grid-select').prop('checked', false);
        $('.dynagrid-cm-female-grid-select').removeAttr('checked');
        $("input:checkbox.dynagrid-cm-female-grid-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');    
    }
    
    $('.female-loader').removeClass('hidden');
    $('#selectedFemaleCount').html('');
    
    $.ajax({
        url: '$storeSessionItemsUrl',
        type: 'post',
        data: {
            unset: unset,
            all: true, 
            entryListDbId: '$entryListId',
            sessionName: 'female_entry_ids',
            experimentDbId: '$experimentDbId'
        },
        cache: false,
        success: function(response) {
            $('.female-loader').addClass('hidden');
            setSelectedParentCounter(response, 'selectedFemaleCount', 'selectedFemaleText');
            toggleTextAreaActionButtons();
        },
        error: function(e) {
            console.log(e);
        }
    });
});

// Click row in female browser
$(document).on('click', '#dynagrid-cm-female-grid tbody tr', function(e) {
    let this_row = $(this).find('input:checkbox')[0];
    let unset = false;
    let entryDbId = parseInt(this_row.id.split('_')[2]);
    var entryData = $('#' + this_row.id).val();
    
    if (this_row.checked) {
        unset = true;
        this_row.checked = false;  
        $('#dynagrid-cm-female-grid-select-all').prop("checked", false);
        $(this_row.id).prop("checked", false);  
        $(this).find('td.red.dynagrid-cm-female-grid-id').addClass('lighten-5').removeClass('lighten-4');
    } else {
        this_row.checked = true;
        $(this_row.id).prop("checked");
        $(this).find('td.red.dynagrid-cm-female-grid-id').addClass('lighten-4').removeClass('lighten-5');
    }
    
    $.ajax({
        url: '$storeSessionItemsUrl',
        type: 'post',
        data: {
            unset: unset,
            all: false,
            entryListDbId: '$entryListId',
            sessionName: 'female_entry_ids',
            entryDbId: entryDbId,
            entryData: entryData
        },
        success: function(response) {
            setSelectedParentCounter(response, 'selectedFemaleCount', 'selectedFemaleText');
            toggleTextAreaActionButtons();
        },
        error: function(e) {
            console.log(e);
        }
    });
});

// Select/unselect all male parents
$(document).on('click', '#dynagrid-cm-male-grid-select-all', function(e) {
    let unset = false;
    
    if ($(this).prop("checked") === true) {
        $(this).attr('checked','checked');
        $('.dynagrid-cm-male-grid-select').attr('checked', 'checked');
        $('.dynagrid-cm-male-grid-select').prop('checked', true);
        $('.dynagrid-cm-male-grid-select:checkbox').parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5');
    } else {
        unset = true;
        $(this).removeAttr('checked');
        $('.dynagrid-cm-male-grid-select').prop('checked', false);
        $('.dynagrid-cm-male-grid-select').removeAttr('checked');
        $("input:checkbox.dynagrid-cm-male-grid-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');    
    }
    
    $('.male-loader').removeClass('hidden');
    $('#selectedMaleCount').html('');
    
    $.ajax({
        url: '$storeSessionItemsUrl',
        type: 'post',
        data: {
            unset: unset,
            all: true, 
            entryListDbId: '$entryListId',
            sessionName: 'male_entry_ids',
            experimentDbId: '$experimentDbId'
        },
        cache: false,
        success: function(response) {
            $('.male-loader').addClass('hidden');
            setSelectedParentCounter(response, 'selectedMaleCount', 'selectedMaleText');
            toggleTextAreaActionButtons();
        },
        error: function(e) {
            console.log(e);
        }
    });
});

// Click row in male browser
$(document).on('click', '#dynagrid-cm-male-grid tbody tr', function(e) {
    let this_row = $(this).find('input:checkbox')[0];
    let unset = false;
    let entryDbId = parseInt(this_row.id.split('_')[2]);
    var entryData = $('#' + this_row.id).val();
    
    if (this_row.checked) {
        unset = true;
        this_row.checked = false;  
        $('#dynagrid-cm-male-grid-select-all').prop("checked", false);
        $(this_row.id).prop("checked", false);  
        $(this).find('td.blue.dynagrid-cm-male-grid-id').addClass('lighten-5').removeClass('lighten-4');
    } else {
        this_row.checked = true;
        $(this_row.id).prop("checked");
        $(this).find('td.blue.dynagrid-cm-male-grid-id').addClass('lighten-4').removeClass('lighten-5');
    }
    
    $.ajax({
        url: '$storeSessionItemsUrl',
        type: 'post',
        data: {
            unset: unset,
            all: false,
            entryListDbId: '$entryListId',
            sessionName: 'male_entry_ids',
            entryDbId: entryDbId,
            entryData: entryData
        },
        success: function(response) {
            setSelectedParentCounter(response, 'selectedMaleCount', 'selectedMaleText');
            toggleTextAreaActionButtons();
        },
        error: function(e) {
            console.log(e);
        }
    });
});

// Pair cross action
$(document).on('click', '#pair-btn', function(e) {
    showPairModal();
    toggleTextAreaActionButtons();
    
    setTimeout(function() {
        $.ajax({
            url: '$pairParentsUrl',
            type: 'post',
            dataType: 'json',
            data: {
                experimentDbId: '$experimentDbId'
            },
            success: function(response) {
                $('.toast').css('display','none');
                
                let textAreaVal = $('#crosses-textarea').val();
                let crossText = response['crossString'];
                let invalidCrossCount = parseInt(response['invalidCrossCount']);
                
                if (textAreaVal !== '') {
                    textAreaVal = $.trim(textAreaVal) + '\\r\\n';
                }
                $('#crosses-textarea').val(textAreaVal + crossText);
                
                if (crossText !== '') {
                    var notif = "<i class='material-icons green-text left'>done</i>Added to the text area.";
                    if (invalidCrossCount > 0) {
                        notif = notif + '<br/>Some crosses are not added</br>because they are invalid.'
                    }
                } else {
                    var notif = "<i class='material-icons orange-text left'>warning</i>Same parent germplasm. Please use SELF feature for selfing.";
                }
                
                $("#crosses-textarea").animate({
                    scrollTop:$("#crosses-textarea")[0].scrollHeight - $("#crosses-textarea").height()
                }, 0, notifyPair(notif,e));
                
                let updatedCrossCount = $('#crosses-textarea').val().split('\\n').filter(Boolean).length;
                $("#cross-count").html(updatedCrossCount);
            },
            error: function(e) {
                showRefreshModal();
            }
        });
    }, 300);
    
});

$('#export-female-germplasm-attr-btn, #export-male-germplasm-attr-btn').on('click',function(){
    $('#system-loading-indicator').css('display','none')
})

$('#export-female-germplasm-attr-btn').off('click').on('click', function (e) {
    e.preventDefault()
    let urlExportGermplasmAttr = '$urlExportGermplasmAttr'
    let exportGermplasmAttrUrl = window.location.origin + urlExportGermplasmAttr + '&role=female&entryListName='
    + '$entryListName' + '&experimentId=' +' $experimentDbId'

    message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
    $('.toast').css('display','none')
    Materialize.toast(message, 5000)

    window.location.replace(exportGermplasmAttrUrl)

    $('#system-loading-indicator').html('')
})

$('#export-male-germplasm-attr-btn').off('click').on('click', function (e) {
    e.preventDefault()
    let urlExportGermplasmAttr = '$urlExportGermplasmAttr'
    let exportGermplasmAttrUrl = window.location.origin + urlExportGermplasmAttr + '&role=male&entryListName='
    + '$entryListName' + '&experimentId=' +' $experimentDbId'

    message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
    $('.toast').css('display','none')
    Materialize.toast(message, 5000)

    window.location.replace(exportGermplasmAttrUrl)

    $('#system-loading-indicator').html('')
})

// Check text area on change
$('#crosses-textarea').on('input change keyup', function() {
    let textAreaVal = $(this).val();
    
    // Update total cross count
    let crossCount = textAreaVal.split('\\n').filter(Boolean).length;
    $('#cross-count').html(crossCount);
    
    toggleTextAreaActionButtons();
});

/**
* Retrieves user's session selection count
*/
function getSessionUserSelection(sessionName) {
    var userSelection;
    $.ajax({
        type: 'POST',
        url: '$getSelectedItemsCountUrl',
        data: { sessionName: sessionName },
        async: false,
        success: function(response) {
            userSelection = response;
        }
    });
    return parseInt(userSelection);
  }

/**
* Sets selected parent counter
*/
function setSelectedParentCounter(response, countId, textId) {
    let count = parseInt(response).toLocaleString();
    let text = ' selected items';
    if (count === '1') {
        text = ' selected item';
    } else if (count === '0') {
        count = 'No';
    }
    $('#' + countId).html(count);
    $('#' + textId).html(text);
}

/**
* Displays modal for showing cross limit
*/
function showLimitModal() {
    $('#validation-header').html("<i class='material-icons orange-text left'>warning</i>Exceeded maximum crosses");		
    $('#validation-body').html("Only <b>$maxCrossCountDisplay</b> crosses are allowed per validation. Please remove the excess crosses in the text area before adding it to the cross list.");
    $('#ok-btn').css('display','');
    $('#clear-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-crosses-btn').css('display','none');
    $('#validation-modal').modal('show');
}

/**
* Displays modal for refreshing page
*/
function showRefreshModal() {
    $('#validation-header').html("<i class='material-icons red-text left'>error</i>Crosses not found!");		
    $('#validation-body').html('There was a problem while adding crosses. Please refresh page.');
    $('#ok-btn').css('display','');
    $('#clear-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-crosses-btn').css('display','none');
    $('#validation-modal').modal('show');
}

/**
* Displays modal for self-cross
*/
function showSelfModal() {
    $('#validation-header').html('Adding self crosses...');		
    $('#validation-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    $('#ok-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-crosses-btn').css('display','none');
    $('#validation-modal').modal('show');
}

/**
* Displays modal when pairing parents
*/
function showPairModal() {
    $('#validation-header').html('Pairing parents...');
    $('#validation-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    $('#clear-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-crosses-btn').css('display','none');
    $('#validation-modal').modal('show');
}

/**
* Displays modal when adding crosses
*/
function showAddModal() {
    $('#validation-header').html('Adding crosses...');
    $('#validation-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    $('#clear-btn, #ok-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-crosses-btn').css('display','none');
    $('#validation-modal').modal('show');
}

/**
* Displays toast notification once pairing is done
*/
function notifyPair(notif,e) {
    setTimeout(function() {
      $('#validation-modal').modal('hide');
      toggleTextAreaActionButtons();
      var hasToast = $('body').hasClass('toast');
      if (!hasToast) {
        $('.toast').css('display','none');
        Materialize.toast(notif, 5000);
      }
      e.preventDefault();
      e.stopImmediatePropagation();
    }, 300);
}


/**
 * Adjusts element sizes to window height
 */
function resize(){
    var maxHeight = ($(window).height() - 320);
    $("#cm-parent-list").css("height", maxHeight);
    $("#dynagrid-cm-female-grid-pjax .kv-grid-wrapper, #dynagrid-cm-male-grid-pjax .kv-grid-wrapper").css("height", maxHeight - 30);
    $("#crosses-textarea").css("height", (maxHeight - 15));
    $(".lines").css("height", (maxHeight - 15));
    $("#crosses-textarea").css("width", $('.linedwrap').width() - $('.codelines').width() - 15);
    $("input:checkbox.dynagrid-cm-female-grid-select").each(function() {
        if ($(this).prop("checked") === true) {
            $(this).parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5');
        }
    });
    $("input:checkbox.dynagrid-cm-male-grid-select").each(function() {
        if ($(this).prop("checked") === true) {
            $(this).parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5');
        }
    });
}

/**
* Resets selected items from female and male browsers
*/
function resetSelection() {
    $.ajax({
        url: '$storeSessionItemsUrl',
        type: 'post',
        data: {
            unset: true,
            all: true, 
            entryListDbId: '$entryListId',
            sessionName: 'female_entry_ids',
            experimentDbId: '$experimentDbId'
        },
        cache: false,
        success: function(response) {
            $('.female-loader').addClass('hidden');
            setSelectedParentCounter(response, 'selectedFemaleCount', 'selectedFemaleText');
            toggleTextAreaActionButtons();
            resetSelectionMale();
        },
        error: function(e) {
            console.log(e);
        }
    });
}

/**
* Resets selected items from male browser
*/
function resetSelectionMale() {
    $.ajax({
        url: '$storeSessionItemsUrl',
        type: 'post',
        data: {
            unset: true,
            all: true, 
            entryListDbId: '$entryListId',
            sessionName: 'male_entry_ids',
            experimentDbId: '$experimentDbId'
        },
        cache: false,
        success: function(response) {
            $('.male-loader').addClass('hidden');
            setSelectedParentCounter(response, 'selectedMaleCount', 'selectedMaleText');
            toggleTextAreaActionButtons();
        },
        error: function(e) {
            console.log(e);
        }
    });
}

/**
 * Resets text area 
 */
function resetTextArea() {
    $("#crosses-textarea").val('');
    $("#cross-count").html(0);
    
    resetLineNumbers();
    toggleTextAreaActionButtons();
}

/**
* Enables/Disables buttons below the text area
*/
function toggleTextAreaActionButtons() {
    if ($('#crosses-textarea').val().trim() !== '') {
        $('#add-cross-btn, #clear-cross-btn').prop('disabled', false);
    } else {
        $('#add-cross-btn, #clear-cross-btn').prop('disabled', true);
        resetLineNumbers();
    }
    $('#pair-btn').prop('disabled', checkSelectedParents());
}

/** 
* Checks if there's at least 1 male and 1 female selected 
*/
function checkSelectedParents() {
    var male, female = false;			
    
    let femaleCount = parseInt($('#selectedFemaleCount').html());
    if (femaleCount > 0) {
        female = true;
    }
    
    let maleCount = parseInt($('#selectedMaleCount').html());
    if (maleCount > 0) {
        male = true;
    }
    return !(female && male);
}

/**
* Resets the line numbers beside the text area
*/
function resetLineNumbers() {
    var lineCount = $('.lineno').length;
      for(i = 1; i <= lineCount; i++){
        $('#row' + i ).html(i);
      }
}

/**
 * Add line numbers in the text area
 * Copyright (c) 2010 Alan Williamson
 * 
 * NOTE: Same functions used in EC
 */
(function($) {
    $.fn.linedtextarea = function(options) {
    
    // Get the Options
    var opts = $.extend({}, $.fn.linedtextarea.defaults, options);
    
    
    /*
    * Helper function to make sure the line numbers are always
    * kept up to the current system
    */
    var fillOutLines = function(codeLines, h, lineNo){
        while ((codeLines.height() - h) <= 0 ){
            if (lineNo == opts.selectedLine){
                codeLines.append("<div class='lineno lineSelect'><i class='fa fa-info-circle'></i> " + lineNo + "</div>");
            }else{
                codeLines.append("<div id = 'row" + lineNo + "' class='lineno'>" + lineNo + "</div>");
            }
            lineNo++;
        }

        return lineNo;
    };
    
    
    /*
    * Iterate through each of the elements are to be applied to
    */
    return this.each(function() {
        var lineNo = 1;
        var textarea = $(this);
        
        /* Turn off the wrapping of as we don't want to screw up the line numbers */
        textarea.attr("wrap", "off");
        textarea.css({resize:'none'});
        var originalTextAreaWidth	= textarea.outerWidth();

        /* Wrap the text area in the elements we need */
        textarea.wrap("<div class='linedtextarea'></div>");
        var linedTextAreaDiv = textarea.parent().wrap("<div class='linedwrap'></div>");
        var linedWrapDiv = linedTextAreaDiv.parent();
        
        linedWrapDiv.prepend("<div class='lines' style='width:50px'></div>");
        
        var linesDiv = linedWrapDiv.find(".lines");
        linesDiv.height( textarea.height() + 6 );
        
        
        /* Draw the number bar; filling it out where necessary */
        linesDiv.append( "<div class='codelines'></div>" );
        var codeLinesDiv = linesDiv.find(".codelines");
        lineNo = fillOutLines( codeLinesDiv, linesDiv.height(), 1 );

        /* Move the textarea to the selected line */ 
        if ( opts.selectedLine != -1 && !isNaN(opts.selectedLine) ){
            var fontSize = parseInt( textarea.height() / (lineNo-2) );
            var position = parseInt( fontSize * opts.selectedLine ) - (textarea.height()/2);
            textarea[0].scrollTop = position;
        }


        /* Set the width */
        var sidebarWidth = linesDiv.outerWidth();
        var paddingHorizontal = parseInt( linedWrapDiv.css("border-left-width") ) + parseInt( linedWrapDiv.css("border-right-width") ) + parseInt( linedWrapDiv.css("padding-left") ) + parseInt( linedWrapDiv.css("padding-right") );
        var linedWrapDivNewWidth = originalTextAreaWidth - paddingHorizontal;
        var textareaNewWidth = originalTextAreaWidth - sidebarWidth - paddingHorizontal - 20;

        textarea.width(textareaNewWidth);

        /* React to the scroll event */
        textarea.scroll( function(tn){
        var domTextArea = $(this)[0];
        var scrollTop = domTextArea.scrollTop;
        var clientHeight = domTextArea.clientHeight;
        codeLinesDiv.css( {'margin-top': (-1*scrollTop) + "px"} );
        lineNo = fillOutLines( codeLinesDiv, scrollTop + clientHeight, lineNo );
        });


        /* Should the textarea get resized outside of our control */
        textarea.resize( function(tn){
        var domTextArea	= $(this)[0];
        linesDiv.height( domTextArea.clientHeight + 6 );
        });

    });
    };

    // default options
    $.fn.linedtextarea.defaults = {
        selectedLine: -1,
        selectedClass: 'lineselect'
    };
})(jQuery);

$("#crosses-textarea").linedtextarea();

/**
 * Toggles next button if all the requirements passed
 */
function disableNextButton(){
    //check if all the required fields were set
    $.ajax({
        url: '$urlCheckRequiredFields',
        type: 'post',
        dataType: 'json',
        data: {
            entryListId: $entryListId
        },
        success: function(response){
            disabled = response > 0 || $('.summary').text() == '';
            $("#next-btn").toggleClass('disabled', disabled);
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while checking the crosses. Please try again or file a report for assistance.';
            Materialize.toast(notif, 5000);
        }
    });
}


JS;
$this->registerJs($script);
?>

<style>
    .larger-text{
        font-size : 115%;
    }

    .linedwrap {
        border: 1px solid #c0c0c0;
        padding: 3px;
    }

    .linedtextarea {
        padding: 0px;
        margin: 0px;
    }

    .linedtextarea textarea {
        padding-right: 0.3em;
        padding-top: 0.3em;
        border: 0;
    }

    .linedwrap .lines {
        margin-top: 0px;
        width: 50px;
        float: left;
        overflow: hidden;
        border-right: 1px solid #c0c0c0;
        margin-right: 10px;
    }

    .linedwrap .codelines {
        padding-top: 5px;
    }

    .linedwrap .codelines .lineno {
        color: #AAAAAA;
        padding-right: 0.5em;
        padding-top: 0.0em;
        text-align: right;
        white-space: nowrap;
    }

    .linedwrap .codelines .lineselect {
        color: red;
        padding-right: 0px;
    }

    .linedwrap .codelines .linevalid {
        color: green;
        padding-right: 0px;
    }

    .smaller {
        font-size: 12px;
    }

    .parent-box {
        margin: 0px;
        padding: 0px !important;
    }

    .pull-right {
        margin-right: 3px;
    }

    input[type=text]:not(.browser-default) {
        height: 2.5rem;
    }
</style>