<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Cross Manager Review Tab
 */

use kartik\dynagrid\DynaGrid;

// Import classes
use yii\helpers\Html;
use yii\helpers\Url;

//import models
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

// Render tabs
echo Yii::$app->controller->renderPartial('/tabs', [
    'entryListId' => $entryListId,
    'experimentDbId' => $experimentDbId,
    'entryListName' => $entryListName,
    'program' => $program, 
    'active' => 'review',
    'entryListStatus' => $entryListStatus
]);
$notSet = '<span class="not-set">(not set)</span>';

echo '
    Review and finalize the cross list creation process.
    <ul class="collapsible"><li>
    <div class="collapsible-header active">' .
        '<i class="material-icons">info_outline</i>' . \Yii::t('app', 'Basic Information') .
    '</div>' .
    '<div class="collapsible-body" style = "display: inline-flex; width: 100%; background-color: white">';
    foreach ($config as $key => $value) {
        $index = $value['target_value'] ?? $value['api_field'];
        $field = isset($entryListInfo[$index]) && !empty($entryListInfo[$index]) ? $entryListInfo[$index] : $notSet;
        $description = $value['field_description'] ?? '';
        $label = $value['field_label'] ?? $index;
        echo
            '<dl class="col col-md-2">
                <dt title="' . $description . '">' . $label. '</dt>
                <dd style="word-break: break-all;"><div id="'.$index.'">'. $field .'</div></dd>
            </dl>';
    }
echo '</div></li></ul>';

$serialColumn = [
    'class' => '\kartik\grid\SerialColumn',
    'header' => false,
    'order' => DynaGrid::ORDER_FIX_LEFT,
    'vAlign' => 'top',
    'hAlign' => 'left',
    'width' => '10px'
];

// Define URLs
$urlFinalize = Url::to(['/crossManager/review/finalize/']);
$urlNextTab = Url::to(['/crossManager', 'program' => $program]);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings(['dynagrid-cm-review-cross-grid','dynagrid-cm-review-parent-grid']);

//set parent list columns and attributes
$columns = [
    $serialColumn,
    [
        'attribute' => 'occurrenceName',
        'label' =>  \Yii::t('app', 'Occurrence'),
        'headerOptions' => [
            'min-width' => '150px',
        ],
    ],
    [
        'attribute' => 'occurrenceCode',
    ],
    [
        'attribute' => 'entryNumber',
        'label' =>  \Yii::t('app', 'Entry no.')
    ],
    [
        'attribute' => 'entryCode',
    ],
    [
        'attribute' => 'parentRole',
    ],
    [
        'attribute' => 'parentType',
    ],
    [
        'attribute' => 'description',
    ],
    [
        'attribute' => 'seedName',
    ],
    [
        'attribute' => 'packageLabel',
    ],
    [
        'attribute' => 'designation',
        'label' =>  \Yii::t('app', 'Germplasm Name'),
    ],
    [
        'attribute' => 'germplasmCode',
        'visible' => false
    ],
    [
        'attribute' => 'parentage',
        'contentOptions' => [
            'class' => 'germplasm-name-col'
        ],
        'headerOptions' => [
            'min-width' => '150px'
        ],
    ],
    [
        'attribute' => 'generation',
    ],
];

$browserId = 'dynagrid-cm-review-parent-grid';

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'dataProvider' => $parentListDataProvider,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => false,
            'before' => '<strong>'.\Yii::t('app', 'Parent List').'</strong>'.
                '<p id="selected-items-paragraph" class = "pull-right">{summary}</p>',
            'after' => false,
        ],
        'toolbar' => [
            'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                ["/crossManager/review/load?program=$program&id=$entryListId"], 
                [
                'id'=>'reset-'.$browserId,
                'class' => 'btn btn-default', 
                'title'=>\Yii::t('app','Reset parent browser'), 
                'data-pjax' => true
                ]).'{dynagridFilter}{dynagridSort}{dynagrid}'
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

//set cross list columns and attributes

$columns = [
    $serialColumn,
    [
        'attribute' => 'crossName',
    ],
    [
        'attribute' => 'crossFemaleParent',
        'label' => \Yii::t('app','Female Parent'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'femaleParentage',
        'headerOptions'=>[
            'class'=>'red lighten-4',
            'style' => [
                'min-width' => '200px'
            ],
        ],
        'contentOptions'=>['class'=>'red lighten-5 germplasm-name-col'],
    ],
    [
        'attribute' => 'crossMaleParent',
        'label' => \Yii::t('app','Male Parent'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'maleParentage',
        'headerOptions'=>['class'=>'blue lighten-4', 'style' => [
            'min-width' => '200px'
        ]],
        'contentOptions'=>['class'=>'blue lighten-5 germplasm-name-col'],
    ],
    [
        'attribute' => 'crossMethod',
        'label' => \Yii::t('app','Method'),
        'headerOptions' => [
            'style' => [
                'min-width' => '200px'
            ],
        ],
    ],
    [
        'attribute' => 'crossRemarks',
        'headerOptions' => [
            'style' => [
                'min-width' => '150px'
            ],
        ],
    ],
    [
        'attribute' => 'femaleParentEntryNumber',
        'label' => \Yii::t('app','Female Entry No.'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleParentEntryNumber',
        'label' => \Yii::t('app','Male Entry No.'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femalePIEntryCode',
        'label' => \Yii::t('app','Female Entry Code'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'malePIEntryCode',
        'label' => \Yii::t('app','Male Entry Code'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleParentSeedSource',
        'label' => \Yii::t('app','Female Seed Source'),
        'visible' => false,
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleParentSeedSource',
        'label' => \Yii::t('app','Male Seed Source'),
        'visible' => false,
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleOccurrenceName',
        'label' => \Yii::t('app','Female Occurrence'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleOccurrenceName',
        'label' => \Yii::t('app','Male Occurrence'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleOccurrenceCode',
        'visible' => false,
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleOccurrenceCode',
        'visible' => false,
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'harvestStatus',
        'format' => 'raw',
        'visible' => false,
        'value' => function ($model) {
            $harvestStatus = isset($model['harvestStatus']) ? $model['harvestStatus'] : 'NO_STATUS';
            $badge = '';

            if($harvestStatus=='NO_HARVEST') {
                $badge = '<span title="No seeds created yet" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'NO HARVEST') . '</strong></span>';
            } else if (strpos($harvestStatus,'INCOMPLETE')!==false) {
                $badge = '<span title="Harvest data is incomplete" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'INCOMPLETE DATA') . '</strong></span>';
            } else if ($harvestStatus=='READY') {
                $badge = '<span title="Cross is ready for seed and package creation" class="new badge center blue darken-2"><strong>' . \Yii::t('app', 'READY') . '</strong></span>';
            } else if ($harvestStatus=='IN_QUEUE') {
                $badge = '<span title="In queue for seed and package creation" class="new badge center grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'IN QUEUE') . '</strong></span>';
            } else if ($harvestStatus=='IN_PROGRESS') {
                $badge = '<span title="Seed and package creation is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'IN PROGRESS') . '</strong></span>';
            } else if ($harvestStatus=='FAILED') {
                $badge = '<span title="Seed and package creation failed" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'FAILED') . '</strong></span>';
            } else if ($harvestStatus=='COMPLETED') {
                $badge = '<span title="Seeds and packages have been created" class="new badge center green darken-2"><strong>' . \Yii::t('app', 'HARVEST COMPLETE') . '</strong></span>';
            } else if ($harvestStatus=='DELETION_IN_PROGRESS') {
                $badge = '<span title="Deletion of traits and seeds/packages is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'DELETION IN PROGRESS') . '</strong></span>';
            } else if (strpos($harvestStatus,'CONFLICT')!==false) {
                $parts = explode(': ', $harvestStatus);
                $title = $parts[1] ?? 'Unknown conflict';
                $badge = '<span title="' . $title . '" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'DATA CONFLICT') . '</strong></span>';
            } else {
                $badge = '<span class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
            }

            return $badge;
        },
    ],
];

$browserId = 'dynagrid-cm-review-cross-grid';

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'dataProvider' => $crossListDataProvider,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => false,
            'before' => '<strong>'.\Yii::t('app', 'Cross List').'</strong>'.
             '<p id="selected-items-paragraph" class = "pull-right">{summary}</p>',
            'after' => false,
        ],
        'toolbar' => [
            'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
              ["/crossManager/review/load?program=$program&id=$entryListId"], 
              [
                'id'=>'reset-'.$browserId,
                'class' => 'btn btn-default', 
                'title'=>\Yii::t('app','Reset cross browser'), 
                'data-pjax' => true
              ]).'{dynagridFilter}{dynagridSort}{dynagrid}'
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ['dynagrid-cm-review-cross-grid','dynagrid-cm-review-parent-grid'],
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS

$(document).ready(function() {
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();
});

// update entry list status
$(document).on('click', '#next-btn', function(){

    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: {
            entryListDbId: $entryListId
        },
        url: '$urlFinalize',
        success: function(response) {
             // redirect user to next tab
             if(response['success']){
                window.location.href = '$urlNextTab'
            }else{
                notif = '<i class="material-icons red-text left">close</i> ' + response['message'] + 'Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while finalizing. Please try again or file a report for assistance.';
	        Materialize.toast(notif, 5000);
        }
    });
});

JS;

$this->registerJs($script);

?>