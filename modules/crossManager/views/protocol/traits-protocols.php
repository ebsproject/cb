<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders traits protocol box view
 */

use marekpetras\yii2ajaxboxwidget\Box;

$helperText = Yii::t('app', 'Specify traits to be observed. You may add by selecting from the list of traits or existing trait lists. Changes are auto-saved');
?>

<p style="margin-bottom:5px"><?= $helperText ?></p>

<div id = "traits-protocol-div" class = "row col col-sm-12 col-md-12 col-lg-12">
    <?php
    $toolsButtons = [
        'reload' => function() {
            return '&nbsp;&nbsp;
                <a href="#!" title="'.\Yii::t('app','Refresh data').'" class="text-muted fixed-color" onclick="$(&quot;#pr-traits-protocols-box&quot;).box(&quot;reload&quot;);">
                <i class="material-icons widget-icons">loop</i></a>';
        }
    ];

    $options = [
        'id' => 'pr-traits-protocols-box',
        'bodyLoad' => ["/crossManager/protocol/traits-protocols?program=$program&id=$id"],
        'toolsTemplate' => '{reload}',
        'toolsButtons' => $toolsButtons
    ]
    ?>
    <?= Box::widget($options); ?>
</div>

<style>
    #pr-traits-protocols-box, #traits-protocol-div {
        background-color: #f1f4f5;
    }
    .box-body, #traits-protocol-div {
        padding: 0px;
    }
    .trait-div {
        padding: 0px 10px 0px 0px!important;
    }
    .box {
        margin: 0px;
    }
    .panel-title {
        margin-top:10px;
    }
    .panel-heading .summary {
        margin-top: 0px;
    }
</style>