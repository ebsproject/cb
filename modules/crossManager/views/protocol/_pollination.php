<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders pollination protocol view
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\GenericFormWidget;

$crossManagerUrl = Url::to(['/crossManager', 'program'=>$program]);
$savePollinationInstructionsUrl = Url::to(['/crossManager/protocol/save-pollination-instructions', 'program'=>$program]);

?>

<div class="col col-sm-12 view-entity-content">
    <div id="content-body">
        <div id="default-content" class="col col-sm-12 col-md-12 col-lg-12">
            <?php
                $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'id' => 'pollination-protocol-form',
                    'action' => ['/crossManager/protocol/pollination-protocol','program'=>$program, 'id'=>$id]
                ]);
            ?>
            <div id="filter-panel" class="parent-box">
                <?php
                    if (isset($widgetOptions)) {
                        echo GenericFormWidget::widget([ 'options' => $widgetOptions ]);
                    } else {
                        echo '<p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'No pollination config found.').'</span></p>';
                    }
                ?>
            </div>
            <div id="dynamic-fields" class="pull-right">
                <?=
                    Html::a('Cancel',
                        $crossManagerUrl,
                        [
                            'id' => 'cancel-update-pollination-protocol',
                            'title' => \Yii::t('app', 'Cancel update'),
                            'style' => [
                                'vertical-align' => '-10px',
                                'margin-right' => '12px',
                            ],
                        ]).
                    Html::button(\Yii::t('app','Save'),
                        [
                            'class' => 'waves-effect waves-light btn btn-primary',
                            'style' => 'margin-right:20px;margin-top:15px;margin-bottom:10px;',
                            'id' => 'save-pollination-btn',
                            'disabled' => true,
                            'title' => Yii::t('app', 'Save pollination Protocols'),
                        ])
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$this->registerJsFile("@web/js/generic-form-validation.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<<JS

    // Styling adjustments
    $('form#pollination-protocol-form').find('.basic-info1').addClass('widen-basic-info');

    // Enable save pollination instruction button
    if ($('.text-input-validate').val() != null) {
        $('#save-pollination-btn').attr('disabled', false);
    }

    // Save pollination instruction
    $(document).on('click','#save-pollination-btn', function() {
        let notif = "<i class='material-icons blue-text'>info</i> Please wait for the update to complete."; 
        $('.toast').css('display', 'none');
        Materialize.toast(notif, 5000);
        
        let formData = $.param($.map($("#pollination-protocol-form").serializeArray(), function(v,i) { return v; }));
        formData = formData + '&' + $.param({['entryListDbId'] : '$id'})
        formData = formData + '&' + $.param({['experimentDbId'] : '$experimentDbId'})
        
        $.ajax({
            url: '$savePollinationInstructionsUrl',
            type: 'post',
            dataType: 'json',
            data: formData,
            success: function(response) {
                $('.toast').css('display', 'none');
                Materialize.toast(response['notif'] + response['message'], 5000);
            },
            error: function() {
                notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while updating pollination instructions. ";
                
                $('.toast').css('display', 'none');
                Materialize.toast(notif, 5000);
            }
        });
    });

JS;
$this->registerJs($script);
?>

<style>
    #content-body {
        margin-top: 10px;
    }
    label {
        font-size: .8rem;
        color: #333333;
    }
    #filter-panel {
        margin-top: 0;
    }
    .widen-basic-info {
        width: 50% !important;
    }
    .parent-box {
        position: relative;
        margin-top: 1rem;
    }
</style>
