<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders the landing page for updating entry list protocols
 */

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\helpers\Url;

$crossManagerUrl = Url::to(['/crossManager', 'program'=>$program]);

$title = Yii::t('app', 'Cross Manager');
$subTitle = empty($entryListName) ?
    Yii::t('app', 'Update Protocols of Cross List') :
    Yii::t('app', "Update Protocols of $entryListName") ;
$toolName = Html::a($title, $crossManagerUrl);
echo '<h3>' . $toolName .' <small>» ' . $subTitle . '</small>' . '</h3>';

?>


<!-- Render tabs for different Protocols and input fields for each -->
<?php
if (sizeof($protocolTabs) > 0) {
    $items = [];

    // Prepare each View file for the Protocols
    foreach ($protocolTabs as $idx => $protocolTab) {
        $isActive = $idx == 0;
        $filename = $protocolTab['action_id'];

        if (isset($filename)) {
            $content = Yii::$app->controller->renderPartial(
                "@app/modules/crossManager/views/protocol/$filename.php",
                [
                    'program' => $program,
                    'id' => $id,
                ]
            );
            $items[] = [
                'label' =>
                    '<i class="'.$protocolTab['item_icon'].'" style="margin-right: 8px;"></i>' .
                    Yii::t('app', $protocolTab['display_name']),
                'active' => $isActive,
                'content' => $content,
                'linkOptions' => ['class' => "pr-tabs pr-$filename-tab'"]
            ];
        }
    }

    echo TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_LEFT,
        'encodeLabels' => false,
        'pluginOptions' => [
            'enableCache' => false
        ]
    ]);
} else {
    Yii::$app->session->setFlash('error',\Yii::t('app','No protocols found in the database configuration. Refresh the page or please contact web admin.'));
}
?>

<?php
$this->registerCssFile("@web/css/box.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'box-css');
?>


