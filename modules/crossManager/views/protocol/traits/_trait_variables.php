<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders display of adding trait variables
 */

use kartik\select2\Select2;

echo '<label class="control-label" >'.\Yii::t('app','Trait') .'</label>';

echo Select2::widget([
    'data' => $traitVariableTags,
    'name' => 'trait-select-variable',
    'id' => 'trait-select-variable',
    'maintainOrder' => true,
    'options' => [
        'placeholder' => \Yii::t('app','Select a trait'),
        'tags' => true
    ]
]);


?>

<div style="margin-top:10px">
    <button class="btn waves-effect waves-light add-replace-vars-btn" title="<?= \Yii::t('app','Add variables') ?>" id="trait-select-variable-add-btn" data-operation="added" disabled style="margin: 0px 7px 0px 0px;"><?= \Yii::t('app','Add') ?></button>

    <button class="hidden btn waves-effect waves-light grey lighten-3 grey-text text-darken-2 add-replace-vars-btn" id="trait-select-variable-replace-btn" data-operation="replaced" disabled style="margin: 0px 10px 0px 0px;" title="<?= \Yii::t('app','Replace all') ?>"><i class="material-icons">swap_horiz</i></button>

    <div class="hidden preloader-wrapper small active" style="width:20px;height:20px;">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>
