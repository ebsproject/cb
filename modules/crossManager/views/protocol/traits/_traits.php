<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders trait protocols view
 */

use kartik\tabs\TabsX;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

// Routes
$traitsProtocolsUrl = Url::to(['/crossManager/protocol/traits-protocols', 'program'=>$program, 'id'=>$id]);
$addVariablesUrl = Url::to(['/crossManager/protocol/add-variables', 'program'=>$program]);
$viewTraitListUrl = Url::to(['/crossManager/protocol/view-trait-list', 'program'=>$program]);
$removeVariablesUrl = Url::to(['/crossManager/protocol/remove-variables', 'program'=>$program]);
$saveRemarksUrl = Url::to(['/crossManager/protocol/save-remarks', 'program'=>$program]);

?>

<!-- Add variables panel -->
<div class="col col-md-5 trait-div">
    <div class="card" style="padding: 5px;">
        <div class="row">
            <div class="col col-md-12">
                <?php
                $traitsVarLabel = Yii::t('app', 'Traits');
                $traitsVarListLabel = Yii::t('app', 'Trait Lists');

                $subTabs = [
                    // Trait variables
                    [
                        'label' => '<i class="fa fa-leaf"></i> ' . $traitsVarLabel,
                        'linkOptions' => ['class' => 'sub-tab', 'data-id' => 1],
                        'items' => [[
                            'label' => $traitsVarLabel,
                            'linkOptions' => ['id' => 'pr-sub-tab-1'],
                            'active' => true,
                            'encode' => false,
                            'content' => Yii::$app->controller->renderPartial('@app/modules/crossManager/views/protocol/traits/_trait_variables.php', [
                                'traitVariableTags' => $traitVariableTags,
                            ])
                        ]],
                        'headerOptions' => [
                            'class' => 'hide-caret',
                            'data-id' => 1,
                            'title' => 'Trait variables'
                        ]
                    ],
                    // Trait lists
                    [
                        'label' => '<i class="fa fa-list-alt"></i> ' . $traitsVarListLabel,
                        'linkOptions' => ['class' => 'sub-tab', 'data-id' => 2],
                        'items' => [[
                            'label' => $traitsVarListLabel,
                            'linkOptions' => ['id' => 'pr-sub-tab-2'],
                            'encode' => false,
                            'content' => Yii::$app->controller->renderPartial('@app/modules/crossManager/views/protocol/traits/_trait_lists.php', [
                                'traitProtocolTags' => $traitProtocolTags,
                            ])
                        ]],
                        'headerOptions' => [
                            'class' => 'hide-caret',
                            'data-id' => 2,
                            'title' => 'Add variables from a trait list'
                        ]
                    ]
                ];

                echo TabsX::widget([
                    'items' => $subTabs,
                    'position' => TabsX::POS_LEFT,
                    'sideways' => true,
                    'encodeLabels' => false,
                    'pluginOptions' => [
                        'enableCache' => false
                    ],
                    'enableStickyTabs' => true,
                    'containerOptions' => ['class' => 'pr-traits-protocols-tab']
                ]);

                ?>
            </div>
        </div>
    </div>
</div>

<!-- Selected traits panel -->
<div class="col col-md-7 trait-div">
    <div class="card" style="padding: 5px">
        <div class="row">
            <div class="col col-md-12">
            <?php
                echo Yii::$app->controller->renderPartial('@app/modules/crossManager/views/protocol/traits/_trait_selected.php', [
                    'dataProvider' => $dataProvider,
                ]);
            ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal for viewing traits in a list -->
<?php
Modal::begin([
    'id' => 'list-preview-modal',
    'header' => '<h4 id="list-preview-header"></h4>',
    'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',[ 'id'=>'back-preview-btn', 'data-dismiss'=>'modal' ]).'&nbsp;&emsp;'.
        Html::a(\Yii::t('app','Confirm'),'#',[ 'id'=>'ok-preview-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal' ]).'&emsp;',
    'size' => 'modal-lg',
    'options' => [ 'data-backdrop'=>'static' ]
]);
    echo '<p id="list-preview-modal-note">'.\Yii::t('app','Below is the list of traits that will be added in this cross list. Click "Confirm" to proceed.').'</p>';
    echo '<p id="list-preview-modal-body">';
Modal::end();
?>

<?php
$this->registerJs(<<<JS
    $(document).on('ready pjax:success', function(e) {
		e.preventDefault();
		refresh();
	});
    
    refresh();

    // Call sub-tab content upon click
	$(document).on('click', '.sub-tab', function(e) {
		let subTabId = $(this).data('id');
		$('#pr-sub-tab-'+subTabId).click();
	});

    // Disable action buttons when there is no record
	var varCount = document.getElementsByClassName("final-grid_select");
	if (varCount.length === 0) {
		$('.trait-list-actions').addClass('disabled');
	} else {
		$('.trait-list-actions').removeClass('disabled');
	}
    
    // Check if there is selected variable
	$('#trait-select-variable').bind("change keyup input", function() {
		let selectedList = $('#trait-select-variable').val();
		
		if (selectedList === '') {
			$('.add-replace-vars-btn').attr('disabled', true);
		} else {
			$('.add-replace-vars-btn').attr('disabled', false);
		}
	});
    
    // Check if there is a selected list
	$('#trait-select-group').bind("change keyup input", function() {
		let selectedList = $('#trait-select-group').val();
		
		if (selectedList === '') {
			$('.add-replace-list-btn').attr('disabled', true);
		} else {
			$('.add-replace-list-btn').attr('disabled', false);
		}
	});
    
    // Add one trait variable
    $('.add-replace-vars-btn').on('click', function() {
        let obj = $(this);
        let operation = obj.data('operation');
        let selectedVar = $('#trait-select-variable').val();
        
        $('.preloader-wrapper').removeClass('hidden');
        
        if (operation === 'added') {
            $.ajax({
                url: '$addVariablesUrl',
				type: 'post',
				dataType: 'json',
				data: {
                    listDbId: '$listDbId',
                    variableDbId: selectedVar
                },
                async: false,
                success: function(response) {
                    reloadSelectedTraitsBrowser();
                    
                    $('.preloader-wrapper').addClass('hidden');
                    
                    $('.toast').css('display', 'none');
                    Materialize.toast(response['notif'] + response['message'], 5000);
                },
                error: function() {
                    let notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";
                    
                    $('.toast').css('display', 'none');
                    Materialize.toast(notif, 5000);
                }
            })
        }
    });
    
    // Add traits via list
    $('.add-replace-list-btn').on('click', function() {
		let selectedList = $('#trait-select-group').val();
		let list = $('#select2-trait-select-group-container').attr('title');
		let header = '<i class="material-icons left">add</i> Add traits from ' + list;

		$('.preloader-wrapper').removeClass('hidden');
        
        // Show preview modal
        $.ajax({
            url: '$viewTraitListUrl',
            type: 'post',
            dataType: 'json',
            data: {
                'listDbId': selectedList
            },
            success: function(response) {
                $('.preloader-wrapper').addClass('hidden');
                $('#list-preview-header').html(header);
				$('#list-preview-modal-body').html(response['htmlData']);
				$('#list-preview-modal').modal('show');
            },
            error: function() {
				var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while displaying preview of variables. ";

				$('.toast').css('display','none');
				Materialize.toast(notif,5000);
			}
        });
    });
    
    // Confirm add traits via list
    $('#ok-preview-btn').on('click', function(e) {
        e.preventDefault();
		e.stopImmediatePropagation();
        
        let obj = $('#list-preview-header').html();
		let operation = obj.includes('Add') ? 'added' : 'replaced';
		let selectedList = $('#trait-select-group').val();
		
		$('.preloader-wrapper').removeClass('hidden');
        
        if (operation === 'added') {
            $.ajax({
                url: '$addVariablesUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    listDbId: '$listDbId',
                    sourceListDbId: selectedList
                },
                async: false,
                success: function(response) {
                    if (response['success']) {
                        reloadSelectedTraitsBrowser();
                    }
                    
                    $('.preloader-wrapper').addClass('hidden');
                    $('#list-preview-modal').modal('hide');
                    
                    $('.toast').css('display', 'none');
                    Materialize.toast(response['notif'] + response['message'], 5000);
                },
                error: function() {
                    let notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";
                    
                    $('.toast').css('display', 'none');
                    Materialize.toast(notif, 5000);
                }
            });
        }
    });
    
    // Remove selected variable/s
    $(document).on('click', '#trait-discard-btn', function(e) {
		let listMemberIds = [];
		$("input:checkbox.final-grid_select").each(function() {
			if ($(this).prop("checked") === true) {
				listMemberIds.push($(this).attr("id"));
			}
		});

		if (listMemberIds.length === 0) {
			let notif = "<i class='material-icons orange-text left'>warning</i> No selected trait/s.";
            
			$('.toast').css('display','none');
			Materialize.toast(notif,5000);
		} else {
			$('#delete-variable-list-modal').modal('show');
		}
	});
    
    // Confirm delete variable/s
    $(document).on('click', '#ok-delete-btn', function (e) {
        e.preventDefault();
		e.stopImmediatePropagation();
        
        let listMemberIds = [];
        $("input:checkbox.final-grid_select").each(function() {
			if ($(this).prop("checked") === true) {
				listMemberIds.push($(this).attr("id"));
			}
		});
        
        $.ajax({
            url: '$removeVariablesUrl',
            type: 'post',
            dataType: 'json',
            data: {
                listMemberIds: listMemberIds
            },
            async: false,
            success: function(response) {
                reloadSelectedTraitsBrowser();
                
                $('#delete-variable-list-modal').modal('hide');
                
                $('.toast').css('display', 'none');
                Materialize.toast(response['notif'] + '<span>' + response['message'] + '</span>', 5000);
            },
            error: function() {
                let notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while removing traits. ";
                
                $('.toast').css('display', 'none');
                Materialize.toast(notif, 5000);
            }
        })
    });
    
    // Check if instruction is updated (on one variable)
    $(document).on('change paste', ".instructions-input-validate", function(e) { 
        let id = this.id;
        let instructionsArr = (id).split('-');
        let listMemberId = instructionsArr[1];
        let remarks = $('#'+id).val();
        
        $('.add-replace-vars-btn').attr('disabled',true);
        
        $.ajax({
            url: '$saveRemarksUrl',
            type: 'post',
            dataType: 'json',
            cache: false,
            data: {
                listMemberId: listMemberId,
                remarks: remarks
            },
            success: function(response) {
                $('.add-replace-vars-btn').attr('disabled', false);
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while updating instructions. ";
                $('.toast').css('display','none');
                Materialize.toast(notif,5000)
            }
        });
    });
    
    
    
    // Check row in variables browser in Selected Traits
	$(document).on('click', '#trait-list-selected-vars-grid tbody tr', function(e) {
		let this_row = $(this).find('input:checkbox')[0];
		if (this_row.checked) {
			this_row.checked = false;  
			$('#trait-list-variables-grid-select-all-id').prop("checked", false);
			$(this).removeClass("grey lighten-4");
		} else {
			$(this).addClass("grey lighten-4");
			this_row.checked=true;  
			$("#"+this_row.id).prop("checked");  
		}
	});
    
    // Select all variables in Selected Traits
	$(document).on('click', '#trait-list-variables-grid-select-all-id', function(e) {
		if ($(this).prop("checked") === true)  {
			$(this).attr('checked', 'checked');
			$('.final-grid_select').attr('checked', 'checked');
			$('.final-grid_select').prop('checked', true);
			$(".final-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
		} else {
			$(this).removeAttr('checked');
			$('.final-grid_select').prop('checked', false);
			$('.final-grid_select').removeAttr('checked');
			$("input:checkbox.final-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");      
		}
	});
    
    // Hide loader if action is cancelled
	$('#back-replace-btn, #back-delete-btn, #back-preview-btn').on('click', function() {
		$('.preloader-wrapper').addClass('hidden');
	});
    
    // Refresh gridview
	function refresh() {
		$(".trait-div").css("height", ($(window).height() - 270));
		$(".kv-grid-wrapper").css("height", $('.trait-div').height() - 140);
	}
    
    // Refresh selected traits
    function reloadSelectedTraitsBrowser() {
        $.pjax.reload({
            url: '$traitsProtocolsUrl',
            container: '#trait-list-selected-vars-grid-pjax',
            replace: false
        });
    }
    
JS);
?>

<style type="text/css">
    .hide-caret .caret,
    .hide-caret .dropdown-menu {
        display: none;
    }

    .tabs-left .tab-content {
        border-left: none;
    }

    .trait-div {
        margin-bottom: 10px;
    }

    .kv-panel-after {
        display: none;
    }
</style>

