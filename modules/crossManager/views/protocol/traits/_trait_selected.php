<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders selected trait list display
 */

use kartik\dynagrid\DynaGrid;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

// Variables columns
$columns = [
    [
        'label' => "checkbox",
        'header' => '
			<input type="checkbox" class="filled-in" id="trait-list-variables-grid-select-all-id" />
			<label for="trait-list-variables-grid-select-all-id"></label>
		',
        'contentOptions' => ['style' => ['max-width' => '100px;']],
        'content' => function($data) {
            return '
                <input class="final-grid_select filled-in" type="checkbox" id="'.$data['listMemberDbId'].'" data-id ="'.$data['variableDbId'].'" />
                <label for="'.$data['listMemberDbId'].'"></label>
            ';
        },
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'width' => '50px',
    ],
    [
        'attribute' => 'label',
        'format' => 'raw',
        'vAlign' => 'middle',
        'value' => function($data) {
            // Render view variable info widget
            return Html::a($data['label'],
                ['#'],
                [
                    'id' => $data['variableDbId'],
                    'label' => $data['label'],
                    'data-toggle' => 'modal',
                    'data-target'=> '#view-variable-modal',
                    'class' => 'view-variable'
                ]
            );
        },
        'contentOptions' => function($data) {
            return ['class' => 'order_tr', "data-varId" => $data['variableDbId']];
        }
    ],
    [
        'attribute' => 'displayName',
        'label' => 'Name',
        'format' => 'raw',
        'vAlign' => 'middle',
    ],
    [
        'attribute' => 'remarks',
        'label' => 'Instructions',
        'format' => 'raw',
        'vAlign'=> 'middle',
        'value' => function($data) {
            return Html::input('text', 'instructions-input', $data['remarks'], [
                'id' => 'instructions-'.$data['listMemberDbId'],
                'class' => 'instructions-input-validate',
                'data-name' => 'remarks'
            ]);
        },
        'width'=>'300px'
    ],
];

$actionDeleteList = Html::a('<i class="material-icons widget-icons">delete</i>',
    '#!',
    [
        'class' => 'btn trait-list-actions red darken-3',
        'id' => 'trait-discard-btn',
        'title' => \Yii::t('app',' Remove item(s) from the list')
    ]
);

// Variables browser
DynaGrid::begin([
    'options' => [ 'id'=>'trait-list-selected-vars-grid' ],
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => 'trait-list-selected-vars-grid',
        'dataProvider' => $dataProvider,
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
                'id' => 'trait-list-selected-vars-grid'
            ],
        ],
        'panel'=> [
            'heading' => '<i class="material-icons left">folder_special</i> '.\Yii::t('app', 'Selected Traits'),
            'footer' => false
        ],
        'responsiveWrap' => false,
        'toolbar' =>  [
            'content' => "$actionDeleteList",
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true
    ]
]);
DynaGrid::end();

// Modal for viewing variable
Modal::begin([
    'id' => 'view-variable-modal',
    'header' => '<h4 id="entity-label"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Ok'),'#', [ 'data-dismiss'=>'modal', 'class'=>'btn btn-primary waves-effect waves-light modal-close' ]).'&emsp;',
    'closeButton' => [ 'class' => 'hidden' ],
    'size' => 'modal-lg',
    'options' => [ 'data-backdrop'=>'static','style'=>'z-index: 9999999;' ]
]);
echo '<div class="view-variable-modal-body"></div>';
Modal::end();

// Modal for deleting traits
Modal::begin([
    'id' => 'delete-variable-list-modal',
    'header' => '<h4><i class="material-icons orange-text left">warning</i>'. \Yii::t('app','Remove Traits').'</h4>',
    'footer' => Html::a(\Yii::t('app','Cancel'),'#', [ 'id'=>'back-delete-btn', 'data-dismiss'=>'modal' ]).'&nbsp;&emsp;'.
        Html::a(\Yii::t('app','Confirm'),'#', [ 'id'=>'ok-delete-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal' ]).'&emsp;',
    'size' => 'modal-md',
    'options' => [ 'data-backdrop'=>'static' ]
]);
    echo \Yii::t('app','Selected trait/s will be deleted from your cross list. This action cannot be undone.');
Modal::end();

$variableUrl = Url::to(['/variable/default/view-info']);
$this->registerJs(<<<JS

    // View variable info
    $(document).on('click', '.view-variable', function(e) {
        var obj = $(this);
        var varId = obj.attr('id');
        var varLabel = obj.attr('label');
    
        $('#entity-label').html('<i class="fa fa-info-circle"></i> ' + varLabel + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        $('.view-variable-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        
        // Load content of view variable information
        setTimeout(function() {
            $.ajax({
                url: '$variableUrl',
                data: {
                    id: varId
                },
                type: 'POST',
                async: true,
                success: function(data) {
                    $('.view-variable-modal').modal('show');
                    $('.view-variable-modal-body').html(data);
                },
                error: function() {
                    $('.view-variable-modal-body').html('<i>There was a problem while loading record information.</i>');
                }
            });
        },300);  
    });
JS);
?>

