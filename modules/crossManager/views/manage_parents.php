<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\export\ExportMenu;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

//id of cm manage browser
$browserId = 'dynagrid-cm-parent-grid';

// Define URLs
$urlUpdateParent = Url::to(['/crossManager/manage-parents/update-parent/', 'program' => $program,'id' => $entryListId]);
$urlCheckRequiredFields = Url::to(['/crossManager/manage-parents/check-required-fields/', 'program' => $program,'id' => $entryListId]);
$urlNextTab = Url::to(['/crossManager/add-crosses/load', 'program' => $program, 'id' => $entryListId]);
$urlDeleteParents = Url::to(['/crossManager/manage-parents/delete-parent', 'program' => $program, 'id' => $entryListId]);
$urlDeleteParentsAll = Url::to(['/crossManager/manage-parents/delete-parent-all', 'program' => $program, 'id' => $entryListId]);
$urlCheckEmptyTable = Url::to(['/crossManager/manage-parents/check-empty-table/', 'program' => $program,'id' => $entryListId]);
$urlModalBody = Url::to(['/crossManager/manage-parents/modal-body', 'program' => $program, 'id' => $entryListId]);
$urlUpdateParentsAll = Url::to(['/crossManager/manage-parents/update-parents-all', 'program' => $program, 'id' => $entryListId]);
$urlUpdateParentsSelected = Url::to(['/crossManager/manage-parents/update-parents-selected', 'program' => $program, 'id' => $entryListId]);
$exportParentsUrl = Url::to(['/crossManager/manage-parents/export-parents', 'id' => $entryListId]);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

//renders Cross Manager Manage Parents Tab
echo Yii::$app->controller->renderPartial('/tabs', [
    'entryListId' => $entryListId,
    'entryListName' => $entryListName,
    'program' => $program, 
    'active' => 'manage-parents',
    'entryListStatus' => $entryListStatus
]);

//set columns and attributes
$columns = [
    [
        'label' => "Checkbox",
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => '<input type="checkbox" class="filled-in" id="cm-parent-browser-select-all-checkbox" "/><label style="padding-left: 20px;" for="cm-parent-browser-select-all-checkbox"></label>',
        'content' => function ($model) {
            $checkboxId = $model['parentDbId'];
            return '
            <input class="cm-parent-browser-single-checkbox filled-in"' .
                'id="' . $checkboxId . '"' .
                'type="checkbox"/>' .
            '<label style="padding-left: 20px;" for="' . $checkboxId . '"></label>';
        },
        'hiddenFromExport' => true
    ],
    [
        'attribute' => 'occurrenceName',
        'label' =>  \Yii::t('app', 'Occurrence'),
        'headerOptions' => [
            'min-width' => '150px',
        ],
    ],
    [
        'attribute' => 'occurrenceCode',
    ],
    [
        'attribute' => 'entryNumber',
        'label' =>  \Yii::t('app', 'Entry no.')
    ],
    [
        'attribute' => 'entryCode',
    ],
    [
        'attribute' => 'parentRole',
        'label' =>  \Yii::t('app','Parent Role').'&nbsp;<span class="required">*</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $parentRoleOptions,
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true]],
        'filterInputOptions'=>['placeholder'=>'--Select--'],
        'value' => function($model) use($parentRoleOptions){
            return Select2::widget([
                'name' => 'parent-role-picker',
                'data' => $parentRoleOptions,
                'value' => $model['parentRole'],
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Select a role',
                    'id' => $model['parentDbId'].'-parentRole',
                    'class' => 'editable-field required',
                    'title' => \Yii::t('app', 'Parent role for '.$model['designation']),
                ],
            ]);
        },
        'headerOptions' => [
            'min-width' => '150px',
        ],
    ],
    [
        'attribute' => 'parentType',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $parentTypeOptions,
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true]],
        'filterInputOptions'=>['placeholder'=>'--Select--'],
        'value' => function($model) use ($parentTypeOptions){
            return Select2::widget([
                'name' => 'parent-type-picker',
                'data' => $parentTypeOptions,
                'value' => $model['parentType'],
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Select a type',
                    'id' => $model['parentDbId'].'-parentType',
                    'class' => 'editable-field',
                    'title' => \Yii::t('app', 'Parent type for '.$model['designation']),
                ],
            ]);
        },
        'headerOptions' => [
            'min-width' => '150px'
        ],
    ],
    [
        'attribute' => 'description',
        'format' => 'raw',
        'value' => function($model){
            return Html::input('text', $model['parentDbId'].'-description', $model['description'],
            [
                'id' => $model['parentDbId'].'-description',
                'data-label' => 'description',
                'class' => "editable-field",
            ]);
        },
        'headerOptions' => [
            'min-width' => '150px'
        ],
    ],
    [
        'attribute' => 'seedName',
    ],
    [
        'attribute' => 'packageLabel',
    ],
    [
        'attribute' => 'designation',
        'label' =>  \Yii::t('app', 'Germplasm Name'),
    ],
    [
        'attribute' => 'germplasmCode',
        'visible' => false
    ],
    [
        'attribute' => 'parentage',
        'contentOptions' => [
            'class' => 'germplasm-name-col'
        ],
        'headerOptions' => [
            'min-width' => '150px'
        ],
    ],
    [
        'attribute' => 'generation',
    ],
];

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $parentModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', 'Review and update your parent list as needed.').'{summary}<br/>
                <p id="total-selected-text" class="pull-right" style="margin: -1px;">
                </p>',
            'after' => false,
        ],
        'toolbar' => 
        [
            ['content' => 
                Html::button('Bulk Update',
                [
                    'type'=>'button', 
                    'title'=>Yii::t('app', 'Bulk update'), 
                    'class'=>'btn', 
                    'id'=>'cm-bulk-update-btn',
                ])
            ],
            ['content' =>
                Html::a(\Yii::t('app', 'Remove')." <span class='caret'></span>",
                '#', 
                [
                    'class' => 'btn dropdown-trigger dropdown-button-pjax',
                    '#',
                    'id' => 'cm-remove-parent-btn',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Remove'),
                        'data-activates' => 'cm-remove-parent-dropdown-actions',
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                ]).
                "<ul id='cm-remove-parent-dropdown-actions' class='dropdown-content' style='width: 160px !important'>".
                    "<li><a href='#' class='remove-parent'>".\Yii::t('app','Remove selected')."</a></li>".
                    "<li><a href='#' class='remove-parent-all'>".\Yii::t('app','Remove all')."</a></li>".
                "</ul>"
            ],
            ['content' =>
                Html::a('<i class="material-icons inline-material-icon">file_download</i>',
                    '#',
                    [
                        'id' => 'export-parents-btn',
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app', 'Export parents in CSV format')
                    ]
                )
            ],
            ['content' =>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                ["/crossManager/manage-parents/load?program=$program&id=$entryListId"], 
                [
                    'id'=>'reset-'.$browserId,
                    'class' => 'btn btn-default', 
                    'title'=>\Yii::t('app','Reset parent browser'), 
                    'data-pjax' => true
                ]).'{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// Show modal when deleting parent record
Modal::begin([
    'id' => 'remove-parents-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align: bottom;">delete</i>' . '&nbsp;' .\Yii::t('app','Remove Parents').'</h4>',
    'footer' => 
        Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-delete-btn','data-dismiss'=>'modal']).'&nbsp;&nbsp'
        .Html::a('Confirm',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light',
                'id' => 'remove-parents-btn'
            ]
        ) . '&emsp;',
    'options' => ['data-backdrop' => 'static'],
    'closeButton' => ['class' => 'hidden']
]);

echo '<div class="loading-div"></div>';
echo '<div id="remove-parents-modal-body"></div>';
Modal::end();

//New Modal for entry management, actions button - select variables
Modal::begin([
    'id'=> 'cm-bulk-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i>  '.\Yii::t('app','Bulk Update').'</h4>',
    'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'cancel-bulk-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="bulk-update-selected" class="btn btn-primary green waves-effect waves-light update-btn-selected">'.\Yii::t('app','Selected').'</a>&nbsp;'.
        '<a id="bulk-update-all" class="btn btn-primary teal waves-effect waves-light update-btn-all">'.\Yii::t('app','All').'</a>',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static', 'tabindex' => false],
    'closeButton' => ['class' => 'hidden']
]);

echo '<div class="loading-div"></div>';
echo '<div id="cm-bulk-update-modal-body"></div>';
Modal::end();

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS

var requiredFields = [
    'parentRole'
];

resize();
disableNextButton();

function disableNextButton(){
    //check if all the required fields were set
    $.ajax({
        url: '$urlCheckRequiredFields',
        type: 'post',
        dataType: 'json',
        data: {
            requiredFields: requiredFields,
            entryListDbId: $entryListId
        },
        success: function(response){
            disabled = response > 0 || $('.summary').text() == '';
            $("#next-btn, #add-crosses-tab, #add-crosses-tab > a").toggleClass('disabled', disabled);
            $("#add-crosses-tab > a").attr('href', disabled ? '#' : '$urlNextTab');
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while checking the required fields. Please try again or file a report for assistance.';
            Materialize.toast(notif, 5000);
        }
    });
}

/**
 * Check if parent table is empty
 * 
 */
function checkEmptyTable(){
    $.ajax({
        url: '$urlCheckEmptyTable',
        type: 'post',
        dataType: 'json',
        data:{
            entryListDbId: $entryListId
        },
        success: function(response){
            //if parent table is not empty
            if(response['success']){
                var message = '<p style = "font-size:100%"> <i class="material-icons orange-text left">warning</i>You are about to remove all parents in this list.' +
                    '</br></br>'+
                    '<b>NOTE: </b> Parents that have already been used in crossing will not be removed, if any.</p>';
                $('#remove-parents-modal-body').html(message);
                $('#remove-parents-btn').removeClass('disabled');
                $('#cancel-delete-btn').show();
                $('#remove-parents-modal').modal('show');
            }
            else{
                var icon = '<i class="material-icons orange-text left">warning</i>';
                var notif = icon + response['message'];
                Materialize.toast(notif, 5000);
            }
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while checking parent table. Please try again or file a report for assistance.';
            Materialize.toast(notif, 5000);
        }
    });
}

// Perform after pjax success of browser
$(document).on('ready pjax:success','#$browserId-pjax', function(e) {
    resize();
});

// Perform after pjax completion of browser
$(document).on('ready pjax:complete','#$browserId-pjax', function(e) {
    resize();
});

// Interact with the editable field columns
$(document).on('keypress keyup blur paste change', ".editable-field", function(event) {
    var id = this.id;
    var fields = (this.id).split('-');
    var parentId = fields[0];
    var column = fields[1];
    var value = $('#' + id).val();

    // Save the changes in the database
    $.ajax({
        url: '$urlUpdateParent',
        type: 'post',
        datatType: 'json',
        cache: false,
        data: {
            parentId: parentId,
            column: column,
            value: value,
        },
        success: function(response){
            if(requiredFields.includes(column)){
                disableNextButton();
            }
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while saving. Please try again or file a report for assistance.';
            Materialize.toast(notif, 5000);
        }
    });
});

/**
 * Resize the grid table upon loading
 */
function resize(){
    var maxHeight = ($(window).height() - 350);
    $(".kv-grid-wrapper").css("height", maxHeight);
}

// Global variables
var entryListId = $entryListId;
var gridId = '$browserId';
var selectionStorageId = entryListId + "_" + gridId;
var values = "";

// Restore checkbox selection
checkboxActions(selectionStorageId, gridId);

// Perform after pjax success of browser
$(document).on('ready pjax:success','#$browserId-pjax', function(e) {
    checkboxActions(selectionStorageId, gridId);
    $('.dropdown-trigger').dropdown();
    resize();
});

// Perform after pjax completion of browser
$(document).on('ready pjax:complete','#$browserId-pjax', function(e) {
    checkboxActions(selectionStorageId, gridId);
    $('.dropdown-trigger').dropdown();
    resize();
});

// Reset parent list
$(document).on('click', '#reset-$browserId', function(){
    sessionStorage.removeItem(selectionStorageId);
});

// Button listener for Remove button
$(document).ready(function(){
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();

    // Check checkbox for checks
    checkboxActions(selectionStorageId, gridId);
    $('.dropdown-trigger').dropdown();
    resize();
    
    // Store checked checkboxes ids
    var selectedRows = sessionStorage.getItem(selectionStorageId);
    var currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));
    // Identify remove type
    var selectionMode;

    // Open confirm delete modal for " selected "
    $(document).on('click', '.remove-parent', function(e) {
        // Set selection mode to selected
        selectionMode = "selected";
        // Update current selected checkboxes
        currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));
        // Count selected checkboxes
        let selectedItemsCountText = $('#selected-items-count').text();

        if (selectedItemsCountText !== 'No') {
            var message = '<p style = "font-size:100%"> <i class="material-icons orange-text left">warning</i>You are about to remove ' + '<b>' + selectedItemsCountText + 
            '</b> selected parents. </br> </br>' +
            '<b>NOTE: </b> Parents that have already been used in crossing will not be removed, if any.</p>';
            $('#remove-parents-modal-body').html(message);
            $('#remove-parents-btn').removeClass('disabled');
            $('#cancel-delete-btn').show();
            $('#remove-parents-modal').modal('show');
        }
        // If no selected item(s)
        else if(currentSelection == null || currentSelection.length == 0){
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> Please select an item.";
                Materialize.toast(notif,2500);
            }
        }
    });

    // Open confirm delete modal for " all "
    $(document).on('click', '.remove-parent-all', function(e) {
        // Set selection mode to all
        selectionMode = "all";
        checkEmptyTable();
    });
    
    // Button listener for confirm button
    $('#remove-parents-btn').on('click', function(e) {
        // Conditional statement for selection mode
        $('#remove-parents-modal-body').html('');
        $('#remove-parents-btn').addClass('disabled');
        $('#cancel-delete-btn').hide();
        $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        if(selectionMode == "selected"){
            $.ajax({
                type:"POST",
                dataType: 'json',
                url: '$urlDeleteParents',
                data: {
                    parentIds: currentSelection
                },
                success: function(response) {
                    var icon = response['success'] ? '<i class="material-icons green-text left">check</i> ' : '<i class="material-icons orange-text left">warning</i>';
                    // Show notification
                    var notif = icon + response['message'];
                    $('.toast').css('display','none');
                    Materialize.toast(notif, 5000);
                    sessionStorage.removeItem(selectionStorageId);
                    disableNextButton();
                    // Reload browser upon success
                    $.pjax.reload({container: '#$browserId-pjax'});
                    //Close confirmation modal
                    $('.loading-div').html('');
                    $('#remove-parents-modal').modal('hide');
                },
                error: function(error){
                    $('.loading-div').html('');
                    $('#remove-parents-modal').modal('hide');
                    notif = '<i class="material-icons red-text left">close</i> An error occurred while saving. Please try again or file a report for assistance.';
                    Materialize.toast(notif, 5000);
                }
            });
        }
        else if(selectionMode == "all"){
            $.ajax({
                type:"POST",
                dataType: 'json',
                url: '$urlDeleteParentsAll',
                success: function(response) {
                    // Show notification
                    var icon = response['success'] ? '<i class="material-icons green-text left">check</i> ' : '<i class="material-icons orange-text left">warning</i>';
                    var notif = icon + response['message'];
                    $('.toast').css('display','none');
                    Materialize.toast(notif, 5000);
                    sessionStorage.removeItem(selectionStorageId);
                    disableNextButton();
                    // Reload browser upon success
                    $.pjax.reload({container: '#$browserId-pjax'});
                    // Close confirmation modal
                    $('.loading-div').html('');
                    $('#remove-parents-modal').modal('hide');
                },
                error: function(error){
                    $('.loading-div').html('');
                    $('#remove-parents-modal').modal('hide');
                    notif = '<i class="material-icons red-text left">close</i> An error occurred while saving. Please try again or file a report for assistance.';
                    Materialize.toast(notif, 5000);
                }
            });
        }
    });

    // Select all checkboxes in current page
    $(document).on('click', '#cm-parent-browser-select-all-checkbox', function(e){
        var checkboxClass = "cm-parent-browser-single-checkbox";
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.'+checkboxClass).attr('checked','checked');
            $('.'+checkboxClass).prop('checked',true);
            $("."+checkboxClass+":checkbox").parent("td").parent("tr").addClass("selected");
            saveSelection('selectAll', gridId);
        }else{
            $(this).removeAttr('checked');
            $('.'+checkboxClass).prop('checked',false);
            $('.'+checkboxClass).removeAttr('checked');
            $("input:checkbox."+checkboxClass).parent("td").parent("tr").removeClass("selected"); 
            saveSelection('deselectAll', gridId);
        }
    });

    // Browser checkbox click event
    $(document).on('click', '.cm-parent-browser-single-checkbox', function(e){
    if($(this).prop("checked") === true)  {
        $(this).parent("td").parent("tr").addClass("selected");
        var row = $(this);
        var value = row.prop("id");
        var selAll = tickSelectAll();

        $('#cm-parent-browser-select-all-checkbox').prop("checked", selAll);
        saveSelection('select', gridId, value);
    } else{
        $(this).parent("td").parent("tr").removeClass("selected");
        var row = $(this);
        var value = row.prop("id");
        saveSelection('deselect', gridId, value);
        $('#cm-parent-browser-select-all-checkbox').prop("checked", false);
    }
    });
})

/**
 * Check if all items in the current page are selected.
 * @return {boolean} whether or not to tick the select all checkbox element
 */
function tickSelectAll() {
    var checkboxClass = "cm-parent-browser-single-checkbox";
    var result = true;
    // Check each checkbox if checked
    $.each($('.'+checkboxClass), function(i, val) {
        result = result && $(this).prop("checked");
    });

    // Return result (true or false)
    return result;
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} mode the mode of saving to be performed (select, deselect, selectAll, deselectAll)
 * @param {string} gridId unique string identifier of the grid 
 * @param {string} value the value of the selection to be saved
 */
function saveSelection(mode, gridId='', value = null) {
    var selectionStorageId = entryListId + "_" + gridId;
    var checkboxClass = "cm-parent-browser-single-checkbox";
    // Retrieve current selection from storage
    var currentSelection = sessionStorage.getItem(selectionStorageId);
    if (currentSelection == null) {
        currentSelection = [];
    } else {
        currentSelection = JSON.parse(currentSelection);
    }

    // For selectAll mode, add all items in the current page to the storage
    if (mode == 'selectAll') {
        $('#'+gridId).find("input:checkbox."+checkboxClass).each(function() {
            if ($(this).prop("checked") === true) {
                var currentValue = $(this).attr('id');
                if ($.inArray(currentValue, currentSelection) === -1) {
                    currentSelection.push(currentValue);
                }
            }
        });

    }
    // For deselectAll mode, remove all items in the current page from the storage
    else if (mode == 'deselectAll') {
        $('#'+gridId).find("input:checkbox."+checkboxClass).each(function() {
            if ($(this).prop("checked") === false) {
                var currentValue = $(this).attr('id');
                var index = currentSelection.indexOf(currentValue);
                if (index !== -1) {
                    currentSelection.splice(index,1);
                }
            }
        });
    }
    // For select mode, add one item to the storage
    else if (mode == 'select') {
        if ($.inArray(value, currentSelection) === -1) {
            currentSelection.push(value);
        }
    }
    // For deselect mode, remove one item from the storage
    else if (mode == 'deselect') {
        var index = currentSelection.indexOf(value);
        if (index !== -1) {
            currentSelection.splice(index,1);
        }
    }
    // Display total count in the browser
    displayTotalCount(currentSelection);
    // Save the selection to the storage
    sessionStorage.setItem(selectionStorageId, JSON.stringify(currentSelection));
}

/**
 * Displays the total count of selected items in the browser
 * @param {array} currentSelection array containing selected item values
 */
function displayTotalCount(currentSelection) {
    var noneSelected = '<b><span id="selected-items-count">No</span></b> <span id="selected-items-text"> selected items. &nbsp;</span>';

    // If current selection is null, display "No selected items"
    if (currentSelection === null) {
        $('#total-selected-text').html(noneSelected);
        return true;
    }
    // Get total count
    var totalCount = currentSelection.length;
    // If total count is greater than 0, display count
    if (totalCount > 0) {
        let s = totalCount == 1 ? '' : 's';
        $('#total-selected-text').html(
            '<b><span id="selected-items-count"> '+totalCount+
            '</span></b> <span id="selected-items-text"> selected item' + s + '. &nbsp;</span>'
        );
    }
    // if total count is less than or equal to 0, display "No selected items"
    else {
        $('#total-selected-text').html(noneSelected);
    }
}

/**
 * Checks and restores the default state of the checkboxes based on the storage values
 * 
 * @param {string} selectionStorageId unique string identifier holding the selected items
 * @param {string} gridId unique string identifier of the grid 
 */
function checkboxActions(selectionStorageId, gridId) {
    var selectedRows = sessionStorage.getItem(selectionStorageId);
    selectedRows = JSON.parse(selectedRows);
    // Display total count in the browser
    displayTotalCount(selectedRows);
    // Set variables
    var checkboxClass = "cm-parent-browser-single-checkbox";
    var selAllId = "cm-parent-browser-select-all-checkbox";
    var grid = $('#'+gridId);
    // Check selected items
    grid.find("."+checkboxClass).each(function() {
        var value = $(this).attr("id");
        if (selectedRows !== null) {
            if ($.inArray(value, selectedRows) !== -1) {
                $(this).prop('checked',true);
                $(this).addClass('isClicked');
                $(this).parent("td").parent("tr").addClass("selected");
            }
        }
    });
    // Tick select all if all items in page are selected
    if (grid.find("."+checkboxClass).length != 0) {
        var all = grid.find("."+checkboxClass).length == grid.find("."+checkboxClass + ":checked").length;
        $('#'+selAllId).prop('checked', all).change();
    }
}

// Bulk update selected parents
 $('#bulk-update-selected').on('click', function(e) {
    // Get ids of checked parents
    var currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));
    // Set column value
    var column = $('#req-vars').val();
    // Validation for bulk update modal
    if(column == ''){
        $('.toast').css('display','none');
        var notif = '<i class="material-icons orange-text left">warning</i>' + 'You have not selected/entered a value';
        Materialize.toast(notif, 5000);
    }
    else if(values == ''){
        $('.toast').css('display','none');
        var notif = '<i class="material-icons orange-text left">warning</i>' + 'You have not selected/entered a value';
        Materialize.toast(notif, 5000);
    }
    else{
        $('#cm-bulk-update-modal-body').html('');
        $('#bulk-update-selected').addClass('disabled');
        $('#bulk-update-all').addClass('disabled');
        $('#cancel-bulk-btn').hide();
        $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        // Save the changes in the database
        $.ajax({
            url: '$urlUpdateParentsSelected',
            type: 'post',
            dataType: 'json',
            data: {
                parentIds: currentSelection,
                column: column,
                values: values,
            },
            success: function(response){
                if(!response['success']){
                    // Close bulk modal
                    $('.loading-div').html('');
                    $('#cm-bulk-modal').modal('hide');
                    var notif = '<i class="material-icons orange-text left">warning</i>' + response['message'];
                    $('.toast').css('display','none');
                    Materialize.toast(notif, 5000);
                }
                else{
                    // Close bulk modal
                    $('.loading-div').html('');
                    $('#cm-bulk-modal').modal('hide');
                    // Reload browser upon success
                    $.pjax.reload({container: '#$browserId-pjax'});
                }
            },
            error: function(error){
                $('.loading-div').html('');
                $('#cm-bulk-modal').modal('hide');
                notif = '<i class="material-icons red-text left">close</i> An error occurred while updating. Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        });
    }
});

// Bulk update all parents
$('#bulk-update-all').on('click', function(e) {
    var column = $('#req-vars').val();

    // Validation for bulk update modal
    if(column == ''){
        $('.toast').css('display','none');
        var notif = '<i class="material-icons orange-text left">warning</i>' + 'You have not selected/entered a value';
        Materialize.toast(notif, 5000);
    }
    else if(values == ''){
        $('.toast').css('display','none');
        var notif = '<i class="material-icons orange-text left">warning</i>' + 'You have not selected/entered a value';
        Materialize.toast(notif, 5000);
    }
    else{
        $('#cm-bulk-update-modal-body').html('');
        $('#bulk-update-selected').addClass('disabled');
        $('#bulk-update-all').addClass('disabled');
        $('#cancel-bulk-btn').hide();

        $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        // Save the changes in the database
        $.ajax({
            url: '$urlUpdateParentsAll',
            type: 'post',
            dataType: 'json',
            data: {
                column: column,
                values: values,
            }, 
            success: function(response){
                if(!response['success']){
                    // Close bulk modal
                    $('.loading-div').html('');
                    $('#cm-bulk-modal').modal('hide');
                    var notif = '<i class="material-icons orange-text left">warning</i>' + response['message'];
                    $('.toast').css('display','none');
                    Materialize.toast(notif, 5000);
                }
                else{
                    // Close bulk modal
                    $('.loading-div').html('');
                    $('#cm-bulk-modal').modal('hide');
                    // Reload browser upon success
                    $.pjax.reload({container: '#$browserId-pjax'});
                }
            },
            error: function(error){
                $('.loading-div').html('');
                $('#cm-bulk-modal').modal('hide');
                notif = '<i class="material-icons red-text left">close</i> An error occurred while updating. Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        });
    }
});

// Show modal upon clicking bulk update button
$(document).on('click','#cm-bulk-update-btn',function(e) {
    $.ajax({
        type:"POST",
        dataType: 'json',
        url: '$urlModalBody',
        success: function(response) {
            var message = "Bulk update the following values for the parent list records.<br> <br>";
            $('#cm-bulk-update-modal-body').html([message,response]);
            $('#bulk-update-selected').removeClass('disabled');
            $('#bulk-update-all').removeClass('disabled');
            $('#cancel-bulk-btn').show();
            $('#cm-bulk-modal').modal('show');
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while saving. Please try again or file a report for assistance.';
            Materialize.toast(notif, 5000);
        }
    });    
});

// Get value for generic widget select2
$(document).on('change','.select2-input-validate' ,function(e){
    //Set value to empty upon change
    values = "";
    var id = $(this).attr('id');  
    // Assign value
    if(id != 'req-vars'){
        values = $('#' + id).val();  
    }
});

// Get value for generic widget text
$(document).on('keypress keyup blur paste','.text-input-validate' ,function(e){
    var id = $(this).attr('id');
    // Assign value
    values = $('#' + id).val();
});

// Export parent list - without Threshold
$('#export-parents-btn').on('click', function (e) {
    // TODO : Add conditional statement below for background wokers
    
    let exportParentsUrl = '$exportParentsUrl'
    let expPntUrl = window.location.origin + exportParentsUrl

    message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
    $('.toast').css('display','none')
    Materialize.toast(message, 5000)
    window.location.replace(expPntUrl)

    $('#system-loading-indicator').html('')
});

JS;

$this->registerJs($script);






