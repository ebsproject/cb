<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\web\JsExpression;

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

// Browser Id
$browserId = 'dynagrid-cm-cross-grid';

// Define URLs
$urlUpdateCross = Url::to(['/crossManager/manage-crosses/update-cross/', 'program' => $program,'id' => $entryListId]);
$urlCheckRequiredFields = Url::to(['/crossManager/manage-crosses/check-required-fields/', 'program' => $program,'id' => $entryListId]);
$urlNextTab = Url::to(['/crossManager/review/load', 'program' => $program, 'id' => $entryListId]);
$urlDeleteCross = Url::to(['/crossManager/manage-crosses/delete-cross/', 'program' => $program]);
$urlMainCMBrowser = Url::to(['/crossManager/default/index', 'program' => $program]);
$urlCheckEmptyTable = Url::to(['/crossManager/manage-crosses/check-empty-table/', 'program' => $program,'id' => $entryListId]);
$urlModalBody = Url::to(['/crossManager/manage-crosses/modal-body/', 'program' => $program,'id' => $entryListId]);
$urlUpdateCrossAll = Url::to(['/crossManager/manage-crosses/update-cross-all/', 'program' => $program,'id' => $entryListId]);
$urlUpdateCrossSelected = Url::to(['/crossManager/manage-crosses/update-cross-selected/', 'program' => $program,'id' => $entryListId]);
$urlGetFilterData = Url::to(['/crossManager/manage-crosses/get-filter-data/', 'program' => $program,'id' => $entryListId]);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

//renders Cross Manager Manage Crosses Tab
echo Yii::$app->controller->renderPartial('/tabs', [
    'entryListId' => $entryListId,
    'entryListName' => $entryListName,
    'program' => $program, 
    'active' => 'manage-crosses',
    'experimentDbId' => $experimentDbId,
    'entryListStatus' => $entryListStatus
]);

//set columns and attributes
$columns = [
    [
        'label' => "Checkbox",
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => '<input type="checkbox" class="filled-in" id="cm-cross-browser-select-all-checkbox" "/><label style="padding-left: 20px;" for="cm-cross-browser-select-all-checkbox"></label>',
        'content' => function ($model) {
            $checkboxId = $model['crossDbId'];
            
            return '
            <input class="cm-cross-browser-single-checkbox filled-in"' .
                'id="' . $checkboxId . '"' .
                'type="checkbox"/>' .
            '<label style="padding-left: 20px;" for="' . $checkboxId . '"></label>';
        },
        'vAlign' => 'top',
        'mergeHeader' => true,
    ],
    [
        'class' => '\kartik\grid\SerialColumn',
        'header' => false,
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'vAlign' => 'top',
        'hAlign' => 'left',
        'width' => '20px'
    ],
    [
        'attribute' => 'harvestStatus',
        'format' => 'raw',
        'visible' => true,
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
            'NO_HARVEST' => 'no harvest',
            'INCOMPLETE' => 'incomplete harvest data',
            'CONFLICT' => 'data conflict',
            'READY' => 'ready for seed creation',
            'IN_QUEUE' => 'in queue for creation',
            'IN_PROGRESS' => 'seed creation in progress',
            'COMPLETED' => 'harvest complete',
            'DELETION_IN_PROGRESS' => 'seed/trait deletion in progress',
            'INVALID_STATE' => 'invalid state',
            'FAILED' => 'seed creation failed'
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => ['multiple'=>false, 'allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Select status'],
        'value' => function ($model) {
            $harvestStatus = isset($model['harvestStatus']) ? $model['harvestStatus'] : 'NO_STATUS';
            $badge = '';

            if($harvestStatus=='NO_HARVEST') {
                $badge = '<span title="No seeds created yet" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'NO HARVEST') . '</strong></span>';
            } else if (strpos($harvestStatus,'INCOMPLETE')!==false) {
                $badge = '<span title="Harvest data is incomplete" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'INCOMPLETE DATA') . '</strong></span>';
            } else if ($harvestStatus=='READY') {
                $badge = '<span title="Cross is ready for seed and package creation" class="new badge center blue darken-2"><strong>' . \Yii::t('app', 'READY') . '</strong></span>';
            } else if ($harvestStatus=='IN_QUEUE') {
                $badge = '<span title="In queue for seed and package creation" class="new badge center grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'IN QUEUE') . '</strong></span>';
            } else if ($harvestStatus=='IN_PROGRESS') {
                $badge = '<span title="Seed and package creation is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'IN PROGRESS') . '</strong></span>';
            } else if ($harvestStatus=='FAILED') {
                $badge = '<span title="Seed and package creation failed" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'FAILED') . '</strong></span>';
            } else if ($harvestStatus=='COMPLETED') {
                $badge = '<span title="Seeds and packages have been created" class="new badge center green darken-2"><strong>' . \Yii::t('app', 'HARVEST COMPLETE') . '</strong></span>';
            } else if ($harvestStatus=='DELETION_IN_PROGRESS') {
                $badge = '<span title="Deletion of traits and seeds/packages is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'DELETION IN PROGRESS') . '</strong></span>';
            } else if (strpos($harvestStatus,'CONFLICT')!==false) {
                $parts = explode(': ', $harvestStatus);
                $title = $parts[1] ?? 'Unknown conflict';
                $badge = '<span title="' . $title . '" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'DATA CONFLICT') . '</strong></span>';
            } else {
                $badge = '<span class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
            }

            return $badge;
        },
    ],
    [
        'attribute' => 'crossName',
    ],
    [
        'attribute' => 'crossFemaleParent',
        'label' => \Yii::t('app','Female Parent'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'femaleParentage',
        'headerOptions'=>['class'=>'red lighten-4', 'style' => [
            'min-width' => '200px'
        ],],
        'contentOptions'=>['class'=>'red lighten-5 germplasm-name-col'],
    ],
    [
        'attribute' => 'crossMaleParent',
        'label' => \Yii::t('app','Male Parent'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'maleParentage',
        'headerOptions'=>['class'=>'blue lighten-4', 'style' => [
            'min-width' => '200px'
        ]],
        'contentOptions'=>['class'=>'blue lighten-5 germplasm-name-col'],
    ],
    [
        'attribute' => 'crossMethod',
        'label' => \Yii::t('app','Method').'&nbsp;<span class="required">*</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions'=>[
            'pluginOptions'=>[
                'allowClear'=>true,
                'ajax' => [
                    // Get cross method value per cross list
                    'url' => ''.$urlGetFilterData.'',
                    'dataType' => 'json',
                    'delay' => 150,
                    'type'=>'POST',
                    'data' => new JsExpression(
                        'function (params) {
                            var value = (params.term === "")? undefined: params.term
                            return {
                                "filter": {
                                    "crossMethod": value
                                },
                                "column": "crossMethod"
                            }
                        }'
                    ),
                    'processResults' => new JSExpression(
                        // Set filter value based on existing method value on cross list
                        'function (data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.items
                            };
                        }'
                    ),
                    'cache' => true
                ],
                'options'=>[
                    'id'=>'crossMethodFilterId'
                ]
            ]
        ],
        'filterInputOptions'=>['placeholder'=>'--Select--'],
        'value' => function($model) use ($methodOptions){
            //disable if harvestStatus is not NO_HARVEST
            $disable = false;
            if(isset($model['harvestStatus']) && $model['harvestStatus'] != "NO_HARVEST"){
                $disable = 'readonly';
            }
            //conditional statement for selfing
            if($model['crossMethod'] == "selfing"){
                $disable = 'readonly';
            }
            else{
                $methodOptions = array_filter(
                    $methodOptions,
                    function($methodOptions){
                        return $methodOptions != "selfing";
                    }
                );
            }
            return Select2::widget([
                'name' => 'method-type-picker',
                'data' => $methodOptions,
                'value' => $model['crossMethod'],
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Select a method',
                    'id' => $model['crossDbId'].'-crossMethod',
                    'class' => 'editable-field required',
                    'title' => \Yii::t('app', 'Cross method type for '.$model['crossDbId']),
                    'disabled' => $disable,
                ],
            ]);
        },
        'headerOptions' => [
            'style' => [
                'min-width' => '200px'
            ],
        ],
    ],
    [
        'attribute' => 'crossRemarks',
        'format' => 'raw',
        'value' => function($model){
            //disable if harvestStatus is not NO_HARVEST
            $disable = false;
            if(isset($model['harvestStatus']) && $model['harvestStatus'] != "NO_HARVEST"){
                $disable = true;
            }
            return Html::input('text', $model['crossDbId'].'-crossRemarks', $model['crossRemarks'],
            [
                'id' => $model['crossDbId'].'-crossRemarks',
                'data-label' => 'crossRemarks',
                'class' => "editable-field",
                'disabled' => $disable,
            ]);
        },
        'headerOptions' => [
            'style' => [
                'min-width' => '150px'
            ],
        ],
    ],
    [
        'attribute' => 'femaleParentEntryNumber',
        'label' => \Yii::t('app','Female Entry No.'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleParentEntryNumber',
        'label' => \Yii::t('app','Male Entry No.'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femalePIEntryCode',
        'label' => \Yii::t('app','Female Entry Code'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'malePIEntryCode',
        'label' => \Yii::t('app','Male Entry Code'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleParentSeedSource',
        'label' => \Yii::t('app','Female Seed Source'),
        'visible' => false,
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleParentSeedSource',
        'label' => \Yii::t('app','Male Seed Source'),
        'visible' => false,
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleOccurrenceName',
        'label' => \Yii::t('app','Female Occurrence'),
        'visible' => false,
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleOccurrenceName',
        'label' => \Yii::t('app','Male Occurrence'),
        'visible' => false,
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleOccurrenceCode',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleOccurrenceCode',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
];

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $crossModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', 'Review and update your crosses as needed.').'{summary}<br/>
                <p id="total-selected-text" class = "pull-right" style = "margin: -1px;">
                </p>',
            'after' => false,
        ],
        'toolbar' => 
            [
                ['content' => 
                        Html::button('Bulk Update', 
                        [
                        'type'=>'button', 
                        'title'=>Yii::t('app', 'Bulk update'), 
                        'class'=>'btn', 
                        'id'=>'cm-bulk-update-btn',
                    ]) 
                ],
                ['content' =>  Html::a(\Yii::t('app', 'Remove')." <span class='caret'></span>",
                    '#', 
                    [
                        'class' => 'btn dropdown-trigger',
                        '#',
                        'id' => 'cm-remove-cross-btn',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Remove Crosses'),
                        'data-activates' => 'cm-remove-cross-dropdown-actions',
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                    ]).
                    "<ul id='cm-remove-cross-dropdown-actions' class='dropdown-content' style='width: 160px !important'>".
                        "<li><a href='#' class='remove-cross' data-selection = 'selected'>".\Yii::t('app','Remove selected')."</a></li>".
                        "<li><a href='#' class='remove-cross' data-selection = 'all'>".\Yii::t('app','Remove all')."</a></li>".
                    "</ul>"
                ],
                ['content' =>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                    ["/crossManager/manage-crosses/load?program=$program&id=$entryListId"], 
                    [
                        'id'=>'reset-'.$browserId,
                        'class' => 'btn btn-default', 
                        'title'=>\Yii::t('app','Reset cross browser'), 
                        'data-pjax' => true
                    ]).'{dynagridFilter}{dynagridSort}{dynagrid}'
                ]
            ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

//Modal for delete cross management
Modal::begin([
    'id' => 'cross-list-confirm-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align: bottom;">delete</i> '.\Yii::t('app','Delete Crosses').'</h4>',
    'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-delete-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a class="btn btn-primary waves-effect waves-light confirm-delete-btn">Confirm</a>',
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static'],
    'closeButton' => ['class' => 'hidden'],
]);

echo '<div class="loading-div"></div>';
echo '<div id="cross-modal-confirm-modal-body"></div>';
Modal::end();

//New Modal for entry management, actions button - select variables
Modal::begin([
    'id'=> 'cm-bulk-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i>  '.\Yii::t('app','Bulk Update').'</h4>',
    'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'cancel-bulk-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="bulk-update-selected" class="btn btn-primary green waves-effect waves-light update-btn-selected">'.\Yii::t('app','Selected').'</a>&nbsp;'.
        '<a id="bulk-update-all" class="btn btn-primary teal waves-effect waves-light update-btn-all">'.\Yii::t('app','All').'</a>',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static', 'tabindex' => false],
    ]);

echo '<div class="loading-div"></div>';
echo '<div id="cm-bulk-update-modal-body"></div>';
Modal::end();

// disable horizontal scroll
$this->registerCss(".body-container { overflow-x: hidden; }");

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS

//TODO: Dynamically retrieve required fields
var requiredFields = [
    'crossMethod'
];

// Next button validation
disableNextButton();

var entryListId = $entryListId;
var gridId = '$browserId';
var selectionStorageId = entryListId + "_" + gridId;

// Restore checkbox selection
checkboxActions(selectionStorageId, gridId);
$('.dropdown-trigger').dropdown();

resize();

// Remove heading summary
$(".panel-heading > div.pull-right > div.summary").hide();

// Perform after pjax success of browser
$(document).on('ready pjax:success','#$browserId-pjax', function(e) {
    checkboxActions(selectionStorageId, gridId);
    $('.dropdown-trigger').dropdown();
    resize();
});

// Perform after pjax completion of browser
$(document).on('ready pjax:complete','#$browserId-pjax', function(e) {
    checkboxActions(selectionStorageId, gridId);
    $('.dropdown-trigger').dropdown();
    resize();
});

$(document).ready(function(){
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();
    checkboxActions(selectionStorageId, gridId);
    $('.dropdown-trigger').dropdown();
    resize();

    // Update cross information
    $(document).on('keypress keyup blur paste change', ".editable-field", function(event) {
        var id = this.id;
        var fields = (this.id).split('-');
        var crossId = fields[0];
        var column = fields[1];
        var value = $('#' + id).val();

        // Save the changes in the database
        $.ajax({
            url: '$urlUpdateCross',
            type: 'post',
            dataType: 'json',
            cache: false,
            data: {
                crossId: crossId,
                column: column,
                value: value,
            },
            success: function(response){
                if(requiredFields.includes(column)){
                    disableNextButton();
                }
            },
            error: function(error){
                notif = '<i class="material-icons red-text left">close</i> An error occurred while saving. Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        });
    });

    // Reset cross list
    $(document).on('click', '#reset-' + gridId, function(){
        sessionStorage.removeItem(selectionStorageId);
    });

    // Select all checkboxes in current page
    $(document).on('click', '#cm-cross-browser-select-all-checkbox', function(e){
        var checkboxClass = "cm-cross-browser-single-checkbox";
        
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.'+checkboxClass).attr('checked','checked');
            $('.'+checkboxClass).prop('checked',true);
            $("."+checkboxClass+":checkbox").parent("td").parent("tr").addClass("selected");
            saveSelection('selectAll', gridId);
        }else{
            $(this).removeAttr('checked');
            $('.'+checkboxClass).prop('checked',false);
            $('.'+checkboxClass).removeAttr('checked');
            $("input:checkbox."+checkboxClass).parent("td").parent("tr").removeClass("selected"); 
            saveSelection('deselectAll', gridId);
        }
    });

    // Browser checkbox click event
    $(document).on('click', '.cm-cross-browser-single-checkbox', function(e){

        if($(this).prop("checked") === true)  {
            $(this).parent("td").parent("tr").addClass("selected");
            var row = $(this);
            var value = row.prop("id");
            var selAll = tickSelectAll();

            $('#cm-cross-browser-select-all-checkbox').prop("checked", selAll);
            saveSelection('select', gridId, value);
        } else{
            $(this).parent("td").parent("tr").removeClass("selected");
            var row = $(this);
            var value = row.prop("id");
            saveSelection('deselect', gridId, value);
            $('#cm-cross-browser-select-all-checkbox').prop("checked", false);
        }
    });

    // Confirm deletion of crosses
    $(document).on('click', '.remove-cross', function(e) {

        var selection = $(this).data('selection');
        var currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));

        // no selected item(s)
        if(selection !== 'all' && (currentSelection == null || currentSelection.length == 0)){
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> Please select an item.";
                Materialize.toast(notif,2500);
            }
        }else{
            //check if the crosses table is empty
            $.ajax({
                url: '$urlCheckEmptyTable',
                type: 'post',
                dataType: 'json',
                data:{
                    entryListDbId: $entryListId
                },
                success: function(response){
                    //if the cross table is not empty
                    if(response['success']){
                        //show confirmation modal
                        var mode = selection == 'selected-on-page' ? 'selected crosses on this page' : selection + ' crosses'
                        var warningIcon = '<i class="material-icons orange-text left">warning</i>';
                        var message = 'You are about to remove <strong id="mode" data-selection = "'+selection+'"> ' + mode + '</strong>' +
                            '. Crosses with committed trait data and crosses that are already harvested will be skipped (if any).';
                        $('#cross-modal-confirm-modal-body').html('<p style="font-size:115%">' + warningIcon +' '+ message + '</p>');
                        $('#cross-list-confirm-modal').modal();
                        $('.confirm-delete-btn').removeClass('disabled');
                        $('#cancel-delete-btn').show();
                    }
                    else{
                        var icon = '<i class="material-icons orange-text left">warning</i>';
                        var notif = icon + response['message'];
                        $('.toast').css('display','none');
                        Materialize.toast(notif, 5000);
                    }
                },
                error: function(error){
                    notif = '<i class="material-icons red-text left">close</i> An error occurred while checking the crosses table. Please try again or file a report for assistance.';
                    Materialize.toast(notif, 5000);
                }
            });
        }
    });

    // Delete crosses
    $(document).on('click','.confirm-delete-btn', function(e){
        var selection = $('#mode').data('selection');
        var currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));

        //show loading of deletion
        $('#cross-modal-confirm-modal-body').html('');
        $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        $('.confirm-delete-btn').addClass('disabled');
        $('#cancel-delete-btn').hide();
        $.ajax({
            url: '$urlDeleteCross',
            type: 'post',
            dataType: 'json',
            data: {
                crossIds : JSON.stringify(currentSelection),
                selection : selection,
                entryListId : entryListId
            },
            success: function(response) {
                if(response['success'] && response['message'] == 'A background process has been created') {
                    window.location = '$urlMainCMBrowser'
                } else {
                    var icon = response['success'] ? '<i class="material-icons green-text left">check</i> ' : '<i class="material-icons orange-text left">warning</i>';
                    $('.loading-div').html('');
                    $('#cross-list-confirm-modal').modal('hide');
                    sessionStorage.removeItem(selectionStorageId);
                    notif = icon + response['message'];
                    $('.toast').css('display','none');
                    Materialize.toast(notif, 5000);
                    disableNextButton();
                    $.pjax.reload({
                        container: '#$browserId-pjax',
                        replace:false
                    });
                }
            },
            error: function(error){
                $('.loading-div').html('');
                $('#cross-list-confirm-modal').modal('hide');
                notif = '<i class="material-icons red-text left">close</i> An error occurred while deleting the crosses. Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        });
    });

});

/**
 * Check if all items in the current page are selected.
 * @return {boolean} whether or not to tick the select all checkbox element
 */
function tickSelectAll() {
    var checkboxClass = "cm-cross-browser-single-checkbox";
    var result = true;
    // Check each checkbox if checked
    $.each($('.'+checkboxClass), function(i, val) {
        result = result && $(this).prop("checked");
    });

    // Return result (true or false)
    return result;
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} mode the mode of saving to be performed (select, deselect, selectAll, deselectAll)
 * @param {string} gridId unique string identifier of the grid 
 * @param {string} value the value of the selection to be saved
 */
function saveSelection(mode, gridId='', value = null) {
    var selectionStorageId = entryListId + "_" + gridId;
    var checkboxClass = "cm-cross-browser-single-checkbox";
    // Retrieve current selection from storage
    var currentSelection = sessionStorage.getItem(selectionStorageId);
    if (currentSelection == null) {
        currentSelection = [];
    } else {
        currentSelection = JSON.parse(currentSelection);
    }

    // For selectAll mode, add all items in the current page to the storage
    if (mode == 'selectAll') {
        $('#'+gridId).find("input:checkbox."+checkboxClass).each(function() {
            if ($(this).prop("checked") === true) {

                var currentValue = $(this).attr('id');

                if ($.inArray(currentValue, currentSelection) === -1) {
                    currentSelection.push(currentValue);
                }
            }
        });

    }
    // For deselectAll mode, remove all items in the current page from the storage
    else if (mode == 'deselectAll') {
        $('#'+gridId).find("input:checkbox."+checkboxClass).each(function() {
            if ($(this).prop("checked") === false) {
                var currentValue = $(this).attr('id');
                var index = currentSelection.indexOf(currentValue);
                if (index !== -1) {
                    currentSelection.splice(index,1);
                }
            }
        });
    }
    // For select mode, add one item to the storage
    else if (mode == 'select') {
        if ($.inArray(value, currentSelection) === -1) {
            currentSelection.push(value);
        }
    }
    // For deselect mode, remove one item from the storage
    else if (mode == 'deselect') {
        var index = currentSelection.indexOf(value);
        if (index !== -1) {
            currentSelection.splice(index,1);
        }
    }
    // Display total count in the browser
    displayTotalCount(currentSelection);
    // Save the selection to the storage
    sessionStorage.setItem(selectionStorageId, JSON.stringify(currentSelection));
}

/**
 * Displays the total count of selected items in the browser
 * @param {array} currentSelection array containing selected item values
 */
function displayTotalCount(currentSelection) {
    var noneSelected = '<b><span id="selected-items-count">No</span></b> <span id="selected-items-text"> selected items. &nbsp;</span>';

    // If current selection is null, display "No selected items"
    if (currentSelection === null) {
        $('#total-selected-text').html(noneSelected);
        return true;
    }
    // Get total count
    var totalCount = currentSelection.length;
    // If total count is greater than 0, display count
    if (totalCount > 0) {
        let s = totalCount == 1 ? '' : 's';
        $('#total-selected-text').html(
            '<b><span id="selected-items-count"> '+totalCount+
            '</span></b> <span id="selected-items-text"> selected item' + s + '. &nbsp;</span>'
        );
    }
    // if total count is less than or equal to 0, display "No selected items"
    else {
        $('#total-selected-text').html(noneSelected);
    }
}

/**
 * Checks and restores the default state of the checkboxes based on the storage values
 * @param {string} selectionStorageId unique string identifier holding the selected items
 * @param {string} gridId unique string identifier of the grid 
 */
function checkboxActions(selectionStorageId, gridId) {
    var selectedRows = sessionStorage.getItem(selectionStorageId);
    selectedRows = JSON.parse(selectedRows);
    // Display total count in the browser
    displayTotalCount(selectedRows);
    // Set variables
    var checkboxClass = "cm-cross-browser-single-checkbox";
    var selAllId = "cm-cross-browser-select-all-checkbox";
    var grid = $('#'+gridId);
    // Check selected items
    grid.find("."+checkboxClass).each(function() {
        var value = $(this).attr("id");
        if (selectedRows !== null) {
            if ($.inArray(value, selectedRows) !== -1) {
                $(this).prop('checked',true);
                $(this).addClass('isClicked');
                $(this).parent("td").parent("tr").addClass("selected");
            }
        }
    });
    // Tick select all if all items in page are selected
    if (grid.find("."+checkboxClass).length != 0) {
        var all = grid.find("."+checkboxClass).length == grid.find("."+checkboxClass + ":checked").length;
        $('#'+selAllId).prop('checked', all).change();
    }
}

/**
 * Resize the grid table upon loading
 */
function resize(){
    var maxHeight = ($(window).height() - 350);
    $(".kv-grid-wrapper").css("height", maxHeight);
}

/**
 * Disable next button when cross list is empty or
 * required fields were not filled
 */
function disableNextButton(){
    $.ajax({
        url: '$urlCheckRequiredFields',
        type: 'post',
        dataType: 'json',
        data: {
            requiredFields: requiredFields,
            entryListDbId: entryListId
        },
        success: function(response){
            disabled = response > 0 || $('.summary').text() == '';
            $("#next-btn, #review-tab, #review-tab > a").toggleClass('disabled', disabled);
            $("#review-tab > a").attr('href', disabled ? '#' : '$urlNextTab');
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while checking the required fields. Please try again or file a report for assistance.';
	        Materialize.toast(notif, 5000);
        }
    });
}

// Show modal upon clicking bulk update button

$(document).on('click','#cm-bulk-update-btn',function(e) {
    $.ajax({
        type:"POST",
        dataType: 'json',
        url: '$urlModalBody',
        success: function(response) {
            var message = "Bulk update the following values for the cross list records. " +
                "Updating of cross method for selfing is not allowed and will be skipped (if any).<br> <br>";
            $('#cm-bulk-update-modal-body').html([message,response]);
            $('#bulk-update-selected').removeClass('disabled');
            $('#bulk-update-all').removeClass('disabled');
            $('#cancel-bulk-btn').show();
            $('#cm-bulk-modal').modal('show');
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while saving. Please try again or file a report for assistance.';
            Materialize.toast(notif, 5000);
        }
    });    
});

// Bulk update all crosses
$('#bulk-update-all').on('click', function(e) {
    var column = $('#req-vars').val();
    var value = $('#req-val').val();
    // Validation for bulk update modal

    if(column == ''){
        $('.toast').css('display','none');
        var notif = '<i class="material-icons orange-text left">warning</i>' + 'You have not selected/entered a value';
        Materialize.toast(notif, 5000);
    }

    else if(value == ''){
        $('.toast').css('display','none');
        var notif = '<i class="material-icons orange-text left">warning</i>' + 'You have not selected/entered a value';
        Materialize.toast(notif, 5000);
    }
    else{
        $('#cm-bulk-update-modal-body').html('');
        $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        $('#bulk-update-selected').addClass('disabled');
        $('#bulk-update-all').addClass('disabled');
        $('#cancel-bulk-btn').hide();
        // Save the changes in the database
        $.ajax({
            url: '$urlUpdateCrossAll',
            type: 'post',
            dataType: 'json',
            data: {
                column: column,
                value: value,
            }, 
            success: function(response){
                if(!response['success']){
                    $('.loading-div').html('');
                    $('#cm-bulk-modal').modal('hide');
                    var notif = '<i class="material-icons orange-text left">warning</i>' + response['message'];
                    $('.toast').css('display','none');
                    Materialize.toast(notif, 5000);
                }
                else{
                    // Close bulk modal
                    $('.loading-div').html('');
                    $('#cm-bulk-modal').modal('hide');
                    // Reload browser upon success
                    disableNextButton();
                    $.pjax.reload({container: '#$browserId-pjax'});
                }
            },
            error: function(error){
                $('.loading-div').html('');
                $('#cm-bulk-modal').modal('hide');
                notif = '<i class="material-icons red-text left">close</i> An error occurred while updating. Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        });
    }
});

// Bulk update selected crosses
$('#bulk-update-selected').on('click', function(e) {
    var column = $('#req-vars').val();
    var value = $('#req-val').val();
    // Validation for bulk update modal

    if(column == ''){
        $('.toast').css('display','none');
        var notif = '<i class="material-icons orange-text left">warning</i>' + 'You have not selected/entered a value';
        Materialize.toast(notif, 5000);
    }

    else if(value == ''){
        $('.toast').css('display','none');
        var notif = '<i class="material-icons orange-text left">warning</i>' + 'You have not selected/entered a value';
        Materialize.toast(notif, 5000);
    }
    else{
        var selection = $(this).data('selection');
        var currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));

        // no selected item(s)
        if(selection !== 'all' && (currentSelection == null || currentSelection.length == 0)){
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> Please select an item.";
                Materialize.toast(notif,2500);
            }
        }else{
            $('#cm-bulk-update-modal-body').html('');
            $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
            $('#bulk-update-selected').addClass('disabled');
            $('#bulk-update-all').addClass('disabled');
            $('#cancel-bulk-btn').hide();
            // Save the changes in the database
            $.ajax({
                url: '$urlUpdateCrossSelected',
                type: 'post',
                dataType: 'json',
                data: {
                    crossIds: currentSelection,
                    column: column,
                    value: value,
                }, 
                success: function(response){
                    if(!response['success']){
                        $('.loading-div').html('');
                        $('#cm-bulk-modal').modal('hide');
                        var notif = '<i class="material-icons orange-text left">warning</i>' + response['message'];
                        $('.toast').css('display','none');
                        Materialize.toast(notif, 5000);
                    }
                    else{
                        // Close bulk modal
                        $('.loading-div').html('');
                        $('#cm-bulk-modal').modal('hide');
                        disableNextButton();
                        // Reload browser upon success
                        $.pjax.reload({container: '#$browserId-pjax'});
                    }
                },
                error: function(error){
                    $('.loading-div').html('');
                    $('#cm-bulk-modal').modal('hide');
                    notif = '<i class="material-icons red-text left">close</i> An error occurred while updating. Please try again or file a report for assistance.';
                    Materialize.toast(notif, 5000);
                }
            });
        }
    }
});

JS;

$this->registerJs($script);






