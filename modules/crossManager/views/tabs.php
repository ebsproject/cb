<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders the header tabs for Cross Manager tool
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

// Set tool name
$toolName = Html::a( Yii::t('app', 'Cross Manager'), Url::to(['/crossManager','program' => $program]));

// Initializes nav bar dropdowns
Modal::begin([]);
Modal::end();

// Main tabs
$tabs = [
    'basic-info', 
    'add-parents', 
    'manage-parents', 
    'add-crosses', 
    'manage-crosses',
    'review'
];

// for ICN cross lists
if (isset($experimentDbId) && $experimentDbId) {
    $tabs = [
        'basic-info',
        'add-crosses',
        'manage-crosses',
        'review'
    ];
}

$tabCount = count($tabs);
$nextTab = $tabs[array_search($active, $tabs) + 1] ?? '';
$next = 'Next';

$nextTabUrl = Url::to(["$nextTab/load", 'program' => $program, 'id' => $entryListId]);
$urlEnableTabs = Url::to(['/crossManager/default/enable-tabs', 'program' => $program,'id' => $entryListId]);

if($active == 'review'){
    $next = 'Finalize';
    $nextTabUrl = '#';
}

if($active == 'basic-info'){
    $toolName = ($entryListId == 0) ? 'Create New Cross List' : 'Update Cross List';
    $nextTabUrl = '#';
}else{
    $toolName = "$toolName <small> &raquo; $entryListName </small>";
}

echo "<h3>$toolName<a id='next-btn' type='submit'  href='$nextTabUrl' class='btn btn-primary waves-effect waves-light pull-right' style='margin-bottom:10px;'>$next</a></h3>";

?>

<div class="col col-md-12" style="padding-right: 0px">
    <ul class="tabs">
        <?php
            $tabIndex = array_search($active, $tabs, true);

            // render tabs
            foreach ($tabs as $key => $value) {
                $label = str_replace('-', ' ', $value);

                // set url parameters
                $url = ["$value/load", 'program' => $program];
                if($entryListId !== 0){
                    $url['id'] = $entryListId;
                }

                $entryListStatus = $entryListStatus ?? "";
                // Validation if entry list is newly created or not
                if($entryListStatus != ""){
                    // set index based on status
                    switch($entryListStatus){
                        case "draft" :
                            $tabIndex = 1; break;
                        case "parents added" : 
                            $tabIndex = 2; break;
                        case "parent list specified" : 
                            $tabIndex = 3; break;
                        case "crosses added" : 
                            $tabIndex = 4; break;
                        case "cross list specified" : 
                            $tabIndex = 5; break;
                        default: 
                            $tabIndex = 0; break;
                    }
                }

                // disable tabs based on index
                $disabled = '';
                if($key > $tabIndex){
                    $url = '#';
                    $disabled = 'disabled';
                }

                // set active tab
                $linkClass = ($value == $active && $url !== '#') ? "active $disabled" : $disabled;

                echo "<li class='tab col $disabled' id='$tabs[$key]-tab'>".
                        '<a href="'.Url::to($url).'" class="'.$linkClass.' a-tab">
                            <span class="badge-step" style="padding:4px 7px;">'.($key + 1).'</span>'.
                        $label.'</a>
                    </li>';

            }
        ?>
    </ul>
</div>
<div class="row"></div>

<?php
// embedded styles
Yii::$app->view->registerCss('
    .badge-step {
        background-color: #ee6e73;
        display: inline-block;
        font-size: 11.84px;
        font-weight: 700;
        margin-right: 5px;
        line-height: 14px;
        color: #fff;
        vertical-align: baseline;
        white-space: nowrap;
        text-shadow: 0 -1px 0 rgba(0,0,0,.25);
        -webkit-border-radius: 50%;
    }
');
?>