<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders the header tabs for View
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

 // Set tool name
$toolName = Html::a( Yii::t('app', 'Cross Manager'), Url::to(['/crossManager','program' => $program]));

// Initializes nav bar dropdowns
Modal::begin([]);
Modal::end();

// Main tabs
$tabs = [
    'basic',
    'crosses'
];

$tabCount = count($tabs);
$nextTab = $tabs[array_search($active, $tabs) + 1] ?? '';
$toolName = "$toolName <small> &raquo; $entryListName</small>";

echo "<h3>$toolName</h3>";
?>

<div class="col col-md-12" style="padding-right: 0px">
    <ul class="tabs">
        <?php
            $step = array_search($active, $tabs, true);

            // render tabs
            foreach ($tabs as $key => $value) {
                $label = str_replace('-', ' ', $value);

                // condition based on tab selected
                if($value == 'basic'){
                    // set url parameters
                    $url = ["basic-info/index", 'program' => $program];
                }
                else if($value == 'crosses'){
                    // set url parameters
                    $url = ["cross-data/load", 'program' => $program];
                }

                if($entryListId !== 0){
                    $url['id'] = $entryListId;
                }

                $disabled = '';

                // set active tab
                $linkClass = ($value == $active && $url !== '#') ? "active $disabled" : $disabled;

                echo "<li class='tab col $disabled' id='$tabs[$key]-tab'>".
                        '<a href="'.Url::to($url).'" class="'.$linkClass.' a-tab">
                            <span class="badge-step" style="padding:4px 7px;"></span>'.
                        $label.'</a>
                    </li>';
            }
        ?>
    </ul>
</div>
<div class="row"></div>




