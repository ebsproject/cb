<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

use \app\models\Variable;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

// Render tabs
echo Yii::$app->controller->renderPartial('/view/tabs', [
    'entryListId' => $entryListId,
    'entryListName' => $entryListName,
    'experimentDbId' => $experimentDbId,
    'program' => $program, 
    'active' => 'crosses',
]);

//id of cm view-cross-list browser
$browserId = 'dynagrid-cm-view-cross-grid';

// Define URLs
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

//set columns and attributes
$columns = [
    [
        'class' => '\kartik\grid\SerialColumn',
        'header' => false,
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'vAlign' => 'top',
        'hAlign' => 'left',
        'width' => '20px'
    ],
    [
        'attribute' => 'harvestStatus',
        'format' => 'raw',
        'visible' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
            'NO_HARVEST' => 'no harvest',
            'INCOMPLETE' => 'incomplete harvest data',
            'CONFLICT' => 'data conflict',
            'READY' => 'ready for seed creation',
            'IN_QUEUE' => 'in queue for creation',
            'IN_PROGRESS' => 'seed creation in progress',
            'COMPLETED' => 'harvest complete',
            'DELETION_IN_PROGRESS' => 'seed/trait deletion in progress',
            'INVALID_STATE' => 'invalid state',
            'FAILED' => 'seed creation failed'
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => ['multiple'=>false, 'allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Select status'],
        'value' => function ($model) {
            $harvestStatus = isset($model['harvestStatus']) ? $model['harvestStatus'] : 'NO_STATUS';
            $badge = '';

            if($harvestStatus=='NO_HARVEST') {
                $badge = '<span title="No seeds created yet" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'NO HARVEST') . '</strong></span>';
            } else if (strpos($harvestStatus,'INCOMPLETE')!==false) {
                $badge = '<span title="Harvest data is incomplete" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'INCOMPLETE DATA') . '</strong></span>';
            } else if ($harvestStatus=='READY') {
                $badge = '<span title="Cross is ready for seed and package creation" class="new badge center blue darken-2"><strong>' . \Yii::t('app', 'READY') . '</strong></span>';
            } else if ($harvestStatus=='IN_QUEUE') {
                $badge = '<span title="In queue for seed and package creation" class="new badge center grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'IN QUEUE') . '</strong></span>';
            } else if ($harvestStatus=='IN_PROGRESS') {
                $badge = '<span title="Seed and package creation is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'IN PROGRESS') . '</strong></span>';
            } else if ($harvestStatus=='FAILED') {
                $badge = '<span title="Seed and package creation failed" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'FAILED') . '</strong></span>';
            } else if ($harvestStatus=='COMPLETED') {
                $badge = '<span title="Seeds and packages have been created" class="new badge center green darken-2"><strong>' . \Yii::t('app', 'HARVEST COMPLETE') . '</strong></span>';
            } else if ($harvestStatus=='DELETION_IN_PROGRESS') {
                $badge = '<span title="Deletion of traits and seeds/packages is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'DELETION IN PROGRESS') . '</strong></span>';
            } else if (strpos($harvestStatus,'CONFLICT')!==false) {
                $parts = explode(': ', $harvestStatus);
                $title = $parts[1] ?? 'Unknown conflict';
                $badge = '<span title="' . $title . '" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'DATA CONFLICT') . '</strong></span>';
            } else {
                $badge = '<span class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
            }

            return $badge;
        },
    ],
    [
        'attribute' => 'crossName',
    ],
    [
        'attribute' => 'crossFemaleParent',
        'label' => \Yii::t('app','Female Parent'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'femaleParentage',
        'headerOptions'=>['class'=>'red lighten-4', 'style' => [
            'min-width' => '200px'
        ],],
        'contentOptions'=>['class'=>'red lighten-5 germplasm-name-col'],
    ],
    [
        'attribute' => 'crossMaleParent',
        'label' => \Yii::t('app','Male Parent'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'maleParentage',
        'headerOptions'=>['class'=>'blue lighten-4', 'style' => [
            'min-width' => '200px'
        ]],
        'contentOptions'=>['class'=>'blue lighten-5 germplasm-name-col'],
    ],
    [
        'attribute' => 'crossMethod',
        'headerOptions' => [
            'style' => [
                'min-width' => '200px'
            ],
        ],
    ],
    [
        'attribute' => 'crossRemarks',
        'headerOptions' => [
            'style' => [
                'min-width' => '150px'
            ],
        ],
    ],
    [
        'attribute' => 'femaleParentEntryNumber',
        'label' => \Yii::t('app','Female Entry No.'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleParentEntryNumber',
        'label' => \Yii::t('app','Male Entry No.'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleEntryCode',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleEntryCode',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleParentSeedSource',
        'label' => \Yii::t('app','Female Seed Source'),
        'visible' => false,
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleParentSeedSource',
        'label' => \Yii::t('app','Male Seed Source'),
        'visible' => false,
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleOccurrenceName',
        'label' => \Yii::t('app','Female Occurrence'),
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleOccurrenceName',
        'label' => \Yii::t('app','Male Occurrence'),
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
    [
        'attribute' => 'femaleOccurrenceCode',
        'visible' => false,
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
    ],
    [
        'attribute' => 'maleOccurrenceCode',
        'visible' => false,
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
    ],
];

$traitAttributes = [];
$traitLabels = [];
$varFields = '';
$variableModel = new Variable();

//plot data
foreach ($traits as $value) {

    $variable = $variableModel->searchAll(["abbrev" => $value], '');
    if(!empty($variable['data'])){
        $label = $variable['data'][0]['label'];
        $columns[] = [
            'attribute' => $value,
            'visible' => true,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'format' => 'html',
            'label' => ucwords(str_replace('_', ' ', strtolower($label)), ' '),
            'value' => function ($model) use ($value) {
                $dataValue = $model[$value]['dataValue'] ?? '';
                if ( isset ($model[$value]['dataQCCode']) && $model[$value]['dataQCCode'] == 'S') {
                    $dataValue = '<span class="grey lighten-2" style="padding:5px;">' . $dataValue . '</span>';
                }
                return $dataValue;
            }
        ];
    }
}

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $crossModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', 'Review your crosses as needed.').'{summary}<br/>
                <p id="total-selected-text" class = "pull-right" style = "margin: -1px;">
                </p>',
            'after' => false,
        ],
        'toolbar' => 
            [
                ['content' =>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                    ["/crossManager/cross-data/load?program=$program&id=$entryListId"], 
                    [
                        'id'=>'reset-'.$browserId,
                        'class' => 'btn btn-default', 
                        'title'=>\Yii::t('app','Reset cross browser'), 
                        'data-pjax' => true
                    ]).'{dynagridFilter}{dynagridSort}{dynagrid}'
                ]
            ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// disable horizontal scroll
$this->registerCss(".body-container { overflow-x: hidden; }");

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS

$(document).ready(function() {
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();
});

resize();

// Perform after pjax success of browser
$(document).on('ready pjax:success', '#$browserId-pjax', function(e) {
    e.stopPropagation();
    e.preventDefault();
    resize();
});

/**
 * Resize the grid table upon loading
 */
function resize(){
    var maxHeight = ($(window).height() - 320);
    $(".kv-grid-wrapper").css("height", maxHeight);
}

JS;
$this->registerJs($script);