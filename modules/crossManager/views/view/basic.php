<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>
*/

use yii\helpers\Html;
use yii\helpers\Url;

// Render tabs
echo Yii::$app->controller->renderPartial('/view/tabs', [
    'entryListId' => $entryListId,
    'entryListName' => $entryListName,
    'experimentDbId' => $experimentDbId,
    'program' => $program, 
    'active' => 'basic',
]);

// If required values are empty, column values replace to $notSet
$notSet = '<span class="not-set">(not set)</span>';
foreach($entryListInfo as $var => $values){
    if(empty($values)){
        $entryListInfo[$var] = $notSet;
    }
}
?>

<div class="main-body">
    <div class="browser-detail-view-viewer-inner">
        <div class="viewer-body">
            <!-- First column -->
            <div class="col col-lg" style="padding:1%">
                <div class="col col-md-14">
                    <div class="viewer-display-name">
                    </div>
                </div>
                <div class="row viewer-primary-info">
                    <div class="col s3">
                        <div class="viewer-primary-info viewer-detail-item">
                            <div class="viewer-label">
                                <strong>Cross List Name</strong>
                            </div>
                            <div class="viewer-value">
                                <?php echo $entryListInfo['entryListName']?>
                            </div>
                        </div> <br> 
                        <div class="viewer-primary-info viewer-detail-item">
                            <div class="viewer-label">
                                <strong>Experiment Year</strong>
                            </div>
                            <div class="viewer-value">
                                <?php echo $entryListInfo['experimentYear']?>
                            </div>
                        </div> <br>
                        <div class="viewer-primary-info viewer-detail-item">
                            <div class="viewer-label">
                                <strong>Description</strong>
                            </div>
                            <div class="viewer-value" title="$title">
                                <?php echo $entryListInfo['description']?>
                            </div>
                        </div>
                    </div>
                    <div class="col s3"> </div>
                    <div class="col s6">
                        <div class="viewer-primary-info viewer-detail-item">
                                <div class="viewer-label">
                                    <strong>Site</strong>
                                </div>
                                <div class="viewer-value" title="$title">
                                    <?php echo $entryListInfo['geospatialObjectName']?>
                                </div>
                        </div> <br>
                        <div class="viewer-primary-info viewer-detail-item">
                                <div class="viewer-label">
                                    <strong>Season</strong>
                                </div>
                                <div class="viewer-value" title="$title">
                                    <?php echo $entryListInfo['seasonCode']?>
                                </div>
                        </div> <br>
                        <div class="viewer-primary-info viewer-detail-item">
                                <div class="viewer-label">
                                    <strong>Experiment Type</strong>
                                </div>
                                <div class="viewer-value" title="$title">
                                    <?php echo $entryListInfo['experimentType']?>
                                </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .main-body{
        background-color: #fff;
        margin: 10px;
    }
</style>

<?php