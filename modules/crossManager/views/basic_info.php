<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Cross Manager Basic Info Tab
 */


// Import classes
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\GenericFormWidget;

$nextTab = $experimentDbId ? 'add-crosses' : 'add-parents';

// Define URLs
$urlNextTab = Url::to(["/crossManager/$nextTab/load", 'program' => $program]);
$checkEntryListNameUrl = Url::to(['/crossManager/basic-info/check-entry-list-name', 'program'=>$program]);

// Render tabs
echo Yii::$app->controller->renderPartial('/tabs', [
    'entryListId' => $entryListId,
    'experimentDbId' => $experimentDbId,
    'program' => $program, 
    'active' => 'basic-info',
    'nextTab' => $nextTab,
    'entryListStatus' => $entryListStatus
]);

// Display basic info form
$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'save-cross-list-form',
    'action' => ['basic-info/save', 'id'=>$entryListId, 'programDbId'=>$programDbId, 'program'=>$program],
]);

echo GenericFormWidget::widget([
    'options' => $widgetOptions
]);
ActiveForm::end();
?>

<?php

// Modal for entry list name updates
Modal::begin([
    'id' => 'entry-list-name-modal',
    'header' => '<h4><i class="fa fa-bell"></i>  '.\Yii::t('app','Notification').'</h4>',
    'footer' => Html::a(\Yii::t('app','Cancel'), ['#'], [ 'id'=>'name-cancel-save-btn', 'data-dismiss'=>'modal' ]).'&emsp;&nbsp;'.
        Html::a(\Yii::t('app','Confirm'),'#', [ 'id'=>'update-name-confirm' , 'class'=>'btn btn-primary waves-effect waves-light modal-close' ]).'&emsp;',
    'size' => 'modal-lg',
    'options' => [ 'data-backdrop'=>'static' ]
]);
    echo '<div class="row" id="entry-list-name-notif"></div>';
Modal::end();
?>

<?php
$this->registerJsFile("@web/js/generic-form-validation.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS

var duplicateNameFlag = false;
var updateFlag = false;
var updateEntryListNameFlag = false;

// styling adjustments
$('.main-panel').css('height', $('.left-side-nav').height() - 100);
$('#basic-info1, #basic-info2').addClass('col col-md-12');
disableNextButton();

enableEntryListNameField();

$('.required-field').on('input change keyup', function() {
    disableNextButton();
});

$(document).on('click', '#next-btn', function(){
    // disable next button
    $("#next-btn").toggleClass('disabled', false);

    var actionUrl = $("#save-cross-list-form").attr("action");
    var formData = $.param($.map($("#save-cross-list-form").serializeArray(), function(v,i) {
        return v;
    }));
    
    if (updateFlag === false) {
        checkEntryListName();
        if (duplicateNameFlag === true) {
            $('#entry-list-name-modal').modal('show');
            let notif = " The entry list name specified is not unique. Click <b>Confirm</b> to proceed.";
            $('#entry-list-name-notif').html(
                '<div class="alert ">'+ 
                    '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                        '<i class="fa fa-info-circle"></i>'+
                            notif+ 
                    '</div>'+
                '</div>'
            );
        } else {
            // Entry list name is not duplicated so proceed with update
            updateFlag = true;
        }
    }
    if (updateFlag === true) {
        $.ajax({
            url: actionUrl,
            type: 'post',
            dataType: 'json',
            data: formData,
            success: function(response) {
                // Redirect user to next tab
                if (response['id']) {
                    window.location.href = '$urlNextTab' + '&id=' + response['id'] + '&success=' + response['operation'];
                } else {
                    notif = '<i class="material-icons red-text left">close</i> No new record created. Please try again or file a report for assistance.';
                    Materialize.toast(notif, 5000);
                }
            },
            error: function(e) {
                notif = '<i class="material-icons red-text left">close</i> An error occurred while saving. Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        });
    }
});

$('#update-name-confirm').on('click', function() {
    updateFlag = true;
    $('#next-btn').trigger('click');
    $('#entry-list-name-modal').modal('hide');
});

// Enable / Disable editing of cross list name field
$(document).on('change','.default-namegen-switch', function() {
    var fieldId = this.id;
    var fieldIdArr = fieldId.split('generate-');
    
    if($(this).prop('checked') == false){
        $('#'+fieldIdArr[1]).removeClass('disabled');
        $('#'+fieldIdArr[1]).removeClass('generated-pattern');
        $('#'+fieldIdArr[1]).removeAttr('readonly');

        updateEntryListNameFlag = true
    } 
    else {
        $('#'+fieldIdArr[1]).addClass('disabled');
        $('#'+fieldIdArr[1]).addClass('generated-pattern');
        $('#'+fieldIdArr[1]).attr('readonly','readonly');
        $('#'+fieldIdArr[1]).val(''); 

        updateEntryListNameFlag = false
    }

    disableNextButton()
})

// Enable editing of entry list name
function enableEntryListNameField(){
    let fieldIdSplit
    let fieldId
    $("input, .editable-field").each(function() {
        fieldIdSplit = this.id.split('-')
        fieldId = fieldIdSplit[0]

        if(fieldId != 'ENTRY_LIST_NAME'){
            $(this).prop('disabled', $experimentDbId);
        }

    });
}

/**
* Sets flag if entry list name already exists in the database
*/
function checkEntryListName() {
    let inputEntryListName = '';
    
    $('.required-field').each(function() {
       let fieldId = $(this).attr('id');
       
       if (fieldId.includes('ENTRY_LIST_NAME')) {
           inputEntryListName = $('#'+fieldId).val();
       }
    });
    
    $.ajax({
       url: '$checkEntryListNameUrl',
       type: 'post',
       dataType: 'json',
       data: {
           id: '$entryListId',
           inputEntryListName: inputEntryListName
       },
       async: false,
       cache: false,
       success: function(response) {
           duplicateNameFlag = response['duplicateFlag'];
       },
       error: function(e) {
           console.log(e);
       }
    });
}

/**
 * Disable next button if required fields are not yet filled
 */
function disableNextButton(){
    let disableNextButton = false;
    let nextTab = '#' + '$nextTab' + '-tab';    

    $('.required-field').each(function(){

        let fieldId = this.id
        let fieldIdArr = fieldId.split('-')

        let hasEntryListName = (fieldIdArr[0] == 'ENTRY_LIST_NAME') && (updateEntryListNameFlag == false || $("#"+this.id).val() != '')
        if($("#"+this.id).val() == '' && hasEntryListName == false){
            disableNextButton = true;
            return false;
        }
    });

    $("#next-btn").toggleClass('disabled', disableNextButton);
}

JS;

$this->registerJs($script);

// embedded styles
Yii::$app->view->registerCss('
label{
    font-size: .8rem;
    color: #333333;
}
.div-class{
    margin-bottom: 10px;
}
.my-container{
    min-width: 50%;
    max-width: 90% !important;
}
.card{
    height: 100%;
    margin-left:-10px;
    margin-top:-10px;
    padding: 30px;
}
.form-inline input{
    flex-direction: column;
    align-items: stretch;
    vertical-align: middle;
}
.form-inline { 
    display: flex;
    flex-flow: row wrap;    
    align-items: center;
}
.view-entity-content{
    background: #ffffff;
}
#basic-info1{
    margin-left:auto;
    margin-right:auto;
    margin-top:15px;
    display: inline-block;
}
#basic-info2{
    margin-left:auto;
    margin-right:auto;
    margin-top:20px;
    display: inline-block;
}
#basic-info2 > div:nth-child(2){
    margin-top:20px;
}
.parent-box{
    position: relative;
    margin-top: 1rem;
}
.main-panel{
    margin-left: 5%;
}

');