<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Cross Manager landing page and 
 * cross list browser
 */

// Import app components
use app\components\FavoritesWidget;
use app\components\PrintoutsWidget;

// Import yii
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

// Import kartik classes
use kartik\date\DatePicker;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\widgets\Select2;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

// Set tool name
$toolName = Yii::t('app', 'Cross Manager') .
FavoritesWidget::widget([
    'module' => 'crossManager'
]);

// Browser Id
$browserId = 'dynagrid-cm-cross-list-grid';

$cmPrintoutsSession = 'cm-printouts';
// Initialize printouts component
$render = Yii::$app->access->renderAccess("CROSS_MANAGER_PRINTOUTS" , "CROSS_MANAGER");
$printouts = !$render ? '' : PrintoutsWidget::widget([
    'product' => 'Cross Manager',
    'program' => $program,
    'occurrenceIds' => [],
    'checkboxSessionStorage' => $cmPrintoutsSession,
    'entity' => 'cross list',
]);

//Modal for selecting occurrence for harvesting
Modal::begin([
    'id' => 'harvest-crosses-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align: bottom;">agriculture</i> '.\Yii::t('app','Harvest Crosses').'</h4>',
    'footer' =>
        Html::a(
            'Cancel',
            '#',
            [
                'id' => 'harvest-crosses-modal-cancel-btn',
                'data-dismiss'=> 'modal'
            ]
        ) .  '&emsp;' .
        Html::a(\Yii::t('app', 'Go to Harvest Manager'), '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light printouts-btn hide-loading disabled',
                'id' => 'harvest-crosses-modal-confirm-button'
            ]
        ) .  '&emsp;',
    'options' => [
        'data-backdrop'=>'static',
        'style'=>'z-index: 1040;'
    ]
]);
echo '<p>'.\Yii::t('app','Select an occurrence to proceed:').'</p>';
echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Occurrence'.' '.'<span class="required">*</span>').'</label>';
echo Select2::widget([
    'name' => 'HarvestOccurrenceSelection',
    'id' => 'harvest-selected-occurrence',
    'options' => [
        'placeholder' => 'Select an occurrence',
        'tags' => true,
        'class'=>'harvest-selected-occurrence',
        'style' => 'overflow-x: visible; overflow-y: visible;width:88%;',
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 0
    ],
]);
Modal::end();

// Define module-level variables
$selectedCount = !empty($selectedEntryListItems) && count($selectedEntryListItems) > 0 ? number_format(count($selectedEntryListItems)) : 'No';

// Define URLs
$resetEntryListBrowserUrl =  Url::to(['/crossManager/default/index', 'program'=>$program]);
$storeSessionItemsUrl = Url::to(['/crossManager/default/store-session-items']);
$getSelectedItemsCountUrl = Url::to(['/crossManager/default/get-user-selection-count']);
$getOccurrencesUrl = Url::to(['/crossManager/default/get-occurrences']);
$canDeleteListUrl = Url::to(['/crossManager/default/can-delete-list']);
$deleteListUrl = Url::to(['/crossManager/default/delete-list-in-background']);
$urlBasicInfo = Url::to(['/crossManager/basic-info/load', 'program'=>$program]);
$urlViewCrossList = Url::to(['/crossManager/basic-info/index', 'program'=>$program]);
$newNotificationsUrl = Url::to(['/crossManager/default/new-notifications-count']);
$notificationsUrl = Url::to(['/crossManager/default/push-notifications']);
$protocolsUrl = Url::to(['/crossManager/protocol/load', 'program'=>$program]);
$exportOtherDataCollectionFilesUrl = Url::to(['/crossManager/default/export-data-collection-other-cross-files','entryListDbId' => '', 'dataCollectionCrossFileName' => '', 'crossCount' => '']);

// URLs for dashboard config
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// Set columns
$columns = [
    [
        'header' => '
            <input type="checkbox" class="filled-in" id="cm-cross-list-select-all" disabled hidden/>
            <label style="padding-left: 20px;" for="cm-cross-list-select-all" disabled class="hidden"></label>
        ',
        'label' => Yii::t('app', 'Checkbox'),
        'content' => function($data) use ($selectedEntryListItems, $browserId) {
            $entryListDbId = $data['entryListDbId'];
            $experimentDbId = $data['experimentDbId'];
            $status = $data['entryListStatus'];

            // Checkboxes per row
            if (isset($selectedEntryListItems[$entryListDbId])) {
                $checkbox = '<input 
                class="'.$browserId.'-select filled-in" 
                type="checkbox" 
                checked="checked" 
                id="'.$entryListDbId.'"
                data-experiment-db-id="'.$experimentDbId.'" 
                data-status="'.$status.'" 
            />';
            } else {
                $checkbox = '<input 
                class="'.$browserId.'-select filled-in" 
                type="checkbox" 
                id="'.$entryListDbId.'"
                data-experiment-db-id="'.$experimentDbId.'" 
                data-status="'.$status.'" 
            />';
            }

            if ($status === 'deletion in progress') {
                $label = '<label style="padding-left: 20px; for="'.$entryListDbId.'" disabled class="hidden"></label>';
            } else {
                $label = '<label style="padding-left: 20px; for="'.$entryListDbId.'" ></label>';
            }

            return $checkbox.$label;
        },
        'hAlign' => 'center',
        'vAlign' => 'top',
        'mergeHeader' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'order'=> DynaGrid::ORDER_FIX_LEFT,
        'header' => false,
        'template' => '{view}{export}{update}{delete}',
        'noWrap' => true,
        'buttons' => [
            'view' => function($url, $data, $key) {

                $render = Yii::$app->access->renderAccess("CROSS_MANAGER_VIEW" , "CROSS_MANAGER", $data['creatorDbId']);

                return !$render ? '' : Html::a('<i class="material-icons">remove_red_eye</i>', 
                    '#',
                    [
                        'class' => 'view-crosslist',
                        'title' => Yii::t('app','View '). $data['entryListName'],
                        'data-id' => $data['entryListDbId'],
                        'style' => 'padding-right: 8px',
                    ]
                );
            }, 
            'update' => function ($url, $data, $key) {
                $status = $data['entryListStatus'];
                if($status == 'draft' || $status == 'cross list specified' || $status == 'crosses added' ||
                  $status == 'parents added' || $status == 'parent list specified'){

                    $render = Yii::$app->access->renderAccess("CROSS_MANAGER_UPDATE" , "CROSS_MANAGER", $data['creatorDbId']);
                    return !$render ? '' :  Html::a('<i class="material-icons">edit</i>', 
                        '#',
                        [
                            'class' => 'update-crosslist',
                            'title' => Yii::t('app','Update '). $data['entryListName'],
                            'data-id' => $data['entryListDbId'],
                            'data-status' => $status
                        ]
                    );
                }
            },
            'export' => function($url, $data, $key) use ($downloadDataCollectionCrossDropdownItemParams) {
                $crossListStatus = $data['entryListStatus'];

                if ($crossListStatus == 'cross list specified' || $crossListStatus == 'crosses added' || $crossListStatus == 'finalized') {
                    $crossListId = $data['entryListDbId'];

                    $htmlDropdownItems = '';
                    foreach ($downloadDataCollectionCrossDropdownItemParams as $value) {
                        $value['options']['data-entry-list-id'] = $data['entryListDbId'];
                        $value['options']['data-cross-count'] = $data['crossCount'];

                        $dropdownItem = Html::a(
                            $value['text'],
                            '#',
                            $value['options']
                        );

                        // Concatenate dropdown items together
                        $htmlDropdownItems .= "<li>$dropdownItem</li>";
                    }

                    $render = Yii::$app->access->renderAccess("CROSS_MANAGER_DOWNLOAD_FILES" , "CROSS_MANAGER", $data['creatorDbId']);

                    return !$render ? '' :
                        "<span class='dropdown bootstrap-dropdown' title='Download files' style='margin-left: -4px;padding-right:4px;'>
                            <a class='dropdown-toggle' type='button' data-toggle='dropdown'><i class='material-icons'>file_download</i></a>
                            <ul class='dropdown-menu'>
                                $htmlDropdownItems
                            </ul>
                        </span>";
                }
            },
            'delete' => function ($url, $data, $key) {

                $status = $data['entryListStatus'];

                if ($status == 'draft' || $status == 'cross list specified' || $status == 'crosses added' ||
                    $status == 'parents added' || $status == 'parent list specified') {

                    $render = Yii::$app->access->renderAccess("CROSS_MANAGER_DELETE" , "CROSS_MANAGER", $data['creatorDbId']);
                    return !$render ? '' : Html::a('<i class="material-icons">delete</i>', 
                        '#',
                        [
                            'class' => 'delete-crosslist',
                            'title' => Yii::t('app','Delete '). $data['entryListName'],
                            'data-id' => $data['entryListDbId'],
                            'data-crosslist-name' => $data['entryListName'],
                            'data-crosslist-status' => $status
                        ]
                    );
                }
            },
            'more' => function($url, $data, $key) {
                $status = $data['entryListStatus'];
                if ($status == 'draft' || $status == 'cross list specified' || $status == 'crosses added' ||
                    $status == 'parents added' || $status == 'parent list specified') {
                    $crossListId = $data['entryListDbId'];
                    $crossListStatus = $data['entryListStatus'];

                    return Html::a('<i class="material-icons">more_vert</i>',
                            '#',
                            [
                                'style' => 'cursor: pointer; margin-top: 10px; margin-left: 5px; margin-right: 5px;',
                                'class' => 'more-crosslist more-crosslist'.$crossListId.' dropdown-trigger dropdown-button-pjax',
                                'title' => Yii::t('app', 'More Action'),
                                'label' => 'More Action',
                                'data-id' => $crossListId,
                                'data-activates' => 'more-crosslist-dropdown-actions-'.$crossListId,
                                'data-beloworigin' => 'true',
                                'data-constrainwidth' => 'false',
                            ]).'&emsp;'.
                        "<ul id='more-crosslist-dropdown-actions-".$crossListId."' class='dropdown-content' style='width:145px; position: static !important;'>
                        <li>".
                        Html::a('<span class="material-icons" style=" font-size: 20px; vertical-align: -4px; margin-right: 4px;" class="material-icons">delete</span><p style="display: inline">'.\Yii::t('app', 'Delete list').'</p>',
                            '#',
                            [
                                'style' => 'cursor: pointer; margin-top: 10px; margin-left: 5px; margin-right: 5px; padding-left:5px; padding-right:0px;',
                                'class' => 'delete-crosslist',
                                'title' => \Yii::t('app', 'Delete List'),
                                'data-id' => $crossListId,
                                'data-crosslist-name' => $data['entryListName'],
                                'data-crosslist-status' => $crossListStatus,
                            ]
                        )
                        ."</li>
                    </ul>";
                }
            },
        ],
        'vAlign' => 'top',
        'hAlign' => 'left',
    ],
    [
        'attribute' => 'entryListStatus',
        'label' => Yii::t('app', 'Status'),
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $statusOptions,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'filterInputOptions' => [
            'placeholder' => 'Select Status',
        ],
        'value' => function($data){
            $status = $data['entryListStatus'];
            switch ($status) {
                case 'completed':
                    $class = 'green darken-3';
                    break;
                case 'finalized':
                    $class = 'green';
                    break;
                case 'crosses added':
                case 'parents added':
                    $class = 'yellow darken-3';
                    break;
                case 'cross list specified':
                case 'parent list specified':
                    $class = 'blue';
                    break;
                case 'deletion in progress':
                    $class = 'yellow darken-2';
                    break;
                case 'deletion failed':
                    $class = 'red';
                    break;
                default:
                    $class = 'grey';
                    break;
            }
            return '<span class="new badge ' . $class . '" style="margin: 1px"><b>' . strtoupper($status) . '</b></span>';
        },
        'contentOptions' => [
            'style'=>'min-width: 150px;'
        ],
    ],
    [
        'attribute' => 'entryListName',
        'label' => Yii::t('app', 'Cross List'),
        'contentOptions' => [
            'style'=>'min-width: 150px;'
        ],
    ],
    [
        'attribute' => 'crossCount',
        'value' => function ($data) {
            return number_format($data['crossCount']);
        }
    ],
    [
        'attribute' => 'geospatialObjectName',
        'label' => Yii::t('app', 'Site')
    ],
    [
        'attribute' => 'experimentYear',
    ],
    [
        'attribute' => 'seasonCode',
        'label' => Yii::t('app', 'Season')
    ],
    [
        'attribute' => 'creator'
    ],
    [
        'attribute' => 'creationTimestamp',
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterInputOptions' => [
            'autocomplete' => 'off'
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'startDate'=> date("1970-01-01"),
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
    ],
    [
        'attribute' => 'experimentType',
        'visible' => false
    ],
];

$render = Yii::$app->access->renderAccess("CROSS_MANAGER_CREATE" , "CROSS_MANAGER");
$createButton = !$render ? '' : '<a 
    class = "light-green darken-3 btn tooltipped" 
    style = "margin-right:5px;"
    id = "create-cross-list-btn"
    data-position = "top" data-delay="50" 
    data-tooltip = "'.Yii::t('app','Create a new cross list').'">
    <strong>'.\Yii::t('app','Create').'</strong>
</a>';

$notificationButtonText = \Yii::t('app', 'Notifications');
$notificationButton = '<a
    class="btn waves-effect waves-light cm-notification-button tooltipped"
    style="margin-right:5px;overflow: visible !important;"
    id="notif-btn-id"
    data-position="top"
    data-tooltip="'.$notificationButtonText.'"
    data-activates="notifications-dropdown"
    data-pjax=0>
    <i class="material-icons large">notifications_none</i><span id="notif-badge-id"></span>
</a>';

$accessPointsDropdownTitleText = Yii::t('app', 'Access breeding tools');
$harvestManagerButtonTitleText = Yii::t('app', 'Go to Harvest Manager');
$render = Yii::$app->access->renderAccess("HARVEST_OCCURRENCE" , "HARVEST_MANAGER");
$accessPointsButton = !$render ? '' : "<a 
    title='$accessPointsDropdownTitleText'
    id='access-points-dropdown'
    class='dropdown-trigger btn'
    href=''
    data-activates='access-points-dropdown-list'
    data-beloworigin='true' 
    data-constrainwidth='false'
    style='margin-right: 6px;'
    >
    <span
        class='material-icons'
        style='
            font-size: 16px;
            vertical-align: -4px;
            margin-right: 4px;
        '
    >
        handyman
    </span> <span class='caret'></span>
    </a>
    <ul
        id='access-points-dropdown-list'
        class='dropdown-content'
        data-beloworigin='false'
    >" .
        // Harvest Manager Access Point
        "<li
                id='harvest-manager-btn'
                class='hide-loading'
                title='$harvestManagerButtonTitleText'
            >" .
        Html::a(
            '
                <span
                    class="material-icons"
                    style="
                        font-size: 20px;
                        vertical-align: -4px;
                        margin-right: 4px;
                    "
                >
                    agriculture
                </span>
                <p style="display: inline;">Harvest Manager</p>
            ',
            null,
            [
                'class' => 'dropdown-trigger',
                'data-activates' => 'hm-dropdown-harvest-options',
                'data-alignment' => 'left',
                'data-hover' => 'hover',
            ]
        ) .
            "<ul
                id='hm-dropdown-harvest-options'
                class='dropdown-content'
                style='
                    max-width: 112px;
                    margin-left: -113px;
                '
            >" .
                // HM Access Point for Harvesting Crosses
                "<li title='Load Crosses for harvesting'>
                    <a
                        data-data-level='cross'
                        class='harvest-crosses-btn'
                    >Harvest Crosses</a>
                </li>" .
            "</ul>" .
        "</li>" .
    "</ul>";

$updateButtonDropdownTitleText = Yii::t('app', 'Update cross list-level data');
$updateProtocolsButtonTitleText = Yii::t('app', 'Update cross list-level protocol variables');
$updateButton = "
    <a
        title='$updateButtonDropdownTitleText'
        id='update-actions-dropdown'
        class='dropdown-trigger btn'
        data-activates='update-actions-list'
        data-beloworigin='true' 
        data-constrainwidth='false'
        >
        <span class='glyphicon glyphicon-edit'></span>
        <span class='caret'></span>
    </a>
    
    <ul id='update-actions-list' class='dropdown-content'>
        <!-- Update cross list protocols entry point -->
        <li 
            id='update-cross-list-protocols-btn'   
            class='hide-loading' 
            title='$updateProtocolsButtonTitleText'>" .
            Html::a(
                'Update cross list protocols'
            ) . "        
        </li> 
    </ul>
";
$render = Yii::$app->access->renderAccess("CROSS_MANAGER_DOWNLOAD_CROSS_DC_FILE" , "CROSS_MANAGER");
$downloadButton = !$render ? '' : "<a 
        class='dropdown-trigger btn'
        data-activates='download-actions'
        data-beloworigin='true' 
        data-constrainwidth='false'
        title='".Yii::t('app','Download cross data collection files')."'>
        <i class='material-icons inline-material-icon'>file_download</i>
        <span class='caret'></span>
    </a>

    <ul id='download-actions' class='dropdown-content' style='width:160px !important' data-beloworigin='false'>" .
        "<li id='trait-data-collection-option' title='Access Data Collection'>" .
        Html::a(
            '<i class="material-icons inline-material-icon" style="font-size: 18px;">file_download</i> Trait data collection',
            '#',
            [
                'class' => 'dropdown-trigger',
                'data-hover' => 'hover',
                'data-alignment' => 'left'
            ]
        ) .
        "</li>" .
    "</ul>";

$render = Yii::$app->access->renderAccess("UPLOAD_DATA" , "QUALITY_CONTROL");
$uploadButton = !$render ? '' : Html::a(
        '<i class="material-icons inline-material-icon">file_upload</i>',
        [
            '/dataCollection/upload',
            'program' => $program,
            'source' => 'cm-main' // To redirect here from Data Collection
        ],
        [
            'class' => 'btn',
            'title' => Yii::t('app','Upload cross data collection files')
        ]
    );

// Cross list data browser
DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $crossListModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => $browserId . '-id',
                'enablePushState' => false
            ],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => "<h3>$toolName</h3>",
            'before' =>
                \Yii::t('app', "Cross list browser: Select cross list for further managing your crosses.").'
                {summary}<br/>
                <span id="selected-items" class="pull-right">
                    <div class="hidden preloader-wrapper male-loader small active" style="width:10px;height:10px;">
                        <div class="spinner-layer spinner-green-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="gap-patch">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                    <b id="selectedCount">'.$selectedCount.'</b><span id="selectedText"> selected items</span>.
                </span>',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' => $createButton . $notificationButton . $accessPointsButton . $updateButton . $printouts .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                    $resetEntryListBrowserUrl,
                    [
                        'id'=>'reset-dynagrid-cm-cross-list-grid',
                        'class' => 'btn btn-default', 
                        'title'=>\Yii::t('app','Reset cross list browser'), 
                        'data-pjax' => true
                    ]).'{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
            [
                'content' => $uploadButton . $downloadButton,
            ]
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// Delete cross list modal
Modal::begin([
    'id' => 'delete-crosslist-modal',
    'header' => '<h4 id="delete-crosslist-modal-header"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Cancel'), '#', [
            'id' => 'delete-crosslist-cancel-btn',
            'data-dismiss' => 'modal'
        ]).'&nbsp;&nbsp'.
        Html::a(\Yii::t('app','Confirm'), '#', [
            'class' => 'btn btn-primary waves-effect waves-light',
            'id' => 'delete-crosslist-confirm-btn'
        ]).'&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static'],
    'closeButton' => ['class'=>'hidden'],
]);
echo '<div class="clearfix"></div><div class="cm-modal-loading"></div>';
echo '<div id="delete-crosslist-modal-body"></div>';
Modal::end();
?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<?php

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = 'dynagrid-cm-cross-list-grid',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS
// Define constants
const selectionStorageId = '$sessionName';
const printoutsStorageId = '$cmPrintoutsSession';
const loadingIndicatorHtml = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>'
// Allowed statuses for Trait Data Collection and Printouts
const allowedEndStatus = ['cross list specified', 'finalized', 'completed'];

var currentSelectedCrossListId = '';

resize();

$(document).ready(function () {
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();
    reloadGrid();
    notifDropdown();
    renderNotifications();
    setTimeout(function() {
        setInterval(updateNotifications, 7000);
    }, 7000);
})

// Perform after pjax success of browser
$(document).on('ready pjax:success', '#$browserId-pjax', function(e) {
    e.stopPropagation();
    e.preventDefault();
    
    notifDropdown();
    $('#notif-btn-id').tooltip();
    $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>');
    renderNotifications();
    setTimeout(function() {
        setInterval(updateNotifications, 7000);
    }, 7000);
    
    resize();
});

// Perform after pjax completion of browser
$(document).on('ready pjax:complete', '#$browserId-pjax', function(e) {
    e.stopPropagation();
    e.preventDefault();
    resize();
    
    let selectedCount = getSessionUserSelection();
    setSelectedCounter(selectedCount, 'selectedCount', 'selectedText');
    
    initDropdown();
});

// Create cross list action
$(document).on('click', '#create-cross-list-btn', function(){
    window.location = '$urlBasicInfo';
});

// Update cross list action
$(document).on('click', '.update-crosslist', function(){
    window.location = '$urlBasicInfo' + '&id=' + $(this).data("id") + '&status=' + $(this).data("status");
});

// View cross list action
$(document).on('click', '.view-crosslist', function(){
    window.location = '$urlViewCrossList' + '&id=' + $(this).data("id");
});

// Update cross list protocols
$(document).on('click', '#update-cross-list-protocols-btn', function() {
    let notif = '';
    let status = 'invalid';
    
    // Retrieve current selection
    let currentSelection = sessionStorage.getItem(selectionStorageId);
    if (currentSelection == null) {
        currentSelection = [];
    } else {
        currentSelection = JSON.parse(currentSelection);
    }

    let count = currentSelection.length;
    let id = 0;
    if (count) {
        let crosslistData = currentSelection[0].split('-');
        id = crosslistData[0];
        status = crosslistData[1];
    }

    if (count === 0 || count > 1) {
        notif = "<i class='material-icons orange-text left'>warning</i> Please select only one (1) cross list.";
    } 
    else if (!allowedEndStatus.includes(status)) {
        notif = "<i class='material-icons red-text left'>close</i> The selected cross list is not yet ready for updating protocols.";
    } 
    else {
        window.location.href = '$protocolsUrl' + '&id=' + id;
    }

    $('.toast').css('display','none');
    Materialize.toast(notif,5000);
});

// Download cross trait DC file action
$(document).on('click', '#trait-data-collection-option', function(){
    let notif = '';
    let status = 'invalid';

    //retrieve current selection
    let currentSelection = sessionStorage.getItem(selectionStorageId);
    if (currentSelection == null) {
        currentSelection = [];
    } else {
        currentSelection = JSON.parse(currentSelection);
    }

    let count = currentSelection.length;
    let id = 0;
    if(count){
        let crosslistData = currentSelection[0].split('-');
        id = crosslistData[0];
        status = crosslistData[1];
    }

    if(count === 0 || count > 1){
        notif = "<i class='material-icons orange-text left'>warning</i> Please select only one (1) cross list.";
    }else if(!allowedEndStatus.includes(status)) {
        notif = "<i class='material-icons red-text left'>close</i> The selected cross list is not yet ready for data collection.";
    }else{
        window.location.href = window.location.origin +
        '/index.php/dataCollection/export-workbook/index?program=' + '$program' +
        '&crossListId=' + id + 
        '&source=cm-main';
    }

    $('.toast').css('display','none');
    Materialize.toast(notif,5000);

});

$(document).on('click', '.data-collection-other-cross-files-btn', function() {
    const obj = $(this)
    const entryListDbId = obj.data('entry-list-id')
    const dataCollectionCrossFileName = obj.data('file-name')
    const crossCount = obj.data('cross-count')
    
    // temporary threshold
    if (crossCount > 500) {
        message = `Kindly wait for the <b>\${dataCollectionCrossFileName}</b> file to be generated.<br>You will be notified in this page once done.`
    
        $('.toast').css('display','none')
        let notif = `<i class='material-icons blue-text left'>info</i><span>\${message}</span>`
        Materialize.toast(notif, 5000)

        $.ajax({
            url: "$exportOtherDataCollectionFilesUrl",
            data: {
                entryListDbId,
                dataCollectionCrossFileName,
                crossCount
            },
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.hasOwnProperty('success') && !response.success) {
                    let message = `Error: \${response.error.message}`

                    $('.toast').css('display','none')
                    let notif = `<i class='material-icons red-text left'>close</i><span>\${message}</span>`
                    Materialize.toast(notif, 5000)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.toast').css('display','none')
                let notif = `<i class='material-icons red-text left'>close</i><span>An error has been encountered: <b>\${textStatus}</b> - \${errorThrown}</span>`
                Materialize.toast(notif, 5000)
            }
        })
    } else {
        $('#system-loading-indicator').html(loadingIndicatorHtml)

        message = `Kindly wait for the <b>\${dataCollectionCrossFileName}</b> file to be generated.<br>The file will be automatically downloaded once done.`

        $('.toast').css('display','none')
        let notif = `<i class='material-icons blue-text left'>info</i><span>\${message}</span>`
        Materialize.toast(notif, 5000)

        let exportUrl =
            window.location.origin +
            '/index.php/crossManager/default/export-data-collection-other-cross-files' +
            '?entryListDbId=' + entryListDbId +
            '&dataCollectionCrossFileName=' + encodeURIComponent(dataCollectionCrossFileName) +
            '&crossCount=' + crossCount
        
        window.location.assign(exportUrl)

        // Hide loading indicator after a moment
        setTimeout(function() {
            $('#system-loading-indicator').css('display','none');
        }, 5000);
    }
})

// Access Harvest Manager (Harvest Crosses)
$(document).on('click', '.harvest-crosses-btn', function(){
    let notif = '';
    let status = 'invalid';
    let currentSelection = getUserSelection();
    let count = currentSelection.length;
    let id = 0;
    let idList = [];
    if(count){
        let crosslistData = currentSelection[0].split('-');
        id = crosslistData[0];
        status = crosslistData[1];
    }

    for (let index = 0; index < count; index++) {
        let cData = currentSelection[index].split('-');
        idList.push(cData[0]);
    }

    if(count === 0 || count > 1){
        notif = "<i class='material-icons orange-text left'>warning</i> Please select only one (1) cross list.";
    }else if(status !== 'finalized') {
        notif = "<i class='material-icons red-text left'>close</i> The selected cross list should be finalized before harvesting crosses.";
    }else{
        $.ajax({
            url: '$getOccurrencesUrl',
            type: 'post',
            data: {
                crossListId: idList,
                requiredStatus: 'planted%'
            },
            success: function(response) {
                occurrenceOptions = JSON.parse(response);
                occurrenceIdList = Object.keys(occurrenceOptions);
                occurrenceCount = occurrenceIdList.length;

                if(occurrenceCount > 1){
                    for (var index in occurrenceOptions) {
                        var newOption = new Option(occurrenceOptions[index], index, false, false);
                        $('#harvest-selected-occurrence').append(newOption).trigger('change');
                    }
                    $('#harvest-crosses-modal').modal('show');
                }else if(occurrenceCount == 1){
                    window.location.href = window.location.origin +
                    '/index.php/harvestManager/harvest-data/crosses?program=' + '$program' +
                    '&occurrenceId=' + occurrenceIdList[0]
                }else{
                    notif = "<i class='material-icons red-text left'>close</i> The selected cross list is not yet ready for harvesting crosses.";
                    $('.toast').css('display','none');
                    Materialize.toast(notif,5000);
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    }

    $('.toast').css('display','none');
    Materialize.toast(notif,5000);

});

// Redirect users to HM after selecting an occurrence
$(document).on('click', '#harvest-crosses-modal-confirm-button', function(){
    window.location.href = window.location.origin +
        '/index.php/harvestManager/harvest-data/crosses?program=' + '$program' +
        '&occurrenceId=' + $('#harvest-selected-occurrence').val();
});

// Enable/Disable 'Go to Harvest Manager' button
$(document).on('change', '#harvest-selected-occurrence', function(){
    $('#harvest-crosses-modal-confirm-button').toggleClass('disabled', $('#harvest-selected-occurrence').val() == '');
});

// Click row in cross list browser
$(document).on('click', '#$browserId tbody tr', function(e) {
    let this_row = $(this).find('input:checkbox')[0];
    let unset = false;
    let entryListDbId = this_row.id;
    let experimentDbId = $(this_row).attr('data-experiment-db-id');
    let id = entryListDbId + '-' + $(this_row).attr('data-status');

    if (this_row.checked) {
        unset = true;
        this_row.checked = false;
        $(this_row.id).prop('checked', false);
        saveSelectionSessionStorage('deselect', id)
    } else {
        this_row.checked = true;
        $(this_row.id).prop('checked');
        saveSelectionSessionStorage('select', id)
    }
    
    setSelectedCounter(getUserSelection().length, 'selectedCount', 'selectedText');
});

// Delete a cross list per row 
$(document).on('click', '.delete-crosslist', function() {
    $('.toast').css('display','none');
    let notif = '';
    
    currentSelectedCrossListId = $(this).data('id');
    let crossListName = $(this).data('crosslist-name');
    let crossListStatus = $(this).data('crosslist-status');
    
    if (crossListStatus === 'finalized') {
        notif = '<i class="material-icons red-text left">close</i> Cross list cannot be deleted since it is finalized.';
    } else {
        $.ajax({
            url: '$canDeleteListUrl',
            type: 'post',
            data: {
                entryListDbId: currentSelectedCrossListId
            },
            async: false,
            success: function(response) {
                if (response === 'true') {
                    showDeleteModal(crossListName);
                } else {
                    notif = '<i class="material-icons red-text left">close</i> Cross list cannot be deleted since ' + 
                        'it has already been harvested or trait data collected.';
                }
            },
            error: function(e) {
                $('#delete-crosslist-modal').modal('hide');
                notif = '<i class="material-icons red-text left">close</i> An error occurred while deleting the cross list. Please try again or file a report for assistance.';
            }
        });
    }
    Materialize.toast(notif, 5000);
})

// Confirm deletion of cross list from modal
$(document).on('click', '#delete-crosslist-confirm-btn', function(e) {
    $('.toast').css('display','none');
    let notif = '';
    
    showDeleteProgressModal();
    
    $.ajax({
        url: '$deleteListUrl',
        type: 'post',
        dataType: 'json',
        data: {
            entryListDbId: currentSelectedCrossListId
        },
        async: true,
        success: function(response) {
            $('#delete-crosslist-modal').modal('hide');
            window.location = '$resetEntryListBrowserUrl';
        },
        error: function(e) {
            console.log(e);
            $('#delete-crosslist-modal').modal('hide');
            notif = '<i class="material-icons red-text left">close</i> An error occurred while deleting the cross list. Please try again or file a report for assistance.';
            Materialize.toast(notif, 5000);
        }
    })
})

// Reset grid including selections
$(document).on('click', '#reset-dynagrid-cm-cross-list-grid', function(e) {
    resetSelection();
})

/**
 * Resize the grid table upon loading
 */
function resize() {
    var maxHeight = ($(window).height() - 260);
    $(".kv-grid-wrapper").css("height", maxHeight);
    $('.dropdown-trigger').dropdown();

    if($('#selectedCount').text() == 'No') {
        sessionStorage.removeItem(selectionStorageId);
        sessionStorage.removeItem(printoutsStorageId);
    }
}

/**
* Reloads data browser
*/
function reloadGrid() {
    $.pjax.reload({
        container: '#' + '$browserId' + '-pjax',
        replace: false
    });
}

/**
* Resets selected cross lists
*/
function resetSelection() {
    sessionStorage.removeItem(selectionStorageId);
    sessionStorage.removeItem(printoutsStorageId);
    setTimeout(function() {
        $.ajax({
            url: '$storeSessionItemsUrl',
            type: 'post',
            data: {
                sessionName: 'entry_list_ids',
                unset: true,
                all: true
            },
            async: false,
            success: function(response) {
                reloadGrid();
            },
            error: function(e) {
                console.log(e)
            }
        });
    }, 300);
}

/**
* Sets selected cross list counter
*/
function setSelectedCounter(items, countId, textId) {
    let count = parseInt(items).toLocaleString();
    let text = ' selected items';
    if (count === '1') {
        text = ' selected item';
    } else if (count === '0') {
        count = 'No';
    }
    $('#' + countId).html(count);
    $('#' + textId).html(text);
}

/**
* Retrieves user's session selection count from PHP session
*/
function getSessionUserSelection() {
    let userSelection;
    
    $.ajax({
        type: 'POST',
        url: '$getSelectedItemsCountUrl',
        data: { 
            sessionName: 'entry_list_ids'
        },
        async: false,
        success: function(response) {
            userSelection = response;
        },
        error: function(e) {
            console.log(e);
        }
    });
    return parseInt(userSelection);
}

/**
* Retrieves current selection from storage
*/
function getUserSelection() {
    let currentSelection = sessionStorage.getItem(selectionStorageId);
    if (currentSelection == null) {
        currentSelection = [];
    } else {
        currentSelection = JSON.parse(currentSelection);
    }
    return currentSelection;
}

/**
 * Add/Clear the checkbox selection from the session storage
 * @param {string} mode the mode of saving to be performed (select, deselect, selectAll, deselectAll)
 * @param {string} value the value of the selection to be saved
 */
function saveSelectionSessionStorage(mode, value = null) {
    let currentSelection = getUserSelection();

    // For select mode, add one item to the storage
    if (mode == 'select') {
        if ($.inArray(value, currentSelection) === -1) {
            currentSelection.push(value);
        }
    }
    // For deselect mode, remove one item from the storage
    else if (mode == 'deselect') {
        var index = currentSelection.indexOf(value);
        if (index !== -1) {
            currentSelection.splice(index,1);
        }
    }

    let idList = [];
    for (let index = 0; index < currentSelection.length; index++) {
        let cData = currentSelection[index].split('-');
        if(allowedEndStatus.includes(cData[1])) idList.push(cData[0]);
    }

    // Save the selection to the storage
    sessionStorage.setItem(selectionStorageId, JSON.stringify(currentSelection));
    sessionStorage.setItem(printoutsStorageId, JSON.stringify(idList));
}

/**
* Initializes dropdown more action of a cross list
*/
function initDropdown() {
    $('.dropdown-trigger').dropdown();
    $('.more-crosslist').on('click', function() {
        let dropdownActionId = $(this).data("id");
        $('#more-crosslist-dropdown-actions-'+dropdownActionId).css({"position": "", "top":"", "left":""});
    });
}

/**
* Displays delete confirmation modal
*/
function showDeleteModal(crossListName) {
    let header = '<h4><i class="material-icons" style="vertical-align: bottom;">delete</i> Delete cross list</h4>';
    let message = '<i class="material-icons orange-text left">warning</i> You are about to delete <strong>'+crossListName+'</strong>.&nbsp;' + 
        'This action will delete all records within the cross list. Click confirm to proceed.</br></br>' +
        '<strong>NOTE: </strong> Only crosses and cross parents will be deleted from cross lists ' +
        'created by Experiment Creation.';
    
    $('#delete-crosslist-modal-header').html(header);
    $('#delete-crosslist-modal-body').html(message);
    $('#delete-crosslist-confirm-btn').removeClass('disabled');
    $('#delete-crosslist-cancel-btn').show();
    $('#delete-crosslist-modal').modal('show');
}

/**
* Displays deletion in progress modal
*/
function showDeleteProgressModal() {
    $('#delete-crosslist-modal-header').html('Deleting cross list...')
    $('#delete-crosslist-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    $('#delete-crosslist-confirm-btn').addClass('disabled');
    $('#delete-crosslist-cancel-btn').hide();
    $('#delete-crosslist-modal').modal('show');
}

/**
* Displays notifications in dropdown UI
*/
function notifDropdown() {
    $('#notif-btn-id').on('click', function(e) {
        $.ajax({
            url: '$notificationsUrl',
            type: 'get',
            async: false,
            success: function(data) {
                renderNotifications();
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
                $('#notifications-dropdown').html(data);

                $('.bg_notif-btn').on('click', function(e) {
                    e.preventDefault()

                    let obj = $(this)
                    let id = obj.data('transaction_id')
                    let abbrev = obj.data('abbrev')
                    let workerName = obj.data('worker-name')
                    let entity = obj.data('entity')
                    let description = obj.data('description')
                    let filename = obj.data('filename')

                    if (workerName == 'buildcsvdata') {
                        // Create temporary entry point to downloading CSV data
                        const a = document.createElement('a')
                        const dataFormat = workerName.includes('csv') ? ( filename.includes('Multi') ? 'zip' : 'csv' ) : 'json'

                        a.setAttribute('hidden', '')
                        a.setAttribute('href', `/index.php/crossManager/default/download-data?filename=\${filename}&dataFormat=\${dataFormat}`)

                        // Temporarily insert element to document
                        document.body.appendChild(a)

                        // Trigger download
                        a.click()

                        // Remove element
                        document.body.removeChild(a)

                        message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
                        $('.toast').css('display','none')
                        Materialize.toast(message, 5000)

                        $('#system-loading-indicator').html('')
                    } else {
                        window.location = '$resetEntryListBrowserUrl';
                    }
                })
            },
            error: function(xhr, status, error) {
                // If response text is empty, don't display error notif
                if (!xhr.responseText) return

                var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');
                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();
            }  
        });
    })
}

/**
* Initializes dropdown materialize for notifications
*/
function renderNotifications() {
    $('.cm-notification-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: false,
        gutter: 0,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });
}

/**
* Shows new notifications
*/
function updateNotifications() {
    $.getJSON('$newNotificationsUrl', 
        function(json) {
            $('#notif-badge-id').html(json);
            if (parseInt(json) > 0) {
                $('#notif-badge-id').addClass('notification-badge red accent-2');
            } else {
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
            }
        }
    );
}

JS;

$this->registerJs($script);
?>

<?php
$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');
?>

<style>
    .pull-right {
        margin-right: 3px;
    }
</style>
