<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Display notifications in browser
 */

echo '
    <li>
        <h6>' . \Yii::t('app', 'NOTIFICATIONS') . '&emsp;&emsp;
            <span class="round red accent-2 white-text pull-right "><b class="notif-count">' . $unseenCount . '</b></span>
        </h6>
    </li>
    <li class="divider"></li>
    ' . $unseenNotifications;
if ($seenNotifications !== '') {
    echo '
            <li class="header grey-text">' . \Yii::t('app', 'earlier') . '</li>
            <li class="divider"></li>
            ' . $seenNotifications;
}
?>