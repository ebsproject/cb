<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Occurrence browser
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

// Browser Id
$browserId = 'dynagrid-cm-occurrence-grid';

// define URLs
$urlEntryBrowser = Url::to([
    '/crossManager/add-parents/load-entries',
    'program' => $program,
    'id' => $entryListId,
]);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$urlCheckRequiredFields = Url::to(['/crossManager/add-parents/check-required-fields/', 'program' => $program,'id' => $entryListId]);

$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// render tabs
echo Yii::$app->controller->renderPartial('/tabs', [
    'entryListId' => $entryListId,
    'entryListName' => $entryListName,
    'program' => $program, 
    'active' => 'add-parents',
    'entryListStatus' => $entryListStatus
]);

$columns = [
    [
        'class' => '\kartik\grid\SerialColumn',
        'header' => false,
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'vAlign' => 'top',
        'hAlign' => 'left',
        'width' => '20px'
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'order'=> DynaGrid::ORDER_FIX_LEFT,
        'header' => false,
        'template' => '{view}',
        'buttons' => [
            'view' => function ($url, $model, $key) {
                return Html::a('<i class="material-icons">remove_red_eye</i>',
                    '#',
                    [
                        'class'=>'view-entries',
                        'title' => Yii::t('app','View entries used in this occurrence'),
                        'data-id' => $model['occurrenceDbId'],
                        'data-name' => $model['occurrenceName'],
                    ]
                );
            },
        ],
        'vAlign' => 'top',
        'hAlign' => 'left',
        'width' => '20px'
    ],
    [
        'attribute'=>'experiment',
    ],
    [
        'attribute'=>'experimentCode',
    ],
    [
        'attribute'=>'experimentType',
    ],
    [
        'attribute'=>'occurrenceName',
        'label' => Yii::t('app', 'Occurrence'),
        'headerOptions' => [
            'min-width' => '250px'
        ]
    ],
    [
        'attribute'=>'occurrenceCode',
    ],
    [
        'attribute'=>'entryCount',
        'width' => '50px',
    ],
    [
        'attribute'=>'experimentYear',
        'width' => '50px',
        'filterInputOptions' =>[
            'disabled' => true
        ],
    ],
    [
        'attribute'=>'site',
        'headerOptions' =>[
            'min-width' => '275px',
        ],
        'filterInputOptions' =>[
            'disabled' => true
        ],
    ],
    [
        'attribute'=>'experimentSeasonCode',
        'label' => Yii::t('app', 'Season'),
        'width' => '50px',
        'filterInputOptions' =>[
            'disabled' => true
        ],
    ],
    [
        'attribute'=>'experimentStageCode',
        'label' => Yii::t('app', 'Stage'),
        'width' => '50px',
    ],
];

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $occurrenceModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', "Here is the list of occurrences within the same program, site, season, and experiment year.
                To start, click the view button to see all of the entries that were used in an occurrence.").'{summary}',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' => 
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                  ["add-parents/load?id=$entryListId&program=$program"], 
                  [
                    'id'=>'reset-'.$browserId,
                    'class' => 'btn btn-default', 
                    'title'=>\Yii::t('app','Reset occurrences browser'), 
                    'data-pjax' => true
                  ]).'{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();
?>

<?php

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS

var entryListId = $entryListId;
var selectionStorageId = entryListId + "_dynagrid-cm-entry-grid";

resize();

// Next button validation
disableNextButton();

$(document).on('click', '#next-btn', function(){
    // disable next button
    $("#next-btn").toggleClass('disabled', false);
});

// Perform after pjax success of browser
$(document).on('ready pjax:success','#$browserId-pjax', function(e) {
    resize();
});

//retrieve and update page size
$(document).ready(function() {
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();
});

$(document).on('click', '.view-entries', function(){
    sessionStorage.removeItem(selectionStorageId);
    window.location = '$urlEntryBrowser' + '&occurrenceId=' + $(this).data("id") + 
        '&occurrence=' + $(this).data("name");
});

/**
 * Toggles next button if all the requirements passed
 */
function disableNextButton(){
    //check if all the required fields were set
    $.ajax({
        url: '$urlCheckRequiredFields',
        type: 'post',
        dataType: 'json',
        data: {
            entryListId: $entryListId
        },
        success: function(response){
            disabled = response > 0 || $('.summary').text() == '';
            $("#next-btn").toggleClass('disabled', disabled);
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while checking the parents. Please try again or file a report for assistance.';
            Materialize.toast(notif, 5000);
        }
    });
}

/**
 * Resize the grid table upon loading
 */
function resize(){
    var maxHeight = ($(window).height() - 330);
    $(".kv-grid-wrapper").css("height",maxHeight);
}

JS;

$this->registerJs($script);
