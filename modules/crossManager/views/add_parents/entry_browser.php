<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for displaying entries extracted from planting instructions records
 */


use kartik\dynagrid\DynaGrid;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

// Browser Id
$browserId = 'dynagrid-cm-entry-grid';

// Define URLs
$urlOccurrenceBrowser = Url::to(['/crossManager/add-parents/load/', 'program' => $program,'id' => $entryListId]);
$urlSaveParents = Url::to(['/crossManager/add-parents/save-parents/', 'program' => $program,'id' => $entryListId]);
$urlNextTab = Url::to(['/crossManager/manage-parents/load/', 'program' => $program,'id' => $entryListId]);
$urlGetOccurrenceEntries = Url::to(['/crossManager/add-parents/get-occurrence-entries', 'program'=>$program]);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$urlCheckRequiredFields = Url::to(['/crossManager/add-parents/check-required-fields/', 
    'program' => $program,'id' => $entryListId]);
$urlExportGermplasmAttr = Url::to(['/crossManager/add-parents/export-germplasm-attr/', 
    'program' => $program,'id' => $entryListId]);

$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// Render tabs
echo Yii::$app->controller->renderPartial('/tabs', [
    'entryListId' => $entryListId,
    'entryListName' => $entryListName,
    'program' => $program, 
    'active' => 'add-parents',
    'entryListStatus' => $entryListStatus
]);

$columns = [
    [
        'label' => "Checkbox",
        'order'=>kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => '
            <input type="checkbox" class="filled-in" id="dynagrid-cm-entry-browser-select-all-checkbox" "/>
            <label style="padding-left: 20px;" for="dynagrid-cm-entry-browser-select-all-checkbox"></label>
        ',
        'content' => function ($model) {
            $checkboxId = $model['entryDbId'];
            
            return '
            <input class="dynagrid-cm-entry-browser-single-checkbox filled-in"' .
                'id="' . $checkboxId . '"' .
                'type="checkbox"/>' .
            '<label style="padding-left: 20px;" for="' . $checkboxId . '"></label>';
        },
        'width' => '1%',
    ],
    [
        'attribute'=>'entryNumber',
        'label' => Yii::t('app', 'Entry No.'),
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'entryName',
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'entryCode',
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'designation',
        'label' => Yii::t('app', 'Germplasm Name'),
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'germplasmType',
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'germplasmState',
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'parentage',
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'generation',
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'entryRole',
        'label' => Yii::t('app', 'Parent Role'),
        'vAlign' => 'top'
    ],
];

$backButton = '<a class = "btn btn-default" id = "back-occurrence-btn"><i class="material-icons white-text">keyboard_backspace</i>&nbsp;</a>';
DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $occurrenceEntryModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => $backButton.'&nbsp;&nbsp;&nbsp;'.\Yii::t('app', 'Back to Occurrence list'),
            'before' => '<strong>'.\Yii::t('app', "Occurrence $occurrenceName: ").'</strong>'.
                \Yii::t('app', 'Select entries and add them as female, male, or female-and-male parent.').'{summary}<br/>
                <p id="total-selected-text" class = "pull-right" style = "margin: -1px;">
                </p>', 
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' => 
                    Html::a(\Yii::t('app', 'Add Parents')." <span class='caret'></span>",
                    '#', 
                    [
                        'class' => 'btn dropdown-trigger dropdown-button-pjax',
                        '#',
                        'id' => 'cm-add-parent-btn',
                        'data-position' => 'top',
                        'data-tooltip' => \Yii::t('app', 'Add Parents'),
                        'data-activates' => 'cm-add-parent-dropdown-actions',
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                    ]).
                    "<ul id='cm-add-parent-dropdown-actions' class='dropdown-content' style='width: 160px !important'>".
                        "<li><a href='#' class='add-parent' data-role = 'female'>".\Yii::t('app','As Female')."</a></li>".
                        "<li><a href='#' class='add-parent' data-role = 'male'>".\Yii::t('app','As Male')."</a></li>".
                        "<li><a href='#' class='add-parent' data-role = 'female-and-male'>".\Yii::t('app','As Female-and-Male')."</a></li>".
                    "</ul>",
            ],
            [
                'content' => 
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                    ["add-parents/load-entries?id=$entryListId&program=$program&occurrenceId=$occurrenceId&occurrence=$occurrenceName"], 
                    [
                        'id'=>'reset-'.$browserId,
                        'class' => 'btn btn-default', 
                        'title'=>\Yii::t('app','Reset entry browser'), 
                        'data-pjax' => true
                    ])
                    .'{dynagridFilter}{dynagridSort}{dynagrid}'
                    .Html::a(
                        '<i class="material-icons inline-material-icon">file_download</i>',
                        '',
                        [
                            'data-pjax' => true,
                            'id' => 'export-germplasm-attr-btn',
                            'class' => 'btn btn-default',
                            'style' => 'margin-left: 5px;',
                            'title' => \Yii::t('app', 'Export germplasm attributes in CSV format')
                        ]
                    )
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// Saving parents modal
Modal::begin([
    'id' => 'add-parents-modal',
    'header' => '<h4><i class="material-icons green-text" style="vertical-align:bottom">check_circle</i>' . '&nbsp;' .\Yii::t('app','Parents Added').'</h4>',
    'footer' => Html::a('Add more',
            '#',
            [
                'class' => 'btn btn-default',
                'id' => 'add-parents-btn',
                'data-dismiss'=> 'modal'
            ]) . '&nbsp;'
        .Html::a('Manage Parents',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light',
                'id' => 'manage-parents-btn'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static'],
    'closeButton' => ['class' => 'hidden'],
]);
echo '<div class="clearfix"></div><div class="cm-modal-loading"></div>';
echo '<div id="add-parents-modal-body"></div>';
Modal::end();

// Back to occurrence list modal
Modal::begin([
    'id' => 'back-occurrence-modal',
    'header' => '<h4><i class="material-icons orange-text" style="vertical-align:bottom">warning</i>' . '&nbsp;' .\Yii::t('app','Warning').'</h4>',
    'footer' =>
        Html::a('Back to Occurrence list',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light',
                'id' => 'back-occurrence-confirm-btn'
            ]) . '&nbsp;' .
        Html::a('Return to entry list',
            ['#'],
            [
                'class' => 'btn btn-default',
                'data-dismiss'=> 'modal'
            ]) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);
echo '<div class="clearfix"></div><div class="cm-modal-loading"></div>';
echo '<div id="back-occurrence-modal-body"></div>';
Modal::end();

// Loading modal
Modal::begin([
    'id'=> 'loading-modal',
    'header' => '<h4 id = "loading-header"></h4>',
    'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'close-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;',
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static'],
    'closeButton' => [ 'class'=>'hidden' ],
]);

echo '<div class="loading-div"></div>';
Modal::end();

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS
var entryListId = $entryListId;
var occurrenceId = $occurrenceId;
var gridId = '$browserId';
var selectionStorageId = entryListId + "_" + gridId;

// Restore checkbox selection
checkboxActions(selectionStorageId, gridId);

// Next button validation
disableNextButton();

$('.dropdown-trigger').dropdown();

//add grid size to window height
resize();

// Remove heading summary
$(".panel-heading > div.pull-right > div.summary").hide();

// Perform after pjax success of browser
$(document).on('ready pjax:success','#$browserId-pjax', function(e) {
    checkboxActions(selectionStorageId, gridId);
    $('.dropdown-trigger').dropdown();
    resize();
    disableNextButton();
});

// Perform after pjax completion of browser
$(document).on('ready pjax:complete','#$browserId-pjax', function(e) {
    checkboxActions(selectionStorageId, gridId);
    $('.dropdown-trigger').dropdown();
    resize();
    disableNextButton();
});

/**
* Displays loading modal when adding parents
*/
function showLoadModal() {
    $('#loading-header').html('Adding parents...');
    $('.loading-div').html('<div class="progress"><div class="indeterminate"></div></div>');
    $('#close-btn').css('display','none');
    $('#loading-modal').modal('show');
}

$(document).ready(function(){
    const loadingIndicatorHtml = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';

    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();
    
    checkboxActions(selectionStorageId, gridId);
    $('.dropdown-trigger').dropdown();
    resize();

    // Go back to Occurrence list
    $(document).on('click', '#back-occurrence-btn', function(){
        let selectedItemsCountText = $('#selected-items-count').text();
        if (selectedItemsCountText !== 'No') {
            var message = '<p style = "font-size:115%">You have selected ' + '<b>' + selectedItemsCountText + 
            '</b> entries that have not been added to the parent list. Do you wish to add these entries as parents? ' +
            'These selections and any changes to the entries will be lost when you leave this screen.</p>';
            $('#back-occurrence-modal-body').html(message);
            $('#back-occurrence-modal').modal('show');
        } else {
            sessionStorage.removeItem(selectionStorageId);
            window.location = '$urlOccurrenceBrowser';
        }
    });

    // Go to Manage Parents tab
    $(document).on('click', '#manage-parents-btn, #next-btn', function(){
        window.location = '$urlNextTab';
    });

    // Go back to Occurrence list
    $(document).on('click', '#reset-$browserId', function(){
        sessionStorage.removeItem(selectionStorageId);
    });
    
    // Go back to Occurrence list from modal
    $(document).on('click', '#back-occurrence-confirm-btn', function() {
        sessionStorage.removeItem(selectionStorageId);
        window.location = '$urlOccurrenceBrowser';
    });

    // Select all checkboxes across pages
    $(document).on('click', '#dynagrid-cm-entry-browser-select-all-checkbox', function(e){
        var checkboxClass = "dynagrid-cm-entry-browser-single-checkbox";
        var selectAllId = 'dynagrid-cm-entry-browser-select-all-checkbox';
        let notif = '';
        
        // Display loading indicator
        $('#system-loading-indicator').html(loadingIndicatorHtml);
        
        setCursorInteraction(false);
        
        // Get the records to select/deselect
        $.ajax({
            url: '$urlGetOccurrenceEntries',
            type: 'post',
            dataType: 'json',
            data: {
                'occurrenceDbId': occurrenceId,
                'idOnly': true
            },
            async: false,
            success: function(response) {
                // Select all
                if ($('#'+selectAllId).prop("checked") === true) {
                    
                    // Tick checkboxes visible on page
                    $('#'+selectAllId).attr('checked', 'checked');
                    $('.'+checkboxClass).attr('checked', 'checked');
                    $('.'+checkboxClass).prop('checked', true);
                    $("."+checkboxClass+":checkbox").parent("td").parent("tr").addClass("selected");
                    
                    response.forEach((entryDbId) => { saveSelection('select', gridId, JSON.stringify(entryDbId)) });
                } 
                // Deselect all
                else {
                    $('#'+selectAllId).removeAttr('checked');
                    $('.'+checkboxClass).prop('checked', false);
                    $('.'+checkboxClass).removeAttr('checked');
                    $("input:checkbox."+checkboxClass).parent("td").parent("tr").removeClass("selected");
                    
                    response.forEach((entryDbId) => { saveSelection('deselect', gridId, JSON.stringify(entryDbId)) });
                }
            },
            error: function(error) {
                notif = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'There was an error in retrieving the entry records.';
                
                $('.toast').css('display','none');
                Materialize.toast(notif, 5000);
            }
        });
        
        // Remove loading indicator
        $('#system-loading-indicator').html('');
        
        setCursorInteraction(true);
    });

    // Browser checkbox click event
    $(document).on('click', '.dynagrid-cm-entry-browser-single-checkbox', function(e){

        if($(this).prop("checked") === true)  {
            $(this).parent("td").parent("tr").addClass("selected");
            var row = $(this);
            var value = row.prop("id");
            var selAll = tickSelectAll();

            $('#dynagrid-cm-entry-browser-select-all-checkbox').prop("checked", selAll);
            saveSelection('select', gridId, value);
        } else{
            $(this).parent("td").parent("tr").removeClass("selected");
            var row = $(this);
            var value = row.prop("id");
            saveSelection('deselect', gridId, value);
            $('#dynagrid-cm-entry-browser-select-all-checkbox').prop("checked", false);
        }
    });

    // Save parents for selected items
    $(document).on('click', '.add-parent', function(e) {

        var parentRole = $(this).data('role');
        var currentSelection = JSON.parse(sessionStorage.getItem(selectionStorageId));

        if(currentSelection == null || currentSelection.length == 0){// no selected item(s)
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> Please select an item.";
                Materialize.toast(notif,2500);
            }
        }else{
            showLoadModal();
            $.ajax({
                type:"POST",
                dataType: 'json',
                url: '$urlSaveParents',
                data: {
                    entryIds: JSON.stringify(currentSelection),
                    parentRole: parentRole,
                    occurrenceId: occurrenceId,
                    selectionMode: 'selected'
                },
                success: function(response) {
                    // Refresh browser upon adding
                    sessionStorage.removeItem(selectionStorageId);
                    $.pjax.reload({container: '#$browserId-pjax'});

                    $('#loading-modal').modal('hide');
                    var parent = response['count'] == 1 ? ' parent' : ' parents';
                    var message = '<p style = "font-size:115%">You have successfully added ' + response['count'] + parent +' to your cross list. ' +
                        'Would you like to add more parents or go to the next screen to manage parents?</p>';
                    $('#add-parents-modal-body').html(message);
                    $('#add-parents-modal').modal('show');
                    disableNextButton();
                },
                error: function(error){
                    notif = '<i class="material-icons red-text left">close</i> An error occurred while saving. Please try again or file a report for assistance.';
                    Materialize.toast(notif, 5000);
                }
            });
            }
    });

    $('#export-germplasm-attr-btn').on('click',function(){
        $('#system-loading-indicator').css('display','none')
    })

    $('#export-germplasm-attr-btn').off('click').on('click', function (e) {
        e.preventDefault()
        let urlExportGermplasmAttr = '$urlExportGermplasmAttr'
        let exportGermplasmAttrUrl = window.location.origin + urlExportGermplasmAttr + '&occurrenceId=' + occurrenceId + '&occurrenceName=' + '$occurrenceName'

        message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
        $('.toast').css('display','none')
        Materialize.toast(message, 5000)

        window.location.replace(exportGermplasmAttrUrl)

        $('#system-loading-indicator').html('')
    })

});


/**
 * Check if all items in the current page are selected.
 * @return {boolean} whether or not to tick the select all checkbox element
 */
function tickSelectAll() {
    var checkboxClass = "dynagrid-cm-entry-browser-single-checkbox";
    var result = true;
    // Check each checkbox if checked
    $.each($('.'+checkboxClass), function(i, val) {
        result = result && $(this).prop("checked");
    });

    // Return result (true or false)
    return result;
}

/**
 * Clear the checkbox selection from the session storage
 * @param {string} mode the mode of saving to be performed (select, deselect, selectAll, deselectAll)
 * @param {string} gridId unique string identifier of the grid 
 * @param {string} value the value of the selection to be saved
 */
function saveSelection(mode, gridId='', value = null) {
    var selectionStorageId = entryListId + "_" + gridId;
    var checkboxClass = "dynagrid-cm-entry-browser-single-checkbox";
    // Retrieve current selection from storage
    var currentSelection = sessionStorage.getItem(selectionStorageId);
    if (currentSelection == null) {
        currentSelection = [];
    } else {
        currentSelection = JSON.parse(currentSelection);
    }

    // For selectAll mode, add all items in the current page to the storage
    if (mode == 'selectAll') {
        $('#'+gridId).find("input:checkbox."+checkboxClass).each(function() {
            if ($(this).prop("checked") === true) {

                var currentValue = $(this).attr('id');

                if ($.inArray(currentValue, currentSelection) === -1) {
                    currentSelection.push(currentValue);
                }
            }
        });

    }
    // For deselectOnPage mode, remove all items in the current page from the storage
    else if (mode == 'deselectOnPage') {
        $('#'+gridId).find("input:checkbox."+checkboxClass).each(function() {
            if ($(this).prop("checked") === false) {
                var currentValue = $(this).attr('id');
                var index = currentSelection.indexOf(currentValue);
                if (index !== -1) {
                    currentSelection.splice(index,1);
                }
            }
        });
    }
    // For deselectAll, remove all items from storage
    else if (mode == 'deselectAll') {
        currentSelection = [];
    }
    // For select mode, add one item to the storage
    else if (mode == 'select') {
        if ($.inArray(value, currentSelection) === -1) {
            currentSelection.push(value);
        }
    }
    // For deselect mode, remove one item from the storage
    else if (mode == 'deselect') {
        var index = currentSelection.indexOf(value);
        if (index !== -1) {
            currentSelection.splice(index,1);
        }
    }
    // Display total count in the browser
    displayTotalCount(currentSelection);
    // Save the selection to the storage
    sessionStorage.setItem(selectionStorageId, JSON.stringify(currentSelection));
}

/**
 * Displays the total count of selected items in the browser
 * @param {array} currentSelection array containing selected item values
 */
function displayTotalCount(currentSelection) {
    var noneSelected = '<b><span id="selected-items-count">No</span></b> <span id="selected-items-text"> selected items. &nbsp;</span>';

    // If current selection is null, display "No selected items"
    if (currentSelection === null) {
        $('#total-selected-text').html(noneSelected);
        return true;
    }
    // Get total count
    var totalCount = currentSelection.length;
    // If total count is greater than 0, display count
    if (totalCount > 0) {
        let s = totalCount == 1 ? '' : 's';
        $('#total-selected-text').html(
            '<b><span id="selected-items-count"> '+totalCount.toLocaleString()+
            '</span></b> <span id="selected-items-text"> selected item' + s + '. &nbsp;</span>'
        );
    }
    // if total count is less than or equal to 0, display "No selected items"
    else {
        $('#total-selected-text').html(noneSelected);
    }
}

/**
 * Checks and restores the default state of the checkboxes based on the storage values
 * @param {string} selectionStorageId unique string identifier holding the selected items
 * @param {string} gridId unique string identifier of the grid 
 */
function checkboxActions(selectionStorageId, gridId) {
    var selectedRows = sessionStorage.getItem(selectionStorageId);
    selectedRows = JSON.parse(selectedRows);
    // Display total count in the browser
    displayTotalCount(selectedRows);
    // Set variables
    var checkboxClass = "dynagrid-cm-entry-browser-single-checkbox";
    var selAllId = "dynagrid-cm-entry-browser-select-all-checkbox";
    var grid = $('#'+gridId);
    // Check selected items
    grid.find("."+checkboxClass).each(function() {
        var value = $(this).attr("id");
        if (selectedRows !== null) {
            if ($.inArray(value, selectedRows) !== -1) {
                $(this).prop('checked',true);
                $(this).addClass('isClicked');
                $(this).parent("td").parent("tr").addClass("selected");
            }
        }
    });
    // Tick select all if all items in page are selected
    if (grid.find("."+checkboxClass).length != 0) {
        var all = grid.find("."+checkboxClass).length == grid.find("."+checkboxClass + ":checked").length;
        $('#'+selAllId).prop('checked', all).change();
    }
}

/**
 * Toggles next button if all the requirements passed
 */
function disableNextButton(){
    //check if all the required fields were set
    $.ajax({
        url: '$urlCheckRequiredFields',
        type: 'post',
        dataType: 'json',
        data: {
            entryListId: $entryListId
        },
        success: function(response){
            disabled = response > 0 || $('.summary').text() == '';
            $("#next-btn").toggleClass('disabled', disabled);
        },
        error: function(error){
            notif = '<i class="material-icons red-text left">close</i> An error occurred while checking the parents. Please try again or file a report for assistance.';
            Materialize.toast(notif, 5000);
        }
    });
}

/**
 * Resize the grid table upon loading
 */
function resize(){
    var maxHeight = ($(window).height() - 350);
    $(".kv-grid-wrapper").css("height", maxHeight);
}

JS;

$this->registerJs($script);

?>

