<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JqueryAsset;
?>
<?php

//Select variable modal body for bulk update
switch($variable){
    case 'Cross Method':
        echo '<label class="control-label" style="margin-bot 10px;">'.\Yii::t('app',$variable.' '.'<span class="required">*</span>').'</label>';
        echo Select2::widget([
            'name' => 'BulkValue',
            'id' => 'req-val',
            'data' => $data,
            'value' => '',
            'options' => [
                'placeholder' => 'Select a value',
                'tags' => true,
                'class'=>'select2-input-validate',
                'style' => 'overflow-x: visible; overflow-y: visible;width:88%;',   
            ],
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [','],
                'maximumInputLength' => 100
            ],
        ]);
        break;
    case 'Cross Remarks':
        echo '<label class="control-label" style="margin-bot 10px;">'.\Yii::t('app',$variable).'</label>';
        echo '<textarea id="req-val" type="textarea" class="form-control editable-field text-input-validate  blue-border-focus" target-column min-width: 150px></textarea>';
        break;
} 


echo '</div>';

?> 

<style>
</style>

