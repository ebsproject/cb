<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/


namespace app\modules\crossManager\models;

// Import interfaces
use app\interfaces\models\ICrop;
use app\interfaces\models\IEntryList;

/**
* Model class for Basic Info tab
*/
class BasicInfoModel
{

    /**
     * Model constructor
     */
    public function __construct(
        public IEntryList $entryList,
        public ICrop $crop){
    }

    /**
     * Create/update cross list
     * 
     * @param Integer $entryListId cross list identifier
     * @param Array $formData 
     */
    public function saveCrossList($entryListId, $formData){
        // update record if id is set
        if($entryListId > 0){
            return $this->entryList->updateOne($entryListId, $formData);
        }

        $formData['entryListStatus'] = 'draft';
        $formData['entryListType'] = 'cross list';
        $formData['cropDbId'] = $this->crop->searchAll(['programDbId' => 'equals '.$formData['programDbId']])['data'][0]['cropDbId'].'' ?? '';

        $requestBody = ['records' => [$formData]];

        return $this->entryList->create($requestBody);
    }
}