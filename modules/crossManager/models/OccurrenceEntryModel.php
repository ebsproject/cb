<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/


namespace app\modules\crossManager\models;

// Import interfaces
use app\interfaces\models\IEntryList;

// Import data providers
use app\dataproviders\ArrayDataProvider;
use app\models\BaseModel;
use app\models\UserDashboardConfig;

use Yii;

/**
* Model class for Occurrence Entry Browser
*/
class OccurrenceEntryModel extends BaseModel
{
    CONST SESSION_KEY_SEARCH = 'cm_occurrence_entry_search';

    // displayed columns
    public $entryNumber;
    public $entryName;
    public $entryCode;
    public $designation;
    public $germplasmDbId;
    public $germplasmType;
    public $germplasmState;
    public $parentage;
    public $generation;
    public $entryRole;
    // other column
    public $entryDbId;

    /**
     * Model constructor
     */
    public function __construct(
        public IEntryList $entryList,
        public UserDashboardConfig $userDashboardConfig,
    )
    {}

    /**
     * Define fields to be validated and included in search filters
     */
    public function rules()
    {
        return [
            [[
                'entryName',
                'entryCode',
                'designation',
                'germplasmDbId',
                'germplasmType',
                'germplasmState',
                'parentage',
                'generation',
                'entryRole',
            ],'string'],
            [[
                'entryDbId',
                'entryNumber',
            ], 'integer'],
        ];
    }

    /**
     * Retrieves entries data provider and saves search filters into session
     * 
     * @param Integer $occurrenceDbId occurrence identifier
     * @param Array $params request body
     * @param String $filters limit/sort/page options
     */
    public function search($occurrenceDbId, $params, $filters = ''){
        $params = empty($params) ? null : $params;
        $this->load(['OccurrenceEntryModel' => $params]);

        // Save search filters into session
        Yii::$app->session->set(self::SESSION_KEY_SEARCH, [ 'params'=>$params, 'filters'=>$filters ]);
        
        // retrieve entries
        $entries =  Yii::$app->api->getResponse(
            'POST',
            'occurrences/'.$occurrenceDbId.'/entries-search',
            json_encode($params),
            $filters,
            false
            )['body'];
        
        $totalPages = $entries['metadata']['pagination']['totalPages'] ?? 0;
        // set page
        if (str_contains($filters, 'page')) {
            $updatedFilter = '';
            $filterList = explode('&', $filters);
            foreach($filterList as $value){
                if(strpos($value, 'page') !== false){
                    $page = explode('=', $value)[1];
                }else{
                    $updatedFilter = $updatedFilter."$value&";
                }
            }

            // retrieve correct set of records upon page change
            if($page > $totalPages){
                $entries =  Yii::$app->api->getResponse(
                    'POST', 
                    'occurrences/'.$occurrenceDbId.'/entries-search',
                    json_encode($params),
                    $updatedFilter.'page=1',
                    false
                )['body'] ?? [];

                $_GET["dynagrid-cm-entry-browser-page"] = 1;
            }
        }

        return new ArrayDataProvider([
            'allModels' => $entries['result']['data'] ?? [],
            'key' => 'entryDbId',
            'sort' => [
                'attributes' => [
                    'entryDbId',
                    'entryCode',
                    'entryNumber',
                    'entryName',
                    'designation',
                    'germplasmDbId',
                    'germplasmType',
                    'germplasmState',
                    'parentage',
                    'generation',
                    'entryRole',
                ],
                'defaultOrder' => [
                    'entryNumber' => SORT_ASC,
                ],
            ],
            'restified' => true,
            'totalCount' => $entries['metadata']['pagination']['totalCount'] ?? 0,
            'id' => 'dynagrid-cm-entry-browser'
        ]);

    }

    /**
     * Retrieves sort, page and limit filters from user dashboard config
     * 
     * @param Array $params $params[sort,page,limit] info
     * 
     * @return String browser data/sort filters and page number
     */
    public function getFilters($params){
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        $gridName = 'dynagrid-cm-entry-grid_';
        $paramPage = isset($params['dynagrid-cm-entry-browser-page']) ? '&page='.$params['dynagrid-cm-entry-browser-page'] : '';
        $paramSort = '&sort=entryNumber:ASC';

        //sorting
        if (isset($params['sort'], $params['_pjax']) && $params['_pjax'] == '#dynagrid-cm-entry-grid-pjax') {
            if(strpos($params['sort'],'-') !== false){
                $sort = str_replace('-', '', $params['sort']);
                $paramSort = "&sort=$sort:DESC";
            }
            else{
                $paramSort = '&sort='.$params['sort'];
            }
        }

        return $paramLimit.$paramPage.$paramSort;
    }


    /**
     * Retrieves accessible entry records in a specific and existing occurrence
     *
     * @param int $occurrenceDbId occurrence identifier
     * @param array $params optional request body
     * @param string $filters optional limit|sort|page query parameters
     * @param bool $retrieveAll optional flag to fetch all data or not
     */
    public function getOccurrenceEntries($occurrenceDbId, $params=null, $filters='', $retrieveAll=false)
    {
        $method = 'POST';
        $path = "occurrences/$occurrenceDbId/entries-search";
        return Yii::$app->api->getParsedResponse($method, $path, rawData: json_encode($params), filter: $filters,
            retrieveAll: $retrieveAll);
    }
}