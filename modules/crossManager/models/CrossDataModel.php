<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/


namespace app\modules\crossManager\models;

// Import data providers
use app\dataproviders\ArrayDataProvider;
use app\models\CrossData;
use app\models\UserDashboardConfig;

use Yii;

/**
* Model class for Cross List Data Browser
*/
class CrossDataModel extends CrossData
{
    //set public variable for each column api returns
    public $crossDbId;
    public $crossName;
    public $crossMethod;
    public $crossRemarks;
    public $entryListDbId;
    public $crossFemaleParent;
    public $femaleParentSeedSource;
    public $crossMaleParent;
    public $maleParentSeedSource;
    public $femaleParentEntryNumber;
    public $maleParentEntryNumber;
    public $femaleEntryCode;
    public $maleEntryCode;
    public $femaleParentage;
    public $maleParentage;
    public $femaleOccurrenceName;
    public $maleOccurrenceName;
    public $femaleOccurrenceCode;
    public $maleOccurrenceCode;
    public $harvestStatus;
    public $traits = [];

    private $dynamicRules = [];
    private $dynamicFields = [];

    /**
     * Model constructor
     */
    public function __construct(
        public UserDashboardConfig $userDashboardConfig,
        $config=[],
    )
    {
        $params = Yii::$app->request->queryParams;
        $entryListDbId = isset($params['id']) ? $params['id'] : 0;

        extract($config);

        $data= [];
        if($entryListDbId !== 0){
            $url = "entry-lists/$entryListDbId/cross-data-table-search?limit=1";
            $response = Yii::$app->api->getResponse('POST', $url, null); 
            $data = $response['body']['result']['data']; 
        }

        $traits = [];
        if (count($data) > 0) {
            $traits = array_keys($data[0]);

            // This is to include only the Traits in the $traits variable
            // 2nd Parameter of array_diff contains the excluded columns
            $traits = array_diff($traits, [
                'crossDbId', 'crossName', 'crossFemaleParent', 'femaleParentage', 'crossMaleParent', 'maleParentage',
                'crossMethod', 'crossRemarks', 'femaleParentEntryNumber', 'maleParentEntryNumber', 'femaleParentSeedSource',
                'maleParentSeedSource', 'femaleOccurrenceName', 'maleOccurrenceName', 'femaleOccurrenceCode',
                'maleOccurrenceCode', 'femaleEntryCode', 'maleEntryCode', 'harvestStatus', 'isMethodAutofilled',
                'germplasmDbId', 'germplasmDesignation', 'germplasmParentage', 'germplasmGeneration', 'entryDbId', 
                'entryNo', 'entryName', 'entryRole', 'entryListDbId', 'experimentDbId', 'experimentName', 'experimentCode',
                'experimentDataProcessId', 'occurrenceDbId', 'experimentTemplate', 'parentDbId', 'femaleSourceEntryDbId',
                'maleSourceEntryDbId', 'femaleSourceSeedName', 'maleSourceSeedName', 'femaleSourcePlotDbId',
                'maleSourcePlotDbId', 'femaleSourcePlot', 'maleSourcePlot', 'locationDbId', 'locationCode',
                'creationTimestamp', 'creatorDbId', 'creator', 'modificationTimestamp', 'modifierDbId','femaleSourceEntry',
                'maleSourceEntry'
            ]);
            $traits = array_values($traits);
            $this->dynamicFields = array_fill_keys($traits, '');

            $this->traits = $traits;
            $this->dynamicRules = $traits;
        }
        parent::__construct($config);
    }

    /**
     * Define fields to be validated and included in search filters
     */
    public function rules(){
        return[
            [[
                'crossName',
                'crossFemaleParent',
                'femaleParentage',
                'crossMaleParent',
                'maleParentage',
                'crossRemarks',
                'crossMethod',
                'femaleParentSeedSource',
                'maleParentSeedSource',
                'femaleOccurrenceName',
                'maleOccurrenceName',
                'femaleOccurrenceCode',
                'maleOccurrenceCode',
                'femaleEntryCode',
                'maleEntryCode',
                'harvestStatus',
            ],'string'],
            [[
                'femaleParentEntryNumber',
                'maleParentEntryNumber',
                'crossDbId'
            ],'integer'],

            [$this->dynamicRules, 'safe']
        ];
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes are the dynamic fields
     *
     * @param string $name property name
     * @return mixed property value
     * @see getAttribute()
     */
    public function __get($name)
    {

        if (array_key_exists($name, $this->dynamicFields))
            return $this->dynamicFields[$name];
        else
            return parent::__get($name);
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes are the dynamic fields
     *
     * @param string $name property name
     * @param mixed $value property value
     */
    public function __set($name, $values)
    {

        if (array_key_exists($name, $this->dynamicFields)){
            $this->dynamicFields[$name] = implode($values);
        }
        else
            parent::__set($name, $values);        
    }

    /**
     * Retrieve cross list browser data provider
     * 
     * @param int $id entryListDbId
     * @param array $params request body
     * @param string $filters limit/sort/page options
     * @return ArrayDataProvider
     */
    public function search($id, $params, $filters = ''){
        //retrieving data from API call, set retrieveAll to true
        $method = 'POST';
        $path = "entry-lists/$id/cross-data-table-search";
        $params = empty($params) ? '' : $params;
        if (!empty($params)) {
            foreach ($params as $column => $values) {
                if (isset($values)) {
                    $values = trim($values);
                    if (!in_array($column, [
                        'crossDbId', 'crossName', 'crossFemaleParent', 'femaleParentage', 'crossMaleParent',
                        'maleParentage', 'crossMethod', 'crossRemarks', 'femaleParentEntryNumber', 'maleParentEntryNumber',
                        'femaleParentSeedSource', 'maleParentSeedSource', 'femaleOccurrenceName', 'maleOccurrenceName',
                        'femaleOccurrenceCode', 'maleOccurrenceCode', 'femaleEntryCode', 'maleEntryCode', 'harvestStatus'
                    ])) {
                        $params[$column] = ['dataValue'=> $values] ?? '';
                    }
                }
            }
        } 

        $data = Yii::$app->api->getParsedResponse($method, $path, json_encode($params), $filters, false);
        $result = [];

        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['data'])){
          $result = $data['data'];
        }

        //total data retrieved from API call
        $totalCount = $data['totalCount'] ?? 0;
        $totalPages = $data['totalPages'] ?? 0;

        //only load data within params
        $this->load(['CrossDataModel' => $params]);

        //set page
        if(strpos($filters, 'page') !== false){
            $updatedFilter = '';
            $filterList = explode('&', $filters);
            foreach($filterList as $value){
                if(strpos($value, 'page') !== false){
                    $page = explode('=', $value)[1];
                }else{
                    $updatedFilter = $updatedFilter."$value&";
                }
            }
            // retrieve correct set of records upon page change
            if($page > $totalPages){
                $data = Yii::$app->api->getParsedResponse($method, $path, json_encode($params), $updatedFilter.'page=1');
                $result = $data['data'];
                $totalCount = $data['totalCount'] ?? 0;
                $_GET["page"] = 1;
            }
        }
        
        $sortAttributes = ['crossDbId', 'crossName', 'crossFemaleParent', 'femaleParentage', 'crossMaleParent',
        'maleParentage', 'crossMethod', 'crossRemarks', 'femaleParentEntryNumber', 'maleParentEntryNumber',
        'femaleParentSeedSource', 'maleParentSeedSource', 'femaleOccurrenceName', 'maleOccurrenceName',
        'femaleOccurrenceCode', 'maleOccurrenceCode', 'femaleEntryCode', 'maleEntryCode', 'harvestStatus'
        ];
        
        $sortAttributes = array_merge($sortAttributes);

        //build dataProvider
        return new ArrayDataProvider([
            'allModels' => $result,
            'key' => 'crossDbId',
            'sort' => [
                'attributes' => $sortAttributes,
                'defaultOrder' => [
                    'crossDbId' => SORT_ASC,
                ],
            ],
            'restified' => true,
            'totalCount' => $totalCount
        ]);
    }

    /**
     * Retrieves sort, page and limit filters from user dashboard config
     *
     * @param Array $params $params[sort,page,limit] info
     * @param String $gridName optional dynagrid cookie name
     * 
     * @return string browser data/sort filters and page number
     */
    public function getFilters($params, $gridName = 'dynagrid-cm-view-cross-grid_'){
        //set defaults for filters
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        $paramSort = '&sort=crossDbId';

        //retrieve page number through params
        $paramPage = isset($params['page']) ? '&page='.$params['page'] : '';

        //column sorting
        if(isset($params['sort'])){

            $sort = str_replace('-', '', $params['sort']);

            if(property_exists($this, $sort)){
                $desc = strpos($params['sort'],'-') !== false ? ':DESC' : '';
                $paramSort = "&sort=$sort".$desc;
            }
        }

        return $paramLimit.$paramPage.$paramSort;
    }

}