<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/


namespace app\modules\crossManager\models;

// Import interfaces
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IConfig;

// Import data providers
use app\dataproviders\ArrayDataProvider;
use app\models\BaseModel;
use app\models\Entry;
use app\models\Api;
use app\models\UserDashboardConfig;
use Yii;

/**
* Model class for Manage Parent Browser
*/
class ParentModel extends BaseModel
{
    //set public variable for each column api returns
    public $creationTimeStamp;
    public $creatorDbId;
    public $description;
    public $designation;
    public $entryCode;
    public $entryDbId;
    public $entryNumber;
    public $entryListDbId;
    public $experimentDbId;
    public $experimentName;
    public $experimentCode;
    public $generation;
    public $germplasmCode;
    public $germplasmDbId;
    public $germplasmState;
    public $germplasmType;
    public $modificationTimeStamp;
    public $modifierDbId;
    public $occurrenceDbId;
    public $occurrenceName;
    public $occurrenceCode;
    public $orderNumber;
    public $packageLabel;
    public $parentDbId;
    public $parentRole;
    public $parentType;
    public $parentage;
    public $seedName;
    public $femaleCount;
    public $maleCount;

    /**
     * API endpoint for parents
     */
    public static function apiEndPoint () {
        return 'parents';
    }

    /**
     * Model constructor
     */
    public function __construct(
        public Api $api,
        public Entry $entry,
        public IOccurrence $occurrence,
        public IConfig $configModel,
        public UserDashboardConfig $userDashboardConfig,
        )
    { }

    /**
     * Save parents to experiment.parent table
     * 
     * @param Integer $id entry list identifier
     * @param Array $entryIds entry id list
     */
    public function saveParents($entryListId, $occurrenceId, $parentRole, $params, $filters)
    {
        Yii::debug('Saving parents '.json_encode(['entryListDbId'=>$entryListId, 'occurrenceDbId'=>$occurrenceId,
            'parentRole'=>$parentRole, 'params'=>$params, 'filters'=>$filters]), __METHOD__);
        $records['records'] = [];

        $entries = $this->entry->searchAll($params, $filters);

        foreach($entries['data'] as $value){
            $records['records'][] = [
                'entryListDbId' => $entryListId.'',
                'parentRole' => $parentRole,
                'experimentDbId' => $value['experimentDbId'].'',
                'occurrenceDbId' => $occurrenceId.'',
                'entryDbId' => $value['entryDbId'].'',
            ];
        }

        return $this->create($records);

    }

    //validation for search text filters
    public function rules(){
        return[
            [[
                'entryCode',
                'occurrenceName',
                'occurrenceCode',
                'experimentName',
                'experimentCode',
                'seedName',
                'packageLabel',
                'designation',
                'description',
                'parentage',
                'germplasmCode',
                'generation',
                'parentRole',
                'parentType',
                'germplasmState'
            ],'string'],
            [[
                'entryNumber',
                'orderNumber',
                'femaleCount',
                'maleCount',
            ],'integer']
        ];
    }

    /**
     * Retrieves `experiment.parent` records given search params and filters
     *
     * Related: `ParentModel::searchEntries`
     *
     * WARNING: `params.fields` does not function correctly as of 22.04
     *
     * @param int $entryListDbId entry list identifier
     * @param array $params optional request body
     * @param string $filters optional limit|sort|page query parameters
     * @param bool $retrieveAll optional flag to fetch all data or not
     */
    public function searchParents($entryListDbId, $params=null, $filters='', $retrieveAll=false) {
        $method = 'POST';
        $path = "entry-lists/$entryListDbId/parents-search";
        return Yii::$app->api->getParsedResponse($method, $path, rawData: json_encode($params), filter: $filters,
            retrieveAll: $retrieveAll);
    }

    /**
     * Retrieves `experiment.entry` records, with added occurrence data
     *
     * @param int $experimentDbId experiment identifier
     */
    public function searchEntryParentsData($experimentDbId) {
        $params['fields'] = "entry.id AS entryDbId".
            "|experiment.id AS experimentDbId".
            "|entry.entry_code AS entryCode".
            "|entry.seed_id AS seedDbId".
            "|germplasm.designation AS designation".
            "|entry.entry_role AS parentRole".
            "|entry.entry_type AS parentType".
            "|entry.entry_number AS entryNumber".
            "|entry.germplasm_id AS germplasmDbId";
        $params['experimentDbId'] = "equals $experimentDbId";
        $params['retrievePIentryCode'] = true;
        $params['retrievePIseedDbId'] = true;

        $parents = $this->entry->searchAll($params)['data'];

        // Get occurrence info and map to each entry
        $occurrenceData = $this->occurrence->searchAll(params: ['experimentDbId'=>"equals $experimentDbId"],
            filters: '&limit=1', retrieveAll: false)['data'][0];

        foreach ($parents as $idx => $parent) {
            $parents[$idx]['occurrenceDbId'] = $occurrenceData['occurrenceDbId'];
            $parents[$idx]['occurrenceName'] = $occurrenceData['occurrenceName'];
            $parents[$idx]['occurrenceCode'] = $occurrenceData['occurrenceCode'];
        }

        return $parents;
    }

    /**
     * Retrieves cross list browser data provider
     *
     * @param Integer $entryListId entry list identifier
     * @param Array $params request body
     * @param String $filters optional limit/sort/page options
     * @param int $id optional ArrayDataProvider identifier
     * @param int $experimentDbId optional experiment identifier. If specified, data source is retrieved from
     *   `experiment.entry` table, otherwise `experiment.parent`
     *
     * @return ArrayDataProvider 
     */
    public function search($entryListId, $params, $filters='', $id=null, $experimentDbId=null) {
        $data = [];

        // Determine data source
        if (empty($experimentDbId)) {
            $response = $this->searchParents($entryListId, $params, $filters);

            if ($response['status'] == 200) {
                $data = $response['data'];
            }

            // Data provider config
            $key = 'parentDbId';
            $defaultOrder = [
                'orderNumber' => SORT_ASC,
            ];
        } else {
            $response = $this->searchEntries($experimentDbId, $params, $filters);

            if ($response['status'] == 200) {
                $data = $response['data'];

                // Get occurrence data and map to each Entry
                $occurrenceParams['experimentDbId'] = "equals $experimentDbId";
                $occurrenceFilters = '&limit=1';
                $occurrenceResponse = $this->occurrence->searchAll($occurrenceParams, $occurrenceFilters, retrieveAll: false);

                $occurrenceName = isset($occurrenceResponse['data'][0]) ? $occurrenceResponse['data'][0]['occurrenceName'] : '';
                $occurrenceCode = isset($occurrenceResponse['data'][0]) ? $occurrenceResponse['data'][0]['occurrenceCode'] : '';

                foreach ($data as $idx => $parentData) {
                    $data[$idx]['occurrenceName'] = $occurrenceName;
                    $data[$idx]['occurrenceCode'] = $occurrenceCode;
                }

                // Set default params
                $params['occurrenceName'] = $occurrenceName;
                $params['occurrenceCode'] = $occurrenceCode;

            }

            // Data provider config
            $key = 'entryDbId';
            $defaultOrder = [
                'entryNumber' => SORT_ASC,
            ];
        }

        $totalCount = $response['totalCount'] ?? 0;
        $totalPages = $response['totalPages'] ?? 0;
        
        //only load data within params
        $this->load(['ParentModel' => $params]);

        //set page
        if (str_contains($filters, 'page')) {
            $updatedFilter = '';
            $filterList = explode('&', $filters);
            foreach ($filterList as $value) {
                if (str_contains($value, 'page')) {
                    $page = explode('=', $value)[1];
                } else {
                    $updatedFilter = $updatedFilter."$value&";
                }
            }
            // Retrieve correct set of records upon page change
            if ($page > $totalPages) {
                $updatedFilter = $updatedFilter.'page=1';

                if (empty($experimentDbId)) {
                    $response = $this->searchParents($entryListId, $params, $updatedFilter);
                    $data = $response['data'];
                } else {
                    $response = $this->searchEntries($experimentDbId, $params, $updatedFilter);
                    $data = $response['data'];
                    foreach ($data as $idx => $parentData) {
                        $data[$idx]['occurrenceName'] = $occurrenceName;
                        $data[$idx]['occurrenceCode'] = $occurrenceCode;
                    }
                }

                $totalCount = $response['totalCount'] ?? 0;
                $_GET["page"] = 1;
                $_GET["parent-page"] = 1;
            }
        }

        //build dataProvider
        return new ArrayDataProvider([
            'allModels' => $data,
            'key' => $key,
            'sort' => [
                'attributes' => [
                    'entryCode',
                    'occurrenceName',
                    'occurrenceCode',
                    'experimentName',
                    'experimentCode',
                    'seedName',
                    'packageLabel',
                    'designation',
                    'description',
                    'parentage',
                    'germplasmCode',
                    'generation',
                    'parentRole',
                    'parentType',
                    'entryNumber',
                    'orderNumber',
                    'femaleCount',
                    'maleCount',
                    'germplasmState'
                ],
                'defaultOrder' => $defaultOrder,
            ],
            'restified' => true,
            'totalCount' => $totalCount,
            'id' => $id
        ]);
    }

    /**
     * Retrieves `experiment.entry` records given search params and filters
     *
     * Related: `ParentModel::searchParents`
     *
     * @param int $experimentDbId experiment identifier
     * @param $params [optional] request body
     * @param $filters [optional] limit/sort/page options
     */
    public function searchEntries($experimentDbId, $params=null, $filters='', $retrieveAll = false) {
        // Remove occurrenceCode or occurrenceName from the Entry filter and params to prevent request error
        $entryFilters = join('&', array_map(
            fn($f) => str_contains($f, 'occurrenceCode') || str_contains($f, 'occurrenceName') ? '' : $f,
            explode('&', $filters)));

        $entryParams = $params;
        if (array_key_exists('occurrenceCode', $params)) {
            unset($entryParams['occurrenceCode']);
        }
        if (array_key_exists('occurrenceName', $params)) {
            unset($entryParams['occurrenceName']);
        }

        return $this->entry->searchAll($entryParams, $entryFilters, retrieveAll: $retrieveAll);
    }

    /**
     * Retrieves sort, page and limit filters from user dashboard config
     * 
     * @param Array $params $params[sort,page,limit] info
     * @param String $gridName browser name/id
     * @param string $sort optional to override sort filter
     * 
     * @return String browser data/sort filters and page number
     */
    public function getFilters($params, $gridName='dynagrid-cm-parent-grid_', $pageName='page', $sort=null){
        //set defaults for filters
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        $paramSort = $sort ?? '&sort=orderNumber';

        //retrieve page number through params
        $paramPage = isset($params[$pageName]) ? '&page='.$params[$pageName] : '';

        //column sorting
        if(isset($params['sort'])){

            $sort = str_replace('-', '', $params['sort']);

            if(property_exists($this, $sort)){
                $desc = strpos($params['sort'],'-') !== false ? ':DESC' : '';
                $paramSort = "&sort=$sort".$desc;
            }
        }

        return $paramLimit.$paramPage.$paramSort;
    }

    /**
     * Returns metadata of a given "experiment.parent" data.
     *
     * @param array $parentList parents information used for crossing
     */
    public function getMetadata($parentList) {
        $count = array();       // overall parent counter identified by "entryCode" and "occurrenceCode:entryCode"
        $femaleCount = array(); // female parent counter identified by "occurrenceCode:entryCode"
        $maleCount = array();   // male parent counter identified by "occurrenceCode:entryCode"
        $parents = array();     // named index of parentList identified by "entryCode" and "occurrenceCode:entryCode"
        $femaleParents = array();
        $maleParents = array();

        foreach ($parentList as $idx => $parent) {
            $key1 = $parent['entryCode'];
            $key2 = $parent['occurrenceCode'].':'.$parent['entryCode'];

            // Count the number of parent occurrences
            $count[$key1] = !isset($count[$key1]) ? 1 : $count[$key1] + 1;
            $count[$key2] = !isset($count[$key2]) ? 1 : $count[$key2] + 1;

            // Count the number of female parent occurrences
            if ($parent['parentRole'] === 'female' || $parent['parentRole'] === 'female-and-male') {
                $femaleCount[$key1] = !isset($femaleCount[$key1]) ? 1 : $femaleCount[$key1] + 1;
                $femaleCount[$key2] = !isset($femaleCount[$key2]) ? 1 : $femaleCount[$key2] + 1;
            }

            // Count the number of male parent occurrences
            if ($parent['parentRole'] === 'male' || $parent['parentRole'] === 'female-and-male') {
                $maleCount[$key1] = !isset($maleCount[$key1]) ? 1 : $maleCount[$key1] + 1;
                $maleCount[$key2] = !isset($maleCount[$key2]) ? 1 : $maleCount[$key2] + 1;
            }

            // Uniquely identify parents data and remove entryCode collisions to avoid ambiguity
            if ($count[$key1] > 1) {
                unset($parents[$key1]);
            } else {
                $parents[$key1] = $parent;
            }
            $parents[$key2] = $parent;

            // Segregate female and male parents data
            if ($parent['parentRole'] === 'female' || $parent['parentRole'] === 'female-and-male') {
                if ($femaleCount[$key1] > 1) {
                    unset($femaleParents[$key1]);
                } else {
                    $femaleParents[$key1] = $parent;
                }
                $femaleParents[$key2] = $parent;
            }
            if ($parent['parentRole'] === 'male' || $parent['parentRole'] === 'female-and-male') {
                if ($maleCount[$key1] > 1) {
                    unset($maleParents[$key1]);
                } else {
                    $maleParents[$key1] = $parent;
                }
                $maleParents[$key2] = $parent;
            }
        }

        return [
            'count' => $count,
            'femaleCount' => $femaleCount,
            'maleCount' => $maleCount,
            'parents' => $parents,
            'femaleParents' => $femaleParents,
            'maleParents' => $maleParents
        ];
    }

    /**
     * Deletes one or multiple records in experiment.parent table
     *
     * @param int $entryListDbId entry list identifier to delete
     */
    public function deleteParentByEntryListId($entryListDbId) {
        $parents = $this->searchParents($entryListDbId)['data'] ?? [];
        $parentDbIds = array_column($parents,'parentDbId');
        return $this->deleteMany($parentDbIds);
    }

    /**
     * Retrieve variable configuration for parents
     * 
     */
    public function retrieveConfiguration(){
        //set abbrev for platform.config table
        $activityAbbrev = 'INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT_VAL';

        //get parents config based on abbrev
        $requiredConfig = $this->configModel->getConfigByAbbrev($activityAbbrev);
        return isset($requiredConfig['Values']) ? $requiredConfig['Values'] : [];
    }

    /**
     * Retrieve germplasm attribute data
     * @param array $germplasmIdList list of germplasm identifiers
     */
    public function getGermplasmAtrrData($germplasmIdList, $config = []){
        $germplasmAttrData = [];
        $attrList = [];
    
        foreach($germplasmIdList as $germplasmId){
            $germplasmAttrList = $this->api->getApiResults(
                'GET',
                "germplasm/$germplasmId/attributes",
                retrieveAll:true
            )['data'];

            foreach($germplasmAttrList as $value){
                $index = $value['germplasmDbId'];
                $germplasmAttrData[$index]['GERMPLASM_NAME'] = $value['designation'];
                $germplasmAttrData[$index]['GERMPLASM_CODE'] = $value['germplasmCode'];
                if (isset($config) && !empty($config)) {
                    foreach($value['attributes'] as $a) {
                        if (in_array($a['variableAbbrev'], $config)) {
                            $attrValue = $a['attributeDataValue'];
                            $abbrev = $a['variableAbbrev'];
                            $attrList[$abbrev] = '';
                            $germplasmAttrData[$index][$abbrev] = $attrValue;
                        }
                    }
                }
            }
        }
        return [
            'germplasmAttrData' => $germplasmAttrData,
            'attrList' => $attrList,
        ];
    }
}
