<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\crossManager\models;

use app\interfaces\models\IBackgroundJob;
use app\interfaces\models\IEntryList;
use app\models\BaseModel;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "api.background_job" for Cross Manager application.
 */
class CrossBackgroundJob extends BaseModel {
    public function __construct (
        public IBackgroundJob $backgroundJob,
        public IEntryList $entryList,
        $config = []
    )
    { parent::__construct($config);}

    /**
     * Format the background process records into dropdown selections
     *
     * @param Array $backgroundProcesses List of background process records
     * @return String $transactions Formatted HTML elements for transactions
     */
    public function getBackgroundJobNotifications($backgroundJobs){
        $transactions = [];

        foreach ($backgroundJobs as $record) {
            $status = strtolower($record['jobStatus']);
            $message = ucfirst($record['message']);

            if ($status === 'done') {
                $iconClass = 'green';
                $icon = 'check';
            } elseif (in_array($status, ['in_progress', 'in_queue'])) {
                $iconClass = 'grey';
                $icon = 'hourglass_empty';
            } elseif ($status === 'failed') {
                $iconClass = 'red';
                $icon = 'priority_high';
            } else {
                $iconClass = 'grey';
                $icon  = 'near_me';
            }

            $entity = $record['entity'];
            $entityId = $record['entityDbId'];
            $endpointEntity = strtoupper($record['endpointEntity']);
            $entityLower = strtolower($entity);
            $entityUCF = ucfirst($entityLower);
            $entityNoUnderscore = str_replace('_',' ',$entityUCF);
            $entityId = $record['entityDbId'];

            $application = strtoupper($record['application']);
            $attributes = '';

            $timeDiff = $this->backgroundJob->ago($record['modificationTimestamp']);
            
            $modelClass = str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($record['endpointEntity']))));
            $fieldParam = '';
            $displayField = '';
            $filterFieldParam = '';

            // Retrieve entry list names
            if ($entity == 'ENTRY_LIST') {
                $fieldParam = 'entryList.entry_list_name AS entryListName | entryList.id AS entryListDbId';
                $filterFieldParam = 'entryListDbId';
                $displayField = 'entryListName';
            }

            $model = '\\app\\models\\'.$modelClass;
            // Call container->get() to dynamically construct a model instance
            $modelInstance = \Yii::$container->get($model);
            $params = [
                'fields' => $fieldParam,
                $filterFieldParam => "equals $entityId"
            ];

            $description = $record['description'] ?? '';

            $result = $modelInstance->searchAll($params, 'limit=1&isBasic=true', false);
            $entityName = isset($result['data'][0][$displayField]) ? $result['data'][0][$displayField] : '';
            $abbrev = isset($result['data'][0]['abbrev']) ? $result['data'][0]['abbrev'] : '';
            $entity = strtolower($record['entity'] ?? '');
            $workerName = strtolower($record['workerName'] ?? '');
            $jobRemarks = $record['jobRemarks'];

            $item = "<li class='bg_notif-btn' data-transaction_id='$entityId' data-abbrev='$abbrev' data-worker-name='$workerName' data-entity='$entity' data-description='$description' data-filename='$jobRemarks' $attributes>
                <a class='grey-text text-darken-2'>
                    <span class='material-icons icon-bg-circle small $iconClass'>$icon</span>
                    $message
                </a>";
            $timeElement = '<time class="media-meta">';
            $item .= $timeElement.' <b>'.$entityName.'</b></time>'.$timeElement.$timeDiff.'</time></li>';
            $transactions[] = $item;
        }
        return implode(' ', $transactions);
    }

    /**
     * Push notifications from background process
     *
     * @param integer $userId user identifier
     * @return array list of notifications
     */
    public function pushNotifications($userId) {
        // Get new notifications
        $params = [
            'creatorDbId' => 'equals '.$userId,
            'application' => 'CROSS_MANAGER',
            'isSeen' => 'equals false'
        ];

        $filter = 'sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);
        $newNotifications = $result['data'] ?? [];
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->getBackgroundJobNotifications($newNotifications);

        // Get old notifications
        $params = [
            'creatorDbId' => 'equals '.$userId,
            'application' => 'CROSS_MANAGER',
            'isSeen' => 'equals true'
        ];
        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);
        $oldNotifications = $result['data'] ?? [];
        $seenCount = count($oldNotifications);
        $seenNotifications = $this->getBackgroundJobNotifications($oldNotifications);
        // Update unseen notifications to seen
        $params = [
            'jobIsSeen' => 'true'
        ];

        $this->backgroundJob->update($newNotifications, $params);

        return [
            'unseenNotifications' => $unseenNotifications,
            'seenNotifications' => $seenNotifications,
            'unseenCount' => $unseenCount,
            'seenCount' => $seenCount
        ];
    }
}