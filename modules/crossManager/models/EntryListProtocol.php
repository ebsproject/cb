<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\crossManager\models;

use app\interfaces\models\IConfig;
use app\interfaces\models\IEntryList;
use app\interfaces\models\IListMember;
use app\interfaces\models\ILists;
use app\interfaces\models\IVariable;

use app\models\BaseModel;
use app\models\EntryListData;
use app\models\PlatformListAccess;

use Yii;

/**
 * Model class for Entry List Protocol
 */
class EntryListProtocol extends BaseModel
{
    // Protocol variable abbrevs
    const PROTOCOL_TRAIT = 'TRAIT_PROTOCOL_LIST_ID';
    const PROTOCOL_POLLINATION = 'POLLINATION_INSTRUCTIONS';

    public function __construct(
        public EntryListData $entryListDataModel,
        public IConfig $config,
        public IEntryList $entryList,
        public IListMember $listMember,
        public ILists $listsModel,
        public IVariable $variableModel,
        public PlatformListAccess $platformListAccessModel,
    ) { }

    /**
     * Creates a Trait list with permission for storing entry list-level protocols
     *
     * @param int $entryListDbId entry list identifier
     * @return int|null identifier of the created list; null if none created
     */
    public function addList($entryListDbId)
    {
        $listId = null;
        if (is_null($entryListDbId)) return null;

        $result = $this->entryList->searchAll([
            'fields' => "entryList.id AS entryListDbId | " .
                "entryList.entry_list_code AS entryListCode | " .
                "entryList.entry_list_name AS entryListName | " .
                "entryList.program_id AS programDbId",
            'entryListDbId' => "equals $entryListDbId"
        ], 'limit=1&sort=entryListDbId:desc', retrieveAll: false);

        if (!empty($result['data'][0])) {
            $entryList = $result['data'][0];
            $entryListCode = $entryList['entryListCode'];
            $entryListName = $entryList['entryListName'];

            $requestData['records'][] = [
                'abbrev' => "TRAIT_PROTOCOL_$entryListCode",
                'name' => "$entryListName Trait Protocol ($entryListCode)",
                'displayName' => "$entryListName Trait Protocol ($entryListCode)",
                'remarks' => '',
                'type' => 'trait',
                'listUsage' => 'working list',
            ];
            $result = $this->listsModel->create($requestData);
            Yii::debug('Created list ' . json_encode($result), __METHOD__);

            if ($result['success']) {
                $listId = $result['data'][0]['listDbId'];
                $result = $this->platformListAccessModel->addAccess($listId, [], [$entryList['programDbId']], permission: 'read_write');
                Yii::debug('Added list permission ' . json_encode($result), __METHOD__);
            }
        }

        return $listId;
    }

    /**
     * Adds a variable (i.e. list member) to a list
     *
     * @param int $listDbId list identifier
     * @param int $variableDbId variable identifier
     * @return int|null identifier of the added variable; null if API error occurred; -1 if list member already exists
     */
    public function addListMember($listDbId, $variableDbId)
    {
        $listMemberId = null;
        if ($this->listMemberExists($listDbId, $variableDbId)) return -1;

        $variable = $this->variableModel->getOne($variableDbId);
        $requestData[] = [
            'id' => $variableDbId,
            'displayValue' => $variable['data']['label'],
        ];
        $result = $this->listsModel->createMembers($listDbId, $requestData);
        Yii::debug('Created list member ' . json_encode($result), __METHOD__);

        if ($result['success']) {
            $listMemberId = $result['data'][0]['listMemberDbId'];
        }

        return $listMemberId;
    }

    /**
     * Adds multiple variables (i.e. list members) in bulk to a list
     *
     * Does not check if variable already exists in a list.
     *
     * @param int $listDbId list identifier
     * @param string[] $variableAbbrevs list of variable abbrevs
     * @return int[] created list member identifiers
     */
    public function addListMembers($listDbId, $variableAbbrevs)
    {
        $listMemberIds = [];

        if (!empty($variableAbbrevs)) {
            $params = [
                'fields' => 'variable.id | variable.label AS displayValue | variable.abbrev as abbrev',
                'abbrev' => 'equals '.implode('|equals ', $variableAbbrevs),
            ];
            $data = $this->variableModel->searchAll($params, '', true)['data'] ?? [];

            $result = $this->listsModel->createMembers($listDbId, $data);
            Yii::debug('Created list member ' . json_encode($result), __METHOD__);

            if ($result['success']) {
                $listMemberIds = array_column($result['data'], 'listMemberDbId');
            }
        }

        return $listMemberIds;
    }

    /**
     * Updates a variable (i.e. list member) fields
     *
     * @param int $listMemberDbId list member identifier
     * @param string $remarks [optional] remarks field
     * @param int number of records successfully updated
     */
    public function updateListMember($listMemberDbId, $remarks='')
    {
        $result = $this->listMember->updateOne($listMemberDbId, [ 'remarks' => $remarks ]);
        return $result['totalCount'];
    }

    /**
     * Deletes one or more variables (i.e. list member) from a list
     *
     * @param array<int> $listMemberIds list member identifiers
     * @return int[] number of success/failed deletions
     */
    public function deleteListMembers($listMemberIds)
    {
        $count = [
            'success' => 0,
            'failed' => 0,
            'failedNoPermission' => 0
        ];

        if (empty($listMemberIds)) return $count;

        $result = $this->listMember->deleteMany($listMemberIds);
        Yii::debug('Deleted list member ' . json_encode($result), __METHOD__);

        if (!$result['success']) $count['failed'] = count($listMemberIds);
        else {
            $data = $result['data'][0];
            $count['success'] = count($data['successful']);
            $count['failed'] = count($data['failed']);

            // Find failed results without permission.
            // Entry lists uses working_list which is shared to all program by default so
            // theoretically permission error should never happen.
            $count['failedNoPermission'] = array_reduce($data['failed'], function ($a,$fail) {
                return $a + (str_contains($fail['response'][0]['message'], 'does not have read and write access') ? 1 : 0);
            }, 0);
        }

        return $count;
    }

    /**
     * Returns the Trait list identifier associated with an entry list
     *
     * @param int $entryListDbId entry list identifier
     * @return int|null identifier of `platform.list` record; null if none found
     */
    public function getListId($entryListDbId)
    {
        $listId = null;

        $result = $this->entryListDataModel->searchAll([
            'fields' => "entry_list_data.entry_list_id AS entryListDbId | " .
                "variable.abbrev AS variableAbbrev | ".
                "entry_list_data.data_value AS dataValue",
            'entryListDbId' => "equals $entryListDbId",
            'variableAbbrev' => 'equals ' . self::PROTOCOL_TRAIT,
        ], 'limit=1', retrieveAll: false);

        if ($result['success'] && isset($result['data'][0]['dataValue'])) {
            $listId = (int) $result['data'][0]['dataValue'];
        }

        return $listId;
    }

    /**
     * Returns list member identifiers of a list
     *
     * @param int $listDbId list identifier
     * @return array list member identifiers
     */
    public function getListMemberIds($listDbId)
    {
        $result = $this->listsModel->searchAllMembers($listDbId, [ 'fields' => "listMember.id AS listMemberDbId" ]);
        if ($result['success'] && $result['totalCount'] > 0) {
            return array_column($result['data'], 'listMemberDbId');
        }
        return [];
    }

    /**
     * Checks if an entry list contains protocol data
     *
     * @param int $entryListDbId entry list identifier
     * @param string $type trait | pollination
     * @return bool true if it exists, false otherwise
     */
    public function hasProtocol($entryListDbId, $type='trait')
    {
        $variableAbbrev = $type === 'trait' ? self::PROTOCOL_TRAIT : self::PROTOCOL_POLLINATION;
        $result = $this->entryListDataModel->searchAll([
            'fields' => "entry_list_data.entry_list_id AS entryListDbId  | " .
                "variable.abbrev AS variableAbbrev",
            'entryListDbId' => "equals $entryListDbId",
            'variableAbbrev' => "equals $variableAbbrev",
        ]);
        return $result['totalCount'] > 0;
    }

    /**
     * Updates value of a protocol
     *
     * @param int $entryListDbId entry list identifier
     * @param string $dataValue new data value
     * @param string $type trait | pollination
     */
    public function saveProtocolData($entryListDbId, $dataValue, $type='pollination')
    {
        $variableAbbrev = $type === 'pollination' ? self::PROTOCOL_POLLINATION : self::PROTOCOL_TRAIT;
        $result = $this->entryListDataModel->searchAll([
            'fields' => "entry_list_data.id AS entryListDataDbId | " .
                "entry_list_data.entry_list_id AS entryListDbId  | " .
                "variable.abbrev AS variableAbbrev",
            'entryListDbId' => "equals $entryListDbId",
            'variableAbbrev' => "equals $variableAbbrev",
        ], 'limit=1', retrieveAll: false);

       if ($result['totalCount'] === 1) {
            $entryListDataDbId = $result['data'][0]['entryListDataDbId'];
            $requestData['records'][] = [ 'dataValue' => $dataValue ];

            $result = $this->entryListDataModel->updateOne($entryListDataDbId, $requestData);
            Yii::debug('Updated entry list data ' . json_encode($result), __METHOD__);
        }

       return $result;
    }

    /**
     * Checks if a variable (i.e. list member) exists within a list
     *
     * @param int $listDbId list identifier
     * @param int $variableDbId variable identifier
     * @return bool true if it exists, false otherwise
     */
    public function listMemberExists($listDbId, $variableDbId)
    {
        $result = $this->listsModel->searchAllMembers($listDbId, [ 'variableDbId' => "equals $variableDbId"]);
        return $result['totalCount'] > 0;
    }

    /**
     * Returns variables from trait configuration
     *
     * @param string $program program code
     */
    public function getTraitsFromConfig($program)
    {
        $config = $this->config->getConfigByAbbrev("CROSS_TRAIT_PROTOCOL_$program");
        if (empty($config)) {
            $config = $this->config->getConfigByAbbrev('CROSS_TRAIT_PROTOCOL_DEFAULT');
        }
        Yii::debug('Trait configuration ' . json_encode($config), __METHOD__);

        $traits = $config['required_traits'] ?? [];
        if (!empty($traits)) {
            $result = $this->variableModel->searchAll([
                'fields' => 'variable.id | variable.abbrev as abbrev',
                'abbrev' => implode('|', $traits)
            ]);
        }

        return $result['data'] ?? [];
    }

    /**
     * Returns information to create entry list data record
     *
     * Data value varies depending on type of protocol
     * . trait : list identifier
     * . pollination : pollination instruction
     *
     * @param int $entryListDbId entry list identifier
     * @param int|string $dataValue entry list data value
     * @param string $type trait | pollination
     */
    public function getEntryListProtocols($entryListDbId, $dataValue, $type='trait')
    {
        $variableAbbrev = $type === 'trait' ? self::PROTOCOL_TRAIT : self::PROTOCOL_POLLINATION;

        $result = $this->variableModel->searchAll([
            'fields' => 'variable.id | variable.abbrev as abbrev',
            'abbrev' => "equals $variableAbbrev",
        ], 'limit=1', retrieveAll: false);
        $variableDbId = $result['data'][0]['id'];

        return $this->entryListDataModel->toRecord($entryListDbId, $variableDbId, $dataValue);
    }
}