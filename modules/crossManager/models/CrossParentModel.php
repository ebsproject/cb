<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\models;

use app\models\BaseModel;

use Yii;

/**
 * This is the model class for table "germplasm.cross_parent".
 */
class CrossParentModel extends BaseModel
{
    /**
     * API endpoint for crosses
     */
    public static function apiEndPoint() {
        return 'cross-parents';
    }

    /**
     * Model constructor
     */
    public function __construct() {

    }

    /**
     * Converts a list of valid crosses and parents to "records" data for
     * inserting into the cross_parent table
     *
     * @param array $crossList valid cross data with "crossDbId" field
     * @param array $parentData parents information used for crossing (see ParentModel.php::getMetadata)
     * @return array cross parent records
     */
    public function toRecords($crossList, $parentData) {
        $records = [];

        // Find parents data of each cross
        foreach ($crossList as $idx => $cross) {

            // Female parent
            $femaleKey = $cross['femaleOccurrenceCode'] === '' ? $cross['femaleEntryCode'] :
                $cross['femaleOccurrenceCode'].':'.$cross['femaleEntryCode'];
            if (array_key_exists($femaleKey, $parentData['parents'])) {
                $femaleParent = $parentData['parents'][$femaleKey];
            } else {
                $femaleParent = $parentData['femaleParents'][$femaleKey];
            }
            $femaleRecord = [
                'crossDbId' => $cross['crossDbId'].'',
                'orderNumber' => '1',
                'parentRole' => 'female',
                'germplasmDbId' => $femaleParent['germplasmDbId'].'',
                'experimentDbId' => $femaleParent['experimentDbId'].'',
                'entryDbId' => $femaleParent['entryDbId'].'',
                'occurrenceDbId' => $femaleParent['occurrenceDbId'].'',
                'seedDbId' => $femaleParent['seedDbId'].'',
            ];
            if (!empty($femaleParent['plotDbId'])) {
                $femaleRecord['plotDbId'] = $femaleParent['plotDbId'].'';
            }

            // Self-cross
            $designations = explode('|', $cross['crossName']);
            if ($designations[0] === $designations[1]) {
                $femaleRecord['parentRole'] = 'female-and-male';
                $records[] = $femaleRecord;
                continue;
            }

            // Male parent
            $maleKey = $cross['maleOccurrenceCode'] === '' ? $cross['maleEntryCode'] :
                $cross['maleOccurrenceCode'].':'.$cross['maleEntryCode'];
            if (array_key_exists($maleKey, $parentData['parents'])) {
                $maleParent = $parentData['parents'][$maleKey];
            } else {
                $maleParent = $parentData['maleParents'][$maleKey];
            }
            $maleRecord = [
                'crossDbId' => $cross['crossDbId'].'',
                'orderNumber' => '2',
                'parentRole' => 'male',
                'germplasmDbId' => $maleParent['germplasmDbId'].'',
                'experimentDbId' => $maleParent['experimentDbId'].'',
                'entryDbId' => $maleParent['entryDbId'].'',
                'occurrenceDbId' => $maleParent['occurrenceDbId'].'',
                'seedDbId' => $maleParent['seedDbId'].'',
            ];
            if (!empty($maleParent['plotDbId'])) {
                $maleRecord['plotDbId'] = $maleParent['plotDbId'].'';
            }

            // Add to cross parent records
            $records[] = $femaleRecord;
            $records[] = $maleRecord;
        }

        return [
            'records' => $records
        ];
    }

    /**
     * Deletes one or multiple records in germplasm.cross_parent table
     *
     * @param array $crossDbIds cross identifier to determine which parent/s to delete
     */
    public function deleteCrossParentByCrossId($crossDbIds) {
        if (empty($crossDbIds)) {
            return 0;
        }

        $crossParents = $this->searchAll([
            'fields' => 'crossParent.id AS crossParentDbId|'.
                'crossParent.cross_id AS crossDbId',
            'crossDbId' => 'equals '. implode('|equals ', $crossDbIds)
        ])['data'] ?? [];

        $crossParentDbIds = array_column($crossParents,'crossParentDbId');

        return $this->deleteMany($crossParentDbIds);
    }
}