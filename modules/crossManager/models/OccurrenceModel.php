<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/


namespace app\modules\crossManager\models;

use Yii;

// Import interfaces
use app\interfaces\models\IEntryList;
use app\interfaces\models\IOccurrence;

use app\dataproviders\ArrayDataProvider;
use app\models\BaseModel;
use app\models\Config;
use app\models\UserDashboardConfig;

/**
* Model class for Occurrences browser
*/
class OccurrenceModel extends BaseModel
{
    // displayed columns
    public $experiment;
    public $experimentCode;
    public $experimentType;
    public $occurrenceDbId;
    public $occurrenceName;
    public $occurrenceCode;
    public $entryCount;
    public $experimentSeasonDbId;
    public $experimentSeasonCode;
    public $experimentYear;
    public $site;
    public $experimentStageCode;
    // other columns
    public $occurrenceStatus;
    public $programDbId;
    public $siteDbId;

    /**
     * Model constructor
     */
    public function __construct(
        public IEntryList $entryList,
        public IOccurrence $occurrence,
        public Config $configModel,
        public UserDashboardConfig $userDashboardConfig,
    ){}

    /**
     * Define fields to be validated and included in search filters
     */
    public function rules ()
    {
        return [
            [[
                'experimentYear',
                'entryCount',
                'programDbId',
                'occurrenceDbId'
            ], 'integer'],
            [[
                'occurrenceCode',
                'occurrenceName',
                'programCode',
                'site',
                'experimentCode',
                'experiment',
                'experimentType',
                'experimentSeasonCode',
                'experimentSeason',
                'experimentStageCode'
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp',
            ], 'safe'],
        ];
    }

    /**
     * Returns experiment types and corresponding stages from Experiment Creation config.
     */
    public function getExperimentTypeStageMappingConfig() {
        $experimentTypeConfigAbbrevs = [
            'Breeding Trial' => 'BREEDING_TRIAL_BASIC_INFO_ACT_VAL',
            'Cross Parent Nursery' => 'CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT_VAL',
            'Generation Nursery' => 'GENERATION_NURSERY_BASIC_INFO_ACT_VAL',
            'Intentional Crossing Nursery' => 'INTENTIONAL_CROSSING_NURSERY_BASIC_INFO_ACT_VAL',
            'Observation' => 'OBSERVATION_BASIC_INFO_ACT_VAL',
            'Agronomic Trial' => 'AGRONOMIC_TRIAL_BASIC_INFO_ACT_VAL',
        ];
        $experimentStageConfig = [];
        foreach ($experimentTypeConfigAbbrevs as $experimentType => $configAbbrev) {
            $result = $this->configModel->getConfigByAbbrev($configAbbrev);
            if (empty($result) || !isset($result['Values'])) {
                continue;
            }

            $configValues = $result['Values'];
            foreach ($configValues as $configValue) {
                if ($configValue['target_value'] === 'stageCode') {
                    $experimentStageConfig[$experimentType] = $configValue['allowed_values'];
                }
            }
        }
        return $experimentStageConfig;
    }

    /**
     * Returns experiment types and corresponding stages eligible for crossing
     *
     * @param string $program program code
     * @param string $stage optional experiment stage code filter
     * @param string $type optional experiment type filter
     */
    public function getValidCrosses($program, $stage = '', $type = '') {
        $config = $this->configModel->getConfigByAbbrev('CM_CROSS_LIST_ADD_PARENTS');
        $data = $config['Values'][0]['experiment_type_stages'][$program] ??
            $config['Values'][0]['experiment_type_stages']['DEFAULT'];

        // Experiment stages to be used as default if not specified by CM config
        $default = $this->getExperimentTypeStageMappingConfig();

        $conditions = [];

        foreach ($data as $experiment => $cfg) {
            if ($cfg['disabled'] || (!empty($type) && !str_contains(strtolower($experiment), strtolower($type)))) continue;
            if (is_null($cfg['stages'])) {
                $stages = $default[$experiment] ?? [];
            } else {
                $stages = $cfg['stages'];
            }
            
            // check if there's a stage code config:
            if(empty($stages)) return false;

            $filteredStages = [];
            foreach ($stages as $s){

                if (!empty($stage) && str_contains(strtolower($s), strtolower($stage))) {
                    $filteredStages[] = $s;
                }
            }
            if (!empty($stage) && !empty($type) && empty($filteredStages)){
                $conditions[] = [
                    'experimentStageCode' => 'equals none',
                    'experimentType' => $experiment,
                ];
                break;
            }elseif (!empty($filteredStages)) {
                $conditions[] = [
                    'experimentStageCode' => 'equals '.implode('|equals ', $filteredStages),
                    'experimentType' => $experiment,
                ];
                break;
            }else {
                $conditions[] = [
                    'experimentStageCode' => 'equals '.implode('|equals ', $stages),
                    'experimentType' => $experiment,
                ];
            }
        }


        return $conditions;
    }

    /**
     * Returns occurrence browser data provider
     * 
     * @param Array $params request body
     * @param String $filters limit/sort/page options
     * @param Array $entryListInfo entry list record details
     * @param Array $conditions list of configured stage and type conditions
     * 
     * @return ArrayDataProvider
     */
    public function search($params, $filters, $entryListInfo, $conditions = []) {
        $model = 'OccurrenceModel';

        // Override default search params with user-specified
        $queryParams = Yii::$app->request->queryParams;
        if (isset($queryParams[$model]['experimentType']) && !empty($queryParams[$model]['experimentType'])) {
            $params['experimentType'] = $queryParams[$model]['experimentType'];
        }
        if (isset($queryParams[$model]['experimentStageCode']) && !empty($queryParams[$model]['experimentStageCode'])) {
            $params['experimentStageCode'] = $queryParams[$model]['experimentStageCode'];
        }
        // TODO: Add validation that experiment type and stage are allowed for the current crop program

        if(empty($conditions) || $conditions == false){
            $occurrenceList['data'] = [];
            $occurrenceList['totalCount'] = 0;
        }else {
            $occurrenceList = $this->occurrence->searchAll($params, $filters, false);
        }

        // set default filters
        $defaultFilters = [$model => $params];
        $defaultFilters[$model]['experimentType'] = $queryParams[$model]['experimentType'] ?? '';
        $defaultFilters[$model]['experimentStageCode'] = $queryParams[$model]['experimentStageCode'] ?? '';
        $defaultFilters[$model]['experimentYear'] = $entryListInfo['experimentYear'];
        $defaultFilters[$model]['site'] = $entryListInfo['geospatialObjectName'];
        $defaultFilters[$model]['experimentSeasonCode'] = $entryListInfo['seasonCode'];
        
        $this->load($defaultFilters);

        // set page
        if(strpos($filters, 'page') !== false){
            $updatedFilter = '';
            $filterList = explode('&', $filters);
            foreach($filterList as $value){
                if(strpos($value, 'page') !== false){
                    $page = explode('=', $value)[1];
                }else{
                    $updatedFilter = $updatedFilter."$value&";
                }
            }
            // retrieve correct set of records upon page change
            if($page > $occurrenceList['totalPages']){
                $occurrenceList = $this->occurrence->searchAll($params, $updatedFilter.'page=1', false);
                $_GET["dynagrid-cm-occurrence-browser-page"] = 1;
            }
        }

        return new ArrayDataProvider([
            'allModels' => $occurrenceList['data'] ?? [],
            'key' => 'occurrenceDbId',
            'sort' => [
                'defaultOrder' => ['occurrenceDbId'=>SORT_ASC],
                'attributes' => [
                    'occurrenceDbId',
                    'occurrenceCode',
                    'occurrenceName',
                    'programDbId',
                    'siteDbId',
                    'site',
                    'experimentDbId',
                    'experimentCode',
                    'experiment',
                    'experimentType',
                    'experimentStageCode',
                    'experimentYear',
                    'experimentSeasonDbId',
                    'experimentSeasonCode',
                    'experimentStageCode',
                    'entryCount',
                    'occurrenceStatus',
                ] 
            ],
            'restified' => true,
            'totalCount' => $occurrenceList['totalCount'] ?? 0,
            'id' => 'cm-occurrence-browser'
        ]);
    }

    /**
     * Retrieves sort, page and limit filters from user dashboard config
     * 
     * @param Array $params $params[sort,page,limit] info
     * 
     * @return String browser data/sort filters and page number
     */
    public function getFilters($params){
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        $gridName = 'dynagrid-cm-occurrence-grid_';
        $paramPage = isset($params['dynagrid-cm-occurrence-browser-page']) ? '&page='.$params['dynagrid-cm-occurrence-browser-page'] : '';
        $paramSort = '';

        //sorting
        if(isset($params['sort'], $params['_pjax']) && $params['_pjax'] == '#dynagrid-cm-occurrence-grid-pjax'){
            if(strpos($params['sort'],'-') !== false){
                $sort = str_replace('-', '', $params['sort']);
                $paramSort = "&sort=$sort:DESC";
            }
            else{
                $paramSort = '&sort='.$params['sort'];
            }
        }
        return $paramLimit.$paramPage.$paramSort;
    }
}