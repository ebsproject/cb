<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/


namespace app\modules\crossManager\models;

// Import interfaces
use app\interfaces\models\IConfig;
use app\interfaces\models\IScaleValue;
use app\interfaces\models\IVariable;

// Import data providers
use app\dataproviders\ArrayDataProvider;
use app\models\Entry;
use app\models\Cross;
use app\models\CrossParent;
use app\models\UserDashboardConfig;

use Yii;
use yii\base\ErrorException;

/**
 * This is the model class for Add, Manage crosses
 */
class CrossModel extends Cross
{
    // Cross input status
    const INVALID = 'invalid';
    const VALID = 'valid';

    const DUPLICATE_INPUT = 0;
    const REPEATED_FPARENT = 1;
    const REPEATED_MPARENT = 2;
    const REPEATED_PARENTS = 3;
    const IDENTICAL_PARENTS = 4;
    const MISSING_FPARENT = 5;
    const MISSING_MPARENT = 6;
    const MISSING_PARENTS = 7;
    const EXISTING_CROSS = 8;
    const SYNTAX_ERROR = 9;
    const VAGUE_INPUT = 10;

    const REGEX_CROSS = "/(?<femaleOccurrenceCode>[^:|]*?):*(?<femaleEntryCode>[^:|]+)\s*\|\s*(?<maleOccurrenceCode>[^:|]*?):*(?<maleEntryCode>[^:|]+)$/i";
    const REGEX_PARENT = "/(?<occurrenceCode>[^:|]*?)\s*:*\s*(?<entryCode>[^:|]+)$/i";
    const REGEX_CROSS_METHOD = "/^CROSS_METHOD_(?<method>[A-Z_]+)$/i";

    //set public variable for each column api returns
    public $crossDbId;
    public $crossName;
    public $crossMethod;
    public $crossRemarks;
    public $entryListDbId;
    public $crossFemaleParent;
    public $femaleParentSeedSource;
    public $crossMaleParent;
    public $maleParentSeedSource;
    public $femaleParentEntryNumber;
    public $maleParentEntryNumber;
    public $femalePIEntryCode;
    public $malePIEntryCode;
    public $femaleParentage;
    public $maleParentage;
    public $femaleOccurrenceName;
    public $maleOccurrenceName;
    public $femaleOccurrenceCode;
    public $maleOccurrenceCode;
    public $harvestStatus;

    /**
     * Model constructor
     */
    public function __construct(
        public IConfig $configModel,
        public IScaleValue $scaleValue,
        public IVariable $variable,
        public Entry $entry,
        public CrossParent $crossParent,
        public UserDashboardConfig $userDashboardConfig
        )
    { }

    //validation for search text filters
    public function rules(){
        return[
            [[
                'crossName',
                'crossFemaleParent',
                'femaleParentage',
                'crossMaleParent',
                'maleParentage',
                'crossRemarks',
                'crossMethod',
                'femaleParentSeedSource',
                'maleParentSeedSource',
                'femaleOccurrenceName',
                'maleOccurrenceName',
                'femaleOccurrenceCode',
                'maleOccurrenceCode',
                'femalePIEntryCode',
                'malePIEntryCode',
                'harvestStatus',
            ],'string'],
            [[
                'femaleParentEntryNumber',
                'maleParentEntryNumber',
                'crossDbId'
            ],'integer']
        ];
    }

    /**
     * Retrieve cross list browser data provider
     * @param array $params request body
     * @param string $filters limit/sort/page options
     * @return ArrayDataProvider
     */
    public function search($params, $filters = ''){
        //only load data within params
        $this->load(['CrossModel' => $params]);
        
        // If params has crossMethod filtering (not set), set it to null
        foreach($params as $key => $value) {
            // process '(not set)' filter for api call
            if($key == 'crossMethod') {
                if($params[$key] == '(not set)'){
                    $params[$key] = "null";
                }
            }
        }

        //retrieving data from API call, set retrieveAll to false
        $data = $this->searchAll($params, $filters, false);
        $result = [];

        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['data'])){
          $result = $data['data'];
        }

        //total data retrieved from API call
        $totalCount = $data['totalCount'] ?? 0;
        $totalPages = $data['totalPages'] ?? 0;

        //set page
        if(strpos($filters, 'page')){
            $updatedFilter = '';
            $filterList = explode('&', $filters);
            foreach($filterList as $value){
                if(strpos($value, 'page') !== false){
                    $page = explode('=', $value)[1];
                }else{
                    $updatedFilter = $updatedFilter."$value&";
                }
            }
            // retrieve correct set of records upon page change
            if($page > $totalPages){
                $data = $this->searchAll($params, $updatedFilter.'page=1');
                $result = $data['data'];
                $totalCount = $data['totalCount'] ?? 0;

                $_GET["page"] = 1;
                $_GET["parent-page"] = 1;
            }
        }

        //build dataProvider
        return new ArrayDataProvider([
            'allModels' => $result,
            'key' => 'crossDbId',
            'sort' => [
                'attributes' => [
                    'crossDbId',
                    'crossName',
                    'crossFemaleParent',
                    'femaleParentage',
                    'crossMaleParent',
                    'maleParentage',
                    'crossMethod',
                    'crossRemarks',
                    'femaleParentEntryNumber',
                    'maleParentEntryNumber',
                    'femaleParentSeedSource',
                    'maleParentSeedSource',
                    'femaleOccurrenceName',
                    'maleOccurrenceName',
                    'femaleOccurrenceCode',
                    'maleOccurrenceCode',
                    'femalePIEntryCode',
                    'malePIEntryCode',
                    'harvestStatus'
                ],
                'defaultOrder' => [
                    'crossDbId' => SORT_ASC,
                ],
            ],
            'restified' => true,
            'totalCount' => $totalCount
        ]);
    }

    /**
     * Retrieves sort, page and limit filters from user dashboard config
     *
     * @param Array $params $params[sort,page,limit] info
     * @param String $gridName optional dynagrid cookie name
     * @param String $pageName optional for page name
     * 
     * @return string browser data/sort filters and page number
     */
    public function getFilters($params, $gridName = 'dynagrid-cm-cross-grid_', $pageName='page'){
        //set defaults for filters, retrieve from userDashboardConfig
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        $paramSort = '&sort=crossDbId';

        //retrieve page number through params
        $paramPage = isset($params[$pageName]) ? '&page='.$params[$pageName] : '';

        //column sorting
        if(isset($params['sort'])){

            $sort = str_replace('-', '', $params['sort']);

            if(property_exists($this, $sort)){
                $desc = strpos($params['sort'],'-') !== false ? ':DESC' : '';
                $paramSort = "&sort=$sort".$desc;
            }
        }

        return $paramLimit.$paramPage.$paramSort;
    }

    /**
     * Sets default value for manage cross parameters
     * in includeParentSource to true and entryListDbId
     *
     * @param integer $entryListDbId value
     * @param array $params $params[includeParentSource, entryListDbId] info
     *
     * @return array browser data/sort filters and page number
     */
    public function getParams($entryListDbId, $params){
        $params['includeParentSource'] = true;
        $params['entryListDbId'] = "equals $entryListDbId";
        return $params;
    }

    /**
     * Retrieves existing crosses of a cross list
     *
     * @param int $entryListDbId entry list identifier
     */
    public function getExistingCrosses($entryListDbId) {
        $params = [ 'entryListDbId' => "equals $entryListDbId" ];
        $result = $this->searchAll($params);
        return $result['success'] && !empty($result['data']) ? $result['data'] : [];
    }

    /**
     * Returns supported cross methods per crop
     *
     * @param string $cropCode crop code, e.g. MAIZE, RICE, WHEAT
     */
    public function getValidCrossMethod($cropCode='') {
        $config = $this->configModel->getConfigByAbbrev('HM_DATA_BROWSER_CONFIG_' . $cropCode);
        if (empty($config)) {
            $config = $this->configModel->getConfigByAbbrev('HM_DATA_BROWSER_CONFIG_DEFAULT');
        }
        $validAbbrevs = array_keys($config);

        // Get scale values
        $scaleValues = $this->getScaleValues('CROSS_METHOD');
        $validScaleValues = array_values(array_filter($scaleValues, fn($x) => in_array($x['abbrev'], $validAbbrevs)));

        return $this->variable->getVariableScaleTags($validScaleValues);
    }

    /**
     * Retrieves variable scale values
     *
     * @param string $abbrev Variable abbrev
     */
    public function getScaleValues($abbrev)
    {
        $variableDbId = $this->variable->searchAll([
                'abbrev' => "equals $abbrev",
                'fields' => 'variable.id AS variableDbId|variable.abbrev as abbrev'
            ])['data'][0]['variableDbId'] ?? 0;
        return $this->scaleValue->getVariableScaleValues($variableDbId, []);
    }

    /**
     * Retrieves germplasm attributes for export
     * 
     * @return array 
     */
    public function getGermplasmAttributesConfig()
    {
        $germplasmAttributes = [];
        $program = Yii::$app->userprogram->get('abbrev');
        $exportConfig = $this->configModel->getconfigByAbbrev('CM_PROGRAM_'.$program.'_GERMPLASM_ATTRIBUTE_SETTINGS');
        $personModel = Yii::$container->get('app\models\Person');
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);

        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $exportConfig = $this->configModel->getconfigByAbbrev('CM_ROLE_COLLABORATOR_GERMPLASM_ATTRIBUTE_SETTINGS');
        } else if (isset($program) && !empty($program) && !empty($exportConfig)) {
            $exportConfig;
        } else { 
            $exportConfig = $this->configModel->getconfigByAbbrev('CM_GLOBAL_GERMPLASM_ATTRIBUTE_SETTINGS');
        }

        if(isset($exportConfig['Export']) && !empty($exportConfig['Export'])){
            $export = $exportConfig['Export'];
            foreach ($export as $value) {
                $germplasmAttributes = $value;
            }       
        }
        return $germplasmAttributes;
    }

    /**
     * Extracts information from raw cross pairings into a structured form with metadata
     *
     * Raw cross pairings can be in the following format:
     * ```
     *   ENTRY_CODE|ENTRY_CODE
     *   OCCURRENCE_CODE:ENTRY_CODE|OCCURRENCE_CODE:ENTRY_CODE
     * ```
     *
     * @param array $crossList cross pairings to be parsed
     * @return array crosses, entries, occurrence data with entry code and cross count
     */
    public function parseCrossList($crossList) {
        $result = [
            'crosses' => array(),
            'entryCodes' => array(),
            'crossKeys' => array(),
        ];

        foreach ($crossList as $idx => $cross) {
            preg_match(self::REGEX_CROSS, $cross, $matches);
            $matches = array_intersect_key($matches, array_flip([
                'femaleOccurrenceCode', 'femaleEntryCode', 'maleOccurrenceCode', 'maleEntryCode'
            ]));

            // Extract female parent info
            $femaleOccurrenceCode = isset($matches['femaleOccurrenceCode']) ? trim($matches['femaleOccurrenceCode']) : '';
            $femaleEntryCode = isset($matches['femaleEntryCode']) ? trim($matches['femaleEntryCode']) : '';
            $female = $femaleOccurrenceCode === '' ? $femaleEntryCode : "$femaleOccurrenceCode:$femaleEntryCode";

            // Extract male parent info
            $maleOccurrenceCode = isset($matches['maleOccurrenceCode']) ? trim($matches['maleOccurrenceCode']) : '';
            $maleEntryCode = isset($matches['maleEntryCode']) ? trim($matches['maleEntryCode']) : '';
            $male = $maleOccurrenceCode === '' ? $maleEntryCode : "$maleOccurrenceCode:$maleEntryCode";

            $crossKey = "$female,$male";

            $result['crosses'][$idx] = [
                'female' => $female,
                'male' => $male,
                'femaleEntryCode' => $femaleEntryCode,
                'maleEntryCode'=> $maleEntryCode,
                'femaleOccurrenceCode' => $femaleOccurrenceCode,
                'maleOccurrenceCode' => $maleOccurrenceCode,
                'crossInput' => $cross,
                'crossKey' => $crossKey
            ];

            // Count the entry code appearance
            $result['entryCodes'][$femaleEntryCode] = !empty($result['entryCodes'][$femaleEntryCode]) ?
                $result['entryCodes'][$femaleEntryCode] + 1 : 1;
            $result['entryCodes'][$maleEntryCode] = !empty($result['entryCodes'][$maleEntryCode]) ?
                $result['entryCodes'][$maleEntryCode] + 1 : 1;

            $result['crossKeys'][$crossKey] = !empty($result['crossKeys'][$crossKey]) ? $result['crossKeys'][$crossKey] + 1 : 1;
        }
        return $result;
    }

    /**
     * Assigns a field or multiple fields to each cross in a cross list, using values from parents data.
     *
     * @param array $fields crossName | experimentDbId | occurrenceDbId
     * @param array $crossList crosses, entries, occurrence data (see CrossModel::parseCrossList)
     * @param array $parentData parents information used for crossing (see ParentModel.php::getMetadata)
     * @param bool $isSelfing optional flag to set self-cross designation
     *
     * @return array cross list with added fields
     */
    public function addCrossFields($fields, $crossList, $parentData, $isSelfing=false) {
        foreach ($crossList as $idx => $cross) {
            $femaleKey = $cross['femaleOccurrenceCode'] === '' ? $cross['femaleEntryCode'] :
                $cross['femaleOccurrenceCode'].':'.$cross['femaleEntryCode'];
            $maleKey = $cross['maleOccurrenceCode'] === '' ? $cross['maleEntryCode'] :
                $cross['maleOccurrenceCode'].':'.$cross['maleEntryCode'];

            $femaleParent = array_key_exists($femaleKey, $parentData['femaleParents']) ? $parentData['femaleParents'][$femaleKey] : null;
            $maleParent = array_key_exists($maleKey, $parentData['maleParents']) ? $parentData['maleParents'][$maleKey] : null;

            if ($isSelfing && is_null($femaleParent)) {
                $femaleParent = $maleParent;
            }
            if ($isSelfing && is_null($maleParent)) {
                $maleParent = $femaleParent;
            }

            try {
                if (in_array('crossName', $fields)) {
                    $crossList[$idx]['crossName'] = $femaleParent['designation'].'|'.$maleParent['designation'];
                }
                if (in_array('experimentDbId', $fields)) {
                    $crossList[$idx]['experimentDbId'] = $femaleParent['experimentDbId'].'';
                }
                if (in_array('occurrenceDbId', $fields)) {
                    $crossList[$idx]['occurrenceDbId'] = $femaleParent['occurrenceDbId'].'';
                }
            } catch (ErrorException $err) {
                Yii::warning('Missing parent data '.
                    json_encode(['femaleParent'=>$femaleParent, 'maleParent'=>$maleParent]), __METHOD__);
                $crossList[$idx]['crossName'] = '';
                $crossList[$idx]['experimentDbId'] = '';
                $crossList[$idx]['occurrenceDbId'] = '';
            }
        }
        return $crossList;
    }

    /**
     * Adds validation fields to a user-specified cross list
     *
     * See Cross Validation Rules doc or unit tests for valid/invalid values.
     *
     * @param array $crossList crosses, entries, occurrence data (see CrossModel::parseCrossList)
     * @param array $crossKeys female and male cross pairings counter in "$crossList"
     * @param array $parentData parents information used for crossing (see ParentModel.php::getMetadata)
     * @param array $existingCrosses crosses that exist in the database
     * @param bool $isSelfingValid optional flag to tag self crosses as valid or invalid
     *
     * @return array validated cross list
     */
    public function validateCrossList($crossList, $crossKeys, $parentData, $existingCrosses, $isSelfingValid=false) {
        // Validation remarks
        $remarks = [
            self::DUPLICATE_INPUT => \Yii::t('app', 'Duplicate input cross.'),
            self::REPEATED_FPARENT => \Yii::t('app','Repeated female parent found in the Parent List.'),
            self::REPEATED_MPARENT => \Yii::t('app', 'Repeated male parent found in the Parent List.'),
            self::REPEATED_PARENTS => \Yii::t('app','Repeated female and male parents found in the Parent List.'),
            self::IDENTICAL_PARENTS => \Yii::t('app','Same parent info. Please use SELF feature for selfing.'),
            self::MISSING_FPARENT => \Yii::t('app','Female parent does not exist in the Parent List!'),
            self::MISSING_MPARENT => \Yii::t('app','Male parent does not exist in the Parent List!'),
            self::MISSING_PARENTS => \Yii::t('app','Both parents do not exist in the Parent List!'),
            self::EXISTING_CROSS => \Yii::t('app','Already exists in the Cross List!'),
            self::SYNTAX_ERROR => \Yii::t('app','Invalid cross format.'),
            self::VAGUE_INPUT => \Yii::t('app','Duplicate entry codes found in the Parent List. Please specify the occurrence code.')
        ];

        // Generate a list of parent entry IDs for
        //   pair: 'femaleEntryDbId,maleEntryDbId'
        //   self: 'entryDbId'
        try {
            $existingParentEntryDbIds = array_flip(array_map(fn($crossDbData) => $crossDbData['parentDbId'], $existingCrosses));
        } catch (ErrorException) {
            Yii::error('Unable to get existing parent entry IDs, possibly due to missing data '.
                json_encode(['crossDbData'=>$existingCrosses]), __METHOD__);
        }

        foreach ($crossList as $idx => $cross) {
            // Set default values
            $crossList[$idx]['status'] = self::VALID;
            $crossList[$idx]['remarks'] = '';

            $femaleEntryCode = $cross['femaleEntryCode'];
            $maleEntryCode = $cross['maleEntryCode'];
            $femaleOccurrenceCode = $cross['femaleOccurrenceCode'];
            $maleOccurrenceCode = $cross['maleOccurrenceCode'];

            $femaleKey = $femaleOccurrenceCode === '' ? $femaleEntryCode : "$femaleOccurrenceCode:$femaleEntryCode";
            $maleKey = $maleOccurrenceCode === '' ? $maleEntryCode : "$maleOccurrenceCode:$maleEntryCode";

            // Check for syntax errors
            if ($femaleEntryCode === '' || $maleEntryCode === '') {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::SYNTAX_ERROR];
                continue;
            }

            // Check if parents do not exist
            if (!array_key_exists($femaleKey, $parentData['femaleCount']) &&
                !array_key_exists($maleKey, $parentData['maleCount'])) {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::MISSING_PARENTS];
                continue;
            }
            if (!$isSelfingValid && !array_key_exists($femaleKey, $parentData['femaleCount'])) {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::MISSING_FPARENT];
                continue;
            }
            if (!$isSelfingValid && !array_key_exists($maleKey, $parentData['maleCount'])) {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::MISSING_MPARENT];
                continue;
            }

            // Check for vague input
            if ((array_key_exists($femaleEntryCode, $parentData['femaleCount']) &&
                    (int) $parentData['femaleCount'][$femaleEntryCode] > 1 && $femaleOccurrenceCode === '') ||
                (array_key_exists($maleEntryCode, $parentData['maleCount']) &&
                    (int) $parentData['maleCount'][$maleEntryCode] > 1 && $maleOccurrenceCode === '')
            ) {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::VAGUE_INPUT];
                continue;
            }

            // Check for repetitions in the parent source
            if (!$isSelfingValid && (int) $parentData['femaleCount'][$femaleKey] > 1  && (int) $parentData['maleCount'][$maleKey] > 1) {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::REPEATED_PARENTS];
                continue;
            }
            if (!$isSelfingValid && (int) $parentData['femaleCount'][$femaleKey] > 1) {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::REPEATED_FPARENT];
                continue;
            }
            if (!$isSelfingValid && (int) $parentData['maleCount'][$maleKey] > 1) {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::REPEATED_MPARENT];
                continue;
            }

            // Check if cross has identical parents, most likely selfing
            if (!$isSelfingValid && $femaleKey === $maleKey) {
                $crossList[$idx]['status'] = self::INVALID;

                // Final check on vague input if only entryCode is specified
                if ((int) $parentData['count'][$femaleKey] > 1) {
                    $crossList[$idx]['remarks'] = $remarks[self::VAGUE_INPUT];

                    // Handle case when parents are unique in their respective source
                    if ((int) $parentData['femaleCount'][$femaleEntryCode] === 1
                        && (int) $parentData['maleCount'][$maleEntryCode] === 1) {
                        $crossList[$idx]['status'] = self::VALID;
                        $crossList[$idx]['remarks'] = '';
                    }
                } else {
                    $crossList[$idx]['remarks'] = $remarks[self::IDENTICAL_PARENTS];
                    continue;
                }
            }
            if ($isSelfingValid && $femaleKey === $maleKey) {
                $crossList[$idx]['status'] = self::VALID;
                $crossList[$idx]['remarks'] = $remarks[self::IDENTICAL_PARENTS];
            }

            // Check if cross already exists
            if ($isSelfingValid) {
                $parentEntryDbId = array_key_exists($femaleKey, $parentData['parents']) ?
                    $parentData['parents'][$femaleKey]['entryDbId'] :
                    $parentData['parents'][$maleKey]['entryDbId'];
            } else {
                $parentEntryDbId = $parentData['femaleParents'][$femaleKey]['entryDbId'].','.$parentData['maleParents'][$maleKey]['entryDbId'];
            }
            if (array_key_exists($parentEntryDbId, $existingParentEntryDbIds)) {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::EXISTING_CROSS];
                continue;
            }

            // Check if cross has a duplicate within the input cross list
            $crossKey = $cross['crossKey'];
            if (array_key_exists($crossKey, $crossKeys) && $crossKeys[$crossKey] > 1) {
                $crossList[$idx]['status'] = self::INVALID;
                $crossList[$idx]['remarks'] = $remarks[self::DUPLICATE_INPUT];
                if ($idx === array_key_first(array_filter($crossList, fn($c) => $c['crossKey'] === $crossKey))) {
                    $crossList[$idx]['status'] = self::VALID;
                }
            }
        }

        return $crossList;
    }

    /**
     * Converts a list of valid crosses to "records" data for inserting into the cross table
     *
     * @param int $entryListDbId entry list identifier
     * @param array $crosses valid cross data
     * @return array cross data records
     */
    public function toRecords($entryListDbId, $crosses) {
        $records = [];
        foreach ($crosses as $idx => $cross) {
            $records[$idx] = [
                'crossName' => $cross['crossName'],
                'crossMethod' => '',
                'entryListDbId' => "$entryListDbId",
                'experimentDbId' => $cross['experimentDbId'].'',
                'occurrenceDbId' => $cross['occurrenceDbId'].''
            ];

            // Self-cross
            $designations = explode('|', $cross['crossName']);
            if ($designations[0] === $designations[1]) {
                $records[$idx]['crossMethod'] = 'selfing';
                $records[$idx]['isMethodAutofilled'] = true;
            }
        }

        // Reset index
        $records = [...$records];

        return [
            'records' => $records
        ];
    }

    /**
     * Generates a string-value pair from female and male parents
     *
     * @param $female "occurrence_code:entry_code" or "entry_code" of female parent
     * @param $male "occurrence_code:entry_code" or "entry_code" of male parent
     * @return string "female|male" pair, or empty string if female and male are identical
     */
    public function makePair($female, $male) {
        // Extract female parent info
        preg_match(self::REGEX_PARENT, $female, $matches);
        $matches = array_intersect_key($matches, array_flip(['occurrenceCode', 'entryCode']));
        $femaleOccurrenceCode = isset($matches['occurrenceCode']) ? trim($matches['occurrenceCode']) : '';
        $femaleEntryCode = isset($matches['entryCode']) ? trim($matches['entryCode']) : '';

        // Extract male parent info
        preg_match(self::REGEX_PARENT, $male, $matches);
        $matches = array_intersect_key($matches, array_flip(['occurrenceCode', 'entryCode']));
        $maleOccurrenceCode = isset($matches['occurrenceCode']) ? trim($matches['occurrenceCode']) : '';
        $maleEntryCode = isset($matches['entryCode']) ? trim($matches['entryCode']) : '';

        $parent1 = $femaleOccurrenceCode === '' ? $femaleEntryCode : "$femaleOccurrenceCode:$femaleEntryCode";
        $parent2 = $maleOccurrenceCode === '' ? $maleEntryCode : "$maleOccurrenceCode:$maleEntryCode";

        if ($parent1 === $parent2) return '';
        return "$parent1|$parent2";
    }

    /**
     * Simplifies cross pairs from OCC_CODE:ENT_CODE format to ENT_CODE if parents are unique
     *
     * @param array $crossList string of cross pairs
     * @param array $parentsData parents information used for crossing (see ParentModel.php::getMetadata)
     */
    public function simplifyPairs($crossList, $parentsData) {
        foreach ($crossList as $idx => $cross) {
            $female = explode('|', $cross)[0];
            $femaleValues = explode(':', $female);
            // Skip simplification if female is already entry code
            if (count($femaleValues) > 1) {
                $femaleEntryCode = $femaleValues[1];
                if ($parentsData['femaleCount'][$female] == 1 &&
                    $parentsData['femaleCount'][$femaleEntryCode] == 1) {
                    $female = $femaleEntryCode;
                }
            }

            $male = explode('|', $cross)[1];
            $maleValues = explode(':', $male);
            // Skip simplification if male is already entry code
            if (count($maleValues) > 1) {
                $maleEntryCode = $maleValues[1];
                if ($parentsData['maleCount'][$male] == 1 &&
                    $parentsData['maleCount'][$maleEntryCode] == 1) {
                    $male = $maleEntryCode;
                }
            }

            $crossList[$idx] = "$female|$male";
        }
        return $crossList;
    }

    /**
     * Delete records in germplasm.cross_parent and germplasm.cross tables
     * @param Array $crossIdList list of cross ids to be deleted
     */
    public function deleteCrosses($crossIdList){
        //retrieve cross parents
        $crossParents = $this->crossParent->searchAll([
            'fields' => 'crossParent.id AS crossParentDbId|crossParent.cross_id AS crossDbId',
            'crossDbId' => 'equals '. implode('|equals ', $crossIdList)
        ])['data'] ?? [];

        $crossParentsDbIds = array_column($crossParents,'crossParentDbId');           
        
        //delete cross parents
        $this->crossParent->deleteMany($crossParentsDbIds);

        //delete crosses
        return $this->deleteMany($crossIdList);
    }

    /**
     * Deletes one or multiple records in germplasm.cross table
     *
     * @param array $crossDbIds cross identifier to delete
     */
    public function deleteCrossById($crossDbIds) {
        if (empty($crossDbIds)) {
            return 0;
        }
        return $this->deleteMany($crossDbIds);
    }

}
