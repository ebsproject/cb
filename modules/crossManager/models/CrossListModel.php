<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/


namespace app\modules\crossManager\models;

// Import interfaces
use app\interfaces\models\IEntryList;
use app\interfaces\models\IScaleValue;
use app\interfaces\models\IVariable;

// Import data providers
use app\dataproviders\ArrayDataProvider;
use app\models\BaseModel;
use app\models\UserDashboardConfig;
use Yii;

/**
* Model class for Cross List Browser
*/
class CrossListModel extends BaseModel
{
    // Cross list status
    const STATUS_DRAFT = 'draft';
    const STATUS_PARENT_LIST_SPECIFIED = 'parent list specified';
    const STATUS_CROSSES_ADDED = 'crosses added';
    const STATUS_CROSS_LIST_SPECIFIED = 'cross list specified';
    const STATUS_FINALIZED = 'finalized';

    // displayed columns
    public $entryListStatus;
    public $entryListName;
    public $entryListCode;
    public $experimentYear;
    public $experimentType;
    public $seasonCode;
    public $geospatialObjectName;
    public $creator;
    public $creationTimestamp;
    // other columns
    public $entryListDbId;
    public $programDbId;
    public $siteDbId;
    public $seasonDbId;
    public $crossCount;

    /**
     * Model constructor
     */
    public function __construct(
        public IEntryList $entryList,
        public IVariable $variable,
        public IScaleValue $scaleValue,
        public UserDashboardConfig $userDashboardConfig,
    )
    {}

    /**
     * Define fields to be validated and included in search filters
     */
    public function rules()
    {
        return [
            [[
                'entryListStatus',
                'entryListName',
                'entryListCode',
                'seasonCode',
                'geospatialObjectName',
                'creator',
            ],'string'],
            [[
                'entryListDbId',
                'programDbId',
                'siteDbId',
                'seasonDbId',
                'crossCount',
                'experimentYear',
            ], 'integer'],
            [[
                'creationTimestamp',
            ], 'safe'],
        ];
    }

    /**
     * Retrieve cross list browser data provider
     * 
     * @param Array $params request body
     * @param String $filters limit/sort/page options
     * 
     * @return ArrayDataProvider 
     */
    public function search($params, $filters = ''){
        //set default parameters
        $params['entryListType'] = 'equals cross list|equals entry list';
        $params['experimentType'] = 'is null|equals Intentional Crossing Nursery';

        $this->load(['CrossListModel' => $params]);
        
        //set experiment year
        $params['experimentYear'] = $params['experimentYear'] ?? 'is not null';

        // Get entry lists with cross list type
        $crossLists = $this->entryList->searchAll($params, $filters, false);
        // set page
        if(strpos($filters, 'page') !== false){
            $updatedFilter = '';
            $filterList = explode('&', $filters);
            foreach($filterList as $value){
                if(strpos($value, 'page') !== false){
                    $page = explode('=', $value)[1];
                }else{
                    $updatedFilter = $updatedFilter."$value&";
                }
            }
            // retrieve correct set of records upon page change
            if($page > $crossLists['totalPages']){
                $crossLists = $this->entryList->searchAll($params, $updatedFilter.'page=1', false);
                $_GET["cm-crosslist-browser-page"] = 1;
            }
        }

        // Assemble data provider
        return new ArrayDataProvider([
            'allModels' => $crossLists['data'],
            'key' => 'entryListDbId',
            'sort' =>  [
                'attributes' => [
                    'entryListStatus',
                    'entryListName',
                    'entryListCode',
                    'experimentDbId',
                    'experimentYear',
                    'experimentType',
                    'seasonCode',
                    'geospatialObjectName',
                    'creator',
                    'entryListDbId',
                    'programDbId',
                    'siteDbId',
                    'seasonDbId',
                    'crossCount',
                    'creationTimestamp',
                ],
                'defaultOrder' => [
                    'experimentYear' => SORT_DESC,
                    'creationTimestamp' => SORT_DESC
                ]
            ],
            'restified' => true,
            'totalCount' => $crossLists['totalCount'] ?? 0,
            'id' => 'cm-crosslist-browser'
        ]);

    }

    /**
     * Retrieves sort, page and limit filters from user dashboard config
     * 
     * @param Array $params $params[sort,page,limit] info
     * 
     * @return String browser data/sort filters and page number
     */
    public function getFilters($params){
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        $paramPage = isset($params['dynagrid-cm-cross-list-browser-page']) ? '&page='.$params['dynagrid-cm-cross-list-browser-page'] : '';
        $paramSort = '&sort=experimentYear:desc|creationTimestamp:desc';

        //sorting
        if(isset($params['sort'], $params['_pjax']) && $params['_pjax'] == '#dynagrid-cm-cross-list-grid-pjax'){
            if(strpos($params['sort'],'-') !== false){
                $sort = str_replace('-', '', $params['sort']);
                $paramSort = "&sort=$sort:DESC";
            }
            else{
                $paramSort = '&sort='.$params['sort'];
            }
        }

        return $paramLimit.$paramPage.$paramSort;
    }

    /**
     * Retrieves variable scale tags
     * 
     * @param String $abbrev variable abbrev
     * 
     * @return Array variable scale tags
     */
    public function getOptions($abbrev){
        $variableDbId = $this->variable->searchAll([
            'abbrev' => "equals $abbrev",
            'fields' => 'variable.id AS variableDbId|variable.abbrev as abbrev'
        ])['data'][0]['variableDbId'] ?? 0;

        $scaleValues = $this->scaleValue->getVariableScaleValues($variableDbId, []);

        return $this->variable->getVariableScaleTags($scaleValues);
    }

    /**
     * Updates cross count of a cross list via API call
     *
     * @param int $entryListDbId entry list identifier
     */
    public function updateCrossCount($entryListDbId) {
        $method = 'POST';
        $path = "entry-lists/$entryListDbId/cross-count-generations";
        return Yii::$app->api->getParsedResponse($method, $path);
    }

    /**
     * Updates a cross list status
     *
     * @param int $entryListDbId entry list identifier
     * @param string $status new state of the entry list
     */
    public function updateStatus($entryListDbId, $status) {
        $validStates = [
            self::STATUS_DRAFT,
            self::STATUS_PARENT_LIST_SPECIFIED,
            self::STATUS_CROSSES_ADDED,
            self::STATUS_CROSS_LIST_SPECIFIED,
            self::STATUS_FINALIZED
        ];

        if (array_search($status, $validStates)) {
            return $this->entryList->updateOne($entryListDbId, ['entryListStatus' => $status]);
        }
    }

}