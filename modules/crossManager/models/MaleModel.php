<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/


namespace app\modules\crossManager\models;

// Import interfaces
use app\interfaces\models\IOccurrence;

// Import data providers
use app\dataproviders\ArrayDataProvider;
use app\models\BaseModel;
use app\models\Entry;

use Yii;

/**
* Model class for Male Parent Browser
*/
class MaleModel extends BaseModel
{
    //set public variable for each column api returns
    public $creationTimeStamp;
    public $creatorDbId;
    public $description;
    public $designation;
    public $entryCode;
    public $entryDbId;
    public $entryNumber;
    public $entryListDbId;
    public $experimentDbId;
    public $experimentName;
    public $experimentCode;
    public $generation;
    public $germplasmCode;
    public $germplasmDbId;
    public $germplasmState;
    public $germplasmType;
    public $modificationTimeStamp;
    public $modifierDbId;
    public $occurrenceDbId;
    public $occurrenceName;
    public $occurrenceCode;
    public $orderNumber;
    public $packageLabel;
    public $parentDbId;
    public $parentRole;
    public $parentType;
    public $parentage;
    public $seedName;
    public $femaleCount;
    public $maleCount;

    /**
     * Model constructor
     */
    public function __construct(
        public Entry $entry,
        public IOccurrence $occurrence,
        public ParentModel $parentModel,
        )
    { }

    //validation for search text filters
    public function rules(){
        return[
            [[
                'entryCode',
                'occurrenceName',
                'occurrenceCode',
                'experimentName',
                'experimentCode',
                'seedName',
                'packageLabel',
                'designation',
                'description',
                'parentage',
                'germplasmCode',
                'generation',
                'parentRole',
                'parentType',
                'germplasmState'
            ],'string'],
            [[
                'entryNumber',
                'orderNumber',
                'femaleCount',
                'maleCount'
            ],'integer']
        ];
    }

    /**
     * Retrieves cross list browser data provider
     *
     * @param Integer $entryListId entry list identifier
     * @param Array $params request body
     * @param String $filters limit/sort/page options
     * @param int $id optional ArrayDataProvider identifier
     * @param int $experimentDbId optional experiment identifier. If specified, data source is retrieved from
     *   `experiment.entry` table, otherwise `experiment.parent`
     *
     * @return ArrayDataProvider 
     */
    public function search($entryListId, $params, $filters='', $id=null, $experimentDbId=null) {
        $data = [];

        // Determine data source
        if (empty($experimentDbId)) {
            $response = $this->parentModel->searchParents($entryListId, $params, $filters);

            if ($response['status'] == 200) {
                $data = $response['data'];
            }

            // Data provider config
            $key = 'parentDbId';
            $defaultOrder = [
                'orderNumber' => SORT_ASC,
            ];
        } else {
            $response = $this->parentModel->searchEntries($experimentDbId, $params, $filters);

            if ($response['status'] == 200) {
                $data = $response['data'];

                // Get occurrence info and map to each entry
                $occurrenceParams['experimentDbId'] = "equals $experimentDbId";
                $occurrenceFilters = '&limit=1';
                $occurrenceResponse = $this->occurrence->searchAll($occurrenceParams, $occurrenceFilters, retrieveAll: false);

                $occurrenceName = isset($occurrenceResponse['data'][0]) ? $occurrenceResponse['data'][0]['occurrenceName'] : '';
                $occurrenceCode = isset($occurrenceResponse['data'][0]) ? $occurrenceResponse['data'][0]['occurrenceCode'] : '';

                foreach ($data as $idx => $parentData) {
                    $data[$idx]['occurrenceName'] = $occurrenceName;
                    $data[$idx]['occurrenceCode'] = $occurrenceCode;
                }

                // Set default params
                $params['occurrenceName'] = $occurrenceName;
                $params['occurrenceCode'] = $occurrenceCode;

            }

            // Data provider config
            $key = 'entryDbId';
            $defaultOrder = [
                'entryNumber' => SORT_ASC,
            ];
        }

        $totalCount = $response['totalCount'] ?? 0;
        $totalPages = $response['totalPages'] ?? 0;

        //only load data within params
        $this->load(['MaleModel' => $params]);

        //set page
        if (str_contains($filters, 'page')) {
            $updatedFilter = '';
            $filterList = explode('&', $filters);
            foreach($filterList as $value){
                if (str_contains($value, 'page')) {
                    $page = explode('=', $value)[1];
                } else {
                    $updatedFilter = $updatedFilter."$value&";
                }
            }
            // Retrieve correct set of records upon page change
            if ($page > $totalPages) {
                $updatedFilter = $updatedFilter.'page=1';

                if (empty($experimentDbId)) {
                    $response = $this->parentModel->searchEntries($entryListId, $params, $updatedFilter);
                    $data = $response['data'];
                } else {
                    $response = $this->parentModel->searchEntries($experimentDbId, $params, $updatedFilter);
                    $data = $response['data'];
                    foreach ($data as $idx => $parentData) {
                        $data[$idx]['occurrenceName'] = $occurrenceName;
                        $data[$idx]['occurrenceCode'] = $occurrenceCode;
                    }
                }

                $totalCount = $response['totalCount'] ?? 0;

                $_GET["page"] = 1;
            }
        }

        //build dataProvider
        return new ArrayDataProvider([
            'allModels' => $data,
            'key' => $key,
            'sort' => [
                'attributes' => [
                    'entryCode',
                    'occurrenceName',
                    'occurrenceCode',
                    'experimentName',
                    'experimentCode',
                    'seedName',
                    'packageLabel',
                    'designation',
                    'description',
                    'parentage',
                    'germplasmCode',
                    'generation',
                    'parentRole',
                    'parentType',
                    'entryNumber',
                    'orderNumber',
                    'femaleCount',
                    'maleCount',
                    'germplasmState'
                ],
                'defaultOrder' => $defaultOrder,
            ],
            'restified' => true,
            'totalCount' => $totalCount,
            'id' => $id
        ]);
    }
}
