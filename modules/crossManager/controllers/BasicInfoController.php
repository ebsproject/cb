<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\controllers;

use app\components\B4RController;

//Import Interfaces
use app\interfaces\controllers\IDashboard;
use app\interfaces\models\IConfig;
use app\interfaces\models\IEntryList;

// Import Models
use app\models\EntryListData;
use app\modules\crossManager\models\BasicInfoModel;
use app\modules\crossManager\models\EntryListProtocol;

use Yii;


/**
 * Basic Info controller for the `crossManager` module
 */
class BasicInfoController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public BasicInfoModel $basicInfoModel,
        public EntryListData $entryListDataModel,
        public EntryListProtocol $entryListProtocol,
        public IConfig $configuration,
        public IDashboard $dashboard,
        public IEntryList $entryList,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Loads the basic info tab page
     */
    public function actionLoad($program){
        $widgetOptions = [];
        $entryListId = $_GET['id'] ?? 0;
        $entryListStatus = $entryListStatus ?? "";

        Yii::info('Rendering Basic Info tab ' . json_encode(['program' => $program, 'entryListId' => $entryListId]), __METHOD__);

        // retrieve dashboard program
        $defaultFilters = $this->dashboard->getFilters();
        $dashProgram = $defaultFilters->program_id;

        $entryListInfo = $this->entryList->searchAll([
            'entryListDbId' => "equals $entryListId",
            'fields' => 'entryList.id AS entryListDbId | '.
                'entryList.experiment_id AS experimentDbId | '.
                'entryList.entry_list_status AS entryListStatus | '.
                'entryList.entry_list_name AS entryListName | '.
                'entryList.creator_id AS creatorDbId '
        ]);

        //if id does not exist, display flash message and go to CM landing page
        if(($entryListInfo['totalCount'] == 0 || empty($entryListInfo['data'][0])) && $entryListId){
            Yii::warning('Missing entry list data ' . json_encode(['entryListDbId' => $entryListId]), __METHOD__);
            Yii::$app->response->redirect(['crossManager', 'program' => $program, 'error' => "Cross list with id no. $entryListId does not exist."]);
        }

        // If entry list is already finalized, go to CM landing page
        if (!empty($entryListInfo['data'][0])) {
            $entryListStatus = $entryListInfo['data'][0]['entryListStatus'];
            $entryListName = $entryListInfo['data'][0]['entryListName'];
            if ($entryListStatus === 'finalized') {
                Yii::$app->response->redirect(['crossManager',
                    'program' => $program,
                    'error' => 'Cannot be updated. ' . ucfirst($entryListName). ' has already been finalized. ']);
            }
        }

        if(Yii::$app->access->renderAccess("CROSS_MANAGER_UPDATE","CROSS_MANAGER", $entryListInfo['data'][0]['creatorDbId'] ?? 0, usage:'url')){
            $config = $this->configuration->getConfigByAbbrev('CM_CROSS_LIST_BASIC_INFO');

            if($entryListId !== null){
                $widgetOptions = [
                    'mainApiResourceMethod' => 'POST',
                    'mainApiResourceEndpoint' => 'entry-lists-search',
                    'mainApiSource' => 'identification',
                    'mainApiResourceFilter' => ["entryListDbId"=>"$entryListId"],
                    'id' => $entryListId
                ];
            }

            $widgetOptions = array_merge($widgetOptions,[
                'mainModel' => 'CrossList',
                'config' => $config['Values'] ?? [],
                'program' => $dashProgram,
            ]);

            return $this->render('/basic_info', [
                'programDbId' => $dashProgram,
                'program' => $program,
                'entryListId' => $entryListId,
                'experimentDbId' => $entryListInfo['data'][0]['experimentDbId'] ?? 0,
                'widgetOptions' => $widgetOptions,
                'entryListStatus' => $entryListStatus
            ]);
        }
    }

    /**
     * Loads the view basic info tab page
     * 
     * @param Integer $id for entry list identifier
     * @param String $program for program code
     */
    public function actionIndex($id,$program){
    
        $entryListInfo = $this->entryList->searchAll([
            'entryListDbId' => "equals $id"
        ]);

        // entry list check
        if($entryListInfo['totalCount'] == 0 || empty($entryListInfo['data'][0])){
            Yii::$app->response->redirect(['crossManager', 'program' => $program, 'error' => "Cross list with id no. $id does not exist."]);
        }elseif(Yii::$app->access->renderAccess("CROSS_MANAGER_VIEW","CROSS_MANAGER", $entryListInfo['data'][0]['creatorDbId'] ?? 0, usage:'url')){
            // Render data
            return $this->render('/view/basic', [
                'program' => $program,
                'entryListId' => $id,
                'entryListName' => $entryListInfo['data'][0]['entryListName'] ?? 0,
                'experimentDbId' => $entryListInfo['data'][0]['experimentDbId'] ?? 0,
                'entryListInfo' => $entryListInfo['data'][0]
            ]);
        }
    }

    /**
     * Creates a new cross list
     */
    public function actionSave(){

        $id = $_GET['id'] ?? 0;

        // retrieve form data
        $rawData = array_column($_POST['CrossList'] ?? [],'identification') ?? [];
        $formData = [];
        foreach($rawData as $value){
            $formData = array_merge($value, $formData);
        }
        $formData['programDbId'] = $_GET['programDbId'] ?? 0;

        // create/update record
        $record = $this->basicInfoModel->saveCrossList($id, $formData);

        if(isset($record['success']) && $record['success']){
            $entryListDbId = $record['data'][0]['entryListDbId'] ?? 0;

            // Create entry list-level Trait protocol records
            if (!empty($entryListDbId) && !$this->entryListProtocol->hasProtocol($entryListDbId)) {
                // Create list to store traits
                $listId = $this->entryListProtocol->addList($entryListDbId);

                // Use default traits from config
                $traits = $this->entryListProtocol->getTraitsFromConfig($_GET['program']);
                $this->entryListProtocol->addListMembers($listId, array_column($traits, 'abbrev'));

                // Create entry list data
                $requestData['records'][] = $this->entryListProtocol->getEntryListProtocols($entryListDbId, $listId);
                $result = $this->entryListDataModel->create($requestData);
                Yii::debug('Created entry list data ' . json_encode($result), __METHOD__);
            }

            return json_encode([
                'id' => $entryListDbId,
                'operation' => $id == 0 ? 'create' : 'update',
            ]);
        }

        //log and throw if error is encountered
        $errorMessage = $record['message'] ?? "Problem in saving cross list id: $id";
        Yii::error($errorMessage, __METHOD__);
        throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));

    }

    /**
     * Checks whether an entry list name already exists in the database
     *
     * @param string $program program code
     */
    public function actionCheckEntryListName($program)
    {
        Yii::debug(json_encode($_POST), __METHOD__);
        extract($_POST);

        $duplicateFlag = false;

        if (!empty($inputEntryListName)) {
            if (empty($id)) {
                // Must be a new list
                $duplicateFlag = $this->entryList->nameExists($inputEntryListName);
            } else {
                // Entry list already exists
                $duplicateFlag = $this->entryList->nameExists($inputEntryListName, exceptId: $id);
            }
        }

        return json_encode([
            'duplicateFlag' => $duplicateFlag
        ]);
    }

}
