<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\controllers;

use app\components\B4RController;

use app\interfaces\models\IConfig;
use app\interfaces\models\IEntryList;
use app\interfaces\models\IOccurrence;

use app\models\EntryListData;
use app\models\ExperimentProtocol;
use app\models\OccurrenceData;
use app\modules\crossManager\models\EntryListProtocol;

use Yii;


/**
 * Protocol controller for the `crossManager` module
 */
class ProtocolController extends B4RController
{
    public function __construct($id, $module,
        public EntryListData $entryListDataModel,
        public EntryListProtocol $entryListProtocol,
        public ExperimentProtocol $experimentProtocolModel,
        public IConfig $configModel,
        public IEntryList $entryListModel,
        public IOccurrence $occurrenceModel,
        public OccurrenceData $occurrenceDataModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Loads protocol views of a cross list
     *
     * @param string $program program code
     * @param int $id entry list identifier
     */
    public function actionLoad($program, $id)
    {
        // Get entry list name
        $result = $this->entryListModel->searchAll([
            'entryListDbId' => "equals $id"
        ], 'limit=1&sort=entryListDbId:desc', retrieveAll: false);

        if ($result['success'] && isset($result['data'][0]['entryListName'])) {
            $entryListName = $result['data'][0]['entryListName'];
        }

        // TODO: Get this from config
        $protocolTabs = [
            [
                'display_name' => 'Pollination',
                'action_id' => 'pollination-protocol',
                'item_icon' => 'fa fa-crosshairs',
            ],
            [
                'display_name' => 'Traits',
                'action_id' => 'traits-protocols',
                'item_icon' => 'fa fa-braille',
            ]
        ];

        if(Yii::$app->access->renderAccess("CROSS_MANAGER_UPDATE_PROTOCOLS","CROSS_MANAGER", $result['data'][0]['creatorDbId'] ?? 0, usage:'url')){

            return $this->render('protocol', [
                'program' => $program,
                'protocolTabs' => $protocolTabs,
                'id' => $id,
                'entryListName' => $entryListName ?? '',
            ]);
        }
    }

    /**
     * Renders traits protocol view
     *
     * @param string $program program code
     * @param int $id entry list identifier
     */
    public function actionTraitsProtocols($program, $id)
    {
        // Get the configuration
        $this->layout = '@app/views/layouts/dashboard';

        // Get all trait variables tags
        $traitVariableTags = $this->experimentProtocolModel->getTraitVariablesTags();

        // Get all trait list tags
        $listDbId = $this->entryListProtocol->getListId($id);
        $traitProtocolTags = $this->experimentProtocolModel->getExperimentProtocolTags($listDbId, 'TRAITS');

        // Get saved traits
        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'][0] != '-' ? $_GET['sort'] : substr($_GET['sort'], 1) . ':desc';
            $paramSort = 'sort=' . $sort;
        } else if (isset($_GET['dp-1-sort'])) {
            $sort = $_GET['dp-1-sort'][0] != '-' ? $_GET['dp-1-sort'] : substr($_GET['dp-1-sort'], 1) . ':desc';
            $paramSort = 'sort=' . $sort;
        } else {
            $paramSort = '';
        }
        $traitList = $this->experimentProtocolModel->getExperimentProtocolProvider($listDbId, pageParams: $paramSort);

        return $this->render('traits/_traits', [
            'program' => $program,
            'id' => $id,
            'traitVariableTags' => $traitVariableTags,
            'traitProtocolTags' => $traitProtocolTags,
            'dataProvider' => $traitList,
            'listDbId' => $listDbId,
        ]);
    }

    /**
     * Renders pollination protocol view
     *
     * @param string $program program code
     * @param int $id entry list identifier
     */
    public function actionPollinationProtocol($program, $id)
    {
        // Get the configuration
        $this->layout = '@app/views/layouts/dashboard';
        $config = $this->configModel->getConfigByAbbrev('INTENTIONAL_CROSSING_NURSERY_POLLINATION_PROTOCOL_ACT_VAL');

        $result = $this->entryListModel->searchAll([
            'entryListDbId' => "equals $id",
            'fields' => 'entryList.id AS entryListDbId | entryList.experiment_id AS experimentDbId'
        ], 'limit=1&sort=entryListDbId:desc', retrieveAll: false);
        $experimentDbId = $result['data'][0]['experimentDbId'];

        // Determine whether to get occurrence-level or entry list-level data
        if (!empty($experimentDbId)) {
            // ICN cross list: occurrence-level

            $result = $this->occurrenceModel->searchAll([
                "fields" => "occurrence.id AS occurrenceDbId | occurrence.experiment_id AS experimentDbId",
                "experimentDbId" => "equals $experimentDbId",
            ], 'limit=1', false);
            $occurrenceDbId = $result['data'][0]['occurrenceDbId'];

            $model = 'Experiment';
            $resourceEndpoint = 'occurrence-data-search';
            $resourceFilter = [ 'occurrenceDbId' => "$occurrenceDbId" ];
        } else {
            // non-ICN cross list: entry list-level
            $model = 'CrossList';
            $resourceEndpoint = 'entry-list-data-search';
            $resourceFilter = [ 'entryListDbId' => "$id" ];
        }

        $widgetOptions = [
            'mainModel' => $model,
            'mainApiResourceMethod' => 'POST',
            'mainApiResourceEndpoint' => $resourceEndpoint,
            'mainApiSource' => 'metadata',
            'mainApiResourceFilter' => $resourceFilter,
            'id' => "$id",
            'config' => $config['Values'] ?? [],
            'program' => $program,
            'shouldUpdateOccurrenceProtocols' => '1'  // Set this due to the conditional in components/GenericFormWidget.php::312
        ];

        return $this->render('_pollination', [
            'program' => $program,
            'id' => $id,
            'experimentDbId' => $experimentDbId,
            'widgetOptions' => $widgetOptions,
        ]);
    }

    /**
     * Creates/Updates pollination instructions
     *
     * @param string $program program code
     */
    public function actionSavePollinationInstructions($program)
    {
        Yii::debug(json_encode($_POST), __METHOD__);
        $formData = Yii::$app->request->post();

        $entryListDbId = $formData['entryListDbId'] ?? 0;
        $experimentDbId = $formData['experimentDbId'] ?? 0;

        if (!empty($entryListDbId)) {
            // Determine whether to use occurrence-level (ICN) or entry list-level data (non-ICN)
            if (!empty($experimentDbId)) {
                $result = $this->occurrenceModel->searchAll([
                    "fields" => "occurrence.id AS occurrenceDbId | occurrence.experiment_id AS experimentDbId",
                    "experimentDbId" => "equals $experimentDbId",
                ], 'limit=1', false);
                $occurrenceDbId = $result['data'][0]['occurrenceDbId'];

                $result = $this->occurrenceDataModel->saveOccurrenceData($formData, "$occurrenceDbId", 'pollination');
            } else {
                // Get user-specified instruction
                $key = array_key_first($formData['CrossList']['POLLINATION_INSTRUCTIONS']['metadata']);
                $text = $formData['CrossList']['POLLINATION_INSTRUCTIONS']['metadata'][$key];

                // Create or Update
                if (!$this->entryListProtocol->hasProtocol($entryListDbId, 'pollination')) {
                    $requestData['records'][] = $this->entryListProtocol->getEntryListProtocols($entryListDbId, $text, 'pollination');
                    $result = $this->entryListDataModel->create($requestData);
                    Yii::debug('Created entry list data ' . json_encode($result), __METHOD__);
                } else {
                    $result = $this->entryListProtocol->saveProtocolData($entryListDbId, $text);
                }
            }

            if ($result === true || $result['success']) {
                $success = true;
                $message = 'Successfully saved pollination protocol variables.';
                $notif = "<i class='material-icons green-text'>check</i> ";
            } else {
                $success = false;
                $message = 'There seems to be a problem while updating pollination instructions.';
                $notif = "<i class='material-icons red-text left'>close</i> ";
            }
        }

        return json_encode([
            'success' => $success,
            'message' => Yii::t('app', $message),
            'notif' => $notif
        ]);
    }

    /**
     * Adds selected variables to a list
     *
     * @param string $program program code
     */
    public function actionAddVariables($program)
    {
        Yii::debug(json_encode($_POST), __METHOD__);
        extract($_POST);

        // Check whether to add single variable or multiple variables from list
        if (isset($listDbId, $variableDbId)) {
            // Single variable
            $listMemberId = $this->entryListProtocol->addListMember($listDbId, $variableDbId);

            if (empty($listMemberId)) {
                $success = false;
                $message = 'There seems to be a problem while adding variables. ';
                $notif = "<i class='material-icons red-text left'>close</i> ";
            } else if ($listMemberId < 0) {
                $success = false;
                $message = 'The selected trait is already in the list';
                $notif = "<i class='material-icons orange-text left'>warning</i> ";
            } else {
                $success = true;
                $message = 'Successfully added trait.';
                $notif = "<i class='material-icons green-text left'>done</i> ";
            }

        } else if (isset($listDbId, $sourceListDbId)) {
            $newTraits = $this->experimentProtocolModel->getExperimentProtocolProvider($sourceListDbId);
            $variableDbIds = array_column($newTraits->getModels(), 'variableDbId');
            $addedVariableCount = 0;

            foreach ($variableDbIds as $variableDbId) {
                $listMemberId = $this->entryListProtocol->addListMember($listDbId, $variableDbId);
                if (!empty($listMemberId) && $listMemberId > 0) $addedVariableCount += 1;
            }

            if ($addedVariableCount === 0) {
                $success = false;
                $message = 'Selected traits are already in the list.';
                $notif = "<i class='material-icons orange-text left'>warning</i> ";
            } else if ($addedVariableCount !== count($variableDbIds)) {
                $success = true;
                $message = 'Successfully added traits. Some of them are already in the list.';
                $notif = "<i class='material-icons green-text left'>done</i> ";
            } else {
                $success = true;
                $message = 'Successfully added traits.';
                $notif = "<i class='material-icons green-text left'>done</i> ";
            }
        }

        return json_encode([
            'success' => $success,
            'message' => Yii::t('app', $message),
            'notif' => $notif
        ]);
    }

    /**
     * Renders variables to be added from another list
     *
     * @param string $program program code
     */
    public function actionViewTraitList($program)
    {
        Yii::debug(json_encode($_POST), __METHOD__);
        extract($_POST);

        if (isset($listDbId)) {
            $traitList = $this->experimentProtocolModel->getExperimentProtocolProvider($listDbId);
            $htmlData = $this->renderPartial('traits/_trait_list_preview', [
                'dataProvider' => $traitList
            ]);
        }

        return json_encode([
            'htmlData' => $htmlData
        ]);
    }

    /**
     * Updates remarks of a variable in `platform.list_member` table
     *
     * @param string $program program code
     */
    public function actionSaveRemarks($program)
    {
        Yii::debug(json_encode($_POST), __METHOD__);
        extract($_POST);

        if (isset($listMemberId, $remarks)) {
            $result = $this->entryListProtocol->updateListMember($listMemberId, $remarks);
            if ($result === 1) {
                $success = true;
                $message = 'Successfully updated trait.';
                $notif = "<i class='material-icons green-text left'>done</i> ";
            } else {
                $success = false;
                $message = 'There seems to be a problem while updating instructions. ';
                $notif = "<i class='material-icons red-text left'>close</i> ";
            }
        }

        return json_encode([
            'success' => $success,
            'message' => Yii::t('app', $message),
            'notif' => $notif
        ]);
    }

    /**
     * Remove one or more variables from a list
     *
     * @param string $program program code
     */
    public function actionRemoveVariables($program)
    {
        Yii::debug(json_encode($_POST), __METHOD__);
        extract($_POST);

        if (isset($listMemberIds) && count($listMemberIds) > 0) {
            $data = $this->entryListProtocol->deleteListMembers($listMemberIds);

            // Set defaults
            $success = false;
            $message = '';
            $notif = "<i class='material-icons red-text left'>close</i> ";

            if ($data['success'] > 0) {
                $success = true;
                $message .= "Successfully removed&nbsp;<b>" . $data['success'] . "</b>&nbsp;trait/s.<br>";
                $notif = "<i class='material-icons green-text left'>done</i> ";
            }
            if ($data['failed'] > 0) {
                $message .= "Failed to remove&nbsp;<b>" . $data['failed'] . "</b>&nbsp;trait/s.<br>";
            }
            if ($data['failedNoPermission'] > 0) {
                $message .= "You do not have permission to remove&nbsp;<b>" . $data['failedNoPermission'] . "</b>&nbsp;trait/s.";
            }
        }

        return json_encode([
            'success' => $success,
            'message' => Yii::t('app', $message),
            'notif' => $notif
        ]);
    }
}