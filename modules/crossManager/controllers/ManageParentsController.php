<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\controllers;

use app\components\B4RController;

// Import Interfaces
use app\interfaces\models\IEntryList;
// Import Models
use app\modules\crossManager\models\CrossListModel;
use app\modules\crossManager\models\ParentModel;
use app\modules\occurrence\models\FileExport;
use app\models\Variable;
use Yii;


/**
 * ManageParents controller for the `crossManager` module
 */
class ManageParentsController extends B4RController
{
    public function __construct($id, $module,
        public CrossListModel $crossListModel,
        public IEntryList $entryList,
        public ParentModel $parentModel,
        public Variable $variableModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for manage parents tab
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionLoad($program, $id){
        //params set to null
        $params = '';
        //checker to entryList
        $entryListInfo = $this->entryList->searchAll([
            'entryListDbId' => "equals $id",
            'fields' => 'entryList.id AS entryListDbId | '.
                'entryList.entry_list_status AS entryListStatus | '.
                'entryList.entry_list_name AS entryListName |' .
                'entryList.entry_list_status AS entryListStatus' 
        ]);

        //if id does not exist, display flash message and go to CM landing page
        if($entryListInfo['totalCount'] == 0 || empty($entryListInfo['data'][0])){
            Yii::$app->response->redirect(['crossManager', 'program' => $program, 'error' => "Cross list with id no. $id does not exist."]);
        }

        $entryListStatus = $entryListInfo['data'][0]['entryListStatus'];
        $entryListName = $entryListInfo['data'][0]['entryListName'];

        // If entry list is already finalized, go to CM landing page
        if ($entryListStatus === 'finalized') {
            Yii::$app->response->redirect(['crossManager',
                'program' => $program,
                'error' => 'Cannot be updated. ' . ucfirst($entryListName). ' has already been finalized. ']);
        }

        //get value of column filter in array and set value to params for filter
        if(isset($_GET['ParentModel'])){
            $params = array_filter($_GET['ParentModel'], 'strlen');
        }
        //if params / filters are empty, set to empty string
        if(empty($params)){
            $params = "";
        }

        //get values and filters
        $request = Yii::$app->request;
        $filters = $this->parentModel->getFilters($request->get());
        $dataProvider = $this->parentModel->search($id, $params, $filters);

        $parentTypeOptions = $this->crossListModel->getOptions('PARENT_TYPE');
        $parentRoleOptions = [
            'female' => 'female',
            'male' => 'male',
            'female-and-male' => 'female-and-male',
        ];

        //return to views to render values
        return $this->render('/manage_parents', [
            'entryListId' => $id,
            'entryListName' => $entryListInfo['data'][0]['entryListName'] ?? '',
            'program' => $program,
            'parentModel' => $this->parentModel,
            'parentTypeOptions' => $parentTypeOptions,
            'parentRoleOptions' => $parentRoleOptions,
            'dataProvider' => $dataProvider,
            'entryListStatus' => $entryListInfo['data'][0]['entryListStatus'] ?? ''
        ]);
    }

    /**
     * Update parent information(role, type, and description)
     */
    public function actionUpdateParent(){
        extract($_POST);
        return json_encode($this->parentModel->updateOne($parentId, [
            $column => $value
        ]));
    }

    /**
     * Delete specific parent record in manage parent browser
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionDeleteParent($program, $id){
        extract($_POST);
        $parents = $this->parentModel->searchParents($id, [
            'parentDbId' => 'equals '.implode('|equals ', $parentIds),
            'includeParentCount' => true,
        ])['data'] ?? [];
        // For parent Validation
        $count = 0;
        $femaleCount = '';
        $maleCount = '';
        while(count($parents) > $count){
            $femaleCount = $parents[$count]['femaleCount'];
            $maleCount = $parents[$count]['maleCount'];
            if($femaleCount > 0 || $maleCount > 0){
                unset($parentIds[$count]);
            }
            $count++;
        }
        if(empty($parentIds)){
            return json_encode(['success' => false, 'message' => Yii::t('app', 'Parents cannot be deleted since they are already used in crosses')]);
        }

        $delete = $this->parentModel->deleteMany($parentIds);
        if($delete['success']){
            $message = Yii::t('app', $delete['totalCount']. ' item(s) deleted');
            return json_encode(['success' => $delete['success'], 'message' => Yii::t('app', $message)]);
        }
        else{
            return false;
        }
    }

    /**
     * Delete all parent records in manage parent browser
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     * 
     */
    public function actionDeleteParentAll($program, $id){
        $parents = $this->parentModel->searchParents($id, [
            'includeParentCount' => true,
        ],'',true)['data'] ?? [];
        $parentIds = array_column($parents,'parentDbId');
        // For parent validation
        $count = 0;
        $femaleCount = '';
        $maleCount = '';
        while(count($parents) > $count){
            $femaleCount = $parents[$count]['femaleCount'];
            $maleCount = $parents[$count]['maleCount'];
            if($femaleCount > 0 || $maleCount > 0){
                unset($parentIds[$count]);
            }
            $count++;
        }
 
        $delete = $this->parentModel->deleteMany($parentIds);
        if($delete['success']){
            $message = Yii::t('app', 'All parents without crosses have been deleted');
            return json_encode(['success' => $delete['success'], 'message' => Yii::t('app', $message)]);
        }
        else{
            return false;
        }
        
    }

    /**
     * Check required fields in crosses' browser
     * Update status of CM based on required fields
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionCheckRequiredFields($program, $id){
        $requiredFields = $_POST['requiredFields'] ?? [];
        $filter['entryListDbId'] = 'equals '.$_POST['entryListDbId'] ?? 0;
        $filter['fields'] = 'parent.id AS parentDbId | parent.entry_list_id AS entryListDbId | parent.parent_role AS parentRole';
        foreach($requiredFields as $field){
            $filter[$field] = 'is null';
        }

        //search all parents added to this entrylist
        $parents = $this->parentModel->searchParents($id, [
            'fields' => 'parent.id AS parentDbId | parent.entry_list_id AS entryListDbId'], 
            'limit=1', false
        )['data'] ?? [];

        //check if all parents have parentRoles
        $checker = $this->parentModel->searchAll($filter,'limit=1', false)['totalCount'];

        //get status of the current entry list
        $getStatus = $this->entryList->searchAll([
            'entryListDbId' => "equals $id",
            'fields' => 'entryList.id AS entryListDbId | entryList.entry_list_status AS entryListStatus'
        ])['data']['0'] ?? [];

        //check if the status is crosses added or specified
        if($getStatus['entryListStatus'] != "crosses added" && $getStatus['entryListStatus'] != "cross list specified"){
            //if parentRole null count to zero and parent table is not empty, update status to specified
            if($checker == 0 && !empty($parents)){
                $update = $this->entryList->updateOne($id, [
                    "entryListStatus" => "parent list specified"
                ]);
                return 0;
            }
            //if the data is empty, update status to draft
            else if(empty($parents))
            {
                $update = $this->entryList->updateOne($id, [
                    "entryListStatus" => "draft"
                ]);           
                return 1;
            }
            //if above is not met, update status to added
            else{
                $update = $this->entryList->updateOne($id, [
                    "entryListStatus" => "parents added"
                ]);           
                return 1;
            }
        }
        //if yes, then return 0 to enable next button
        else{
            return 0;
        }
        
    }

    /**
     * Check if parent table is empty
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionCheckEmptyTable($program, $id){
        //search all parents added to this entrylist
        $parents = $this->parentModel->searchParents($id, [
            'fields' => 'parent.id AS parentDbId | parent.entry_list_id AS entryListDbId'], 
            'limit=1', false
        )['data'] ?? [];

        //if parents were empty return message
        if(empty($parents))
        {      
            return json_encode(['success' => false, 'message' => Yii::t('app', 'No parents found.')]);
        }
        //if parents are not empty return
        else{
            return json_encode(['success' => true]);
        }
    }

    /**
     * Set bulk update modal body
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionModalBody($program, $id){
        //get config
        $config = $this->parentModel->retrieveConfiguration();
        $fields = [];
        $parentForm = "/crossManager/manage-parents/render-config-form";
        
        //set each value to fields array
        foreach($config as $value){
            //get variable
            $varInfo = $this->variableModel->getVariableByAbbrev($value['variable_abbrev']);

            $header = isset($value['display_name']) && !empty($value['display_name']) ? $value['display_name'] : $varInfo['label'];
            $fields[$value['variable_abbrev']] = ucwords(strtolower($header));
        }

        $htmlData = $this->renderAjax('/bulk_update_form.php',[
            'data' => $fields,
            'id' => $id,
            'program' => $program,
            'parentForm' => $parentForm,
        ], true, false);

        return json_encode($htmlData);
    }

    /**
     * Set retrieve configuration based on chosen variable
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionRenderConfigForm($id, $program = null){
        //get configuration
        $config = $this->parentModel->retrieveConfiguration();
        $setConfig = [];

        if(isset($_POST)){
            $variableAbbrev = isset($_POST['selected']) ? strtoupper($_POST['selected']) : '';
            $formType = isset($_POST['formType']) ? strtoupper($_POST['formType']) : '';

            //loop configuration until it matches the selected variable
            foreach($config as $value){
                if($value['variable_abbrev'] == $variableAbbrev && !(isset($value['is_hidden']) && $value['is_hidden'])){
                    $setConfig[] = $value;
                }
            }

            $widgetOptions = [
                'id' => 'req-vals',
                'config' => $setConfig,
                'formType' => $formType
            ];

            //render Ajax
            $htmlData = $this->renderAjax('/bulk_update_value.php',[
                'widgetOptions' => $widgetOptions,
            ]);
            
            //return to modal body
            return json_encode($htmlData);
        }
    }

    /**
     * Bulk update selected parents
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionUpdateParentsSelected($program, $id){
        $column = $_POST['column'] ?? [];
        $values = $_POST['values'] ?? [];
        $parentIds = $_POST['parentIds'] ?? [];

        if($column == "ENTRY_ROLE"){
            $column = "parentRole";
        }
        
        if($column == "PARENT_TYPE"){
            $column = "parentType";
        }
        
        if($column == "DESCRIPTION"){
            $column = "description";
        }

        if(empty($parentIds)){
            return json_encode(['success' => false, 'message' => Yii::t('app', 'Please select an item.')]);
        }

        //Update
        $update = $this->parentModel->updateMany($parentIds, [
            $column => $values
        ]);

        if($update['success']){
            $message = Yii::t('app', 'Selected parents updated');
            return json_encode(['success' => $update['success'], 'message' => Yii::t('app', $message)]);
        }
        else{
            return false;
        }
    }

    /**
     * Bulk update all parents
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionUpdateParentsAll($program, $id){
        $column = $_POST['column'] ?? [];
        $values = $_POST['values'] ?? [];
        $parents = $this->parentModel->searchParents($id, [
            'includeParentCount' => true,
            'fields' => 'parent.id AS parentDbId | parent.entry_list_id AS entryListDbId |'.
                        'parent.parent_type AS parentType | parent.parent_role AS parentRole |'.
                        'parent.description AS description'
        ],'',true)['data'] ?? [];

        $parentIds = array_column($parents,'parentDbId');

        //Set column values for parent table
        if($column == "ENTRY_ROLE"){
            $column = "parentRole";
        }
        
        if($column == "PARENT_TYPE"){
            $column = "parentType";
        }
        
        if($column == "DESCRIPTION"){
            $column = "description";
        }

        if(empty($parentIds)){
            return json_encode(['success' => false, 'message' => Yii::t('app', 'No parents found.')]);
        }

        //Update
        $update = $this->parentModel->updateMany($parentIds, [
            $column => $values
        ]);

        if($update['success']){
            $message = Yii::t('app', 'All parents updated');
            return json_encode(['success' => $update['success'], 'message' => Yii::t('app', $message)]);
        }
        else{
            return false;
        }
    }

    /**
     * Export parent list in CSV format
     *
     * @param integer $id entryListDbId identifier
     */
    public function actionExportParents($id)
    {
        $csvContent = [[]];
        $i = 1;
        // Declare attributes for csv
        $csvAttributes = [
                "occurrenceName",
                "occurrenceCode",
                "entryNumber",
                "entryCode",
                "parentRole",
                "seedName",
                "packageLabel",
                "designation",
                "generation",
                "parentage",
        ];
        // Declare headers for csv
        $csvHeaders = [
            "OCCURRENCE NAME",
            "OCCURRENCE CODE",
            "ENTRY NO.",
            "ENTRY CODE",
            "PARENT ROLE",
            "SEED NAME",
            "PACKAGE LABEL",
            "GERMPLASM NAME",
            "GENERATION",
            "PARENTAGE",
        ];

        // Search all parents within this entry list
        $parents = $this->parentModel->searchParents($id, [
            'fields' => 'occ.occurrence_name AS occurrenceName | occ.occurrence_code AS occurrenceCode | '.
                        'entry.entry_number AS entryNumber | entry.entry_code AS entryCode | '.
                        'parent.parent_role AS parentRole | seed.seed_name AS seedName | '.
                        'package.package_label AS packageLabel | germplasm.designation AS designation | '.
                        'germplasm.generation AS generation | germplasm.parentage AS parentage'
        ],'', true) ?? [];

        // Assigning values from parents, csvAttribute and csvHeaders to csvContent 
        if (!empty($parents['data']) && isset($parents['data'])) {
            foreach ($csvHeaders as $key => $value)
                array_push($csvContent[0], $value);

            foreach ($parents['data'] as $key => $value) {
                $csvContent[$i] = [];

                foreach ($csvAttributes as $k => $v)
                    array_push($csvContent[$i], $value[$v]);

                $i += 1;
            }
        }

        // Retrieve entry list name and id
        $entryListData = $this->entryList->searchAll([
            'entryListDbId' => "equals $id",
            'fields' => 'entryList.id AS entryListDbId | entryList.entry_list_name AS entryListName'
        ]);

        $entryListId = $entryListData['data']['0']['entryListDbId'];
        $entryListName = $entryListData['data']['0']['entryListName'];

        // Retrieve file types and formats
        $toExport = new FileExport(FileExport::PARENT_LIST, FileExport::CSV, time());
        $toExport->addExperimentData($entryListName, [$entryListId], [$csvContent]);

        // Set filename
        $buffer = $toExport->getUnzipped();
        $filename = array_key_first($buffer);
        $content = $buffer[$filename];

        // Insert and build csv file
        header("Content-Disposition: attachment; filename={$filename}; Content-Type: application/csv;");

        $fp = fopen('php://output', 'wb');

        if ($fp === false) die('Failed to create CSV file.');

        foreach ($content as $line) fputcsv($fp, $line);

        fclose($fp);

        exit;
    }
}