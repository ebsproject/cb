<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\controllers;

use app\components\B4RController;

//Import Interfaces
use app\interfaces\models\IEntryList;
use app\interfaces\models\IOccurrence;

//Import Models
use app\models\Api;
use app\models\Entry;
use app\models\User;
use app\modules\crossManager\models\CrossListModel;
use app\modules\crossManager\models\CrossModel;
use app\modules\crossManager\models\CrossParentModel;
use app\modules\crossManager\models\ParentModel;
use app\modules\crossManager\models\MaleModel;
use Yii;


/**
 * AddCrosses controller for the `crossManager` module
 */
class AddCrossesController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public Api $api,
        public CrossListModel $crossListModel,
        public CrossModel $crossModel,
        public CrossParentModel $crossParentModel,
        public Entry $entryModel,
        public IEntryList $entryList,
        public IOccurrence $occurrence,
        public ParentModel $parentModel,
        public MaleModel $maleModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Loads the Add Crosses tab
     * 
     * @param String $program program code
     * @param Integer $id entry list identifier
     */
    public function actionLoad($program, $id){
        Yii::info('Loading Add Crosses tab '.json_encode(['program'=>$program, 'entryListId'=>$id]), __METHOD__);

        // get threshold config
        $threshold = !empty(Yii::$app->config->getAppThreshold('CM', 'validateCrosses')) ? Yii::$app->config->getAppThreshold('CM', 'validateCrosses') : 1500;
        $userId = (new User())->getUserId();
        $selectedFemaleIdName = "cm_$userId"."_selected_female_entry_ids_$id";
        $selectedMaleIdName = "cm_$userId"."_selected_male_entry_ids_$id";

        // Destroy selected items in session, if applicable
        if (((isset($_GET['reset']) && $_GET['reset'] == 'female_hard_reset' && !isset($_GET['page']))) ||
            (!isset($_GET['reset']) && !isset($_GET['page']) && !isset($_GET['_pjax'])) && isset($_SESSION[$selectedFemaleIdName])) {
            unset($_SESSION[$selectedFemaleIdName]);
        }
        if (((isset($_GET['reset']) && $_GET['reset'] == 'male_hard_reset' && !isset($_GET['male-page']))) ||
            (!isset($_GET['reset']) && !isset($_GET['male-page']) && !isset($_GET['_pjax'])) && isset($_SESSION[$selectedMaleIdName])) {
            unset($_SESSION[$selectedMaleIdName]);
        }

        $selectedFemaleItems = Yii::$app->session->get($selectedFemaleIdName, []);
        $selectedMaleItems = Yii::$app->session->get($selectedMaleIdName, []);

        $entryListInfo = $this->entryList->searchAll([
               'fields' => 'entryList.id AS entryListDbId | '.
                   'entryList.experiment_id AS experimentDbId | '.
                   'entryList.entry_list_status AS entryListStatus | '.
                   'entryList.entry_list_name AS entryListName',
               'entryListDbId' => "equals $id"
        ]);

        //if id does not exist, display flash message and go to CM landing page
        if($entryListInfo['totalCount'] == 0 || empty($entryListInfo['data'][0])){
            Yii::warning('Missing entry list data ' . json_encode(['entryListDbId'=>$id]), __METHOD__);
            Yii::$app->response->redirect(['crossManager', 'program' => $program, 'error' => "Cross list with id no. $id does not exist."]);
        }

        $entryListStatus = $entryListInfo['data'][0]['entryListStatus'];
        $entryListName = $entryListInfo['data'][0]['entryListName'];

        // If entry list is already finalized, go to CM landing page
        if ($entryListStatus === 'finalized') {
            Yii::$app->response->redirect(['crossManager',
                'program' => $program,
                'error' => 'Cannot be updated. ' . ucfirst($entryListName). ' has already been finalized. ']);
        }

        $experimentDbId = $entryListInfo['data'][0]['experimentDbId'];
        $maxCrossCount = $threshold;
        $request = Yii::$app->request;
        $femaleParams['parentRole'] = 'equals female|equals female-and-male';
        $maleParams['parentRole'] = 'equals male|equals female-and-male';
        
        if(isset($_GET['ParentModel'])){
            $params = array_filter($_GET['ParentModel'], 'strlen');
        }
        
        // retrieve female browser search parameters and filters
        if(isset($_GET['_pjax']) && $_GET['_pjax'] == '#cm-female-grid-pjax'){
            $femaleParams = $params ?? [];
            $femaleParams['parentRole'] = $femaleParams['parentRole'] ?? 'equals female|equals female-and-male';
        }
        Yii::$app->session->set('cm_female_entry_ids_params', $femaleParams);

        //set params to include count on femaleCount
        $femaleParams['includeParentCount'] = true;
        $femaleParams['retrievePIentryCode'] = true;
        $femaleParams['retrievePIseedDbId'] = true;

        $filters = $this->parentModel->getFilters($request->get(), 'cm-female-grid_');

        // Retrieve female parents created in EC
        if (!empty($experimentDbId)) {
            $filters = $this->parentModel->getFilters($request->get(), 'cm-female-grid_',
                sort: '&sort=entryNumber');

            $femaleParams['fields'] = "entry.id AS entryDbId".
                "|experiment.id AS experimentDbId".
                "|entry.entry_code AS entryCode".
                "|experiment.experiment_name AS experimentName".
                "|experiment.experiment_code AS experimentCode".
                "|seed.seed_name AS seedName".
                "|package.package_label AS packageLabel".
                "|germplasm.designation AS designation".
                "|entry.description AS description".
                "|germplasm.parentage AS parentage".
                "|germplasm.germplasm_code AS germplasmCode".
                "|germplasm.generation AS generation".
                "|entry.entry_role AS parentRole".
                "|entry.entry_type AS parentType".
                "|entry.entry_number AS entryNumber".
                "|femaleCountField".
                "|maleCountField".
                "|germplasm.germplasm_state AS germplasmState".
                "|entry.germplasm_id AS germplasmDbId";
            $femaleParams['experimentDbId'] = "equals $experimentDbId";
        }
        $femaleDataProvider = $this->parentModel->search($id, $femaleParams, $filters, experimentDbId: $experimentDbId);

        // retrieve male browser search parameters and filters
        if(isset($_GET['MaleModel'])){
            $params = array_filter($_GET['MaleModel'], 'strlen');
        }

        if(isset($_GET['_pjax']) && $_GET['_pjax'] == '#cm-male-grid-pjax'){
            $maleParams = $params ?? [];
            $maleParams['parentRole'] = $maleParams['parentRole'] ?? 'equals male|equals female-and-male';
        }
        Yii::$app->session->set('cm_male_entry_ids_params', $maleParams);

        //set params to include count on femaleCount
        $maleParams['includeParentCount'] = true;
        $maleParams['retrievePIentryCode'] = true;
        $maleParams['retrievePIseedDbId'] = true;

        $filters = $this->parentModel->getFilters($request->get(), 'cm-male-grid_', 'male-page');

        // Retrieve female parents created in EC
        if (!empty($experimentDbId)) {
            $filters = $this->parentModel->getFilters($request->get(), 'cm-male-grid_', 'male-page',
                sort: '&sort=entryNumber');

            $maleParams['fields'] = $femaleParams['fields'];
            $maleParams['experimentDbId'] = "equals $experimentDbId";
        }
        $maleDataProvider = $this->maleModel->search($id, $maleParams, $filters, 'male', experimentDbId: $experimentDbId);

        return $this->render('/add_crosses/main', [
            'program' => $program,
            'entryListId' => $id,
            'entryListName' => $entryListInfo['data'][0]['entryListName'] ?? '',
            'femaleDataProvider' => $femaleDataProvider,
            'maleDataProvider' => $maleDataProvider,
            'parentModel' => $this->parentModel,
            'maleModel' => $this->maleModel,
            'maxCrossCount' => $maxCrossCount,
            'selectedFemaleItems' => $selectedFemaleItems,
            'selectedMaleItems' => $selectedMaleItems,
            'experimentDbId' => $entryListInfo['data'][0]['experimentDbId'] ?? 0,
            'entryListStatus' => $entryListInfo['data'][0]['entryListStatus'] ?? 0
        ]);
    }

    /**
     * Creates pairs of female and male parent
     *
     * @param string $program program code
     * @param int $id entry list identifier
     */
    public function actionPairParents($program, $id) {
        Yii::debug(json_encode($_POST), __METHOD__);

        $userId = (new User())->getUserId();
        $selectedFemaleIdName = "cm_$userId"."_selected_female_entry_ids_$id";
        $selectedMaleIdName = "cm_$userId"."_selected_male_entry_ids_$id";
        $selectedFemaleItems = Yii::$app->session->get($selectedFemaleIdName, []);
        $selectedMaleItems = Yii::$app->session->get($selectedMaleIdName, []);

        // Generate combinations from selected items
        $crosses = [];
        foreach ($selectedFemaleItems as $female) {
            $crosses = array_merge($crosses, array_map(array($this->crossModel, 'makePair'),
                array_fill(0, count($selectedMaleItems), $female), $selectedMaleItems));
        }
        $crosses = array_filter($crosses);

        // Simplify crosses to just entry code if parents are unique
        extract($_POST);
        if (!empty($experimentDbId)) {
            $parents = $this->parentModel->searchEntryParentsData($experimentDbId);
        } else {
            $parents = $this->parentModel->searchParents($id, retrieveAll: true)['data'] ?? [];
        }
        $crosses = $this->crossModel->simplifyPairs($crosses, $this->parentModel->getMetadata($parents));

        $results = [
            'crossString' => join("\n", $crosses),
            'invalidCrossCount' => count($selectedFemaleItems) * count($selectedMaleItems) - count($crosses)
        ];

        return json_encode($results);
    }

    /**
     * Creates self cross and saves corresponding parent record
     *
     * @param string $program program code
     * @param int $id entry list identifier
     */
    public function actionSelfCrosses($program, $id) {
        Yii::info('Saving SELF cross and cross parent records '.
            json_encode(['program'=>$program, 'entryListDbId'=>$id]), __METHOD__);
        Yii::debug(json_encode($_POST), __METHOD__);

        $validCrossCount = 0;
        
        if ($_POST['sessionName']) {
            extract($_POST);

            $userId = (new User())->getUserId();
            $selectedIdName = "cm_$userId"."_selected_$sessionName"."_$id";
            $selectedItems = Yii::$app->session->get($selectedIdName, []);

            // Ignore duplicate entries
            $selectedItems = array_unique($selectedItems);

            $crossList = array_map(fn($item) => "$item|$item", $selectedItems);
            $result = $this->crossModel->parseCrossList($crossList);
            $crosses = $result['crosses'];
            $crossKeys = $result['crossKeys'];

            // Get all possible parents
            if (!empty($experimentDbId)) {
                $parents = $this->parentModel->searchEntryParentsData($experimentDbId);
            } else {
                $params = [ 'entryCode' => 'equals '.implode('|equals ', array_keys($result['entryCodes'])) ];
                $parents = $this->parentModel->searchParents($id, $params, retrieveAll: true)['data'] ?? [];
            }
            $parentsMetadata = $this->parentModel->getMetadata($parents);

            // Add additional fields to cross data
            $moreCrossInfo = ['crossName', 'experimentDbId', 'occurrenceDbId'];
            Yii::info('Generating cross info from parent data '.json_encode(['crossInfo'=>$moreCrossInfo]), __METHOD__);
            $crosses = $this->crossModel->addCrossFields($moreCrossInfo, $crosses, $parentsMetadata, isSelfing: true);

            $existingCrosses = $this->crossModel->getExistingCrosses($id);

            $crosses = $this->crossModel->validateCrossList($crosses, $crossKeys, $parentsMetadata, $existingCrosses, isSelfingValid: true);
            $validCrosses = array_filter($crosses, fn($cross) => $cross['status'] === 'valid');
            $validCrossCount = count($validCrosses);

            // Create cross records and store the new cross IDs
            $crossRecords = $this->crossModel->toRecords($id, $validCrosses);
            $result = $this->crossModel->create($crossRecords);
            if ($result['status'] == 200 && count($result['data']) > 0) {
                Yii::info('Successfully created cross records '.json_encode(['entryListDbId'=>$id,
                        'count'=>$result['totalCount']]),__METHOD__);
                // Assumes the response data returns successful inserts in order that the records were passed
                $createdResponseData = array_reverse($result['data']);
                foreach ($crosses as $idx => $cross) {
                    if ($cross['status'] == 'valid') {
                        $crosses[$idx]['crossDbId'] = array_pop($createdResponseData)['crossDbId'];
                    }
                }
                $validCrosses = array_filter($crosses, fn($cross) => $cross['status'] === 'valid');
            } else {
                Yii::warning(substr('Failed creating cross records '.json_encode(['entryListDbId'=>$id,
                        'crossRecords'=>$crossRecords]), 0, length: 1000), __METHOD__);
            }

            // Create cross parent records for each cross record
            $crossParentRecords = $this->crossParentModel->toRecords($validCrosses, $parentsMetadata);
            $result = $this->crossParentModel->create($crossParentRecords);
            if ($result['status'] != 200) {
                Yii::warning(substr('Failed creating cross parent records '.json_encode(['entryListDbId'=>$id,
                        'crossParentRecords'=>$crossParentRecords]), 0, length: 1000), __METHOD__);
            }
        }

        // This transaction is at the very end to avoid database race condition between INSERT and counting
        if ($validCrossCount > 0) {
            $final = $this->crossListModel->updateCrossCount($id);
            if($final['success']){
                //check if all crosses have prepopulated crossMethod
                $checker = $this->crossModel->searchAll([
                    'entryListDbId' => 'equals '.$id,
                    'crossMethod' => 'is null',
                    'fields' => 'germplasmCross.id AS crossDbId | germplasmCross.entry_list_id AS entryListDbId | germplasmCross.cross_method as crossMethod'
                ], 'limit=1&sort=crossDbId:asc', false)['totalCount'];
                if($checker == 0){
                    $update = $this->entryList->updateOne($id, [
                        "entryListStatus" => "cross list specified"
                    ]); 
                }
                else{
                    $update = $this->entryList->updateOne($id, [
                        "entryListStatus" => "crosses added"
                    ]); 
                }
            }
        }

        return json_encode($validCrossCount);
    }

    /**
     * Saves cross and corresponding cross parent records
     *
     * @param string $program program code
     * @param int $id entry list identifier
     */
    public function actionSaveCrosses($program, $id) {
        Yii::info('Saving cross and cross parent records '.json_encode(['program'=>$program, 'entryListDbId'=>$id]), __METHOD__);
        Yii::debug(json_encode($_POST), __METHOD__);

        extract($_POST);

        $crossList = array_filter(preg_split("/[\r\n]/", $crosses));
        $result = $this->crossModel->parseCrossList($crossList);
        $crosses = $result['crosses'];
        $crossKeys = $result['crossKeys'];

        // Get all possible parents
        if (!empty($experimentDbId)) {
            $parents = $this->parentModel->searchEntryParentsData($experimentDbId);
        } else {
            $params = [ 'entryCode' => 'equals '.implode('|equals ', array_keys($result['entryCodes'])) ];
            $parents = $this->parentModel->searchParents($id, $params, retrieveAll: true)['data'] ?? [];
        }
        $parentsMetadata = $this->parentModel->getMetadata($parents);

        $moreCrossInfo = ['crossName', 'experimentDbId', 'occurrenceDbId'];
        Yii::info('Generating cross info from parent data '.json_encode(['crossInfo'=>$moreCrossInfo]), __METHOD__);
        $crosses = $this->crossModel->addCrossFields($moreCrossInfo, $crosses, $parentsMetadata);

        $existingCrosses = $this->crossModel->getExistingCrosses($id);

        $crosses = $this->crossModel->validateCrossList($crosses, $crossKeys, $parentsMetadata, $existingCrosses);
        $validCrosses = array_filter($crosses, fn($cross) => $cross['status'] === 'valid');
        $invalidCrosses = array_filter($crosses, fn($cross) => $cross['status'] === 'invalid');
        $validCrossCount = count($validCrosses);
        $invalidCrossCount = count($invalidCrosses);

        // Create cross records and store the new cross IDs
        $crossRecords = $this->crossModel->toRecords($id, $validCrosses);
        $result = $this->crossModel->create($crossRecords);
        if ($result['status'] == 200 && count($result['data']) > 0) {
            Yii::info('Successfully created cross records '.json_encode(['entryListDbId'=>$id,
                'count'=>$result['totalCount']]),__METHOD__);
            // Assumes the response data returns successful inserts in order that the records were passed
            $createdResponseData = array_reverse($result['data']);
            foreach ($crosses as $idx => $cross) {
                if ($cross['status'] == 'valid') {
                    $crosses[$idx]['crossDbId'] = array_pop($createdResponseData)['crossDbId'];
                }
            }
            $validCrosses = array_filter($crosses, fn($cross) => $cross['status'] === 'valid');
        } else {
            Yii::warning(substr('Failed creating cross records '.json_encode(['entryListDbId'=>$id,
                'crossRecords'=>$crossRecords]), 0, length: 1000), __METHOD__);
        }

        // Create cross parent records for each cross record
        $crossParentRecords = $this->crossParentModel->toRecords($validCrosses, $parentsMetadata);
        $result = $this->crossParentModel->create($crossParentRecords);
        if ($result['status'] != 200) {
            Yii::warning(substr('Failed creating cross parent records '.json_encode(['entryListDbId'=>$id,
                'crossParentRecords'=>$crossParentRecords]), 0, length: 1000), __METHOD__);
        }

        $note = '';
        // Handle duplicate records
        $duplicateCrosses = array_filter($invalidCrosses, fn($cross) =>
            $cross['remarks'] === $this->crossModel::REPEATED_FPARENT ||
            $cross['remarks'] === $this->crossModel::REPEATED_MPARENT ||
            $cross['remarks'] === $this->crossModel::REPEATED_PARENTS
        );
        if (count($duplicateCrosses) > 0) {
            $note = "<br/><br/> <i class='orange-text smaller fa fa-exclamation-circle '></i> <b> NOTICE: </b>
             Some crosses have parent/s with duplicate info.";
        }

        $message = "
            <ul>
                <li><i class='green-text smaller fa fa-check-circle'></i> Total of valid crosses: <b> " .number_format($validCrossCount). "</b></li>
                <li><i class='red-text smaller fa fa-question-circle'></i> Total of invalid crosses: <b>  " .number_format($invalidCrossCount). "</b></li>
                </ul>
            Valid crosses are automatically added in the cross list. Invalid crosses are left out in the text area for checking - hover the icon located on each row for more details.
            $note";

        // This transaction is at the very end to avoid database race condition between INSERT and counting
        if ($validCrossCount > 0) {
            $final = $this->crossListModel->updateCrossCount($id);
            //if update is success, update status
            if($final['success']){
                $update = $this->entryList->updateOne($id, [
                    "entryListStatus" => "crosses added"
                ]); 
            }
        }

        $results = [
            'crosses' => $crosses,
            'invalidCount' => $invalidCrossCount,
            'message' => \Yii::t('app', $message),
        ];

        return json_encode($results);
    }

    /**
     * Sets selected items in the grid
     * TODO: Use modules/crossManager/controllers/DefaultController.php::actionStoreSessionItems instead
     */
    public function actionStoreSessionItems() {
        Yii::debug(json_encode($_POST), __METHOD__);

        $count = 0;

        if (isset($_POST['unset'], $_POST['all'], $_POST['entryListDbId'])) {
            extract($_POST);

            $sessionName = $sessionName ?? 'entry_ids';
            $userId = (new User())->getUserId();
            $selectedIdName = "cm_$userId"."_selected_$sessionName"."_$entryListDbId";

            $items = Yii::$app->session->get($selectedIdName, []);

            // Deselect items
            if ($unset === 'true') {
                if ($all === 'true') {
                    $items = [];
                } else if (isset($entryDbId)) {
                    unset($items[$entryDbId]);
                }
            } else {  // Select items
                if ($all === 'true') {
                    $items = [];
                    $params = Yii::$app->session->get('cm_'.$sessionName.'_params');

                    // Select data source of parents
                    if (!empty($experimentDbId)) {
                        // From experiment.entry
                        $params['fields'] = "entry.id AS entryDbId" .
                            "|experiment.id AS experimentDbId".
                            "|entry.entry_role AS parentRole".
                            "|entry.entry_code AS entryCode";
                        $params['experimentDbId'] = "equals $experimentDbId";
                        $parents = $this->entryModel->searchAll($params)['data'];

                        // Get occurrence info and map to each entry
                        $occurrenceData = $this->occurrence->searchAll(params: ['experimentDbId'=>"equals $experimentDbId", 'fields' => 'occurrence.experiment_id AS experimentDbId | occurrence.occurrence_code AS occurrenceCode'],
                            filters: '&limit=1', retrieveAll: false)['data'][0];
                        $occurrenceCode = $occurrenceData['occurrenceCode'];
                    } else {  // From experiment.parent
                        $parents = $this->parentModel->searchParents($entryListDbId, $params, retrieveAll: true)['data'] ?? [];
                    }

                    foreach ($parents as $parent) {
                        $key = empty($experimentDbId) ? $parent['parentDbId'] : $parent['entryDbId'];
                        $occurrenceCode = empty($experimentDbId) ? $parent['occurrenceCode'] : $occurrenceCode;
                        $entryCode = $parent['entryCode'];
                        $items[$key] = "$occurrenceCode:$entryCode";
                    }
                } else if (isset($entryDbId) && isset($entryData) && !isset($items[$entryDbId])) {
                    $items[$entryDbId] = $entryData;
                }
            }

            Yii::$app->session->set($selectedIdName, $items);
            $count = count($items);
        }

        return json_encode($count);
    }

    /**
     * Gets the number of currently selected cross parents in session.
     * * TODO: Use modules/crossManager/controllers/DefaultController.php::actionGetUserSelectionCount instead
     *
     * @param int $id entry list identifier
     */
    public function actionGetUserSelectionCount($id) {
        $userId = (new User())->getUserId();
        if (isset($_POST)) {
            $sessionName = $_POST['sessionName'];
            $sessionCount = isset($_SESSION["cm_$userId"."_selected_".$sessionName."_".$id]) ? count($_SESSION["cm_$userId"."_selected_".$sessionName."_".$id]) : 0;
            return json_encode($sessionCount);
        }else{
            return json_encode(false);
        }
    }

    /**
     * Check required fields in manage crosses browser
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionCheckRequiredFields($program, $id){
        //search all crosses in this entry list; filtered to one and only crossDbId
        $crosses = $this->crossModel->searchAll([
            'entryListDbId' => 'equals '.$id,
            'fields' => 'germplasmCross.id AS crossDbId | germplasmCross.entry_list_id AS entryListDbId'
        ], 'limit=1&sort=crossDbId:asc', false) ?? [];

        //check if there are crosses, if yes, enable next button
        if($crosses['data']){
            return 0;
        }
        else{
            return 1;
        }   
    }

    /**
     * Export germplasm atrribute CSV file
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionExportGermplasmAttr($program, $id){
        $params = [];
        $experimentId = $_GET['experimentId'] ?? 0;
        $defaultHeaders = ['GERMPLASM_NAME', 'GERMPLASM_CODE'];
        $additionalHeaders = [];
        $variableInfo = [];
        $occurrenceCode = '';

        // get germplasm attributes config
        $config = $this->crossModel->getGermplasmAttributesConfig();

        $storedParams = Yii::$app->session->get('cm_'.$_GET['role'].'_entry_ids_params');
        if(isset($storedParams['parentRole'])){
            $params['parentRole'] = $storedParams['parentRole'];
        }

        $csvContent = [[]];
        $i = 1;

        //fill variableInfo array from config
        if ($config) {
            foreach($config as $abbrev) {
                if (!in_array($abbrev, $defaultHeaders)) {
                    // abbrev to camel case
                    $column = str_replace('_','', lcfirst(ucwords(strtolower($abbrev),'_')));
                    array_push($additionalHeaders, $abbrev);

                    $variableInfo[$abbrev] = $column;
                }
            }
        }

        if($experimentId > 0){
            $params['experimentDbId'] = "equals $experimentId";
            // For Cross Lists with experiments (ICN): change parentRole to entryRole to prevent query error
            unset($params['parentRole']);
            $params['entryRole'] = $storedParams['parentRole'];
            $params['retrievePIseedDbId'] = true;
            
            // separate processing since Occurrence Code is not returned as part of searchEntries
            if (in_array('OCCURRENCE_CODE', $additionalHeaders)) {
                // Get occurrence data and map to each Entry
                $occurrenceParams['experimentDbId'] = "equals $experimentId";
                $occurrenceFilters = '&limit=1';
                $occurrenceResponse = $this->occurrence->searchAll($occurrenceParams, $occurrenceFilters, retrieveAll: false);
            
                $occurrenceCode = $occurrenceResponse['data'][0]['occurrenceCode'] ?? '';
            }

            $germplasm = $this->parentModel->searchEntries($experimentId, $params, retrieveAll:true)['data'] ?? [];
        }else{
            $germplasm = $this->parentModel->searchParents($id, params: $params, retrieveAll:true)['data'] ?? [];
        }
        
        $germplasmIdList = array_column($germplasm, 'germplasmDbId');

        $formattedData = $this->parentModel->getGermplasmAtrrData($germplasmIdList, $config);
        $attrList = array_keys($formattedData['attrList']);

        // if there are additional columns: such as ENTRY_CODE and/or OCCURRENCE_CODE
        if ($additionalHeaders) {
            $nonAttributeData = [];
            
            // Identify columns that are not in attribute list
            $additionalHeaders = array_diff($additionalHeaders, $attrList);

            // Map Values for additional columns
            foreach($germplasm as $data) {
                $index = $data['germplasmDbId'];
            
                foreach($additionalHeaders as $key => $abbrev) {
                    $columnName = $variableInfo[$abbrev] ?? '';
                    $value = $data[$columnName] ?? '';
                    $nonAttributeData[$index][$abbrev] = $value;
                    
                    if ($value == '') {
                        // ICN: override for occurrenceCode
                        if ($experimentId > 0 && $abbrev == 'OCCURRENCE_CODE') {
                            $nonAttributeData[$index][$abbrev] = $occurrenceCode;
                        } else {
                            // value is not found in endpoint: unset abbrev from additionalHeaders
                            unset($additionalHeaders[$key]);
                        }
                    } 
                }
            }

            // Merge additional columns
            $defaultHeaders = array_merge($defaultHeaders, $additionalHeaders);
        }

        $csvHeaders = array_merge($defaultHeaders, $attrList);

        if (!empty($formattedData['germplasmAttrData'])) {
            foreach ($csvHeaders as $value)
                array_push($csvContent[0], $value);

            foreach ($formattedData['germplasmAttrData'] as $key => $value) {
                $csvContent[$i] = [];

                foreach ($csvHeaders as $v){
                    if (in_array($v, $additionalHeaders) && !in_array($v, $attrList)) {
                        $finalValue = $nonAttributeData[$key][$v] ?? null;
                    } else {
                        $finalValue = $value[$v] ?? null;
                    }
                    array_push($csvContent[$i], $finalValue);
                }
                $i += 1;
            }
        }

        // get file name from config by occurrence ID
        $fileName = 'Germplasm_Attributes('.$_GET['role'].')_'.$_GET['entryListName'].'.csv';

        header("Content-Disposition: attachment; filename={$fileName}; Content-Type: application/csv;");

        $fp = fopen('php://output', 'wb');

        if ($fp === false) die('Failed to create CSV file.');

        foreach ($csvContent as $line) fputcsv($fp, $line);

        fclose($fp);

        exit;
    }
}
