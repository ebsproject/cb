<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\controllers;

use app\components\B4RController;

// Import Interfaces
use app\interfaces\models\IConfig;
use app\interfaces\models\IEntryList;

// Import Models
use app\modules\crossManager\models\CrossModel;
use app\modules\crossManager\models\ParentModel;
use Yii;


/**
 * Review controller for the `crossManager` module
 */
class ReviewController extends B4RController
{
    public function __construct($id, $module,
        public IEntryList $entryList,
        public IConfig $configuration,
        public ParentModel $parentModel,
        public CrossModel $crossModel,

        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for review tab
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionLoad($program, $id){

        Yii::info('Rendering Review tab ' . json_encode(['program'=>$program, 'entryListId'=>$id]), __METHOD__);

        //checker to entryList
        $entryListInfo = $this->entryList->searchAll([
            'entryListDbId' => "equals $id"
        ]);

        //if id does not exist, display flash message and go to CM landing page
        if($entryListInfo['totalCount'] == 0 || empty($entryListInfo['data'][0])){
            Yii::warning('Missing entry list data ' . json_encode(['entryListDbId' => $id]), __METHOD__);
            Yii::$app->response->redirect(['crossManager', 'program' => $program, 'error' => "Cross list with id no. $id does not exist."]);
        }

        $entryListStatus = $entryListInfo['data'][0]['entryListStatus'];
        $entryListName = $entryListInfo['data'][0]['entryListName'] ?? '';
        $experimentDbId = $entryListInfo['data'][0]['experimentDbId'] ?? 0;

        // If entry list is already finalized, go to CM landing page
        if ($entryListStatus === 'finalized') {
            Yii::$app->response->redirect(['crossManager',
                'program' => $program,
                'error' => 'Cannot be updated. ' . ucfirst($entryListName). ' has already been finalized. ']);
        }

        //fetch basic info configuration
        $config = $this->configuration->getConfigByAbbrev('CM_CROSS_LIST_BASIC_INFO')['Values'] ?? [];
        $config[] = ['field_label' => 'Experiment Type', 'target_value' => 'experimentType'];

        //get values and filters
        $request = Yii::$app->request;
        $filters = $this->crossModel->getFilters($request->get(), 'cm-review-cross-grid_', 'page');
        $params = $this->crossModel->getParams($id, []);
        $params['retrieveDataFromPI'] = true;
        
        $crossListDataProvider = $this->crossModel->search($params, $filters);

        $reviewTabDetails = [
            'config' => $config,
            'entryListInfo' => $entryListInfo['data'][0] ?? [],
            'entryListName' => ucfirst($entryListName),
            'entryListId' => $id,
            'program' => $program,
            'experimentDbId' => $experimentDbId,
            'crossListDataProvider' => $crossListDataProvider,
            'entryListStatus' => $entryListInfo['data'][0]['entryListStatus'] ?? ''
        ];

        if($experimentDbId == 0){
            $filters = $this->parentModel->getFilters($request->get(), 'cm-review-parent-grid_', 'parent-page');
            $reviewTabDetails['parentListDataProvider'] = $this->parentModel->search($id, '', $filters, 'parent');
        } else {
            $params['fields'] = "entry.id AS entryDbId | entry.entry_code AS entryCode | entry.entry_number AS entryNumber | entry.entry_name AS entryName | entry.entry_type AS parentType | entry.entry_role AS parentRole | entry.description AS description | entry.entry_list_id AS entryListDbId | experiment.id AS experimentDbId | entry.seed_id AS seedDbId | seed.seed_code AS seedCode | seed.seed_name AS seedName | entry.germplasm_id AS germplasmDbId | germplasm.germplasm_code AS germplasmCode | germplasm.designation AS designation | germplasm.parentage AS parentage | germplasm.generation AS generation | germplasm.germplasm_state AS germplasmState | package.id AS packageDbId | package.package_code AS packageCode | package.package_label AS packageLabel | package.package_unit AS packageUnit | entry.creation_timestamp AS creationTimestamp | creator.id AS creatorDbId | creator.person_name AS creator | entry.modification_timestamp AS modificationTimestamp | modifier.id AS modifierDbId | modifier.person_name AS modifier";
            $filters = $this->parentModel->getFilters($request->get(), 'cm-review-parent-grid_', 'parent-page', '&sort=entryDbId');
            $reviewTabDetails['parentListDataProvider'] = $this->parentModel->search($id, $params, $filters, 'parent', $experimentDbId);
        }

        return $this->render('/review', $reviewTabDetails);
    }

    /**
     * Finalize the cross list
     */
    public function actionFinalize(){
        $entryListDbId = $_POST['entryListDbId'] ?? 0;

        $update = $this->entryList->updateOne($entryListDbId, [
            'entryListStatus' => 'finalized'
        ]);
        
        if (isset($update['success']) && $update['success'] === true) {
            $entryListInfo = $this->entryList->searchAll([
                'entryListDbId' => "equals $entryListDbId"
            ]);
            $entryListName = $entryListInfo['data'][0]['entryListName'] ?? '';

            Yii::$app->session->setFlash("success", "<i class='fa fa-check'></i> $entryListName has been successfully finalized.");
        }

        return json_encode(['success' => $update['success'], 'message' => $update['message']]);
    }

}