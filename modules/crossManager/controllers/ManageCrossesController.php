<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\controllers;

use app\components\B4RController;

// Import Interfaces
use app\interfaces\models\ICrop;
use app\interfaces\models\IEntryList;

// Import Models
use app\models\Crop;
use app\models\CrossData;
use app\modules\crossManager\models\CrossListModel;
use app\modules\crossManager\models\CrossModel;
use app\interfaces\models\IWorker;

use Yii;

/**
 * ManageCrosses controller for the `crossManager` module
 */
class ManageCrossesController extends B4RController
{
    public function __construct($id, $module,
        public IEntryList $entryList,
        public ICrop $crop,
        public CrossData $crossData,
        public CrossListModel $crossListModel,
        public CrossModel $crossModel,
        public IWorker $worker,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for manage crosses tab
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionLoad($program, $id){
        //params set to null
        $params = [];

        //checker to entryList
        $response = $this->entryList->searchAll([
            'entryListDbId' => "equals $id",
            'fields' => 'entryList.id AS entryListDbId |'.
                'entryList.entry_list_status AS entryListStatus | '.
                'entryList.entry_list_name AS entryListName |'.
                'entryList.crop_id AS cropDbId |'.
                'entryList.experiment_id AS experimentDbId |'.
                'entryList.entry_list_status AS entryListStatus'
        ]);

        //if id does not exist, display flash message and go to CM landing page
        if($response['totalCount'] == 0 || empty($response['data'][0])){
            Yii::$app->response->redirect(['crossManager', 'program' => $program, 'error' => "Cross list with id no. $id does not exist."]);
        }
        $entryListInfo = $response['data'][0];

        $entryListStatus = $entryListInfo['entryListStatus'];
        $entryListName = $entryListInfo['entryListName'];

        // If entry list is already finalized, go to CM landing page
        if ($entryListStatus === 'finalized') {
            Yii::$app->response->redirect(['crossManager',
                'program' => $program,
                'error' => 'Cannot be updated. ' . ucfirst($entryListName). ' has already been finalized. ']);
        }

        //get value of column filter in array and set value to params for filter
        if(isset($_GET['CrossModel'])){
            $params = array_filter($_GET['CrossModel'], 'strlen');
        }

        //set default value params
        $params = $this->crossModel->getParams($id, $params);
        $params['retrieveDataFromPI'] = true;

        //get values and filters
        $request = Yii::$app->request;
        $filters = $this->crossModel->getFilters($request->get(),'page');
        $dataProvider = $this->crossModel->search($params, $filters);

        $cropInfo = $this->crop->getCrop($entryListInfo['cropDbId']);
        $methodOptions = $this->crossModel->getValidCrossMethod($cropInfo[0]['cropCode']);

        //return to views to render values
        return $this->render('/manage_crosses', [
            'entryListId' => $id,
            'entryListName' => $entryListInfo['entryListName'] ?? '',
            'program' => $program,
            'crossModel' => $this->crossModel,
            'dataProvider' => $dataProvider,
            'methodOptions' => $methodOptions,
            'experimentDbId' => $entryListInfo['experimentDbId'] ?? 0,
            'entryListStatus' => $entryListInfo['entryListStatus'] ?? 0
        ]);
    }

    /**
     * Update cross information(role, type, and description)
     */
    public function actionUpdateCross(){
        extract($_POST);
        //if column edit to crossRemarks, set value to remarks for DB
        $column = $column == "crossRemarks" ? "remarks" : $column; 
        
        return json_encode($this->crossModel->updateOne($crossId, [
            $column => $value
        ]));
    }

    /**
     * Check required fields in crosses' browser
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionCheckRequiredFields($program, $id){
        $requiredFields = $_POST['requiredFields'] ?? [];
        $_POST['entryListDbId'] = $POST['entryListDbId'] ?? $id;
        $filter['entryListDbId'] = 'equals '.$_POST['entryListDbId'] ?? 0;
        $filter['fields'] = 'germplasmCross.id AS crossDbId | germplasmCross.entry_list_id AS entryListDbId | germplasmCross.cross_method AS crossMethod';
        foreach($requiredFields as $field){
            $filter[$field] = 'is null';
        }

        //search all crosses in this entry list; filtered to one and only crossDbId
        $data = $this->crossModel->searchAll([
            'entryListDbId' => 'equals '.$id,
            'fields' => 'germplasmCross.id AS crossDbId | germplasmCross.entry_list_id AS entryListDbId'
        ], 'limit=1&sort=crossDbId:asc', false);

        //check if all cross list have methods
        $checker = $this->crossModel->searchAll($filter,'limit=1&sort=crossDbId:asc', false)['totalCount'];

        if($checker == 0 && $data['totalCount']){
            $update = $this->entryList->updateOne($id, [
                "entryListStatus" => "cross list specified"
            ]);
            
            return 0;
        }

        //if the data is empty, update status to parent list specified
        else if(!$data['totalCount'])
        {
            $update = $this->entryList->updateOne($id, [
                "entryListStatus" => "parent list specified"
            ]);

            return 1;
        }

        //if above is not met, update status to crosses added
        else{
            $update = $this->entryList->updateOne($id, [
                "entryListStatus" => "crosses added"
            ]);      

            return 1;
        }
    }

    /**
     * Delete crosses
     * @param String $program for program code
     */
    public function actionDeleteCross($program) {
        // get threshold config
        $threshold = !empty(Yii::$app->config->getAppThreshold('CM', 'deleteCrosses')) ? Yii::$app->config->getAppThreshold('CM', 'deleteCrosses') : 1000;

        // if all, rerieve all not_harvest and without cross data
        extract($_POST);
        $selectionMode = $_POST['selection'] ?? '';
        $entryListDbId = $_POST['entryListId'] ?? 0;
        $crossIds = isset($_POST['crossIds']) ? json_decode($_POST['crossIds'], true) : [];

        if($selectionMode == 'all'){
            //retrieve crosses
            $crosses = $this->crossModel->searchAll([
                'fields' => 'germplasmCross.id AS crossDbId | germplasmCross.entry_list_id AS entryListDbId| germplasmCross.harvest_status AS harvestStatus',
                'harvestStatus' => 'not equals COMPLETED',
                'entryListDbId' => "equals $entryListDbId"
            ])['data'] ?? [];
            
            $crossIds = array_column($crosses, 'crossDbId');
        }

        if(!empty($crossIds)){
            // check committed data
            $crossData = $this->crossData->searchAll([
                'crossDbId' => 'equals '. implode('|equals ', $crossIds),
                "distinctOn" => "crossDbId",
            ])['data'] ?? [];

            $crossIdList = array_flip($crossIds);
            foreach($crossData as $value){
                unset($crossIdList[$value['crossDbId']]);
            }
            $crossIds = array_keys($crossIdList);
            if(empty($crossIds)){
                return json_encode(['success' => false, 'message' => Yii::t('app', 'Crosses cannot be deleted since the trait data has already been collected.')]);
            }
        }
        else{
            $message = 'Crosses cannot be deleted since they are all harvested.';
        }

        if(count($crossIds) < $threshold) {
            $delete = $this->crossModel->deleteCrosses($crossIds);

            if($delete['success']){
                $this->crossListModel->updateCrossCount($entryListDbId);
                Yii::info('Successfully deleted cross records '.json_encode(['entryListDbId'=>$entryListDbId,
                    'count'=>$delete['totalCount']]),__METHOD__);
                $message = Yii::t('app', $delete['totalCount']. ' item(s) deleted');
            } else {
                Yii::warning(substr('Failed deleting cross records '.json_encode(['entryListDbId'=>$entryListDbId,
                    'crossRecords'=>$crossIds]), 0, length: 1000), __METHOD__);
                return false;
            }

            return json_encode(['success' => $delete['success'], 'message' => Yii::t('app', $message)]);
        } else {
            // start worker
            $response = $this->worker->invoke(
                'DeleteCrossListRecords',
                "Delete records for entry list ID: {$entryListDbId}",
                'CROSS',
                $entryListDbId,
                'CROSS',
                'CROSS_MANAGER',
                'POST',
                [
                    'entryListDbId' => $entryListDbId,
                    'processName' => 'cross-record',
                    'crossDbIds' => $crossIds
                ]
            );

            // Prepare flash message
            if ($response['success']) {
                $type = 'info';
                $message = '<i class="fa fa-info-circle"></i>&nbsp;&nbsp;
                    The cross list records are being deleted in the background. 
                    You may proceed with other tasks. You will be notified in this page once done.';
            } else {
                $type = 'warning';
                $message = '<i class="fa-fw fa fa-warning"></i>&nbsp;&nbsp;
                    A problem occurred while starting the background process. 
                    Please try again at a later time or 
                    <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
            }

            // Set flash
            Yii::$app->session->setFlash($type, $message);

            return json_encode(['success' => true, 'message' => Yii::t('app', 'A background process has been created')]);
        }
    }

    /**
     * Check if crosses table is empty
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionCheckEmptyTable($program, $id){
        //search all crosses in this entry list with only crossDbId
        $data = $this->crossModel->searchAll([
            'entryListDbId' => 'equals '.$id,
            'fields' => 'germplasmCross.id AS crossDbId | germplasmCross.entry_list_id AS entryListDbId'
        ], 'limit=1&sort=crossDbId:asc', false);

        //if crosses were empty return message
        if(!$data['totalCount'])
        {      
            return json_encode(['success' => false, 'message' => Yii::t('app', 'No crosses found.')]);
        }
        //if crosses are not empty return
        else{
            return json_encode(['success' => true]);
        }
    }

    /**
     * Set bulk update modal body
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionModalBody($program, $id){
        //get config
        // $config = $this->retrieveConfiguration();
        $fields = [];

        $crossForm = "/crossManager/manage-crosses/render-config-form";

        $fields = [
            'crossMethod' => 'CROSS_METHOD',
            'crossRemarks' => 'REMARKS',
        ];

        $htmlData = $this->renderAjax('/bulk_update_form.php',[
            'data' => $fields,
            'id' => $id,
            'program' => $program,
            'parentForm' => $crossForm,
        ], true, false);

        return json_encode($htmlData);

    }

    /**
     * Set retrieve configuration based on chosen variable
     * 
     * @param Integer $id for entry list identifier
     * @param String $program for program code
     */
    public function actionRenderConfigForm($id, $program = null){

        if(isset($_POST)){
            $variableAbbrev = isset($_POST['selected']) ? strtoupper($_POST['selected']) : '';
            $data = [];

            if($variableAbbrev == "CROSSMETHOD"){
                $variable = "Cross Method";
                $response = $this->entryList->searchAll([
                    'entryListDbId' => "equals $id",
                    'fields' => 'entryList.id AS entryListDbId | entryList.crop_id AS cropDbId'
                ]);
    
                $entryListInfo = $response['data'][0];
                $cropInfo = $this->crop->getCrop($entryListInfo['cropDbId']);
                $data = $this->crossModel->getValidCrossMethod($cropInfo[0]['cropCode']);
                //remove selfing method
                $data = array_filter(
                    $data,
                    function($data){
                        return $data != "selfing";
                    }
                );

                // render Ajax
                $htmlData = $this->renderAjax('/cross_bulk_update_value.php',[
                    'data' => $data,
                    'variable' => $variable,
                ]);

                return json_encode($htmlData);
            }
            else{
                $variable = "Cross Remarks";
                // render Ajax
                $htmlData = $this->renderAjax('/cross_bulk_update_value.php',[
                    'variable' => $variable
                ]);
                return json_encode($htmlData);
            }
        }
    }

    /**
     * Update all crosses 
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionUpdateCrossAll($program, $id){
        $column = $_POST['column'] ?? [];
        $value = $_POST['value'] ?? [];
        $crosses = [];

        $checker = $this->crossModel->searchAll([
            'entryListDbId' => "equals $id",
            'fields' => 'germplasmCross.id AS crossDbId | germplasmCross.entry_list_id AS entryListDbId'], 
            null, 'limit=1&sort=crossDbId:asc', false
        )['totalCount'];

        //check if crosses were empty
        if(empty($checker)){
            return json_encode(['success' => false, 'message' => Yii::t('app', 'No crosses found.')]);   
        }

        if($column=="crossRemarks"){
            $column = "remarks";
            //search crosses and filter selfing
            $crosses = $this->crossModel->searchAll([
                'entryListDbId' => "equals $id",
                'fields' => 'germplasmCross.id AS crossDbId |'.
                            'germplasmCross.entry_list_id AS entryListDbId |'.
                            'germplasmCross.remarks AS crossRemarks'
            ])['data'];
        }
        else{
            //search crosses and filter selfing
            $crosses = $this->crossModel->searchAll([
                'crossMethod' => 'not equals selfing',
                'entryListDbId' => "equals $id",
                'fields' => 'germplasmCross.id AS crossDbId |'.
                            'germplasmCross.entry_list_id AS entryListDbId |'.
                            'germplasmCross.cross_method AS crossMethod'
            ])['data'];

            // Empty means all crosses are selfing
            if (empty($crosses)) {
                return json_encode([
                    'success' => false,
                    'message' => Yii::t('app', 'Updating of cross method for selfing is not allowed.')
                ]);
            }
        }

        $crossIds = array_column($crosses, 'crossDbId');

        $update = $this->crossModel->updateMany($crossIds, [
            $column => $value
        ]);

        if($update['success']){
            $message = Yii::t('app', 'All crosses updated');
            return json_encode(['success' => $update['success'], 'message' => Yii::t('app', $message)]);
        }
        else{
            return false;
        }
    }

    /**
     * Update selected crosses 
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionUpdateCrossSelected($program, $id){
        extract($_POST);
        $column = $_POST['column'] ?? [];
        $value = $_POST['value'] ?? [];
        $crossId = $_POST['crossIds'] ?? [];
        $crosses = [];

        $checker = $this->crossModel->searchAll([
            'entryListDbId' => "equals $id",
            'fields' => 'germplasmCross.id AS crossDbId | germplasmCross.entry_list_id AS entryListDbId'],
            null, 'limit=1&sort=crossDbId:asc', false
        )['totalCount'];
        //check if crosses were empty
        if(empty($checker)){
            return json_encode(['success' => false, 'message' => Yii::t('app', 'No crosses found.')]); 
        }

        // Search crosses and filter selfing
        if($column=="crossRemarks"){
            $column = "remarks";
            $crosses = $this->crossModel->searchAll([
                'crossDbId' => 'equals '. implode('|equals ', $crossIds),
                'entryListDbId' => "equals $id",
                'fields' => 'germplasmCross.id AS crossDbId |'.
                            'germplasmCross.entry_list_id AS entryListDbId |'.
                            'germplasmCross.remarks AS crossRemarks'
            ])['data'];
        }
        else{
            $crosses = $this->crossModel->searchAll([
                'crossDbId' => 'equals '. implode('|equals ', $crossIds),
                'crossMethod' => 'not equals selfing',
                'entryListDbId' => "equals $id",
                'fields' => 'germplasmCross.id AS crossDbId |'.
                            'germplasmCross.entry_list_id AS entryListDbId |'.
                            'germplasmCross.cross_method AS crossMethod'
            ])['data'];    
        }

        // If the crosses are not empty but all selfing method
        $crossIds = array_column($crosses, 'crossDbId');
        if (empty($crossIds)) {
            return json_encode([
                'success' => false,
                'message' => Yii::t('app', 'Updating of cross method for selfing is not allowed.')
            ]);
        }
        // Update
        $update = $this->crossModel->updateMany($crossIds, [
            $column => $value
        ]);

        if($update['success']){
            $message = Yii::t('app', 'All crosses updated');
            return json_encode(['success' => $update['success'], 'message' => Yii::t('app', $message)]);
        }
        else{
            return false;
        }
    }

    /**
     * Retrieves the values for the filter columns of the entry list browser.
     * 
     * @param String $program for program code
     * @param integer $id entry list identifier
     * 
     * @return json data for the Select2 widget
     */
    public function actionGetFilterData($program, $id) {

        // Take value of existing crossMethod
        $response = $this->crossModel->searchAll([
            "entryListDbId" => "equals $id",
            "distinctOn" => "crossMethod"
        ]);

        $data = $response['data'] ?? [];

        // Add (not set) option
        $items = [
            [
                'id' => '(not set)',
                'text' => '(not set)'
            ]
        ];

        // Set value based on call
        foreach($data as $d) {
            $items[] = [
                'id' => $d['crossMethod'],
                'text' => $d['crossMethod']
            ];
        }
        
        $output['items'] = $items;
        return json_encode($output);
    }
}