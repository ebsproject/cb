<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\controllers;

use app\components\B4RController;

use app\interfaces\controllers\IDashboard;
use app\interfaces\models\ICross;
use app\interfaces\models\IEntryList;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IWorker;

// Import Models
use app\models\BackgroundJob;
use app\models\CrossData;
use app\models\User;
use app\models\Config;
use app\models\Api;
use app\modules\occurrence\models\FileExport;
use app\modules\crossManager\models\CrossListModel;
use app\modules\crossManager\models\CrossModel;
use app\modules\crossManager\models\CrossParentModel;
use app\modules\crossManager\models\ParentModel;
use app\modules\crossManager\models\CrossBackgroundJob;
use Yii;

/**
 * Default controller for the `crossManager` module
 */
class DefaultController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        protected Api $api,
        public IWorker $worker,
        public BackgroundJob $backgroundJob,
        public IDashboard $dashboard,
        public IEntryList $entryList,
        public CrossData $crossData,
        public CrossListModel $crossListModel,
        public ICross $cross,
        public IOccurrence $occurrence,
        public CrossModel $crossModel,
        public CrossParentModel $crossParentModel,
        public CrossBackgroundJob $crossBackgroundJob,
        public ParentModel $parentModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($program){
        Yii::info('Loading Cross Manager main page '.json_encode(['program'=>$program]), __METHOD__);

        if(isset($_GET['error'])){
            Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-times"></i>' . \Yii::t('app', $_GET['error']));
        }else if(isset($_GET['success'])){
            Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> '. \Yii::t('app', $_GET['success']));
        }

        // Get user-selected cross lists from session
        $userId = (new User())->getUserId();
        $selectedEntryListName = "cm_$userId"."_selected_entry_list_ids";
        // Reset selection on page load, except when browsing across pages
        if (!isset($_GET['_pjax']) && !isset($_GET['dynagrid-cm-cross-list-browser-page'])) {
            Yii::$app->session->set($selectedEntryListName, []);
        }
        $selectedEntryListItems = Yii::$app->session->get($selectedEntryListName, []);

        // set search parameters and filters
        $request = Yii::$app->request;
        $filters = $this->crossListModel->getFilters($request->get());

        if(isset($_GET['CrossListModel'])){
            $params = array_filter($_GET['CrossListModel'], 'strlen');
        }

        $dashboardFilters = (array) $this->dashboard->getFilters();

        if(isset($dashboardFilters['year'])){
            $params['experimentYear'] = implode('|', $dashboardFilters['year']);
        }
        if(isset($dashboardFilters['season_id'])){
            $params['seasonDbId'] = implode('|', $dashboardFilters['season_id']);
        }
        if(isset($dashboardFilters['site_id'])){
            $params['siteDbId'] = implode('|', $dashboardFilters['site_id']);
        }
        if(isset($dashboardFilters['program_id'])){
            $params['programDbId'] = $dashboardFilters['program_id'].'';
        }
        if(isset($dashboardFilters['owned']) && $dashboardFilters['owned'] == 'true'){
            $params['creatorDbId'] = "equals $userId";
        }

        $dataProvider = $this->crossListModel->search($params, $filters);

        $configModel = new Config();
        $crossOtherFilesConfig = $configModel->getconfigByAbbrev("DC_PROGRAM_".$program."_OTHER_CROSS_FILES_SETTINGS");
        // GET configuration based on ROLE
        $personModel = Yii::$container->get('app\models\Person');
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);
        
        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $crossOtherFilesConfig = $configModel->getconfigByAbbrev('DC_ROLE_COLLABORATOR_OTHER_CROSS_FILES_SETTINGS');
        } else if (isset($program) && !empty($program) && !empty($crossOtherFilesConfig)) {
            $crossOtherFilesConfig;
        } else { 
            $crossOtherFilesConfig = $configModel->getconfigByAbbrev('DC_GLOBAL_OTHER_CROSS_FILES_SETTINGS');
        }

        $crossOtherFilesConfig = (isset($crossOtherFilesConfig)) ? array_keys($crossOtherFilesConfig) : [];
        $dataCollectionCrossDropdownParams = [];

        foreach ($crossOtherFilesConfig as $value) {

            $dataCollectionCrossDropdownParams[] = [
                'text' => $value,
                'options' => [
                    'class' => 'data-collection-other-cross-files-btn',
                    'data-hover' => 'hover',
                    'data-file-name' => $value,
                    'title' => "Download the $value file for a single cross"
                ]
            ];
        }
        
        return $this->render('/index',[
            'dataProvider' => $dataProvider,
            'program' => $program,
            'crossListModel' => $this->crossListModel,
            'statusOptions' => $this->crossListModel->getOptions('ENTRY_LIST_STATUS'),
            'selectedEntryListItems' => $selectedEntryListItems,
            'sessionName' => $selectedEntryListName,
            'downloadDataCollectionCrossDropdownItemParams' => $dataCollectionCrossDropdownParams
        ]);
    }

    /**
     * Checks whether a cross list is eligible for deletion
     */
    public function actionCanDeleteList() {
        Yii::debug(json_encode($_POST), __METHOD__);
        extract($_POST);

        if (isset($_POST['entryListDbId'])) {
            $crosses = $this->crossModel->searchAll([
                'fields' => 'germplasmCross.id AS crossDbId |'.
                    'germplasmCross.entry_list_id AS entryListDbId|'.
                    'germplasmCross.harvest_status AS harvestStatus',
                'entryListDbId' => "equals $entryListDbId"
            ])['data'] ?? [];

            // Cannot delete when harvested
            $harvestStatuses = array_column($crosses, 'harvestStatus');
            if (count(array_filter($harvestStatuses, fn($status) => $status == 'COMPLETED')) > 0) {
                return json_encode(false);
            }

            // Cannot delete with committed data
            $crossDbIds = array_column($crosses, 'crossDbId');
            if (!empty($crossDbIds)) {
                $crossData = $this->crossData->searchAll([
                        'crossDbId' => 'equals '. implode('|equals ', $crossDbIds),
                        'distinctOn' => 'crossDbId',
                    ])['data'] ?? [];
                if (!empty($crossData)) {
                    return json_encode(false);
                }
            }

            return json_encode(true);
        }
    }

    /**
     * Deletes a cross list and corresponding records for
     *   . ICN cross lists: cross_parents, crosses
     *   . non-ICN cross lists: cross_parents, crosses, parents, entry_lists
     *
     * Related: `DefaultController::actionDeleteListInBackground`
     */
    public function actionDeleteList() {
        Yii::debug(json_encode($_POST), __METHOD__);
        extract($_POST);

        if (isset($_POST['entryListDbId'])) {

            // Get cross list data
            $entryListDbId = $_POST['entryListDbId'];
            $response = $this->entryList->searchAll(['entryListDbId' => "equals $entryListDbId", 
                'fields' => 'entryList.id AS entryListDbId |'.
                            'entryList.entry_list_name AS entryListName |'.
                            'entryList.experiment_id AS experimentDbId'
            ]);

            if (!$response['success']) { 
                return json_encode(['success' => false]); 
            }

            $crossListName = $response['data'][0]['entryListName'];
            $experimentDbId = $response['data'][0]['experimentDbId'];

            $crosses = $this->crossModel->searchAll([
                'fields' => 'germplasmCross.id AS crossDbId |'.
                    'germplasmCross.entry_list_id AS entryListDbId',
                'entryListDbId' => "equals $entryListDbId"
            ])['data'] ?? [];
            $crossDbIds = array_column($crosses, 'crossDbId');

            // Delete cross parents
            $result = $this->crossParentModel->deleteCrossParentByCrossId($crossDbIds);
            if (isset($result['success'])) {
                if (!$result['success']) { return json_encode(['success' => false]); }
                $crossParentCount = $result['totalCount'];
            }

            // Delete crosses
            $result = $this->crossModel->deleteCrossById($crossDbIds);
            if (isset($result['success'])) {
                if (!$result['success']) { return json_encode(['success' => false]); }
                $crossCount = count($result['data'][0]['successful']);

                // Update cross list state
                $this->crossListModel->updateStatus($entryListDbId,CrossListModel::STATUS_PARENT_LIST_SPECIFIED);
            }

            // Handle non-ICN cross list
            if (empty($experimentDbId)) {

                // Delete parents
                $result = $this->parentModel->deleteParentByEntryListId($entryListDbId);
                if (isset($result['success'])) {
                    if (!$result['success']) { return json_encode(['success' => false]); }
                    $parentCount = count($result['data'][0]['successful']);
                }

                // Delete entry list
                $result = $this->entryList->deleteOne($entryListDbId);
                if (isset($result['success'])) {
                    if (!$result['success']) { return json_encode(['success' => false]); }
                    $entryListCount = count($result['data'][0]);
                }
            }

            // Update cross counter
            $this->crossListModel->updateCrossCount($entryListDbId);

            return json_encode([
                'success' => true,
                'crossListName' => $crossListName,
                'crossParentCount' => $crossParentCount ?? 0,
                'crossCount' => $crossCount ?? 0,
                'parentCount' => $parentCount ?? 0,
                'entryListCount' => $entryListCount ?? 0,
            ]);
        }
    }

    /**
     * Using a background worker, deletes a cross list and corresponding records for
     *   . ICN cross lists: cross_parents, crosses
     *   . non-ICN cross lists: cross_parents, crosses, parents, entry_lists
     *
     * Related: `DefaultController::actionDeleteList`
     */
    public function actionDeleteListInBackground() {
        Yii::debug(json_encode($_POST), __METHOD__);
        extract($_POST);

        if (isset($_POST['entryListDbId'])) {
            $entryListDbId = $_POST['entryListDbId'];

            $response = $this->worker->invoke(
                'DeleteCrossListRecords',
                "Delete records for entry list ID: {$entryListDbId}",
                'ENTRY_LIST',
                $entryListDbId,
                'ENTRY_LIST',
                'CROSS_MANAGER',
                'POST',
                [
                    "entryListDbId" => $entryListDbId
                ]
            );

            // Prepare flash message
            if ($response['success']) {
                $type = 'info';
                $message = '<i class="fa fa-info-circle"></i>&nbsp;&nbsp;
                    The cross list records are being deleted in the background. 
                    You may proceed with other tasks. You will be notified in this page once done.';
            } else {
                $type = 'warning';
                $message = '<i class="fa-fw fa fa-warning"></i>&nbsp;&nbsp;
                    A problem occurred while starting the background process. 
                    Please try again at a later time or 
                    <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
            }
            // Set flash
            Yii::$app->session->setFlash($type, $message);

            return json_encode([
                'success' => $response['success'],
                'crossListName' => $crossListName ?? '',
                'crossParentCount' => $crossParentCount ?? 0,
                'crossCount' => $crossCount ?? 0,
                'parentCount' => $parentCount ?? 0,
                'entryListCount' => $entryListCount ?? 0,
            ]);
        }
    }

    /**
     * Stores/Removes selected items in the grid to/from session
     * TODO: Handle storing of all items in a cross list given $filters and $params (see ::actionIndex)
     */
    public function actionStoreSessionItems() {
        Yii::debug(json_encode($_POST), __METHOD__);
        extract($_POST);

        $sessionName = $sessionName ?? '';
        $userId = (new User())->getUserId();
        $selectedIdName = "cm_$userId"."_selected_$sessionName";

        $items = Yii::$app->session->get($selectedIdName, []);

        // Handle selection from cross list browser, where $key=entryListDbId
        if ($sessionName === 'entry_list_ids') {
            if ($unset === 'true') {
                if ($all === 'true') {
                    // Remove all items
                    $items = [];
                } else if (isset($key)) {
                    // Remove 1 item
                    unset($items[$key]);
                }
            } else {
                if (isset($key) && !isset($items[$key])) {
                    // Store 1 item
                    $items[$key] = $value;
                }
            }
        }

        Yii::$app->session->set($selectedIdName, $items);
        $count = count($items);

        return json_encode($count);
    }

    /**
     * Gets the number of currently selected items in session
     */
    public function actionGetUserSelectionCount() {
        extract($_POST);

        $sessionName = $sessionName ?? '';
        $userId = (new User())->getUserId();
        $selectedIdName = "dynagrid-cm_$userId"."_selected_$sessionName";

        $items = Yii::$app->session->get($selectedIdName, []);
        $result = count($items);

        return json_encode($result);
    }

    /**
     * Returns the occurrence list
     */
    public function actionGetOccurrences(){
        $crossListIdList = $_POST['crossListId'] ?? [];
        $crossListData = $this->cross->searchAll([
            'distinctOn' => 'occurrenceDbId',
            'entryListDbId' => 'equals '.implode('|equals ', $crossListIdList),
        ])['data'] ?? [];

        $occurrenceIdList = array_column($crossListData, 'occurrenceDbId');

        if(empty($occurrenceIdList)){
            return json_encode($occurrenceIdList);
        }

        // Set parameters
        $params['occurrenceDbId'] = 'equals '.implode('|equals ', $occurrenceIdList);
        $params['fields'] = 'occurrence.id AS occurrenceDbId|occurrence.occurrence_status AS occurrenceStatus|occurrence.occurrence_name AS occurrenceName';

        if(isset($_POST['requiredStatus'])){
            $params['occurrenceStatus'] = $_POST['requiredStatus'];
        }

        $occurrenceData = $this->occurrence->searchAll($params)['data'] ?? [];

        // Format data for select2
        $occurrenceOptions = [];
        foreach($occurrenceData as $data) {
            $id = $data['occurrenceDbId'];
            $text = $data['occurrenceName'];
            $occurrenceOptions[$id] = $text;
        }

        return json_encode($occurrenceOptions);
    }

    /**
     * Exporting of CSV files for DC Other Cross Files
     * @param string|int $entryListDbId Unique entry list indentifier
     * @param string $dataCollectionCrossFileName Data Collection file name
     * @param int $crossCount total cross count of an cross list
     */
    public function actionExportDataCollectionOtherCrossFiles($entryListDbId, $dataCollectionCrossFileName, $crossCount = 0) {
        $entryListDbId = $_POST['entryListDbId'] ?? $entryListDbId;
        $dataCollectionCrossFileName = $_POST['dataCollectionCrossFileName'] ?? $dataCollectionCrossFileName;
        $crossCount = $_POST['crossCount'] ?? $crossCount;

        $program = Yii::$app->userprogram->get('abbrev');

        $entryListName = $this->api->getApiResults(
            'POST',
            'entry-lists-search',
            json_encode([
                'fields' => 'entryList.id AS entryListDbId | entryList.entry_list_name AS entryListName',
                'entryListDbId' => "equals $entryListDbId"
            ]),
            '&limit=1',
            true
        )['data'][0]['entryListName'];
        
        $dataCollectionFileNameAbbrevised = strtoupper(str_replace(' ', '_', $dataCollectionCrossFileName));
        $entryListNameAbbrevised = strtoupper(str_replace(' ', '_', $entryListName));
        $fileName = $entryListNameAbbrevised.'_'.$dataCollectionFileNameAbbrevised.'.csv';

        $configModel = new Config();
        $config = $configModel->getconfigByAbbrev("DC_PROGRAM_".$program."_OTHER_CROSS_FILES_SETTINGS");
        // GET configuration based on ROLE
        $personModel = Yii::$container->get('app\models\Person');
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);
        
        if (!$isAdmin && isset($role) && strtoupper($role) == 'COLLABORATOR'){
            $config = $configModel->getconfigByAbbrev('DC_ROLE_COLLABORATOR_OTHER_CROSS_FILES_SETTINGS');
        } else if (isset($program) && !empty($program) && !empty($config)) {
            $config;
        } else { 
            $config = $configModel->getconfigByAbbrev('DC_GLOBAL_OTHER_CROSS_FILES_SETTINGS');
        }

        if (isset($config[$dataCollectionCrossFileName]['Export']) && !empty($config[$dataCollectionCrossFileName]['Export']) ) {
            $export = $config[$dataCollectionCrossFileName]['Export'];

            $csvAttributes = [];
            $csvHeaders = [];
            $crossAttributes = [];

            foreach ($export as $value) {
                $header = $value['header'];
                $attribute = $value['attribute'];
            
                $csvHeaders[] = $header;
                $csvAttributes[] = $attribute;
                $crossAttributes[$attribute] = $header;
            }
        }

        // temporary threshold
        if ($crossCount > 500) {
            $description = "Preparing $dataCollectionCrossFileName data for downloading";

            $this->worker->invoke(
                'BuildCsvData',
                $description,
                'ENTRY_LIST',
                "".$entryListDbId,
                'ENTRY_LIST',
                'CROSS_MANAGER',
                'GET',
                [
                    'description' => $description,
                    'fileName' => $fileName,
                    'httpMethod' => 'POST',
                    'csvAttributes' => $csvAttributes,
                    'csvHeaders' => $csvHeaders,
                    'url' => "crosses-search",
                    'requestBody' => [
                        'includeParentSource' => true,
                        'entryListDbId' => "".$entryListDbId
                    ]
                ],
                [
                    'remarks' => $fileName
                ],
            );

            return json_encode([
                'success' => true
            ]);
        } else {
            $crosses =  $this->cross->searchAll([
                'entryListDbId' => $entryListDbId,
                'includeParentSource' => true,
            ])['data'] ?? [];

            $csvContent = [[]];

            // Set headers: row[0]
            foreach ($crossAttributes as $value){
                array_push($csvContent[0], $value);
            }

            // Set succeeding rows starting row[1]
            $row = 1;
            foreach ($crosses as $key => $value) {
                $csvContent[$row] = [];
                foreach ($crossAttributes as $k => $v){
                    array_push($csvContent[$row], $value[$k]);
                }
                $row += 1;
            }
            
            // Set headers
            header('Content-Type: application/csv');
            header("Content-Disposition: attachment; filename={$fileName};");
        
            // Write to output stream
            $fp = fopen('php://output', 'w');

            if (!$fp) {
                die('Failed to create CSV file.');
            }

            foreach ($csvContent as $line) {
                fputcsv($fp, $line);
            }
            
            exit;
        }
    }

    /**
     * Download data (CSV or JSON format) from filepath 
     * @param string $filename file name identifier
     * @param string $dataFormat data format identifier
     */
    public function actionDownloadData ($filename, $dataFormat)
    {
        $filename = $_POST['filename'] ?? $filename;
        $filename = str_contains($filename, '.') ? $filename : "$filename.$dataFormat";

        $pathToFile = realpath(Yii::$app->basePath) . "/files/data_export/$filename";

        if (file_exists($pathToFile)) {
            // Set headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($pathToFile));

            // Flush system output buffer
            flush();

            // Read/Download CSV file
            readfile($pathToFile);

            die();
        }
    }

    /**
     * Returns new notifications from background process
     */
    public function actionNewNotificationsCount() {
        $userId = (new User())->getUserId();

        $params = [
            'creatorDbId' => "equals $userId",
            'application' => 'equals CROSS_MANAGER',
            'distinctOn' => 'entityDbId',
            'isSeen' => 'equals false',
        ];
        $filter = 'sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter);

        if ($result['status'] != 200) {
            return json_encode([
                'success' => false,
                'error' => 'Error while retrieving background processes. Kindly contact Administrator.'
            ]);
        }

        return json_encode($result['totalCount']);
    }

    /**
     * Returns push notifications from background process
     */
    public function actionPushNotifications() {
        $userModel = new User();
        $userId = $userModel->getUserId();
        $notifs = $this->crossBackgroundJob->pushNotifications($userId);
        
        return $this->renderAjax(
            'notifications',
            $notifs
        );
    }
}
