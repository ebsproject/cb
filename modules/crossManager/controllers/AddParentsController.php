<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\controllers;

use Yii;

use app\components\B4RController;

//Import Interfaces
use app\interfaces\models\IEntryList;
use app\models\Api;
use app\models\CropProgram;
use app\modules\crossManager\models\CrossModel;
use app\modules\crossManager\models\OccurrenceModel;
use app\modules\crossManager\models\OccurrenceEntryModel;
use app\modules\crossManager\models\ParentModel;


/**
 * AddParents controller for the `crossManager` module
 */
class AddParentsController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public IEntryList $entryList,
        public OccurrenceModel $occurrenceModel,
        public OccurrenceEntryModel $entryModel,
        public ParentModel $parentModel,
        public CrossModel $crossModel,
        public CropProgram $cropProgramModel,
        public Api $api,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Loads the Add Parents tab: Occurrence List page
     * 
     * @param String $program program code
     * @param Integer $id entry list identifier
     */
    public function actionLoad($program, $id){
        Yii::info('Rendering occurrences for Add Parents tab ' . json_encode(['program'=>$program,
            'entryListId'=>$id]), __METHOD__);

        $entryListInfo = $this->entryList->searchAll([
            'entryListDbId' => "equals $id"
        ]);

        //if id does not exist, display flash message and go to CM landing page
        if($entryListInfo['totalCount'] == 0 || empty($entryListInfo['data'][0])){
            Yii::warning('Missing entry list data ' . json_encode(['entryListDbId' => $id]), __METHOD__);
            return Yii::$app->response->redirect(['crossManager', 'program' => $program, 'error' => "Cross list with id no. $id does not exist."]);
        }

        $entryListStatus = $entryListInfo['data'][0]['entryListStatus'];
        $entryListName = $entryListInfo['data'][0]['entryListName'];

        // If entry list is already finalized, go to CM landing page
        if ($entryListStatus === 'finalized') {
            Yii::$app->response->redirect(['crossManager',
                'program' => $program,
                'error' => 'Cannot be updated. ' . ucfirst($entryListName). ' has already been finalized. ']);
        }

        $crossList = $entryListInfo['data'][0];
        //if it is a newly created record
        if(isset($_GET['success']) && $_GET['success'] == 'create'){
            Yii::$app->session->setFlash('success',  '<i class="fa fa-check"></i> ' .
            Yii::t('app', 'Successfully created <strong>').$crossList['entryListName']. '</strong>');
        }

        $params = [];
        if(isset($_GET['OccurrenceModel'])){
            $params = array_filter($_GET['OccurrenceModel'], 'strlen');
        }

        // set search parameters and filters
        $equals ='equals ';

        $conditions = $this->occurrenceModel->getValidCrosses($program, $params['experimentStageCode'] ?? '', $params['experimentType'] ?? '');

        // Check and notify user if there are no stage codes found from config
        if ($conditions == false) {
            $message = Yii::t('app', 'Missing experiment stage code configuration. Please try again or file a report for assistance.');
            Yii::$app->session->setFlash('warning', '<i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> '. $message);
        } else {

            foreach ($conditions as $key => $value) {
                $conditions[$key]['occurrenceStatus'] = 'planted%';
                $conditions[$key]['programDbId'] =  $equals.($crossList['programDbId']);
                $conditions[$key]['experimentSeasonDbId'] = $equals.($crossList['seasonDbId']);
                $conditions[$key]['experimentYear'] = $equals.($crossList['experimentYear']);
                $conditions[$key]['siteDbId'] = $equals.($crossList['siteDbId']);
                foreach($params as $p => $v){
                    if($p !== 'experimentStageCode' && $p !== 'experimentType') $conditions[$key][$p] = $v;
                }
            }

            $params['conditions'] = $conditions;
            $params['fields'] = 'occurrence.id AS occurrenceDbId|experiment.experiment_year AS experimentYear|occurrence.entry_count AS entryCount|program.id AS programDbId|occurrence.occurrence_code AS occurrenceCode|occurrence.occurrence_name AS occurrenceName|program.program_code AS programCode|site.geospatial_object_name AS site|experiment.experiment_code AS experimentCode|experiment.experiment_name AS experiment|experiment.experiment_type AS experimentType|season.id AS experimentSeasonDbId|season.season_code AS experimentSeasonCode|season.season_name AS experimentSeason|stage.stage_code AS experimentStageCode|occurrence.occurrence_status AS occurrenceStatus|site.id AS siteDbId';

        }

        $request = Yii::$app->request;
        $filters = $this->occurrenceModel->getFilters($request->get());

        $dataProvider = $this->occurrenceModel->search($params, $filters, $entryListInfo['data'][0], $conditions);

        return $this->render('/add_parents/occurrence_browser', [
            'program' => $program,
            'entryListId' => $id,
            'entryListName' => $entryListInfo['data'][0]['entryListName'] ?? '',
            'dataProvider' => $dataProvider,
            'occurrenceModel' => $this->occurrenceModel,
            'entryListStatus' => $entryListInfo['data'][0]['entryListStatus'] ?? ''
        ]);
    }

    /**
     * Renders view for the occurrence entry records
     * 
     * @param String $program program code
     * @param Integer $id entry list identifier
     */
    public function actionLoadEntries($program, $id){

        $entryListInfo = $this->entryList->searchAll([
            'entryListDbId' => "equals $id",
            'fields' => 'entryList.id AS entryListDbId | entryList.entry_list_name AS entryListName | entryList.entry_list_status AS entryListStatus'
        ]);

        //if id does not exist, display flash message and go to CM landing page
        if($entryListInfo['totalCount'] == 0 || empty($entryListInfo['data'][0])){
            Yii::$app->response->redirect(['crossManager', 'program' => $program, 'error' => "Cross list with id no. $id does not exist."]);
        }

        //set params and filters
        $request = Yii::$app->request;
        $filters = $this->entryModel->getFilters($request->get());
        $params = null;
        if(isset($_GET['OccurrenceEntryModel'])){
            $params = array_filter($_GET['OccurrenceEntryModel'], 'strlen');
        }
        // Exclude entries without seeds
        // Note: Does NOT filter seeds that are voided/deleted
        $params['seedDbId'] = "is not null";

        $occurrenceId = 0;
        $dataProvider = [];
        if(isset($_GET['occurrenceId'])){
            $occurrenceId = $_GET['occurrenceId'];
            $dataProvider = $this->entryModel->search($occurrenceId, $params, $filters);
        }
        else{// no occurrence id found
            Yii::$app->response->redirect(['crossManager/add-parents/load', 'program' => $program, 'id' => "$id"]);
        }

        return $this->render('/add_parents/entry_browser', [
            'program' => $program,
            'entryListId' => $id,
            'entryListName' => $entryListInfo['data'][0]['entryListName'] ?? '',
            'dataProvider' => $dataProvider,
            'occurrenceEntryModel' => $this->entryModel,
            'occurrenceId' => $occurrenceId,
            'occurrenceName' => $_GET['occurrence'] ?? '',
            'entryListStatus' => $entryListInfo['data'][0]['entryListStatus'] ?? ''
        ]);
    }

    /**
     * Saves parent records
     * 
     * @param String $program program code
     * @param Integer $id entry list identifier
     */
    public function actionSaveParents($program, $id){
        $entryIds = isset($_POST['entryIds']) ? json_decode($_POST['entryIds'], true) : [];
        $selectionMode = $_POST['selectionMode'] ?? '';
        $parentRole = $_POST['parentRole'] ?? '';
        $occurrenceId = $_POST['occurrenceId'] ?? 0;
        
        //set params and filters
        $request = Yii::$app->request;
        $filters = $this->entryModel->getFilters($request->get());
        $params = [];
        if(isset($_GET['OccurrenceEntryModel'])){
            $params = array_filter($_GET['OccurrenceEntryModel'], 'strlen');
        }

        if($selectionMode == 'selected'){
            $params['entryDbId'] = 'equals '. implode('|equals ',$entryIds);
        }

        $result = $this->parentModel->saveParents($id, $occurrenceId, $parentRole, $params, $filters);
        Yii::debug('Created parents '.json_encode($result), __METHOD__);

        if($result['success']){
            $update = $this->entryList->updateOne($id, [
                "entryListStatus" => "parent list specified"
            ]);
        }

        return json_encode([
            'count' => count($result['data'])
        ]);
    }

    /**
     * Returns entries associated with an occurrence
     *
     * @param string $program program code
     */
    public function actionGetOccurrenceEntries($program)
    {
        extract($_POST);
        $occurrenceEntries = [];

        if (isset($occurrenceDbId) && !empty($occurrenceDbId)) {

            // Get search filters from session
            // Skip sort, page, limit filters because we want to retrieve all data
            $params = Yii::$app->session->get(OccurrenceEntryModel::SESSION_KEY_SEARCH)['params'] ?? null;

            // Retrieve data
            $result = $this->entryModel->getOccurrenceEntries($occurrenceDbId, $params, retrieveAll: true);

            if ($idOnly) {
                if ($result['totalCount'] > 0) {
                    $occurrenceEntries = array_column($result['data'], 'entryDbId');
                }
            }
        }

        return json_encode($occurrenceEntries);
    }

    /**
     * Check required fields in manage parents browser
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionCheckRequiredFields($program, $id){
        //search all parents added to this entrylist
        $parents = $this->parentModel->searchParents($id, [
            'fields' => 'parent.id AS parentDbId | parent.entry_list_id AS entryListDbId'], 
            'limit=1', false
        )['data'] ?? [];
        
        //check if there is parents, if yes, enable next button
        if($parents){        
            return 0;
        }
        else{
            return 1;
        }
    }

    /**
     * Export germplasm atrribute CSV file
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionExportGermplasmAttr($program, $id){
        //TODO: Inherit filters and sort param from the main call
        $csvContent = [[]];
        $i = 1;
        $occurrenceId = $_GET['occurrenceId'] ?? 0;

        // get germplasm attributes config
        $config = $this->crossModel->getGermplasmAttributesConfig();

        $germplasm = $this->api->getApiResults(
            'POST',
            "occurrences/$occurrenceId/entries-search",
            json_encode([
                'fields' => 'pi.germplasm_id AS germplasmDbId'
            ]),
            '',
            true
        )['data'] ?? [];

        $germplasmIdList = array_column($germplasm, 'germplasmDbId');

        $formattedData = $this->parentModel->getGermplasmAtrrData($germplasmIdList, $config);

        $csvHeaders = array_merge(['GERMPLASM_NAME', 'GERMPLASM_CODE'], array_keys($formattedData['attrList']));
        if (!empty($formattedData['germplasmAttrData'])) {
            foreach ($csvHeaders as $value)
                array_push($csvContent[0], $value);

            foreach ($formattedData['germplasmAttrData'] as $value) {
                $csvContent[$i] = [];

                foreach ($csvHeaders as $v){
                    $finalValue = $value[$v] ?? null;
                    array_push($csvContent[$i], $finalValue);
                }
                $i += 1;
            }
        }

        // get file name from config by occurrence ID
        $fileName = 'Germplasm_Attributes_'.$_GET['occurrenceName'].'.csv';

        header("Content-Disposition: attachment; filename={$fileName}; Content-Type: application/csv;");

        $fp = fopen('php://output', 'wb');

        if ($fp === false) die('Failed to create CSV file.');

        foreach ($csvContent as $line) fputcsv($fp, $line);

        fclose($fp);

        exit;
    }
}
