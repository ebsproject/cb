<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\modules\crossManager\controllers;

use app\components\B4RController;

// Import interfaces
use app\interfaces\models\IEntryList;

// Import Models
use app\modules\crossManager\models\CrossDataModel;
use Yii;


/**
 * Cross List controller for the `crossManager/view` module
 */
class CrossDataController extends B4RController
{

    public function __construct($id, $module,
        public CrossDataModel $crossDataModel,
        public IEntryList $entryList, 
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

     /**
     * Renders the index view for cross list
     * 
     * @param String $program for program code
     * @param Integer $id for entry list identifier
     */
    public function actionLoad($program, $id){
        //params set to null
        $params = [];

        //checker to entryList
        $response = $this->entryList->searchAll([
            'entryListDbId' => "equals $id",
            'fields' => 'entryList.id AS entryListDbId | entryList.entry_list_name AS entryListName | entryList.experiment_id AS experimentDbId'
        ]);

        //if id does not exist, display flash message and go to CM landing page
        if($response['totalCount'] == 0 || empty($response['data'][0])){
            Yii::$app->response->redirect(['crossManager', 'program' => $program, 'error' => "Cross list with id no. $id does not exist."]);
        }
        $entryListInfo = $response['data'][0] ?? '';

        //get value of column filter in array and set value to params for filter
        if(isset($_GET['CrossDataModel'])){
            $params = array_filter($_GET['CrossDataModel'], 'strlen');
        }
        
        //add retrieve from planting instruction flag
        $params['retrieveDataFromPI'] = true;

        //get values and filters
        $request = Yii::$app->request;
        $filters = $this->crossDataModel->getFilters($request->get());
        $dataProvider = $this->crossDataModel->search($id,$params, $filters);
        $traits = $this->crossDataModel->traits;

        //return to views to render values
        return $this->render('/view/cross_list', [
            'entryListId' => $id,
            'entryListName' => $entryListInfo['entryListName'] ?? '',
            'program' => $program,
            'crossModel' => $this->crossDataModel,
            'traits' => $traits,
            'dataProvider' => $dataProvider,
            'experimentDbId' => $entryListInfo['experimentDbId'] ?? 0,
        ]);
    }
}