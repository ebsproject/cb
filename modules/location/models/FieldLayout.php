<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for Field layout
 */

namespace app\modules\location\models;

use Yii;
use app\interfaces\models\IOccurrence;
use app\models\Config;
use app\models\ExperimentData;
use app\models\LocationOccurrenceGroup;
use app\models\Variable;

use app\modules\experimentCreation\models\ExperimentModel;

class FieldLayout
{
    public function __construct(
        public Config $config,
        public ExperimentData $expData,
        public ExperimentModel $expModel,
        public LocationOccurrenceGroup $locOccGrp,
        public IOccurrence $occurrence,
        public Variable $variable
    ) {}

    /**
    * Get Layout data by Location identifier
    *
    * @param int|string $id Location identifier
    * @param string $type Type of Layout (design, field, etc.)
    */
    public function getLayoutDataByLocationId($id, $type='design'){
        // get occurrences of the Location
        $params = [
            'locationDbId' => "equals $id" 
        ];
        $logs = $this->locOccGrp->searchAll($params, 'sort=occurrenceName');
        $logs = isset($logs['data']) && !empty($logs['data']) ? $logs['data'] : [];

        // if there are occurrences in the Location
        if(!empty($logs)){
            $firstOccurrenceId = isset($logs[0]['occurrenceDbId']) ? $logs[0]['occurrenceDbId'] : 0;

            if($firstOccurrenceId == NULL){
                $occurrenceDbId = $logs[0]['occurrenceDbId'];
                $occurrenceName = $logs[0]['occurrenceName'];
            } else {
                $occurrenceIdx = array_search($firstOccurrenceId, array_column($logs, 'occurrenceDbId'));
                $occurrenceDbId = $logs[$occurrenceIdx]['occurrenceDbId'];
                $occurrenceName = $logs[$occurrenceIdx]['occurrenceName'];
            }

            $plotRecords = $this->occurrence->searchAllPlots($occurrenceDbId, null, 'sort=plotNumber');

            // if there are plot records
            if(isset($plotRecords['totalCount']) > 0 && $plotRecords['totalCount'] > 0){
                $layoutArray = $this->expModel->generateLayout($occurrenceDbId, null, false, $type);

                return [
                    'layout' => $layoutArray,
                    'occurrencesList' => $logs,
                    'occurrenceName' => $occurrenceName,
                    'occurrenceDbId' => $occurrenceDbId
                ];
            }

        }
    }

    /**
    * Get Layout data by Occurrence identifier
    *
    * @param int|string $id Occurrence identifier
    * @param string $type Type of Layout (design, field, etc.)
    */
    public function getLayoutDataByOccurrenceId($id, $type='design'){
        // get occurrences of the Location
        $params = [
            'occurrenceDbId' => "equals $id" 
        ];
        $logs = $this->occurrence->searchAll($params, 'sort=occurrenceName');
        $logs = isset($logs['data']) && !empty($logs['data']) ? $logs['data'] : [];

        $layout = [];
        // if occurrence record exist
        if(!empty($logs)){

            if($id == NULL){
                $occurrenceDbId = $logs[0]['occurrenceDbId'];
                $occurrenceName = $logs[0]['occurrenceName'];
            } else {
                $occurrenceDbId = $logs[0]['occurrenceDbId'];
                $occurrenceName = $logs[0]['occurrenceName'];
            }

            $plotRecords = $this->occurrence->searchAllPlots($occurrenceDbId, null, 'sort=plotNumber');

            // if there are plot records
            if(isset($plotRecords['totalCount']) > 0 && $plotRecords['totalCount'] > 0){
                $layoutArray = $this->expModel->generateLayout($occurrenceDbId, null, false, $type);

                return [
                    'layout' => $layoutArray,
                    'occurrencesList' => $logs,
                    'occurrenceName' => $occurrenceName,
                    'occurrenceDbId' => $occurrenceDbId
                ];
            }
        }
    }

    /**
     * Download layout data
     *
     * @param int $id Occurrence or Location indentifier
     * @param string $level Whether Occurrence or Location level
     * @param string $type Whether Design or Field layout
     * @return array Layout information 
     */
    public function getDownloadLayoutData($id, $level='occurrence', $type='design'){
        $repColors = array();
        $plotLayout = array();
        $xAttribute = 'designX';
        $yAtrribute = 'designY';

        if($type == 'field'){
            $xAttribute = 'fieldX';
            $yAtrribute = 'fieldY';
        }

        // get max row and col of layout
        $plotDesignXMax = $this->occurrence->searchAllPlots($id, null,"sort=$xAttribute:DESC&limit=1", false);
        $plotDesignYMax = $this->occurrence->searchAllPlots($id, null,"sort=$yAtrribute:DESC&limit=1", false);
        $plotRepMax = $this->occurrence->searchAllPlots($id, null,'sort=rep:DESC&limit=1', false);
        $plotBlkMax = $this->occurrence->searchAllPlots($id, null,'sort=blockNumber:DESC&limit=1', false);

        $maxRows = $plotDesignYMax['data'][0]['plots'][0][$yAtrribute];
        $maxCols = $plotDesignXMax['data'][0]['plots'][0][$xAttribute];

        $maxRep = $plotRepMax['data'][0]['plots'][0]['rep'];
        $maxBlk = $plotBlkMax['data'][0]['plots'][0]['blockNumber'];

        return compact('maxRows','maxCols','maxRep','maxBlk','xAttribute','yAtrribute');

    }

    /**
     * Get valid file name for downloading file
     * 
     * @param string $file File name to validate
     * @return string $file Validated file name
     */
    public function getValidFileName($file){
        // Remove anything which isn't a word, whitespace, number
        // or any of the following caracters -_~,;[]().
        // If you don't need to handle multi-byte characters
        // you can use preg_replace rather than mb_ereg_replace
        $file = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $file);
        // Remove any runs of periods
        $file = mb_ereg_replace("([\.]{2,})", '', $file);

        return $file;
    }

    /**
     * Convert number to an excel column letter
     *
     * @param int $c Column number
     * @return string $letter Excel column letter 
     */
    public function columnLetter($c){

        $c = intval($c);
        if ($c <= 0) return '';

        $letter = '';

        while($c != 0){
            $p = ($c - 1) % 26;
            $c = intval(($c - $p) / 26);
            $letter = chr(65 + $p) . $letter;
        }

        return $letter;

    }

    /**
     * Get layout first plot position by Occurrence or Location ID
     *
     * @param int $id Record identifier
     * @param text $entity Whether Occurrence or Location
     * @return boolean $flipped Whether display is flipped or not
     */
    public function getLayoutFirstPlotPosition($id, $entity = 'occurrence'){
        // get experiment ID
        $occurrenceParams = [
            $entity."DbId" => (string) $id
        ];

        $occurrence = $this->occurrence->searchAll($occurrenceParams, 'limit=1&sort=occurrenceDbId:desc', false);

        $experimentDbId = (isset($occurrence['data'][0]['experimentDbId'])) ? $occurrence['data'][0]['experimentDbId'] : 0;

        // load FIRST_PLOT_POSITION_VIEW value in experiment data
        $varAbbrev = 'FIRST_PLOT_POSITION_VIEW';

        $expData = $this->expData->getExperimentData($experimentDbId, ["abbrev" => "equals "."$varAbbrev"]);

        if(isset($expData) && count($expData) > 0 && isset($expData[0]['dataValue'])){
            if($expData[0]['dataValue'] == "Top Left"){
                $flipped = true;
            } else {
                $flipped = false;
            }   
        }else {

            // if no saved in experiment data, get default first plot position in config
            $config = $this->config->getConfigByAbbrev('FIRST_PLOT_POSITION_VIEW_DEFAULT');

            $flipped = (isset($config['value']) && !empty($config['value']) && $config['value'] = 'Bottom Left') ? false : true;

        }

        return $flipped;
    }
}