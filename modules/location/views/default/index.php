<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
use app\components\FavoritesWidget;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;

/**
* Renders locations browser
*/

// check if there is a selected record, if yes, display viewer and add attributes
$viewerClass = 'hidden';
$selectedIdAttrs = '';
$browserId = 'locations-grid';

if(isset($selectedRecordId) && !empty($selectedRecordId)){
    $viewerClass = '';
    $selectedIdAttrs = ' data-id="'. $selectedRecordId . '"'; // ID of selected record
    $selectedIdAttrs .= ' data-model-class="Location"'; // model class to retrieve record info
    $selectedIdAttrs .= ' data-model-class-action="getDetailViewInfo"'; // action in model to retrieve record info 
}

echo 
'<h3>'. \Yii::t('app', 'Locations') . FavoritesWidget::widget([
    'module' => 'location'
]).
'</h3>';

// render search form
echo Yii::$app->controller->renderPartial('@app/widgets/views/browser/search_form.php',[
    'filter'=>$filter,
    'totalCount' => $totalCount,
    'gridMode' => $gridMode,
    'browserId' => $browserId
]);

?>

<div class="row no-margin"></div>
<div class="browser-detail-view-panel">
    <div class="browser-detail-view-container">
        <?= 
            /**
             * Renders categories panel
             * Key - category; the plural of the key is displayed as category header
             * modelClass - Model class where to retrieve the category items
             * modelClassAction - function to retrieve the category items
             */
            Yii::$app->controller->renderPartial('@app/widgets/views/browser/category_panel.php',[
                'categories' => [
                    'experiment' => [
                        'modelClass' => 'Occurrence',
                        'modelClassAction' => 'findExperimentCategories'
                    ]
                ]
            ]); 
        ?>

        <!-- Renders browser of the entity -->
        <div class="browser-detail-view-browser-container">
            <div class="browser-detail-view-container-wrapper">
                <?php
                // render detail view browser
                echo Yii::$app->controller->renderPartial('browser.php',[
                    'program' => $program,
                    'browserId' => $browserId,
                    'dataProvider' => $dataProvider,
                    'filter' => $filter,
                    'page' => 1,
                    'totalCount' => $totalCount,
                    'selectedRecordId' => $selectedRecordId,
                    'gridMode' => $gridMode
                ]);
            ?>
            </div>
        </div>
        
        <!-- Renders view entity panel -->
        <div class="browser-detail-view-viewer <?= $viewerClass ?>" <?=  $selectedIdAttrs ?> >
            <!-- View entity is loaded here via ajax call -->
        </div>
    </div>
</div>
<?php
/**
 * Render search help modal
 *
 * attributes - fields where user can search for a location
 * examples - examples for filtering
 */
echo Yii::$app->controller->renderPartial('@app/widgets/views/browser/search_help.php',[
    'attributes' => [
        'Location name',
        'Location code',
        'Location type',    
        'Site',
        'Geospatial object',
        'Stage',
        'Year',
        'Season',
        'Creator',
        'Status'
    ],
    'examples' => [
        [
            'value' => '2016 wet AYT',
            'description' => 'locations during year 2016 wet season with AYT as stage'
        ],
        [
            'value' => 'f5 2017 dry completed',
            'description' => 'completed experiments during 2017 dry season with F5 as stage'
        ]
    ]
]);

// css
$this->registerCssFile("@web/css/browser/detail-view.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-detail-view');

// generic get category items url
$renderCategoriesUrl = Url::to(['/browser/render-categories']);
// generic view item url
$viewItemUrl = Url::to(['/browser/view-item']);
// load more records url
$loadMoreRecordsUrl = Url::to(['/location/default/load-more']);
// switch browser view url
$switchBrowserViewUrl = Url::to(['/browser/save-view-mode']);

// browser js variables for generic detail view browser; do not change variable names
$this->registerJs(
    "
    var renderCategoriesUrl = '$renderCategoriesUrl';
    var browserId = '$browserId';
    var selectedCategory = '$category';
    var selectedCategoryId = '$selectedCategoryId';
    var viewItemUrl = '$viewItemUrl';
    var selectedRecordId = '$selectedRecordId';
    var loadMoreRecordsUrl = '$loadMoreRecordsUrl';
    var switchBrowserViewUrl = '$switchBrowserViewUrl';
    ",
    \yii\web\View::POS_HEAD
);
$this->registerJsFile("@web/js/browser/detail-view.js", 
    [
        'depends' => ['\yii\web\JqueryAsset'],
        'position' => \yii\web\View::POS_END
    ]
);
