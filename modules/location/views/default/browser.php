<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\dynagrid\DynaGrid;
use app\controllers\BrowserController;
use yii\helpers\Url;
use macgyer\yii2materializecss\widgets\Button;

/**
 * Renders detail view browser of locations
 */

// check whether to show load more button or not
$pages = ($totalCount / 50);
// if first page
$nextPage = 2;
$startPage = 1;
$lastPage = 50;
$loadMoreBtnClass = ($totalCount < 50) ? 'hidden' : '';

// for next pages
if($page != 1) {
    $startPage = (50 * ($page - 1)) + 1;
    $lastPage = ($page > $pages) ? $totalCount : 50 * $page;
    $browserId = $browserId . $page;
    $loadMoreBtnClass = ($page > $pages) ? 'hidden' : '';
    $nextPage = ($page+1);
    
    echo '<div class="browser-detail-view-pager text-muted text-center"><b>' . $startPage . '</b>-<b>' . $lastPage .'</b> items</div>';
}

// if in detail view mode
if($gridMode == 'detail') {

    $gridClass = 'browser-detail-view';
    // Columns for browser detail view
    $columns = [
        [
            'attribute' => 'locationDbId',
            'format' => 'raw',
            'contentOptions' => [
                'class' => 'record-status-td'
            ],
            'value' => function($model) {
                $class = 'grey';
                $status = ucfirst(trim($model['locationStatus']));
                if($status == 'Committed'){
                    $class = 'green darken-3';
                } else if ($status == 'Completed') {
                    $class = 'blue darken-2';
                }

                $statusVal = '<div title="Status: '.$status.'" class="record-status '.$class.'"> </span>';
                return $statusVal;
            }
        ],
        [
            'attribute' => 'locationName',
            'format' => 'raw',
            'value' => function($model) {
                $tooltip = '<span title="';
                $metadata = '';
                
                $location = $model['locationName'];
                $metadata = '<div class="bold browser-detail-view-main-info">' . 
                    '<span title="Name: '. $location .'">' . $location . '</span>' .
                '</div>';

                $site = $model['siteCode'];
                $metadata = $metadata . $tooltip . 'Site: '. $site . '">'. $site .'</span> · ';
                
                $year = $model['year'];
                $metadata =  $metadata . $tooltip . 'Year: '. $year . '">'. $year .'</span> · ';
                
                $season = $model['season'];
                $metadata =  $metadata . $tooltip . 'Season: '. $season . '">'. $season .'</span> · ';
                
                $field = $model['field'];
                $metadata = $metadata . $tooltip . 'Planting area: '. $field . '">'. $field .'</span> · ';
                
                $plotCount = $model['plotCount'];
                $plotCount = $plotCount . ' ' . BrowserController::pluralize($plotCount, 'plot');
                $metadata = $metadata . $tooltip . 'Plot count: '. $plotCount . '">'. $plotCount .'</span> ';

                $moreInfo = '<div class="browser-detail-view-more-info">'.
                    $metadata
                .'</div>';

                return $moreInfo;
            }
        ],
        [
            'attribute' => 'locationType',
            'format' => 'raw',
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'value' => function($model) {
                $rawType = $model['locationType'];
                $typeStrLen = strlen($rawType);
                $type = ($typeStrLen > 13) ? substr($rawType, 0, 13) . '...' : $rawType;
                $type = strtoupper($type);
                
                $rawCode = $model['locationCode'];
                $codeStrLen = strlen($rawCode);
                $code = ($codeStrLen > 14) ? '...' . substr($rawCode, -14) : $rawCode;
                
                $rawCreator = $model['creator'];
                $creator = strtoupper(substr($rawCreator, 0, 1));

                $type = '<span class="bold badge new grey lighten-2 black-text record-type" title="Location type: ' . $rawType . '">' . $type . '</span> ';
                $code = '<span class="record-code" title="Code: '. $rawCode .'">' . $code . '</span>';
                $creator = '<div class="item-theme-color in-circle" title="Creator: '. $rawCreator .'">' . $creator .'</div> ';

                return '<div class="browser-detail-view-basic-metadata">' . $type . $code . $creator . '</div>';
            }
        ],
    ];
} else {

    $gridClass = 'browser-grid-view';
    // Columns for browser grid view
    $columns = [
        [
            'attribute' => 'locationStatus',
            'header' => 'Status',
            'format' => 'raw',
            'value' => function($model) {
                $class = 'grey';
                $status = strtoupper(trim($model['locationStatus']));
                if($status == 'COMMITTED'){
                    $class = 'green darken-3';
                } else if ($status == 'COMPLETED') {
                    $class = 'blue darken-2';
                }

                $statusVal = '<span title="Status: '.$status.'" class="new badge '.$class.'">'. $status .'</span>';
                return $statusVal;
            }
        ],
        [
            'attribute' => 'locationName',
            'label' => 'name'
        ],
        [
            'attribute' => 'locationType',
            'label' => 'Type'
        ],
        [
            'attribute' => 'site'
        ],
        [
            'attribute' => 'year'
        ],
        [
            'attribute' => 'season'
        ],
        [
            'attribute' => 'field',
            'label' => 'Planting area'
        ],
        [
            'attribute' => 'plotCount'
        ]
    ];
}

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => false,
    'showFilter' => true,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId,
        'class' => 'browser-detail-view-inner',
        'dataProvider' => $dataProvider,
        'pjax' => true,
        'responsive' => false,
        'rowOptions' => function($model) use ($program){
            // if selected, add active row class
            $activeRowClass = ( isset($selectedRecordId) &&
                ($selectedRecordId == $model['locationDbId']) ) ?
                'browser-detail-view-active' : '';

            return [
                'title' => \Yii::t('app', 'Click to view more information'),
                'class' => 'browser-detail-view-row ' . $activeRowClass,
                'data-model-class' => 'Location', // model class to retrieve record info
                'data-model-class-action' => 'getDetailViewInfo', // action in model to retrieve record info
                // buttons to be rendered in view record
                'data-buttons' => [],
                // main button actions to be rendered in view record
                'data-main-action-buttons' => [
                    [
                        Button::widget([
                            'label' => 'Complete',
                            'icon' => [
                                'name' => 'check',
                                'position' => 'right'
                            ],
                            'options' => [
                                'class' => 'white-text disabled',
                                'style' => 'cursor: not-allowed',
                                'data-target' => '#commit-occurrence-modal',
                                'data-toggle' => 'modal',
                                'href' => '#!',
                                'title' => \Yii::t('app', 'Commit occurrence')
                            ],
                        ])
                    ],
                    [
                        '&nbsp;&nbsp;'.Button::widget([
                            'label' => '',
                            'icon' => [
                                'name' => 'print'
                            ],
                            'options' => [
                                'class' => 'dropdown-trigger grey lighten-3 black-text no-label',
                                'href' => '#!',
                                'data-activates' => 'location-printouts-dropdown-button',
                                'data-beloworigin' => 'true',
                                'data-constrainwidth' => 'false',
                                'title' => \Yii::t('app', 'Printouts')
                            ],
                        ]) . 
                        "
                        <ul id='location-printouts-dropdown-button' class='dropdown-content' style='width: 160px !important' data-beloworigin='false'>
                            <li><a href='#!' style='cursor: not-allowed'>Plot tags</a></li>
                            <li><a href='#!' style='cursor: not-allowed'>Harvest tags</a></li>
                        </ul>"
                    ],
                    [
                        '&nbsp;'.Button::widget([
                            'label' => '',
                            'icon' => [
                                'name' => 'file_download'
                            ],
                            'options' => [
                                'class' => 'dropdown-trigger grey lighten-3 black-text no-label',
                                'href' => '#!',
                                'data-activates' => 'location-download-dropdown-button',
                                'data-beloworigin' => 'true',
                                'data-constrainwidth' => 'false',
                                'title' => \Yii::t('app', 'Download files')
                            ],
                        ]) . 
                        "
                        <ul id='location-download-dropdown-button' class='dropdown-content' style='width: 160px !important' data-beloworigin='false'>
                            <li title='Download data collection files'><a href='".Url::to(
                                [
                                    '/dataCollection/export-workbook/index',
                                    'program'=>$program,
                                    'locationId' => $model['locationDbId']
                                ]
                            ) .
                            "'>Data collection files</a></li>
                            <li><a href='#!' style='cursor: not-allowed'>Entries</a></li>
                            <li><a href='#!' style='cursor: not-allowed'>Plots</a></li>
                        </ul>"
                    ],
                ]
            ];
        },
        'panel' => [
            'before'=>false,
            'heading' => '<span class="total-count-summary-hidden" data-count="'.$totalCount.'"
                data-label="'.BrowserController::pluralize($totalCount, 'item').'"></span>',
            'after' => '<div class="primary-button load-more-button text-center '. $loadMoreBtnClass . '" data-page="'. $nextPage . '" itle="Load more items">Load more...</div>',
            'footer' => false
        ],
    ],
    'options' => [
        'id' => $browserId,
        'class' => $gridClass
    ]
]);
DynaGrid::end();

$morePanelClass = ($nextPage > 1) ? 'load-more-panel' : '';
?>

<div class="detail-view-page <?= $morePanelClass ?>" data-value="<?= $nextPage ?>"></div>
