<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>
*/
use macgyer\yii2materializecss\widgets\Button;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/**
 * Renders basic information of the Location
 */

echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $id,
    'program' => $program,
    'locationName' => $locationName,
    'active' => 'basic'
]);

Modal::begin([
    'id' => 'generic-modal',
]);
Modal::end();

extract($data);
$notSet = '<span class="not-set">(not set)</span>';

// set display of code label
$codeLabel = isset($code['label']) ? htmlspecialchars($code['label']) : '';
$codeValue = isset($code['value']) ? htmlspecialchars($code['value']) : $notSet;
$codeValue = isset($viewUrl) && !empty($viewUrl) ? 
	'<a href="'. $viewUrl .'" title="This is the '. $codeLabel .'. Click to view more information.">
		<i class="material-icons inline-material-icon">remove_red_eye</i> '. $codeValue .
	'</a>' : 
	'<span title="'. $codeLabel .'">'. $codeValue .'</span>';

// Download Data Collection Files
$downloadDataCollectionFilesBtn = "
    <li title='Download data collection files for this Location'>" .
        Html::a(
            'Data collection files',
            Url::to([
                    '/dataCollection/export-workbook/index',
                    'program' => $program,
                    'locationId' => $id,
                    'source' => 'em-loc'
            ]),
            []
        ) .
    "</li>";

// Download Planting Instructions (Location only)
$downloadPlantingInstructionsFileBtn = "
    <li 
        id='download-planting-arrays-btn'
        title='Download planting instructions file for this Location'
    >" .
        Html::a(
            'Planting instructions',
            null,
            [
                'class'=>'export-planting-instructions-btn hide-loading',
            ]
        ) .
    "</li>";

// Menu of entry points for Download files
$downloadDropdownMenu = "
    <ul
        id='location-dropdown-button'
        class='dropdown-content'
        style='width: 160px !important'
        data-beloworigin='false'
    >" .
        $downloadDataCollectionFilesBtn .
        $downloadPlantingInstructionsFileBtn .
    "</ul>";

// Initialize Download Files button
$downloadFilesBtn = [];

// If status has planted or mapped, render Download Files button
if (strpos(strtolower($status['label']), 'planted') !== false || strpos(strtolower($status['label']), 'mapped') !== false) {
    $downloadFilesBtn = [
        Button::widget([
            'label' => '',
            'icon' => [
                'name' => 'file_download'
            ],
            'options' => [
                'class' => 'dropdown-trigger grey lighten-3 black-text no-label',
                'href' => '',
                'data-activates' => 'location-dropdown-button',
                'data-beloworigin' => 'true',
                'data-constrainwidth' => 'false',
                'title' => \Yii::t('app', 'Download files here')
            ],
        ]) .
        $downloadDropdownMenu
    ];
}

// Load action buttons here
$mainButtons = [
    $downloadFilesBtn,
];
?>

<div class="browser-detail-view-viewer-panel">
	<div class="browser-detail-view-viewer-inner">
    
		<div class="viewer-body">
			<!-- First column -->
			<div class="col col-md-8">
				
				<div class="col col-md-12">

					<div class="viewer-display-name">
						<h3><?= !empty($name) ? '<span title="'.htmlspecialchars($name['label']).'">'.htmlspecialchars($name['value']) . '</span>' : $notSet ?></h3>
					</div>

					<!-- status to be displayed for smaller screens -->
					<div class="viewer-status for-small-screen">
						<span class="badge new <?= $status['statusClass'] ?>">
							<?= htmlspecialchars($status['label']) ?>
						</span>
					</div>

					<?php
						// if there are main action buttons
						if(!empty($mainButtons)){
							echo '<div class="viewer-main-buttons with-margin-bottom">';
								foreach ($mainButtons as $key => $value) {
									echo $value[0] ?? null;
								}
							echo '</div>';
						}
					?>
					<div class="viewer-description viewer-detail-item">
						<div class="viewer-description viewer-label">
							Description
						</div>

						<div class="viewer-description-value viewer-value">
							<?= !empty($description) ? htmlspecialchars($description) : $notSet ?>
						</div>
					</div>

				</div>

				<div class="viewer-primary-info">

					<?php
						// primary information
						if(isset($primaryInfo) && !empty($primaryInfo)){
							
							$primaryInfoStr = '';
							foreach ($primaryInfo as $key => $value) {

								$label = htmlspecialchars($value['label']);
								$val = (!empty($value['value']) || $value['value'] == '0') ? htmlspecialchars($value['value']) : $notSet;
								$title = (!empty($value['title']) && isset($value['title'])) ? htmlspecialchars($value['title']) : '';
								$val = (!empty($value['url']) && isset($value['url'])) ? 
									'<b><a href="'.$value['url'].'">'. $val .'</a></b>' : $val;
								$attribute = (!empty($value['attribute']) && isset($value['attribute'])) ? $value['attribute'] : '';
								
								$colClass = '6';
								if(in_array($attribute, ['experiments', 'occurrences']))
									$colClass = '12';

								$detailInfo = <<<EOD
									<div class="col col-lg-$colClass">
										<div class="viewer-primary-info viewer-detail-item">
											<div class="viewer-label">
												$label
											</div>
											<div class="viewer-value" title="$title">
												$val
											</div>
										</div>
									</div>
EOD;

								$primaryInfoStr .= $detailInfo;

								if($key % 2){
									$primaryInfoStr .= '<div class="row no-margin"></div>';
								}
							}

							echo $primaryInfoStr;
						}

					?>
				</div>
				<div class="clearfix"></div>
				<!-- For status monitoring -->
                <div id="more-info-container">
                    <?php
                        echo Button::widget([
                            'label' => \Yii::t('app', 'View More Info'),
                            'options' => [
                                'title' => \Yii::t('app', 'Load status monitoring'),
                                'style' => [
                                    'margin-right' => '4px',
                                ],
                                'id' => 'view-more-info-btn'
                            ],
                        ])
                    ?>
                </div>
			</div>

			<!-- Second column -->
			<div class="col col-md-4">
				<!-- status to be displayed for wider screens -->
				<div class="viewer-status for-wide-screen">
					<span class="badge new <?= $status['statusClass'] ?>">
						<?= htmlspecialchars($status['label']) ?>
					</span>
				</div>

			<?php
				// secondary information
				if(isset($secondaryInfo) && !empty($secondaryInfo)){

					$secondaryInfoStr = '';
					foreach ($secondaryInfo as $key => $value) {
						$label = htmlspecialchars($value['label']);
						$val = !empty($value['value']) ? htmlspecialchars($value['value']) : $notSet;

						$secondaryInfoStr .= <<<EOD
							<div class="viewer-secondary-info viewer-detail-item">
								<div class="viewer-label">
									$label
								</div>
								<div class="viewer-value">
									$val
								</div>
							</div>
EOD;
					}

					echo $secondaryInfoStr;
				}

				echo '<div class="divider margin-bottom"></div>';

				// autdit information
				if(isset($auditInfo) && !empty($auditInfo)){
					foreach ($auditInfo as $key => $value) {
						if(!empty($value['value']) && isset($value['value'])){
							$label = $value['label'];
							$val = $value['value'];

							echo <<<EOD
								<div class="viewer-audit-info text-muted">
									$label $val
								</div>
EOD;
						}
					}
				}

			?>
			
			</div>
		</div>
	</div>
</div>

<?php
// Export Planting Instructions file URL
$exportPlantingInstructionsOfLocationURL = Url::to(['/location/default/export-planting-instructions-of-location']);
// render location status monitoring info url
$renderLocationStatusMonitoringInfoUrl = Url::to(['/location/view/render-location-status-monitoring-info','id' => $id]);

// css
$this->registerCssFile("@web/css/browser/page-view.css");

$this->registerJs(<<<JS
    var program = '$program'
    var preloader =
        '<div class="category-panel-loading-indicator text-center">' +
            '<div class="preloader-wrapper small active">' +
                '<div class="spinner-layer spinner-green-only">' +
                    '<div class="circle-clipper left">' +
                        '<div class="circle"></div>' +
                    '</div>' +
                    '<div class="gap-patch">' +
                        '<div class="circle"></div>' +
                    '</div>' +
                    '<div class="circle-clipper right">' +
                        '<div class="circle"></div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>';

    // Render button dropdown
    $('.dropdown-trigger').dropdown()

    // Export planting instructions
    $(document).on('click', '.export-planting-instructions-btn', function (e) {
        e.preventDefault()

        let id = '$id'
        let exportPlantingInstructionsOfLocationURL = '$exportPlantingInstructionsOfLocationURL'
    
        let homeUrl = window.location.href
        let plantInsUrl =
            window.location.origin +
            exportPlantingInstructionsOfLocationURL +
            '?locationDbId=' + id +
            '&fileType=csv' +
            '&program='+program +
            '&redirectUrl='+encodeURIComponent(homeUrl)            

        window.location.replace(plantInsUrl)
    })

    // Hide loading indicator
    $(document).on('click', '.hide-loading', function hideLoadingIndicator () {
        $('#system-loading-indicator').css('display','block')

        setTimeout(function setLoadingIndicatorTimeout () {
            $('#system-loading-indicator').css('display','none')
        }, 2000)
    })

    // Render location status monitoring info
    $(document).on('click', '#view-more-info-btn', function(e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        $('#more-info-container').html(preloader.replace('text-center',''))

        $.ajax({
            url: '$renderLocationStatusMonitoringInfoUrl',
            type: 'post',
            dataType: 'json',
            data:{},
            success: function (response) {
                $('#more-info-container').html(response)
            },
            error: function () {
            }
        })

    })
JS);