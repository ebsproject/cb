<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for saving plots from location as a list
**/

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;

echo '<p id="location-plot-save-list-preview-error"></p>';

echo '<div class="location-plot-save-list-modal-loading"></div>';

$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'create-plot-saved-list-form',
    'action' => ['/location/view/plot'],
    'fieldConfig' => function ($model, $attribute) {
        if (in_array($attribute, ['abbrev', 'display_name', 'name', 'type'])) {
            return ['options' => ['class' => 'input-field form-group col-md-6']];
        } else {
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    }
]);

$model = \Yii::$container->get('app\models\PlatformList');

echo
    '<div class="modal-notif"></div>' . //modal notification field

        $form->field($model, 'name')->textInput([
            'maxlength' => true,
            'id' => 'location-plot-list-name',
            'title' => 'Name identifier of the list',
            'oninput' => 'js:
            $("#location-plot-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());
            $("#location-plot-list-display_name").val(this.value);
            Materialize.updateTextFields();',
        ]) .
        $form->field($model, 'abbrev')->textInput([
            'maxlength' => true,
            'id' => 'location-plot-list-abbrev',
            'title' => 'Short name identifier or abbreviation of the list',
            'oninput' => 'js: $("#plot-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());',
        ]) .
        $form->field($model, 'display_name')->textInput([
            'maxlength' => true,
            'id' => 'location-plot-list-display_name',
            'title' => 'Name of the list to show to users',
        ]) .
        '<div class="input-field form-group col-md-6 field-plot-list-type required">' .
        '<label class="control-label" for="plot-list-type" style="top: -100%">Type</label>' .
        Select2::widget([
            'name' => 'MyList[type]',
            'data' => [
                'plot' => 'Plot'
            ],
            'options' => [
                'placeholder' => 'Choose a list type',
                'id' => 'location-plot-list-type',
                'class' => 'form-control',
                'required' => true,
                'data-name' => 'Type',
            ],
            'hideSearch' => true
        ]) .
        '</div>' .
        $form->field($model, 'description')->textarea([
            'class' => 'materialize-textarea',
            'id' => 'location-plot-list-description',
            'title' => 'Description about the list',
        ]) .
        $form->field($model, 'remarks')->textarea([
            'class' => 'materialize-textarea',
            'title' => 'Additional details about the list',
            'row' => 2,
            'id' => 'location-plot-list-remarks',
        ]);

ActiveForm::end();

echo '<input id="checked-boxes-string" class="hidden" value=""></input>';

echo '
    <div class="location-total-plot-count">
        <span>Number of plots to be saved as a list: <b><span id="save-item-count"></span></b></span>
    </div>';

$retrieveDbIdsUrl = Url::to(['view/retrieve-db-ids' . "?locationId=$locationId" . "&entity=plot"]);
$saveAsListUrl = Url::to(['view/save-as-list' . "?locationId=$locationId" . "&entity=plot"]);

$this->registerJs(<<<JS
    let loadingIndicatorHtml = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
    let forBackgroundProcessing = false;
    let threshold = '$threshold';

    // validate if all required fields are specified
    $('.form-control').bind("change keyup input", function () {
        let abbrev = $('#location-plot-list-abbrev').val()
        let name = $('#location-plot-list-name').val()
        let displayName = $('#location-plot-list-display_name').val()
        let type = $('#location-plot-list-type').val()

        if (abbrev != '' && name != '' && displayName != '' && type != '') {
            $('#location-plot-save-list-confirm-btn').removeClass('disabled')
        } else {
            $('#location-plot-save-list-confirm-btn').addClass('disabled')
        }
    })

    $('#location-plot-list-type').on('select2:select', function (e) {
        if (checkedBoxes.length > threshold) {
            forBackgroundProcessing = true;
        } else {
            forBackgroundProcessing = false;
        }
    });

    $('#location-plot-save-list-confirm-btn').on('click', function (e){
        e.preventDefault();
        e.stopImmediatePropagation();

        $('.location-plot-save-list-modal-loading').html(loadingIndicatorHtml);
        $(this).addClass('disabled');

        let itemsCount = $('#save-item-count').val();

        let abbrev = $('#location-plot-list-abbrev').val();
        let name = $('#location-plot-list-name').val();
        let displayName = $('#location-plot-list-display_name').val();
        let listType = $('#location-plot-list-type').val();
        let description = $('#location-plot-list-description').val();
        let remarks = $('#location-plot-list-remarks').val();

        if(forBackgroundProcessing){
            $('#cancel-save').css({'pointer-events': ''}) // disable cancel button
            
            $('.toast').css('display','none');
            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + 'Saving of list will be forwarded to background process.';
            Materialize.toast(message, 5000);
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '$saveAsListUrl',
            data: {
                dataType: 'plots',
                type: listType,
                abbrev: abbrev,
                name: name,
                displayName: displayName,
                description: description,
                remarks: remarks,
                dbIds: (checkedBoxes.length === totalCount) ? JSON.stringify([]) : JSON.stringify(checkedBoxes),
                isSelectAllChecked: isSelectAllChecked,
                selectAllMode: selectAllMode
            },
            async: true,
            success: function(response){
                $('#cancel-save').css({'pointer-events': ''}) // disable cancel button

                let message = '';
                if (response['success']) {
                    message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i>' + response['message']
                    $('#location-plot-save-list-modal').modal('hide');
                } else {
                    message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + response['message']
                }
                
                $('.toast').css('display','none')
                Materialize.toast(message, 5000)
                $('.location-plot-save-list-modal-loading').html('');
                $('#location-plot-save-list-confirm-btn').removeClass('disabled');
            },
            error: function(response){
                $('#cancel-save').css({'pointer-events': ''}) // disable cancel button
            }
        })
    });

JS);
?>