<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\dropdown\DropdownX;
use yii\helpers\Url;

/**
 * Renders field layout of the location
 */

if($data == 'invalid_location'){
    // if Location is invalid or voided
    echo '<div class="row">&emsp;<i>Location is invalid.</i></div>';
}else if(empty($data)){
    // If there are no plot records
    echo '<div class="row">&emsp;<i>There are no plot records.</i></div>';
}else{

    $entnoLabel ='';
    $plotnoLabel ='';
    $repnoLabel = '';
    $codeLabel = '';
    $blkLabel = '';
    if($label == 'entno'){
        $entnoLabel = 'teal darken-4 white-text';
    } else if($label == 'plotno'){
        $plotnoLabel = 'teal darken-4  white-text';
    } else if($label == 'block'){
        $blkLabel = 'teal darken-4  white-text';
    } else if($label == 'plotcode'){
        $codeLabel = 'teal darken-4  white-text';
    }
    if(empty($maxBlk) || $maxBlk == NULL){
        $hideBlk = "hidden";
    } else {
        $hideBlk = "";
    }

    $updateLayoutUrl = Url::to(['view/change-occurrence-layout']);
    $downloadLayoutUrl = Url::to(['view/download-layout']);
    ?>

    <div id="layout-viewer-panel" style="overflow:auto;">
        
        <?php if($noOfCols > 0){ ?>
            <a id="entno" class='dropdown-change-label btn <?=$entnoLabel?>' href='#' >Entry number</a>
            <a id="plotno" class='dropdown-change-label btn <?=$plotnoLabel?>' href='#' >Plot number</a>
            <a id="plotcode" class='dropdown-change-label btn <?=$codeLabel?>' href='#' >Plot Code</a>
            <a id="block"  class='dropdown-change-label btn <?=$blkLabel?> <?=$hideBlk?>' href='#' >Block number</a>
        <?php } ?>

        <?php
            $buttons = '<div class="row">&emsp;<i>Plots do not have Field X and Field Y values.</i></div>';
            if($noOfCols > 0){
                $buttons = "<a class='waves-effect waves-light btn pull-right field-layout-download-btn pull-right tooltipped' style='margin-left:0px;' title='Download field layout'><i class='material-icons'>file_download</i></a>

                    <a class='waves-effect waves-light btn pull-right fullscreen-btn pull-right tooltipped' style='margin-left:15px;' data-position='top' data-delay='50' title='Click to toggle fullscreen'><i class='material-icons'>fullscreen</i></a>";
            }

            $actionsDropdown = $buttons.
                "<a id='occurrence-selection-id' class='dropdown-button pull-right' href='#' data-activates='occurrences-dropdown-actions' data-beloworigin='true' title='Current selected occurrence' data-constrainwidth='false'><span id='occurrence-name-span'>".$occurrenceName."</span><span class='caret'></span></a>".
                "<ul id='occurrences-dropdown-actions' class='dropdown-content' data-constrainwidth='false' data-beloworigin='false'>";

            // display all occurrences
            foreach($occurrencesList as $occurrence){
                $actionsDropdown .=  "<li><a href='#' class='dropdown-button occurrence-selection-list' data-hover='hover' id='".$occurrence['occurrenceDbId']."' data-name='".$occurrence['occurrenceName']."' data-alignment'='left' >".$occurrence['occurrenceName']."</a></li>";
            }

            echo $actionsDropdown."</ul>";
        ?>

        <?php if($noOfCols > 0){ ?>

            <div id="layout-view-pane"  class="col col-sm-12 view-entity-content">
                <div id="notifications-div" hidden>
                    <div class="progress"><div class="indeterminate"></div></div>
                </div>
                <div id="design-layout-panel"  class="chartContainer"  style="margin-top:15px; overflow: auto;">
                    <div class="progress"><div class="indeterminate"></div></div>
                </div>
                <div class="row col-sm-6 first-plot-position" style="margin-left: 53px;">
                    <label>First Plot Position</label>
                    <div class="switch">
                        <label>
                            Top Left
                            <input type="checkbox" id="flipped-switch-id" <?= ($flipped) ? '' : 'checked' ?>>
                            <span class="lever"></span>
                            Bottom Left
                        </label>
                    </div>
                    <i style="color: #9e9e9e">Please note that the first plot position is always top left in the downloaded field layout file.</i>
                </div>
            </div>
        <?php } ?>
    </div>

<script type="text/javascript">
        
    var startingPlotNo = <?= $startingPlotNo ?>;
    var noOfCols = <?= $noOfCols ?>;
    var noOfRows = <?= $noOfRows ?>;
    var data = JSON.parse('<?= json_encode($data) ?>');
    var label = '<?= $label?>';
    var dataArray = data[label];
    var panelWidth = <?= $panelWidth ?>;
    var flipped = '<?= $flipped ?>';
    var updateLayoutUrl =  '<?= $updateLayoutUrl ?>'
    var downloadLayoutUrl =  '<?= $downloadLayoutUrl ?>';
    var fullscreenFlag = null;
    var occurrenceDbId = <?= $occurrenceDbId ?>;
    var occurrenceName = '<?= $occurrenceName?>';
    var xAxis = JSON.parse('<?= json_encode($xAxis) ?>');
    var yAxis = JSON.parse('<?= json_encode($yAxis) ?>');

    var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
    var width = 0;
    var height = 0;
    var wbWidth = 65;
    var hbWidth = 40;
    var row = Object.keys(yAxis).length;
    var col = Object.keys(xAxis).length;

    computeDimension();

    $(document).ready(function(){
        $('.dropdown-button').dropdown();
        if(noOfCols > 0){
            renderDesignLayout();
        }else{
            $('#design-layout-panel > .progress').addClass('hidden');
            $('.first-plot-position').addClass('hidden');
            $('#design-layout-panel').html('<div class="row">&emsp;<i>Plots do not have Field X and Field Y values.</i></div>');
        }

        $(".dropdown-change-label").click(function(){
            label = this.id;
            dataArray = data[label];

            $(".dropdown-change-label").removeClass("teal darken-4");

            $(this).addClass("teal darken-4 white-text");

            if(fullscreenFlag == 1){
                $('#design-layout-panel').html(loadingIndicator);
            }
            renderDesignLayout();
        });

        $("#flipped-switch-id").on('change', function (e, params) {
          
            if($('#flipped-switch-id').prop('checked') == true){
                flipped = false;
            } else{
                flipped = true;
            }
            if(fullscreenFlag == 1){
                $('#design-layout-panel').html(loadingIndicator);
            }
            renderDesignLayout();
        });

        $(".occurrence-selection-list").click(function(){
            occurrenceDbId = this.id;
            occurrenceName =  $(this).attr("data-name");

            $('#design-layout-panel').html(loadingIndicator);

            $.ajax({
                url: updateLayoutUrl,
                type: 'post',
                data: {
                    occurrenceDbId: occurrenceDbId,
                    fullScreen:fullscreenFlag
                },
                success: function(response) {
                    layoutData = JSON.parse(response);

                    if (layoutData['xAxis'].length > 0 && layoutData['yAxis'].length > 0) {
                        data = layoutData['dataArrays'];
                        dataArray = data[label];
                        xAxis = layoutData['xAxis'];
                        yAxis = layoutData['yAxis'];

                        row = Object.keys(yAxis).length;
                        col = Object.keys(xAxis).length;

                        //change currently selected occurrence
                        var aField = document.getElementById("occurrence-name-span");
                        aField.textContent = occurrenceName;
                        computeDimension();
                        renderDesignLayout();
                        $('#flipped-switch-id').removeAttr('disabled')
                    } else {
                        $('#design-layout-panel').html('<div class="row">&emsp;<i>This Occurrence\'s Plots do not have Field X and Field Y values.</i></div>')
                        $('#flipped-switch-id').attr('disabled', 'true')
                    }
                },
                error: function(e) {
                }
            });
        });

        $('.fullscreen-btn').click(function(e){
            if(fullscreenFlag == null || fullscreenFlag == 0){
                $('.fullscreen-btn').html('<i class="material-icons">fullscreen_exit</i>');
                fullscreenFlag = 1;
            } 
            else if(fullscreenFlag == 1){
                fullscreenFlag = 0;
                $('#main-left-panel').removeClass('hidden');
                $('.fullscreen-btn').html('<i class="material-icons">fullscreen</i>');
            }
            $('#design-layout-panel').html(loadingIndicator);
            $.ajax({
                url: updateLayoutUrl,
                type: 'post',
                data: {
                    occurrenceDbId: occurrenceDbId,
                    fullScreen:fullscreenFlag
                },
                success: function(response) {
                    layoutData = JSON.parse(response);

                    if (layoutData['xAxis'].length > 0 && layoutData['yAxis'].length > 0) {
                        data = layoutData['dataArrays'];
                        dataArray = data[label];

                        xAxis = layoutData['xAxis'];
                        yAxis = layoutData['yAxis'];

                        row = Object.keys(yAxis).length;
                        col = Object.keys(xAxis).length;

                        $('#layout-viewer-panel').toggleClass('fullscreen');

                        computeDimension();
                        renderDesignLayout();
                        $('#flipped-switch-id').removeAttr('disabled')
                    } else {
                        $('#design-layout-panel').html('<div class="row">&emsp;<i>This Occurrence\'s Plots do not have Field X and Field Y values.</i></div>')
                        $('#flipped-switch-id').attr('disabled', 'true')
                    }
                },
                error: function(e) {
                }
            });
        });

        // download field layout in excel file
        $('.field-layout-download-btn').click(function(e){
            // display loading indicator
            $('#system-loading-indicator').css('display','block');
            setTimeout(function() {
                $('#system-loading-indicator').css('display','none');
            },3000);

            window.location = downloadLayoutUrl + '?id=' + occurrenceDbId + '&label=' + label;
            
        });
    });

    // compute dimension of layout
    function computeDimension(){

        if(fullscreenFlag == 1){
            if(col <= 20){
                wbWidth = 80;
            }
            if(row <= 20){
                hbWidth = 40;
            }
        }

        //compute width of chart
        width = wbWidth * col;
        if(width < 200){
            width = 200;
        }

        //compute height of chart
        height = hbWidth * row;
        if(height < 200){
            height = 200;
        }

        if(!fullscreenFlag){
            height = width = null;
        }
    }

    // render layout
    function renderDesignLayout(){
        setTimeout(function(){
            layout = Highcharts.chart('design-layout-panel',{
                chart: {
                    marginTop: 70,
                    plotBorderWidth: 1,
                    type: 'heatmap',
                    plotBorderWidth: 1,
                    plotBorderRadius: 5,
                    animation: false,
                    height: height,
                    width: width
                },
                title: {
                    text: 'Field Layout'
                },
                xAxis: {
                  opposite: true,
                  categories: xAxis,
                  title:{
                    text:"Column"
                  }
                },
                yAxis: {
                    reversed: flipped,
                    categories: yAxis,
                    title:{
                        text:"Row"
                    }
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    enabled: false
                },
                series: [{
                    name: 'Layout',
                    borderWidth: 1,
                    borderColor: '#fff',
                    color: '#7cb342',
                    turboThreshold:Number.MAX_VALUE,//set it to a larger threshold, it is by default to 1000
                    data: dataArray,
                    dataLabels: {
                        enabled: true
                    }
                }]
            });
        },50);
    }
</script>
<?php
}
?>

<style type="text/css">

    .view-entity-content {
        background: #ffffff;
    }

    #occurrence-name-span {
        font-size: 1rem;
    }

    #layout-viewer-panel.fullscreen{
        padding: 25px;
        z-index: 9999; 
        width: 100%; 
        height: 100%; 
        position: fixed; 
        top: 0; 
        left: 0;
        background: #ffffff;
    }

    .highcharts-container{
        width:100%;
    }

    .chartContainer{
        overflow:auto !important;
    }

    .dropdown-content {
        overflow: auto !important;;
    }
</style>
