<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
use yii\helpers\Html;
use yii\helpers\Url;

/*
 * Renders the header tab for the view occurrence
 */
$occurrencesUrl = Url::to(['/occurrence','program' => $program]);
$locationBasicUrl = Url::to(['basic','program' => $program, 'id' => $id]);
$locationPlotUrl = Url::to(['plot','program' => $program, 'id' => $id]);
$locationLayoutUrl = Url::to(['layout','program' => $program, 'id' => $id]);

$redirectToLocationUrl = Url::to([
    '/occurrence',
    'program' => $program,
    'OccurrenceSearch[location]' => $locationName
]);

echo
'<h3>' . Html::a( Yii::t('app', 'Experiment Manager'), $occurrencesUrl) .
    ' <small>» '. Html::a($locationName, $redirectToLocationUrl) . '</small>'.
'</h3>';

$tabsArr = [
    [
        'label' => 'Basic',
        'url' => $locationBasicUrl,
        'linkClass' => ($active == 'basic') ? 'active' : ''
    ],
    [
        'label' => 'Plot',
        'url' => $locationPlotUrl,
        'linkClass' => ($active == 'plot') ? 'active' : ''
    ],
    [
        'label' => 'Field Layout',
        'url' => $locationLayoutUrl,
        'linkClass' => ($active == 'layout') ? 'active' : ''
    ]
];

?>
<div class="col col-md-12" style="padding-right: 0px">
    <ul class="tabs">
        <?php
            // render tabs
            foreach ($tabsArr as $value) {

                $label = $value['label'];
                $url = $value['url'];
                $linkClass = $value['linkClass'];

                $item = '<li class="tab col">'.
                    '<a href="'.$url.'" class="'.$linkClass.' a-tab">' . $label . '</a>
                </li>';

                echo $item;
            }
        ?>
    </ul>
</div>
<div class="row"></div>

