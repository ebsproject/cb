<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders plot of a location
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use app\controllers\BrowserController;
use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;
use yii\helpers\Url;
use \app\models\Variable;


Yii::$app->session->set('location-plot-filters' . $id, $filters);
Yii::$app->session->set('location-plot-export-filters' . $id, $exportFilters);
Yii::$app->session->set('location-plot-param-sort' . $id, $paramSort);
Yii::$app->session->set('location-plots-program', $program);

$browserId = 'dynagrid-em-location-plots';
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '$browserId',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

$thresholdValue = Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'saveListMembers');

// modal for saving as new list
Modal::begin([
    'id' => 'location-plot-save-list-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">add</i>' . Yii::t('app', 'Save as New List') . '</h4>',
    'footer' => '' . Html::a(
                    'Cancel', 
                    '', 
                    ['id' => 'cancel-plot-list-save', 'data-dismiss' => 'modal']
                ) . 
                '&emsp;&emsp;' . 
                Html::a(
                    'Submit' . '<i class="material-icons right">send</i>',
                    '',
                    [
                        'class' => 'btn btn-primary waves-effect waves-light disabled',
                        'url' => '',
                        'id' => 'location-plot-save-list-confirm-btn'
                    ]
                ) . 
                '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);

echo Yii::$app->controller->renderPartial(
    '_save_list_modal',
    [
        'program'=>$program, 
        'locationId' => $id,
        'threshold' => $thresholdValue
    ]);

Modal::end();

echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $id,
    'program'=>$program,
    'locationName' => $locationName,
    'active'=>'plot'
]);

$columns = [
    [
        //checkbox
        'label' => '',
        'header' => '
            <input type="checkbox" class="filled-in" id="checkbox-select-all" />
            <label for="checkbox-select-all"></label>
        ',
        'content' => function ($data) {
            $plotDbId = $data['plotDbId'];

            $checkbox = '<input class="grid-select filled-in" type="checkbox" id="' . $plotDbId . '"/>';

            return $checkbox . '
                <label for="' . $plotDbId . '"></label>
            ';
        },
        'hAlign' => 'center',
        'vAlign' => 'top',
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    ...$configCols,
];

$traitAttributes = [];
$traitLabels = [];
$varFields = '';
$retrievePlotDbIdsUrl = null;
$variableModel = new Variable();

$saveAsNewListDropdown = '&emsp;' . Html::a(
    "<i class='material-icons' style='vertical-align:bottom'>add_shopping_cart</i>",
    '',
    [
        'id' => 'location-plot-save-list-btn',
        'title' => 'Save as New List',
        'class' => 'dropdown-button btn dropdown-button-pjax',
        'data-activates' => 'manage-plot-dropdown-save',
        'data-beloworigin' => true,
        'data-constrainwidth' => false
    ]
) . '&emsp;';

//plot data
foreach ($traits as $value) {

    $variable = $variableModel->searchAll(["abbrev" => $value], '');

    if (!empty($variable['data'])) {
        $label = $variable['data'][0]['label'];

        $varFields .= ' | ' . $value . ' AS ' . $value;

        // add info into collection
        array_push($traitAttributes, $value);
        array_push($traitLabels, $label);

        $columns[] = [
            'attribute' => $value,
            'visible' => true,
            'order' => DynaGrid::ORDER_FIX_RIGHT,
            'format' => 'html',
            'label' => ucwords(str_replace('_', ' ', strtolower($label)), ' '),
            'value' => function ($model) use ($value) {
                $dataValue = $model[$value]['dataValue'] ?? 'NA';
                
                // process display of value based on QC code
                if ( isset ($model[$value]['dataQCCode']) ) {

                    // if data is questionable, display only value
                    if(strtoupper($model[$value]['dataQCCode']) == 'Q'){
                        return $dataValue;
                    }

                    // get QC code class and description
                    $qcCode = BrowserController::getClassByQcCode($model[$value]['dataQCCode']);
                    return '<span class="badge text-center ' . $qcCode['class']. '" title="'.Yii::t('app', $qcCode['description']).'">'.$dataValue.'</span>';
                }
            }
        ];
    }
}

Yii::$app->session->set('location-plot-export-filter-fields' . $id, $varFields);
Yii::$app->session->set('location-plot-trait-attributes' . $id, $traitAttributes);
Yii::$app->session->set('location-plot-trait-labels' . $id, $traitLabels);

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => "$browserId-id",
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => "$browserId-id"],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsiveWrap' => false,
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', 'This is the browser for the plots of the location.') .
                '{summary}' .
                '<br/>
            <p id="selected-items-paragraph" class = "pull-right" style = "margin-right: 2px;">
                <b><span id="selected-items-count"> </span></b> <span id="selected-items-text"> </span>
            </p>',
            'after' => false,
        ],
        'toolbar' =>  [
            [
                'content' =>
                '&nbsp;' .
                    Html::a(
                        '<i class="material-icons inline-material-icon">file_download</i>',
                        '',
                        [
                            'data-pjax' => true,
                            'id' => 'export-plots-btn',
                            'class' => 'btn btn-default',
                            'style' => 'margin-left: 5px;',
                            'title' => \Yii::t('app', 'Export all plots and measured traits in CSV format')
                        ]
                    ) .
                    $saveAsNewListDropdown .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [
                            '/location/view/plot',
                            'program' => $program,
                            'id' => $id
                        ],
                        [
                            'data-pjax' => true,
                            'id' => 'reset-plot-grid',
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid')
                        ]
                    ) .
                    '{dynagridFilter}{dynagridSort}{dynagrid}',
            ]
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-delete',
        'label' => 'Remove',
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// URLs for export
$exportLocationPlotsUrl = Url::to(['/occurrence/view/export-location-plots','id'=>$id]);
$exportPlotsUrl = Url::to(['/occurrence/view/export-nonworker-location-plots']);

(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
let checkboxSessionStorageName = 'plotCheckboxIds' + '$id'
let totalCheckboxesSessionStorageName = 'plotTotalCheckboxIds' + '$id'
let selectAllModeSessionStorageName = 'plotSelectAllMode' + '$id'

sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')

if (!sessionStorage.getItem(checkboxSessionStorageName))
    sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify([]))
if (!sessionStorage.getItem(totalCheckboxesSessionStorageName))
    sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')
if (!sessionStorage.getItem(selectAllModeSessionStorageName))
    sessionStorage.setItem(selectAllModeSessionStorageName, 'include')

let checkedBoxes = JSON.parse(sessionStorage.getItem(checkboxSessionStorageName))
let totalCheckboxes = sessionStorage.getItem(totalCheckboxesSessionStorageName)
let selectAllMode = sessionStorage.getItem(selectAllModeSessionStorageName)
let isSelectAllChecked = false
let browserId = '$browserId'

const exportRecordsThresholdValue = $thresholdValue
const program = '$program'
const totalCount = $totalCount
let filteredTotalCount = totalCount

// pre-check all tracked checkboxes in a given page
if (checkedBoxes.length > 0) {
    $('input:checkbox.grid-select').each(function () {
        if (checkedBoxes.includes($(this).attr('id'))) {
            $(this).prop("checked", true)
        }
    })
}

$(document).ready(function() {
    adjustBrowserPageSize()
    refresh()
})

$(document).on('ready pjax:success', function(e) {
    e.stopPropagation()
    e.preventDefault()
    refresh()
});

function refresh() {
    // verify if initial count is same as filtered count
    let summary = $(".summary").text()
    if (summary.trim().length) {
        filteredTotalCount = parseInt($('.summary')[0].children[1].innerHTML.split(',').join(''))
        if (filteredTotalCount !== totalCount) {
            // Store filtered plot count
            totalCheckboxes = filteredTotalCount
            sessionStorage.setItem(totalCheckboxesSessionStorageName, filteredTotalCount)
        } else {
            // Store total plot count
            totalCheckboxes = totalCount
            sessionStorage.setItem(totalCheckboxesSessionStorageName, totalCount)
        }
    } else {
        // if no results found
        filteredTotalCount = 0
    }

    if (
        (selectAllMode === 'include' && totalCheckboxes === checkedBoxes.length) ||
        (selectAllMode === 'exclude' && checkedBoxes.length === 0)
    ) { // auto-tick Select All checkbox
        $('#checkbox-select-all').trigger('click')
    } else if (checkedBoxes.length > 0) { // auto-tick tracked checkedboxes
        $('#checkbox-select-all').prop('checked', false)
        $(".grid-select:checkbox").each(function () {
            let entryDbId = this.id
            
            if (
                (selectAllMode === 'include' && checkedBoxes.includes(entryDbId)) ||
                (selectAllMode === 'exclude' && !checkedBoxes.includes(entryDbId))
            ) {
                $(this)
                    .prop('checked',true)
                    .parent("td")
                    .parent("tr")
                    .addClass('grey lighten-4')
            }
        });

        compareCheckBoxCountWithTotalCount()
    }

    // set dynamic height for the browser
    let maxHeight = ($(window).height() - 320)
    $('.kv-grid-wrapper').css('height', maxHeight)

    $('#export-plots-btn').on('click',function(){
        $('#system-loading-indicator').css('display','none')
    })

    $('#export-plots-btn').on('click', function (e) {
        e.preventDefault()
        $('#system-loading-indicator').css('display','none')

        // Invoke background worker if threshold exceed
        if(totalCheckboxes >= exportRecordsThresholdValue) {
            let exportLocationPlotsUrl = '$exportLocationPlotsUrl'
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: exportLocationPlotsUrl,
                    data: {
                    },
                    success: async function (ajaxResponse) {},
                    error: function (xhr, error, status) {}
                })
        } else{
            let exportLocationPlotsUrl = '$exportPlotsUrl'
            let homeUrl = window.location.href
            let expLocationPlotUrl = 
                window.location.origin + 
                exportLocationPlotsUrl + 
                '?id='+'$id' +
                '&program='+'$program' +
                '&redirectUrl='+encodeURIComponent(homeUrl)

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
            $('.toast').css('display','none')
            Materialize.toast(message, 5000)
        
            window.location.replace(expLocationPlotUrl)
        }
    })

    // show save as new list modal
    $(document).on('click','#location-plot-save-list-btn',function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        let saveItemCount = 0

        if (selectAllMode === 'include') {
            saveItemCount = (checkedBoxes.length === 0) ? $('.summary')[0].children[1].innerHTML : checkedBoxes.length
        } else if (selectAllMode === 'exclude') {
            saveItemCount = totalCheckboxes - checkedBoxes.length
        }
        $('#save-item-count').html(saveItemCount)
        $('#location-plot-save-list-modal').modal('show')
    })

    // validate if all required fields are specified
    $('.form-control').bind("change keyup input", function () {
        let abbrev = $('#location-plot-list-abbrev').val()
        let name = $('#location-plot-list-name').val()
        let displayName = $('#location-plot-list-display_name').val()
        let type = $('#location-plot-list-type').val()

        if (abbrev != '' && name != '' && displayName != '' && type != '') {
            $('#experiment_manager-save-list-confirm-btn').removeClass('disabled')
        } else {
            $('#experiment_manager-save-list-confirm-btn').addClass('disabled')
        }
    })
}

// select all plots
$(document).on('click', '#checkbox-select-all', function(e) {
    if ($(this).prop("checked") === true) {
        isSelectAllChecked = true

        selectAllMode = 'exclude'
        sessionStorage.setItem(selectAllModeSessionStorageName, selectAllMode)

        $(this).prop('checked', true)
        $(".grid-select")
            .prop('checked',true)
            .parent("td")
            .parent("tr")
            .addClass("grey lighten-4")
    } else {
        isSelectAllChecked = false

        selectAllMode = 'include'
        sessionStorage.setItem(selectAllModeSessionStorageName, selectAllMode)

        $(this).prop('checked', false)
        $(".grid-select")
            .prop('checked',false)
            .parent("td")
            .parent("tr")
            .removeClass("grey lighten-4")
    }

    checkedBoxes = []
    
    sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
    
    compareCheckBoxCountWithTotalCount()
    
})

// prevent default behavior of checkbox
$(document).on('click', '.grid-select', function(e) {
    e.preventDefault()
    e.stopImmediatePropagation()
})

// click checkbox in plot browser
$(document).on('click', `#\${browserId} tbody tr`, function (e) {
    e.preventDefault()
    e.stopImmediatePropagation()

    let thisRow = $(this).find('input:checkbox')[0]

    // Handle checkedBoxes
    if (checkedBoxes.includes(thisRow.id)) {
        checkedBoxes.splice(checkedBoxes.indexOf(thisRow.id), 1)
    } else {
        checkedBoxes.push(thisRow.id)
    }

    $('#' + thisRow.id).trigger('click')

    // Update clicked checkbox's state
    if (thisRow.checked) {
        $(this).addClass('grey lighten-4')
    } else {
        $(this).removeClass('grey lighten-4')
    }

    if (isSelectAllChecked) {
        isSelectAllChecked = false
        $('#checkbox-select-all').prop('checked', false)
    }

    sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
    compareCheckBoxCountWithTotalCount()
})

// compare check box count with total count
function compareCheckBoxCountWithTotalCount () {
    if (selectAllMode === 'exclude') {
        let currentCount = totalCheckboxes - checkedBoxes.length

        if (checkedBoxes.length > 0) {
            isSelectAllChecked = false
            $('#checkbox-select-all').prop('checked', false)
        } else {
            isSelectAllChecked = true
            $('#checkbox-select-all').prop('checked', true)
        }

        // update checked box counter text
        if (currentCount === 1) {
            $('#selected-items-count').html(currentCount)
            $('#selected-items-text').html('selected item.')
        } else if(currentCount > 1) {
            $('#selected-items-count').html(currentCount)
            $('#selected-items-text').html('selected items.')
        } else {
            $('#selected-items-count').html(filteredTotalCount)
            $('#selected-items-text').html('selected items.')
        }
    } else if (selectAllMode === 'include') {
        if (totalCheckboxes === checkedBoxes.length) {
            isSelectAllChecked = true
            $('#checkbox-select-all').prop('checked', true)
        } else {
            isSelectAllChecked = false
            $('#checkbox-select-all').prop('checked', false)
        }

        // update checked box counter text
        if (checkedBoxes.length === 1) {
            $('#selected-items-count').html(checkedBoxes.length)
            $('#selected-items-text').html('selected item.')
        } else if(checkedBoxes.length > 1) {
            $('#selected-items-count').html(checkedBoxes.length)
            $('#selected-items-text').html('selected items.')
        } else if (checkedBoxes.length === 0) {
            $('#selected-items-count').html('No')
            $('#selected-items-text').html('selected items.')
        }
    }
}
JS);

$this->registerCss('
    .select2.select2-container.select2-container--krajee.select2-container--below.select2-container--focus {
        width: 100%;
        margin-top: 2.5%;
    }

    .select2.select2-container.select2-container--krajee.select2-container--below {
        width: 100%;
        margin-top: 2.5%;
    }

    .select2.select2-container.select2-container--krajee {
        width: 100%;
        margin-top: 2.5%;
    }
');
?>
