<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\location\controllers;

use app\components\B4RController;
use app\models\OccurrenceData;
use app\modules\dataCollection\models\ExportWorkbook;
use app\modules\occurrence\models\Manage as OccurrenceManage; // set alias to this to avoid duplicates
use yii\helpers\Url;
use Yii;

/**
 * Controller for monitoring locations
 */
class MonitorController extends B4RController
{
    public function __construct ($id, $module,
        protected ExportWorkbook $exportWorkbook,
        protected OccurrenceManage $occurrenceManage,
        protected OccurrenceData $occurrenceData,
        $config = []
    ) { parent::__construct($id, $module, $config); }

    /**
     * Get location monitoring status data
     *
     * @param integer $id location identifier
     * @param integer @occurrenceId occurrence identifier
     * @return view file for location monitoring status data
     */
    public function getLocationStatusData($id=null, $occurrenceId=null){

        // As of 21.12 task graphic is disabled
        // monitor tasks
        // $data['tasks']['label'] = 'Tasks completed';

        // monitor traits
        $traits = $this->getTraitDataStatus($id, $occurrenceId);
        $data['traits'] = $traits;

        // monitor seeds
        $seeds = $this->getSeedsHarvestedStatus($id, $occurrenceId);
        $data['seeds'] = $seeds;

        return $data;
    }

    /**
     * Get trait data status
     *
     * @param $id integer location identifier
     * @param $occurrenceId integer occurrence identifier
     * @param $locationId integer location identifier
     * @return array status monitoring data
     */
    public function getTraitDataStatus($id=null, $occurrenceId=null, $locationId=null){

        // if in view Occurrence
        if(!empty($occurrenceId)){

            $statusArr = $this->getTraitsStatusByEntity($occurrenceId);
        } else {

            $statusArr = $this->getTraitsStatusByEntity($id, 'location');
        }

        $status = $statusArr['status'];
        $collectedCount = $statusArr['collectedCount'];
        $traitsCount = $statusArr['traitsCount'];
        // add link to data collection transactions browser

        $url = null;

        $occurrenceUrl = Url::toRoute([
            '/occurrence/view/plot', 
            'program' => Yii::$app->userprogram->get('abbrev'),
            'id' => $occurrenceId
        ]);

        $locationUrl = Url::toRoute([
            '/location/view/plot', 
            'program' => Yii::$app->userprogram->get('abbrev'),
            'id' => $id
        ]);

        if(!empty($collectedCount)){
            $url = (isset($occurrenceId) && !empty($occurrenceId)) ? $occurrenceUrl : $locationUrl;
        }

        return [
            'label' => 'Traits collected',
            'doneCount' => $collectedCount,
            'allCount' => $traitsCount,
            'status' => $status,
            'url' => $url,
            'urlTitle' => 'View plot data'
        ];

    }

    /**
     * Get traits status by Location
     *
     * @param Integer $id Occurrence identifier
     * @param Text $entity Whether Occurrence or Location
     * @return Array Traits status information
     */
    public function getTraitsStatusByEntity($id, $entity = 'occurrence'){
        // get trait protocol variable abbrevs
        $traitProtocolVarAbbrevs = $this->getTraitProtocolVarAbbrevsByEntityId($id, $entity);

        // get collected traits from the plots
        $actualTraitVarAbbrevs = $this->getActualTraitsByEntityId($id, $entity);

        // get unique trait var abbrevs
        $varAbbrevs = array_unique(array_merge($traitProtocolVarAbbrevs, $actualTraitVarAbbrevs));

        // get count of unique variables
        $totalVarCount = count($varAbbrevs);

        // get plot count
        $params[$entity.'DbId'] = (string) $id;
        $modelClass = ($entity == 'occurrence') ? 'app\models\Occurrence' : 'app\models\Location';
        $entityModel = \Yii::$container->get($modelClass)->searchAll($params, '', false);
        $plotCount = isset($entityModel['data'][0]['plotCount']) ? $entityModel['data'][0]['plotCount'] : 0;

        // set filter for occurrences and locations data search
        $filter = ($entity == 'occurrence') ? 'limit=1&sort=locationDbId:desc' : 'limit=1&plotDbId:desc';

        // retrieve number of plots with collected data
        $path = $entity.'s/'.$id.'/plot-data-search';
        $sumCollectedCount = $varPlotCount = 0;
        foreach ($varAbbrevs as $value) {
            $params = [
                'variableAbbrev' => (string) $value
            ];

            $plotData = Yii::$app->api->getParsedResponse('POST', $path, json_encode($params), $filter, false);

            // get total number of plots with collected data
            $varPlotCount = isset($plotData['totalCount']) ? $plotData['totalCount'] : 0;

            $sumCollectedCount = $sumCollectedCount + $varPlotCount;
        }

        // compute percentage
        $totalTraitsCount =  $plotCount * $totalVarCount;
        $statusPercentage = ($totalTraitsCount) ? ( $sumCollectedCount / $totalTraitsCount ) * 100 : 0;

        return [
            'status' => $statusPercentage,
            'collectedCount' => $sumCollectedCount,
            'traitsCount' => $totalTraitsCount
        ];

    }

    /**
     * Get trait protocol variable abbrevs of the Entity
     *
     * @param Integer $id Entity identifier
     * @param Text $entity Whether Occurrence or Location
     * @return Array $varAbbrevs List of variable abbrevs
     */
    public function getTraitProtocolVarAbbrevsByEntityId($id, $entity = 'occurrence'){

        if($entity == 'location'){
            // get variables from trait protocols of location
            $listIds = $this->exportWorkbook->getTraitProtocols($id, $entity);
            // get variable ids from the list
            $varAbbrevs = [];
            foreach ($listIds as $list) {
                $listMembers = $this->exportWorkbook->getListMembers($list);

                $varAbbrevsArr = !empty($listMembers) ? array_column($listMembers, 'abbrev') : [];

                $varAbbrevs = array_merge($varAbbrevs, $varAbbrevsArr);
            }

        } else {
            $varAbbrevs = [];
            $listAbbrev = 'TRAIT_PROTOCOL_LIST_ID';
            $params = [
                'occurrenceDbId' => "equals $id",
                'variableAbbrev' => "equals $listAbbrev",
                'fields' => 'occurrence_data.occurrence_id AS occurrenceDbId|variable.abbrev AS variableAbbrev|occurrence_data.data_value AS dataValue'
            ];

            $list = $this->occurrenceData->searchAll($params, 'limit=1', false);
            $listId = $list['data'][0]['dataValue'] ?? 0;

            // get list variables IDs
            $listMembers = $this->exportWorkbook->getListMembers($listId);

            // list of variable abbrevs
            $varAbbrevs = !empty($listMembers) ? array_column($listMembers, 'abbrev') : [];
        }

        return $varAbbrevs;

    }

    /**
     * Get actual collected traits variable abbrevs of the Entity
     *
     * @param Integer $id Entity identifier
     * @param Text $entity Whether Occurrence or Location
     */
    public function getActualTraitsByEntityId($id, $entity = 'occurrence'){

        // Set api request
        $path = $entity.'s/'.$id.'/plot-data-search';
        $params['distinctOn'] = 'variableAbbrev';

        $result = Yii::$app->api->getParsedResponse('POST', $path, json_encode($params), '', true);

        $variables = isset($result['data'][0]['plotData']) ? $result['data'][0]['plotData'] : [];

        // list of variable abbrevs
        $varAbbrevs = !empty($variables) ? array_column($variables, 'variableAbbrev') : [];

        return $varAbbrevs;
    }

    /**
     * Get seeds harvested status
     *
     * @param $id integer location identifier
     * @param $occurrenceId integer occurrence identifier
     * @return array status monitoring data for seeds harvested
     */
    public function getSeedsHarvestedStatus($locationId=null, $occurrenceId=null){
        // get seeds harvested status for Occurrence
        if(!empty($occurrenceId)){
            $seeds = $this->occurrenceManage->getSeedsHarvestedStatus(null, $occurrenceId);
        }else{
            $seeds = $this->occurrenceManage->getSeedsHarvestedStatus($locationId, null);
        }

        Yii::info('Got seeds harvested data ' . json_encode(['locationDbId'=>$locationId,
            'occurrenceDbId'=>$occurrenceId, 'seeds'=>$seeds]), __METHOD__);

        return $seeds;
    }
}