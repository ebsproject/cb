<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\location\controllers;

use app\components\B4RController;
use app\controllers\BrowserController;
use app\models\Api;
use app\models\Location;
use app\interfaces\models\IPerson;
use app\models\User;
use app\models\DataBrowserConfiguration;
use app\modules\location\models\FieldLayout;
use app\modules\occurrence\models\FileExport;
use Yii;

/**
 * Default controller for the `location` module
 */
class DefaultController extends B4RController
{
    public function __construct ($id, $module,
        protected Api $api,
        protected BrowserController $browserController,
        protected DataBrowserConfiguration $dataBrowserConfigurationModel,
        protected FieldLayout $fieldLayout,
        protected Location $locationModel,
        protected IPerson $person,
        $config=[]
    )
    { parent::__construct($id, $module, $config); }

  /**
   * Renders the browser for the occurrences
   *
   * @param $program text current program of the user
   * @param $filter text search filters
   * @param $category text current filter category
   * @param $selectedCategoryId integer selected category item
   * @param $selectedRecordId integer selected record item
   * @return mixed browser for occurrences
   */
  public function actionIndex($program, $filter = null, $category = null, 
  $selectedCategoryId=null, $selectedRecordId=null){

    $data = $this->locationModel->browse($filter, $category, $selectedCategoryId, null);

    $dataProvider = $data['dataProvider'];
    $totalCount = $data['totalCount'];

    // get grid view mode
    $gridMode = $this->getGridMode();

    return $this->render('index',[
      'dataProvider' => $dataProvider,
      'totalCount' => $totalCount,
      'program' => $program,
      'filter' => $filter,
      'category' => $category,
      'selectedCategoryId' => $selectedCategoryId,
      'selectedRecordId' => $selectedRecordId,
      'gridMode' => $gridMode
    ]);
  }

  /**
   * Load more pages for locations browser
   *
   * @param browser for other page of locations browser
   */
  public function actionLoadMore(){
    $filter = $_POST['filter'];
    $page = $_POST['page'];

    $data = Location::browse($filter, $category, null, $page);
    $dataProvider = $data['dataProvider'];
    $totalCount = $data['totalCount'];

    // get grid view mode
    $gridMode = $this->getGridMode();

    return Yii::$app->controller->renderPartial('browser.php',[
      'dataProvider' => $dataProvider,
      'totalCount' => $totalCount,
      'page' => $page,
      'gridMode' => $gridMode
    ]);
  }

  /**
   * Get location browser grid mode
   *
   * @return text $gridMode whether grid or detail
   */
  public function getGridMode(){
    $gridMode = 'detail';

    $userModel = new User();
    $userId = $userModel->getUserId();
    $dataBrowserId = 'locations-grid';

    $searchParams = [
      'userDbId' => (string)$userId,
      'dataBrowserDbId' => $dataBrowserId,
      'type' => 'view'
    ];

    // check if configuration already exists
    $config = $this->dataBrowserConfigurationModel->searchAll($searchParams);

    if(isset($config['data'][0]['data']['view'])){
      $gridMode = $config['data'][0]['data']['view'];
    }

    return $gridMode;
  }

    /**
     * Export a Planting Instructions file given a Location ID
     *
     * @param int|string $locationDbId Location identifier
     * @param string $fileType file format
     */
    public function actionExportPlantingInstructionsOfLocation ($locationDbId, $fileType = 'csv', $program='', $redirectUrl='')
    {
        $redirectUrl = empty($redirectUrl) ? "index?program={$program}" : $redirectUrl;

        Yii::info('Exporting file '.json_encode(['locationDbId'=>$locationDbId, 'fileType'=>$fileType,
                'program'=>$program, 'redirectUrl'=>$redirectUrl]), __METHOD__);

        // Get this Location's info
        $params = [
            'locationDbId' => (string) $locationDbId
        ];

        $response = $this->api->getApiResults(
            'POST',
            'locations-search',
            json_encode($params),
            '?limit=1',
            true
        );
        if (empty($response['success'])) {
            $this->redirect($redirectUrl)->send();
            exit;
        }

        // If Location exists
        if (!empty($response)) {
            // Prepare file for export
            $locationCode = $this->fieldLayout->getValidFileName($response['data'][0]['locationCode']);
            $toExport = new FileExport(FileExport::PLANTING_INSTRUCTIONS_LOCATION, FileExport::CSV, time(),
                location: $locationCode);
            $filename = $toExport->getZippedFilename();  // returns .zip filename
            $filename = substr_replace($filename, 'csv', -3);

            // Build CSV data
            $this->buildPlantingIntructionsOfLocation($program, $locationDbId, $filename);
        }

        exit;
    }

    /**
     * Build content in export Planting Instructions of a Location
     *
     * @param int|string $locationDbId Location identifier
     * @param string $fileName file format
     * @param string $program system program
     */
    public function buildPlantingIntructionsOfLocation ($program, $locationDbId, $fileName)
    {
        [
            $role,
        ] = $this->person->getPersonRoleAndType($program);

        [
            $csvAttributes,
            $csvHeaders,
        ] = $this->browserController->getColumns(
                ($role == 'COLLABORATOR') ? $role : $program,
                'DOWNLOAD_PLANTING_INSTRUCTION_FILES',
                'location',
                mode: 'download'
            );

        // Retrieve Location's Planting Instructions
        $plantingIns = $this->api->getApiResults(
            'GET',
            'locations/' . $locationDbId . '/planting-instructions',
            null,
            'sort=occurrenceName|plotNumber',
            true
        );

        $plantingInsArr = [[]];
        $i = 1;

        if (!empty($plantingIns['data']) && isset($plantingIns['data'])) {
            // Headers
            foreach ($csvHeaders as $value) {
                array_push($plantingInsArr[0], $value);
            }

            // Loop through each plot records
            foreach ($plantingIns['data'] as $value) {
                $plantingInsArr[$i] = [];

                // All plot attributes
                foreach ($csvAttributes as $v) {
                    array_push($plantingInsArr[$i], $value[$v]);
                }

                $i += 1;
            }
        } else {
            $plantingInsArr = [['There are no planting instruction records found.']];
        }

        // Set headers
        header('Content-Type: application/csv');
        header("Content-Disposition: attachment; filename={$fileName};");

        // Write to memory and then export to web browser
        $fp = fopen('php://output', 'w');

        if (!$fp) {
            die('Failed to create CSV file.');
        }

        foreach ($plantingInsArr as $line) {
            fputcsv($fp, $line);
        }

        fclose($fp);
    }
}
