<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\location\controllers;

use app\components\B4RController;

use app\controllers\BrowserController;
use app\models\Location;
use app\models\Occurrence;
use app\modules\location\models\FieldLayout;
use app\modules\occurrence\models\FileExport;
use app\modules\occurrence\models\LocationPlot;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\occurrence\models\SaveList;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Yii;
use yii\helpers\Url;

/**
 * Controller for the view location
 */
class ViewController extends B4RController
{
    public function __construct($id, $module,
        protected BrowserController $browserController,
        protected ExperimentModel $expModel,
        protected FieldLayout $fieldLayout,
        protected Location $location,
        protected LocationPlot $locationPlot,
        protected Occurrence $occurrence,
        protected SaveList $saveListModel,
        $config = []
    )
    { parent::__construct($id, $module, $config); }

    /**
     * View location basic information
     *
     * @param text $program program identifier
     * @param integer $id location identifier
     */
    public function actionBasic($program, $id)
    {

        $data = $this->location->getDetailViewInfo($id);

        // Get location name
        $locationResponse = $this->occurrence->searchAll(
            [ 'locationDbId' => "equals $id" ],
            'limit=1'
        );

        if (!$locationResponse['success'] || !$locationResponse['data']) {
            throw new \Exception('Location not found.');
        }
        
        $locationName = $locationResponse['data'][0]['location'];

        return $this->render('basic', [
            'program' => $program,
            'id' => $id,
            'data' => $data,
            'locationName' => $locationName,
        ]);
    }

    /**
     * View location status monitoring information
     *
     * @param integer $id occurrence identifier
     */
    public function actionRenderLocationStatusMonitoringInfo($id)
    {
        $data = '';
        $monitoringInfo = $this->location->getMonitoringInfo($id);

        if(!empty($monitoringInfo) && isset($monitoringInfo)){
            $data = '<div class="viewer-monitoring-info">';

                // loop through all status monitoring data
                foreach ($monitoringInfo as $key => $value) {

                    if(isset($value['label'])){
                        $doneCount = isset($value['doneCount']) ? $value['doneCount'] : 0;
                        $allCount = isset($value['allCount']) ? $value['allCount'] : 0;
                        $statusVal = isset($value['status']) ? $value['status'] : 0;
                        $url = isset($value['url']) && !empty($value['url']) ? $value['url'] : '';
                        $progressUrl = $progressUrlEnd = '';

                        if(!empty($url)){
                            $urlTitle = isset($value['urlTitle']) && !empty($value['urlTitle']) ? $value['urlTitle'] : '';

                            $progressUrl = '<a href="'.$url.'" title="'.$urlTitle.'">';
                            $progressUrlEnd = '</a>';
                        }

                        $data .= '<div class="col col-lg-6 status-monitoring">'.
                            '<div class="progress-label">'.
                                $value['label'] .
                            '</div>'.
                            $progressUrl.
                            '<div class="progress-info-panel">'.
                                '<div class="progress-summary text-muted">'.
                                    $doneCount . ' of ' . $allCount . ' (' . round($statusVal, 2) . '%)'.
                                '</div>'.
                                '<div class="progress">'.
                                    '<div class="determinate" style="width: '.$statusVal.'%"></div>'.
                                '</div>'.
                            '</div>'.
                            $progressUrlEnd.
                        '</div>';
                    }
                }

            $data .= '</div>';
        }

        return json_encode($data);
    }

    /**
     * View plots of the location
     *
     * @param text $program program identifier
     * @param integer $id location identifier
     */
    public function actionPlot($program, $id)
    {

        $searchModel = $this->locationPlot;
        $params = Yii::$app->request->queryParams;
        $params['LocationPlot']['locationDbId'] = $id;

        $output = $searchModel->search($params);
        $traits = $searchModel->traits;

        $configCols = $this->browserController->getColumns($program, 'VIEW_LOCATION_DATA', 'VIEW_PLOT_COLUMNS', [], 'view');

        // Get location name
        $locationResponse = $this->occurrence->searchAll(
            [ 'locationDbId' => "equals $id" ],
            'limit=1'
        );

        if (!$locationResponse['success'] || !$locationResponse['data']) {
            throw new \Exception('Location not found.');
        }
        
        $locationName = $locationResponse['data'][0]['location'];

        return $this->render('plot', [
            'program' => $program,
            'id' => $id,
            'searchModel' => $searchModel,
            'dataProvider' => $output[0],
            'traits' => $traits,
            'totalCount' => $output[1],
            'paramSort' => $output[2],
            'filters' => $output[3],
            'exportFilters' => $output[4],
            'configCols' => $configCols,
            'locationName' => $locationName,
        ]);
    }

    /**
     * View location field layout
     *
     * @param text $program program identifier
     * @param integer $id location identifier
     */
    public function actionLayout($program, $id)
    {
        // Get location name
        $locationResponse = $this->occurrence->searchAll(
            [ 'locationDbId' => "equals $id" ],
            'limit=1'
        );

        if (!$locationResponse['success'] || !$locationResponse['data']) {
            throw new \Exception('Location not found.');
        }
        
        $locationName = $locationResponse['data'][0]['location'];

        return $this->render('layout', [
            'program' => $program,
            'id' => $id,
            'locationName' => $locationName,
        ]);
    }

    /**
     * @param Integer $id Location identifier
     * @param Text $label Field to be displayed
     * @param Boolean $flipped Starting position whether top or bottom
     * @param Integer $occurrenceDbId Occurrence identifier
     *
     * @return Array field layout of an Occurrence
     */
    public function actionRenderLayout($id, $label='plotno', $flipped=true, $occurrenceDbId=null){
        $panelWidth = $_POST['panelWidth'] ?? 0;

        // retrieve occurrences
        $data = $this->fieldLayout->getLayoutDataByLocationId($id, 'field');
     
        // check first plot position
        $flipped = $this->fieldLayout->getLayoutFirstPlotPosition($id, 'location');

        // if Location is invalid
        if(empty($data)){
            $htmlData = $this->renderPartial('_layout_preview.php',[
                'data' => 'invalid_location',
            ],true,false);

            return json_encode([
                'success'=> 'success',
                'htmlData' => $htmlData,
            ]);
        }
    
        $layoutArray = isset($data['layout']) ? $data['layout'] : null;
        $occurrenceName = isset($data['occurrenceName']) ? $data['occurrenceName'] : null;
        $occurrenceDbId = isset($data['occurrenceDbId']) ? $data['occurrenceDbId'] : null;
        $occurrencesList = isset($data['occurrencesList']) ? $data['occurrencesList'] : null;

        $htmlData = $this->renderPartial('_layout_preview.php',[
            'data' => isset($layoutArray['dataArrays']) ? $layoutArray['dataArrays'] : null,
            'label'=> $label,
            'startingPlotNo' => 1,
            'panelWidth' => $panelWidth,
            'noOfRows' => $layoutArray['maxRows'] == NULL ? 0:$layoutArray['maxRows'],
            'noOfCols' => $layoutArray['maxCols'] == NULL ? 0:$layoutArray['maxCols'],
            'statusDesign' => 'completed',
            'deletePlots' => 'true',
            'planStatus' => 'randomized',
            'flipped' => $flipped,
            'occurrencesList' => $occurrencesList,
            'occurrenceName' => $occurrenceName,
            'occurrenceDbId' => $occurrenceDbId,
            'xAxis' => $layoutArray['xAxis'],
            'yAxis' => $layoutArray['yAxis'],
            'maxBlk' => $layoutArray['maxBlk']
        ],true,false);

        return json_encode([
            'success'=> 'success',
            'htmlData' => $htmlData,
        ]);
    }

    /**
     * Update the layout view based on the selected occurrence
     * 
     * @return array layout information
     */
    public function actionChangeOccurrenceLayout(){

        $occurrenceDbId = $_POST['occurrenceDbId'];

        $fullScreen = false;
        if($_POST['fullScreen'] == 1){
            $fullScreen = true;
        }

        try {
            $layoutArray = $this->expModel->generateLayout($occurrenceDbId, null, $fullScreen, 'field');
        } catch (yii\base\ErrorException $err) {
            Yii::error($err, __METHOD__);
            $program = Yii::$app->session->get('program');
            $this->redirect(Url::to("layout?program={$program}&id={$occurrenceDbId}"))->send();
            exit();
        }


        return json_encode($layoutArray);
    }

    /**
     * Download field layout in Excel format
     *
     * @param Integer $id Occurrence identifier
     * @param string $label Label displayed in Layout preview
     * @param string $type Type of Layout (design, field, etc.)
     */
    public function actionDownloadLayout($id, $label='plotno', $type = 'field'){
        Yii::info('Exporting file '.json_encode(['occurrenceDbId'=>$id, 'previewLabel'=>$label, 'type'=>$type]), __METHOD__);

        // get layout data
        try {
            $data = $this->fieldLayout->getDownloadLayoutData($id,'occurrence',$type);
        } catch (yii\base\ErrorException $err) {
            Yii::error($err, __METHOD__);
            $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl)->send();
            exit();
        }

        extract($data);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        if($maxRows != NULL || $maxRows > 0){
            $repColors[1]='#c5e1a5';
            
            for($i=2; $i<=$maxRep; $i++){
                $percent = ($i/(int)$maxRep)*100;
                
                $repColors[$i] = $this->expModel->adjustBrightness($repColors[1], ($i/(int) $maxRep)*65, true);
            }

            // set layout
            $searchPlotsParams = [
                'fields' => 'plot.plot_number AS plotNumber|entry.entry_number AS entryNumber|
                    plot.plot_qc_code AS plotQcCode|plot.block_number AS blockNumber|
                    entry.entry_type AS entryType|plot.occurrence_id AS occurrenceDbId|plot.rep AS rep|
                    plot.design_x AS designX|plot.design_y AS designY|plot.field_x AS fieldX|plot.field_y AS fieldY|
                    plot.plot_code AS plotCode'
            ];

            $plotRecords = $this->occurrence->searchPlots($id, $searchPlotsParams,'sort=plotNumber:ASC', true);

            foreach($plotRecords['data'] as $plots){

                if($label == 'plotno'){
                    $value = $plots['plotNumber'];
                } else if ($label == 'entno'){
                    $value = $plots['entryNumber'];
                } else if ($label == 'plotcode'){
                    $value = $plots['plotCode'];
                } else if ($label == 'block'){
                    $value = $plots['blockNumber'];
                }
                $repValue = $plots['rep'];

                $columnLetter = $this->fieldLayout->columnLetter($plots[$xAttribute]);
                $yAxis = $plots[$yAtrribute];
                $cell = $columnLetter . $yAxis;

                $sheet->setCellValue($cell, $value);

                $color = substr($repColors[$repValue], 1);

                if($repValue > 1){
                    $fontColor = 'FFFFFF';
                }else{
                    $fontColor = '000000';
                }

                if($plots['entryType'] == 'check'){
                    $color = 'e67e00';
                    $fontColor = 'FFFFFF';
                }
                
                // apply styling
                $style = $sheet->getStyle($cell);
                $border = Border::BORDER_THIN;
                $styleArray = [
                    'font' => [
                        'size' => 9,
                        'color' => ['rgb' => $fontColor]
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => $border,
                        ],
                        'bottom' => [
                            'borderStyle' => $border,
                        ],
                        'right' => [
                            'borderStyle' => $border,
                        ],
                        'left' => [
                            'borderStyle' => $border,
                        ]
                    ],
                    'fill' => [
                        'fillType' => Fill::FILL_SOLID,
                        'startColor' => [
                            'rgb' => $color,
                        ],
                    ],
                ];
                $style->applyFromArray($styleArray);
            }
        }

        // Prepare to export file
        if ($type == 'field') {
            // Field Layout
            $downloadFeature = 'FIELD_LAYOUT';
        } else {
            // Design Layout
            $downloadFeature = 'DESIGN_LAYOUT';
        }
        
        // get file name from config by occurrence ID
        $filename = $this->browserController->getFileNameFromConfigByEntityId($id, $downloadFeature, FileExport::XLSX);

        chdir('files');
        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment; filename={$filename};");

        $writer->save('php://output');

        chdir('../');    

        exit();
    }

    /**
     * Search for plot records associated with a location
     * 
     * @param Integer $locationId id of location
     * @param String $entity entity to be retrieved
     * @param String $listType type of target list
     * 
     * @return Array $plotIdsArr array of plot ids
     */
    public function actionRetrieveDbIds($locationId, $entity){
        $plotIdsArr = [];
        
        if(isset($_POST) && !empty($_POST)){

            $listType = isset($_POST['type']) && !empty($_POST['type']) ? $_POST['type'] : '';
            $selectedItemsStr = isset($_POST['items']) && !empty($_POST['items']) ? $_POST['items'] : '';
            $entity = isset($entity) && !empty($entity) ? $entity : 'plot';
            $attribute = $entity."DbId";

            $results = $this->locationPlot->getLocationPlotRecords($locationId,$entity,$attribute,$listType,$selectedItemsStr);

            if(isset($results) && !empty($results)){
                $plotIdsArr = array_column($results,$attribute);
            }
        }
        return json_encode($plotIdsArr);
    }

    /**
     * Facilitate saving of plots to list
     * 
     * @param Integer $locationId id of associated location
     * @param String $entity entity of list
     * 
     * @return Array success status object
     */
    public function actionSaveAsList($locationId, $entity){
        if(isset($_POST) && !empty($_POST)){

            $listType = isset($_POST['type']) && !empty($_POST['type']) ? $_POST['type'] : '';
            $abbrev = isset($_POST['abbrev']) && !empty($_POST['abbrev']) ? $_POST['abbrev'] : '';
            $name = isset($_POST['name']) && !empty($_POST['name']) ? $_POST['name'] : '';
            $displayName = isset($_POST['displayName']) && !empty($_POST['displayName']) ? $_POST['displayName'] : '';
            $description = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description'] : '';
            $remarks = isset($_POST['remarks']) && !empty($_POST['remarks']) ? $_POST['remarks'] : '';
            $isSelectAllChecked = isset($_POST['isSelectAllChecked']) && !empty($_POST['isSelectAllChecked']) ? $_POST['isSelectAllChecked'] : '';
            $dataType = isset($_POST['dataType']) && !empty($_POST['dataType']) ? $_POST['dataType'] : '';  
            $dbIds = isset($_POST['dbIds']) && !empty($_POST['dbIds']) ? json_decode($_POST['dbIds']) : [];

            $attribute = $entity."DbId";
            $selectAllMode = $_POST['selectAllMode'] ?? 'include';
            $filterObject = null;

            if(!isset($dataType) || empty($dataType)){
                return [
                    'success' => false,
                    'message' => 'The system could not determine which type of data it is going to save as a list.'
                ];
            }

            if(!empty($listType) && !empty($abbrev) && !empty($name) && !empty($displayName)){
                $listParams = [
                    'dataType' => $dataType,
                    'listType' => $listType,
                    'abbrev' => $abbrev,
                    'name' => $name,
                    'displayName' => $displayName,
                    'description' => $description,
                    'remarks' => $remarks,
                    'isSelectAllChecked' => $isSelectAllChecked
                ];

                $table = 'plot';
                $endpoint = 'plot-data-tables';
                // retrieve filters from either isSelectAllChecked flag, session, or $_POST
                $filters = Yii::$app->session->get('location-'.$table.'-filters' . $locationId);

                if ( !empty($filters) ) {
                    if ( isset($filters['entryName']) ) {
                        // Rename "entryName" key to "displayValue"
                        $filters['displayValue'] = $filters['entryName'];
                        // Remove old key "entryName"
                        unset($filters['entryName']);
                    }
                    $filterObject = [
                        'filters' => $filters,
                        'additionalFields' => $this->generateFields(
                            array_keys($filters)
                        ),
                    ];
                }

                $saveData = $this->saveListModel->saveAsList('location',$locationId, $listParams, $dbIds, $selectAllMode, $filterObject);
                return json_encode($saveData);
            }
        }
    }

    /**
     * Generate string of fields for API request body
     *
     * @param array $attributes list of specified attributes
     * 
     * @return string generated string of fields
     */
    public function generateFields($attributes) {
        $locationPlotDataTableAttributes = [
            'plotDbId' => 'plot.id AS plotDbId',
            'entryDbId' => 'plot.entry_id AS entryDbId',
            'entryCode' => 'pi.entry_code AS entryCode',
            'entryNumber' => 'pi.entry_number AS entryNumber',
            'entryName' => 'pi.entry_name AS entryName',
            'entryType' => 'pi.entry_type AS entryType',
            'entryRole' => 'pi.entry_role AS entryRole',
            'entryClass' => 'pi.entry_class AS entryClass',
            'entryStatus' => 'pi.entry_status AS entryStatus',
            'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
            'germplasmCode' => 'g.germplasm_code AS germplasmCode',
            'parentage' => 'g.parentage',
            'generation' => 'g.generation',
            'germplasmState' => 'g.germplasm_state AS germplasmState',
            'germplasmType' => 'g.germplasm_type AS germplasmType',
            'seedDbId' => 'pi.seed_id AS seedDbId',
            'seedCode' => 'seed.seed_code AS seedCode',
            'seedName' => 'seed.seed_name AS seedName',
            'packageDbId' => 'pi.package_id AS packageDbId',
            'packageCode' => 'package.package_code AS packageCode',
            'packageLabel' => 'package.package_label AS packageLabel',
            'plotCode' => 'plot_code AS plotCode',
            'plotNumber' => 'plot_number AS plotNumber',
            'plotType' => 'plot_type AS plotType',
            'rep' => 'rep',
            'designX' => 'design_x AS designX',
            'designY' => 'design_y AS designY',
            'plotOrderNumber' => 'plot_order_number AS plotOrderNumber',
            'paX' => 'pa_x AS paX',
            'paY' => 'pa_y AS paY',
            'fieldX' => 'field_x AS fieldX',
            'fieldY' => 'field_y AS fieldY',
            'plotStatus' => 'plot_status AS plotStatus',
            'plotQcCode' => 'plot_qc_code AS plotQcCode',
            'creationTimestamp' => 'pi.creation_timestamp AS creationTimestamp',
            'creatorDbId' => 'creator.id AS creatorDbId',
            'creator' => 'creator.person_name AS creator',
            'modificationTimestamp' => 'plot.modification_timestamp AS modificationTimestamp',
            'modifierDbId' => 'modifier.id AS modifierDbId',
            'modifier' => 'modifier.person_name AS modifier',
        ];

        $fields = '';

        foreach ($attributes as $value) {
            if (!array_key_exists($value, $locationPlotDataTableAttributes))
                continue;

            $fields .= "{$locationPlotDataTableAttributes[$value]} |";
        }

        $fields = rtrim($fields, '|');

        return $fields;
    }
}