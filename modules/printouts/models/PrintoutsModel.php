<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\printouts\models;

use app\interfaces\models\ICross;
use app\interfaces\models\IOccurrence;
use app\models\Printout;

/**
 * Model file for PrintoutsModel
 */
class PrintoutsModel extends Printout
{
    /**
     * Model constructor
     */
    public function __construct(
        public IOccurrence $occurrence,
        public ICross $cross,
    ) {
    }

    /**
     * Retrieves occurrence data and applies format needed
     * for select2 data.
     * 
     * @param Array $selectedIds list of selected ids
     * @param String $entity name of entity: occurrence or cross list level
     * 
     * @return Array containing occurrences in select2 data format and occurrence count
     */
    public function getOccurrenceSelect2Data($selectedIds, $entity = 'occurrence')
    {
        if ($entity == 'cross list') {
            $crossListData = $this->cross->searchAll([
                'distinctOn' => 'occurrenceDbId',
                'entryListDbId' => 'equals ' . implode('|equals ', $selectedIds),
            ])['data'] ?? [];

            $selectedIds = array_column($crossListData, 'occurrenceDbId');
        }

        // Retrieve occurrence dropdown data
        $result = $this->occurrence->searchAll([
            "fields" => "occurrence.id AS occurrenceDbId|occurrence.occurrence_name AS occurrenceName",
            "occurrenceDbId" => 'equals ' . implode(' | equals ', $selectedIds),
        ]);
        $occurrenceData = isset($result['data']) ? $result['data'] : [];
        $occurrenceCount = isset($result['totalCount']) ? $result['totalCount'] : 0;

        // Format data for select2
        $occurrences = [];
        foreach ($occurrenceData as $data) {
            $id = $data['occurrenceDbId'];
            $text = $data['occurrenceName'];
            $occurrences[$id] = $text;
        }

        // Return result
        return [
            'occurrences' => $occurrences,
            'occurrenceCount' => $occurrenceCount
        ];
    }

    /**
     * Retrieve the parameters of the report given the name
     * 
     * @param String name of the report template
     */
    public function getReportParameters($templatesNames)
    {
        $parameterArray = [];

        foreach ($templatesNames as $templatesName) {
            $result = $this->getParameters($templatesName);
            $parameterArray[$templatesName] = $result;
        }

        return $parameterArray;
    }
}
