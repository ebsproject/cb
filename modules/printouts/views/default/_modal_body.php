<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Url;
use kartik\widgets\Select2;

if ($csApiStatus && $csApiStatus === '500') {
    echo Yii::t('app', 'Printouts server is unreachable at the moment. Please try again or file a report for assistance.');
    return;
}

// Define URLs
$urlDownloadGeneratedReport = Url::toRoute(['/printouts/default/download-generated-report']);

// Variables
$dbIds = implode(",", $entityDbIds);

// Render modal UI
echo '<p>Select a template and file type to download.</p>';
echo '<div style="padding-left:20px; padding-bottom:20px;">';
echo '<div style="width:95%">';

// Template
echo '<label class="control-label" style="margin-top:10px">' . \Yii::t('app', 'Template' . ' ' . '<span class="required">*</span>') . '</label>';
echo Select2::widget([
    'name' => 'TemplateSelection',
    'id' => 'printouts-input-template',
    'data' => $templatesData,
    'value' => '',
    'options' => [
        'placeholder' => 'Select a template',
        'disabled' => false,
        'tags' => true,
        'class' => 'printouts-input',
        'style' => 'overflow-x: visible; overflow-y: visible;width:88%;',
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 0
    ],
]);

// File type
echo '<label class="control-label" style="margin-top:10px;">' . \Yii::t('app', 'File type' . ' ' . '<span class="required">*</span>') . '</label>';
echo Select2::widget([
    'name' => 'FileTypeSelection',
    'id' => 'printouts-input-file-type',
    'data' => $fileTypes,
    'value' => 'pdf',
    'options' => [
        'placeholder' => 'Select file type',
        'tags' => true,
        'class' => 'printouts-input',
        'style' => 'overflow-x: visible; overflow-y: visible;width:88%;',
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 0
    ],
]);

// Format variable name from entity name
$entityParts = explode(" ", $entityName);
for ($i = 1; $i < count($entityParts); $i += 1) {
    $entityParts[$i] = ucfirst($entityParts[$i]);
}
$entityIdFieldName = implode("", $entityParts) . "DbId";

// If request parameters is empty, build.
if (empty($requestParameters)) {
    $requestParameters[$entityIdFieldName] = $dbIds;
}
// JSON encode arrays
$requestParametersJSON = json_encode($requestParameters);
$reportParametersJSON = json_encode($reportParameters);

// Remder info div
echo '
    <div
        class="fus_summary"
        id="file-upload-summary-data-level-info"
    >
        <p>
            Generating reports for the <strong>' . $entityName . '</strong> record.<br><br>' .
    '<strong>Note:</strong> Use <code>' . $entityIdFieldName . '</code> as parameter when creating templates in the Printouts Manager.
        </p>
    </div>
';

echo '</div></div>';

$script = <<< JS

$("document").ready(function(){
    // select2 change event
    $(document).on("change",".printouts-input", function(e) {
        var value = $(this).val();
        var complete = true;
        
        // Check if all select2 have input
        $(".printouts-input").each(function( index ) {
            if ($(this).val() === '') complete = false;
        });

        // Enable/disable download button
        if (complete) {
            $('#printouts-modal-download-button').removeClass('disabled');
        }
        else {
            $('#printouts-modal-download-button').addClass('disabled');
        }
    });

    // Download button click event
    $('#printouts-modal-download-button').off('click').on('click', function() {
        // Hide system loading indicator
        $('#system-loading-indicator').css('display','none');
        // Variables
        var dbIds = '$dbIds';
        var template = $('#printouts-input-template').val();
        var format = $('#printouts-input-file-type').val();
        var displayName = '$entityDisplayName';
        var fileName = template + " - " + displayName + "." + format;

        // Check if the requestParameters match the report's parameters
        var requestParameters = $requestParametersJSON;
        var requestParamNames = Object.keys(requestParameters);
        var reportParameters = $reportParametersJSON;
        var currentTemplateSupported = reportParameters[template];
        var hasSupportedParam = false;

        // Check the param names in the request parameters
        for(var paramName of requestParamNames) {
            if(currentTemplateSupported.includes(paramName)) hasSupportedParam = true;
        }

        // If the template has no params, set flag to true
        if(currentTemplateSupported.length == 0) hasSupportedParam = true;

        // If the selected report template does not contain
        // any of the params in the request, display error message
        if(!hasSupportedParam) {
            var notif = "<i class='material-icons red-text'>close</i>&nbsp;The selected template does not support $entityName records.";
            Materialize.toast(notif, 5000);
            return;
        }

        // Generate the report
        var notif = "<i class='material-icons blue-text'>info</i>&nbsp;Generating report...";
        Materialize.toast(notif, 2500);

        $('#system-loading-indicator').css('display','block');
        setTimeout(function() {
            $('#system-loading-indicator').css('display','none');
        },3000);

        // Build download url
        var downloadURL = window.location.origin
            + '$urlDownloadGeneratedReport?'
            + 'template=' + template + '&'
            + 'parameters=' + JSON.stringify(requestParameters) + '&'
            + 'format=' + format + '&'
            + 'fileName=' + fileName;
        

        // Download the file
        window.location.assign(downloadURL);
    });

    // Display and hide loading indicator
    $(document).on('click','.hide-loading',function(){
        $('#system-loading-indicator').css('display','block');
        setTimeout(function() {
            $('#system-loading-indicator').css('display','none');
        },3000);
    });
});


JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    .disabled{
        pointer-events: none;
    }

    code {
        color: #424242;
        background-color: #e0e0e0;
    }

    .fus_summary {
        padding-left: 10px;
        padding-top: 10px;
        padding-right: 10px;
        padding-bottom: 10px;
        border-radius: 5px;
        margin-top: 20px;
        background: #d9edf7;
        color: #31708f;
    }
');
