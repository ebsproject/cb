<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Url;
use kartik\widgets\Select2;

if ($csApiStatus && $csApiStatus === '500') {
    echo Yii::t('app', 'Printouts server is unreachable at the moment. Please try again or file a report for assistance.');
    return;
}

// Define URLs
$generateReportUrl = Url::toRoute(['/printouts/default/generate-report']);
$downloadReportUrl = Url::toRoute(['/printouts/default/download-report']);
$setSelectedOccurrenceIdsUrl = Url::toRoute(['/printouts/default/set-selected-occurrence-ids']);

// If only one occurrence is available, set as default value
$defaultValue = $occurrenceCount == 1 ? array_keys($occurrences)[0] : '';

// Render modal UI
echo '<p>Select an occurrence and a template to download. Occurrences are downloaded into a single file.</p>';
echo '<div style="padding-left:20px; padding-bottom:20px;">';
echo '<div style="width:95%">';

// Occurrence
echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Occurrence'.' '.'<span class="required">*</span>').'</label>';
echo Select2::widget([
    'name' => 'OccurrenceSelection',
    'id' => 'printouts-input-occurrence',
    'data' => $occurrences,
    'value' => $defaultValue,
    'options' => [
        'placeholder' => 'Select an occurrence',
        'disabled' => $occurrenceCount == 1,
        'tags' => true,
        'class'=>'printouts-input',
        'style' => 'overflow-x: visible; overflow-y: visible;width:88%;',   
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 0
    ],
]);


if($occurrenceCount > 1){
    $note = "($occurrenceCount)";
    $disabledSelect = '';
    // get threshold for occurrences limit
    $config = \Yii::$container->get('app\models\Config')->getConfigByAbbrev('PRINTOUTS_MAX_NUMBER_OF_OCCURRENCES');
    $occThreshold = (isset($config) && !empty($config)) ? intval($config) : 100;

    if($occurrenceCount > $occThreshold){
        $note = "(selected occurrences exceeds $occThreshold)";
        $disabledSelect = 'disabled="disabled"';
    }

    //Select All occurrences option
    echo '<div style="margin-top:10px;">
            <input type="checkbox" class="filled-in" id="selectall-occ" '.$disabledSelect.'>
            <label for="selectall-occ">Select all '.\Yii::t('app',$note).'</label>
        </div>';
}


// Template
echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Template'.' '.'<span class="required">*</span>').'</label>';
echo Select2::widget([
    'name' => 'TemplateSelection',
    'id' => 'printouts-input-template',
    'data' => $templatesData,
    'value' => '',
    'options' => [
        'placeholder' => 'Select a template',
        'disabled' => false,
        'tags' => true,
        'class'=>'printouts-input',
        'style' => 'overflow-x: visible; overflow-y: visible;width:88%;',   
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 0
    ],
]);

// File type
echo '<label class="control-label" style="margin-top:10px;">'.\Yii::t('app','File type'.' '.'<span class="required">*</span>').'</label>';
echo Select2::widget([
    'name' => 'FileTypeSelection',
    'id' => 'printouts-input-file-type',
    'data' => $fileTypes,
    'value' => 'pdf',
    'options' => [
        'placeholder' => 'Select file type',
        'tags' => true,
        'class'=>'printouts-input',
        'style' => 'overflow-x: visible; overflow-y: visible;width:88%;',   
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 0
    ],
]);

echo '</div>';

$occurrencesJSON = json_encode($occurrences);

$script = <<< JS
var occurrences = $occurrencesJSON;
var selectedAll = false;
var generateReportUrl = '$generateReportUrl';
var downloadReportUrl = '$downloadReportUrl';
var setSelectedOccurrenceIdsUrl = '$setSelectedOccurrenceIdsUrl';

$("document").ready(function(){
    // select2 change event
    $(document).on("change",".printouts-input", function(e) {
        var value = $(this).val();
        var complete = true;
        
        // Check if all select2 have input
        $(".printouts-input").each(function( index ) {
            if (this.id == 'printouts-input-occurrence'){ 
                if($(this).val() === '' && !selectedAll) complete = false;
            }
            else {
                if($(this).val() === '') complete = false;
            }
        });

        // Enable/disable download button
        if (complete) {
            $('#printouts-modal-download-button').removeClass('disabled');
        }
        else {
            $('#printouts-modal-download-button').addClass('disabled');
        }
    });

    // Download button click event
    $('#printouts-modal-download-button').off('click').on('click', function() {
        let occurrenceIds = $('#printouts-input-occurrence').val();
        let template = $('#printouts-input-template').val();
        let format = $('#printouts-input-file-type').val();
        let occurrenceName = occurrences[occurrenceIds];

        if (selectedAll) {
            occurrenceName = "selected-occurrences";

            // Store selected occurrence IDs to session
            $.ajax({
                type: 'POST',
                url: setSelectedOccurrenceIdsUrl,
                data: {
                    selectedOccurrenceIds: Object.keys(occurrences).join(",")
                },
                success: function () {
                    occurrenceIds = 0
                },
                error: function (jqXHR, textStatus, errorThrown) {}
            })
        }

        let fileName = occurrenceName + "-" + template + "." + format;
        const occurrenceIdParam = (occurrenceIds) ? (`occurrenceId=\${occurrenceIds}&`) : ''

        // Build URL for actionGenerateReport
        let url = generateReportUrl + '?'
            + 'template=' + template + '&'
            + occurrenceIdParam
            + 'format=' + format + '&'
            + 'fileName=' + fileName;

        // Generate report then download
        $.ajax({
            url,
            success: function (response) {
                response = JSON.parse(response)

                if (response.success) {
                    const downloadUrl = window.location.origin
                        + downloadReportUrl

                    // Download the file
                    window.location.assign(downloadUrl);
                } else {
                    $('.toast').css('display','none')
                    const notif = `
                        <i class='material-icons red-text left'>close</i>
                        \${response.message}
                    `
                    Materialize.toast(notif,5000)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {}
        })
    });

    // Display and hide loading indicator
    $(document).on('click','.hide-loading',function(){
        $('#system-loading-indicator').css('display','block');
        setTimeout(function() {
            $('#system-loading-indicator').css('display','none');
        },3000);
    });

    $(document).on("click","#selectall-occ", function(e) {
        selectedAll = $(this).is(":checked");

        if(selectedAll){
            // var resetSelect = document.getElementById("printouts-input-occurrence");
            // resetSelect.selectedIndex = -1;
            $('#printouts-input-occurrence').val(null);
            $('#printouts-input-occurrence').trigger('change');
            $('#printouts-input-occurrence').attr('disabled','disabled');

        } else {
            $('#printouts-input-occurrence').removeAttr('disabled');

            var complete = true;
            // Check if all select2 have input
            $(".printouts-input").each(function( index ) {
                if (this.id == 'printouts-input-occurrence'){ 
                    if($(this).val() === '' && !selectedAll) complete = false;
                }
                else {
                    if($(this).val() === '') complete = false;
                }
            });

            // Enable/disable download button
            if (complete) {
                $('#printouts-modal-download-button').removeClass('disabled');
            }
            else {
                $('#printouts-modal-download-button').addClass('disabled');
            }
        }   
    });
});


JS;

$this->registerJs($script);

?>