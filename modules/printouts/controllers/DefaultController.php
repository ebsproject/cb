<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\printouts\controllers;

use yii\web\Controller;
use app\interfaces\models\IOccurrence;
use app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel;
use app\models\Program;
use app\modules\printouts\models\PrintoutsModel;

/**
 * Default controller for the `printouts` module
 */
class DefaultController extends Controller
{
    /**
     * Controller constructor
     */
    public function __construct(
        $id,
        $module,
        public IOccurrence $occurrence,
        public IOccurrenceDetailsModel $occurrenceDetailsModel,
        public Program $program,
        public PrintoutsModel $printoutsModel,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        // Parse post parameters
        $selectedIds = isset($_POST['selectedIds']) ? json_decode($_POST['selectedIds']) : [];
        $product = isset($_POST['product']) ? $_POST['product'] : '';
        $programCode = isset($_POST['program']) ? $_POST['program'] : '';
        $entity = isset($_POST['entity']) ? $_POST['entity'] : '';
        $csApiStatus = '';
        $templatesData = [];
        $fileTypes = ['pdf' => 'PDF',];

        // Retrieve occurrence dropdown data
        $occurenceSelect2Data = $this->printoutsModel->getOccurrenceSelect2Data($selectedIds, $entity);
        $occurrences = $occurenceSelect2Data['occurrences'];
        $occurrenceCount = $occurenceSelect2Data['occurrenceCount'];

        // Retrieve program name
        $programInfo = $this->program->getProgramByProgramCode($programCode);
        $programName = $programInfo['programName'] ?? '';

        try {
            // Retrieve program in CS
            $programCS = $this->printoutsModel->getAllPrograms($programName);
            if (isset($programCS['status']) && $programCS['status'] === 500)
                throw new \Exception(500);
            $programIdCS = $programCS[0]['id'] ?? 0;

            // Retrieve product in CS
            $productCS = $this->printoutsModel->getAllProducts($product);
            if (isset($productCS['status']) && $productCS['status'] === 500)
                throw new \Exception(500);
            $productIdCS = $productCS[0]['id'] ?? 0;

            // Retrieve templates in CS
            $templatesCS = $this->printoutsModel->getAllTemplates($productIdCS, $programIdCS);
            if (isset($templatesCS['status']) && $templatesCS['status'] === 500)
                throw new \Exception(500);
            // Build templates select2
            foreach ($templatesCS as $template) {
                $templatesData[$template['name']] = $template['name'];
            }

            // Retrieve file formats in CS
            $fileTypes = $this->printoutsModel->getAllFormats();
            if (isset($fileTypes['status']) && $fileTypes['status'] === 500)
                throw new \Exception(500);
        } catch (\Exception $e) {
            $csApiStatus = $e->getMessage();
        }

        // Render the bulk update modal content
        return $this->renderAjax('index', [
            'product' => $product,
            'program' => $programCode,
            'occurrences' => $occurrences,
            'occurrenceCount' => $occurrenceCount,
            'templatesData' => $templatesData,
            'csApiStatus' => $csApiStatus,
            'fileTypes' => $fileTypes,
        ]);
    }

    /**
     * Renders the contents of the printouts modal
     */
    public function actionRenderPrintoutsModal()
    {
        $product = isset($_POST['product']) ? $_POST['product'] : '';
        $program = isset($_POST['program']) ? $_POST['program'] : '';
        $entityName = isset($_POST['entityName']) ? $_POST['entityName'] : '';
        $entityDisplayName = isset($_POST['entityDisplayName']) ? $_POST['entityDisplayName'] : '';
        $entityDbIds = isset($_POST['entityDbIds']) ? json_decode($_POST['entityDbIds']) : [];
        $entityType = isset($_POST['entityType']) ? $_POST['entityType'] : '';
        $requestParameters = [];
        $csApiStatus = '';
        $templatesData = [];
        $reportParameters = [];
        $fileTypes = [];

        // Retrieve program name
        $programInfo = $this->program->getProgramByProgramCode($program);
        $programName = $programInfo['programName'] ?? '';

        try {
            // Retrieve program in CS
            $programCS = $this->printoutsModel->getAllPrograms($programName);

            if (isset($programCS['status']) && $programCS['status'] === 500)
                throw new \Exception(500);
            $programIdCS = $programCS[0]['id'] ?? 0;

            // Retrieve product in CS
            $productCS = $this->printoutsModel->getAllProducts($product);

            if (isset($productCS['status']) && $productCS['status'] === 500)
                throw new \Exception(500);
            $productIdCS = $productCS[0]['id'] ?? 0;

            // Retrieve templates in CS
            $templatesCS = $this->printoutsModel->getAllTemplates($productIdCS, $programIdCS);

            if (isset($templatesCS['status']) && $templatesCS['status'] === 500)
                throw new \Exception(500);
            // Build templates select2
            foreach ($templatesCS as $template) {
                $templatesData[$template['name']] = $template['name'];
            }

            // Get the parameters of the reports
            $reportParameters = $this->printoutsModel->getReportParameters(array_keys($templatesData));

            // Retrieve file formats in CS
            $fileTypes = $this->printoutsModel->getAllFormats();
            if (isset($fileTypes['status']) && $fileTypes['status'] === 500)
                throw new \Exception(500);
        } catch (\Exception $e) {
            $csApiStatus = $e->getMessage();
        }

        // Render the bulk update modal content
        return $this->renderAjax('_modal_body', [
            'product' => $product,
            'program' => $program,
            'entityName' => $entityName,
            'entityDisplayName' => $entityDisplayName,
            'entityDbIds' => $entityDbIds,
            'entityType' => $entityType,
            'requestParameters' => $requestParameters,
            'templatesData' => $templatesData,
            'csApiStatus' => $csApiStatus,
            'reportParameters' => $reportParameters,
            'fileTypes' => $fileTypes,
        ]);
    }

    /**
     * Generate report/printout
     * @param String template name of the template to download
     * @param Integer occurrenceId occurrence identifier
     * @param String format file formate of the template to download
     * @param String fileName name of the file to be downloaded
     */
    public function actionGenerateReport ($template = '', $occurrenceId = 0, $format = '', $fileName = '')
    {
        // If function param $occurrenceId is empty...
        // ...then retrieve selected occurrence IDs from session
        $occurrenceId = (!$occurrenceId) ? \Yii::$app->session->get('selectedOccurrenceIds') : $occurrenceId;
        
        // Check if occurrence IDs are empty
        if (!$occurrenceId) {
            return json_encode([
                'success' => false,
                'message' => 'No occurrence selected. Please select one or all occurrences before downloading.',
            ]);
        }

        // Replace any forward or backward slashes in $fileName with hyphens
        $fileName = str_replace(['/', '\\'], '-', $fileName);

        if (isset(
            $template,
            $occurrenceId,
            $format,
            $fileName
        )) {
            // Generate report...
            // ...and retrieve resulting file path
            $filePath = $this->printoutsModel->exportReport(
                rawurlencode($template),
                $format,
                $occurrenceId,
                $fileName
            );

            \Yii::debug('Checking file path: ' . json_encode(compact('filePath')), __METHOD__);

            // If error in generating report occurs
            if (!is_string($filePath)) {
                \Yii::debug('Error found in downloading printouts: ' . json_encode(compact('filePath')), __METHOD__);

                return json_encode([
                    'success' => false,
                    'message' => $filePath['body']['metadata']['status'][0]['message'],
                ]);
            }

            // If file path does not exist
            if (!file_exists($filePath)) {
                return json_encode([
                    'success' => false,
                    'message' => 'Download failed. Printout file not found. Please try again or file a report for assistance.',
                ]);
            }

            // Return generated file path
            \Yii::debug('Generated printout file exists: ' . json_encode(compact('filePath')), __METHOD__);

            \Yii::$app->session->set('filePath', $filePath);

            return json_encode([
                'success' => true,
                'message' => '', // $filePath is already stored in session
            ]);
        } else {
            return json_encode([
                'success' => false,
                'message' => 'Download failed. Printout request info is missing. Please try again or file a report for assistance.',
            ]);
        }
    }

    /**
     * Download report/printout
     * @param String filePath path to the file to be downloaded
     */
    public function actionDownloadReport ()
    {
        $filePath = \Yii::$app->session->get('filePath');

        if (!file_exists($filePath)) {
            throw new \yii\web\NotFoundHttpException('The printout report file does not exist. Please try again or file a report for assistance.');
        }

        return \Yii::$app->response->sendFile($filePath);
    }

    /**
     * Download report/printout
     * @param String template name of the template to download
     * @param String $parameters array of parameters,
     *                  where the key is the variable name,
     *                  and the value is the parameter value.
     * @param String format file formate of the template to download
     * @param String fileName name of the file to be downloaded
     */
    public function actionDownloadGeneratedReport($template = '', $parameters = '', $format = '', $fileName = '')
    {
        if (!empty($parameters)) {
            $parameters = (array) json_decode($parameters);
        } else $parameters = [];

        if (isset($template, $parameters, $format, $fileName)) {
            $filePath = $this->printoutsModel->generateReport(rawurlencode($template), $format, $parameters, $fileName);

            if (gettype($filePath) == "string" && file_exists($filePath)) { // trigger successful download
                \Yii::$app->response->sendFile($filePath);
                return \Yii::$app->end();
            } else if (gettype($filePath) != "string") { // If file path is not a string, display error
                \Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-times"></i>Report generation unsuccessful.');
            } else {    // file is not created/ does not exist
                \Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-times"></i>File not found.');
            }
        } else {
            \Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-times"></i>Printout information is missing.');
        }
    }

    /**
     * Set selected occurrence IDs to session
     */
    public function actionSetSelectedOccurrenceIds () {
        \Yii::$app->session->set('selectedOccurrenceIds', $_POST['selectedOccurrenceIds']);
    }
}
