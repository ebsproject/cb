<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\workingList\models;

use Yii;

// import models

// import interfaces

/**
 * Class file for Template model functions for the Working List module
 */
class TemplateModel extends \app\models\BaseModel
{
    /**
     * Model constructor
     */
    public function __construct ()
    {
        
    }

    /**
     * Extract template column headers from config
     * 
     * @param Array $variableConfig variable configuration
     * @param String $entity type of template (seed,package)
     * 
     * @return Array column headers
     */
    public function extractTemplateHeaders($variableConfig,$entity){
        $headers = [];

        $configArr = array_filter($variableConfig,function($config) use ($entity){
            if(isset($config['entity']) && !empty($config['entity'])){
                return $config['entity'] == $entity; 
            }
            else{
                return isset($config['abbrev']) && !empty($config['abbrev']);
            }
        });

        $headers = array_unique(array_merge($headers,array_column($configArr,'abbrev')));

        return $headers;
    }
}