<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\workingList\models;

use Yii;

use app\models\PlatformListMember;

// import interfaces
use app\interfaces\modules\workingList\models\IListManagementModel;
use app\interfaces\models\IListMember;
use app\interfaces\models\IPlatformListMember;

/**
 * Class file for List Management model functions for the Working List module
 */
class ListManagementModel extends \app\models\BaseModel implements IListManagementModel
{
    // Constant variables
    CONST PREPEND_ABBREV = 'WORKING_LIST';
    CONST COMPONENT_NAME = 'Working List';

    CONST ATTRIB_EQUIV = [
        'SEED_CODE' => 'GID',
        'GERMPLASM_NAME' => 'designation',
        'PACKAGE_LABEL' => 'label',
        'PROGRAM' => 'seedManager'
    ];

    public $listMemberModel;
    public $platformListMemberModel;

    /**
     * Model constructor
     */
    public function __construct (
        IListMember $listMemberModel,
        IPlatformListMember $platformListMemberModel
    )
    {
        $this->listMemberModel = $listMemberModel;
        $this->platformListMemberModel = $platformListMemberModel;
    }

    /**
     * Update order of list items given sort configuration
     * 
     * @param String $sort sort configuration
     * @param Integer $listId list identifier
     * 
     * @return Integer update process status
     */
    public function updateListItemOrder($sort,$listId){
        $method = 'POST';
        $endpoint = 'lists/'.$listId.'/reorder-list-members';

        $params = [
            "reorderCondition" => "SORT",
            "sort" => $sort
        ];

        $apiCallResult = Yii::$app->api->getResponse($method,$endpoint,json_encode($params));

        return $apiCallResult['status'];
    }

    /**
     * Remove list items
     * 
     * @param Integer $listItemIds list item ID
     * @param Boolean $bgprocess background process 
     * 
     * @return Integer update process status
     */
    public function removeItems($listItemIds,$bgprocess){
        $params = [];
        if($bgprocess){
            $params = [
                "description" => "Deletion of working list items",
                "entity" => "LIST",
                "endpointEntity" => "LIST_MEMBER",
                "application" => "LISTS"
            ];
        }

        $apiCallResult = $this->listMemberModel->deleteMany($listItemIds,$bgprocess,$params);
        return $apiCallResult['status'];
    }

    /**
     * Build export file columns
     * 
     * @param Array $variables variable array
     * 
     * @return Array columns
     */
    public function buildExportColumns($variables){
        $columns = [];

        foreach($variables as $column){
            $attribStr = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($column['abbrev'])))));
            $attribStr = isset(self::ATTRIB_EQUIV[$column['abbrev']]) ? self::ATTRIB_EQUIV[$column['abbrev']] : $attribStr;

            $isExportColumn = $column['visible_export'] == 'true' ? true : false;

            if($isExportColumn){
                $columns[$attribStr] = $column['abbrev'];
            }
        }

        return $columns;
    }

    /**
     * Insert items to working list
     * 
     * @param Integer $listId working list identifier
     * @param Array $listItemIdArr array of list items to be inserted
     * 
     * @return mixed $results
     */
    public function addItemsToWorkingList($listId,$listItemIdArr){
        if(!empty($listItemIdArr)){
            // process array
            $listItemsArr = array_map(function($item){
                return [
                    'id' => (isset($item['id']) ? $item['id'] : $item['dataDbId']),
                    'displayValue' => "".$item['displayValue']
                ];
            },$listItemIdArr);
            
            // add items to working list
            $method = 'POST';
            $path = 'lists/'.$listId.'/members';
            $params = [
                'records' => $listItemsArr,
            ];

            $results = Yii::$app->api->getResponse(
                $method, 
                $path, 
                json_encode($params,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            );

            return $results;
        }
    }

    /**
     * Retrieve experiment data provider
     * 
     * @param String $program program code value
     * 
     * @return mixed
     */
    public function getExperimentDataProvider($program){
        $isOwned = 'isOwned';
        $pageParams = '&sort=experimentYear:desc&experimentDbId:asc';

        $path = 'experiments-search?'.$isOwned.$pageParams;
        $params = [
            "experimentStatus" => "in: ('draft', 'entry list created')",
            "programCode"=>"equals ".$program
        ];

        return Yii::$app->api->getParsedResponse('POST', $path, json_encode($params), null, true);
    }

    /**
     * Generate new entry list item records to be added to the selected experiment
     * 
     * @return Array records to be added to the entry list
     */
    public function getNewEntryListRecords($experimentId,$experiment,$configData,$entryListId,$listInfo,$programId){
        // retrieve experiment config default values
        $entryType = null;
        $entryClass = null;
        $entryRole = null;

        foreach($configData as $config){
            if(isset($config['default']) && !empty($config['default'])){
                switch($config['variable_abbrev']){
                    case 'ENTRY_TYPE':
                        $entryType = $config['default'];
                        break;
                    case 'ENTRY_CLASS':
                        $entryClass = $config['default'];
                        break;
                    case 'ENTRY_ROLE':
                        $entryRole = $config['default'];
                        break;
                    default:
                        break;
                }
            }
        }
        // retrieve working list items
        $workingListMembersData = $this->platformListMemberModel->searchAllMembers($listInfo['listDbId']);

        $workingListMembers = [];
        if(isset($workingListMembersData['data']) && !empty($workingListMembersData['data'])){
            $workingListMembers = $workingListMembersData['data'];
        }

        $createRecords = [];
        foreach($workingListMembers as $member){
            if($member['isActive']){
                $germplasmId = $member['germplasmDbId'];
                $temp = [
                    'entryName' => $member['designation'],
                    'entryType' => !empty($entryType) ? $entryType : 'test',
                    'entryStatus' => 'active',
                    'entryListDbId' => "$entryListId",
                    'germplasmDbId' => "$germplasmId"
                ];

                if(!empty($entryClass)){
                    $temp['entryClass'] = $entryClass;
                }

                if(!empty($experiment['experimentType']) && 
                    ($experiment['experimentType'] != 'Breeding Trial' && $experiment['experimentType'] != 'Observation')){
                if(!empty($entryRole)){
                    $temp['entryRole'] = $entryRole;
                }
                }

                if(isset($member['seedDbId'])){
                    $seedDbId = $member['seedDbId'];
                    $temp['seedDbId'] = "$seedDbId";
                }
                if(isset($member['packageDbId']) && $member['programDbId'] == $programId){
                    $packageDbId = $member['packageDbId'];
                    $temp['packageDbId'] = "$packageDbId";
                }

                $createRecords[] = $temp;
            }
        }

        return $createRecords;
    }

    /**
     * Build export CSV file content
     * 
     * @param Array $columns csv file columns
     * @param Array $listItems working list items
     * 
     * @return Array csv content
     */
    public function buildCSVContent($columns,$listItems){
        $csvContent = [[]];

        // build headers
        foreach($columns as $key => $value){
            array_push($csvContent[0],$value);
        }

        // build content
        $index = 1;
        foreach($listItems as $item){
            $csvContent[$index] = [];
            foreach($columns as $ind => $val){
                if(isset($item[$ind])){
                    array_push($csvContent[$index],$item[$ind]);
                }
                else{
                    array_push($csvContent[$index],null);
                }
            }
            $index += 1;
        }

        return $csvContent;
    }

    /**
     * Get ATTRIB_EQUIV values
     */
    public function getAttribEquiv(){
        return self::ATTRIB_EQUIV;
    }
}