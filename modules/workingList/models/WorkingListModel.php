<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\workingList\models;

use Yii;

// import models

// import interfaces
use app\interfaces\modules\workingList\models\IWorkingListModel;

use app\interfaces\models\IApi;
use app\interfaces\models\ILists;
use app\interfaces\models\IProgram;
use app\interfaces\models\IConfig;
use app\interfaces\models\IVariable;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\models\IPlatformListMember;

use app\dataproviders\ArrayDataProvider;

/**
 * Class file for Working List model functions for the Working List module
 */
class WorkingListModel extends \app\models\BaseModel implements IWorkingListModel
{
    // Constant variables
    CONST PREPEND_ABBREV = 'WORKING_LIST';
    CONST COMPONENT_NAME = 'Working List';

    CONST ATTRIB_EQUIV = [
        'SEED_CODE' => 'GID',
        'GERMPLASM_NAME' => 'designation',
        'PACKAGE_LABEL' => 'label',
        'PROGRAM' => 'seedManager'
    ];

    private $dynamicFields = [];
    private $dynamicRules = [];
    private $columns = [];

    public $listsModel;
    public $programModel;
    public $configModel;
    public $variableModel;
    public $apiModel;
    public $userDashboardConfig;
    public $platformListMemberModel;

    /**
     * Model constructor
     */
    public function __construct (
        ILists $listsModel,
        IProgram $programModel,
        IConfig $configModel,
        IVariable $variableModel,
        IApi $apiModel,
        IUserDashboardConfig $userDashboardConfig,
        IPlatformListMember $platformListMemberModel
    )
    {
        $this->listsModel = $listsModel;
        $this->programModel = $programModel;
        $this->configModel = $configModel;
        $this->variableModel = $variableModel;
        $this->apiModel = $apiModel;
        $this->userDashboardConfig = $userDashboardConfig;
        $this->platformListMemberModel = $platformListMemberModel;
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes are the dynamic fields
     *
     * @param string $name property name
     * @return mixed property value
     * @see getAttribute()
     */
    public function __get($name) {
        if (array_key_exists($name, $this->dynamicFields))
            return $this->dynamicFields[$name];
        else
            return parent::__get($name);
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes are the dynamic fields
     *
     * @param string $name property name
     * @param mixed $value property value
     */
    public function __set($name, $value) {

        if (array_key_exists($name, $this->dynamicFields))
            $this->dynamicFields[$name] = $value;
        else
            parent::__set($name, $value);
    }

    /**
     * Returns the validation rules for attributes.
     * This method is overridden
     * 
     * @return array validation rules
     */
    public function rules() {
        return [
            [$this->dynamicRules,'safe'],
            [$this->dynamicRules,'string']
        ];
    }

    /**
    * Set dynamic columns
    * @param Array $variables config variables array
    */
    public function setColumnsForFiltering($variables){
        foreach($variables as $column){
            $columnAbbrev = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($column['abbrev'])))));
            $columnAbbrev = isset(self::ATTRIB_EQUIV[$column['abbrev']]) ? self::ATTRIB_EQUIV[$column['abbrev']] : $columnAbbrev;

            if (in_array($column['abbrev'], ['VOLUME', 'PACKAGE_QUANTITY'])) {
                $columnAbbrev = 'quantity';
            }

            $this->dynamicFields[$columnAbbrev] = null;
            $this->dynamicRules[] = $columnAbbrev;
            $this->columns[] = $columnAbbrev;
        }

        return true;
    }

    /**
     * Retrieves working list record given the user id and list type
     * 
     * @param Integer $userId user identifier
     * @param String $listType type of working list
     * 
     * @return Array working list information
     */
    public function getWorkingList($userId,$listType){
        // retrieve working list info for user
        $params = [
            'type' => $listType,
            'status' => 'draft',
            'abbrev' => self::PREPEND_ABBREV.'_'.strtoupper($listType).'_'.$userId
        ];
        $workingListInfo = $this->listsModel->searchAll($params,'&showWorkingList=true');

        $listInfo = [];
        if($workingListInfo['status'] == 200){
            
            // has existing working list
            if(isset($workingListInfo['data']) && !empty($workingListInfo['data'])){
                $listInfo = $workingListInfo['data'][0];
            }
        }

        return $listInfo;
    }

    /**
     * Create new working list record
     * 
     * @param Integer $userId user identifier
     * @param String $listType type of working list
     * 
     * @return Integer status of creating new working list
     */
    public function createWorkingList($userId,$listType){

        $record = [
            'abbrev'=> self::PREPEND_ABBREV.'_'.strtoupper($listType).'_'.$userId,
            'name'=> self::COMPONENT_NAME.' '.ucwords($listType).' '.$userId,
            'description'=>'user session '.strtolower($listType).' working list',
            'displayName'=>self::COMPONENT_NAME.' '.ucwords($listType),
            'remarks'=>'user session '.strtolower($listType).' working list',
            'type'=> ''.$listType,
            'status' => 'draft',
            'isActive' => 'false',
            'listUsage' => 'working list'
        ];

        $params = [
            'records' => [$record]
        ];

        $apiCallResult = Yii::$app->api->getResponse('POST','lists',json_encode($params));

        return $apiCallResult['status'];
    }

    /**
     * Delete working list record
     * 
     * @param Integer $listId working list ID
     * @param String $listType working list type
     * @param Integer $userID user ID
     * 
     * @return Integer status of deleting working list
     */
    public function deleteWorkingList($listId,$listType,$userId){
        $params = [
            "abbrev" => self::PREPEND_ABBREV.'_'.strtoupper($listType).'_'.$userId.'-DELETED-'.$listId,
            "name" => self::COMPONENT_NAME.' '.ucwords($listType).' '.$userId.'-DELETED-'.$listId,
            "displayName" => 'Working List Package DELETED'
        ];
        
        $apiCallResult = $this->listsModel->updateOne($listId, $params);

        return $apiCallResult['status'];
    }

    /**
     * Retrieve list items
     * 
     * @param Array $param search parameters
     * @param Integer $listId list ID
     * 
     * @return Array list items
     */
    public function getListItems($params,$listId){

        $apiCallResult = $this->platformListMemberModel->searchAllMembers($listId,'',$params);

        return $apiCallResult['data'];
    
    }

    /**
     * Create new list record
     * 
     * @param Array $listInfo new list information
     * 
     * @return mixed
     */
    public function createNewList($listInfo){

        $params = [
            'records' => [$listInfo]
        ];

        $apiCallResult = Yii::$app->api->getResponse('POST','lists',json_encode($params));

        return $apiCallResult;
    }

    /**
     * Retrieve variable configuration record
     * 
     * @param String $programCode program code value
     * 
     * @return Array config-based variable information
     */
    public function getVariableConfiguration($programCode){
        $baseConfigAbbrev = self::PREPEND_ABBREV.'_VARIABLES_CONFIG';
        $variableConfig = [];

        // Get crop code
        $cropCode = $this->programModel->getCropCode($programCode);
        $cropConfigAbbrev = $baseConfigAbbrev.'_'.$cropCode.'_';

        // Get variable configuration for given crop
        $configAbbrev = $cropConfigAbbrev.$programCode;
        $configData = $this->configModel->getConfigByAbbrev($configAbbrev);

        // if program-specific crop variable configuration does not exist
        if(empty($configData)){
            $configAbbrev = $cropConfigAbbrev.'DEFAULT';
            $configData = $this->configModel->getConfigByAbbrev($configAbbrev);

            // if crop-default variable configuration does not exist
            if(empty($configData)){
                $configAbbrev = $baseConfigAbbrev.'_SYSTEM_DEFAULT';
                $configData = $this->configModel->getConfigByAbbrev($configAbbrev);
            }
        }

        $variableConfig = isset($configData['values']) ? $configData['values'] : []; 

        return $variableConfig;
    }

    /**
     * Validate variables if existing in database
     * 
     * @param Array $variables config-based variable information
     * 
     * @return Array validated variables
     */
    public function validateVariables($variables){
        $validatedVariables = [];

        foreach($variables as $key=>$value){
            $variableInfo = $this->variableModel->getVariableByAbbrev($variables[$key]['abbrev']);

            if(isset($variableInfo) && !empty($variableInfo)){
                $variables[$key]['description'] = isset($variableInfo['description']) ? $variableInfo['description'] : '';
                $variables[$key]['label'] = isset($variableInfo['label']) ? $variableInfo['label'] : '';

                $validatedVariables[] = $variables[$key];
            }
        }

        return $validatedVariables;
    }

    /**
     * Build browser columns from config-based variables
     * 
     * @param Array variable information
     * 
     * @return Array browser columns
     */
    public function buildBrowserColumns($variables){
        $columns = [];

        foreach($variables as $column){
            $attribStr = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($column['abbrev'])))));
            $attribStr = isset(self::ATTRIB_EQUIV[$column['abbrev']]) ? self::ATTRIB_EQUIV[$column['abbrev']] : $attribStr;

            if (in_array($column['abbrev'], ['VOLUME', 'PACKAGE_QUANTITY'])) {
                $attribStr = 'quantity';
            }

            $isColumn = $column['browser_column'] == 'true' ? true : false;
            $visibility = $column['visible_browser'] == 'true' ? true : false;

            if($isColumn){
                array_push($columns,[
                    'attribute' => $attribStr,
                    'label' => $column['display_name'],
                    'visible' => $visibility,
                    'hidden' => !$visibility
                ]);
            }
        }

        return $columns;
    }

    /**
     * Main search function for Working List members
     * 
     * @param Object $param search parameters
     * @param Integer $listId working list identifier
     * @param String $listType type of working list
     * 
     * @return mixed data provider for main working list browser
     */
    public function search($params,$listId,$listType){
        $this->load($params);

        $method = 'POST';
        $endpoint = 'lists/'.$listId.'/members-search';

        // Build search params
        $getParams = \Yii::$app->request->getQueryParams();

        $params = [];
        if(isset($getParams['WorkingListModel']) && !empty($getParams['WorkingListModel'])){
            foreach($getParams['WorkingListModel'] as $ind=>$value){
                if(!empty($value)){
                    $params[$ind] = $value;
                }
            }
            //check if params is empty, if yes, set params to empty string for filter
            if(empty($params)){
                $params = [];
            }
        }

        $params['isBasic'] = "true";


        // Build filters
        $gridId = 'datagrid-working-list';
        $filters = $this->assembleFilters($gridId);

        // Retrieve list member data
        $requestBody = json_encode($params);
        $results = Yii::$app->api->getResponse($method, $endpoint, $requestBody, $filters);

        // If empty data, reset page
        if (empty($results['body']['result']['data'])) {
            $filters = $this->assembleFilters($gridId, true);
            $requestBody = json_encode($params);
            $results = Yii::$app->api->getResponse($method, $endpoint, $requestBody, $filters);
        }

        $data = $results['body']['result']['data'] ?? [];
        $total = $results['body']['metadata']['pagination']['totalCount'] ?? 0;

        unset($params['isBasic']);
        //only load data within params; Retain text filter on the column text field
        $this->load(['WorkingListModel' => $params]);

        $key = 'listMemberDbId';

        return new ArrayDataProvider([
            'allModels' => $data,
            'key' => $key,
            'restified' => true,
            'totalCount' => $total,
            'id' => $gridId
        ]);
    }

    /**
     * Build filters for retrieving data provider
     * 
     * @param String $gridId browser identifier
     * @param Boolean $reset if need to reset data, default value is FALSE
     * 
     * @return String filter parameter
     */
    public function assembleFilters($gridId,$reset=false){
        $queryParams = [];

        // build sorting
        $getParams = \Yii::$app->request->getQueryParams();
        $paramSort = 'sort=orderNumber:ASC';

        if(isset($getParams['sort'])){
            $paramSort = 'sort=';

            $sortParams = $this->processSortParams($getParams['sort']);
            if($sortParams != ''){
                $paramSort .= $sortParams;
            }
        }

        // build pagination
        $paramPage = '';

        if($reset){
            $paramPage = 'page=1';

            if(isset($getParams[$gridId.'-page'])){
                $getParams[$gridId.'-page'] = 1;
            }
            else if(isset($getParams['page'])){
                $getParams['page'] = 1;
            }
            \Yii::$app->request->setQueryParams($getParams);
        }

        if(isset($getParams[$gridId.'-page'])){
            $paramPage = 'page='.$getParams[$gridId.'-page'];
        }
        else if(isset($getParams['page'])){
            $paramPage = 'page='.$getParams['page'];
        }

        // build page limit
        $defaultPageSize = $this->userDashboardConfig->getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        if($paramLimit!='') $queryParams[] = $paramLimit;
        if($paramPage!='') $queryParams[] = $paramPage;
        if($paramSort!='') $queryParams[] = $paramSort;

        return implode('&',$queryParams);
    }

    /**
     * Process sort parameter values
     * 
     * @param $queryParamsSort sort values
     * 
     * @return String processed sort
     */
    public function processSortParams($queryParamsSort){
        $paramSort = '';

        $queryParamsSort = explode('|', $queryParamsSort);
        $countParam = count($queryParamsSort);
        $currParam = 1;

        foreach($queryParamsSort as $column) {
            if(isset($column[0]) && $column[0] == '-') {
                $paramSort = $paramSort . substr($column, 1) . ':DESC';
            } else {
                $paramSort = $paramSort . $column . ':ASC';
            }

            if($currParam < $countParam) {
                $paramSort = $paramSort . '|';
            }
            $currParam += 1;
        }

        return $paramSort;
    }

    /**
     * Build parameters for retrieving background process notifications
     * 
     * @param String $workers background process worker names
     * @param String $remark background process worker remarks
     * @param String $isSeen background process worker isSeen status
     * @param Integer $userId user identifier
     * 
     * @return Array search parameters
     */
    public function processNotifParams($workers, $remarks, $isSeen, $userId){
        $workersArr = explode('|',$workers);

        return [
                "creatorDbId" => "equals $userId",
                "workerName" => "equals " . implode("|equals ", $workersArr),
                "isSeen" => "equals $isSeen"
            ];
    }

    /**
     * Reorder items according to specificed sorting order
     * 
     * @param Integer $listId id of target list
     * @param String $sortParams sort parameters
     * 
     * @return Integer update status
     */
    public function reorderListItems($listId,$sortParams){
        $method = 'POST';
        $endpoint = 'lists/'.$listId.'/reorder-list-members';
        $requestData = [
            'reorderCondition' =>'SORT',
            "sort" => $sortParams
        ];

        $apiCallResult = Yii::$app->api->getResponse($method,$endpoint,json_encode($requestData));

        return $apiCallResult['status'];
    }
}