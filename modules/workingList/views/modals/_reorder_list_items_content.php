<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders browser for selected list items to be reordered
 */

use kartik\grid\GridView;
use kartik\widgets\Select2;

use yii\helpers\Html;
use yii\helpers\Url;

use app\dataproviders\ArrayDataProvider;
?>
<p>
    <?= \Yii::t(
        'app', 
        'Change the order number within the provided range values based on the <strong>Starting Order Number</strong>. '.
        'Click <strong>CONFIRM</strong> to save the changes made.'
    )?>
</p>
<div id="mod-reorder-notif" class="hidden"></div>

<?php
    $startOrderNumber = Select2::widget([
        'name' => 'start-order-number-fld',
        'id' => 'start-order-number-fld',
        'data' => $options,
        'value' => '',
        'options' => [
            'id' => 'start-order-number-fld',
            'placeholder' => 'Select a value',
            'disabled'=> false,
        ],
        'pluginOptions' => [
            'allowClear' => false,
            'dropdownParent' => '#reorder-items-modal'
        ]
    ]);
    echo '<label class="control-label" style="margin-top:10px">'.
         \Yii::t('app','Starting Order Number&emsp;<span class="required">*</span>').'</label>';
        echo '<div style="width:30%">';
            echo $startOrderNumber;
        echo '</div>';

    $coreColumns = [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => false
        ],
        [
            'header' =>Yii::t('app', 'New Order No.'),
            'format' => 'raw',
            'headerOptions'=>[
                'style'=>'min-width:200px !important'
            ],
            'encodeLabel' => false,
            'vAlign'=>'middle',
            'content' => function($data) use ($options){
                return Select2::widget([
                    'name' => 'order-number-options',
                    'id' => 'reorder-'.$data['listMemberDbId'].'-'.$data['orderNumber'],
                    'data' => [],
                    'value' => $data['orderNumber'],
                    'options' => [
                        'id' => 'reorder-'.$data['listMemberDbId'].'-'.$data['orderNumber'],
                        'placeholder' => 'Select a value',
                        'disabled'=> false,
                        'class'=>'reorder-list-item-fld'
                    ],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'dropdownParent' => '#reorder-items-modal'
                    ]
                ]);
            },
            'value' => ''
        ],
        [   
            'header' =>Yii::t('app', 'Current Order Number'),
            'attribute'=>'orderNumber',
            'enableSorting' => false,
        ],
        [   
            'header' =>Yii::t('app', `Seed Code`),
            'attribute'=>'seedName',
            'enableSorting' => false,
        ],
        [
            'header' =>Yii::t('app', `Package Label`),
            'attribute'=>'label',
            'enableSorting' => false,
        ],
    ];

    $dataProvider = $dataProvider ?? new ArrayDataProvider([]);

    echo $grid = GridView::widget([
        'pjax' => true,
        'dataProvider' => $dataProvider,
        'id' => 'reorder-list-items-table-grid',
        'tableOptions' => [
            'id' => 'reorder-list-items-table', 
            'class' => 'table table-bordered white z-depth-3'
        ],
        'columns' => $coreColumns,
        'toggleData'=>true,
        'striped'=>false
      ]);
?>

<?php
$updateListOrderUrl = Url::to(['list-management/update-list-order', 'listId' => $listId, 'listType' => $listType]);

$script = <<< JS

    var selectedItemsCount = '$itemCount'
    var startOrderNumValue = null
    var gridId = '$browserId'

    var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>'
    
    $(document).ready(function() {
        $('.reorder-list-item-fld').attr('disabled','disabled')
        $('#confirm-reorder-items-btn').attr('disabled','disabled')

        // when a starting orde rnumber has been selected
        $('#start-order-number-fld').on('change', function(e){
            startOrderNumValue = parseInt(this.value)

            var startValCount = startOrderNumValue
            var maxVal = startOrderNumValue + parseInt(selectedItemsCount)

            var valObj = []
            var valueString = ''
            
            for(var i=startOrderNumValue; i<maxVal; i++){
                valObj.push('"'+i+'":'+i+'');
            }

            valueString = JSON.parse("{"+valObj.join(",")+"}");

            $(".reorder-list-item-fld").each(function(){
                updateItemOrderField(this.id, valueString, startValCount);
                startValCount++;
            });

            $(".reorder-list-item-fld").removeAttr('disabled');
            $("#confirm-reorder-items-btn").removeAttr('disabled');
        })

        // when a select2 filed in the new order number column is updated
        $('.reorder-list-item-fld').on('change', function(e){
            var usedEntno = [];
            var errorFlag = false;

            $(".reorder-list-item-fld").each(function(){
                if(usedEntno.includes(this.value)){
                    //show notif and highlight row
                    errorFlag = true;
                } else {
                    usedEntno.push(this.value);
                }
            });

            if(errorFlag){
                $("#mod-reorder-notif").html(
                    '<div class="alert-danger" style="padding: 10px;">'+
                    'There is a <strong>duplicate order number</strong>. Please correct the other one to proceed.</div>'
                );
                $("#mod-reorder-notif").removeClass("hidden");
                $('#confirm-reorder-items-btn').attr('disabled','disabled');               
            } else {
                $("#mod-reorder-notif").html('');
                $("#mod-reorder-notif").addClass("hidden");

                $("#confirm-reorder-items-btn").removeAttr('disabled');
            }            
        })

        $('#confirm-reorder-items-btn').on('click',function(e){
            e.preventDefault()
            e.stopPropagation() 

            $("#confirm-reorder-items-btn").attr('disabled','disabled')
            $(".close").addClass("hidden")

            disableUpdateWorkingListBtns()

            var reorderedItemsArr = [];
            var tempStr = '';

            $(".reorder-list-item-fld").each(function(){
                tempStr = (this.id).split("-");
                reorderedItemsArr.push({
                        'listMemberId':tempStr[1], 
                        'orderNumber':tempStr[2], 
                        'targetOrderNumber':this.value
                    });
            });

            $(".reorder-items-modal-body").html(loadingIndicator)

            if(reorderedItemsArr.length > 0){
                showMessage('info','List items are being updated...')

                reorderItemsBgProcess(reorderedItemsArr)
            }
        })
    });

    // update list item dropdown options field
    function updateItemOrderField(id, values, initial){
        var s1 = document.getElementById(id);

        $('#'+id).find('option').remove();
        
        var newOption = null;
        $.each(values, function (key, value) {        
            newOption = document.createElement("option");
            newOption.value = key;
            newOption.innerHTML = value;
            s1.options.add(newOption);
        });

        $('#'+id).val(initial);
    }

    // process reordering of all list items using background process
    function reorderItemsBgProcess(reorderedItemsArr){
        $.ajax({
                url: '$updateListOrderUrl',
                type: 'post',
                dataType:'json',
                async: false,
                cache: false,
                data: {
                    itemsArr:reorderedItemsArr
                },
                success: function(response) {
                   var notif = '';
                    
                    if(response){
                        $('#'+gridId).b4rCheckboxColumn("clearSelection")

                        showMessage(
                            'success',
                            'Reordering of list items is currently in progress. Check notifications for updates.')

                        enableUpdateWorkingListBtns()

                        $.pjax.reload({
                            container: '#'+gridId+'-pjax'
                        });
                    }

                    $('#reorder-items-modal').modal('hide');
                },
                complete: function() {
                    enableUpdateWorkingListBtns()
                }
            });
    }

    // process toast message to be displayed
    function showMessage(type,message) {
        $('.toast').css('display','none');

        var typeIconEquiv = {
            'error': "<i class='material-icons red-text left'>error</i>",
            'warning': "<i class='material-icons orange-text left'>warning</i>",
            'success': "<i class='material-icons green-text left'>check</i>",
            'info': "<i class='material-icons blue-text left'>info</i>"
        }

        var notif = typeIconEquiv[type] + " " + message;
        $('.toast').css('display','none');
        Materialize.toast(notif, 3500);
    }

    /**
     * Disable worknig list management buttons
     */
    function disableUpdateWorkingListBtns(){
        // update list buttons
        $('#remove-list-items-btn').addClass('disabled').trigger('change');
        $('#save-working-list-btn').addClass('disabled').trigger('change');
        $('#more-options-btn').addClass('disabled').trigger('change');

        // browser management buttons
        $('#reset-'+gridId+'-grid').addClass('disabled').trigger('change');
    }

    /**
     * Enable working list management buttons
     */
    function enableUpdateWorkingListBtns(){
        // update list buttons
        $('#remove-list-items-btn').removeClass('disabled').trigger('change');
        $('#save-working-list-btn').removeClass('disabled').trigger('change');
        $('#more-options-btn').removeClass('disabled').trigger('change');

        // browser management buttons
        $('#reset-'+gridId+'-grid').removeClass('disabled').trigger('change');
    }
JS;

$this->registerJs($script);
?>