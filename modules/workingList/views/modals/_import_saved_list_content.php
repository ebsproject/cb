<?php
/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file creating the basic info of a list
 **/

use app\models\Lists;

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

$required = '<span style="color: red">*</span>';

$notif = '<div id="invalid-select" class="red-text text-darken-2" style = "display:none">No list selected!</div>';

// retrieve data
$listDataProvider = $dataProvider ?? new \yii\data\ArrayDataProvider([]);
$searchModel = $searchModel ?? null;
$dataType = $dataType ?? 'package';

$dataCount = ($listDataProvider == null || $listDataProvider->allModels == null ? 0 : count($listDataProvider->allModels));

echo $notif . Html::input(
    'text',
    'dataType',
    $dataType,
    [
        'id' => 'dataType-hidden',
        'class' => 'input-text-hidden hidden variable-filter input-text-filter'

    ]
) . Html::input(
    'text',
    'dataCount',
    $dataCount,
    [
        'id' => 'data-count-hidden',
        'class' => 'input-text-hidden hidden data-count'

    ]
);

?>

<div class="row" style = "margin-bottom:0px;">
<?php
    $actionColumns = [
        ['class' => 'yii\grid\SerialColumn'], 
        [
            'class'=>'kartik\grid\ActionColumn',
            'header' => Yii::t('app', 'Actions'),
            'template' => '{select}',
            'buttons' => [
                'select' => function ($url, $model, $key) use ($action) {
                    return Html::a(
                        '<i class="material-icons">exit_to_app</i>',
                        '#',
                        [
                            'class'=>'import-list-items-to-list',
                            'title' => Yii::t('app', 'Select saved list'),
                            'data-saved_list_id' => $model["listDbId"],
                            'data-saved_list_abbrev' => $model["abbrev"],
                            'data-action' => $action
                        ]
                    );
                },
            ]
        ]
    ];
    
    $columns = [
        [
            'attribute' => 'displayName',
            'header' => 'Name',
            'visible' => true,
            'format' => 'raw',
        ],
        [
            'attribute' => 'abbrev',
            'header' => 'Abbrev',
            'visible' => true,
            'format' => 'raw',
        ],
        [
            'attribute' => 'memberCount',
            'header' => 'Number of Items',
            'visible' => true,
            'format' => 'raw',
        ],
    
    ];
    $gridColumns = array_merge($actionColumns, $columns);
    
    $browserId = 'dynagrid-working-list-saved-list-grid';
    
    $dynagrid = DynaGrid::begin([
        'columns' => $gridColumns,
        'theme' => 'panel-default',
        'showPersonalize' => true,
        'storage' => 'cookie',
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions' => [
            'id' => $browserId,
            'dataProvider' => $listDataProvider,
            'floatHeader' =>true,
            'floatOverflowContainer'=> true,
            'showPageSummary' => false,
            'pjax' => true,
            'pjaxSettings' => [
                'options' => [
                    'id' => $browserId,
                ],
            ],
            'panel' => [
                'before' => false,
                'after' => false,
            ],
            'toolbar' => '',
        ],
        'options' => [
            'id'=>$browserId
        ],
        'submitButtonOptions' => [
            'icon' => 'glyphicon glyphicon-ok',
        ],
        'deleteButtonOptions' => [
            'icon' => 'glyphicon glyphicon-remove',
            'label' => 'Remove',
        ],
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }
    DynaGrid::end();
?>
</div>