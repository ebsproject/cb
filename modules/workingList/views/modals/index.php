<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;

// save confirmation modal
Modal::begin([
    'id' => 'confirm-save-modal',
    'header' => '
        <div>
            <div class="col-md-6">
                <h4><i class="material-icons widget-icons">check_circle</i>&nbsp;Save Confirmation</h4>
            </div>
            <div class="col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        ',
    'footer' =>
        Html::a(\Yii::t('app', 'Cancel'),['#'],
            [
                'class' => 'modal-close text-align:center',
                'id' => 'cancel-save-list-btn',
                'title' => 'Go back to working list',
                'data-dismiss'=>'modal'
            ]) . '&emsp;'.
        Html::a(\Yii::t('app', 'Confirm'),'#!',
            [
                'class' => 'btn btn-primary waves-effect waves-light modal-close text-align:center',
                'id' => 'save-list-btn',
                'title' => 'Continue to save working list',
                'style' => 'margin-right:5px;'
            ]),
    'closeButton' => [
        'class' => 'hidden',
    ],
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
]);

echo '<div class="create-list-modal-body">
            <span>
                '.\Yii::t('app', 'Do you want to save your working list? Saving will clear the working list.').'
            </span>
        </div>';
Modal::end();

// save list modal
Modal::begin([
    'id' => 'create-list-modal',
    'header' => '
        <div>
            <div class="col-md-6">
                <h4><i class="material-icons widget-icons">add</i>&nbsp;'.Yii::t('app','Create List').'</h4>
            </div>
            <div class="col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        ',
    'footer' =>
        Html::a(\Yii::t('app', 'Create'),'#!',
            [
                'class' => 'btn btn-primary waves-effect waves-light modal-close',
                'id' => 'create-list-btn',
                'title' => 'Create new list'
            ]) . '&emsp;',
    'closeButton' => [
        'class' => 'hidden',
    ],
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999;'],
]);

echo '<div class="create-list-modal-body">' .
    Yii::$app->controller->renderPartial('/modals/_create_list_content', [
        'name' => '',
        'abbrev' => '',
        'display_name' => '',
        'type' => '',
        'description' => '',
        'remarks' => '',
        'id' => null,
        'header' => '',
        'showTabs' => false,
        'create' => true,
        'listType' => $listType
    ]);
Modal::end();

// import saved list items modal
Modal::begin([
    'id' => 'import-saved-list-modal',
    'header' => '
        <div>
            <div class="col-md-6">
                <h4><i class="material-icons widget-icons">playlist_add</i>&nbsp;'.Yii::t('app','Select a Saved List').'</h4>
            </div>
            <div class="col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        ',
    'closeButton' => [
        'class' => 'hidden',
    ],
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999;'],
]);

echo '<div class="import-saved-list-modal-body"></div>';

Modal::end();

// export to experiment modal
// save list modal
Modal::begin([
    'id' => 'export-to-experiment-modal',
    'header' => '
        <div>
            <div class="col-md-6">
                <h4><i class="material-icons widget-icons">add</i>&nbsp;'.Yii::t('app','Export List to Experiment').'</h4>
            </div>
            <div class="col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        ',
    'footer' => false,
    'closeButton' => [
        'class' => 'hidden',
    ],
    'size' => 'modal-xl',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999;'],
]);

echo '<div class="export-to-experiment-modal-body"></div>';
Modal::end();

// reorder selected items modal
Modal::begin([
    'id' => 'reorder-items-modal',
    'header' => '
        <div>
            <div class="col-md-6">
                <h4>'.Yii::t('app','Reorder Working List Items').'</h4>
            </div>
            <div class="col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        ',
    'footer' =>
        Html::a(\Yii::t('app', 'Cancel'),['#'],
            [
                'class' => 'modal-close text-align:center',
                'id' => 'cancel-reorder-list-btn',
                'title' => 'Go back to working list',
                'data-dismiss'=>'modal'
            ]) . '&emsp;'.
        Html::a(\Yii::t('app', 'Confirm'),'#!',
            [
                'class' => 'btn btn-primary waves-effect waves-light modal-close',
                'id' => 'confirm-reorder-items-btn',
                'title' => 'Reorder List Items'
            ]) . '&emsp;',
    'closeButton' => [
        'class' => 'hidden',
    ],
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999;'],
]);

echo '<div class="reorder-items-modal-body"></div>';
Modal::end();

// confirm save to existing list modal
Modal::begin([
    'id' => 'confirm-save-to-list-modal',
    'header' => '
        <div>
            <div class="col-md-6">
                <h4><i class="material-icons widget-icons">check_circle</i>&nbsp;Save Confirmation</h4>
            </div>
            <div class="col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        ',
    'footer' =>
        Html::a(\Yii::t('app', 'Cancel'),['#'],
            [
                'class' => 'modal-close text-align:center',
                'id' => 'cancel-save-list-btn',
                'title' => 'Go back to working list',
                'data-dismiss'=>'modal'
            ]) . '&emsp;'.
        Html::a(\Yii::t('app', 'Confirm'),'#!',
            [
                'class' => 'btn btn-primary waves-effect waves-light modal-close text-align:center',
                'id' => 'confirm-save-to-existing-list-btn',
                'title' => 'Continue to save working list',
                'style' => 'margin-right:5px;'
            ]),
    'closeButton' => [
        'class' => 'hidden',
    ],
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
]);

echo Html::input(
    'text',
    'selectedIdVal',
    '',
    [
        'id' => 'selected-list-id-hidden',
        'class' => 'input-text-hidden hidden selected-list-id'

    ]);
echo '<div class="confirm-save-to-list-modal-body"></div>';
Modal::end();