<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* Renders working list viewer page
*/

\yii\jui\JuiAsset::register($this);

use kartik\dynagrid\DynaGrid;

use yii\helpers\Url;
use yii\helpers\Html;

use app\models\UserDashboardConfig;
use app\dataproviders\ArrayDataProvider;
use app\models\DataBrowserConfiguration;
use app\components\ManageSortsWidget;

$listId = isset($listInfo['listDbId']) ? $listInfo['listDbId'] : null;
$workingListType = isset($listType) ? $listType : null;

$browserId = $browserId ?? 'datagrid-working-list-main-browser';

// Define URLs
$resetGridUrl = Url::to(['default/index?']);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
$baseUrl = Url::base(); //set base URL for dynamic redirection of tools

?>

<div class="panel panel-default">
    <!-- Header -->
    <div class="row">
        <!-- Page Title -->            
        <div class="col-md-10">
            <div class="working-list-title pull-left" style="margin-left:10px;">
                <h3>
                    <?=Html::a(Yii::t('app', $sourceTool), Url::to($baseUrl.'/index.php/'.$returnUrl)).' <small> » ' . Yii::t('app', 'Working List') . '</small>'; ?>
                </h3>
            </div>
        </div>

        <!-- Back Button -->
        <div class="col-md-2">
            <div class="working-list-title pull-right align=right">
                <?= Html::a(
                        Yii::t('app','BACK'),
                        Url::to($baseUrl.'/index.php/'.$returnUrl),
                        [
                            'class' => 'btn btn-default pull-right lighten-4',
                            'id' => 'working-list-back-btn',
                            'type' => 'button',
                            'style' => 'margin-bottom:5px;margin-top:5px',
                        ]
                    );
                ?>
            </div>
        </div>
    </div>

    <!-- Loading Indicator -->
    <div class="progress loading-progress-indicator hidden" style="margin:0px;padding:0px;">
        <div class="indeterminate"></div>
    </div>

    <!-- Data Browser -->
    <div class="row">
        <div style="padding-left:15px; padding-right:15px;">
            <?php 
                // Selection summary information
                $summarySelectInfo = '
                    <br/>
                    <p id="total-selected-text" class="pull-right" style="margin:-1px;"></p>
                    <p id="total-selected-text2" class = "pull-right" style = "margin: -1px;">
                        <b><span id="selected-items-count"> </span></b> <span id="selected-items-text"> </span>
                    </p>';

                // Build notification button and area
                echo '<div id="notif-container">
                        <ul id="notifications-dropdown" class="dropdown-content notifications-dropdown"></ul>
                      </div>';

                $notifBtn = Html::a(
                    '<i class="material-icons large">notifications_none</i>
                    <span id="notif-badge-id" class=""></span>',
                    '#',
                    [
                        'id' => 'working-list-notifs-btn',
                        'class' => 'btn waves-effect waves-light working-list-notification-button wl-tooltipped',
                        'data-pjax' => 0,
                        'style' => "overflow: visible !important;",
                        "data-position" => "top",
                        "data-tooltip" => \Yii::t('app', 'Notifications'),
                        "data-activates" => "notifications-dropdown",
                        'disabled' => false,
                    ]
                );

                // build toolbar buttons
                $addItemsBtnGroup = "
                    <ul id='dropdown-add-list-options' class='dropdown-content toolbar-btn-options' data-beloworigin='false'>
                        <li><a href='#!' id='import-saved-list-items-btn' class='add_items'>Import From Saved List</a></li>
                    </ul>
                ";

                $addItemsBtn = Html::a(
                    '<i class="material-icons right" style="margin-left:0px;">add</i>', 
                    '#', 
                    [
                        'data-pjax' => true,
                        'class' => 'btn btn-default light-green darken-3 dropdown-trigger dropdown-button-pjax pull-right',
                        'style' => '',
                        'id' => 'add-list-items-btn', 
                        'title'=>\Yii::t('app','Add list items'),
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                        'data-activates'=>'dropdown-add-list-options'
                    ]).
                    '&emsp;'.
                    $addItemsBtnGroup;

                $removeOptionBtnGroup = "
                    <ul id='dropdown-remove-list-options' class='dropdown-content toolbar-btn-options' data-beloworigin='false'>
                        <li><a href='#!' id='remove-selected-btn' class='remove_items'>Remove Selected Items</a></li>
                        <li><a href='#!' id='clear-list-btn' class='remove_items'>Clear List</a></li>
                    </ul>
                ";

                $removeBtn = Html::a(
                    '<i class="material-icons right" style="margin-left:0px;">delete</i>', 
                    '#', 
                    [
                        'data-pjax' => true,
                        'class' => 'btn btn-default light-green darken-3 dropdown-trigger dropdown-button-pjax pull-right',
                        'style' => '',
                        'id' => 'remove-list-items-btn', 
                        'title'=>\Yii::t('app','Remove list items'),
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                        'data-activates'=>'dropdown-remove-list-options'
                    ]).
                    '&emsp;'.
                    $removeOptionBtnGroup;

                $saveOptionBtnGroup = "
                    <ul id='dropdown-save-list-options' class='dropdown-content toolbar-btn-options'  data-beloworigin='false'>
                        <li><a href='#!' id='save-list-items-btn' class='save_items'>Save As New List</a></li>
                        <li><a href='#!' id='save-to-existing-list-btn' class='save_items'>Save To Existing List</a></li>
                        <li><a href='#!' id='save-list-order-btn' class='update_items'>Save List Order</a></li>
                    </ul>";
                
                $saveBtn = Html::a(
                    '<i class="material-icons right" style="margin-left:0px;">save</i>', 
                    '#', 
                    [
                        'data-pjax' => true,
                        'class' => 'btn btn-default light-green darken-3 dropdown-trigger dropdown-button-pjax pull-right',
                        'style' => '',
                        'id' => 'save-working-list-btn', 
                        'title'=>\Yii::t('app','Save list'),
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                        'data-activates'=>'dropdown-save-list-options'
                    ]).
                    '&emsp;'.
                    $saveOptionBtnGroup;
                    
                $templateOptionsBtnGroup = "
                    <ul id='dropdown-template-options' class='dropdown-content template-options' style='max-width: 130px; margin-left: 129px;'>
                        <li>
                            <a 
                                id='seed-template-btn' 
                                class='template_options'
                                data-type='seed'
                            >Seeds Only</a>
                        </li>
                        <li>
                            <a 
                                id='pkg-template-btn' 
                                class='template_options'
                                data-type='package'
                            >Packages Only</a>
                        </li>
                    </ul>
                ";

                $moreFxnOptionsBtnGroup = "
                    <ul id='dropdown-more-function-options' class='dropdown-content toolbar-btn-options'  data-beloworigin='false'>
                        <li><a href='#!' id='reorder-items-group-btn' class='reorder_items'>Reorder Selected Items</a></li>
                        <li><a href='#!' id='export-list-btn' class='export_items'>Export List Items</a></li>
                        <li><a href='#!' id='export-list-experiment-btn' class='export_items'>Export List to Experiment</a></li>
                        <li>".
                            Html::a(
                                \Yii::t('app','Download Template'), 
                                null, 
                                [
                                    'class' => 'template_options_group dropdown-trigger dropdown-button  dropdown-button-pjax',
                                    'id' => 'template-options-group-btn', 
                                    'data-hover' => 'hover',
                                    'data-activates' => 'dropdown-template-options',
                                    'data-alignment' => 'right'
                                ]).$templateOptionsBtnGroup.
                        "</li>
                    </ul>";

                $moreFxnBtn = Html::a(
                    '<i class="material-icons right" style="margin-left:0px;">handyman<span class="caret" style="margin-left:3px;"></span></i>', 
                    '#', 
                    [
                        'data-pjax' => true,
                        'class' => 'btn btn-default light-green darken-3 dropdown-trigger dropdown-button-pjax pull-right',
                        'style' => 'margin-right:5px;',
                        'id' => 'more-options-btn', 
                        'title'=>\Yii::t('app','More list management options'),
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                        'data-activates'=>'dropdown-more-function-options'
                    ]).
                    '&emsp;'.$moreFxnOptionsBtnGroup;

                //build summary information
                $sumInfo = '<p id = "info-sum" class ="pull-left"><b><span id="total-count"></span></b> items in working list. 
                    <b><span id="total-remove"></span></b> items have been removed from the list </p>';

                // build core columns
                $idColumn = 'listMemberDbId';

                $coreColumns = [
                    [
                        'class' => 'app\components\B4RCheckboxColumn',
                        'label'=> "Checkbox",
                        'header'=>'
                            <input type="checkbox" class="filled-in" id="working-list-select-all-id"/>
                            <label for="working-list-select-all-id"></label>
                        ',
                        'checkboxOptions' => function ($model) use ($idColumn){
                            return [
                                'value' => $model['listMemberDbId'],
                                'id' => $model['listMemberDbId'],
                                'class'=> 'working-list-grid_select',
                            ];
                        },
                        'content'=>function($model) use ($idColumn){
                            $checked = '';

                            return '
                                <input 
                                    class="working-list-grid_select filled-in" 
                                    type="checkbox" id="'.$model['listMemberDbId'].'" 
                                    value="'.$model['listMemberDbId'].'" '.$checked.'/>
                                <label for="'.$model['listMemberDbId'].'"></label>
                            ';
                        },
                        'order' => DynaGrid::ORDER_FIX_LEFT,
                        'hAlign' => 'center',
                        'vAlign' => 'top',
                        'hiddenFromExport' => true,
                        'mergeHeader' => true,
                        'width' => '1%',
                        'selectAllId' => 'select-all-working-list-items',
                        'displaySummaryId' => 'total-selected-text2'
                    ],
                ];

                $columns = array_merge($coreColumns,$columns);

            // retreive data provider
            if (isset($listId) && isset($listType)) {
                $dataProvider = $searchModel->search(null, $listId, $listType);
            } else {
                $dataProvider = new ArrayDataProvider([
                    'allModels' => [],
                    'key' => "listMemberDbId",
                    'restified' => true,
                    'totalCount' => 0,
                    'id' => 'datagrid-working-list'
                ]);
            }
                
                // Render grid
                $dynagrid = DynaGrid::begin([
                    'columns'=>$columns,
                    'storage' => DynaGrid::TYPE_SESSION,
                    'theme'=>'simple-default',
                    'showPersonalize' => true,
                    'showFilter' => false,
                    'showSort' => false,
                    'allowFilterSetting' => false,
                    'allowSortSetting' => false,
                    'gridOptions'=>[
                        'dataProvider'=>$dataProvider,
                        'filterModel'=>$searchModel,
                        'tableOptions'=>[
                            'class'=>$browserId.'-grid-table',
                            'id'=>$browserId
                        ],
                        'options'=>[
                            'id'=>$browserId.'-grid-table-con'
                        ],
                        'id' => $browserId.'-grid-table-con',
                        'striped'=>false,
                        'responsive' => true,
                        'responsiveWrap' => false,
                        'floatHeader' => true,
                        'floatHeaderOptions' => [
                            'scrollingTop' => '0',
                            'position' => 'absolute',
                        ],
                        'hover' => true,
                        'floatOverflowContainer' => true,
                        'showPageSummary' => false,
                        'pjax' => true,
                        'pjaxSettings'=>[
                        'neverTimeout'=>true,
                        'options' => [
                                'id' => $browserId, 
                                'enablePushState' => true,
                            ],
                            'beforeGrid'=>'',
                            'afterGrid'=>''
                        ],
                        'pager' => [
                            'firstPageLabel' => 'First',
                            'lastPageLabel' => 'Last'
                        ],
                        'rowOptions'=> [],
                        'panel'=> [
                            'heading'=> false,
                            'before' => $sumInfo.' {summary}&emsp;'.$summarySelectInfo,
                            'after'=> false
                        ],
                        'toolbar' =>  [
                            [
                                'content' => '&emsp;'.$moreFxnBtn.$saveBtn.$removeBtn.$addItemsBtn
                            ],
                            [
                                'content' => ''.
                                    ManageSortsWidget::widget([
                                        'dataBrowserId' => $browserId,
                                        'gridId' => $browserId,
                                        'searchModel' => 'WorkingList',
                                        'btnClass' => 'sort-action-button',
                                        'resetGridUrl' => $resetGridUrl
                                    ]).
                                    $notifBtn.'&emsp;'.
                                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                                    Url::to($baseUrl.'/index.php/workingList'),
                                    [
                                        'id'=>'reset-'.$browserId.'-grid',
                                        'class' => 'btn btn-default', 
                                        'title'=>\Yii::t('app','Reset working list browser'), 
                                        'data-pjax' => true
                                    ]).
                                    '&emsp;'.
                                    '{dynagridFilter}{dynagridSort}{dynagrid}'
                            ]
                        ],
                    ],
                    'options'=> [
                        'id' => $browserId
                    ]
                ]);

                DynaGrid::end();
            ?>
        </div>
    </div>
</div>

<?php
echo \Yii::$app->controller->renderPartial('/modals/index.php',[ 'listType' => $workingListType ]);

// Define URLs
$saveListOrderUrl = Url::to(['list-management/save-list-order']);
$clearListUrl = Url::to(['list-management/clear-list']);
$saveListUrl = Url::to(['list-management/save-list']);
$validateListUrl = Url::to(['list-management/validate-list-info']);
$exportListUrl = Url::to(['list-management/export-working-list']);
$removeSelectedItemsUrl = Url::to(['list-management/remove-selected-items']);
$rollbackSaveListUrl = Url::to(['list-management/rollback-save-list']);

$getExperimentDataProvider = Url::to(['list-management/get-experiment-data-provider']);
$exportListToExperiment = Url::to(['list-management/export-list-to-experiment','listType' => $workingListType]);
$reorderSelectedItemsUrl = Url::to(['list-management/reorder-selected-items','listType' => $workingListType,'listId' => $listId]);

$checkListAccessUrl = Url::to(['list-management/check-list-permission']);
$getSavedListsUrl = Url::to(['list-management/get-saved-lists','listType' => $workingListType]);
$getSavedListItemsUrl = Url::to(['list-management/get-saved-list-items','listType' => $workingListType]);
$saveToSavedListUrl = Url::to(['list-management/save-to-existing-list','listType' => $workingListType]);

$downloadTemplateUrl = Url::to(['template-management/download-template','program' => $program]);

$newNotificationsUrl = Url::to(['new-notifications-count']);
$notificationsUrl = Url::to(['push-notifications']);
$downloadDataUrl = Url::to(['download-data']);
$browserUrl = Url::to(['index']);

$checkForActiveUpdatesUrl = Url::to(['check-for-active-updates']);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS

    var gridId = '$browserId'
    var listId = '$listId'
    var workingListType = '$workingListType'

    var pageUrl = '$browserUrl'
    var workerName = 'UpdateListItems|ExecuteProcessor|BuildCsvData|AddListItems'

    var sessRemovedItems = 'wl-remove-items'
    // set tempStorage value to store session Ids
    var tempStorage = sessionStorage.getItem(sessRemovedItems) ?? ""

    var hasAccessToList = true;

    refresh();

    notifDropdown();
    renderNotifications();
    updateNotif();
    showSummaryInfo();
    displaySessionSelectedItems();

    $("document").ready(function() {
        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();

        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        refresh();

        renderNotifications();
        notifDropdown();
        updateNotif();
        showSummaryInfo();
        displaySessionSelectedItems();

        // Reload grid on pjax success
        $(document).on('ready pjax:success', function(e) {
            e.preventDefault();
            e.stopPropagation();

            $('.dropdown-button').dropdown();

            refresh();
            manageNotifs();
            //to avoid complete reset for remove Ids in session storage upon refresh
            saveRemovedItemIds(tempStorage);
            showSummaryInfo();

            displaySessionSelectedItems();
        });

        // Reload grid on pjax success
        $(document).on('ready pjax:complete', '#'+gridId, function(e) {
            $('.dropdown-trigger').dropdown();
            $('.dropdown-button').dropdown();

            refresh();
            manageNotifs();
            showSummaryInfo();

            displaySessionSelectedItems();
        });

        // Click on Save List Order button
        $(document).on('click','#save-list-order-btn', function(e) {
            e.preventDefault();
            e.stopPropagation();

            // extract sort values
            var currUrl = window.location.search;
            var urlSplit = currUrl.split('&');
            var sort = urlSplit[urlSplit.length-1].split('sort=')[1];

            if(sort != undefined){
                $.ajax({
                    url: '$saveListOrderUrl',
                    type: 'post',
                    data: {
                        listType: workingListType,
                        sort: sort
                    },
                    success: function(response) {
                        var res = JSON.parse(response)

                        if(res['success'] && !res['bgprocess']){
                            saveRemovedItemIds([])
                            showMessage('success',res['message'])
                        }
                        else if(res['success'] && res['bgprocess']){
                            saveRemovedItemIds([])
                            location.reload()
                        }
                        else{
                            showMessage('error',res['message'])
                        }
                        
                        showSummaryInfo()
                    },
                    error: function(error){
                        showMessage('error','Error encountered in updating the list items.')
                    }
                })
            }
            else{
                showMessage('warning','No sort order to be applied.')
            }
        });

        // Click on Remove Selected Items button
        $(document).on('click','#remove-selected-btn', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var selectedItemIds = getSelectedItems();

            if(selectedItemIds != undefined && selectedItemIds.length > 0){
                $(".loading-progress-indicator").removeClass("hidden").trigger("change")

                saveRemovedItemIds(selectedItemIds)

                $.ajax({
                    url: '$removeSelectedItemsUrl',
                    type: 'post',
                    data: {
                        listId: listId,
                        selectedItemIds: JSON.stringify(selectedItemIds)
                    },
                    success: function(response) {
                        var res = JSON.parse(response)

                        if(res['success'] && !res['bgprocess']){
                            showSummaryInfo()
                            showMessage('success',res['message'])

                            $('#'+gridId).b4rCheckboxColumn("clearSelection")
                        }
                        else if(res['success'] && res['bgprocess']){
                            $('#'+gridId).b4rCheckboxColumn("clearSelection")
                            showSummaryInfo()
                            location.reload()
                        }
                        else{
                            showMessage('error',res['message'])
                        }
                    },
                    complete: function(){
                        var pageUrl = '$browserUrl'

                        $(".loading-progress-indicator").removeClass("hidden").addClass("hidden").trigger("change")
                        showSummaryInfo()
                        location.reload()
                    },
                    error: function(error){
                        showMessage('error','Error encountered in updating the list items.')
                    }
                })                
            }
            else{
                showMessage('warning','No items selected.')
            }
        })

        // Click on Clear List button
        $(document).on('click','#clear-list-btn', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var itemCount = getWorkingListItemCount()

            if(itemCount == 0){
                showMessage('warning','Working list is empty.')
            }
            else{
                $(".loading-progress-indicator").removeClass("hidden").trigger("change")

                $.ajax({
                    url: '$clearListUrl',
                    type: 'post',
                    data: {
                        listId: listId,
                        listType: workingListType
                    },
                    success: function(response) {
                        var res = JSON.parse(response)

                        $(".loading-progress-indicator").removeClass("hidden").addClass("hidden").trigger("change")

                        if(res['success']){
                            $('#'+gridId).b4rCheckboxColumn("clearSelection")
                            saveRemovedItemIds([])
                            displaySessionSelectedItems()
                            showSummaryInfo()

                            showMessage('success',res['message'])
                        }
                        else{
                            showMessage('error',res['message'])
                        }              
                    },
                    complete: function(){
                        showSummaryInfo()
                        location.reload()
                    },
                    error: function(error){
                        showMessage('error','Error encountered in updating the list items.')
                    }
                })
            }
        })

        // Click on Save List button
        $(document).on('click','#save-list-items-btn', function(e) {
            e.preventDefault()
            e.stopPropagation()

            var itemsCount = getWorkingListItemCount()

            if(itemsCount > 0){
                $('#confirm-save-modal').modal('show')
            }
            else{
                showMessage('warning','Working list is empty!')
            }
        })

        // Click on Confirm Save List button
        $(document).on('click','#save-list-btn', function(e) {
            e.preventDefault()
            e.stopPropagation()

            $('#confirm-save-modal').modal('hide')

            // Open create list modal
            $('#create-list-modal').modal('show')
        })

        // Create on Create button
        $(document).on('click','#create-list-btn', function(e) {
            e.preventDefault()
            e.stopPropagation()

            var newListInfo = []
            var formInfo = $('#create-new-list-form').serializeArray()
            
            // extract form values
            var name = ''
            $.each(formInfo,function(ind,val){
                if(val.name.includes('[') && val.name.includes('[')){
                    name = val.name.split("[")[1]
                    name = name.split("]")[0]

                    newListInfo[name] = val.value
                }
            })
            
            // validate required fields
            var hasAllRequired = ( newListInfo['abbrev'] != '' && newListInfo['display_name'] != '' && 
                                  newListInfo['name'] != '' && newListInfo['type'] != '' )

            if(hasAllRequired){
                // validate list info
                $.ajax({
                    url: '$validateListUrl',
                    type: 'post',
                    data: {
                        abbrev: newListInfo['abbrev']
                    },
                    success: function(response){ 
                        var res = JSON.parse(response)

                        if(!res['success']){
                            showMessage('error',res['message'])
                            $('#list-abbrev').focus()
                        }
                        else{
                            $('#create-list-modal').modal('hide')
                            saveWorkingList(newListInfo)
                        }
                    },
                    error: function(error){
                        showMessage('error','Error encountered in validating list information.')
                    }
                })
            }
            else{
                showMessage('error', 'Missing required fields! ABBREV, DISPLAY NAME, NAME, and TYPE need to be filled!')

                if(newListInfo['abbrev'] == ''){
                    $('#list-abbrev').focus()
                }

                if(newListInfo['display_name'] == ''){
                    $('#list-display-name').focus()
                }

                if(newListInfo['name'] == ''){
                    $('#list-name').focus()
                }

                if(newListInfo['type'] == ''){
                    $('#list-type').focus()
                }
            }
        })

        // Click on Export List button
        $(document).on('click','#export-list-btn',function(e) {
            e.preventDefault()
            e.stopPropagation()         

            var itemCount = getWorkingListItemCount()

            if(itemCount == 0){
                showMessage('warning','Working list is empty.')
            }
            else{
                // process current sorting of items
                sort = ''
                sortParams = extractSortParams()

                if(sortParams != undefined && sortParams != null && sortParams != ''){
                    sort = `&sortParams=\${sortParams}`
                }

                exportLink = `\${window.location.origin}$exportListUrl?listType=\${workingListType}&listId=\${listId}` + sort
                window.location.assign(exportLink)
            }

        })

        // update display when checkbox is clicked
        $('.working-list-grid_select').on('click', function(e) {
            displaySessionSelectedItems() 
        })

        // update display on select all click
        $('#select-all-working-list-items').on('click', function(e) {
            displaySessionSelectedItems() 
        })

        // load saved lists onto the import saved list modal
        $(document).on('click','#import-saved-list-items-btn',function(e) {
            $.ajax({
                url: '$getSavedListsUrl',
                type: 'post',
                data: {
                    listType: workingListType,
                    action: 'import'
                },
                success: function(response) { 
                    $('.import-saved-list-modal-body').html(response).trigger('change');
                    $('#import-saved-list-modal').modal('show')
                },
                error: function(error){
                    showMessage('error','Error encountered in saving list items.')
                }
            })
        })
        
        // load items of selected saved list onto the working list
        $(document).on('click','.import-list-items-to-list',function(e) {
            e.preventDefault();
            e.stopPropagation();

            var selectedObj = $(this)
            var selectedListId = selectedObj.data('saved_list_id')
            var action = selectedObj.data('action')
            var listAbbrev = selectedObj.data('saved_list_abbrev')

            $('#import-saved-list-modal').modal('hide')

            // disable working list update buttons
            disableUpdateWorkingListBtns()
            $('#import-saved-list-items-btn').addClass('disabled').trigger('change')

            if(action == 'import'){
                showMessage('info','Loading items of saved list...')
                importItemsToWorkingList(selectedListId,listId)
            }

            if(action == 'save'){
                checkListAccess(selectedListId,listId,action);
            }

        })
        
        // Click on Export Items to List button, show modal
        $(document).on('click','#export-list-experiment-btn', function(e) {
            var itemCount = getWorkingListItemCount()
            
            if(itemCount != null && itemCount != 0){
                $.ajax({
                    url: '$getExperimentDataProvider',
                    type: 'post',
                    data: {},
                    success: function(response) {
                        $('.export-to-experiment-modal-body').html(response).trigger('change');
                        $('#export-to-experiment-modal').modal('show')
                    },
                    error: function(error){
                        showMessage('error','Error encountered in retrieving experiment data provider.')
                    }
                })
            }
            else{
                showMessage('warning','Working List is empty! No items to be exported to experiment.')
            }
        })

        // Select an experiment from Export List ot Experiment modal browser
        $(document).on('click', '.export-items-to-experiment', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var selectedObj = $(this)
            var selectedExpId = selectedObj.data('experiment_id')

            $('#export-to-experiment-modal').modal('hide')

            $.ajax({
                url: '$exportListToExperiment',
                type: 'post',
                data: {
                    experimentId: selectedExpId,
                    listId: listId
                },
                success: function(response) {
                    if(!response.success){
                        showMessage('error',response.message)
                    }
                },
                error: function(error){
                    
                }
            })
        })    

        // load reorder items modal
        $(document).on('click','#reorder-items-group-btn',function(e) {

            // check if has selected
            var selectedIdsArr = getSelectedItems() ?? [];
            
            $.ajax({
                url: '$checkForActiveUpdatesUrl',
                type: 'post',
                data: {},
                success: function(response) {
                    var res = JSON.parse(response)

                    if(res.status == false){
                        // if yes, call ajax
                        if(selectedIdsArr == null || selectedIdsArr.length == 0){
                            showMessage('warning', 'Select at least 1 working list item.')
                        }
                        else{
                            showMessage('info', 'Loading selected items to be reordered...')

                            $.ajax({
                                url: '$reorderSelectedItemsUrl',
                                type: 'post',
                                data: {
                                    selectedIdsArr: selectedIdsArr
                                },
                                success: function(response) {
                                    $('.reorder-items-modal-body').html(response)
                                    $('#reorder-items-modal').modal('show')

                                    // refresh browser
                                    $.pjax.reload({
                                        container:'#'+gridId+'-pjax',
                                        url: pageUrl,
                                        replace: false,
                                        type: 'POST',
                                        data: {
                                            forceReset:true
                                        }
                                    });
                                },
                                error: function(error){
                                    showMessage('error','Error encountered in reordering the list items.')
                                }
                            })   
                        } 
                    }
                    else{
                        showMessage(
                            'info', 
                            'List order is currently being updated. Wait for the process to finish pefore proceeding.'
                        );
                    }
                },
                error: function(error){
                   
                }
            })  
        })
        
        // Faciliatate download of template of chosen type for the working list 
        $(document).on('click', '.template_options', function(e) {
            var selection = $(this)
            var templateType = selection.data('type')

            var action = "update"
            var itemCount = getWorkingListItemCount();

            if(itemCount > 0){
                // build template download url
                let downloadUrl = '$downloadTemplateUrl' + '&type='+templateType + 
                                    '&listDbId=' + listId + '&action=' + action

                // build redirect url
                let redirectUrl = window.location.origin + downloadUrl + 
                                    '&redirectUrl=' + encodeURIComponent(window.location.href)

                window.location.assign(redirectUrl);
            }
            else{
                showMessage('warning','Working List is emppty!')
            }
        })

        // show confirmation modal for saving to existing list
        $(document).on('click','#confirm-save-to-existing-list-btn', function(e) {
            $(".loading-progress-indicator").removeClass("hidden").trigger("change")
            $('#confirm-save-to-list-modal').modal('hide')

            var selectedListId = $('#selected-list-id-hidden').val();

            if(selectedListId != undefined){
                appendItemsToSavedList(selectedListId,listId);
            }
            else{
                showMessage('error','Error encountered in appending items to saved list!')
            }
        })

        // load saved lists modal
        $(document).on('click','#save-to-existing-list-btn', function(e) {
            var itemCount = getWorkingListItemCount()

            if(itemCount > 0){
                $.ajax({
                    url: '$getSavedListsUrl',
                    type: 'post',
                    data: {
                        listType: workingListType,
                        action: 'save'
                    },
                    success: function(response) { 
                        $('.import-saved-list-modal-body').html(response).trigger('change');
                        $('#import-saved-list-modal').modal('show')
                    },
                    error: function(error){
                        showMessage('error','Error encountered in saving list items.')
                    }
                })

            }
            else{
                showMessage('warning', 'Working list is empty!')
            }
        })
    });

    // manage loading indicator for export file
    $(window).on('beforeunload', function(){
        $('#loading').html('');
        $('#system-loading-indicator').css('display','none');
    });

    // Facilitate loading of list items onto the working list
    function importItemsToWorkingList(selectedListId,listId){
        // retrieve and load list items
        $.ajax({
            url: '$getSavedListItemsUrl',
            type: 'post',
            data: {
                listId: selectedListId,
                workingListId: listId
            },
            success: function(response) { 
                var res = JSON.parse(response)

                if(res['success'] && !res['bgprocess']){
                    showMessage('success','Successfully loaded list items!')
                }
                else if(res['success'] && res['bgprocess']){
                    showMessage('info',res['message'])
                    location.reload()
                }
                else if(!res['success'] && res['message'] != ''){ // if process has an error message
                    showMessage('error', res['message'])
                }
                else{
                    showMessage('error', 'Error encountered in importing list items.')
                }

                enableUpdateWorkingListBtns()
                $('#import-saved-list-items-btn').removeClass('disabled').trigger('change')

                $.pjax.reload({
                    container:'#'+gridId+'-pjax',
                    url: pageUrl,
                    replace: false,
                    type: 'POST',
                    data: {
                        forceReset:true
                    }
                });
            },
            error: function(error){
                showMessage('error','Error encountered in importing list items.')
                enableUpdateWorkingListBtns()
            }
        })
    }

    // facilitate appending working list items to saved list
    function appendItemsToSavedList(selectedListId,listId){
        // retrieve and load list items
        $.ajax({
            url: '$saveToSavedListUrl',
            type: 'post',
            data: {
                listId: selectedListId,
                workingListId: listId
            },
            success: function(response) { 
                var res = JSON.parse(response)

                $(".loading-progress-indicator").addClass("hidden").trigger("change")

                if(res['success'] && !res['bgprocess']){
                    saveRemovedItemIds([])
                    showSummaryInfo()
                    $('#'+gridId).b4rCheckboxColumn("clearSelection")
                    location.reload()
                }
                else if(res['success'] && res['bgprocess']){
                    showMessage('info',res['message'])
                    location.reload()
                }
                else{
                    showMessage('error',res['message'])
                }
                enableUpdateWorkingListBtns()
            },
            error: function(error){
                $(".loading-progress-indicator").addClass("hidden").trigger("change")
                enableUpdateWorkingListBtns()
            }
        })
    }

    // save working list as new list record
    function saveWorkingList(listInfo){
        var newListInfo = []

        // process object values
        for(const [index,value] of Object.entries(listInfo)){
            newListInfo.push({[index]:value})
        }

        // process current sorting of items
        sortParams = extractSortParams()

        $.ajax({
            url: '$saveListUrl',
            type: 'post',
            data: {
                listInfo: newListInfo,
                listType: workingListType,
                sort: sortParams
            },
            success: function(response) { 
                var res = JSON.parse(response)

                if(res['success']){
                    saveRemovedItemIds([])
                    showSummaryInfo()

                    $('#'+gridId).b4rCheckboxColumn("clearSelection")
                    location.reload()
                }
                else{
                    rollbackSaveList(listInfo,res['message'])
                }

            },
            error: function(error){
                rollbackSaveList(listInfo,'Error encountered in saving list items.')
            }
        })
    }

    // rollback failed saving of lists
    function rollbackSaveList(listInfo,message){
        var newListInfo = listInfo
        var abbrev = newListInfo['abbrev'] ?? ''

        if(Object.keys(newListInfo).length > 0 && abbrev != ''){
            $.ajax({
                url: '$rollbackSaveListUrl',
                type: 'post',
                data: { abbrev: abbrev },
                success: function(response) { 
                    var res = JSON.parse(response)
                    
                    if(!res['success']){
                        showMessage('error',message+' Kindly try saving the list again.')
                    }
                },
                error: function(error){
                    showMessage('error','Error encountered in saving list items. Kindly try again.')
                }
            })
        }
    }

    // store removed item ids to session
    function saveRemovedItemIds(idsArr){
        //check and validate passed value if empty
        if(idsArr != null && idsArr.length > 0){
            //check if tempStorage/storage session if empty
            if (tempStorage != [] && tempStorage != ""){
                //if not, parse tempStorage and concat new removed ids
                tempStorage = JSON.parse(tempStorage);
                tempStorage = tempStorage.concat(',',idsArr);
            }
            else{
                tempStorage = tempStorage.concat(idsArr);
            }
            //set storage value from tempStorage
            sessionStorage.setItem(sessRemovedItems, JSON.stringify(tempStorage));
        }
        else{
            //if given value is empty, reset tempStorage and session storage
            tempStorage = [];
            sessionStorage.setItem(sessRemovedItems, "");
        }
    }

    // display selected items
    function displaySessionSelectedItems(){
        var selectedItemIds = getSelectedItems()
        var itemIdsCount = selectedItemIds != undefined && selectedItemIds != null ? selectedItemIds.length : 0

        var endStr = 'selected items.'
        if(itemIdsCount == 0){
            itemIdsCount = ''
            endStr = ''
        }

        $('#selected-items-count').html(itemIdsCount)
        $('#selected-items-text').html(endStr)
    }

    // retrieve selected list items in browser
    function getSelectedItems(){
        var ids = $('#'+gridId).b4rCheckboxColumn("getSelectedRows");

        return ids;
    }

    // extract list item count from summary information
    function getWorkingListItemCount(){
        var value = $('#'+gridId+' .kv-panel-before .summary').html();
        var totalCount = 0;

        var itemTotalCount = $('#total-count').val();

        if(value != null){
            var split = value.split('of');
            
            if(split[1] != null){
                split = split[1].replace('items', '');
                split = split.replace('item', '');
                split = split.replace('<b>', '');
                split = split.replace('</b>', '');
                split = split.replace(' ', '');
                split = split.replace(',', '');
                totalCount = parseInt(split);
            }
        }
        else if(itemTotalCount != ''){
            totalCount = parseInt(itemTotalCount)
        }

        return totalCount;
    }

    // check user access to selected list
    function checkListAccess(selectedListId,workingListId,action){
        hasAccessToList = true;

        // check access to selected list
        $.ajax({
            url: '$checkListAccessUrl',
            type: 'post',
            data: {
                listId: selectedListId
            },
            success: function(response) { 
                var res = JSON.parse(response)

                if(res['success'] && !res['hasAccess']){
                    hasAccessToList = false;

                    showMessage('error',res['message'])
    
                    enableUpdateWorkingListBtns()
                    $(".loading-progress-indicator").addClass("hidden").trigger("change")
                }
                else if(res['success'] && res['hasAccess']){
                    if(action == 'save'){

                        // show confirmation modal
                        var content =   "<span>Do you want to save your working list item to <strong>" +
                                        res['abbrev'] + "</strong>? " +
                                        "Saving will clear the current working list.</span>"

                        $('#selected-list-id-hidden').val(selectedListId).trigger('change');                        
                        $('.confirm-save-to-list-modal-body').html(content).trigger('change');

                        $('#confirm-save-to-list-modal').modal('show')

                        appendItemsToSavedList(selectedListId,workingListId)
                    }
                }
                else{
                    hasAccessToList = false;

                    showMessage('error',showMessage('error',res['message']))
                    enableUpdateWorkingListBtns()
                }
            },
            error: function(error){
                hasAccessToList = false;

                $(".loading-progress-indicator").addClass("hidden").trigger("change")
                enableUpdateWorkingListBtns()
            }
        })
    }

    // Notification functions

    function manageNotifs(){
        notifDropdown();

        $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="display: none;"></ul>');

        renderNotifications();
        updateNotif();
    }

    /**
     * Initialize dropdown materialize for notifications
     */
    function renderNotifications(){
        $('#working-list-notifs-btn').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            belowOrigin: true,
            alignment: 'right',
            stopPropagation: true
        });
    }

    /**
     * Shows new notifications
     */
    function updateNotif() {
        $.getJSON(
            '$newNotificationsUrl', 
            {
                workerName: workerName,
                remarks: 'working list'
            },
            function(json){
                $('#notif-badge-id').html(json);

                if(parseInt(json)>0){
                    $('#notif-badge-id').addClass('notification-badge red accent-2');
                }else{
                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                }
            }
        )
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log("Message: "+textStatus+" : "+errorThrown);
        });
    }

    /**
     * Displays the notification in dropdown UI
     */
    function notifDropdown(){
        $('#working-list-notifs-btn').on('click',function(e){
            $.ajax({
                url: '$notificationsUrl',
                type: 'post',
                data: {
                    workerName: workerName,
                    remarks: 'working list'
                },
                async:false,
                success: function(data) {
                    
                    var pieces = data.split('||');
                    var height = pieces[1];

                    $('<style>#notifications-dropdown.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');

                    data = pieces[0];
                    renderNotifications();

                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                    $('#notifications-dropdown').html(data);

                    $('.bg_notif-btn').on('click', function(e){
                        var processId = $(this).data('process-id')
                        var workerName = $(this).data('worker-name')

                        var pageUrl = '$browserUrl'

                        if(workerName == 'BuildCsvData'){
                            downloadExportFile($(this).text())
                        }
                        else{
                            $.pjax.reload({
                                container:'#'+gridId+'-pjax',
                                url: pageUrl,
                                replace: false,
                                type: 'POST',
                                data: {
                                    forceReset:true
                                }
                            });
                        }
                    });
                },
                error: function(xhr, status, error) {
                    var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + 
                    ' </blockquote></span></div>' +
                    '</li> </ul>');

                    Materialize.toast(toastContent, 'x', 'red');
                    $('.toast-error').collapsible();
                }
            });
        });
    }

    // process toast message to be displayed
    function showMessage(type,message) {
        $('.toast').css('display','none');

        var typeIconEquiv = {
            'error': "<i class='material-icons red-text left'>error</i>",
            'warning': "<i class='material-icons orange-text left'>warning</i>",
            'success': "<i class='material-icons green-text left'>check</i>",
            'info': "<i class='material-icons blue-text left'>info</i>"
        }

        var notif = typeIconEquiv[type] + " " + message;
        $('.toast').css('display','none');
        Materialize.toast(notif, 3500);
    }

    // Download export file
    function downloadExportFile(text){
        var pieces = text.split('(')
        var pieces = pieces[1].split(')')
        var fileName = pieces[0]

        downloadLink = `\${window.location.origin}$downloadDataUrl?filename=`+fileName+`&dataFormat=csv`
        window.location.assign(downloadLink)
    }

    /**
    * Resize grid table upon loading 
    */
    function refresh() {
        var maxHeight = ($(window).height() - 350);
        $(".kv-grid-wrapper").css("height", maxHeight);
    }

    /*
    * Display total number of items and removed items in the Working List
    */
    function showSummaryInfo(){
        //get total count in existing working list
        totalCount = getWorkingListItemCount();
        //get total removed items from exisiting working list
        totalRemove = sessionStorage.getItem(sessRemovedItems) ?? '';
        //validate if total remove is empty
        if(totalRemove == '' && totalRemove == []){
            totalRemove = 0;
        }
        else{
            //if not empty, parse and get remove ids
            totalRemove = JSON.parse(totalRemove);

            //split string and turn into array; totalRemove has the list of ids removed in an array
            totalRemove = totalRemove.split(",");
            totalRemove = totalRemove.length;
        }

        //set value to html
        $('#total-remove').html(totalRemove);
        $('#total-count').html(totalCount);
    }

    /**
     * Disable worknig list management buttons
     */
    function disableUpdateWorkingListBtns(){
        // update list buttons
        $('#remove-list-items-btn').addClass('disabled').trigger('change');
        $('#save-working-list-btn').addClass('disabled').trigger('change');
        $('#more-options-btn').addClass('disabled').trigger('change');

        // browser management buttons
        $('#reset-'+gridId+'-grid').addClass('disabled').trigger('change');
    }

    /**
     * Enable working list management buttons
     */
    function enableUpdateWorkingListBtns(){
        // update list buttons
        $('#remove-list-items-btn').removeClass('disabled').trigger('change');
        $('#save-working-list-btn').removeClass('disabled').trigger('change');
        $('#more-options-btn').removeClass('disabled').trigger('change');

        // browser management buttons
        $('#reset-'+gridId+'-grid').removeClass('disabled').trigger('change');
    }

    /**
     * Extract sorting parameters
     */
    function extractSortParams(){
        // extract sort values
        var currUrl = window.location.search;
        var urlSplit = currUrl.split('&');
        var sort = urlSplit[urlSplit.length-1].split('sort=')[1];

        return sort;
    }
JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
    .toolbar-btn-options ul {
        width: 180px !important; 
        margin-top:5px;
        margin-left:5px;
    }

    #notif-container div > ul {
        white-space: nowrap; 
        opacity: 1; 
        left: 1641.61px; 
        position: absolute; 
        top: 64px; 
        display: none;
    }
    .notification-badge {
        font-family: "Poppins", sans-serif;
        position: relative;
        right: 0px;
        top: -20px;
        color: #ffffff;
        background-color: #3f51b5;
        margin: 0 -.8em;
        border-radius: 50%;
        padding: 2px 5px;
    }

    #notifications-dropdown h5 {
        font-size: 1rem;
        text-transform: capitalize;
        font-weight: 500;
    }

    #notifications-dropdown li {
        padding: 8px 16px;
        font-size: 1rem;
    }

    #notifications-dropdown li > a {
        padding: 0;
        font-size: 1.1rem;
        font-weight: 300;
    }

    #notifications-dropdown li > a > span {
        display: inline-block;
        font-size: 1.2rem;
        position: relative;
        top: 4px;
        margin-right: 5px;
    }

    #notifications-dropdown li > time {
        font-size: 0.8rem;
        font-weight: 400;
        margin-left: 38px;
    }

    #notifications-dropdown li.divider {
        padding: 0;
    }

    .small {
        font-size: 1.0rem !important;
    }
    
    .icon-bg-circle {
        color: #fff;
        padding: .4rem;
        border-radius: 50%;
    }
    
    h6 {
        font-size: 1rem;
        line-height: 110%;
        margin: 0.5rem 0 0.4rem 0;
    }
    
    .round {
        display: inline-block;
        height: 30px;
        width: 30px;
        line-height: 30px;
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #222;    
        color: #FFF;
        text-align: center;  
    }

    .disabled{
        pointer-events:none;
        opacity:0.7;
    }
');

?>