<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\workingList\controllers;

use Yii;
use yii\web\Controller;

use app\dataproviders\ArrayDataProvider;

// import models
use app\models\User;
use app\models\Program;
use app\models\VariableSet;
use app\models\VariableSetMember;

use app\modules\inventoryManager\models\TemplatesModel;

use app\modules\workingList\models\WorkingListModel;
use app\modules\workingList\models\ListManagementModel;
use app\modules\workingList\models\TemplateModel;

// import interfaces
use app\interfaces\controllers\IDashboard;
use app\interfaces\modules\inventoryManager\models\IListsModel;

/**
 * Template management controller for the `workingList` module
 */
class TemplateManagementController extends Controller
{
    public $userModel;
    public $dashboardModal;
    public $programModel;
    public $variableSetModel;
    public $variableSetMemberModel;
    public $workingListModel;
    public $workingListTemplateModel;
    public $listsModel;
    public $templatesModel;

    /**
     * Constructor for controller
     */
    public function __construct(
        $id, 
        $module, 
        User $userModel,
        TemplatesModel $templatesModel,
        IDashboard $dashboardModel,
        Program $programModel,
        VariableSet $variableSetModel,
        VariableSetMember $variableSetMemberModel,
        TemplateModel $workingListTemplateModel,
        WorkingListModel $workingListModel,
        IListsModel $listsModel,
        $config = [])
    {
        $this->userModel = $userModel;
        $this->dashboardModal = $dashboardModel;
        $this->programModel = $programModel;
        $this->variableSetModel = $variableSetModel;
        $this->workingListModel = $workingListModel;
        $this->templatesModel = $templatesModel;
        $this->workingListTemplateModel = $workingListTemplateModel;
        $this->variableSetMemberModel = $variableSetMemberModel;
        $this->listsModel = $listsModel;

        parent::__construct($id, $module, $config);
    }

    /**
     * Facilitate download of template from working list
     * 
     * @param String $program program code
     * @param String $type type of template to be downloaded
     * @param Integer $listDbId working list record ID
     * @param String $action intended purpose for the template
     * 
     */
    public function actionDownloadTemplate($program, $type, $listDbId, $action){
        // build template column headers
        $headers = $this->buildTemplateHeaders($program, $type, $listDbId, $action);

        // extract data
        if(!empty($listDbId)) {
            $data = $this->templatesModel->getListData($listDbId, $type, $headers, $action);
            $fileNamePrefix = $this->listsModel->getListAbbrev($listDbId) . "_" . strtoupper($action);
        }
        
        // Build CSV
        if(!empty($headers)){
            $this->templatesModel->buildCSVFile(
                headers: $headers,
                programCode: $program,
                type: $type,
                data: $data,
                fileNamePrefix: $fileNamePrefix
            );
        }
        else{
            return false;
        }
    }

    /**
     * Facilitate building of template column headers
     * 
     * @param String $program program code
     * @param String $type type of template to be downloaded
     * @param Integer $listDbId working list record ID
     * @param String $action intended purpose for the template
     * 
     */
    public function buildTemplateHeaders($program, $type, $listDbId, $action){
        // retrieve IM core config variables
        $configVariables = $this->templatesModel->getConfigurations($program, $type, $action);

        // If no configs found, throw error
        if (empty($configVariables)) {
            $errorMessage = 'No configurations found. '
                . json_encode([
                    'program'=>$program,
                    'type'=>$type,
                    'listDbId'=>$listDbId,
                    'action'=>$action
                ]);
            Yii::error($errorMessage);
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }
        
        // retrieve seed inventory variable set members
        $variableSetAbbrev = "SEED_INVENTORY_".strtoupper($type)."_METADATA";
        $variableSetInfo = $this->variableSetModel->searchAll([ "abbrev" => $variableSetAbbrev ]);

        if(!isset($variableSetInfo['data']) || empty($variableSetInfo['data'])){
            $errorMessage = 'No seed inventory variable set found. '
                . json_encode([
                    'program'=>$program,
                    'type'=>$type,
                    'listDbId'=>$listDbId,
                    'action'=>$action
                ]);
            Yii::error($errorMessage);
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        $variableSetMembers = $this->variableSetMemberModel->getVariableSetMembers($variableSetInfo['data'][0]['variableSetDbId']);
        
        // build column headers
        $columnVariables = array_merge($configVariables,$variableSetMembers);
        $columnHeaders = $this->workingListTemplateModel->extractTemplateHeaders($columnVariables,$type);

        $columnHeaders = $columnHeaders ?? [];

        return $columnHeaders;
    }
}