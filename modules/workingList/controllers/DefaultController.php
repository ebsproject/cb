<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\workingList\controllers;

use Yii;

use yii\web\Controller;

// import models
use app\models\User;
use app\models\Program;
use app\models\BackgroundJob;
use app\modules\workingList\models\WorkingListModel;

// import interfaces
use app\interfaces\controllers\IDashboard;
use app\interfaces\models\IPlatformListMember;
use app\interfaces\models\IListMember;

/**
 * Default controller for the `workingList` module
 */
class DefaultController extends Controller
{
    public $userModel;
    public $workingListModel;
    public $dashboardModal;
    public $programModel;
    public $listMemberModel;
    public $backgroundJobModel;

    /**
     * Constructor for controller
     */
    public function __construct(
        $id, 
        $module, 
        User $userModel,
        WorkingListModel $workingListModel,
        IDashboard $dashboardModel,
        Program $programModel,
        IListMember $listMemberModel,
        BackgroundJob $backgroundJobModel,
        $config = [])
    {
        $this->userModel = $userModel;
        $this->workingListModel = $workingListModel;
        $this->dashboardModal = $dashboardModel;
        $this->programModel = $programModel;
        $this->listMemberModel = $listMemberModel;
        $this->backgroundJobModel = $backgroundJobModel;

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders index page for Working List viewer page
     * 
     * @param String $sourceTool name of source tool
     * @param String $listType type of working list
     * @param String $returnUrl url of source tool
     * 
     */
    public function actionIndex($sourceTool = null,$listType = null,$returnUrl = null) {
        $pageValues = [];
        $params = [];

        //Default values if parameters are null
        $sourceTool = $sourceTool ?? 'Seed Search';
        $listType = $listType ?? 'package';
        $returnUrl = $returnUrl ?? 'seedInventory/find-seeds/find';

        // set session variables
        Yii::$app->session->set('listUpdatingStatus', 'false');

        //get value of column filter in array and set value to params for filter
        if(isset($_GET['WorkingListModel'])){
            $params = array_filter($_GET['WorkingListModel'], 'strlen') ?? '';
        }

        if(isset($sourceTool) && isset($listType)){
            // retrieve user ID
            $userId = $this->userModel->getUserId();

            $listInfo = $this->workingListModel->getWorkingList($userId,$listType);

            // if has no existing working with the given type
            if(isset($listInfo) && empty($listInfo)){
                $createNewList = $this->workingListModel->createWorkingList($userId,$listType);

                if($createNewList == 200){
                    $listInfo = $this->workingListModel->getWorkingList($userId,$listType);
                }
            }

            // Retrieve program code given the dashboard program filter
            $defaultFilters = $this->dashboardModal->getFilters();
            $programId = $defaultFilters->program_id;
            $programInfo = $this->programModel->getProgram($programId);
            $programCode = isset($programInfo['programCode']) ? $programInfo['programCode'] : '';

            // build browser columns
            $variableConfig = $this->workingListModel->getVariableConfiguration($programCode);
            $variableConfig = $this->workingListModel->validateVariables($variableConfig);
            $browserColumns = $this->workingListModel->buildBrowserColumns($variableConfig);

            $searchModel = $this->workingListModel;
            $this->workingListModel->setColumnsForFiltering($variableConfig);

            $pageValues = [
                'listInfo' => $listInfo,
                'sourceTool' => $sourceTool,
                'listType' => $listType,
                'returnUrl' => $returnUrl,
                'columns' => $browserColumns,
                'searchModel' => $searchModel,
                'params' => $params,
                'browserId' => 'datagrid-working-list-main-browser',
                'program' => $programCode,
                'templateAction' => 'update'
            ];
        }

        // Render page
        return $this->render('index', $pageValues);
    }

    /**
     * Facilitates retrieval of background process notifications
     * 
     * @param String $workerNames worker names
     * 
     */
    public function actionPushNotifications($workerNames = null){
        // Build search parameters
        $workerName = $workerNames ?? 'UpdateListItems|ExecuteProcessor|BuildCsvData|AddListItems';
        $remarks = 'working list';

        // Get user ID
        $userId = $this->userModel->getUserId();

        // retrieve the unseen notifications
        $params = $this->workingListModel->processNotifParams($workerName,$remarks,'false', $userId);
        $filter = 'sort=modificationTimestamp:desc';   

        $result = $this->backgroundJobModel->searchAll($params, $filter);

        $newNotifs = $result['data'];
        $unseenNotifCount = count($newNotifs);
        $unseenNotifications = $this->backgroundJobModel->getGenericNotifications($newNotifs);

        // retrieve the already viewed notifications
        $params = $this->workingListModel->processNotifParams($workerName,$remarks,'true', $userId);
        $filter = 'limit=5&sort=modificationTimestamp:desc';   

        $result = $this->backgroundJobModel->searchAll($params, $filter, false);

        $viewedNotifs = $result['data'];

        foreach($viewedNotifs as $key => $value){
            if($value['jobStatus'] == 'DONE' && $value['workerName'] == 'BuildCsvData'){
                $viewedNotifs[$key]['message'] = 'Preparing export file ('.$value['jobRemarks'].') for download.';
            }
        }

        $viewedNotifCount = count($viewedNotifs);
        $viewedNotifications = $this->backgroundJobModel->getGenericNotifications($viewedNotifs);

        // update isSeen value of unseen notifications
        $result = $this->backgroundJobModel->update($newNotifs, ["jobIsSeen" => "true"]);
        
        return $this->renderAjax(
            '_notifications',
            [
                "unseenNotifications" => $unseenNotifications,
                "seenNotifications" => $viewedNotifications,
                "unseenCount" => $unseenNotifCount,
                "seenCount" => $viewedNotifCount,
            ]
        );
    }

    /**
     * Retrieve count of new background process notifications
     * 
     * @param String $workerName name of workers
     * @param String $remarks process remarks
     * 
     * @return JSON totalCount
     */
    public function actionNewNotificationsCount($workerName='',$remarks=''){

        // Get user ID
        $userId = $this->userModel->getUserId();

        $params = $this->workingListModel->processNotifParams($workerName,$remarks,'false', $userId);
        $filter = 'sort=modificationTimestamp:desc';   

        $result = $this->backgroundJobModel->searchAll($params, $filter);        

        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        }

        return json_encode($result["totalCount"]);
    }

    /**
     * Download data from filepath 
     * 
     * @param String $filename Name of file to be downloaded
     * @param String $dataFormat file format
     */
    public function actionDownloadData($filename,$dataFormat){
        $filename = $_POST['filename'] ?? $filename;
        $filename = str_contains($filename, '.') ? $filename : "$filename.$dataFormat";

        $pathToFile = realpath(Yii::$app->basePath) . "/files/data_export/$filename";

        if (file_exists($pathToFile)) {
            // Set headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($pathToFile));

            // Flush system output buffer
            flush();

            // Read/Download CSV file
            readfile($pathToFile);

            die();
        }
    }

    /**
     * Check for active background processes for updating list items
     * 
     * @return mixed
     */
    public function actionCheckForActiveUpdates()
    {
        // Build search parameters
        $workerName = 'UpdateListItems';
        $remarks = 'working list';

        // Get user ID
        $userId = $this->userModel->getUserId();

        // retrieve the unseen notifications
        $params = $this->workingListModel->processNotifParams($workerName, $remarks, 'false', $userId);
        $filter = 'sort=modificationTimestamp:desc';

        $result = $this->backgroundJobModel->searchAll($params, $filter);
        $newNotifs = $result['data'];

        // retrieve the already viewed notifications
        $params = $this->workingListModel->processNotifParams($workerName, $remarks, 'true', $userId);
        $filter = 'limit=5&sort=modificationTimestamp:desc';

        $result = $this->backgroundJobModel->searchAll($params, $filter, false);
        $viewedNotifs = $result['data'];

        $jointNotif = array_merge($newNotifs, $viewedNotifs);
        $hasUpdateListProcess = array_filter($jointNotif, function ($x) {

            return $x['workerName'] == "UpdateListItems" &&
                $x["jobStatus"] == "IN_PROGRESS" &&
                $x['message'] == "Updating of list items is in progress";
        });

        if (isset($hasUpdateListProcess) && !empty($hasUpdateListProcess)) {
            Yii::$app->session->set('listUpdatingStatus', 'true');

            return json_encode(['status' => true]);
        } else if (isset($hasUpdateListProcess) && empty($hasUpdateListProcess)) {
            Yii::$app->session->set('listUpdatingStatus', 'false');

            return json_encode(['status' => false]);
        }
    }
}
