<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\workingList\controllers;

use Yii;
use yii\web\Controller;

use app\dataproviders\ArrayDataProvider;

// import models
use app\models\User;
use app\models\Program;
use app\models\Worker;
use app\models\PlatformList;

use app\models\EntryList;
use app\models\Entry;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\EntryOrderModel;

use app\modules\workingList\models\WorkingListModel;
use app\modules\workingList\models\ListManagementModel;

// import interfaces
use app\interfaces\controllers\IDashboard;
use app\interfaces\models\IPlatformListMember;
use app\interfaces\models\IListMember;
use app\interfaces\models\ILists;

/**
 * List management controller for the `workingList` module
 */
class ListManagementController extends Controller
{
    public $userModel;
    public $dashboardModal;
    public $programModel;
    public $listMemberModel;
    public $listsModel;
    public $platformListMemberModel;
    public $platformListModel;
    public $experimentModel;
    public $entryListModel;
    public $entryModel;
    public $worker;

    public $entryOrderModel;
    public $workingListModel;
    public $listMgtModel;

    /**
     * Constructor for controller
     */
    public function __construct(
        $id, 
        $module, 
        User $userModel,
        WorkingListModel $workingListModel,
        ListManagementModel $listMgtModel,
        IDashboard $dashboardModel,
        Program $programModel,
        IListMember $listMemberModel,
        ILists $listsModel,
        IPlatformListMember $platformListMemberModel,
        PlatformList $platformListModel,
        ExperimentModel $experimentModel,
        EntryList $entryListModel,
        EntryOrderModel $entryOrderModel,
        Entry $entryModel,
        Worker $worker,
        $config = [])
    {
        $this->userModel = $userModel;
        $this->dashboardModal = $dashboardModel;
        $this->programModel = $programModel;
        $this->listMemberModel = $listMemberModel;
        $this->listsModel = $listsModel;
        $this->platformListMemberModel = $platformListMemberModel;
        $this->platformListModel = $platformListModel;
        $this->experimentModel = $experimentModel;
        $this->entryListModel = $entryListModel;
        $this->entryModel = $entryModel;
        $this->entryOrderModel = $entryOrderModel;
        $this->worker = $worker;
        
        $this->workingListModel = $workingListModel;
        $this->listMgtModel = $listMgtModel;

        parent::__construct($id, $module, $config);
    }

    /**
     * Facilitates saving of list item order
     * 
     * @return JSON success status and process message
     */
    public function actionSaveListOrder() {
        $postValues = $_POST;

        $listType = $postValues['listType'] ?? null;
        $sortParams = $postValues['sort'] ?? '';

        $userId = $this->userModel->getUserId();

        if(isset($listType) && isset($userId)){
            $listInfo = $this->workingListModel->getWorkingList($userId,$listType);

            $listId = intval($listInfo['listDbId']);
            $memberCount = intval($listInfo['memberCount']);

            $threshValue = Yii::$app->config->getAppThreshold('WORKING_LIST','updateListItems');

            if($memberCount > 0){
                if(isset($threshValue) && !empty($threshValue)){
                    $processedSort = $this->workingListModel->processSortParams($sortParams);

                    if($memberCount < $threshValue){
                        $reorderItems = $this->listMgtModel->updateListItemOrder($processedSort,$listId);

                        if($reorderItems != 200){
                            return json_encode([
                                'success' => false,
                                'bgprocess' => false,
                                'message' => 'Error encountered while reordering list item records.'
                            ]);
                        }

                        return json_encode([
                            'success' => true,
                            'bgprocess' => false,
                            'message' => 'Successfully updated the ordering of the list items.'
                        ]);
                    }
                    else{
                        // invoke worker
                        $this->initiateWorker(
                            $listId,
                            'UpdateListItems',
                            'Update '.strtolower($listType).' working list items for user: '.$userId,
                            'LIST',
                            'POST',
                            [
                                "listId" => $listId,
                            ],
                            null,
                            [
                                "processName" => 'reorder-all-by-sort',
                                "sort" => $processedSort
                            ],
                            'updating the list order'
                        );

                        return json_encode([
                            'success' => true,
                            'bgprocess' => true,
                            'message' => ''
                        ]);
                    }                        
                }
            }
            else{
                return json_encode([
                    'success' => true,
                    'bgprocess' => false,
                    'message' => 'No items to be updated.'
                ]);
            }
        }
        else{
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'Missing required information to update list item records.'
            ]);
        }
    }

    /**
     * Facilitate removing selected list items
     * 
     * @return JSON success status and process message
     */
    public function actionRemoveSelectedItems(){
        $postValues = $_POST;

        $listId = $postValues['listId'] ?? null;

        $selectedIds = $postValues['selectedItemIds'] ?? '';
        $selectedIds = json_decode($selectedIds);
        $selectedIdsCount = count($selectedIds);

        // Retrieve delete threshold
        $threshValue = Yii::$app->config->getAppThreshold('WORKING_LIST','deleteWorkingList') ?? 1000;

        if(!empty($selectedIds)){
            if(isset($threshValue) && !empty($threshValue)){
                if($selectedIdsCount > 0 && $selectedIdsCount < $threshValue){

                    $removeItems = $this->listMgtModel->removeItems($selectedIds,false);

                    if($removeItems != 200){
                        return json_encode([
                            'success' => false,
                            'bgprocess' => false,
                            'message' => 'Error encountered while removing items from list.'
                        ]);
                    }

                    return json_encode([
                        'success' => true,
                        'bgprocess' => false,
                        'message' => 'Successfully removed items from the list.'
                    ]);
                }
                else if($selectedIdsCount > 0 && $selectedIdsCount > $threshValue){
                    $removeItems = $this->platformListMemberModel->bulkDelete(implode('|',$selectedIds));

                    return json_encode([
                        'success' => true,
                        'bgprocess' => false,
                        'message' => 'Successfully removed items from the list.'
                    ]);
                }
                else if($selectedIdsCount == 0){
                    return json_encode([
                        'success' => true,
                        'bgprocess' => false,
                        'message' => 'No items selected.'
                    ]);
                }
            }
        }
        else{
            return json_encode([
                'success' => true,
                'bgprocess' => false,
                'message' => 'No items selected.'
            ]);
        }
    }

    /**
     * Facilitate clearing the items in the working list
     * 
     * @return JSON success status and process message
     */
    public function actionClearList(){
        $postValues = $_POST;

        $listId = $postValues['listId'] ?? null;
        $listType = $postValues['listType'] ?? '';

        // list ID and list type values are not available
        if(!isset($listId) || empty($listId) || empty($listType)){
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'Missing required fields.'
            ]);
        }

        // Retrieve delete threshold
        $threshValue = Yii::$app->config->getAppThreshold('WORKING_LIST','deleteWorkingList') ?? 1000;

        // Retrieve working list
        $userId = $this->userModel->getUserId();
        $listInfo = $this->workingListModel->getWorkingList($userId,$listType);

        $memberCount = intval($listInfo['memberCount']);

        if(!isset($memberCount) || empty($memberCount) || !isset($threshValue) || empty($threshValue)){
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'Error encountered while clearing list. Issues with member count and threshold values.'
            ]);
        }

        // Use backend process if less than threshold value
        if(isset($memberCount) && !empty($memberCount) && $memberCount < $threshValue){
            // Retrieve list members
            $params = [
                "fields" => 'listMember.id AS listMemberDbId|listMember.data_id AS '.strtolower($listType).'DbId'
            ];
            $listMembers = $this->platformListMemberModel->searchAllMembers($listId,'',$params);

            if(isset($listMembers['data']) && !empty($listMembers['data'])){
                $data = $listMembers['data'];
                $listMemberIds = array_column($data,"listMemberDbId");

                $removeItems = $this->listMgtModel->removeItems($listMemberIds,false);
                
                if($removeItems != 200){
                    return json_encode([
                        'success' => false,
                        'bgprocess' => false,
                        'message' => 'Error encountered while clearing list.'
                    ]);
                }

                return json_encode([
                    'success' => true,
                    'bgprocess' => false,
                    'message' => 'Successfully cleared the working list.'
                ]);
            }
        }

        // Use background process worker if greater than threshold value
        if(isset($memberCount) && !empty($memberCount) && $memberCount > $threshValue){
            // delete current working list
            $prevList = $this->workingListModel->deleteWorkingList($listId,$listType,$userId);

            // create new working list
            $newList = $this->workingListModel->createWorkingList($userId,$listType);

            // void list items of previous working list in bg process
            
            // invoke worker
            $additionalParams = [
                "description" => "Clearing of working list items",
                "entity" => "LIST",
                'entityDbId' => (string) $listId, 
                "endpointEntity" => "LIST_MEMBER",
                "application" => "LISTS",
                'requestData' => [
                    "prerequisite" => [
                        "endpoint" => "lists/".$listId."/members-search"
                    ]
                ]
            ];

            $response = $this->listMemberModel->deleteMany("listMemberDbId", true, $additionalParams);
    
            if($response['status'] != 200){
                return json_encode([
                    'success' => false,
                    'bgprocess' => false,
                    'message' => 'Error encountered while clearing list with background process.'
                ]);
            }

            // inform user
            Yii::$app->session->setFlash(
                'success', 
                '<i class="fa fa-check"></i>&nbsp;'.
                'The background process for clearing the working lists is in progress. You will be informed once the process is completed.'
            );

            return json_encode([
                'success' => true,
                'bgprocess' => true,
                'message' => 'Background process is in progress.'
            ]);
        }
    }

    /**
     * Facilitate validation of list info
     *  
     * @return JSON success status and process message
     */
    public function actionValidateListInfo(){
        $postValues = $_POST;
        $listAbbrev = $postValues['abbrev'] ?? '';

        if(!empty($listAbbrev)){
            $validateAbbrev = $this->platformListModel->validateAbbrev($listAbbrev);

            if(!$validateAbbrev){
                return json_encode([
                    'success' => false,
                    'bgprocess' => false,
                    'message' => 'Abbrev already exists!'
                ]);
            }

            return json_encode([
                'success' => true,
                'bgprocess' => false,
                'message' => ''
            ]);            
        }
        else{
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'No abbrev to be validated.'
            ]);
        }
    }
    
    /**
     * Facilitates saving of working list items to newly create list
     * 
     *  @return JSON success status and process message
     */
    public function actionSaveList(){
        $postValues = $_POST;

        $listInfo = $postValues['listInfo'] ?? [];
        $listType = $postValues['listType'] ?? [];
        $sortParams = $postValues['sort'] ?? '';

        // Retrieve save threshold
        $threshValue = Yii::$app->config->getAppThreshold('WORKING_LIST','saveList') ?? 1000;

        // Retrieve current working list info
        $userId = $this->userModel->getUserId();
        $workingListInfo = $this->workingListModel->getWorkingList($userId,$listType);
        $memberCount = isset($workingListInfo) && !empty($workingListInfo) ? intval($workingListInfo['memberCount']) : null;

        if(!empty($listInfo)){
            // create new list record
            $listInfoParams = [
                'abbrev'=> $listInfo[1]['abbrev'],
                'name'=> $listInfo[0]['name'],
                'description'=> $listInfo[4]['description'],
                'displayName'=> $listInfo[2]['display_name'],
                'remarks'=> $listInfo[5]['remarks'],
                'type'=> $listInfo[3]['type'],
                'status' => 'created',
                'isActive' => 'true',
                'listUsage' => 'final list'
            ];

            $newListInfo = $this->workingListModel->createNewList($listInfoParams);

            if($newListInfo['status'] == 200){
                // retrieve id of new list
                $newListInfo = $newListInfo['body']['result']['data'][0] ?? [];
                $newListId = $newListInfo['listDbId'] ?? null;

                if(!empty($newListInfo) && isset($newListId)){
                    // update list_id of current working list items to new list
                    if($memberCount < $threshValue){
                        // save current sorting of items
                        $reorderItems = $this->saveListSorting($sortParams,$workingListInfo['listDbId']);

                        if($reorderItems == 200){
                            $params = [
                                "fields" => 'listMember.id AS listMemberDbId|listMember.data_id AS '.strtolower($listType).'DbId'
                            ];
    
                            $workingListMemberIds = $this->workingListModel->getListItems($params,$workingListInfo['listDbId']);

                            if(!empty($workingListMemberIds)){
                                $workingListMemberIds = array_column($workingListMemberIds,'listMemberDbId');
    
                                $params = [
                                    "listDbId" => $newListId
                                ];

                                $updateItems = $this->platformListMemberModel->bulkUpdate(implode('|',$workingListMemberIds),$newListId,$params);

                                if($updateItems['status'] != 200){

                                    return json_encode([
                                        'success' => false,
                                        'bgprocess' => false,
                                        'message' => 'Error encountered in updating working list items.'
                                    ]);
                                }
    
                                Yii::$app->session->setFlash(
                                    'success', 
                                    '<i class="fa fa-check"></i>&nbsp;'.
                                    'Successfully saved the working list items. '.
                                    'Navigate to the <strong>LIST MANAGER</strong> tool to view the created list <strong>'.
                                    $listInfo[1]['abbrev'].'</strong>.'
                                );
    
                                return json_encode([
                                    'success' => true,
                                    'bgprocess' => false,
                                    'message' => ''
                                ]);
                            }
                        }
                        else{

                            return json_encode([
                                'success' => false,
                                'bgprocess' => false,
                                'message' => 'Error encountered in saving items sort.'
                            ]);
                        }
                    }
                    else{

                        // process sort parameters
                        $processedSort = $this->workingListModel->processSortParams($sortParams);

                        // Enable workers for large lists
                        $this->bgSaveList($workingListInfo['listDbId'], $newListId, $userId, $listType,$processedSort);

                        return json_encode([
                            'success' => true,
                            'bgprocess' => true,
                            'message' => ''
                        ]);
                    }
                }
                else{

                    return json_encode([
                        'success' => false,
                        'bgprocess' => false,
                        'message' => 'Error encountered in saving list information.'
                    ]);
                }
            }
            else{

                return json_encode([
                    'success' => false,
                    'bgprocess' => false,
                    'message' => 'Error encountered in creating new package list.'
                ]);
            }
        }
        else{

            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'No list info to be saved.'
            ]);
        }
    }

    /**
     * Rollback failed save list
     */
    public function actionRollbackSaveList(){
        $postParams = $_POST;
        $abbrev = $postParams['abbrev'] ?? '';

        if($abbrev != ''){
            // retrieve saved list
            $url = 'lists-search';
            $param = [ "abbrev" => $abbrev ];
            $savedList = Yii::$app->api->getResponse('POST',$url,json_encode($param));

            if($savedList['status'] == 200){
                $listData = $savedList['body']['result']['data'][0] ?? [];

                // delete saved list
                if( isset($listData['listDbId']) && 
                    !empty($listData['listDbId']) && 
                    $listData['memberCount'] == 0){
                    
                    $this->listsModel->deleteOne($listData['listDbId']);

                    return json_encode([ 'success' => true ]);
                }
            }
        }
    }

    /**
     * Facilitates exporting of working list items
     * 
     * @param String $listType type of list
     * @param Integer $listId working list ID
     * @param String $sortParams sort parameters
     */
    public function actionExportWorkingList($listType,$listId,$sortParams = ''){

        // retrieve working list information
        $userId = $this->userModel->getUserId();
        $listInfo = $this->workingListModel->getWorkingList($userId,$listType);

        // extract member count
        $memberCount = isset($listInfo['memberCount']) && !empty($listInfo['memberCount']) ? intval($listInfo['memberCount']) : null;
        $workingListId = $listInfo['listDbId'] ?? $listId;

        if($memberCount == 0){
            // redirect
            Yii::$app->response->redirect(["/workingList"]);
        }

        // retrieve threshold
        $threshValue = Yii::$app->config->getAppThreshold('WORKING_LIST','exportWorkingList') ?? 1000;

        $variableConfig = $this->getValidatedConfigVariables();
        $exportColumnVariables = $this->listMgtModel->buildExportColumns($variableConfig);

        // build CSV filename
        $timestamp = date("Ymd_His", time());
        $filename = ucwords($listType).'_Working_List_Items_'.$timestamp.'.csv';

        if(isset($exportColumnVariables) && !empty($exportColumnVariables)){
            if(isset($threshValue) && !empty($threshValue)){
                // save current sorting of items
                $reorderItems = $this->saveListSorting($sortParams,$workingListId);

                if($memberCount < $threshValue){ // regular backend process
                    if($reorderItems == 200){
                        // retrieve working list members
                        $params = [];
                        $workingListMembers = $this->workingListModel->getListItems($params,$workingListId);

                        if(isset($workingListMembers) && !empty($workingListMembers)){
                            // build csv content
                            $csvContent = $this->listMgtModel->buildCSVContent($exportColumnVariables,$workingListMembers);

                            // generate CSV file
                            header("Content-Disposition: attachment; filename={$filename}; Content-Type: application/csv;");
                            $fp = fopen('php://output', 'wb');

                            if ($fp === false) die('Failed to create CSV file.');

                            foreach ($csvContent as $line) fputcsv($fp, $line);

                            fclose($fp);

                            exit;
                        }
                    }
                }
                else{ 
                    $csvHeaders = array_values($exportColumnVariables);
                    $csvAttributes = array_keys($exportColumnVariables);

                    $workerParams = [
                        'url' => "lists/$workingListId/members-search?sort=orderNumber",
                        'requestBody' => '',
                        'description' => 'Preparing CSV Working List for downloading',
                        'csvHeaders' => $csvHeaders,
                        'csvAttributes' => $csvAttributes,
                        'fileName' => $filename,
                        'httpMethod' => 'POST',
                    ];

                    // invoke worker
                    $this->initiateWorker(
                        $listId,
                        'BuildCsvData',
                        'Export '.strtolower($listType).' working list items for user: '.$userId,
                        'LIST',
                        'POST',
                        $workerParams,
                        [
                            'remarks' => $filename,
                        ],
                        null,
                        'exporting working list items'
                    );

                    // redirect
                    Yii::$app->response->redirect(["/workingList"]);
                }
            }
            else{
                return json_encode([
                    'success' => false,
                    'bgprocess' => false,
                    'message' => 'Error encountered in retrieving threshold value.'
                ]);
            }
        }
        else{
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'Error encountered in retrieving export file column values.'
            ]);
        }
    }

    /** 
     * Retrieve and render experiment data provider for Export List to Experiment modal browser
     */
    public function actionGetExperimentDataProvider(){
        // retrieve current program
        $program = $this->getDashboardProgram();

        // add condition for filtering owned experiments
        $experimentData = $this->listMgtModel->getExperimentDataProvider($program);

        $experimentDataProvider = null;
        if($experimentData['status'] !== 200){
            $experimentDataProvider = new ArrayDataProvider([
                'key' => '',
                'allModels' => [],
                'pagination' => false
            ]);
        }
        else{
            $experimentDataProvider = new ArrayDataProvider([
                'key' => 'experimentDbId',
                'allModels' => $experimentData['data'],
                'pagination' => false
            ]);
        }

        return Yii::$app->controller->renderPartial(
            '@app/modules/workingList/views/modals/_export_to_experiment', 
            [
                'experimentDataProvider'=> $experimentDataProvider,
                'dataType'=>'exp-datatype'
            ]
        );
    }

    /**
     * Facilitate exporting of working list items to selected Experiment
     * 
     * @param $listType type of working list
     */
    public function actionExportListToExperiment($listType){
        $postParams = $_POST;
        $experimentId = $postParams['experimentId'];
        $workingListId = $postParams['listId'];

        if(!isset($experimentId) || empty($experimentId)){
            return json_encode($result = [
                'success' => false,
                'message' => 'No experiment selected!'
            ]);
        }

        // retrieve experiment information
        $experimentData = $this->experimentModel->getOne($experimentId);
        $experiment = $this->experimentModel->getExperiment($experimentId);

        //Get entry list
        $entryListId = $this->entryListModel->getEntryListId($experimentId, true);

        // retrieve program id and information
        $defaultFilters = $this->dashboardModal->getFilters();
        $programId = $defaultFilters->program_id;
        $program = $this->getDashboardProgram();

        // retrieve entry config
        $configData = $this->experimentModel->retrieveConfiguration(
            'specify-entry-list', 
            $program, 
            $experimentId, 
            $experiment['dataProcessDbId']
        );

        // retrieve list information
        $userId = $this->userModel->getUserId();
        $listInfo = $this->workingListModel->getWorkingList($userId,$listType);

        if(!isset($listInfo) || empty($listInfo)){
            return json_encode($result = [
                'success' => false,
                'message' => 'Error encountered in retrieving list information!'
            ]);
        }

        // retrieve create entries threshold
        $addEntryThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries') : 500;

        if($listInfo['memberCount'] > $addEntryThreshold){
            // create entries via background worker
            $bgExportProcess = $this->exportListToExperimentViaBgProcess($experimentId,$listInfo['listDbId'],$entryListId,$configData);

            if($bgExportProcess){
                //redirect to experiment creation
                Yii::$app->session->setFlash(
                    'info', 
                    '<i class="fa fa-info-circle"></i> The entry records for <strong>'.$experiment['experimentName'].
                    '</strong> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.'
                );

                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$experimentId]);
            }
            else{
                //redirect to experiment creation
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$experimentId]);
            }
        }
        else{
            $newEntryListRecords = $this->listMgtModel->getNewEntryListRecords(
                $experimentId,
                $experiment,
                $configData,
                $entryListId,
                $listInfo,
                $programId
            );

            if(isset($newEntryListRecords) && !empty($newEntryListRecords)){
                // create entry list records
                $createdEntryIds = $this->entryModel->createRecords($newEntryListRecords);

                //update experiment status
                $this->entryOrderModel->entryListAfterUpdate($experimentId);

                //Update seedDbId for entry if list is product
                if(in_array($listInfo['type'], ['designation', 'germplasm'])){
                    $this->experimentModel->autoSelectSinglePackage($entryListId, $experimentId);
                }

                // redirect to experiment creation page     
                Yii::$app->session->setFlash(
                    'info', 
                    '<i class="fa fa-info-circle"></i> The entry records for <strong>'.$experiment['experimentName'].
                    '</strong> have been successfully created.'
                );

                Yii::$app->response->redirect(
                    ['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$experimentId]
                );
            }
            else{
                return json_encode([
                    'success' => false,
                    'message' => 'Error encountered in exporting list items to '.$experiment['programCode']
                ]);
            }  
        }                                           
    }

    /**
     * Facilitate exporting of working list items via background process
     * 
     * @param Integer $experimentId selected experiment ID
     * @param Integer $listId working list ID
     * @param Integer $entryListId experiment entry list ID
     * @param Array $configData experiment entry configuration data
     * 
     * @return Boolean success status
     */
    public function exportListToExperimentViaBgProcess($experimentId,$listId,$entryListId,$configData){
        foreach($configData as $config){
            if(isset($config['default']) && !empty($config['default'])){
                switch($config['variable_abbrev']){
                    case 'ENTRY_TYPE':
                        $entryType = $config['default'];
                        break;
                    case 'ENTRY_CLASS':
                        $entryClass = $config['default'];
                        break;
                    case 'ENTRY_ROLE':
                        $entryRole = $config['default'];
                        break;
                    default:
                        break;
                }
            }
        }

        // build params for background process
        $addtlParams = [
            "description"=>"Creating of entry records",
            "entity"=>"ENTRY",
            "entityDbId"=>$experimentId,
            "endpointEntity"=>"ENTRY", 
            "application"=>"EXPERIMENT_CREATION"
        ];

        $dataArray = [
            'entity'=>'list',
            'entityId' => $listId."",
            'defaultValues' => [
                'entryListDbId' => $entryListId,
                'entryType' => isset($entryType) ? $entryType : null,
                'allowedEntryType' => null,
                'entryClass' => isset($entryClass) ? $entryClass : null,
                'entryRole' => isset($entryRole) ? $entryRole : null
            ],
            'processName' => 'create-entry-records',
            'experimentDbId' => $experimentId.""
        ];

        // call background process for creating entries
        $createEntries = $this->experimentModel->create($dataArray, true, $addtlParams);
        
        // retrieve experiment information
        $experiment = $this->experimentModel->getExperiment($experimentId);
        $experimentStatus = $experiment['experimentStatus'];

        $status = [];
        if(!empty($experimentStatus)){
            $status = explode(';',$experimentStatus);
            $status = array_filter($status);
        }

        if(!empty($createEntries['data'])){
            $bgStatus = $this->experimentModel->formatBackgroundWorkerStatus($createEntries['data'][0]['backgroundJobDbId']);

            if($bgStatus !== 'done' && !empty($bgStatus)){
                $updatedStatus = implode(';', $status);
                $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;

                $this->experimentModel->updateOne($experimentId, ["experimentStatus"=>"$newStatus"]);

                return true;
            }
        }
        else{
            return false;
        }
    }

    /**
     * Retrieve programCode value of dashboard program filter
     * 
     * @return String programCode value
     */
    public function getDashboardProgram(){
        $defaultFilters = $this->dashboardModal->getFilters();
        $programId = $defaultFilters->program_id;
        $programInfo = $this->programModel->getProgram($programId);
        $programCode = isset($programInfo['programCode']) ? $programInfo['programCode'] : '';

        return $programCode;
    }

    /**
     * Faciliate retrieval of saved lists
     * 
     * @param $listType working list type
     * @param $action type of action to be applied; default value is import
     */
    public function actionGetSavedLists($listType, $action = 'import'){

        $userModel = new User();
        $userId = $userModel->getUserId();
        
        $postParams = $_POST;
        if(isset($postParams['listType'])){
            $listType = $postParams['listType'];
        }

        if(isset($postParams['action'])){
            $action = $postParams['action'];
        }

        $params = [
            'type' => 'equals '.$listType,
            'creatorDbId' => `equals $userId`,
            'isActive' => 'true'
        ];

        // limit shown lists to those with read_write access
        if(isset($postParams['action']) && $postParams['action'] == 'save'){
            $params['permission'] = 'read_write';
        }

        $data = $this->listsModel->searchAll($params, '', true);

        $dataProvider = new \yii\data\ArrayDataProvider([
            'key' => 'listDbId',
            'allModels' => $data['data'],
            'pagination' => false
        ]);

        return Yii::$app->controller->renderPartial('/modals/_import_saved_list_content', [
            'name' => '',
            'abbrev' => '',
            'display_name' => '',
            'type' => '',
            'description' => '',
            'remarks' => '',
            'id' => null,
            'header' => '',
            'showTabs' => false,
            'create' => true,
            'dataProvider' => $dataProvider,
            'searchModel' => $this->listsModel,
            'listType' => $listType,
            'action' => $action
        ]);
    }

    /**
     * Faciliate retrieval of items from selected saved list
     * 
     * @param String $listType type of working list
     * 
     * @return mixed success status, bg process, process message 
     */
    public function actionGetSavedListItems($listType){
        $postParams = $_POST;
        $listId = $postParams['listId'] ?? null;
        $workingListId = $postParams['workingListId'] ?? null;
        $workingListLimit = 40000; // Working list maximum limit

        // Get user id
        $userId = $this->userModel->getUserId();

        if(isset($listId)){
            // Get working list item count
            $listInfo = $this->workingListModel->getWorkingList($userId,$listType);
            $listMemberCount = isset($listInfo['memberCount']) ? intval($listInfo['memberCount']) : 0;

            // get list item ids
            $savedistItems = $this->listsModel->searchAllMembers(
                $listId,
                [
                    'fields' => "listMember.data_id AS id|listMember.display_value AS displayValue",
                    'isBasic' => "true"
                ]
            );

            // retrieve list item ids
            $newItemIdsArr = $savedistItems['data'] ?? [];
            // get total number of import list item that will be added in working list
            $savedListItemCount = isset($savedistItems['totalCount']) ? intval($savedistItems['totalCount']) : 0;
            $estimatedWorkingListCount = $savedListItemCount + $listMemberCount;

            // add items to working list
            if($estimatedWorkingListCount < $workingListLimit){
                // Check bgprocess
                $isBgProcess = $this->checkForBgProcess(count($newItemIdsArr),'addListItems');

                if(!empty($newItemIdsArr) && !$isBgProcess){
                    $addResults = $this->listMgtModel->addItemsToWorkingList($workingListId,$newItemIdsArr);
                    
                    if (isset($addResults['status']) && $addResults['status'] !== 200) {
                        return json_encode([
                            'success' => false,
                            'bgprocess' => false,
                            'message' => 'Error encountered in loading working list items.'
                        ]);
                    }

                    return json_encode([
                        'success' => true,
                        'bgprocess' => false,
                        'message' => 'Successfully loaded list items.'
                    ]);
                }
                else if(!empty($newItemIdsArr) && $isBgProcess){
                    $this->addItemsViaBgProcess($listId,$workingListId,$listType,$userId);

                    return json_encode([
                        'success' => true,
                        'bgprocess' => true,
                        'message' => 'A background process will facilitate the addition of items.'
                    ]);
                }
                else{
                    return json_encode([
                        'success' => false,
                        'bgprocess' => false,
                        'message' => 'Error encountered in loading working list items.'
                    ]);
                }
            }
            else{
                return json_encode([
                    'success' => false,
                    'bgprocess' => false,
                    'message' => 'Reached maximum working list limit of '.$workingListLimit
                ]);
            }
        }
        else{
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'Error encountered in loading working list items.'
            ]);
        }       
    }

    /**
     * Verify if process needs to be faciitated by a background process
     * 
     * @param Integer $itemCount total number of items
     * @param String $threshAttrib threshold attribute value
     * 
     * @return Boolean if total item count exceeds threshold value
     */
    public function checkForBgProcess($itemCount,$threshAttrib){
        // retrieve threshold
        $threshValue = Yii::$app->config->getAppThreshold('WORKING_LIST',$threshAttrib) ?? 1000;

        return ($itemCount > $threshValue);
    }

    /**
     * Facilitate addition of items via background process
     * 
     * @param $listId source list identifier
     * @param $workingListId working list identifier
     * @param $listType working list type
     * @param $userId user identifier
     */
    public function addItemsViaBgProcess($listId,$workingListId,$listType,$userId){
        $workerParams = [
            "data" => [
                "sourceListDbId" => "".$listId
            ],
            "listDbId" => "".$workingListId
        ];
        $additionalBgJobParams = [
            "remarks"
        ];

        // invoke worker
        $this->initiateWorker(
            $listId,
            'AddListItems',
            'Addition of '.strtolower($listType).' items to current woking list for user: '.$userId,
            'LIST',
            'POST',
            $workerParams,
            $additionalBgJobParams,
            null,
            'importing save list items to working list'
        );
    }

    /**
     * Facilitate the rendering of the Reorder Selected Items modal
     * 
     * @param String $listType working list type
     * @param Integer $listId working list ID
     * 
     * @return modal content
     */
    public function actionReorderSelectedItems($listType,$listId){
        $dataProvider = new ArrayDataProvider([]);
        $options = [];
        $itemCount = 0;

        $postParams = $_POST;
        $selectedItemsIdsArr = $postParams['selectedIdsArr'] ?? [];

        // retrieve selected items info
        $selectedItemsIdsStr = 'equals '.implode('|equals ',$selectedItemsIdsArr);
        $params = [
            'listMemberDbId' => $selectedItemsIdsStr
        ];
        $selectedListItems = $this->workingListModel->getListItems($params,$listId);

        // retrieve working list member count
        $userId = $this->userModel->getUserId();

        $listInfo = $this->workingListModel->getWorkingList($userId,$listType);
        $listMemberCount = isset($listInfo['memberCount']) ? intval($listInfo['memberCount']) : 0;

        if(isset($selectedListItems) && !empty($selectedListItems)){
            $itemCount = count($selectedListItems);

            $rangeValues = range(1,($listMemberCount-count($selectedItemsIdsArr)+1));
            $options = array_combine($rangeValues, $rangeValues);

            $dataProvider = new ArrayDataProvider([
                'allModels' => $selectedListItems,
                'key' => 'listMemberDbId',
                'totalCount' => $itemCount
            ]);
        }

        // render modal content
        return $this->renderAjax(
            '@app/modules/workingList/views/modals/_reorder_list_items_content',
            [
                'options' => $options,
                'listType' => $listType,
                'dataProvider' => $dataProvider,
                'itemCount' => $itemCount,
                'listId' => $listId,
                'selectedItemsIdsArr' => $selectedItemsIdsArr,
                'browserId' => 'datagrid-working-list-main-browser'
            ],
        );
    }

    /**
     * Facilitate the update of order number of the working list items
     * 
     * @param Integer $listId working list ID
     * @param String $listType working list type
     */
    public function actionUpdateListOrder($listId,$listType){
        $postParams = $_POST;
        $reorderedItemsArr = $postParams['itemsArr'] ?? [];

        $userId = $this->userModel->getUserId();

        $listItemIdsArr = array_column($reorderedItemsArr,'listMemberId');
        $newOrderNumberArr = array_column($reorderedItemsArr,'targetOrderNumber');
        $newOrderArr = array_combine($newOrderNumberArr, $listItemIdsArr);

        $reorderedItemsInfoArr = [
            'newOrderArr' => $newOrderArr,
            'newOrderedIds' => $listItemIdsArr
        ];

        if(!empty($reorderedItemsArr)){
            // trigger background process worker
            $this->initiateWorker(
                $listId,
                'UpdateListItems',
                'Update '.strtolower($listType).' working list items for user: '.$userId,
                'LIST',
                'POST',
                [
                    "listId" => $listId,
                ],
                null,
                [
                    "processName" => 'reorder-all-items',
                    "reorderedListItems" => $reorderedItemsInfoArr
                ],
                'updating the list order'
            );
        }

        // inform user
        Yii::$app->session->setFlash(
            'success',
            '<i class="fa fa-check"></i>&nbsp;' .
                'The background process for updating the list order is in progress. You will be informed once the process is completed.'
        );

        return true;
    }

    /**
     * Facilitate appending of working list items to selected saved list
     * 
     * @param String $listType working list type
     * 
     * @return mixed
     */
    public function actionSaveToExistingList($listType){
        $postParams = $_POST;
        $selectedListId = $postParams['listId'] ?? null;
        $workingListId = $postParams['workingListId'] ?? null;

        // if has missing fields
        if(!isset($selectedListId) || empty($selectedListId) || !isset($workingListId) || empty($workingListId)){
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'Error encountered saving items to existing list! Missing required fields!'
            ]);
        }

        $listType = $listType ?? 'PACKAGE';

        // retrieve items from working list
        $params = [
            "fields" => 'listMember.id AS listMemberDbId|listMember.data_id AS '.strtolower($listType).'DbId'
        ];
        $workingListMemberIds = $this->workingListModel->getListItems($params,$workingListId);

        if(!isset($workingListMemberIds)){
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'Error encountered in retrieving working list items!'
            ]);
        }

        if(empty($workingListMemberIds)){
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'Error encountered in saving working list items! Working list is empty!'
            ]);
        }

        // check threshold values
        $itemCount = isset($workingListMemberIds) ? count($workingListMemberIds) : 0;
        $threshAttrib = 'saveList';
        $useBgProcess = $this->checkForBgProcess($itemCount,$threshAttrib);

        if($useBgProcess){
            $userId = $this->userModel->getUserId();

            // invoke worker
            $this->initiateWorker(
                $workingListId,
                'UpdateListItems',
                'Update '.strtolower($listType).' working list items for user: '.$userId,
                'LIST',
                'POST',
                [
                    "listId" => $workingListId,
                ],
                [
                    'remarks' => 'Saving working list items to saved list.'
                ],
                [
                    "processName" => 'update-parent-list',
                    "newParentListId" => $selectedListId
                ],
                'saving list items to existing list.'
            );

            return json_encode([
                'success' => true,
                'bgprocess' => true,
                'message' => 'Background process triggered to facilitate saving process.'
            ]);
        }

        // Default backend process
        // update parent list id of working list members to selected list
        $params = ["listDbId" => $selectedListId];

        $workingListMemberIds = array_column($workingListMemberIds,'listMemberDbId');
        $itemIdsStr = implode('|',$workingListMemberIds);

        $updateItems = $this->platformListMemberModel->bulkUpdate($itemIdsStr,$selectedListId,$params);

        if($updateItems['status'] != 200){
            return json_encode([
                'success' => false,
                'bgprocess' => false,
                'message' => 'Error encountered in saving working list items.'
            ]);
        }

        // TODO: add catch for if target list is too large

        // update order number of all items in the new list
        $updateOrderNumber = $this->workingListModel->reorderListItems($selectedListId,'creationTimestamp:ASC');

        Yii::$app->session->setFlash(
            'success', 
            '<i class="fa fa-check"></i>&nbsp;Successfully saved the working list items. '.
            'Navigate to the <strong>LIST MANAGER</strong> tool to view the updated list.'
        );

        return json_encode([
            'success' => true,
            'bgprocess' => false,
            'message' => 'Successfully saved working list items to existing list!'
        ]);
    }

    /**
     * Retrieve validated config variable records
     * 
     * @return Array $variableConfig validated variables
     */
    public function getValidatedConfigVariables(){
        // Retrieve program code given the dashboard program filter
        $defaultFilters = $this->dashboardModal->getFilters();
        $programId = $defaultFilters->program_id;
        $programInfo = $this->programModel->getProgram($programId);
        $programCode = isset($programInfo['programCode']) ? $programInfo['programCode'] : '';

        // retrieve export file columns
        $variableConfig = $this->workingListModel->getVariableConfiguration($programCode);
        $variableConfig = $this->workingListModel->validateVariables($variableConfig);

        return $variableConfig;
    }

    /**
     * Facilitates invoking of background process worker
     * 
     * @param Integer $listId list identifier
     * @param String $workerName background process worker name
     * @param String $taskMessage background process worker message
     * @param String $endpointEntity background process endpoint entity
     * @param String $method background process method
     * @param Array $workerParams background process worker parameters
     * @param Array $additionalBgJobParams background job parameters
     * @param Array $additionalWorkerData additional background process worker data
     * @param String $flashMessage session flash message
     * 
     */
    public function initiateWorker($listId,$workerName,$taskMessage,$endpointEntity,$method,$workerParams,$additionalBgJobParams,$additionalWorkerData,$flashMessage){
        // invoke worker
        $this->worker->invoke(
            $workerName,                                        // Worker name
            $taskMessage,                                       // Description
            'LIST',                                             // Entity
            $listId,                                            // Entity ID
            ''.$endpointEntity,                                 // Endpoint Entity
            'LISTS',                                            // Application
            ''.$method,                                         // Method
            $workerParams,                                      // Worker parameters
            $additionalBgJobParams,                             // Additional background job parameters
            $additionalWorkerData                               // Additional worker data
        );

        // inform user
        Yii::$app->session->setFlash(
            'success', 
            '<i class="fa fa-check"></i>&nbsp;'.
            'The background process for '.$flashMessage.' is in progress. You will be informed once the process is completed.'
        );

        return true;
    }

    /**
     * Saving large list items
     * 
     * @param Integer $listId list id
     * @param Integer $newListId new id to update list id
     * @param String $userId id of the user
     * @param String $listType type of the list
     * @param String $sortParams sort parameters
     */
    public function bgSaveList($listId, $newListId, $userId, $listType, $sortParams = ''){
        // invoke worker
        $this->initiateWorker(
            $listId,
            'UpdateListItems',
            'Update '.strtolower($listType).' working list items for user: '.$userId,
            'LIST',
            'POST',
            [
                "listId" => $listId,
            ],
            [
                'remarks' => 'Saving working list items in progress...'
            ],
            [
                "processName" => 'update-parent-list',
                "newParentListId" => $newListId,
                "sort" => $sortParams
            ],
            'saving list items.'
        );
    }

    /**
     * Save sorting of list items
     * 
     * @param String $sortParams sort parameters
     * @param Integer $listId list ID
     * 
     * @return update status
     */
    public function saveListSorting($sortParams,$listId){
        $updateOrder = 200;

        if(!empty($sortParams)){
            // save current sorting of items
            $processedSort = $this->workingListModel->processSortParams($sortParams);
            $updateOrder = $this->listMgtModel->updateListItemOrder($processedSort,$listId);
        }

        return $updateOrder;
    }

    /**
     * Retrive access permission of selected list
     * 
     * @return mixed
     */
    public function actionCheckListPermission(){
        $postParams = $_POST;
        $selectedListId = $postParams['listId'] ?? '';

        if(!isset($selectedListId) || empty($selectedListId)){
            return json_encode([
                'success' => false,
                'hasAccess' => false,
                'message' => 'No list ID provided!'
            ]);
        }

        // check user access to selected list
        $selectedListInfo = $this->platformListModel->searchList(["listDbId" => $selectedListId],false);

        if(empty($selectedListInfo) || !isset($selectedListInfo[0]['permission'])){
            return json_encode([
                'success' => false,
                'hasAccess' => false,
                'abbrev' => $selectedListInfo[0]['abbrev'],
                'message' => 'Error encountered retrieving list access information!'
            ]);
        }
        
        $hasPermission = $selectedListInfo[0]['permission'] != 'read_write' ? false : true;

        if($hasPermission == false){
            return json_encode([
                'success' => true,
                'hasAccess' => false,
                'abbrev' => $selectedListInfo[0]['abbrev'],
                'message' => 'User do not have permission to update selected list!'
            ]);
        }

        return json_encode([
            'success' => true,
            'hasAccess' => true,
            'abbrev' => $selectedListInfo[0]['abbrev'],
            'message' => 'User has permission to update selected list!'
        ]);
    }
}
