<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Contains methods for profile page
 */

namespace app\modules\account\controllers;
use Yii;
use app\components\B4RController;
use app\models\UserDashboardConfig;
use app\models\User;
use app\models\Team;

class ProfileController extends B4RController
{
    public function __construct($id, $module,
        public Team $team,
        public User $user,
        $config=[]) {

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders profile page
     */
    public function actionIndex(){

        $session = Yii::$app->session;
        $image = $session->get('user.picture');

        $userType = $this->user->isAdmin();
        $userId = $this->user->getUserId();
        $getUser = $this->user->getUser($userId);

        $teams = $this->user->getUserTeamsInfoCs(); // get teams info

        $teamIds = (!empty($teams)) ? array_column($teams, 'id') : null; //get team ids

        $teamsTags = $this->team->getAllTeamTags($teamIds);
        
        $rolesTags = $this->team->getAllRoleTags();

        return $this->render('index',compact('userType', 'userId', 'image', 'getUser', 'teams', 'teamsTags', 'rolesTags'));
    }

    /**
     * Adds user to team
     */
    public function actionAddusertoteam(){
        $userId = $_POST['user_id'];
        $teamId = $_POST['team_id'];
        $roleId = $_POST['role_id'];

        // Add user to team
        $this->user->addUserToTeam($userId,$teamId,$roleId);

        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> '.\Yii::t('app', 'Successfully added to the team.').'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');
    }
}