<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\account\controllers;

use Yii;
use yii\helpers\Url;

use app\components\B4RController;
use app\dataproviders\ArrayDataProvider;
use app\controllers\Dashboard;
use app\models\PlatformList;
use app\models\PlatformListAccess;
use app\models\PlatformListMember;
use app\models\GermplasmList;
use app\models\Germplasm;
use app\models\Traits;
use app\models\Lists;
use app\models\ListsOperations;
use app\models\User;
use app\models\Variable;
use app\models\Worker;
use app\models\Program;
use app\modules\account\models\ListModel;
use app\modules\account\models\ListMemberModel;
use app\modules\account\models\ListAccessSearch;
use app\modules\account\models\ListLocationSearch;
use app\modules\account\models\SharedList;
use app\modules\dataCollection\models\ExportWorkbook;
use yii\helpers\Html;

/**
 * Controller for list management tool
 */

class ListController extends B4RController
{

    protected $lists;
    protected $listOperations;
    protected $listModel;
    protected $listMemberModel;
    protected $platformListMember;
    protected $user;
    protected $platformListAccess;
    protected $listAccessSearch;
    protected $germplasmList;
    protected $germplasm;
    protected $traits;
    protected $sharedList;
    protected $worker;
    public $dashboardModel;
    public $programModel;

    const LM_ACTIONS = [
        "VIEW_LISTS",
        "LIST_MANAGER_VIEW",
        "LIST_MANAGER_MERGE",
        "LIST_MANAGER_UPDATE",
        "LIST_MANAGER_ADD_ITEMS",
        "LIST_MANAGER_SPLIT",
        "LIST_MANAGER_CREATE",
        "LIST_MANAGER_DELETE",
        "LIST_MANAGER_SHARE",
        "LIST_MANAGER_DOWNLOAD"
    ];

    public function __construct(
        $id,
        $module,
        Lists $lists,
        ListModel $listModel,
        ListMemberModel $listMemberModel,
        PlatformListMember $platformListMember,
        User $user,
        PlatformListAccess $platformListAccess,
        ListAccessSearch $listAccessSearch,
        GermplasmList $germplasmList,
        Germplasm $germplasm,
        Traits $traits,
        SharedList $sharedList,
        ListsOperations $listOperations,
        Worker $worker,
        Dashboard $dashboardModel,
        Program $programModel,
        $config = []
    ) {

        $this->listModel = $listModel;
        $this->lists = $lists;
        $this->listMemberModel = $listMemberModel;
        $this->platformListMember = $platformListMember;
        $this->user = $user;
        $this->platformListAccess = $platformListAccess;
        $this->listAccessSearch = $listAccessSearch;
        $this->germplasmList = $germplasmList;
        $this->germplasm = $germplasm;
        $this->traits = $traits;
        $this->sharedList = $sharedList;
        $this->listOperations = $listOperations;
        $this->worker = $worker;
        $this->dashboardModel = $dashboardModel;
        $this->programModel = $programModel;

        parent::__construct($id, $module, $config);
    }

    /**
     * Action for data browser for lists
     * @param String $program currently selected program of the user
     * @return view file to list browser
     **/

    public function actionIndex($program)
    {

        $sharedListModel = $this->sharedList;
        $userId = $this->user->getUserId();

        // Check if user has access to List Manager
        $isAdmin = Yii::$app->session->get('isAdmin');

        $hasAccess = Yii::$app->access->renderAccess('VIEW_LISTS', 'LISTS');

        $defPermission =  ["VIEW_LISTS"];
        $currPermission = Yii::$app->session->get('currentPermissions')['permission'] ?? $defPermission;
        $permissions = $isAdmin ? self::LM_ACTIONS : $currPermission;

        $columnVariables = [];
        $programCode = null;

        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programId = $dashboardFilters['program_id'] ?? '';

        if (!empty($programId)) {
            $programInfo = $this->programModel->getProgram($programId);
            $programCode = $programInfo['programCode'];
        }

        $columnVariables = $this->listMemberModel->retrieveVariablesFromConfig($programCode);

        Yii::$app->session->set("list_members-column-variables", $columnVariables);

        // Redirect to proper page depending on access permissions
        if (!$isAdmin) {
            if (!$hasAccess) {
                Yii::$app->session->setFlash(
                    'error',
                    "You have no permission to the page you are trying to access. Please contact your data administrator."
                );

                Yii::$app->response->redirect(Url::base() . '/index.php');
                return;
            }
        }

        return $this->render('index', [
            "program" => $program,
            "userId" => $userId,
            "sharedListModel" => $sharedListModel
        ]);
    }

    /**
     * Create list record with basic information
     * @param Integer $id of the list, null if list is new
     * @param String $program of the current user
     * @return view file to create page	
     **/

    public function actionCreate(int $id = null, $program)
    {

        $name = '';
        $abbrev = '';
        $displayName = '';
        $description = '';
        $type = '';
        $subType = '';
        $remarks = '';



        if (!empty($id)) {
            $response = $this->lists->getOne($id);

            if (isset($response) && !empty($response) && isset($response['data'])) {
                $model = $response['data'];

                if (!empty($model['listDbId'])) {
                    $name = $model['name'];
                    $abbrev = $model['abbrev'];
                    $displayName = $model['displayName'];
                    $description = $model['description'];
                    $type = $model['type'];
                    $subType = $model['subType'] == null || $model['subType'] == '' ? '' : $model['subType'];
                    $remarks = $model['remarks'];
                } else {
                    Yii::$app->session->setFlash('error', 'List does not exist.');
                    return $this->redirect(['list/index', 'program' => $program]);
                }
            }
        }

        if (isset(Yii::$app->request->post()['Basic'])) {

            $returnArr = $this->listModel->validateCreateForm($id);
            if ($returnArr['success']) {
                Yii::$app->session->setFlash('success', $returnArr['message']);
            }

            return json_encode($returnArr);
        } else {
            return $this->render('create', [
                'program' => $program,
                'name' => $name,
                'abbrev' => $abbrev,
                'display_name' => $displayName,
                'type' => $type,
                'subtype' => $subType,
                'description' => $description,
                'remarks' => $remarks,
                'id' => $id,
                'showTabs' => true,
                'create' => false,
            ]);
        }
    }

    /**
     * Renders view options for adding members to list
     * @param Integer $id of the list, null if list is new
     * @param String $program of the current user
     * @return view file to add items page
     **/

    public function actionAddItems($program, $id = null)
    {
        $model = $this->checkListId($id, $program);
        if (isset($model->type) && !empty($model->type) && !in_array($model->type, ['germplasm', 'trait'])) {

            Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-times"></i> Unsupported list type for add items feature');
            return $this->redirect(['list/index', 'program' => $program])->send();
        }

        return $this->render('items', [
            'id' => $id,
            'model' => $model,
            'program' => $program
        ]);
    }

    /**
     * Renders view for preview of list items
     * @param Integer $id of the list, null if list is new
     * @param String $program of the current user
     * @return view file to preview page
     **/

    public function actionPreview($program, $id)
    {
        $model = $this->checkListId($id, $program);
        $searchModel = $this->listMemberModel;

        $sessColumnVariables = Yii::$app->session->get("list_members-column-variables");

        $columnVariables = [];
        if (!isset($sessColumnVariables) || empty($sessColumnVariables)) {
            $columnVariables = $this->listMemberModel->retrieveVariablesFromConfig($program);
        } else {
            $columnVariables = $sessColumnVariables;
        }

        return $this->render('preview', [
            'id' => $id,
            'model' => $model,
            'program' => $program,
            'searchModel' => $searchModel,
            'columnVariables' => $columnVariables
        ]);
    }

    /**
     * Renders view to review/edit of list items
     * @param Integer $id of the list, null if list is new
     * @param String $program of the current user
     * @return view file to manage items page
     **/

    public function actionReview($program, $id)
    {

        $model = $this->checkListId($id, $program);

        $searchModel = \Yii::$container->get('app\modules\account\models\ListMemberModel');

        // set searchModel for final items grid browser
        $finalGridSearchModel = $searchModel;
        $finalGridSearchModelName = 'FinalGridBrowserList';
        $finalGridSearchModel->setFormName($finalGridSearchModelName);

        // set searchModel for removed items grid browser
        $itemsGridSearchModel = \Yii::$container->get('app\modules\account\models\RemovedListMemberModel');
        $itemGridSearchModelName = 'RemovedItemsGridList';
        $itemsGridSearchModel->setFormName($itemGridSearchModelName);

        $sessColumnVariables = Yii::$app->session->get("list_members-column-variables");

        $columnVariables = [];
        if (!isset($sessColumnVariables) || empty($sessColumnVariables)) {

            $columnVariables = $this->listMemberModel->retrieveVariablesFromConfig($program);
        } else {
            $columnVariables = $sessColumnVariables;
        }

        return $this->render('review', [
            'id' => $id,
            'model' => $model,
            'program' => $program,
            'searchModel' => $searchModel,
            'finalGridSearchModel' => $finalGridSearchModel,
            'itemsGridSearchModel' => $itemsGridSearchModel,
            'columnVariables' => $columnVariables
        ]);
    }

    /**
     * Action for rendering replicate list items modal
     *
     * @param integer $listDbId ID of the list
     * @param string $listAbbrev abbreviation of list
     * @param string $listType type of list
     * @param string $idString IDs of selected list items
     * @param integer $totalCount total list member count
     *
     * @return html HTML content for the Replicate List Items modal
     **/
    public function actionRenderReplicateListItems($listDbId, $listAbbrev, $listType, $idString, $totalCount)
    {
        $listDbId = $_POST['listDbId'] ?? $listDbId ?? null;
        $listAbbrev = $_POST['listAbbrev'] ?? $listAbbrev ?? '';
        $listType = $_POST['listType'] ?? $listType ?? '';
        $idString = $_POST['idString'] ?? $idString ?? '';
        $totalCount = $_POST['totalCount'] ?? $totalCount ?? '';

        $additionalInfoColumn = ['enableSorting' => false,];
        $additionalField = '';
        $dataProvider = [];
        $isSelectAllChecked = (empty($idString)) ? true : false;

        Yii::$app->session->set('isSelectAllChecked', $isSelectAllChecked);

        Yii::info('Rendering replicate list items modal' . json_encode(compact(
            'listDbId',
            'listAbbrev',
            'listType',
            'idString',
            'totalCount',
            'isSelectAllChecked'
        )), __METHOD__);


        if (str_contains($listType, 'trait') || str_contains($listType, 'management') || $listType === 'variable') {
            $additionalInfoColumn['attribute'] = 'abbrev';
            $additionalField = 'variable.abbrev |';
        } elseif ($listType === 'designation' || $listType === 'germplasm') {
            $additionalInfoColumn['attribute'] = 'germplasmCode';
            $additionalField = 'germplasm.germplasm_code AS germplasmCode |';
        } elseif ($listType === 'seed') {
            $additionalInfoColumn['attribute'] = 'seedCode';
            $additionalField = 'seed.seed_code AS seedCode |';
        } elseif ($listType === 'plot') {
            $additionalInfoColumn['attribute'] = 'plotCode';
            $additionalField = 'plot.plot_code AS plotCode |';
        } elseif ($listType === 'location') {
            $additionalInfoColumn['attribute'] = 'locationCode';
            $additionalField = 'location.location_code AS locationCode |';
        } elseif ($listType === 'package') {
            $additionalInfoColumn['attribute'] = 'packageCode';
            $additionalField = 'package.package_code AS packageCode |';
        } elseif ($listType === 'sites') {
            $additionalInfoColumn['attribute'] = 'geospatialObjectCode';
            $additionalInfoColumn['label'] = 'siteCode';
            $additionalField = '"geospatialObject".geospatial_object_code AS geospatialObjectCode |';
        } elseif ($listType === 'experiment') {
            $additionalInfoColumn['attribute'] = 'experimentCode';
            $additionalField = 'experiment.experiment_code AS experimentCode |';
        } else {
            $additionalInfoColumn = [];
        }

        Yii::$app->session->set('additionalField', $additionalField);

        if (!$isSelectAllChecked) {
            Yii::$app->session->set('additionalInfoColumn', $additionalInfoColumn);

            $response = $this->lists->searchAllMembers(
                $listDbId,
                [
                    'fields' => "$additionalField listMember.id AS listMemberDbId | listMember.data_id AS dataDbId | listMember.display_value AS displayValue | listMember.order_number AS orderNumber",
                    'listMemberDbId' => "$idString",
                ]
            );

            $dataProvider = new ArrayDataProvider([
                'allModels' => $response['data'],
                'key' => 'listMemberDbId',
                'restified' => true,
                'totalCount' => $response['totalCount'],
                'sort' => [
                    'defaultOrder' => [
                        'orderNumber' => SORT_ASC,
                    ],
                    'attributes' => [
                        'listMemberDbId',
                        'orderNumber',
                        'displayValue',
                        'abbrev',
                        'germplasmCode',
                        'seedCode',
                        'plotCode',
                        'locationCode',
                        'packageCode',
                        'geospatialObjectCode',
                        'experimentCode',
                        'numberOfCopies',
                    ],
                ],
            ]);

            Yii::$app->session->set('allModels', $response['data']);
        }

        $maxRepValue = Yii::$app->config->getAppThreshold('LISTS', 'maxReplicate') ?? 21;

        return $this->renderAjax(
            '_replicate_items_browser',
            compact(
                'dataProvider',
                'additionalInfoColumn',
                'listAbbrev',
                'isSelectAllChecked',
                'totalCount',
                'maxRepValue'
            )
        );
    }


    /**
     * Action for rendering the summary/confirmation step
     * of the replicate list items modal
     *
     * @param string $listAbbrev abbreviation of list
     * @param integer $totalCount total list member count
     * @param object $numberOfCopiesValueMap map containing number of copies per selected list item
     * @param boolean|string $isAppendChecked flag to determine whether or not to append replication count to new display names
     *
     * @return html HTML content for the Replicate List Items modal
     **/
    public function actionRenderConfirmReplicateListItems($listAbbrev, $totalCount, $numberOfCopiesValueMap = [], $isAppendChecked = '')
    {
        $listAbbrev = $_POST['listAbbrev'] ?? $listAbbrev ?? '';
        $totalCount = $_POST['totalCount'] ?? $totalCount ?? '';
        $numberOfCopies = $_POST['numberOfCopies'] ?? null;
        $numberOfCopiesValueMap = $_POST['numberOfCopiesValueMap'] ?? null;
        $isAppendChecked = $_POST['isAppendChecked'] ?? '';

        Yii::$app->session->set('numberOfCopies', $numberOfCopies);
        $isSelectAllChecked = Yii::$app->session->get('isSelectAllChecked');
        $additionalInfoColumn = [];
        $dataProvider = [];

        Yii::info('Rendering confirm replicate list items modal content' . json_encode(compact(
            'listAbbrev',
            'totalCount',
            'numberOfCopies',
            'numberOfCopiesValueMap',
            'isAppendChecked',
            'isSelectAllChecked'
        )), __METHOD__);

        if (!$isSelectAllChecked) {
            $additionalInfoColumn = Yii::$app->session->get('additionalInfoColumn');
            $allModels = Yii::$app->session->get('allModels');
            $confirmModels = [];

            foreach ($allModels as $key => $value) {
                foreach ($numberOfCopiesValueMap as $listMemberDbId => $numberOfCopies) {
                    if ((int) $numberOfCopies === 0) {
                        continue;
                    }

                    if ("{$value['listMemberDbId']}" === "$listMemberDbId") {
                        $value['numberOfCopies'] = $numberOfCopies;
                        $confirmModels[] = $value;
                    }
                }
            }

            Yii::$app->session->set('confirmModels', $confirmModels);

            $dataProvider = new ArrayDataProvider([
                'allModels' => $confirmModels,
                'key' => 'orderNumber',
                'restified' => true,
                'totalCount' => count($confirmModels),
                'sort' => [
                    'defaultOrder' => [
                        'orderNumber' => SORT_ASC,
                    ],
                    'attributes' => [
                        'listMemberDbId',
                        'orderNumber',
                        'displayValue',
                        'abbrev',
                        'germplasmCode',
                        'seedCode',
                        'plotCode',
                        'locationCode',
                        'packageCode',
                        'geospatialObjectCode',
                        'experimentCode',
                        'numberOfCopies',
                    ],
                ],
            ]);
        }

        return $this->renderAjax(
            '_replicate_items_confirm',
            compact(
                'dataProvider',
                'additionalInfoColumn',
                'isAppendChecked',
                'listAbbrev',
                'totalCount',
                'isSelectAllChecked',
                'numberOfCopies'
            )
        );
    }

    /**
     * Action for processing replication of selected list items
     *
     * @param string|int $listDbId ID of the list
     * @param string $listAbbrev abbreviation of the list
     * @param string $namePatternOption name pattern type; either default or displayName + "-[replicationCount]"
     * @param string $addMemberOption option for adding new list members; if "default", add new list members to the bottom of the list
     *
     **/
    public function actionConfirmReplicateListItems($listDbId, $listAbbrev, $namePatternOption, $addMemberOption)
    {
        $isSelectAllChecked = Yii::$app->session->get('isSelectAllChecked');
        $numberOfCopies = Yii::$app->session->get('numberOfCopies');
        $additionalField = Yii::$app->session->get('additionalField');

        Yii::info('Replicating selected list items ' . json_encode(compact(
            'listDbId',
            'listAbbrev',
            'numberOfCopies',
            'namePatternOption',
            'addMemberOption',
            'isSelectAllChecked'
        )), __METHOD__);

        $confirmedItems = ($isSelectAllChecked) ? null : Yii::$app->session->get('confirmModels');

        // Trigger background worker
        $this->worker->invoke(
            'AddListItems',                     // Worker name
            'Replicating selected list items',  // Description
            'LIST_MEMBER',                      // Entity
            $listDbId,                          // Entity ID
            'LIST_MEMBER',                      // Endpoint Entity
            'LISTS',                            // Application
            'POST',                             // Method
            [                                   // Worker data
                'processName' => 'copy-list-members',
                'data' => $confirmedItems,
                'isSelectAllChecked' => $isSelectAllChecked,
                'numberOfCopies' => $numberOfCopies,
                'additionalField' => $additionalField,
                'namePatternOption' => $namePatternOption,
                'addMemberOption' => $addMemberOption,
                'listDbId' => $listDbId,
            ]
        );

        $flashMessage = Yii::t('app', "The replicates for <b>$listAbbrev</b> are being created and added to the list in the background. You may proceed with other tasks. You will be notified in this page once done.");

        // Populate Flash message
        Yii::$app->session->setFlash('info', '<i class="fa fa-fw fa-info-circle  font-md"></i> ' . $flashMessage);

        // Redirect back to the LM main data browser
        $this->redirect([
            '/account/list',
            'program' => Yii::$app->session->get('program') ?? '',
            'MyList[abbrev]' => $listAbbrev,
        ]);
    }

    /**
     * Action for adding items to plot list
     * @param Integer $id of the list
     * @return json specifies if insert it successful
     **/

    public function actionAddPlotList($id)
    {

        $selectedData = isset($_POST['selectedData']) ? $_POST['selectedData'] : [];
        $selectedStudies = $_POST['selectedStudies'];
        $selectedStudies = json_decode($selectedStudies, true);

        $totalCount = ListModel::addToPlotList($id, $selectedStudies, $selectedData);
        return json_encode(["success" => true, 'totalCount' => $totalCount]);
    }

    /**
     * Action for adding records to list_members given list ID
     * @param Integer $id of the list, null if list is new
     * @param String $program of the current user
     * @param String $listType can be designation, location, seed, study, or plot
     * @return json specifies if action is successful
     **/

    public function actionAddMembersByInput($program, $id, $listType)
    {

        $message = $this->listModel->addMembersByInput($id, $listType);
        return json_encode(['message' => $message]);
    }

    /**
     * Validate list from input list
     * @param Integer $id of the list, null if list is new 
     * @param String $program of the current user; 
     * @param String $listType - can be designation, location, seed, study, or plot
     * @return json different return options for different types
     **/

    public function actionValidateList($program, $id, $listType)
    {
        $message = 'Missing parameter';

        if (isset($_POST['inputList'])) {

            $returnArr = $this->listModel->validateInputList($_POST['inputList'], $id, $listType, $program);
            if (!$returnArr['success']) {
                if ($returnArr['message']) {
                    return json_encode([
                        'valid' => false,
                        'message' => $returnArr['message']
                    ]);
                } else {
                    return json_encode([
                        'valid' => false,
                        'message' => $message
                    ]);
                }
            } else {
                $returnArr = $returnArr['returnArr'];
                $htmlData = $this->renderPartial('input_types/input_designation', $returnArr);

                return json_encode([
                    'htmlData' => $htmlData,
                    'validCount' => $returnArr['validCount']
                ]);
            }
        } else {

            return json_encode([
                'valid' => false,
                'message' => $message
            ]);
        }
    }

    /**
     * Checks if the $id parameter is empty
     * Checks the given ID of the list if it exists in the database.
     * @param Integer $id of the list, null if list is new; 
     * @param String $program of the current user; 
     * @param String $listType - can be designation, location, seed, study, or plot
     * @return PlatformList model record for specified ID
     **/

    public function checkListId($id, $program)
    {

        if (empty($id)) {
            Yii::$app->session->setFlash('error', 'Missing required parameter <b>id</b>');
            return $this->redirect(['list/index', 'program' => $program]);
        }

        $model = $this->lists->getOne($id);

        if (isset($model) && !empty($model)) {
            if (isset($model['totalCount']) && $model['status'] == 200 && $model['totalCount'] < 1) {
                Yii::$app->session->setFlash('error', 'List does not exist.');
                return $this->redirect(['list/index', 'program' => $program])->send();
            }

            $model['data']['success'] = true;
            $model['data']['message'] = $model['message'];
            return  json_decode(json_encode($model['data']));
        } else {
            return [];
        }
    }

    /** 
     * Renders upload CSV widget 
     * @return $json if action is successful
     **/

    public function actionGetUpload()
    {

        try {
            if (isset($_FILES['list_file'])) {

                $importer = new \app\modules\account\models\FileParser();
                return $importer::reader('list_file');
            } else {
                return json_encode(["success" => false, "error" => "No file uploaded."]);
            }
        } catch (\Exception $e) {

            return json_encode(["success" => false, "error" => "Error in uploading CSV File."]);
        }
    }

    /**
     * Deletes list items given IDS
     * @return json response
     */
    public function actionRemoveItem()
    {

        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $listId = isset($_POST['listId']) ? $_POST['listId'] : '';
        if (trim($id) === '') {
            $id = 0;
        }

        $this->platformListMember->bulkDelete($id);

        // add lines to update modifier of list
        $processRemark = 'Delete list member';
        $this->listModel->updateListInfo($listId, $processRemark);

        return json_encode(["success" => true, "message" => "Succesfully removed items!"]);
    }

    /**
     * Delete the given list
     * @return json if action is successful
     **/

    public function actionDelete()
    {

        $id = isset($_POST['id']) ? $_POST['id'] : 0;

        $returnArr = $this->lists->deleteOne($id);
        if (isset($returnArr) && !empty($returnArr) && isset($returnArr['status'])) {
            if ($returnArr['status'] == 200) {
                $returnArr['success'] = true;
            } else {
                $newMessage = explode('|', $returnArr['message']);
                $newMessage = explode(':', $newMessage[0])[1];

                $returnArr['message'] = strpos($newMessage, strval('The client is trying to retrieve a list that the client is not a creator of.')) ? 'Cannot delete record! This list has been shared with at least 1 other user.' : strval($newMessage);
                $returnArr['success'] = false;
            }
        } else {
            $returnArr['success'] = false;
        }

        return json_encode($returnArr);
    }

    /**
     * Renders view for preview of list items
     * @param Integer $id of the list; 
     * @param String $program of the current user
     * @return view
     **/

    public function actionView($program, $id)
    {

        $model = $this->checkListId($id, $program);

        $sessColumnVariables = Yii::$app->session->get("list_members-column-variables");

        $columnVariables = [];
        if (!isset($sessColumnVariables) || empty($sessColumnVariables)) {
            $columnVariables = $this->listMemberModel->retrieveVariablesFromConfig($program);
        } else {
            $columnVariables = $sessColumnVariables;
        }

        $searchModel = $this->listMemberModel;
        return $this->renderAjax('view', [
            'id' => $id,
            'model' => $model,
            'program' => $program,
            'searchModel' => $searchModel,
            'columnVariables' => $columnVariables
        ]);
    }

    /**
     * Function for removing duplicate items in a list
     * @return json if action is successful
     **/

    public function actionRemoveDuplicates()
    {

        $listId = isset($_POST['listId']) ? $_POST['listId'] : 0;

        // get duplicates
        $duplicateMembers = $this->platformListMember->searchAllMembers($listId, 'duplicatesOnly=true');
        if ($duplicateMembers['totalCount'] > 0) {
            // parse list members Id
            $listMemberIds = '';
            foreach ($duplicateMembers['data'] as $member) {
                if (!empty($member['listMemberDbId'])) {
                    $listMemberIds .= ($listMemberIds != '') ? '|' : '';
                    $listMemberIds .= $member['listMemberDbId'];
                }
            }
            // bulk update and set duplicates as inactive members
            $requestData = [
                "isActive" => "false"
            ];

            $this->platformListMember->bulkUpdate($listMemberIds, $listId, $requestData);

            // add lines to update modifier of list
            $processRemark = 'Removed duplicates from list';
            $this->listModel->updateListInfo($listId, $processRemark);

            return json_encode(
                array(
                    "success" => true,
                    "duplicateCount" => $duplicateMembers['totalCount']
                )
            );
        } else {
            return json_encode(['success' => true, 'duplicateCount' => 0]);
        }
    }

    /**
     * Updates the items in the list when:
     * 1. Items are removed
     * 2. Items are added
     * @return if action is successful
     **/

    public function actionUpdateListMember()
    {

        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $isVoid = isset($_POST['is_void']) ? $_POST['is_void'] : 'false';
        if ($id === '') {
            $id = '0';
        }

        $listId = isset($_POST['list_id']) ? $_POST['list_id'] : 0;
        $isActive = ($isVoid === 'true') ? 'false' : 'true';
        $requestData = [
            "isActive" => $isActive
        ];

        $this->platformListMember->bulkUpdate($id, $listId, $requestData);

        // add lines to update modifier of list
        $processRemark = 'Removed update list member';
        $this->listModel->updateListInfo($listId, $processRemark);

        return json_encode(array("success" => true));
    }

    /**
     * Function for reordering list members
     * @return if action is successful
     **/

    public function actionReorderList()
    {

        $listMemberDbId = isset($_POST['dataId']) ? $_POST['dataId'] : 0;
        $newRowNumber = isset($_POST['newRowNumber']) ? $_POST['newRowNumber'] : 1;
        $listId = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $requestData = [
            "orderNumber" => $newRowNumber
        ];
        $this->platformListMember->updateOne($listMemberDbId, $requestData);

        // add lines to update modifier of list
        $processRemark = 'Reorder list members';
        $this->listModel->updateListInfo($listId, $processRemark);

        return json_encode(array("success" => true));
    }

    /**
     * Renders share list feature into a modal
     * @return view file
     **/

    public function actionRenderShareList()
    {

        $listId = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $userId = $this->user->getUserId();
        $programData = $this->listModel->getPrograms();
        $userData = $this->listModel->getUsers($listId, $userId, null, 'tags');
        $searchModel = $this->listAccessSearch;

        return $this->renderAjax('share_list', [
            'listId' => $listId,
            'programData' => $programData,
            'userData' => $userData,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Get users for selected team
     * @return json containing the users
     **/

    public function actionGetUsersByTeam()
    {

        $teamId = isset($_POST['teamId']) ? $_POST['teamId'] : 0;
        $listId = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $userId = $this->user->getUserId();

        $users = $this->listModel->getUsers($listId, $userId, $teamId, 'id');

        return json_encode(['users' => $users]);
    }


    /**
     * Add access to selected users to given list
     * @return json if action is successful
     **/

    public function actionAddAccess()
    {

        $listId = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $selectedUsers = isset($_POST['selectedUsers']) ? $_POST['selectedUsers'] : [];
        $selectedTeams = isset($_POST['selectedTeams']) ? $_POST['selectedTeams'] : [];

        $response = $this->platformListAccess->addAccess($listId, $selectedUsers, $selectedTeams);
        if (!isset($response) || empty($response)) {
            $response = [];
        }

        // add lines to update modifier of list
        $processRemark = 'Add list access';
        $this->listModel->updateListInfo($listId, $processRemark);

        return json_encode($response);
    }


    /**
     * Revoke access to selected users to given list
     * @return json the number of remaining access to list
     **/

    public function actionRevokeAccess()
    {
        $listId = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $selectedUsers = isset($_POST['userEntityIds']) ? $_POST['userEntityIds'] : [];
        $selectedTeams = isset($_POST['programEntityIds']) ? $_POST['programEntityIds'] : [];

        // get existing access data
        $count = 0;
        $data = $this->lists->getOne($listId);
        if (isset($data) && !empty($data) && isset($data['data'])) {
            $listData = $data['data'];
            $accessData = $listData['accessData'];
            $count = $this->platformListAccess->revokeAccess($listId, $accessData, $selectedUsers, $selectedTeams);

            // add lines to update modifier of list
            $processRemark = 'Revoke list access';
            $this->listModel->updateListInfo($listId, $processRemark);
        }

        return json_encode(['remaining' => $count]);
    }

    /**
     * Update access of selected user for given list
     * @return json if action is successful
     **/

    public function actionUpdateAccess()
    {

        $listId = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $userEntityIds = isset($_POST['userEntityIds']) ? $_POST['userEntityIds'] : [];
        $programEntityIds = isset($_POST['programEntityIds']) ? $_POST['programEntityIds'] : [];
        $newPermission = $_POST['newPermission'];

        $this->platformListAccess->addAccess($listId, $userEntityIds, $programEntityIds, $newPermission);

        // add lines to update modifier of list
        $processRemark = 'Updated list access';
        $this->listModel->updateListInfo($listId, $processRemark);

        return json_encode(['success' => true]);
    }


    /**
     * Refresh shared list access when adding new users
     * @return view
     **/

    public function actionRefreshShareListGrid()
    {

        $listId = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $searchModel = $this->listAccessSearch;
        $dataProvider = $searchModel->search(null, $listId);

        return $this->renderAjax('share_list_grid', [
            'listId' => $listId,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Include or exclude id of selected result as a possible list member
     * @return Integer count of selected_ids array
     **/

    public function actionSelectRow()
    {
        if (isset($_POST['id'], $_POST['type'])) {
            $id = $_POST['id'];
            $type = $_POST['type'];

            $currentSelectedIds = Yii::$app->session[$type . '-selected_ids'];

            if (in_array($id, $currentSelectedIds)) {  // remove from selected_ids
                $currentSelectedIds = array_diff($currentSelectedIds, [$id]);
            } else {    // add to selected_ids
                $currentSelectedIds[] = $id;
            }

            Yii::$app->session->set($type . '-selected_ids', $currentSelectedIds);
            return count($currentSelectedIds);
        }
    }

    /**
     * Include or exclude id of selected result as a possible list member
     **/

    public function actionToggleSelectAll()
    {
        if (isset($_POST['type'])) {
            $type = $_POST['type'];
            $mode = $_POST['mode'];

            Yii::$app->session->set($type . '-select_all_flag', $mode);
            Yii::$app->session->set($type . '-selected_ids', []);

            return true;
        }
    }

    /**
     * Preview members when saving new list
     */
    public function actionRetrieveListMemberPreview()
    {
        $data = new ArrayDataProvder([
            'allModels' => []
        ]);

        $htmlData = $this->renderPartial('save_list_preview', ['dataProvider' => $data['dataProvider']]);

        return json_encode([
            'htmlData' => $htmlData,
            'count' => $data['count']
        ]);
    }

    /**
     * Get selected items from session
     * @return mixed object contains the session values for browser selection
     */
    public function actionGetSelectedFromSession()
    {
        $type = isset($_POST['type']) ? $_POST['type'] : 'germplasm';   // germplasm or variable

        $result = [
            'currentSelectedIds' => Yii::$app->session[$type . '-selected_ids'],
            'selectMode' => Yii::$app->session[$type . '-select_all_flag'],
            'filter' => Yii::$app->session[$type . '-api_filter'],
            'totalResultsCount' => Yii::$app->session[$type . '-total_results_count']
        ];

        return json_encode(json_encode($result));
    }

    /**
     * Create list record with basic information
     * @return mixed object contains the boolean result and string message
     **/
    public function actionSaveList()
    {
        $abbrev = isset($_POST['abbrev']) ? trim($_POST['abbrev']) : '';
        $name = isset($_POST['name']) ? trim($_POST['name']) : '';
        $displayName = isset($_POST['displayName']) ? $_POST['displayName'] : '';
        $type = isset($_POST['type']) ? $_POST['type'] : 'germplasm';   // germplasm or variable
        $description = isset($_POST['description']) ? $_POST['description'] : '';
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : '';
        $selectedFromSession = $_POST['selectedFromSession'];   // required
        $selectedFromSession = json_decode($selectedFromSession, true);

        $currentSelectedIds = $selectedFromSession['currentSelectedIds'];
        $selectMode = $selectedFromSession['selectMode'];
        $filter = $selectedFromSession['filter'];
        $totalResultsCount = $selectedFromSession['totalResultsCount'];
        $notIncluded = null;

        // Check if list name or abbrev already exists
        $abbrevOrNameAlreadyExists = false;
        $response = $this->lists->searchAll(
            ['abbrev' => 'equals ' . $abbrev]
        );
        if (isset($response['data'][0])) {
            $abbrevOrNameAlreadyExists = true;
        }
        $response = $this->lists->searchAll(
            ['name' => 'equals ' . $name]
        );
        if (isset($response['data'][0])) {
            $abbrevOrNameAlreadyExists = true;
        }
        if ($abbrevOrNameAlreadyExists) {
            $response = ['result' => false, 'message' => 'Name or Abbrev already exists. Please provide a new one.'];
            return json_encode($response);
        }

        $listValues = [
            'abbrev' => $abbrev,
            'name' => $name,
            'displayName' => $displayName,
            'type' => $type,
            'description' => $description,
            'remarks' => $remarks
        ];
        $listId = null;

        if ($type == 'germplasm') {
            $model = $this->germplasmList;

            // Save germplasm list
            $isGermplasmListSaved = $model->saveGermplasmList($listValues);
            if (!$isGermplasmListSaved['result']) {  // if errors were encountered during saving of germplasm list
                return json_encode($isGermplasmListSaved);
            }

            $listId = $isGermplasmListSaved['listId'];
        } else {    // trait or other types
            $request = [
                "records" => [
                    0 => $listValues
                ]
            ];

            // Save list
            $saveListResponse = $this->lists->create($request);
            if (isset($saveListResponse) && !empty($saveListResponse)) {
                if (!$saveListResponse['success']) { // if saving list was unsuccessful
                    return json_encode($saveListResponse);
                }

                $listId = $saveListResponse['data'][0]['listDbId'];
            } else {
                return json_encode(['success' => false]);
            }
        }

        $isBgProcess = false;
        $message = 'List:&nbsp;<b>' . $name . '</b>&nbsp;successfully created.';

        // Save list members
        $totalNumberOfIds = 0;
        $ids = [];
        $idColumn = '';

        if ($selectMode == 'include mode') { // include mode, get total number from count($type.'-selected_ids')
            $totalNumberOfIds = count($currentSelectedIds);
            $ids = $currentSelectedIds;

            // Build the filters (only used at bg process)
            if (!empty($currentSelectedIds)) $included = 'equals ' . implode(' | equals ', $currentSelectedIds);
            if ($type == 'germplasm') {
                $filter = ['germplasmDbId' => $included];
            } else if ($type == 'trait') {
                $filter = ['variableDbId' => $included];
            }
        } else {    // exclude mode, get total number from $type.'-total_results_count' (session) minus count($type.'-selected_ids')
            $totalNumberOfIds = $totalResultsCount - count($currentSelectedIds);
            // Build the filters
            if (!empty($currentSelectedIds)) $notIncluded = 'not equals ' . implode(' | not equals ', $currentSelectedIds);
            if ($type == 'germplasm') {
                $model = $this->germplasm;
                $idColumn = 'germplasm';
                if (!is_null($notIncluded)) $filter['germplasmDbId'] = $notIncluded;
            } else if ($type == 'trait') {
                $model = $this->traits;
                $idColumn = 'variable';
                if (!is_null($notIncluded)) $filter['variableDbId'] = $notIncluded;
            }
        }

        // If list has less than 500 members do this, else use the bg process
        if ($totalNumberOfIds <= 500) {  // normal thing
            if ($selectMode == 'exclude mode') {
                $result = [];
                $ids = [];

                // Retrieve using filter minus the ids
                $response = $model->searchAll($filter);

                if (isset($response) && !empty($response) && isset($response['data'])) {
                    $result = $response['data'];
                    $ids = array_column($result, $idColumn . 'DbId');
                }
            }

            // Retrieve ids first using filter and selected_ids
            $saveListMembersResponse = $this->lists->createMembers($listId, $ids, false, [], true);
            if (isset($saveListMembersResponse['success']) && !empty($saveListMembersResponse['success']) && !$saveListMembersResponse['success']) {  // if errors were encountered during saving of list members
                return json_encode($saveListMembersResponse);
            }
        } else {    // bg process
            // Update list remark to show creating list member is in progress
            $requestData = [
                'remarks' => '<' . $listId . '-creating list member is in progress>' . $remarks
            ];
            $this->lists->updateOne($listId, $requestData);

            $endpoint = '';
            $idColumn = '';
            if ($type == 'germplasm') {
                $endpoint = 'germplasm-search';
                $idColumn = 'germplasmDbId';
            } else if ($type == 'trait') {
                $endpoint = 'variables-search';
                $idColumn = 'variableDbId';
            }
            $parameters['columnFilters'] = $filter;
            $parameters['endpoint'] = $endpoint;
            $parameters['idColumn'] = $idColumn;
            $parameters['listId'] = $listId;

            $this->worker->invoke(
                'CreateListMembers',                // Worker name
                'creating list members',            // Description
                'LIST_MEMBER',                      // Entity
                $listId,                            // Entity ID
                'LIST_MEMBER',                      // Endpoint Entity
                'LISTS',                            // Application
                'POST',                             // Method
                [                                   // Worker data
                    'parameters' => $parameters,
                    'type' => $type
                ]
            );

            $isBgProcess = true;
            $flashMessage = 'Saving list members for <b>' . $name . '</b> is in progress.';
            Yii::$app->session->setFlash('warning', '<i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> ' . $flashMessage);
            $message = 'Background process created. Redirecting to List Browser.';
        }

        $result = ['result' => true, 'message' => $message, 'isBgProcess' => $isBgProcess];

        return json_encode($result);
    }

    /**
     * Renders notifications from background processes
     */
    public function actionPushNotifications()
    {
        $userId = $this->user->getUserId();

        $notifs = $this->lists->getPushNotifications($userId);

        return $this->renderAjax(
            'notifications',
            $notifs
        );
    }

    /**
     * Update seen state og background job
     */
    public function actionUpdateSeenState()
    {
        $userId = $this->user->getUserId();

        $notifs = $this->lists->updateBgJobSeenState($userId);

        return true;
    }

    /**
     * Returns new notifications from background processes
     */
    public function actionNewNotificationsCount()
    {
        $userId = $this->user->getUserId();

        return $this->lists->getNewNotifsCount($userId);
    }

    /**
     * Save as new list of type variable
     */
    public function actionSaveVariableList()
    {
        $ids = isset($_POST['ids']) ? $_POST['ids'] : [];
        $tbl = isset($_POST['tbl']) ? $_POST['tbl'] : 'entry';
        $studyId = isset($_POST['studyId']) ? $_POST['studyId'] : '';

        $attributes = [
            'abbrev' => isset($_POST['abbrev']) ? $_POST['abbrev'] : '',
            'name' => isset($_POST['name']) ? $_POST['name'] : '',
            'displayName' => isset($_POST['displayName']) ? $_POST['displayName'] : '',
            'description' => isset($_POST['description']) ? $_POST['description'] : '',
            'remarks' => isset($_POST['remarks']) ? $_POST['remarks'] : '',
            'type' => isset($_POST['type']) ? $_POST['type'] : 'product'
        ];

        $data = '';

        return json_encode(['msg' => $data]);
    }

    /**
     * Split list into multiple child lists
     * @return json status of operation and output list ids
     */
    public function actionCheckSplitList($program)
    {
        $success = true;
        $message = '';
        $params = [];
        $bgprocess = false;

        if (isset($_POST['params']) && !empty($_POST['params'])) {
            $listData = [];

            foreach ($_POST['params'] as $value) {
                foreach ($value as $key => $val) {
                    $listData[$key] = $val;
                }
            }
        }

        $listId = isset($listData['listId']) && !empty($listData['listId']) ? $listData['listId'] : null;
        $childListCount = isset($listData['splitCount']) && !empty($listData['splitCount']) ? intval($listData['splitCount']) : 0;
        $splitOrder = isset($listData['splitOrder']) && !empty($listData['splitOrder']) ? $listData['splitOrder'] : '';

        if ($listId != null) {

            $params = [
                'listDbId' => 'equals ' . $listId
            ];

            $result = $this->lists->searchAll($params);
            if (isset($result) && !empty($result) && isset($result['status']) && $result['status'] == 200) {
                $listId = $result['data'][0]['listDbId'];
                $listType = $result['data'][0]['type'];

                $checkListIdResults = $this->checkListId($listId, $program);

                if (isset($checkListIdResults->success) && $checkListIdResults->success) {
                    $allMembers = $this->lists->searchAllMembers($listId, null, '', false);
                    $listMemberCount = isset($allMembers) && !empty($allMembers) && isset($allMembers['totalCount']) ? $allMembers['totalCount'] : 0;

                    $splitCheckResults = $this->listModel->validateSplit($listId, $listType, $childListCount, $splitOrder, $listMemberCount);

                    $success = $splitCheckResults['valid'];
                    $message = $splitCheckResults['message'];
                    $splitCheckResults['bgprocess'] = isset($splitCheckResults['bgprocess']) ? $splitCheckResults['bgprocess'] : '';
                    $bgprocess = $splitCheckResults['bgprocess'];

                    if ($success) {
                        if ($bgprocess) {
                            $success = false;
                        }
                        $params = $listData;
                        $params['abbrev'] = $result['data'][0]['abbrev'];
                    }
                } else {
                    $success = false;
                    $message = 'Invalid split operation.';
                }
            } else {
                $success = false;
                $message = 'Invalid split operation.';
            }
        }

        return json_encode(['success' => $success, 'message' => $message, 'bgprocess' => $bgprocess, 'params' => $params]);
    }

    /**
     * Split list into multiple child lists
     * @return json status of operation and output list ids
     */
    public function actionSplitList()
    {

        $status = true;
        $message = '';

        $parentListId = isset($_POST['params']['listId']) ? intval($_POST['params']['listId']) : null;
        $outputListCount = isset($_POST['params']['splitCount']) ? intval($_POST['params']['splitCount']) : 0;
        $splitOrder = isset($_POST['params']['splitOrder']) ? $_POST['params']['splitOrder'] : 'desc';

        $splitListResponse = $this->listOperations->splitList($parentListId, $outputListCount, $splitOrder);
        if (isset($splitListResponse['childListMemberIds']) && isset($splitListResponse['targetLists']) && $splitListResponse['childListMemberIds'] != null && !empty($splitListResponse['childListMemberIds']) && $splitListResponse['targetLists'] != null && !empty($splitListResponse['targetLists'])) {
            $updateChildListsResponse = $this->listOperations->updateParentInfo($splitListResponse['childListMemberIds'], $splitListResponse['targetLists']);

            $status = $updateChildListsResponse['success'];
            $message = $updateChildListsResponse['message'];
        } else {

            $status = $splitListResponse['success'];
            $message = $splitListResponse['message'];
        }
        return json_encode(['success' => $status, 'message' => $message]);
    }

    /**
     * Split list into multiple child lists in background job
     * @param String program current program filter
     * @return json status of operation and output list ids
     */
    public function actionSplitListBg($program)
    {
        $status = true;
        $message = '';

        $parentListId = isset($_POST['params']['listId']) ? intval($_POST['params']['listId']) : null;
        $outputListCount = isset($_POST['params']['splitCount']) ? intval($_POST['params']['splitCount']) : 0;
        $splitOrder = isset($_POST['params']['splitOrder']) ? $_POST['params']['splitOrder'] : 'desc';
        $abbrev = $_POST['params']['abbrev'] ?? '';

        $bgJobProcess = $this->listModel->splitListBackground($parentListId, $outputListCount, $splitOrder);

        $message = '<i class="fa fa-info-circle"></i>&nbsp;' . \Yii::t('app', "List <b>" . $abbrev . "</b> is being split in the background. You may proceed with other tasks. You will be notified in this page once done.");
        Yii::$app->session->setFlash('info', $message);

        $this->redirect(['list/index', 'program' => $program, 'MyList[abbrev]' => $abbrev]);
    }

    /**
     * Retrieve selected lists for merge modal
     */
    public function actionGetMergeLists()
    {
        $data = [];

        $selectedListsIds = isset($_POST['ids']) && !empty($_POST['ids']) ? $_POST['ids'] : null;

        if ($selectedListsIds != null) {

            $params = [
                'listDbId' => 'equals ' . implode('|equals ', $selectedListsIds)
            ];

            $result = $this->lists->searchAll($params);

            if (!isset($result) || empty($result)) {
                return json_encode(['error' => true, 'message' => 'Connection error encountered. Please try again.']);
            }

            if (isset($result['status']) && !empty($result['status']) && $result['status'] != 200) {
                return json_encode(['error' => true, 'message' => $result['message']]);
            }

            if (isset($result['status']) && !empty($result['status']) && $result['status'] == 200) {
                foreach ($result['data'] as $value) {
                    $data[$value['listDbId']] = $value['displayName'];
                }
                $listType = array_values(array_unique(array_column($result['data'], 'type')));
                $listMemberCount = array_sum(array_map('intval', array_values(array_column($result['data'], 'memberCount'))));

                // if selected lists are not of the same type
                if (count($listType) > 1) {
                    return json_encode(['error' => true, 'message' => 'Lists should be of the same type.']);
                }

                // if selected lists have 0 total number of items
                if ($listMemberCount < 1) {
                    return json_encode(['error' => true, 'message' => 'Selected lists are empty. No items to be merged.']);
                } else {
                    return json_encode(
                        [
                            'error' => false,
                            'data' => $this->renderAjax('merge_lists_content', [
                                'listCount' => count($data),
                                'listMemberCount' => $listMemberCount,
                                'listOptions' => ($data),
                                'listType' => $listType[0],
                                'listIds' => implode('|', $selectedListsIds)
                            ])
                        ]
                    );
                }
            }
        }
    }

    /**
     * Retrieve selected lists for merge modal
     */
    public function actionMergeLists()
    {
        $mergeOrder = 'desc';
        $mode = 'copy';
        $removeDuplicates = true;

        if (isset($_POST['params']) && !empty($_POST['params'])) {

            // update status of lists to be merged
            $selectedListsIds = $_POST['ids'];
            if (isset($selectedListsIds) && !empty($selectedListsIds)) {
                $selectedListsIds = explode('|', $_POST['ids']);
                $this->listModel->updateListStatus($selectedListsIds, 'in use');
            }

            $mergeParameters = [];
            foreach ($_POST['params'] as $value) {
                foreach ($value as $key => $val) {
                    $mergeParameters[$key] = $key == 'target_list' ? intval($val) : $val;
                }
            }
            $mergeParameters['order'] = $mergeOrder;
            $mergeParameters['ids'] = explode(',', $mergeParameters['ids']);

            if ($mergeParameters['option'] == 'new_list') {

                // create new list
                $param['records'][] = [
                    "abbrev" => $mergeParameters['abbrev'],
                    "name" => trim($mergeParameters['name']),
                    "displayName" => trim($mergeParameters['display_name']),
                    "type" => $mergeParameters['type'],
                    "description" => "",
                    "remarks" => ""
                ];

                $result = $this->lists->create($param);

                if (isset($result['status']) && !empty($result['status']) && $result['status'] == 200) {
                    $mergeParameters['target_list'] = $result['data'][0]['listDbId'];
                } else {
                    $message = $result['status'] === 400 ? 'Name/Abbrev already exists!' : $result['message'];
                    return json_encode(['success' => false, 'message' => $message]);
                }
            }
            $mergeResults = $this->listOperations->mergeList($mergeParameters['ids'], $mergeParameters['target_list'], $mode, $removeDuplicates);
            $mergeResults['message'] = isset($mergeResults['message']) ? $mergeResults['message'] : '';

            // update status of lists to be merged
            if (isset($selectedListsIds) && !empty($selectedListsIds)) {
                $this->listModel->updateListStatus($selectedListsIds, 'created');
            }

            return json_encode(['success' => $mergeResults['success'], 'message' => $mergeResults['message']]);
        } else {
            return json_encode(['success' => false, 'message' => 'Error in merging lists.']);
        }
    }

    /**
     * Merge lists into new lists in background job
     * @return json status of operation and output list ids
     */
    public function actionMergeListsBg($program)
    {
        if (isset($_POST['params']) && !empty($_POST['params'])) {

            // update status of lists to be merged
            $selectedListsIds = $_POST['ids'];
            if (isset($selectedListsIds) && !empty($selectedListsIds)) {
                $selectedListsIds = explode('|', $_POST['ids']);
                $this->listModel->updateListStatus($selectedListsIds, 'in use');
            }

            $mergeParameters = [];
            foreach ($_POST['params'] as $value) {
                foreach ($value as $key => $val) {
                    $mergeParameters[$key] = $key == 'target_list' ? intval($val) : $val;
                }
            }
            $mergeParameters['order'] = 'desc';
            $mergeParameters['ids'] = explode(',', $mergeParameters['ids']);

            if ($mergeParameters['option'] == 'new_list') {
                // create new list
                $param['records'][] = [
                    "abbrev" => $mergeParameters['abbrev'],
                    "name" => trim($mergeParameters['name']),
                    "displayName" => trim($mergeParameters['display_name']),
                    "type" => $mergeParameters['type'],
                    "description" => "",
                    "remarks" => ""
                ];
                $result = $this->lists->create($param);

                if (isset($result['status']) && !empty($result['status']) && $result['status'] == 200) {
                    $mergeParameters['target_list'] = $result['data'][0]['listDbId'];
                } else {
                    $message = $result['status'] === 400 ? 'Name/Abbrev already exists!' : $result['message'];
                    return json_encode(['success' => false, 'message' => $message]);
                }
            }
            $removeDuplicates = false; // Temporarily set to false - possible cause for performance issues in larger lists
            $bgJobProcess = $this->listModel->mergeListsBackground($mergeParameters['ids'], $mergeParameters['target_list'], 'copy', $removeDuplicates);

            $message = '<i class="fa fa-info-circle"></i>&nbsp;' . \Yii::t('app', 'Background process has been initiated for merging of lists. Kindly check the notifications for progress on the process.');
            Yii::$app->session->setFlash('info', $message);

            $this->redirect(['list/index', 'program' => $program]);
        } else {
            return json_encode(['success' => false, 'message' => 'Error in merging lists.']);
        }
    }

    /**
     * Facilitate updating of order_number according to sorting configuration
     * @param String program associated with list
     * @param Integer id id of list
     * 
     * @return Boolean true
     */
    public function actionReorderBySort($program, $id)
    {
        $threshold = !empty(Yii::$app->config->getAppThreshold('LISTS', 'reorderAllBySort')) ? Yii::$app->config->getAppThreshold('LISTS', 'reorderAllBySort') : 1000;
        $paramsSort = '?sort=';
        $memberCount = 0;

        if (isset($_POST['sort']) && !empty($_POST['sort'])) {

            // process current sorting
            $sortParams = $_POST['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();
            if (!empty($sortParams) && !empty($sortParams[0])) {
                foreach ($sortParams as $column) {
                    if (substr($column, 0, 1) == '-') {
                        $sortOrder[substr($column, 1)] = SORT_DESC;
                        $sortAttribute = substr($column, 1);

                        $paramsSort = $paramsSort . $sortAttribute . ':desc';
                    } else {
                        $sortOrder[$column] = SORT_ASC;
                        $sortAttribute = $column;

                        $paramsSort = $paramsSort . $sortAttribute;
                    }
                    if ($currParam < $countParam) {
                        $paramsSort = $paramsSort . '|';
                    }
                    $currParam += 1;
                }
            } else {
                $paramsSort .= 'orderNumber:asc';
            }

            $listMembers = Yii::$app->api->getParsedResponse('POST', 'lists/' . $id . '/members-search' . $paramsSort);

            if ($listMembers['status'] === 200 && isset($listMembers['data']) && !empty($listMembers['data'])) {
                $memberCount = $listMembers['totalCount'];
                $listMembers = $listMembers['data'];

                if ($memberCount > $threshold) {
                    //  pass to bg process trigger
                    $this->listMemberModel->reorderListMembersBySortConfigBgProcess($id, $paramsSort);

                    $listInfo = Yii::$app->api->getParsedResponse('POST', 'lists-search', json_encode(['listDbId' => 'equals ' . $id]));
                    $listName = isset($listInfo) && !empty($listInfo['data']) ? $listInfo['data'][0]['abbrev'] : '';

                    $this->listModel->updateListStatus([$id], 'in use');

                    $message = '<i class="fa fa-info-circle"></i>&nbsp;' .
                        \Yii::t('app', "The list members for <b>" . $listName . "</b> are being reordered in the background. You may proceed with other tasks. You will be notified in this page once done.");
                    Yii::$app->session->setFlash('info', $message);

                    $this->redirect(['list/index', 'program' => $program]);
                } else {
                    $this->listMemberModel->reorderListMembersBySortConfig($id, $paramsSort);

                    return json_encode(['success' => true, 'bgprocess' => false]);
                }
            } else {
                return json_encode(['success' => false, 'bgprocess' => false]);
            }
        }
    }

    // initiate background process worker for generating export CSV data
    public function actionExportToCsv()
    {
        Yii::debug(json_encode($_POST), __METHOD__);

        $postParams = $_POST;

        $description = $postParams['description'] ?? '';
        $endpointEntity = $postParams['endpointEntity'] ?? '';
        $application = $postParams['application'] ?? '';
        $entity = $postParams['entity'] ?? '';
        $entityDbId = $postParams['entityDbId'] ?? 0;
        $entityDbId = intval($entityDbId);

        // Build worker data
        $processName = $postParams['processName'] ?? '';
        $fileName = $_POST['fileName'] ?? '';
        $listType = $postParams['listType'] ?? '';
        $endpoint = $postParams['endpoint'] ?? '';
        $requestBody = $postParams['requestBody'] ?? '{}';
        $isTemplateUrl = false;
        $idArray = [];
        $csvAttributes = [];
        $csvHeaders = [];

        $programCode = null;

        $dashboardFilters = (array) $this->dashboardModel->getFilters();
        $programId = $dashboardFilters['program_id'] ?? '';

        if (!empty($programId)) {
            $programInfo = $this->programModel->getProgram($programId);
            $programCode = $programInfo['programCode'];
        }

        $columnVariables = $this->listMemberModel->retrieveVariablesFromConfig($programCode);

        if ($listType == 'plot') {
            ListMemberModel::getListMemberColumns('plot', $columnVariables, null, [], [], 'export');
        } else if ($listType == 'seed') {
            ListMemberModel::getListMemberColumns('seed', $columnVariables, null, [], [], 'export');
        } else if ($listType == 'germplasm') {
            ListMemberModel::getListMemberColumns('germplasm', $columnVariables, null, [], [], 'export');
        } else if ($listType == 'location') {
            ListMemberModel::getLocationMemberColumns(null, '', '', true, true, false);
        } else if ($listType == 'study') {
            ListMemberModel::getStudyMemberColumns(null, '', '', true, true, false);
        } else if ($listType == 'trait') {
            ListMemberModel::getListMemberColumns('trait', $columnVariables, null, [], [], 'export');
        } else if ($listType == 'package') {
            ListMemberModel::getListMemberColumns('package', $columnVariables, null, [], [], 'export');
        } else {
            $tempColumns = [
                'displayValue' => 'String',
                'orderNumber' => 'String'
            ];
            Yii::$app->session->set("list_members-columns", $tempColumns);
        }
        $columns = Yii::$app->session->get("list_members-columns");

        $isTemplateUrl = true;

        $csvAttributes = array_keys($columns);
        $csvHeaders = array_keys($columns);
        $csvHeaders = array_map(function ($header) {
            if ($header == strtoupper($header)) return $header;
            return ucwords(strtolower(implode(' ', preg_split('/(?=[A-Z])/', $header))));
        }, $csvHeaders);
        $idArray[] = $entityDbId;

        $userDbId = $this->user->getUserId();

        $result = $this->worker->invoke(
            'BuildCsvData',
            $description,
            $entity,
            $entityDbId,
            $endpointEntity,
            $application,
            'POST',
            [
                'processName' => $processName,
                'description' => $description,
                'userId' => $userDbId,
                'url' => $endpoint,
                'isTemplateUrl' => $isTemplateUrl,
                'idArray' => $idArray,
                'requestBody' => $requestBody,
                'fileName' => $fileName . '_' . date('Ymd_His', time()),
                'csvAttributes' => $csvAttributes,
                'csvHeaders' => $csvHeaders
            ],
            [
                'remarks' => $fileName . '_' . date('Ymd_His', time()) . '.csv'
            ]
        );

        return json_encode($result);
    }
}
