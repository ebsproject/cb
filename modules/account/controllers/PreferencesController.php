<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Contains methods for preferences
 */

namespace app\modules\account\controllers;
use Yii;
use app\components\B4RController;
use app\models\UserDashboardConfig;

class PreferencesController extends B4RController
{

    /**
     * Saves site nav state whether shown or hidden
     */
    public function actionSavesitenavstate(){
    	$showSideNav = Yii::$app->request->post('show_side_nav');
        Yii::$app->session->set('showSideNav',$showSideNav);

        UserDashboardConfig::saveLayoutPreferences('showSideNav',$showSideNav);
    }

    /**
     * Saves side nav skin whether dark or light
     */
    public function actionSavesidenavskin(){
    	$sideNavSkin = Yii::$app->request->post('side_nav_skin');

    	Yii::$app->session->set('sideNavSkin',$sideNavSkin);

        UserDashboardConfig::saveLayoutPreferences('sideNavSkin',$sideNavSkin);
    }

    /**
     * Save site font size
     */
    public function actionSavesitefontsize(){
        $fontSize = Yii::$app->request->post('font_size');

        Yii::$app->session->set('fontSize',$fontSize);

        UserDashboardConfig::saveLayoutPreferences('fontSize',$fontSize);
    }

    /**
     * Save site language
     */
    public function actionSavelanguage(){
        $language = Yii::$app->request->post('language');

        UserDashboardConfig::saveLayoutPreferences('language',$language);
    }
}