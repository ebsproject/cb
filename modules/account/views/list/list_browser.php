<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for list browser
**/

use app\components\FavoritesWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\bootstrap\Button;
use kartik\dynagrid\DynaGrid;
use kartik\select2\Select2;
use app\modules\account\models\ListModel;


?>

<?php

$userId = \Yii::$container->get('\app\models\User')->getUserId();
$showFromMyList = true;
$hideFromMyList = true;
$additionalOptions= isset($AdditionalOptions) ? $additionalOptions : '';
$listTypeOptions = [
    'germplasm' => 'Germplasm',
    'package' => 'Package',
    'trait' => 'Trait',
    'plot' => 'Plot',
    'seed' => 'Seed'
];

$isAdmin = Yii::$app->session->get('isAdmin');

$gridId = 'dynagrid-list-manager-my-lists-grid';

if($gridId == 'dynagrid-list-manager-my-lists-grid'){
    $createButton = "";
    $mergeButton = "";

    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_CREATE', 'LISTS');
    if ($isAdmin || $hasAccess) {
        $createButton = Html::a(
            \Yii::t('app', 'Create'),
            ['#'],
            [
                'class' => 'light-green darken-3 btn btn-success listmgt-add-list-btn waves-light waves-effect',
                'title' => \Yii::t('app', 'Add new list'),
                'id' => 'listmgt-add-list-btn',
                'data-toggle' => 'modal',
                'data-target' => '#add-list-modal',
                'style' => 'margin-right:5px; !important;',
            ]
        );
    }

    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_MERGE', 'LISTS');
    if ($isAdmin || $hasAccess) {
        $mergeButton = Html::a(
            '<i class="material-icons right">content_copy</i> ',
            ['#'],
            [
                'class' => 'btn btn-success lstmgt-merge-lists-btn waves-light waves-effect disabled',
                'title' => \Yii::t('app', 'Merge lists. Requires user to select atleast 2 lists for merging.'),
                'disabled' => true,
                'id' => 'lstmgt-merge-lists-btn',
                'data-toggle' => 'modal',
                'data-target' => '#merge-lists-modal',
                'style' => 'margin-right:5px;background-color:#5cb85c !important;',
            ]
        );
    }

    $showFromMyList = false;
}else{
    $createButton = '';
    $mergeButton = '';
    $hideFromMyList = false;
}

$columns = [
    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => '',
        'noWrap' => true,
        'template' => $buttons,
        'order'=>DynaGrid::ORDER_FIX_LEFT,
        'buttons' => [
            'view' => function ($url, $model, $key) use ($program, $isAdmin) {
                $styleOption = null;
                $inProgressString = '<'.$model['listDbId'].'-creating list member is in progress>';
                $isListInProgress = !is_null($model['remarks']) && substr($model['remarks'], 0, strlen($inProgressString)) === $inProgressString;
                if($isListInProgress) { // hide if creating list members is in progress
                    $styleOption = 'display:none';
                }

                $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_VIEW', 'LISTS');
                $viewButton = "";
                if ($isAdmin || $hasAccess) {
                    $viewButton = Html::a(
                        '<i class="material-icons">visibility</i> ',
                        '#',
                        [

                            'title' => \Yii::t('app', 'View List'),
                            'id' => 'view-list-btn',
                            'data-toggle' => 'modal',
                            'data-target' => '#view-list-modal',
                            'data-id' => $model['listDbId'],
                            'data-display_name' => $model['displayName'],
                            'style' => $styleOption
                        ]
                    );
                }

                return  $viewButton;
            },
            'manage' => function ($url, $model, $key) use ($program, $hideFromMyList, $isAdmin) {
                $disabled = $model['status'] != 'in use' ? '' : 'hidden';
                if ($model['permission'] == 'read_write') {
                    $inProgressString = '<'.$model['listDbId'].'-creating list member is in progress>';
                    $isListInProgress = !is_null($model['remarks']) && substr($model['remarks'], 0, strlen($inProgressString)) === $inProgressString;
                    $htmlOptions = [
                        "title"=>"Manage", 
                        'id'=>'update-list-btn', 
                        'class'=>'update-list-btn update-list-btn-'.$model['listDbId'].' '.$disabled
                    ];
                    if(!$hideFromMyList){
                        if (strpos($model['permission'], 'write') === false) {
                            $htmlOptions['style'] = 'display:none';
                        }
                    }

                    // hide if list type is variable or if creating list members is in progress
                    if ($model['type'] == 'variable' || $isListInProgress || $disabled != ''){
                        $htmlOptions['style'] = 'display:none';
                    }

                    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_UPDATE', 'LISTS');
                    $editButton = "";
                    if ($isAdmin || $hasAccess) {
                        $editButton = Html::a(
                            '<i class="material-icons">edit</i>',
                            $model['status'] != 'in use' ? Url::to([
                                '/account/list/create',
                                "id" => $model['listDbId'],
                                "program" => $program,
                            ]) : '#',
                            $htmlOptions
                        );
                    }

                    return $editButton;
                }
            },
            'delete' => function ($url, $model, $key) use ($userId, $isAdmin) {
                if ($model['permission'] == 'read_write' && ($userId == $model['creatorDbId'])) {
                    $inProgressString = '<'.$model['listDbId'].'-creating list member is in progress>';
                    $isListInProgress = !is_null($model['remarks']) && substr($model['remarks'], 0, strlen($inProgressString)) === $inProgressString;
                    $style = '';
                    // hide if list type is variable or if creating list members is in progress
                    if ($model['type'] == 'variable' || $isListInProgress){
                        $style = 'display:none';
                    }

                    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_DELETE', 'LISTS');
                    $deleteButton = "";
                    if ($isAdmin || $hasAccess) {
                        $deleteButton = Html::a(
                            '<i class="material-icons">delete_forever</i>',
                            '#',
                            [
                                'style' => 'cursor: pointer;' . $style,
                                'class' => 'ajaxDelete',
                                'data-list_id' => $model['listDbId'],
                                'data-toggle' => 'modal',
                                'data-target' => '#listmgt-delete-modal',
                                'data-transaction_id' => $model['listDbId'],
                                'data-list_display_name' => $model['displayName'],
                                'data-share_count' => $model['shareCount'],
                                'title' => \Yii::t('app', 'Delete List')
                            ]
                        );
                    }

                    return $deleteButton;
                }
            },
            'add' => function ($url, $model, $key) use ($program, $hideFromMyList, $isAdmin) {
                if ($model['permission'] == 'read_write') {
                    $inProgressString = '<'.$model['listDbId'].'-creating list member is in progress>';
                    $isListInProgress = !is_null($model['remarks']) && substr($model['remarks'], 0, strlen($inProgressString)) === $inProgressString;
                    $htmlOptions = ['title' => 'Add items'];
                    if(!$hideFromMyList){
                        if (strpos($model['permission'], 'write') === false) {
                            $htmlOptions['style'] = 'display:none';
                        }
                    }

                    // hide if list type is variable or if creating list members is in progress
                    if (!in_array($model['type'], ['trait', 'germplasm']) || $isListInProgress){
                        $htmlOptions['style'] = 'display:none';
                    }

                    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_ADD_ITEMS', 'LISTS');
                    $addButton = "";
                    if ($isAdmin || $hasAccess) {
                        $addButton = Html::a(
                            '<i class="material-icons">add</i>',
                            Url::to([
                                '/account/list/add-items',
                                'id' => $model['listDbId'],
                                'program' => $program
                            ]),
                            $htmlOptions
                        );
                    }

                    return $addButton;
                }
            },
            'share' => function ($url, $model, $key) use ($userId, $isAdmin) {
                if ($model['permission'] == 'read_write' && ($userId == $model['creatorDbId'])) {
                    $addtlStyleOption = '';
                    $inProgressString = '<'.$model['listDbId'].'-creating list member is in progress>';
                    $isListInProgress = !is_null($model['remarks']) && substr($model['remarks'], 0, strlen($inProgressString)) === $inProgressString;
                    if($isListInProgress) { // hide if creating list members is in progress
                        $addtlStyleOption = 'display: none;';
                    }

                    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_SHARE', 'LISTS');
                    $shareButton = "";
                    if ($isAdmin || $hasAccess) {
                        $shareButton = Html::a(
                            '<i class="material-icons">share</i>',
                            '#',
                            [
                                'style' => 'cursor: pointer;' . $addtlStyleOption,
                                'class' => 'listmgt-share',
                                'data-list_id' => $model['listDbId'],
                                'data-list_display_name' => $model['displayName'],
                                'title' => \Yii::t('app', 'Share List')
                            ]
                        );
                    }
                    return $shareButton;
                }
            },
            'split' => function ($url, $model, $key) use ($isAdmin) {
                if ($model['permission'] == 'read_write') {
                    $addtlStyleOption = '';
                    $inProgressString = '<'.$model['listDbId'].'-creating list member is in progress>';
                    $isListInProgress = !is_null($model['remarks']) && substr($model['remarks'], 0, strlen($inProgressString)) === $inProgressString;
                    if($isListInProgress) { // hide if creating list members is in progress
                        $addtlStyleOption = 'display: none;';
                    }

                    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_SPLIT', 'LISTS');
                    $splitButton = "";
                    if ($isAdmin || $hasAccess) {
                        $splitButton = Html::a(
                            '<i class="material-icons">call_split</i>',
                            '#',
                            [
                                'style' => 'cursor: pointer; margin-top: 10px; margin-left: 5px; margin-right: 5px;' . $addtlStyleOption,
                                'class' => 'listmgt-split',
                                'data-id' => $model['listDbId'],
                                'data-toggle' => 'modal',
                                'data-target' => '#split-list-modal',
                                'title' => \Yii::t('app', 'Split list'),
                                'label' => 'Split List',
                            ]
                        );
                    }
                    return $splitButton;
                }
            },
            'more' => function ($url, $model, $key) use ($hideFromMyList, $program, $isAdmin) {

                if ($model['permission'] == 'read_write' && $model['status'] != 'in use') {
                    $addtlStyleOption = '';
                    $inProgressString = '<'.$model['listDbId'].'-creating list member is in progress>';
                    $isListInProgress = !is_null($model['remarks']) && substr($model['remarks'], 0, strlen($inProgressString)) === $inProgressString;
                    if($isListInProgress) { // hide if creating list members is in progress
                        $addtlStyleOption = 'display: none;';
                    }
                    $htmlOptions = ["title"=>"Manage"];
                    if(!$hideFromMyList){
                        if (strpos($model['permission'], 'write') === false) {
                            $htmlOptions['style'] = 'display:none';
                        }
                    }

                    // hide if list type is variable
                    if ($model['type'] == 'variable'){
                        $htmlOptions['style'] = 'display:none';
                    }

                    $inlineStyle = isset($htmlOptions['style']) ? $htmlOptions['style'] : '';
                    $style = $inlineStyle . ' cursor: pointer; margin-top: 10px; margin-left: 5px; margin-right: 5px; padding-left:5px; padding-right:0px;' . $addtlStyleOption;
                    $htmlOptions['style'] = $style;

                    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_SPLIT', 'LISTS');
                    $splitBtn = ($isAdmin || $hasAccess) ? "<li>" . Html::a(
                        '<span class="material-icons" style=" font-size: 20px; vertical-align: -4px; margin-right: 4px;">call_split</span><p style="display: inline">' . \Yii::t('app', 'Split list') . '</p>',
                        '#',
                        [
                            'style' => $style,
                            'class' => 'listmgt-split',
                            'data-id' => $model['listDbId'],
                            'data-toggle' => 'modal',
                            'data-target' => '#split-list-modal',
                            'title' => \Yii::t('app', 'Split list'),
                            'label' => 'Split List',
                        ]
                    ) . "</li>" : "";

                    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_DELETE', 'LISTS');
                    $deleteBtn = ($isAdmin || $hasAccess) ?  "<li>" . Html::a(
                        '<span class="material-icons" style=" font-size: 20px; vertical-align: -4px; margin-right: 4px;" class="material-icons">delete_forever</span><p style="display: inline">' . \Yii::t('app', 'Delete list') . '</p>',
                        '#',
                        [
                            'style' => $style,
                            'class' => 'ajaxDelete',
                            'data-list_id' => $model['listDbId'],
                            'data-toggle' => 'modal',
                            'data-target' => '#listmgt-delete-modal',
                            'data-transaction_id' => $model['listDbId'],
                            'data-list_display_name' => $model['displayName'],
                            'data-share_count' => $model['shareCount'],
                            'title' => \Yii::t('app', 'Delete List')
                        ]
                    ) . "</li>" : "";

                    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_ADD_ITEMS', 'LISTS');
                    $addBtn = ($isAdmin || $hasAccess) ? "<li>" . Html::a(
                        '<span class="material-icons" style=" font-size: 20px; vertical-align: -4px; margin-right: 4px;" class="material-icons">add</span><p style="display: inline">' . \Yii::t('app', 'Add items to list') . '</p>',
                        Url::to([
                            '/account/list/add-items',
                            'id' => $model['listDbId'],
                            'program' => $program
                        ]),
                        $htmlOptions
                    ) . "</li>" : "";

                    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_SHARE', 'LISTS');
                    $shareBtn = ($isAdmin || $hasAccess) ? "<li>" . Html::a(
                        '<span class="material-icons" style=" font-size: 20px; vertical-align: -4px; margin-right: 4px;" class="material-icons">share</span><p style="display: inline">' . \Yii::t('app', 'Share list') . '</p>',
                        '#',
                        [
                            'style' => $style,
                            'class' => 'listmgt-share',
                            'data-list_id' => $model['listDbId'],
                            'data-list_display_name' => $model['displayName'],
                            'title' => \Yii::t('app', 'Share List')
                        ]
                    ) . "</li>" : "";

                    return Html::a(
                        '<i class="material-icons">more_vert</i>',
                        '#',
                        [
                            'style' => 'cursor: pointer; margin-top: 10px; margin-left: 5px; margin-right: 5px;' . $addtlStyleOption,
                            'class' => 'listmgt-more listmgt-more-' . $model['listDbId'] . ' dropdown-trigger dropdown-button-pjax',
                            'data-id' => $model['listDbId'],
                            'data-beloworigin' => 'true',
                            'data-constrainwidth' => 'false',
                            'data-activates' => 'dropdown-more-actions-' . $model['listDbId'],
                            'title' => \Yii::t('app', 'More Action'),
                            'label' => 'More Action',
                        ]
                    ) . '&emsp;' .
                        "<ul id='dropdown-more-actions-" . $model['listDbId'] . "' class='dropdown-content' style='width:145px; position: static !important;'>"
                        . $splitBtn
                        . $deleteBtn
                        . $addBtn
                        . $shareBtn
                        . "</ul>";
                }
            },
            'download' => function ($url, $model, $key) use ($program, $isAdmin) {
                $styleOption = null;
                $inProgressString = '<'.$model['listDbId'].'-creating list member is in progress>';
                $isListInProgress = !is_null($model['remarks']) && substr($model['remarks'], 0, strlen($inProgressString)) === $inProgressString;
                if($isListInProgress) { // hide if creating list members is in progress
                    $styleOption = 'display:none';
                }

                $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_DOWNLOAD', 'LISTS');
                $downloadButton = "";
                if ($isAdmin || $hasAccess) {
                    $downloadButton =
                        Html::a(
                            '<i class="material-icons">file_download</i> ',
                            '#',
                            [
                                'class' => 'download-list-members-btn',
                                'title' => \Yii::t('app', 'Download List Members'),
                                'data-id' => $model['listDbId'],
                                'data-abbrev' => $model['abbrev'],
                                'data-type' => $model['type'],
                                'style' => $styleOption
                            ]
                        );
                }

                return  $downloadButton;
            }
        ]
    ],
    [
        'attribute' => 'type',
        'visible' => true,
        'format' => 'raw',
        'noWrap'=>false,
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>$listTypeOptions, 
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true,'id'=>"select-status-data-browser-id"],],
        'filterInputOptions'=>['placeholder'=>'type','id'=>"select-status-".$gridId],
    ],
    [
        'attribute' => 'subType',
        'visible' => true,
        'format' => 'raw'
    ],
    [
        'attribute' => 'abbrev',
        'visible' => true,
        'format' => 'raw'
    ],
    [
        'attribute' => 'name',
        'visible' => true,
        'format' => 'raw'
    ],
    [
        'attribute' => 'displayName',
        'visible' => false,
        'format' => 'raw'
    ],
    [
        'attribute' => 'description',
        'visible' => true,
        'format' => 'raw'
    ],

    [
        'label' => 'Created by',
        'attribute' => 'creator',
        'filter' => true,
        'visible' => false
    ],
    [   
        'label' => 'Creation Timestamp',
        'attribute'=>'creationTimestamp',
        'format' => 'date',
        'visible' => true,
        'filter' => true,
        'filterType' => GridView::FILTER_DATE,
        'filterInputOptions' => [
            'autocomplete' => 'off'
        ],
        'filterWidgetOptions' => [
            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'clearBtn' => true,
                'todayHighlight' => true,
            ],
            'pluginEvents' => [
                'clearDate' => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
    ],    
    [

        'label' => 'Modified by',
        'attribute' => 'modifier',
        'format' => 'raw',
        'visible' => false,
        'filter' => true,
    ],
    [   
        'label' => 'Modification Timestamp',
        'attribute'=>'modificationTimestamp',
        'format' => 'date',
        'visible' => false,
        'filter' => true,
        'filterType' => GridView::FILTER_DATE,
        'filterInputOptions' => [
            'autocomplete' => 'off'
        ],
        'filterWidgetOptions' => [
            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'clearBtn' => true,
                'todayHighlight' => true,
            ],
            'pluginEvents' => [
                'clearDate' => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],  
    ],
    [
        'label' => 'Owned by',
        'attribute' => 'creator',
        'visible' => true
    ],
    [
        'label' => 'Count',
        'visible' => true,
        'contentOptions' => ['class' => 'text-center'],
        'headerOptions' => ['class' => 'text-center'],
        'format' => 'raw',
        'value' => function ($model){
            $count = $model['memberCount'];
            $style = ($count > 0) ? '' : 'style="background-color:gray"';
            return '<span class="badge new" '.$style.'><strong>'.$count.'</strong></span>';
        }
    ],
    [
        'attribute' => 'permission',
        'label' => 'Access Type',
        'visible' => true,
        'format' => 'raw',
        'noWrap'=>false,
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>[
            'owned' => 'Owned',
            'shared' => 'Shared with me'
        ], 
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true,'id'=>"select-permission-data-browser-id"],],
        'filterInputOptions'=>['placeholder'=>'access','id'=>"select-permission-".$gridId],
        'value' => function ($model) use ($userId) {
            
            if ($userId == $model['creatorDbId']) {
                return '<span class="new badge blue darken-3">Owned</span>';
            } else {
                return '<span class="new badge yellow darken-3">Shared with me</span>';
            }
        }
    ],
    [
        'label' => 'Settings',
        'attribute' => 'shared_settings',
        'visible' => true,
        'hidden' => $showFromMyList,
        'contentOptions' => ['class' => 'text-center'],
        'headerOptions' => ['class' => 'text-center'],
        'width'=>'9%',
        'format' => 'raw',
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>['all'=>'All','shared'=>'Shared','not_shared'=>'Not Shared'], 
        'filterWidgetOptions'=>['pluginOptions'=>['id'=>"select-settings-data-browser-".$gridId],],
        'filterInputOptions'=>['id'=>"select-settings-".$gridId],
        'value' => function ($model){

            if($model['shareCount']>0){
                return '<i class="material-icons">lock_open</i>';
            }else{
                return '<i class="material-icons">lock</i>';
            }
        }
    ],
];

if($gridId == 'dynagrid-list-manager-my-lists-grid'){
    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_CREATE', 'LISTS');
    $createButton = "";
    if ($isAdmin || $hasAccess) {

        $createButton = Html::a(
            \Yii::t('app', 'Create'),
            ['#'],
            [
                'class' => 'light-green darken-3 btn btn-success listmgt-add-list-btn waves-light waves-effect',
                'title' => \Yii::t('app', 'Add new list'),
                'id' => 'listmgt-add-list-btn',
                'data-toggle' => 'modal',
                'data-target' => '#add-list-modal',
                'style' => 'margin-right:5px; !important;',
            ]
        );
    }

    $hasAccess = Yii::$app->access->renderAccess('LIST_MANAGER_MERGE', 'LISTS');
    $mergeButton = "";
    if ($isAdmin || $hasAccess) {
        $mergeButton = Html::Button(
            '<i class="material-icons">merge_type</i> ',
            [
                'class' => 'btn lstmgt-merge-lists-btn waves-light waves-effect disabled',
                'style' => 'margin-right:5px;',
                'title' => \Yii::t('app', 'Merge lists. Requires user to select atleast 2 lists for merging.'),
                'disabled' => true,
                'id' => 'lstmgt-merge-lists-btn'
            ]
        );
    }

    $showFromMyList = false;

    $columns = array_merge([ 
        [
            'class' => 'app\components\B4RCheckboxColumn',
            'checkboxOptions' => function ($model) {
                $options = [
                    'value' => $model['listDbId'],
                    'class' => 'list-browser_checkbox'
                ];
                if ($model['permission'] != 'read_write') {
                    $options['style'] = 'display:none';
                    $options['hide'] = true;
                }
                return $options;
            },
            'displaySummaryId' => 'total-selected-text',
            'order'=>DynaGrid::ORDER_FIX_LEFT,
            'selectAllId' => 'lists-select-all'
        ],
    ], $columns);
}else{
    $createButton = '';
    $mergeButton = '';
    $hideFromMyList = false;
    $columns = array_merge([ 
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => false,
            'visible'=>true,
            'order' => DynaGrid::ORDER_FIX_LEFT,
        ]
    ], $columns);
}

$dynaGrid = DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $gridId,
        'dataProvider' => $dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>$gridId
            ],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            'heading' => '<h3>'.
                Yii::t('app', 'List Manager').
                FavoritesWidget::widget([
                    'module' => 'account',
                    'controller' => 'list'
                ])
                .'<h3>',
            'before' => \Yii::t('app', 'This is the browser for Lists you have access to. You may manage your Lists here.'). 
                '<br/>
                <p id="total-selected-text" class="pull-right" style="margin: -1px;"></p>',
                'beforeOptions' => [
                    'style' => 'height:50px'
                ],
            'after' => false
        ],
        'toolbar' =>  [
            [
                'content'=> $createButton.' '.$mergeButton.' '.
                Html::a(
                    '<i class="material-icons large">notifications_none</i>
                    <span id="notif-badge-id" class=""></span>',
                    '#',
                    [
                        'id' => 'notif-btn-id',
                        'class' => 'btn waves-effect waves-light list-browser-notification-button tooltipped',
                        'data-pjax' => 0,
                        'style' => "overflow: visible !important;",
                        "data-position" => "top",
                        "data-tooltip" => \Yii::t('app', 'Notifications'),
                        "data-activates" => "notifications-dropdown",
                        'disabled' => false,
                    ]
                ).
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index', "program" => $program], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid'), 'id'=>'list-reset']).'{dynagridFilter}{dynagridSort}{dynagrid}',
            ],
        ],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $gridId
        ]
]);


DynaGrid::end();
?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<?php
$listUrl = Url::toRoute(['/account/list','program'=>Yii::$app->userprogram->get('abbrev')]);
$newNotificationsUrl = Url::toRoute(['/account/list/new-notifications-count']);
$notificationsUrl = Url::toRoute(['/account/list/push-notifications']);
$updateSeenStateUrl = Url::toRoute(['/account/list/update-seen-state']);
$exportToCsvUrl = Url::to(['/account/list/export-to-csv']);

$this->registerJS(<<<JS

    var gridId = '$gridId';

    var newNotificationsUrl='$newNotificationsUrl';
    var notificationsUrl='$notificationsUrl';
    var updateSeenStateUrl='$updateSeenStateUrl';

    notifDropdown();
    renderNotifications();
    updateNotif();
    notificationInterval=setInterval(updateNotif, 10000);

    $('.dropdown-trigger').dropdown();

    $('#add-list-modal').on('shown.bs.modal', function () {
        $('#name-fld').focus();
    });

    $('.listmgt-split').on('click',function(){
        $('#id-fld').val($(this).attr('data-id')).trigger('change');
    });
    
    $('.listmgt-more').on('click',function(){
        updateDropdownPos($(this).data("id"));
    });

    $('#list-reset').on('click', function() {
        $('#'+gridId).b4rCheckboxColumn("clearSelection");
    });

    // Download list members
    $(document).on('click', '.download-list-members-btn', function(e) {
        e.preventDefault();

        var obj = $(this);
        var listDbId = obj.data('id');
        var listAbbrev = obj.data('abbrev');
        var listType = obj.data('type');

        // Invoke background worker
        $.ajax({
            url: '$exportToCsvUrl',
            type: 'post',
            dataType: 'json',
            data: {
                description: 'Preparing the members of List: ' + listAbbrev + ' for downloading',
                entity: 'LIST_MEMBER',
                entityDbId: listDbId,
                listType: listType,
                endpointEntity: 'LIST_MEMBER',
                application: 'LISTS',
                endpoint: 'lists/:id/members-search',
                fileName: listAbbrev+'-List_Members-Export'
            },
            async: false,
            success: function(response) {
                var success = response.success;
                var status = response.status;

                // Display message
                if (success) {
                    var notif = "<i class='material-icons green-text left'>check</i> The export file is being created in the background. You may proceed with other tasks. You will be notified in this page once done.";
                    Materialize.toast(notif,5000);
                } else {
                    // Failure message
                    var message = '<i class="material-icons red-text left">close</i> A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status !== 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if (status == null) message += ' (#002)';
                    Materialize.toast(message,5000);
                }
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There was a problem loading the content.";
                Materialize.toast(notif,5000);
            }
        });
    });

    /**
     * Displays the notification in dropdown UI
     */
    function notifDropdown(){
        $('.list-browser-notification-button').on('click',function(e){
            $.ajax({
                url: notificationsUrl,
                type: 'post',
                data: {
                    processName: 'CreateListMembers|SplitList|MergeLists|UpdateListItems'
                },
                async:false,
                success: function(data) {
                    var pieces = data.split('||');
                    var height = pieces[1];
                    $('<style>.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');

                    data = pieces[0];
                    renderNotifications();
                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                    $('#notifications-dropdown').html(data);

                    $('.bg_notif-btn').on('click', function(e){
                        var obj = $(this);
                        var abbrev = obj.data('abbrev');
                        var workerName = obj.data('worker-name')
                        var fileName = obj.data('filename')

                        switch(workerName.toUpperCase()){
                            case 'BUILDCSVDATA':
                                downloadExportFile(fileName, 'csv');
                                break;
                            default:
                                // refresh browser
                                $.pjax.reload({
                                    container: '#$gridId',
                                    replace: false,
                                    url: '$listUrl'+'&MyList%5Babbrev%5D='+abbrev
                                });

                                break;
                        }
                    });

                    updateSeenState();
                },
                error: function(xhr, status, error) {
                    var toastContent = $('<span>' + 'There was an internal error with your request.'+
                        '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                        '<i class="material-icons red toast-close-btn">close</i></button>' +
                        '<ul class="collapsible toast-error grey lighten-2">' +
                        '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                        '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                        '</li> </ul>');
                    Materialize.toast(toastContent, 'x', 'red');
                    $('.toast-error').collapsible();
                }
            });
        });
    }

    /**
     * Initialize dropdown materialize  for notifications
     */
    function renderNotifications(){
        $('.list-browser-notification-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            belowOrigin: true,
            alignment: 'right',
            stopPropagation: true
        });
    }

    function updateSeenState(){
        $.ajax({
            url: updateSeenStateUrl,
            type: 'post',
            async:false,
            success: function() {}
        });
    }

    /**
     * Shows new notifications
     */
    function updateNotif() {
        $.getJSON(newNotificationsUrl,
        {
            "processName": "CreateListMembers|SplitList|MergeLists"
        },
        function(json){
            $('#notif-badge-id').html(json);
            if(parseInt(json)>0){

                $('#notif-badge-id').addClass('notification-badge red accent-2');
            }else{
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
            }
        });
    }

    /**
     * Facilitate download of exported file
     *
     * @param {String} text file name from notification
     * @param {String} dataFormat data format of file to be downloaded
     */
    function downloadExportFile(text, dataFormat){
        var downloadDataUrl = '/index.php/occurrence/default/download-data'

        var toastContent = ''
        if(text == undefined || text == ''){
            toastContent = $('<span>' + 'The file name for the requested export file is unavailable.'+
            '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
            '<i class="material-icons red toast-close-btn">close</i></button>');

            Materialize.toast(toastContent, 'x', 'red');
        }
        else{
            var pieces = text.split('.')
            var fileName = pieces[0]

            downloadLink =  window.location.origin + `` + downloadDataUrl + `?filename=` + fileName + `&dataFormat=` + dataFormat
            window.location.href = downloadLink

            // remove loading indicator after 5 seconds
            setTimeout( function(){
                $('.progress').css('display','none');
                $('.indeterminate').css('display','none');
            }, 5000);
        }
    }
JS
);

$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');
