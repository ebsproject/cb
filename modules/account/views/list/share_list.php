<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for sharing access to list feature
**/

use kartik\select2\Select2;
use app\modules\account\models\ListModel;
use app\modules\account\models\ListAccessSearch;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

$userId = \Yii::$container->get('\app\models\User')->getUserId();

$getUsersByTeamUrl = Url::to(['list/get-users-by-team']);
$addAccessUrl = Url::to(['list/add-access']);
$refreshGridUrl = Url::to(['list/refresh-share-list-grid']);
$updateAccessUrl = Url::to(['list/update-access']);
$removeAccessUrl = Url::to(['list/revoke-access']);
$dataProvider = $searchModel->search(null, $listId);

?>

<div id="discard-panel" class="alert warning" style="display:none">
    <div class="card-panel"> You are about to revoke 
        <span style="color:red" id="revoke-user-count"></span> access records 
        <a class="pull-right revoke-link" id="cancel-revoke" href="#" style="margin-top: -10px;"><em class="material-icons revoke-icon">close</em></a>
        <a class="pull-right revoke-link" id="listmgt-revoke-access-btn" href="#" style="margin-top: -10px;"><em class="material-icons revoke-icon">check</em></a>
    </div>
</div>

<div class="col col-md-4">
<?php

echo Select2::widget([
    'name' => 'select_team',
    'data' => $programData,
    'options' => ['placeholder' => 'Select Program', 'id' => 'select2-team'],
    'pluginOptions' => [
        'allowClear' => true,
        'multiple' => true
    ]
]);

?>

<?php
echo '<br/>';
echo Select2::widget([
    'name' => 'select_user',
    'data' => $userData,
    'options' => ['placeholder' => 'Select Users', 'id' => 'select2-user'],
    'pluginOptions' => [
        'allowClear' => true,
        'multiple' => true
    ],
]);

echo '<br/><div class="row">'

.\macgyer\yii2materializecss\widgets\Button::widget([
    'label' => Yii::t('app', 'Add'),
    'options' => [
        'class' => 'btn-success pull-right',
        'id' => 'add-access-btn',
    ]

]);

echo '</div>';
?>

<ul class="collapsible">
    <li class="active">
        <div class="collapsible-header active"><em class="material-icons">edit</em>Update Permission</div>
        <div class="collapsible-body">
        <?php 
            echo Select2::widget([
                'name' => 'update_permission',
                'data' => [
                    'read' => 'Read',
                    'read_write' => 'Read/Write'
                ],
                'options' => ['placeholder' => 'Select Permission', 'id' => 'select2-permission'],
            ]);

            echo '<br/><div class="row">'

            .\macgyer\yii2materializecss\widgets\Button::widget([
                'label' => Yii::t('app', 'Apply to selected'),
                'options' => [
                    'class' => 'btn-success pull-right disabled',
                    'id' => 'update-permission-btn',
                ]

            ]);

            echo '</div>';
        ?>
        </div>
    </li>
</ul>

</div>
<div class="col col-md-8" id="share-list-grid-div">
<?php
    $count = $dataProvider->getTotalCount();
    echo Yii::$app->controller->renderPartial('share_list_grid', [
        'listId' => $listId,
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel
    ]);
?>
</div>

<?php
$removeAccessUrl = Url::to(['list/revoke-access']);

$this->registerJs(<<<JS
    var getUsersByTeamUrl = '$getUsersByTeamUrl';
    var addAccessUrl = '$addAccessUrl';
    var updateAccessUrl = '$updateAccessUrl';
    var refreshGridUrl = '$refreshGridUrl';
    var removeAccessUrl = '$removeAccessUrl';
    var listId = '$listId';
    var totalCount = '$count';

    var removeAccessUrl = '$removeAccessUrl';

    $('.collapsible').collapsible();
    updatePermissionButton();

    $('#add-access-btn').on('click', function() {
        var selectedUsers = $('#select2-user').val();
        var selectedTeams = $('#select2-team').val();
        if (selectedUsers.length == 0 && selectedTeams.length == 0) {
            var notif = "<i class='material-icons orange-text'>warning</i> Select at least 1 user or program";
            Materialize.toast(notif, 5000);
        } else {
            $('#share-list-grid-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    async: false,
                    url: addAccessUrl,
                    data: {selectedUsers: selectedUsers, selectedTeams: selectedTeams, listId: listId},
                    success: function(response) {
                        $.ajax({
                            type: 'POST',
                            url: refreshGridUrl,
                            data: {listId: listId},
                            success: function (data) {
                                $('#share-list-grid-div').html(data);
                                $('#select2-user').val(null).trigger('change');
                                $('#select2-team').val(null).trigger('change');

                                totalCount = selectedUsers.length + selectedTeams.length;
                                updatePermissionButton();
                            }
                        });
                    }
                });
            }, 1000);
        }
    });

    $('#update-permission-btn').on('click', function() {
        var programEntityIds = [];
        var userEntityIds = [];
        $.each($('.share-list-cb:checkbox:checked'), function(i, v) {
            if ($(v).attr('data-entity') == 'user') {
                userEntityIds.push($(v).attr('data-id'));
            } else {
                programEntityIds.push($(v).attr('data-id'));
            }
        });
        if (programEntityIds.length == 0 && userEntityIds.length == 0) {
            var notif = "<i class='material-icons orange-text'>warning</i> Select at least 1 user or program";
            Materialize.toast(notif, 5000);
        } else {
            var newPermission = $('#select2-permission').val();
            if (newPermission == '') {
                var notif = "<i class='material-icons orange-text'>warning</i> Choose new permission";
                Materialize.toast(notif, 5000);
            } else {
                $('#share-list-grid-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
                setTimeout(function(){
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        async: false,
                        url: updateAccessUrl,
                        data: {programEntityIds: programEntityIds, userEntityIds: userEntityIds, listId: listId, newPermission: newPermission},
                        success: function(response) {

                            if (response.success) {
                                $.ajax({
                                    type: 'POST',
                                    url: refreshGridUrl,
                                    data: {listId: listId},
                                    success: function (data) {
                                        $('#share-list-grid-div').html(data);
                                        $('#select2-user').val(null).trigger('change');
                                        $('#select2-team').val(null).trigger('change');
                                    }
                                });
                            } else {
                                var message = "<i class='material-icons red-text'>close</i> " + response.message;
                                Materialize.toast(notif, 5000);
                            }
                        }
                    });
                }, 1000);
            }
        }
    });

    $('#cancel-revoke').on('click', function(e){

        e.preventDefault();

        $('.btn').removeClass('disabled');
        $('#discard-panel').hide();
    });

    $('#listmgt-revoke-access-btn').on('click', function(e){
        e.preventDefault();

        $('.btn').removeClass('disabled');
        $('#share-list-grid-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        $('#discard-panel').hide();

        setTimeout(function(){
            $.ajax({
                type: 'POST',
                dataType: 'json',
                async: false,
                url: removeAccessUrl,
                data: {programEntityIds: programEntityIds, userEntityIds: userEntityIds, listId: listId},
                success: function(response) {
                    var remainingCount = response.remaining;
                    if (remainingCount > 0) {
                        $('#update-permission-btn').removeClass('disabled');
                    } else {
                        $('#update-permission-btn').addClass('disabled');
                    }

                    $.ajax({
                        type: 'POST',
                        url: refreshGridUrl,
                        data: {listId: listId},
                        success: function (data) {
                            $('#share-list-grid-div').html(data);
                            $('#select2-user').val(null).trigger('change');
                            $('#select2-team').val(null).trigger('change');
                            
                            var notif = "<i class='material-icons green-text'>check</i> Successfully revoked access for selected users/programs";
                            Materialize.toast(notif, 5000);
                        }
                    });
                }
            });
        }, 1000);
    });

    function updatePermissionButton() {
        if (totalCount > 0) {
            $('#update-permission-btn').removeClass('disabled');
        } else {
            $('#update-permission-btn').addClass('disabled');
        }
    }
JS
);
?>
<style>  
    .revoke-icon {
        font-size: 35px;
    }
    .revoke-link {
        margin-top: -10px;
    }
</style>