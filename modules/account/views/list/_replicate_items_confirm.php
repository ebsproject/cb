<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for Replicate List Items - Assign Number of Copies step
**/

use kartik\dynagrid\DynaGrid;
?>

<p><a href='' class='tooltipped go-back-to-replicate-list-items-browse-parent-panel' data-position='top' data-tooltip='<?echo Yii::t('app', 'Go back to previous step');?>'>
    <i class='material-icons'>arrow_back</i>
</a></p>

<?if ($isSelectAllChecked) {
    ?><div class='row'><?
        echo Yii::t('app', "You are about to generate <strong>$numberOfCopies</strong> replicates for all <strong>$totalCount</strong> list items. Are you sure you want to proceed with these changes to the list items?")
    ?></div><?
} else {
    ?><div class='row'>
    You are about to replicate the following items of <strong><?echo $listAbbrev?></strong>. Are you sure you want to proceed with these changes to the list items?
    </div>
    
    <div class='row'><?
        DynaGrid::begin([
            'columns' => [
                [
                    'attribute' => 'numberOfCopies',
                    'value' => function ($data) {
                        return "<span class='display-name-suffix green-text text-darken-3' style='font-weight: 700'>{$data['numberOfCopies']}</span>";
                    },
                    'format' => 'raw',
                    'hAlign'=>'center',
                    'vAlign'=>'middle',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'orderNumber',
                    'hAlign'=>'center',
                    'vAlign'=>'middle',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'displayValue',
                    'label' => 'Display Name',
                    'enableSorting' => false,
                ],
                $additionalInfoColumn,
                [
                    'attribute' => 'displayValue',
                    'header' => '<span class="tooltipped" data-position="top" data-tooltip="' . Yii::t('app', 'Preview of the display name for the copies to be created') . '">Display Name Pattern <i class="material-icons" style="font-size:medium;vertical-align:sub;cursor:default;">info</i></span>',
                    'value' => function ($data) use ($isAppendChecked) {
                        if ($isAppendChecked) {
                            return "<span class='display-name-base'>{$data['displayValue']}<span class='display-name-suffix green-text text-darken-3' style='font-weight: 700'>-000000000001</span></span>";
                        }
    
                        return "<span class='display-name-suffix green-text text-darken-3' style='font-weight: 700'>{$data['displayValue']}</span>";
                    },
                    'format' => 'raw',
                    'enableSorting' => false,
                ],
            ],
            'theme' => 'simple-default',
            'showPersonalize' => false,
            'storage' => 'cookie',
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => [
                'id' => 'dynagrid-lm-confirm-replicate-list-items-id',
                'dataProvider' => $dataProvider,
                'filterModel'=>null,
                'showPageSummary'=>false,
                'responsive' => true,
                'pjax'=>true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options'=>['id'=>'dynagrid-lm-confirm-replicate-list-items-id'],
                    'beforeGrid'=>'',
                    'afterGrid'=>''
                ],
                'responsiveWrap'=>false,
                'panel'=>[
                    'heading'=>false,
                    'after' => false,
                    'footer'=> false,
                ],
                'toolbar' =>  '',
            ],
            'options' => [ 'id' => 'dynagrid-lm-confirm-replicate-list-items' ]
        ]);
        DynaGrid::end();
    ?></div><?
}?>

<div class='row'>
    <p><span><b>Add new copies</b>:</span></p>

    <p><input checked name='addNewCopies' id='insert-to-bottom' type="radio" class="insertion-method with-gap" value="append"/>
    <label for='insert-to-bottom'>To bottom of list</label></p>

    <p><input disabled name='addNewCopies' id='insert-after-copied' type="radio" class="insertion-method with-gap" value="reorder"/>
    <label for='insert-after-copied' class='disabled'>After copied list items</label></p>
</div>

<?
$this->registerJs(<<<JS
    $(document).ready(function (e) {
        $('.tooltipped').tooltip()
    })


    // go back to parent panel
    $('.go-back-to-replicate-list-items-browse-parent-panel').on('click', function(e){
        e.preventDefault()
        e.stopImmediatePropagation()

        // Go back to previous step (Browse step)
        panelAction('parent', 'show')
        panelAction('child', 'hide')

        $('#replicate-list-items-browse').removeClass('hidden')

        // show next button
        $('#next-replicate-list-items-modal-page').removeClass('hidden')
        $('#confirm-replicate-list-items').addClass('hidden')
    })
JS);
?>