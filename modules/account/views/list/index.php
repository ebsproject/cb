<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* Index file for list browser
**/

use app\components\FavoritesWidget;
use yii\bootstrap\Button;
use yii\helpers\Url;
use yii\helpers\Html;

use app\models\DataBrowserConfiguration;

?>

<!-- 
<ul class="tabs card">
    <li class="tab col s12"><a clas="active" href="#my-lists"><i class="glyphicon glyphicon-th-list"></i> My lists</a></li>
    <li class="tab col s12"><a class="" href="#shared-with-me"><span class="glyphicon glyphicon-share"></span>Shared with me</a></li>

</ul> -->

<div id="my-lists" class="col s12">
<?php
    $searchModel = \Yii::$container->get('\app\models\PlatformList');
    $userDashboardConfig = \Yii::$container->get('\app\models\UserDashboardConfig');
    $userId = \Yii::$container->get('\app\models\User')->getUserId();

    // Set page limit
    $limitVal = $userDashboardConfig->getDefaultPageSizePreferences();
    $paramLimit = 'limit='.$limitVal;

    // Set search filters
    $filters = 'sort=creationTimestamp:desc&'.$paramLimit;

    $params=Yii::$app->request->queryParams;
    $params['PlatformList']['creator_id']=$userId;

    $dataProvider = $searchModel->search($params, $filters);

    $gridId = 'dynagrid-list-manager-my-lists-grid';   
    
    // Set dataProvider grid id
    $dataProvider->id = $gridId;

    echo Yii::$app->controller->renderFile('@app/modules/account/views/list/list_browser.php', [
        'dataProvider' => $dataProvider,
        'dataBrowserId' => $gridId,
        'searchModel' => $searchModel,
        'program' => $program,
        'buttons' => '{view}{download}{manage}{more}'
    ]);

    $closeButton = '<div class="row">' .
        \macgyer\yii2materializecss\widgets\Button::widget([
            'label' => 'Close',
            'options' => [
                'class' => 'btn pull-right',
                'data-dismiss' => 'modal'

            ],
        ])

        . '</div>';
?>

</div>
<!-- Create Modal -->
<div id="add-list-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><em class="material-icons">add</em> Create List</h4>
            </div>
            <div class="modal-body">

                <?php
                echo Yii::$app->controller->renderPartial('create', [
                    'name' => '',
                    'abbrev' => '',
                    'display_name' => '',
                    'type' => '',
                    'subtype' => '',
                    'description' => '',
                    'remarks' => '',
                    'program' => $_GET['program'],
                    'id' => null,
                    'header' => '',
                    'showTabs' => false,
                    'create' =>true
                ]);
                ?>

            </div>
        </div>
    </div>
</div>

<!-- View List Modal -->
<div id="view-list-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4><em class="material-icons">format_list_numbered</em> View List <span id="listmgt-view-name" style="color: green;font-weight: bold;font-size:x-large;"></span></h4>
            </div>
            <div class="modal-body" id="view-list-body">
            </div>
            <div class="modal-footer">

                <?php 
                    echo $closeButton;
                ?>

            </div>
        </div>
    </div>
</div>

<!-- Share list modal -->
<div id="share-list-modal" class="fade modal in" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><em class="material-icons">share</em>Share List <span id="listmgt-share-name" style="color: green;font-weight: bold;font-size:x-large;"></span></h4>
            </div>
            <div class="modal-body" id="listmgt-share-body">

            </div>
            <div class="modal-footer">

                <?php 

                    echo $closeButton;
                ?>

            </div>
        </div>
    </div>
</div>

<!-- Confirm delete modal -->
<div class="modal fade" id="listmgt-delete-modal" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><em class="material-icons">warning</em>Confirm Delete </h4>
            </div>
            <div class="modal-body" id="listmgt-delete-body">
                <p>You are about to delete <strong><span id="listmgt-delete-listname">Plot list</span></strong>. This action cannot be undone.</p>
                <p id="listaccess-notif" style="color:red"></p>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" href="#">Cancel</a>&emsp;
                <button type="button" class="btn btn-danger" id="listmgt-delete-btn">Delete</button>
            </div>
        </div>
    </div>
</div>

<!-- Split List Modal -->
<div id="split-list-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" id="split-list-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><em class="material-icons">call_split</em> Split List</h4>
            </div>
            <div class="modal-body" id="split-list-body">

                <?php
                    echo Yii::$app->controller->renderPartial('split-list-content', [
                        'program' => $program,
                    ]);
                ?>

            </div>
        </div>
    </div>
</div>

<div id="shared-with-me" class="col s12" style="display:none">
<?php
    // $params=Yii::$app->request->queryParams;
    // $dataProvider2 = $sharedListModel->search($params);
    
    // // echo Yii::$app->controller->renderPartial('list_browser', [
    // //     'dataProvider' => $dataProvider2,
    // //     'gridId' => 'shared-with-me-grid',
    // //     'searchModel' => $sharedListModel,
    // //     'program' => $program,
    // //     'buttons' => '{view}{manage}{split}'

    // // ]);

    $viewUrl = Url::to(['list/view', 'program' => $program,]);
    $renderShareListUrl = Url::to(['list/render-share-list']);
    $deleteListUrl = Url::to(['list/delete']);
    $viewListUrl = Url::to(['list/view', 'program' => $program]);
    $mergeListsUrl =  Url::to(['list/get-merge-lists']);
?>
</div>

<!-- Merge List Modal -->
<div id="merge-lists-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" id="split-list-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><em class="material-icons">merge_type</em> Merge List</h4>
            </div>
            <div class="modal-body" id="merge-lists-body">

                <?php
                    echo Yii::$app->controller->renderPartial('merge_lists_content', [
                        'program' => $program,
                    ]);
                ?>

            </div>
        </div>
    </div>
</div>

<?php

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = $userDashboardConfig->getDefaultPageSizePreferences('default');

// Check if browser Id is empty
$browserId = empty($gridId) ? ['none'] : $gridId;

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ". json_encode($browserId) .",
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

(new DataBrowserConfiguration())->saveDataBrowserSettings([$gridId]);

$this->registerJs(<<<JS

    var gridId = '$gridId';

    $("document").ready(function(e){
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
    })
    
    $(document).on('ready pjax:success', function(e) {
        e.preventDefault();
        listCbActions();
    });

    $(document).on('ready pjax:complete', function(e) {
        e.stopPropagation();
        e.preventDefault();
        listCbActions();

        initDropdown();
        
    });
    renderModals();
    listCbActions();

    function renderModals(){

        var selectedLists = 0;
        var listsIds = $('#'+gridId).b4rCheckboxColumn("getSelectedRows");
        if (listsIds != null) {
            selectedLists = listsIds.length;
        }

        if(selectedLists > 1){
            $('#lstmgt-merge-lists-btn').attr('disabled',false);
            $('#lstmgt-merge-lists-btn').removeClass('disabled').trigger('change');
        }
        else{
            $('#lstmgt-merge-lists-btn').attr('disabled',true);
            $('#lstmgt-merge-lists-btn').removeClass('disabled').addClass('disabled').trigger('change');
        }

        var renderShareListUrl = "$renderShareListUrl";
        var deleteListUrl = "$deleteListUrl";

        $('#view-list-modal').on('show.bs.modal', function(event){
            $("#view-list-body").html('<div class="progress"><div class="indeterminate"></div></div>');
            var id = $(event.relatedTarget).data("id");
            var displayName = $(event.relatedTarget).data("display_name");
            $('#listmgt-view-name').html(displayName);
            var viewUrl = "$viewListUrl" +'&id='+id;
            setTimeout(function(){
                $.ajax({
                    type: 'GET',
                    url: viewUrl,
                    async: false,
                    success: function(data) {
                        $("#view-list-body").html(data);
                    }
                });	
            }, 500);
        });

        $('#listmgt-delete-modal').on('show.bs.modal', function(event){

            $('#listmgt-delete-btn').addClass('disabled');
            $('#listaccess-notif').html('<div class="progress"><div class="indeterminate"></div></div>');

            var listDisplayName = $(event.relatedTarget).data("list_display_name");
            var listId = $(event.relatedTarget).data("list_id");
            var shareCount = $(event.relatedTarget).data("share_count");

            $('#listmgt-delete-listname').html(listDisplayName);

            $('#listmgt-delete-btn').attr('data-listId', listId);
            $('#listmgt-delete-btn').attr('data-listName', listDisplayName);

            if (parseInt(shareCount) > 0) {
                $('#listaccess-notif').html('<i><b>WARNING</b></i>: This list has been shared to <b>' + shareCount + '</b> user/s.');
                $('#listaccess-notif').show();
            } else {
                $('#listaccess-notif').html('');
                $('#listaccess-notif').hide();
            }
            $('#listmgt-delete-btn').removeClass('disabled');
        });

        $('#listmgt-delete-btn').on('click', function(e){
            e.preventDefault();
            if (!$(this).hasClass('disabled')) {
                $('#listmgt-delete-btn').addClass('disabled');
                $('#listaccess-notif').html('<div class="progress"><div class="indeterminate"></div></div>');

                var listId = $(this).attr('data-listId');
                var listDisplayName = $(this).attr('data-listName');
                
                
                $.ajax({
                    url: deleteListUrl,
                    type: 'POST',
                    data: {id: listId},
                    dataType: 'json',
                    success: function(data){
                        
                        $.pjax.reload({
                            container: '#'+gridId+'-pjax'
                        });
                        
                        $('#listmgt-delete-modal').modal('hide');
                        if (data.success) {
                            var message = "<i class='material-icons green-text'>check</i> Successfully deleted list: <b>"+listDisplayName+"</b>.";
                        } else {
                            var message = "<i class='material-icons red-text'>close</i> "+data.message;
                        }
                        Materialize.toast(message, 5000);
                    }
                });
            }
        });     

        $(document).on('click','#lstmgt-merge-lists-btn', function(event){
            event.preventDefault();

            // retrieve ids of selected lists for merging
            var listIds = $('#'+gridId).b4rCheckboxColumn("getSelectedRows");
            if (listIds == null) {
                listIds = [];
            }

            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: '$mergeListsUrl',
                    data: {
                        ids: listIds
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if(data.error == true){
                            var message = '<span style="text-align: left;"><i class="material-icons red-text">warning</i>&nbsp;'+data.message+'</span>';
							Materialize.toast(message, 5000);
                        }
                        else{
                            $("#merge-lists-body").html(data.data);
                            $('#ids-fld').val(listIds.join(','));

                            $('#merge-lists-modal').modal('show');
                        }
                    }
                });	
            }, 500);
        });   
    }

    function listCbActions() {
        var maxHeight = ($(window).height() - 240);

        $(".kv-grid-container").css("height",maxHeight);

        $("#"+gridId+" tbody tr:not(a)").on('click',function(e){
            var selectedLists = 0;
            var listsIds = $('#'+gridId).b4rCheckboxColumn("getSelectedRows");
            if (listsIds != null) {
                selectedLists = listsIds.length;
            }

            if(selectedLists > 1){
                $('#lstmgt-merge-lists-btn').attr('disabled',false);
                $('#lstmgt-merge-lists-btn').removeClass('disabled').trigger('change');
            }
            else{
                $('#lstmgt-merge-lists-btn').attr('disabled',true);
                $('#lstmgt-merge-lists-btn').removeClass('disabled').addClass('disabled').trigger('change');
            }
        });
        
        $('#lists-select-all').on('click',function(e){
            var selectClass = 'list-browser_checkbox';

            var count = 0;
            var listsIds = $('#'+gridId).b4rCheckboxColumn("getSelectedRows");
            if (listsIds != null) {
                count = listsIds.length;
            }
            if(count > 1){
                $('#lstmgt-merge-lists-btn').attr('disabled',false);
                $('#lstmgt-merge-lists-btn').removeClass('disabled').trigger('change');
            } else {
                $('#lstmgt-merge-lists-btn').attr('disabled',true);
                $('#lstmgt-merge-lists-btn').removeClass('disabled').addClass('disabled').trigger('change');
            }
        });

        var renderShareListUrl = "$renderShareListUrl";

        $('.listmgt-share').on('click', function() {

            var listId = $(this).attr('data-list_id');
            var listDisplayName = $(this).attr('data-list_display_name');

            $('#listmgt-share-name').html(listDisplayName);
            $('#listmgt-share-body').html('<div class="progress"><div class="indeterminate"></div></div>');
            $('#share-list-modal').modal('show');

            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: renderShareListUrl,
                    async: false,
                    data: {listId, listId},
                    success: function(data) {
                        $("#listmgt-share-body").html(data);
                    }
                });
            }, 1000);

        });
    }

    $('#split-list-modal').on('show.bs.modal', function(event){
        var id = $(event.relatedTarget).data("id");
        $('#id-fld').val(id).trigger('change');
    });

    function initDropdown(){
        $('.dropdown-trigger').dropdown();

        $('.listmgt-more').on('click',function(){
            updateDropdownPos($(this).data("id"));
        });
    }

    function updateDropdownPos(id){
        $('#dropdown-more-actions-'+id).css({"position": "","top":"","left":""});
    };
    
JS
);