<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for manage items step
**/	

?>

<?php 
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Button;
use yii\data\ArrayDataProvider;
use app\modules\account\models\ListMemberModel;
use app\components\ManageSortsWidget;
use yii\bootstrap\Modal;

// Modal for Replicate List Items
Modal::begin([
    'id' => 'replicate-list-items-modal',
    'header' => "<h4><em class='material-icons' style='vertical-align: -4px; margin-right: 4px'>copy</em>Replicate List Items <small> >> {$model->abbrev} </small></h4> ",
    'closeButton' => [
        'class' => 'tooltipped close close-replicate-list-items-modal',
        'data-dismiss' => 'modal',
        'data-position' => 'top',
        'data-tooltip' => Yii::t('app', 'Cancel replication of selected times'),
    ],
    'footer' => Html::a('Cancel', '', [
        'class' => 'tooltipped close-replicate-list-items-modal',
        'data-dismiss' => 'modal',
        'data-position' => 'top',
        'data-tooltip' => Yii::t('app', 'Cancel replication of selected times'),
    ]) . Button::widget([
        'label' => 'Next',
        'options' => [
            'id' => 'next-replicate-list-items-modal-page',
            'class' => 'disabled btn tooltipped waves-effect waves-light',
            'data-position' => 'top',
            'data-tooltip' => Yii::t('app', 'Proceed to the next step'),
        ],
    ]) . Button::widget([
        'label' => 'Confirm',
        'options' => [
            'id' => 'confirm-replicate-list-items',
            'class' => 'hidden btn tooltipped waves-effect waves-light',
            'data-position' => 'top',
            'data-tooltip' => Yii::t('app', 'Confirm and begin replication of selected items'),
        ],
    ]),
    'footerOptions' => [
        'id' => 'replicate-list-items-footer',
        'class' => 'modal-footer',
    ],
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
    ],
]);
echo '<div style="margin: 10px 20px 5px 28px;"><div id="modal-progress" class="progress hidden"><div class="indeterminate"></div></div></div>';
echo '<div id="replicate-list-items-browse" class="replicate-list-items-modal-content parent-panel"></div>';
echo '<div id="replicate-list-items-confirm" class="replicate-list-items-modal-content child-panel"></div>';
Modal::end();

\yii\jui\JuiAsset::register($this);

$this->registerCss('
    [type="checkbox"]:not(:checked), 
    [type="checkbox"]:checked {
        opacity: 0;
    }
    label{
        margin-bottom: 0px;

    }
    kv-panel-before {
        padding: 5px;
    }

    .btn, .btn-large, .btn-flat {
        height: 28px;
        line-height: 28px;
    }
    input[type=text]:not(.browser-default){
        height: 34px;
    }

    #replicate-list-items-footer > * {
        margin-right: 16px;
    }
');

$gridId = ['dynagrid-list-manager-final-grid','dynagrid-list-manager-all-items-grid'];

echo Yii::$app->controller->renderPartial('tabs', [
    'active' => 'review',
    'id' => $id,
    'program' => $program,
    'abbrev' => isset($model->abbrev) && !empty($model->abbrev) ? $model->abbrev : '',
    'model' => $model,
    'browserId' => $gridId
]);

echo '<br/><div class="row">'

.\macgyer\yii2materializecss\widgets\Button::widget([
    'label' => Yii::t('app', 'Save'),
    'options' => [
        'class' => 'pull-right nxt-btn',
    ]

]);

echo '</div>';

$advancedTypes = [];
    
$finalGridSelectAllId = 'final-grid-select-all-id';
$finalGridSelectId = 'final-grid_select';

$params = Yii::$app->request->queryParams;
$modelType = isset($model->type) && !empty($model->type) ? $model->type : '';
$modelAbbrev = isset($model->abbrev) && !empty($model->abbrev) ? $model->abbrev : '';

$finalGridSearchModelName = 'FinalGridBrowserList';

if(!isset($params[$finalGridSearchModelName])){
    $params[$finalGridSearchModelName] = null;
};

$columns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'displayValue',
            'visible' => true,
            'format' => 'raw'
        ],
        'orderNumber',
        [	
            'attribute'=>'creationTimestamp',
            'format' => 'date',
            'visible' => false,
            
            'filterType' => GridView::FILTER_DATE,
            'filterWidgetOptions' => [

                'pluginOptions'=> [
                    'format' => 'yyyy-mm-dd',
                    'autoclose' => true,
                    'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton'=>true,
                    'todayHighlight' => true,
                    'showButtonPanel'=>true, 

                    'clearBtn' => true,
                    'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton'=>true,
                ]
            ],
        ],	
    ]
];
$dataProvider = new ArrayDataProvider([]);

if ($modelType != '' && $modelType == 'plot') {
    $dataProvider = $finalGridSearchModel->search('plot', $id, 'true', $params, false, false, $finalGridSearchModelName);
    $columns = ListMemberModel::getListMemberColumns(
        'plot',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $finalGridSelectAllId,
            'checkBoxClass' => $finalGridSelectId
        ],
        [],
        'view'
    );
} else if ($modelType != '' && $modelType == 'seed') {
    $dataProvider = $finalGridSearchModel->search('seed', $id, 'true', $params, false, false, $finalGridSearchModelName);
    $columns = ListMemberModel::getListMemberColumns(
        'seed',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $finalGridSelectAllId,
            'checkBoxClass' => $finalGridSelectId
        ],
        [],
        'view'
    );
} else if ($modelType != '' && $modelType == 'germplasm') {
    $dataProvider = $finalGridSearchModel->search('germplasm', $id, 'true', $params, false, false, $finalGridSearchModelName);
    $columns = ListMemberModel::getListMemberColumns(
        'germplasm',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $finalGridSelectAllId,
            'checkBoxClass' => $finalGridSelectId
        ],
        [],
        'view'
    );
} else if ($modelType != '' && $modelType == 'location') {
    $dataProvider = $finalGridSearchModel->search('location', $id, 'true', $params, false, false, $finalGridSearchModelName);
    $columns = ListMemberModel::getLocationMemberColumns($model, $finalGridSelectAllId, $finalGridSelectId);
} else if ($modelType != '' && $modelType == 'study') {
    $dataProvider = $finalGridSearchModel->search('study', $id, 'true', $params, false, false, $finalGridSearchModelName);
    $columns = ListMemberModel::getStudyMemberColumns($model, $finalGridSelectAllId, $finalGridSelectId);
} else if ($modelType != '' && $modelType == 'trait') {
    $dataProvider = $finalGridSearchModel->search('trait', $id, 'true', $params, false, false, $finalGridSearchModelName);
    $columns = ListMemberModel::getListMemberColumns(
        'trait',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $finalGridSelectAllId,
            'checkBoxClass' => $finalGridSelectId
        ],
        [],
        'view'
    );
    $columns = ListMemberModel::getTraitMemberColumns($model, $finalGridSelectAllId, $finalGridSelectId);
} else if ($modelType != '' && $modelType == 'package') {
    $dataProvider = $finalGridSearchModel->search('package', $id, 'true', $params, false, false, $finalGridSearchModelName);
    $columns = ListMemberModel::getListMemberColumns(
        'package',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $finalGridSelectAllId,
            'checkBoxClass' => $finalGridSelectId
        ],
        [],
        'view'
    );
}

$finalGridBrowserId = $gridId[0];
$dataProvider->id = $gridId[0];
$totalCount = $dataProvider->totalCount;

echo '<div class="row" style="margin-top: 8px;"> <div class="col s6">';

if (isset($model->type) && !empty($model->type) && in_array($model->type, $advancedTypes)) {
    echo '<ul class="collapsible" id="advanced-filter-final">
            <li>
                <div class="collapsible-header" style="padding:0.5rem; color: #26a69a;">Advanced filter</div>
                <div class="collapsible-body">
                '.Yii::$app->controller->renderPartial('filters/'.$model->type, [
                    'sliderId' => 'listmgt-volume-slider-final',
                    'browserId' => $finalGridBrowserId,
                    'dataProvider' => $dataProvider,
                    'program' => $program,
                    'id' => $id
                ]).'
                </div>
            </li>
        </ul>
    ';
}

$resetGridUrl = Url::to(['list/review', 'program' => $program, 'id' => $id]);

$dynaGrid = DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'panel-success',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $finalGridBrowserId,
        'dataProvider' => $dataProvider,
        'filterModel'=>$finalGridSearchModel,
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'showPageSummary'=>false,
        'responsive' => true,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>$finalGridBrowserId
            ],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            'heading' => '<h3 class="panel-title"><i class="fa fa-list"></i> List</h3>',
            'before' => \Yii::t('app', 'This is your final list.')  .
                '<p id="selected-items-paragraph" class="pull-right" style="margin-right: 8px">
                    <b><span id="selected-items-count"> </span></b> <span id="selected-items-text"> </span>
                </p>',
            'beforeOptions' => [
                'style' => 'height:70px'
            ], 
            'after' => false
        ],
        'toolbar' =>  [
            [
                'content'=> Html::a("<span
                            class='material-icons'
                            style='
                                font-size: 16px;
                                vertical-align: -4px;
                            '
                        > handyman </span> <span class='caret'/>", 
                    '#',
                    [
                        'id' => 'lm-functions-dropdown-btn',
                        'class' => 'btn dropdown-button dropdown-button-pjax', 
                        'title' => \Yii::t('app', 'See more functions'),
                        'style' => 'margin-right:5px; background-color:#5cb85c !important;',
                        'data-activates' => 'lm-functions-dropdown-list',
                        'data-beloworigin' => 'true',
                        'data-constrainwidth' => 'false',
                    ])
                . "<ul
                        id='lm-functions-dropdown-list'
                        class='dropdown-content'
                    >
                        <li class='hide-loading tooltipped' data-position='right' data-tooltip='" . \Yii::t('app', 'Remove duplicate item(s)') . "'>" .
                            Html::a('<p style="display: inline;">' . \Yii::t('app', 'Remove duplicates') . '</p>', 
                            '',
                            [
                                'id' => 'remove-duplicates-btn',
                                'class' => 'remove-duplicates-btn', 
                            ]) .
                        "</li>
                        <li class='hide-loading tooltipped' data-position='right' data-tooltip='" . \Yii::t('app', 'Remove duplicate item(s)') . "'>" .
                            Html::a('<p style="display: inline;">' . \Yii::t('app', 'Remove items') . '</p>', 
                            '',
                            [
                                'id' => 'discard-btn',
                                'class' => 'add-list-btn',
                            ]) .
                        "</li>
                        <li class='hide-loading tooltipped' data-position='right' data-tooltip='" . \Yii::t('app', 'Replicate item(s) from the list') . "'>" .
                            Html::a('<p style="display: inline;">' . \Yii::t('app', 'Replicate items') . '</p>', 
                            '',
                            [
                                'id' => 'replicate-btn',
                            ]) .
                        "</li>
                    </ul>"
                . ManageSortsWidget::widget([
                    'dataBrowserId' => $finalGridBrowserId,
                    'gridId' => $finalGridBrowserId,
                    'searchModel' => 'ListMember',
                    'btnClass' => 'sort-action-button',
                    'resetGridUrl' => $resetGridUrl.'&reset=hard_reset'
                ])
                . '{dynagridFilter}{dynagridSort}{dynagrid}',
            ],
        ],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $finalGridBrowserId
        ]
]);


DynaGrid::end();

// set browser and parameters for removed items browser

$params = Yii::$app->request->queryParams;

$itemGridSelectAllId = 'items-grid-select-all-id';
$itemGridSelectId = 'all-items-grid_select';

$itemGridSearchModelName = 'RemovedItemsGridList';

if(!isset($params[$itemGridSearchModelName])){
    $params[$itemGridSearchModelName] = null;
};

$dataProvider = new ArrayDataProvider([]);
$columns = [];

if ($modelType != '' && $modelType == 'plot') {
    $dataProvider = $itemsGridSearchModel->search('plot', $id, 'false', $params, false, false, $itemGridSearchModelName);
    $columns = ListMemberModel::getListMemberColumns(
        'plot',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $itemGridSelectId
        ],
        [
            'isPreview' => false,
            'showOrderNumber' => false,
            'isFinalList' => false
        ],
        'view'
    );
} else if ($modelType != '' && $modelType == 'seed') {
    $dataProvider = $itemsGridSearchModel->search('seed', $id, 'false', $params, false, false, $itemGridSearchModelName);
    $columns = ListMemberModel::getListMemberColumns(
        'seed',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $itemGridSelectId
        ],
        [
            'isPreview' => false,
            'showOrderNumber' => false,
            'isFinalList' => false
        ],
        'view'
    );
} else if ($modelType != '' && $modelType == 'germplasm') {
    $dataProvider = $itemsGridSearchModel->search('germplasm', $id, 'false', $params, false, false, $itemGridSearchModelName);
    $columns = ListMemberModel::getListMemberColumns(
        'germplasm',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $itemGridSelectId
        ],
        [
            'isPreview' => false,
            'showOrderNumber' => false,
            'isFinalList' => false
        ],
        'view'
    );
} else if ($modelType != '' && $modelType == 'location') {
    $dataProvider = $itemsGridSearchModel->search('location', $id, 'false', $params, false, false, $itemGridSearchModelName);
    $columns = ListMemberModel::getLocationMemberColumns($model, $itemGridSelectAllId, $itemGridSelectId, false, false, false);
} else if ($modelType != '' && $modelType == 'study') {
    $dataProvider = $itemsGridSearchModel->search('study', $id, 'false', $params, false, false, $itemGridSearchModelName);
    $columns = ListMemberModel::getStudyMemberColumns($model, $itemGridSelectAllId, $itemGridSelectId, false, false, false);
} else if ($modelType != '' && $modelType == 'trait') {
    $dataProvider = $itemsGridSearchModel->search('trait', $id, 'false', $params, false, false, $itemGridSearchModelName);
    $columns = ListMemberModel::getListMemberColumns(
        'trait',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $itemGridSelectId
        ],
        [
            'isPreview' => false,
            'showOrderNumber' => false,
            'isFinalList' => false
        ],
        'view'
    );
} else if ($modelType != '' && $modelType == 'package') {
    $dataProvider = $itemsGridSearchModel->search('package', $id, 'false', $params, false, false, $itemGridSearchModelName);

    $columns = ListMemberModel::getListMemberColumns(
        'package',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $itemGridSelectId
        ],
        [
            'isPreview' => false,
            'showOrderNumber' => false,
            'isFinalList' => false
        ],
        'view'
    );
}

$removedItemsBrowserId = $gridId[1];
$dataProvider->id = $gridId[1];

echo '</div> <div class="col text-center">
<div class="dynagrid-sortable-separator"><i class="glyphicon glyphicon-resize-horizontal"></i></div>
</div>  <div class="col s5" style="width:45%;">';

if (isset($model->type) && !empty($model->type) && in_array($model->type, $advancedTypes)) {
    echo '<ul class="collapsible" id="advanced-filter-draft">
            <li>
                <div class="collapsible-header" style="padding:0.5rem; color: #26a69a;">Advanced filter</div>
                <div class="collapsible-body">
                '.Yii::$app->controller->renderPartial('filters/'.$model->type, [
                    'sliderId' => 'listmgt-volume-slider-draft',
                    'browserId' => $removedItemsBrowserId,
                    'dataProvider' => $dataProvider,
                    'program' => $program,
                    'id' => $id
                ]).'
                </div>
            </li>
        </ul>
    ';
}

$dynaGrid = DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'panel-info',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $removedItemsBrowserId,
        'dataProvider' => $dataProvider,
        'filterModel'=>$itemsGridSearchModel,
        'showPageSummary'=>false,
        'floatHeader'=>true,
        'floatOverflowContainer'=>true,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>$removedItemsBrowserId
            ],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            'heading' => '<h3 class="panel-title"><i class="fa fa-shopping-cart"></i>  Removed from the list</h3>',
            'before' => \Yii::t('app', 'Select from removed items below to re-add to your list.'),
            'beforeOptions' => [
                'style' => 'height:70px'
            ],
            'after' => false
        ],
        'toolbar' =>  [
            [
                'content'=> Html::a(\Yii::t('app', 'Return to list'), 
                '',
                [
                    'class' => 'btn btn-success add-list-btn waves-light waves-effect', 
                    'title' => \Yii::t('app', 'Return item(s) to list'),
                    'id'=>'add-btn',
                    'style'=>'margin-right:5px;background-color:#5cb85c !important;',
                ]). 
                Html::a(
                    'Delete item(s)', 
                    '', 
                    [
                        'id'=>'remove-btn-id',
                        'class'=>'btn btn-danger waves-effect waves-light', 
                        'style'=>'margin-right:5px;',
                    ]). '{dynagridFilter}{dynagridSort}{dynagrid}',
            ],
        ],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $removedItemsBrowserId
        ]
]);


DynaGrid::end();

echo '</div>';

echo '<br/><div class="row">'

.\macgyer\yii2materializecss\widgets\Button::widget([
    'label' => Yii::t('app', 'Save'),
    'options' => [
        'class' => 'pull-right nxt-btn',
    ]

]);

echo '</div>';

?>


<div class="modal fade in" id="confirm-remove" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <span id="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title commit-title">Remove Items</h4>
            </div>
            <div class="modal-body ">

                <p id="commit-remove-body">The selected items will be removed from the list.</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-warning selected-records amber" id="remove-selected" style="background-color: #f0ad4e !important;">Remove selected</button>

                <button type="button" class="btn btn-warning select-all-pages amber" id="remove-all" style="background-color: #f0ad4e !important;">Remove all</button>

                <button type="button" class="btn btn-warning amber" id="confirm-remove-btn" style="background-color: #f0ad4e !important;">Confirm</button>
            </div>
        </div>
    </div>
</div>

<?php
$updateUrl = Url::to(['list/update-list-member']);
$reorderUrl = Url::to(['list/reorder-list']);
$previewUrl = Url::to(['list/preview', 'program' => $program, 'id' => $id]);
$removeDuplicatesUrl = Url::to(['list/remove-duplicates', 'id' => $id]);
$reviewUrl = Url::to(['list/review', 'program' => $program, 'id' => $id]);
$sortAllUrl = Url::to(['list/reorder-by-sort', 'program' => $program, 'id' => $id, 'sort'=>isset($_GET['sort'])? $_GET['sort'] : '']);
$renderReplicateListItemsUrl = Url::to(['list/render-replicate-list-items',
    'listDbId' => $id,
    'listAbbrev' => $modelAbbrev,
    'listType' => $modelType,
    'idString' => '',
    'totalCount' => $totalCount,
]);
$renderConfirmReplicateListItemsUrl = Url::to(['list/render-confirm-replicate-list-items',
    'listAbbrev' => $modelAbbrev,
    'totalCount' => $totalCount,
]);
$confirmReplicateListItemsUrl = Url::to(['list/confirm-replicate-list-items']);

$this->registerJS(<<<JS
    var updateUrl = "$updateUrl";
    var reorderUrl = "$reorderUrl";
    var previewUrl = "$previewUrl";
    var reviewUrl = "$reviewUrl";
    var removeDuplicatesUrl = "$removeDuplicatesUrl";
    var renderReplicateListItemsUrl = '$renderReplicateListItemsUrl';
    var renderConfirmReplicateListItemsUrl = '$renderConfirmReplicateListItemsUrl';
    var confirmReplicateListItemsUrl = '$confirmReplicateListItemsUrl';
    var modelType = '$modelType';
    var modelAbbrev = '$modelAbbrev';
    var hasToast = false;
    var isSorted = false;

    var finalGridBrowserId = '$finalGridBrowserId';
    var removedItemsBrowserId = '$removedItemsBrowserId';

    var bgProcessInUse = false;
    var idCount = 0;
    var totalCount = $totalCount;
    var isSelectAllChecked = 0;

    $('.nxt-btn').on('click', function(){
        window.location = previewUrl;
    });

    $(document).on('ready pjax:success', function(e) {
        e.preventDefault();
        render();

        updateItemsBySort();
    });

    $("document").ready(function(e){
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        updateSelectedCountText()
        $('.dropdown-button').dropdown()
        $('.tooltipped').tooltip()
    })

    $(document).on('click','#sort-action-modal-apply-btn', function(e) {
        isSorted = true;
    });

    var select_all_id_string='d';
    var default_msg = 'Select at least one record to remove.';
    var default_msg1 = 'The selected item(s) will be removed from the list.';
    var list_id = "$id";

    render();

    function render(){
        // When traversing pages, if Select All is ticked, tick all checkboxes
        if (isSelectAllChecked) {
            $('#final-grid-select-all-id').prop('checked', true)
            $('.final-grid_select:checkbox').prop('checked', true).parent("td").parent("tr").addClass("warning")
        }

        $('.collapsible').collapsible()
        $('.dropdown-button').dropdown()
        $('.tooltipped').tooltip()

        $(".kv-grid-wrapper").css("height",$( window ).height()-330);
        $("#"+finalGridBrowserId+" tbody").sortable({
            opacity: 0.325,
            tolerance: 'pointer',
            cursor: 'move',
            update: function(event, ui) {}
        });
        $('#'+finalGridBrowserId+' tbody').on('sortupdate',function(event,ui){
            var uiItem = $(ui.item)[0];
            var dataId = $($(uiItem).find('.sort_order_arr')[0]).attr("data-id")
            var previousRowNumber = $($(uiItem['previousElementSibling']).find('.sort_order_arr')[0]).html();
            var newRowNumber = (previousRowNumber === undefined) ? 1 : parseInt(previousRowNumber) + 1;

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: reorderUrl,
                data: {dataId:dataId, newRowNumber:newRowNumber, listId:list_id},
                async: false,
                success: function(data) {
                    $.pjax.reload({
                        container: '#'+finalGridBrowserId+'-pjax'
                    });
                }
            });
        });

        // start GRID 2
        $('.final-grid_select').on('click',function(e){
            if($(this).prop("checked") === true)  {
                $(this)
                    .attr('checked','checked')
                    .parent("td")
                    .parent("tr")
                    .addClass("warning");
                idCount += 1;
            } else {
                idCount -= 1

                if (isSelectAllChecked) {
                    $('#final-grid-select-all-id').prop('checked', false)
                    $(".final-grid_select:checkbox")
                        .prop('checked', false)
                        .parent("td")
                        .parent("tr")
                        .removeClass("warning");
                    idCount = 0
                }

                $("#final-grid-select-all-id").prop("checked", false);
                $(this).removeAttr('checked');
                $(this).parent("td").parent("tr").removeClass("warning");
                isSelectAllChecked = 0
            }

            updateSelectedCountText()
        });

        $("#final-grid-select-all-id").change(function() {
            idCount = 0

            if (this.checked) {
                $(".final-grid_select:checkbox")
                    .prop('checked', true)
                    .parent("td")
                    .parent("tr")
                    .addClass("warning");
                isSelectAllChecked = 1;
                idCount = totalCount;
            } else {
                $(".final-grid_select:checkbox")
                    .prop('checked', false)
                    .parent("td")
                    .parent("tr")
                    .removeClass("warning");
                isSelectAllChecked = 0;
            }

            updateSelectedCountText()
        });
        // end GRID 2
            
        // start GRID 1
        $('.all-items-grid_select').on('click',function(e){
            if($(this).prop("checked") === true)  {
                $(this).attr('checked','checked');
                $(this).parent("td").parent("tr").addClass("warning");
                var isSelectAllChecked = 1;
                $(".all-items-grid_select").each(function() {
                    if (!this.checked)
                        isSelectAllChecked = 0;
                });
                if (isSelectAllChecked == 1) {
                    $("#items-grid-select-all-id").prop("checked", true);
                }
            }else{
                $("#items-grid-select-all-id").prop("checked", false);
                $(this).removeAttr('checked');
                $(this).parent("td").parent("tr").removeClass("warning");
            }
        });

        $("#items-grid-select-all-id").change(function() {
            if (this.checked) {
                $(".all-items-grid_select").each(function() {
                    this.checked=true;
                });
                $(".all-items-grid_select:checkbox").parent("td").parent("tr").addClass("warning");
            } else {
                $(".all-items-grid_select").each(function() {
                    this.checked=false;
                });
                $("input:checkbox.all-items-grid_select").parent("td").parent("tr").removeClass("warning");
            }
        });
        // end GRID 1
        $('#remove-btn-id').on('click',function(e){

            e.preventDefault();
            e.stopImmediatePropagation();
            
            if($('#select-all-id').attr("checked")){
                if(select_all_id_string===''){
                    var notif = "<i class='material-icons orange-text'>warning</i> Select at least one record to remove.";
                    Materialize.toast(notif, 10000);
                }
                else{
                    $('#confirm-remove').modal('show');
                }
            }
            else{
                var id=[];
                $("input:checkbox.all-items-grid_select").each(function(){
                    if($(this).prop("checked") === true)  {
                        id.push($(this).attr("id"));
                    }
                });
                id_string=id.join(",");

                if(id_string===''){
                    var notif = "<i class='material-icons orange-text'>warning</i> Select at least one record to remove.";
                    Materialize.toast(notif, 10000);
                }
                else{
                    $('#confirm-remove').modal('show');
                }
            }
        });

        $('#add-btn').on('click',function(e){
            e.preventDefault();
            $('#loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
            var id=[];

            $("input:checkbox.all-items-grid_select").each(function(){
                if($(this).prop("checked") === true)  {
                    id.push($(this).attr("id"));
                }
            });
            id_string=id.join("|");
            var notif = '';

            if (id_string !== '') {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: updateUrl,
                    data: {id: id_string, is_void: false, list_id: list_id},
                    async: false,
                    success: function(data) {
                        $.pjax.reload({
                            container: '#'+finalGridBrowserId+'-pjax'
                        });
                        $.pjax.xhr = null;
                        $.pjax.reload({
                            container: '#'+removedItemsBrowserId+'-pjax'
                        });
                        $.pjax.xhr = null;

                        notif = "<i class='material-icons green-text'>check</i> Successfully returned the item/s from list!";                            
                        Materialize.toast(notif, 5000);
                    }
                });
            }
            else{
                notif = "<i class='material-icons blue-text'>info</i> No item/s selected from the removed item/s list!";
                Materialize.toast(notif, 5000);
            }
        });

        $('#discard-btn').on('click',function(e){
            e.preventDefault();
            var id=[];

            $("input:checkbox.final-grid_select").each(function(){
                if($(this).prop("checked") === true)  {
                    id.push($(this).attr("id"));
                }
            });
            id_string=id.join("|");

            var notif = '';
            if (id_string !== '') {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: updateUrl,
                    data: {id: id_string, is_void: true, list_id: list_id},
                    async: false,
                    success: function(data) {

                        $.pjax.reload({
                            container: '#'+finalGridBrowserId+'-pjax'
                        });
                        $.pjax.xhr = null;
                        $.pjax.reload({
                            container: '#'+removedItemsBrowserId+'-pjax'
                        });
                        $.pjax.xhr = null;

                        if(!hasToast){ 
                            notif = "<i class='material-icons green-text'>check</i> Successfully removed items from list!";
                            Materialize.toast(notif, 5000); 
                            hasToast = true;
                        }
                    }
                });
            }
            else{
                notif = "<i class='material-icons orange-text'>warning</i> No item/s selected from the removed item/s list!";
                Materialize.toast(notif, 5000);
            }
        });

        $('#remove-duplicates-btn').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            var notif = '';
            $('#remove-duplicates-btn').removeClass('disabled').addClass('disabled').trigger('change');

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: removeDuplicatesUrl,
                data: {listId: list_id},
                async: false,
                success: function(data) {
                    if (data.duplicateCount !== 0) {
                        $.pjax.reload({
                            container: '#'+finalGridBrowserId+'-pjax'
                        });
                        $.pjax.xhr = null;
                        $.pjax.reload({
                            container: '#'+removedItemsBrowserId+'-pjax'
                        });
                        $.pjax.xhr = null;

                        notif = "<i class='material-icons green-text'>check</i> Successfully removed duplicates from list!";
                        Materialize.toast(notif, 5000);
                        hasToast = true;
                    }
                },
                complete: function(data){
                    if(data.responseJSON.duplicateCount == 0){
                        if(!hasToast){ 
                            notif = "<i class='material-icons blue-text'>info</i> No duplicates to be removed!";
                            Materialize.toast(notif,5000); 
                            hasToast = true;
                        }
                        else{
                            hasToast = false;
                        }
                    }

                    $('#remove-duplicates-btn').removeClass('disabled').trigger('change');
                }
            });
        });

        $('#replicate-btn').on('click', function (e) {
            e.preventDefault()
            e.stopPropagation()
            let id = []

            $("input:checkbox.final-grid_select").each(function() {
                if($(this).prop("checked") === true)  {
                    id.push($(this).attr("id"))
                }
            })
            idString = (isSelectAllChecked) ? '' : ( id.join("|") || '' )

            $('#next-replicate-list-items-modal-page').addClass('disabled')
            $("#replicate-list-items-confirm").addClass('hidden')
            $("#replicate-list-items-browse").addClass('hidden')
            $('#modal-progress').removeClass('hidden')

            $('#replicate-list-items-modal').modal('show')

            $.ajax({
                type: 'POST',
                url: renderReplicateListItemsUrl,
                data: {
                    listDbId: list_id,
                    listAbbrev: modelAbbrev,
                    listType: modelType,
                    idString: idString,
                    totalCount: totalCount,
                },
                success: function (data) {
                    $("#replicate-list-items-browse").html(data)

                    $('#modal-progress').addClass('hidden')
                    $('#replicate-list-items-browse').removeClass('hidden')
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#modal-progress').addClass('hidden')
                    $('#replicate-list-items-confirm').addClass('hidden')
                    $('#replicate-list-items-browse').removeClass('hidden')
                    $('#replicate-list-items-browse').html('There was a problem encountered.')
                }
            })
        })

        $('#next-replicate-list-items-modal-page').on('click', function (e) {
            e.stopPropagation()

            $("#replicate-list-items-browse").addClass('hidden')
            $('#modal-progress').removeClass('hidden')

            $.ajax({
                type: 'POST',
                url: renderConfirmReplicateListItemsUrl,
                data: {
                    listAbbrev: modelAbbrev,
                    numberOfCopies: parseInt(sessionStorage.getItem('numberOfCopies')),
                    numberOfCopiesValueMap: JSON.parse(sessionStorage.getItem('numberOfCopiesValueMap')),
                    isAppendChecked: sessionStorage.getItem('isAppendChecked'),
                },
                success: function (data) {
                    $("#replicate-list-items-confirm").html(data)

                    $('#modal-progress').addClass('hidden')
                    $('#replicate-list-items-browse').addClass('hidden')
                    $('#next-replicate-list-items-modal-page').addClass('hidden')
                    $('#confirm-replicate-list-items').removeClass('hidden')

                    $('#replicate-list-items-confirm').removeClass('hidden')

                    // Go to next step (Confirm step)
                    panelAction('parent', 'hide')
                    panelAction('child', 'show')
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#modal-progress').addClass('hidden')
                    $('#replicate-list-items-browse').addClass('hidden')
                    $('#next-replicate-list-items-modal-page').addClass('hidden')
                    $('#confirm-replicate-list-items').removeClass('hidden')
                    $('#replicate-list-items-confirm').removeClass('hidden')

                    $('#replicate-list-items-confirm').html('There was a problem encountered.')
                }
            })
        })

        $('.sort-action-button').on('click', function(e){
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons yellow-text'>warning</i>&nbsp;"+
                            "List item order numbers will be automatically updated once sort configuration is applied.";
                Materialize.toast(notif, 5000);
            }
        });

        $('.close-replicate-list-items-modal').on('click', function closeReplicateListItemsModal (e) {
            // Clear modal content
            $('.replicate-list-items-modal-content').html('')
            $('#replicate-list-items-browse').removeClass('slide-out-parent')
            $('#replicate-list-items-browse').css('display', '')
            $('#replicate-list-items-confirm').removeClass('slide-in-child')
            $('#replicate-list-items-confirm').css('display', '')

            // Reset footer buttons
            $('#next-replicate-list-items-modal-page').removeClass('hidden')
            $('#confirm-replicate-list-items').addClass('hidden')

            // Reset session variables
            sessionStorage.setItem('numberOfCopiesValueMap', JSON.stringify({}))
            sessionStorage.setItem('isAppendChecked', '')
        })

        $('#confirm-replicate-list-items').on('click', function (e) {
            const namePatternOption = sessionStorage.getItem('isAppendChecked') ? 'append' : 'retain'
            let addMemberOption = ''

            // Get selected insertion method
            $('.insertion-method').each(function () {
                if (this.checked) {
                    addMemberOption = this.value
                }
            })

            let confirmReplicateUrl =
                window.location.origin +
                confirmReplicateListItemsUrl +
                '?listDbId=' + list_id +
                '&listAbbrev=' + modelAbbrev +
                '&namePatternOption=' + namePatternOption +
                '&addMemberOption=' + addMemberOption

            // Trigger background processing
            window.location.assign(confirmReplicateUrl)
        })
    }
    
    // facilitates the updating of list item order number
    function updateItemsBySort(){
        var  currUrl = window.location.search
        
        $('.sort-action-button').removeClass('disabled').addClass('disabled').trigger('change');

        var urlSplit = currUrl.split('&');
        var sort = urlSplit[urlSplit.length-1].split('sort=')[1];
        if(sort !== undefined && sort !== ''){
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "$sortAllUrl",
                data: {
                    sort: sort
                },
                success: function(response) {
                    var hasToast = $('body').hasClass('toast');
                    var notif = '';
                    
                    if(response.success && !response.bgprocess){
                        if(!hasToast){
                            $('.toast').css('display','none');
                            notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully updated order number of list members!</span>";
                        }
                    }

                    if(isSorted){
                        window.location.href = window.location.search;
                        isSorted = false;

                        Materialize.toast(notif, 5000);
                    }

                    $('.sort-action-button').removeClass('disabled').trigger('change');
                },
                error: function() {
                    $('.sort-action-button').removeClass('disabled').trigger('change');
                }
            });
        }
        $('.sort-action-button').removeClass('disabled').trigger('change');
    };

    $('#confirm-remove').on('show.bs.modal',function(e){
        if($('#select-all-id').attr("checked")){  
            if(select_all_id_string!==''){
                $('#commit-remove-body').html(default_msg1);
                $('.selected-records').removeClass('hidden');
                $('.select-all-pages').removeClass('hidden');
                $('#confirm-remove-btn').addClass('hidden');
            }
        }else{
            var id=[];
            $("input:checkbox.all-items-grid_select").each(function(){
                if($(this).prop("checked") === true)  {
                    id.push($(this).attr("id"));
                }
            });
            id_string=id.join(",");

            if(id_string!==''){
                $('#commit-remove-body').html(default_msg1);
                $('.selected-records').removeClass('hidden');
                $('.select-all-pages').addClass('hidden');
                $('#confirm-remove-btn').addClass('hidden');
            }
        }
    });

    $('#remove-selected').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();
        $('#loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        var id=[];

        $("input:checkbox.all-items-grid_select").each(function getListMemberDbIds () {

            if($(this).prop("checked") === true)  {
                id.push($(this).attr("id"));
            }

        });
        id_string = id.join("|");
        var notif = '';

        if (id_string !== '') {
            $.ajax({
                url:"remove-item",
                type: 'post',
                dataType: 'json',
                data: {id:id_string, listId:list_id},
                success: function(response) {
                    $('#loading-div').html('');
                    $('#confirm-remove').modal('hide');
                    if (response.success) {
                        Materialize.toast(response.message, 2000, 'green');
                        $.pjax.reload({
                            container: '#'+removedItemsBrowserId+'-pjax'
                        });
                        $.pjax.xhr = null;
                    }else{
                        notif = "<i class='material-icons red-text'>error</i> "+response.error;
                        Materialize.toast(notif, 10000);
                    }
                },
                error: function() {
                    notif = "<i class='material-icons red-text'>error</i> Error encountered in deleting item/s.";
                    Materialize.toast(notif, 5000);
                }
            });
        }
    });

    $('.listmgt-filter-btn').on('click', function(){

        reviewUrl = "$reviewUrl";
        var browserId = this.id;
        $.each($('.advance-filter-flds-'+browserId), function(i,v) {
            var name = $(v).attr('data-name');
            var value = $(v).val();
            if (value !== '' && value.length !== 0) {
                reviewUrl = reviewUrl + '&List%5B'+name+'%5D=' + value;
            }
        });

        $.pjax.reload({
            container: '' + browserId + '-pjax',
            url: reviewUrl,
            replace: false
        });
    });

    $('.refresh-btn').on('click', function() {
        reviewUrl = "$reviewUrl"
        var browserId = this.id;
        $.pjax.reload({
            container: '' + browserId + '-pjax',
        });
        $.pjax.xhr=null;
    });

    /**
     * Checks and updates selected count text... 
     * ...based on selected checkboxes in Final List
     */
    function updateSelectedCountText () {
        if (idCount === 1) {
            $('#selected-items-count').html(idCount)
            $('#selected-items-text').html('selected item.')
        } else if(idCount > 1) {
            $('#selected-items-count').html(idCount)
            $('#selected-items-text').html('selected items.')
        } else if (idCount === 0) {
            $('#selected-items-count').html('No')
            $('#selected-items-text').html('selected items.')
        }
    }
JS
);