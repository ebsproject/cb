<?php

/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* View file for seed filters
**/

use macgyer\yii2materializecss\widgets\form\RangeInput;
use kartik\widgets\Select2;
use yii\helpers\Url;

macgyer\yii2materializecss\assets\NoUiSliderAsset::register($this);
	
$dataProviderModels = $dataProvider->getModels();

$sourceStudiesTags = array_filter(array_unique(array_column($dataProviderModels, 'study')));
asort($sourceStudiesTags);

$sourceStudiesTags = array_combine($sourceStudiesTags, $sourceStudiesTags);

$sourceYearTags = array_filter(array_unique(array_column($dataProviderModels, 'year')));
asort($sourceYearTags);

$sourceYearTags = array_combine($sourceYearTags, $sourceYearTags);

$containerTags = array_filter(array_unique(array_column($dataProviderModels, 'container_label')));
asort($containerTags);

$containerTags = array_combine($containerTags, $containerTags);


$volumeTags = array_unique(array_column($dataProviderModels, 'volume'));

$min = empty($volumeTags) ? 0 : min($volumeTags);
$max = empty($volumeTags) ? 0 : max($volumeTags);

$currMin = $min;
$currMax = $max;

$fldClass = 'advance-filter-flds-' . $browserId;

?>

<div class="row">
	<label>Volume:</label>
</div>
<div class="col s2">
	<?php echo '<input class="'.$fldClass.' min-input" data-name="min" type="number" min="'.$min.'" max="'.$max.'" id="min-'.$browserId.'" value="'.$min.'"/>'?>
</div>
<div class="col s7" id="<?php echo $sliderId;?>" data-browserId="<?php echo $browserId;?>"></div>
<div class="col s2">
	<?php echo '<input class="'.$fldClass.' max-input" data-name="max" type="number" min="'.$min.'" max="'.$max.'" id="max-'.$browserId.'" value="'.$max.'"/>'?>
</div>

<div class="row">
	<div class="col s6">
		<label>Source Study:</label>
		<?php 
			echo Select2::widget([
				'name' => 'input_type',
				'data' => $sourceStudiesTags,
				'size' => Select2::SMALL,
				'value' => '',
				'options' => [
					'placeholder' => '',
					'id' => 'source-study-' . $browserId,
					'multiple' => true,
					'class' => $fldClass,
					'data-name' => 'study'
				],
			], true);
		?>
			<label>Source Year:</label>

		<?php
			echo Select2::widget([
				'name' => 'input_type',
				'data' => $sourceYearTags,
				'size' => Select2::SMALL,
				'value' => '',
				'options' => [
					'placeholder' => '',
					'id' => 'source-year-' . $browserId,
					'multiple' => true,
					'class' => $fldClass,
					'data-name' => 'year'
				],
			], true);
		?>
	</div>
	<div class="col s6">
		<label>Container</label>
		<?php 
			echo Select2::widget([
				'name' => 'input_type',
				'data' => $containerTags,
				'size' => Select2::SMALL,
				'value' => '',
				'options' => [
					'placeholder' => '',
					'id' => 'container-' . $browserId,
					'multiple' => true,
					'class' => $fldClass,
					'data-name' => 'container_label'
				],
			], true);
		?>
	</div>
</div>

<div class="row">
	
	<?php 

		echo \macgyer\yii2materializecss\widgets\Button::widget([
			'label' => Yii::t('app', 'Filter'),
			'icon' => [
				'name' => 'filter_list',
				'position' => 'right'
			],
			'options' => [
				'class' => 'btn-success pull-right listmgt-filter-btn',
				'id' => $browserId,
			]
		])

		. \macgyer\yii2materializecss\widgets\Button::widget([
			'label' => Yii::t('app', ''),
			'icon' => [
				'name' => 'refresh',
				'position' => 'right'
			],
			'options' => [
				'class' => 'btn-success pull-right refresh-btn',
				'id' => $browserId,
				'style'=>'margin-right:5px;background-color:transparent !important;color:black'
			]
		]);
	?>

</div>

<?php 
	
$url = Url::to(['list/review', 'program' => $program, 'id' => $id]);

$this->registerJS(<<<JS
    var sliderId = "$sliderId";
    var min = parseInt("$min");
    var max = parseInt("$max");
    var currMax = parseInt("$currMax");
    var currMin = parseInt("$currMin");
    var browserId = "$browserId";
    var url = "$url";

    if (min === max) {
        min = min-1;
    }

    var slider = document.getElementById(sliderId);
    noUiSlider.create(slider, {
        start: [currMin, currMax],
        connect: true,
        step: 1,
        orientation: 'horizontal', // 'horizontal' or 'vertical'
        range: {
            'min': min,
            'max': max
        },
    });

    slider.noUiSlider.on('update', function (values, handle, unencoded) {

        var value = values[handle];
        var browserId = $($(this)[0]['target']).attr("data-browserId");
        if (handle) { // max
            $('#max-'+browserId).val(value);
        } else {
            $('#min-'+browserId).val(value);
        }
    });
            
    $('.min-input').on('change', function() {
        slider.noUiSlider.set([this.value, null]);
    });

    $('.max-input').on('change', function() {
        slider.noUiSlider.set([null, this.value]);
    });
JS
);
?>