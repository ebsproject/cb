<?php

/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* View file for plot filters
**/

use kartik\widgets\Select2;

$fldClass = 'advance-filter-flds-' . $browserId;

$dataProviderModels = $dataProvider->getModels();

$studyTags = array_filter(array_unique(array_column($dataProviderModels, 'occurrenceName')));
asort($studyTags);

$studyTags = array_combine($studyTags, $studyTags);

$plotCodeTags = array_filter(array_unique(array_column($dataProviderModels, 'plotCode')));
asort($plotCodeTags);

$plotCodeTags = array_combine($plotCodeTags, $plotCodeTags);

$entCodeTags = array_filter(array_unique(array_column($dataProviderModels, 'entryCode')));
asort($entCodeTags);

$entCodeTags = array_combine($entCodeTags, $entCodeTags);

$designationTags = array_filter(array_unique(array_column($dataProviderModels, 'designation')));
asort($designationTags);

$designationTags = array_combine($designationTags, $designationTags);

?>

<div class="row">
	<div class="col s6">
		<label>Occurrence:</label>
		<?php 
			echo Select2::widget([
				'name' => 'input_type',
				'data' => $studyTags,
				'size' => Select2::SMALL,
				'value' => '',
				'options' => [
					'placeholder' => '',
					'id' => 'occurrence-name-' . $browserId,
					'multiple' => true,
					'class' => $fldClass,
					'data-name' => 'occurrenceName'
				],
			], true);
		?>
	</div>
	<div class="col s6">
		<label>Designation:</label>
		<?php 
			echo Select2::widget([
				'name' => 'input_type',
				'data' => $designationTags,
				'size' => Select2::SMALL,
				'value' => '',
				'options' => [
					'placeholder' => '',
					'id' => 'designation-' . $browserId,
					'multiple' => true,
					'class' => $fldClass,
					'data-name' => 'designation'
				],
			], true);
		?>
	</div>
</div>

<div class="row">
	<div class="col s6">
		<label>Entry Code:</label>
		<?php 
			echo Select2::widget([
				'name' => 'input_type',
				'data' => $entCodeTags,
				'size' => Select2::SMALL,
				'value' => '',
				'options' => [
					'placeholder' => '',
					'id' => 'entcode-' . $browserId,
					'multiple' => true,
					'class' => $fldClass,
					'data-name' => 'entryCode'
				],
			], true);
		?>
	</div>
	<div class="col s6">
		<label>Plot Code:</label>
		<?php 
			echo Select2::widget([
				'name' => 'input_type',
				'data' => $plotCodeTags,
				'size' => Select2::SMALL,
				'value' => '',
				'options' => [
					'placeholder' => '',
					'id' => 'plotcode-' . $browserId,
					'multiple' => true,
					'class' => $fldClass,
					'data-name' => 'plotCode'
				],
			], true);
		?>
	</div>
</div>


<div class="row">
	
	<?php 

		echo \macgyer\yii2materializecss\widgets\Button::widget([
			'label' => Yii::t('app', 'Filter'),
			'icon' => [
				'name' => 'filter_list',
				'position' => 'right'
			],
			'options' => [
				'class' => 'btn-success pull-right listmgt-filter-btn',
				'id' => $browserId,
			]
		])

		. \macgyer\yii2materializecss\widgets\Button::widget([
			'label' => Yii::t('app', ''),
			'icon' => [
				'name' => 'refresh',
				'position' => 'right'
			],
			'options' => [
				'class' => 'btn-success pull-right refresh-btn',
				'id' => $browserId,
				'style'=>'margin-right:5px;background-color:transparent !important;color:black'
			]
		]);
	?>

</div>