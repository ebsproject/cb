<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for Replicate List Items - Assign Number of Copies step
**/

use kartik\dynagrid\DynaGrid;
?>

<div class="row">
    <? echo
        Yii::t('app', 'Please specify the number of additional replicates to add to the existing selected items. You may assign individually by filling in the leftmost column, OR you may assign in bulk in the field below.');
    ?>
</div>

<div class="row">
    <? echo
        Yii::t('app', '<strong>EXAMPLE</strong>: If you choose to add 3 more copies to a list item that is present only once in your starting list, that list item will appear 4 times in your final list.');
    ?>
</div>

<div class="row">
    <div class="col s6">
        <div class='row'>
            <div class='col s6 input-field tooltipped' data-position='top' data-tooltip='<?echo Yii::t('app', 'Choose how many copies to make for all selected items');?>'>
                <input id='number-of-copies-apply-to-all' class='number-of-copies-field' type='number' placeholder='Enter a number here' min='0' max='<?echo $maxRepValue?>'>
                <label for='number-of-copies-field' class='active'>Number of Copies</label>
            </div>
            <div class='col s6 input-field'>
                <a id='assign-number-of-copies-button' class='btn tooltipped waves-effect waves-light' data-position='top' data-tooltip='<?echo Yii::t('app', 'Assign number of copies to all selected list items');?>'><?echo Yii::t('app', 'Assign');?></a>
            </div>
        </div>
    </div>
    <div class="col s6 tooltipped" data-position='top' data-tooltip='<?echo Yii::t('app', 'Toggle whether to append entity name with serial number or not');?>'>
        <input checked id='append-replication-count-checkbox' type='checkbox' class='filled-in'/>
        <label for='append-replication-count-checkbox'>
        <span style='font-size: 14px;'>Append Entity Name with " -[replication_count] "</span>
        </label>
    </div>
</div>

<!-- Render HTML content: If isSelectAllChecked is true, render helper text; else, render simple data browser displaying selected list items for replication -->
<div class="row">
<?if ($isSelectAllChecked) {
    echo Yii::t('app', "<strong>NOTE</strong>: You are about to replicate all <strong>$totalCount</strong> list items.");
} else {
    $maxReplicate = $maxRepValue ?? 21;

    DynaGrid::begin([
        'columns' => [
            [
                'label'=> "Number of Copies",
                'content'=> function ($data) use ($maxReplicate) {
                    return "<input
                        id='$data[listMemberDbId]'
                        class='number-of-copies-field data-browser'
                        type='number'
                        placeholder='Enter a number here'
                        min='0'
                        max='$maxReplicate'
                        value='0'
                    >";
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
            ],
            [
                'attribute' => 'orderNumber',
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'displayValue',
                'label' => 'Display Name',
                'enableSorting' => false,
            ],
            $additionalInfoColumn,
            [
                'attribute' => 'displayValue',
                'header' => '<span class="tooltipped" data-position="top" data-tooltip="' . Yii::t('app', 'Preview of the display name for the copies to be created') . '">Display Name Pattern <i class="material-icons" style="font-size:medium;vertical-align:sub;cursor:default;">info</i></span>',
                'value' => function ($data) {
                    return "<span class='display-name-base'>{$data['displayValue']}<span class='display-name-suffix green-text text-darken-3' style='font-weight:700;'>-000000000001</span></span>";
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
        ],
        'theme' => 'simple-default',
        'showPersonalize' => false,
        'storage' => 'cookie',
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions' => [
            'id' => 'dynagrid-lm-replicate-list-items-id',
            'dataProvider' => $dataProvider,
            'filterModel'=>null,
            'showPageSummary'=>false,
            'responsive' => true,
            'pjax'=>true,
            'pjaxSettings'=>[
                'neverTimeout'=>true,
                'options'=>['id'=>'dynagrid-lm-replicate-list-items-id'],
                'beforeGrid'=>'',
                'afterGrid'=>''
            ],
            'responsiveWrap'=>false,
            'panel'=>[
                'heading'=>false,
                'before' => \Yii::t('app', "This data browser displays selected items in <b>$listAbbrev</b>.") . '{summary}',
                'beforeOptions' => [
                    'style' => 'height:50px'
                ],
                'after' => false,
                'footer'=> false,
            ],
            'toolbar' =>  ''
        ],
        'options' => [ 'id' => 'dynagrid-lm-replicate-list-items' ]
    ]);
    DynaGrid::end();
}?>
</div>

<?
$this->registerJS(<<<JS
    var numberOfCopiesValueMap = {}
    var maxRepValue = $maxRepValue

    $(document).ready(function (e){
        $('.tooltipped').tooltip()

        // Initialize map
        $('.number-of-copies-field.data-browser').each(function initializeValueMap () {
            numberOfCopiesValueMap[this.id] = 0
        })

        // Initialize session variables
        sessionStorage.setItem('numberOfCopies', 0)
        sessionStorage.setItem('numberOfCopiesValueMap', JSON.stringify({}))
        sessionStorage.setItem('isAppendChecked', 'checked')
    })

    // Set accepted characters for Leading Zeroes input field
    $('input.number-of-copies-field').on('keypress', function validateLeadingZeroesOnKeypress (e) {
        const LEADING_ZEROES_ACCEPTED_CHARACTERS = /[0-9]+/

        if (!LEADING_ZEROES_ACCEPTED_CHARACTERS.test(event.key)) {
            event.preventDefault()
        }
    })

    $('#assign-number-of-copies-button').on('click', function assignNumberOfCopiesToAllSelectedItems (e) {
        e.preventDefault()
        e.stopPropagation()

        let numberOfCopies = parseInt($('#number-of-copies-apply-to-all').val()) || 0

        // Store number of copies to session variable
        sessionStorage.setItem('numberOfCopies', numberOfCopies)

        if (numberOfCopies > maxRepValue) {
            let notif = `<i class='material-icons orange-text' style='margin-right:8px;'>warning</i> <span>The max number of copies to be made per selected item is <b>\${maxRepValue}</b></span>`
            Materialize.toast(notif, 5000)

            return
        } else if (numberOfCopies === 0) {
            $('#next-replicate-list-items-modal-page').addClass('disabled')
        } else if (numberOfCopies > 0) {
            $('#next-replicate-list-items-modal-page').removeClass('disabled')
        }

        $('.number-of-copies-field').val(numberOfCopies)

        // Update map
        $('.number-of-copies-field.data-browser').each(function updateValueMap () {
            numberOfCopiesValueMap[this.id] = numberOfCopies
        })

        // Store map to session variable
        sessionStorage.setItem('numberOfCopiesValueMap', JSON.stringify(numberOfCopiesValueMap))

        let notif = `<i class='material-icons blue-text' style='margin-right:8px;'>info</i> <span>Successfully set the number of copies for all selected items to <b>\${numberOfCopies}</b></span>`
        Materialize.toast(notif, 5000)
    })

    $('#append-replication-count-checkbox').on('click', function toggleAppendReplicationCountCheckbox (e) {
        e.stopPropagation()

        if (e.currentTarget.checked) {
            sessionStorage.setItem('isAppendChecked', 'checked')
            $('.display-name-base').removeClass('grey-text')
            $('.display-name-suffix').removeClass('grey-text')
            $('.display-name-suffix').addClass('green-text')
            $('.display-name-suffix').addClass('text-darken-3')
        } else {
            sessionStorage.setItem('isAppendChecked', '')
            $('.display-name-suffix').removeClass('green-text')
            $('.display-name-suffix').removeClass('text-darken-3')
            $('.display-name-suffix').addClass('grey-text')
            $('.display-name-base').addClass('grey-text')
        }
    })
        
    $('.number-of-copies-field.data-browser').on('change', function checkValuesForNextButton (e) {
        e.stopPropagation()

        numberOfCopiesValueMap[e.currentTarget.id] = parseInt(e.currentTarget.value)
        
        // Store map to session variable
        sessionStorage.setItem('numberOfCopiesValueMap', JSON.stringify(numberOfCopiesValueMap))

        if (parseInt(e.currentTarget.value) || Object.values(numberOfCopiesValueMap).some(e => e > 0)) {
            $('#next-replicate-list-items-modal-page').removeClass('disabled')
        } else {
            $('#next-replicate-list-items-modal-page').addClass('disabled')
        }
    })
JS);
?>