<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for items tab
**/

use kartik\tabs\TabsX;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use app\modules\account\models\ListModel;
?>

<?php 
    $buttonLabel = 'Save';

    echo Yii::$app->controller->renderPartial('tabs', [
        'active' => 'items',
        'id' => $id,
        'program' => $program,
        'abbrev' => isset($model->abbrev) && !empty($model->abbrev) ? $model->abbrev : '',
        'model' => $model
    ]);

    echo '<div class="row">'

    .\macgyer\yii2materializecss\widgets\Button::widget([
        'label' => Yii::t('app', $buttonLabel),
        'options' => [
            'class' => 'pull-right listmgt-items-nxt',
        ]
    ])
    . Html::a('Skip', ['#'], [
        'data-dismiss'=> 'modal',
        'class' => 'pull-right skip-btn',
        'style' => 'margin: 7px 5px 0 0'
    ])
    .'</div>';

    

    $items = [
        [
            'label'=>'<i class="glyphicon glyphicon-list-alt"></i> Input List',
            'content'=> Yii::$app->controller->renderPartial('input_types/input_list',[
                'model' => $model,
                'program' => $program
            ]),
            'active'=> true,
            'headerOptions' => ['id' => 'input_list']
        ]
    ];

    echo TabsX::widget([
        'items'=>$items,
        'position'=>TabsX::POS_LEFT,
        'encodeLabels'=>false,
        'options' => [
            'id' => 'options_tabs',
        ]
    ]);
    $modelType = isset($model->type) && !empty($model->type) ? $model->type : '';
    $previewUrl = Url::to(['list/review', 'program' => $program, 'id' => $id]);
    $validateUrl = Url::to(['list/validate-list', 'program' => $program, 'id' => $id, 'listType' => $modelType]);
    $addMemberUrlByInput = Url::to(['list/add-members-by-input', 'program' => $program, 'id' => $id, 'listType' => $modelType]);
    $addMemberUrlByStudy = Url::to(['list/add-members-by-study', 'program' => $program, 'id' => $id, 'listType' => $modelType]);
    $addLocationListUrl = Url::to(['list/add-location-to-list', 'program' => $program, 'id' => $id, 'listType' => $modelType]);


    echo '<div class="row">'

    .\macgyer\yii2materializecss\widgets\Button::widget([
        'label' => Yii::t('app',$buttonLabel),
        'options' => [
            'class' => 'pull-right listmgt-items-nxt',
        ]
    ])
    . Html::a('Skip', ['#'], [
        'data-dismiss'=> 'modal',
        'class' => 'pull-right skip-btn',
        'style' => 'margin: 7px 5px 0 0'
    ])
    .'</div><br/>';


    // define confirmation modal for list_type = STUDY

    Modal::begin([
        'id' => 'listmgt-studytype-modal',
        'header' => '<h4>Summary</h4>',
        'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
            .Html::a('Confirm',
                '#',
                [
                    'class' => 'btn waves-effect waves-light',
                    'url' => '#',
                    'id' => 'listmgt-save-study'
                ]
            ) . '&emsp;',
        'footerOptions' => ['id' => 'study-modal-footer'],
        'size' => 'modal-sm',
        'options' => ['data-backdrop' => 'static']
    ]);

    echo '<div id="studytype-loading-data_type"></div>';
    echo '<div id="listmgt-study-alert"></div>';


    Modal::end();
?>

<?php

$this->registerJs(<<<JS
    $('.tab-content').addClass('card');

    var previewUrl = "$previewUrl";
    var addMemberUrlByStudy = "$addMemberUrlByStudy";
    var addLocationListUrl = "$addLocationListUrl";
    var addMemberUrlByInput = "$addMemberUrlByInput";
    var program = "$program";
    var id = "$id";
    var studyIds = [];
    var locationIds = [];
    var listType = "$modelType";
    var addMoreFlag = false;
    var originalFooter = '';

    locationScripts();
    studyTypeScripts();

    $('.skip-btn').on('click', function(e) {
        e.preventDefault();
        window.location = previewUrl;
    });

    $('.listmgt-items-nxt').on('click', function() {

        if (listType == 'location') {
            locationIds = [];
            $.each($('.listmgt-location-select:checkbox:checked'), function(i, v) {
                locationIds.push($(v).attr('id'));
            })

            if (locationIds.length == 0) {
                Materialize.toast("Please select at least one (1) location!", 3000, 'red');
            } else {

                if (!addMoreFlag) {
                    originalFooter = $('#location-modal-footer').html();
                } else {
                    $('#location-modal-footer').html(originalFooter);
                    $('#loading-data_type').html("");
                    locationScripts();
                }
                $('#listmgt-location-alert').html("<h4>Adding " + locationIds.length + " location/s to list.</h4>");
                $('#listmgt-preview-modal').modal('show');
            }
        } else {
            var activeTabId = $('ul#options_tabs').find('li.active').attr('id');

            if (activeTabId === 'input_list') {
                var inputType = $('#input_type_fld').val();
                var inputList = $('#input_list_fld').val();
                var validateUrl = "$validateUrl";

                if (inputList !== '' && inputList !== null && inputList.length !== 0) {

                    $('#listmgt-desgcont-btn').addClass("disabled");
                    $('#listmgt-designation-alert').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
                    $('#listmgt-designation-modal').modal('show');

                    if (listType == 'seed') {
                        var withVolume = $('#with_volume').prop('checked');
                        if ($('#input_source_year').length > 0 && $('#input_source_year').val().length > 0) {
                            var sourceYears = $('#input_source_year').val();
                            var data = {inputType: inputType, inputList: inputList, sourceYears: sourceYears, withVolume: withVolume};
                        } else {
                            var data = {inputType: inputType, inputList: inputList, withVolume: withVolume};
                        }
                    } else {
                        var data = {inputType: 'designation', inputList: inputList};
                    }
                    setTimeout(function(){
                        $.ajax({
                            type: 'POST',
                            url: validateUrl,
                            data: data,
                            dataType: 'json',
                            success: function(response) {
                                if (response.validCount > 0) {
                                    $('#listmgt-desgcont-btn').removeClass("disabled");
                                } else {
                                    $('#listmgt-desgcont-btn').addClass("disabled");
                                    Materialize.toast(response.message, 5000, 'red');
                                }
                                $('#listmgt-designation-alert').html(response.htmlData);
                            }
                        })
                    }, 500);
                } else {

                    Materialize.toast("Input list is empty", 5000, 'red');

                }
            }
        }
    });

    $('#data_type_fld').on('change', function(){
        resetDataSummary($(this).val());
    });

    $('#listmgt-save-from-study').on('click', function() {

        var option = $('#data_type_fld').val();
        $.ajax({
            url: addMemberUrlByStudy,
            type: 'POST',
            dataType: 'json',
            data: {studyIds: studyIds, option: option},
            async: false,
            success: function(data){

                if (data.success) {
                    window.location = previewUrl;
                } else {
                    Materialize.toast("Error in adding to list", 3000, 'red');
                    location.reload();
                }
            }
        });
    });

    function locationScripts() {		
        $('#listmgt-save-location').on('click', function() {
            $('#listmgt-location-alert').html("");
            $('#location-modal-footer').html("");

            $('#loading-data_type').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
            setTimeout(function(){
                $.ajax({
                    url: addLocationListUrl,
                    type: 'POST',
                    dataType: 'json',
                    data: {locationIds: locationIds},
                    async: false,
                    success: function(data) {
                        $.pjax.reload({
                            container: '#location-browser-pjax'
                        });
                        setTimeout(function(){
                            $('#loading-data_type').html("<p style='color:green'><i class='material-icons'>check</i><b>Successfully added location/s to list!</b></p>");
                            $('#listmgt-location-alert').html('<a href="#" data-dismiss="modal" style="margin: 10px 15px 0 0; font-size: 15px; text-decoration: underline;">Add more</a>' +
                                '<a href="'+previewUrl+'" class="btn-succes btn">Done</a>'
                            );
                            addMoreFlag = true;
                            $('#listmgt-location-alert').attr('style', 'text-align:center;');
                        }, 1000);
                    }
                });
            }, 1000);
        });
    }
    function studyTypeScripts() {
        $('#listmgt-save-study').on('click', function() {
            $('#listmgt-study-alert').html("");
            $('#study-modal-footer').html("");

            $('#studytype-loading-data_type').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');

            setTimeout(function(){
                $.ajax({
                    url: addMemberUrlByStudy,
                    type: 'POST',
                    dataType: 'json',
                    data: {studyIds: studyIds, option: 'NA'},
                    async: false,
                    success: function(data) {

                        $.pjax.reload({
                            container: '#from_study_grid-pjax'
                        });

                        setTimeout(function(){

                            $('#studytype-loading-data_type').html("<p style='color:green'><i class='material-icons'>check</i><b>Successfully added study/ies to list!</b></p>");

                            $('#listmgt-study-alert').html('<a href="#" data-dismiss="modal" style="margin: 10px 15px 0 0; font-size: 15px; text-decoration: underline;">Add more</a>' +
                                '<a href="'+previewUrl+'" class="btn-succes btn">Done</a>'
                            );
                            addMoreFlag = true;
                            $('#listmgt-study-alert').attr('style', 'text-align:center;');
                        }, 1000);
                    }
                });
            }, 1000);
        });
    }
JS
);