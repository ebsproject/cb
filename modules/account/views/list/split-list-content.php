<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file creating the basic info of a list
**/

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\Select2;

$displayBtnTop = empty($id) ? 'none' : '';
$id = empty($id) ? '' : $id;
$createClass = empty($createClass) ? '' : $createClass;

$disabled = '';
$required = '<span style="color: red">*</span>';

$hiddenListIds = '
<div class="input-field col-md-6 s6" id="option-field-div" style="display:none;">' .
Html::input('text', 'SplitList[listId]', '', ['class' => 'split-text-field-var form-control hidden','id'=>'id-fld', 'required' => 'required', 'data-name'=>'' ]) .
'<label id="label-display" class="control-label" for="id-fld" value="'.$id.'"></label></div>';

$basicContent = '<br/><div style="display:'.$displayBtnTop.'" class="row">' .
\macgyer\yii2materializecss\widgets\Button::widget([
	'label' => 'Split',
	'icon' => [
		'name' => 'call_split',
		'position' => 'right'
	],
	'options' => [
		'class' => 'btn-success pull-right split-lists-btn',
	],
]) .
Html::a('Cancel', ['#'], [
    'data-dismiss'=> 'modal',
    'class' => 'pull-right split-list-cancel-btn',
    'style' => 'margin: 5px 5px 0 0'
])
. '</div>'.
'<div id="split-info-tab"><div class="row '.$createClass.'">'. '<div class="card-content">'.
Html::beginForm('', '', ['class'=>'form-horizontal', 'id' => 'split-list-form']).
'	    <div class="input-field col-md-6 s6">' .
            Html::input('number', 'SplitList[splitCount]', '', ['class' => 'split-text-field-var form-control '.$disabled,'id'=>'split-count-fld', 'required' => 'required', 'data-name'=>'Split Count' ]) .
            '<label id="label-display" class="control-label" for="split-count-fld">Number of child lists'.$required.'</label>
        </div>

        <div class="col-md-6 s6" style="padding-left:10px;">' .
            '<div class="input-field col-md-3 s9">
                <label id="label-display" class="control-label" for="split-count-fld" style="padding-top:3px;">Split order: '.$required.'</label>
            </div>'.
            '<div class="col-md-9 s9" style="padding-top:25px;">'.
                '<div class="radio-btn-row">
                    <input class="radio-btn-row split-option with-gap" type="radio" name="split-option" id="split-option-1" value=""/><label for="split-option-1">Asending</label>
                    &nbsp;&nbsp;
                    <input class="radio-btn-row split-option with-gap" checked type="radio" name="split-option" id="split-option-2" value=""/><label for="split-option-2">Descending</label>
                </div>'
            .'</div>'.
        '<div>' .
'</div></div>'.$hiddenListIds.
'<div class="progress hidden" style="margin:0;z-index:999"><div class="indeterminate"></div></div>'.
Html::endForm() . '</div></div></div></div>' .
'<div class="row" style="padding-top:10px; padding-bottom: 10px; padding-right:10px;">' .
\macgyer\yii2materializecss\widgets\Button::widget([
	'label' => 'Split',
	'options' => [
		'class' => 'btn-success btn pull-right waves-effect split-list-btn',
	],
]) .
Html::a('Cancel', ['#'], [
    'data-dismiss'=> 'modal',
    'class' => 'pull-right split-list-cancel-btn',
    'style' => 'margin: 5px 5px 0 0'
]);

echo $basicContent;

$browserUrl = Url::to(['list/index', 'program' => $program]);
$splitUrl = Url::to(['list/split-list', 'program' => $program]);
$splitBgProcessUrl = Url::to(['list/split-list-bg', 'program' => $program]);
$checkSplitUrl = Url::to(['list/check-split-list', 'program' => $program]);

$this->registerJS(<<<JS

    if($('#split-count-fld').val() == ""){
        $('.split-list-btn').attr('disabled',true);
        $('.split-list-btn').removeClass('disabled').addClass('disabled').trigger('change');
    }

    $('#split-count-fld').on('change',function(e) {
        $('.split-list-btn').attr('disabled',false);
        $('.split-list-btn').removeClass('disabled').trigger('change');
    })

    $('.split-list-cancel-btn').on('click', function(e) {
        var id = "$id";
        if (id !== null && id !== '') {
            e.preventDefault();
            window.location = "$browserUrl";
        }
    });

    $('.split-list-btn').on('click', function(e) {
        
        var form_value = [];
        var listId = $('#id-fld').val()

        form_value = $('#split-list-form').serializeArray().map(function(elem){
            var name = (elem.name != "_method" && elem.name != "_csrf" && elem.value != "") ? [elem.name.split("[")[1]].toString().split("]")[0] : null;
            return (name != null ? {[name]:elem.value} : null);
        }).filter(function(el){ 
            return el != null;
        });
        
        if(form_value.length > 0){
            if(form_value[0]['splitCount'] <= 1){
                var message = '<span style="text-align: left;"><i class="material-icons red-text">close</i>&nbsp;Invalid split count.</span>';
                Materialize.toast(message, 5000);
            }
            else{
                if($("#split-option-1").is(":checked")){
                    form_value.push({'splitOrder':'desc'});
                }
                else{
                    form_value.push({'splitOrder':'asc'});
                }

                setTimeout(function(){
                    $('.split-list-btn').removeClass('disabled').addClass('disabled').trigger('change');
                    $('.split-list-btn').attr('disabled',true);

                    $('.progress').removeClass('hidden').trigger('change');
                
                    $.ajax({
                        type: 'POST',
                        url: '$checkSplitUrl',
                        dataType: 'json',
                        data: {
                            params: form_value
                        },
                        async: false,
                        success: function(data) {
                            var message ='';

                            if(data.success == false){
                                if(data.bgprocess){
                                    message = '<span style="text-align: left;"><i class="material-icons yellow-text">warning</i>&nbsp;'+data.message+'</span>';
                                    splitListInBackground(data.params);
                                }
                                else{
                                    message = '<span style="text-align: left;"><i class="material-icons red-text">close</i>&nbsp;'+data.message+'</span>';
                                }
                                Materialize.toast(message, 5000);

                                $('.split-list-btn').attr('disabled',false);
                                $('.split-list-btn').removeClass('disabled').trigger('change');
                            }
                            else{
                                $('.listmgt-more-'+listId).addClass('hidden').trigger('change');
                                $('.update-list-btn-'+listId).addClass('hidden').trigger('change');

                                splitList(data.params);
                            }
                            $('.split-list-btn').attr('disabled',false);
                            $('.split-list-btn').removeClass('disabled').trigger('change');
                        }
                    });	
                }, 500);
            }
        }
        else{
            var message = '<span style="text-align: left;"><i class="material-icons red-text">close</i>&nbsp;Missing required fields.</span>';
            Materialize.toast(message, 5000);
        }
        
    });

    // split list
    function splitList(params){

        if(params != []){
			setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: '$splitUrl',
                    dataType: 'json',
                    data: {
                        params: params
                    },
                    async: false,
                    success: function(data) {
                        $('.progress').removeClass('hidden').addClass('hidden').trigger('change');

                        var message ='';
                        if(data.success != true){
                            message = '<span style="text-align: left;"><i class="material-icons red-text">close</i>&nbsp;'+data.message+'</span>';
                            Materialize.toast(message, 5000);
                        }
                        else{
                            $(".split-list-cancel-btn").trigger('click');
							message = '<span style="text-align: left;"><i class="material-icons green-text">check</i>&nbsp;Successfully split list.</span>';
                            Materialize.toast(message, 5000);

                            $.pjax.reload({
                                container: '#my-lists-grid-pjax'
                            });
                            
                            $('#my-lists-grid').b4rCheckboxColumn("clearSelection");
                        }
                        $('.split-list-btn').attr('disabled',false);
                        $('.split-list-btn').removeClass('disabled').trigger('change');
                    }
                });	
            }, 500);
		}
    }

    // split list in background
    function splitListInBackground(params){
        var listId = $('#id-fld').val()
    
        $('.split-list-btn').attr('disabled',true);
        $('.split-list-btn').removeClass('disabled').addClass('disabled').trigger('change');
        
        if(params != []){
			setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: '$splitBgProcessUrl',
                    dataType: 'json',
                    data: {
                        params: params
                    },
                    async: false,
                    success: function(data) {
                        var message ='';
                        
                        $('.progress').removeClass('hidden').addClass('hidden').trigger('change');

                        if(data.success != true){
                            message = '<span style="text-align: left;"><i class="material-icons red-text">close</i>&nbsp;'+data.message+'</span>';
                            Materialize.toast(message, 5000);

                            $('.split-list-btn').attr('disabled',false);
                            $('.split-list-btn').removeClass('disabled').trigger('change');
                        }
                    }
                });	
            }, 500);
		}
    }

JS
);