<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for listing all the users that list has been shared to
**/

use app\modules\account\models\ListAccessSearch;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

$dynaGrid = DynaGrid::begin([

    'columns' => [
        [
            'label'=> "checkbox",
            'visible'=> true,
            'header'=>'
                <input class="filled-in" type="checkbox" data-id-string="" id="share-list-cb-all" />
                <label for="share-list-cb-all"></label>
            ',
            'contentOptions' => ['style' => ['max-width' => '100px;',]],
            'content'=>function($data){
                return '
                    <input class="share-list-cb filled-in" type="checkbox" id="'.$data["rowDbId"].'" data-id="'.$data["entityDbId"].'" data-entity="'.$data["entity"].'"></input>
                    <label for="'.$data["rowDbId"].'"></label>
                
                ';
            },
            'hAlign'=>'center',
            'vAlign'=>'middle',
            'hiddenFromExport'=>true,
            'mergeHeader'=>true,
            'width'=>'50px'
        ],
        [
            'attribute' => 'entity',
            'visible' => true,
            'format' => 'raw'
        ],
        [
            'label' => 'Entity Name',
            'visible' => true,
            'value' => function($data) {
                if ($data['entity'] == 'user') {
                    return $data['personName'];
                } else {
                    return $data['programName'];
                }
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'permission',
            'visible' => true,
            'content' => function($data) {

                return '<span class="badge '. (($data['permission'] == 'read') ? ' new badge amber darken-2">READ' : ' new">READ/WRITE') .'</span>';

            },
            'format' => 'raw'
        ]
    ],
    'theme' => 'simple-default',
    'showPersonalize' => false,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel'=>$searchModel,
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'showPageSummary'=>false,
        'responsive' => true,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>['id'=>'share-list-grid'],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            
            'before' => \Yii::t('app', 'Displays users that have access to this list'),
            'beforeOptions' => [
                'style' => 'height:50px'
            ],
            'after' => false,
            'footer'=>false
        ],
        'toolbar' =>  [
            [
                'content'=>Html::a('Remove', '', [ 'class' => 'btn btn-default gray add-list-btn waves-light waves-effect', 'id' => 'remove-permission-btn', 'title'=>\Yii::t('app','Remove item')]).'{dynagridFilter}{dynagridSort}{dynagrid}',
            ],
        ]
    ],
    'options' => ['id' => 'share-list-grid']
]);
    
DynaGrid::end();

$removeAccessUrl = Url::to(['list/revoke-access']);
$refreshGridUrl = Url::to(['list/refresh-share-list-grid']);

$this->registerJs(<<<JS
    var listAccessIds = [];
    var removeAccessUrl = '$removeAccessUrl';
    var refreshGridUrl = '$refreshGridUrl';
    var listId = '$listId';
    var programEntityIds = [];
    var userEntityIds = [];


    $('#remove-permission-btn').on('click', function(e) {
        e.preventDefault();
        
        programEntityIds = [];
        userEntityIds = [];
        $.each($('.share-list-cb:checkbox:checked'), function(i, v) {
            if ($(v).attr('data-entity') == 'user') {
                userEntityIds.push($(v).attr('data-id'));
            } else {
                programEntityIds.push($(v).attr('data-id'));
            }
        });

        if (programEntityIds.length == 0 && userEntityIds.length == 0) {
            var notif = "<i class='material-icons orange-text'>warning</i> Select at least 1 user or program";
            Materialize.toast(notif, 5000);
        } else {
            var totalCount = programEntityIds.length + userEntityIds.length;
            $('#revoke-user-count').html(totalCount);
            $('.btn').addClass('disabled');
            $('#discard-panel').show();
        }
    });

    $('.share-list-cb').on('click',function(e){
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $(this).parent("td").parent("tr").addClass("warning");
            var isAllChecked = 0;
            $(".share-list-cb").each(function() {
                if (!this.checked)
                    isAllChecked = 1;
            });
            if (isAllChecked == 0) {
                $("#share-list-cb-all").prop("checked", true);
            }
        }else{
            $("#share-list-cb-all").prop("checked", false);
            $(this).removeAttr('checked');
            $(this).parent("td").parent("tr").removeClass("warning");
        }
    });

    $("#share-list-cb-all").change(function() {
        if (this.checked) {
            $(".share-list-cb").each(function() {
                this.checked=true;
            });
            $(".share-list-cb:checkbox").parent("td").parent("tr").addClass("warning");
        } else {
            $(".share-list-cb").each(function() {
                this.checked=false;
            });
            $("input:checkbox.share-list-cb").parent("td").parent("tr").removeClass("warning");  
        }
    });
JS
);