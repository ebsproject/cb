<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for create page of list
**/

use app\components\FavoritesWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\builder\Form;
use kartik\tabs\TabsX;
use yii\helpers\Url;

?>

<?php 

    if($showTabs){
        echo Yii::$app->controller->renderPartial('tabs', [
            'active' => 'basic',
            'id' => $id,
            'program' => $program,
            'showTabs' => $showTabs,
            'abbrev' => $abbrev ==='' ? 'Create' : $abbrev,
            'model' => null
        ]);
    }

    echo Yii::$app->controller->renderPartial('basicinfo-content', [
        'name' => $name,
        'abbrev' => $abbrev,
        'display_name' => $display_name,
        'type' => $type,
        'subType' => $subtype,
        'description' => $description,
        'remarks' => $remarks,
        'create' => $create,
        'id' => $id,
        'program' => $program
    ]);


    if (!empty($id)) {
        $createUrl = Url::to(['list/create', 'program' => $program, 'id' => $id]);
        $itemsUrl = Url::to(['list/add-items', 'program' => $program]);
        $reviewUrl = Url::to(['list/review', 'program' => $program]);
    } else {
        $createUrl = Url::to(['list/create', 'program' => $program]);
        $itemsUrl = Url::to(['list/add-items', 'program' => $program]);
        $reviewUrl = Url::to(['list/review', 'program' => $program]);
    }
?>

<?php
$this->registerJs(<<<JS
    var list_id = "$id";

    $('#name-fld').on('input', function(){
        $('#abbrev-fld').val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());
        $('#display-fld').val(this.value);
        Materialize.updateTextFields();
    });

    $('.basic-info-nxt').on('click', function(){
        var isValid = true;
        var createUrl = "$createUrl";
        var type = "$type";
        var subtype = "$subtype";
        var nextUrl = "$reviewUrl";

        if (type === '') {
            type = $('#type-fld').val();
        }

        if (subtype === '') {
            subtype = $('#sub-type-fld').val();
        }

        if (type == 'trait' || type == 'germplasm') {
            nextUrl = "$itemsUrl";
        }
        
        $.each($('.text-field-var[required]'), function(i, v){

            if ($(this).val() === '' || $(this).val() === null) {
                var notif = "<i class='material-icons red-text'>close</i> " + $(this).attr('data-name') + " field is required!";
                Materialize.toast(notif, 5000);

                if (this.id === 'type-fld') {

                    $(this).select2('open');

                } else {
                    $(this).focus();
                }
                isValid = false;
                return false;
            }
        });

        if (isValid) {

            if (isValid) {

                var form_data = $("#basic-info-form").serialize();
                var itemsUrl = "$itemsUrl";

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: createUrl,
                    data: form_data,
                    success: function(data) {
                        if (data.success == true) {
                            window.location = nextUrl + '&id=' + data.list_id;
                        } else {
                            // location.reload();
                            var notif = "<i class='material-icons red-text'>close</i> " + data.error;
                            Materialize.toast(notif, 5000);
                        }
                    }
                });
            }
        }
    });
JS
);