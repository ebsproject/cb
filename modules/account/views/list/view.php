<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for viewing a list and its items
**/

?>


<?php 
$this->registerCss('
	[type="checkbox"]:not(:checked), 
	[type="checkbox"]:checked {
		opacity: 0;
	}
	label{
		margin-bottom: 0px;

	}
	kv-panel-before {
		padding: 5px;
	}

	.btn, .btn-large, .btn-flat {
		height: 28px;
		line-height: 28px;
	}
	input[type=text]:not(.browser-default){
		height: 34px;
	}
	');

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\bootstrap\Button;

use app\modules\account\models\ListModel;
use app\modules\account\models\ListMemberModel;

use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;

$params=Yii::$app->request->queryParams;
$itemGridSelectAllId = 'final-grid-select-all-id';
$itemGridSelectId = 'final-grid_select';
$modelType = isset($model->type) && !empty($model->type) ? $model->type : '';

$dataProvider = $searchModel->search(null, $id, null, $params, true, true);
if ($modelType != '' && $modelType == 'plot') {
	$columns = ListMemberModel::getListMemberColumns(
		'plot',
		$columnVariables,
		$model,
		[
			'checkBoxId' => $itemGridSelectAllId,
			'checkBoxClass' => $itemGridSelectId
		],
		[
			'isPreview' => true,
			'showOrderNumber' => true,
			'isFinalList' => false
		],
		'view'
	);
} else if ($modelType != '' && $modelType == 'seed') {
	$columns = ListMemberModel::getListMemberColumns(
		'seed',
		$columnVariables,
		$model,
		[
			'checkBoxId' => $itemGridSelectAllId,
			'checkBoxClass' => $itemGridSelectId
		],
		[
			'isPreview' => true,
			'showOrderNumber' => true,
			'isFinalList' => false
		],
		'view'
	);
} else if ($modelType != '' && $modelType == 'germplasm') {
	$columns = ListMemberModel::getListMemberColumns(
		'germplasm',
		$columnVariables,
		$model,
		[
			'checkBoxId' => $itemGridSelectAllId,
			'checkBoxClass' => $itemGridSelectId
		],
		[
			'isPreview' => true,
			'showOrderNumber' => true,
			'isFinalList' => false
		],
		'view'
	);
} else if ($modelType != '' && $modelType == 'location') {
	$columns = ListMemberModel::getLocationMemberColumns($model, $itemGridSelectAllId, $itemGridSelectId, true, true, false);
} else if ($modelType != '' && $modelType == 'study') {
	$columns = ListMemberModel::getStudyMemberColumns($model, $itemGridSelectAllId, $itemGridSelectId, true, true, false);
} else if ($modelType != '' && $modelType == 'trait') {
	$columns = ListMemberModel::getListMemberColumns(
		'trait',
		$columnVariables,
		$model,
		[
			'checkBoxId' => $itemGridSelectAllId,
			'checkBoxClass' => $itemGridSelectId
		],
		[
			'isPreview' => true,
			'showOrderNumber' => true,
			'isFinalList' => false
		],
		'view'
	);
} else if ($modelType != '' && $modelType == 'package') {
	$columns = ListMemberModel::getListMemberColumns(
		'package',
		$columnVariables,
		$model,
		[
			'checkBoxId' => $itemGridSelectAllId,
			'checkBoxClass' => $itemGridSelectId
		],
		[
			'isPreview' => true,
			'showOrderNumber' => true,
			'isFinalList' => false
		],
		'view'
	);
} else {
	$dataProvider = $searchModel->search(null, $id);
	$columns = [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'displayValue',
				'visible' => true,
				'format' => 'raw'
			],
			'orderNumber',
			[	
				'attribute'=>'creationTimestamp',
				'format' => 'date',
				'visible' => false,
				
				'filterType' => GridView::FILTER_DATE,
				'filterWidgetOptions' => [

					'pluginOptions'=> [
						'format' => 'yyyy-mm-dd',
						'autoclose' => true,
						'type'=>DatePicker::TYPE_COMPONENT_APPEND,
						'removeButton'=>true,
						'todayHighlight' => true,
						'showButtonPanel'=>true, 

						'clearBtn' => true,
						'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
						'removeButton'=>true,
					]
				],
			],	
		]
	];
}

if (!empty($dataProvider) && ($dataProvider instanceof app\dataproviders\ArrayDataProvider)) {
	$dataProvider->setPagination(false);
}

$itemCount = count($dataProvider->allModels);
$totalItemCount = $dataProvider->totalCount;
$countSummary = $itemCount == $totalItemCount ? 'all' : $itemCount.' of the '.$totalItemCount;

$gridId = 'dynagrid-list-manager-preview-grid';

$dynaGrid = DynaGrid::begin([
	'columns' => $columns,
	'theme' => 'panel-default',
	'showPersonalize' => true,
	'storage' => 'cookie',
	'showFilter' => false,
	'showSort' => false,
	'allowFilterSetting' => false,
	'allowSortSetting' => false,
	'gridOptions' => [
		'id' => $gridId,
		'dataProvider' => $dataProvider,
		'filterModel'=>false,
		'showPageSummary'=>false,
		'hover' => true,
		'pjax'=>true,
		'pjaxSettings'=>[
			'neverTimeout'=>true,
			'options'=>[
				'id'=>$gridId
			],
			'beforeGrid'=>'',
			'afterGrid'=>''
		],
		'responsiveWrap'=>false,
		'panel'=>[
			'heading'=>false,
			'before' => \Yii::t('app', 'This data browser displays '.$countSummary.' items in the list.'),
			'beforeOptions' => [
				'style' => 'height:50px'
			],
			'after' => false,
			'footer'=>false
		],
		'toolbar' =>  '',
	],
	'options' => [
		'id' => $gridId
		]
]);


DynaGrid::end();

$this->registerJs(<<<JS
    $('.collapsible').collapsible();
JS);