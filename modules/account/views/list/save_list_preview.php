<?php

/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* Preview list of variables
**/

use kartik\grid\GridView;
use yii\helpers\Html;


// columns

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
    	'attribute'=>'designation',
    	'enableSorting' => false
    ],
    [
        'attribute'=>'product_type',
        'enableSorting' => false,
    ],
    [
        'attribute'=>'generation',
        'enableSorting' => false,
    ],
    [
        'attribute'=>'parentage',
        'enableSorting' => false,
    ]
];

echo $grid = GridView::widget([
    'pjax' => true,
    'dataProvider' => $dataProvider,
    'id' => 'entry-list-save-list-grid',
    'columns' => $columns,
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
]);

?>

<style type="text/css">
.table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #fff;
}
</style>
