<?php

/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for plot fields form
**/

use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\helpers\Html;

$strDisplay = "No available plots for the studies selected.";

$studyDisplayStr = '';
$count = 0;


foreach ($result as $res) {

    $count++;

    $labelClass = ($res['count'] > 0 ) ? 'success' : 'warning';

    $inputListContent = '<div style="margin-bottom:5%">' .

        '<div class="col col-md-7 legend-container"><b>Search by:</b>'
        . Select2::widget([
                'name' => 'input_type',
                'data' => [
                    'designation' => 'Designation',
                    'entry_code' => 'Entry Code',
                    'plot_code' => 'Plot Code'
                ],
                'size' => Select2::SMALL,
                'value' => 'designation',
                'options' => [
                    'placeholder' => '',
                    'id' => 'listmgt-plot-input-'.$res['id'],
                ],

            ], true) 

        . '</div><div class="col col-md-5">'

        .\macgyer\yii2materializecss\widgets\Button::widget([
            'label' => Yii::t('app','Filter plots'),
            'icon' => [
                'name' => 'check',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'green pull-right listmgt-filter-plot',
                'data-studyId' => $res['id']
            ]
        ])

        . '</div></div>'

        . '<br/><div class="row"> '

        . '<div id="loading-area-'.$res['id'].'"></div>'
        . '<textarea name="input_list" class="form-control input-filter variable-filter" 
            id="input-list-fld-'.$res['id'].'" rows="20" spellcheck="false" 
            style="position: relative; background-color: white; line-height: 20px;"></textarea>'
        . '</div>'
    ;


    $studyDisplayStr .= '
        <li>
          <div class="collapsible-header filter-plot">'
          .'<div class="col col-md-9">'.$count.' <b>'.$res['name'].' ('.$res['study'].')</b></div>
          <div class="col col-md-3"><span>Selected: <span class="label label-'.$labelClass.'" id="'.$res['id'].'-plot-count">'.$res['count'].'</span></span>
          <a href="#"><i class="material-icons pull-right">filter_list</i></a>
          </div>
          </div>
          <div class="collapsible-body">
              '.$inputListContent.'
          </div>
        </li>
    ';

} 

if (!empty($studyDisplayStr)) {

    $strDisplay = '
        <span class="instruction">You have selected <b> '.$totalStudiesCount.' </b> studies. You may filter the plots further by clicking the filter icon <i class="material-icons"  style="font-size: 15px">filter_list</i> Input either designation, entry codes, or plot codes separated by comma or new line.</span>
        <ul class="listmgt-plot-collapsible collapsible">' . $studyDisplayStr . '</ul>';

} 

$response = $strDisplay;

echo $response;


$validateUrl = Url::to(['list/validate-list', 'program' => $program, 'id' => $id, 'listType' => $listType]);
$addPlotListUrl = Url::to(['list/add-plot-list', 'id' => $id]);
$previewUrl = Url::to(['list/review', 'program' => $program, 'id' => $id]);

?>

<script type="text/javascript">
    
    $(document).ready(function(){

        var validateUrl = '<?php echo $validateUrl; ?>';
        var addPlotListUrl = '<?php echo $addPlotListUrl; ?>';
        var previewUrl = '<?php echo $previewUrl;?>';
        var selectedStudies = '<?php echo json_encode($studyIds);?>';

        var selectedData = {};

        $('.filter-plot').on('click', function(){

            var activeClass = $(this).hasClass('active');

            if (activeClass) {
                $('#listmgt-save-plots').removeClass('disabled');
            } else {
                $('#listmgt-save-plots').addClass('disabled');
            }
        });

        $('.listmgt-filter-plot').on('click', function(e){
            e.preventDefault();
            var studyId = $(this).attr('data-studyId');
            var inputType = $('#listmgt-plot-input-'+studyId).val();
            var inputList = $('#input-list-fld-' + studyId).val();
            $('#loading-area-'+studyId).html('<div class="progress"><div class="indeterminate"></div></div>');

            if (inputList !== '' && inputList !== null && inputList.length !== 0) {

                Materialize.toast("Validating list...", 1000, 'blue');
                setTimeout(function(){
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: validateUrl,
                        data: {inputType: inputType, inputList: inputList, studyId: studyId},
                        success: function(data) {

                            if (!data.valid) {
                                Materialize.toast(data.message, 5000, 'orange');
                                $('#listmgt-save-plots').addClass('disabled');
                            } else {
                                Materialize.toast(data.message, 2000, 'green');
                            }

                            if (data.validCount > 0) {
                                $('#'+studyId+'-plot-count').html(data.validCount);

                                selectedData[studyId] = data.plotIds;

                                $('#listmgt-save-plots').removeClass('disabled');
                                $(".collapsible-header").removeClass(function(){
                                   return "active";
                                  });
                                $(".collapsible").collapsible({accordion: true});
                                $(".collapsible").collapsible({accordion: false});
                            }
                            $('#loading-area-'+studyId).html('');
                        }
                    })
                }, 1000);
            } else {
                Materialize.toast("Input list is empty", 5000, 'red');
            }
        });

        $('#listmgt-save-plots').on('click', function() {
            $('#listmgt-preview-footer').html('');
            $('#listmgt-preview-body').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
            setTimeout(function(){
                $.ajax({
                    url: addPlotListUrl,
                    type: 'POST',
                    dataType: 'json',
                    data: {selectedData: selectedData, selectedStudies: selectedStudies},
                    async: false,
                    success: function(data) {
                        var totalCount = data.totalCount;
                        var content = "<div style='text-align:center'><p style='color:green'><i class='material-icons'>check</i><b>Successfully added "+totalCount+" plot/s to list!</b></p>" +
                            '<a href="#" data-dismiss="modal" style="margin: 10px 15px 0 0; font-size: 15px; text-decoration: underline;">Add more</a>' +
                            '<a href="'+previewUrl+'" class="btn-succes btn">Done</a></div>'
                        ;
                        $('#listmgt-preview-body').html(content);
                    }
                });
            }, 1000)
        });
    });


</script>