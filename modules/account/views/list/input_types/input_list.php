<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for input list option
**/

use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\modules\account\models\ListModel;


$traitsUrl = Url::to(['/traits/default', 'program' => $program]);
$listType = isset($model->type) && !empty($model->type) ? $model->type : '';
$title = 'Germplasm';
$instructionLabel = 'germplasm';
$entityAttribute = 'name/s';
$linkToTraits = '';
if ($listType == 'trait') {
    $instructionLabel = 'trait/s';
    $title = 'Traits';
    $entityAttribute = 'label/s';
    $linkToTraits = ' You can view the <a class="btn" style="margin-bottom:5px;" href="'.$traitsUrl.'" target="_blank">Traits browser</a> to view all the traits in the system.';
} 

$inputListContent = 
    '<br/><div class="row"> '
        .'<div>Input the '.$entityAttribute.' of the '.$instructionLabel.' that you want to add to the list. Separate each value with a comma or a new line. '.$linkToTraits.' Maximum of <strong>500 records</strong> per validation.</div>'
		. Html::textarea('input_list', '', [
			'id' => 'input_list_fld',
			'class' => 'materialize-textarea',
			'style' => [
				'height' => '400px',
				'border'=> '1px solid #ddd',
				'padding'=> '8px',
				'width'=> '98%',
				'margin-bottom'=> '5px',
				'border-radius'=> '5px'
			]
		])

	. '</div>
';

echo $inputListContent;

Modal::begin([
	'id' => 'listmgt-designation-modal',
	'header' => '<h4><i class="material-icons">add</i> Add '.$title.'</h4>',
	'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
		.Html::a('Continue',
			'#',
			[
				'class' => 'btn btn-primary waves-effect waves-light',
				'url' => '#',
				'id' => 'listmgt-desgcont-btn'
			]
		) . '&emsp;',
	'footerOptions' => ['id' => 'designation-modal-footer'],
	'bodyOptions' => ['id' => 'designation-modal-body', 'class' => 'modal-body'],
	'size' => 'modal-lg',
	'options' => ['data-backdrop' => 'static']
]);

echo '<div id="listmgt-designation-alert"></div>';
Modal::end();
?>

<style type="text/css">
	
.verticalLine {
  border-left: thick solid #ff0000;
}
</style>