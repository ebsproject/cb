<?php

/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for uploading file
**/

use kartik\widgets\FileInput;
use yii\helpers\Url;
use yii\helpers\Html;

yii\bootstrap\BootstrapPluginAsset::register($this);
$this->registerCss('
	.kv-fileinput-caption .file-caption-name{ 
		height:unset;
	}
	');
echo '<div>';
echo FileInput::widget([
	'name' => 'list_file',
	'options'=>[
		'multiple'=>false
	],

	'pluginOptions' => [
		'uploadAsync'=>false,
		'uploadUrl' => Url::to(['/account/list/get-upload']),
		'allowedFileExtensions' => ['csv'],
		'browseClass' => 'btn waves-effect waves-light',
		'uploadClass' => 'btn',
		'removeClass' => 'btn btn-danger',
		'cancelClass' => 'btn grey lighten-2 black-text',
		'dropZoneTitle' =>'Drag & drop file here ...',
		'elCaptionContainer'=>'file-caption-name-custom',

	],
	'options' => ['accept' => 'csv/*','id'=>'file-input-id']
]);
echo '</div>';
?>
<div class="modal fade map-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Upload CSV File</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="box">
					<div class="box-body no-padding">
						<p>Select column header to map in the file</p>
						<div class="col-md-10" style="position: relative; z-index: 999;padding-top: 20px;">
							<div class="form-group required">
								<label class="col-sm-4 control-label" for="<?= $model->type ?>"><?= ucfirst($model->type) ?></label>
								<div class="col-sm-8">
									<?php echo Html::dropDownList( $model->type, $selection = null, [], ['class' => 'form-control',"id"=>'dropdownlist-id']); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="upload-confirm-id" type="button" class="btn btn-primary">Confirm</button>
				<button type="button" class="btn grey" data-dismiss="modal">Cancel Upload</button>
			</div>
		</div>
	</div>
</div>

<?php 
	$getListUrl = Url::to(['list/get-input-list']);
	$addMemberUrlByInput = Url::to(['list/add-members-by-input', 'program' => $program, 'id' => $model->id, 'list_type' => $model->type]);
	$validateUrl = Url::to(['list/validate-list', 'program' => $program, 'id' => $model->id, 'list_type' => $model->type]);
 ?>

<script type="text/javascript">
	$( document ).ready(function() {
		
		var temp_table;
		var inputList;

		$('#file-input-id').on('filebatchuploadsuccess', function(event, data) {

			var form = data.form, files = data.files, extra = data.extra,
			response = data.response, reader = data.reader;

			temp_table= response.temporary_table;
			$('.map-modal').modal('show');

			$.each(response.headers, function() {
				$("#dropdownlist-id").append($("<option />").val(this).text(this));
			});

		});

		$('#upload-confirm-id').on('click', function(event) {
			
			var inputType = '<?php echo $model->type; ?>';
			var csvColumn = $("#dropdownlist-id").val();
			var getInputList = ' <?php echo $getListUrl; ?>';
			var addMemberUrlByInput = '<?php echo $addMemberUrlByInput; ?>';
			var validateUrl = '<?php echo $validateUrl; ?>';

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: getInputList,
				data: {csvColumn: csvColumn, temp_table: temp_table},
				async: false,
				success: function(data) {
					inputList=data;
				}
			});

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: validateUrl,
				data: {inputType: inputType, inputList: inputList},
				async: false,
				success: function(data) {
					if (data.valid == false) {

						Materialize.toast(data.message, 10000, 'red');

					} else {

						Materialize.toast(data.message, 2000, 'green');

						window.location = addMemberUrlByInput;

					}

				}
			});
		});
		
	});

</script>