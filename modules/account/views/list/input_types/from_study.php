<?php
/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* View file for from study option
**/

use app\components\FavoritesWidget;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;


$this->registerCss('
    [type="checkbox"]:not(:checked), 
    [type="checkbox"]:checked {
        opacity: 0;
    }
    .instruction{
        display: block;
        margin: 5px 3px;
        padding: 4px 6px;
        border: 1px dotted #ddd;
        background: rgba(27, 101, 59, 0.17);
        border-radius: 1px;
        line-height: 1.25em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    .instruction-error{
        display: block;
        margin: 5px 3px;
        padding: 4px 6px;
        border: 1px dotted #ddd;
        background: rgba(255, 0, 0, 0.17);
        border-radius: 1px;
        line-height: 1.25em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
');

$dynaGrid = DynaGrid::begin([
    'columns' => [
        [
          'label'=> "checkbox",
          'visible'=>true,
          'header' => '<span style="display:none"></span>',
          'header'=>'
              <input type="checkbox" data-id-string="" id="study-select-all-id" />
              <label for="study-select-all-id"></label>
          ',
          'contentOptions'=>['class'=>''],
          'content'=>function($data) {
              return '
              		<input class="from_study_select" type="checkbox" id="'.$data["id"].'" />
              		<label for="'.$data["id"].'"></label>
              ';
          },
          'hAlign'=>'center',
          'vAlign'=>'middle',
          'hiddenFromExport'=>true,
          'mergeHeader'=>true,
          'width'=>'50px'
        ],
        [
            'attribute' => 'study',
            'visible' => true,
            'format' => 'raw'
        ],
        [
            'attribute' => 'study_name',
            'label' => 'Name',
            'visible' => true,
            'value' => function($data) {

                return empty($data['study_name']) ? '' : $data['study_name'];

            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'place',
            'visible' => true,
            'header' => 'Location',
            'format' => 'raw'
        ],
        [
            'attribute' => 'phase',
            'visible' => true,
            'format' => 'raw'
        ],
        [
            'attribute' => 'year',
            'visible' => true,
            'format' => 'raw'
        ],
        [
            'attribute' => 'season',
            'visible' => true,
            'format' => 'raw'
        ],
        [
            'attribute' => 'entry_count',
            'visible' => true,
            'format' => 'raw'
        ],
        [
            'attribute' => 'plot_count',
            'visible' => true,
            'format' => 'raw'
        ]
    ],
    'theme' => 'panel-default',
    'showPersonalize' => true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => 'grid2',
        'dataProvider' => $studyDataProvider,
        'filterModel' => $searchModel,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>['id'=>'from_study_grid'],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            'before' => \Yii::t('app', 'Select from the studies below to add members to your list.'),
            'beforeOptions' => [
                'style' => 'height:50px'
            ],
            'after' => false,
        ],
        'toolbar' =>  [
            [
                'content'=> Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['add-items','program'=>$program,'id'=>$id], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid')]).'{dynagridFilter}{dynagridSort}{dynagrid}',
            ],
        ]
    ],
    'options' => ['id' => 'from_study_grid']
]);


DynaGrid::end();


// define modal

Modal::begin([
    'id' => 'listmgt-preview-modal',
    'header' => '<h4>Select data from study</h4>',
    'footer' => Html::a('Cancel', ['#'], ['id'=>'cancel-save', 'data-dismiss'=>'modal']) . '&emsp;&emsp;'
        .Html::a('Confirm',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light', 
                'url' => '#',
                'id' => 'listmgt-save-from-study'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static'],
    'headerOptions' => ['id' => 'listmgt-preview-header'],
    'footerOptions' => ['id' => 'listmgt-preview-footer'],
    'bodyOptions' => ['id' => 'listmgt-preview-body', 'class' => 'modal-body']
]);


    echo '<div id="div-data_type">'. 
    '<span id="data_type_label">Select if you want to add the <b>entries</b> or the <b>harvested products</b> of the studies to your list</span>' .
    Select2::widget([
        'name' => 'study_data_type',
        'data' => [
            'copy' => 'Entries',
            'harvest' => 'Harvested Products'
        ],
        'size' => Select2::SMALL,
        'value' => 'copy',
        'options' => [
            'placeholder' => '',
            'id' => 'data_type_fld',
        ],

    ], true) . '</div><br/>';

    echo '<div id="box-data_type">
        <div id="loading-data_type"></div>
        <div id="summary-data_type"></div>
    </div>';

Modal::end();

$this->registerJS(<<<JS
    studyGridEvent();

    $(document).on('ready pjax:success', function(e){
        e.preventDefault();
        studyGridEvent();
    });

    function studyGridEvent() {

        $("#from_study_grid tbody tr").on('click',function(e){

            this_row=$(this).find('input:checkbox')[0];
            if(this_row.checked){
            this_row.checked=false;  
            $(this).removeClass("success");
            }else{
            $(this).addClass("success");
            this_row.checked=true;  
            $("#"+this_row.id).prop("checked");  
            }

        });

        $('.from_study_select').on('click',function(e){

            if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $(this).parent("td").parent("tr").addClass("success");
            }else{
            $(this).removeAttr('checked');
            $(this).parent("td").parent("tr").removeClass("success");
            }

        });

        $('#study-select-all-id').on('click',function(e){

            if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.from_study_select').attr('checked','checked');
            $('.from_study_select').prop('checked',true);
            $(".from_study_select:checkbox").parent("td").parent("tr").addClass("success");
            
            }else{

            $(this).removeAttr('checked');
            $('.from_study_select').prop('checked',false);
            $('.from_study_select').removeAttr('checked');
            $("input:checkbox.from_study_select").parent("td").parent("tr").removeClass("success");      
            }
        });
    }
JS
);