<?php

/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for input designation options
**/

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php
    if ($listType == 'germplasm'){
    
?>
<p class="instruction"><?= \Yii::t('app', 'Highlighted rows means that your inputted product name is different from the standard designation.
You can remove the <b> duplicate</b> germplasm in the <b>Manage items</b> step.') ?></p>

<?php
} else {
?>

<p class="instruction"><?= \Yii::t('app', 'Below are the valid and invalid trait labels from your input list.') ?></p>


<?php } ?>
<br/>
<div style="height:25px !important" class="row">
    <div class="col col-sm-6 view-entity-content">
        <h4>
            Valid <?php echo $inputType; ?>
        </h4>
    </div>
</div>

<div class="row">
<?php
    if ($listType == 'germplasm') {
        $columns = [
            ['class' => 'yii\grid\SerialColumn'], 
            [
                'attribute'=>'inputProductName',
                'label' => Yii::t('app', 'Input Germplasm'),
                'format' => 'raw',
                'enableSorting' => false,
                'value' => function($data){
                    return $data['inputProductName'];
                },
                'contentOptions' => function ($model, $key, $index, $column) {
                    if(isset($model['designation']) && isset($model['inputProductName']) && $model['designation'] != $model['inputProductName']){
                        return ['style' => 'background-color:salmon'];
                    }
                },
            ],
            [
                'attribute'=>'designation',
                'label' => Yii::t('app', 'Standard Germplasm'),
                'format' => 'raw',
                'enableSorting' => false,
                'value' => function($data){
                    return $data['designation'];
                },
                'contentOptions' => function ($model, $key, $index, $column) {
                    if(isset($model['designation']) && isset($model['inputProductName']) && $model['designation'] != $model['inputProductName']){
                        return ['style' => 'background-color:#4CAF50'];
                    }
                },
            ]
        ];

    } else {

        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'label',
                'label' => Yii::t('app', 'Label'),
                'format' => 'raw',
                'value' => function($data){
                    return $data['label'];
                }
            ],
            [
                'attribute'=>'abbrev',
                'label' => Yii::t('app', 'Abbrev'),
                'format' => 'raw',
            ],
            [
                'attribute'=>'name',
                'label' => Yii::t('app', 'Name'),
                'format' => 'raw',
            ],
            [
                'attribute'=>'dataType',
                'label' => Yii::t('app', 'Data Type'),
                'format' => 'raw',
            ]
        ];
    }

    echo $grid = GridView::widget([
        'pjax' => true, // pjax is set to always true for this demo
        'dataProvider' => $dataProvider,
        'id' => 'copy-experiment',
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
        'toggleData'=>true,
        'columns'=> $columns,
    ]);

    ?>
</div>

<div class="row">

    Invalid <?php echo $inputType; ?>:

    <?php
        $values = implode(", ", $invalidItems);
    ?>

    <?=Html::textArea('invalid_products',$values,[
        'id' => 'listmgt-designation-txtarea',
        'readonly' => true,
        'style'=>'background-color:white; resize: none; min-height: 30px !important; '
    ]);?>
</div>
<?php 
    $addMemberUrlByInput = Url::to(['list/add-members-by-input', 'program' => $program, 'id' => $listDbId, 'listType' => $listType]);
    $previewUrl = Url::to(['list/review', 'program' => $program, 'id' => $listDbId]);
?>
<script>
    $(document).ready(function() {
        var isClicked = false;
        $('#listmgt-desgcont-btn').on('click', function(e) {
            if (!isClicked) {
                e.preventDefault();
                var addMemberUrlByInput = '<?php echo $addMemberUrlByInput; ?>';
                var previewUrl = '<?php echo $previewUrl; ?>';
                $.ajax({
                    url: addMemberUrlByInput,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        productList: '<?php echo str_replace("'", "\'", json_encode($validItems))?>'
                    },
                    async: false,
                    success: function(data) {
                        
                        e.preventDefault();
                        e.stopImmediatePropagation();
                        var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>"+data.message+"</span>";
                        Materialize.toast(notif, 3000);
                        setTimeout(function(){
                            window.location = previewUrl;
                        }, 3000);
                    }
                });
                isClicked = true;
            }
        });
    });
</script>