<?php

/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for location list
**/

use app\models\Place;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Modal;

$this->registerCss('
    [type="checkbox"]:not(:checked), 
    [type="checkbox"]:checked {
        opacity: 0;
    }
    .instruction{
        display: block;
        margin: 5px 3px;
        padding: 4px 6px;
        border: 1px dotted #ddd;
        background: rgba(27, 101, 59, 0.17);
        border-radius: 1px;
        line-height: 1.25em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    .instruction-error{
        display: block;
        margin: 5px 3px;
        padding: 4px 6px;
        border: 1px dotted #ddd;
        background: rgba(255, 0, 0, 0.17);
        border-radius: 1px;
        line-height: 1.25em;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
');


// export configuration
$export = ExportMenu::widget([
    'dataProvider' => $locationDataProvider,
    'columns' => [
        [
            'attribute' => 'abbrev',
            'visible' => true,
            'format' => 'raw'
        ],
        [
            'attribute' => 'name',
            'visible' => true,
            'format' => 'raw'
        ]
    ],
    'fontAwesome' => true,
    'exportConfig' => [
        ExportMenu::FORMAT_PDF => false
    ],
    'showColumnSelector' => false
]);
$placeType=json_decode(\app\modules\account\models\ListLocationSearch::getPlaceTypes(), true);

$dynaGrid = DynaGrid::begin([
'columns' => [
    [
        'label' => 'checkbox',
        'visible' => true,
        'header' => '<span style="display:none"></span>',
        'contentOptions' => ['class' => ''],
        'content' =>  function($data) {
            return '
                <input class="listmgt-location-select" type="checkbox" id="'.$data['id'].'" />
                <label for="'.$data['id'].'"></label>
            ';
        },
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'width' => '50px'
    ],
    [
        'attribute' => 'abbrev',
        'visible' => true,
        'format' => 'raw'
    ],
    [
        'attribute' => 'country',
        'visible' => true,
        'format' => 'raw'
    ],
    [
        'attribute' => 'name',
        'visible' => true,
        'format' => 'raw'
    ],
    [
        'attribute' => 'display_name',
        'visible' => true,
        'format' => 'raw'
    ],
    [
        'attribute' => 'place_type',
        'visible' => true,
        'format' => 'raw',
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>$placeType, 
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true,'id'=>"select-place_type-id"],],
        'filterInputOptions'=>['placeholder'=>'place type','id'=>"select-place_type"],
    ],
    [
        'attribute' => 'latitude',
        'visible' => true,
        'format' => 'raw'
    ],
    [
        'attribute' => 'longitude',
        'visible' => true,
        'format' =>  'raw'
    ],
    [
        'attribute' => 'organization',
        'visible' => true,
        'format' => 'raw'
    ],
    [
        'attribute' => 'remarks',
        'visible' => false,
        'format' => 'raw'
    ],
    [
        'label' => 'Status',
        'visible' => true,
        'header' => 'Status',
        'content' => function($data) {

            return '<span class="badge '. (($data['is_active']) ? ' new">Active' : ' new badge amber darken-2">Draft') . '</span>';

        }
    ]
],
'theme' => 'panel-default',
'showPersonalize' => true,
'storage' => 'cookie',
'showFilter' => false,
'showSort' => false,
'allowFilterSetting' => false,
'allowSortSetting' => false,
'gridOptions' => [
    'id' => 'location-browser',
    'dataProvider' => $locationDataProvider,
    'filterModel' => $searchModel,
    'showPageSummary'=>false,
    'pjax'=>true,
    'pjaxSettings'=>[
        'neverTimeout'=>true,
        'options'=>['id'=>'location-browser'],
        'beforeGrid'=>'',
        'afterGrid'=>''
    ],
    'responsiveWrap'=>false,
    'panel'=>[
        'headingOptions' => [
            'style' => 'height:20px'
        ],
        'before' => \Yii::t('app', 'Select from the locations below to add members to your list.'),
        'beforeOptions' => [
            'style' => 'height:50px'
        ],
        'after' => false,
    ],
    'toolbar' =>  [
        [
            'content'=> Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['add-items','program'=>$program,'id'=>$id], ['class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid')]).'{dynagridFilter}{dynagridSort}{dynagrid}',
        ],
    ]
],
'options' => ['id' => 'location-browser']
]);

DynaGrid::end();


// define modal

Modal::begin([
    'id' => 'listmgt-preview-modal',
    'header' => '<h4>Summary</h4>',
    'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
        .Html::a('Confirm',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light',
                'url' => '#',
                'id' => 'listmgt-save-location'
            ]
        ) . '&emsp;',
    'footerOptions' => ['id' => 'location-modal-footer'],
    'size' => 'modal-sm',
    'options' => ['data-backdrop' => 'static']
]);

echo '<div id="loading-data_type"></div>';
echo '<div id="listmgt-location-alert"></div>';


Modal::end();

$this->registerJs(<<<JS
    locationGridEvent();

    $(document).on('ready pjax:success', function(e){
        e.preventDefault();
        locationGridEvent();
    });

    function locationGridEvent() {
        $("#location-browser tbody tr").on('click',function(e){

            this_row=$(this).find('input:checkbox')[0];
            if(this_row.checked){
                this_row.checked=false;  
                $(this).removeClass("success");
            }else{
                $(this).addClass("success");
                this_row.checked=true;  
                $("#"+this_row.id).prop("checked");  
            }

        });
    }

JS
);