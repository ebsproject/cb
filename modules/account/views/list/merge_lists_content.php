<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file creating the basic info of a list
**/

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\Select2;

$displayBtnTop = empty($id) ? 'none' : '';
$listIds = !isset($listIds) ? '' : $listIds;
$createClass = !isset($createClass) ? '' : $createClass;
$listType = !isset($listType) ? '' : $listType;
$listOptions = !isset($listOptions) ? [] : $listOptions;
$targetList = !isset($targetList) ? '' : $targetList;
$listCount = !isset($listCount) ? '' : $listCount;
$id = !isset($id) ? '' : $id;
$listMemberCount = !isset($listMemberCount) ? '' : $listMemberCount;
$disabled = '';

$mergeThreshold = !empty(Yii::$app->config->getAppThreshold('LISTS', 'mergeLists')) ? Yii::$app->config->getAppThreshold('LISTS', 'mergeLists') : 500;

$required = '<span style="color: red">*</span>';

$targetListOptions = [
	'placeholder' => 'Select target list',
	'id' => 'target-list-fld',
	'class' => ''.($disabled != '' ? '' : 'disabled'),
	'required' => 'required',
	'data-name' => 'targetList'
]; 

$additionalMessage = ' Please note that duplicate items will be removed during the merging process.';
if($listMemberCount > $mergeThreshold){
	$additionalMessage = '';
}
$hiddenMergeOption = '
<div class="input-field col-md-6 s6" id="option-field-div" style="display:none;">' .
Html::input('text', 'MergeLists[option]', '', ['class' => 'merge-text-field-var form-control hidden','id'=>'option-fld', 'required' => 'required', 'data-name'=>'' ]) .
'<label id="label-display" class="control-label" for="option-fld"></label></div>';

$hiddenListIds = '
<div class="input-field col-md-6 s6" id="option-field-div" style="display:none;">' .
Html::input('text', 'MergeLists[ids]', '', ['class' => 'merge-text-field-var form-control hidden','id'=>'ids-fld', 'required' => 'required', 'data-name'=>'' ]) .
'<label id="label-display" class="control-label" for="ids-fld" value="'.$listIds.'"></label></div>';

$basicContent = '<br/><div style="display:'.$displayBtnTop.'" class="row">' .
\macgyer\yii2materializecss\widgets\Button::widget([
	'label' => 'Merge',
	'icon' => [
		'name' => 'content_copy',
		'position' => 'right'
	],
	'options' => [
		'class' => 'btn-success pull-right merge-lists-btn',
	],
]) .
Html::a('Cancel', ['#'], [
    'data-dismiss'=> 'modal',
    'class' => 'pull-right merge-list-cancel-btn',
    'style' => 'margin: 5px 5px 0 0'
])
. '</div>'.

'<div id="merge-info-tab ">'.
    '<div class="row '.$createClass.'">'. 
        '<div class="card-content">'.
            Html::beginForm('', '', ['class'=>'form-horizontal', 'id' => 'merge-lists-form']).
        '<div class="radio-btn-row col-md-12 s12">' .
        '<input class="radio-btn-row merge-option-btn with-gap" type="radio" name="merge-option" checked id="merge-option-1" value=""/>
	    <label for="merge-option-1">Create new list</label>
	
	<div class="row">'.
	'<div class="input-field col-md-6 s6" id="name-field-div">' .
		Html::input('text', 'MergeLists[name]', '', ['class' => 'merge-text-field-var form-control '.$disabled,'id'=>'list-name-fld', 'required' => 'required', 'data-name'=>'Name' ]) .
		'<label id="label-display" class="control-label" for="name-fld">Name'.$required.'</label></div>'.

		'<div class="input-field col-md-6 s6" id="abbrev-field-div">' .
		Html::input('text', 'MergeLists[abbrev]', '', ['class' => 'merge-text-field-var form-control '.$disabled,'id'=>'list-abbrev-fld', 'required' => 'required', 'data-name'=>'Abbrev' ]) .
		'<label id="label-display" class="control-label" for="abbrev-fld">Abbrev'.$required.'</label></div>

		<div class="input-field col-md-6 s6" id="display-name-field-div">' .
		Html::input('text', 'MergeLists[display_name]', '', ['class' => 'merge-text-field-var form-control '.$disabled,'id'=>'display-name-fld', 'required' => 'required', 'data-name'=>'Display Name']) .
		'<label id="label-display" class="control-label" for="display-name-fld">Display Name'.$required.'</label></div>
		
		<div class="input-field col-md-6 s6" id="list-type-field-div">' .
		Html::input('text', 'MergeLists[type]', $listType, ['class' => 'merge-text-field-var form-control ', 'readonly'=>true, 'id'=>'list-type-fld', 'required' => 'required', 'data-name'=>'Type']) .
		'<label id="label-display" class="control-label" for="list-type-fld">Type'.$required.'</label></div>'.
		
		'</div>
	</div>

<div class="radio-btn-row col-md-12 s12">
<input class="radio-btn-row merge-option-btn with-gap" type="radio" name="merge-option" id="merge-option-2" value=""/>
	<label for="merge-option-2">Use existing list</label>'.

	'<div class="input-field col-md-12 s12" id="target-list-field-div">' .
		'<div class="row">' .
			Select2::widget([
				'name' => 'MergeLists[target_list]',
				'data' => $listOptions,
				'value' => $targetList,
				'options' => $targetListOptions,
				'disabled' => $disabled != '' ? false : true,
			]) 
			.'</div>' 
		.'<div>
</div>'.
$hiddenListIds.$hiddenMergeOption.
Html::endForm() . '</div></div>' .
'<div><i class="fa fa-info-circle"></i>&nbsp;&nbsp;You are about to merge <strong>'.$listCount.'</strong>&nbsp;lists with <strong>'.$listMemberCount.'</strong> members in total.'.$additionalMessage.'</div><br />'.

'<div class="row">' .
\macgyer\yii2materializecss\widgets\Button::widget([
	'label' => 'Merge',
	'options' => [
		'class' => 'btn-success pull-right merge-lists-btn',
	],
]) .
Html::a('Cancel', ['#'], [
    'data-dismiss'=> 'modal',
    'class' => 'pull-right merge-list-cancel-btn',
    'style' => 'margin: 5px 5px 0 0'
]);

echo $basicContent;

$browserUrl = Url::to(['list/index']);
$mergeUrl = Url::to(['list/merge-lists']);
$mergeListsBgUrl =  Url::to(['list/merge-lists-bg','program'=>Yii::$app->userprogram->get('abbrev')]);

$this->registerJS(<<<JS
	$('#option-fld').val('new_list');

	if($('#list-type-fld').val() != ""){
		$('#list-type-fld').trigger('change');
	}

    $('.merge-option-btn').on('click', function(e) {
        var id = $(this).attr('id');  
		
		if(id == 'merge-option-1'){
			$('#target-list-fld').attr('disabled',true);
			$('#target-list-fld').trigger('change');

			$('#list-abbrev-fld').attr('disabled',false);
			$('#list-name-fld').attr('disabled',false);
			$('#display-name-fld').attr('disabled',false);

			$('#option-fld').val('new_list');
		}
		
		if(id == 'merge-option-2'){
			
			$('#list-abbrev-fld').attr('disabled',true);
			$('#list-name-fld').attr('disabled',true);
			$('#display-name-fld').attr('disabled',true);

			$('#target-list-fld').attr('disabled',false);
			$('#target-list-fld').trigger('change');

			$('#option-fld').val('existing_list');
		}
    });

	$('.merge-list-cancel-btn').on('click', function(e) {
        var id = "$id";
        if (id !== null && id !== '') {
            e.preventDefault();
            window.location = "$browserUrl";
        }
    });

    $('#list-name-fld').on('input', function(){
        $('#list-abbrev-fld').val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());
        $('#display-name-fld').val(this.value);
        Materialize.updateTextFields();
    });

	$('.merge-lists-btn').on('click', function(e){

		if ($("#merge-option-1").is(':checked') && ($('#list-abbrev-fld').val() == "" || $('#display-name-fld').val() == "" || $('#list-name-fld').val() == "")){
			var message = '<span style="text-align: left;"><i class="material-icons red-text">warning</i>&nbsp;Missing required fields.</span>';
			Materialize.toast(message, 5000);
		}

		if ($("#merge-option-2").is(':checked') && ($("#target-list-fld").val() == "" || $("#target-list-fld").val() == null)){
			var message = '<span style="text-align: left;"><i class="material-icons red-text">warning</i>&nbsp;No list selected.</span>';
			Materialize.toast(message, 5000);
		}

		var count = '$listMemberCount';
		var limit = '$mergeThreshold';

		if(parseInt(count) <= parseInt(limit)){
			mergeLists();
		}
		else{
			var message = '<span style="text-align: left;"><i class="material-icons blue-text">info</i>&nbsp;Merging of lists with '+limit+' or more members'+"'"+' total count will be forwarded to the background process.</span>';
			Materialize.toast(message, 5000);
			mergeListBgProcess();
		}
    });

	function mergeLists(){
		var form_value = $("#merge-lists-form").serializeArray().map(function(elem){
			var name = [elem.name.split("[")[1]].toString().split("]")[0];
			return elem.name != "_method" && elem.name != "_csrf" && elem.value != "" ? {[name]:elem.value} : null;
		});

		var params = form_value.filter(function(el){ return el != null; });
		var option = params.filter(function(el){ if(Object.keys(el) == 'option') { return Object.values(el); }});
		var formComplete = true;
		
		if(Object.values(option[0]) == 'new_list'){
			if($("#list-name-fld").val() == "" || $("#list-abbrev-fld").val() == "" || $("#display-name-fld").val() == "" || $("#list-type-fld").val() == ""){
				formComplete = false;
			}
		}

		if(Object.values(option[0]) == 'existing_list'){
			if($("#target-list-fld").val() == ""){
				formComplete = false;
			}
		}
		
		if(params != [] && formComplete == true){
			setTimeout(function(){
				$('.merge-lists-btn').attr('disabled',true);
                $('.merge-lists-btn').removeClass('disabled').addClass('disabled').trigger('change');
				var hasToast=false;
				$.ajax({
					type: 'POST',
					url: '$mergeUrl',
					dataType: 'json',
					data: {
						params: params,
						ids: '$listIds'
					},
					async: false,
					success: function(data) {
						var message = '';

						if(data.success){
							$(".merge-list-cancel-btn").trigger('click');
							message = '<span style="text-align: left;"><i class="material-icons green-text">check</i>&nbsp;Successfully merged lists.</span>';

							$('.merge-lists-btn').attr('disabled',false);
							$('.merge-lists-btn').removeClass('disabled').trigger('change');

							$.pjax.reload({
								container: '#my-lists-grid-pjax'
							});
                            
                            $('#my-lists-grid').b4rCheckboxColumn("clearSelection");
						}
						else{
							message = '<span style="text-align: left;"><i class="material-icons red-text">close</i>&nbsp;'+data.message+'</span>';
						}
						if (hasToast != true){ Materialize.toast(message, 5000); }
					}
				});	
			}, 500);
		}
	}

	// extract input values from form
	function extractFormValues(){
		var form_value = $("#merge-lists-form").serializeArray().map(function(elem){
			var name = [elem.name.split("[")[1]].toString().split("]")[0];
			return elem.name != "_method" && elem.name != "_csrf" && elem.value != "" ? {[name]:elem.value} : null;
		});

		var params = form_value.filter(function(el){ return el != null; });
		var option = params.filter(function(el){ if(Object.keys(el) == 'option') { return Object.values(el); }});
		var formComplete = true;

		if(Object.values(option[0]) == 'new_list'){
			if($("#list-name-fld").val() == "" || $("#list-abbrev-fld").val() == "" || $("#display-name-fld").val() == "" || $("#list-type-fld").val() == ""){
				formComplete = false;
			}
		}

		if(Object.values(option[0]) == 'existing_list'){
			if($("#target-list-fld").val() == ""){
				formComplete = false;
			}
		}

		return {'params':params, 'formComplete':formComplete };
	}

	// merge lists in the background
	function mergeListBgProcess(){
		formValues = extractFormValues();

		$('#merge-lists-modal').modal('hide');

		var params = formValues['params'];
		var formComplete = formValues['formComplete'];

		if(params != [] && formComplete == true){
			setTimeout(function(){
				$('.merge-lists-btn').attr('disabled',true);
                $('.merge-lists-btn').removeClass('disabled').addClass('disabled').trigger('change');
				var hasToast=false;
				
				$.ajax({
					type: 'POST',
					url: '$mergeListsBgUrl',
					dataType: 'json',
					data: {
						params: params,
						ids: '$listIds'
					},
					async: false,
					success: function(data) {
						var message = '';

						if(!data.success){
							message = '<span style="text-align: left;"><i class="material-icons red-text">close</i>&nbsp;'+data.message+'</span>';
						}
						else{
							message = '<span style="text-align: left;"><i class="material-icons green-text">check</i>&nbsp;'+data.message+'</span>';
						}
						if (hasToast != true){ Materialize.toast(message, 5000); }
					}
				});	
			}, 500);
		}
	}
JS
);