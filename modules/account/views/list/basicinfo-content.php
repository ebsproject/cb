<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file creating the basic info of a list
**/

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\Select2;

$listType = [
	'germplasm' => 'Germplasm',
	'trait' => 'Trait'
];

$subTypes = [
	'default' => [
		'population list'=>'Population List',
		'entry list'=>'Entry List',
		'sample list'=>'Sample List',
		'shipment list'=>'Shipment List'
	],
	'trait' => [],
	'germplasm' => [
		'population list'=>'Population List',
		'shipment list'=>'Shipment List'
	],
	'package' => [
		'entry list'=>'Entry List',
		'sample list'=>'Sample List',
		'shipment list'=>'Shipment List'
	]
];

if(isset($type) && !empty($type) && $type==='plot'){
	$listType['plot'] = 'plot';
}

if (isset($id) && !empty($id)) {
    $listType['package'] = 'Package';
}

$required = '<span style="color: red">*</span>';

$listTypeOptions = [
	'placeholder' => 'Select type',
	'id' => 'type-fld',
	'class' => 'text-field-var',
	'required' => 'required',
	'data-name' => 'Type'
];

$subTypeOptions = [
	'placeholder' => 'Select list sub type',
	'id' => 'sub-type-fld',
	'class' => 'subtype-text-field-var',
	'required' => '',
	'data-name' => 'SubListType',
	'allowClear'=> true,
	'style' => 'margin-top: 10px !important;'
];

$hiddenListType = '';
$hiddenSubType = '';
$displayBtnTop = empty($id) ? 'none' : '';
$btnLabel = empty($id) ? 'Submit' : 'Save';

if (!empty($type)) {
	$listTypeOptions['disabled'] = 'disabled';

	$hiddenListType = '<input name="Basic[type]" type="text" value="'.$type.'" style="display:none"/>';
}

if(!empty($subType)){
	$hiddenSubType = '<input name="Basic[subType]" type="text" value="'.$subType.'" id="hidden-sub-type" style="display:none"/>';
}

$createClass='';
if($create){
	$createClass='';
}else if(!$create){
	$createClass='card';
}

$basicContent = ($displayBtnTop !== 'none' ? '<br/><div style="display:'.$displayBtnTop.'" class="row">' .
\macgyer\yii2materializecss\widgets\Button::widget([
	'label' => $btnLabel,
	'options' => [
		'class' => 'pull-right basic-info-nxt',
	],
]) .
Html::a('Cancel', ['#'], [
    'data-dismiss'=> 'modal',
    'class' => 'pull-right save-list-cancel-btn',
    'style' => 'margin: 5px 10px 0 0'
])
. '</div>' : '') . '&emsp;'.

'<div id="basic-info-tab "><div class="row '.$createClass.'">'. '<div class="card-content">'.
Html::beginForm('', '', ['class'=>'form-horizontal', 'id' => 'basic-info-form']) . '

<div class="input-field col-md-6 s6" id="name-field-div">' . 
Html::input(
	'text', 
	'Basic[name]', 
	$name, 
	[
		'class' => 'text-field-var form-control',
		'id'=>'name-fld', 
		'required' => 'required', 
		'data-name' => 'Name'
	]
) .
'<label id="label-name" class="control-label" for="name-fld" style="padding-top:0px !important;">Name '.$required.'</label></div>'. 


'<div class="input-field col-md-6 s6" id="abbrev-field-div">' . 
Html::input(
	'text', 
	'Basic[abbrev]', 
	$abbrev, 
	[
		'class' => 'text-field-var form-control',
		'id'=>'abbrev-fld', 
		'required' => 'required', 
		'data-name' => 'Abbrev'
	]
) .
'<label id="label-name" class="control-label" for="name-fld" style="padding-top:0px !important;">Abbrev '.$required.'</label></div></br>'. 

'<div class="input-field col-md-6 s6" id="display-field-div">' .
Html::input(
	'text', 
	'Basic[display_name]', 
	$display_name, 
	[
		'class' => 'text-field-var form-control',
		'id'=>'display-fld', 
		'required' => 'required', 
		'data-name'=>'Display Name' 
	]
) .
'<label id="label-display" class="control-label" for="display-fld" style="padding-top:0px !important;">Display Name '.$required.'</label></div>'. 


'<div class="input-field col-md-6 s6" id="desc-field-div">' .
Html::input(
	'text', 
	'Basic[description]', 
	$description, 
	[
		'class' => 'text-field-var form-control',
		'id'=>'desc-fld', 
		'data-name' => 'Description'
	]
) .
'<label id="label-desc" class="control-label" for="desc-fld" style="padding-top:0px !important;">Description</label></div>'. 

'<div class="input-field col-md-6 s6" id="type-field-div">' .
'<div class="row">' .
'<label id="type-desc" class="control-label" for="type-fld" style="top: -35%">Type '.$required.'</label><br/>'.
Select2::widget([
	'name' => 'Basic[type]',
	'data' => $listType,
	'value' => $type,
	'options' => $listTypeOptions,
	'id' => "type-fld"
]) 
.'</div>' . $hiddenListType .
'</div>' .

'<div class="input-field col-md-6 s6" id="sub-type-field-div">' .
'<div class="row">' .
'<label id="list-sub-type-desc" class="control-label" for="sub-type-fld" style="top: -35%">List Sub Type</label><br/>'.
Select2::widget([
	'name' => 'Basic[subType]',
	'data' => in_array($type,['','plot','seed']) ? $subTypes['default'] : $subTypes[$type],
	'value' => $subType,
	'options' => $subTypeOptions,
	'id' => "sub-type-fld"
]) 
.'</div>' . $hiddenSubType .
'</div>' .

'<div class="input-field col-md-6 s6" id="remarks-field-div">' .
Html::input(
	'text', 
	'Basic[remarks]', 
	$remarks, 
	[
		'class' => 'text-field-var form-control',
		'id'=>'remarks-fld', 
		'data-name' => 'Remarks'
	]
) .
'<label id="label-desc" class="control-label" for="remarks-fld" style="padding-top:0px !important;">Remarks</label></div>'.

Html::endForm() . '</div>'.'</div>'.

'<br/><div class="row">' .
\macgyer\yii2materializecss\widgets\Button::widget([
	'label' => $btnLabel,
	'options' => [
		'class' => 'pull-right basic-info-nxt',
	],
]) .
Html::a('Cancel', ['#'], [
    'data-dismiss'=> 'modal',
    'class' => 'pull-right save-list-cancel-btn',
    'style' => 'margin: 5px 10px 0 0'
])
. '</div>'
. '</div>';

echo $basicContent;

$browserUrl = Url::to(['list/index', 'program' => $program]);

$this->registerJS(<<<JS

    $('.save-list-cancel-btn').on('click', function(e) {
        var id = "$id";
        if (id !== null && id !== '') {
            e.preventDefault();
            window.location = "$browserUrl";
        }
    });

	if($('#type-fld').val() != ""){
		$('#type-fld').trigger('change');
		
		var type = $('#type-fld').val();
		setSubTypeData(type);
	}

	$('#type-fld').on('change', function() {
		var type = $('#type-fld').val();
		setSubTypeData(type);

	});

	$('#sub-type-fld').on('change', function() {
		$("#hidden-sub-type").val($(this).val()).trigger('change');
	});

	function setSubTypeData(type){
		var subTypeFldId = "sub-type-fld";
		var subTypes = {
				'default' : [
					{id:'population list',text:'Population List'},
					{id:'entry list',text:'Entry List'},
					{id:'sample list',text:'Sample List'},
					{id:'shipment list',text:'Shipment List'}
				],
				'germplasm' : [
					{id:'population list',text:'Population List'},
					{id:'shipment list',text:'Shipment List'}
				],
				'package' : [
					{id:'entry list',text:'Entry List'},
					{id:'sample list',text:'Sample List'},
					{id:'shipment list',text:'Shipment List'}
				],
				'trait' : []
			};
		
		var dataArr = (type != "" && subTypes[type] != undefined && subTypes[type] != null) ? subTypes[type] : subTypes['default'];
		var dataCount = dataArr.length

		// clear subtype field
		$("#"+subTypeFldId).html('')
		
		// if has corresponding subtype options, load options data
		if(dataCount > 0){
			newOption = new Option('Select list subtype', '', false, false);
			$("#"+subTypeFldId).append(newOption).trigger('change');

			for(var i = 0; i < dataCount; i++) {
				data = dataArr[i];
				newOption = new Option(data.text, data.id, false, false);
				$("#"+subTypeFldId).append(newOption).trigger('change');
			}
		}
	}

JS
);
?>