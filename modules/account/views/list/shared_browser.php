<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for shared lists browser
**/

$searchModel = new \app\models\PlatformList();

$dynaGrid = dynaGrid::begin([
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],
		[
			'class' => 'kartik\grid\ActionColumn',
			'header' => 'Actions',
			'noWrap' => true,
			'template' => '{view}',
			'buttons' => [
				'view' =>  function ($url, $model, $key) use ($program) {

					return Html::a('<i class="material-icons">visibility</i> ',
						['#'],
						[
							'title' => \Yii::t('app', 'View List'),
							'id' => 'view-list-btn',
							'data-toggle' => 'modal',
							'data-target' => '#view-list-modal',
							'data-id' => $model->id
						]
					);

				}

			]
		],
		[
			'attribute' => 'type',
			'visible' => true,
			'format' => 'raw',
			'noWrap' =>  false,
			'filterType' => GridView::FILTER_SELECT2,
			'filter' => ['designation' => 'Designation', 'plot' => 'Plot', 'seed' => 'Seed', 'study' => 'Study'],
			'filterWidgetOptions' => [
				'pluginOptions' => [
					'allowClear' => true,
					'id' => 'select-status-data-browser-id'
				]
			],
			'filterInputOptions' => ['placeholder' => 'type', 'id' => 'select-status-' . $grid_id]
		],
		[
			'attribute' => 'abbrev',
			'visible' => true,
			'format' => 'raw'
		],
		[
			'attribute' => 'name',
			'visible' => true,
			'format' => 'raw'
		],
		[
			'attribute' => 'display_name',
			'visible' => true,
			'format' => 'raw'
		],
		[
			'attribute' => 'description',
			'visible' => true,
			'format' => 'raw'
		],
		[
			'attribute' => 'creator',
			'label' => 'Created by',
			'filter' => 'false',
			'visible' => true,
			'value' => function($model) {
				return $model->creator->display_name;
			}
		]
	],
	'theme' => 'panel-default',
	'showPersonalize' => true,
	'storage' => 'cookie',
	'showFilter' => false,
	'showSort' => false,
	''





]);