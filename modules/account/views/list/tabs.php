<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for tab options for creating lists
**/

use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

use \app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

$this->registerCss('
	.badge-step {
        background-color: #ee6e73;
		display: inline-block;
		font-size: 11.84px;
		font-weight: 700;
		line-height: 14px;
		color: #fff;
		vertical-align: baseline;
		white-space: nowrap;
		text-shadow: 0 -1px 0 rgba(0,0,0,.25);
		-webkit-border-radius: 50%;
	}'
);

if ($active == 'basic') {

	$basicClass = 'active';
	$itemsClass = '';
	$previewClass = '';
	$shareClass = '';

	$reviewClass = '';
	$itemsClass = (empty($id)) ? $itemsClass . ' disabled' : $itemsClass;
	$basicUrl = '#';
	$itemsUrl = (empty($id)) ? '#' : Url::to(['list/add-items', 'program'=>$program, 'id' => $id]);
	$previewUrl = (empty($id)) ? '#' : Url::to(['list/preview', 'program'=>$program, 'id' => $id]);
	$shareUrl = (empty($id)) ? '#' : Url::to(['list/share', 'program'=>$program, 'id' => $id]);
	$reviewUrl = (empty($id)) ? '#' : Url::to(['list/review', 'program'=>$program, 'id' => $id]);

}
else if ($active == 'review'){
	$basicClass = '';
	$itemsClass = '';
	$shareClass = '';
	$previewClass = '';
	$reviewClass = 'active';
	$reviewClass = (empty($id)) ? $reviewClass . ' disabled' : $reviewClass;
	$basicUrl = Url::to(['list/create', 'program' => $program, 'id' => $id]);
	$itemsUrl = (empty($id)) ? '#' : Url::to(['list/add-items', 'program' => $program, 'id' => $id]);
	$shareUrl = (empty($id)) ? '#' : Url::to(['list/share', 'program'=>$program, 'id' => $id]);
	$previewUrl = (empty($id)) ? '#' : Url::to(['list/preview', 'program'=>$program, 'id' => $id]);
	$reviewUrl = '#';
}
else if ($active == 'preview'){
	$basicClass = '';
	$itemsClass = '';
	$shareClass = '';
	$reviewClass = '';
	$previewClass = 'active';
	$previewClass = (empty($id)) ? $previewClass . ' disabled' : $previewClass;
	$basicUrl = Url::to(['list/create', 'program' => $program, 'id' => $id]);
	$itemsUrl = (empty($id)) ? '#' : Url::to(['list/add-items', 'program' => $program, 'id' => $id]);
	$shareUrl = (empty($id)) ? '#' : Url::to(['list/share', 'program'=>$program, 'id' => $id]);
	$reviewUrl = (empty($id)) ? '#' : Url::to(['list/review', 'program'=>$program, 'id' => $id]);

	$previewUrl = '#';
}
else if ($active == 'share'){
	$basicClass = '';
	$itemsClass = '';
	$previewClass = '';
	$reviewClass = '';

	$shareClass = 'active';
	$shareClass = (empty($id)) ? $shareClass . ' disabled' : $shareClass;
	$basicUrl = Url::to(['list/create', 'program' => $program, 'id' => $id]);
	$itemsUrl = (empty($id)) ? '#' : Url::to(['list/add-items', 'program' => $program, 'id' => $id]);
	$previewUrl = (empty($id)) ? '#' : Url::to(['list/preview', 'program'=>$program, 'id' => $id]);
	$reviewUrl = (empty($id)) ? '#' : Url::to(['list/review', 'program'=>$program, 'id' => $id]);

	$shareUrl = '#';
}
else{
	$basicClass = '';
	$itemsClass = 'active a-tab';
	$previewClass = '';
	$shareClass = '';
	$reviewClass = '';

	$itemsClass = (empty($id)) ? $itemsClass . ' disabled' : $itemsClass;
	$basicUrl = Url::to(['list/create', 'program' => $program, 'id' => $id]);
	$itemsUrl = '#';
	$previewUrl = (empty($id)) ? '#' : Url::to(['list/preview', 'program'=>$program, 'id' => $id]);
	$shareUrl = (empty($id)) ? '#' : Url::to(['list/share', 'program'=>$program, 'id' => $id]);
	$reviewUrl = (empty($id)) ? '#' : Url::to(['list/review', 'program'=>$program, 'id' => $id]);
}

$header = "<h3>" .yii\helpers\Html::a('List Manager', Url::to(['/account/list/index', "program"=>$program, ]), 
	[ 
		"title" => "Return to My Lists", 
	]). "<small> » " . $abbrev . " </small> ". 
'</h3>'.'<br>';


if (!isset($showTabs)){
	$showTabs='';
} else if (!$showTabs){
	$showTabs = 'hidden';
	$header = '';
} else{
	$showTabs='';	
}
?>

<?php 
    echo $header;
    $addItemsTab = '';
    $lastTabNo = '1';
    if (isset($id) && !empty($id)) {
		$model = Yii::$app->controller->checkListId($id, $program);
		$modelType = isset($model->type) && !empty($model->type) ? $model->type : '';
		if (!empty($model)) {
            if (in_array($modelType, ['trait', 'germplasm'])) {
                $addItemsTab = '
                    <li class="tab col s3 '. $itemsClass .'" id="item-li">
                        <a href="'.$itemsUrl.'" class="'.$itemsClass.' a-tab" id="item-tab">
                        <span class="badge-step" style="padding:4px 7px;">2</span></span>&nbsp;Add Items</a>
                    </li>
                ';
                
                $lastTabNo = '2';
            }
        }
    }
?>
<ul class="tabs card row <?php echo $showTabs; ?>">

	<li class="tab col s3">
		<a class="<?php echo $basicClass;?> a-tab" href="#" id="basic-tab">
        <span class="badge-step" style="padding:4px 7px;">1</span></span>&nbsp;Basic Info</a>
    </li>
    <?php echo $addItemsTab; ?>

    <li class="tab col s3 <?php echo $reviewClass; ?>" >
        <a href="<?php echo $reviewUrl;?>" class="<?php echo $reviewClass;?> a-tab" id="review-tab">
        <span class="badge-step new" style="padding:4px 7px;"><?php echo ($lastTabNo+1);?></span>&nbsp;Manage Items</a>
    </li>

    <li class="tab col s3 <?php echo $previewClass; ?>" >
        <a href="<?php echo $previewUrl;?>" class="<?php echo $previewClass;?> a-tab" id="preview-tab">
        <span class="badge-step circle" style="padding:4px 7px;"><?php echo ($lastTabNo+2);?></span>&nbsp;Preview</a>
    </li>

	<!-- <li class="tab col s2 <?php //echo $shareClass; ?>" >
		<a href="<?php //echo $shareUrl;?>" class="<?php //echo $shareClass;?>" id="share-tab">
			<span class="badge-step" style="padding:4px 7px;">5</span>&nbsp;Share</a>
		</li> -->

    <li class="indicator" style="right: 1216px; left: 0px;"></li>
</ul>
<br/>
<?php
if (isset($model) && !empty($model)) {
    $modelDataProvider = new ArrayDataProvider([
        'allModels' => [
            [
                'name' => isset($model->name) && !empty($model->name) ? $model->name : '',
                'display_name' => isset($model->displayName) && !empty($model->displayName) ? $model->displayName : '',
				'type' => isset($model->type) && !empty($model->type) ? $model->type : '',
				'subType' => isset($model->subType) && !empty($model->subType) ? $model->subType : '',
                'description' => isset($model->description) && !empty($model->description) ? $model->description : '',
                'abbrev' => isset($model->abbrev) && !empty($model->abbrev) ? $model->abbrev : ''
            ]
        ],
    ]);
    echo GridView::widget([
        'dataProvider' => $modelDataProvider,
        'columns' => [
            'name',
            'display_name',
            [
                'label' => 'Type',
                'value' => function ($data) {
                    return strtoupper($data['type']);
                }
            ],
            [
                'label' => 'Sub Type',
                'value' => function ($data) {

                    return empty($data['subType']) ? '<span class="not-set">(not set)</span>' : $data['subType'];
                },
                'format' => 'raw'
			],
            [
                'label' => 'Description',
                'value' => function ($data) {

                    return empty($data['description']) ? '<span class="not-set">(not set)</span>' : $data['description'];
                },
                'format' => 'raw'
            ]
        ],
        'layout' => '{items}'
    ]);

}

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');

// Check if browser Id is empty
$browserId = empty($browserId) ? ['none'] : $browserId;

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ". json_encode($browserId) .",
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

(new DataBrowserConfiguration())->saveDataBrowserSettings($browserId);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
	$('#basic-tab').on('click', function(e){
		window.location = "$basicUrl";
	});

	$('#item-tab').on('click', function() {
		window.location = "$itemsUrl";
	});

	$('#review-tab').on('click', function() {
		window.location = "$reviewUrl";
	});

	$('#preview-tab').on('click', function() {
		window.location = "$previewUrl";
	});

	$('#share-tab').on('click', function() {
		window.location = "$shareUrl";
	});
JS
);