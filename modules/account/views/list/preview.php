<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for preview
**/

?>

<?php 
    $this->registerCss('
        [type="checkbox"]:not(:checked), 
        [type="checkbox"]:checked {
            opacity: 0;
        }
        label{
            margin-bottom: 0px;

        }
        kv-panel-before {
            padding: 5px;
        }

        .btn, .btn-large, .btn-flat {
            height: 28px;
            line-height: 28px;
        }
        input[type=text]:not(.browser-default){
            height: 34px;
        }
    ');

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\bootstrap\Button;
use yii\data\ArrayDataProvider;
use app\modules\account\models\ListModel;
use app\modules\account\models\ListMemberModel;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;

\yii\jui\JuiAsset::register($this);

$browserId = ['dynagrid-list-manager-preview-grid'];
$gridId = $browserId[0];

echo Yii::$app->controller->renderPartial('tabs', [
    'active' => 'preview',
    'id' => $id,
    'program' => $program,
    'abbrev' => isset($model->abbrev) && !empty($model->abbrev) ? $model->abbrev : '',
    'model' => $model,
    'browserId' => $browserId
]);

$params=Yii::$app->request->queryParams;

echo '<div class="row">';

echo \macgyer\yii2materializecss\widgets\Button::widget([
    'label' => Yii::t('app','Done'),
    'options' => [
        'class' => 'pull-right done-btn'
    ]
]);

echo '</div>';

$finalGridSelectAllId = 'final-grid-select-all-id';
$finalGridSelectId = 'final-grid_select';

$params = Yii::$app->request->queryParams;
$itemGridSelectAllId = 'final-grid-select-all-id';
$itemGridSelectId = 'final-grid_select';
$modelType = isset($model->type) && !empty($model->type) ? $model->type : '';

$dataProvider = $searchModel->search(null, $id, null, $params, true);
if ($modelType != '' && $modelType == 'plot') {
    $columns = ListMemberModel::getListMemberColumns(
        'plot',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $itemGridSelectId
        ],
        [],
        'view'
    );
} else if ($modelType != '' && $modelType == 'seed') {
    $columns = ListMemberModel::getListMemberColumns(
        'seed',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $itemGridSelectId
        ],
        [],
        'view'
    );
} else if ($modelType != '' && $modelType == 'germplasm') {
    $columns = ListMemberModel::getListMemberColumns(
        'germplasm',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $itemGridSelectId
        ],
        [],
        'view'
    );
} else if ($modelType != '' && $modelType == 'location') {
    $columns = ListMemberModel::getLocationMemberColumns($model, $itemGridSelectAllId, $itemGridSelectId, true, true, false);
} else if ($modelType != '' && $modelType == 'study') {
    $columns = ListMemberModel::getStudyMemberColumns($model, $itemGridSelectAllId, $itemGridSelectId, true, true, false);
} else if ($modelType != '' && $modelType == 'trait') {
    $columns = ListMemberModel::getListMemberColumns(
        'trait',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $itemGridSelectId
        ],
        [],
        'view'
    );
} else if ($modelType != '' && $modelType == 'package') {
    $columns = ListMemberModel::getListMemberColumns(
        'package',
        $columnVariables,
        $model,
        [
            'checkBoxId' => $itemGridSelectAllId,
            'checkBoxClass' => $finalGridSelectId
        ],
        [],
        'view'
    );
} else {
    $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'displayValue',
                'visible' => true,
                'format' => 'raw'
            ],
            'orderNumber',
            [	
                'attribute'=>'creationTimestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                    ]
                ],
            ],	
        ]
    ];
}
$dataProvider->id = $gridId;
$itemsCount = isset($dataProvider->totalCount) ? $dataProvider->totalCount : 0;

$dynaGrid = DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'panel-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $gridId,
        'dataProvider' => $dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>false,
        'hover' => true,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>$gridId
            ],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            'before' => \Yii::t('app', 'This data browser displays all items in my list.'),
            'beforeOptions' => [
                'style' => 'height:50px'
            ],
            'after' => false,
            // 'footer'=>false
        ],
        'toolbar' =>  [
            [
                'content' => Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>', 
                    ['preview','program'=>$program,'id'=>$id], 
                    [
                        'data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid')
                    ]
                ).'{dynagridFilter}{dynagridSort}{dynagrid}',
            ],
        ],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $gridId
        ]
]);

DynaGrid::end();
echo '<div class="row">';

echo \macgyer\yii2materializecss\widgets\Button::widget([
    'label' => Yii::t('app','Done'),
    'options' => [
        'class' => 'pull-right done-btn'
    ]
]);

echo '</div>';


$browserUrl = Url::to(['list/index', 'program' => $program]);
$itemsUrl = Url::to(['list/add-items', 'program' => $program, 'id' => $id]);
?>

<!-- Confirm create modal when list items is empty -->

<div class="modal fade" id="listmgt-confirm-preview-modal" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><em class="material-icons">warning</em>Confirm</h4>
            </div>
            <div class="modal-body" id="listmgt-delete-body">
                <h4>Your list: <strong><?php echo isset($model->name) && !empty($model->name) ? $model->name : '';?> </strong> is empty!</h4>
            </div>
            <div class="modal-footer">
                <a href="<?php echo $browserUrl;?>" style="margin: 10px 15px 0 0; font-size: 15px; text-decoration: underline;">Skip</a>
                <a href="<?php echo $itemsUrl;?>" class="btn-succes btn">Add items</a>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(<<<JS
    var itemsCount = "$itemsCount";
    var browserUrl = "$browserUrl";

    $('.done-btn').on('click', function(){
        setTimeout(function(){
            window.location = browserUrl;
        }, 1000);
    });

    $(document).on('ready pjax:success', function(e) {
        $('.collapsible').collapsible();
    });

    $("document").ready(function(e){
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
    })

JS
);