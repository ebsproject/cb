<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Contains preferences sample page
 */

use kartik\select2\Select2;
use yii\helpers\Url;

$showSideNav = Yii::$app->session->get('showSideNav');
$sideNavSkin = Yii::$app->session->get('sideNavSkin');
$fontSize = Yii::$app->session->get('fontSize');
$checked = ((!empty($showSideNav) && $showSideNav == 'true') || !isset($showSideNav)) ? 'checked' : '';
$lc = ''; $dc = '';
if(isset($sideNavSkin) && !empty($sideNavSkin) && $sideNavSkin == 'dark'){
    $dc = 'checked';
}else{
    $lc = 'checked';
}
?>

<ul id="layout-preferences" class="side-nav">

<br/>
<li>
    <label><?= \Yii::t('app','Left nav')?>:</label>
    <div class="switch">
        <label>
        <input type="checkbox" id="show-side-nav" <?= $checked ?>>
        <span class="lever"></span>
        <?= \Yii::t('app','Always show') ?>
        </label>
    </div>
</li>

<li>
    <label><?= \Yii::t('app','Theme color') ?>:</label>
    <div style="position: relative; height: 60px;margin-top: -15px">
        <div class="fixed-action-btn horizontal click-to-toggle active" style="position: absolute; left: 375px; right: inherit;bottom:inherit;">
              <ul>
                  <li class="theme-color-item"><a class="btn-floating rosegold darken-1 theme-color" data="rosegold" darken="1" style="opacity: 1;"></a></li>
                  <li class="theme-color-item"><a class="btn-floating blue darken-3 theme-color" data="blue" darken="3" style="opacity: 1;"></a></li>
                  <li class="theme-color-item"><a class="btn-floating light-green darken-4 theme-color" data="light-green" darken="4" style="opacity: 1;"></a></li>
                  <li class="theme-color-item"><a class="btn-floating teal darken-1 theme-color" data="teal" darken="1" style="opacity: 1;"></a></li>
                  <li class="theme-color-item"><a class="btn-floating cyan darken-3 theme-color" data="cyan" darken="3" style="opacity: 1;"></a></li>
              </ul>
        </div>
    </div>
    <div style="position: relative; height: 70px;">

      <div class="fixed-action-btn horizontal click-to-toggle active" style="position: absolute; left: 247px; right: inherit;bottom:inherit;">
          <ul>
              <li class="theme-color-item"><a class="btn-floating blue-grey darken-2 theme-color" data="blue-grey" darken="2" style="opacity: 1;"></a></li>
              <li class="theme-color-item"><a class="btn-floating deep-purple darken-2 theme-color" data="deep-purple" darken="2" style="opacity: 1;"></a></li>
              <li class="theme-color-item"><a class="btn-floating red darken-3 theme-color" data="red" darken="3" style="opacity: 1;"></a></li>
          </ul>
      </div>
    </div>
</li>

<li>
<label><?= \Yii::t('app','Left nav skin') ?></label><br/>&emsp;
  <input name="side-nav-skin" type="radio" id="light" value="light" <?= $lc ?>/>
  <label for="light"><?= \Yii::t('app','Light') ?></label>&emsp;
  <input name="side-nav-skin" type="radio" id="dark" value="dark" <?= $dc ?>/>
  <label for="dark"><?= \Yii::t('app','Dark') ?></label>
</li>

<li>
<label><?= \Yii::t('app','Font size') ?></label>
  <?php
      echo Select2::widget([
          'name' => 'save-font-size',
          'value' => $fontSize, // current value
          'data' => ['smallest'=>'smallest','smaller'=>'smaller','normal'=>'normal','larger'=>'larger','largest'=>'largest'],
          'id' => 'site-font-size',
          'options' => ['class' => 'font-size-select2'],
          'pluginOptions' => [
              'tags' => true,
              'width' => '100%'
          ],
      ]);
    ?>

</li>

<li>
<label><?= \Yii::t('app','Language') ?></label>
  <?php
      echo Select2::widget([
          'name' => 'select-language',
          'id' => 'select-language',
          'options' => ['class' => 'language-select2','placeholder' => 'Loading content. Please wait...',],
          'pluginOptions' => [
              'tags' => true,
              'width' => '100%'
          ],
      ]);
    ?>

</li>
</ul>

<?php

$saveFontSizeUrl = Url::to(['/account/preferences/savesitefontsize']);
$saveLanguageUrl = Url::to(['/account/preferences/savelanguage']);
$getLanguageTags = Url::to(['/dashboard/default/getlanguagetags']);

$this->registerJs(<<<JS
//upon selecting font size
$( "#site-font-size" ).change(function(){
    var val = this.value;

    $.ajax({
        url: '$saveFontSizeUrl',
        type: 'post',
        async: false,
        data: {
          font_size: val
        },
        success: function(response) {
        },
        error: function() {
        }
    });

    $('html').attr('class','font-size-'+val);
});
//upon selecting language
$( "#select-language" ).change(function(){
    var val = this.value;

    $.ajax({
        url: '$saveLanguageUrl',
        type: 'post',
        async: false,
        data: {
          language: val
        },
        success: function(response) {
          location.reload();

        },
        error: function() {
        }
    });
});
//set options for language
$(document).ready(function(){
    $.ajax({
        url: '$getLanguageTags',
        type: 'post',
        dataType: 'json',
        async: true,
        success: function(response) {
            option = response;
            var lookup = {};
            var slanguage = document.getElementById('select-language');

            for(var i=0; i<option.length;i++){
                var newLanguageOption = document.createElement("option");

                if (!(option[i]['id'] in lookup)) {

                    lookup[option[i]['id']] = 1;
                    newLanguageOption.value = option[i]['id'];
                    newLanguageOption.innerHTML = option[i]['text'];
                    slanguage.options.add(newLanguageOption);
                }
            }
            $('#select-language').val('$language');
        },
        error: function() {
        }
    });
});
JS
);
?>

<style>
#layout-preferences > li:not(.header) {
    margin: 0px 20px 0px 15px;
    line-height: 28px;
    border-bottom: 1px solid #ddd;
    padding: 10px 0 10px 0px;
}
li.theme-color-item>a.btn-floating {
    margin: 0 !important;
}
.side-nav li.theme-color-item>a {
    padding: 0px 23px;
}

.select2-results > ul > li[id$="smallest"] {
    font-size: 9px;
}

.select2-results > ul > li[id$="smaller"] {
    font-size: 10px;
}

.select2-results > ul > li[id$="normal"] {
    font-size: 12px;
}

.select2-results > ul > li[id$="larger"] {
    font-size: 14px;
}

.select2-results > ul > li[id$="largest"] {
    font-size: 16px;
}
</style>