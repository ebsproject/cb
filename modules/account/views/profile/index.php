<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders profile page
 */
use yii\helpers\Url;
use marekpetras\yii2ajaxboxwidget\Box;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use app\controllers\SiteController;
$layoutObj = SiteController::getLayout();
extract($layoutObj);

$firstName = $userName = $email = $lastName = '';
if(!empty($getUser)){
	$firstName = isset($getUser['firstName']) ? $getUser['firstName'] : '';
	$userName = isset($getUser['username']) ? $getUser['username'] : '';
	$email = isset($getUser['email']) ? $getUser['email'] : '';
	$lastName = isset($getUser['lastName']) ? $getUser['lastName'] : '';
}
?>

<div class="col col-md-3">

	<div class="card">
    	<div class="card-image">
			<img src="<?php echo Url::to($image);?>">
	    </div>
	    <div class="card-content">
			<p class="profile-basic-info" title="<?= \Yii::t('app', 'Full name') ?>"><a href="#" class="profile-icons"><em class="material-icons">payment</em></a><span><?= $firstName . ' ' . $lastName ?></span></p>
			<p class="profile-basic-info" title="<?= \Yii::t('app', 'Username') ?>"><a href="#" class="profile-icons"><em class="material-icons">account_box</em></a><span><?= $userName ?></span></p>
			<p class="profile-basic-info" title="<?= \Yii::t('app', 'Email') ?>"><a href="#" class="profile-icons"><em class="material-icons">email</em></a><span><?= $email ?></span></p>
	    </div>
  	</div>
</div>


<div class="col col-md-5">
    
    <div class="card">
		<div class="box-header with-border">
	        <h3 class="box-title"><span title="<?= \Yii::t('app', 'Organizational units where user belongs to') ?>"><em class="material-icons" style="vertical-align:bottom;font-size:110%">people</em> <?= \Yii::t('app', 'Organizational units') ?></span>
	    	</h3>	            
        </div>

	    <div class="card-content" style="padding:0px 24px">

	    <?php
		$teamMembers = '';
	    if(empty($teams)){
	    	echo '<div class="card-content ">'.\Yii::t('app', 'You do not belong to any organizational units yet.').'</div></div>';
	    }else{
	    ?>
			<p><?= \Yii::t('app', 'Below are the organizational units where you belong to. Click on an organizational unit to view members.') ?>
			
			</p>
		    </div>
		    <div class="card-tabs">
				<ul class="collection">
				<?php
	
					//loop through teams
					foreach ($teams as $key => $value) {
						$class = ($key == 0) ? 'active' : '';
						echo '<li class="collection-item avatar '.$class.'" id="header-team-'.$value['id'].'" title="'. \Yii::t('app', 'Click to view team members.') .'">
							<a href="#team-'.$value['id'].'" class="view-team-members" data-team_id="'.$value['id'].'">
								<i class="material-icons circle '.$themeColor . ' '. $darken .'">people</i>
								<span class="title">'.$value['team'].'</span>
								<p class="text-muted">'.$value['role'].'</p>
								<a href="#!" class="secondary-content hide-in-mobile"><b title="'.\Yii::t('app', 'Number of members').'">'.$value['count'].'</b></a>
							</a>';

						$teamModel = \Yii::$container->get('app\models\Team');
						$members = $teamModel->getTeamMembersCs($value['id']); 
						$style = ($key != 0) ? 'style="display: none;"' : '';
						$teamMembers .= '<div id="team-'.$value['id'].'" '.$style.' class="team-members-panel"><ul class="collection with-header">';
						$teamMembers .= '<li class="collection-header"><h5>'.$value['abbrev'].' '.\Yii::t('app', 'Organizational unit members').'</h5></li>';

						//loop through each team members
						foreach ($members as $k => $v) {
							$teamMembers .= '<li class="collection-item avatar team-members">';
							$teamMembers .= '<img class="circle" src=\''.Yii::getAlias('@web/images/user.png').'\';">';
							$teamMembers .= '<span class="title">'.$v['full_name'].'</span><br/>';
							$teamMembers .= '<span class="text-muted">'.$v['email'].'</span>';
							$teamMembers .= '<span class="secondary-content text-muted hide-in-mobile tooltipped" data-tooltip="'.$v['role'].'">'.$v['role_abbrev'].'</span>';
						}

						$teamMembers .= '</ul></div>';
					}
				?>
				</ul>
			</div>
			<div class="card-content grey lighten-4">
		        <?= $teamMembers ?>
	      	</div>
		<?php } ?>		
    </div>
</div>

<div class="col col-md-4">
	<?php
		$toolsButtons = [
			'others' => function(){ //if there are other buttons defined in the widget
				return '<a href="#!" title="Search" class="text-muted fixed-color search-recently-used-widget"><i class="material-icons widget-icons">search</i></a>';
			},
			'reload' => function(){
				return '
					&nbsp;&nbsp;<a href="#!" title="'. \Yii::t('app', 'Refresh data').'" class="text-muted fixed-color" onclick="$(&quot;#dashboard-widget-recently-used&quot;).box(&quot;reload&quot;);"><i class="material-icons widget-icons">loop</i></a>
					';
			}
		];

		$options = [
			'id'=>'dashboard-widget-recently-used',
			'title'=>'<span title="'.\Yii::t('app', 'Displays list of recently used applications').'"><i class="material-icons" style="vertical-align:bottom;font-size:110%">access_time</i> '.\Yii::t('app', 'Recently used tools').'</span>',
			'bodyLoad'=> ['/dashboard/recentlyused?col=1&key=2'],
			'toolsTemplate' => '{others} {reload}',
			'toolsButtons' => $toolsButtons
		];

	?>
	<?= Box::widget($options); ?>
</div>

<div class="clearfix"></div>

<!-- add to team modal -->
<div id="add-to-team-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4><em class="material-icons">add</em>&nbsp;<?= \Yii::t('app','Add to team') ?><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
        </div>
        <div class="modal-body add-to-team-modal-body">
            
            <div class="col col-md-6">
                <?php
                    echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Team') . '&nbsp;<span class="required">*</span></label>';
                    echo Select2::widget([
                    	'data' => $teamsTags,
                        'name' => 'select-team',
                        'id' => 'team-profile-select',
                        'maintainOrder' => true,
                        'options' => [
                            'placeholder' => \Yii::t('app','Select team'),
                            'tags' => true,
                        ]
                    ]);
                ?>
            </div>
            <div class="col-md-6">
                <?php
                    echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Role').'&nbsp;<span class="required">*</span></label>';
                    echo Select2::widget([
                        'name' => 'select-role',
                        'data' => $rolesTags,
                        'id' => 'role-profile-select',
                        'maintainOrder' => true,
                        'options' => [
                            'placeholder' => \Yii::t('app','Select role'),
                            'tags' => true,
                        ]
                    ]);
                ?>
            </div>

        </div>
        <div class="modal-footer">
        <a class="modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Cancel') ?></a>&emsp;
        <button class="disabled btn waves-effect waves-light" id="add-to-team-profile-confirm" style="margin-right:20px;"><?= \Yii::t('app','Add') ?></button>
        </div>
    </div>
  </div>
</div> 

<?php
Modal::begin([
    'id' => 'generic-modal',
]);
Modal::end();

$addToTeamUrl = Url::to(['/account/profile/addusertoteam']);

$this->registerJs(<<<JS
//view team members
$(document).on('click','.view-team-members', function(e){
	var obj = $(this);
	var id = obj.data('team_id');

	$('.team-members-panel').css('display','none');	
	$('#team-'+id).css('display','block');

	$('.collection-item').removeClass('active');
	$('#header-team-'+id).addClass('active');
});

//validate if program and team are selected
$( "[id$=-profile-select]" ).change(function(){
    var selected_team = $("#team-profile-select").val();
    var selected_role = $("#role-profile-select").val();

    if(selected_team !== '' && selected_role !== ''){
    	$('#add-to-team-profile-confirm').removeClass('disabled');
    }else{
    	$('#add-to-team-profile-confirm').addClass('disabled');
    }
});

//add to team
$('#add-to-team-profile-confirm').on('click', function(e){
	var selected_team = $("#team-profile-select").val();
    var selected_role = $("#role-profile-select").val();

    $.ajax({
        url: '$addToTeamUrl',
        type: 'post',
        data: {
            team_id: selected_team,
            role_id: selected_role,
            user_id: $userId
        },
        success: function(response) {
            location.reload();
        },
        error: function() {
        }
    });
});
JS
);

?>

<style type="text/css">
	.profile-icons{
		cursor: default;
	}

	.halfway{
		right: 5px;
		position: absolute;
	}

	.profile-icons i{
		vertical-align: middle;
	}
	.box {
		position: relative;
		background: #ffffff;
		-webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2); 
		box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2); 
		border-top:none;
		margin: .5rem 0 1rem 0;
		border-radius: 2px;
		width: 100%;
	}
	.box-header.with-border {
	    border-bottom: none; 
	}
	.box-header > .box-tools {
	    top: 10px;
	}
	.box-header {
	    padding: 15px;
	}
	.box-body {
	    padding: 0px 15px 10px 15px;
	    max-height: 800px;
	    overflow-y: auto;
	    overflow-x: hidden;
	}
	.panel-default {
	    border-color: transparent;
	}
	.panel {
	    margin-bottom: -10px;
	    background-color: transparent;
	    border: 1px solid transparent;
	}
	.panel-default > .panel-heading {
	    margin-bottom: -15px;
	    color: #333;
	    background-color: transparent;
	    border-color: transparent;
	}
	.panel-footer {
	    padding: 10px 0px;
	    background-color: transparent;
	}
	.recently-used-tools-widget{
		max-height: none !important;
	}
	.profile-basic-info span:not(.not-set){
		margin: 4px 12px 4px 15px;
    	position: absolute;
	}
	.collection .collection-item.avatar {
	    min-height: 64px;
	}
	.collection .collection-item.active {
	    background-color: #f5f5f5 !important;
	}
	.team-members-panel{
		animation: 0.5s ease-out 0s 1 load;
	}
	.view-team-members{
		display: block;
	}
	@keyframes load {
		from {
	        top: 100px;
	        opacity:0;
	    }
	    to {
	        top: 0px;
	        opacity:1;
	    }
	}
</style>