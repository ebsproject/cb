<?php

/* 
 * This file is part of Breeding4Rice. 
 * 
 * Breeding4Rice is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * Breeding4Rice is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */ 

namespace app\modules\account\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\modules\account\models\ListModel;
use app\modules\account\models\ListMemberModel;
use kartik\grid\GridView;
use kartik\date\DatePicker;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

use app\interfaces\modules\account\models\IListMemberModel;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\models\ILists;

/**
 * Class for all list members related methods
 */

class RemovedListMemberModel extends ListMemberModel{
    
    public function __construct(
        public IUserDashboardConfig $userDashboardConfig,
        $config = []
        ) {

    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param string $type list type
     * @param integer $id list ID
     * @param boolean $is_void if list items should be voided or not
     * @param array $params contains the different filter options
     * @param boolean $pagination whether the pagination of the browser should be set
     * @param boolean $isView whether the intended target for the dataprovider is the modal view or not
     * @param string $searchModel the name of the search model that is used for filtering
     *
     * @return ArrayDataProvider
     */
    public function search(string $type=null, $id, $isVoid=null, $params=null, $pagination=false, $isView=false, $searchModel='List') {
        $this->load($params);

        /** get current page number and page size */
        $paramPage = '';
        $paramLimit = (!$isView ? 'limit=10' : '');
        $dataProviderId = isset($isVoid) ? ($isVoid == 'false' ? 'dynagrid-list-manager-all-items-grid' : 'dynagrid-list-manager-final-grid') : 'dynagrid-list-manager-preview-grid';

        if (isset($_GET[$dataProviderId . '-page'])){
            $paramPage = '&page=' . $_GET[$dataProviderId . '-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        }

        $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

        $param = [];
        $url = !$isView ? 'lists/'.$id.'/members-search?sort=orderNumber&'.$paramLimit.$paramPage : 'lists/'.$id.'/members-search?sort=orderNumber';

        if (!is_null($isVoid)) {
            $param['isActive'] = $isVoid;
        }

        // parse filters
        if (!is_string($searchModel)) {
            throw new \yii\web\HttpException(500,"Please provide only the string name of the class!");
        }

        // check if advanced filters are set
        if (isset($_GET['Advance'])) {
            $advancedFilters = $_GET['Advance'];
            if (isset($advancedFilters['min']) && isset($advancedFilters['max'])) {
                $param['volume'] = [
                    "range" => $advancedFilters['min'] . "|" . $advancedFilters['max']
                ];
            }
            if (isset($advancedFilters['study']) && !empty($advancedFilters['study'])) {
                $studiesStr = str_replace(',', '|', $advancedFilters['study']);
                $param['study'] = $studiesStr;
            }
            if (isset($advanceFilters['year']) && !empty($advanceFilters['year'])) {
                $yearStr = str_replace(',', '|', $advancedFilters['year']);
                $param['year'] = $yearStr;
            }
            if (isset($advanceFilters['container_label']) && !empty($advanceFilters['container_label'])) {
                $labelStr = str_replace(',', '|', $advancedFilters['container_label']);
                $param['container_label'] = $labelStr;
            }
            if (isset($advanceFilters['studyName']) && !empty($advanceFilters['studyName'])) {
                $studyNamesStr = str_replace(',', '|', $advancedFilters['studyName']);
                $param['studyName'] = $studyNamesStr;
            }

            if (isset($advanceFilters['product_name']) && !empty($advanceFilters['product_name'])) {
                $productNameStr = str_replace(',', '|', $advancedFilters['product_name']);
                $param['product_name'] = $productNameStr;
            }

            if (isset($advanceFilters['entcode']) && !empty($advanceFilters['entcode'])) {
                $productNameStr = str_replace(',', '|', $advancedFilters['product_name']);
                $param['product_name'] = $productNameStr;
            }
        }

        $filters = isset($params[$searchModel]) ? $params[$searchModel]: null;

        if (!is_null($filters)) {
            foreach ($filters as $key => $value) {
                $prepend = str_contains($value,'%') ? '' : 'equals';
                $append = !empty($prepend) ? '' : "%";

                if (!empty($value)){
                    $param[$key] = $prepend. ' ' . $value . $append;
                }

                if ($key === 'quantity' && $value === '0'){
                    $param[$key] = $value;
                }
            }
        }
        
        if (empty($param)) {
            $param = '';
        }

        $data = Yii::$app->api->getResponse('POST',$url,json_encode($param));

        // get lists data provider
        $result = [];
        $totalCount = 0;
        if(isset($data) && !empty($data)){
            if (isset($data['status']) && $data['status'] == 200) {
                $data = $data['body'];
                $totalPages = (isset($data['metadata']['pagination']['totalPages'])) ? $data['metadata']['pagination']['totalPages'] : null;
                $totalCount = (isset($data['metadata']['pagination']['totalCount'])) ? $data['metadata']['pagination']['totalCount'] : null;
                $result = (isset($data['result']['data'])) ? $data['result']['data'] : [];

                // build data provider
                // if isView is true, it'll only get the 1st batch of items,
                // but if not then it should continue to retrieve the rest of the items
                if($totalPages > 1 && !$isView && $paramLimit == ''){       
                    for ($i=2; $i <= $totalPages; $i++) {
                        $data = Yii::$app->api->getResponse('POST',$url.'&page='.$i, json_encode($param));                
                        if($data['status'] == 200) {
                            $data = $data['body'];
                            $addlResult = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                        }
    
                        $result = array_merge($result,$addlResult);
                    }
                }

                // check if need to redirect browser page
                $currentPage = $data['metadata']['pagination']['currentPage'];
                if(count($result) == 0 && $totalCount == 0 && $currentPage > 1){
                    $url = 'lists/'.$id.'/members-search?sort=orderNumber&'.$paramLimit;
                    $data = Yii::$app->api->getResponse('POST',$url, json_encode($param));                
                    
                    if($data['status'] == 200) {
                        $data = $data['body'];
                        $result = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                        $totalCount = $data['metadata']['pagination']['totalCount'];

                        // redirect to 1st page
                        $_GET['page'] = 1;
                        $_GET[$dataProviderId . '-page'] = 1;
                    }
                }
            }
        }

        $options = [
            'key' => 'listMemberDbId',
            'allModels' => $result,
            'totalCount' => $totalCount,
            'restified' => true,
            'pagination' => [ 'pageSize' => 10 ]
        ];

        return new ArrayDataProvider($options);

    }
}