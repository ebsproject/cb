<?php

/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\account\models;

use Yii;
use app\models\BaseModel;
use app\models\Lists;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use app\dataproviders\ArrayDataProvider;
use app\models\User;
use app\interfaces\models\ILists;

/**
 * @property int $id
 * @property string $abbrev
 * @property string $name
 * @property string $display_name
 * @property string $type
 * @property int $entity_id
 * @property string $description
 * @property string $remarks
 * @property string $creation_timestamp
 * @property int $creator_id
 * @property string $modification_timestamp
 * @property int $modifier_id
 * @property string $notes
 * @property bool $is_void
 * @property string $record_uuid
 */

/**
* Model class for all list sharing methods
**/

class SharedList extends \app\models\PlatformList
{
    public $formNameParam = 'SharedList';
    public $lists;

    public function __construct(
        ILists $lists,
        $config=[]
    ){
        $this->lists = $lists;
    }
    
    /**
    * Get form name parameter for data browsers
    * @return string form name parameter
    **/

    public function formName()
    {
        return $this->formNameParam;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params contains different filter options
     *
     * @return ArrayDataProvider data source for list browsers
     */
    public function search($params, $filters='', $pagination=true)
    {
        $this->load($params);

        // build filters
        

        if (!empty($this->type)) {
            $param['type'] = $this->type;
        }
        if (!empty($this->abbrev)) {
            $param['abbrev'] = $this->abbrev;
        }
        if (!empty($this->name)) {
            $param['name'] = $this->name;
        }
        if (!empty($this->displayName)) {
            $param['displayName'] = $this->displayName;
        }
        if (!empty($this->description)) {
            $param['description'] = $this->description;
        }

        $filters = 'ownershipType=shared';
        if (!isset($param['type'])) {
            $param['type'] = 'not equals trait protocol|not equals management protocol';
        }
        
        $data = $this->lists->searchAll($param, $filters);
        $result = isset($data) && !empty($data) && isset($data['data']) ? $data['data'] : [];
        
        return new ArrayDataProvider([
            'key' => 'listDbId',
            'allModels' => $result,
        ]);
    }
}