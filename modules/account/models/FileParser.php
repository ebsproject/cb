<?php 
/* 
 * This file is part of Breeding4Rice. 
 * 
 * Breeding4Rice is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * Breeding4Rice is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */ 

namespace app\modules\account\models;

use SplFileObject;
use Yii;

/**
* Class containing method for uploading files to list
**/

class FileParser {

	/**
	* Read and parse uploaded file
	* @param String $fileName name of the file
	* @return json parsed result from file
	**/

	public static function reader($filename) {

		$uploadPath = realpath(Yii::$app->basePath) .'/uploads/';
		$file = \yii\web\UploadedFile::getInstanceByName($filename);
		
		$newFileName = \Yii::$app->security->generateRandomString().'.'.$file->extension;

		if ($file->saveAs($uploadPath . '/' . $newFileName)) {
			

			$csv=new \app\components\CsvReader($uploadPath . '/' . $newFileName);
			$csv->setHasHeader(true);

			foreach ($csv as $line) {
				
				 $data[]=$line;
			}		
			
			if(!empty($csv->getInvalids()) ){

				$error=[];
				$errs=$csv->getInvalids();
				foreach($errs as $err){
					$error[]=$err;
				}

				return json_encode(array("success"=>false,"error"=>$error));
			}

			$headers=$csv->getHeader();
			$result=FileParser::createTmpTbl(json_encode($data),$headers);

			unlink($uploadPath . '/' . $newFileName);

			return json_encode($result);
		}
	}

	/**
	* Create table from file data
	* @param String $data data from file
	* @param Array $headers column headers
	* @return Array if access is successful; table name; and message
	**/

	public function createTmpTbl($data, $headers) {
		$transaction_db = Yii::$app->db->beginTransaction();
		try {
			$columnHeaders=array_map('strtolower', $headers);
			
			$tbl = 't' . uniqid();
			Yii::$app->db->createCommand()->createTable('temporary_data.' . $tbl, array_fill_keys($columnHeaders, 'string'))->execute();
			$columns=implode(",",$columnHeaders);

			//insert the json data into a temp table
			Yii::$app->db->createCommand('insert into '
				. 'temporary_data.' . $tbl . "($columns) "
				. "select $columns from json_populate_recordset(null::temporary_data.$tbl,'{$data}')")
			->execute();

			$transaction_db->commit();

			return ["success"=>true,"temporary_table"=>'temporary_data.'.$tbl, "headers"=>$headers];
			
		} catch (\Exception $e) {
			
			return ["success"=>false,"error"=>$e->getMessage()];
			$transaction_db->rollBack();
		}
	}
}