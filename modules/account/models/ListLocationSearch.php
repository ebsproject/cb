<?php

/* 
 * This file is part of Breeding4Rice. 
 * 
 * Breeding4Rice is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * Breeding4Rice is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


namespace app\modules\account\models;

use Yii;
use yii\data\SqlDataProvider;

/**
*	Search model for locations used in list
**/

class ListLocationSearch extends \yii\db\ActiveRecord{

	public $abbrev;
	public $name;
	public $display_name;
	public $country;
	public $place_type;
	public $latitude;
	public $longitude;
	public $organization;
	public $status;
	public $is_active;

	/**
     * {@inheritdoc}
     */

	public function rules(){
		return [
			[['abbrev','name','display_name','country', 'place_type', 'latitude', 'longitude', 'organization', 'remarks'], 'safe'],
			[['is_active'], 'boolean']
		];
	}
}
