<?php

/* 
 * This file is part of Breeding4Rice. 
 * 
 * Breeding4Rice is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * Breeding4Rice is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


namespace app\modules\account\models;

use Yii;
use yii\data\SqlDataProvider;
use app\dataproviders\ArrayDataProvider;
use app\models\User;
use app\models\BaseModel;
use app\interfaces\models\IUser;

/**
* Search model for list access use in share list feature
**/

class ListAccessSearch extends BaseModel{

    public $user;

    public function __construct(
        IUser $user,
        $config=[]) {
        
        $this->user = $user;

        parent::__construct($config);
    }
    
    /**
     * Search method for list access data browser
     * @param Array $params contains different filter options
     * @param Integer $listId ID of the list
     * @return ArrayDataProvider data object for browser
     */
    public function search($params, $listId) {

        $userId = $this->user->getUserId();
        $param = '';
        $url = 'lists/'.$listId.'/permissions';
        $data = Yii::$app->api->getResponse('GET',$url,json_encode($param),'sort=entity|addedOn:desc',true);

        // get lists data provider
        $result = [];
        if(isset($data) && !empty($data)){
            if (isset($data['status']) && $data['status'] == 200) {
                $data = $data['body'];
                $result = (isset($data['result']['data'])) ? $data['result']['data']: [];
    
                // exclude current user; TODO permissions-search call to exclude the user ID there
                foreach ($result as $index => $res) {
                    if ($res['entity'] == 'user' && $res['entityDbId'] == $userId) {
                        unset($result[$index]);
                        break;
                    }
                }
    
            }
        }
        $options = [
            'key' => 'rowDbId',
            'allModels' => $result,
            'pagination' => false
        ];
        
        return new ArrayDataProvider($options);

    }

    /**
     * Retrieve existing user IDs that already has access to a specific list
     * @param ArrayDataProvider the data provider from the list access grid
     * @return Array contains the user Ids
     */
    public static function getExistingAccessIds($dataProvider) {
        $returnIds = [];
        foreach ($dataProvider->allModels as $data) {
            $returnIds[] = $data['userdbid'];
        }
        return $returnIds;
    }

}