<?php 

/* 
 * This file is part of Breeding4Rice. 
 * 
 * Breeding4Rice is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * Breeding4Rice is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */ 
 
namespace app\modules\account\models;

use Yii;
use yii\data\SqlDataProvider;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use app\models\Variable;
use app\models\User;
use app\models\Germplasm;
use app\models\GermplasmName;
use app\models\Lists;
use app\models\Person;
use app\models\TenantProgram;
use app\models\TenantTeam;
use app\models\Traits;
use app\models\ListMember;
use app\models\BackgroundJob;
use app\dataproviders\ArrayDataProvider;
use app\modules\account\models\ListAccessSearch;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

use app\interfaces\modules\account\models\IListModel;
use app\interfaces\models\ILists;
use app\interfaces\models\IUser;
use app\interfaces\models\IPerson;
use app\interfaces\models\ITenantTeam;
use app\interfaces\models\ITenantProgram;
use app\interfaces\models\IGermplasm;
use app\interfaces\models\ITraits;

/**
* Class for all list management related methods
**/

class ListModel extends \app\models\PlatformListMember implements IListModel{

    public $abbrev;
    public $name;
    public $country;
    public $status;
    public $generation;
    public $parentage;
    public $designation;
    public $label;
    public $gid;
    public $volume;
    public $study;
    public $study_name;
    public $studyName;
    public $place;
    public $phase;
    public $year;
    public $season;
    public $entry_count;
    public $plot_count;
    public $program;
    public $plotno;
    public $code;
    public $entcode;
    public $source_study;
    public $source_year;
    public $product_name;
    public $container_label;
    public $is_active;
    public $formNameParam='List';

    // package columns
    public $experiment;
    public $experimentOccurrence;
    public $seedName;
    public $seedSourcePlotCode;
    public $quantity;
    public $unit;

    public $occurrenceName;
    public $plotCode;
    public $plotNumber;
    public $entryCode;
    public $entryNumber;

    public $lists;
    public $traits;
    public $germplasm;
    public $tenantTeam;
    public $tenantProgram;
    public $user;
    public $person;
    protected $listMember;

    public $germplasmNameModel;

    public $userModel;
    public $backgroundJob;

    public function __construct(
        ILists $lists,
        ITraits $traits,
        IGermplasm $germplasm,
        ITenantTeam $tenantTeam,
        ITenantProgram $tenantProgram,
        IUser $user,
        IPerson $person,
        ListMember $listMember,
        BackgroundJob $backgroundJob,
        GermplasmName $germplasmNameModel,
        $config=[]) {
        
        $this->lists = $lists;
        $this->traits = $traits;
        $this->germplasm = $germplasm;
        $this->tenantTeam = $tenantTeam;
        $this->tenantProgram = $tenantProgram;
        $this->user = $user;
        $this->person = $person;
        $this->listMember = $listMember;
        $this->germplasmNameModel = $germplasmNameModel;
        $this->backgroundJob = $backgroundJob;

        parent::__construct($config);
    }

    /**
    * Get form name parameter for data browsers
    * @return string form name parameter
    **/
    
    public function formName()
    {
        return $this->formNameParam;
    }

    public function setFormName($browserFormName)
    {
        return $this->formNameParam = $browserFormName;
    }

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['abbrev', 'name', 'country', 'status', 'generation', 'parentage', 'designation', 'label', 'gid', 'volume', 'study','study_name','studyName','place','phase', 'year', 'season', 'entry_count', 'plot_count','program', 'plotno', 'code', 'entcode', 'product_name','source_study', 'source_year', 'container_label', 'is_active', 'experiment', 'experimentOccurrence', 'seedName', 'seedSourcePlotCode',
            'quantity', 'unit', 'plotCode', 'plotNumber', 'entryCode', 'entryNumber', 'occurrenceName'
            ], 'safe'],
            [['is_void'], 'boolean'],
        ];
    }

    /**
    * Retrieve columns for review and preview page for list_type = seed
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getSeedListMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false, $orderNumber=true, $isFinalList=true) {

        $checkBoxVisible = ($isPreview) ? false : true;
        $reorderVisible = ($isFinalList) ? true : false;

        return [
            [
                'label'=> "checkbox",
                'visible'=> $reorderVisible,
                
                'header' => 'Reorder',

                'contentOptions' => ['style' => ['cursor' => 'pointer;',]],
                'content'=>function($model) {
                    return '
                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>

                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', "data-id"=>$data['id']];
                }
            ],
            [
                'attribute' => 'designation',
                'label' => 'Designation',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'label',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'gid',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'volume',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'source_study',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'source_year',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'container_label',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute'=>'creation_timestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];
    }

    /**
    * Retrieve columns for review and preview pages for list_type = location
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getLocationMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false, $orderNumber=true, $isFinalist=true) {

        $checkBoxVisible = ($isPreview) ? false : true;
        $reorderVisible = ($isFinalist) ? true : false;

        return [

            [
                'label' => "checkbox",
                'visible' => $reorderVisible,
                'header' => 'Reorder',
                'contentOptions' => ['style' => ['cursor' => 'pointer;']],
                'content' => function($model) {
                    return '

                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'hiddenFromExport'=> true,
                'mergeHeader' => true,
                'width' => '50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', 'data-id' => $data['id']];
                }
            ],
            [
                'attribute' => 'abbrev',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'country',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'is_active',
                'label' => 'Status',
                'visible' => true,
                'header' => 'Status',
                'content' => function($data) {
                    return '<span class="badge '. (($data['is_active']) ? ' new">Active' : ' new badge amber darken-2">Draft') . '</span>';
                },
                'noWrap'=>false,
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>[
                    'true' =>'Active',
                    'false' =>'Draft',
                ], 
                'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true,'id'=>"location_active"],],
                'filterInputOptions'=>['placeholder'=>'status','id'=>"location_active"],
            ],
            [
                'attribute'=>'creation_timestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];

    }

    /**
    * Retrieve columns for review and preview page for list_type = plot
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getPlotListMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false, $orderNumber=true,$isFinalist=true) {

        $checkBoxVisible = ($isPreview) ? false : true;
        $reorderVisible = ($isFinalist) ?  true : false;

        return [
            [
                'label'=> "checkbox",
                'visible'=> $reorderVisible,
                'header' => 'Reorder',
                'contentOptions' => ['style' => ['cursor' => 'pointer;',]],
                'content'=>function($model) {
                    return '
                    
                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', "data-id"=>$data['id']];
                }
            ],
            [
                'attribute' => 'study',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'study_name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'product_name',
                'label' => 'Designation',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'code',
                'label' => 'Plot Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'plotno',
                'label' => 'Plot No.',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'entcode',
                'label' => 'Entry Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [   
                'attribute'=>'creation_timestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]

        ];

    }

    /**
    * Retrieve columns for review and preview page for list_type = study
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getStudyMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false, $orderNumber=true, $isFinalist=true) {

        $checkBoxVisible = ($isPreview) ? false : true;
        $reorderVisible = ($isFinalist) ?  true : false;

        return [
            [
                'label'=> "checkbox",
                'visible'=> $reorderVisible,
                'header' => 'Reorder',
                'contentOptions' => ['style' => ['cursor' => 'pointer;',]],
                'content'=>function($model) {
                    return '
                    
                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', "data-id"=>$data['id']];
                }
            ],
            [
                'attribute' => 'program',
                'label' => 'Program',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'study_name',
                'label' => 'Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'study',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'place',
                'label' => 'Location',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'phase',
                'label' => 'Phase',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'year',
                'label' => 'Year',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'season',
                'label' => 'Season',
                'visible' => true,
                'format' => 'raw'
            ],
            [   
                'attribute'=>'creation_timestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];
    }

    /**
    * Retrieve columns for review and preview page for list_type = designation
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getDesignationMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false,$orderNumber=true,$isFinalList=true) {

        $checkBoxVisible = ($isPreview) ?  false : true;
        $reorderVisible = ($isFinalList) ? true : false;

        return [
            [
                'label' => 'checbox',
                'visible' => $reorderVisible,
                'header' => 'Reorder',
                'contentOptions' => ['style' => ['cursor' => 'pointer;']],
                'content' => function($model) {
                    return '
                        <i class="material-icons">drag_handle</i>
                    ';
                },
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'hiddenFromExport' => true,
                'mergeHeader' => true,
                'width' => '50px'
            ],
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="'.$checkBoxClass.'" type="checkbox" id="'.$data["id"].'" />
                        <label for="'.$data["id"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'attribute' => 'order_number',
                'label' => '',
                'filter' => false,
                'format' => 'raw',
                'visible' => $orderNumber,
                'contentOptions' => function($data) {
                    return ['class' => 'sort_order_arr', "data-id"=>$data['id']];
                }
            ],
            [
                'attribute' => 'designation',
                'label' => 'Designation',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'generation',
                'label' => 'Generation',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'other_names',
                'label' => 'Other Names',
                'visible' => true,
                'format' => 'raw',
                'value' => function ($data) {

                    if (strlen($data['other_names']) > 30) {
                        $displayNames = substr($data['other_names'],0,30);
                        $otherNames = $data['other_names'];

                        $display = '

                            <ul class="collapsible">
                                <li>
                                  <div class="collapsible-header" style="padding:0.5rem; color: #26a69a;">'.$displayNames.'</div>
                                  <div class="collapsible-body">'.$otherNames.'</div>
                                </li>
                              </ul>

                        ';

                    } else {
                        $display = $data['other_names'];
                    }

                    return $display;

                }
            ]
        ];
    }

    /**
    * Insert item members for plot type
    * @param Integer $id ID of the list
    * @param Array $selectedStudies the ID's of the selected studies
    * @param Array $selectedData contains the seleted plots for each selected study
    * @return Integer $totalCount total number of records inserted
    **/
    public static function addToPlotList($id, $selectedStudies, $selectedData) {

        $jsonArr = [];
        // get plots for studies
        foreach ($selectedStudies as $studyId) {
            $selectedPlots = [];
            if (isset($selectedData[$studyId])) {
                $selectedPlots = $selectedData[$selectedStudies[0]];
                $selectedPlots = "equals ".str_replace(",", "|equals ", $selectedPlots);
            }
            $param = '';
            if (!empty($selectedPlots)) {
                $param = [];
                $param["plotDbId"] = $selectedPlots;
            }
            // get plots
            $url = 'studies/'.$studyId.'/plots-search';
            $data = Yii::$app->api->getResponse('POST', $url, json_encode($param));

            if(!isset($data) || !isset($data['status']) || empty($data['status'])){
                return 0;
            }

            // form json data for insert
            if ($data['status'] == 200) {
                $data = $data['body'];
                $totalPages = (isset($data['metadata']['pagination']['totalPages'])) ? $data['metadata']['pagination']['totalPages'] : null;
                $results = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                // build data provider
                if($totalPages > 1){       
                    for ($i=2; $i <= $totalPages; $i++) {
                        $data = Yii::$app->api->getResponse('POST',$url.'?page='.$i, json_encode($param));
                        
                        if(isset($data['status']) && !empty($data['status'])){
                            if($data['status'] == 200) {
                                $data = $data['body'];
                                $addlResult = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                            }
                            $results = array_merge($results,$addlResult);
                        }
                    }
                }

                if (!empty($results)){
                    foreach ($results as $result) {

                        $jsonArr[] = [
                            "id" => $result["plotDbId"],
                            "displayValue" => $result["code"]
                        ];
                    }
                }
            }
        }

        // insert records
        $url = 'lists/'.$id.'/members';
        $param = [
            "records" => $jsonArr
        ];

        $data = Yii::$app->api->getResponse('POST', $url, json_encode($param));

        if(!isset($data) || !isset($data['status']) || empty($data['status'])){
            return 0;
        }

        $totalCount = 0;
        if ($data['status'] == 200) {
            $metadata = $data['body']['metadata'];
            $totalCount = $metadata['pagination']['totalCount'];
        }
        return $totalCount;
    }

    /**
    * Validate input for creating lists with type Trait
    * Inputs for trait lists will be the name of the trait
    * @param array $inputListSplitArr array containing the input values
    * @return array if list is valid
    **/

    public function validateTraitInput($inputListSplitArr) {
        $foundTraitArray = [];
        $notFoundTraitArray = [];
        $orderNumber = 0;
        $arrangedArray = [];

        $searchLabels = 'equals '.implode('|equals ', $inputListSplitArr);
        $filters = [
            'type' => 'equals observation',
            'label' => $searchLabels
        ];
        $data = $this->traits->searchAll($filters);

        if(!isset($data) || !isset($data['data'])){
            return [
                'dataProvider' => new ArrayDataProvider([]),
                'invalidItems' => $notFoundTraitArray,
                'validItems' => $arrangedArray,
                'validCount' => $data['totalCount']
            ];
        }

        $foundTraits = $data['data'];
        $foundTraitArray = array_column($foundTraits, 'label');
        
        $traitDbIds = [];
        $foundTraitArray = array_map('strtolower', $foundTraitArray);
        foreach ($inputListSplitArr as $inputTrait) {
            $orderNumber+=1;
            if (in_array(strtolower($inputTrait), $foundTraitArray)) {
                $index = array_search(strtolower($inputTrait), $foundTraitArray);

                if (!in_array($foundTraits[$index]['variableDbId'], $traitDbIds)) {
                    $temp = [
                        'variableDbId' => $foundTraits[$index]['variableDbId'],
                        'label' => trim($foundTraits[$index]['label']),
                        'inputTraitLabel' => trim($inputTrait),
                        'abbrev' => trim($foundTraits[$index]['abbrev']),
                        'name' => trim($foundTraits[$index]['name']),
                        'dataType' => trim($foundTraits[$index]['dataType']), 
                        'orderNumber' => $orderNumber
                    ];

                    $traitDbIds[] = $foundTraits[$index]['variableDbId'];
                }
                
                $arrangedArray[] = $temp;
            } else {
                $notFoundTraitArray[] = $inputTrait;
            }
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $arrangedArray,
            'key' => 'variableDbId',
            'pagination' => false,
            'totalCount' => $data['totalCount']
        ]);

        return [
            'dataProvider' => $dataProvider,
            'invalidItems' => $notFoundTraitArray,
            'validItems' => $arrangedArray,
            'validCount' => $data['totalCount']
        ];
    }

    /**
    * Validate input for creating lists with type Germplasm
    * @param array $inputListSplitArr array of input values
    * @return array if list is valid and notification message
    **/    

    public function validateDesignationInput($inputListSplitArr) {

        $normInputProductNameResults = $inputListSplitArr;

        //search for each product
        $foundProductArray = [];
        $notFoundProductArray = [];
        $orderNumber = 0;
        $arrangedArray = [];
        
        $searchNames = 'equals '.implode('|equals ', $inputListSplitArr);

        $filters['names'] = $searchNames;
        $data = $this->germplasm->searchAll($filters);

        if(!isset($data) || !isset($data['data'])){
            return [
                'dataProvider' => new ArrayDataProvider([]),
                'invalidItems' => $notFoundProductArray,
                'validItems' => $arrangedArray,
                'validCount' => 0
            ];
        }

        $foundProducts = $data['data'];
        $foundProductArray = array_column($foundProducts, 'germplasmNormalizedName');
        $otherNamesArray = array_column($foundProducts, 'otherNames');
        $productFound = false;
        $germplasmDbIds = array();

        foreach($normInputProductNameResults as $inputProduct){    
            $productFound = false;
            $orderNumber+=1;

            //get normalized product name
            $normalizedInputProductNameRes = $this->germplasmNameModel->getStandardizedGermplasmName([$inputProduct]);
            $normalizedInputProductNameRes = isset($normalizedInputProductNameRes) && !empty($normalizedInputProductNameRes) ? $normalizedInputProductNameRes[0]['norm_input_product_name'] : $inputProduct;

            if (in_array($normalizedInputProductNameRes, $foundProductArray)){
                $index = array_search($normalizedInputProductNameRes, $foundProductArray);
                
                if(!in_array($foundProducts[$index]['germplasmDbId'], $germplasmDbIds)){
                    $temp = [
                        'germplasmDbId' => $foundProducts[$index]['germplasmDbId'],
                        'germplasmNormalizedName' => $foundProducts[$index]['germplasmNormalizedName'],
                        'inputProductName' => $inputProduct,
                        'designation' =>$foundProducts[$index]['designation'],
                        'orderNumber' => $orderNumber
                    ];

                    $germplasmDbIds[] = $foundProducts[$index]['germplasmDbId'];
                }
                  
                $arrangedArray[] = $temp;
                $productFound = true;
            } else {
                foreach ($otherNamesArray as $index => $otherNames) {
                    
                    // normalize otherNames values
                    $normOtherNameValues = $this->germplasmNameModel->getStandardizedGermplasmName(explode(';',$otherNames));
                    $normOtherNameValuesResArr = array_column($normOtherNameValues,'norm_input_product_name');
                    
                    if(in_array(strtoupper($normalizedInputProductNameRes),$normOtherNameValuesResArr)){
                        if(!in_array($foundProducts[$index]['germplasmDbId'], $germplasmDbIds)){
                            $temp = [
                                'germplasmDbId' => $foundProducts[$index]['germplasmDbId'],
                                'germplasmNormalizedName' => $foundProducts[$index]['germplasmNormalizedName'],
                                'inputProductName' => $inputProduct,
                                'designation' =>$foundProducts[$index]['designation'],
                                'orderNumber' => $orderNumber
                            ];
        
                            $germplasmDbIds[] = $foundProducts[$index]['germplasmDbId'];
                            $arrangedArray[] = $temp;
                        }
                        $productFound = true;

                        break;
                    }
                }
            }

            if (!$productFound) {
                $notFoundProductArray[] = $inputProduct;
            }
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $arrangedArray,
            'key' => 'germplasmDbId',
            'pagination' => false,
            'totalCount' => $data['totalCount']
        ]);

        return [
            'dataProvider' => $dataProvider,
            'invalidItems' => $notFoundProductArray,
            'validItems' => $arrangedArray,
            'validCount' => $data['totalCount']
        ];
    }


    /**
    * Get all team options for share list feature
    * @return array containing the team ID and team name
    **/
    public function getTeams() {
        $tags = [];
        $response = $this->tenantTeam->searchAll();
    
        if(isset($response) && isset($response['data'])){
            $data = $response['data'];
            foreach($data as $record) {
                if ($record['teamCode'] == 'SG') //exclude system group team
                    continue;

                $tags[$record['teamDbId']] = $record['teamName'] . ' ('.$record['teamCode'].')'; 
            }
        }

        return $tags;
    }

    /**
    * Get all program options for share list feature
    * @return array containing the program ID and program name
    **/
    public function getPrograms() {
        $tags = [];

        $response = $this->tenantProgram->getAll();

        if(isset($response) && isset($response['data'])){
            $data = $response['data'];
            foreach($data as $record) {
                if ($record['programCode'] == 'SG') //exclude system group team
                    continue;

                $tags[$record['programDbId']] = $record['programName'] . ' ('.$record['programCode'].')'; 
            }
        }

        return $tags;
    }


    /**
    * Get all user options for share list feature
    * @param integer $listId ID of list
    * @param integer|array $userId ID of user
    * @param integer $teamId ID of team
    * @param string $return if return should be tags
    * @return array of users
    **/

    public function getUsers($listId, $userId, $teamId=null,$return='tags') {
        $tags = [];
        $userId = $userId ?? $this->user->getUserId();

        $response = $this->person->searchAll('', 'sort=personName');

        if(isset($response) && isset($response['data'])){
            $results = $response['data'];
            foreach ($results as $res) {
                if (
                    (gettype($userId) == 'integer' && $res['personDbId'] == $userId) ||
                    (gettype($userId) == 'array' && in_array($res['personDbId'], $userId))
                )
                    continue;

                if ($return == 'id') {
                    $tags[] = $res['personDbId'];
                } else {
                    $tags[$res['personDbId']] = $res['personName'];
                }
            }
        }

        return $tags;
    }

    /**
     *  Validates form input for creating a list
     * @param Integer $id ID of the list, null if the list is new
     * @return Array contains the error message if operation failed
     */
    public function validateCreateForm($id) {

        $newName = '';
        $newId = '';
        $returnArr = [];

        if (isset(Yii::$app->request->post()['Basic'])) {
            $postData = Yii::$app->request->post()['Basic'];
            $success = false;
            $error = [];

            if (!empty($id)) {

                $param = [
                    "abbrev" => trim($postData['abbrev']),
                    "name" => trim($postData['name']),
                    "displayName" => $postData['display_name'],
                    "type" => $postData['type'],
                    "description" => $postData['description'],
                    "remarks" => $postData['remarks']
                ];
                
                if(isset($postData['subType']) && $postData['subType'] != null && $postData['subType'] != ''){
                    $param["subType"] = $postData['subType'];
                }

                $data = $this->lists->updateOne($id, $param);

                if(isset($data) && isset($data['status']) && !empty($data['status'])){
                    if ($data['status'] == 200) {
                        $success = true;
                        $newName = $param['name'];
                        $newId = $data['data'][0]['listDbId'];
                    } else {
                        $error[] = $data['status'] == 400 ? "Name or Abbrev already exists. Please provide a new one." : $data['message'];
                    }
                }
                else{
                    $error[] = "Error encountered in updating list information. Please try again.";
                }
            } else {
                
                $paramArr = [
                    "abbrev" => trim($postData['abbrev']),
                    "name" => trim($postData['name']),
                    "displayName" => $postData['display_name'],
                    "type" => $postData['type'],
                    "description" => $postData['description'],
                    "remarks" => $postData['remarks']
                ];

                if(isset($postData['subType']) && $postData['subType'] != null && $postData['subType'] != ''){
                    $paramArr["subType"] = $postData['subType'];
                }

                $param['records'][] = $paramArr;
                $data = $this->lists->create($param);
                $param['name'] = isset($param['name']) ? $param['name'] : '';

                if(isset($data) && isset($data['status']) && !empty($data['status'])){
                    if ($data['status'] == 200) {
                        $success = true;
                        $newName = $param['name'];
                        $newId = $data['data'][0]['listDbId'];
                    } else {
                        $error[] = $data['status'] == 400 ? "Name or Abbrev already exists. Please provide a new one." : $data['message'];
                    }
                }
                else{
                    $error[] = "Connection error encountered. Please try again.";
                }
            }

            if ($success){
                $action = !empty($id) ?  'updated' : 'created';

                $returnArr =[
                    'success' => true,
                    'message' => "List: <b>".$newName."</b> successfully ". $action."."
                ];
            } else {
                if (!empty($error)) {

                    $errorStr = '';
                    foreach ($error as $e) {
                        if (is_array($e)) {
                            $e = $e[0];
                        }
                        $errorStr = empty($errorStr) ? $errorStr : $errorStr . "<br/>";
                        $errorStr = $errorStr . ($data['status'] == 400 ? "Name or Abbrev already exists. Please provide a new one." : $e);
                    }
                }

                $returnArr = [
                    'success' => false,
                    'message' => $error,
                    'error' => $error
                ];
            }

            $returnArr['list_name'] = $newName;
            $returnArr['list_id'] = $newId;
            return $returnArr;
        } 
    }

    /**
     * Processes the data for inserting list members to a list
     * @param Integer $id ID of the list
     * @param String $listType type of the list
     * @return String notification message
     */
    public function addMembersByInput($id, $listType) {

        $recordCount = 0;

        $postParams = \Yii::$app->request->post();
        $productList = $postParams['productList'];
        $productList = str_replace('\\', '$', $productList);
        $productList = json_decode($productList, true);

        switch ($listType) {
            case 'germplasm' :
                $memberRecords = [];
                foreach ($productList as $product) {
                    $memberRecords[] = [
                        'id' => $product['germplasmDbId'],
                        'displayValue' => $product['designation']
                    ];
                }

                $response = $this->lists->createMembers($id, $memberRecords);               
                $recordCount = isset($response) && isset($response['totalCount']) ? $response['totalCount'] : 0;
            break; //end of list_type = designation

            case 'trait':
                $memberRecords = [];
                foreach ($productList as $product) {
                    $memberRecords[] = [
                        'id' => $product['variableDbId'],
                        'displayValue' => $product['label']
                    ];
                }

                $response = $this->lists->createMembers($id, $memberRecords);
                $recordCount = isset($response) && isset($response['totalCount']) ? $response['totalCount'] : 0;
            break; // end of list_type = trait

            default:
                break;
        }
        // add lines to update modifier of list
        $processRemark = 'Added list members via input';
        $this->updateListInfo($id,$processRemark);

        return 'Succesfully added ' . $recordCount . ' item/s to list.';

    }

    /**
     * Update list info
     * @param Integer $listId ID of the list
     * @param String $processRemarks remarks identifying the update
     * @param Integer $listMemberId id of list member
     */
    public function updateListInfo($listId,$processRemarks,$listMemberId=null){
        if(!empty($listId) && $listId != null){
            // get list info
            $response = $this->lists->getOne($listId);

            if(isset($response) && isset($response['data'])){
                $model = $response['data'];
    
                if (!empty($model['listDbId'])) {
                    $remarks = $model['remarks'];
                    
                    // Update list remark to show creating list member is in progress
                    $requestData = [
                        'remarks' => $remarks
                    ];
                    $this->lists->updateOne($listId, $requestData);
                }
            }
        }

        return true;
    }

    /**
     * Update list status
     * @param Array $selectedListIds array if ids being used
     * @param String $status status of selected lists
     */
    public function updateListStatus($selectedListIds,$status){
        if(isset($selectedListIds) && isset($status) && !empty($selectedListIds) && $status != ''){
            // get list records
            $searchParams = ['listDbId' => 'equals '.implode('|equals ',$selectedListIds)];
            $records = $this->lists->searchAll($searchParams);

            if(isset($records) && isset($records['data']) && !empty($records['data'])){
                $listIds = array_column($records['data'],'listDbId');
                // set update params
                $param = [
                    "status" => $status
                ];

                // update list status
                foreach($listIds as $listId){
                    $response = $this->lists->updateOne($listId,$param);
                }
            }
        }

        return true;
    }

    /**
     * Validates the inputted names/abbrev for adding to list
     * @param Array $inputList contains the list of names
     * @param Integer $id ID of the list
     * @param String $listType the type of the list
     * @param String $program abbrev of the program
     * @return Array return the needed data for rendering the view file 
     */
    public function validateInputList($inputList, $id, $listType, $program) {

        $inputListSplitArr = array_map('trim', array_filter(preg_split("/[\r\n|,|\n]/", $inputList)));
        $inputListSplitArr = array_unique($inputListSplitArr);
        $validationLimit = 500;
        
        if($inputListSplitArr && count($inputListSplitArr) > $validationLimit){
            return [
                'success' => false,
                'message' => 'Exceeds validation record limit.'
            ];
        }

        if ($listType == 'germplasm') {

            $returnArr = $this->validateDesignationInput($inputListSplitArr);

            $returnArr['inputType'] = 'names';
            $returnArr['listType'] = $listType;
            $returnArr['listDbId'] = $id;
            $returnArr['program'] = $program;

            return [
                'success' => true,
                'returnArr' => $returnArr
            ];

        } else if ($listType == 'trait') {
            
            $returnArr = $this->validateTraitInput($inputListSplitArr);

            $returnArr['inputType'] = 'label';
            $returnArr['listType'] = $listType;
            $returnArr['listDbId'] = $id;
            $returnArr['program'] = $program;

            return [
                'success' => true,
                'returnArr' => $returnArr
            ];
        }

        return [
            'success' => false,
        ];
    }

    /**
     * Validate if split is possible or valid
     * @param Integer $listId id of list to be split
     * @param Integer $childListCount number of expected child lists
     * @param String $splitOrder order of split
     * @return Array if transaction is valid, error message 
     */
    public function validateSplit($listId,$listType,$childListCount,$splitOrder,$listMemberCount) {
        $valid = true;
        $errmessage = '';
        $bgprocess = false;

        $splitThreshold = !empty(Yii::$app->config->getAppThreshold('LISTS', 'splitList')) ? Yii::$app->config->getAppThreshold('LISTS', 'splitList') : 500;

        // too small split count
        if($childListCount <= 1){
            $valid = false && $valid;
            $errmessage = 'Invalid split count.';

            return ['valid'=>$valid,'message'=>$errmessage,'bgprocess'=>$bgprocess];
        }

        // too few list members
        if($listMemberCount <= 1){
            $valid = false && $valid;
            $errmessage = 'Insufficient number of list members for split.';

            return ['valid'=>$valid,'message'=>$errmessage,'bgprocess'=>$bgprocess];
        }

        // check # of list members v split count
        if($listMemberCount < $childListCount){
            $valid = false && $valid;
            $errmessage = 'Split count exceeds list member count.';

            return ['valid'=>$valid,'message'=>$errmessage,'bgprocess'=>$bgprocess];
        }

        // check if for bg processing
        if($listMemberCount > $splitThreshold){
            $bgprocess = true;
            $errmessage = 'Splitting of list with 500 or more members will be forwarded to the background process.';
            
            return ['valid'=>$valid,'message'=>$errmessage,'bgprocess'=>$bgprocess];
        }

        return ['valid'=>$valid,'message'=>$errmessage];
    }

    /**
     * Split list into specified number of child lists in background process
     * @param Integer $listId id of list to be split
     * @param Integer $childListCount number of expected child lists
     * @param String $splitOrder order of split
     * @return Array objects needed to render status of operation 
     */
    public function splitListBackground($listId,$childListCount,$splitOrder) {
        $userId = $this->user->getUserId();

        // create background job record
        $backgroundJobParams = [
            "workerName" => "SplitList",
            "description" => "Split list into new lists.",
            "entity" => "LIST",
            "entityDbId" => "".$listId,
            "endpointEntity" => "LIST",
            "application" => "LISTS",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()"
        ];

        if(isset($userId) && !empty($userId)){
            $backgroundJobParams["creatorDbId"] = $userId;
        }

        $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);

        if(isset($transaction) && !empty($transaction)){
            if(isset($transaction["status"]) && !empty($transaction["status"]) && $transaction["status"] != 200) {
                return json_encode([
                    "success" => false,
                    "error" => "Error while creating background process record. Kindly contact Administrator."
                ]);
            }
            else{
                if(isset($transaction["data"]) && !empty($transaction["data"])){
                    $backgroundJobDbId = $transaction["data"][0]["backgroundJobDbId"];

                    $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');                
                    $host = getenv('CB_RABBITMQ_HOST');
                    $port = getenv('CB_RABBITMQ_PORT');
                    $user = getenv('CB_RABBITMQ_USER');
                    $password = getenv('CB_RABBITMQ_PASSWORD');
        
                    $connection = new AMQPStreamConnection($host, $port, $user, $password);
        
                    $channel = $connection->channel();
        
                    $channel->queue_declare(
                        'SplitList', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                        false,  //passive - can use this to check whether an exchange exists without modifying the server state
                        true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                        false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                        false   //auto delete - queue is deleted when last consumer unsubscribes
                    );
        
                    $msg = new AMQPMessage(
                        json_encode([
                            "listDbId" => $listId,
                            "splitCount" => $childListCount,
                            "splitOrder" => $splitOrder,
                            "backgroundJobDbId" => $backgroundJobDbId,
                            "accessToken" => $accessToken,
                            "userId" => $userId,
                        ]),
                        array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                    );
        
                    $channel->basic_publish(
                        $msg,   //message 
                        '', //exchange
                        'SplitList'  //routing key (queue)
                    );
                    $channel->close();
                    $connection->close();
                    
                    return array("success" => true);
                }
            }
        }
        else{
            return json_encode([
                "success" => false,
                "error" => "Error while creating background process record. Kindly contact Administrator."
            ]);
        }
    }

    /**
     * Merge lists into a new list in background process
     * @param Array $listIdArr list ids to be merged
     * @param Integer $newListDbId id of new list
     * @param String $mode
     * @param Boolean $removeDuplicates 
     */
    public function mergeListsBackground($listDbIdArr, $newListDbId, $mode='copy', $removeDuplicates=false) {
        $userId = $this->user->getUserId();
        

        // create background job record
        $backgroundJobParams = [
            "workerName" => "MergeLists",
            "description" => "Merge lists into new list.",
            "entity" => "LIST",
            "entityDbId" => "".$newListDbId,
            "endpointEntity" => "LIST",
            "application" => "LISTS",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()"
        ];

        if(isset($userId) && !empty($userId)){
            $backgroundJobParams["creatorDbId"] = $userId;
        }

        $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);

        if(isset($transaction) && !empty($transaction)){
            if (isset($transaction["status"]) && !empty($transaction["status"]) && $transaction["status"] != 200) {
                return json_encode([
                    "success" => false,
                    "error" => "Error while creating background process record. Kindly contact Administrator."
                ]);
            }
            else{
                if(isset($transaction["data"]) && !empty($transaction["data"])){
                    $backgroundJobDbId = $transaction["data"][0]["backgroundJobDbId"];
    
                    $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
                    $host = getenv('CB_RABBITMQ_HOST');
                    $port = getenv('CB_RABBITMQ_PORT');
                    $user = getenv('CB_RABBITMQ_USER');
                    $password = getenv('CB_RABBITMQ_PASSWORD');
        
                    $connection = new AMQPStreamConnection($host, $port, $user, $password);
        
                    $channel = $connection->channel();
        
                    $channel->queue_declare(
                        'MergeLists', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                        false,  //passive - can use this to check whether an exchange exists without modifying the server state
                        true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                        false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                        false   //auto delete - queue is deleted when last consumer unsubscribes
                    );
        
                    $msg = new AMQPMessage(
                        json_encode([
                            "listDbIds" => $listDbIdArr,
                            "newListDbId" => $newListDbId,
                            "mode" => $mode,
                            "removeDuplicates" => $removeDuplicates,
                            "backgroundJobDbId" => $backgroundJobDbId,
                            "accessToken" => $accessToken,
                            "userId" => $userId,
                        ]),
                        array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                    );
        
                    $channel->basic_publish(
                        $msg,   //message 
                        '', //exchange
                        'MergeLists'  //routing key (queue)
                    );
                    $channel->close();
                    $connection->close();
                    
                    return json_encode(array("success" => true));
                }
            }
        }
        else{
            return json_encode([
                "success" => false,
                "error" => "Error while creating background process record. Kindly contact Administrator."
            ]);
        }
    }
}