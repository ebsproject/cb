<?php

/* 
 * This file is part of Breeding4Rice. 
 * 
 * Breeding4Rice is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * Breeding4Rice is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

namespace app\modules\account\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\modules\account\models\ListModel;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\dynagrid\DynaGrid;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

use app\interfaces\modules\account\models\IListMemberModel;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\models\ILists;
use app\interfaces\models\IConfig;

use app\models\User;
use app\models\Variable;
use app\models\BackgroundJob;
use app\models\Person;
use app\models\Program;
use app\models\Config;

/**
 * Class for all list members related methods
 */

class ListMemberModel extends ListModel implements IListMemberModel
{

    public $user;
    public $backgroundJob;

    public function __construct(
        User $user,
        BackgroundJob $backgroundJob,
        public IUserDashboardConfig $userDashboardConfig,
        public Variable $variableModel,
        public IConfig $configModel,
        public Person $personModel,
        public Program $programModel,
        public Config $configuration,
        $config = []
    ) {

        $this->user = $user;
        $this->backgroundJob = $backgroundJob;
        $this->personModel = $personModel;
        $this->programModel = $programModel;
        $this->configuration = $configuration;
    }

    /**
     * Retrieve variable from identified config
     * 
     * @return mixed
     */
    public function retrieveVariablesFromConfig($programCode = null)
    {
        // Retrieve process thresholds
        $baseConfigAbbrevSuffix = '_VARIABLE_COLUMNS_CONFIG';

        $configValues = $this->getConfigValues(
            $programCode,
            'LM_GLOBAL' . $baseConfigAbbrevSuffix,
            $baseConfigAbbrevSuffix
        );

        $validVarArr = [];
        foreach ($configValues as $varConfig) {
            $varConfigInfo = $this->validateVariable($varConfig['abbrev']);

            if (!empty($varConfigInfo)) {
                $varConfig['label'] = $varConfigInfo['label'];
                $varConfig['description'] = $varConfigInfo['description'];
                $varConfig['display_name'] = $varConfigInfo['displayName'];

                array_push($validVarArr, $varConfig);
            }
        }

        return $validVarArr;
    }

    /**
     * Check if variable has corresponding variable record, given abbrev value
     * 
     * @param String abbrev Abbrev value of config variable
     * 
     * @return mixed
     */
    public function validateVariable($abbrev)
    {
        $variableInfo = $this->variableModel->getVariableByAbbrev($abbrev);

        if (empty($variableInfo)) {
            return [];
        }

        return [
            'label' => $variableInfo[0]['label'] ?? $variableInfo['label'],
            'description' => $variableInfo[0]['description'] ?? $variableInfo['description'],
            'displayName' => $variableInfo[0]['displayName'] ?? $variableInfo['displayName']
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param string $type list type
     * @param integer $id list ID
     * @param boolean $is_void if list items should be voided or not
     * @param array $params contains the different filter options
     * @param boolean $pagination whether the pagination of the browser should be set
     * @param boolean $isView whether the intended target for the dataprovider is the modal view or not
     * @param string $searchModel the name of the search model that is used for filtering
     *
     * @return ArrayDataProvider
     */
    public function search(string $type=null, $id, $isVoid=null, $params=null, $pagination=false, $isView=false, $searchModel='List') {
        $this->load($params);

        /** get current page number and page size */
        $paramPage = '';
        $paramLimit = (!$isView ? 'limit=10' : '');
        $dataProviderId = isset($isVoid) ? ($isVoid == 'false' ? 'dynagrid-list-manager-all-items-grid' : 'dynagrid-list-manager-final-grid') : 'dynagrid-list-manager-preview-grid';

        if (isset($_GET[$dataProviderId . '-page'])){
            $paramPage = '&page=' . $_GET[$dataProviderId . '-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        }

        $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

        $param = [];
        $url = !$isView ? 'lists/'.$id.'/members-search?sort=orderNumber&'.$paramLimit.$paramPage : 'lists/'.$id.'/members-search?sort=orderNumber';

        if (!is_null($isVoid)) {
            $param['isActive'] = $isVoid;
        }

        // parse filters
        if (!is_string($searchModel)) {
            throw new \yii\web\HttpException(500,"Please provide only the string name of the class!");
        }

        // check if advanced filters are set
        if (isset($_GET['Advance'])) {
            $advancedFilters = $_GET['Advance'];
            if (isset($advancedFilters['min']) && isset($advancedFilters['max'])) {
                $param['volume'] = [
                    "range" => $advancedFilters['min'] . "|" . $advancedFilters['max']
                ];
            }
            if (isset($advancedFilters['study']) && !empty($advancedFilters['study'])) {
                $studiesStr = str_replace(',', '|', $advancedFilters['study']);
                $param['study'] = $studiesStr;
            }
            if (isset($advanceFilters['year']) && !empty($advanceFilters['year'])) {
                $yearStr = str_replace(',', '|', $advancedFilters['year']);
                $param['year'] = $yearStr;
            }
            if (isset($advanceFilters['container_label']) && !empty($advanceFilters['container_label'])) {
                $labelStr = str_replace(',', '|', $advancedFilters['container_label']);
                $param['container_label'] = $labelStr;
            }
            if (isset($advanceFilters['studyName']) && !empty($advanceFilters['studyName'])) {
                $studyNamesStr = str_replace(',', '|', $advancedFilters['studyName']);
                $param['studyName'] = $studyNamesStr;
            }

            if (isset($advanceFilters['product_name']) && !empty($advanceFilters['product_name'])) {
                $productNameStr = str_replace(',', '|', $advancedFilters['product_name']);
                $param['product_name'] = $productNameStr;
            }

            if (isset($advanceFilters['entcode']) && !empty($advanceFilters['entcode'])) {
                $productNameStr = str_replace(',', '|', $advancedFilters['product_name']);
                $param['product_name'] = $productNameStr;
            }
        }

        $filters = isset($params[$searchModel]) ? $params[$searchModel]: null;

        if (!is_null($filters)) {
            foreach ($filters as $key => $value) {
                $prepend = str_contains($value,'%') ? '' : 'equals';
                $append = !empty($prepend) ? '' : "%";

                if (!empty($value)){
                    $param[$key] = $prepend. ' ' . $value . $append;
                }

                if ($key === 'quantity' && $value === '0'){
                    $param[$key] = $value;
                }
            }
        }

        if (empty($param)) {
            $param = '';
        }

        $data = Yii::$app->api->getResponse('POST',$url,json_encode($param));

        // get lists data provider
        $result = [];
        $totalCount = 0;
        if(isset($data) && !empty($data)){
            if (isset($data['status']) && $data['status'] == 200) {
                $data = $data['body'];
                $totalPages = (isset($data['metadata']['pagination']['totalPages'])) ? $data['metadata']['pagination']['totalPages'] : null;
                $totalCount = (isset($data['metadata']['pagination']['totalCount'])) ? $data['metadata']['pagination']['totalCount'] : null;
                $result = (isset($data['result']['data'])) ? $data['result']['data'] : [];

                // build data provider
                // if isView is true, it'll only get the 1st batch of items,
                // but if not then it should continue to retrieve the rest of the items
                if($totalPages > 1 && !$isView && $paramLimit == ''){       
                    for ($i=2; $i <= $totalPages; $i++) {
                        $data = Yii::$app->api->getResponse('POST',$url.'&page='.$i, json_encode($param));                
                        if($data['status'] == 200) {
                            $data = $data['body'];
                            $addlResult = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                        }
    
                        $result = array_merge($result,$addlResult);
                    }
                }

                // check if need to redirect browser page
                $currentPage = $data['metadata']['pagination']['currentPage'];
                if(count($result) == 0 && $totalCount == 0 && $currentPage > 1){
                    $url = 'lists/'.$id.'/members-search?sort=orderNumber&'.$paramLimit;
                    $data = Yii::$app->api->getResponse('POST',$url, json_encode($param));                
                    
                    if($data['status'] == 200) {
                        $data = $data['body'];
                        $result = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                        $totalCount = $data['metadata']['pagination']['totalCount'];

                        // redirect to 1st page
                        $_GET['page'] = 1;
                        $_GET[$dataProviderId . '-page'] = 1;
                    }
                }
            }
        }

        $options = [
            'key' => 'listMemberDbId',
            'allModels' => $result,
            'totalCount' => $totalCount,
            'restified' => true,
            'pagination' => [ 'pageSize' => 10 ]
        ];

        return new ArrayDataProvider($options);

    }

   /**
    * Retrieve columns for review and preview page for list_type = designation
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getDesignationMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false,$orderNumber=true,$isFinalList=true) {
        $checkBoxVisible = ($isPreview) ?  false : true;
        $reorderVisible = ($isFinalList) ? true : false;

        $columns = [
            'germplasmName' => 'String',
            'germplasmCode' => 'String',
            'germplasmNameType' => 'String',
            'generation' => 'String',
            'otherNames' => 'String',
            'parentage' => 'String'
        ];

        Yii::$app->session->set("list_members-columns",$columns);

        return [
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" class="filled-in" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="filled-in '.$checkBoxClass.'" type="checkbox" id="'.$data["listMemberDbId"].'" />
                        <label for="'.$data["listMemberDbId"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
                'visible' => $orderNumber
            ],
            [
                'attribute' => 'germplasmName',
                'label' => 'Germplasm Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'germplasmCode',
                'label' => 'Germplasm Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'germplasmNameType',
                'label' => 'Germplasm Name Type',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'generation',
                'label' => 'Generation',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'otherNames',
                'label' => 'Other Names',
                'visible' => true,
                'format' => 'raw',
                'value' => function ($data) {

                    if (strlen($data['otherNames']) > 30) {
                        $displayNames = substr($data['otherNames'],0,30);
                        $otherNames = $data['otherNames'];

                        $display = '

                            <ul class="collapsible">
                                <li>
                                  <div class="collapsible-header" style="padding:0.5rem; color: #26a69a;">'.$displayNames.'</div>
                                  <div class="collapsible-body">'.$otherNames.'</div>
                                </li>
                              </ul>

                        ';
                    } else {
                        $display = $data['otherNames'];
                    }

                    return $display;
                }
            ],
            [
                'attribute' => 'parentage',
                'label' => 'Parentage',
                'visible' => true,
                'format' => 'raw'
            ],
        ];
    }

    /**
    * Retrieve columns for review and preview page for list_type = trait
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getTraitMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false,$orderNumber=true,$isFinalList=true) {
        $checkBoxVisible = ($isPreview) ?  false : true;
        $reorderVisible = ($isFinalList) ? true : false;

        $columns = [
            'abbrev' => 'String',
            'label' => 'String',
            'name' => 'String',
            'displayName' => 'String',
            'dataType' => 'String',
            'dataLevel' => 'String'
        ];

        Yii::$app->session->set("list_members-columns",$columns);

        return [
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" class="filled-in" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="filled-in '.$checkBoxClass.'" type="checkbox" id="'.$data["listMemberDbId"].'" />
                        <label for="'.$data["listMemberDbId"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
                'visible' => $orderNumber
            ],
            [
                'attribute' => 'abbrev',
                'label' => 'Abbrev',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'label',
                'label' => 'Label',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'name',
                'label' => 'Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'displayName',
                'label' => 'Display Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'dataType',
                'label' => 'Data Type',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'dataLevel',
                'label' => 'Data Level',
                'visible' => true,
                'format' => 'raw'
            ]
        ];
    }

    /**
    * Retrieve columns for review and preview page for list_type = package
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getPackageMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false,$orderNumber=true,$isFinalList=true) {
        $checkBoxVisible = ($isPreview) ?  false : true;
        $reorderVisible = ($isFinalList) ? true : false;

        $columns = [
            'experimentOccurrence' => 'String',
            'experiment' => 'String',
            'seedName' => 'String',
            'seedCode' => 'String',
            'seedProgram' => 'String',
            'program' => 'String',
            'label' => 'String',
            'germplasmName' => 'String',
            'germplasmCode' => 'String',
            'parentage' => 'String',
            'seedSourcePlotCode' => 'String',
            'quantity' => 'Integer',
            'unit' => 'String'
        ];

        Yii::$app->session->set("list_members-columns",$columns);

        return [
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" class="filled-in" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="filled-in '.$checkBoxClass.'" type="checkbox" id="'.$data["listMemberDbId"].'" />
                        <label for="'.$data["listMemberDbId"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
                'visible' => $orderNumber
            ],
            [
                'attribute' => 'experimentOccurrence',
                'label' => 'Occurrence',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'experiment',
                'label' => 'Experiment',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'seedName',
                'label' => 'Seed Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'seedCode',
                'label' => 'Seed Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'program',
                'label' => 'Program',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'label',
                'label' => 'Label',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'germplasmName',
                'label' => 'Germplasm Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'germplasmCode',
                'label' => 'Germplasm Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'seedSourcePlotCode',
                'label' => 'Source Plot Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'quantity',
                'label' => 'Quantity',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'unit',
                'label' => 'Unit',
                'visible' => true,
                'format' => 'raw'
            ]
        ];
    }

    /**
    * Retrieve columns for review and preview page for list_type = plot
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getPlotListMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false, $orderNumber=true,$isFinalist=true) {

        $checkBoxVisible = ($isPreview) ? false : true;
        $reorderVisible = ($isFinalist) ?  true : false;

        $columns = [
            'occurrenceName' => 'String',
            'germplasmName' => 'String',
            'germplasmCode' => 'String',
            'plotCode' => 'String',
            'plotNumber' => 'Integer',
            'entryNumber' => 'Integer',
            'entryCode' => 'String'
        ];

        Yii::$app->session->set("list_members-columns",$columns);

        return [
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" class="filled-in" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="filled-in '.$checkBoxClass.'" type="checkbox" id="'.$data["listMemberDbId"].'" />
                        <label for="'.$data["listMemberDbId"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
                'visible' => $orderNumber
            ],
            [
                'attribute' => 'occurrenceName',
                'label' => 'Occurrence Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'germplasmName',
                'label' => 'Germplasm Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'germplasmCode',
                'label' => 'Germplasm Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'plotCode',
                'label' => 'Plot Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'plotNumber',
                'label' => 'Plot No.',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'entryNumber',
                'label' => 'Entry Number',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'entryCode',
                'label' => 'Entry Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [   
                'attribute'=>'creationTimestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];
    }

    /**
    * Retrieve columns for review and preview page for list_type = seed
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getSeedListMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false, $orderNumber=true, $isFinalList=true) {

        $checkBoxVisible = ($isPreview) ? false : true;
        $reorderVisible = ($isFinalList) ? true : false;

        $columns = [
            'germplasmName' => 'String',
            'germplasmCode' => 'String',
            'seedCode' => 'String',
            'seedName' => 'String',
            'programName' => 'String',
            'experimentYear' => 'Integer',
            'experimentStageCode' => 'String'
        ];

        Yii::$app->session->set("list_members-columns",$columns);

        return [
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" class="filled-in" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>

                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="filled-in '.$checkBoxClass.'" type="checkbox" id="'.$data["listMemberDbId"].'" />
                        <label for="'.$data["listMemberDbId"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
                'visible' => $orderNumber
            ],
            [
                'attribute' => 'germplasmName',
                'label' => 'Germplasm Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'germplasmCode',
                'label' => 'Germplasm Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'seedCode',
                'label' => 'Seed Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'seedName',
                'label' => 'Seed Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'programName',
                'label' => 'Program Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'experimentYear',
                'label' => 'Experiment Year',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'experimentStageCode',
                'label' => 'Experiment Stage Code',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute'=>'creationTimestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];
    }

    /**
    * Retrieve columns for review and preview pages for list_type = location
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getLocationMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false, $orderNumber=true, $isFinalist=true) {

        $checkBoxVisible = ($isPreview) ? false : true;
        $reorderVisible = ($isFinalist) ? true : false;

        $columns = [
            'abbrev' => 'String',
            'name' => 'String',
            'country' => 'String',
            'status' => 'String'
        ];

        Yii::$app->session->set("list_members-columns",$columns);

        return [
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" class="filled-in" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="filled-in '.$checkBoxClass.'" type="checkbox" id="'.$data["listMemberDbId"].'" />
                        <label for="'.$data["listMemberDbId"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
                'visible' => $orderNumber
            ],
            [
                'attribute' => 'abbrev',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'country',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'visible' => true,
                'header' => 'Status',
                'content' => function($data) {
                    return '<span class="badge '. (($data['status']) ? ' new">Active' : ' new badge amber darken-2">Draft') . '</span>';
                },
                'noWrap'=>false,
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>[
                    'true' =>'Active',
                    'false' =>'Draft',
                ], 
                'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true,'id'=>"location_active"],],
                'filterInputOptions'=>['placeholder'=>'status','id'=>"location_active"],
            ],
            [
                'attribute'=>'creationTimestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];
    }

    /**
    * Retrieve columns for review and preview page for list_type = study
    * @param Class $model for browser
    * @param Integer $checkBoxId ID for the checkboxes
    * @param String $checkBoxClass class for checkbox
    * @param boolean $isPreview if action is preview
    * @param boolean $orderNumber if order number should be displayed
    * @param boolean $isFinalList if browser is for finalized items
    * @return array columns for browser
    **/

    public static function getStudyMemberColumns($model, $checkBoxId, $checkBoxClass, $isPreview=false, $orderNumber=true, $isFinalist=true) {

        $checkBoxVisible = ($isPreview) ? false : true;
        $reorderVisible = ($isFinalist) ?  true : false;

        $columns = [
            'programAbbrev' => 'String',
            'name' => 'String',
            'study' => 'String',
            'placeAbbrev' => 'String',
            'phaseAbbrev' => 'String',
            'year' => 'Integer',
            'season' => 'String'
        ];

        Yii::$app->session->set("list_members-columns",$columns);

        return [
            [
                'label'=> "checkbox",
                'visible'=> $checkBoxVisible,
                'header'=>'
                    <input type="checkbox" class="filled-in" data-id-string="" id="'.$checkBoxId.'" />
                    <label for="'.$checkBoxId.'"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content'=>function($data) use ($checkBoxClass) {

                    return '
                        <input class="filled-in '.$checkBoxClass.'" type="checkbox" id="'.$data["listMemberDbId"].'" />
                        <label for="'.$data["listMemberDbId"].'"></label>
                    
                    ';
                },
                'hAlign'=>'center',
                'vAlign'=>'middle',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'width'=>'50px'
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
                'visible' => $orderNumber
            ],
            [
                'attribute' => 'programAbbrev',
                'label' => 'Program',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'name',
                'label' => 'Name',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'study',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'placeAbbrev',
                'label' => 'Location',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'phaseAbbrev',
                'label' => 'Phase',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'year',
                'label' => 'Year',
                'visible' => true,
                'format' => 'raw'
            ],
            [
                'attribute' => 'season',
                'label' => 'Season',
                'visible' => true,
                'format' => 'raw'
            ],
            [   
                'attribute'=>'creationTimestamp',
                'format' => 'date',
                'visible' => false,
                
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [

                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton'=>true,
                        'todayHighlight' => true,
                        'showButtonPanel'=>true, 

                        'clearBtn' => true,
                        'options'=>['type'=>DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton'=>true,
                        ]
                    ],
                ],
            ]
        ];
    }

    /**
     * Update order_number of list members according to sorting configuration
     * @param Integer listId id of associated list
     * @param String sort sorting configuration
     * 
     * @return Boolean true
     */
    public function reorderListMembersBySortConfig($listId,$sort) {
        if(isset($sort) && !empty($sort)){
            // retrieving list members
            $listMembers = Yii::$app->api->getResponse('POST','lists/'.$listId.'/members-search'.$sort,'');

            if($listMembers['status'] === 200 && isset($listMembers['body']['result']['data']) && !empty($listMembers['body']['result']['data'])){
                // update list members order_number according to sorting configuration
                $listMembers = $listMembers['body']['result']['data'];

                $memberCount = 1;
                foreach($listMembers as $listMember){
                    $params = [
                        "id" => "".$listMember['listMemberDbId'],
                        "displayValue" => "".$listMember['displayValue'],
                        "remarks" => "updated order number",
                        "listDbId" => "".$listId,
                        "orderNumber" =>  "".$memberCount
                    ];

                    Yii::$app->api->getResponse('PUT','list-members/'.$listMember['listMemberDbId'],json_encode($params));

                    $memberCount += 1;
                }
            }
        }
        return true;
    }

    /**
     * Trigger bakcground process to update order_number of list members according to sorting configuration
     * @param Integer listId id of associated list
     * @param String sort sorting configuration
     * 
     * @return Boolean true
     */
    public function reorderListMembersBySortConfigBgProcess($listId,$sort) {
        $userId = $this->user->getUserId();

        if(isset($sort) && !empty($sort)){
            $requestDataParams = [
                'processName' => "reorder-all-by-sort",
                'listDbId' => "".$listId,
                'sort' => "".$sort
                
            ];

            // create background job record
            $backgroundJobParams = [
                "workerName" => "UpdateListItems",
                "description" => "Update list and list member records.",
                "entity" => "LIST",
                "entityDbId" => "".$listId,
                "endpointEntity" => "LIST",
                "application" => "LISTS",
                "method" => "POST",
                "jobStatus" => "IN_QUEUE",
                "startTime" => "NOW()"
            ];

            if(isset($userId) && !empty($userId)){
                $backgroundJobParams["creatorDbId"] = $userId;
            }

            $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);

            if(isset($transaction) && !empty($transaction)){
                if(isset($transaction["status"]) && !empty($transaction["status"]) && $transaction["status"] != 200) {
                    return json_encode([
                        "success" => false,
                        "error" => "Error while creating background process record. Kindly contact Administrator."
                    ]);
                }
                else{
                    if(isset($transaction["data"]) && !empty($transaction["data"])){
                        $backgroundJobDbId = $transaction["data"][0]["backgroundJobDbId"];

                        // Set token object values
                        $token = Yii::$app->session->get('user.b4r_api_v3_token');
                        $refreshToken = Yii::$app->session->get('user.refresh_token');
                        $tokenObject = [
                            "token" => $token,
                            "refreshToken" => $refreshToken
                        ];

                        $host = getenv('CB_RABBITMQ_HOST');
                        $port = getenv('CB_RABBITMQ_PORT');
                        $user = getenv('CB_RABBITMQ_USER');
                        $password = getenv('CB_RABBITMQ_PASSWORD');
            
                        $connection = new AMQPStreamConnection($host, $port, $user, $password);
            
                        $channel = $connection->channel();
            
                        $channel->queue_declare(
                            'UpdateListItems', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                            false,  //passive - can use this to check whether an exchange exists without modifying the server state
                            true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                            false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                            false   //auto delete - queue is deleted when last consumer unsubscribes
                        );
            
                        $msg = new AMQPMessage(
                            json_encode([
                                "listId" => $listId,
                                "backgroundJobId" => $backgroundJobDbId,
                                "requestData" => $requestDataParams,
                                "tokenObject" => $tokenObject,
                                "userId" => $userId,
                            ]),
                            array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                        );
            
                        $channel->basic_publish(
                            $msg,   //message 
                            '', //exchange
                            'UpdateListItems'  //routing key (queue)
                        );
                        $channel->close();
                        $connection->close();
                        
                        return array("success" => true);
                    }
                }
            }
            else{
                return json_encode([
                    "success" => false,
                    "error" => "Error while creating background process record. Kindly contact Administrator."
                ]);
            }
        }
        return true;
    }

    /**
     * Retrieve lst member columns
     * 
     * @param String $listType type of list
     * @param Array $colVarArr array of column variables
     * @param ArrayDataProvider $model dataprovider
     * @param Array $checkBoxData
     * @param Array $configData
     * @param String $action
     * 
     * @return mixed
     * 
     */
    public static function getListMemberColumns($listType, $colVarArr, $model, $checkBoxData, $configData, $action = 'view')
    {

        $checkBoxId = isset($checkBoxData['checkBoxId']) ? $checkBoxData['checkBoxId'] : '';
        $checkBoxClass = isset($checkBoxData['checkBoxClass']) ? $checkBoxData['checkBoxClass'] : '';

        $isPreview = isset($configData['isPreview']) ? $configData['isPreview'] : false;
        $showOrderNumber = isset($configData['showOrderNumber']) ? $configData['showOrderNumber'] : true;
        $isFinalList = isset($configData['isFinalList']) ? $configData['isFinalList'] : true;

        $checkBoxVisible = ($isPreview) ?  false : true;
        $reorderVisible = ($isFinalList) ? true : false;

        $outputVisibility = 'visible_browser';
        if ($action == 'export') {
            $outputVisibility = 'visible_export';
        }

        // Extract and build columns
        $columnVariables = array_filter($colVarArr, function ($x) use ($listType, $outputVisibility) {
            $columnOptionsInfo = $x['column_options'][$listType] ?? $x['column_options']['default'];

            $isVisible = $columnOptionsInfo[$outputVisibility] == "true";
            $inListTypeArr = in_array($listType, explode(',', $x['list_type']));

            return $inListTypeArr && $isVisible;
        });

        $columns = [];
        foreach ($columnVariables as $columnVar) {
            $columns[$columnVar['api_field_name']] = ucfirst($columnVar['data_type']);
        }

        Yii::$app->session->set("list_members-columns", $columns);

        $baseBrowserColumns = [
            [
                'label' => "checkbox",
                'visible' => $checkBoxVisible,
                'header' => '
                    <input type="checkbox" class="filled-in" data-id-string="" id="'
                    . $checkBoxId . '" />
                    <label for="' . $checkBoxId . '"></label>
                ',
                'contentOptions' => ['style' => ['max-width' => '100px;',]],
                'content' => function ($data) use ($checkBoxClass) {

                    return '
                        <input class="filled-in ' . $checkBoxClass
                        . '" type="checkbox" id="'
                        . $data["listMemberDbId"] . '" />
                        <label for="' . $data["listMemberDbId"] . '"></label>
                    
                    ';
                },
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'hiddenFromExport' => true,
                'mergeHeader' => true,
                'width' => '50px'
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order' => DynaGrid::ORDER_FIX_LEFT,
                'visible' => $showOrderNumber
            ]
        ];

        $browserColumns = $baseBrowserColumns;

        $additionalColumns = [];
        foreach ($columnVariables as $column) {
            array_push($additionalColumns, [
                'attribute' => $column['api_field_name'],
                'label' => $column['label'],
                'visible' => true,
                'format' => 'raw'
            ]);
        }

        if (!empty($additionalColumns)) {
            $browserColumns = array_merge($baseBrowserColumns, $additionalColumns);
        }

        return $browserColumns;
    }

    /**
     * Retrieves config values
     * 
     * @param String $program program code
     * @param String $globalConfigAbbrev global config abbrev
     * 
     * @return mixed
     */
    public function getConfigValues($program, $globalConfigAbbrev, $baseConfigAbbrevSuffix)
    {
        $cropConfig = [];
        $programConfig = [];
        $roleConfig = [];
        $config = [];

        $baseConfigAbbrevPrefix = 'LM_';

        // if program is provided
        if (isset($program) && !empty($program)) {
            // get crop code
            $cropCode = $this->programModel->getCropCode($program);

            // get crop level config
            if (!empty($cropCode)) {
                $abbrev = $baseConfigAbbrevPrefix . 'CROP_' . (strtoupper($cropCode)) . $baseConfigAbbrevSuffix;
                $cropConfig = $this->configuration->getConfigByAbbrev($abbrev);

                if (!empty($cropConfig)) {
                    $config = $cropConfig;
                }
            }

            if (empty($cropConfig)) {
                // get program level config
                $abbrev = $baseConfigAbbrevPrefix . 'PROGRAM_' . (strtoupper($program)) . $baseConfigAbbrevSuffix;
                $programConfig = $this->configuration->getConfigByAbbrev($abbrev);

                if (!empty($programConfig)) {
                    $config = $programConfig;
                }
            }
        }

        // get role level config
        if (empty($cropConfig) && empty($programConfig)) {
            // get user role
            [$role, $isAdmin, $userId] = $this->personModel->getPersonRoleAndType($program);

            // get role config
            $abbrev = $baseConfigAbbrevPrefix . 'ROLE_' . (strtoupper($role)) . $baseConfigAbbrevSuffix;
            $roleConfig = $this->configuration->getConfigByAbbrev($abbrev);

            if (!empty($roleConfig)) {
                $config = $roleConfig;
            }
        }

        if (empty($cropConfig) && empty($programConfig) && empty($roleConfig)) {
            // get global config
            $globalConfig = $this->configuration->getConfigByAbbrev($globalConfigAbbrev);

            if (!empty($globalConfig)) {
                $config = $globalConfig;
            }
        }

        return $config;
    }
}
