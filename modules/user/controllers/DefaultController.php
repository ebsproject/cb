<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Contains methods for managing users
 */

namespace app\modules\user\controllers;

use Yii;
use app\components\B4RController;
use app\models\User;
use app\models\UserSearch;
use app\models\Team;
use yii\web\NotFoundHttpException;
use yii\widgets\DetailView;

/**
 * Default controller for the `user` module
 */
class DefaultController extends B4RController
{
    protected $user;
    protected $userSearch;
    protected $team;

    public function __construct (
        $id,
        $module,
        User $user,
        UserSearch $userSearch,
        Team $team,
        $config=[]
    )
    {
        $this->user = $user;
        $this->userSearch = $userSearch;
        $this->team = $team;

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     * 
     * @return mixed render info
     */
    public function actionIndex (int $userId = null, $program)
    {
        Yii::$app->session->setFlash('error', Yii::t('app', "Page not found. Kindly contact your administrator."));
        return $this->goHome();

        //set user id to session
        if (isset($userId) && !empty($userId)) {
            $session = Yii::$app->session;
            $session->set('userId', $userId);
        }

        // Check if at least 1 field has a value
        $userSearchFilters = isset($_GET['UserSearch']) ?
            $_GET['UserSearch'] : [];
        $hasFilters = false;
        foreach ($userSearchFilters as $key => $value)
            if ($value != "") $hasFilters = true;

        if (isset($userSearchFilters) && $hasFilters) {
            $searchParams = [];

            foreach ($userSearchFilters as $key => $value)
                if ($value != "") $searchParams[$key] = $value;

            $searchParams = ['UserSearch' => $searchParams];

            if (!empty($searchParams))
                $dataProvider = $this->userSearch->search($searchParams);
        } else {
            $dataProvider = $this->user->getUsersArrayDataProvider();
        }

        $teamsTags = $this->team->getAllTeamTags([]);
        $rolesTags = $this->team->getAllRoleTags();

        // View all persons
        // Renders persons browser
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $this->user,
            'searchModel' => $this->userSearch,
            'program' => $program,
            'teamsTags' => $teamsTags,
            'rolesTags' => $rolesTags
        ]);
    }

    /**
     * Creates a user/person
     * 
     * @return mixed JSON object containing flags and user info
     */
    public function actionCreate ()
    {

        if ($this->user->load(Yii::$app->request->post())) {
            // Create a user
            $post = Yii::$app->request->post();
            $userParams = $post['User'];

            // Build POST params
            $processedBodyParams = $this->user->processUserParams(
                $userParams,
                'POST'
            );

            // Check if createdUser has a non-falsy value
            $createdUser = $this->user->createUser($processedBodyParams);
            $fullName = $processedBodyParams['records'][0]['firstName'] . ' ' . $processedBodyParams['records'][0]['lastName'];
            $userDbId = null;
            $result = false;

            if ($createdUser) {
                $userDbId = $createdUser['userDbId'];

                $result = true;
            } else {
                $result = false;
            }

            if (isset($_POST['Form']) && $result) {
                return json_encode([
                    'success' => $result,
                    'name' => $fullName,
                    'error' => [],
                    'create_another' => $_POST['Form']['create_another'],
                    'userDbId' => $userDbId
                ]);
            } else if ($result) {
                Yii::$app->session->setFlash(
                    'success',
                    '<i class="fa fa-check"></i> You successfully created <b>' .
                        $fullName .
                    '\'s</b> account!');
                return json_encode([
                    'success' => $result,
                    'name' => $fullName,
                    'error' => [],
                    'create_another' => "0",
                    'userDbId' => $userDbId
                ]);
            } else {
                return json_encode(['success' => false]);
            }
        }
    }

    /**
     * Displays a single User.
     * 
     * @param integer id
     * 
     * @return string HTML snippet
     */
    public function actionView ()
    {
        $id = $_POST['user_id'];
        $data = $this->user->getUser($id);
        $teams = $this->user->getUserTeamsInfo($id);

        if (
            isset($teams['body']['metadata']['pagination']['totalCount']) &&
            $teams['body']['metadata']['pagination']['totalCount'] == 0
        ) {
            $display = '<p>No teams to display</p>';
        } else {
            //loop through teams
            $display = '<ul>';

            foreach ($teams as $key => $value) {

                if (!empty($value['team'])) {
                    $display .= '<li'.'">
                        <div class="row" style="margin-bottom:auto"><a><span style="font-weight:bold" class="title">'.
                            $value['team'] .
                        '</span>
                        <span class="text-muted">' . $value['role'] . '</span></a>
                        <a href="#" title="Remove user from team" data-user_id="' .
                        $id .
                        '" data-team_member_id="' .
                        $value['teamMemberId'] .
                        '" class="pull-left remove-member-link"><i class="material-icons">delete</i></a>&emsp;</div>
                    ';
                }
            }
            $display .= '</ul>';
        }

        $response['grid'] = '
            <div style="margin-bottom: 16px;">
                <span>Below displays the basic and audit information about the person.</span>
            </div>'.
            '<label>Basic Information</label>'.
            DetailView::widget([
                'model' => $data,
                'attributes' => [
                    'email',
                    'username',
                    'firstName',
                    'lastName',
                    [
                        'label' => 'Full Name',
                        'value' => function ($model) {
                            return $model['personName'];
                        },
                    ],
                    [
                        'label' => 'Type',
                        'value' => function ($model) {
                            return $model['personType'];
                        },
                    ],
                    [
                        'label' => 'Is Active?',
                        'value' => function ($model) {
                            return ($model['isActive'] == 1) ? 'Yes' : 'No';
                        },
                    ],
                ],
            ]) . '
            <label>Audit Information</label>' .
            DetailView::widget([
                'model' => $data,
                'attributes' => [
                    [
                        'label' => 'Creator',
                        'value' => function ($model) {
                            return $model['creator'];
                        },
                    ],
                    'creationTimestamp:datetime',
                    [
                        'label' => 'Modifier',
                        'value' => function ($model) {
                            return $model['modifier'];
                        },
                    ],
                    'modificationTimestamp:datetime',

                ],
            ]) . '
            <label>Teams</label>' .
            $display;

        return json_encode($response);
    }

    /**
     * Renders update user form
     * 
     * @param integer $id
     * 
     * @return mixed
     */
    public function actionUpdateUserForm ($program)
    {
        $id = $_POST['user_id'];
        $model = $this->findModel($id);

        $htmlData = $this->renderAjax('update', [
            'personDbId' => $id,
            'model' => $model,
            'program' => $program
        ], true, false);

        return json_encode([
            'htmlData' => $htmlData
        ]);
    }

    /**
     * Updates a registered User.
     * If update is successful, the browser will be redirected to the 'browse' page.
     * 
     * @param integer $id
     * @param $email salutation firstName middleName lastName validStartDate validEndDate remarks userType
     * 
     * @return mixed
     */
    public function actionUpdate ($id, $program)
    {
        $oldModel = $this->user->getUser($id);

        if ($this->user->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $userParams = $post['User'];

            // build PUT parameters
            $processedBodyParams = $this->user->processUserParams($userParams);

            $result = $this->user->updateUser($id, $processedBodyParams);

            if ($result) {
                $updatedModel = $this->user->getUser($id);

                Yii::$app->session->setFlash(
                    'success',
                    '<i class="fa fa-check"></i> You successfully updated <b>' .
                        $updatedModel['firstName'] . ' ' . $updatedModel['lastName'] .
                    '\'s</b> account details!'
                );
                
                return json_encode([
                    'success' => $result,
                    'name' => $updatedModel['firstName'] . ' ' . $updatedModel['lastName'],
                    'error' => []
                ]);
            } else {
                return json_encode([
                    'success' => false,
                    'name' => $oldModel['firstName'] . ' ' . $oldModel['lastName'], 
                    'error' => []
                ]);
            }
        }

        return $this->redirect(['default/index', 'program' => $program]);
    }

    /**
     * Deletes a registeted User.
     * 
     * @param integer $id
     * 
     * @return mixed
     */
    public function actionDelete ()
    {
        $id = $_POST['user_id'];
        $model = $this->user->getUser($id);
        $result = $this->user->deleteUser($id);

        return json_encode([
            'success' => $result,
            'firstName' => $model['firstName'],
            'lastName' => $model['lastName'],
        ]);
    }

     /**
     * retrieves a registeted User's team code.
     * 
     * @param integer $id
     * 
     * @return mixed
     */
    public function actionGetPersonTeamsId ()
    {
        $id = $_POST['user_id'];
        $result = json_encode($this->user->getUserTeamsId($id));

        return $result;

    }
    /**
     * Checks if input email already exists in the database.
     * If it already exists, this function returns a boolean value of true; else, false.
     * 
     * @param string $email
     * 
     * @return boolean
     */
    public function actionCheckEmailAddress ($email, int $userId = null, $action)
    {
        $result = $this->user->isEmailAddressExisting($email, $userId, $action);
        
        if ($result)
            return json_encode(['exists' => true]);
        else 
            return json_encode(['exists' => false]);
    }
   
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @param integer $id
     * 
     * @return User the loaded model
     * 
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel ($id)
    {
        $model = $this->user->getInitializedUserModel($id);

        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested user does not exist.');
        }
    }

    /**
     * Renders view more information of a person in widget
     * @return mixed
     */
    public function actionViewInfo ()
    {
        $id = $_POST['id'];
        $data = $this->user->getPersonInfo($id);

        return Yii::$app->controller->renderAjax('@app/widgets/views/person/tabs.php',[
            'data' => $data
        ]);
    }

    /**
     * Function for removing a user from a team
     * @return mixed
     */
    public function actionRemoveMember()
    {

        $teamMemberId = $_POST['teamMemberId'];

        $response = Yii::$app->api->getParsedResponse('DELETE', 'team-members/'.$teamMemberId);

        return json_encode($response);

    }
}
