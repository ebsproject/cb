<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

    /**
     * Renders update web form
     */

    use yii\bootstrap\ActiveForm;

    /* @var $this yii\web\View */
    /* @var $model app\models\User */

    // Instantiate (via DI) a User object for the public methods
    $user = Yii::$container->get('user');

    $form = ActiveForm::begin([
        'enableClientValidation'=>false,
        'id'=>'update-user-form',
        'action'=>['/user/default/update', 'id' => $personDbId, 'program' => $program],
        'fieldConfig' => function ($model, $attribute) {
            if (in_array($attribute, [
                'email',
                'first_name',
                'last_name',
            ])) {
                return ['options' => ['class' => 'input-field form-group col-md-4']];
            } else if (in_array($attribute, ['is_active', 'person_type'])) {
                return ['options' => ['class' => 'input-field form-group col-md-6']];
            } else {
                return ['options' => ['class' => 'input-field form-group col-md-12']];
            }
        }
    ]);
?>
<?= 
    '<div class="modal-notif"></div>'. //modal notification field
    
    /*
        First row, consisting of fields for the following:
        -> email
        -> first_name
        -> last_name
    */
    '<div class="row">'.
        // email field
        $form->field($model, 'email')->textInput([
            'maxlength' => true,
            'id'=>'user-email-textfield-update',
            'title'=>'Email address of the user',
            'placeholder' => 'a.sample@email.com',
            'spellcheck' => 'false',
        ]).
        // first name field
        $form->field($model, 'first_name')->textInput([
            'id'=>'user-first_name-textfield-update',
            'title'=>'First name of the user',
            'placeholder' => 'Enter First Name',
            'oninput' => 'js: $("#user-first_name-textfield-update").val(this.value.charAt(0).toUpperCase().concat(this.value.slice(1)));',
            'spellcheck' => 'false',
        ]).
        // last name field
        $form->field($model, 'last_name')->textInput([
            'id'=>'user-last_name-textfield-update',
            'title'=>'Last name of the user',
            'placeholder' => 'Enter Last Name',
            'oninput' => 'js: $("#user-last_name-textfield-update").val(this.value.charAt(0).toUpperCase().concat(this.value.slice(1)));',
            'spellcheck' => 'false',
        ]).
    '</div>'.

    // Is Admin checkbox row
    '<input
        type="checkbox" '.
        ((strtolower($user->getUser($personDbId)['personType']) == 'admin') ? 'checked ' : '').
        'id="user-person_type-chkbx-update"
        class="filled-in"
        title="Access privilege of the person"
        name="User[person_type]"
        style="opacity:0"
    />'.
    '<label for="user-person_type-chkbx-update">'.\Yii::t('app','Is Admin?').'</label>&emsp;&nbsp;'.
    // Is Active checkbox row
    '<input
        type="checkbox"'.
        (($user->getUser($personDbId)['isActive']) ? 'checked ' : '').
        'id="user-is_active-chkbx-update"
        class="filled-in"
        title="Activation status of the person"
        name="User[is_active]"
        style="opacity:0"
    />
    <label for="user-is_active-chkbx-update">'.\Yii::t('app','Is Active?').'</label>&emsp;&nbsp;';
 ?>

<?php ActiveForm::end(); ?>

<style type="text/css">
    .control-label {
        transform: translateY(-14px) scale(0.8) !important;
        -webkit-transform-origin: 0 0 !important;
        transform-origin: 0 0 !important;
    }
</style>