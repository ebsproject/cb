<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* Renders browser and manage persons page
*/

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\Button;
use yii\bootstrap\Collapse;
use yii\bootstrap\ActiveForm;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use yii\web\JqueryAsset;
use app\components\FavoritesWidget;
use kartik\select2\Select2;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

$browserId = 'dynagrid-user';

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

?>

<div class="user-index">
    <!-- Browse persons -->
    <?php
        // action columns
        $actionColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
            ],
            [
                'class'=>'kartik\grid\ActionColumn',
                'header' => false,
                'noWrap' => true,
                'template' => '{view} {update} {delete} {add}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {                        
                        return Html::a('<i class="material-icons">remove_red_eye</i>',
                            '#',
                            [
                                'class' => 'view-user',
                                'title' => 'View person info',
                                'data-user_id' => $model['personDbId'],
                                'data-user_label' => $model['personName'],
                            ]
                        );
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<i class="material-icons">edit</i>',
                            '#',
                            [
                                'class' => 'update-user',
                                'title' => 'Update person',
                                'data-user_id' => $model['personDbId'],
                                'data-user_label' => $model['personName'],
                            ]
                        );
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<i class="material-icons">delete</i>',
                            '#',
                            [
                                'class'=>'delete-user',
                                'title' => 'Delete person',
                                'data-user_id' => $model['personDbId'],
                                'data-user_label' => $model['personName'],
                            ]
                        );
                    },
                    'add' => function ($url, $model, $key) {
                        return Html::a('<i class="material-icons">add</i>',
                            '#',
                            [
                                'class'=>'add-team',
                                'title' => 'Add to team',
                                'data-user_id' => $model['personDbId'],
                                'data-user_label' => $model['personName'],
                                'data-toggle' => 'modal',
                                'data-target' => '#add-to-team-modal'
                            ]
                        );
                    }
                ],
                'visible' => true,
            ],
        ];
        // all the columns that can be viewed in persons browser
        $columns = [
            [
                'attribute'=>'email',
            ],
            [
                'attribute'=>'username',
            ],
            [
                'attribute'=>'firstName',
            ],
            [
                'attribute'=>'lastName',
            ],
            [
                'attribute'=>'personName',
                'label' => 'Full Name',
            ],
            [
                'attribute'=>'personType',
                'label' => 'Type',
                'format' => 'raw',
                'filter' => [
                    'Admin' => 'Admin',
                    'User' => 'User'
                ],
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'pluginOptions'=> [
                        'autoclose' => true,
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => [
                    'placeholder' => '',
                ],
            ],
            [
                'class' => '\kartik\grid\BooleanColumn',
                'attribute'=>'isActive',
                'vAlign' => 'middle',
                'format' => 'raw',
                'label' => 'Is Active?',
                'trueLabel' => 'Yes',
                'falseLabel' => 'No',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'creator',
            ],
            [
                'attribute' => 'modifier',
                'visible' => false,
            ],
            [
                'attribute'=>'creationTimestamp',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => [
                    'autocomplete' => 'off'
                ],
                'filterWidgetOptions' => [
                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'clearBtn' => true,
                        'todayHighlight' => true,
                    ],
                    'pluginEvents' => [
                        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                    ],
                ],
                'visible' => false,
            ],
            [
                'attribute'=>'modificationTimestamp',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => [
                    'autocomplete' => 'off'
                ],
                'filterWidgetOptions' => [
                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'clearBtn' => true,
                        'todayHighlight' => true,
                    ],
                    'pluginEvents' => [
                        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                    ],
                ],
                'visible' => false,
            ]
        ];  

        $gridColumns = array_merge($actionColumns,$columns);

        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'filename' => 'Persons',
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false,
            ],
            'showColumnSelector' => false,
            'showConfirmAlert' => false
        ]);
        
        $createButton = '<a class="light-green darken-3 btn waves-effect waves-light tooltipped" style="margin-right:5px;" id="add-user-btn" data-position="top" 
        data-toggle="modal" data-target="#add-user-modal" data-delay="50" data-tooltip="'.Yii::t('app','Create Persons').'">'.\Yii::t('app','Create').'</a>';

        //dynagrid configuration
        $dynagrid = DynaGrid::begin([
            'columns' => $gridColumns,
            'theme'=>'simple-default',
            'showPersonalize'=>true,
            'storage' => DynaGrid::TYPE_SESSION,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'id' => 'grid',
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'showPageSummary'=>false,
                'pjax'=>true,
                'responsiveWrap'=>false,
                'panel'=>[
                    'heading'=>'<h3>'.\Yii::t('app', 'Persons') .FavoritesWidget::widget([
                        'module' => 'user'
                        ]).'&nbsp;'.
                     '</h3>',
                    'before' => \Yii::t('app', 'This is the browser for all the registered persons in the system. You may manage the persons here.'),
                    'after' => false,
                ],
                'toolbar' =>  [
                    [
                        'content'=> 
                            $createButton .
                            " " .
                            $export .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index','program'=>$program], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid')]).'{dynagridFilter}{dynagridSort}{dynagrid}',
                    ],
                ]
            ],
            'submitButtonOptions' => [
                'icon' => 'glyphicon glyphicon-ok',
            ],
            'deleteButtonOptions' => [
                'icon' => 'glyphicon glyphicon-delete',
                'label' => 'Remove',
            ],
            'options'=>['id'=> $browserId] // a unique identifier is important
        ]);
        DynaGrid::end();
    ?>    
</div>


<!-- Create a new person -->
<?php 
    $form = ActiveForm::begin([
        'enableClientValidation'=>false,
        'id'=>'create-user-form',
        'action'=>['/user/default/create','program' => $program],
        'fieldConfig' => function($model,$attribute){
            if (in_array($attribute, [
                    'email',
                    'first_name',
                    'last_name',
                ])){
                return ['options' => ['class' => 'input-field form-group col-md-4']];
            } else if (in_array($attribute, ['is_active', 'person_type'])) {
                return ['options' => ['class' => 'input-field form-group col-md-6']];
            } else {
                return ['options' => ['class' => 'input-field form-group col-md-12']];
            }
        }
    ]); 
?>
<?php
    Modal::begin([
        'id' => 'add-user-modal',
        'header' => '<h4><i class="material-icons">add</i> '.\Yii::t('app','Create').' '. \Yii::t('app','Person').'</h4>',
        'closeButton' => [
            'tag' => 'close-btn',
        ],
        'footer' =>
            Html::a(\Yii::t('app','Cancel'),
            ['#'],
            [
                'id'=>'cancel-save-btn',
                'data-dismiss'=>'modal'
            ]).
            '&emsp;&nbsp;'. // Insert whitespaces
            '<input
                type="checkbox"
                id="user-create-another"
                class="filled-in"
                name="Form[create_another]"
                value="1"
                style="opacity:0"
            />'.
            '<label for="user-create-another">'.\Yii::t('app','Create another').'</label>&emsp;&nbsp;'.
            Html::a(\Yii::t('app','Submit'),
                ['#'],
                [
                    'class'=>'btn btn-primary waves-effect waves-light',
                    'url'=>'#',
                    'id'=>'save-btn'
                ]).'&emsp;',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static']
    ]);
?>
<?= 
    '<div class="modal-notif"></div>'. //modal notification field

    /*
        First row, consisting of fields for the following:
        -> email
        -> first_name
        -> last_name
    */
    '<div class="row">'.
        '<div id="person-fields">'.        
        // email field
        $form->field($model, 'email')->textInput([
            'maxlength' => true,
            'id'=>'user-email-textfield',
            'title'=>'Email address of the person',
            'placeholder' => 'a.sample@email.com',
            'spellcheck' => 'true',
        ]).
        // first name field
        $form->field($model, 'first_name')->textInput([
            'id'=>'user-first_name-textfield',
            'title'=>'First name of the person',
            'placeholder' => 'Enter first name',
            'oninput' => 'js: $("#user-first_name-textfield").val(this.value.charAt(0).toUpperCase().concat(this.value.slice(1)));',
            'spellcheck' => 'true',
        ]).
        // last name field
        $form->field($model, 'last_name')->textInput([
            'id'=>'user-last_name-textfield',
            'title'=>'Last name of the person',
            'placeholder' => 'Enter last name',
            'oninput' => 'js: $("#user-last_name-textfield").val(this.value.charAt(0).toUpperCase().concat(this.value.slice(1)));',
            'spellcheck' => 'true',
        ]).
        '</div>'.
    '</div>'.
    // Is Admin checkbox row
    '<input
        type="checkbox"
        id="user-person_type-chkbx"
        class="filled-in"
        title="Access privilege of the person"
        name="User[person_type]"
        style="opacity:0"
    />'.
    '<label for="user-person_type-chkbx">'.\Yii::t('app','Is Admin?').'</label>&emsp;&nbsp;'.
    // Is Active checkbox row
    '<input
        type="checkbox"
        id="user-is_active-chkbx"
        class="filled-in"
        title="Is the account activated?"
        name="User[is_active]"
        style="opacity:0"
    />'.
    '<label for="user-is_active-chkbx">'.\Yii::t('app','Is Active?').'</label>&emsp;&nbsp;'
?>

<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>

<?php
    // view person modal
    Modal::begin([
        'id' => 'view-user-modal',
        'header' => '<h4 id="user-label"></h4>',
        'footer' =>
            Html::a(\Yii::t('app','Close'),
            '#',
            [
                'data-dismiss'=>'modal',
                'class'=>'modal-close'
            ]).
            '&emsp;',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static'],
    ]);
    Modal::end();

    //update person modal
    Modal::begin([
        'id' => 'update-user-modal',
        'header' => '<h4 id="update-user-label"></h4>',
        'closeButton' => [
            'tag' => 'close-btn',
        ],
        'footer' =>
            Html::a(\Yii::t('app','Cancel'),
            ['#'],
            [
                'id'=>'cancel-save-btn',
                'data-dismiss'=>'modal'
            ]).
            '&emsp;'.
            Html::a(\Yii::t('app','Submit'),
            ['#'],
            [
                'class'=>'btn btn-primary waves-effect waves-light',
                'url'=>'#',
                'id'=>'update-save-btn'
            ]).
            '&emsp;',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static'],
    ]);
    Modal::end();

    // delete person modal
    Modal::begin([
        'id' => 'delete-user-modal',
        'header' => '<h4 id="delete-user-label"></h4>',
        'footer' =>
            Html::a(
                \Yii::t('app','Cancel'),
                null,
                [
                    'data-dismiss'=>'modal'
                ]
            ) .
            '&nbsp;&nbsp' .
            Html::a(
                \Yii::t('app','Confirm'),
                null,
                [
                    'class'=>'btn btn-primary waves-effect waves-light delete-user-btn',
                    'id'=>'delete-btn'
                ]
            ) .
            '&nbsp;&nbsp',
        'options' => ['data-backdrop'=>'static']
    ]);
?>
<?= '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete <b><span id="user-delete"></span>\'s</b> account. Note that the person will no longer be able to access the system. Click confirm to proceed.' ?>
<?php Modal::end(); ?>


<!-- add to team modal -->
<div id="add-to-team-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4><i class="material-icons">add</i>&nbsp;<?= \Yii::t('app','Add to team') ?>&nbsp; <b><span id="user-display-txt"></span></b><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
        </div>
        <div class="modal-body add-to-team-modal-body">
            <div id="add-team-progress" style="display:none" class="progress"><div class="indeterminate"></div></div>
            <div id="add-team-body"  style="display:none">    
                <div class="col col-md-6">
                    <?php
                        echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Team') . '&nbsp;<span class="required">*</span></label>';
                        echo Select2::widget([
                            'data' => $teamsTags,
                            'name' => 'select-team',
                            'id' => 'team-profile-select',
                            'maintainOrder' => true,
                            'options' => [
                                'placeholder' => \Yii::t('app','Select team'),
                                'tags' => true,
                            ]
                        ]);
                    ?>
                </div>
                <div class="col-md-6">
                    <?php
                        echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Role').'&nbsp;<span class="required">*</span></label>';
                        echo Select2::widget([
                            'name' => 'select-role',
                            'data' => $rolesTags,
                            'id' => 'role-profile-select',
                            'maintainOrder' => true,
                            'options' => [
                                'placeholder' => \Yii::t('app','Select role'),
                                'tags' => true,
                            ]
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        <a class="modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Cancel') ?></a>&emsp;
        <button class="disabled btn waves-effect waves-light" id="add-to-team-profile-confirm" style="margin-right:20px;"><?= \Yii::t('app','Add') ?></button>
        </div>
    </div>
  </div>
</div> 


<?php
$viewUrl = Url::to(['default/view']); // url for view person ajax
$updateUrl = Url::to(['default/update-user-form','program'=>$program]); // url for update person ajax
$deleteUrl = Url::to(['default/delete','program'=>$program]); // url for delete person ajax
$removeMemberUrl = Url::to(['default/remove-member']);
$checkEmailAddressUrl = Url::to(['default/check-email-address']); // url for check email address ajax
$validEmailDomains = "'" . getenv('CB_VALID_EMAIL_DOMAINS') . "'"; // get list of valid email domains
$addToTeamUrl = Url::to(['/account/profile/addusertoteam']);
$getTeamsIdUrl = Url::to(['default/get-person-teams-id', 'program'=>$program]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    let validEmailDomains = $validEmailDomains.split('|')
    let userId = null
    let obj
    let user_label
    let user_id
    $(document).on('ready pjax:success', function(e) {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        
        // remove loading indicator upon export
        $('*[class^="export-full-"]').on('click',function(){
            $('#system-loading-indicator').css('display','none');
        });

        e.stopPropagation();
        e.preventDefault();
    });

    $(document).on('click', '.add-team', function(){
        $('#add-team-progress').show();
        $('#add-team-body').hide();

        var obj = $(this);
        user_id = obj.data('user_id');
        var userName = obj.data('user_label');

        $('#user-display-txt').html(userName);
        $('#add-to-team-modal').removeAttr('tabindex');
        //enter functuion here
        let copyId = user_id
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: '$getTeamsIdUrl',
            data:{
                user_id : copyId
            },
            success: function(data) {
                $('#team-profile-select option').prop('disabled', false);
                let counter = data.length;
                for(let i= 0; i<counter; i++){
                    $('#team-profile-select option[value="'+data[i] +'"]').prop('disabled', true);
                    //resetSelect2();
                }
                
                $('#add-team-progress').hide();
                $('#add-team-body').show();
            },
         });  
    });

    //create new person
    $('#add-user-modal').on('shown.bs.modal', function(e) { //set focus on first field
        $('#user-email-textfield').focus();
        $('.modal-notif').html("");
    });

    $("#save-btn").on('click', async function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        // validate input fields BEGIN
        // sanitize email
        let email = $('#user-email-textfield').val()
        $('#user-email-textfield').val(email.trim())
        email = email.trim()

        // sanitize first name
        let firstName = $('#user-first_name-textfield').val()
        $('#user-first_name-textfield').val(firstName.trim())
        firstName = firstName.trim()

        // sanitize last name
        let lastName = $('#user-last_name-textfield').val()
        $('#user-last_name-textfield').val(lastName.trim())
        lastName = lastName.trim()
         //check for teams
        var selected_team = $("#team-profile-select").val();
        var selected_role = $("#role-profile-select").val();

        let allValid = true

        // validate email address
        if (email == '') {
            $(".field-user-email-textfield").addClass("has-error")
            $(".field-user-email-textfield").find(".help-block").html('Please enter an email address')
            allValid = false
        } else if (!isEmailAddressValid(email)) {
            $(".field-user-email-textfield").addClass("has-error")
            $(".field-user-email-textfield").find(".help-block").html('Please enter a valid email address')
            allValid = false
        } else if (!isEmailDomainValid(email.split('@')[1])) {
            $(".field-user-email-textfield").addClass("has-error")
            $(".field-user-email-textfield").find(".help-block").html('Please enter a valid email domain')    
            allValid = false        
        } else if (await isEmailAddressExisting(email, 'email-textfield', 'create')) {    
            allValid = false
        } else {
            $(".field-user-email-textfield").removeClass("has-error")
            $(".field-user-email-textfield").find(".help-block").html('')
        }

        // validate first name
        if (firstName == '') {
            $(".field-user-first_name-textfield").addClass("has-error")
            $(".field-user-first_name-textfield").find(".help-block").html('Please enter a first name')
            allValid = false
        } else {
            $(".field-user-first_name-textfield").removeClass("has-error")
            $(".field-user-first_name-textfield").find(".help-block").html('')
        }

        // validate last name
        if (lastName == '') {
            $(".field-user-last_name-textfield").addClass("has-error")
            $(".field-user-last_name-textfield").find(".help-block").html('Please enter a last name')
            allValid = false
        } else {
            $(".field-user-last_name-textfield").removeClass("has-error")
            $(".field-user-last_name-textfield").find(".help-block").html('')
        }
        // validate input fields END
       


        if (allValid) $("#create-user-form").submit();
        else return

        $('.modal-notif').html('<div class="progress"><div class="indeterminate"></div></div>');

        $("#create-user-form").submit(function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $(".form-group").removeClass("has-error"); //remove error class
            $(".help-block").html(""); //remove existing error messages
            $('.modal-notif').html("");

            var form_data = $("#create-user-form").serialize();
            var action_url = $("#create-user-form").attr("action");
            $.ajax({
                type:"POST",
                dataType: 'json',
                url: action_url,
                data: form_data,
                success: function(data) {
                    $('.modal-notif').html('');
                    if (data.success == true){//data saved successfully                
                        if(data.create_another == '1'){ //create another person
                            $('#create-user-form')[0].reset()
                            $('#user-email-textfield').focus() // Reset input fields and checkbox
                            $('#user-create-another').prop('checked', true) // Retain state

                            var ctgs = $(".field-user-email > label");
                            ctgs.addClass("active");

                            $('#user-email-textfield').focus()

                            $('.modal-notif').html('<div id="w1-success-0" class="alert success"><div class="card-panel"><i class="fa fa-check"></i> You successfully created <b>' + data.name + '\'s</b> account!<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button></div></div>');
                        }else{
                            location.reload();
                        }
                    } else { // validation errors occurred
                        $('.modal-notif').html('<div id="w1-error-0" class="alert error"><div class="card-panel"><i class="fa fa-times"></i> There seems to be a problem while creating the person\'s account. Please check that all required fields are correctly specified</div></div>');
                        $.each(data.error, function(fieldName, errorMessage) { // show errors to user
                            $(".field-user-"+fieldName).addClass("has-error");
                            $(".field-user-"+fieldName).find(".help-block").html(errorMessage);
                        });
                    }
                },
                error: function(){
                    $('.modal-notif').html('');
                    $('.modal-notif').html('<div id="w1-error-0" class="alert error"><div class="card-panel"><i class="fa fa-times"></i> There seems to be a problem while creating the person\'s account. Please check that all required fields are correctly specified</div></div>');
                }
            });            
            
            $('.modal-notif').html('');

            return false;
        });
    });

    $(document).keyup(function (e) {
        // Upon pressing Escape key, modal clears all input fields, checkboxes, and error messages
        if (e.keyCode === 27 && (!!document.getElementById('cancel-save-btn')))
            clearForm()
    })

    $('#cancel-save-btn').on('click', function(e) {
        clearForm()
    })

    $('close-btn').on('click', function (e) {
        clearForm()

    })

    //validate if program and team are selected
    $( "[id$=-profile-select]" ).change(function(){
        var selected_team = $("#team-profile-select").val();
        var selected_role = $("#role-profile-select").val();

        if(selected_team !== '' && selected_role !== ''){
            $('#add-to-team-profile-confirm').removeClass('disabled');
        }else{
            $('#add-to-team-profile-confirm').addClass('disabled');
        }
    });

    //add to team
    $('#add-to-team-profile-confirm').on('click', function(e){
        var selected_team = $("#team-profile-select").val();
        var selected_role = $("#role-profile-select").val();

        $.ajax({
            url: '$addToTeamUrl',
            type: 'post',
            data: {
                team_id: selected_team,
                role_id: selected_role,
                user_id: user_id
            },
            success: function(response) {
                location.reload();
            },
            error: function() {
            }
        });
    });

    function clearForm(update = null) {
        // Clear email error message
        $(".field-user-email-textfield").removeClass("has-error")
        $(".field-user-email-textfield").find(".help-block").html('')
        $(".field-user-email-textfield-update").removeClass("has-error")
        $(".field-user-email-textfield-update").find(".help-block").html('')

        // Clear first name error message
        $(".field-user-first_name-textfield").removeClass("has-error")
        $(".field-user-first_name-textfield").find(".help-block").html('')
        $(".field-user-first_name-textfield-update").removeClass("has-error")
        $(".field-user-first_name-textfield-update").find(".help-block").html('')

        // Clear last name error message
        $(".field-user-last_name-textfield").removeClass("has-error")
        $(".field-user-last_name-textfield").find(".help-block").html('')
        $(".field-user-last_name-textfield-update").removeClass("has-error")
        $(".field-user-last_name-textfield-update").find(".help-block").html('')

        // Clear Create another
        $('#user-create-another').prop('checked', false)

        // Reset input fields and checkbox
        $('#create-user-form')[0].reset()
    }
    //end create new person


    // view person
    $(document).on('click', '.view-user', function() {
        var obj = $(this);
        var user_label = obj.data('user_label');
        var user_id = obj.data('user_id');
        var url = '$viewUrl';
        var view_modal = '#view-user-modal > .modal-dialog > .modal-content > .modal-body';

        $('#user-label').html('<i class="material-icons">remove_red_eye</i> ' + user_label);
        $('#view-user-modal').modal('show');

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
            user_id: obj.data('user_id')
            },
            success: function(response) {
                if (response.grid) {
                    $(view_modal).html(response.grid);
                }
            },
            error: function() {
                $(view_modal).html('<div class="card-panel red"><span class="white-text">There seems to be a problem while viewing the person.</span></div>');
            }
        });
    });
    // end view person

    $(document).on('click', '.remove-member-link', function() {
        var obj = $(this);
        var teamMemberId = obj.data('team_member_id');
        var userId = obj.data('user_id');
        var url = '$removeMemberUrl';
        var view_modal = '#view-user-modal > .modal-dialog > .modal-content > .modal-body';
        var viewUrl = '$viewUrl';

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                "teamMemberId": teamMemberId
            },
            success: function(response) {
                Materialize.toast("<i class='material-icons green-text'>check</i>&nbsp; Successfully removed member from team. ", 5000);

                $.ajax({
                    url: viewUrl,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        user_id: userId
                    },
                    success: function(response) {
                        if (response.grid) {
                            $(view_modal).html(response.grid);
                        }
                    },
                    error: function() {
                        $(view_modal).html('<div class="card-panel red"><span class="white-text">There seems to be a problem while viewing the person.</span></div>');
                    }
                });
            }
        });
    });

    //update person
    $('#update-user-modal').on('shown.bs.modal', function() {
        $('#user-email-textfield-update').focus();
        $('.modal-notif').html("");
    });

    $(document).on('click', '.update-user', function() {
        var obj = $(this);
        var user_label = obj.data('user_label');
        var user_id = obj.data('user_id');
        userId = user_id
        var url = '$updateUrl';
        var update_modal = '#update-user-modal > .modal-dialog > .modal-content > .modal-body';
        
        $('#update-user-label').html('<i class="material-icons">edit</i> '+user_label);
        $('#update-user-modal').modal('show');

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                user_id: obj.data('user_id')
            },
            success: function(response) {
                if (response) {
                    $(update_modal).html(response.htmlData)
                }
            },
            error: function(response) {
                $(update_modal).html('<div class="card-panel red"><span class="white-text">There seems to be a problem while updating the person</span></div>');
            }
        });
    });

    $('#update-save-btn').on('click', async function(e){
        e.preventDefault()
        e.stopImmediatePropagation()

        // sanitize email
        let email = $('#user-email-textfield-update').val()
        $('#user-email-textfield-update').val(email.trim())
        email = email.trim()

        // sanitize first name
        let firstName = $('#user-first_name-textfield-update').val()
        $('#user-first_name-textfield-update').val(firstName.trim())
        firstName = firstName.trim()

        // sanitize last name
        let lastName = $('#user-last_name-textfield-update').val()
        $('#user-last_name-textfield-update').val(lastName.trim())
        lastName = lastName.trim()

        let allValid = true

        // validate email address
        if (email == '') {
            $(".field-user-email-textfield-update").addClass("has-error")
            $(".field-user-email-textfield-update").find(".help-block").html('Please enter an email address')
            allValid = false
        } else if (!isEmailAddressValid(email)) {
            $(".field-user-email-textfield-update").addClass("has-error")
            $(".field-user-email-textfield-update").find(".help-block").html('Please enter a valid email address')
            allValid = false
        } else if (!isEmailDomainValid(email.split('@')[1])) {
            $(".field-user-email-textfield-update").addClass("has-error")
            $(".field-user-email-textfield-update").find(".help-block").html('Please enter a valid email domain')   
            allValid = false         
        } else if (await isEmailAddressExisting(email, 'email-textfield-update', 'update')) {    
            allValid = false
        } else {
            $(".field-user-email-textfield-update").removeClass("has-error")
            $(".field-user-email-textfield-update").find(".help-block").html('')
        }

        // validate first name
        if (firstName == '') {
            $(".field-user-first_name-textfield-update").addClass("has-error")
            $(".field-user-first_name-textfield-update").find(".help-block").html('Please enter a first name')
            allValid = false
        } else {
            $(".field-user-first_name-textfield-update").removeClass("has-error")
            $(".field-user-first_name-textfield-update").find(".help-block").html('')
        }

        // validate last name
        if (lastName == '') {
            $(".field-user-last_name-textfield-update").addClass("has-error")
            $(".field-user-last_name-textfield-update").find(".help-block").html('Please enter a last name')
            allValid = false
        } else {
            $(".field-user-last_name-textfield-update").removeClass("has-error")
            $(".field-user-last_name-textfield-update").find(".help-block").html('')
        }

        if (allValid) $('#update-user-form').submit();
        else return

        $('.modal-notif').html('<div class="progress"><div class="indeterminate"></div></div>');

        $("#update-user-form").submit(async function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $(".form-group").removeClass("has-error"); // remove error class
            $(".help-block").html(""); // remove existing error messages
            $('.modal-notif').html("");

            var form_data = $("#update-user-form").serialize();
            var action_url = $("#update-user-form").attr("action");

            await $.ajax({
                type:"POST",
                dataType: 'json',
                url: action_url,
                data: form_data,
                success: function(data) {
                    $('.modal-notif').html('');
                    if (data.success == true) {// person's account details updated successfully
                        location.reload()
                    } else { // validation errors occurred
                        $('.modal-notif').html('<div id="w1-error-0" class="alert error"><div class="card-panel"><i class="fa fa-times"></i> There seems to be a problem while updating the person\'s account details. Please check that all required fields are correctly specified</div></div>');
                        $.each(data.error, function(fieldName, errorMessage) { // show errors to user
                            $(".field-user-"+fieldName).addClass("has-error");
                            $(".field-user-"+fieldName).find(".help-block").html(errorMessage);
                        });
                    }
                },
                error: function(){
                    $('.modal-notif').html('');
                    $('.modal-notif').html('<div id="w1-error-0" class="alert error"><div class="card-panel"><i class="fa fa-times"></i> There seems to be a problem while updating the person\'s account details. Please check that all required fields are correctly specified</div></div>');
                }
            })

            $(".modal-notif").removeClass("progress")

            return false
        })
    });
    //end update person

    // delete person
    $(document).on('click', '.delete-user', function() {
        obj = $(this)
        user_label = obj.data('user_label')
        user_id = obj.data('user_id')

        $('#delete-user-label').html('<i class="material-icons">delete</i> Delete <b>' + user_label + '\'s</b> Account')
        $('#user-delete').html(user_label)
        document.getElementById("delete-btn").setAttribute("data-user_id", user_id)
        $('#delete-user-modal').modal('show')
    });

    $('#delete-btn').on('click', function(e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        let url = '$deleteUrl'

        $('#delete-btn').addClass('disabled')

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                user_id: obj.data('user_id')
            },
            async: true,
            success: function (res) {
                let message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i> There seems to be a problem with deleting ' + res.firstName + ' ' + res.lastName + '\'s account.'

                if (res.success) {
                    $.pjax.reload({
                        container: '#dynagrid-user-pjax'
                    })
                    message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i> Successfully deleted ' + res.firstName + ' ' + res.lastName +'\'s account!'
                }

                $('#delete-user-modal').modal('hide')
                $('#delete-btn').removeClass('disabled')

                Materialize.toast(message, 5000)
            },
            error: function (res) {
                let message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i> There seems to be a problem with deleting ' + res.firstName + ' ' + res.lastName + '\'s account.'

                $('#delete-btn').modal('hide')
                $('#delete-btn').removeClass('disabled')
                Materialize.toast(message, 5000)
            }
        })
    })
    // end delete person

    // update date input field labels
    $("[for=user-person_type-chkbx]").addClass('active');
    $("[for=user-person_type-chkbx-update]").addClass('active');
    $("[for=user-is_active-chkbx]").addClass('active');
    $("[for=user-is_active-chkbx-update]").addClass('active');

    /*
        Validators
    */

    // validates that the input string is a valid date formatted as 'personal_info@domain'
    function isEmailAddressValid(emailAddress) {
        let mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

        if (emailAddress.match(mailFormat))
            return true
        else
            return false
    }

    // validates that the input string is a valid email domain as per the Institute's policies
    function isEmailDomainValid(domain) {
        let result = validEmailDomains.includes(domain)
        
        return result
    }

    async function isEmailAddressExisting(emailAddress, fieldName, action) {
        userId = (userId == null) ? 0 : userId
        
        let checkEmailAddressUrl =
            '$checkEmailAddressUrl' +
            '?email=' + emailAddress +
            '&userId=' + userId +
            '&action=' + action
        let exists = false
        let obj = $(this)

        await $.ajax({
            url: checkEmailAddressUrl,
            type: 'post',
            dataType: 'json',
            data: {
                user_id: obj.data('user_id')
            },
            success: function (data) {
                if (data.exists) {
                    $('.field-user-' + fieldName).addClass("has-error")
                    $('.field-user-' + fieldName).find(".help-block").html('Email address already exists in the database. Please try a different one')
                    exists = true
                }
            },
            error: function (data) {
                $('.field-user-' + fieldName).addClass("has-error")
                $('.field-user-' + fieldName).find(".help-block").html('There was a problem with processing email address')
                exists = true
            }
        });

        return exists
    }
JS
);
?>