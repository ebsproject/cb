<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders preview upload mapped plots
 */

use app\models\Person;
use kartik\grid\GridView;

echo '<a href="#" class="go-back-to-parent-panel" title="' . \Yii::t('app', 'Go back') . '">
    <i class="material-icons">arrow_back</i>
</a>';

echo '<p>Below location and mapped plots will be created and populated. Click confirm to proceed.</p>';

if ($allowMultipleSiteNotification != '') {
    echo '<div class="alert warning"><div class="card-panel">' . \Yii::t('app', $allowMultipleSiteNotification) . '</div></div>';
}

echo GridView::widget([
    'dataProvider' => $locationDataProvider,
    'id' => 'preview-location-grid',
    'layout' => '{items}',
    'columns' => [
        [
            'attribute' => 'locationName',
            'group' => true,  // enable grouping,
            'value' => function ($data) {
                return !empty($data['locationName']) ? $data['locationName'] : '<i>Auto-generated</i>';
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'locationSteward',
            'value' => function ($data) {
                $personModel = \Yii::$container->get('app\models\Person');
                $person = $personModel->getOne(intval($data['locationSteward']));

                if (isset($person['data']['personName'])) {
                    return $person['data']['personName'];
                }
            },
            'group' => true,  // enable grouping
        ],
        [

            'attribute' => 'occurrenceName',
            'value' => function ($data) {
                return str_replace('$$', '<br/>', $data['occurrenceName']);
            },
            'format' => 'raw'
        ],
        "site"
    ],
    'tableOptions' => [
        'class' => 'table white z-depth-3'
    ],
]);

// occurrence ID in search
$occurrenceParam['occurrenceDbId'] = $occurrenceId;
?>
<ul class="collapsible" data-collapsible="accordion">
    <li>
        <div class="collapsible-header">Plots</div>
        <div class="collapsible-body">
            <?php

            // if plots are greater than 100, display a note
            if($totalPlotCount > 100){
                echo '100 out of ' . number_format($totalPlotCount) . ' plots are displayed here.';
            }

            $columns = [
                [
                    'header' => '',
                    'class' => 'yii\grid\SerialColumn'
                ],
                [
                    'attribute' => 'occurrenceName',
                    'enableSorting' => false
                ],
                [
                    'attribute' => 'plotno',
                    'enableSorting' => false
                ],
                [
                    'attribute' => 'designation',
                    'header' => 'germplasm name',
                    'enableSorting' => false
                ],
                'pa_x',
                'pa_y',
                'field_x',
                'field_y'
            ];

            echo GridView::widget([
                'dataProvider' => $plotDataProvider,
                'id' => 'preview-upload-planting-arrays-plot-grid',
                'columns' => $columns,
                'tableOptions' => [
                    'class' => 'table white z-depth-3'
                ],
            ]);
            ?>
        </div>
    </li>
    <?php
    $columns = [
        [
            'header' => '',
            'class' => 'yii\grid\SerialColumn'
        ],

        [
            'attribute' => 'designation',
            'header' => 'germplasm',
            'enableSorting' => false
        ],
        'pa_x',
        'pa_y',
        'field_x',
        'field_y'
    ];
    if ($action != 'generateLocation') {
        if (count($borderDataProvider->getModels()) > 0) {
    ?>
            <li>
                <div class="collapsible-header">Borders</div>
                <div class="collapsible-body">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $borderDataProvider,
                        'id' => 'preview-upload-planting-arrays-border-grid',
                        'columns' => $columns,
                        'tableOptions' => [
                            'class' => 'table white z-depth-3'
                        ],
                    ]);
                    ?>
                </div>
            </li>
        <?php }
        if (count($fillerDataProvider->getModels()) > 0) { ?>
            <li>
                <div class="collapsible-header">Fillers</div>
                <div class="collapsible-body">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $fillerDataProvider,
                        'id' => 'preview-upload-planting-arrays-filler-grid',
                        'columns' => $columns,
                        'tableOptions' => [
                            'class' => 'table white z-depth-3'
                        ],
                    ]);
                    ?>
                </div>
            </li>
    <?php
        }
    } ?>
</ul>
<?php
$this->registerJs(
    <<<JS
    $('.collapsible').collapsible();
    $(document).on('ready pjax:success', function(e) {
        $('.collapsible').collapsible();
    });
JS
);
