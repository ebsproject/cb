<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders upload planting array web form
 */
use app\components\GenericFormWidget;
use kartik\widgets\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$currentPage = $currentPage ?? 0;

// initialize use existing location selection to session
Yii::$app->session->set('is-loc-existing-selection', false);

// loading indicator
echo '<div class="hidden margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div><br/>';

// web form
ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'upload-planting-arrays-form'
]);

$mapToExistingLoc = 'Map the occurrence to an existing location or re-upload mapping data';
$mapToNewLoc = 'Map the occurrence to a new location';
$cursor = $switchTitle = $mapToExistingLocNote = '';
if($action == 'generate-location') {
    $switchClass = 'disabled';
    $cursor = 'cursor:not-allowed;';
    $switchTitle = $mapToExistingLoc = $mapToNewLoc = 'Mapping to existing Location is not possible when generating Location';
    $mapToExistingLocNote = '<br/><span class="text-muted" style="font-size:97%">Mapping to existing Location is not possible when generating Location</span>';
}
echo '
    <div class="switch col col-md-6" style="margin-left: 5px;">
        <label title="'.$switchTitle.'" style="' .$cursor. '">
            <span title="'.$mapToNewLoc.'">New location</span>
            <input type="checkbox" id="upload-planting-arrays-switch" '.($switchClass ?? '').'>
            <span class="lever" style="margin: 10px 7px; '.$cursor.' "></span>
            <span title="'.$mapToExistingLoc.'">Existing location</span>
        </label>'.$mapToExistingLocNote.'
    </div>
    <div class="clearfix"></div>';

echo GenericFormWidget::widget([
    'options' => $widgetOptions
]);

ActiveForm::end();

// if render generate location form, display use design layout checkbox
if($action == 'generate-location') {
echo '<div class="col col-md-6"></div><div class="col col-md-6 use-design-layout"><input
    type="checkbox"
    id="use_design_layout"
    class="filled-in"
    name="Location[use_design_layout]"
    style="opacity:0"
/>'.
'<label 
    for="use_design_layout"
    title="Whether the design layout exactly matches the planting area and field layout and can be used for spatial analysis and field navigation">'.
    \Yii::t('app','Design layout matches Planting area and Field layout').
'</label></div>';
}

// if the form to be rendered is not in generating location
if($action != 'generate-location') {
    echo '<div class="col col-md-6 upload-panel" style="margin-top:-35px">';

    echo FileInput::widget([
        'name' => 'upload_planting_arrays',
        'pluginOptions' => [
            'uploadAsync' => false,
            'uploadUrl' => Url::to(['/occurrence/upload/planting-arrays']),
            'allowedFileExtensions' => ['csv'],
            'browseClass' => 'btn waves-effect waves-light browse-btn',
            'showCaption' => false,
            'showRemove' => false,
            'browseLabel' => 'Browse',
            'removeLabel' => '',
            'uploadLabel' => 'Validate',
            'uploadTitle' => 'Validate file',
            'uploadClass' => 'btn',
            'removeClass' => 'btn btn-danger',
            'cancelClass' => 'btn grey lighten-2 black-text',
            'dropZoneTitle' => \Yii::t('app', 'Drag and drop file here or<br/>click Browse<br/><small>Use the downloaded mapping files in CSV format. Specify required fields to validate.</small>'),
            'overwriteInitial' => true,
            'maxFileCount' => 1,
            'autoReplace' => true,
            'disabledPreviewTypes' => ['text'],
            'fileActionSettings' => [
                'showUpload' => false,
                'showZoom' => false,
                'removeIcon' => '<i class="fa fa-trash"></i>',
                'showRotate' => false,
                'indicatorNew' => '<i class="fa fa-plus-circle text-warning"></i>',
                'indicatorSuccess' => '<i class="fa fa-check-circle text-success"></i>',
                'indicatorError' => '<i class="fa fa-exclamation-circle text-danger"></i>',
                'indicatorLoading' => '<i class="fa fa-hourglass-half text-muted"></i>',
                'indicatorPaused' => '<i class="fa fa-pause-circle text-info"></i>',
            ],
            'theme' => 'fa',
        ],
        'pluginEvents' => [
            'fileclear' => 'function() { validate("clear"); }',
            'filereset' => 'function() { validate(); }',
            'fileloaded' => 'function() { validate(); }',
            'fileuploaderror' => 'function() { validate(); }',
        ],
        'options' => [
            'multiple' => false,
            'accept' => 'csv/*', 
            'id' => 'planting-arrays-file-input'
        ]
    ]);

    echo '<div>';
}

if($action == 'generate-location'){
    $this->registerCss("
        .generate-location-modal-body .parent-box.box-1 { 
            margin-top: 39px !important; 
        }

        .generate-location-modal-body .parent-box.box-2 { 
            margin-top: 39px !important; 
        }

        .use-design-layout {
            margin-top: -49px;
        }

        @media(max-width:992px) {
            .use-design-layout {
                margin-top: 0px;
            }
        }
    ");
}

// preview upload mapped plots url
$previewUploadUrl = Url::to(['/occurrence/upload/preview-planting-arrays']);
// confirm upload mapped plots url
$confirmUploadUrl = Url::to(['/occurrence/upload/confirm-upload-planting-arrays']);
// get steward and description of Location
$getLocationInfoUrl = Url::to(['/occurrence/upload/get-location-info']);
// render Location options url
$renderLocationOptionsUrl = Url::to(['/occurrence/upload/render-location-options']);
// validate map to existing Location
$validateMapToLocUrl = Url::to(['/occurrence/upload/validate-map-to-existing-loc']);
// save is existing location selection to session url
$saveIsExLocSelectionUrl = Url::to(['/occurrence/upload/save-is-ex-loc-selection']);

$this->registerJs(<<<JS
// whether map the occurrence to an existing location or not
var isExLoc = false;
var locationId = null;
var program = '$program';
var validationSucc = false;
var noLoc = false;
var currentPage = '$currentPage';
var useDesignLayout = false;
var mappedLocationId = null; // mapped location ID to an Occurrence that is already mapped

$.ajax({
    url: '$saveIsExLocSelectionUrl',
    type: 'POST',
    data: {
        isExLoc: isExLoc
    }
});

// hide location selection
$("div[class*=' field-LOCATION-']").addClass('hidden');

// get currently viewed record
var searchParams = new URLSearchParams(window.location.search);
var selectedRecordId = searchParams.get('selectedRecordId');

// download file template
$(document).on('click', '.download-planting-arrays-file-template', function(){
    var obj = $(this);
    var url = obj.attr('href');

    window.location = url;
});

// validate required fields
$('.required-field').on("keypress keyup blur paste",function (e) {
    // if required field not specified

    if($(this).val().length == 0){
        $('.browse-btn, .fileinput-upload-button, .go-to-preview-generate-location-btn').addClass('disabled');
    } else {
        $('.browse-btn, .fileinput-upload-button, .go-to-preview-generate-location-btn').removeClass('disabled');
    }
});

// if validation is successful
$('#planting-arrays-file-input').on('filebatchuploadsuccess', function(e, data) {
    
    var response = data.response;

    // if successfully validated, display preview
    if(response.success){
        occurrenceId = response.id;
        // flag if validation is successful
        validationSucc = true;

        // check if mapping to an existing location
        isExLoc = $('#upload-planting-arrays-switch').is(':checked');

        // if map to new location
        if(!isExLoc) {

            $('.confirm-upload-planting-arrays').addClass('disabled');

            showPreview(occurrenceId);

            panelAction('child', 'show');
            panelAction('parent', 'hide');

            // show confirm button
            $('.confirm-upload-planting-arrays').removeClass('hidden');
            $('#cancel-upload-planting-array').removeClass('modal-close-button');
            
            mappedLocationId = null;

        }else{
            // check if the Occurrence is already mapped
            if(response.mappedLocId !== undefined && response.mappedLocId){
                mappedLocationId = response.mappedLocId;
            } else {
                mappedLocationId = null;
            }

            // if map to an existing location, show the form
            showSpecifyLocForm(occurrenceId, mappedLocationId);
        }
    }
});

// cancel upload and generate location, reset panel displays
$(document).on('click', '#cancel-upload-planting-array, #cancel-generate-location', function(){
    $('.parent-panel').removeClass('slide-in-parent');
    $('.parent-panel').removeClass('slide-out-parent');
    $('.child-panel').removeClass('slide-in-child');
    $('.child-panel').removeClass('slide-out-child');
    $('.parent-panel').css('display','block');
    $('.child-panel').css('display','none');

    // remove confirm button
    $('.confirm-upload-planting-arrays, .confirm-generate-location').addClass('hidden');
    $('.go-to-preview-generate-location-btn').removeClass('hidden');
    $('#cancel-upload-planting-array').addClass('modal-close-button');
});

// go back to parent panel
$(document).on('click', '.go-back-to-parent-panel', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    panelAction('parent', 'show');
    panelAction('child', 'hide');

    // change browse label
    $('.browse-btn span').text('Re-upload');
    // show next button
    $('.next-upload-planting-arrays').removeClass('hidden');
    $('.confirm-upload-planting-arrays').addClass('hidden');
    $('.go-to-preview-generate-location-btn').removeClass('hidden');
    $('.confirm-generate-location').addClass('hidden');

});

// upon clicking next button, update location information
$(document).on('click', '.next-upload-planting-arrays', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    $('.confirm-upload-planting-arrays').addClass('disabled')
    showPreview(occurrenceId);

    // show preview panel
    panelAction('child', 'show');
    panelAction('parent', 'hide');

    // update buttons
    $('#cancel-upload-planting-array').removeClass('modal-close-button');
    $('.confirm-upload-planting-arrays').removeClass('hidden');
    $('.next-upload-planting-arrays').addClass('hidden');
});

// upon clicking next button in generate location, update location information
$(document).on('click', '.go-to-preview-generate-location-btn', function(e){

    let obj = $(this);
    let id = obj.attr('data-id');
    useDesignLayout = $('#use_design_layout').is(":checked");

    $('.confirm-generate-location').addClass('disabled');
    $('.loading').removeClass('hidden');

    setTimeout(function(){

        showPreview(id, 'generateLocation');

        // show preview panel
        panelAction('child', 'show');
        panelAction('parent', 'hide');

        // update buttons
        $('.confirm-generate-location').removeClass('hidden');
        $('.go-to-preview-generate-location-btn').addClass('hidden');
    }, 500);

    e.preventDefault();
    e.stopImmediatePropagation();
});

// upon clicking validate mapping an Occurrence to an existing Location
$(document).on('click', '.validate-upload-planting-arrays', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    var selectedLoc = $("[id*='LOCATION-identification-']").val();

    $('.loading').removeClass('hidden');

    $(this).addClass('hidden');
    $('#cancel-upload-planting-array').addClass('modal-close-button');

    $('.file-error-message').empty();

    // if the Occurrence is not yet mapped to a Location, proceed with validation
    if(mappedLocationId == null){
        setTimeout(function(){
            $.ajax({
                url: '$validateMapToLocUrl',
                type: 'POST',
                data: {id: selectedLoc},
                dataType: 'json',
                success: function(response) {
                    $('.loading').addClass('hidden');
                    // if there is a duplicate
                    if(!response.success){
                        $('.file-error-message').html('<ul><li>Validation Error<pre>'+response.message+'.</pre></li></ul>');
                        $('.file-error-message').css('display','block');

                        $('.kv-upload-progress > .progress').empty();
                        // validation error progress
                        var errorProgress = '<div class="progress-bar bg-danger progress-bar-danger" style="width:100%;">Validation Error</div>';
                        $('.kv-upload-progress > .progress').html(errorProgress);
                    } else {
                        showPreview(occurrenceId);

                        panelAction('child', 'show');
                        panelAction('parent', 'hide');

                        // show confirm button
                        $('.confirm-upload-planting-arrays').removeClass('hidden');
                        $('#cancel-upload-planting-array').removeClass('modal-close-button');
                    }
                }
            });
        }, 500);
    } else {
        showPreview(occurrenceId);

        panelAction('child', 'show');
        panelAction('parent', 'hide');

        // show confirm button
        $('.confirm-upload-planting-arrays').removeClass('hidden');
        $('#cancel-upload-planting-array').removeClass('modal-close-button');
    }

});

// remove file upon clicking re-upload
$(document).on('click', '.browse-btn', function(e){
    // remove previously uploaded file
    $('.fileinput-remove').trigger('click');
    // change browse label
    $('.browse-btn span').text('Browse');
    // remove next button
    $('.next-upload-planting-arrays').addClass('hidden');
    $('#cancel-upload-planting-array').addClass('modal-close-button');
});

// hide validate button upon remove of file
$(document).on('click','.fileinput-remove, .kv-file-remove', function(e){
    $('.validate-upload-planting-arrays').addClass('hidden');
    
    if(isExLoc){
        $('#cancel-upload-planting-array').addClass('modal-close-button');
    }
});

// display preview of upload
function showPreview(occurrenceId, action='') {

    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
    var modalBody = '.upload-planting-arrays-preview-modal-body';
    
    if(action=='generateLocation'){
        var modalBody = '.generate-location-preview-modal-body';
    }
    
    $(modalBody).html(loading);

    $('#upload-planting-arrays-form').submit();

    $('#upload-planting-arrays-form').submit(function(e){

        let formData = $("#upload-planting-arrays-form").serialize();

        formData = formData + '&' + $.param({
            ['occurrenceId'] : occurrenceId,
            ['isExLoc'] : isExLoc,
            ['action'] : action,
            ['useDesignLayout'] : useDesignLayout
        });

        setTimeout(function(){
            $.ajax({
                url: '$previewUploadUrl',
                type: 'POST',
                data: formData,
                success: function(data) {
                    $(modalBody).html(data);
                    $('.loading').addClass('hidden');

                    $('.confirm-generate-location, .confirm-upload-planting-arrays').removeClass('disabled');

                },
                error: function() {
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBody).html(errorMessage);

                    $('.confirm-generate-location, .confirm-upload-planting-arrays').addClass('disabled');
                }
            });
        }, 300);

        e.preventDefault();
        e.stopImmediatePropagation();
    });
}

// display specify existing Location form
function showSpecifyLocForm(occurrenceId, mappedLocationId){

    $.ajax({
        url: '$renderLocationOptionsUrl'+'?id='+occurrenceId+'&mappedLocationId='+mappedLocationId,
        type: 'POST',
        dataType: 'json',
        data: {
            occurrenceId: occurrenceId
        },
        success: function(response) {

            option = response;
            var lookup = {};

            var selector = '[id*=LOCATION-identification-]';
            var selectId = document.querySelector(selector).id;
            var loc = document.getElementById(selectId);
            
            $(selector).empty();
            $('.no-loc-warning, .warning-upload-mapped-plot').remove();

            // if there are no available locations
            if(!option.length){
                noLoc = true;
                var warningIcon = '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i>';
                $( '.parent-box.box-1' ).prepend( '<p class="no-loc-warning">'+warningIcon+' There are no available Locations with same year, season and site.</p>' );
              
            }else {
                $('.validate-upload-planting-arrays').removeClass('hidden');
                for (var i=0; i<option.length; i++) {
                    var newLocOption = document.createElement("option");

                    if (!(parseInt(option[i]['id']) in lookup)) {
                        lookup[parseInt(option[i]['id'])] = 1;

                        if(i==0){
                            var initVal = parseInt(option[i]['id']);
                        }

                        newLocOption.value = parseInt(option[i]['id']);
                        newLocOption.innerHTML = option[i]['text'];
                        loc.options.add(newLocOption);
                    }
                }

                // if occurrence is already mapped
                if(mappedLocationId !== null){

                    var initVal = mappedLocationId;

                    $( '.parent-box.box-1' ).prepend('<div class="alert alert-warning warning-upload-mapped-plot" style="padding:5px;margin-bottom:10px"><i class="material-icons" style="vertical-align:bottom">warning</i> The Occurrence has already been mapped. Click <strong>PROCEED</strong> if you want to update the mapping data.</div>');

                    $('.validate-upload-planting-arrays').html('<i id="w22" class="material-icons right">arrow_forward</i> Proceed');
                    $("[id*='LOCATION-identification-']").attr("disabled",true);
                }

                // display validate button
                $('.validate-upload-planting-arrays').removeClass('hidden');
                $('#cancel-upload-planting-array').removeClass('modal-close-button');
            }

            $(selector).val(initVal).trigger('change');

            $('#upload-planting-arrays-form .box-1').removeClass('hidden');
            $('#upload-planting-arrays-form .box-1').css('float','right');
        }
    });
}

// confirm upload mapped plots
$(document).on('click','.confirm-upload-planting-arrays', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
    
    var modalBody = '.upload-planting-arrays-preview-modal-body';
    $(modalBody).html(loading);

    // disable modal buttons
    $('#cancel-upload-planting-array').addClass('disabled');
    $('.confirm-upload-planting-arrays').addClass('disabled');

    setTimeout(function(){
        $.ajax({
            url: '$confirmUploadUrl'+'?mappedLocationId='+mappedLocationId,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                noAction = true;

                // remove initial toast
                $('.toast').css('display','none');
                
                var color = "green";
                var icon = "check";
                var message = "Successfully uploaded mapped plots.";

                // if unsuccessful
                if (data.success == 'false'){
                    var color = "red";
                    var icon = "close";
                    var message = "There was a problem while uploading mapped plots.";
                }

                // display notification
                var notif = "<i class='material-icons "+ color +"-text left'>"+ icon +"</i> <span class='white-text'>"+ message +"</span>";
                Materialize.toast(notif, 5000);

                $('#upload-planting-arrays-modal').modal('hide');

                // reset modal; show preview panel
                panelAction('child', 'hide');
                panelAction('parent', 'show');

                // reset buttons
                $('#cancel-upload-planting-array').addClass('modal-close-button');
                $('#cancel-upload-planting-array').removeClass('hidden');
                $('.confirm-upload-planting-arrays').addClass('hidden');
                $('.next-upload-planting-arrays').addClass('hidden');
                $('#cancel-upload-planting-array').removeClass('disabled');
                $('.confirm-upload-planting-arrays').removeClass('disabled');

                if (data.success == 'true'){
                    // refresh browser to reflect changes
                    $.pjax.reload({
                        container: '#occurrences-grid-pjax',
                        url: '/index.php/occurrence?program='+ program
                    });
                }
            }
        });
    }, 10);

});

// confirm generate location
$(document).on('click','.confirm-generate-location', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';

    var modalBody = '.generate-location-preview-modal-body';
    $(modalBody).html(loading);

    // disable modal buttons
    $('#cancel-generate-location').addClass('disabled');
    $('.confirm-generate-location').addClass('disabled');

    setTimeout(function(){
        $.ajax({
            url: '$confirmUploadUrl',
            type: 'POST',
            dataType: 'json',
            data: {
                action: 'generateLocation'
            },
            success: function(data){
                noAction = true;

                // remove initial toast
                $('.toast').css('display','none');
                
                var color = "green";
                var icon = "check";
                var message = "Successfully generated location.";

                // if unsuccessful
                if (data.success == 'false'){
                    var color = "red";
                    var icon = "close";
                    var message = "There was a problem while generating location.";
                }

                // display notification
                var notif = "<i class='material-icons "+ color +"-text left'>"+ icon +"</i> <span class='white-text'>"+ message +"</span>";
                // Only display this Toast if generate location was done on a data browser or any pjax-capable widgets
                if (currentPage != 'view-occurrence-basic') {
                    Materialize.toast(notif, 5000);
                }

                $('#generate-location-modal').modal('hide');

                // reset modal; show preview panel
                panelAction('child', 'hide');
                panelAction('parent', 'show');

                $('.confirm-generate-location').addClass('hidden');
                $('.go-to-preview-generate-location-btn').removeClass('hidden');
                $('#cancel-generate-location').removeClass('disabled');
                $('.confirm-generate-location').removeClass('disabled');

                if (data.success == 'true') {
                    // If location was generated within View Occurrence BASIC tab
                    if (window.location.pathname.includes('/occurrence/view')) {
                        // Refresh page to reflect changes
                        window.location.reload()
                    } else {
                        // Refresh data browser to reflect changes
                        $.pjax.reload({
                            container: '#occurrences-grid-pjax',
                            url: '/index.php/occurrence?program='+ program
                        })
                    }
                }
            },
            error: function() {
                var errorMessage = '<i>There was a problem loading the content.</i>';
            }
        });
    }, 100);
});

// upon change of switch selected
$(document).on('click','#upload-planting-arrays-switch', function(e){
    isExLoc = $(this).is(':checked');

    // update location fields
    var selectedLoc = $("[id*='LOCATION-identification-']").val();
    updateLocFields(selectedLoc);
    // to remove previous file uploaded
    $('.kv-file-remove').trigger('click');

    // update form fields
    if(isExLoc) {
        $('.browse-btn, .fileinput-upload-button, .go-to-preview-generate-location-btn').removeClass('disabled');

        // change order of display of form
        $('#upload-planting-arrays-form .box-1').css('float','right');
        $('.upload-panel').css('margin-top','0px');

        // if previous file was already validated
        if(validationSucc){
            $('#upload-planting-arrays-form .box-1').removeClass('hidden');
            $('.validate-upload-planting-arrays').removeClass('hidden');
        }else {
            $('#upload-planting-arrays-form .box-1').addClass('hidden');

            $('.validate-upload-planting-arrays').addClass('hidden');
            $('#cancel-upload-planting-array').addClass('modal-close-button');
        }

        $('.next-upload-planting-arrays').addClass('hidden');
        
        // show validate button when mapping to existing location
        $("div[class*=' field-LOCATION-']").removeClass('hidden');
        $("div[class*=' field-LOCATION_NAME-']").addClass('hidden');
        $("input[id*='DESCRIPTION-identification-']").attr("readonly",true);
        $("[id*='LOCATION_STEWARD-identification-']").attr("disabled",true);
    } else {
        validationSucc = false;

        $('#upload-planting-arrays-form .box-1').removeClass('hidden');
        $('#upload-planting-arrays-form .box-1').css('float','left');
        $('.upload-panel').css('margin-top','-35px');
        $('.next-upload-planting-arrays').addClass('hidden');
        $('.validate-upload-planting-arrays').addClass('hidden');
        $('cancel-upload-planting-array').addClass('modal-close-button');

        $("[id*='LOCATION_NAME-identification-']").trigger('keyup');
        $("div[class*=' field-LOCATION-']").addClass('hidden');
        $("div[class*=' field-LOCATION_NAME-']").removeClass('hidden');
        $("input[id*='DESCRIPTION-identification-']").attr("readonly",false);
        $("[id*='LOCATION_STEWARD-identification-']").attr("disabled",false);

        $('.browse-btn, .fileinput-upload-button').removeClass('disabled');
    }

    // remove notification message
    $('.warning-upload-mapped-plot').remove();

    // save to session is existing location selection
    $.ajax({
        url: '$saveIsExLocSelectionUrl',
        type: 'POST',
        data: {
            isExLoc: isExLoc
        }
    });

});

// upon change of location selection
$(document).on('change', '[id*="LOCATION-identification-"]', function(e){

    locationId = $(this).val();
    updateLocFields(locationId);

    // hide next button and show validate button
    $('.next-upload-planting-arrays').addClass('hidden');
    
    // if there are no valid locations
    if(noLoc){
        $('.validate-upload-planting-arrays').addClass('hidden');
    } else {
        $('.validate-upload-planting-arrays').removeClass('hidden');
    }
});

// update location fields
function updateLocFields(locationId){
    // update steward and description based on the selected location
    $.ajax({
        url: '$getLocationInfoUrl',
        type: 'POST',
        dataType: 'json',
        data: {
            locationId: locationId    
        },
        success: function(data) {
            var stewardId = data.stewardId;
            var description = data.description;

            $("[id*='LOCATION_STEWARD-identification-']").val(stewardId);
            $("[id*='LOCATION_STEWARD-identification-']").trigger('change');
            $("[id*='DESCRIPTION-identification-']").val(description);
        }
    });
}

// validate if required fields specified upon file load
function validate(action){
    var reqField = $('.required-field').val();

    if($('.default-namegen-switch').prop('checked') == false){ 
        if(reqField.length == 0 && !isExLoc){
            $('.browse-btn, .fileinput-upload-button').addClass('disabled');
        } else {
            $('.browse-btn, .fileinput-upload-button').removeClass('disabled');
        }
    } else {
        $('.browse-btn, .fileinput-upload-button').removeClass('disabled');
    }

    // if file is cleared
    if(action == 'clear'){
        validationSucc = false;
    }
}
JS
);
