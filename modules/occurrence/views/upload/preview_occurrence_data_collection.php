<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders preview upload occurrence data collection
 */
use kartik\grid\GridView;

echo '<a href="#" class="go-back-to-parent-panel" title="' . \Yii::t('app', 'Go back') . '">
    <i class="material-icons">arrow_back</i>
</a>';

echo '<p>Below occurrence data values will either be inserted as a new occurrence data, or be used to update existing values. Click confirm to proceed.</p>';

?>
<ul class="collapsible" data-collapsible="accordion">
    <li>
        <div class="collapsible-header">Occurrences</div>
        <div class="collapsible-body">
            <?php

            // if occurrences are greater than 100, display a note
            if($totalOccurrenceCount > 100){
                echo '100 out of ' . number_format($totalOccurrenceCount) . ' occurrences are displayed here.';
            }

            $columns = array_merge([
                [
                    'header' => '',
                    'class' => 'yii\grid\SerialColumn'
                ],
                
            ], array_keys($occurrenceDataProvider->allModels[0]));

            echo GridView::widget([
                'dataProvider' => $occurrenceDataProvider,
                'id' => 'preview-upload-occurrence-data-collection-occurrence-grid',
                'columns' => $columns,
                'tableOptions' => [
                    'class' => 'table white z-depth-3'
                ],
            ]);
            ?>
        </div>
    </li>
</ul>

<?php
$this->registerJs(
    <<<JS
    $('.collapsible').collapsible();
    $(document).on('ready pjax:success', function(e) {
        $('.collapsible').collapsible();
    });
JS
);
