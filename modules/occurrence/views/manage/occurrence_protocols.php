<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders the landing page for Update/Manage Occurrences
 */

use kartik\dynagrid\DynaGrid;
use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\helpers\Url;

$occurrencesUrl = Url::to(['/occurrence', 'program' => $program]);

if (!$streamlined) {
    $subTitle = ($hasMultipleSelectedOccurrences) ? 'Update Occurrence Protocols' : 'Update Occurrence Protocols of ' . $occurrenceName;

    echo '
        <h3>' .
            Html::a( Yii::t('app', 'Experiment Manager'), $occurrencesUrl) .
            ' <small>» ' . $subTitle . '</small>'.
        '</h3>';
} else {
    $subTitle = ($hasMultipleSelectedOccurrences) ? 'Update Occurrence Protocols' : 'Update Occurrence Protocols of ' . "<b>$occurrenceName</b>";

    echo '<p>' . $subTitle . '</p>';
}

// if no write permission to any of the selected occurrences, notify the user and do not display the form
if($occurrenceDbIdsString == ''){
    echo '<div class="alert alert-warning" style="padding:5px;margin-bottom:10px"><i class="material-icons" style="vertical-align:bottom">warning</i> You do not have permission to any of the selected occurrence/s.</div>';
}
else {
?>

<!-- Render a data browser to display current Occurrence protocol variable values -->
<?if ($hasMultipleSelectedOccurrences) {
    $columns = [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => false,
            'visible'=>true,
        ],
        [
            'attribute' => 'experimentCode',
        ],
        [
            'attribute' => 'experiment',
        ],
        [
            'attribute' => 'occurrence',
        ],
        [
            'attribute' => 'siteCode',
        ],
        [
            'attribute' => 'fieldCode',
        ],
        [
            'attribute' => 'location',
        ],
        $occurrenceAttributesColumns[0],
        $occurrenceAttributesColumns[1],
        $occurrenceAttributesColumns[2],
        ...$plantingProtocolVariablesColumns,
        $occurrenceAttributesColumns[3],
        $occurrenceAttributesColumns[4],
        $occurrenceAttributesColumns[5],
        $occurrenceAttributesColumns[6],
        $occurrenceAttributesColumns[7],
    ];

    echo '
        <ul class="collapsible">
            <li>
                <div class="collapsible-header active">
                    <i class="material-icons">remove_red_eye</i> ' . \Yii::t('app', "This is the browser for the selected occurrences for updating. You may use this for reference. Click this tab to toggle the display.") . '
                </div>
                <div class="collapsible-body">';
                    // Variables browser
                    DynaGrid::begin([
                        'options' => [
                            'id' => 'selected-occurrence-list-grid'
                        ],
                        'columns' => $columns,
                        'theme' => 'simple-default',
                        'showPersonalize' => true,
                        'storage' => 'cookie',
                        'showFilter' => false,
                        'showSort' => false,
                        'allowFilterSetting' => false,
                        'allowSortSetting' => false,
                        'gridOptions' => [
                            'id' => 'selected-occurrence-list-grid',
                            'dataProvider' => $dataProvider,
                            'pjax' => true,
                            'pjaxSettings' => [
                                'options' => [
                                    'enablePushState' => false,
                                    'id' => 'selected-occurrence-list-grid'
                                ],
                            ],
                            'panel' => [
                                'before' => false,
                                'after' => false,
                            ],
                            'responsiveWrap' => true,
                            'floatHeader' => true,
                            'floatOverflowContainer' => true,
                            'toolbar' => [],
                        ],
                    ]);
                    DynaGrid::end();
    echo '
                </div>
            </li>
        </ul>';
}?>


<!-- Render tabs for different Protocols and input fields for each -->
<?
$viewArrCount = sizeof($viewArr);

if ($viewArrCount > 0) {
    $items = [];

    // Prepare each View file for the Protocols
    foreach ($viewArr as $key => $value) {
        if (
            (!$isAdmin && strtoupper($role) == 'COLLABORATOR') &&
            !in_array(
                strtolower($value['display_name']),
                ['planting', 'management', 'harvest']
            )
        )
            continue;
        $isActive = ($key == 0) ? true : false;
        $filename = $value['action_id'];

        // View file exists
        if (isset($filename)) {
            $content = Yii::$app->controller->renderPartial(
                "@app/modules/experimentCreation/views/protocol/$filename.php",
                [
                    'id' => $experimentDbId,
                    'program' => $program,
                    'processId' => $dataProcessDbId,
                    'shouldUpdateOccurrenceProtocols' => '1',
                    'occurrenceDbId' => $templateOccurrenceDbId,
                    'occurrenceDbIdsString' => $occurrenceDbIdsString,
                    'hasMultipleSelectedOccurrences' => ($hasMultipleSelectedOccurrences) ? '1' : '',
                ]
            );

            $items []= [
                'label' =>
                    '<i class="' .$value['item_icon'] . '" style="margin-right: 8px;"></i>' .
                    Yii::t('app',$value['display_name']),
                'active' => $isActive,
                'content' => $content,
                'linkOptions'=> ['class'=>"pr-tabs pr-$filename-tab"]
            ];
        }
    }

    // Render the side tabs and content representing their respective Protocol
    if ($hasMultipleSelectedOccurrences) echo '<div class="alert alert-info" style="padding:5px;margin-bottom:10px"><i class="material-icons" style="vertical-align:bottom">info</i> Note that saving changes to the Protocols do not apply to certain Experiment Types.</div>';

    echo TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_LEFT,
        'encodeLabels' => false,
        'pluginOptions' => [
            'enableCache' => false
        ]
    ]);
} else {
    Yii::$app->session->setFlash('error',\Yii::t('app','No protocols found in the database configuration. Refresh the page or please contact web admin.'));
}
}
?>

<?
$script = <<<JS
    $(document).ready(function() {
        // Dynamically reduce vertical space within data browser        
        var maxRows = Math.min($('tr.selected-occurrence-list-grid').length, 5);
        var maxHeight = 50 + 60 * maxRows;
        $(".kv-grid-wrapper").css("height", maxHeight);
        
        // Reduce vertical space below data browser
        $('#selected-occurrence-list-grid').find('.panel-footer').addClass('hidden').trigger('change');
    }) 
JS;

$this->registerJS($script);

$this->registerCssFile("@web/css/box.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'box-css');
?>