<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders manage occurrence basic information and data web form
 */
use yii\helpers\Url;
use app\components\GenericFormWidget;
use yii\bootstrap\ActiveForm;

// loading indicator
echo '<div class="hidden margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div><br/>';

// web form
ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'browser-detail-view-basic-info-form',
    'action' => ['/occurrence/manage/basic-info']
]);

echo GenericFormWidget::widget([
    'options' => $widgetOptions
]);

ActiveForm::end();

$this->registerJs(<<<JS
// for generic validation
function checkFields(){
    var response = [];
    response['success'] = true;
    response['required'] = null;
    response['datatype'] = null;
    var tempArr = [];
    var count = 0;

    $(".required-field").each(function(){
        var divFieldId = 'field-'+$(this).attr('id');

        if($(this).val().length == 0){
            response['success'] = false;
            var strArr = (this.id).split('-');
            var label =  $('label[for=' + strArr[0] + ']').attr("data-label");
            var count = tempArr.length;
            tempArr.push((count+1)+'. '+label);
            $('.'+divFieldId).addClass('has-error');
            $('.'+divFieldId).find(".help-block").html('Please enter a value for '+label+'.');
        }else{
            $('.'+divFieldId).removeClass('has-error');
            $('.'+divFieldId).find(".help-block").html('');
        }
    });

    response['datatype'] = tempArr.join("<br>");
    return response;
}
JS
);
?>
