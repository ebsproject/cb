<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders entry point-streamlined manage Occurrence Protocols
 */

use kartik\tabs\TabsX;

// Partially render  manage tabs
echo Yii::$app->controller->renderPartial('tabs', [
    'experimentId' => $experimentId,
    'program' => $program,
    'active' => 'codePatterns',
    'experiment' => $experiment,
    'occurrenceId' => $occurrenceId,
    'isAdmin' => $isAdmin,
    'role' => $role,
    'creatorId' => $creatorId,
    'accessData' => $accessData,
]);
?>

<!-- render Toggle here -->
<div class="row switch" >
    <label>
        Experiment
        <input 
            type="checkbox"
            class="code-generation-data-level-switch"
            <?echo ($level === 'experiment') ? '' : 'checked';?>>
        <span class="lever"></span>
        Occurrence
    </label>
</div>

<?
$contentArr = [];
$items = [];

// Partially render Code Patterns - Entry 
$contentDataArr []= [
    'content' => Yii::$app->controller->renderPartial('code_patterns', $entryCodePatternsParams),
    'active' => true,
    'displayName' => 'Entry',
    'className' => 'code-patterns-entry', 
];

// Partially render  Code Patterns - Plot
$contentDataArr []= [
    'content' => Yii::$app->controller->renderPartial('code_patterns', $plotCodePatternsParams),
    'active' => false,
    'displayName' => 'Plot',
    'className' => 'code-patterns-plot', 
];

// Load the 2 Code Patterns pages
foreach ($contentDataArr as $key => $contentData) {
    $items []= [
        'label' => Yii::t('app',$contentData['displayName']),
        'active' => $contentData['active'],
        'content' => $contentData['content'],
        'linkOptions'=> ['class'=>"pr-tabs pr-{$contentData['className']}-tab"],
    ];
}

// Render the pages separated by TabsX's
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_LEFT,
    'encodeLabels' => false,
    'pluginOptions' => [
        'enableCache' => false
    ]
]);

$this->registerJs(<<<JS
    $(document).on('change', '.code-generation-data-level-switch', function (e) {
        if($('.code-generation-data-level-switch').prop('checked') == false) {
            // Experiment-level
            window.location = `/index.php/occurrence/manage/code-generation?program=$program&experimentId=$experimentId&occurrenceId=$occurrenceId&level=experiment`
        } else {
            // Occurrence-level
            window.location = `/index.php/occurrence/manage/code-generation?program=$program&experimentId=$experimentId&occurrenceId=$occurrenceId&level=occurrence`
        }
    })
JS);