<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders manage experiment info
 */

use app\components\GenericFormWidget;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

// render manage tabs
echo Yii::$app->controller->renderPartial('tabs', [
    'experimentId' => $experimentId, 
    'program' => $program, 
    'active' => 'experiment',
    'experiment' => $experiment,
    'occurrenceId' => $occurrenceId,
    'isAdmin' => $isAdmin,
    'role' => $role,
    'creatorId' => $creatorId,
    'accessData' => $accessData,
]);

echo '<p>' . 
    \Yii::t('app','You may specify and add more variables for experiment information here.') . ' ' .
    '<strong>' . \Yii::t('app','Changes are autosaved.') . '</strong>' .
'</p>';

// add variables select2 plugin options
$dataUrl = Url::home() . '/occurrence/' . Url::to('manage/get-variable-options');
$pluginOptions = [
    'allowClear' => true,
    'minimumInputLength' => 0,
    'placeholder' => 'Select variable',
    'minimumResultsForSearch' => 1,
    'language' => [
        'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
    ], 
    'ajax' => [
        'url' => $dataUrl,
        'dataType' => 'json',
        'delay' => 250,
        'data' => new JsExpression(
            'function (params){
                var attr = $(this).attr("data-attr");
                var abbrevs = $(this).attr("data-excludedVars");
                var level = $(this).attr("data-level");

                return {
                    q: params.term, 
                    attr: attr,
                    page: params.page,
                    excludedVars: abbrevs,
                    level: level
                }
            }'
        ),
        'processResults' => new JSExpression(
            'function (data, params) {

                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 5) < data.totalCount
                    },
                };
            }'
        ),
        'cache' => true
    ]
];

$options = [
    'placeholder' => 'Select variable',
    'tags' => true,
    'class' => 'filter-dashboard-select',
    'data-attr' => 'label',
    'data-excludedVars' => json_encode($excludedVars),
    'data-level' => 'experiment',
    'minimumResultsForSearch' => 1,
    'multiple' => true
];

// render add more variables field
echo '<div class="col col-md-12"><div class="col col-md-6">'. Select2::widget([
    'name' => 'manage-experiment-add-variable',
    'id' => 'manage-experiment-add-variable',
    'maintainOrder' => true,
    'options' => $options,
    'pluginOptions' => $pluginOptions,
    'value' => '',
    'showToggleAll' => false
]) . '</div>' .
'<div class="col col-md-4">' . Html::a(\Yii::t('app','Add variable'),
    '#!',
    [
        'class' => 'btn btn-primary waves-effect waves-light add-variable-btn disabled',
        'style' => 'height:37px; line-height:37px',
        'title' => 'Add new variables in the form'
    ]) . '</div>' .
'</div>' .
'<div class="row"></div>';

ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'create-basic-experiment-form',
    'action'=>['/occurrence/manage/experiment', 'id' => $experimentId, 'program' => $program],
]);

echo GenericFormWidget::widget([
    'options' => $widgetOptions
]);

ActiveForm::end();

// update experiment name modal confirmation
Modal::begin([
    'id' => 'update-experiment-name-modal',
    'header' => '<h4><i class="fa fa-edit"></i> Update experiment name</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#',
            [
                'data-dismiss'=>'modal',
                'id' => 'cancel-update-form-value-btn',
                'title' => 'Cancel update'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm').'<i class="material-icons right">send</i>',
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light update-form-value-btn',
                'id'=>'confirm-update-value-btn',
                'data-dismiss'=>'modal',
                'title' => 'Confirm updating of value'
            ]
        ) .
        '&nbsp;&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> Do you also want to update the experiment name and occurrence names? <br><br>This action cannot be undone. Click confirm to proceed.';

Modal::end();

// delete value modal confirmation
Modal::begin([
    'id' => 'delete-value-confirmation-modal',
    'header' => '<h4><i class="fa fa-trash"></i> Delete value</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#',
            [
                'data-dismiss'=>'modal',
                'id' => 'cancel-delete-form-value-btn',
                'title' => 'Cancel deletion'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm').'<i class="material-icons right">send</i>',
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light delete-form-value-btn',
                'id'=>'confirm-delete-value-btn',
                'data-dismiss'=>'modal',
                'title' => 'Confirm deletion of value'
            ]
        ) .
        '&nbsp;&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete the value. This action cannot be undone. Click confirm to proceed.';

Modal::end();

// encode added variables array
$varAbbrevsStr = !empty($varAbbrevs) ? json_encode($varAbbrevs) : json_encode($varAbbrevs);
// manage experiment url
$manageExpUrl = Url::to(['manage/experiment', 'program' => $program, 'experimentId' => $experimentId]);
// save form values url
$saveRowValueUrl = Url::to(['manage/save-tabular-form-row-value']);
// update experiment name url
$updateExperimentNameUrl = Url::to(['manage/update-experiment-name','id'=>$experimentId]);

$this->registerJs(<<<JS
    var originalValue = '';
    var id = null;
    var fields = null;
    var column = null; // variable abbrev
    var variableId = null; // variable identifier
    var experimentDbId = $experimentId; // experiment identifier
    var value = null; // value
    var isRequired = null; // whether required or not
    var type = '';
    var exptNameFieldId = '';
    var skipColumnCheckFlag = false;

    // if variable is selected, enable add variable button
    $(document).on('change', "#manage-experiment-add-variable", function(e) {
        var selected = $('#manage-experiment-add-variable').val();

        // enable add variable button if there is selected variable
        if(selected.length){
            $('.add-variable-btn').removeClass('disabled');
        } else {
            $('.add-variable-btn').addClass('disabled');
        }
    });

    // add new variables in the form
    $(document).on('click', '.add-variable-btn', function(e){
        // newly added variable
        var varAbbrevs = $('#manage-experiment-add-variable').val();
        
        // previously added vars
        var varAbbrevsStr = '$varAbbrevsStr';
        var origVarAbbrevs = JSON.parse(varAbbrevsStr);

        // merge original variables and newly added vars
        var newCombinedVarAbbrevs = origVarAbbrevs.concat(varAbbrevs);
        var varAbbrevsStr = JSON.stringify(newCombinedVarAbbrevs);
        var manageUrl = '$manageExpUrl' + '&varAbbrevs=' + varAbbrevsStr;

        window.location = manageUrl;
    });

    // upon mousedown on datepicker, get current value to store when cancel delete
    $(document).on('mousedown', '.krajee-datepicker', function(e){
        id = this.id;
        value = $('#' + id).val().trim(); // value

        originalValue = (value != '') ? value : originalValue;
    });

    // upon change of select2 and date field value
    $(document).on('keyup change', ".select2-form-fields", function(e) {
        e.stopPropagation();
        e.preventDefault();

        id = this.id;
        fields = (this.id).split('-');
        column = fields[0]; // variable abbrev
        variableId = fields[2]; // variable identifier
        value = $('#' + id).val().trim(); // value
        isRequired = $(this).hasClass('required-field'); // whether required or not
        type = $('#' + id).attr('type');

        originalValue = (value != '') ? value : originalValue;

        // if value is empty, display confirmation
        if(value == ''){
            $('#delete-value-confirmation-modal').modal('show');
        } else {
            saveFormValue();
        }
    });

    // upon change of text input field value
    $(document).on('blur', ".text-editable-field, .time-editable-field, .numeric-editable-field, .float-editable-field", function(e) {
        e.stopPropagation();
        e.preventDefault();

        id = this.id;
        fields = (this.id).split('-');
        column = fields[0]; // variable abbrev
        variableId = fields[2]; // variable identifier
        value = $('#' + id).val().trim(); // value
        isRequired = $(this).hasClass('required-field'); // whether required or not
        type = $('#' + id).attr('type');

        originalValue = (value != '') ? value : originalValue;

        // if field is required and value specified is empty
        if(isRequired && value == ''){

            message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'Field is required. Please specify a value.'

            $('.toast').css('display','none');
            Materialize.toast(message, 5000);

            // set original value
            $(this).val(originalValue);

        } else{
            // if value is empty, display confirmation
            if(value == '' && originalValue != ''){
                $('#delete-value-confirmation-modal').modal('show');
            } else {
                saveFormValue();
            }
        }
    });

    // upon change of text input field value
    $(document).on('change', ".time-editable-field", function(e) {
        e.stopPropagation();
        e.preventDefault();

        id = this.id;
        value = $('#' + id).val().trim(); // value
 
        var isValid = isValidDate(value);

        // invalid date
        if(!isValid && value != ''){
            message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'Invalid input for date.'

            $('.toast').css('display','none');
            Materialize.toast(message, 5000);

            // set original value
            $(this).val(originalValue);
        } else if(value == '' && originalValue != ''){
            // if value is empty, display confirmation

            $('#delete-value-confirmation-modal').modal('show');
        } else {
            saveFormValue();
        }
    });

    // upon change input and date field
    $(document).on('mousedown', ".text-editable-field, .time-editable-field, .numeric-editable-field, .float-editable-field", function(e) {
        id = this.id;
        value = $('#' + id).val().trim(); // value

        originalValue = value;
    });

    // cancel confirm delete field value
    $(document).on('click', '#cancel-delete-form-value-btn', function(e){

        // if field is not select2
        if(type !== undefined){
            // set original value before removing value
            $('#' + id).val(originalValue);
        } else {
            location.reload();
        }

        message = "<i class='material-icons orange-text left'>warning</i> Delete cancelled.";

        $('.toast').css('display','none');
        Materialize.toast(message, 5000);

    });

    // confirm delete value
    $(document).on('click', '#confirm-delete-value-btn', function(e){
        saveFormValue('delete');
    });

    // Check if date is valid
    function isValidDate(dateString) {
        var isValid = true;
        var regEx = /^\d{4}-\d{2}-\d{2}$/;
        
        isValid = dateString.match(regEx) != null;

        // if format is valid, check if year, month and day are valid
        if(isValid){
            var dateArr = dateString.split("-");

            var year = dateArr[0];
            var month = dateArr[1];
            var day = dateArr[2];

            if(year < 1970){
                return 'Please enter year later than 1969';
            }

            if(month > 12){
                return 'Please enter a valid month';
            }

            if(day > 31){
                return 'Please enter a valid day';
            }
        }

        return isValid;
    }

    // Save form value
    function saveFormValue(action='save'){

        const columnsInExperimentName = ['STAGE','EXPERIMENT_YEAR','SEASON']
        if (columnsInExperimentName.includes(column) && skipColumnCheckFlag === false) {
            $('#update-experiment-name-modal').modal('show');
        } else {
            
            $.ajax({
                url: '$saveRowValueUrl',
                type: 'post',
                cache: false,
                data: {
                    entityDbId: experimentDbId,
                    column: column,
                    variableDbId: variableId,
                    dataValue: value,
                    entity: 'experiment'
                },
                success: function(response) {
                    if(action == 'delete'){
                        message = "<i class='material-icons green-text left'>check</i> Value deleted.";

                        $('.toast').css('display','none');
                        Materialize.toast(message, 5000);
                    }

                    skipColumnCheckFlag = false;
                },
                error: function(){
                    message = "<i class='material-icons red-text left'>close</i> There was a problem saving the value.";

                    $('.toast').css('display','none');
                    Materialize.toast(message, 5000);
                }
            });
        }
    }

    // cancel update experiment name
    $(document).on('click', '#cancel-update-form-value-btn', function(e) {
        e.stopPropagation();
        e.preventDefault();
        
        skipColumnCheckFlag = true;
        saveFormValue();
    })

    // confirm update experiment name
    $(document).on('click', '#confirm-update-value-btn', function(e) {
        
        // get existing values from stage, experiment year, and season
        $('.required-field').each(function(){
            var fieldId = $(this).attr('id');
            
            if(fieldId.includes('STAGE')){
                inputStage = $('#'+fieldId).val();
                var tempStr = inputStage.split('-');
                inputStage = tempStr[1];
            }
            if(fieldId.includes('EXPERIMENT_YEAR')){
                inputYear = $('#'+fieldId).val();
            }
            if(fieldId.includes('SEASON')){
                inputSeason = $('#'+fieldId).val();
            }
            if(fieldId.includes('EXPERIMENT_NAME')){
                exptNameFieldId = fieldId;
                inputExperimentName =  $('#'+fieldId).val();
            }
        });

        inputs = JSON.stringify({
            'stage': inputStage,
            'year': inputYear,
            'season': inputSeason,
            'inputExperimentName': inputExperimentName,
        })
    
        skipColumnCheckFlag = true;
        updateExperimentName(inputs)
    })

    function updateExperimentName(inputs) {
        params = JSON.parse(inputs)

        $.ajax({
            url: '$updateExperimentNameUrl',
            type: 'post',
            dataType: 'json',
            async: false,
            cache: false,
            data: {
                stage: params['stage'],
                year: params['year'],
                season: params['season']
            },
            success: function(response) {
                var oldExperimentName = params['inputExperimentName']
                var newExperimentName = response.experimentName;

                // show new Experiment Name in input field
                $('#'+ exptNameFieldId).val(newExperimentName);

                var breadcrumbExperimentName = $("h3 small a:contains("+oldExperimentName+")");
                var breadcrumbUrl = breadcrumbExperimentName.attr('href');
                breadcrumbUrl = breadcrumbUrl.replace(oldExperimentName, newExperimentName);

                // use new Experiment Name as label and redirect param
                breadcrumbExperimentName.text(newExperimentName);
                breadcrumbExperimentName.attr('href', breadcrumbUrl);

                message = "<i class='material-icons blue-text left'>info</i> New experiment name and occurrence name(s) generated. Kindly click the Experiment Name at the top to see the updated occurrence names.";

                $('.toast').css('display','none');
                Materialize.toast(message, 5000);
                saveFormValue();
            }
        });
    }
JS);

$this->registerCss('
    .required{
        color:red;
    }
    label{
        font-size: .8rem;
        color: #333333;
      }
    .readonly{
        cursor: not-allowed;
        user-select: none;
        -webkit-user-modify: read-only;
        pointer-events: none;
        border: 1px solid grey;
        background-color: lightgrey;
    }
    .div-class{
        margin-bottom: 10px;
    }
    .my-container{
        min-width: 50%;
        max-width: 90%
    }
    .card{
        height: 100%;
        margin-left:-10px;
        margin-top:-10px;
        padding: 30px;
    }
    .form-inline input{
        flex-direction: column;
        align-items: stretch;
        vertical-align: middle;
    }
    .form-inline { 
        display: flex;
        flex-flow: row wrap;    
        align-items: center;
    }
    .view-entity-content{
        background: #ffffff;
    }
    #basic-info1{
        margin-left:auto;
        margin-right:auto;
        margin-top:15px;
        display: inline-block;
    }
    #basic-info2{
        margin-left:auto;
        margin-right:auto;
        margin-top:20px;
        display: inline-block;
    }
    .parent-box{
        position: relative;
        margin-top: 1rem;
    }
    .main-panel{
        margin-left: 5%;
    }
    .datepicker{
        z-index: 2 !important;
    }
');