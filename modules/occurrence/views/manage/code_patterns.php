<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders the landing page for Update/Manage Occurrences
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/*
    ADD THIS TO ENABLE JS-CSS
*/
Modal::begin([]);
Modal::end();

$occurrencesUrl = Url::to(['/occurrence', 'program' => $program]);

if (!$streamlined) {
    $subTitle = Yii::t('app', "Generate $entity code patterns");

    echo '<h3>' .
        Html::a( Yii::t('app', 'Experiment Manager'), $occurrencesUrl) .
        ' <small>» ' . $subTitle . '</small>'.
    '</h3>';
}?>

<div class="container">
    <!-- This is the helper text above the collapsible panels -->
    <? echo ($level === 'experiment' && !$streamlined) ? "<div class='row'>
        <div id='main-helper-text' class='col s12'>You have selected <b>" . count($ids) . " {$level}s</b>.</div>
    </div>" : ''?>
    <!-- This contains the collapsible panels -->
    <div id="collapsible-section" class="row">
        <div class="col s6">
            <ul class="collapsible">
                <? // Render collapsible content here
                    $i = 0;
                    foreach ($ids as $key => $value) {
                        if (empty($value)) continue;
                        
                        $key = $entity.$key;
                        $testVar = $value[array_key_first($value)][0];
                        $occurrenceCount = 0;
                        $experimentName = $experimentNames[$i] ?? '';

                        // Traverse through array of occurrences mapped to each experiment ID
                        foreach ($value as $k => $v) {
                            $occurrenceCount += count($value[$k]);
                        }

                        $helperText = ($level === 'experiment') ? "Experiment <b>{$experimentName}</b> has <b>" . $occurrenceCount . " occurrences</b>. Generate your $entity code pattern here." :
                            "You have selected <b> $occurrenceCount occurrences</b>. Generate your $entity code pattern here.";


                        $applyToAllOccurrencesCheckbox = ($level == 'experiment') ? ("
                            <div class='col s12 left-align'>
                                <input id='apply-to-all-checkbox-$key' type='checkbox' class='filled-in'/>
                                <label for='apply-to-all-checkbox-$key'>
                                <span
                                    title='" . Yii::t('app', "Applies same sequence number to all occurrences of $experimentName") . "'
                                    style='font-size: 12px;'
                                >Apply to all occurrences</span>
                                </label>
                            </div>") : '';

                        $i += 1;
                        echo "
                            <li>
                                <div id='preview-header-$key' class='collapsible-header active item-theme-color white-text'>
                                        <span 
                                            class='col s12'
                                        >
                                        " . $experimentName . "
                                        </span>
                                        <i class='material-icons'>expand_more</i>
                                </div>
                                <div class='collapsible-body'>
                                    <div class='row'>
                                        <span class='col s12'>$helperText</span>
                                    </div>
                                    <div class='row center-align' style='margin-left: 32px;'>
                                        <form id='generate-code-pattern-form-$key'>
                                            <div id='starting-sequence-div-$key' class='row'>
                                            <div class='col s6 input-field'>
                                                <input
                                                    id='starting-sequence-field-$key'
                                                    type='text'
                                                    class='starting-sequence-field'
                                                    data-key='$key'
                                                    title='" . Yii::t('app', 'Number that the code sequence starts at') . "'
                                                    placeholder='101, 201, 301'
                                                    required
                                                >
                                                <label for='starting-sequence-field-$key'>Starting Sequence Number <span style='color: red''><b>*</b></span></label>
                                                <span class='help-block left'></span>
                                            </div>
                                            " . $applyToAllOccurrencesCheckbox . "
                                            </div>
                                            <div id='leading-zeroes-div-$key' class='row'>
                                            <div class='col s3 input-field '>
                                                <input
                                                    id='leading-zeroes-field-$key'
                                                    type='number'
                                                    class='leading-zeroes-field'
                                                    data-key='$key'
                                                    title='" . Yii::t('app', 'Number of leading zeroes') . "'
                                                    value='0'
                                                    min='0'
                                                >
                                                <label for='leading-zeroes-field-$key'>Leading Zeroes</label>
                                            </div>
                                            </div>
                                            <div id='code-pattern-div-$key'>
                                                <div class='row'>
                                                    <label class='col s12 left-align' title='" . Yii::t('app', 'Configure your code pattern here') . "'>Pattern</label>
                                                    <span class='col s12 left-align'>Colons (:) and pipes (|) are not allowed.</span>
                                                    <span id='help-block-error-$key' class='col s12 left-align'></span>
                                                </div>
                                                <div class='row'>
                                                    <div class='col s12 left-align fixed'>
                                                        <a
                                                            id='choose-prefix-dropdown-$key'
                                                            class='waves-effect waves-light dropdown-button btn'
                                                            data-activates='choose-prefix-dropdown-list-$key'
                                                            data-beloworigin='true' 
                                                            data-constrainwidth='false'
                                                            title='" . Yii::t('app', 'Add code prefix') . "'
                                                        >" .
                                                            'Add ' . Yii::t('app', 'prefix') .
                                                        "</a>
                                                        <ul
                                                            id='choose-prefix-dropdown-list-$key'
                                                            class='dropdown-content'
                                                        >" .       
                                                            "<li
                                                            >" .
                                                                Html::a(
                                                                    '<p style="display: inline;">Free Text</p>',
                                                                    null,
                                                                    [
                                                                        'id' => "add-free-text-prefix-$key",
                                                                        'class' => "add-affix-$entity",
                                                                        'data-position' => 'prefix',
                                                                        'data-key' => "$key",
                                                                        'data-type' => 'free text',
                                                                        'data-entity' => $entity,
                                                                    ]
                                                                ) .
                                                            "</li>
                                                            <li
                                                            >" .
                                                                Html::a(
                                                                    '<p style="display: inline;">Variable</p>',
                                                                    null,
                                                                    [
                                                                        'id' => "add-variable-prefix-$key",
                                                                        'class' => "add-affix-$entity",
                                                                        'data-position' => 'prefix',
                                                                        'data-key' => "$key",
                                                                        'data-type' => 'variable',
                                                                        'data-entity' => $entity,
                                                                    ]
                                                                ) .
                                                            "</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id='code-pattern-affixes-$key' class='row draggable-container'>
                                                    <div class='draggable row' draggable='true'>
                                                        <div
                                                            class='col s4 push-s1 card'
                                                            style='
                                                                padding-top: 12px;
                                                                padding-bottom: 12px;
                                                                text-align: left;
                                                            '
                                                        >
                                                            <input
                                                                type='hidden'
                                                                class='code-affix-$entity'
                                                                data-type='sequence'
                                                                value='sequence'
                                                            >
                                                            <span><b>SEQUENCE NUMBER</b></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='row'>
                                                    <div class='col s12 left-align'>
                                                        <a
                                                            id='choose-suffix-dropdown-$key'
                                                            class='waves-effect waves-light dropdown-button btn'
                                                            data-activates='choose-suffix-dropdown-list-$key'
                                                            data-beloworigin='true' 
                                                            data-constrainwidth='false'
                                                            title='" . Yii::t('app', 'Add code suffix') . "'
                                                        >" .
                                                            'Add ' . Yii::t('app', 'suffix') .
                                                        "</a>
                                                        <ul
                                                            id='choose-suffix-dropdown-list-$key'
                                                            class='dropdown-content'
                                                        >" .             
                                                            "<li
                                                            >" .
                                                                Html::a(
                                                                    '<p style="display: inline;">Free Text</p>',
                                                                    null,
                                                                    [
                                                                        'id' => "add-free-text-suffix-$key",
                                                                        'class' => "add-affix-$entity",
                                                                        'data-position' => 'suffix',
                                                                        'data-key' => "$key",
                                                                        'data-type' => 'free text',
                                                                        'data-entity' => $entity,
                                                                    ]
                                                                ) .
                                                            "</li>
                                                            <li
                                                            >" .
                                                                Html::a(
                                                                    '<p style="display: inline;">Variable</p>',
                                                                    null,
                                                                    [
                                                                        'id' => "add-variable-suffix-$key",
                                                                        'class' => "add-affix-$entity",
                                                                        'data-position' => 'suffix',
                                                                        'data-key' => "$key",
                                                                        'data-type' => 'variable',
                                                                        'data-entity' => $entity,
                                                                    ]
                                                                ) .
                                                            "</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='right-align'>
                                                " .
                                                Html::button(
                                                    \Yii::t('app', 'Save') ,
                                                    [
                                                        'id' => "generate-code-pattern-$key",
                                                        'class' => "waves-effect waves-light btn generate-code-pattern-$entity",
                                                        'data-key' => "$key",
                                                        'data-occurrence-count' => "$occurrenceCount",
                                                        'data-entity' => $entity,
                                                        'disabled' => true,
                                                        'title' => Yii::t('app', 'Save Planting Protocols'),
                                                ]) .
                                            "</div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                        ";
                    }
                ?>
            </ul>
        </div>
    </div>
</div>

<?
// Generate code pattern
$generateCodePatternUrl = Url::to(['/occurrence/manage/generate-code-pattern?level=' . $level]);
?>

<?$this->registerJs(<<<JS
    /* INITIALIZE GLOBAL JS VARIABLES HERE */
    program = '$program'
    entity = '$entity'
    level = '$level'
    encodedIds = '$encodedIds'
    draggables = document.querySelectorAll('.draggable')
    containers = document.querySelectorAll('.draggable-container')
    /* INITIALIZE CSS HERE */
    $(document).ready(function() {
        refresh()
    })


    /* DEFINE FUNCTIONS HERE */
    draggables.forEach(draggable => {
        draggable.addEventListener('dragstart', () => {
            // Reduce opacity to tell user that the element is being dragged
            draggable.classList.add('dragging')
        })

        draggable.addEventListener('dragend', () => {
            // Reset opacity to 1.0 after dragging
            draggable.classList.remove('dragging')
        })
    })

    containers.forEach(container => {
        container.addEventListener('dragover', (e) => {
            e.preventDefault()

            const afterElement = getDragAfterElement(container, e.clientY)
            const draggable = document.querySelector('.dragging')

            if (afterElement == null) {
                container.appendChild(draggable)
            } else {
                container.insertBefore(draggable, afterElement)
            }
        })
    })

    function getDragAfterElement (container, y)
    {
        const draggableElements = [ ...container.querySelectorAll('.draggable:not(.dragging)') ]

        // This returns the element that is closest to the one being dragged
        return draggableElements.reduce((closest, child) => {
            const box = child.getBoundingClientRect()
            const offset = y - box.top - (box.height / 2)

            if (offset < 0 && offset > closest?.offset) {
                return { offset: offset, element: child }
            } else {
                return closest
            }
        }, { offset: Number.NEGATIVE_INFINITY }).element
    }

    function refresh () 
    {
        // Initialize Materialize dropdowns
        $('.dropdown-button').dropdown()

        $('.collapsible-header').on('click', function (e) {
            // Get current expand icon of collapsible header
            const currentExpandIcon = e.currentTarget.children[1].innerHTML

            e.currentTarget.children[1].innerHTML = (currentExpandIcon == 'expand_more') ? 'expand_less' : 'expand_more'
        })
        
        // Validate text field for starting sequence number...
        // ...while user is typing in that text field
        $('input.starting-sequence-field').on('input', function validateStartingSequenceOnInput (e) {
            const key = $(this).data('key')

            if ($(this)[0].value) {
                $(`#generate-code-pattern-\${key}`).removeAttr('disabled')
            } else {
                $(`#generate-code-pattern-\${key}`).attr('disabled', 'disabled')                
            }
        })

        // Block colons (:) and pipes (|) from dynamically created Free Text fields
        $(document).on('keypress', '.free-text', function validateFreeTextInput (e) {
            if (e.shiftKey && (e.keyCode == 58 || e.keyCode == 124)) {
                e.preventDefault()
            }
        })

        // Block pasted colons (:) and pipes (|)  from dynamically created Free Text fields
        $(document).on('paste', '.free-text', function validateFreeTextPastedInput (e) {
            const regex = /.*:+\|*.*|.*:*\|+.*/gi
            const key = e.originalEvent.clipboardData.getData('text')
            
            if (regex.test(key)) {
                e.preventDefault()
            }
        })

        // Set accepted characters for Starting Sequence Number input field
        $('input.starting-sequence-field').on('keypress', function validateStartingSequenceOnKeypress (e) {
            const STARTING_SEQUENCE_NUMBER_ACCEPTED_CHARACTERS = /[0-9, ]+/

            if (!STARTING_SEQUENCE_NUMBER_ACCEPTED_CHARACTERS.test(event.key)) {
                event.preventDefault()
            }
        })

        // Set accepted characters for Leading Zeroes input field
        $('input.leading-zeroes-field').on('keypress', function validateLeadingZeroesOnKeypress (e) {
            const LEADING_ZEROES_ACCEPTED_CHARACTERS = /[0-9]+/

            if (!LEADING_ZEROES_ACCEPTED_CHARACTERS.test(event.key)) {
                event.preventDefault()
            }
        })

        // Add prefix or suffix
        $('a.add-affix-entry, a.add-affix-plot').off().on('click', function addAffix (e) {
            const dataPosition = $(e.currentTarget).data('position')
            const dataKey = $(e.currentTarget).data('key')
            const dataType = $(e.currentTarget).data('type')
            const dataEntity = $(e.currentTarget).data('entity')

            if (dataPosition === 'prefix') {
                $('#code-pattern-affixes-' + dataKey).prepend(
                    (dataType === 'free text') ? renderFreeTextField(dataEntity, dataKey) : renderVariableSelect(dataEntity, dataKey)
                )
            } else {
                $('#code-pattern-affixes-' + dataKey).append(
                    (dataType === 'free text') ? renderFreeTextField(dataEntity, dataKey) : renderVariableSelect(dataEntity, dataKey)
                )
            }

            // This activates the dragging functionality of the new div
            draggables = document.querySelectorAll('.draggable')
            draggables.forEach(draggable => {
                draggable.addEventListener('dragstart', () => {
                    // Reduce opacity to tell user that the element is being dragged
                    draggable.classList.add('dragging')
                })

                draggable.addEventListener('dragend', () => {
                    // Reset opacity to 1.0 after dragging
                    draggable.classList.remove('dragging')
                })
            })
        })

        // Remove affix field (free text or variable)
        $(document).on('click', '.remove-affix-entry, .remove-affix-plot', function removeAffix (e) {
            // Get grandparent of remove-affix and then remove it from DOM
            $(e.currentTarget).parents('div')[1].remove()
        })

        // Generate code pattern
        $('button.generate-code-pattern-entry, button.generate-code-pattern-plot').off().on('click', function generateCodePattern (e) {
            const obj = $(this)
            const dataKey = obj.data('key')
            const occurrenceCount = obj.data('occurrence-count')
            const applyToAllIsChecked = (level == 'experiment') ? $(`#apply-to-all-checkbox-\${dataKey}`)[0].checked : null
            const dataEntity = obj.data('entity')
            let hasError = false
            let startingSequenceNumberArray = []

            // Remove error messages in Staring Sequence Number field
            const helpBlockSequenceNumber = $(`#starting-sequence-field-\${dataKey}`).siblings('.help-block')[0]

            $(`#starting-sequence-field-\${dataKey}`).removeClass('has-error')

            helpBlockSequenceNumber.classList.remove('help-block-error')
            helpBlockSequenceNumber.innerHTML = ''

            if (applyToAllIsChecked) {
                // Check for existence of commas
                startingSequenceNumberArray = $(`#starting-sequence-field-\${dataKey}`)[0].value
                searchString = startingSequenceNumberArray.includes(',') ? ',' : ' '

                if (startingSequenceNumberArray.includes(',')) {
                    const helpBlock = $(`#starting-sequence-field-\${dataKey}`).siblings('.help-block')[0]

                    $(`#starting-sequence-field-\${dataKey}`).addClass('has-error')

                    helpBlock.classList.add('help-block-error')
                    helpBlock.innerHTML = 'Please remove all commas when applying sequence number to all occurrences.'

                    hasError = true
                }

                // Check if there are more than 1 sequence number
                startingSequenceNumberArray = startingSequenceNumberArray
                                                .split(searchString) // This splits the input into array elements
                                                .map( (sequenceNumber) => sequenceNumber.trim() ) // This removes leading and trailing whitespaces
                                                .filter( (sequenceNumber) => sequenceNumber != '') // This removes empty strings

                if (startingSequenceNumberArray.length > 1) {
                    const helpBlock = $(`#starting-sequence-field-\${dataKey}`).siblings('.help-block')[0]

                    $(`#starting-sequence-field-\${dataKey}`).addClass('has-error')

                    helpBlock.classList.add('help-block-error')
                    helpBlock.innerHTML = 'Please enter only 1 sequence number when applying it to all occurrences.'

                    hasError = true
                }

                if (!hasError) {
                    const sequenceNumber = startingSequenceNumberArray[0]

                    // This is to still "match" the number sequences with the occurrenceCount...
                    // ...and remove the need to update implementation in Controller-side
                    while (startingSequenceNumberArray.length != occurrenceCount) {
                        startingSequenceNumberArray.push(sequenceNumber)
                    }
                }
            } else {
                startingSequenceNumberArray = $(`#starting-sequence-field-\${dataKey}`)[0].value
                                                .split(',') // This splits the input into array elements
                                                .map( (sequenceNumber) => sequenceNumber.trim() ) // This removes leading and trailing whitespaces
                                                .filter( (sequenceNumber) => sequenceNumber != '') // This removes empty strings

                // Check if the number of input in Starting Sequence Number...
                // ...matches the number of selected Occurrences for that collapsible window
                if (startingSequenceNumberArray.length != occurrenceCount) {
                    const helpBlock = $(`#starting-sequence-field-\${dataKey}`).siblings('.help-block')[0]

                    $(`#starting-sequence-field-\${dataKey}`).addClass('has-error')

                    helpBlock.classList.add('help-block-error')
                    helpBlock.innerHTML = 'Please match the sequences with the number of selected occurrences.'

                    hasError = true
                }
            }

            // This removes the leading zeroes from the input (from '000123' to '123')
            const leadingZeroes = parseInt($(`#leading-zeroes-field-\${dataKey}`)[0].value)
            let numberOfDigits = 0

            if (leadingZeroes) {
                // This computes the number of digits needed to generate sequence numbers of the code
                numberOfDigits = leadingZeroes + startingSequenceNumberArray[0].length
            }

            // Remove the 'entry'/'plot' substring from the dataKey
            arrayIndex = dataKey.replace(/\D/g,'');

            const ids = JSON.parse(encodedIds)[arrayIndex]
            // Get affix types and values
            const codeAffixes = document.querySelectorAll(`#generate-code-pattern-form-\${dataKey} .code-affix-\${dataEntity}`)
            const affixInfoArray = []

            codeAffixes.forEach(affix => {
                let affixType = affix.attributes['data-type'].value;
                let affixValue = affix.value;

                affixInfoArray.push({ [affixType]: affixValue })
            })

            // Remove error messages in Pattern section
            const helpBlockPatternConfig = $(`#help-block-error-\${dataKey}`)[0]
            const label = $(`#help-block-error-\${dataKey}`).siblings('label')[0]

            helpBlockPatternConfig.classList.remove('help-block-error')
            helpBlockPatternConfig.innerHTML = ''

            label.classList.remove('help-block-error')

            // Check if there are duplicate variables
            if (hasDuplicateVariables(affixInfoArray)) {
                const helpBlock = $(`#help-block-error-\${dataKey}`)[0]
                const label = $(`#help-block-error-\${dataKey}`).siblings('label')[0]

                helpBlock.classList.add('help-block-error')
                helpBlock.innerHTML = 'Please avoid adding duplicate variables into the pattern configuration.'
                label.classList.add('help-block-error')

                hasError = true
            }

            if (hasError) return

            // Notify user
            $('.toast').css('display','none')
            let notif = `
                <i class='material-icons blue-text left'>info</i>
                Please wait for code patterns to be generated.
            `
            Materialize.toast(notif,5000)

            $.ajax({
                url: '$generateCodePatternUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    startingSequenceNumberArray: JSON.stringify(startingSequenceNumberArray),
                    numberOfDigits: numberOfDigits,
                    affixValues: JSON.stringify(affixInfoArray),
                    ids: JSON.stringify(ids),
                    entity: dataEntity
                },
                success: function (response) {
                    if (response == 0) {
                        $('.toast').css('display','none')
                        let notif = `
                            <i class='material-icons red-text left'>close</i>
                            The generated codes were too long. Please shorten the codes to 128 characters.
                        `

                        Materialize.toast(notif,5000)
                    } else if (response == 1) {
                        $('.toast').css('display','none')
                        let notif = `
                            <i class='material-icons green-text left'>done</i>
                            Successfully generated code patterns.
                        `

                        Materialize.toast(notif,5000)
                    }
                },
                error: function () {
                    $('.toast').css('display','none')
                    let notif = `
                        <i class='material-icons red-text left'>close</i>
                        There seems to be a problem with generating code patterns. Please contact an Administrator.
                    `

                    Materialize.toast(notif,5000)
                }
            })
        })
    }

    function renderFreeTextField (dataEntity, dataKey) {
        // Generate unique ID
        const dateTime = new Date().getTime()

        return `
            <div class='draggable row' draggable='true'>
                <div class='col s4 push-s1 card'>
                    <input
                        id='enter-free-text-\${dataKey}-\${dateTime}'
                        data-type='freeText'
                        class='code-affix-\${dataEntity} free-text'
                    >
                    </input>
                </div>
                <div class='col s1 push-s1'>
                    <button
                        type='button'
                        class='remove-affix-btn remove-affix-\${dataEntity} btn red'
                        data-key='\${dataKey}'
                        title='Remove free text' 
                        style='
                            padding-left:10px;
                            padding-right:10px;
                        '
                    >
                        <i class='material-icons right' style='margin-left:0px;'>remove</i>
                    </button>
                </div>
            </div>`
    }

    function renderVariableSelect (dataEntity, dataKey) {
        // Generate unique ID
        const dateTime = new Date().getTime()
        // Render entry or plot variables
        const options = (dataEntity === 'entry') ? `
            <option value='' disabled selected>Choose a variable</option>
            <option value='EXPERIMENT_CODE'>EXPERIMENT CODE</option>
            <option value='EXPERIMENT_NAME'>EXPERIMENT NAME</option>
            <option value='EXPERIMENT_YEAR'>EXPERIMENT YEAR</option>
            <option value='SEASON_CODE'>SEASON CODE</option>
            <option value='OCCURRENCE_CODE'>OCCURRENCE CODE</option>
            <option value='OCCURRENCE_NAME'>OCCURRENCE NAME</option>
            <option value='ENTRY_NUMBER'>ENTRY NUMBER</option>
            <option value='ENTRY_CLASS'>ENTRY CLASS</option>
            <option value='ENTRY_ROLE'>ENTRY ROLE</option>` : `
            <option value='' disabled selected>Choose a variable</option>
            <option value='EXPERIMENT_CODE'>EXPERIMENT CODE</option>
            <option value='EXPERIMENT_NAME'>EXPERIMENT NAME</option>
            <option value='EXPERIMENT_YEAR'>EXPERIMENT YEAR</option>
            <option value='SEASON_CODE'>SEASON CODE</option>
            <option value='OCCURRENCE_CODE'>OCCURRENCE CODE</option>
            <option value='OCCURRENCE_NAME'>OCCURRENCE NAME</option>
            <option value='ENTRY_NUMBER'>ENTRY NUMBER</option>
            <option value='ENTRY_CODE'>ENTRY CODE</option>
            <option value='ENTRY_CLASS'>ENTRY CLASS</option>
            <option value='ENTRY_REP'>ENTRY REP</option>
            <option value='PLOT_NUMBER'>PLOT NUMBER</option>
            <option value='SITE</'>SITE</option>
            <option value='SITE_CODE'>SITE CODE</option>`

        return `
            <div class='draggable row' draggable='true'>
                <div class='col s4 push-s1 card'>
                    <select
                        id='choose-variable-\${dataKey}-\${dateTime}'
                        data-type='variable'
                        class='code-affix-\${dataEntity} variable-select browser-default default-dropdown'
                    >
                        \${options}
                    </select>
                </div>
                <div class='col s1 push-s1'>
                    <button
                        type='button'
                        class='remove-affix-btn remove-affix-\${dataEntity} btn red'
                        data-key='\${dataKey}'
                        title='Remove variable' 
                        style='
                            padding-left:10px;
                            padding-right:10px;
                        '
                    >
                        <i class='material-icons right' style='margin-left:0px;'>remove</i>
                    </button>
                </div>
            </div>`
    }

    // This checks if there are duplicate variables in the pattern configuration
    function hasDuplicateVariables (affixInfoArray) {
        const valueArray = affixInfoArray
                            .map( (affix) => affix.variable ) // Get each variable values
                            .filter( ( value ) => value != null || value != undefined ) // Remove undefined or null values from map()
        const hasDuplicate = valueArray.some( (item, index) => valueArray.indexOf(item) != index )

        return hasDuplicate
    }
JS);?>

<?
$this->registerCss('
    .default-dropdown {
        font-family: "Roboto", sans-serif;
        font-weight: bold;
        margin-left: -10px;
        border: none;
    }

    .remove-affix-btn {
        padding-left: 8px;
        padding-right: 8px;
        vertical-align: -32px;
    }
    
    .draggable:active {
        cursor: move;
    }

    .draggable.dragging {
        opacity: 0.4;
    }

    div.draggable.row {
        margin-bottom: 0px;
    }

    /* Margin for variable and free text cards */
    .col.s4.push-s1.card {
        margin-bottom: 8px;
    }

    /* Starting sequence label color */
    input.starting-sequence-field.has-error + label {
      color: red;
    }

    /* Starting sequence input underline color */
    input.starting-sequence-field.has-error {
      border-bottom: 1px solid red;
      box-shadow: 0 1px 0 0 red;
    }

    span.help-block-error, label.help-block-error {
        color: red;
    }
');
?>