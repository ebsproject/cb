<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use app\components\FavoritesWidget;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;

// Dummy call Modal to make Nav Bar buttons work
Modal::begin([]);
Modal::end();

/**
 * Set columns for Experiment Permissions browser
 */
$columns = [
    [
        // Checkbox (select all)
        'header' => '
            <input type="checkbox" class="filled-in" id="occurrence-collaborator-permission-select-all" />
            <label for="occurrence-collaborator-permission-select-all" title="Select all in this page"></label>
        ',
        // Checkbox (for each row)
        'content' => function ($data) {
            $occurrenceDbId = $data['occurrenceDbId'];

            $checkbox = "<input
                class='occurrence-collaborator-permission-grid-select filled-in'
                id='$occurrenceDbId'
                type='checkbox'
            />";

            return $checkbox . '
                <label for="' . $data['occurrenceDbId'] . '"></label>       
            ';
        },
        'hAlign' => 'center',
        'vAlign' => 'top',
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'occurrenceStatus',
        'label' => 'Status',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
                'ajax' => [
                    'url' => '/index.php/occurrence/default/get-filters-data',
                    'dataType' => 'json',
                    'delay' => 150,
                    'type'=>'POST',
                    'data' => new JsExpression(
                        'function (params) {
                            var value = (params.term === "")? undefined: params.term

                            return {
                                "filter": {
                                    "occurrenceStatus": value
                                },
                                "column": "occurrenceStatus"
                            }
                        }'
                    ),
                    'processResults' => new JSExpression(
                        'function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.items
                            };
                        }'
                    ),
                    'cache' => true
                ]
            ]
        ],
        'filterInputOptions' => [
            'placeholder' => '',
            'style' => 'min-width:100px'
        ],
        'value' => function ($model) {
            $status = strtoupper(trim($model['occurrenceStatus']));
            $statuses = explode(';', $status);

            $statusVal = '';
            foreach ($statuses as $key => $value) {

                $class = 'grey';
                if (strpos(strtolower($value), 'pack') !== false) {
                    $plantingJobModel = \Yii::$container->get('app\models\PlantingJob');
                    $class = $plantingJobModel->getStatusClass(strtolower($value));
                } else if ($value == 'COMMITTED' || $value == 'PLANTED' || $value == 'CROSSED') {
                    $class = 'green darken-3';
                } else if ($value == 'MAPPED' || strpos($value, 'IN PROGRESS')) {
                    $class = 'amber darken-3';
                } else if(strpos($value, 'FAILED')) {
                    $class = 'red';
                }

                $statusVal .= '<span
                    id="occurrence-status-' . $model['occurrenceDbId'] .'"
                    title="Status: ' . $value . '"
                    class="bold new badge ' . $class . '"
                    data-occurrence-detailed-status="' . strtolower($status) . '"
                >' . $value . '</span>';
            }

            return $statusVal;
        },
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'experimentName',
        'group' => true,
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentName'] . '</span>';
        },
        'groupHeader' => function ($model) { // Closure method
            return [
                'content' => [ // content to show in each summary cell
                    2 => $model['experimentName'],
                    3 => $model['experimentSteward'],
                ],
                'options' => ['class' => 'bold light-green lighten-4 table-info' . $model['experimentDbId']]
            ];
        },
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'experimentSteward',
        'group' => true,
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentName'] . '</span>';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'occurrenceName',
        'label' => 'Occurrence',
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'countryCode',
        'label' => 'Country',
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'siteCode',
        'label' => 'Site',
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'locationSteward',
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'collaboratorPermissions',
        'label' => 'Collaborator Permissions',
        'noWrap' => true,
        'width' => '180px',
        'format' => 'html',
        'enableSorting' => false,
        'value' => function ($model) use ($program) {
            if (!$model['collaboratorPermissions']) {
                return null;
            } else {
                $nullValue = ['null'=>'read'];
                $collaboratorPermissions = $model['collaboratorPermissions'];

                // remove null values
                foreach ($collaboratorPermissions as $key => $value) {
                    if($key == 'null'){
                        unset($collaboratorPermissions[$key]);
                    }
                }

                $count = count($collaboratorPermissions);
                $finalValue = ' <ul class="collapsible" data-collapsible="accordion" style="min-width: 158px; margin: 2px;">';

                
                $s = ($count > 1) ? 's' : '';
                $finalValue .= '<li>
                <div class="collapsible-header" style="padding: .5rem;"><span class="secondary-content">'
                    . $count. ' collaborator permission'.$s.'</span>' .
                    '</div>
                <div class="collapsible-body" style="padding:0px">';

                $dataModel = [];
                if (!empty((array) $collaboratorPermissions)) {

                    if($collaboratorPermissions == $nullValue){
                        return null;
                    } else {
                        foreach ($collaboratorPermissions as $collaborator => $permission) {
                            // exclude null values
                            if ($collaborator != 'null'){

                                $permission = strtoupper($permission);

                                $dataModel[] = [ 'collaboratorPermissions' => "$collaborator | $permission" ];
                            }
                        }
                    }
                } else {
                    return null;
                }

                $provider = new ArrayDataProvider([
                    'allModels' => $dataModel,
                    'pagination' => false
                ]);

                $finalValue .= GridView::widget([
                    'dataProvider' => $provider,
                    'layout' => '{items}',
                    'showHeader' => false,
                    'columns' => [
                        [
                            'attribute' => 'collaboratorPermissions',
                            'format' => 'raw',
                            'label' => false,
                        ]
                    ],
                    'tableOptions' => ["style" => "margin-bottom:0px"]
                ]);
                $finalValue .= '</div></li>';

                $finalValue = $finalValue .= '</ul>';

                return $finalValue;
            }
        }
    ],
];

// Manage permissions dropdown
$managePermissionsDropdown =  Html::a(
    'MANAGE <span class="caret"></span>',
    '',
    [
        'title' => Yii::t('app', 'Manage permissions'),
        'class' => 'dropdown-button btn dropdown-button-pjax',
        'id' => 'manage-permissions-btn',
        'data-activates' => 'manage-permissions-dropdown',
        'data-beloworigin' => 'true',
        'data-constrainwidth' => 'false',
    ]
) . "
    <ul id='manage-permissions-dropdown' class='dropdown-content'>
        <li
            title='" . Yii::t('app', 'Manage permissions selected within this page') . "'
        >
            " .
            Html::a(
                'Selected in page',
                null,
                [
                    'class' => 'manage-permissions-btn dropdown-button dropdown-button-pjax non-link',
                    'data-constrainwidth' => 'false',
                    'data-hover' => 'hover',
                    'data-selection-mode' => 'selected',
                ]
            ) .
            "
        </li>
        <li
            title='" . Yii::t('app', 'Manage all permissions within this page') . "'
        >
            " .
            Html::a(
                'All in page',
                null,
                [
                    'class' => 'manage-permissions-btn dropdown-button dropdown-button-pjax non-link',
                    'data-constrainwidth' => 'false',
                    'data-hover' => 'hover',
                    'data-selection-mode' => 'all',
                ]
            ) .
            "
        </li>
    </ul>
";

$browserId = 'occurrence-collaborator-permission-grid';

$params = Yii::$app->request->queryParams;

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'storage' => 'cookie',
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'showPageSummary' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => ('<h3>' .
                Yii::t('app', 'Experiment Permissions') .
                FavoritesWidget::widget([
                    'module' => 'occurrence'
                ]) .
                '</h3>'),
            'before' => \Yii::t('app', "This is the browser for experiment permissions. You may manage your permissions here.") .
                '{summary}' .
                '<br/>
                <p id="selected-items-paragraph" class = "pull-right" style = "margin-right: 6px;">
                    <b><span id="selected-items-count"> </span></b> <span id="selected-items-text"> </span>
                </p>',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' =>
                    Html::beginTag('div', [
                        'style' => [
                            'display' => 'flex',
                            'justify-content' => 'space-between',
                            'min-width' => '232px',
                        ],
                    ]) .
                        '<span>' . Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            '',
                            [
                                'data-pjax' => true,
                                'class' => 'btn btn-default',
                                'title' => \Yii::t('app', 'Reset grid'),
                                'id' => 'reset-occurrence-collaborator-permission-grid-id'
                            ]
                        ) . '</span>' .
                        '<span>' . '{dynagridFilter}{dynagridSort}{dynagrid}' . '</span>' .
                        '<span>' . $managePermissionsDropdown . '</span>' .
                    Html::endTag('div')
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// delete person modal
Modal::begin([
    'id' => 'remove-permissions-modal',
    'header' => '<h4 id="remove-permission-label"><i class="material-icons">delete</i> Remove Permissions</h4>',
    'closeButton' => [
        'class' => 'close cancel-remove-btn',
    ],
    'bodyOptions' => [
        'class' => 'modal-body',
        'id' => 'remove-permissions-modal-body',
    ],
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '',
            [
                'data-dismiss'=>'modal',
                'class' => 'cancel-remove-btn',
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm'),
            '',
            [
                'class'=>'btn btn-primary waves-effect waves-light remove-permission-btn',
                'id'=>'manage-permissions-revoke-access-btn',
            ]
        ) .
        '&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
?>
<?= '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to remove <b><span id="remove-permissions-count"></span></b> permissions. Note that the selected entities will no longer be able to access the occurrences. Click confirm to proceed.' ?>
<?
Modal::end();

$closeButton = '<div class="row">' .
    \macgyer\yii2materializecss\widgets\Button::widget([
        'label' => 'Close',
        'options' => [
            'class' => 'btn pull-right close-permissions-modal',
            'data-dismiss' => 'modal'
        ],
    ])
. '</div>';
?>

<!-- Manage Permissions modal -->
<div id="manage-permissions-modal" class="fade modal in" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-permissions-modal" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><em class="material-icons" style="vertical-align: -4px">share</em> Manage Permissions</h4>
            </div>
            <div class="modal-body" id="manage-permissions-body">
                <!-- Insert HTML here via jQuery -->
            </div>
            <div class="modal-footer">
                <? echo $closeButton; ?>
            </div>
        </div>
    </div>
</div>

<?
// Reset grid url
$resetOccurrenceCollaboratorPermissionsGridUrl = Url::toRoute(["/occurrence/manage/experiment-permissions?program=$program&occurrenceIds=$occurrenceIds"]);
// Render Manage Permissions Modal Content
$managePermissionsUrl = Url::to(["manage/permissions?program=$program"]);
// Set session variables
$setSessionVariablesUrl = Url::to(["manage/set-session-variables"]);

$this->registerCss('
    .modal-body {
        overflow-x: hidden;
    }
');

$this->registerJs(<<<JS
    // URLs
    const resetOccurrenceCollaboratorPermissionsGridUrl = '$resetOccurrenceCollaboratorPermissionsGridUrl'
    const managePermissionsUrl = '$managePermissionsUrl'
    const setSessionVariablesUrl = '$setSessionVariablesUrl'
    const gridId = '$browserId'

    $(document).ready(function () {
        // Unselect all checkboxes
        $('input:checkbox.occurrence-collaborator-permission-grid-select').each(function getTickedCheckboxes () {
            if ($(this).prop('checked')) {
                $(this).prop('checked', false)
            }
        })

        refresh()
    })

    // Reload grid on pjax success
    $(document).on('ready pjax:success', function onGridReady (e) {
        // remove loading indicator upon export
        $('*[class^="export-full-"]').on('click',function removeLoadingIndicator () {
            $('#system-loading-indicator').css('display','none')
        })

        e.stopPropagation()
        e.preventDefault()
        refresh()
    });

    function refresh () {
        let maxHeight = ($(window).height() - 250)
        $(".kv-grid-wrapper").css("height",maxHeight)

        // Load collapsible functionality
        $('.collapsible').collapsible();

        let loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
        $('.dropdown-button').dropdown();
        $('.button-collapse').sideNav();

        // display and hide loading indicator
        $(document).on('click', '.hide-loading', function () {
            $('#system-loading-indicator').css('display','block')

            setTimeout(function () {
                $('#system-loading-indicator').css('display', 'none')
            },3000)
        });

        // Resets data browser
        $('#reset-occurrence-collaborator-permission-grid-id').on('click', function resetGrid (e) {
            e.preventDefault()
            $.pjax.reload({
                container: '#' + gridId + '-pjax',
                replace: true,
                url: resetOccurrenceCollaboratorPermissionsGridUrl
            })
        })

        $('.occurrence-collaborator-permission-grid-select').on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
        });

        // Selects all occurrences
        $('#occurrence-collaborator-permission-select-all').on('click', function(e) {
            if ($(this).prop('checked') === true) {
                $('.occurrence-collaborator-permission-grid-select').prop('checked',true)
                $('.occurrence-collaborator-permission-grid-select:checkbox')
                    .parent('td')
                    .parent('tr')
                    .addClass('grey lighten-4')
            } else {
                $('.occurrence-collaborator-permission-grid-select').prop('checked',false)
                $('.occurrence-collaborator-permission-grid-select:checkbox')
                    .parent('td')
                    .parent('tr')
                    .removeClass('grey lighten-4')
            }
        })

        // Triggers click on checkbox of the row in occurrence browser
        $('#occurrence-collaborator-permission-grid tbody tr').on('click', function (e) {
            e.preventDefault()
            e.stopImmediatePropagation()

            let thisRow = $(this).find('input:checkbox')[0]

            if (thisRow != undefined) {
                $('#' + thisRow.id).trigger('click')

                if (thisRow.checked) {
                    $(this).addClass('grey lighten-4')
                } else {
                    $(this).removeClass('grey lighten-4')
                }

                if (allCheckboxesSelected())
                    $(`#occurrence-collaborator-permission-select-all`).prop('checked', true)
                else
                    $(`#occurrence-collaborator-permission-select-all`).prop('checked', false)
            }
        })

        $('.manage-permissions-btn').on('click', async function managePermissions (e) {
            let obj = $(this)
            selectionMode = obj.data('selection-mode')
            let ids = []

            if (selectionMode == 'selected') {
                ids = getSelectedOccurrences().join('|')

                if (!ids) {
                    message = `<i class="material-icons orange-text" style="margin-right: 8px;">warning</i> Select at least one (1) Occurrence.`

                    $('.toast').css('display','none')
                    Materialize.toast(message, 5000)

                    return
                }
            } else if (selectionMode == 'all') {
                $('input:checkbox.occurrence-collaborator-permission-grid-select').each(function getAllCheckboxesInPage () {
                        ids.push($(this).attr('id'))
                })

                ids = ids.join('|')
            }

            $('#manage-permissions-body').html('<div class="progress"><div class="indeterminate"></div></div>')
            $('#manage-permissions-modal').modal('show')

            $.ajax({
                type: 'POST',
                url: managePermissionsUrl,
                data: {
                    occurrenceIds: ids,
                },
                success: function (data) {
                    $("#manage-permissions-body").html(data)
                }
            })
        })

        // Reload Occurrence Collaborator Permissions data browser on Modal close
        $('.close-permissions-modal').on('click', function closeManagePermissionsModal (e) {
            $.pjax.reload({
                container: '#' + gridId + '-pjax',
                replace: true,
                url: resetOccurrenceCollaboratorPermissionsGridUrl
            })            
        })

        /**
         * Retrieve selected Occurrences based on
         * clicked/ticked checkboxes on the current page
         * 
         * @return array An array containing IDs of selected Occurrences
         */
        function getSelectedOccurrences () {
            let ids = []

            $('input:checkbox.occurrence-collaborator-permission-grid-select').each(function getTickedCheckboxes () {
                // If checkbox is clicked/ticked
                if ($(this).prop('checked')) {
                    ids.push($(this).attr('id'))
                }
            })

            return ids
        }

        /**
         * Check if all checkboxes are selected
         * 
         * @return boolean
         */
        function allCheckboxesSelected () {
            let allCheckboxesSelected = true

            $('input:checkbox.occurrence-collaborator-permission-grid-select').each(function getTickedCheckboxes () {
                // If at least 1 checkbox is unchecked, set flag to false
                if (!$(this).prop('checked'))
                    allCheckboxesSelected = false
            })

            return allCheckboxesSelected      
        }
    }
JS);