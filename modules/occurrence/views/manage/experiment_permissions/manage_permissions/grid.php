<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for listing all the users that Occurrence has been shared to
**/

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

$dynaGrid = DynaGrid::begin([
    'columns' => [
        [
            'label'=> "checkbox",
            'visible'=> true,
            'header'=>'
                <input class="filled-in" type="checkbox" data-id-string="" id="manage-permissions-cb-all" />
                <label for="manage-permissions-cb-all"></label>
            ',
            'contentOptions' => ['style' => ['max-width' => '100px;',]],
            'content'=> function($data) {
                return " <input 
                    id='{$data['occurrenceDbId']}-{$data['entityDbId']}'
                    class='manage-permissions-cb filled-in'
                    type='checkbox'
                    data-id='{$data['entityDbId']}'
                    data-entity='{$data['entity']}'
                    data-occurrence-id='{$data['occurrenceDbId']}'/>
                <label for='{$data['occurrenceDbId']}-{$data['entityDbId']}'/>
                ";
            },
            'hAlign'=>'center',
            'vAlign'=>'middle',
            'hiddenFromExport'=>true,
            'mergeHeader'=>true,
        ],
        [
            'attribute' => 'experimentName',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'occurrenceName',
            'label' => 'Occurrence',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'countryCode',
            'label' => 'Country',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'siteCode',
            'label' => 'Site',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'entity',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'collaborator',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'permission',
            'content' => function ($data) {
                return '<span class="badge '. (($data['permission'] == 'read') ? ' new badge amber darken-2">READ' : ' new">WRITE') .'</span>';
            },
            'format' => 'raw',
            'enableSorting' => false,
        ],
    ],
    'theme' => 'simple-default',
    'showPersonalize' => false,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => 'manage-permission-grid-id',
        'dataProvider' => $dataProvider,
        'filterModel'=>null,
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'showPageSummary'=>false,
        'responsive' => true,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>['id'=>'manage-permissions-grid-id'],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            
            'before' => \Yii::t('app', '<span>Add new permissions using the <b>Share to Collaborators</b> menu.<br>To <b>Update Permissions</b>, select the checkboxes and click <b>APPLY</b> to apply the desired permission.</span>'),
            'after' => false,
            'footer'=>false
        ],
        'toolbar' =>  [
            [
                'content'=>Html::a('Remove', '', [ 'class' => 'btn btn-default gray add-list-btn waves-light waves-effect', 'id' => 'remove-permission-btn', 'title'=>\Yii::t('app','Remove item')]).'{dynagridFilter}{dynagridSort}{dynagrid}',
            ],
        ]
    ],
    'options' => [ 'id' => 'manage-permissions-grid' ]
]);
DynaGrid::end();

$this->registerJs(<<<JS
    $('#remove-permission-btn').on('click', function onClickRemovePermissionsButton (e) {
        e.preventDefault()
        
        totalCount = 0
        programEntityIds = {}
        personEntityIds = {}

        $.each($('.manage-permissions-cb:checkbox:checked'), function(i, v) {
            const entity = $(v).attr('data-entity')
            const entityDbId = $(v).attr('data-id')
            const occurrenceId = $(v).attr('data-occurrence-id')

            if (entity == 'person') {
                if (!personEntityIds[occurrenceId])
                    personEntityIds[occurrenceId] = []
                
                personEntityIds[occurrenceId].push(entityDbId)
            } else {
                if (!programEntityIds[occurrenceId])
                    programEntityIds[occurrenceId] = []
                
                programEntityIds[occurrenceId].push(entityDbId)
            }

            totalCount += 1
        })

        if (
            Object.entries(programEntityIds).length == 0 && 
            Object.entries(personEntityIds).length == 0
        ) {
            $('.toast').css('display','none')
            let notif = "<i class='material-icons orange-text'>warning</i> Select at least 1 user or program"
            Materialize.toast(notif, 5000)
        } else {
            $('#remove-permissions-count').html(totalCount)

            // Transition from Manage Permissions Modal to Remove Permissions Modal
            $('#manage-permissions-modal').modal('hide')
            $('#remove-permissions-modal-body').html(`<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to remove <b><span id="remove-permissions-count">\${totalCount}</span></b> permissions. Note that the selected entities will no longer be able to access the occurrences. Click confirm to proceed.`)
            $('#remove-permissions-modal').modal('show')
        }
    })

    $('.manage-permissions-cb').on('click',function(e){
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $(this).parent("td").parent("tr").addClass("warning");
            var isAllChecked = 0;
            $(".manage-permissions-cb").each(function() {
                if (!this.checked)
                    isAllChecked = 1;
            });
            if (isAllChecked == 0) {
                $("#manage-permissions-cb-all").prop("checked", true);
            }
        }else{
            $("#manage-permissions-cb-all").prop("checked", false);
            $(this).removeAttr('checked');
            $(this).parent("td").parent("tr").removeClass("warning");
        }
    });

    $("#manage-permissions-cb-all").change(function() {
        if (this.checked) {
            $(".manage-permissions-cb").each(function() {
                this.checked=true;
            });
            $(".manage-permissions-cb:checkbox").parent("td").parent("tr").addClass("warning");
        } else {
            $(".manage-permissions-cb").each(function() {
                this.checked=false;
            });
            $("input:checkbox.manage-permissions-cb").parent("td").parent("tr").removeClass("warning");  
        }
    });
JS
);