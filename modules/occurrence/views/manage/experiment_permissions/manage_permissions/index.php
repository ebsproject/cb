<?php
/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View file for sharing access to list feature
**/

use kartik\select2\Select2;
use yii\helpers\Url;

$addAccessUrl = Url::to(['manage/add-access']);
$resetManagePermissionsGridUrl = Url::to(['manage/reset-manage-permissions-grid']);
$removeAccessUrl = Url::to(['manage/revoke-access']);
?>

<div class="col col-md-4">
    <h5><strong> Share to Collaborators </strong></h5>
    <?php
    echo Select2::widget([
        'name' => 'select_team',
        'data' => $programData,
        'options' => ['placeholder' => 'Select Program', 'id' => 'select2-team'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
            'maximumSelectionLength' => 3,
            'allowSelectAll' => false,
        ],
        'toggleAllSettings' => [
            'selectLabel' => '',
            'unselectLabel' => '',
            'selectOptions' => ['class' => 'text-success'],
            'unselectOptions' => ['class' => 'text-danger'],
        ],
    ]);
    ?>

    <?php
    echo '<br/>';
    echo Select2::widget([
        'name' => 'select_person',
        'data' => $userData,
        'options' => ['placeholder' => 'Select Person', 'id' => 'select2-person'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
            'maximumSelectionLength' => 3,
            'allowSelectAll' => false,
        ],
        'toggleAllSettings' => [
            'selectLabel' => '',
            'unselectLabel' => '',
            'selectOptions' => ['class' => 'text-success'],
            'unselectOptions' => ['class' => 'text-danger'],
        ],
    ]);
    echo '<br/>';
    ?>

    <!-- render Toggle for Share here -->
    <div class="row switch" >
        <label class='pull-left'>
            READ
            <input 
                type = "checkbox"
                id = "share-access-level-switch"
                checked
            >
            <span class="lever"></span>
            WRITE
        </label>
        <?
        echo \macgyer\yii2materializecss\widgets\Button::widget([
            'label' => Yii::t('app', 'Add'),
            'options' => [
                'class' => 'btn-success pull-right',
                'id' => 'add-access-btn',
            ]

        ]);
        ?>
    </div>

    <hr class='solid'>

    <h5><strong> Update Permissions </strong></h5>
    <!-- render Toggle for Update here -->
    <div class="row switch" >
        <label class='pull-left'>
            READ
            <input 
                type = "checkbox"
                id = "update-access-level-switch"
                checked
                disabled
            >
            <span class="lever" disabled></span>
            WRITE
        </label>
        <?php
            echo \macgyer\yii2materializecss\widgets\Button::widget([
                'label' => Yii::t('app', 'Apply to selected'),
                'options' => [
                    'class' => 'btn-success pull-right disabled',
                    'id' => 'update-permission-btn',
                    'disabled' => 'disabled',
                ]
            ]);
        ?> 
    </div>
</div>

<div class="col col-md-8" id="manage-permissions-grid-div">
<?php
    $count = $dataProvider->getTotalCount();

    echo Yii::$app->controller->renderPartial('experiment_permissions/manage_permissions/grid', [
        'dataProvider' => $dataProvider,
    ]);
?>
</div>

<?php
$this->registerJs(<<<JS
    totalCount = '$count'
    addAccessUrl = '$addAccessUrl'
    resetManagePermissionsGridUrl = '$resetManagePermissionsGridUrl'
    removeAccessUrl = '$removeAccessUrl'
    programEntityIds = []
    personEntityIds = []
    occurrenceIds = '$occurrenceIds'

    $('.collapsible').collapsible()
    updatePermissionButton()

    $(document).ready(function () {
        $('#add-access-btn').on('click', function addAccess () {
            let selectedPersons = $('#select2-person').val()
            let selectedTeams = $('#select2-team').val()
            let access = $('#share-access-level-switch').prop('checked') ? 'write' : 'read'

            if (selectedPersons.length == 0 && selectedTeams.length == 0) {
                $('.toast').css('display','none')
                let notif = "<i class='material-icons orange-text'>warning</i> Select at least one (1) person or program."
                Materialize.toast(notif, 5000)
            } else {
                $('#manage-permissions-grid-div').html(' <div class="progress"><div class="indeterminate"></div></div>')

                setTimeout(() => {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: addAccessUrl,
                        data: {
                            occurrenceIds: occurrenceIds,
                            selectedPersons: selectedPersons,
                            selectedTeams: selectedTeams,
                            access: access,
                        },
                        success: function (response)
                        {
                            // reset main data browser
                            $('#reset-occurrence-collaborator-permission-grid-id').trigger('click')

                            $.ajax({
                                type: 'POST',
                                url: resetManagePermissionsGridUrl,
                                data: {
                                    occurrenceIds: occurrenceIds
                                },
                                success: function (data) {
                                    $('#manage-permissions-grid-div').html(data)
                                    $('#select2-person').val(null).trigger('change')
                                    $('#select2-team').val(null).trigger('change')

                                    // retain modal data browser's height
                                    $('#manage-permission-grid-id-container').addClass('restricted-height')

                                    const message = response.message
                                    const color = response.color
                                    const icon = response.icon
                                    const notif = `<i class='material-icons \${color}-text'>\${icon}</i> <span>\${message}</span>`

                                    $('.toast').css('display','none')
                                    Materialize.toast(notif, 5000)

                                    totalCount = response.trimmedPersonsCount + response.trimmedTeamsCount
                                    
                                    // set button state
                                    updatePermissionButton()
                                }
                            })
                        }
                    })                
                }, 1000)
            }
        })

        $('.cancel-remove-btn').on('click', function cancelRevoke (e) {
            e.preventDefault()

            // Transition from Remove Permissions Modal to Manage Permissions Modal
            $('#remove-permissions-modal').modal('hide')
            $('#manage-permissions-modal').modal('show')
        })

        $('#manage-permissions-revoke-access-btn').on('click', function removePermissions (e) {
            e.preventDefault()
            e.stopPropagation()

            $('.btn').removeClass('disabled')
            $('#remove-permissions-modal-body').html(' <div class="progress"><div class="indeterminate"></div></div>')

            setTimeout(function () {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: removeAccessUrl,
                    data: {
                        occurrenceIds: occurrenceIds,
                        programEntityIds: JSON.stringify(programEntityIds),
                        personEntityIds: JSON.stringify(personEntityIds),
                        currentUserId: '$userId',
                    },
                    success: function (response) {
                        let remainingCount = response.remaining
                        let failedResponses = response.failedResponses

                        // reset main data browser
                        $('#reset-occurrence-collaborator-permission-grid-id').trigger('click')

                        if (remainingCount > 0) {
                            $('#update-permission-btn').removeClass('disabled')
                        } else {
                            $('#update-permission-btn').addClass('disabled')
                        }

                        $.ajax({
                            type: 'POST',
                            url: resetManagePermissionsGridUrl,
                            data: {
                                occurrenceIds: occurrenceIds
                            },
                            success: function (data) {
                                // Transition from Remove Permissions Modal to Manage Permissions Modal
                                $('#remove-permissions-modal').modal('hide')
                                $('#manage-permissions-grid-div').html(data)
                                $('#manage-permissions-modal').modal('show')
                                // retain modal data browser's height
                                $('#manage-permission-grid-id-container').addClass('restricted-height')

                                $('#select2-person').val(null).trigger('change')
                                $('#select2-team').val(null).trigger('change')
                                
                                let notif = "<i class='material-icons green-text'>check</i> Successfully revoked access for selected persons/programs"

                                if (failedResponses) notif += `. However, failed to revoke access from \${failedResponses} Occurrences.`

                                $('.toast').css('display','none')
                                Materialize.toast(notif, 5000)
                            }
                        })
                    }
                })
            }, 1000)
        })
    })

    function updatePermissionButton ()
    {
        if (totalCount > 0) {
            $('#update-permission-btn').removeClass('disabled')
        } else {
            $('#update-permission-btn').addClass('disabled')
        }
    }
JS)

?>
<style>  
    .revoke-icon {
        font-size: 35px;
    }
    .revoke-link {
        margin-top: -10px;
    }
    .restricted-height {
        height: 300px !important;
    }
</style>