<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
use yii\helpers\Html;
use yii\helpers\Url;

/*
 * Renders the header tab for the manage experiment and occurrence
 */

// params
$params = [
    'program' => $program,
    'experimentId' => $experimentId,
    'occurrenceId' => $occurrenceId,
    'isAdmin' => $isAdmin,
    'role' => $role
];
// urls
$browserUrl = Url::to(['/occurrence','program' => $program]);
$redirectToExperimentUrl = Url::to(['/occurrence','program' => $program, 'OccurrenceSearch[experiment]' => $experiment]);
$experimentUrl = Url::to(array_merge(['manage/experiment'], $params));
$occurrencesUrl = Url::to(array_merge(['manage/occurrences'], $params));
$managementUrl = Url::to(array_merge(['manage/management'], $params));
$codePatternsExperimentLevelStreamlinedUrl = Url::to(array_merge(['manage/code-generation'], array_merge($params, ['level' => 'experiment'])));
$occurrenceProtocolsUrl = Url::to(array_merge(['manage/protocols'], $params));

echo 
'<h3>' . Html::a(Yii::t('app', 'Experiment Manager'), $browserUrl) .
    ' <small>» Update ' . Html::a($experiment, $redirectToExperimentUrl) . '</small>' .
'</h3>';


$renderProtocolsTab = (!$isAdmin) ? Yii::$app->access->renderAccess(
    "EXPERIMENT_MANAGER_UPDATE_OCC_PROTOCOL",
    'OCCURRENCES',
    $creatorId,
    $accessData,
    'write'
) : true;

$protocolsTabLinkClass = !$renderProtocolsTab ? 'hidden' : (($active == 'protocols') ? 'active' : '');

$protocolsTab = [
    'label' => 'Protocol',
    'url' => $occurrenceProtocolsUrl,
    'linkClass' => $protocolsTabLinkClass,
];

$tabs = (!$isAdmin && strtoupper($role) == 'COLLABORATOR') ?  [
    [
        'label' => 'Occurrence',
        'url' => $occurrencesUrl,
        'linkClass' => ($active == 'occurrences') ? 'active' : ''
    ],
    [
        'label' => 'Management Data',
        'url' => $managementUrl,
        'linkClass' => ($active == 'management') ? 'active' : ''
    ],
    $protocolsTab,
] : [
    [
        'label' => 'Experiment',
        'url' => $experimentUrl,
        'linkClass' => ($active == 'experiment') ? 'active' : ''
    ],
    [
        'label' => 'Occurrence',
        'url' => $occurrencesUrl,
        'linkClass' => ($active == 'occurrences') ? 'active' : ''
    ],
    [
        'label' => 'Management Data',
        'url' => $managementUrl,
        'linkClass' => ($active == 'management') ? 'active' : ''
    ],
    [
        'label' => 'Code Generation',
        'url' => $codePatternsExperimentLevelStreamlinedUrl,
        'linkClass' => ($active == 'codePatterns') ? 'active' : ''
    ],
    $protocolsTab,
];

?>

<div class="col col-md-12" style="padding-right: 0px">
    <ul class="tabs">
        <?php
            // render tabs
            foreach ($tabs as $value) {
                $label = $value['label'];
                $url = $value['url'];
                $linkClass = $value['linkClass'];

                $item = '<li class="tab col">'.
                    '<a href="'.$url.'" class="'.$linkClass.' a-tab">' . $label . '</a>
                </li>';

                echo $item;
            }
        ?>
    </ul>
</div>
<div class="row"></div>