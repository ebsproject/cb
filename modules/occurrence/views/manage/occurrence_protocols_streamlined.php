<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders entry point-streamlined manage Occurrence Protocols
 */
// Partially render  manage tabs
echo Yii::$app->controller->renderPartial('tabs', [
    'experimentId' => $params['experimentDbId'],
    'program' => $params['program'],
    'active' => 'protocols',
    'experiment' => $experiment,
    'occurrenceId' => $params['occurrenceDbId'],
    'isAdmin' => $isAdmin,
    'role' => $role,
    'creatorId' => $creatorId,
    'accessData' => $accessData,
]);

// Partially render  Occurrence Protocols
echo Yii::$app->controller->renderPartial('occurrence_protocols', $params);