<?php
/*
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders manage occurrence-level information of Experiment
 */
use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;
use kartik\dynagrid\DynaGrid;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

// render manage tabs
echo Yii::$app->controller->renderPartial('tabs', [
    'experimentId' => $experimentId,
    'program' => $program,
    'active' => 'occurrences',
    'experiment' => $experiment,
    'occurrenceId' => $occurrenceId,
    'isAdmin' => $isAdmin,
    'role' => $role,
    'creatorId' => $creatorId,
    'accessData' => $accessData,
]);

echo '<p>' .
    \Yii::t('app','You may specify and add more variables for occurrence information here.') . ' ' .
    '<strong>' . \Yii::t('app','Changes are autosaved.') . '</strong>' .
'</p>';

$browserId = 'manage-add-occurrences-browser';
// Set default page size url
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
// Save data browser settings
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);
// back to occurrences tab url
$backToUrl = Url::toRoute(
    [
        '/occurrence/manage/occurrences?' .
        'program=' . $program .
        '&experimentId=' . $experimentId .
        '&occurrenceId='.$occurrenceId
    ]
);
// display row number
$rowNumber = [
    [
        'class' => 'yii\grid\SerialColumn',
        'header' => false,
        'order'=> DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => false,
        'noWrap' => true,
        'vAlign' => 'middle',
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'template' => '{delete}',
        'buttons' => [
            'delete' => function ($url, $data) use ($program) {
                if($data['occurrenceStatus'] == 'draft'){
                    return Html::a(
                        '<i class="material-icons">delete</i>','#',
                        [
                            'title' => Yii::t('app', 'Delete Occurrence'),
                            'class' => 'delete-occurrence-row',
                            'id'=>$data['occurrenceDbId'].'-row',
                            'data-occurrenceDbId' => $data['occurrenceDbId']
                        ]
                    );
                }
            }
        ]
    ],
];

$columns = array_merge($rowNumber, $columns);

$addOccurrenceBtn = '';

if(isset($planTemplate['status']) && $planTemplate['status'] == 'uploaded'){
    $addOccurrenceBtn = '&nbsp;<a id="confirm-add-occurrence-btn" href="#" class="grey lighten-3 black-text btn btn-primary waves-effect waves-light pull-left disabled" style="margin-right:5px" data-type="uploaded"><i class="material-icons">file_upload</i></a>';
} else {
    $addOccurrenceBtn = Html::button('Confirm', ['type'=>'button', 'title'=>\Yii::t('app', 'Confirm creation of new occurrences'), 'class'=>'light-green darken-3 btn disabled', 'style'=>'margin-right:5px;', 'id'=>'confirm-add-occurrence-btn', 'data-type'=>'randomization']);
}

$beforeHeader = (isset($planTemplate['status']) && $planTemplate['status'] == 'uploaded') ?'<div class="alert-warning" style="padding: 10px;width:650px">The design for this experiment is generated via upload. Kindly upload the design for the new occurrences.<br><em>Note: Required fields <span class="required">*</span> should be supplied to enable uploading of design for new occurrences.</em></div>':'';
$addBtnDisabled = "";
if($experimentType == 'Breeding Trial' && $planTemplate == null){
    $beforeHeader = '<div class="alert-warning" style="padding: 10px;width:650px">There is no design record for this experiment. Adding of occurrences is not allowed.</div>';
    $addBtnDisabled = "disabled";
}

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'storage' => DynaGrid::TYPE_SESSION,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'showPageSummary' => false,
        'dataProvider' => $dataProvider,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => true,
        'panel' => [
            'heading' => false,
            'before' => '{summary} <br />'.$beforeHeader,
            'after' => Html::button('Add occurrence', ['type'=>'button', 'title'=>\Yii::t('app', 'Add new occurrence'), 'class'=>'light-green darken-3 btn pull-left '.$addBtnDisabled, 'id'=>'add-rowOcc-btn']).'
                    <a class="btn-floating btn-small pull-right" id="scroll-down-btn">
                        <i class="material-icons">expand_more</i>
                    </a><br>
            '
        ],
        'toolbar' => [
            [
                'content' =>
                    Html::beginTag('div', ['class' => 'text-left dropdown']) .
                    Html::a('<i class="material-icons">arrow_back</i>', $backToUrl, ['title'=>\Yii::t('app', 'Back to Occurrences tab'), 'class'=>'btn btn-primary waves-effect waves-light','style'=>'margin-right:5px;', 'id'=>'back-to-tab-btn']
                    ).
                    $addOccurrenceBtn.
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        '#',
                        [
                            'data-pjax' => true,
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid'),
                            'id' => 'reset-occurrences-grid-id'
                        ]
                    ).
                    '{dynagridFilter}{dynagridSort}{dynagrid}' .
                    Html::endTag('div')
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// delete value modal confirmation
Modal::begin([
    'id' => 'delete-occurrence-modal',
    'header' => '<h4><i class="fa fa-trash"></i> Delete draft occurrence</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#',
            [
                'data-dismiss'=>'modal',
                'id' => 'cancel-delete-occurrence-btn',
                'title' => 'Cancel deletion'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm'),
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light delete-occurrence-btn',
                'id'=>'confirm-delete-occurrence-btn',
                'data-dismiss'=>'modal',
                'title' => 'Confirm deletion of draft occurrence'
            ]
        ) .
        '&nbsp;&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete the draft occurrence. This action cannot be undone. Click confirm to proceed.';

Modal::end();

// delete value modal confirmation
Modal::begin([
    'id' => 'delete-value-confirmation-modal',
    'header' => '<h4><i class="fa fa-trash"></i> Delete value</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#',
            [
                'data-dismiss'=>'modal',
                'id' => 'cancel-delete-form-value-btn',
                'title' => 'Cancel deletion'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm'),
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light delete-form-value-btn',
                'id'=>'confirm-delete-value-btn',
                'data-dismiss'=>'modal',
                'title' => 'Confirm deletion of value'
            ]
        ) .
        '&nbsp;&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete the value. This action cannot be undone. Click confirm to proceed.';

Modal::end();

// creation modal confirmation
Modal::begin([
    'id' => 'confirm-creation-occurrence-modal',
    'header' => '<h4><i class="fa fa-trash"></i> Confirm Occurrence/s Creation</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#',
            [
                'data-dismiss'=>'modal',
                'id' => 'cancel-creation-occurrence-btn',
                'title' => 'Cancel creation'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm'),
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light confirm-creation-class-btn',
                'id'=>'confirm-creation-occurrence-btn',
                'data-dismiss'=>'modal',
                'title' => 'Confirm occurrence/s creation'
            ]
        ) .
        '&nbsp;&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to generate occurrence related records. This action cannot be undone. You will be redirected after clicking the Confirm button.';

Modal::end();

// creation modal confirmation
Modal::begin([
    'id' => 'confirm-paramset-creation-occurrence-modal',
    'header' => '<h4><i class="fa fa-trash"></i> Select and Confirm Occurrence/s Creation</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#',
            [
                'data-dismiss'=>'modal',
                'id' => 'cancel-creation-occurrence-btn',
                'title' => 'Cancel creation'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm'),
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light confirm-creation-class-btn',
                'id'=>'confirm-paramset-creation-occurrence-btn',
                'data-dismiss'=>'modal',
                'title' => 'Confirm occurrence/s creation'
            ]
        ) .
        '&nbsp;&nbsp;&nbsp',
    'size' => 'modal-lg',
    'options' => ['data-backdrop'=>'static']
]);

echo '<div class="confirm-paramset-creation-form-modal-body parent-panel"></div>';

Modal::end();

// reset grid url
$resetGridUrl = Url::toRoute(
    [
        '/occurrence/manage/occurrences?' .
        'program=' . $program .
        '&experimentId=' . $experimentId .
        '&updateOccurrence=1'.
        '&occurrenceId='.$occurrenceId
    ]
);
// save row values url
$saveRowValueUrl = Url::to(['manage/save-tabular-form-row-value']);
// manage occurrences url
$manageOccUrl = Url::to(['manage/occurrences', 'program' => $program, 'experimentId' => $experimentId]);
$addOccurrenceUrl = Url::to(['manage/add-new-occurrence', 'program' => $program, 'experimentId' => $experimentId, 'occurrenceId'=>$occurrenceId]);
$deleteOccurrenceUrl = Url::to(['manage/delete-occurrence', 'program' => $program, 'experimentId' => $experimentId, 'occurrenceId'=>$occurrenceId]);
$occurrenceBgProcessUrl = Url::to(['manage/prepare-occurrence-creation', 'program' => $program, 'experimentId' => $experimentId, 'occurrenceId'=>$occurrenceId]);
$redirectToUpdateDesignUrl = Url::to(['design/generate-design', 'program' => $program, 'experimentId'=>$experimentId]);
$getOccurrenceInfo = Url::to(['manage/get-occurrences-info', 'program' => $program, 'experimentId' => $experimentId]);
$renderParamSetUrl = Url::to(['manage/get-parameter-sets','experimentId'=>$experimentId, 'program'=>$program, 'entityType' => 'parameter_sets']);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);


$this->registerJs(<<<JS

    var gridId = '$browserId';
    var containerId = gridId+'-id-container';
    var resetGridUrl = '$resetGridUrl';
    var originalValue = '';
    var id = null;
    var fields = null;
    var column = null; // variable abbrev
    var variableId = null; // variable identifier
    var occurrenceDbId = null; // occurrence identifier
    var value = null; // value
    var thisValue = null; // current field value
    var isRequired = null; // whether required or not
    var type = '';
    var flagOriginalEmpty = true; // to check if original value is empty
    var deleteOccurrenceId = null;
    var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
    var draftOccurrences = '$draftOccurrenceIds';
    var incDraftOccCount = '$incDraftOccCount';
    var draftOccCount = '$draftOccCount';
    var paramSet = '$paramSet'
    var selectedParamSet = null

    $(document).ready(function() {
        refresh();
        draftOccurrences = '$draftOccurrenceIds';
        checkRequiredField();

        //check to show floating button for automatic scroll
        checkScroll();
        
        adjustBrowserPageSize();
    });

    // to fix display of selet2 panel when pjax is triggered
    $(document).on('ready pjax:start', function(e) {
        e.stopPropagation();
        e.preventDefault();

        $(".text-editable-field, .time-editable-field, .editable-field").attr("disabled", "disabled");
        $("#add-rowOcc-btn").addClass("disabled");
        $('.select2-panel').css('position','absolute');
    });

    // reload grid on pjax
    $(document).on('ready pjax:success', '#' + gridId + '-pjax', function(e) {
        e.stopPropagation();
        e.preventDefault();
        refresh();

        //check required field
        checkRequiredField();

        // to remove postion absolute css
        $('.select2-panel').css('position','');


        $(".text-editable-field, .time-editable-field, .editable-field").removeAttr("disabled");
        $("#add-rowOcc-btn").removeClass("disabled");
        setTimeout(function(){
            scrollToBottom();
        }, 3000);

    });


    // js to load even after pjax reload
    function refresh(){
        var maxHeight = ($(window).height() - 370);
        $(".kv-grid-wrapper").css("height",maxHeight);

        // reset occurrences browser
        $('#reset-occurrences-grid-id').on('click', function(e) {
            e.preventDefault();

            $.pjax.reload({
                container: '#' + browserId + '-pjax',
                replace: true,
                url: resetGridUrl
            });
        });

        // upon change of select2 and date field value
        $(document).on('keyup change', ".select2-form-fields", function(e) {

            e.stopPropagation();
            e.preventDefault();

            id = this.id;
            fields = (this.id).split('-');
            column = fields[0]; // variable abbrev


            $("#add-rowOcc-btn").addClass("disabled");
            $(".text-editable-field, .time-editable-field, .editable-field").attr("disabled", "disabled");

            variableId = fields[2]; // variable identifier
            occurrenceDbId = fields[3]; // occurrence identifier
            thisValue = $('#' + id).val().trim(); // value
            isRequired = $(this).hasClass('required-field'); // whether required or not
            type = $('#' + id).attr('type');

            // if field is required and value specified is empty
            if(isRequired && thisValue == ''){

                message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'Field is required. Please specify a value.'

                $('.toast').css('display','none');
                Materialize.toast(message, 5000);

                // set original value
                $(this).val(originalValue);

            } else{
                // if value is empty, display confirmation
                if(thisValue == ''){
                    $('#delete-value-confirmation-modal').modal('show');
                } else {
                    saveFormValue();
                }
            }
        });

        // upon change input and date field
        $(document).on('mousedown', ".text-editable-field, .time-editable-field, .editable-field", function(e) {
            e.stopPropagation();
            $("#add-rowOcc-btn").addClass("disabled");
            if(flagOriginalEmpty){

                id = this.id;
                value = $('#' + id).val().trim(); // value

                originalValue = value;
            }
        });

        // hide reomve in select2 when change to an input field is triggered
        $(document).on('keyup', ".text-editable-field, .time-editable-field, .editable-field", function(e) {
            var newVal = $('#' + id).val().trim(); // value

            $("#add-rowOcc-btn").addClass("disabled");

            if(newVal == '' && originalValue != ''){
                $('.select2-selection__clear').css('display', 'none');

                flagOriginalEmpty = false;
            } else {
                flagOriginalEmpty = true;
            }
        });

        // upon change of text input field value
        $(document).on('change', ".text-editable-field", function(e) {
            e.stopPropagation();
            e.preventDefault();

            $("#add-rowOcc-btn").addClass("disabled");

            id = this.id;
            fields = (this.id).split('-');
            column = fields[0]; // variable abbrev
            variableId = fields[2]; // variable identifier
            occurrenceDbId = fields[3]; // occurrence identifier
            thisValue = $('#' + id).val().trim(); // value
            isRequired = $(this).hasClass('required-field'); // whether required or not
            type = $('#' + id).attr('type');


            // if value is empty, display confirmation
            if(thisValue == '' && originalValue != ''){
                $('#delete-value-confirmation-modal').modal('show');
            } else {
                saveFormValue();
            }

        });

        // cancel confirm delete field value
        $(document).on('click', '#cancel-delete-form-value-btn', function(e){

            // if field is not select2
            if(type !== undefined){
                // set original value before removing value
                $('#' + id).val(originalValue);
            } else {
                $('#reset-occurrences-grid-id').trigger('click');
            }

            message = "<i class='material-icons orange-text left'>warning</i> Delete cancelled.";

            $('.toast').css('display','none');
            Materialize.toast(message, 5000);

            $('#reset-occurrences-grid-id').removeClass('disabled');

        });

        // confirm delete value
        $(document).on('click', '#confirm-delete-value-btn', function(e){
            saveFormValue('delete');
        });

        $(document).on("click",".option-paramsets", function(e) {
            var optArr = (this.value).split("-")
            selectedParamSet = optArr[1]
        });


    }

    // Save form value
    function saveFormValue(action='save'){

        $.ajax({
            url: '$saveRowValueUrl',
            type: 'post',
            cache: false,
            data: {
                entityDbId: occurrenceDbId,
                column: column,
                variableDbId: variableId,
                dataValue: thisValue,
            },
            success: function(response){

                if(action == 'delete'){
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        message = "<i class='material-icons green-text left'>check</i> Value deleted.";

                        $('.toast').css('display','none');
                        Materialize.toast(message, 5000);
                    }
                }

                // if column is site, trigger pjax reload
                if(column == 'SITE'){
                    $.pjax.reload({
                        container: '#' + gridId + '-pjax',
                        replace: true,
                        url: resetGridUrl
                    });
                } else {
                    $('#add-rowOcc-btn').removeClass('disabled');
                    $(".text-editable-field, .time-editable-field, .editable-field").removeAttr("disabled");

                }
                //check required field
                checkRequiredField();

            },
            complete: function(){
                $('#reset-occurrences-grid-id').removeClass('disabled');
            },
            error: function(){
                message = "<i class='material-icons red-text left'>close</i> There was a problem saving the value.";

                $('.toast').css('display','none');
                Materialize.toast(message, 5000);
            }
        });
    }

    // scroll down
    $(document).on('click', '#scroll-down-btn', function(e){
        scrollToBottom();
    });

    // add new occurrence
    $(document).on('click', '#add-rowOcc-btn', function(e){
        e.preventDefault();
        e.stopPropagation();

        $('#add-rowOcc-btn').addClass('disabled');

        $.ajax({
            url: '$addOccurrenceUrl',
            type: 'post',
            dataType: 'json',
            cache: false,
            data: {
            },
            success: function(response){

                if(response.success){
                    message = "<i class='material-icons green-text left'>check</i> Successfully added an occurrence.";

                    $('.toast').css('display','none');
                    Materialize.toast(message, 5000);
                    draftOccurrences = response.draftOccurrences;
                    $('#add-rowOcc-btn').removeClass('disabled');
                    $.pjax.reload({
                        container: '#' + gridId + '-pjax',
                        replace: true,
                        url: resetGridUrl
                    });
                } else {
                    $('#add-rowOcc-btn').removeClass('disabled');
                    message = "<i class='material-icons red-text left'>close</i> There was a problem creating the occurrence.";

                    $('.toast').css('display','none');
                    Materialize.toast(message, 5000);
                }
            },
            error: function(){
                $('#add-rowOcc-btn').removeClass('disabled');
                message = "<i class='material-icons red-text left'>close</i> There was a problem creating the occurrence.";

                $('.toast').css('display','none');
                Materialize.toast(message, 5000);
            }
        });
    });

    // show delete confirmation modal
    $(document).on('click', '.delete-occurrence-row', function(e){
        var varId = this.id;
        deleteOccurrenceId = $('#' + varId).attr('data-occurrenceDbId');
        $('#delete-occurrence-modal').modal('show');
    });

    $(document).on('click', '#confirm-delete-occurrence-btn', function(e){
        $("#add-rowOcc-btn").removeClass("disabled");
        $.ajax({
            url: '$deleteOccurrenceUrl',
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {
                deleteOccurrenceId: deleteOccurrenceId
            },
            success: function(response){
                $('#delete-occurrence-modal').modal('hide');

                if(response.success){
                    message = "<i class='material-icons green-text left'>check</i> Successfully deleted an occurrence.";

                    $('.toast').css('display','none');
                    Materialize.toast(message, 5000);
                    draftOccurrences = response.draftOccurrences;

                    checkRequiredField();

                    $.pjax.reload({
                        container: '#' + gridId + '-pjax',
                        replace: true,
                        url: resetGridUrl
                    });
                } else {
                    message = "<i class='material-icons red-text left'>close</i> There was a problem deleting the occurrence record.";

                    $('.toast').css('display','none');
                    Materialize.toast(message, 5000);
                }

            },
            error: function(){
                message = "<i class='material-icons red-text left'>close</i> There was a problem deleting the occurrence record.";

                $('.toast').css('display','none');
                Materialize.toast(message, 5000);
            }
        });

    });

    $(document).on('click', '#confirm-add-occurrence-btn', function(e){
        var designType = $(this).data('type')
        if(designType == 'uploaded'){

            window.location.assign('$redirectToUpdateDesignUrl'+
                '&occurrenceDbIds='+draftOccurrences+
                '&regenerate=false')
        } else {

            //show parameter set configuration
            console.log(paramSet)
            if(paramSet == 1){
                $('#confirm-paramset-creation-occurrence-modal').modal('show');
                var modalBody = '.confirm-paramset-creation-form-modal-body';
               
                $.ajax({
                    url: '$renderParamSetUrl',
                    type: 'post',
                    dataType: 'json',
                    data:{},
                    success: function(response){
                        $(modalBody).html(response.htmlData);
                    },
                    error: function(){
                        var errorMessage = '<i>There was a problem loading the content.</i>';
                        $(modalBody).html(errorMessage);
                    }
                });
            } else {
                $('#confirm-creation-occurrence-modal').modal('show');
            }
        }

    });

    $('#confirm-creation-occurrence-btn').off('click').on('click', function (e) {
        e.preventDefault()
        var hasToast = $('body').hasClass('toast');

        var update_modal = '#confirm-creation-occurrence-modal > .modal-dialog > .modal-content > .modal-body';
        $(update_modal).html(loadingIndicator);
        if(!hasToast){
            message = "<i class='material-icons green-text left'>check</i> Creation of occurrence has started.";

            $('.toast').css('display','none');
            Materialize.toast(message, 5000);
        }
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '$occurrenceBgProcessUrl',
            data: {},
            success: async function (ajaxResponse) {},
            error: function (xhr, error, status) {}
        });

    })

    $('#confirm-paramset-creation-occurrence-btn').off('click').on('click', function (e) {
        e.preventDefault()
        var hasToast = $('body').hasClass('toast');

        var update_modal = '#confirm-paramset-creation-occurrence-modal > .modal-dialog > .modal-content > .modal-body';
        $(update_modal).html(loadingIndicator);
        if(!hasToast){
            message = "<i class='material-icons green-text left'>check</i> Creation of occurrence has started.";

            $('.toast').css('display','none');
            Materialize.toast(message, 5000);
        }
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '$occurrenceBgProcessUrl'+'&planTemplateDbId='+selectedParamSet,
            data: {},
            success: async function (ajaxResponse) {},
            error: function (xhr, error, status) {}
        });

    })

    function checkScroll(){
        var tableElement = document.getElementById(containerId).querySelectorAll('table');
        var scrollBottom = $('#'+containerId).height() - $(tableElement[0]).height();

        var div= document.getElementById(containerId); // need real DOM Node, not jQuery wrapper
        var hasVerticalScrollbar= div.scrollHeight >  div.clientHeight;

        if(!hasVerticalScrollbar){
            //hide scroll button
            $('#scroll-down-btn').addClass('hidden');
        }
    }

    function scrollToBottom() {
        checkScroll();
        var tableElement = document.getElementById(containerId).querySelectorAll('table');
        var rows = tableElement[0].querySelectorAll('tr');
        var line = rows.length - 1;

        rows.forEach(row => row.classList.remove('active'))
        rows[line].scrollIntoView({
            behavior: 'smooth',
            block: 'center'
        });
    }

    function scrollToTop() {
        var tableElement = document.getElementById(containerId).querySelectorAll('table');
        var rows = tableElement[0].querySelectorAll('tr');
        var line = 1;

        rows.forEach(row => row.classList.remove('active'))
        rows[line].scrollIntoView({
            behavior: 'smooth',
            block: 'center'
        });
    }

    function checkRequiredField(){
        var disableFlag = false;
        var countField = 0;
        var hiddenFields = true
        $(".required-field").each(function(){
            hiddenFields = false
            if($(this).val().length == 0){
                disableFlag = true;
            }
            countField++;
        });

        //get updated occurrences info
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '$getOccurrenceInfo',
            data: {},
            success:function (response) {
                draftOccCount = response['draftOccCount']
                incDraftOccCount = response['incDraftOccCount']
            },
            error: function (xhr, error, status) {}
        });
        
        if(((disableFlag || countField == 0) && !hiddenFields) || (parseInt(draftOccCount) > 0 && parseInt(incDraftOccCount) > 0)){
            $('#confirm-add-occurrence-btn').addClass('disabled');
        } else {
            $('#confirm-add-occurrence-btn').removeClass('disabled');
        }
        if(parseInt(draftOccCount) == 0){
            $('#confirm-add-occurrence-btn').addClass('disabled');
        }
    }
JS);

$this->registerCss('
    body {
        overflow: hidden;
    }

    .datepicker{
        z-index: 998 !important;
    }
');
?>