<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders manage occurrence-level information of Experiment
 */

use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;
use kartik\dynagrid\DynaGrid;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$browserId = 'dynagrid-em-management-data-browser';
// Set default page size url
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
// Save data browser settings
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// render manage tabs
echo Yii::$app->controller->renderPartial('tabs', [
    'experimentId' => $experimentId,
    'program' => $program,
    'active' => 'management',
    'experiment' => $experiment,
    'occurrenceId' => $occurrenceId,
    'isAdmin' => $isAdmin,
    'role' => $role,
    'creatorId' => $creatorId,
    'accessData' => $accessData,
]);

echo '<p>' .
    \Yii::t('app','You may specify and add more variables for management protocol here.') . ' ' .
    '<strong>' . \Yii::t('app','Changes are autosaved.') . '</strong>' .
'</p>';


// display row number
$rowNumber = [
    [
        'class' => 'yii\grid\SerialColumn',
        'header' => false,
        'order'=> DynaGrid::ORDER_FIX_LEFT
    ]
];

$columns = array_merge($rowNumber, $columns);

// add variables select2 plugin options
$dataUrl = Url::home() . '/occurrence/' . Url::to('manage/get-variable-options?usage=management');
$pluginOptions = [
    'allowClear' => true,
    'minimumInputLength' => 0,
    'placeholder' => 'Select variable',
    'minimumResultsForSearch' => 1,
    'language' => [
        'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
    ],
    'ajax' => [
        'url' => $dataUrl,
        'dataType' => 'json',
        'delay' => 250,
        'data' => new JsExpression(
            'function (params){
                var attr = $(this).attr("data-attr");
                var abbrevs = $(this).attr("data-excludedVars");

                return {
                    q: params.term,
                    attr: attr,
                    page: params.page,
                    excludedVars: abbrevs
                }
            }'
        ),
        'processResults' => new JSExpression(
            'function (data, params) {

                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 5) < data.totalCount
                    },
                };
            }'
        ),
        'cache' => true
    ]
];

$options = [
    'placeholder' => 'Select variable',
    'tags' => true,
    'class' => 'filter-dashboard-select',
    'data-attr' => 'label',
    'data-excludedVars' => json_encode($excludedVars),
    'minimumResultsForSearch' => 1,
    'multiple' => true
];
echo '<div class="col col-md-6 select2-panel" style="margin: 6px 0px 0px 6px;">'. Select2::widget([
    'name' => 'manage-management-protocol-add-variable',
    'id' => 'manage-management-protocol-add-variable',
    'maintainOrder' => true,
    'options' => $options,
    'pluginOptions' => $pluginOptions,
    'value' => '',
    'showToggleAll' => false
]) . '</div>' .
'<div class="col col-md-4 select2-panel" style="margin-top: 6px">' . Html::a(\Yii::t('app','Add variable'),
    '#!',
    [
        'class' => 'btn btn-primary waves-effect waves-light add-variable-btn disabled',
        'style' => 'height:37px; line-height:37px',
        'title' => 'Add new variables in the form'
    ]) . '</div>';

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'storage' => DynaGrid::TYPE_SESSION,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => "$browserId-id",
        'showPageSummary' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => "$browserId-id"],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => true,
        'panel' => [
            'heading' => false,
            'before' => '',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' =>
                Html::beginTag('div', ['class' => 'text-left dropdown']) .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        '#',
                        [
                            'data-pjax' => true,
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid'),
                            'id' => 'reset-management-protocol-grid-id'
                        ]
                    ).
                    '{dynagridFilter}{dynagridSort}{dynagrid}' .
                    Html::endTag('div')
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
         'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// delete value modal confirmation
Modal::begin([
    'id' => 'delete-value-confirmation-modal',
    'header' => '<h4><i class="fa fa-trash"></i> Delete value</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#',
            [
                'data-dismiss'=>'modal',
                'id' => 'cancel-delete-form-value-btn',
                'title' => 'Cancel deletion'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm').'<i class="material-icons right">send</i>',
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light delete-form-value-btn',
                'id'=>'confirm-delete-value-btn',
                'data-dismiss'=>'modal',
                'title' => 'Confirm deletion of value'
            ]
        ) .
        '&nbsp;&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete the value. This action cannot be undone. Click confirm to proceed.';

Modal::end();

$varAbbrevsStr = !empty($varAbbrevs) ? json_encode($varAbbrevs) : json_encode($varAbbrevs);

// reset grid url
$resetGridUrl = Url::toRoute(
    [
        '/occurrence/manage/management?' .
        'program=' . $program .
        '&experimentId=' . $experimentId .
        '&varAbbrevs=' .$varAbbrevsStr
    ]
);
// save row values url
$saveRowValueUrl = Url::to(['manage/save-tabular-form-row-value']);
// update transaction dataset
$updateTransactionUrl = Url::to(['manage/update-transaction']);
$transactionUrl = Url::toRoute(["/dataCollection/terminal?program=$program&TerminalTransaction[occurrenceName]="]);
// manage management protocol url
$manageOccUrl = Url::to(['manage/management', 'program' => $program, 'experimentId' => $experimentId, 'occurrenceId' => $occurrenceId]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS

    var browserId = '$browserId';
    var resetGridUrl = '$resetGridUrl';
    var originalValue = '';
    var id = null;
    var fields = null;
    var column = null; // variable abbrev
    var variableId = null; // variable identifier
    var occurrenceDbId = null; // occurrence identifier
    var value = null; // value
    var thisValue = null; // current field value
    var isRequired = null; // whether required or not
    var fieldToDelete = null; // field to delete value
    var type = '';
    var flagOriginalEmpty = true; // to check if original value is empty

    $(document).ready(function() {
        refresh();
        adjustBrowserPageSize()
    });

    // to fix display of selet2 panel when pjax is triggered
    $(document).on('ready pjax:start', function(e) {
        e.stopPropagation();
        e.preventDefault();
        
        $('.select2-panel').css('position','absolute');
    });
    
    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
        refresh();

        // to remove postion absolute css
        $('.select2-panel').css('position','');
    });

    // js to load even after pjax reload
    function refresh(){
        var maxHeight = ($(window).height() - 315);
        $(".kv-grid-wrapper").css("height",maxHeight);

        // reset management protocol browser
        $('#reset-management-protocol-grid-id').on('click', function(e) {
            e.preventDefault();

            $.pjax.reload({
                container: '#' + browserId + '-pjax',
                replace: true,
                url: resetGridUrl
            });

        });

        // upon change of select2 and date field value
        $(document).on('keyup change', ".select2-form-fields", function(e) {
            e.stopPropagation();
            e.preventDefault();

            $('#reset-management-protocol-grid-id').addClass('disabled');

            id = this.id;
            fields = (this.id).split('-');
            column = fields[0]; // variable abbrev
            variableId = fields[2]; // variable identifier
            occurrenceDbId = fields[3]; // occurrence identifier
            thisValue = $('#' + id).val().trim(); // value
            isRequired = $(this).hasClass('required-field'); // whether required or not
            type = $('#' + id).attr('type');

            // if field is required and value specified is empty
            if(isRequired && value == ''){

                message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'Field is required. Please specify a value.'

                $('.toast').css('display','none');
                Materialize.toast(message, 5000);

                // set original value
                $(this).val(originalValue);

            } else{
                // if value is empty, display confirmation
                if(thisValue == ''){
                    $('#delete-value-confirmation-modal').modal('show');
                } else {
                    saveFormValue();
                }
            }
        });

        // upon change input and date field
        $(document).on('mousedown', ".text-editable-field, .time-editable-field, .editable-field, .numeric-editable-field, .float-editable-field", function(e) {
            e.stopPropagation();

            if(flagOriginalEmpty){

                id = this.id;
                value = $('#' + id).val().trim(); // value

                originalValue = value;
            }

        });

        // hide remove in select2 when change to an input field is triggered
        $(document).on('keyup', ".text-editable-field, .time-editable-field, .editable-field, .numeric-editable-field, .float-editable-field", function(e) {
            var newVal = $('#' + id).val().trim(); // value

            if(newVal == '' && originalValue != ''){
                $('.select2-selection__clear').css('display', 'none');

                flagOriginalEmpty = false;
            } else {
                flagOriginalEmpty = true;
            }
        });

        // upon change of text input field value
        $(document).on('change paste', ".text-editable-field, .numeric-editable-field, .float-editable-field", function(e) {
            e.stopPropagation();

            $('#reset-management-protocol-grid-id').addClass('disabled');

            id = this.id;
            fields = (this.id).split('-');
            column = fields[0]; // variable abbrev
            variableId = fields[2]; // variable identifier
            occurrenceDbId = fields[3]; // occurrence identifier
            thisValue = value = $('#' + id).val().trim(); // value
            isRequired = $(this).hasClass('required-field'); // whether required or not
            type = $('#' + id).attr('type');

            // if field is required and value specified is empty
            if(isRequired && thisValue == ''){

                message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'Field is required. Please specify a value.'

                $('.toast').css('display','none');
                Materialize.toast(message, 5000);

                // set original value
                $(this).val(originalValue);

            } else{
                // if value is empty, display confirmation

                if(thisValue == '' && originalValue != ''){
                    $('#delete-value-confirmation-modal').modal('show');
                } else {
                    saveFormValue();
                }
            }

            $('.select2-selection__clear').css('display', '');

        });

        // upon change of time input field value
        $(document).on('change paste', ".time-editable-field", function(e) {
            e.stopPropagation();
            e.preventDefault();

            $('#reset-management-protocol-grid-id').addClass('disabled');

            id = this.id;
            fields = (this.id).split('-');
            column = fields[0]; // variable abbrev
            variableId = fields[2]; // variable identifier
            occurrenceDbId = fields[3]; // occurrence identifier
            thisValue = $('#' + id).val().trim(); // value
            isRequired = $(this).hasClass('required-field'); // whether required or not
            type = $('#' + id).attr('type');
     
            var isValid = isValidDate(thisValue);

            // invalid date
            if(!isValid && thisValue != ''){
                message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'Invalid input for date.'

                $('.toast').css('display','none');
                Materialize.toast(message, 5000);

                // set original value
                $(this).val(originalValue);
            } else if(thisValue == '' && originalValue != ''){
                // if value is empty, display confirmation

                $('#delete-value-confirmation-modal').modal('show');
            } else {
                saveFormValue();
            }
        });

        // cancel confirm delete field value
        $(document).on('click', '#cancel-delete-form-value-btn', function(e){

            // if field is not select2
            if(type !== undefined){
                // set original value before removing value
                $('#' + id).val(originalValue);
            } else {
                $('#reset-management-protocol-grid-id').trigger('click');
            }

            message = "<i class='material-icons orange-text left'>warning</i> Delete cancelled.";

            $('.toast').css('display','none');
            Materialize.toast(message, 5000);

            $('#reset-management-protocol-grid-id').removeClass('disabled');

        });

        // confirm delete value
        $(document).on('click', '#confirm-delete-value-btn', function(e){
            saveFormValue('delete');
        });

        // add new variables in the form
        $(document).on('click', '.add-variable-btn', function(e){
            // newly added variable
            var varAbbrevs = $('#manage-management-protocol-add-variable').val();
            
            // previously added vars
            var varAbbrevsStr = '$varAbbrevsStr';
            var origVarAbbrevs = JSON.parse(varAbbrevsStr);

            // merge original variables and newly added vars
            var newCombinedVarAbbrevs = origVarAbbrevs.concat(varAbbrevs);
            var varAbbrevsStr = JSON.stringify(newCombinedVarAbbrevs);
            var manageUrl = '$manageOccUrl' + '&varAbbrevs=' + varAbbrevsStr;

            window.location = manageUrl;
        });

        // if variable is selected, enable add variable button
        $(document).on('change', "#manage-management-protocol-add-variable", function(e) {
            var selected = $('#manage-management-protocol-add-variable').val();

            // enable add variable button if there is selected variable
            if(selected.length){
                $('.add-variable-btn').removeClass('disabled');
            } else {
                $('.add-variable-btn').addClass('disabled');
            }
        });
    }

    // Validate date
    // check if date is valid
    function isValidDate(dateString) {
        var isValid = true;
        var regEx = /^\d{4}-\d{2}-\d{2}$/;
        
        isValid = dateString.match(regEx) != null;

        // if format is valid, check if year, month and day are valid
        if(isValid){
            var dateArr = dateString.split("-");

            var year = dateArr[0];
            var month = dateArr[1];
            var day = dateArr[2];

            if(year < 1970){
                return 'Please enter year later than 1969';
            }

            if(month > 12){
                return 'Please enter a valid month';
            }

            if(day > 31){
                return 'Please enter a valid day';
            }
        }

        return isValid;
    }

    // Save form value
    function saveFormValue(action='save'){
        $('#reset-management-protocol-grid-id').addClass('disabled');

        $.ajax({
            url: '$saveRowValueUrl',
            type: 'post',
            cache: false,
            data: {
                entityDbId: occurrenceDbId,
                column: column,
                variableDbId: variableId,
                dataValue: thisValue,
            },
            success: function(response){

                if(action == 'delete'){
                    message = "<i class='material-icons green-text left'>check</i> Value deleted.";

                    $('.toast').css('display','none');
                    Materialize.toast(message, 5000);
                }

                // if column is site, trigger pjax reload
                if(column == 'SITE'){
                    $('#reset-management-protocol-grid-id').trigger('click');
                }

                if(response && action=='save'){
                    updateTransaction(occurrenceDbId, variableId);
                }

            },
            complete: function(){
                $('#reset-management-protocol-grid-id').removeClass('disabled');
            }
        });
    }

    function updateTransaction(occurrenceDbId, variableDbId){
        $.ajax({
            url: '$updateTransactionUrl',
            type: 'post',
            cache: false,
            data: {
                occurrenceDbId: occurrenceDbId,
                variableDbId: variableId,
            },
            success: function(response){
                if(response !== ''){
                    const message = "<i class='material-icons green-text left'>check</i> Successfully updated " +
                        '<strong><a class=white-text href="' + '$transactionUrl' + response +
                        '">&nbsp;trait data collection transaction(s)</a></strong>';
                    $('.toast').css('display','none');
                    Materialize.toast(message, 5000);
                }
            },
        });
    }

JS);

$this->registerCss('
    body {
        overflow: hidden;
    }

    .datepicker{
        z-index: 998 !important;
    }
');
?>