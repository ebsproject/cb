<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
/**
 * Renders Experiment manager filters
 */
extract($dashboardFilters);

$ownedExperimentsSwitch = (isset($owned) && ($owned=='true')) ? 'checked' : '';
?>
<ul id="occurrence-filters-panel" class="side-nav" style="padding-bottom: 300px;">
    <li class="header">
      <a class="subheader" style="display: inline-block;width: 155px;white-space: nowrap;" title="Filter Experiments"><i class="material-icons">filter_alt</i>Filter Experiments</a>
    </li>

    <div style="margin: 15px 0px 10px 15px;">
        <a href="#!" class="clear-experiment-filters bold" title="<?= \Yii::t('app', 'Clear all selected values') ?>" ><?= \Yii::t('app', 'Clear all') ?></a>
    </div>

    <?php

        $fields = [
            'program',
            'year',
            'season',
            'stage',
            'site',
            'field_name', // this will result into "field.geospatial_object_name AS field"
            'experiment_code',
            'experiment_name',
            'occurrence_name',
            'location', // this will result into "location.location_name AS location"
        ];

        foreach ($fields as $key => $value) {
            /* This trims fields with "_name" to follow the B4R Terminology convention */
            // Remove "_name" substrings
            $trimmedField = str_replace('_name', '', $value);
            // Replace remaining '_' with whitespaces
            $trimmedField = str_replace('_', ' ', $trimmedField);

            $required = ($value == 'program') ? ' <span class="required">*</span>' : '';
            echo '<li><label class="control-label">'.\Yii::t('app', ucfirst($trimmedField)). $required . '</label>';

            $multiple = ['multiple' => true, 'tokenSeparators' => [',', ' ']];
            $options = [
                'placeholder' => 'Select ' . $trimmedField,
                'tags' => true,
                'class' => 'filter-occurrences-select'
            ];

            if($value == 'program'){
                
                echo Select2::widget([
                    'name' => 'data_filter_'.$value,
                    'id' => $value.'-occurrence_select',
                    'maintainOrder' => true,
                    'options' => $options
                ]).'</li>'; 
            
            }else{
                
                $options = [
                    'placeholder' => 'Select ' . $trimmedField,
                    'tags' => true,
                    'class' => 'filter-occurrences-select',
                    'data-attr' => $value,
                    'minimumResultsForSearch' => 1,
                    'multiple' => true
                ];

                $dataUrl = Url::home() . '/occurrence/' . Url::to('default/get-filter-data');
               
                // select2 plugin options
                $pluginOptions = [
                    'allowClear' => true,
                    'minimumInputLength' => 0,
                    'placeholder' => 'Select ' . $trimmedField,
                    'minimumResultsForSearch' => 1,
                    'language' => [
                        'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
                    ], 
                    'ajax' => [
                        'url' => $dataUrl,
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression(
                            'function (params){
                                var attr = $(this).attr("data-attr");
                                return {
                                    q: params.term, 
                                    attr: attr,
                                    page: params.page,
                                    programId: '.$programId.'
                                }
                            }'
                        ),
                        'processResults' => new JSExpression(
                            'function (data, params) {

                                params.page = params.page || 1;

                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 5) < data.totalCount
                                    },
                                };
                            }'
                        ),
                        'cache' => true
                    ]
                ];

                $field = $value;
                if(in_array($value, ['stage', 'site', 'season']))
                    $field = $value . '_id';

                $initValueText = [];
                // load saved values
                if(!empty($occurrenceTextFilters)){
                    foreach ($occurrenceTextFilters as $k => $v) {
                        $fieldObj = explode('-', $k);
                        $fld = $fieldObj[0];

                        if($fld == $value){
                            $initValueText = $occurrenceTextFilters[$k];
                        }
                    }
                }

                $fieldValue = (isset($$field) && !empty($$field)) ? ($$field) : [];

                if(count($fieldValue) != count($initValueText)){
                    $fieldValue = $initValueText = [];
                }

                echo Select2::widget([
                    'name' => 'data_filter_'.$value,
                    'id' => $value,
                    'maintainOrder' => true,
                    'options' => $options,
                    'pluginOptions' => $pluginOptions,
                    'value' => $fieldValue,
                    'initValueText' => $initValueText,
                    'showToggleAll' => false
                ]).
                '</li>';
            }
        }
        
    ?>

    <!-- Switch to show owned experiments only -->
    <li>
        <div class="switch">
            <label class="pull-right">
                Show all
                    <input type="checkbox" id="data_filter_show_owned_occurrences" <?= $ownedExperimentsSwitch ?>>
                    <span class="lever" style="margin: 10px 7px;"></span>
                My experiments
            </label>
        </div>
    </li>

    <!-- Apply filters -->
    <li class="header" style="margin-top: 10px">
        <div class="clearfix"></div>
        <button class="btn pull-right waves-effect waves-light" id="apply-occurrences-filter" style="margin-right:20px;max-width: 105px;"><?= \Yii::t('app', 'Apply') ?></button>
        <button class="btn pull-right waves-effect black-text lighten-2 waves-light grey cancel-left-filter" style="margin-right:5px;max-width: 105px;"><?= \Yii::t('app', 'Cancel') ?></button>
    </li>

</ul>

<style>
    #occurrence-filters-panel > li:not(.header){
        margin: 0px 20px 0px 15px;
        line-height: 28px;
    }
    .side-nav li>a.btn-flat:hover{
        box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    }
</style> 

<?php
// loop through each fields and retrieve saved data
$fieldVals = [];
$fields = ['program'];
foreach ($fields as $key => $value) {
    $field = $value;
    if(in_array($value, ['program', 'stage', 'site', 'season']))
        $field = $value . '_id';
    
    $fieldVals[$value] = (isset($$field) && !empty($$field)) ? ($$field) : [];

}

$fieldValsStr = json_encode($fieldVals);
$fieldsStr = json_encode($fields);

//get url parameters
$currUrlParams = addslashes(json_encode($_GET));
// save filters URL
$applyFiltersUrl = Url::to(['/occurrence/default/apply-filters']);

$this->registerJs(<<<JS
$(document).ready(function(){

    /* Apply Dashboard Filters BEGIN */
    var fields = JSON.parse('$fieldsStr');
    var fieldsValStr = '$fieldValsStr';
    var fieldVals = JSON.parse(fieldsValStr.replace(/ 0+(?![\. }])/g, ' '));

    var i;
    for(i=0; i < fields.length; i++) {

        var field = fields[i];
        var variableVal = field[0].toUpperCase() + field.slice(1);
        var url = '/index.php/dashboard/default/getstudyattrtags?attr='+field;

        if(field == 'program'){
            var url = '/index.php/dashboard/default/getprogramtags';
        }

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            async: false,
            success: function(response) {
                option = response;
                var lookup = {};
                this['s'+field] = document.getElementById(field+'-occurrence_select');

                for(var i=0; i<option.length;i++){
                    this['new' + variableVal +'Option'] = document.createElement("option");

                    if (!(parseInt(option[i]['id']) in lookup)) {

                        lookup[parseInt(option[i]['id'])] = 1;

                        this['new' + variableVal +'Option'].value = parseInt(option[i]['id']);
                        this['new' + variableVal +'Option'].innerHTML = option[i]['text'];
                        this['s'+field].options.add(this['new' + variableVal +'Option']);
                    }
                }

                var selectedVals = fieldVals[field];
                $('#'+field+'-occurrence_select').val(selectedVals);
                $('#'+field+'-occurrence_select').trigger('change');

            },
            error: function() {
            }
        });
    }
    /* Apply Dashboard Filters END */

    // apply filters
    $('#apply-occurrences-filter').click(function(){

        var elemValsArr = [];
        var elemValsTextArr = [];
        
        // show owned occurrences filter
        var showOwnedExperiments = document.getElementById('data_filter_show_owned_occurrences').checked;
        var ownedVal = {
            owned: showOwnedExperiments
        };
        elemValsArr.push(ownedVal);

        // loop through each elements
        $('.filter-occurrences-select').each(function(index, element){
            var elemId = $(element).attr('id');

            var elemObj = elemId.split('-');
            var elemAttr = elemObj[0];
            var elemVal = $(element).val();

            var newElemVal = {
                [elemAttr]: elemVal
            };

            var elemValsTextVals = [];
            var textVals = $('#'+elemId).select2('data'); 

            textVals.forEach(function (item) { 
                elemValsTextVals.push(item.text);
            })

            var newElemText = {
                [elemAttr+'-text']: elemValsTextVals
            };

            if(textVals !== null)
                elemValsTextArr.push(newElemText);
            if(elemVal !== null)
                elemValsArr.push(newElemVal);

        });

        $.ajax({
            url: '$applyFiltersUrl',
            type: 'post',
            data: {
                data: elemValsArr,
                texts: elemValsTextArr,
                curr_url_params: '$currUrlParams',
            },
            success: function(response) {
                window.location = response;
            },
            error: function() {
            }
        });

    });

    // clear all filters
    $('.clear-experiment-filters').click(function(){
        $('.filter-occurrences-select').each(function(index, element){
            var elemId = $(element).attr('id');
            var elemObj = elemId.split('-');
            var elemAttr = elemObj[0];

            // exclude clearing selected program
            if(elemAttr != 'program'){
                $(element).val(null);
                $(element).trigger('change');
            }
        });
    });
  
});
JS
);