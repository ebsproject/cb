<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use app\components\FavoritesWidget;
use app\components\PrintoutsWidget;
use app\models\DataBrowserConfiguration;
use app\models\PlanTemplate;
use app\models\UserDashboardConfig;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use macgyer\yii2materializecss\widgets\Button;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use app\components\AdvancedDataBrowserWidget;
use app\components\DownloadWidget;

Yii::$app->session->set('program', $program);

// Retrieve query param values
$queryParamsFileName = explode('filename=', $_SERVER['QUERY_STRING']);
$queryParamsEntity = explode('entity=', $_SERVER['QUERY_STRING']);
$queryParamsEntityId = explode('entityId=', $_SERVER['QUERY_STRING']);

$fileName = (count($queryParamsFileName) > 1) ? strtok($queryParamsFileName[1], '&') : '';
$entity = (count($queryParamsEntity) > 1) ? strtok($queryParamsEntity[1], '&') : '';
$entityId = (count($queryParamsEntityId) > 1) ? strtok($queryParamsEntityId[1], '&') : '';

$notificationsString = \Yii::t('app', 'NOTIFICATIONS');
$earlierString = \Yii::t('app', 'earlier');
// If threshold is not present, set Select all checkboxes' threshold to the page's limit
$selectOccurrenceThreshold = Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'selectOccurrences') ?? UserDashboardConfig::getDefaultPageSizePreferences();
$isAdmin = Yii::$app->session->get('isAdmin');

$printouts = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_PRINTOUTS', 'OCCURRENCES')) ?
    PrintoutsWidget::widget([
        "product" => "Experiment Manager",
        "program" => $program,
        "occurrenceIds" => [ ],
        "checkboxSessionStorage" => "occurrenceCheckboxIds"
    ]) : '';

// Load HTML, JS, CSS, and jQuery content of Download Widget
echo DownloadWidget::widget([
    'program' => $program,
    'occurrenceIds' => [],
    'checkboxSessionStorage' => 'occurrenceCheckboxIds',
]);
/**
 * Renders experiment manager browser
 */

$mappingFileInJsonFormatClass = ($role == 'COLLABORATOR') ? 'hidden' : '';

// BA PDM URL
$baUrl = Yii::$app->session->get('BA_URL');
$pdmUrl = $baUrl.'pdm?occurrenceDbId=';
$toolAbbrev = "OCCURRENCES"; // abbreviation of the tool

$columns = [
    [
        // Checkbox (select all)
        'header' => "
            <input type='checkbox' class='filled-in' id='occurrence-select-all' />
            <label for='occurrence-select-all' title='Select up to $selectOccurrenceThreshold occurrence records'></label>
        ",
        // Checkbox (for each row)
        'content' => function ($data) {
            $dataProcessAbbrev = $data['dataProcessAbbrev'] ?? 'none';

            $checkbox = "<input
                class='occurrence-grid-select filled-in'
                id='{$data['occurrenceDbId']}'
                experimentStewardId='{$data['experimentStewardDbId']}'
                creatorId='{$data['creatorDbId']}'
                type='checkbox'
                occurrence-name='{$data['occurrenceName']}'
                data-data-process-abbrev='$dataProcessAbbrev'
                data-experiment-db-id='{$data['experimentDbId']}'
                data-experiment-code='{$data['experimentCode']}'
                data-location-db-id='{$data['locationDbId']}'
                data-location-code='{$data['locationCode']}'
                data-occurrence-number='{$data['occurrenceNumber']}'
                data-occurrence-status ='{$data['occurrenceStatus']}'
                data-plot-count ='{$data['plotCount']}'
                data-experiment-type ='{$data['experimentType']}'
                data-experiment-design-type ='{$data['experimentDesignType']}'
                data-occurrence-design-type ='{$data['occurrenceDesignType']}'
            />";

            return $checkbox . '
                <label for="' . $data['occurrenceDbId'] . '"></label>
            ';
        },
        'hAlign' => 'center',
        'vAlign' => 'top',
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => false,
        'noWrap' => true,
        'vAlign' => 'top',
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'template' => '{view} {export} {update} {commit} {generate_location}{delete}',
        'buttons' => [
            'view' => function ($url, $data) use ($isAdmin, $program) {
                if(in_array($data['occurrenceStatus'], ['design deletion in progress', 'design deletion done','design generation in progress', 'design generation done'])){

                    return '';
                } else {
                    if (strpos(strtolower($data['occurrenceStatus']), 'draft;') !== false || strpos(strtolower($data['experimentStatus']), 'in progress') !== false || strpos(strtolower($data['occurrenceStatus']), 'deletion in progress') !== false){
                        return '';
                    }

                    return ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_VIEW_OCCURRENCE', 'OCCURRENCES', $data['creatorDbId'])) ?
                        Html::a(
                            '<i class="material-icons">remove_red_eye</i>',
                            [
                                '/occurrence/view',
                                'program' => $program,
                                'id' => $data['occurrenceDbId'],
                            ],
                            [
                                'title' => Yii::t('app', 'View Occurrence')
                            ]
                        ) : '';
                }
            },
            'export' => function ($url, $data) use ($isAdmin, $program, $downloadDataCollectionDropdownItemParams) {

                if(in_array($data['occurrenceStatus'], ['design deletion in progress', 'design deletion done','design generation in progress', 'design generation done'])){

                    return '';
                } else {
                    if (strpos(strtolower($data['occurrenceStatus']), 'draft;') !== false || strpos(strtolower($data['experimentStatus']), 'in progress') !== false || strpos(strtolower($data['occurrenceStatus']), 'deletion in progress') !== false){
                        return '';
                    }

                    $htmlDropdownItems = '';
                
                    foreach ($downloadDataCollectionDropdownItemParams as $value) {
                        $value['options']['data-occurrence-id'] = $data['occurrenceDbId'];
                        $value['options']['data-plot-count'] = $data['plotCount'];

                        $dropdownItem = Html::a(
                            $value['text'],
                            '#',
                            $value['options']
                        );

                        // Concatenate dropdown items together
                        $htmlDropdownItems .= "<li>$dropdownItem</li>";
                    }


                    return ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES', $data['creatorDbId'])) ?
                        "<span class='dropdown bootstrap-dropdown' title='Download files'>
                            <a class='dropdown-toggle' type='button' data-toggle='dropdown'><i class='material-icons'>file_download</i></a>
                            <ul class='dropdown-menu'>
                               <li title='Download mapping file in CSV format'>
                                    <a class='download-mapping-file-btn hide-loading' data-id='".$data['occurrenceDbId']."' data-type='csv'>Mapping file (CSV)
                                    </a>
                                </li>
                                <li title='Download data collection file for the Occurrence'>
                                    <a class='download-data-collection-file-btn hide-loading'
                                        href='".Url::toRoute(['/dataCollection/export-workbook/index',
                                            'program' => $program,
                                            'occurrenceId' => $data['occurrenceDbId'],
                                            'locationId' => ($data['locationDbId']),
                                            'source' => 'em-occ' ?? null
                                        ])."'>
                                        ".Yii::t('app', 'Trait data collection ')."
                                    </a>
                                </li>
                                $htmlDropdownItems
                                <li title='Download data collection summary file with Trait as column headers'>
                                    <a
                                        class='download-single-data-collection-summary-btn hide-loading'
                                        data-id='{$data['occurrenceDbId']}'
                                        data-column-header-type='trait'
                                    >
                                        Data collection summary (trait headers)
                                    </a>
                                </li>
                                <li title='Download data collection summary file with Occurrence names as column headers'>
                                    <a
                                        class='download-single-data-collection-summary-btn hide-loading'
                                        data-id='{$data['occurrenceDbId']}'
                                        data-column-header-type='occurrence'
                                    >
                                        Data collection summary (occurrence name headers)
                                    </a>
                                </li>
                            </ul>
                        </span>" : '';
                }
            },
            'update' => function ($url, $data) use ($program, $toolAbbrev, $isAdmin){
                if(in_array($data['occurrenceStatus'], ['design deletion in progress', 'design deletion done','design generation in progress', 'design generation done'])){

                    $template = \Yii::$container->get('app\models\PlanTemplate')->searchAll(['entityType'=>'occurrence', 'occurrenceApplied'=> "%".$data['occurrenceDbId']."%"], 'sort=creationTimestamp:DESC');
                    
                    if($template['totalCount'] > 0){
                        $occurrenceJson = $template['data'][0];

                        // check permission
                        $render = (!$isAdmin) ? Yii::$app->access->renderAccess("UPDATE_OCCURRENCE", $toolAbbrev, $data['creatorDbId'], $data['accessData'], 'write') : true;

                        return !$render ? '' : Html::a(
                            '<i class="material-icons">edit</i>',
                            "#",
                            [
                                'data-program' => $program, 
                                'data-experimentdbid' => $data['experimentDbId'], 
                                'data-occurrencedbids' => str_replace(";", "|", $occurrenceJson['occurrenceApplied']),
                                'class' => 'update-design-btn',
                                
                            ],
                            [
                                'title' => Yii::t('app', 'Update Occurrence Design')
                            ]
                        );
                    }
                } else {
                    if (strpos(strtolower($data['occurrenceStatus']), 'completed') !== false 
                        || strpos(strtolower($data['occurrenceStatus']), 'in progress') !== false
                        || strpos(strtolower($data['experimentStatus']), 'in progress') !== false
                    ){
                        return '';
                    }

                    // check permission
                    $render = (!$isAdmin) ? Yii::$app->access->renderAccess("UPDATE_OCCURRENCE", $toolAbbrev, $data['creatorDbId'], $data['accessData'], 'write') : true;

                    return !$render ? '' : Html::a(
                        '<i class="material-icons">edit</i>',
                        [
                            'manage/occurrences', 
                            'program' => $program, 
                            'experimentId' => $data['experimentDbId'],
                            'experiment' => $data['experiment'],
                            'occurrenceId' => $data['occurrenceDbId']
                        ],
                        [
                            'title' => Yii::t('app', 'Update Experiment and Occurrence')
                        ]
                    );
                }
            },
            'generate_location' => function ($url, $data) use ($role, $isAdmin, $toolAbbrev) {
                if ($role == 'COLLABORATOR' || strpos(strtolower($data['occurrenceStatus']), 'created') === false 
                    || strpos(strtolower($data['occurrenceStatus']), 'draft;') !== false
                    || strpos(strtolower($data['experimentStatus']), 'in progress') !== false
                ){
                    return '';
                }

                // check permission
                $render = (!$isAdmin) ? Yii::$app->access->renderAccess("GENERATE_LOCATION", $toolAbbrev, $data['creatorDbId'], $data['accessData'], 'write') : true;

                return !$render ? '' : Html::a(
                    '<i class="material-icons">add_location</i>',
                    '#',
                    [
                        'data-target' => '#generate-location-modal',
                        'data-toggle' => 'modal',
                        'class' => 'generate-location-btn',
                        'data-name' => htmlspecialchars($data['occurrenceName']),
                        'data-id' => $data['occurrenceDbId'],
                        'href' => '#!',
                        'title' => \Yii::t('app', 'Generate location')
                    ]
                );
            },
            'commit' => function ($url, $data) use ($role, $isAdmin, $toolAbbrev) {
                if ($role == 'COLLABORATOR' 
                    || strpos(strtolower($data['occurrenceStatus']), 'mapped') === false 
                    || strpos(strtolower($data['occurrenceStatus']), 'draft;') !== false
                    || strpos(strtolower($data['occurrenceStatus']), 'upload mapped plots failed') !== false
                    || strpos(strtolower($data['experimentStatus']), 'in progress') !== false
                )
                    return '';

                // check permission
                $render = (!$isAdmin) ? Yii::$app->access->renderAccess("COMMIT_OCCURRENCE", $toolAbbrev, $data['creatorDbId'], $data['accessData'], 'write') : true;

                return !$render ? '' : Html::a(
                    '<i class="material-icons">check_circle</i>',
                    '#',
                    [
                        'data-target' => '#commit-occurrence-modal',
                        'data-toggle' => 'modal',
                        'class' => 'commit-occurrence-btn',
                        'data-name' => htmlspecialchars($data['occurrenceName']),
                        'data-id' => $data['occurrenceDbId'],
                        'href' => '#!',
                        'title' => \Yii::t('app', 'Commit occurrence')
                    ]
                );
            },
            'delete' => function ($url, $data) use ($program, $isAdmin, $toolAbbrev, $role){
                if (!in_array($data['occurrenceStatus'], ['created', 'mapped', 'planted', 'draft']) || $role == 'COLLABORATOR'){
                    return '';
                }

                // check permission
                $render = (!$isAdmin) ? Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DELETE_OCCURRENCE', 'OCCURRENCES', $data['creatorDbId'], $data['accessData'], 'write') : true;

                return !$render ? '' : Html::a(
                    '<i class="material-icons">delete</i>',
                    '#',
                    [
                        'class'=>'delete-occurrence',
                        'title' => Yii::t('app', 'Delete Occurrence'),
                        'data-id' =>$data['occurrenceDbId'],
                        'data-occurrence_name' => htmlspecialchars($data['occurrenceName'])
                    ]
                );
            },
        ]
    ],
    [
        'attribute' => 'occurrenceStatus',
        'label' => 'Status',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
                'ajax' => [
                    'url' => '/index.php/occurrence/default/get-filters-data',
                    'dataType' => 'json',
                    'delay' => 150,
                    'type'=>'POST',
                    'data' => new JsExpression(
                        'function (params) {
                            var value = (params.term === "")? undefined: params.term

                            return {
                                "filter": {
                                    "occurrenceStatus": value
                                },
                                "column": "occurrenceStatus"
                            }
                        }'
                    ),
                    'processResults' => new JSExpression(
                        'function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.items
                            };
                        }'
                    ),
                    'cache' => true
                ]
            ]
        ],
        'filterInputOptions' => [
            'placeholder' => '',
            'style' => 'min-width:100px'
        ],
        'value' => function ($model) {
            $status = strtoupper(trim($model['occurrenceStatus']));
            $statuses = explode(';', $status);

            $statusVal = '';
            foreach ($statuses as $key => $value) {

                $class = 'grey';
                if(strpos(strtolower($value), 'pack') !== false){
                    $plantingJobModel = \Yii::$container->get('app\models\PlantingJob');
                    $class = $plantingJobModel->getStatusClass(strtolower($value));
                } else if ($value == 'COMMITTED' || $value == 'PLANTED' || $value == 'CROSSED' || $value == 'HARVEST COMPLETED') {
                    $class = 'green darken-3';
                } else if ($value == 'MAPPED' || strpos($value, 'IN PROGRESS')) {
                    $class = 'amber darken-3';
                } else if(strpos($value, 'FAILED')) {
                    $class = 'red';
                }

                $statusVal .= '<span
                    id="occurrence-status-' . $model['occurrenceDbId'] .'"
                    title="Status: ' . $value . '"
                    class="bold new badge ' . $class . '"
                    data-occurrence-detailed-status="' . strtolower($status) . '"
                >' . $value . '</span>';
            }

            return $statusVal;
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'programCode',
        'label' => 'program',
        'format' => 'raw',
        'visible' => (Yii::$app->session->get('isAdmin') == true) ? false : true, // display program column if non-admin user
        'group' => true,
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'experimentCode',
        'group' => true,
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'groupHeader' => function ($model) { // Closure method
            $status = explode(';', strtoupper($model['experimentStatus']));

            return [
                'content' => [ // content to show in each summary cell
                    2 => end($status),
                    3 => $model['programCode'],
                    4 => $model['experimentCode'],
                    5 => $model['experiment'],
                    10 => $model['experimentType'],
                    11 => $model['experimentStageCode'],
                    12 => $model['experimentYear'],
                    13 => $model['experimentSeasonCode'],
                ],
                'options' => ['class' => 'bold light-green lighten-4 table-info' . $model['experiment']]
            ];
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'experiment',
        'format' => 'raw',
        'group' => true,
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'occurrenceName',
        'format' => 'raw',
        'label' => 'Occurrence',
        'value' => function ($model) {
            $occurrenceDbId = $model['occurrenceDbId'];
            $occurrenceName = $model['occurrenceName'];

            return '
                <span
                    id="occurrence-name-' . $occurrenceDbId . '"
                >' . $occurrenceName . '</span>
            ';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'siteCode',
        'label' => 'Site',
        'value' => function ($model) {
            return '
                <span title="' . $model['site'] . '">' . 
                    $model['siteCode'] . 
                '</span>
            ';
        },
        'format' => 'raw',
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'field',
        'format' => 'raw',
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'location',
        'format' => 'raw',
        'value' => function ($data) use ($program) {
            return Html::a(
                $data['location'],
                [
                    '/location/view/basic',
                    'program' => $program,
                    'id' => $data['locationDbId']
                ],
                [
                    'title' => Yii::t('app', 'View Location')
                ]

            );
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    /*
    To be added later
    [
        'attribute' => 'programCode',
        'group' => true,
        'label' => 'program',
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">'.$model['experimentCode'].'</span>';
        }
    ],
    [
        'attribute' => 'project',
        'group' => true,
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">'.$model['experimentCode'].'</span>';
        }
    ],
    */
    [
        'attribute' => 'experimentType',
        'format' => 'raw',
        'group' => true,
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'experimentStageCode',
        'label' => 'Stage',
        'group' => true,
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'experimentYear',
        'label' => 'Year',
        'group' => true,
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'experimentSeasonCode',
        'label' => 'Season',
        'group' => true,
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'occurrenceDesignType',
        'label' => 'Design',
        'format' => 'raw',
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'entryCount',
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'plotCount',
        'format' => 'raw',
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'value' => function ($model) {
            $occurrenceDbId = $model['occurrenceDbId'];
            $plotCount = $model['plotCount'];

            return '
                <span
                    id="occurrence-plot-count-' . $occurrenceDbId . '"
                >' . $plotCount . '</span>
            ';
        },
    ],
    [
        'attribute' => 'experimentSteward',
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'locationSteward',
        'order' => DynaGrid::ORDER_FIX_LEFT
    ]
];

// same as $columns, except this excludes 'order' property
$exportColumns = [
    [
        'attribute' => 'occurrenceStatus',
        'label' => 'Status',
        'format' => 'raw',
        'value' => function ($model) {
            $class = 'grey';
            $status = strtoupper(trim($model['occurrenceStatus']));
            if ($status == 'COMMITTED' || $status == 'PLANTED') {
                $class = 'green darken-3';
            } else if ($status == 'MAPPED') {
                $class = 'amber darken-3';
            }

            $statusVal = '<span title="Status: ' . $status . '" class="new badge ' . $class . '">' . $status . '</span>';
            return $statusVal;
        },
    ],
    [
        'attribute' => 'experimentCode',
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
    ],
    [
        'attribute' => 'experiment'
    ],
    [
        'attribute' => 'occurrenceName',
        'format' => 'raw',
        'label' => 'Occurrence',
    ],
    [
        'attribute' => 'siteCode',
        'label' => 'Site',
    ],
    [
        'attribute' => 'location',
    ],
    /*
    To be added later
    [
        'attribute' => 'programCode',
        'group' => true,
        'label' => 'program',
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">'.$model['experimentCode'].'</span>';
        }
    ],
    [
        'attribute' => 'project',
        'group' => true,
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">'.$model['experimentCode'].'</span>';
        }
    ],
    */
    [
        'attribute' => 'experimentType',
    ],
    [
        'attribute' => 'experimentStageCode',
        'label' => 'Stage'
    ],
    [
        'attribute' => 'experimentYear',
        'label' => 'Year'
    ],
    [
        'attribute' => 'experimentSeasonCode',
        'label' => 'Season'
    ],
    [
        'attribute' => 'occurrenceDesignType',
        'label' => 'Design',
    ],
    [
        'attribute' => 'entryCount',
    ],
    [
        'attribute' => 'plotCount',
    ],
    [
        'attribute' => 'experimentSteward',
    ],
    [
        'attribute' => 'locationSteward',
    ]
];

$browserId = 'dynagrid-em-occurrences';
$actionsDropdownTitleText = Yii::t('app', 'Download files in CSV or JSON format');
$titleText = Yii::t('app', 'Download files in CSV or JSON format');

/* Prepare Dropdown for Access Points to Linked Breeding Tools */

// Access Points Title Texts
$experimentPermissionsButtonTitleText = Yii::t('app', 'Manage experiment permissions');
$accessPointsDropdownTitleText = Yii::t('app', 'Access breeding tools');
$updateDropdownTitleText = Yii::t('app', 'Update occurrence-level data');
$harvestManagerButtonTitleText = Yii::t('app', 'Manage available plots for the harvest');
$plantingInstructionsManagerButtonTitleText = Yii::t('app', 'Manage planting instructions');
$pollinationInstructionsButtonTitleText = Yii::t('app', 'RELEASING SOON - Manage cross lists and pollinations');

// Experiment Permissions Button
$experimentPermissionsButton =  ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_MANAGE_OCC_PERMISSION', 'OCCURRENCES')) ?
    "<a
            title='$experimentPermissionsButtonTitleText'
            id='experiment-permissions-btn'
            class='btn'
            href=''
            data-beloworigin='true' 
            data-constrainwidth='false'
            style='
                margin-right: 6px;
            '
        >
            <span
                class='material-icons'
                style='
                    font-size: 18px;
                    vertical-align: -6px;
                '
            >
                share
            </span>
        </a>
    " : '';

$createAPackingJobButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_PIM_CREATE_PACKING_JOB', 'OCCURRENCES')) ?
    "<li title='Create a packing job'>
        <a
            id='create-packing-job-btn'
            class='hide-loading'
        >Create Packing Job</a>
    </li>" : '';

$plantingInstructionsManagerButton = ($createAPackingJobButton) ? // Planting Instructions Manager Access Point
    "<li
        class='hide-loading'
        title='$plantingInstructionsManagerButtonTitleText'
    >" .
        Html::a(
            '
                <span
                    class="material-icons"
                    style="
                        font-size: 20px;
                        vertical-align: -4px;
                        margin-right: 4px;
                    "
                >
                    grass
                </span>
                <p style="display: inline;">Planting Instructions Manager</p>
            ',
            null,
            [
                'data-activates' => 'planting-instructions-manager-dropdown-actions',
                'class' => 'dropdown-button dropdown-button-pjax cursor-default',
                'data-hover' => 'hover',
                'data-alignment' => 'left',
            ]
        ) .
            "<ul
                id='planting-instructions-manager-dropdown-actions'
                class='dropdown-content'
                style='
                    max-width: 136px;
                    margin-left: -136px;
                '
            >" .
                $createAPackingJobButton .
                "<li
                    title='Create a planting job'
                    style='
                        cursor: not-allowed;
                        opacity: 0.6;
                '>
                    <a
                        class='hide-loading'
                    >
                        <p style='
                            display: inline;
                            color:gray;
                        '>Create Planting Job</p>
                    </a>
                </li>
            </ul>
    </li>" : '';


$harvestPlotsButton = ($isAdmin || Yii::$app->access->renderAccess('HARVEST_OCCURRENCE', 'HARVEST_MANAGER')) ?
    "<li title='Load Plots for harvesting'>
        <a
            data-data-level='plot'
            class='harvest-manager-btn'
        >Harvest Plots</a>
    </li>" : '';


$harvestCrossesButton = ($isAdmin || Yii::$app->access->renderAccess('HARVEST_OCCURRENCE', 'HARVEST_MANAGER')) ?
    "<li title='Load Crosses for harvesting'>
        <a
            data-data-level='cross'
            class='harvest-manager-btn'
        >Harvest Crosses</a>
    </li>" : '';

$harvestManagerButton = ($harvestPlotsButton || $harvestCrossesButton) ?
    "<li
        id='harvest-manager-btn'
        class='hide-loading'
        title='$harvestManagerButtonTitleText'
    >" .
        Html::a(
            '
                <span
                    class="material-icons"
                    style="
                        font-size: 20px;
                        vertical-align: -4px;
                        margin-right: 4px;
                    "
                >
                    agriculture
                </span>
                <p style="display: inline;">Harvest Manager</p>
            ',
            null,
            [
                'class' => 'harvest-manager-btn dropdown-button dropdown-button-pjax',
                'data-activates' => 'hm-dropdown-harvest-options',
                'data-alignment' => 'left',
                'data-hover' => 'hover',
            ]
        ) .
            "<ul
                id='hm-dropdown-harvest-options'
                class='dropdown-content'
                style='
                    max-width: 112px;
                    margin-left: -113px;
                '
            >" .
                // HM Access Point for Harvesting Plots
                $harvestPlotsButton .
                // HM Access Point for Harvesting Crosses
                $harvestCrossesButton .
            "</ul>
    </li>" : '';

// Access Point Dropdown
$accessPointsDropdown = ($role == 'COLLABORATOR') ? '' :  "
    <a
        title='$accessPointsDropdownTitleText'
        id='access-points-dropdown'
        class='dropdown-button btn dropdown-button-pjax'
        href=''
        data-activates='access-points-dropdown-list'
        data-beloworigin='true'
        data-constrainwidth='false'
        style='
            margin-right: 6px;
        '
    >
        <span
            class='material-icons'
            style='
                font-size: 16px;
                vertical-align: -4px;
                margin-right: 4px;
            '
        >
            handyman
        </span> <span class='caret'></span>
    </a>
        <ul
            id='access-points-dropdown-list'
            class='dropdown-content'
            data-beloworigin='false'
        >" .
            // Planting Instructions Manager Access Point
            $plantingInstructionsManagerButton .
            // Harvest Manager Access Point
            $harvestManagerButton .
            // Phenotypic Data Manager Access Point
            "<li
                id='phenotypic-data-manager-btn'
                class='hide-loading'
                title='Go to Phenotypic Data Manager'
                style='
                    font-size: 20px;
                    vertical-align: -4px;
                    margin-right: 4px;
                '
            >" .
                Html::a(
                    '
                        <span
                            class="material-icons"
                            style="
                                font-size: 20px;
                                vertical-align: -4px;
                                margin-right: 4px;
                            "
                        >
                            insert_chart
                        </span>
                        <p style="display: inline;">Phenotypic Data Manager</p>
                    '
                ) .
            "</li>" .
            // Pollination Instructions Access Point
            /*
            Temporarily remove Pollination instructions CORB-4803
            "<li 
                id='pollination-instructions-btn'
                class='hide-loading'
                title='$pollinationInstructionsButtonTitleText'
                style='
                    cursor: not-allowed;
                    opacity: 0.6;
                '
            >" .
                Html::a(
                    '
                        <span
                            class="material-icons"
                            style="
                                font-size: 20px;
                                vertical-align: -4px;
                                margin-right: 4px;
                                color: gray;
                            "
                        >
                            emoji_nature
                        </span>
                        <p style="display: inline; color: gray;">Pollination Instructions</p>
                    '
                ) .
            "</li>" .*/
        "</ul>";

$generateEntryCodeExperimentLevelButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_GEN_ENTCODE_EXPERIMENT', 'OCCURRENCES')) ?
    "<li title='Generate entry codes at Experiment level'>
        <a
            class='generate-code-pattern-btn'
            data-entity='entry'
            data-level='experiment'
        >Experiment level</a>
    </li>"
    : '';
$generateEntryCodeOccurrenceLevelButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_GEN_ENTCODE_OCCURRENCE', 'OCCURRENCES')) ?
    "<li title='Generate entry codes at Occurrence level'>
        <a
            class='generate-code-pattern-btn'
            data-entity='entry'
            data-level='occurrence'
        >Occurrence level</a>
    </li>"
    : '';
$generatePlotCodeExperimentLevelButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_GEN_PLOTCODE_EXPERIMENT', 'OCCURRENCES')) ?
    "<li title='Generate plot codes at Experiment level'>
        <a
            class='generate-code-pattern-btn'
            data-entity='plot'
            data-level='experiment'
        >Experiment level</a>
    </li>"
    : '';
$generatePlotCodeOccurrenceLevelButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_GEN_PLOTCODE_OCCURRENCE', 'OCCURRENCES')) ?
    "<li title='Generate plot codes at Occurrence level'>
        <a
            class='generate-code-pattern-btn'
            data-entity='plot'
            data-level='occurrence'
        >Occurrence level</a>
    </li>"
    : '';

$isGenerateEntryCodeDropdownHidden = 'hidden';
$isGeneratePlotCodeDropdownHidden = 'hidden';

// Set visibility of generate entry code dropdown
if ($generateEntryCodeExperimentLevelButton || $generateEntryCodeOccurrenceLevelButton) {
    $isGenerateEntryCodeDropdownHidden = '';
}

// Set visibility of generate plot code dropdown
if ($generatePlotCodeExperimentLevelButton || $generatePlotCodeOccurrenceLevelButton) {
    $isGeneratePlotCodeDropdownHidden = '';
}

$bulkUpdateOccurrencesButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_UPDATE_OCC_PROTOCOL', 'OCCURRENCES')) ?
    "<li
        id='bulk-update-occurrences-btn'
        class='hide-loading'
        title='Update occurrence-level protocol variables'
    >
        " .
        Html::a(
            'Update occurrence protocols'
        ) . "
    </li>" : '';

$regenerateDesignButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_REGENERATE_DESIGN', 'OCCURRENCES')) ?
    "<li
        id='regenerate-design-btn'
        class='hide-loading'
        title='Regenerate occurrences design'
    >
        " . Html::a(Yii::t('app', 'Regenerate Design')) . "
    </li>" : '';

// update dropdown
$updateDropdown = (
    !$isGenerateEntryCodeDropdownHidden ||
    !$isGeneratePlotCodeDropdownHidden ||
    $bulkUpdateOccurrencesButton ||
    $regenerateDesignButton
) ?
    Html::a(
        '<i class="glyphicon glyphicon-edit"></i> <span class="caret"></span>',
        '',
        [
            'title' => Yii::t('app', 'Update Experiment- or Occurrence-level data'),
            'class' => 'dropdown-button btn dropdown-button-pjax',
            'id' => 'bulk-update-btn',
            'data-activates' => 'bulk-update-dropdown',
            'data-beloworigin' => 'true',
            'data-constrainwidth' => 'false',
        ]
    ) . "
        <ul id='bulk-update-dropdown' class='dropdown-content'>
            <li
                title='" . Yii::t('app', 'Generate entry code pattern') . "'
                class='$isGenerateEntryCodeDropdownHidden'
            >
                " .
                Html::a(
                    'Generate entry code',
                    null,
                    [
                        'class' => 'dropdown-button dropdown-button-pjax cursor-default',
                        'data-activates' => 'generate-entry-code-options',
                        'data-constrainwidth' => 'false',
                        'data-hover' => 'hover',
                    ]
                ) .
                "<ul
                    id='generate-entry-code-options'
                    class='dropdown-content'
                    style='
                        margin-left: -120px;
                    '
                >" .
                    // Entry Code Access Point for Experiment-level Code Generation
                    $generateEntryCodeExperimentLevelButton .
                    // Entry Code Access Point for Occurrence-level Code Generation
                    $generateEntryCodeOccurrenceLevelButton .
                "</ul>
            </li>
            <li
                title='" . Yii::t('app', 'Generate plot code pattern') . "'
                class='$isGeneratePlotCodeDropdownHidden'
            >
                " .
                Html::a(
                    'Generate plot code',
                    null,
                    [
                        'class' => 'dropdown-button dropdown-button-pjax cursor-default',
                        'data-activates' => 'generate-plot-code-options',
                        'data-constrainwidth' => 'false',
                        'data-hover' => 'hover',
                    ]
                ) .
                "<ul
                    id='generate-plot-code-options'
                    class='dropdown-content'
                    style='
                        margin-left: -120px;
                    '
                >" .
                    // Plot Code Access Point for Experiment-level Code Generation
                    $generatePlotCodeExperimentLevelButton .
                    // Plot Code Access Point for Occurrence-level Code Generation
                    $generatePlotCodeOccurrenceLevelButton .
                "</ul>
            </li>
            $bulkUpdateOccurrencesButton
            $regenerateDesignButton
        </ul>
    " : '';

$downloadMappingFilesCsvButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES_FORMAT', 'OCCURRENCES')) ?
    "<li
        id='download-mapping-files-btn'
        class='hide-loading'
        title='Download mapping files in CSV format'
    >" .
        Html::a(
            '<i
                class="material-icons inline-material-icon"
                style="font-size: 18px;"
            >file_download</i> Mapping files (CSV)'
        ) .
    "</li>" : '';

$downloadMappingFilesJsonButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES_FORMAT', 'OCCURRENCES')) ?
    "<li
        id='download-mapping-files-json-btn'
        class='hide-loading '".$mappingFileInJsonFormatClass."
        title='Download mapping file in JSON format'
    >" .
        Html::a(
            '<i
                class="material-icons inline-material-icon"
                style="font-size: 18px;"
            >file_download</i> Mapping file (JSON)'
        ) .
    "</li>" : '';

$downloadDataCollectionFilesOccurrenceLevelButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li>" .
        Html::a(
            'Occurrence',
            '#',
            [
                'data-activates' => 'data-collection-occurrence-dropdown-actions',
                'class' => 'dropdown-button dropdown-button-pjax export-data-collection-btn hide-loading',
                'data-hover' => 'hover',
                'data-entity' => 'occurrence',
                'title' => 'Access the Download Data Collection Files page for a single Occurrence',
            ]
        ) .
    "</li>
    <ul id='data-collection-occurrence-dropdown-actions' class='dropdown-content'>
        <li title='Download Occurrence-level trait data collection file(s) with trait protocols as additional headers'>
            <a
                href='#'
                class='export-data-collection-btn hide-loading'
                data-entity='occurrence'
                data-download-option='with trait abbrevs'
                data-should-download-multiple-files
                data-has-headers>With trait abbreviations</a></li>
        <li title='Download Occurrence-level trait data collection file(s) without trait protocols as additional headers'>
            <a
                href='#'
                class='export-data-collection-btn hide-loading'
                data-entity='occurrence'
                data-download-option='for fieldbook app'
                data-should-download-multiple-files>For FieldBook App</a></li>
    </ul>" : '';

$downloadDataCollectionFilesLocationLevelButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li>" .
        Html::a(
            'Location',
            '#',
            [
                'data-activates' => 'data-collection-location-dropdown-actions',
                'class' => 'dropdown-button dropdown-button-pjax export-data-collection-btn hide-loading',
                'data-hover' => 'hover',
                'data-entity' => 'location',
                'title' => 'Access the Download Data Collection Files page for a single Location',
            ]
        ) .
    "</li>
    <ul id='data-collection-location-dropdown-actions' class='dropdown-content'>" .
        "<li title='Download Location-level trait data collection file(s) with trait protocols as additional headers'>
            <a
                href='#'
                class='export-data-collection-btn hide-loading'
                data-entity='location'
                data-download-option='with trait abbrevs'
                data-should-download-multiple-files
                data-has-headers>With trait abbreviations</a></li>
        <li title='Download Location-level trait data collection file(s) without trait protocols as additional headers'>
            <a
                href='#'
                class='export-data-collection-btn hide-loading'
                data-entity='location'
                data-download-option='for fieldbook app'
                data-should-download-multiple-files>For FieldBook App</a></li>" .
    "</ul>" : '';

$downloadDataCollectionFilesButton = (
    $downloadDataCollectionFilesOccurrenceLevelButton ||
    $downloadDataCollectionFilesLocationLevelButton
) ?
    "<li id='download-data-collection-files-btn'>" .
        Html::a(
            '<i
                class="material-icons inline-material-icon"
                style="font-size: 18px;"
            >file_download</i> Trait data collection',
            '#',
            [
                'data-activates' => 'data-collection-dropdown-actions',
                'class' => 'cursor-default dropdown-button dropdown-button-pjax',
                'data-hover' => 'hover',
                'title' => 'Choose an option from the left',
            ]
        ) .
    "</li>" : '';

$downloadPlantingInstructionsFileOccurrenceLevelButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li
        title='Download planting instructions file for the Occurrence'
    ><a
        href='#!'
        data-entity='occurrence'
        class='export-planting-instructions-btn hide-loading'
    >Occurrence</a></li>" : '';

$downloadPlantingInstructionsFileLocationLevelButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li
        title='Download planting instructions file for the Location'
    ><a
        href='#!'
        data-entity='location'
        class='export-planting-instructions-btn hide-loading'
    >Location</a></li>" : '';

$downloadPlantingInstructionsButton = (
    $downloadPlantingInstructionsFileOccurrenceLevelButton ||
    $downloadPlantingInstructionsFileLocationLevelButton
) ?
    "<li id='download-planting-arrays-btn' title='Download Planting Instructions file'>" .
        Html::a(
            '<i class="material-icons inline-material-icon" style="font-size: 18px;">file_download</i> Planting instructions',
            '#',
            [
                'data-activates' => 'planting-instructions-dropdown-actions',
                'class' => 'cursor-default dropdown-button dropdown-button-pjax',
                'data-hover' => 'hover',
                'data-alignment' => 'left',
            ]
        ) .
    "</li>" : '';

$downloadDataCollectionSummariesTraitHeadersButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li
        title='Download data collection summaries file with Trait as column headers'
    >
        <a
            class='download-multiple-data-collection-summaries-btn hide-loading'
            data-column-header-type='trait'
        >Trait headers</a>
    </li>" : '';

$downloadDataCollectionSummariesOccurrenceNameHeadersButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li
        title='Download data collection summaries file with Occurrence as column headers'
    >
        <a
            class='download-multiple-data-collection-summaries-btn hide-loading'
            data-column-header-type='occurrence'
        >Occurrence name headers</a>
    </li>" : '';

$downloadDataCollectionSummariesButton = (
    $downloadDataCollectionSummariesTraitHeadersButton ||
    $downloadDataCollectionSummariesOccurrenceNameHeadersButton
) ?
    "<li id='download-data-collection-summaries-btn' title='Download Data Collection summaries file'>" .
        Html::a(
            '<i class="material-icons inline-material-icon" style="font-size: 18px;">file_download</i> Data collection summaries',
            '#',
            [
                'data-activates' => 'data-collection-summaries-dropdown-actions',
                'class' => 'cursor-default dropdown-button dropdown-button-pjax',
                'data-hover' => 'hover',
                'data-alignment' => 'left',
            ]
        ) .
    "</li>" : '';

$downloadOccurrenceMetadataAndPlotDataButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li id='download-occurrence-metadata-and-plot-data' title='Download occurrence metadata and plot data'>" .
        Html::a(
            '<i
                class="material-icons inline-material-icon"
                style="font-size: 18px;"
            >file_download</i>
            <p
                class="download-functionality-button"
                style="display: inline;"
            >Metadata and plot data</p>',
            '#',
            [
                'class' => '
                    download-widget-entry-point-button
                    dropdown-button-pjax
                ',
                'data-modal-header' => 'Metadata and Plot Data',
                /* The array (optional) elements below represent which category of variables
                 * should be displayed in the Download Widget modal UIs
                 */
                'data-entities' => json_encode([
                    'experiment',
                    'occurrence',
                    'entry',
                    'germplasm',
                    'plot',
                    'planting_protocol',
                    'management_protocol',
                    'trait'
                ]),
                'data-filename-abbrev' => 'METADATA_AND_PLOT_DATA',
                'data-is-in-data-browser-page' => 'true',
                'data-return-url' => 'occurrence?program=' . $program,
                'title' => \Yii::t('app', 'Download occurrence level data'),
            ]
        ) .
    "</li>" : '';

$downloadOccurrenceDataCollectionButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li id='download-occurrence-data-collection' title='Download occurrence data collection file'>" .
        Html::a(
            '<i
                class="material-icons inline-material-icon"
                style="font-size: 18px;"
            >file_download</i>
            <p
                class="download-functionality-button"
                style="display: inline;"
            >Occurrence data collection</p>',
            '#',
            [
                'class' => '
                    download-widget-entry-point-button
                    dropdown-button-pjax
                ',
                'data-modal-header' => 'Occurrence Data Collection',
                /* The array (optional) elements below represent which category of variables
                 * should be displayed in the Download Widget modal UIs
                 */
                'data-entities' => json_encode([
                    'occurrence',
                    'management_protocol'
                ]),
                'data-filename-abbrev' => 'OCCURRENCE_DATA_COLLECTION',
                'data-is-in-data-browser-page' => 'true',
                'data-return-url' => 'occurrence?program=' . $program,
                'title' => \Yii::t('app', 'Download occurrence data collection'),
            ]
        ) .
    "</li>" : '';

// actions dropdown
$actionsDropdown = (
    $downloadMappingFilesCsvButton ||
    $downloadMappingFilesJsonButton ||
    $downloadDataCollectionFilesButton ||
    $downloadPlantingInstructionsButton ||
    $downloadDataCollectionSummariesButton ||
    $downloadOccurrenceMetadataAndPlotDataButton ||
    $downloadOccurrenceDataCollectionButton
) ? "
    <a
        title='$actionsDropdownTitleText'
        id='occurrence-action'
        class='dropdown-button btn dropdown-button-pjax'
        href=''
        data-activates='manage-occurrence-dropdown-actions'
        data-beloworigin='true'
        data-constrainwidth='false'
        style='margin-left:5px'
    >
        <i class='material-icons inline-material-icon'>file_download</i> <span class='caret'></span>
    </a>

    <ul id='manage-occurrence-dropdown-actions' class='dropdown-content' data-beloworigin='false'>" .
        // Download Mapping files (CSV)
        $downloadMappingFilesCsvButton .
        // Download Mapping files (JSON)
        $downloadMappingFilesJsonButton .
        // Access Download trait data collection files page
        $downloadDataCollectionFilesButton .
        // Download Planting instructions file
        $downloadPlantingInstructionsButton .
        // Download Data collection summaries file
        $downloadDataCollectionSummariesButton .
        // Download Occurrence metadata and plot data
        $downloadOccurrenceMetadataAndPlotDataButton .
        // Download Occurrence data collection
        $downloadOccurrenceDataCollectionButton .
    "</ul>" .
    // Sub-menus for download features
    "<ul id='planting-instructions-dropdown-actions' class='dropdown-content'>" .
        $downloadPlantingInstructionsFileOccurrenceLevelButton .
        $downloadPlantingInstructionsFileLocationLevelButton .
    "</ul>
    <ul id='data-collection-dropdown-actions' class='dropdown-content'>" .
        $downloadDataCollectionFilesOccurrenceLevelButton .
        $downloadDataCollectionFilesLocationLevelButton .
    "</ul>
    <ul id='data-collection-summaries-dropdown-actions' class='dropdown-content'>" .
        $downloadDataCollectionSummariesTraitHeadersButton .
        $downloadDataCollectionSummariesOccurrenceNameHeadersButton .
    "</ul>" : '';

$uploadMappedPlotsButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_UPLOAD_MAPPED_PLOTS', 'OCCURRENCES')) ?
    '<li id="upload-planting-arrays-btn" title="Upload mapped plots">' .
        Html::a(
            '<i class="material-icons inline-material-icon" style="font-size: 18px;">file_upload</i> Mapped plots'
        ) .
    '</li>' : '';

$uploadTraitDataCollectionButton = ($isAdmin || Yii::$app->access->renderAccess('UPLOAD_DATA', 'QUALITY_CONTROL')) ?
    '<li id="upload-data-collection-files-btn" title="Upload Trait Data Collection files">' .
        Html::a(
            '<i class="material-icons inline-material-icon" style="font-size: 18px;">file_upload</i> Trait data collection'
        ) .
    '</li>' : '';

$uploadOccurrenceDataCollectionButton = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_UPLOAD_OCCURRENCE_DATA_COLLECTION', 'OCCURRENCES')) ?
'<li id="upload-occurrence-data-collection-btn" title="Upload Occurrence Data Collection file">' .
    Html::a(
        '<i class="material-icons inline-material-icon" style="font-size: 18px;">file_upload</i> Occurrence data collection'
    ) .
'</li>' : '';

$uploadPlantArrs = (
    $uploadMappedPlotsButton ||
    $uploadTraitDataCollectionButton ||
    $uploadOccurrenceDataCollectionButton
) ? '
    <a
        id="occurrence-upload-action"
        class="light-green darken-3 btn btn-success waves-effect waves-light render-upload-planting-arrays-form disabled dropdown-button btn dropdown-button-pjax"
        data-activates="manage-occurrence-dropdown-upload-actions"
        data-beloworigin="true"
        data-constrainwidth="false"
        style="margin-right:5px;"
    ">'
        . \Yii::t('app', 'Upload ') . '<span class="caret"></span>' .
    '</a>

    <ul id="manage-occurrence-dropdown-upload-actions" class="dropdown-content" data-beloworigin="false">' .
        $uploadMappedPlotsButton .
        $uploadTraitDataCollectionButton .
        $uploadOccurrenceDataCollectionButton .
        '<li id="upload-planting-instructions-btn" class="disabled" title="Upload Planting Instructions">' .
            Html::a(
                '<div style="color: #808080">
                    <i class="material-icons inline-material-icon" style="font-size: 18px; margin-right: 8px;">file_upload</i> Planting instructions
                </div>'
            ) .
        '</li>
    </ul>' : '';

$emUrl = Url::toRoute(['/occurrence?program=' . $program]);

// check if background processing is triggered
$bgProcessingNotif = '';
$params = Yii::$app->request->queryParams;

if (
    Yii::$app->session->get('em-bg-processs') !== null &&
    (
        isset($params['OccurrenceSearch']['occurrenceDbId']) ||
        isset($params['OccurrenceSearch']['experimentDbId']) ||
        isset($params['OccurrenceSearch']['location'])
    )
) {
    if (Yii::$app->session->get('em-bg-processs-message')) {
        $notifMessage = Yii::$app->session->get('em-bg-processs-message');
        Yii::$app->session->remove('em-bg-processs-message');
    } else if (isset($params['OccurrenceSearch']['occurrenceDbId'])) {
        // get status of transaction
        $bgEntityId = $params['OccurrenceSearch']['occurrenceDbId'];
        $searchParams['occurrenceDbId'] = (string) $bgEntityId;

        $occurrence = $occurrenceModel->searchAll($searchParams, 'limit=1', false);
        $occName = isset($occurrence['data'][0]['occurrenceName']) ? $occurrence['data'][0]['occurrenceName'] : '';
        $occStatus = isset($occurrence['data'][0]['occurrenceStatus']) ? $occurrence['data'][0]['occurrenceStatus'] : '';

        $notifMessage = 'You are now seeing the transaction for <b>' . $occName . '</b> is now <b>'.$occStatus.'</b>. Click the Reset Grid button to display all records.';
    }

    if (str_contains($notifMessage, 'Please try again or file a report for assistance')) {
        $bgProcessingNotif = '
            <div class="alert warning">
                <div class="card-panel" style="">
                    <i class="fa fa-warning"></i> '.
                    $notifMessage
                    .'<button data-dismiss="alert" class="close" type="button">×</button>
                </div>
            </div>
        ';
    } else {
        $bgProcessingNotif = '
            <div class="alert info">
                <div class="card-panel" style="">
                    <i class="fa fa-info"></i> '.
                    $notifMessage
                    .'<button data-dismiss="alert" class="close" type="button">×</button>
                </div>
            </div>
        ';
    }
}
// unset background process session
Yii::$app->session->remove('em-bg-processs');

echo AdvancedDataBrowserWidget::widget([
    'mode' => 'DynaGrid',
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'storage' => DynaGrid::TYPE_SESSION,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'showPageSummary' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => ('<h3>' .
                Yii::t('app', 'Experiment Manager') .
                FavoritesWidget::widget([
                    'module' => 'occurrence'
                ]) .
                '</h3>') . $bgProcessingNotif,
            'before' => \Yii::t('app', "This is the browser for created experiments. You may manage your experiments here.") .
                '<br/>
                <p id="selected-items-paragraph" class = "pull-right" style = "margin-right: 6px;">
                    <b><span id="selected-items-count"> </span></b> <span id="selected-items-text"> </span>
                </p>',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' =>
                Html::beginTag('div', ['class' => 'tex-left dropdown']) .
                    $uploadPlantArrs . ' ' .
                    Html::a('<i class="material-icons">notifications_none</i>
                        <span id="notif-badge-id"></span>',
                        '#',
                        [
                            'id' => 'notif-btn-id',
                            'class' => 'btn waves-effect waves-light em-notification-button tooltipped',
                            'data-pjax' => 0,
                            'style' => 'margin-right:5px;overflow: visible !important;',
                            'data-position' => "top",
                            'data-tooltip' => \Yii::t('app', 'Notifications'),
                            'data-activates' => 'notifications-dropdown'
                        ]
                    ).
                    $experimentPermissionsButton . 
                    $accessPointsDropdown .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        '#',
                        [
                            'data-pjax' => true,
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid'),
                            'id' => 'reset-occurrences-grid-id'
                        ]
                    ) .
                    '{dynagridFilter}{dynagridSort}{dynagrid}' .
                    Html::a(
                        '<i class="material-icons">filter_alt</i>',
                        '#',
                        [
                            'class' => 'btn btn-default button-collapse in-nav',
                            'title' => \Yii::t('app', 'Filter experiments'),
                            'data-activates' => 'occurrence-filters-panel',
                            'id' => 'filter-experiments'
                        ]
                    ) .
                    $updateDropdown .
                    $printouts .
                    $actionsDropdown .
                    Html::endTag('div')
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $browserId
    ]
]);

// upload mapped plots modal
Modal::begin([
    'id' => 'upload-planting-arrays-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">file_upload</i>
            Upload mapped plots
        </h4>',
    'footer' =>
    Html::a('Cancel', [''], [
        'id' => 'cancel-upload-planting-array',
        'class' => 'modal-close-button',
        'title' => \Yii::t('app', 'Cancel'),
        'data-dismiss' => 'modal'
    ]) . '&emsp;&nbsp;'
        . Button::widget([
            'label' => 'Confirm',
            'options' => [
                'class' => 'confirm-upload-planting-arrays hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Confirm upload mapped plots')
            ],
        ])
        . Button::widget([
            'label' => 'Next',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'next-upload-planting-arrays hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Go to preview')
            ],
        ]). Button::widget([
            'label' => 'Validate',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'validate-upload-planting-arrays hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Validate and go to preview')
            ],
        ]),
    'closeButton' => [
        'class' => 'hidden'
    ],
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin'
    ],
]);
echo '<div class="upload-planting-arrays-form-modal-body parent-panel"></div>';
echo '<div class="upload-planting-arrays-preview-modal-body child-panel"></div>';
Modal::end();

// upload occurrence data collection modal
Modal::begin([
    'id' => 'upload-occurrence-data-collection-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">file_upload</i>
            Upload occurrence data collection
        </h4>',
    'footer' =>
    Html::a('Cancel', [''], [
        'id' => 'cancel-upload-occurrence-data-collection',
        'class' => 'modal-close-button',
        'title' => \Yii::t('app', 'Cancel'),
        'data-dismiss' => 'modal'
    ]) . '&emsp;&nbsp;'
        . Button::widget([
            'label' => 'Confirm',
            'options' => [
                'class' => 'confirm-upload-occurrence-data-collection hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Confirm upload occurrence data')
            ],
        ])
        . Button::widget([
            'label' => 'Next',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'next-upload-occurrence-data-collection hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Go to preview')
            ],
        ]). Button::widget([
            'label' => 'Validate',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'validate-upload-occurrence-data-collection hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Validate and go to preview')
            ],
        ]),
    'closeButton' => [
        'class' => 'hidden'
    ],
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin'
    ],
]);
echo '<div class="upload-occurrence-data-collection-form-modal-body parent-panel"></div>';
echo '<div class="upload-occurrence-data-collection-preview-modal-body child-panel"></div>';
Modal::end();

// manage basic info modal
Modal::begin([
    'id' => 'manage-basic-info-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">edit</i>
            Manage basic info for 
            <span class="record-label-modal"></span>
        </h4>',
    'footer' =>
    Html::a('Cancel', ['#'], [
        'id' => 'cancel-save',
        'title' => \Yii::t('app', 'Cancel'),
        'data-dismiss' => 'modal'
    ]) . '&emsp;&nbsp;' .
        Button::widget([
            'label' => 'Save',
            'icon' => [
                'name' => 'send',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'browser-detail-view-save-basic-info',
                'href' => '#!',
                'title' => \Yii::t('app', 'Save basic information')
            ],
        ])
        . '&emsp;&emsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin'
    ],
]);
echo '<div class="manage-basic-info-modal-body browser-detail-view-generic-form"></div>';
Modal::end();

// commit occurrence modal
Modal::begin([
    'id' => 'commit-occurrence-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">check</i>
            Commit  
            <span class="commit-record-label-modal"></span>
        </h4>',
    'footer' =>
    Html::a('Cancel', ['#'], [
        'id' => 'cancel-save',
        'title' => \Yii::t('app', 'Cancel'),
        'data-dismiss' => 'modal'
    ]) . '&emsp;&nbsp;' .
        Button::widget([
            'label' => 'Confirm',
            'options' => [
                'class' => 'commit-occurrence-confirm-btn',
                'href' => '#!',
                'title' => \Yii::t('app', 'Confirm commit occurrence')
            ],
        ])
        . '&emsp;&emsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    'options' => [
        'data-backdrop' => 'static'
    ],
]);

?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<div class="commit-occurrence-modal-body">
    <div class="commit-confirmation-panel"><i class="fa fa-warning orange-text text-darken-3 fa-2x"></i>
        You are about to commit the occurrence. Upon confirmation, the occurrence cannot be modified anymore and the location will be operational. Click confirm to proceed.
    </div>
    <div class="commit-confirmation-loading-panel hidden">
        <div class="margin-right-big loading">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
        </div>
    </div>
</div>
<?php
Modal::end();

// generate location modal
Modal::begin([
    'id' => 'generate-location-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">add_location</i>
            Generate location for   
            <span class="generate-location-record-label-modal"></span>
        </h4>',
    'footer' =>
    Html::a('Cancel', ['#'], [
        'id' => 'cancel-generate-location',
        'title' => \Yii::t('app', 'Cancel'),
        'data-dismiss' => 'modal'
    ]) . '&nbsp;&emsp;'
        . Button::widget([
            'label' => 'Confirm',
            'options' => [
                'class' => 'confirm-generate-location hidden',
                'href' => '#!',
                'style' => 'margin-right:10px',
                'title' => \Yii::t('app', 'Confirm generate location')
            ],
        ]) .
        Button::widget([
            'label' => 'Next',
            'options' => [
                'class' => 'go-to-preview-generate-location-btn',
                'href' => '#!',
                'title' => \Yii::t('app', 'Preview generate location')
            ],
        ])
        . '&emsp;&emsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static'
    ],
]);

?>

<div class="generate-location-modal-body parent-panel"></div>
<div class="generate-location-preview-modal-body child-panel"></div>

<?php

Modal::end();

// create-packing job modal
Modal::begin([
    'id' => 'create-packing-job-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">check</i>
            Create Packing Job  
            <span class="commit-record-label-modal"></span>
        </h4>',
    'footer' =>
    Html::a('Cancel', ['#'], [
        'id' => 'cancel-save',
        'title' => \Yii::t('app', 'Cancel'),
        'data-dismiss' => 'modal'
    ]) . '&emsp;&nbsp;' .
        Button::widget([
            'label' => 'Confirm',
            'options' => [
                'class' => 'confirm-create-packing-job-btn',
                'href' => '#!',
                'title' => \Yii::t('app', 'Confirm create Packing job')
            ],
        ])
        . '&emsp;&emsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    'options' => [
        'data-backdrop' => 'static'
    ],
]);

?>

<div class="create-packing-job-modal-body">
    <div class="create-packing-job-panel"><i class="fa fa-warning orange-text text-darken-3 fa-2x"></i>
        You are about to create Packing job for <b id="occurrence-count-label-modal"></b>. This action cannot be undone. Click confirm to proceed.
    </div>
</div>
<?php
Modal::end();

//delete occurrence modal
Modal::begin([
    'id' => 'occurrence-delete-modal',
    'header' => '<h4 id="delete-occurrence-label"></h4>',
    'footer' =>
        // Temporarily remove confirmation button
        Html::a(\Yii::t('app','Cancel'),['#'],['data-dismiss'=>'modal']).'&nbsp;&nbsp'.
        Html::a(\Yii::t('app','Confirm'),['#'],['class'=>'btn btn-primary waves-effect waves-light delete-occurrence-btn','url'=>'#','id'=>'delete-btn']).'&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo \Yii::t('app', '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete <span id="occurrence-delete"></span>. This action will delete all records within the occurrence. Click confirm to proceed.');
echo '<div class = "row"><div id="mod-progress" class="hidden"></div><div id="mod-notif" class="hidden"></div></div>';

Modal::end();

// render query params
echo Yii::$app->controller->renderPartial('filters.php', compact('dashboardFilters','occurrenceTextFilters', 'programId'));

$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');

// render form upload mapped plots url
$renderFormUploadPlantingArraysUrl = Url::to(['/occurrence/upload/render-upload-planting-arrays-form']);
// render upload occurrence data collection url
$renderUploadOccurrenceDataCollectionUrl = Url::to(['/occurrence/upload/render-upload-occurrence-data-collection']);
// reset grid url
$resetGridUrl = Url::toRoute(['/occurrence?program=' . $program]);
// export mapping files url
$exportMappingFilesUrl = Url::to(['/occurrence/default/export-mapping-files']);
// render generic web form in modal
$renderGenericFormUrl = Url::to(['/browser/render-generic-form']);
// commit occurrence url
$commitOccurrenceUrl = Url::to(['/occurrence/manage/commit-occurrence']);
// export planting instructions file url
$exportPlantInsUrl = Url::to(['/occurrence/default/export-planting-instructions']);
// retrieve new notifications count
$newNotificationsUrl = Url::toRoute(['/occurrence/default/new-notifications-count']);
// retrieve notifications url
$notificationsUrl = Url::toRoute(['/occurrence/default/push-notifications']);
// retrieve location DB ID url
$retrieveLocationDbIdUrl = Url::to(['/occurrence/default/retrieve-location-db-id']);
// retrieve BG process task type
$getBgProcessTaskTypeUrl = Url::to(['/occurrence/default/get-bg-process-details']);
// determine Harvest Manager accessing method
$determineHarvestManagerAccessingMethodUrl = Url::to(['/occurrence/default/determine-harvest-manager-accessing-method']);
// create Packing Job transaction
$storeOccurrenceIdsUrl = Url::to(['/plantingInstruction/default/store-occurrence-ids']);
// redirect to Update Occurrence Protocols page
$redirectToUpdateOccurrencesUrl = Url::to(['/occurrence/manage/occurrence-protocols?program=' . $program]);
// redirect to Generate Code Pattern page
$redirectToGenerateCodePatternUrl = Url::to(['/occurrence/manage/code-patterns?program=' . $program]);
// retrieve occurrence DB IDs URL
$retrieveDbIdsUrl = Url::to(['default/retrieve-db-ids']);
// retrieve Valid and Invalid Occurrences
$retrieveValidAndInvalidOccurrencesUrl = Url::to(['default/retrieve-valid-and-invalid-occurrences']);
// retrieve Experiment DB IDs
$retrieveExperimentDbIdsUrl = Url::to(['default/retrieve-experiment-db-ids']);
// retrieve Data Process Abbrevs
$retrieveDataProcessAbbrevsUrl = Url::to(['default/retrieve-data-process-abbrevs']);
// export data collection summaries url
$exportDataCollectionSummariesUrl = Url::to(['/occurrence/default/export-data-collection-summaries']);
// export trait data collection files url
$exportDataCollectionFilesUrl = Url::to(['default/export-data-collection-files', 'selectedDbIds' => '', 'entity' => '']);
// export other data collection files url
$exportOtherDataCollectionFilesUrl = Url::to(['default/export-other-data-collection-files', 'occurrenceDbId' => '', 'dataCollectionFileName' => '', 'plotCount' => '']);
// set session variables
$setSessionVariablesUrl = Url::to(["manage/set-session-variables"]);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
// retrieve saved trait protocol
$getTraitProtocolsUrl = Url::to(['/occurrence/default/get-trait-protocols']);

// delete occurrence url
$deleteUrl = Url::to(['/occurrence/manage/delete-created-occurrence?program=' . $program]);

// check occurrence write permission URL
$checkOccurrenceWritePermissionUrl = Url::to(['/occurrence/default/check-write-permission?program=' . $program]);

// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

$redirectToUpdateDesignUrl = Url::to(['design/generate-design?program=' . $program]);
$this->registerCss('
    .modal-body {
        overflow-x: hidden;
    }
');

// browser js variables for generic detail view browser; do not change variable names
$this->registerJs(
    <<<JS
    var renderGenericFormUrl = '$renderGenericFormUrl';
    var program = '$program';
JS,
    \yii\web\View::POS_HEAD
);

// Get current page size
$pageSize = Yii::$app->session->get('occurrence-search-occurrences-session-vars')[1];
// Get number value of page size
$pageSize = substr($pageSize, 6);

$this->registerJs(<<<JS

var resetGridUrl = '$resetGridUrl';
var gridId = '$browserId';
var program = '$program';
var programId = '$programId';
var newNotificationsUrl = '$newNotificationsUrl';
var notificationsUrl = '$notificationsUrl';
let redirectToUpdateOccurrencesUrl = '$redirectToUpdateOccurrencesUrl'
let redirectToGenerateCodePatternUrl = '$redirectToGenerateCodePatternUrl'
let retrieveValidAndInvalidOccurrencesUrl = '$retrieveValidAndInvalidOccurrencesUrl'
let retrieveExperimentDbIdsUrl = '$retrieveExperimentDbIdsUrl'
let retrieveDataProcessAbbrevsUrl = '$retrieveDataProcessAbbrevsUrl'
let setSessionVariablesUrl = '$setSessionVariablesUrl'
let exportDataCollectionFilesUrl = '$exportDataCollectionFilesUrl'
let exportOtherDataCollectionFilesUrl = '$exportOtherDataCollectionFilesUrl'
let redirectToUpdateDesignUrl = '$redirectToUpdateDesignUrl'
let checkOccurrenceWritePermissionUrl = '$checkOccurrenceWritePermissionUrl'
let noAction = true;
let validOccurrences = [];
const notificationsString = '$notificationsString'
const earlierString = '$earlierString'
const loadingIndicatorHtml = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>'
let retrieveNotificationsAjax = null

let checkboxSessionStorageName = 'occurrenceCheckboxIds'
let totalCheckboxesSessionStorageName = 'occurrenceTotalCheckboxIds'
let retrieveOccurrenceDbIdsAjax = null
let isBGProcessing = false
const selectOccurrenceThreshold = parseInt($selectOccurrenceThreshold)
const pageSize = parseInt($pageSize)
const thresholdValueForExportCsvMappingFiles = parseInt($thresholdValueForExportCsvMappingFiles)

sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify([]))
sessionStorage.setItem(totalCheckboxesSessionStorageName, selectOccurrenceThreshold)

let checkedBoxes = JSON.parse(sessionStorage.getItem(checkboxSessionStorageName))
let totalCheckboxes = parseInt(sessionStorage.getItem(totalCheckboxesSessionStorageName))
let isSelectAllChecked = null

const personId = '$personId'
const isAdmin = '$isAdmin'

let setDefaultPageSizeUrl = '$setDefaultPageSizeUrl'
let browserIds = ['dynagrid-em-occurrences']
let currentDefaultPageSize = '$currentDefaultPageSize'
let selectedOccurrenceId = null

$(document).ready(function () {
    refresh()
    notifDropdown()
    renderNotifications()

    // Remove loading indicator after timeout
    setTimeout(function() {
        $('#system-loading-indicator').css('display','none');
    },3000);
    
    setTimeout(function(){
        notificationInterval=setInterval(updateNotif, 5000)
    }, 5000)

    // Unselect all checkboxes
    $('input:checkbox.occurrence-grid-select').each(function getTickedCheckboxes () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', false)
        }
    })

    adjustBrowserPageSize()
})

// reload grid on pjax
$(document).on('ready pjax:success', function(e) {
    // remove loading indicator upon export
    $('*[class^="export-full-"]').on('click',function(){
        $('#system-loading-indicator').css('display','none');
    });

    // Remove loading indicator when timeout
    setTimeout(function() {
        $('#system-loading-indicator').css('display','none');
    },3000);

    e.stopPropagation();
    e.preventDefault();
    refresh();

    // display notifications
    $('#notif-btn-id').tooltip();
    notifDropdown();
    $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>');
    renderNotifications();
    updateNotif();
});

function refresh(){
    var maxHeight = ($(window).height() - 258);
    $(".kv-grid-wrapper").css("height",maxHeight);
    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
    $('.dropdown-button').dropdown();
    $('.button-collapse').sideNav();
    $('.tooltipped').tooltip();

    compareCheckBoxCountWithTotalCount()

    // auto-tick tracked checkedboxes
    if (checkedBoxes.length > 0) {
        const occurrenceIds = getOccurrenceDbIds()

        $('input:checkbox.occurrence-grid-select').each(function () {
            if (occurrenceIds.includes($(this).attr('id'))) {
                $(this).prop("checked", true)
            }
        })
    }

    // auto-tick Select All checkbox
    if (isSelectAllChecked) {
        $('#checkbox-select-all').prop('checked', true)
    } else {
        $('#checkbox-select-all').prop('checked', false)
    }

    $('.render-upload-planting-arrays-form').removeClass('disabled');

    // display and hide loading indicator
    $(document).on('click','.hide-loading',function(){
        $('#system-loading-indicator').css('display','block');
        setTimeout(function() {
            $('#system-loading-indicator').css('display','none');
        },3000);
    });

    $('#reset-occurrences-grid-id').on('click', function(e) {
        e.preventDefault();
        $.pjax.reload({
            container: '#' + gridId + '-pjax',
            replace: true,
            url: resetGridUrl
        });
    });

    $(document).on('click','.occurrence-grid-select',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
    });

    // render upload mapped plots web form in modal
    $(document).on('click', '#upload-planting-arrays-btn', function(e){

        e.preventDefault();
        e.stopImmediatePropagation();

        var modalBody = '.upload-planting-arrays-form-modal-body';
        $(modalBody).html(loading);

        $.ajax({
            url: '$renderFormUploadPlantingArraysUrl',
            type: 'post',
            dataType: 'json',
            data:{
            },
            success: function(response){
                $(modalBody).html(response);
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                $(modalBody).html(errorMessage);
            }
        });
    });

    // render upload occurrence data collection in modal
    $('#upload-occurrence-data-collection-btn').on('click', function (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        // trigger action ongoing
        noAction = false
        $('#upload-occurrence-data-collection-modal').modal('show')

        var modalBody = '.upload-occurrence-data-collection-form-modal-body'
        $(modalBody).html(loading)

        $.ajax({
            url: '$renderUploadOccurrenceDataCollectionUrl',
            type: 'post',
            dataType: 'json',
            data:{
            },
            success: function (response) {
                $(modalBody).html(response)
            },
            error: function () {
                var errorMessage = '<i>There was a problem loading the content.</i>'
                $(modalBody).html(errorMessage)
            }
        })
    })

    //redirect view to design tab
    // render generate location form
    $(document).on('click', '.update-design-btn', function(e){
        let obj = $(this);
        var experimentId = obj.attr('data-experimentdbid');
        var occurrenceDbIds = obj.attr('data-occurrencedbids');
        var program = obj.attr('data-program');
        var regenerate = obj.attr('data-regenerate');

        // Redirect user to design tab
        window.location.href = window.location.origin +
            '/index.php/occurrence/design/generate-design?program=' + '$program' +
            '&experimentDbId=' + experimentId + 
            '&occurrenceDbIds=' + occurrenceDbIds +
            '&regenerate=true'
    });

    // render generate location form
    $(document).on('click', '.generate-location-btn', function(e){

        noAction = false;

        var modalBody = '.generate-location-modal-body';
        $(modalBody).html(loading);

        let obj = $(this);
        var id = obj.attr('data-id');
        var name = obj.attr('data-name');

        // make the name print safe
        var safe = name.replace(/</g,"&lt;").replace(/>/g,"&gt;");
        // add it to the modal header as text
        $('.generate-location-record-label-modal').html(safe);

        // $('.go-to-preview-generate-location-btn').attr('data-id',id);

        var els = document.querySelectorAll('.go-to-preview-generate-location-btn');
        for (var i=0; i < els.length; i++) {
            els[i].setAttribute("data-id", id);
        }

        $.ajax({
            url: '$renderFormUploadPlantingArraysUrl',
            type: 'post',
            dataType: 'json',
            data:{
                action: 'generate-location'
            },
            success: function(response){
                $(modalBody).html(response);
                $('.go-to-preview-generate-location-btn').removeClass('disabled');
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                $(modalBody).html(errorMessage);
                $('.go-to-preview-generate-location-btn').addClass('disabled');
            }
        });

        e.preventDefault();
        e.stopImmediatePropagation();

    });

    // cancel upload mapped plots and generate location, reset modal centent
    $(document).on('click', '#cancel-generate-location, #cancel-upload-planting-array', function(){
        $('.generate-location-modal-body').html('');
        $('.upload-planting-arrays-form-modal-body').html('');
    });

    // download mapping file in each row
    $('.download-mapping-file-btn').on('click', function (e) {
        let obj = $(this)
        let id = obj.data('id')
        let type = obj.data('type')
        let exportMappingFilesUrl = '$exportMappingFilesUrl'
        const plotCount = $('#occurrence-plot-count-' + id).text()

        if (plotCount >= 20000) {
            // Display loading indicator
            $('#system-loading-indicator').html('')

            $('.toast').css('display','none')
            let notif = `
                <i class='material-icons red-text left'>close</i>
                The total Plot count exceeds 20,000. Please refrain from downloading Mapping Files that exceed the Plot count limit.
            `
            Materialize.toast(notif,5000)

            return
        }

        let expMappingFilesUrl =
            window.location.origin +
            exportMappingFilesUrl +
            '?id=' + id +
            '&type=' + type +
            '&program=' + program +
            '&redirectUrl=' + encodeURIComponent(window.location.href) +
            '&plotCount=' + plotCount

        window.location.assign(expMappingFilesUrl)
    });

    // export planting instructions
    $('.export-planting-instructions-btn').on('click', function (e) {
        let ids = []
        let selected = false
        let exportPlantInsUrl = '$exportPlantInsUrl'
        let obj = $(this);
        let entity = obj.data('entity');

        $("input:checkbox.occurrence-grid-select").each(function () {
            if ($(this).prop("checked") === true) {
                ids.push($(this).attr("id"))
                selected = true
            }
        })

        if (!selected) {
            $('.toast').css('display','none')
            let notif = "<i class='material-icons orange-text left'>warning</i> Please select one (1) Occurrence."
            Materialize.toast(notif,5000)
        } else if (ids.length > 1) {
            $('.toast').css('display','none')
            let notif = "<i class='material-icons orange-text left'>warning</i> Please select only one (1) Occurrence."
            Materialize.toast(notif,5000)
        } else {
            let homeUrl = window.location.href
            let plantInsUrl =
                window.location.origin +
                exportPlantInsUrl +
                '?id=' + ids[0] +
                '&type=csv' +
                '&entity='+entity +
                '&program='+program +
                '&redirectUrl='+encodeURIComponent(homeUrl) +
                '&retrieveExperimentData=true'

            window.location.replace(plantInsUrl)
        }

    });

    // via dropdown (at download icon at the top-right of occurrence browser)
    $('#download-mapping-files-btn').on('click', async function (e) {
        let ids = getOccurrenceDbIds()
        let exportMappingFilesUrl = '$exportMappingFilesUrl'
        let plotCount = 0

        ids.forEach(id => {
            plotCount += parseInt($('#occurrence-plot-count-' + id).text())
        })

        if (!ids.length) {
            $('.toast').css('display','none')
            let notif = `
                <i class='material-icons orange-text left'>warning</i>
                Please select at least one (1) Occurrence.
            `
            Materialize.toast(notif,5000)
        } else if (ids.length > 1) {
            $('.toast').css('display','none')
            let notif = `
                <i class='material-icons blue-text left'>info</i>
                Please wait for the CSV exporting to complete.
            `
            Materialize.toast(notif,5000)

            ids = ids.toString().replace(/,/g, '-')

            let expMappingFilesUrl =
                window.location.origin +
                exportMappingFilesUrl +
                '?id=' + ids +
                '&type=csv' +
                '&program=' + program +
                '&redirectUrl=' + encodeURIComponent(window.location.href) +
                '&plotCount=' + plotCount

            window.location.assign(expMappingFilesUrl)
        } else {
            if (plotCount >= 20000) {
                // Display loading indicator
                $('#system-loading-indicator').html('')

                $('.toast').css('display','none')
                let notif = `
                    <i class='material-icons red-text left'>close</i>
                    The total Plot count exceeds 20,000. Please refrain from downloading Mapping Files that exceed the Plot count limit.
                `
                Materialize.toast(notif,5000)

                return
            }

            let expMappingFilesUrl =
                window.location.origin +
                exportMappingFilesUrl +
                '?id=' + ids[0] +
                '&type=csv' +
                '&program=' + program +
                '&redirectUrl=' + encodeURIComponent(window.location.href) +
                '&plotCount=' + plotCount

            window.location.assign(expMappingFilesUrl)
        }
    });

    // Export mapping file in JSON format...
    // ...via dropdown (at download icon at the top-right of data browser)
    $('#download-mapping-files-json-btn').on('click', function (e)
    {
        let ids = getOccurrenceDbIds()
        let exportMappingFilesUrl = '$exportMappingFilesUrl'

        ids = ids.toString().replace(/,/g, '-')

        if (!ids.length) {
            $('.toast').css('display','none')
            let notif = "<i class='material-icons orange-text left'>warning</i> Please select at least one (1) Occurrence."
            Materialize.toast(notif,5000)
            $('#system-loading-indicator').css('display','block')
        } else {
            let homeUrl = window.location.href
            let expMappingFilesUrl =
                window.location.origin +
                exportMappingFilesUrl +
                '?id=' + ids +
                '&type=json' +
                '&program='+program +
                '&redirectUrl='+encodeURIComponent(homeUrl)

            window.location.assign(expMappingFilesUrl)
        }
    });

    // render occurrence name in commit modal
    $(document).on('click','.commit-occurrence-btn', function(){
        var obj = $(this);
        var id = obj.attr('data-id');
        var name = obj.attr('data-name');

        // make the name print safe
        var safe = name.replace(/</g,"&lt;").replace(/>/g,"&gt;");
        // add it to the modal header as text
        $('.commit-record-label-modal').html(safe);

        $('.commit-occurrence-confirm-btn').attr('data-id', id);
        $('.commit-occurrence-confirm-btn').attr('data-name', name);
    });

    // commit the occurrence
    $(document).on('click','.commit-occurrence-confirm-btn', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var obj = $(this);
        var id = obj.attr('data-id');
        var name = obj.attr('data-name');
        // make the name print safe
        var safe = name.replace(/</g,"&lt;").replace(/>/g,"&gt;");

        // show loading indicator
        $('.commit-confirmation-loading-panel').removeClass('hidden');
        $('.commit-confirmation-panel').addClass('hidden');

        //get selected record ID
        var searchParams = new URLSearchParams(window.location.search);
        var selectedRecordId = searchParams.get('selectedRecordId');

        let modalBody = '.commit-occurrence-modal-body'

        setTimeout(function(){
            $.ajax({
                url: '$commitOccurrenceUrl',
                type: 'post',
                dataType: 'json',
                data:{
                    id: id
                },
                success: function(response){
                    // remove initial toast
                    $('.toast').css('display','none');
                    
                    let color, icon, message = ''
                    if (!response.success) {
                        color = 'red'
                        icon = 'close'
                        message = 'There seems to be a problem while committing occurrence. '
                    } else {
                        color = "green";
                        icon = "check";
                        message = "Successfully committed "+ safe +".";
                    }

                    // display notification
                    var notif = "<i class='material-icons "+ color +"-text left'>"+ icon +"</i> <span class='white-text'>"+ message +"</span>";
                    Materialize.toast(notif, 5000);
                    
                    $('#commit-occurrence-modal').modal('hide');
                    
                    // refresh gird view and view record to reflect changes
                    $('#browser-detail-view-search-input').val('');
                    $('.browser-detail-view-submit-btn').trigger('click');
                    
                    // refresh content of commit confirmation modal
                    $('.commit-confirmation-loading-panel').addClass('hidden');
                    $('.commit-confirmation-panel').removeClass('hidden');

                    // refresh browser to reflect changes
                    $.pjax.reload({
                        container: '#' + gridId + '-pjax',
                        url: '/index.php/occurrence?program='+ program
                    });
                },
                error: function(){
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBody).html(errorMessage);
                }
            });
        }, 50);
    });

    // Access Download trait data collection files via dropdown
    $('.export-data-collection-btn').on('click', function (e) {
        const obj = $(this)
        const entity = obj.data('entity')
        const downloadOption = obj.data('download-option')

        // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both
        const shouldDownloadMultipleFiles = (
            typeof obj.data('should-download-multiple-files') !== typeof undefined &&
            obj.data('should-download-multiple-files') !== false
        ) ? true : false

        const hasHeaders = (typeof obj.data('has-headers') !== typeof undefined && obj.data('has-headers') !== false) ? '1' : ''
        // let ids = []
        let ids = new Set()
        const selectedOccurrences = JSON.parse(sessionStorage.getItem('occurrenceCheckboxIds'))

        // Determine if there are ticked checkboxes
        selectedOccurrences.forEach(selectedOccurrence => {
            ids.add(selectedOccurrence['id'])
        })
        
        // Convert Set to Array
        ids = [ ...ids ]

        if (shouldDownloadMultipleFiles) {
            if (ids.length === 0) {
                $('.toast').css('display','none')
                let notif = "<i class='material-icons orange-text left'>warning</i>Please select at least one (1) Occurrence."
                Materialize.toast(notif,5000)
            
                return
            }

            let selectedDbIds = {}
            const entityDbIdString = (entity === 'occurrence') ? 'id' : `\${entity}DbId`

            // Display loading indicator
            $('#system-loading-indicator').html(loadingIndicatorHtml)

            // Load Occurrence or Location IDs, depending on entity
            for (const selectedOccurrence of selectedOccurrences) {
                if (entity === 'location' && !selectedOccurrence[entityDbIdString]) {
                    $('.toast').css('display','none')
                    let notif = "<i class='material-icons orange-text left'>warning</i> Please select Occurrences that are mapped to a Location."
                    Materialize.toast(notif,5000)
                
                    // Hide loading indicator
                    $('#system-loading-indicator').html('')
                    return
                }

                if (entity === 'occurrence') {
                    selectedDbIds[selectedOccurrence['id']] = {
                        'experimentCode': selectedOccurrence['experimentCode'],
                        'occurrenceNumber': selectedOccurrence['occurrenceNumber'],
                        'locationDbId': selectedOccurrence['locationDbId'],
                    }
                } else if (entity === 'location') {
                    selectedDbIds[selectedOccurrence['locationDbId']] = {
                        'locationCode': selectedOccurrence['locationCode']
                    }
                }
            }

            // Store the selected occurrenceIds in an array
            var occurrenceIds = [];
            ids.forEach(function(occurrenceId) {
                occurrenceIds.push(occurrenceId);
            });

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '$getTraitProtocolsUrl',
                data: {
                    occurrenceId: occurrenceIds,
                },
                async: false,
                success: function (response) {
                    let message = ''
                    let textColor = 'blue'
                    let icon = 'info'
                    let hasZeroCount = response.hasZeroCount;
                    let listIdCount = response.listIdCount;

                    if (
                        downloadOption == 'with trait abbrevs' &&
                        hasZeroCount &&
                        listIdCount > 1
                    ) {
                        message = 'Some of the selected occurrences currently have no saved traits.';
                        $('.toast').css('display','none')
                        let notif = `<i class='material-icons \${textColor}-text left'>\${icon}</i><span>\${message}</span>`
                        Materialize.toast(notif, 5000)
                    } else if (
                        downloadOption == 'with trait abbrevs' &&
                        hasZeroCount &&
                        listIdCount == 1
                    ) {
                        message = 'The selected occurrence currently has no saved traits.';
                        $('.toast').css('display','none')
                        let notif = `<i class='material-icons \${textColor}-text left'>\${icon}</i><span>\${message}</span>`
                        Materialize.toast(notif, 5000)
                    } else {
                        $.ajax({
                            url: '$exportDataCollectionFilesUrl',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                selectedDbIds,
                                entity,
                                hasHeaders
                            },
                            success: function (response) {
                                let message = ''
                                let textColor = 'blue'
                                let icon = 'info'

                                if (!response.success) {
                                    textColor = 'red'
                                    icon = 'close'
                                    message = `Error: \${response.error.message}`
                                }
                                else {
                                    message = 'Kindly wait for Data Collection file(s) to be generated.<br>You will be notified in this page once done.'
                                }
                                $('.toast').css('display','none')
                                let notif = `<i class='material-icons \${textColor}-text left'>\${icon}</i><span>\${message}</span>`
                                Materialize.toast(notif, 5000)
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $('.toast').css('display','none')
                                let notif = `<i class='material-icons red-text left'>close</i><span>An error has been encountered: <b>\${textStatus}</b> - \${errorThrown}</span>`
                                Materialize.toast(notif, 5000)
                            }
                        })
                    }
                },
                error: function (data) {
                }
            })
        } else {
            if (ids.length === 0 || ids.length > 1) {
                $('.toast').css('display','none')
                let notif = "<i class='material-icons orange-text left'>warning</i> Please select only one (1) Occurrence."
                Materialize.toast(notif,5000)
            
                // Hide loading indicator
                $('#system-loading-indicator').html('')
                return
            }

            // Display loading indicator
            $('#system-loading-indicator').html(loadingIndicatorHtml)
            const occurrenceStatus = $('#occurrence-status-' + ids[0])[0].innerText
            
            if (
                entity === 'location' &&
                occurrenceStatus !== 'PLANTED' &&
                occurrenceStatus !== 'MAPPED'
            ) {
                $('.toast').css('display','none')
                let notif = "<i class='material-icons red-text left'>close</i> The Occurrence and/or Location is not yet ready for data collection."
                Materialize.toast(notif,5000)
            } else {
                $.ajax({
                    url: '$retrieveLocationDbIdUrl',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        occurrenceDbId: ids[0]
                    },
                    success: function (locationDbId) {
                        if (!locationDbId && !ids[0]) {
                            $('.toast').css('display','none')
                            let notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem with proceeding this Occurrence for data collection."
                            Materialize.toast(notif,5000)

                            return
                        }

                        if (entity == 'occurrence') {
                            if (!locationDbId) locationDbId = ''

                            // Download Occurrence for Field Book
                            window.location.href =
                                window.location.origin +
                                '/index.php/dataCollection/export-workbook/index?program=' + '$program' +
                                '&locationId=' + locationDbId + '&occurrenceId=' + ids[0] +
                                '&source=em-occ'                       
                        } else if (entity == 'location') {
                            // Download Location for Field Book
                            window.location.href =
                                window.location.origin +
                                '/index.php/dataCollection/export-workbook/index?program=' + '$program' +
                                '&locationId=' + locationDbId +
                                '&source=em-loc'
                        }
                    },
                    error: function (response) {}
                })
            }
        }

        // Hide loading indicator
        $('#system-loading-indicator').html('')
    })

    // Download Other Data Collection Files
    $('.data-collection-other-files-btn').on('click', function (e) {
        const obj = $(this)
        const occurrenceDbId = obj.data('occurrence-id')
        const dataCollectionFileName = obj.data('file-name')
        const plotCount = obj.data('plot-count')

        if (plotCount >= thresholdValueForExportCsvMappingFiles) {
            message = `Kindly wait for the <b>\${dataCollectionFileName}</b> file to be generated.<br>You will be notified in this page once done.`

            $('.toast').css('display','none')
            let notif = `<i class='material-icons blue-text left'>info</i><span>\${message}</span>`
            Materialize.toast(notif, 5000)

            $.ajax({
                url: exportOtherDataCollectionFilesUrl,
                type: 'post',
                dataType: 'json',
                data: {
                    occurrenceDbId,
                    dataCollectionFileName,
                    plotCount
                },
                success: function (response) {
                    if (response.hasOwnProperty('success') && !response.success) {
                        let message = `Error: \${response.error.message}`

                        $('.toast').css('display','none')
                        let notif = `<i class='material-icons red-text left'>close</i><span>\${message}</span>`
                        Materialize.toast(notif, 5000)
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.toast').css('display','none')
                    let notif = `<i class='material-icons red-text left'>close</i><span>An error has been encountered: <b>\${textStatus}</b> - \${errorThrown}</span>`
                    Materialize.toast(notif, 5000)
                }
            })
        } else {
            // Display loading indicator
            $('#system-loading-indicator').html(loadingIndicatorHtml)

            message = `Kindly wait for the <b>\${dataCollectionFileName}</b> file to be generated.<br>The file will be automatically downloaded once done.`

            $('.toast').css('display','none')
            let notif = `<i class='material-icons blue-text left'>info</i><span>\${message}</span>`
            Materialize.toast(notif, 5000)

            let exportUrl =
                window.location.origin +
                '/index.php/occurrence/default/export-other-data-collection-files' +
                '?occurrenceDbId=' + occurrenceDbId +
                '&dataCollectionFileName=' + encodeURIComponent(dataCollectionFileName) +
                '&plotCount=' + plotCount

            window.location.assign(exportUrl)

            // Hide loading indicator after a moment
            setTimeout(function() {
                $('#system-loading-indicator').css('display','none');
            }, 5000);
        }

    })

    // Access Upload mapped plots via dropdown
    $('#upload-planting-arrays-btn').on('click', function (e) {

        // trigger action ongoing
        noAction = false;
        $('#upload-planting-arrays-modal').modal('show')

    })

    // Access Upload trait data collection files via dropdown
    $('#upload-data-collection-files-btn').on('click', function (e) {
        window.location.href =
            window.location.origin +
            '/index.php/dataCollection/upload?program=' +
            '$program&source=em-main'
    })

    // Access Harvest Manager (Harvest Plots or Crosses)
    $('.harvest-manager-btn').on('click', function validateExperimentBeforeAccessingHM (e) {
        e.preventDefault()

        let occurrenceDbIds = getOccurrenceDbIds()
        let obj = $(this)
        let dataLevel = obj.data('data-level')

        if (occurrenceDbIds.length > 1) {
            $('.toast').css('display','none')
            let notif = "<i class='material-icons orange-text left'>warning</i> Please select at most one (1) Occurrence."
            Materialize.toast(notif, 5000)
        } else {
            $.ajax({
                url: '$determineHarvestManagerAccessingMethodUrl',
                type: 'POST',
                data: {
                    occurrenceDbIds: occurrenceDbIds,
                    dataLevel: dataLevel
                },
                success: (response) => {
                    // for Array of accepted  
                    let harvestingDataArray = []
                    // for Array of Occurrences with invalid Experiment Types or Statuses
                    let occurrenceDbIds = []
                    // Parse encoded response
                    response = JSON.parse(response)

                    if (response.statusCode === 0) { // problem in retrieving occurrence
                        $('.toast').css('display','none')
                        let notif = "<i class='material-icons red-text left'>close</i> There was a problem retrieving occurrence information."
                        Materialize.toast(notif, 5000)
                    } else {

                        response.forEach(function (res) {
                            // If no Occurrence was selected
                            if (res.statusCode === 1) {
                                let harvestManagerOccUrl = window.location.origin +
                                    '/index.php/harvestManager/occurrence-selection' +
                                    '?program=' +
                                    '$program'
                                
                                window.location.assign(harvestManagerOccUrl)
                            } else if (res.statusCode === 2) { // Else if there is a selected and accepted Occurrence
                                let occurrenceId = res.occurrenceData.occurrenceDbId.toString()
                                let harvestManagerHDataUrl = window.location.origin +
                                    '/index.php/harvestManager/harvest-data/plots' +
                                    '?program=' +
                                    '$program' +
                                    '&occurrenceId=' +
                                    occurrenceId

                                window.location.assign(harvestManagerHDataUrl)
                            } else if (res.statusCode === 9) { // Else if no write access to the Occurrence

                                $('.toast').css('display','none')
                                let notif = "<i class='material-icons red-text left'>close</i> You are not allowed to harvest on the selected occurrence." 
                                Materialize.toast(notif, 5000)
                            } else if (res.statusCode === 3 || res.statusCode === 4) { // Else if there is a selected and accepted Ocurrence for Harvesting Crosses or Plots
                                let action = dataLevel == 'plot' ? 'plots' : 'crosses'
                                let occurrenceId = res.occurrenceData.occurrenceDbId.toString()
                                let harvestManagerHDataUrl = window.location.origin +
                                    '/index.php/harvestManager/harvest-data/' + action +
                                    '?program=' +
                                    '$program' +
                                    '&occurrenceId=' +
                                    occurrenceId

                                window.location.assign(harvestManagerHDataUrl)
                            } else if (res.statusCode === 5) { // Else if there is an Experiment of an invalid Type
                                const experimentName = res.occurrenceData.experimentName
                                const experimentType = res.occurrenceData.experimentType

                                $('.toast').css('display','none')
                                let notif = "<i class='material-icons red-text left'>close</i> Harvesting seeds for&nbsp;<b>" + experimentName + " (" + experimentType + ")</b>&nbsp;is not yet available."
                                Materialize.toast(notif, 5000)
                            } else if (res.statusCode === 8) { // Else if there is an Occurrence that is not yet PLANTED
                                const experimentName = res.occurrenceData.experimentName

                                $('.toast').css('display','none')
                                let notif = "<i class='material-icons red-text left'>close</i>&nbsp;<b>" + experimentName + "</b>&nbsp;is not yet ready for harvesting plots."
                                Materialize.toast(notif, 5000)
                            } 
                        });
                    }
                },
                error: (err) => {
                }
            })
        }
    })

    // Go to Phenotypic Data Manager tool
    $(document).on('click', '#phenotypic-data-manager-btn', function (e){
        e.stopPropagation()

        // get selected occurrence in the browser
        let occurrenceDbIds = getOccurrenceDbIds()
        let totalOccurrencesCount = occurrenceDbIds.length

        // if selected occurrence is not 1, notify the user
        if (totalOccurrencesCount !== 1) {

            $('.toast').css('display','none')
            let notif = "<i class='material-icons orange-text left'>warning</i> " + 'Please select one Occurrence.'
            Materialize.toast(notif, 5000)

            return
        }

        if (!$(`#\${occurrenceDbIds[0]}`).data('occurrence-status').toLowerCase().includes('trait data collected')) {
            $('.toast').css('display','none')
            let notif = "<i class='material-icons orange-text left'>warning</i> " + 'Selected Occurrence should have "trait data collected" status.'
            Materialize.toast(notif, 5000)

            return            
        }

        // check if user has write access to occurrence
        $.ajax({
            url: '$checkOccurrenceWritePermissionUrl',
            type: 'post',
            data:{
                occurrenceIds: occurrenceDbIds
            },
            success: function(response){
                var parsedResponse = JSON.parse(response);

                // no write permission in selected occurrence
                if(parsedResponse['noAccessCount'] !== undefined || '$baUrl' == ''){
                    $('.toast').css('display','none')
                    let notif = "<i class='material-icons red-text left'>close</i> You are not allowed to manage data for the selected occurrence."
                    Materialize.toast(notif, 5000)

                    return
                } else {
                    let pdmUrl = '$pdmUrl';

                    window.open(pdmUrl + occurrenceDbIds[0], '_blank')
                }
            }
        });

    })

    // Create a Packing Job for Planting Instructions Manager
    $(document).on('click', '#create-packing-job-btn', function validateOccurrenceBeforeCreatingPackingJob (e) {
        e.stopPropagation()
        e.preventDefault()

        let occurrenceDbIds = getOccurrenceDbIds()
        let totalOccurrencesCount = occurrenceDbIds.length
        let invalidOccurrences = []
        let occurrenceName = ''
        let occurrenceStatus = ''

        if (totalOccurrencesCount === 0) {
            $('.toast').css('display','none')
            let notif = "<i class='material-icons orange-text left'>warning</i> " + 'Please select at least one (1) Occurrence.'
            Materialize.toast(notif, 5000)

            return
        }

        // Display loading indicator
        $('#system-loading-indicator').html(loadingIndicatorHtml)

        // Create Packing Job transaction
        $.ajax({
            url: retrieveValidAndInvalidOccurrencesUrl,
            type: 'post',
            data:{
                occurrenceDbIds: occurrenceDbIds
            },
            success: function (response) {
                const parsedResponse = JSON.parse(response)
                let toastMessage = ''

                // if there is at least one selected occurrence without write access
                if(parsedResponse['noAccessCount'] != undefined){
                    // Hide loading indicator
                    $('#system-loading-indicator').html('')
                    let plural = parsedResponse['noAccessCount'] > 1 ? 's' : '';
                    
                    toastMessage = 'You are not allowed to create packing job to&nbsp;<b> ' + parsedResponse['noAccessCount'] + ' </b>&nbsp;selected occurrence' + plural + '.';
                    
                    $('.toast').css('display','none')
                    let notif = "<i class='material-icons red-text left'>close</i> " + toastMessage
                    Materialize.toast(notif, 5000)
                } else {

                    validOccurrences = parsedResponse['validOccurrences']
                    invalidOccurrences = parsedResponse['invalidOccurrences']

                    // Hide loading indicator
                    $('#system-loading-indicator').html('')

                    invalidCount = invalidOccurrences.length || 0
                    validCount = validOccurrences.length || 0

                    // This prevents redirection to proceed
                    // if there is at least 1 invalid Occurrence
                    // regardless if there are valid Occurrences
                    if (invalidCount) {

                        if (invalidCount > 1) {
                            toastMessage = 'There were&nbsp;<b>' + invalidCount + ' out of ' + totalOccurrencesCount + '</b>&nbsp;invalid Occurrences for packing job creation (should not be Planted and does not belong to a Packing job).'
                        } else {
                            toastMessage = '<b>' + invalidOccurrences[0] + '</b>&nbsp;is invalid for packing job creation (should not be Planted and does not belong to a Packing job).'
                        }

                        $('.toast').css('display','none')
                        let notif = "<i class='material-icons red-text left'>close</i> " + toastMessage
                        Materialize.toast(notif, 5000)
                    } else if (invalidCount == 0 && validCount == 0) {
                        toastMessage = 'There was a problem retrieving occurrence information.'
                        $('.toast').css('display','none')
                        let notif = "<i class='material-icons red-text left'>close</i> " + toastMessage
                        Materialize.toast(notif, 5000)
                    }
                    else {
                        // Display confirmation window
                        var occurrenceCountLabel = (totalOccurrencesCount > 1) ? ' Occurrences' : ' Occurrence';
                        $('#occurrence-count-label-modal').html(totalOccurrencesCount + occurrenceCountLabel);

                        $('#create-packing-job-modal').modal('show')
                    }
                }
            },
            error: function () {
                // Hide loading indicator
                $('#system-loading-indicator').html('')
            }
        });
    })

    // Confirm create packing job
    $(document).on('click', '.confirm-create-packing-job-btn', function(e){
        e.stopPropagation()
        e.preventDefault()

        // Prevent redundant clicking of this CONFIRM button...
        // ...by disabling it upon initial click
        $('.confirm-create-packing-job-btn').addClass('disabled')

        let uniqueValidOccurrences = [...new Set(validOccurrences)];

        // Create Packing Job transaction
        $.ajax({
            url: '$storeOccurrenceIdsUrl',
            type: 'post',
            data:{
                occurrenceIds: uniqueValidOccurrences,
                program: program,
                programId: programId
            },
            success: function (response) {
                $('.confirm-create-packing-job-btn').removeClass('disabled')
            },
            error: function () {}
        });
    })

    // Upon clicking Cancel, re-enable CONFIRM button for Create Packing Job
    $(document).on('click', '#cancel-save', function (e) {
        $('.confirm-create-packing-job-btn').removeClass('disabled')
    })

    // Access Update Occurrences page
    $('#bulk-update-occurrences-btn').on('click', function redirectToUpdateOccurrences (e) {
        let selectedOccurrenceDbIds = getOccurrenceDbIds()
        let selectedOccurrenceDbIdsCount = selectedOccurrenceDbIds.length
        let selectedOccurrenceDbIdsString = selectedOccurrenceDbIds.join('|')
        // This is needed for the retrieval of protocols and their input fields
        let templateOccurrenceDbId = selectedOccurrenceDbIds[0]
        let dataProcessAbbrevsArray = []
        let dataProcessAbbrevsString = []

        if (selectedOccurrenceDbIdsCount === 0) {
            toastMessage = 'Please select at least one (1) Occurrence.'
            let notif = "<i class='material-icons orange-text left'>warning</i> " + toastMessage

            Materialize.toast(notif, 5000)
        } else {
            // Display loading indicator
            $('#system-loading-indicator').html(loadingIndicatorHtml)

            // check if user has write access to occurrences
            $.ajax({
                url: '$checkOccurrenceWritePermissionUrl',
                type: 'post',
                data:{
                    occurrenceIds: selectedOccurrenceDbIds
                },
                success: function(response){
                    var parsedResponse = JSON.parse(response);

                    // no write permission to at least one selected occurrence
                    if(parsedResponse['noAccessCount'] !== undefined){
                        let invalidOccurrenceCount = parsedResponse['noAccessCount'];
                        let label = (invalidOccurrenceCount > 1) ? 'occurrences.' : 'occurrence.';

                        $('.toast').css('display','none')
                        let notif = "<i class='material-icons red-text left'>close</i> You are not allowed to update protocols to&nbsp;<b>" + invalidOccurrenceCount + '</b>&nbsp;' + label
                        Materialize.toast(notif, 5000)

                        return
                    } else {
                        
                        $.ajax({
                            url: retrieveDataProcessAbbrevsUrl,
                            type: 'post',
                            data: {
                                occurrenceDbIds: selectedOccurrenceDbIds
                            },
                            success: function (response) {
                                const parsedResponse = JSON.parse(response)

                                window.location.assign(redirectToUpdateOccurrencesUrl +
                                    '&templateOccurrenceDbId=' + templateOccurrenceDbId +
                                    '&occurrenceDbIdsString=' + selectedOccurrenceDbIdsString +
                                    '&dataProcessAbbrevsString=' + parsedResponse['dataProcessAbbrevString']
                                )
                            },
                            error: function (x, s, e) {
                                // Hide loading indicator
                                $('#system-loading-indicator').html('')

                                $('.toast').css('display','none')
                                let notif = `<i class='material-icons red-text left'>close</i>
                                    There seems to be a problem with retrieving experiment records.`

                                Materialize.toast(notif,5000)
                            }
                        })
                    }
                }
            });
        }
    })

    // Access Generate Code Pattern page
    $('.generate-code-pattern-btn').on('click', function redirectToGenerateCodePattern (e) {
        let obj = $(this)
        let entity = obj.data('entity')
        let level = obj.data('level')
        const occurrenceIds = getOccurrenceDbIds()

        if (occurrenceIds.length === 0) {
            $('.toast').css('display','none')
            let notif = `
                <i class='material-icons orange-text left'>warning</i>
                Please select at least one (1) Occurrence.
            `

            Materialize.toast(notif,5000)
        } else {
            if (level === 'experiment') {
                // Display loading indicator
                $('#system-loading-indicator').html(loadingIndicatorHtml)

                $.ajax({
                    url: retrieveExperimentDbIdsUrl,
                    type: 'post',
                    data: {
                        occurrenceDbIds: occurrenceIds
                    },
                    success: function (response) {
                        const parsedResponse = JSON.parse(response)

                        // no access permission to at least one selected occurrence
                        if(parsedResponse['noAccessCount'] !== undefined){
                            // Hide loading indicator
                            $('#system-loading-indicator').html('')

                            let invalidOccurrenceCount = parsedResponse['noAccessCount'];
                            let label = (invalidOccurrenceCount > 1) ? 'occurrences.' : 'occurrence.';
                            $('.toast').css('display','none')
                            let notif = `<i class='material-icons red-text left'>close</i>
                                You are not allowed to generate code to&nbsp;<b>` + invalidOccurrenceCount + '</b>&nbsp;' + label

                            Materialize.toast(notif,5000)

                            return
                        }

                        window.location.assign(redirectToGenerateCodePatternUrl +
                        '&entity=' + entity +
                        '&level=' + level + 
                        '&experimentDbIdsString=' + parsedResponse['experimentDbIdString'])
                    },
                    error: function (x, s, e) {
                        // Hide loading indicator
                        $('#system-loading-indicator').html('')

                        $('.toast').css('display','none')
                        let notif = `<i class='material-icons red-text left'>close</i>
                            There seems to be a problem with retrieving experiment records.`

                        Materialize.toast(notif,5000)
                    }
                })
            } else if (level === 'occurrence') {
                let occurrenceDbIdsString = occurrenceIds.join('|')

                // check if user has write access to occurrences
                $.ajax({
                    url: '$checkOccurrenceWritePermissionUrl',
                    type: 'post',
                    data:{
                        occurrenceIds: occurrenceIds
                    },
                    success: function(response){
                        var parsedResponse = JSON.parse(response);

                        // no write permission to at least one selected occurrence
                        if(parsedResponse['noAccessCount'] !== undefined){
                            let invalidOccurrenceCount = parsedResponse['noAccessCount'];
                            let label = (invalidOccurrenceCount > 1) ? 'occurrences.' : 'occurrence.';

                            $('.toast').css('display','none')
                            let notif = "<i class='material-icons red-text left'>close</i> You are not allowed to generate code to&nbsp;<b>" + invalidOccurrenceCount + '</b>&nbsp;' + label
                            Materialize.toast(notif, 5000)

                            return
                        } else {
                            window.location.assign(redirectToGenerateCodePatternUrl +
                                '&entity=' + entity +
                                '&level=' + level + 
                                '&occurrenceDbIdsString=' + occurrenceDbIdsString)
                        }
                    }
                });
            }
        }
    })

    $('body').on('click', function (e) {
        // Abort retrieving notifications if the notif menu has been hidden by the user
        if ($('#notifications-dropdown').is(':visible') && retrieveNotificationsAjax) {
            retrieveNotificationsAjax.abort()
            retrieveNotificationsAjax = null
        }
    })

    // Manage Experiment Permissions
    $('#experiment-permissions-btn').on('click', async function redirectToExperimentPermissions (e) {
        let validOccurrenceCount = 0
        let invalidOccurrenceCount = 0
        let occurrenceIds = []

        if (checkedBoxes.length == 0) {
            $('.toast').css('display','none')

            let notif = `<i class='material-icons orange-text left'>warning</i>  Please select at least one (1) Occurrence.`

            Materialize.toast(notif,5000)
            return
        }

        if (isAdmin) {
            checkedBoxes.map(element => occurrenceIds.push(element.id))
            occurrenceIds = occurrenceIds.join('|')
        } else {
            checkedBoxes.map(element => {
                if (personId == element.experimentStewardId || personId == element.creatorId) {
                    validOccurrenceCount += 1
                    occurrenceIds.push(element.id)
                }
                else
                    invalidOccurrenceCount +=1
            })

            if (validOccurrenceCount == 0) {
                $('.toast').css('display','none')

                let notif = `<i class='material-icons red-text left'>close</i>  You are not allowed to manage permissions of all the selected Occurrences.`

                Materialize.toast(notif,5000)

                return
            }

            occurrenceIds = occurrenceIds.join('|')
        }

        // Redirect from EM to Experiment to Permissions tool
        let experimentPermissionsUrl = window.location.origin +
            '/index.php/occurrence/manage/experiment-permissions' +
            '?program=' +
            program +
            '&occurrenceIds=' +
            occurrenceIds +
            '&invalidOccurrenceCount=' +
            invalidOccurrenceCount

        window.location.assign(experimentPermissionsUrl)
    })

    // when personalize grid settings button is clicked
    $('.personalize-grid-icon').parent().on('click', function(e) {
        $('#pageSize-' + browserId).val(currentDefaultPageSize); // put current default value in grid settings page size input
    });

    // when save grid settings is clicked
    $(document).on('click', '.dynagrid-submit', function(e) {
        if($(this).parents('#' + browserId + '-grid-modal').length) {
            var value = $('#pageSize-' + browserId).val().trim();
            // check if value is not empty, is a number, and is an integer
            if(value != '' && !isNaN(value) && !value.includes('.')) {
                updateDefaultPageSize(value)
            }
        }
    });

    // trigger saving of default page size when user presses the enter button from the textfield
    $(document).on('keypress', '#pageSize-' + browserId, function(e) {
        var inputPageSize = parseInt($(this).val())
        var keyCode = (e.keyCode? e.keyCode : e.which);
        if(keyCode == 13) { // enter key is pressed
            $('#' + browserId + '-grid-modal').find('.dynagrid-submit').eq(0).trigger('click');
        }
    });

    // when remove grid settings and confirm button is clicked
    $(document).on('click', '.btn-warning', function(e) {
        if($(this).parents('.modal-content').find('.bootstrap-dialog-message').text()
            == 'Are you sure you want to delete the setting?') {    // this is the prompt for the confirm modal
            updateDefaultPageSize('-1');  // resets the value to default
        }
    });

    // trigger saving of default page size on personalize grid settings modal when user clicks apply button in preferences
    $(document).on('click', '#apply-default-page-size-btn', function(e) {
        e.stopPropagation();
        e.preventDefault();
        var inputPageSize = parseInt($('#pageSize-' + browserId).val());
        updateDefaultPageSize(inputPageSize);
        $('#pageSize-' + browserId).val(inputPageSize); // put current default value in grid settings page size input
        $('#' + browserId + '-grid-modal').find('.dynagrid-submit').eq(0).trigger('click');
    });

    // trigger downloading data collection summary (Single Occurrence)
    $('.download-single-data-collection-summary-btn').on('click', function clickDownloadSingleDataCollectionSummaryButton (e) {
        const occurrenceDbId = $(this).data('id')
        const columnHeaderType = $(this).data('column-header-type')
        const exportDataCollectionSummariesUrl = '$exportDataCollectionSummariesUrl'
        const experimentCode = $(`#\${occurrenceDbId}.occurrence-grid-select`).data('experiment-code')
        const occurrenceNumber = $(`#\${occurrenceDbId}.occurrence-grid-select`).data('occurrence-number')
        const plotCount = $(`#\${occurrenceDbId}.occurrence-grid-select`).data('plot-count')

        if (!$(`#\${occurrenceDbId}.occurrence-grid-select`).data('occurrence-status').includes('trait data collected')) {
            message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + ' The selected Occurrence has&nbsp;<b>no available trait data</b>. Collect trait data first before attempting to export.'

            $('.toast').css('display','none')
            Materialize.toast(message, 5000)

            return
        }

        const expDataCollectionSummariesUrl = window.location.origin +
            exportDataCollectionSummariesUrl +
            '?occurrenceDbId=' + occurrenceDbId +
            '&program=' + program +
            '&columnHeaderType=' + columnHeaderType +
            '&experimentCode=' + experimentCode +
            '&occurrenceNumber=' + occurrenceNumber +
            '&plotCount=' + plotCount

        message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + ' Please wait for the download to finish.'

        $('.toast').css('display','none')
        Materialize.toast(message, 5000)

        window.location.assign(expDataCollectionSummariesUrl)
    })

    // trigger downloading data collection summary (Multiple Occurrences)
    $('.download-multiple-data-collection-summaries-btn').on('click', function clickDownloadMultipleDataCollectionSummariesButton (e) {
        // Retrieve ids of selected occurrences in sesssion
        const selectedOccurrences = JSON.parse(sessionStorage.getItem('occurrenceCheckboxIds'))

        // If session storage for selected occurrences is empty, show notification
        if(selectedOccurrences.length == 0){
            $('.toast').css('display','none')
            let notif = "<i class='material-icons orange-text left'>warning</i> Please select one (1) Occurrence"
            Materialize.toast(notif,5000)
            return
        }

        // Check if all have statuses PLANTED && TRAIT DATA COLLECTED
        invalidOccurrenceCount = 0
        checkedBoxes.forEach(elem => elem.occurrenceStatus
            .includes('trait data collected') ? 1 : ++invalidOccurrenceCount
        )

        // Notify user that some of the selected occurrences
        // ...do not have TRAIT DATA COLLECTED status
        // ...and end execution
        if (invalidOccurrenceCount) {
            message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + ' All selected occurrence must have&nbsp;<b>trait data collected</b>.'

            $('.toast').css('display','none')
            Materialize.toast(message, 5000)
            
            return
        }

        // Get occurrence DB IDs
        const occurrenceDbIds = getOccurrenceDbIds()
        const occurrenceDbIdsString = occurrenceDbIds.join(',')

        // Get total plot count based on selected occurrences
        let plotCount = 0
        occurrenceDbIds.forEach(id => plotCount += $('#' + id).data('plot-count'))

        // Build URL
        const columnHeaderType = $(this).data('column-header-type')
        const exportDataCollectionSummariesUrl = '$exportDataCollectionSummariesUrl'

        let expDataCollectionSummariesUrl = window.location.origin +
            exportDataCollectionSummariesUrl +
            '?occurrenceDbId=' + occurrenceDbIdsString +
            '&program=' + program +
            '&columnHeaderType=' + columnHeaderType

        // Get experiment code and occurrece number for file naming
        // ...if only 1 occurrence is selected
        if (occurrenceDbIds.length === 1) {
            let checkBox = $('#' + occurrenceDbIds[0])
            expDataCollectionSummariesUrl = expDataCollectionSummariesUrl +
                '&experimentCode=' + checkBox.data('experiment-code') +
                '&occurrenceNumber=' + checkBox.data('occurrence-number')
        }

        expDataCollectionSummariesUrl = expDataCollectionSummariesUrl +
            '&plotCount=' + plotCount

        message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + ' Please wait for the download to finish.'

        $('.toast').css('display','none')
        Materialize.toast(message, 5000)

        window.location.assign(expDataCollectionSummariesUrl)
    })

    // Display Toast notification
    $('.download-widget-download-button').on('click', function (e) {
        message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + 'Background download processor started. You may proceed with other tasks.'

        $('.toast').css('display','none')
        Materialize.toast(message, 5000)
    })
}

// select all occurrences
$(document).on('click', '#occurrence-select-all', function(e) {
    if ($(this).prop("checked") === true) {
        if (pageSize > selectOccurrenceThreshold) {
            $('.occurrence-grid-select').each(function (i, obj) {
                if (i+1 > selectOccurrenceThreshold) {
                    // Break the loop
                    return false
                }

                if ($(this).prop('checked') === false) {
                    $(this).prop('checked', true)
                    $(this).parent().parent().addClass('grey lighten-4')
                }
            })

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + ` <span>Selecting occurrence records. Note that only up to <strong>\${selectOccurrenceThreshold}</strong> records can be selected.</span>`

            $('.toast').css('display','none')
            Materialize.toast(message, 5000)
        } else {
            // Tick all checkboxes
            $('.occurrence-grid-select').prop('checked',true)
            $(".occurrence-grid-select:checkbox")
                .parent("td")
                .parent("tr")
                .addClass("grey lighten-4")

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + ` <span>Selecting occurrence records. Note that only up to <strong>\${selectOccurrenceThreshold}</strong> records can be selected.</span>`

            $('.toast').css('display','none')
            Materialize.toast(message, 5000)
        }

        const dataBrowserPages = $('li[class="active"]').children()
        // If data browser's pagination UI is displayted...
        // Display this message if user clicks Select All from Page 2 and up
        if (dataBrowserPages.length && dataBrowserPages[0].innerHTML > 1) {
            Materialize.toast(`<i class="material-icons blue-text" style="margin-right: 8px;">info</i> If you wish to select rows from a previous page, please return to that page and click Select All.`, 5000)
        }

        // Display loading indicator
        $('#system-loading-indicator').html(loadingIndicatorHtml)

        setCursorInteraction(false)

        retrieveOccurrenceDbIdsAjax = $.ajax({
            type: 'POST',
            dataTye: 'json',
            url: '$retrieveDbIdsUrl',
            data: {},
            async: true,
            success: function (response) {
                checkedBoxes = [ ...JSON.parse(response) ]

                sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))

                totalCheckboxes = checkedBoxes.length

                compareCheckBoxCountWithTotalCount()

                retrieveOccurrenceDbIdsAjax = null

                message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i>' + 'Successfully retrieved the occurrence records.'

                $('.toast').css('display','none')

                Materialize.toast(message, 5000)

                // Remove loading indicator
                $('#system-loading-indicator').html('')

                setCursorInteraction(true)
            },
            error: function (x, s, e) {
                if (retrieveOccurrenceDbIdsAjax) { // cancel ajax request if it is ongoing
                    retrieveOccurrenceDbIdsAjax.abort()
                    retrieveOccurrenceDbIdsAjax = null
                }
                console.log('x:', x)
                console.log('s:', s)
                console.log('e:', e)
                message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'There was an error in retrieving the occurrence records.'
                $('.toast').css('display','none')
                Materialize.toast(message, 5000)

                // Remove loading indicator
                $('#system-loading-indicator').html('')

                setCursorInteraction(true)
            }
        })
    } else {
        if (retrieveOccurrenceDbIdsAjax) { // cancel ajax request if it is ongoing
            retrieveOccurrenceDbIdsAjax.abort()
            retrieveOccurrenceDbIdsAjax = null

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + 'Cancelled retrieving the occurrence records.'

            $('.toast').css('display','none')
            
            Materialize.toast(message, 5000)

            // Remove loading indicator
            $('#system-loading-indicator').html('')

            setCursorInteraction(true)
        }
        $('.occurrence-grid-select').prop('checked',false)
        $("input:checkbox.grid-select")
            .parent("td")
            .parent("tr")
            .removeClass("grey lighten-4")

        checkedBoxes = []
        sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
        compareCheckBoxCountWithTotalCount()
    }
})

// auto generate location name
$(document).on('click', '.default-namegen-switch', function(e) {
        var fid = this.id;
        var fieldId = fid.split('generate-');

        if($(this).prop('checked') == false){
            $('#'+fieldId[1]).removeClass('disabled');
            $('#'+fieldId[1]).removeClass('generated-pattern');
            $('#'+fieldId[1]).removeAttr('readonly');
            $('.go-to-preview-generate-location-btn').addClass('disabled');
            $('.browse-btn').addClass('disabled');
            $('.fileinput-upload-button').addClass('disabled');
        } else {
            $('#'+fieldId[1]).addClass('disabled');
            $('#'+fieldId[1]).addClass('generated-pattern');
            $('#'+fieldId[1]).attr('readonly','readonly');
            $('#'+fieldId[1]).val('');
            $('.go-to-preview-generate-location-btn').removeClass('disabled');
            $('.browse-btn').removeClass('disabled');
            $('.fileinput-upload-button').removeClass('disabled');
        }
})

// click row in occurrence browser
$(document).on('click', '#' + gridId + ' tbody tr', function (e) {
    e.preventDefault()
    e.stopImmediatePropagation()

    if (retrieveOccurrenceDbIdsAjax) {
        message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'Please wait for the system to finish retrieving the occurrence records.'

        $('.toast').css('display','none')

        Materialize.toast(message, 5000)
        return
    }

    let thisRow = $(this).find('input:checkbox')[0]

    if (!thisRow) return

    thisRowAttributes = {
        'id': thisRow.id,
        'experimentStewardId': thisRow.getAttribute('experimentStewardId'),
        'creatorId': thisRow.getAttribute('creatorId'),
        'locationDbId': $('#' + thisRow.id).data('location-db-id'),
        'locationCode': $('#' + thisRow.id).data('location-code'),
        'occurrenceStatus': $('#' + thisRow.id).data('occurrence-status'),
        'occurrenceNumber': $('#' + thisRow.id).data('occurrence-number'),
        'experimentCode': $('#' + thisRow.id).data('experiment-code'),
        'plotCount': $('#' + thisRow.id).data('plot-count'),
        'experimentType': $('#' + thisRow.id).data('experiment-type'),
        'experimentDesignType': $('#' + thisRow.id).data('experiment-design-type'),
        'occurrenceDesignType': $('#' + thisRow.id).data('occurrence-design-type'),
    }

    if (thisRow != undefined) {
        $('#' + thisRow.id).trigger('click')

        if (thisRow.checked) {
            checkedBoxes.push(thisRowAttributes)
            sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
            let checkedBoxesCount = checkedBoxes.length
            compareCheckBoxCountWithTotalCount()

            $(this).addClass('grey lighten-4')
        } else {
            let index = -1

            // Find ID for selection removal
            for (let i = 0; i < checkedBoxes.length; i++) {
                const element = checkedBoxes[i]
                
                if (element.id == (thisRow.id)) {
                    index = i
                    break
                }
            }

            checkedBoxes.splice(index, 1)
            sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
            let checkedBoxesCount = checkedBoxes.length
            compareCheckBoxCountWithTotalCount()

            $(this).removeClass('grey lighten-4')
        }
    }
    getOccurrenceDbIds()
})

// delete occurrence
$(document).on('click','.delete-occurrence', function() {
    var obj = $(this);
    var occurrenceName = obj.data('occurrence_name');
    var occurrenceId = obj.data('id');
    var deleteModal = '#occurrence-delete-modal > .modal-dialog > .modal-content > .modal-body';
    selectedOccurrenceId = occurrenceId;

    $('#delete-occurrence-label').html('<i class="fa fa-trash"></i> Delete ' + occurrenceName);
    $('#occurrence-delete').html(occurrenceName);

    $('#occurrence-delete-modal').modal('show');
});

$(document).on('click', '.delete-occurrence-btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var url = '$deleteUrl';

        //Send to background job indicator
        $('#mod-progress').removeClass('hidden');
        var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
        $('#mod-progress').html(loading);
        $('#mod-notif').removeClass('hidden');
        $('#mod-notif').html('<div class="alert ">'+ 
                            '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                '<i class="fa fa-info-circle"></i>'+
                                    ' A background job is being created.' +
                            '</div></div>');

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                id: selectedOccurrenceId
            },
            async: false,
            success: function(response) {

            }
        });
    });

// regenerate occurrence design
$(document).on('click','#regenerate-design-btn', function() {
    let selectedOccurrenceDbIds = getOccurrenceDbIds()

    // check if user has write access to occurrence
    $.ajax({
        url: '$checkOccurrenceWritePermissionUrl',
        type: 'post',
        data:{
            occurrenceIds: selectedOccurrenceDbIds
        },
        success: function(response){
            var parsedResponse = JSON.parse(response);

            // no write permission in selected occurrence
            if(parsedResponse['noAccessCount'] !== undefined){
                $('.toast').css('display','none')
                let plural = parsedResponse['noAccessCount'] > 1 ? 's' : '';
                    
                let notif = "<i class='material-icons red-text left'>close</i> You are not allowed to regenerate design to&nbsp;<b> " + parsedResponse['noAccessCount'] + ' </b>&nbsp;selected occurrence' + plural + '.';

                Materialize.toast(notif, 5000)

                return
            } else {

                const experimentCodes = getOccurrenceAttributes('experimentCode')
                let uniqueExpts = [...new Set(experimentCodes)];
                
                if(uniqueExpts.length > 1){
                    $('.toast').css('display','none')
                    let notif = `
                        <i class='material-icons red-text left'>close</i>
                        Please select occurrences (created or mapped) within the same experiment to proceed with generation of design.
                    `
                    Materialize.toast(notif,5000)
                } else if(uniqueExpts.length == 0){
                    $('.toast').css('display','none')
                    let notif = `
                        <i class='material-icons red-text left'>close</i>
                        Please select one or more occurrences (created or mapped) within the same experiment to proceed with generation of design.
                    `
                    Materialize.toast(notif,5000)
                } else {
                    const occurrenceStatus = getOccurrenceAttributes('occurrenceStatus')
                    let uniqueStatus = [...new Set(occurrenceStatus)]

                    let allowedStatus = ['created', 'mapped']
                    let errorStatus = false;
                    for(let i=0; i<uniqueStatus.length; i++){
                        if(allowedStatus.includes(uniqueStatus[i])){
                            continue
                        } else {
                            errorStatus = true
                            break
                        }
                    }
                    const expDesignType = getOccurrenceAttributes('experimentDesignType')

                    const expType = getOccurrenceAttributes('experimentType')
                    let uniqueExpType = [...new Set(expType)]

                    if(!errorStatus && uniqueExpType.length == 1 && uniqueExpType[0] == 'Breeding Trial'){

                        const expDesignType = getOccurrenceAttributes('experimentDesignType')

                        if(!errorStatus && expDesignType[0] != ''){
                            // Display loading indicator
                            $('#system-loading-indicator').html(loadingIndicatorHtml)
                            
                            let selectedOccurrenceDbIdsCount = selectedOccurrenceDbIds.length
                            let selectedOccurrenceDbIdsString = selectedOccurrenceDbIds.join('|')

                            window.location.assign(redirectToUpdateDesignUrl +
                                '&occurrenceDbIds=' + selectedOccurrenceDbIdsString +
                                '&regenerate=true'
                            ) 
                        } else {
                            $('.toast').css('display','none')
                            let notif = `
                                <i class='material-icons red-text left'>close</i>
                                Regeneration of design is unavailable for this experiment. Please select other occurrences.
                            `
                            Materialize.toast(notif,5000)
                        }
                        
                    } else {
                        $('.toast').css('display','none')
                        let notif = `
                            <i class='material-icons red-text left'>close</i>
                            Please select one or more occurrences (created or mapped) within the same Breeding Trial experiment to proceed with generation of design.
                        `
                        Materialize.toast(notif,5000)
                    }
                }
            }
        }
    });
});

// Display Toast message for downloading printouts
$(document).on('click', '#printouts-modal-download-button', function (e) {
    message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + 'Please wait for the download to finish.'

    $('.toast').css('display','none')
    Materialize.toast(message, 5000)
})

// display notifications in dropdown UI
function notifDropdown(){
    $('.em-notification-button').on('click', function(e){
        // Display a loading indicator for the notif menu
        $('#notifications-dropdown').html(`<li>
                <h6 class="center ">\${notificationsString}&emsp;&emsp;
                    <span class="round red accent-2 white-text pull-right "><b class="notif-count">0</b></span>
                </h6>
            </li>
            <li class="divider"></li>
            <li class="header center">\${loadingIndicatorHtml}</li>
        `)

        retrieveNotificationsAjax = $.ajax({
            url: notificationsUrl,
            type: 'post',
            async: true,
            success: function(data){
                renderNotifications();
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
                $('#notifications-dropdown').html(data);

                $('.bg_notif-btn').on('click', function(e){
                    e.preventDefault()

                    let obj = $(this)
                    let id = obj.data('transaction_id')
                    let abbrev = obj.data('abbrev')
                    let workerName = obj.data('worker-name')
                    let entity = obj.data('entity')
                    let description = obj.data('description')
                    let filename = obj.data('filename')

                    if (workerName == 'buildcsvdata' || workerName == 'buildjsondata') {
                        // Create temporary entry point to downloading CSV data
                        const a = document.createElement('a')
                        const dataFormat = workerName.includes('csv') ? ( filename.includes('Multi') ? 'zip' : 'csv' ) : 'json'

                        a.setAttribute('hidden', '')
                        a.setAttribute('href', `/index.php/occurrence/default/download-data?filename=\${filename}&dataFormat=\${dataFormat}`)

                        // Temporarily insert element to document
                        document.body.appendChild(a)

                        // Trigger download
                        a.click()

                        // Remove element
                        document.body.removeChild(a)

                        message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
                        $('.toast').css('display','none')
                        Materialize.toast(message, 5000)

                        $('#system-loading-indicator').html('')
                    } else if (abbrev) { // If the abbrev is set, the transaction is saving of list
                        // go to List Management to view List
                        window.location.href =
                            window.location.origin +
                            '/index.php/account/list?program=' + '$program&' +
                            'MyList%5Babbrev%5D=' + abbrev
                    } else if(workerName == 'createharvestrecords'){
                        let hmUrl = obj.data('url')
                        // go to harvest manager
                        window.location.href =
                            window.location.origin + hmUrl
                    } else if(workerName == 'createoccurrencerecords'){
                        $.pjax.reload({
                            container: '#' + gridId + '-pjax',
                            replace: true,
                            url: '/index.php/occurrence?program='+ program +'&OccurrenceSearch[occurrenceDbId]=' + description
                        })
                    }else {
                        $.pjax.reload({
                            container: '#' + gridId + '-pjax',
                            replace: true,
                            url: '/index.php/occurrence?program='+ program +'&OccurrenceSearch[occurrenceDbId]=' + id
                        })
                    }
                })
            },
            error: function(xhr, status, error) {
                // If response text is empty, don't display error notif
                if (!xhr.responseText) return

                var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');
                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();
            }  
        });
    });
}

// initialize dropdown materialize for notifications
function renderNotifications(){
    $('.em-notification-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: false,
        gutter: 0,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });
}

// show new notifications
function updateNotif() {
    // if no action is triggered
    if(noAction){
        $.getJSON(newNotificationsUrl, 
        function(json){
            $('#notif-badge-id').html(json);
            if(parseInt(json)>0){
                $('#notif-badge-id').addClass('notification-badge red accent-2');
            }else{
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
            }
        });
    }
}

/**
 * Retrieve Occurrence IDs from "occurrenceCheckboxIds" JS session variable
 * 
 * @return array An array containing IDs of selected Occurrences
 */
function getOccurrenceDbIds () {
    return checkedBoxes.map(element => element.id)
}

/**
 * Retrieve attributes from "occurrenceCheckboxIds" JS session variable
 * 
 * @return array An array of attributes for selected Occurrences
 */
function getOccurrenceAttributes (key) {
    return checkedBoxes.map(element => element[key])
}

function compareCheckBoxCountWithTotalCount () {
    if (totalCheckboxes > 0 && totalCheckboxes > checkedBoxes.length) {
        isSelectAllChecked = false
        $('#occurrence-select-all').prop('checked', false)

        // update checked box counter text
        if (checkedBoxes.length === 1) {
            $('#selected-items-count').html(checkedBoxes.length)
            $('#selected-items-text').html('selected item.')
        } else if(checkedBoxes.length > 1) {
            $('#selected-items-count').html(checkedBoxes.length)
            $('#selected-items-text').html('selected items.')
        } else if (checkedBoxes.length === 0) {
            $('#selected-items-count').html('No')
            $('#selected-items-text').html('selected items.')
        }
    } else if (totalCheckboxes > 0 && totalCheckboxes === checkedBoxes.length) {
        isSelectAllChecked = true
        $('#occurrence-select-all').prop('checked', true)

        // update checked box counter text
        if (checkedBoxes.length === 1) {
            $('#selected-items-count').html(checkedBoxes.length)
            $('#selected-items-text').html('selected item.')
        } else if(checkedBoxes.length > 1) {
            $('#selected-items-count').html(checkedBoxes.length)
            $('#selected-items-text').html('selected items.')
        }
    }
}

// save new value for data browser default page size
function updateDefaultPageSize(value) {
    value = parseInt(value);
    $.ajax({
        type: 'POST',
        url: setDefaultPageSizeUrl,
        async: false,
        data: {
            newDefaultPageSize: value
        },
        success: function() {
        },
        error: function() {
        }
    });
}

// Performs adjustments related to the unified browser size across the system 
function adjustBrowserPageSize() {
    for (browserId of browserIds) {
        var browserPageSize = $('#pageSize-' + browserId).val() === undefined ?
            null : $('#pageSize-' + browserId).val().trim();  // upon loading

        // If page sizes of browser and preferences does not match, then apply page size from preferences (to also apply it to browser cookies)
        if (browserPageSize !== null && parseInt(browserPageSize) != currentDefaultPageSize) {
            var notif = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Applying default page size. The page will refresh.';
            Materialize.toast(notif, 2500);
            $('#pageSize-' + browserId).val(currentDefaultPageSize); // put current default value in grid settings page size input
            $('#' + browserId + '-grid-modal').find('.dynagrid-submit').eq(0).trigger('click');   // trigger click
        }
    }
}
JS);

$this->registerJsFile(
    "@web/js/browser/detail-view.js",
    [
        'depends' => ['\yii\web\JqueryAsset'],
        'position' => \yii\web\View::POS_END
    ]
);

$this->registerCss('
    .disabled {
        pointer-events:none;
        opacity:0.6;
    }

    .dropdown-content {
        overflow-y: visible;
    }

    .dropdown-content .dropdown-content {
        margin-left: -100%;
    }

    .non-link, .cursor-default {
        cursor: default
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;        
    }
');
?>