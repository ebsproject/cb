<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>
*/
use macgyer\yii2materializecss\widgets\Button;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\components\DownloadWidget;

/**
 * Renders basic information of an occurrence
 */

 extract($data);
 $notSet = '<span class="not-set">(not set)</span>';
 $codeValue = isset($code['value']) ? htmlspecialchars($code['value']) : $notSet;
 $locationCode = $primaryInfo[8]['value'];
 $locationName = $primaryInfo[7]['value'];
 $locationType = $secondaryInfo[2]['value'];
 $occurrenceName = isset($name['value']) ? $name['value'] : '';
 $isAdmin = Yii::$app->session->get('isAdmin');
 $toolAbbrev = 'OCCURRENCES';
 
echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $id, 
    'program'=>$program,
    'occurrenceName' => $occurrenceName,
    'active'=>'basic'
]);

// Main action buttons from EM data browser
$manageBasicInfoButton = [];
$generateLocationButton = [];
$commitBtn = [];

// Load HTML, JS, CSS, and jQuery content of Download Widget
echo DownloadWidget::widget([
    'program' => $program,
    'occurrenceIds' => [$id],
    'checkboxSessionStorage' => '',
]);

// Generate location modal
Modal::begin([
    'id' => 'generate-location-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">add_location</i>
            ' . \Yii::t('app', 'Generate location for') . '
            <span class="generate-location-record-label-modal"></span>
        </h4>',
    'footer' => Html::a(
            'Cancel',
            '',
            [
                'id' => 'cancel-generate-location',
                'title' => \Yii::t('app', 'Cancel'),
                'data-dismiss' => 'modal'
            ]
        ) .
        '&nbsp;&emsp;' .
        Button::widget([
            'label' => 'Confirm',
            'icon' => [
                'name' => 'send',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'confirm-generate-location hidden',
                'href' => '',
                'style' => 'margin-right:10px',
                'title' => \Yii::t('app', 'Confirm generate location')
            ],
        ]) .
        Button::widget([
            'label' => 'Next',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'go-to-preview-generate-location-btn',
                'href' => '',
                'title' => \Yii::t('app', 'Preview generate location')
            ],
        ]) .
        '&emsp;&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
        'size' => 'modal-lg',
        'options' => [
            'data-backdrop' => 'static'
        ],
]);

?>

<div class="generate-location-modal-body parent-panel"></div>
<div class="generate-location-preview-modal-body child-panel"></div>

<?php

Modal::end();

// download planting instructions
$plantingInstructions = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li id='download-planting-arrays-btn' title='Download planting instructions file'>".
        Html::a(
            'Planting instructions','#',
            [
                'data-activates'=>'planting-instructions-dropdown-actions',
                'class'=>'dropdown-button dropdown-button-pjax',
                'data-hover'=>'hover',
                'data-alignment' => 'left'
            ]
        ) .
    "</li>".
    "<ul id='planting-instructions-dropdown-actions' class='dropdown-content' style='marin-left:250pz'>".
        "<li title='Download planting instructions file for the occurrence'><a href='#!' data-entity='occurrence' class='export-planting-instructions-btn hide-loading'>Occurrence</a></li>".
        "<li title='Download planting instructions file for the location'><a href='#!' data-entity='location' class='export-planting-instructions-btn hide-loading'>Location</a></li>".
    "</ul>" : '';

// Download mapping files (partial style)
$exportMappingFilesCsv = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES_FORMAT', 'OCCURRENCES')) ?
    "<li title='Download mapping file in CSV format'>
        <a class='hide-loading replace-url-id' href='" . Url::to(
        [
            '/occurrence/default/export-mapping-files',
            'id' => $id,
            'type' => 'csv',
            'program' => $program,
            'redirectUrl' => $_SERVER['REQUEST_URI'],
        ]
    ) .
    "'>Mapping file (CSV)
        </a>
    </li>" : '';

$exportMappingFilesJson = (
        $isAdmin ||
        (
            Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES_FORMAT', 'OCCURRENCES') &&
            $role != 'COLLABORATOR'
        )
    ) ?
    "<li title='Download mapping file in JSON format'>
        <a class='hide-loading replace-url-id' href='" . Url::to(
        [
            '/occurrence/default/export-mapping-files',
            'id' => $id,
            'type' => 'json',
            'program' => $program,
            'redirectUrl' => $_SERVER['REQUEST_URI'],
        ]
    ) .
    "'>Mapping file (JSON)
        </a>
    </li>" : "";

$exportMetadataAndPlotData = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li id='download-occurrence-metadata-and-plot-data' title='Download occurrence metadata and plot data'>" .
        Html::a(
            '<p
                class="download-functionality-button"
                style="display: inline;"
            >Metadata and plot data</p>',
            '#',
            [
                'class' => '
                    download-widget-entry-point-button
                    dropdown-button-pjax
                ',
                'data-modal-header' => 'Metadata and Plot Data',
                /* The array (optional) elements below represent which category of variables
                * should be displayed in the Download Widget modal UIs
                */
                'data-entities' => json_encode([
                    'experiment',
                    'occurrence',
                    'entry',
                    'germplasm',
                    'plot',
                    'planting_protocol',
                    'management_protocol',
                    'trait'
                ]),
                'data-filename-abbrev' => 'METADATA_AND_PLOT_DATA',
                'data-is-in-data-browser-page' => 'false',
                'data-return-url' => 'occurrence/view?program=' . $program,
                'title' => \Yii::t('app', 'Download occurrence level data'),
            ]
        ) .
    "</li>" : '';

$exportOccurrenceDataCollection = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    "<li id='download-occurrence-data-collection' title='Download occurrence data collection file'>" .
        Html::a(
            '<p
                class="download-functionality-button"
                style="display: inline;"
            >Occurrence data collection</p>',
            '#',
            [
                'class' => '
                    download-widget-entry-point-button
                    dropdown-button-pjax
                ',
                'data-modal-header' => 'Occurrence Data Collection',
                /* The array (optional) elements below represent which category of variables
                * should be displayed in the Download Widget modal UIs
                */
                'data-entities' => json_encode([
                    'occurrence',
                    'management_protocol'
                ]),
                'data-filename-abbrev' => 'OCCURRENCE_DATA_COLLECTION',
                'data-is-in-data-browser-page' => 'false',
                'data-return-url' => 'occurrence/view?program=' . $program,
                'title' => \Yii::t('app', 'Download occurrence data collection'),
            ]
        ) .
    "</li>" : '';

$dataCollectionStyle = $dataCollectionClass = '';

if(empty($locationId)){
    $dataCollectionStyle = 'pointer-events:none';
    $dataCollectionClass = 'class="disabled"';
}

$traitDataCollectionFilesBtn = (
        ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) &&
        // If occurrence has either MAPPED or PLANTED status, then display this download option
        (strpos(strtolower($status), 'planted') !== false || strpos(strtolower($status), 'mapped') !== false)
    ) ?
    "<li id='download-data-collection-files-btn' title='Access Data Collection' $dataCollectionClass >" .
            Html::a(
                'Trait data collection',
                '#',
                [
                    'data-activates' => 'data-collection-dropdown-actions',
                    'class' => 'dropdown-button dropdown-button-pjax',
                    'data-hover' => 'hover',
                    'style' => $dataCollectionStyle
                ]
            ) .
    "</li>
    <ul id='data-collection-dropdown-actions' class='dropdown-content' style='marin-left:250pz'>" .
        "<li title='Download data collection file for the Occurrence'><a href='".
            Url::to(
            [
                '/dataCollection/export-workbook/index',
                'program' => $program,
                'locationId' => $locationId,
                'occurrenceId' => $id,
                'source' => 'em-occ'
            ]
        )
        ."'>Occurrence</a></li>" .
        "<li title='Download data collection file for the Location'><a href='".
            Url::to(
            [
                '/dataCollection/export-workbook/index',
                'program' => $program,
                'locationId' => $locationId,
                'source' => 'em-loc'
            ]
        )
        ."''>Location</a></li>" .
    "</ul>" : '';

// Render available download features
$downloadFilesOptions = (
    $exportMappingFilesCsv ||
    $exportMappingFilesJson ||
    $traitDataCollectionFilesBtn ||
    $plantingInstructions ||
    $exportMetadataAndPlotData ||
    $exportOccurrenceDataCollection
) ?
    "<ul id='occurrence-dropdown-button' class='dropdown-content' data-beloworigin='false'>
        $exportMappingFilesCsv
        $exportMappingFilesJson
        $traitDataCollectionFilesBtn
        $plantingInstructions
        $exportMetadataAndPlotData
        $exportOccurrenceDataCollection
    </ul>" : '';

// If user is an Admin or the occurrence's status is not yet completed...
// ...show MANAGE BASIC INFO button
if (
    $isAdmin ||
    (
        $role != 'COLLABORATOR' &&
        !str_contains($status, 'completed') &&
        Yii::$app->access->renderAccess(
            'UPDATE_OCCURRENCE',
            $toolAbbrev,
            $creatorDbId,
            $accessData,
            'write'
        )
    )
) {
    $manageBasicInfoButton = [
        Button::widget([
            'label' => \Yii::t('app', 'Manage Basic Info'),
            'options' => [
                'title' => \Yii::t('app', 'Manage basic information'),
                'style' => [
                    'margin-right' => '4px',
                ],
                'class' => 'manage-experiment',
                'href' => Url::to([
                    'manage/occurrences',
                    'program' => $program,
                    'experimentId' => $experimentId,
                    'occurrenceId' => $id,
                ])
            ],
        ])
    ];
}
// If user is an Admin or the occurrence's status is not yet completed...
// ...show GENERATE LOCATION button
if (
    $isAdmin ||
    (
        $role != 'COLLABORATOR' &&
        str_contains($status, 'created') &&
        Yii::$app->access->renderAccess(
            'GENERATE_LOCATION',
            $toolAbbrev,
            $creatorDbId,
            $accessData,
            'write'
        )
    )
) {
    $generateLocationButton = [
        Button::widget([
            'label' => \Yii::t('app', 'Generate Location'),
            'options' => [
                'class' => 'generate-location-btn',
                'data-target' => '#generate-location-modal',
                'data-toggle' => 'modal',
                'data-name' => htmlspecialchars($occurrenceName),
                'data-id' => $id,
                'title' => \Yii::t('app', 'Generate location'),
                'style' => [
                    'margin-right' => '4px',
                ]
            ],
        ])
    ];
}

// if status is mapped, show commit button
if($role != 'COLLABORATOR' && strpos(strtolower($status), 'mapped') !== false) {
    $commitBtn = [
        Button::widget([
            'label' => 'Commit',
            'options' => [
                'class' => 'white-text commit-occurrence-btn',
                'data-target' => '#commit-occurrence-modal',
                'data-toggle' => 'modal',
                'data-name' => htmlspecialchars($name['value']),
                'data-id' => $id,
                'href' => '#!',
                'title' => \Yii::t('app', 'Commit occurrence')
            ],
        ]).'&nbsp;&nbsp;'
    ];
}

$printoutsBtn = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_PRINTOUTS', 'OCCURRENCES')) ? [
    '&nbsp;&nbsp;' . Button::widget([
        'label' => '',
        'icon' => [
            'name' => 'print'
        ],
        'options' => [
            'class' => 'dropdown-trigger grey lighten-3 black-text no-label',
            'href' => '#!',
            'data-activates' => 'occurrence-printouts-dropdown-button',
            'data-beloworigin' => 'true',
            'data-constrainwidth' => 'false',
            'title' => \Yii::t('app', 'Printouts')
        ],
    ]) .
    "<ul id='occurrence-printouts-dropdown-button' class='dropdown-content' data-beloworigin='false'>
        <li><a href='#!' style='cursor: not-allowed'>Plot tags</a></li>
        <li><a href='#!' style='cursor: not-allowed'>Harvest tags</a></li>
    </ul>"
] : [];

$downloadFilesBtn = ($downloadFilesOptions) ? [
    Button::widget([
        'label' => '',
        'icon' => [
            'name' => 'file_download'
        ],
        'options' => [
            'class' => 'dropdown-trigger grey lighten-3 black-text no-label',
            'href' => '#!',
            'data-activates' => 'occurrence-dropdown-button',
            'data-beloworigin' => 'true',
            'data-constrainwidth' => 'false',
            'title' => \Yii::t('app', 'Download files')
        ],
    ]) .
    $downloadFilesOptions
] : [];

$mainButtons = [
    $manageBasicInfoButton,
    $generateLocationButton,
    $commitBtn,
    $printoutsBtn,
    $downloadFilesBtn,
];

// Disable buttons if there's no data
if (empty($id) || empty($data) || empty($code) || empty($name)) {
    Yii::warning(substr('Missing occurrence data ' . json_encode(['occurrenceDbId'=>$id,'program'=>$program,
            'data'=>$data]), 0, 1000), __METHOD__);
    $mainButtons = [];
}

?>

<div class="browser-detail-view-viewer-panel">
    <div class="browser-detail-view-viewer-inner">
    
        <div class="viewer-body">
            <!-- First column -->
            <div class="col col-md-8">
                
                <div class="col col-md-12">

                    <div class="viewer-display-name">
                        <h3><?= $codeValue ?></h3>
                    </div>

                    <!-- status to be displayed for smaller screens -->
                    <div class="viewer-status for-small-screen">

                        <?php
                            foreach ($statusArr as $key => $value) {
                                echo '<span class="badge new '. $value['statusClass'] .'">'.htmlspecialchars($value['label']).'</span>';
                            }
                        ?>
                    </div>

                    <?php
                        // if there are main action buttons
                        if(!empty($mainButtons)){
                            echo '<div class="viewer-main-buttons with-margin-bottom">';
                                foreach ($mainButtons as $key => $value) {
                                    echo $value[0] ?? null;
                                }
                            echo '</div>';
                        }
                    ?>
                    <div class="viewer-description viewer-detail-item">
                        <div class="viewer-description viewer-label">
                            Description
                        </div>

                        <div class="viewer-description-value viewer-value">
                            <?= !empty($description) ? htmlspecialchars($description) : $notSet ?>
                        </div>
                    </div>

                </div>

                <div class="viewer-primary-info">

                    <?php
                        // primary information
                        if(isset($primaryInfo) && !empty($primaryInfo)){
                            
                            $primaryInfoStr = '';
                            foreach ($primaryInfo as $key => $value) {
                                $label = htmlspecialchars($value['label']);
                                $val = (!empty($value['value']) || $value['value'] == '0') ? htmlspecialchars($value['value']) : $notSet;
                                $title = (!empty($value['title']) && isset($value['title'])) ? htmlspecialchars($value['title']) : '';
                                $val = (!empty($value['url']) && isset($value['url'])) ? 
                                    '<b><a href="'.$value['url'].'">'. $val .'</a></b>' : $val;
                                $attribute = (!empty($value['attribute']) && isset($value['attribute'])) ? $value['attribute'] : '';
                                
                                $colClass = '6';
                                if(in_array($attribute, ['experiments', 'occurrences']))
                                    $colClass = '12';
                                // If the label is "Package Harvested", run code only for this label
                                if($label == "Packages Harvested"){
                                    $val2 = json_decode($value['value'], true);
                                    $packagesHarvestedSpecifiedLabel = '<a title="View Occurrence in HM Occurrence Tab" href="'.$val2['urlHM'].'">'. $label .'</a>';
                                    $packagesHarvestedSpecifiedValue = 'Cross <a title="View Crosses in HM Management Tab" href="'.$val2['crossURL'].'"><b>'. $val2['cross'] .'</b></a> | Plot <a title="View Plots in HM Management Tab" href="'.$val2['plotURL'].'"><b>'. $val2['plot'] .'</b></a>';
                                    $detailInfo = <<<EOD
                                    <div class="col col-lg-$colClass">
                                        <div class="viewer-primary-info viewer-detail-item">
                                            <div class="viewer-label">
                                                $packagesHarvestedSpecifiedLabel
                                            </div>
                                            <div class="viewer-value" title="$title">
                                                $packagesHarvestedSpecifiedValue
                                            </div>
                                        </div>
                                    </div>
                                    EOD;
                                }
                                else{
                                    $detailInfo = <<<EOD
                                    <div class="col col-lg-$colClass">
                                        <div class="viewer-primary-info viewer-detail-item">
                                            <div class="viewer-label">
                                                $label
                                            </div>
                                            <div class="viewer-value" title="$title">
                                                $val
                                            </div>
                                        </div>
                                    </div>
                                    EOD;
                                }

                                $primaryInfoStr .= $detailInfo;

                                if($key % 2){
                                    $primaryInfoStr .= '<div class="row no-margin"></div>';
                                }
                            }

                            echo $primaryInfoStr;
                        }

                    ?>
                </div>
                <div class="clearfix"></div>
                <!-- For status monitoring -->
                <div id="more-info-container">
                    <?php
                        echo Button::widget([
                            'label' => \Yii::t('app', 'View More Info'),
                            'options' => [
                                'title' => \Yii::t('app', 'Load status monitoring'),
                                'style' => [
                                    'margin-right' => '4px',
                                ],
                                'id' => 'view-more-info-btn'
                            ],
                        ])
                    ?>
                </div>
            </div>

            <!-- Second column -->
            <div class="col col-md-4">
                <!-- status to be displayed for wider screens -->
                <div class="viewer-status for-wide-screen">

                    <?php
                            foreach ($statusArr as $key => $value) {
                                echo '<span class="badge new '. $value['statusClass'] .'">'.htmlspecialchars($value['label']).'</span>';
                            }
                        ?>
                </div>

            <?php
                // secondary information
                if(isset($secondaryInfo) && !empty($secondaryInfo)){

                    $secondaryInfoStr = '';
                    foreach ($secondaryInfo as $key => $value) {
                        $label = htmlspecialchars($value['label']);
                        $val = !empty($value['value']) ? htmlspecialchars($value['value']) : $notSet;

                        $secondaryInfoStr .= <<<EOD
                            <div class="viewer-secondary-info viewer-detail-item">
                                <div class="viewer-label">
                                    $label
                                </div>
                                <div class="viewer-value">
                                    $val
                                </div>
                            </div>
EOD;
                    }

                    echo $secondaryInfoStr;
                }

                echo '<div class="divider margin-bottom"></div>';

                // autdit information
                if(isset($auditInfo) && !empty($auditInfo)){
                    foreach ($auditInfo as $key => $value) {
                        if(!empty($value['value']) && isset($value['value'])){
                            $label = $value['label'];
                            $val = $value['value'];

                            echo <<<EOD
                                <div class="viewer-audit-info text-muted">
                                    $label $val
                                </div>
EOD;
                        }
                    }
                }

            ?>
            
            </div>
        </div>
    </div>
</div>

<?php

// commit occurrence modal
Modal::begin([
    'id' => 'commit-occurrence-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">check</i>
            Commit    
            <span class="commit-record-label-modal"></span>
        </h4>',
    'footer' => 
        Html::a('Cancel', ['#'], [
            'id' => 'cancel-save', 
            'title' => \Yii::t('app', 'Cancel'),
            'data-dismiss'=> 'modal'
        ]) . '&emsp;&nbsp;'.
        Button::widget([
            'label' => 'Confirm',
            'icon' => [
                'name' => 'send',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'commit-occurrence-confirm-btn',
                'href' => '#!',
                'title' => \Yii::t('app', 'Confirm commit occurrence')
            ],
        ])
        . '&emsp;&emsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    // 'size' =>'modal-lg',
    'options' => [
        'data-backdrop' => 'static'
    ],
]);

?>
<div class="commit-occurrence-modal-body">
    <div class="commit-confirmation-panel"><i class="fa fa-warning orange-text text-darken-3 fa-2x"></i>
        You are about to commit the occurrence. Upon confirmation, the occurrence cannot be modified anymore and the location will be operational. Click confirm to proceed.
    </div>
    <div class="commit-confirmation-loading-panel hidden">
        <div class="margin-right-big loading">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
        </div>
    </div>
</div>
<?php Modal::end();

// commit occurrence url
$commitOccurrenceUrl = Url::to(['/occurrence/manage/commit-occurrence']);
// export planting instructions file url
$exportPlantInsUrl = Url::to(['/occurrence/default/export-planting-instructions']);
// render form upload mapped plots url
$renderFormUploadPlantingArraysUrl = Url::to(['/occurrence/upload/render-upload-planting-arrays-form']);
// render occurrence status monitoring info url
$renderOccurrenceStatusMonitoringInfoUrl = Url::to(['/occurrence/view/render-occurrence-status-monitoring-info','id' => $id]);
// redirect user to Occurrences browser URL
$redirectToOccurrencesBrowserUrl = Url::to([
    '/occurrence/view/redirect-to-occurrences-browser',
    'program' => $program,
    'id' => $id,
]);

// css
$this->registerCssFile("@web/css/browser/page-view.css");

$this->registerCss('
    .dropdown-content {
        overflow-y: visible;
    }

    .dropdown-content .dropdown-content {
        margin-left: 100%;
    }
');

// Register this JS file into basic.php
$this->registerJsFile(
    "@web/js/browser/detail-view.js",
    [
        'depends' => ['\yii\web\JqueryAsset'],
        'position' => \yii\web\View::POS_END
    ]
);

// These set the variables to be used in b4r/js/browser/detail-view.js
// Do NOT change the variable names below
$this->registerJs(
    <<<JS
    program = '$program'
JS,
    \yii\web\View::POS_HEAD
);

$this->registerJs(<<<JS
    var program = '$program';
    var redirectToOccurrencesBrowserUrl = '$redirectToOccurrencesBrowserUrl'

    // render button dropdown
    $('.dropdown-trigger').dropdown();

    // hide loading indicator
    $(document).on('click','.hide-loading',function(){
        $('#system-loading-indicator').css('display','block');
        setTimeout(function() {
            $('#system-loading-indicator').css('display','none');
        },2000);
    });

    // Manage experiment
    $(document).on('click', '.manage-experiment', function(e){
        var url = $(this).attr('href');

        window.location = url;
    });

    // Render generate location form
    $(document).on('click', '.generate-location-btn', function renderGenerateLocationForm (e) {
        e.preventDefault()
        e.stopImmediatePropagation()
        let id = '$id'
        let name = '$occurrenceName'
        let modalBody = '.generate-location-modal-body'

        // Show loading bar first
        $(modalBody).html('<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>')

        // Make the name print safe, then...
        // ...add it to the modal header as text
        let safe = name.replace(/</g,"&lt;").replace(/>/g,"&gt;")
        $('.generate-location-record-label-modal').html(safe)

        let els = document.querySelectorAll('.go-to-preview-generate-location-btn')
        for (let i = 0; i < els.length; i++) {
            els[i].setAttribute("data-id", id)
        }

        $.ajax({
            url: '$renderFormUploadPlantingArraysUrl',
            type: 'post',
            dataType: 'json',
            data:{
                action: 'generate-location',
            },
            success: function (response) {
                $(modalBody).html(response)
                $('.go-to-preview-generate-location-btn').removeClass('disabled');
            },
            error: function () {
                let errorMessage = '<i>There was a problem loading the content.</i>'
                $(modalBody).html(errorMessage)
                $('.go-to-preview-generate-location-btn').addClass('disabled');
            }
        })

    })

    // Render occurrence status monitoring info
    $(document).on('click', '#view-more-info-btn', function(e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        $('#more-info-container').html(preloader.replace('text-center',''))

        $.ajax({
            url: '$renderOccurrenceStatusMonitoringInfoUrl',
            type: 'post',
            dataType: 'json',
            data:{},
            success: function (response) {
                $('#more-info-container').html(response)
            },
            error: function () {
            }
        })

    })

    // auto generate location name
    $(document).on('click', '.default-namegen-switch', function(e) {
        var fid = this.id;
        var fieldId = fid.split('generate-');

        if($(this).prop('checked') == false){
            $('#'+fieldId[1]).removeClass('disabled');
            $('#'+fieldId[1]).removeClass('generated-pattern');
            $('#'+fieldId[1]).removeAttr('readonly');
            $('.go-to-preview-generate-location-btn').addClass('disabled'); 
            $('.browse-btn').addClass('disabled');
            $('.fileinput-upload-button').addClass('disabled');
        } else {
            $('#'+fieldId[1]).addClass('disabled');
            $('#'+fieldId[1]).addClass('generated-pattern');
            $('#'+fieldId[1]).attr('readonly','readonly');
            $('#'+fieldId[1]).val('');
            $('.go-to-preview-generate-location-btn').removeClass('disabled');
            $('.browse-btn').removeClass('disabled');
            $('.fileinput-upload-button').removeClass('disabled');
        }
    })

    // render occurrence name in commit modal
    $(document).on('click','.commit-occurrence-btn', function(){
        var obj = $(this);
        var name = obj.data('name');
        var id = obj.data('id');

        // make the name print safe
        var safe = name.replace(/</g,"&lt;").replace(/>/g,"&gt;");
        // add it to the modal header as text
        $('.commit-record-label-modal').html(safe);

        $('.commit-occurrence-confirm-btn').attr('data-id', id);
        $('.commit-occurrence-confirm-btn').attr('data-name', name);
    });

    // commit the occurrence
    $(document).on('click','.commit-occurrence-confirm-btn', function(){
        var obj = $(this);
        var id = obj.attr('data-id');
        var name = obj.attr('data-name');
        // make the name print safe
        var safe = name.replace(/</g,"&lt;").replace(/>/g,"&gt;");

        // show loading indicator
        $('.commit-confirmation-loading-panel').removeClass('hidden');
        $('.commit-confirmation-panel').addClass('hidden');

        //get selected record ID
        var searchParams = new URLSearchParams(window.location.search);
        var selectedRecordId = searchParams.get('selectedRecordId');
        
        let modalBody = '.commit-occurrence-modal-body'

        setTimeout(function(){
            $.ajax({
                url: '$commitOccurrenceUrl',
                type: 'post',
                dataType: 'json',
                data:{
                    id: id
                },
                success: function(response){
                    // remove initial toast
                    $('.toast').css('display','none');
                    
                    let color, icon, message = ''
                    if (!response.success) {
                        color = 'red'
                        icon = 'close'
                        message = 'There seems to be a problem while committing occurrence. '
                    } else {
                        color = "green";
                        icon = "check";
                        message = "Successfully committed "+ safe +".";
                    }

                    // display notification
                    var notif = "<i class='material-icons "+ color +"-text left'>"+ icon +"</i> <span class='white-text'>"+ message +"</span>";
                    Materialize.toast(notif, 5000);
                    
                    $('#commit-occurrence-modal').modal('hide');
                    
                    // refresh gird view and view record to reflect changes
                    $('#browser-detail-view-search-input').val('');
                    $('.browser-detail-view-submit-btn').trigger('click');
                    
                    // refresh content of commit confirmation modal
                    $('.commit-confirmation-loading-panel').addClass('hidden');
                    $('.commit-confirmation-panel').removeClass('hidden');

                    location.reload();
                },
                error: function(){
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBody).html(errorMessage);
                }
            });
        }, 50);
    });

    // export planting instructions
    $(document).on('click', '.export-planting-instructions-btn', function (e) {
        let id = $id
        let exportPlantInsUrl = '$exportPlantInsUrl'
        let obj = $(this);
        let entity = obj.data('entity');
    
        let homeUrl = window.location.href
        let plantInsUrl =
            window.location.origin +
            exportPlantInsUrl +
            '?id=' + id +
            '&type=csv' +
            '&entity='+entity +
            '&program='+program +
            '&redirectUrl='+encodeURIComponent(homeUrl)

        window.location.replace(plantInsUrl)

    });

    // Display Toast notification
    $('.download-widget-download-button').on('click', function (e) {
        const entity = $(this).data('filename-abbrev')

        $.ajax({
            url: redirectToOccurrencesBrowserUrl,
            type: 'post',
            dataType: 'json',
            data: {
                entity: entity,
            },
            success: function (response) {},
            error: function () {}
        })
    })
JS
);