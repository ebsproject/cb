<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders protocols of an Experiment within View Occurrence
 */

use kartik\tabs\TabsX;

echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $occurrenceDbId,
    'program' => $program,
    'occurrenceName' => $occurrenceName,
    'active' => 'protocol',
]);

$viewArrCount = sizeof($viewArr);

if ($viewArrCount > 0) {
    $items = [];

    // Prepare each View file for the Protocols
    foreach ($viewArr as $key => $value) {
        $isActive = ($key == 0) ? true : false;
        $filename = $value['action_id'];

        // View file exists
        if (isset($filename)) {
            $content = Yii::$app->controller->renderPartial(
                "@app/modules/experimentCreation/views/protocol/$filename.php",
                [
                    'id' => $experimentDbId,
                    'program' => $program,
                    'processId' => $dataProcessDbId,
                    'isViewOnly' => '1',
                    'occurrenceDbId' => $occurrenceDbId,
                ]
            );

            $items []= [
                'label' =>
                    '<i class="' .$value['item_icon'] . '" style="margin-right: 8px;"></i>' .
                    Yii::t('app',$value['display_name']),
                'active' => $isActive,
                'content' => $content,
                'linkOptions'=> ['class'=>"pr-tabs pr-$filename-tab"]
            ];
        }
    }

    // Render the side tabs and content representing their respective Protocol
    echo TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_LEFT,
        'encodeLabels' => false,
        'pluginOptions' => [
            'enableCache' => false
        ]
    ]);
} else {
    Yii::$app->session->setFlash('error',\Yii::t('app','No protocols found in the database configuration. Refresh the page or please contact web admin.'));
}

$this->registerCssFile("@web/css/box.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'box-css');
?>