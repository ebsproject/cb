<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders management variable list display
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;

// Variables columns
$columns = [
    [ // Row Number
        'class' => 'kartik\grid\SerialColumn',
        'header' => '',
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [ // Variable Label (Management Attribute Label)
        'attribute' => 'label',
        'format' => 'raw',
        'vAlign' => 'middle',
        'value' => function ($data) {
            return Html::a($data['label'],
                '', 
                [
                    'id' => $data['variableDbId'],
                    'label' => $data['label'],
                    'data-toggle' => 'modal',
                    'data-target'=> '#view-management-attribute-modal',
                    'class' => 'view-management-attribute'
                ]
            );
        },
        'contentOptions' => function ($data) {
            return [
                'class' => 'order_tr',
                'data-varId' => $data['variableDbId']
            ];
        },
        'width' => '15%',
    ],
    [ // Variable Name (Management Attribute Name)
        'attribute' => 'displayName',
        'label' => 'Name',
        'format' => 'raw',
        'vAlign' => 'middle',
        'width' => '30%',
    ],
    [ // Remarks/Instructions
        'attribute' => 'remarks',
        'label' => 'Instructions',
        'format'  => 'raw',
        'vAlign' => 'middle',
        'value' => function ($data) {

            return $data['remarks'];
        },
        'width' => '55%',
    ],
];

$panel = [
    'heading' => '<i class="material-icons left">folder_special</i> '.\Yii::t('app', 'Selected management attributes'),
];
// Variables browser
DynaGrid::begin([
    'options' => ['id'=>'management-list-selected-vars-grid'],
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => 'management-list-selected-vars-grid',
        'dataProvider' => $dataProvider,
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
                'id' => 'management-list-selected-vars-grid'
            ],
        ],
        'panel' => $panel,
        'responsiveWrap' => true,
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'toolbar' => [],
    ],
]);
DynaGrid::end();

// View variable modal
Modal::begin([
    'id' => 'view-management-attribute-modal',
    'header' => '<h4 id="view-management-attribute-label"></h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Ok'),
            null,
            [
                'data-dismiss' => 'modal',
                'class' => 'btn btn-primary waves-effect waves-light modal-close'
            ]
        ) . '&emsp;',
    'closeButton' => ['class' => 'hidden'],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);
echo '<div class="view-management-attribute-modal-body"></div>';
Modal::end();

$variableUrl = Url::to(['/variable/default/view-info']);

$this->registerJs(<<<JS
    // View variable info
    $(document).on('click', '.view-management-attribute', function (e) {
        var obj = $(this)
        var varId = obj.attr('id')
        var varLabel = obj.attr('label')

        $('#view-management-attribute-label').html('<i class="fa fa-info-circle"></i> ' + varLabel + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
        $('.view-management-attribute-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>')

        // Load content of view variable information
        setTimeout(function () {
            $.ajax({
                url: '$variableUrl',
                data: {
                    id: varId
                },
                type: 'POST',
                async: true,
                success: function (data) {
                    $('#view-management-attribute-modal').modal('show')
                    $('.view-management-attribute-modal-body').html(data)
                },
                error: function () {
                    $('.view-management-attribute-modal-body').html('<i>There was a problem while loading record information.</i>')
                }
            })
        },300)
    })

    $('#view-management-attribute-modal').on('hidden.bs.modal', function () {
        // Clear modal body
        $('.view-management-attribute-modal-body').empty()
    })
JS
);
?>
