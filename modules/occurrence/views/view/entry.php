<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders entries of an occurrence
 */

use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;
use kartik\dynagrid\DynaGrid;
use kartik\select2\Select2;
use macgyer\yii2materializecss\widgets\Button;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;


Yii::$app->session->set('occurrence-entry-filters' . $id, $filters);
Yii::$app->session->set('occurrence-entry-param-sort' . $id, $paramSort);
Yii::$app->session->set('program', $program);
Yii::$app->session->set('occurrence-entries-program', $program);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
$isAdmin = Yii::$app->session->get('isAdmin');

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '$browserId',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $id,
    'program' => $program,
    'occurrenceName' => $occurrenceName,
    'active' => 'entry'
]);

// Set parentage column's display with "truncated" class
foreach ($mainCols as $key => $value) {
    if ($value['attribute'] === 'parentage') {
        $mainCols[$key]['format'] = 'raw';
        $mainCols[$key]['value'] = function ($model) {
            return "<div
                title='{$model['parentage']}'
                class='truncated'
            >{$model['parentage']}</div>";
        };
    }
    $mainCols[$key]['headerOptions'] = ['id' => $mainCols[$key]['attribute']];
}

$checkboxCol = [
    [
        //checkbox
        'label' => '',
        'header' => '
            <input type="checkbox" class="filled-in" id="checkbox-select-all" />
            <label for="checkbox-select-all" title="Select all entry records"></label>
        ',
        'content' => function ($data) {
            $entryDbId = $data['entryDbId'];
            $entryNumber = $data['entryNumber'];
            $germplasmDbId = $data['germplasmDbId'];
            $packageDbId = $data['packageDbId'];
            $occurrenceName = $data['occurrenceName'];
            $germplasmName = $data['entryName'];

            $checkbox = "<input
                id='$entryDbId'
                class='grid-select filled-in'
                type='checkbox'
                data-entry-number='$entryNumber'
                data-occurrence-name='$occurrenceName'
                data-germplasm-id='$germplasmDbId'
                data-package-id='$packageDbId'
                data-germplasm-name='$germplasmName'
            />";

            return $checkbox . '
        <label for="' . $entryDbId . '"></label>
      ';
        },
        'hAlign' => 'center',
        'vAlign' => 'top',
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
];

$columns = array_merge($checkboxCol, $mainCols);

// Replace Entry Dropdown
$replaceEntryDropdownTitleText = Yii::t('app', 'Replace entry');
$replaceEntryDropdown = ($isPacked) ? "" : "
    <a
        title='$replaceEntryDropdownTitleText'
        id='replace-entry-dropdown-btn'
        class='dropdown-button btn dropdown-button-pjax'
        href=''
        data-activates='replace-entry-dropdown-list'
        data-beloworigin='true'
        data-constrainwidth='false'
        style='
            margin-right: 6px;
        '
        disabled
    >
    </span> EDIT </span>
    </span> <span class='caret'> </span>
    </a>
        <ul
            id='replace-entry-dropdown-list'
            class='dropdown-content'
            data-beloworigin='false'
        >
            <li
                id='replace-occurrence-entry-btn'
                class='hide-loading'
                title='Replace entry at the occurrence level'
                style='
                    font-size: 20px;
                    vertical-align: -4px;
                    margin-right: 4px;
                '
            >" .
            // Replace occurrence entry option/item
            Html::a(
                '<p style="display: inline;">Replace occurrence entry</p>',
                null
            ) .
            "</li>
            <li
                id='replace-experiment-entry-btn'
                class='disabled hide-loading'
                title='Replace entry at the experiment level'
                style='
                    font-size: 20px;
                    vertical-align: -4px;
                    margin-right: 4px;
                '
            >" .
            // Replace experiment entry option/item
            Html::a('<p style="display: inline; color: #808080;">Replace experiment entry</p>') .
            "</li>
        </ul>";

$downloadEntriesBtn = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    Html::a(
        '<i class="material-icons inline-material-icon">file_download</i>',
        '',
        [
            'data-pjax' => true,
            'id' => 'export-entries-btn',
            'class' => 'btn btn-default',
            'title' => \Yii::t('app', 'Export all entries and measured traits in CSV format')
        ]
    ) : '';

$saveAsNewListDropdown = "
    <a 
        id='entry-save-list-btn'
        title='Save as New List'
        class='dropdown-button btn dropdown-button-pjax'
        href=''
        data-activates='manage-entry-dropdown-save'
        data-beloworigin='true'
        data-constrainwidth='false'
    >
        <i class='material-icons' style='vertical-align:bottom'>
            add_shopping_cart
        </i>
    </a>
";

// modal for saving as new list
Modal::begin([
    'id' => 'experiment_manager-save-list-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">add</i>' . Yii::t('app', 'Save as New List') . '</h4>',
    'footer' => Html::a('Cancel', '', ['id' => 'cancel-save', 'data-dismiss' => 'modal']) . '&emsp;&emsp;'
        . Html::a(
            Yii::t('app','Submit'),
            '',
            [
                'class' => 'btn btn-primary waves-effect waves-light disabled',
                'url' => '',
                'id' => 'experiment_manager-save-list-confirm-btn'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);

echo '<p id="export-workbook-save-list-type-info"></p>';

echo '<p id="export-workbook-save-list-preview-error"></p>';

echo '<div class="experiment_manager-modal-loading"></div>';

$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'create-saved-list-form',
    'action' => ['/occurrence/view/entry'],
    'fieldConfig' => function ($model, $attribute) {
        if (in_array($attribute, ['abbrev', 'display_name', 'name', 'type'])) {
            return ['options' => ['class' => 'input-field form-group col-md-6']];
        } else {
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    }
]);

$model = \Yii::$container->get('app\models\PlatformList');
echo
    '<div class="modal-notif"></div>' . //modal notification field

        $form->field($model, 'name')->textInput([
            'maxlength' => true,
            'id' => 'entry-list-name',
            'title' => 'Name identifier of the list',
            'oninput' => 'js:
      $("#entry-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());
      $("#entry-list-display_name").val(this.value);
      Materialize.updateTextFields();',
        ]) .
        $form->field($model, 'abbrev')->textInput([
            'maxlength' => true,
            'id' => 'entry-list-abbrev',
            'title' => 'Short name identifier or abbreviation of the list',
            'oninput' => 'js: $("#entry-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());',
        ]) .
        $form->field($model, 'display_name')->textInput([
            'maxlength' => true,
            'id' => 'entry-list-display_name',
            'title' => 'Name of the list to show to users',
        ]) .
        '<div class="input-field form-group col-md-6 field-entry-list-type required">' .
        '<label class="control-label" for="entry-list-type" style="top: -100%">Type</label>' .
        Select2::widget([
            'name' => 'MyList[type]',
            'data' => [
                'germplasm' => 'Germplasm',
                'package' => 'Package'
            ],
            'options' => [
                'placeholder' => 'Choose a list type',
                'id' => 'entry-list-type',
                'class' => 'form-control',
                'required' => true,
                'data-name' => 'Type',
            ],
            'hideSearch' => true,
        ]) .
        '</div>' .
        $form->field($model, 'description')->textarea([
            'class' => 'materialize-textarea',
            'id' => 'entry-list-description',
            'title' => 'Description about the list',
        ]) .
        $form->field($model, 'remarks')->textarea([
            'class' => 'materialize-textarea',
            'title' => 'Additional details about the list',
            'row' => 2,
            'id' => 'entry-list-remarks',
        ]);

ActiveForm::end();

echo '
    <div class="experiment_manager-total-count">
        <span>
          Number of entries to be saved as a list: <b><span id="save-item-count"></span></b>
        </span>
    </div>';

Modal::end();

// modal for replace entry form
Modal::begin([
    'id'=> 'replace-entry-modal',
    'header' => '<h4 class="replace-entry-header"><i class="material-icons inline-material-icon" style="margin-right: 4px;">swap_horiz</i>' . \Yii::t('app','Replace entry').'</h4>',
    'closeButton' => [
        'class'=>'close cancel-replace-entry-btn',
    ],
    'footer' => Html::a(\Yii::t('app','Cancel'),['#'],[
            'class'=>'cancel-replace-entry-btn',
            'data-dismiss'=>'modal'
        ]).'&emsp;&nbsp;'.
        Button::widget([
            'label' => 'Next',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'replace-entry-next-btn btn-primary waves-effect waves-light',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Go to next step')
            ],
        ]).
        Button::widget([
            'label' => 'Next',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'select-next-btn btn-primary waves-effect waves-light hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Go to preview')
            ],
        ]).
        Button::widget([
            'label' => 'Confirm',
            'icon' => [
                'name' => 'send',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'select-confirm-btn btn-primary waves-effect waves-light hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Confirm replacement')
            ],
        ]),
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static', 'tabindex' => false],
]);
    echo '<div style="margin: 10px 20px 5px 28px;"><div id="modal-progress" class="progress hidden"><div class="indeterminate"></div></div></div>';

    echo '<div class="replace-entry-modal-content parent-panel"></div>';
    echo '<div class="replace-entry-form-modal-content child-panel"></div>';
    echo '<div class="replace-entry-preview-modal-content"></div>';
Modal::end();

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => "$browserId-id",
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => "$browserId-id"],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsiveWrap' => false,
        'panel' => [
            'heading' => false,
            'before' =>
            \Yii::t('app', 'This is the browser for the entries of the occurrence.') .
                '{summary}' .
                '<br/>
        <p id="selected-items-paragraph" class = "pull-right" style = "margin-right: 6px;">
            <b><span id="selected-items-count"> </span></b> <span id="selected-items-text"> </span>
        </p>',
            'after' => false,
        ],
        'toolbar' =>  [
            [
                'content' =>
                '&nbsp;' .
                    $replaceEntryDropdown .
                    $downloadEntriesBtn .
                    $saveAsNewListDropdown .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        [
                            '/occurrence/view/entry',
                            'program' => $program,
                            'id' => $id
                        ],
                        [
                            'data-pjax' => true,
                            'id' => 'reset-entry-grid',
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid')
                        ]
                    ) .
                    '{dynagridFilter}{dynagridSort}{dynagrid}',
            ]
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-delete',
        'label' => 'Remove',
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

$exportEntriesUrl = Url::to(['/occurrence/view/export-entries']);
$saveAsListUrl = Url::to(['view/save-as-list']);
$retrieveDbIdsUrl = Url::to(['view/retrieve-db-ids' . "?occurrenceId=$id" . "&entity=entry"]);
$prepareDataForCsvDownloadUrl = Url::to(['view/prepare-data-for-csv-download' . "?occurrenceId=$id&entity=entry&program=$program"]);
// render replace entry modal form url
$replaceFormUrl = Url::to(['view/replace-entry-form']);
// render replace entry selection modal form url
$replaceSelectionFormUrl = Url::to(['view/replace-entry-selection-form']);
// render replacement preview url
$replacePreviewUrl = Url::to(['view/render-replacement-preview']);
// confirm replacement url
$confirmReplacementUrl = Url::to(['view/confirm-replacement']);
$harvestedCheckerUrl = Url::to(['view/harvested-checker']);

(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
let occurrenceDbId = '$id'
let experimentDbId = '$experimentDbId'
let checkboxSessionStorageName = 'entryCheckboxIds' + occurrenceDbId
let totalCheckboxesSessionStorageName = 'entryTotalCheckboxIds' + occurrenceDbId
let selectAllModeSessionStorageName = 'entrySelectAllMode' + occurrenceDbId
let retrieveEntryDbIdsAjax = null
let isBGProcessing = false
let saveListMembersThresholdValue = '$saveListMembersThresholdValue'
let browserId = '$browserId'
let occurrenceName = ''
let germplasmName = ''
let entryNumber = ''
let entryIdToReplace = ''
let germplasmIdToReplace = ''
let packageIdToReplace = ''
let selectedReplacementType = ''
let selectedReplacementTypeText = ''
let replacementId = null
let isBulkReplacement = false

sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')

if (!sessionStorage.getItem(checkboxSessionStorageName))
    sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify([]))
if (!sessionStorage.getItem(totalCheckboxesSessionStorageName))
    sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')
if (!sessionStorage.getItem(selectAllModeSessionStorageName))
    sessionStorage.setItem(selectAllModeSessionStorageName, 'include')

let checkedBoxes = JSON.parse(sessionStorage.getItem(checkboxSessionStorageName))
let totalCheckboxes = parseInt(sessionStorage.getItem(totalCheckboxesSessionStorageName))
let selectAllMode = sessionStorage.getItem(selectAllModeSessionStorageName)
let isSelectAllChecked = null
let loadingIndicatorHtml = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>'
const prepareDataForCsvDownloadUrl = '$prepareDataForCsvDownloadUrl';
const saveAsListUrl = '$saveAsListUrl';
const exportRecordsThresholdValue = $exportRecordsThresholdValue
const program = '$program'
const totalCount = $totalCount
let filteredTotalCount = totalCount

$(document).ready(function() {
    adjustBrowserPageSize()
    refresh()
})

$(document).on('ready pjax:success', function(e) {
    e.stopPropagation()
    e.preventDefault()
    refresh()
});

function refresh () {
    $('.dropdown-button').dropdown()

    let summary = $(".summary").text()
    if (summary.trim().length) {
        filteredTotalCount = parseInt($('.summary')[0].children[1].innerHTML.split(',').join(''))
        if (filteredTotalCount !== totalCount) {
            // Store filtered plot count
            totalCheckboxes = filteredTotalCount
            sessionStorage.setItem(totalCheckboxesSessionStorageName, filteredTotalCount)
        } else {
            // Store total plot count
            totalCheckboxes = totalCount
            sessionStorage.setItem(totalCheckboxesSessionStorageName, totalCount)
        }
    } else {
        // if no results found
        filteredTotalCount = 0
    }

    if (
        (selectAllMode === 'include' && totalCheckboxes === checkedBoxes.length) ||
        (selectAllMode === 'exclude' && checkedBoxes.length === 0)
    ) { // auto-tick Select All checkbox
        $('#checkbox-select-all').trigger('click')
    } else if (checkedBoxes.length > 0) { // auto-tick tracked checkedboxes
        $('#checkbox-select-all').prop('checked', false)
        $(".grid-select:checkbox").each(function () {
            let entryDbId = this.id
            
            if (
                (selectAllMode === 'include' && checkedBoxes.includes(entryDbId)) ||
                (selectAllMode === 'exclude' && !checkedBoxes.includes(entryDbId))
            ) {
                $(this)
                    .prop('checked',true)
                    .parent("td")
                    .parent("tr")
                    .addClass('grey lighten-4')
            }
        });

        compareCheckBoxCountWithTotalCount()
    }

    // set dynamic height for the browser
    let maxHeight = ($(window).height() - 320)
    $('.kv-grid-wrapper').css('height', maxHeight)

    $('#entry-save-list-btn').on('click', function (e) {
        let saveItemCount = 0

        if (selectAllMode === 'include') {
            saveItemCount = (checkedBoxes.length === 0) ? $('.summary')[0].children[1].innerHTML : checkedBoxes.length
        } else if (selectAllMode === 'exclude') {
            saveItemCount = totalCheckboxes - checkedBoxes.length
        }

        $('#save-item-count').html(saveItemCount)
        $('#experiment_manager-save-list-modal').modal('show')
    })

    // validate if all required fields are specified
    $('.form-control').bind("change keyup input", function () {
        let abbrev = $('#entry-list-abbrev').val()
        let name = $('#entry-list-name').val()
        let displayName = $('#entry-list-display_name').val()
        let type = $('#entry-list-type').val()

        if (abbrev != '' && name != '' && displayName != '' && type != '') {
            $('#experiment_manager-save-list-confirm-btn').removeClass('disabled')
        } else {
            $('#experiment_manager-save-list-confirm-btn').addClass('disabled')
        }
    })

    // confirm save list
    $('#experiment_manager-save-list-confirm-btn').on('click', function (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        $('.experiment_manager-modal-loading').html(loadingIndicatorHtml)
        $(this).addClass('disabled')

        let abbrev = $('#entry-list-abbrev').val()
        let name = $('#entry-list-name').val()
        let displayName = $('#entry-list-display_name').val()
        let listType = $('#entry-list-type').val()
        let description = $('#entry-list-description').val()
        let remarks = $('#entry-list-remarks').val()

        if (isBGProcessing)
            $('#cancel-save').css({'pointer-events': 'none'}) // disable cancel button

        $.ajax({
            type: 'POST',
            url: saveAsListUrl,
            data: {
                occurrenceId: occurrenceDbId,
                dataType: 'entries',
                abbrev: abbrev,
                name: name,
                displayName: displayName,
                listType: listType,
                description: description,
                remarks: remarks,
                dbIds: (checkedBoxes.length === $totalCount) ? JSON.stringify([]) : JSON.stringify(checkedBoxes),
                isSelectAllChecked: isSelectAllChecked,
                selectAllMode: selectAllMode
            },
            dataType: 'json',
            async: true,
            success: function (response) {
                $('#cancel-save').css({'pointer-events': ''}) // disable cancel button
                let message

                if (response['success']) {
                    message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i>' + response['message']
                    $('#experiment_manager-save-list-modal').modal('hide')
                } else {
                    message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + response['message']
                }

                Materialize.toast(message, 5000)
                $('.experiment_manager-modal-loading').html('')
                $('#experiment_manager-save-list-confirm-btn').removeClass('disabled')
            },
            error: function (response) {
                $('#cancel-save').css({'pointer-events': ''}) // disable cancel button
            }
        })
    })

    $('#export-entries-btn').off('click').on('click', function (e) {
        e.preventDefault()

        if (totalCheckboxes >= exportRecordsThresholdValue) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: prepareDataForCsvDownloadUrl,
                data: {},
                success: async function (ajaxResponse) {},
                error: function (xhr, error, status) {}
            })
        } else {
            let exportEntriesUrl = '$exportEntriesUrl'
            let expEntUrl = window.location.origin + exportEntriesUrl + '?program=' + program + '&id=' + occurrenceDbId

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'

            $('.toast').css('display','none')

            Materialize.toast(message, 5000)
            window.location.replace(expEntUrl)

            $('#system-loading-indicator').html('')
        }
    })

    $('#replace-occurrence-entry-btn').on('click', function clickReplaceOccurrenceEntryButton (e) {
        $.ajax({
            url: '$harvestedCheckerUrl',
            type: 'post',
            data: {
                'id': occurrenceDbId
            },
            success: function (response) {
                // Parse response
                response = JSON.parse(response)
                if(response.success){
                    $('#replace-entry-modal').modal('show')
                    replaceOccurrenceForm()
                }
                else {
                    showMessage(response.message, response.state);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.progress').addClass('hidden')
                $('.replace-entry-modal-content').html('<i>There was a problem while loading the content.</i>')
            },
        })
    })

    //  Render replace occurrence modal
    function replaceOccurrenceForm(){
        const obj = $('#' + checkedBoxes[0])

        occurrenceName = obj.data('occurrence-name')
        germplasmName = obj.data('germplasm-name')
        entryNumber = obj.data('entry-number')
        entryIdToReplace = obj.attr('id')
        germplasmIdToReplace = obj.data('germplasm-id')
        packageIdToReplace = obj.data('package-id')

        // update header
        $('.replace-entry-header').html('<i class="material-icons inline-material-icon" style="margin-right: 4px;">swap_horiz</i> Replace entry')
        // show progress indicator
        $('.progress').removeClass('hidden')
        // clear modal content
        $('.replace-entry-modal-content').html('')

        $.ajax({
            url: '$replaceFormUrl',
            type: 'post',
            data: {
                'id': occurrenceDbId,
                'entryDbId': obj.attr('id'),
            },
            success: function (response) {
                // hide progress indicator
                $('.progress').addClass('hidden')
                // load replace entry form
                $('.replace-entry-modal-content').html(response)

                // load data values within replace entry form
                $('.replace-occurrence-name').html(occurrenceName)
                $('.replace-entry-number').html(entryNumber)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.progress').addClass('hidden')
                $('.replace-entry-modal-content').html('<i>There was a problem while loading the content.</i>')
            },
        })
    }

    // replace entry next button
    $('#replace-entry-modal').on('click', '.replace-entry-next-btn', function clickReplaceEntryNextButton (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        // render form content
        showReplacementForm(experimentDbId)

        // show preview panel
        panelAction('child', 'show')
        panelAction('parent', 'hide')

        // update buttons
        $('.select-next-btn').removeClass('hidden')
        $('.replace-entry-next-btn').addClass('hidden')
    });

    // cancel replace entry
    $('#replace-entry-modal').on('click', '.cancel-replace-entry-btn', function clickCancelReplaceEntryButton (e) {
        selectedReplacementType = null
        selectedReplacementTypeText = null

        resetReplaceModal()
    })

    // go back to parent panel
    $('#replace-entry-modal').on('click', '.go-back-to-replace-entry-parent-panel', function clickGoBackToReplaceEntryParentPanel (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        selectedReplacementType = null
        selectedReplacementTypeText = null
        $('.modal-body').css('max-height','550px')

        panelAction('parent', 'show')
        panelAction('child', 'hide')

        // show next button
        $('.replace-entry-next-btn').removeClass('hidden')
        $('.select-next-btn').addClass('hidden')
    })

    // click row in entry replacement selection browser
    $('#replace-entry-modal').on('click', '#grid-em-occurrence-entries-select-replacement-grid-container tbody tr', function clickSelectReplacementGridContainerRow (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        var thisRow = $(this).find('input:radio')[0]

        replacementId = $(thisRow).attr('id')

        if (thisRow !== undefined) {
            $("#"+replacementId).prop("checked", true)
        }
    })

    // select entity as a replacement
    $('#replace-entry-modal').on('change', 'input[type=radio][name=selection-radio-button]', function clickEntrySelectionRow () {
        replacementId = this.value
    })

    // select record
    $('#replace-entry-modal').on('click', '.select-next-btn', function clickSelectNextButton (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        $('.progress').removeClass('hidden')
        $('.replace-entry-form-modal-content').addClass('hidden')

        // render form content
        showReplacementPreview(replacementId, occurrenceName, entryNumber)
    })

    // go back to selection panel
    $('#replace-entry-modal').on('click', '.go-back-to-entry-selection-parent-panel', function (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        $('.modal-body').css('max-height','550px')

        // update buttons
        $('.select-confirm-btn').addClass('hidden')
        $('.select-next-btn').removeClass('hidden')

        // update modal content
        $('.replace-entry-preview-modal-content').addClass('hidden')
        $('.replace-entry-form-modal-content').removeClass('hidden')
    })

    // confirm selection
    $('#replace-entry-modal').on('click', '.select-confirm-btn', function (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        let entryNumbers = []

        // Retrieve entry numbers for replacing an entry with check/filler/germplasm
        checkedBoxes.forEach(plantingInstructionDbId => entryNumbers.push(
            $('#' + plantingInstructionDbId).data('entry-number')
        ))

        let additionalMessage = ''
        if (selectedReplacementType === 'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM') {
            additionalMessage = " "+germplasmName+" will be updated to the chosen germplasm. All involved cross records that use this entry will also be updated."
        }

        $('.progress').removeClass('hidden')
        $('.replace-entry-form-modal-content').html('')
            
        setTimeout(function () {
            $.ajax({
                url: '$confirmReplacementUrl',
                type: 'post',
                cache: false,
                data: {
                    occurrenceDbId: occurrenceDbId,
                    entryNumbers: entryNumbers,
                    checkedBoxes: checkedBoxes,
                },
                success: function (response) {
                    const res = (typeof response == 'string') ? JSON.parse(response) : response

                    $('.progress').addClass('hidden')

                    if (res.true) { // display success if there is at least 1 successful replacement
                        $('#replace-entry-modal').modal('hide')

                        // Display toast
                        let message = 'Successfully selected replacement for the entry.'+additionalMessage
                        let notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>"+ message +"</span>"
                        Materialize.toast(notif, 5000)

                        // Reset data browser to reflect entry replacement
                        $('#reset-entry-grid').trigger('click')

                        // Reset Replace entry modal to Step 1
                        resetReplaceModal()
                    } else {
                        $('.replace-entry-form-modal-content').html('<i>There was a problem while replacing the entry.</i>')
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.replace-entry-form-modal-content').html('<i>There was a problem while replacing the entry.</i>')
                },
            })
        }, 100)

        selectedReplacementType = null
        selectedReplacementTypeText = null
    })
}

// prevent default behavior of checkbox
$(document).on('click', '.grid-select', function(e) {
    e.preventDefault()
    e.stopImmediatePropagation()
})

// select all entries
$(document).on('click', '#checkbox-select-all', function(e) {
    if ($(this).prop("checked") === true) {
        isSelectAllChecked = true

        selectAllMode = 'exclude'
        sessionStorage.setItem(selectAllModeSessionStorageName, selectAllMode)

        $(this).prop('checked', true)
        $(".grid-select")
            .prop('checked',true)
            .parent("td")
            .parent("tr")
            .addClass("grey lighten-4")
    } else {
        isSelectAllChecked = false

        selectAllMode = 'include'
        sessionStorage.setItem(selectAllModeSessionStorageName, selectAllMode)

        $(this).prop('checked', false)
        $(".grid-select")
            .prop('checked',false)
            .parent("td")
            .parent("tr")
            .removeClass("grey lighten-4")
    }

    checkedBoxes = []
    
    sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
    
    compareCheckBoxCountWithTotalCount()
})

// click checkbox in entry browser
$(document).on('click', `#\${browserId} tbody tr`, function (e) {
    e.preventDefault()
    e.stopImmediatePropagation()

    let thisRow = $(this).find('input:checkbox')[0]

    // Handle checkedBoxes
    if (checkedBoxes.includes(thisRow.id)) {
        checkedBoxes.splice(checkedBoxes.indexOf(thisRow.id), 1)
    } else {
        checkedBoxes.push(thisRow.id)
    }

    $('#' + thisRow.id).trigger('click')

    // Update clicked checkbox's state
    if (thisRow.checked) {
        $(this).addClass('grey lighten-4')
    } else {
        $(this).removeClass('grey lighten-4')
    }

    if (isSelectAllChecked) {
        isSelectAllChecked = false
        $('#checkbox-select-all').prop('checked', false)
    }

    sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
    compareCheckBoxCountWithTotalCount()
})

$('#entry-list-type').on('select2:select', function (e) {
    let listType = e.params.data.id

    $('.modal-notif').html('')

    $('#export-workbook-save-list-type-info').html(`<div class="alert info">
        <div class="card-panel" style="">
            <i class="fa fa-info" style="margin-right: 4px"></i> Upon <strong>submission</strong>, if there are no valid <strong>\${listType}</strong> IDs present, there will still be a list created but <strong>no list members</strong> will be saved in the list.
        </div>
    </div>`)

    // Enable SUBMIT button
    $('#experiment_manager-save-list-confirm-btn').attr('disabled', false)
})

// on Save as New List modal close, reset form fields
$('#experiment_manager-save-list-modal').on('hidden.bs.modal', function (e) {
    $('#create-saved-list-form').trigger('reset')
    $('#export-workbook-save-list-type-info').html('')
    $('#entry-list-type').val('')
})

/**
 * Checks current state of checkedBoxes and updates text for summary count conditionally
 */
function compareCheckBoxCountWithTotalCount ()
{
    if (selectAllMode === 'exclude') {
        let currentCount = totalCheckboxes - checkedBoxes.length

        if (checkedBoxes.length > 0) {
            isSelectAllChecked = false
            $('#checkbox-select-all').prop('checked', false)
        } else {
            isSelectAllChecked = true
            $('#checkbox-select-all').prop('checked', true)
        }

        // update checked box counter text
        if (currentCount === 1) {
            $('#selected-items-count').html(currentCount)
            $('#selected-items-text').html('selected item.')
        } else if(currentCount > 1) {
            $('#selected-items-count').html(currentCount)
            $('#selected-items-text').html('selected items.')
        } else {
            $('#selected-items-count').html(filteredTotalCount)
            $('#selected-items-text').html('selected items.')
        }
    } else if (selectAllMode === 'include') {
        if (totalCheckboxes === checkedBoxes.length) {
            isSelectAllChecked = true
            $('#checkbox-select-all').prop('checked', true)
        } else {
            isSelectAllChecked = false
            $('#checkbox-select-all').prop('checked', false)
        }

        // update checked box counter text
        if (checkedBoxes.length === 1) {
            $('#selected-items-count').html(checkedBoxes.length)
            $('#selected-items-text').html('selected item.')
        } else if(checkedBoxes.length > 1) {
            $('#selected-items-count').html(checkedBoxes.length)
            $('#selected-items-text').html('selected items.')
        } else if (checkedBoxes.length === 0) {
            $('#selected-items-count').html('No')
            $('#selected-items-text').html('selected items.')
        }
    }

    handleEditButtonState()
}

/**
 * Toggles EDIT button state depending on number of selected entry records in the data browser
 */
function handleEditButtonState ()
{
    if (
        (selectAllMode === 'include' && checkedBoxes.length === 1) ||
        (selectAllMode === 'exclude' && (totalCheckboxes - checkedBoxes.length) === 1)
    ) {
        // Enable EDIT button
        $('#replace-entry-dropdown-btn').attr('disabled', false)
    } else  {
        // Disable EDIT button
        $('#replace-entry-dropdown-btn').attr('disabled', true)
    }
}

// show replacement entry form
function showReplacementForm (experimentId)
{
    var radios = $("input[name='replacement-type']")
    var selectedObj = radios.filter(":checked")
    selectedReplacementType = selectedReplacementType || $(selectedObj).val()
    selectedReplacementTypeText = selectedReplacementTypeText || $(selectedObj).attr('text')

    $('.progress').removeClass('hidden')
    $('.replace-entry-form-modal-content').html('')
    $('.select-next-btn').addClass('disabled')

    $.ajax({
        url: '$replaceSelectionFormUrl',
        type: 'post',
        dataType: 'json',
        data: {
            'experimentId': experimentId,
            'replacementType': selectedReplacementType,
            'entryIdToReplace': entryIdToReplace,
            'germplasmIdToReplace': germplasmIdToReplace,
            'packageIdToReplace': packageIdToReplace,
            'germplasmName': germplasmName,
        },
        success: function (response) {
            $('.progress').addClass('hidden')

            $('.replace-entry-form-modal-content').removeClass('hidden')
            $('.replace-entry-form-modal-content').html(response.data)
            
            // if no available records for replacement, hide next button
            if(!response.count){
                $('.select-next-btn').addClass('disabled')
            } else {
                $('.select-next-btn').removeClass('disabled')

                // Auto select first row
                $('#grid-em-occurrence-entries-select-replacement-grid-container tbody tr:first-child').trigger('click')

                // If there is only 1 available record, auto-redirect user to the next step
                if (parseInt(response.count) == 1) {
                    $('.select-next-btn').trigger('click')
                }
            }
        },
    })
}

// display replacement preview
function showReplacementPreview (replacementId, occurrenceName, entryNumber) {
    $('.select-next-btn').addClass('disabled')

    $.ajax({
        url: '$replacePreviewUrl',
        type: 'post',
        cache: false,
        dataType: 'json',
        data: {
            experimentId: experimentDbId,
            replacementType: selectedReplacementType,
            replacementTypeText: selectedReplacementTypeText,
            replacementId: replacementId,
            checkedBoxes: checkedBoxes,
            isBulkReplacement: isBulkReplacement,
        },
        success: function (response) {
            $('.progress').addClass('hidden')

            $('.replace-entry-preview-modal-content').removeClass('hidden')
            $('.replace-entry-preview-modal-content').html(response)

            $('.replace-occurrence-name').html(occurrenceName)
            $('.replace-entry-number').html(entryNumber)

            // update buttons
            $('.select-confirm-btn').removeClass('hidden')
            $('.select-next-btn').addClass('hidden')
            $('.select-next-btn').removeClass('disabled')
        },
        error: function (jqXHR, textStatus, errorThrown) {},
    })
}

// replace modal content
function resetReplaceModal () {
    // update content panels
    $('.parent-panel').removeClass('slide-out-parent')
    $('.parent-panel').css('display','block')
    $('.child-panel').removeClass('slide-in-child')
    $('.child-panel').css('display','none')
    $('.replace-entry-preview-modal-content').addClass('hidden')

    // update buttons
    $('.select-next-btn').addClass('hidden')
    $('.select-next-btn').removeClass('disabled')
    $('.select-confirm-btn').addClass('hidden')
    $('.replace-entry-next-btn').removeClass('hidden')
}

// Renders Toast message
function showMessage(message,type){
    var output = '';
    var icon = '';

    if(type == 'success'){
        icon = "<i class='material-icons green-text'>check</i>&nbsp; ";
    }
    else if(type == 'error'){
        icon = "<i class='material-icons red-text'>clear</i>&nbsp; ";
    }
    else if(type == 'warning'){
        icon = "<i class='material-icons yellow-text'>warning</i>&nbsp; ";
    }
    else if(type == 'message'){
        icon = "<i class='material-icons white-text'>info</i>&nbsp; ";
    }
    output = '<span class="left-align" style="text-align:left;">'+icon.concat(message)+'</span>';
    $('.toast').css('display','none');
    Materialize.toast(output, 5000);
}
JS);

$this->registerCss('
    .select2.select2-container.select2-container--krajee.select2-container--below.select2-container--focus {
        width: 100%;
        margin-top: 2.5%;
    }

    .select2.select2-container.select2-container--krajee.select2-container--below {
        width: 100%;
        margin-top: 2.5%;
    }

    .select2.select2-container.select2-container--krajee {
        width: 100%;
        margin-top: 2.5%;
    }

    .summary {
        margin-right: 6px;
    }

    html {
        overflow-x: hidden;
    }

    .disabled {
        pointer-events:none;
        opacity:0.6;
    }
');
