<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
use yii\helpers\Html;
use yii\helpers\Url;

/*
 * Renders the header tab for the view occurrence
 */
$occurrencesUrl = Url::to(['/occurrence','program' => $program]);
$basicUrl = Url::to(['view/index','program' => $program, 'id' => $id]);
$entryUrl = Url::to(['entry','program' => $program, 'id' => $id]);
$plotUrl = Url::to(['plot','program' => $program, 'id' => $id]);
$crossesUrl = Url::to(['crosses','program' => $program, 'id' => $id]);
$protocolUrl = Url::to(['protocol', 'program' => $program, 'id' => $id]);
$locationLayoutUrl = Url::to(['layout','program' => $program, 'id' => $id]);

$redirectToOccurrenceUrl = Url::to([
    '/occurrence',
    'program' => $program,
    'OccurrenceSearch[occurrenceName]' => $occurrenceName,
]);

echo 
'<h3>' . Html::a( Yii::t('app', 'Experiment Manager'), $occurrencesUrl) .
    ' <small>» ' . Html::a($occurrenceName, $redirectToOccurrenceUrl) . '</small>'.
'</h3>';

$occurrenceTabs = [
    [
        'label' => 'Basic',
        'url' => $basicUrl,
        'linkClass' => ($active == 'basic') ? 'active' : ''
    ],
    [
        'label' => 'Entry',
        'url' => $entryUrl,
        'linkClass' => ($active == 'entry') ? 'active' : ''
    ],
    [
        'label' => 'Plot',
        'url' => $plotUrl,
        'linkClass' => ($active == 'plot') ? 'active' : ''
    ],
    [
        'label' => 'Protocol',
        'url' => $protocolUrl,
        'linkClass' => ($active == 'protocol') ? 'active' : ''
    ],
    [
        'label' => 'Design Layout',
        'url' => $locationLayoutUrl,
        'linkClass' => ($active == 'layout') ? 'active' : ''
    ]
];

$crossesTab = [
    [
        'label' => 'Crosses',
        'url' => $crossesUrl,
        'linkClass' => ($active == 'crosses') ? 'active' : ''
    ]
];

// check if Occurrence has at least one cross then display Crosses tab
$crossModel = \Yii::$container->get('app\models\Cross');
$crossParams = [
    'occurrenceDbId' => "equals $id",
    'fields' => 'germplasmCross.occurrence_id AS occurrenceDbId'
];

$cross = $crossModel->searchAll($crossParams, 'limit=1', false);

$tabsArr = ($cross['totalCount'] > 0) ? array_merge($occurrenceTabs, $crossesTab) : $occurrenceTabs;

?>
<div class="col col-md-12" style="padding-right: 0px">
    <ul class="tabs">
        <?php
            // render tabs
            foreach ($tabsArr as $value) {

                $label = $value['label'];
                $url = $value['url'];
                $linkClass = $value['linkClass'];

                $item = '<li class="tab col">'.
                    '<a href="'.$url.'" class="'.$linkClass.' a-tab">' . $label . '</a>
                </li>';

                echo $item;
            }
        ?>
    </ul>
</div>
<div class="row"></div>

