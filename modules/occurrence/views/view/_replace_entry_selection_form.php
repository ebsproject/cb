<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;

/**
 * Replace entry selection form
 */
// go back button
if ( !$plantingJobEntriesDataProvider ) echo '<a href="#!" class="go-back-to-replace-entry-parent-panel" title="'. \Yii::t('app', 'Go back') .'">
	    <i class="material-icons">arrow_back</i>
	</a>';

if ( $plantingJobEntriesDataProvider ) {?>
    <ul class='collapsible selected-entries-panel'>
        <li>
            <div class="collapsible-header">
                Entry Summary
            </div>
            <div class="collapsible-body xl">
                <div class="row no-margin-bottom">
                    <div class="col s3">
                        <p style="font-weight: bold;">Germplasm Name</p>
                        <span><?echo $germplasmName;?></span>
                    </div>
                    <div class="col s3">
                        <p style="font-weight: bold;">Parentage</p>
                        <span><?echo $parentage;?></span>
                    </div>
                </div>
                <div class="row no-margin-bottom">
                    <div class="col s12" style='max-height: 160px; overflow: auto;'>
                        <?// Render selected entries browser
                        echo GridView::widget([
                            'pjax' => true,
                            'dataProvider' => $plantingJobEntriesDataProvider,
                            'id' => 'grid-em-occurrence-entries-selected-planting-job-entries-grid',
                            'columns' => [
                                [
                                    'attribute' => 'experimentName',
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'entryCode',
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'siteCode',
                                    'label' => Yii::t('app', 'Site'),
                                    'format' => 'raw',
                                    'vAlign' => 'middle',
                                    'value' => function ($model) {
                                        return "<span title='{$model['siteName']}'>
                                            {$model['siteCode']}
                                        </span>
                                        ";
                                    }
                                ],
                                [
                                    'attribute' => 'occurrenceName',
                                    'label' => Yii::t('app', 'Occurrence'),
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'actualSeedName',
                                    'label' => Yii::t('app', 'Seed Name'),
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'actualPackageLabel',
                                    'label' => Yii::t('app', 'Pacakge Label'),
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'requiredPackageQuantity',
                                    'label' => Yii::t('app', 'Required'),
                                    'format' => 'raw',
                                    'vAlign' => 'middle',
                                    'value' => function ($model) {
                                        return "<span>
                                            {$model['requiredPackageQuantity']} {$model['requiredPackageUnit']}
                                        </span>";
                                    }
                                ],
                            ],
                            'tableOptions' => [
                                'class' => 'table table-bordered white z-depth-3'
                            ],
                            'striped' => false,
                        ]);?>
                    </div>
                </div>
            </div>
        </li>
    </ul>
<?
}

if ($entity == 'germplasm') {
    ?>
    <p>Choose the type of values you want to search, then input the search value in the text field.</p>

    <?if (isset($germplasmName)) {?>
        <p>Original germplasm name: <b><?echo $germplasmName?></b></p>
    <?}?>

    <br/>

    <p><b>Search parameters</b></p>

    <div><?
        echo Select2::widget([
            'name' => 'germplasm-field-type-selection',
            'id' => 'germplasm-field-type-selection',
            'data' => $fieldTypeOptions,
            'value' => 'empty',
            'options' => [
                'tags' => true,
                'class' => 'field-type-selection variable-filter',
                'multiple' => false,
                'options' => $fieldTypeOptionTooltip,
                'placeholder' => 'Select a field type',
            ],
        ]);
        ?><div class='input-field'>
            <input
                id='search-germplasm-records-input'
                title='<?Yii::t('app', 'Enter search value here')?>'
                placeholder='Enter search value here'
            >
        </div>
        <div class='right-align'>
            <?echo Html::button(
                \Yii::t('app', 'Search'),
                [
                    'id' => "search-germplasm-records-button",
                    'class' => "waves-effect waves-light btn",
                    'disabled' => true,
                    'title' => Yii::t('app', 'Search germplasm records'),
                ]
            );?>
        </div>
        <div id='modal-loading-indicator' style='margin:10px 0px 10px 0px'></div>
        <div id='modal-germplasm-selection-grid'></div>
    </div><?
} else if ($totalCount) {
	echo '<p>Select the '. $entity .' you want to use then click the next button.</p>';

	echo GridView::widget([
		'pjax' => true,
		'dataProvider' => $dataProvider,
		'id' => 'grid-em-occurrence-entries-select-replacement-grid',
		'columns' => $columns,
		'tableOptions' => [
			'class' => 'table table-bordered white z-depth-3'
		],
		'striped' => false
	]);
} else {
    ?><br/><p>
        <i class="fa fa-warning orange-text text-darken-3 fa-2x"></i>
        There are no other available <?echo "{$entity}s";?> that can be used as an entry replacement.
    </p>
<?}

// allow array to be passed to Controller via post
$packageIdToReplace = json_encode($packageIdToReplace);

// set session variables
$getGermplasmSelectionDataUrl = Url::to(["view/get-germplasm-selection-data"]);

$this->registerJs(<<<JS
    loadingIndicatorHtml = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>'
    packageIdToReplace = '$packageIdToReplace'
    getGermplasmSelectionDataUrl = '$getGermplasmSelectionDataUrl'

    $(document).ready(function() {
        refresh()
    })

    function refresh () {
        $('.collapsible').collapsible()

        // Enable Search button whenever both dropdown and text field have non-null values
        $('#germplasm-field-type-selection').on('change', function (e) {
            if ($(this).val() && $('#search-germplasm-records-input').val()) {
                $('#search-germplasm-records-button').prop('disabled', '')
            } else {
                $('#search-germplasm-records-button').prop('disabled', 'disabled')
            }
        })

        // Enable Search button whenever both dropdown and text field have non-null values
        $('#search-germplasm-records-input').on('input', function (e) {
            if ($(this).val() && $('#germplasm-field-type-selection').val()) {
                $('#search-germplasm-records-button').prop('disabled', '')
            } else {
                $('#search-germplasm-records-button').prop('disabled', 'disabled')
            }
        })

        $('#search-germplasm-records-button').on('click', function (e) {
            const attribute = $('#germplasm-field-type-selection').val()
            const searchValue = $('#search-germplasm-records-input').val().trim()
            const modalBody = '#modal-loading-indicator'
            const selectionGrid = '#modal-germplasm-selection-grid'

            // Display loading indicator
            $(modalBody).html(loadingIndicatorHtml)

            $.ajax({
                url: getGermplasmSelectionDataUrl,
                type: 'post',
                dataType: 'json',
                data:{
                    attribute: attribute,
                    searchValue: searchValue,
                    packageDbId: packageIdToReplace,
                },
                success: function (response) {
                    // Hide loading indicator
                    $(modalBody).html('')

                    // Display available germplasm records
                    $(selectionGrid).html(response.htmlData)

                    if (response.count) {
                        // Enable next button
                        $('.select-next-btn').removeClass('disabled')
                        // Auto-select first row
                        $('#grid-em-occurrence-entries-select-replacement-grid-container tbody tr:first-child').trigger('click')
                    }
                    else
                        // Disable next button
                        $('.select-next-btn').addClass('disabled')
                },
                error: function (request, status, error) {
                    const errorMessage = '<i>There was a problem loading the content.</i>'

                    // Replace loading indicator with error message
                    $(modalBody).html(errorMessage)
                }
            })
        })
    }
JS);