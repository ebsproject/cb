<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders crosses of an occurrence
 */

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

Yii::$app->session->set('occurrence-cross-params' . $id, $params);

$browserId = 'dynagrid-em-occurrence-crosses';
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
$isAdmin = Yii::$app->session->get('isAdmin');

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '$browserId',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $id,
    'program'=>$program,
    'occurrenceName' => $occurrenceName,
    'active'=>'crosses'
]);

//Crosses table
$columns = [
    [
        'class' => '\kartik\grid\SerialColumn',
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'vAlign' => 'top',
        'hAlign' => 'left',
        'width' => '20px'
    ],
    [
        'attribute' => 'harvestStatus',
        'format' => 'raw',
        'visible' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
            'NO_HARVEST' => 'no harvest',
            'INCOMPLETE' => 'incomplete harvest data',
            'CONFLICT' => 'data conflict',
            'READY' => 'ready for seed creation',
            'IN_QUEUE' => 'in queue for creation',
            'IN_PROGRESS' => 'seed creation in progress',
            'COMPLETED' => 'harvest complete',
            'DELETION_IN_PROGRESS' => 'seed/trait deletion in progress',
            'INVALID_STATE' => 'invalid state',
            'FAILED' => 'seed creation failed'
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => ['multiple'=>false, 'allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Select status'],
        'value' => function ($model) {
            $harvestStatus = isset($model['harvestStatus']) ? $model['harvestStatus'] : 'NO_STATUS';
            $badge = '';

            if($harvestStatus=='NO_HARVEST') {
                $badge = '<span title="No seeds created yet" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'NO HARVEST') . '</strong></span>';
            } else if (strpos($harvestStatus,'INCOMPLETE')!==false) {
                $badge = '<span title="Harvest data is incomplete" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'INCOMPLETE DATA') . '</strong></span>';
            } else if (strpos($harvestStatus,'CONFLICT')!==false) {
                $parts = explode(': ', $harvestStatus);
                $title = $parts[1] ?? 'Unknown conflict';
                $badge = '<span title="' . $title . '" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'DATA CONFLICT') . '</strong></span>';
            } else if ($harvestStatus=='FAILED') {
                $badge = '<span title="Seed and package creation failed" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'FAILED') . '</strong></span>';
            } else if ($harvestStatus=='BAD_QC_CODE') {
                $badge = '<span title="Harvest data with B (bad) QC code detected" class="new badge center red darken-2"><strong>' . \Yii::t('app', 'BAD QC CODE') . '</strong></span>';
            } else if ($harvestStatus=='DELETION_IN_PROGRESS') {
                $badge = '<span title="Deletion of traits and seeds/packages is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'DELETION IN PROGRESS') . '</strong></span>';
            } else if ($harvestStatus=='REVERT_IN_PROGRESS') {
                $badge = '<span title="Reversion of failed harvest is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'REVERT IN PROGRESS') . '</strong></span>';
            } else if ($harvestStatus=='HARVEST_IN_PROGRESS') {
                $badge = '<span title="Seed and package creation is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'HARVEST IN PROGRESS') . '</strong></span>';
            } else if ($harvestStatus=='UPDATE_IN_PROGRESS') {
                $badge = '<span title="Update of harvest data is in progress" class="new badge center orange darken-2"><strong>' . \Yii::t('app', 'UPDATE IN PROGRESS') . '</strong></span>';
            } else if ($harvestStatus=='COMPLETED') {
                $badge = '<span title="Seeds and packages have been created" class="new badge center green darken-2"><strong>' . \Yii::t('app', 'HARVEST COMPLETE') . '</strong></span>';
            } else if ($harvestStatus=='READY') {
                $badge = '<span title="Cross is ready for seed and package creation" class="new badge center blue darken-2"><strong>' . \Yii::t('app', 'READY') . '</strong></span>';
            } else if ($harvestStatus=='QUEUED_FOR_HARVEST') {
                $badge = '<span title="Queued for seed and package creation" class="new badge center grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'QUEUED FOR HARVEST') . '</strong></span>';
            } else if ($harvestStatus=='QUEUED_FOR_UPDATE') {
                $badge = '<span title="Queued for harvest data update" class="new badge center grey lighten-2 grey-text text-darken-3"><strong>' . \Yii::t('app', 'QUEUED FOR UPDATE') . '</strong></span>';
            } else {
                $badge = '<span class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>';
            }

            return $badge;
        },
    ],
    [
        'attribute' => 'crossName',
        'label' => '<span title="Cross Name">'.Yii::t('app', 'Cross Name').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['crossName']) ? $data['crossName'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'entryListName',
        'label' => '<span title="Cross List Name">'.Yii::t('app', 'Cross List Name').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['entryListName']) ? $data['entryListName'] : '<span class="not-set">(not set)</span>';
        },
        'value' => function ($data) use ($program) {
            return !empty($data['entryListName']) ?  Html::a(
                $data['entryListName'],
                "/index.php/crossManager/basic-info/index?program=$program&id={$data['entryListDbId']}",
                [
                    'title' => Yii::t('app', 'View Cross List'),
                ]
            ) : '<span class="not-set">(not set)</span>';
        },
    ],
    [
        'attribute' => 'crossFemaleParent',
        'label' => '<span title="Female Parent"></i>'.Yii::t('app', 'Female Parent').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['crossFemaleParent']) ? $data['crossFemaleParent'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleParentage',
        'label' => '<span title="Female Parentage"></i> '.Yii::t('app', 'Female Parentage').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['femaleParentage']) ? $data['femaleParentage'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossMaleParent',
        'label' => '<span title="Male Parent"></i>'.Yii::t('app', 'Male Parent').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['crossMaleParent']) ? $data['crossMaleParent'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleParentage',
        'label' => '<span title="Male Parentage"></i> '.Yii::t('app', 'Male Parentage').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['maleParentage']) ? $data['maleParentage'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossMethod',
        'label' => '<span title="Method">'.Yii::t('app', 'Cross Method').'</span>',
        'format' => 'raw',
        'headerOptions'=>['style'=>'min-width:200px !important'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['crossMethod']) ? $data['crossMethod'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossRemarks',
        'format' => 'raw',
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['crossRemarks']) ? $data['crossRemarks'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleParentEntryNumber',
        'label' => '<span title="Female Entry No."></i>'.Yii::t('app', 'Female Entry No.').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['femaleParentEntryNumber']) ? $data['femaleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleParentEntryNumber',
        'label' => '<span title="Male Entry No."></i>'.Yii::t('app', ' Male Entry No.').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            if($data['crossMethod'] == 'selfing'){
                return !empty($data['femaleParentEntryNumber']) ? $data['femaleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
            } else return !empty($data['maleParentEntryNumber']) ? $data['maleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femalePIEntryCode',
        'label' => '<span title="Female Entry Code"></i>'.Yii::t('app', 'Female Entry Code').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['femalePIEntryCode']) ? $data['femalePIEntryCode'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'malePIEntryCode',
        'label' => '<span title="Male Entry Code"></i>'.Yii::t('app', ' Male Entry Code').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            if($data['crossMethod'] == 'selfing'){
                return !empty($data['femalePIEntryCode']) ? $data['femalePIEntryCode'] : '<span class="not-set">(not set)</span>';
            } else return !empty($data['malePIEntryCode']) ? $data['malePIEntryCode'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleOccurrenceName',
        'label' => '<span title="Female Occurrence"></i> '.Yii::t('app', 'Female Occurrence').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['femaleOccurrenceName']) ? $data['femaleOccurrenceName'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleOccurrenceName',
        'label' => '<span title="Male Occurrence"></i> '.Yii::t('app', 'Male Occurrence').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['maleOccurrenceName']) ? $data['maleOccurrenceName'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleOccurrenceCode',
        'label' => '<span title="Female Occurrence Code"></i> '.Yii::t('app', 'Female Occurrence Code').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['femaleOccurrenceCode']) ? $data['femaleOccurrenceCode'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleOccurrenceCode',
        'label' => '<span title="Male Occurrence Code"></i> '.Yii::t('app', 'Male Occurrence Code').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['maleOccurrenceCode']) ? $data['maleOccurrenceCode'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleParentSeedSource',
        'label' => '<span title="Female Seed Source"></i> '.Yii::t('app', 'Female Seed Source').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['femaleParentSeedSource']) ? $data['femaleParentSeedSource'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false,
    ],
    [
        'attribute' => 'maleParentSeedSource',
        'label' => '<span title="Male Seed Source"></i> '.Yii::t('app', 'Male Seed Source').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['maleParentSeedSource']) ? $data['maleParentSeedSource'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false,
    ],
    [
        'attribute' => 'femaleSourcePlot',
        'label' => '<span title="Female Source Plot"></i> '.Yii::t('app', 'Female Source Plot').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['femaleSourcePlot']) ? $data['femaleSourcePlot'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false,
    ],
    [
        'attribute' => 'maleSourcePlot',
        'label' => '<span title="Male Source Plot"></i> '.Yii::t('app', 'Male Source Plot').'</span>',
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['maleSourcePlot']) ? $data['maleSourcePlot'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false,
    ]
];

$downloadCrossesBtn = ($isAdmin || Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_DOWNLOAD_FILES', 'OCCURRENCES')) ?
    Html::a(
        '<i class="material-icons inline-material-icon">file_download</i>',
        '',
        [
            'data-pjax' => true,
            'id' => 'export-crosses-btn',
            'class' => 'btn btn-default',
            'title' => \Yii::t('app', 'Export all crosses in CSV format')
        ]
    ) : '';

DynaGrid::begin([
    'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => "$browserId-id",
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => "$browserId-id"],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsiveWrap' => false,
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', 'This is the browser for the crosses of the occurrence.').' {summary}',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content'=> 
                '&nbsp;' .
                $downloadCrossesBtn .
                Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    [
                        '/occurrence/view/crosses',
                        'program'=>$program,
                        'id' => $id
                    ],
                    [
                        'data-pjax' => true,
                        'id' => 'reset-crosses-grid',
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app','Reset grid')
                    ]
                ) .
                '{dynagridFilter}{dynagridSort}{dynagrid}',
            ]
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-delete',
        'label' => 'Remove',
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

$exportCrossesUrl = Url::to(["view/export-crosses?id=$id"]);
$prepareDataForCsvDownloadUrl = Url::to(['view/prepare-data-for-csv-download' . "?program=$program&occurrenceId=$id&entity=cross"]);

(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
const prepareDataForCsvDownloadUrl = '$prepareDataForCsvDownloadUrl';
const exportCrossesThresholdValue = $exportCrossesThresholdValue
const loadingIndicatorHtml = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>'
const totalCount = $totalCount
let browserId = '$browserId'
refresh();

$(document).ready(function() {
    adjustBrowserPageSize()
})

$(document).on('ready pjax:success', function(e) {
    e.stopPropagation();
    e.preventDefault();
    refresh();
});

function refresh(){
    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        // remove loading indicator upon export
        $('*[class^="export-full-"]').on('click',function(){
            $('#system-loading-indicator').css('display','none');
        });

        e.stopPropagation();
        e.preventDefault();
        refresh();
    });


    // set dynamic height for the browser
    var maxHeight = ($(window).height() - 310);
    $(".kv-grid-wrapper").css("height",maxHeight);

    $('#export-crosses-btn').off('click').on('click', function (e) {
        e.preventDefault()

        if (totalCount >= exportCrossesThresholdValue) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: prepareDataForCsvDownloadUrl,
                data: {},
                success: async function (ajaxResponse) {},
                error: function (xhr, error, status) {}
            })
        } else {
            let exportCrossesUrl = '$exportCrossesUrl'
            let expCrsUrl = window.location.origin + exportCrossesUrl

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'

            $('.toast').css('display','none')

            Materialize.toast(message, 5000)
            window.location.replace(expCrsUrl)

            $('#system-loading-indicator').html('')
        }
    })
}
JS);

$this->registerCss('
    .select2.select2-container.select2-container--krajee.select2-container--below.select2-container--focus {
        width: 100%;
        margin-top: 2.5%;
    }

    .select2.select2-container.select2-container--krajee.select2-container--below {
        width: 100%;
        margin-top: 2.5%;
    }

    .select2.select2-container.select2-container--krajee {
        width: 100%;
        margin-top: 2.5%;
    }
');
?>
