<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>
*/

use yii\bootstrap\Modal;
use yii\helpers\Url;

/**
 * Renders design layout of the Occurrence
 */

echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $id,
    'program'=>$program,
    'occurrenceName' => $occurrenceName,
    'active'=>'layout'
]);
Modal::begin([
    'id' => 'design-recently-used-tools-modal'
]);
Modal::end();
?>

<div class="col col-md-12" id="layout-panel-div">
    <div id="loading-indicator" class="progress">
            <div class="indeterminate"></div>
    </div>
</div>

<?php
$renderLayoutUrl = Url::to(['view/render-layout', 'id'=>$id, 'program'=>$program]);

$this->registerJsFile('https://code.highcharts.com/stock/highstock.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://code.highcharts.com/modules/heatmap.js',['position' => \yii\web\View::POS_END]);

$this->registerJs(<<<JS
    var panelWidth = $('#layout-panel-div').width() + 10;

    previewLayout();
    function previewLayout(){
        $.ajax({
            url: '$renderLayoutUrl',
            type: 'post',
            dataType: 'json',
            data: {
                panelWidth:panelWidth
            },
            success: function(response) {
                $('#loading-indicator').css('display', 'none')

                if(response.success == 'success'){
                    $('#layout-panel-div').html(response.htmlData);
                } else {
                    $('#layout-panel-div').html('');
                }
            },
            error: function() {
            }
        });
    }

JS
);