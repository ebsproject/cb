<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders design step for experiment creation
 */
use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use app\dataproviders\ArrayDataProvider;
use yii\web\JsExpression;
use kartik\dynagrid\DynaGrid;
use yii\bootstrap\Modal;


// if filters are set
$selectedItems = isset($selectedItems) ? $selectedItems : [];
$selectedItemsCount = count($selectedItems) > 0 ? count($selectedItems) : 'No';
$dataUrl = Url::home() . '/experimentCreation/' . Url::to('design/get-scale-values');
 
$columns = [
    // [
    //     'label' => "Checkbox",
    //     'header' => '
    //         <input type="checkbox" class="filled-in" id="pa-select-entries-all"/>
    //         <label for="pa-select-entries-all" id="label-for-pa-select-entries-all" style="height: 11px;"></label>',
    //     'content'=>function($data) use ($selectedItems){
    //         $entryDbId = $data['entryDbId'];

    //         if(in_array($entryDbId, $selectedItems)){
    //             $checkbox = '<input class="pa-entries-grid_select filled-in" type="checkbox" checked="checked" id="'.$data['entryDbId'].'" />';
    //         }
    //         else{
    //             $checkbox = '<input class="pa-entries-grid_select filled-in" type="checkbox" id="'.$data['entryDbId'].'" />';
    //         }
    //         return $checkbox.'
    //             <label for="'.$data['entryDbId'].'"></label>       
    //         ';
    //     },  
    //     'hAlign' => 'center',
    //     'vAlign' => 'middle',
    //     'visible' => true,
    //     'hidden' => false
    // ],
    [
        'attribute' => 'designation',
        'label' => '<span title="Germplasm">' . Yii::t('app', 'Germplasm Name') . '</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'visible' => true,
        'hidden' => false,
        'hAlign' => 'center',
    ],
    [
        'attribute' => 'entryNumber',
        'label' => '<span title="Sequential numbering of the entries">' . Yii::t('app', 'Entry No.') . '</span>',
        'format' => 'raw',
        'width'=>'100px',
        'encodeLabel' => false,
        'visible' => true,
        'hidden' => false,
        'hAlign' => 'center',
    ],
    'entryType',
    [
     	'attribute' => 'entryRole',
        'label' => '<span title="Entry Role">' . Yii::t('app', 'Entry Role') . '</span>',
        'format' => 'raw',
        'width'=>'170px',
        'contentOptions'=>['style'=>'padding-left:0px;'],
        'content'=>function($data){
        	$values = [
        		'spatial check' => 'Spatial Check',
        		'repeated check' => 'Repeated Check'
        	];
            // return Select2::widget([
			//     'id' => 'entryRole-'.$data['entryDbId'],
			//     'name' => 'entryRoleName',
			//     'maintainOrder' => true,
			//     'value' => $data['entryRole'],
			//     'data' => $values,
			//     'options' => [
			//     	'placeholder' => 'Select entry role',
			//     	'data-id' => $data['entryDbId'],
			//     	'data-attribute' => 'entryRole',
			//     	'class' => 'entryRole-class design-parameters pull-left',
			//     	'style'=>'margin-left:0px;padding-left:0px;'
			//     ],
			//     'pluginOptions' => [
			//         'allowClear' => false,
			//         'width' =>'100%',
			        
			//     ],
			// ]);
            return $values[$data['entryRole']];
        },
        'encodeLabel' => false,
        'visible' => true,
        'hAlign' => 'center',
        'hidden' => false
    ],
    [
        'attribute' => 'entryClass',
        'width'=>'150px',
        'label' => '<span title="Entry Class">' . Yii::t('app', 'Entry Class') . '</span>',
        'format' => 'raw',
        'contentOptions'=>['style'=>'padding-left:0px;'],
        'content'=>function($data){
        	
        	$input = Html::input('text', 'entryRepField', $data['entryClass'], ['id'=>'entryClass-'.$data['entryDbId'],'class' => 'entryClass-class design-parameters editable-field', 'autofocus' => true, 'data-attribute'=>'entryClass', 'style' => 'width:100%', 'data-id' => $data['entryDbId']]);
            return $data['entryClass'];
        },
        'encodeLabel' => false,
        'visible' => true, 
        'hAlign' => 'center',
        'hidden' => false
    ],
    
    [
        'label' => '<span title="Number of Replications per entry">' . Yii::t('app', 'No. of Reps') . '</span>',
        'format' => 'raw',
        'width'=>'100px',
        'contentOptions'=>['style'=>'padding-left:0px;'],
        'content'=>function($data) use ($uiTemplateInfo){
        	$notSet = '<span class="not-set">(not set)</span>';
        	$value = isset($uiTemplateInfo[$data['entryDbId']]) ? $uiTemplateInfo[$data['entryDbId']] : 2;
        	
        	$show = 'hidden';
        	if($data['entryRole'] == 'repeated check'){
        		$show = '';
        	}
        	$input = Html::input('number', 'entryRepField', $value, ['id'=>'entryRep-'.$data['entryDbId'],'class' => 'entryRep-class numeric-input-validate design-parameters '.$show, 'autofocus' => true, 'min' => 2, 'style' => 'width:100%', 'data-id' => $data['entryDbId'], 'data-attribute' => 'entryRep']);
            return $value;
        },
        'encodeLabel' => false,
        'visible' => true,
        'hidden' => false,
        'hAlign' => 'center',
    ],
];
	
	 echo '<br><div class="row" style="margin:0px;margin-bottom:0px;padding:5px;background-color:#ffffff">'.
        '<div class="col-md-6"><span>'.\Yii::t('app', 'This lists the checks in the entry list.').'</span></div>'.
        // '<div class="col-md-6" style="padding-right:0px;margin-bottom:0px;"><div class="row" style="padding-right:0px;margin-bottom:0px;">'.
        // Html::a('<i class="glyphicon glyphicon-edit"></i>', '#', ['style' => 'margin-bottom:5px;', 'type' => 'button', 'title' => \Yii::t('app', "Assign Entries to group"), 'class' => 'pull-right tabular-btn btn waves-effect waves-light btn btn-primary light-green darken-3', 'id' => 'bulk-checklist', 'data-toggle' => 'modal','data-target' => '#update-entry-modal']).'</div><p id = "selected-items" class="pull-right" style = "margin:-1px"><b id = "selected-count">'.$selectedItemsCount.'</b> selected items.</p></div>'.
        '</div>';
	// render data browser
    DynaGrid::begin([
            'options' => ['id' => 'design-search-checks-grid'],
            'columns' => $columns,
            'theme' => 'simple-default',
            'storage' => 'cookie',
            'showPersonalize' => false,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pa-search-checks-grid'
                    ],
                    'beforeGrid' => '',
                    'afterGrid' => ''
                ],
                'responsive' => true,
                'responsiveWrap' => false,
                'floatHeader' => false,
                'floatOverflowContainer' => true,
                'panel' => [
                    'heading' => false,
                    'after' => false,
                    'before' => false
                ],
                'toolbar' => [                             
                ],
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
            ]
        ]);
        
     DynaGrid::end();

//New Modal for entry management, actions button - select variables
Modal::begin([
    'id'=> 'update-entry-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i>  '.\Yii::t('app','Bulk Update').'</h4>',
    'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'close-bulk-update-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="bulk-update-fields-selected" class="btn btn-primary green waves-effect waves-light confirm-update-btn" mode ="selected">'.\Yii::t('app','Selected').'</a>&nbsp;'.
        '<a id="bulk-update-fields-all" class="btn btn-primary teal waves-effect waves-light confirm-update-btn" mode ="all">'.\Yii::t('app','All').'</a>',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static', 'tabindex' => false],
]);
    echo '<div id = "modal-progress" class = "progress hidden"><div class="indeterminate"></div></div>';
    echo '<div id = "modal-content"><p id="instructions"></p>';
    echo '<span id="note"></span>';
    echo '<div id="config-var" style="margin-left:30px;"></div></div>';
Modal::end();

$uiTemplateStr = json_encode($uiTemplateInfo);
$updateUrl = Url::to(['/experimentCreation/design/update-design-parameters', 'id' => $experimentDbId]);
$storeSessionItemsUrl = Url::to(['/experimentCreation/design/store-session-items']);
$getSelectedItemsUrl2 = Url::to(['/experimentCreation/design/get-selected-items']);

$renderBulkUpdateUrl = Url::to(['create/render-bulk-update','id'=>$experimentDbId,'program'=>$program]);
$genericRenderConfigUrl = Url::to(['create/render-config-form', 'id'=>$experimentDbId, 'program'=>$program]);

$updateFieldsUrl = Url::to(['design/bulk-update-fields', 'id' => $experimentDbId]);
$entryColumns = isset($entryColumns) ?? '';
$entryDataColumns = isset($entryDataColumns) ?? '';
$configValues = json_encode($configData);

$this->registerJs(<<<JS
	var uiGroups = JSON.parse('$uiTemplateStr');
	var selectedEntryIds = {};
    var variable = '';
	
     $(document).on('ready pjax:success', function(e) {
        $('.dropdown-button').dropdown();

        var selectedInfo = getSelected();
        if($('#pa-select-entries-all').prop("checked") === true || selectedInfo['isSelectedAll'] == true)  {
            $('#pa-select-entries-all').attr('checked','checked');
            $('.pa-entries-grid_select').attr('checked','checked');
            $('.pa-entries-grid_select').prop('checked',true);
            $(".pa-entries-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
        }else{
            $('#pa-select-entries-all').removeAttr('checked');
            $('.pa-entries-grid_select').prop('checked',false);
            $('.pa-entries-grid_select').removeAttr('checked');
            $("input:checkbox.pa-entries-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
        }
    });

    $(document).on('click', '#bulk-checklist', function() {
        var note;
       
        $('#config-var').removeAttr('tabindex');
        $.ajax({
            url: '$renderBulkUpdateUrl', 
            type: 'post',
            dataType: 'json',
            data: {
                entryColumns: '$entryColumns', 
                entryDataColumns: '$entryDataColumns', 
                configVars: '$configValues'
            },
            success: function(response){
                $('#instructions').html("Bulk update the following values for the entry records. The changes will be reflected after closing the modal.");

                $('#config-var').html(response);
                $('.loading-req-vars').remove();
                $('#req-vars').css('visibility','visible');
                $('#req-vars').css('width','');
                $('#req-vars').css('height','');
            }
        });
    });

    $(document).on('change','#req-vars',function(){
        variable = $(this).val();
      
        if(variable != ""){
             $.ajax({
                 url: '$genericRenderConfigUrl',
                 type: 'post',
                 dataType: 'json',
                 data:{
                    selected: variable, 
                    configVars: '$configValues',
                    formType: 'modal'
                },
                 success: function(response){
                    $('#input-field-var').html(response);
                    $('.select-input').css('visibility','visible');
                    $('.select-input').css('width','');
                    $('.select-input').css('height','');
                 },
                 error: function(){
                     
                 }
             });
        }else{
         $('#input-field-var').html('');
        }
    });

    $(document).on('click', '.confirm-update-btn', function(e){
        var selectedInfo = getSelected();
        var fieldId = $('#req-vars').val();
        var fieldValue = '';

        $(".required-field").each(function(){
            fieldValue = $(this).val()
        });
        var updateAll = true;
        var fieldBtn = this.id;
       

        if(fieldValue == ''){
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please input a value.</span>";
                Materialize.toast(notif, 5000);
            }
            e.preventDefault();
            e.stopImmediatePropagation();
        } else if(fieldBtn == 'bulk-update-fields-selected' && selectedInfo['selectedItemsCount']  == 0){
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> No items selected.</span>";
                Materialize.toast(notif, 5000);
            }
            e.preventDefault();
            e.stopImmediatePropagation();
        }else {
            var entryDbIds = selectedInfo['selectedItems'];
            if(fieldBtn == 'bulk-update-fields-selected'){
                updateAll = false;
            }
            $.ajax({
                url: '$updateFieldsUrl',
                type: 'post',
                data:{
                    fieldId: fieldId,
                    fieldValue: fieldValue,
                    entryDbIds: entryDbIds,
                    uiGroups: uiGroups,
                    updateAll: updateAll,
                    entryListId:'$entryListId'
                },
                success: function(response) {
                    response = JSON.parse(response);
                    if(response['success']){
                        uiGroups = response['uiGroups'];
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons green-text left'>check</i>Successfully updated.";
                            Materialize.toast(notif,5000);
                        }
                        $.pjax.reload({
                            container: '#design-search-checks-grid-pjax', 
                            replace:false
                        });
                        $('#close-bulk-update-btn').click();
                    } else {
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons red-text left'>close</i>Update failed. Please try again.";
                            Materialize.toast(notif,5000);
                        }
                        
                    }
                },
                error: function() {
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons red-text left'>close</i>Update failed. Please try again.";
                        Materialize.toast(notif,5000);
                    }
                }
            });
        }
    });

    $(document).on('change', '.design-parameters', function(){
        var entryDbId = (this.id).split('-');

        if(entryDbId[0] == 'entryRole'){
        	if(this.value == 'repeated check'){
        		$('#entryRep-'+entryDbId[1]).removeClass('hidden');
	        } else {
                $('#entryRep-'+entryDbId[1]).val('2');
	        	$('#entryRep-'+entryDbId[1]).addClass('hidden');
	        }
        }

        if(entryDbId[0] == 'entryRep'){
            uiGroups[entryDbId[1]] = this.value;
        }
       
        $.ajax({
            url: '$updateUrl',
            type: 'post',
            data:{
                fieldId: entryDbId[0],
                entryDbIds: [entryDbId[1]],
                fieldValue: this.value,
                uiGroups: uiGroups
            },
            success: function(response) {
            	response = JSON.parse(response);
            	if(response['success']){
	                uiGroups = response['uiGroups'];
	                var hasToast = $('body').hasClass('toast');
	                if(!hasToast){
	                    $('.toast').css('display','none');
	                    var notif = "<i class='material-icons green-text left'>check</i>Successfully updated.";
                    	Materialize.toast(notif,5000);
	                }
	            } else {
	            	var hasToast = $('body').hasClass('toast');
	                if(!hasToast){
	                    $('.toast').css('display','none');
	                    var notif = "<i class='material-icons red-text left'>close</i>Update failed. Please try again.";
                    	Materialize.toast(notif,5000);
	                }
	            	
	            }
            },
            error: function() {
            	var hasToast = $('body').hasClass('toast');
	            if(!hasToast){
	                $('.toast').css('display','none');
	                var notif = "<i class='material-icons red-text left'>close</i>Update failed. Please try again.";
	            	Materialize.toast(notif,5000);
	            }
            }
        });
    });

    // select all entries
    $(document).on('click','#pa-select-entries-all',function(e){
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.pa-entries-grid_select').attr('checked','checked');
            $('.pa-entries-grid_select').prop('checked',true);
            $(".pa-entries-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
        }else{
            $(this).removeAttr('checked');
            $('.pa-entries-grid_select').prop('checked',false);
            $('.pa-entries-grid_select').removeAttr('checked');
            $("input:checkbox.pa-entries-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
        }

        var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
        var unset = $(this).prop('checked') ? false : true;
        var url = '$storeSessionItemsUrl';

        $.ajax({
            url: url,
            type: 'post',
            data: {
                entryDbId: 0,
                unset: unset,
                storeAll: true,
                experimentDbId : '$experimentDbId'
            },
            success: function(response) {
                $('#selected-count').html(response);
                
                if(unset){
                    $('#selected-count').html('No');
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

     // click row in browser
    $(document).on('click',"#design-search-checks-grid tbody tr",function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        this_row=$(this).find('input:checkbox')[0];

        if(this_row.checked){
            this_row.checked=false;
            $('#pa-select-entries').prop("checked", false);
            $(this).removeClass("grey lighten-4");

            $('#pa-select-entries-all').prop('checked',false);
            $('#pa-select-entries-all').removeAttr('checked');

            unset = true;
        }else{
            $(this).addClass("grey lighten-4");
            this_row.checked=true;
            $("#"+this_row.id).prop("checked");
            unset = false;
        }
        
         $.ajax({
            url: '$storeSessionItemsUrl',
            type: 'post',
            data: {
                entryDbId: $("#"+this_row.id).prop("id"),
                unset: unset,
                storeAll: false,
                experimentDbId : '$experimentDbId'
            },
            success: function(response) {
                $('#selected-count').html(response);
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    //retrieve user's session selection
    function getSelected(){
        var userSelection;
        $.ajax({
            url: '$getSelectedItemsUrl2',
            type: 'post',
            dataType:'json',
            async:false,
            data: {
                experimentDbId: '$experimentDbId'
            },
            success: function(response) {
                userSelection = response;
            },
            error: function() {
            
            }
        });
        return userSelection;
    }


JS
);
?>