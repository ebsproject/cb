<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders design step for experiment creation
 */

use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use app\dataproviders\ArrayDataProvider;
use yii\web\JsExpression;
use kartik\dynagrid\DynaGrid;

?>

<ul class="collapsible">
    <li id="collapsible-flag" class="collapsible-search-filter">
        <div class="collapsible-header active" id="specify-groups"><i class="material-icons">apps</i>Specify Groups</div>
        <div class="collapsible-body" style="padding: 0.2rem">

<?php
$options = '<option value="check">Check Group</option>'.
		 	'<option value="test">Test Group</option>';

// echo '<b style="margin-top:5px; margin-bottom:0px;">Specify Groups &nbsp;</b>';

// echo '<p style="margin-bottom:15px;"></p><div class="row" style="margin-bottom:5px;"><div class="col col-md-5"><b style="margin-top:5px; margin-bottom:0px;">Group type</b>&nbsp;<select id="group-type-selection-id" class="check-groups-select2 pull-left form-control" name="checkGroupSelect">'.$options.'</select></div><div class="col col-md-5 pull-right" style="margin-top:20px;">'.
// Html::a('Create group','#',['style'=>'margin-right:0px;','type'=>'button', 'title'=>\Yii::t('app',"Create group"), 'class'=>'pull-right tabular-btn btn black-text grey lighten-2 waves-effect waves-light','id'=>'group-add-rows-btn']).'</div></div>';

echo '<table id="create-groups-table2" class="table table-bordered white kv-grid-table kv-table-wrap" style="margin-bottom:0px;">';
echo '<th class="kv-align-middle" style="width:30%">GROUP NAME</th><th>GROUP TYPE</th><th>NO. OF ENTRIES</th><th>NO. OF REPS</th>';
echo '<div id="check-groups-body-div">';

    if(isset($uiTemplateInfo) && !empty($uiTemplateInfo)){

        foreach($uiTemplateInfo as $group){

           
            $removeStr = Html::a('<i class="material-icons">delete</i>',false, 
                [
                  'class'=>'pa-remove-row-createGrp',
                  'title' => 'Remove row',
                  'style'=>"cursor:pointer;",
                  'data-id'=>$group['group']
                ]);
            $repno = Html::input('number', 'createGroup-apply', $group['repno'], ['id' => 'repno-'.$group['group'], 'class' => 'repno-group_class createGroup-validate numeric-input-validate pull-left', 'style'=>'width:75px;', 'min'=> 1]);
            $trStr = '<tr data-id="'.$group['group'].'" class="tr-checkgrp-class">'.
                '<td>'.$group['name'].'</td><td>'.$group['type'].'</td><td id="group-'.$group['group'].'">'.count($group['entryIds']).'</td><td>'.$group['repno'].'</td></tr>';

            echo $trStr;
        }
    } else {
        echo '<tr><td id="no-results-id" colspan="2">No results found.</td></tr>';
    }
echo '</div>';
echo '</table>';
?>

        </div>
    </li>
</ul>


<!-- will enable the following next sprint
<?php

//Filter entries
$col2Str = '';
$col1Str = '<div class="row" style="margin-bottom:5px;margin-bottom:0px;"><div class="col col-md-4"><label class="control-label" style="margin-top:15px;">' . \Yii::t('app', 'Entry no. range from ') . '</label></div>';
$col1Str .= '<div class="col col-md-8 search-input-field" style="padding-left:0">' .
 '<span class="entryNumber-range-panel">' .
    Html::input('number', 'filter-entryNumber-number-from', '', ['id'=>'filter-entryNumber-number-from-id','class' => 'pa-filter-entryNumber-field-from numeric-input-validate pa-filter-field', 'autofocus' => true, 'min' => 1, 'style' => 'width:45%', 'data-attr' => 'entryNumber']) . 'to ' .
    Html::input('number', 'filter-entryNumber-number-to', '', ['id'=>'filter-entryNumber-number-to-id','class' => 'pa-filter-entryNumber-field-to numeric-input-validate pa-filter-field', 'min' => 1, 'style' => 'width:45%', 'data-attr' => 'entryNumber']) .
    '</span></div></div>';
$col1Str .= '<div class="col col-md-4"><label class="control-label">' . \Yii::t('app', 'Designation') . '</label></div>';
$col1Str .= Html::input('text', 'filter-designation', '', ['class' => 'pa-filter-field hidden', 'name'=>'designation','data-attr' => 'designation', 'data-isvar' => 'false']);
$dataUrl = Url::home() . '/experimentCreation/' . Url::to('planting-arrangement/get-filter-data');

//Notification
echo '<div id="page-notif" class="hidden"></div>';

$pluginOptions = [
    'allowClear' => true,
    'minimumInputLength' => 0,
    'placeholder' => 'Select Designation',
    'minimumResultsForSearch' => 1,
    'language' => [
        'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
     ], 'ajax' => [
            'url' => $dataUrl,
            'dataType' => 'json',
            'delay' => 250,
            'data' => new JsExpression(
                'function (params){
                    var entryListId = "'.$entryListId.'";
                    var attr = $(this).attr("data-attr");
                    return {
                        q: params.term, 
                        attr: attr,
                        page: params.page,
                        listId: entryListId,
                        id: "'.$experimentDbId.'"
                    }
                }'
            ),
            'processResults' => new JSExpression(
                'function (data, params) {
                    
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                        more: (params.page * 5) < data.totalCount
                        },
                    };
                }'
            ),
            'cache' => true
        ]
];
$col1Str .= '<div class="col col-md-8 search-input-field" style="padding-left:0">' . Select2::widget([
    'id' => 'pa_select_designation',
    'name' => 'pa_data_filter_designation',
    'maintainOrder' => true,
    'value' => isset($filters['designation']) ? $filters['designation'] : null,
    'options' => [
        'tags' => true,
        'multiple' => true,
        'style' => 'width:100%',
        'data-attr' => 'designation',
        'minimumResultsForSearch' => 1,
    ],
    'pluginOptions' => $pluginOptions,
    'pluginEvents' => [
        "change" => "
            function(){
                if($(this).val()!=''){
                    var value = $(this).val()+'';
                    var values = value.split(',');
                    var val = '', counter = 1;

                    $.each(values,function(i,v){ 
                        val = val+v;
                        
                        if(counter < values.length){
                            val = val+',';
                        }
                        counter += 1;
                    })
                    $('#pa_select_designation'+'-hidden').val(val);
                }else{
                    $('#pa_select_designation'+'-hidden').val($(this).val());
                }
            }",
        ],
        'showToggleAll' => false
]) . '</div>';


//column2 filters

$col2Str .= '<div class="row" style="margin-top:5px;margin-bottom:3px;"><div class="col col-md-4"><label class="control-label" style="margin-top:15px;">' . \Yii::t('app', 'Entry Type') . '</label></div>';
$col2Str .= Html::input('text', 'pa_select_entryType-hidden', '', ['class' => 'pa-filter-field hidden', 'name'=>'entryType','data-attr' => 'entryType', 'data-isvar' => 'false']);
$dataUrl = Url::home() . '/experimentCreation/' . Url::to('planting-arrangement/get-filter-data');

//Notification
echo '<div id="page-notif" class="hidden"></div>';

$pluginOptions = [
    'allowClear' => true,
    'minimumInputLength' => 0,
    'placeholder' => 'Select Entry type',
    'minimumResultsForSearch' => 1,
    'language' => [
        'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
     ], 'ajax' => [
            'url' => $dataUrl,
            'dataType' => 'json',
            'delay' => 250,
            'data' => new JsExpression(
                'function (params){
                    var entryListId = "'.$entryListId.'";
                    var attr = $(this).attr("data-attr");
                    return {
                        q: params.term, 
                        attr: attr,
                        page: params.page,
                        listId: entryListId,
                        id: "'.$experimentDbId.'"
                    }
                }'
            ),
            'processResults' => new JSExpression(
                'function (data, params) {
                    
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                        more: (params.page * 5) < data.totalCount
                        },
                    };
                }'
            ),
            'cache' => true
        ]
]; 
$col2Str .= '<div class="col col-md-8 search-input-field" style="padding-left:0">' . Select2::widget([
    'id' => 'pa_select_entryType',
    'name' => 'pa_data_filter_entryType',
    'maintainOrder' => true,
    'value' => isset($filters['entryType']) ? $filters['entryType'] : null,
    'options' => [
        'tags' => true,
        'multiple' => true,
        'style' => 'width:100%',
        'data-attr' => 'entryType',
        'minimumResultsForSearch' => 1,
    ],
    'pluginOptions' => $pluginOptions,
    'pluginEvents' => [
        "change" => "
            function(){
                if($(this).val()!=''){
                    var value = $(this).val()+'';
                    var values = value.split(',');
                    var val = '', counter = 1;

                    $.each(values,function(i,v){ 
                        val = val+v;
                        
                        if(counter < values.length){
                            val = val+',';
                        }
                        counter += 1;
                    })
                    $('#pa_select_entryType'+'-hidden').val(val);
                }else{
                    $('#pa_select_entryType'+'-hidden').val($(this).val());
                }
            }",
        ],
        'showToggleAll' => false
]) . '</div></div>';


$col2Str .= '<div class="row" style="margin-top:10px; margin-bottom:0px;"><div class="col col-md-4"><label class="control-label">' . \Yii::t('app', 'Entry Class') . '</label></div>';
$col2Str .= Html::input('text', 'pa_select_entryClass-hidden', '', ['class' => 'pa-filter-field hidden', 'name'=>'entryClass','data-attr' => 'entryClass', 'data-isvar' => 'false']);
$dataUrl = Url::home() . '/experimentCreation/' . Url::to('planting-arrangement/get-filter-data');


$pluginOptions = [
    'allowClear' => true,
    'minimumInputLength' => 0,
    'placeholder' => 'Select Entry class',
    'minimumResultsForSearch' => 1,
    'language' => [
        'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
     ], 'ajax' => [
            'url' => $dataUrl,
            'dataType' => 'json',
            'delay' => 250,
            'data' => new JsExpression(
                'function (params){
                    var entryListId = "'.$entryListId.'";
                    var attr = $(this).attr("data-attr");
                    return {
                        q: params.term, 
                        attr: attr,
                        page: params.page,
                        listId: entryListId,
                        id: "'.$experimentDbId.'"
                    }
                }'
            ),
            'processResults' => new JSExpression(
                'function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                        more: (params.page * 5) < data.totalCount
                        },
                    };
                }'
            ),
            'cache' => true
        ]
];
$col2Str .= '<div class="col col-md-8 search-input-field" style="padding-left:0">' . Select2::widget([
    'id' => 'pa_select_entryClass',
    'name' => 'pa_data_filter_entryClass',
    'maintainOrder' => true,
    'value' => isset($filters['entryClass']) ? $filters['entryClass'] : null,
    'options' => [
        'tags' => true,
        'multiple' => true,
        'style' => 'width:100%',
        'data-attr' => 'entryClass',
        'minimumResultsForSearch' => 1,
    ],
    'pluginOptions' => $pluginOptions,
    'pluginEvents' => [
        "change" => "
            function(){
                if($(this).val()!=''){
                    var value = $(this).val()+'';
                    var values = value.split(',');
                    var val = '', counter = 1;

                    $.each(values,function(i,v){ 
                        val = val+v;
                        
                        if(counter < values.length){
                            val = val+',';
                        }
                        counter += 1;
                    })
                    $('#pa_select_entryClass'+'-hidden').val(val);
                }else{
                    $('#pa_select_entryClass'+'-hidden').val($(this).val());
                }
            }",
        ],
        'showToggleAll' => false
]) . '</div></div>';
?>

<ul class="collapsible" style="margin-bottom: 0px;">
    <li id="collapsible-flag" class="collapsible-search-filter">
        <div class="collapsible-header active" id="search-filter"><i class="material-icons">search</i>Filter entries</div>
        <div class="collapsible-body" style="padding: 0.2rem">
            <?php
            echo '<div class="row" style="margin-bottom:0px;"><div class="col col-md-5">' . $col1Str . '</div>';
            echo '<div class="col col-md-5" style="padding-right:0px;">' . $col2Str . '</div>';
            echo  '<div class="col col-md-2" style="padding-left:0px;">' .Html::a('Apply', '#', ['style' => 'margin-top:10px;width:105px;', 'type' => 'button', 'title' => \Yii::t('app', "Search entries"), 'class' => 'tabular-btn btn waves-effect waves-light', 'id' => 'filter-search-entries-btn']). Html::a('Reset', '#', ['style' => 'margin-top:10px;width:105px;', 'type' => 'button', 'title' => \Yii::t('app', "Reset filters"), 'class' => 'tabular-btn btn waves-effect waves-light black-text grey lighten-2', 'id' => 'filter-search-entries-reset-btn']) .'</div></div>';
            ?>
        </div>
    </li>
</ul>

<?php
// if filters are set
$selectedItems = isset($selectedItems) ? $selectedItems : [];
$selectedItemsCount = count($selectedItems) > 0 ? count($selectedItems) : 'No';
$columns = [
    [
        'label' => "Checkbox",
        'header' => '
            <input type="checkbox" class="filled-in" id="pa-select-entries-all"/>
            <label for="pa-select-entries-all" id="label-for-pa-select-entries-all" style="height: 11px;"></label>',
        'content'=>function($data) use ($selectedItems){
            $entryDbId = $data['entryDbId'];

            if(in_array($entryDbId, $selectedItems)){
                $checkbox = '<input class="pa-entries-grid_select filled-in" type="checkbox" checked="checked" id="'.$data['entryDbId'].'" />';
            }
            else{
                $checkbox = '<input class="pa-entries-grid_select filled-in" type="checkbox" id="'.$data['entryDbId'].'" />';
            }
            return $checkbox.'
                <label for="'.$data['entryDbId'].'"></label>       
            ';
        },  
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'visible' => true,
        'hidden' => false
    ],
    [
        'attribute' => 'entryNumber',
        'label' => '<span title="Sequential numbering of the entries">' . Yii::t('app', 'Entry No.') . '</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'visible' => true,
        'hidden' => false
    ],
    [
        'attribute' => 'designation',
        'label' => '<span title="Germplasm">' . Yii::t('app', 'Germplasm Name') . '</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'visible' => true,
        'hidden' => false
    ],
    'entryType',
    'entryClass',
];


$gridColumns = $columns;

$selectGroup = Select2::widget([
    'id' => 'select_group_type-id',
    'name' => 'assign-groupType',
    'class' => 'pull-right',
    'maintainOrder' => true,
    'options' => [
        'tags' => false,
        'multiple' => false,
        // 'style' => 'width:60%',
    ],
    'pluginOptions' =>  [
        'allowClear' => true,
        'minimumInputLength' => 0,
        'placeholder' => 'Select Group',
        'minimumResultsForSearch' => 1,
        'width' =>'100%'
    ],
    'pluginEvents' => [
        "select2:opening" => "function() { populateGroupSelect();}",
        ],
        'showToggleAll' => false
]);
?>
<div class="entries-result col-md-12" style="padding-left: 0px; padding-right: 0px;">
    <ul class="collapsible collapsible-result-browser" id="collapsible-result-browser">
        <li class="result-browser">
            <div class="collapsible-header" id="result-browser"><i class="material-icons">list</i>Entries</div>
            <div class="collapsible-body" id="query-results">
                <?php
                    echo '<div class="row" style="margin-right:0px;margin-bottom:0px;padding:5px;background-color:#ffffff">'.
                        '<div class="col-md-6"><span>'.\Yii::t('app', 'This lists the entries based from filters specified above.').'</span></div>'.
                        '<div class="col-md-6" style="padding-right:0px;margin-bottom:0px;"><div class="row" style="margin-bottom:5px;"><div class="col col-md-8" style="padding:0px;">'.$selectGroup.'</div><div class="col col-md-4" style="padding:0px;">'.
                        Html::a('Assign', '#', ['style' => 'margin-left:3px;width:105px;', 'type' => 'button', 'title' => \Yii::t('app', "Assign Entries to group"), 'class' => 'tabular-btn btn waves-effect waves-light btn btn-primary light-green darken-3', 'id' => 'pa-add-to-list']).'</div></div></div>'.
                        '</div>';

                    // render data browser
                    DynaGrid::begin([
                            'options' => ['id' => 'design-search-entries-grid', 'class' => $gridClass],
                            'columns' => $gridColumns,
                            'theme' => 'simple-default',
                            'storage' => 'cookie',
                            'showPersonalize' => false,
                            'showFilter' => false,
                            'showSort' => false,
                            'allowFilterSetting' => false,
                            'allowSortSetting' => false,
                            'gridOptions' => [
                                'dataProvider' => $dataProvider,
                                'showPageSummary' => false,
                                'pjax' => true,
                                'pjaxSettings' => [
                                    'neverTimeout' => true,
                                    'options' => [
                                        'id' => 'pa-search-entries-grid'
                                    ],
                                    'beforeGrid' => '',
                                    'afterGrid' => ''
                                ],
                                'responsive' => true,
                                'responsiveWrap' => false,
                                'floatHeader' => false,
                                'floatOverflowContainer' => true,
                                'panel' => [
                                    'heading' => false,
                                    'before' => '&emsp;'.'{summary}<br><p id = "selected-items" class="pull-right" style = "margin:-1px"><b id = "selected-count">'.$selectedItemsCount.'</b> selected items.</p>',
                                    'after' => false,
                                ],
                                'toolbar' => [                             
                                ],
                                'pager' => [
                                    'firstPageLabel' => 'First',
                                    'lastPageLabel' => 'Last'
                                ],
                            ]
                        ]);
                        
                     DynaGrid::end();
                ?>
            </div>
        </li>
    </ul>
</div> -->

<?php
$uiTemplateStr = json_encode($uiTemplateInfo);
$saveFiltersUrl = Url::to(['/experimentCreation/create/save-filters', 'id' => $experimentDbId]);
$assignEntriesUrl = Url::to(['/occurrence/design/generate-design', 'occurrenceDbIds' => $occurrenceDbIds, 'program' => $program, 'regenerate' => $regenerate]);
$storeSessionItemsUrl = Url::to(['/experimentCreation/design/store-session-items']);
$getSelectedItemsUrl2 = Url::to(['/experimentCreation/design/get-selected-items']);
$resetFilters = Url::to(['/experimentCreation/design/reset-session-filter', 'id' => $experimentDbId]);
$saveChanges = Url::to(['/experimentCreation/design/save-group-changes', 'id' => $experimentDbId]);

$this->registerJs(<<<JS
	var uiGroups = JSON.parse('$uiTemplateStr');
    var optionStr = '';
    var entryDbIds = [];
    var totalEntries = '$totalEntries';
    var totalEntriesCount = '$dataProvider->totalCount';

    statusEntries();
    checkSelectAll();
    function statusEntries(){
        var totalEntryIds = 0;
        for(var i=0; i<uiGroups.length; i++){
            totalEntryIds = totalEntryIds + uiGroups[i]['entryIds'].length;
        }

        $("#no-entries-span").html(totalEntryIds);
        var statusDiv = document.getElementById('no-entries-div');
        if(totalEntryIds != totalEntries){
            statusDiv.style.color = '#b71c1c';
        } else {
            statusDiv.style.color = '#1b5e20';
        }
    }

    function saveChanges(){
        $.ajax({
            url: '$saveChanges',
            type: 'post',
            data:{
                uiGroups: JSON.stringify(uiGroups)
            },
            success: function(response) {
            },
            error: function() {
            }
        });
    }

	$(document).on('click','.pa-remove-row-createGrp', function(){
       
        var id = $(this).attr("data-id");
        $(this).closest('tr').remove();

        for(var i=0; i<uiGroups.length; i++){
            if(uiGroups[i]['group'] == id){
                uiGroups[i]['entryIds'] = [];
            }
        }
        removeRow(id);
        $('#select_group_type-id').val(null).trigger('change');
        checkFields();
        $('#nFieldRow').val(null).trigger('change');
        statusEntries();
        saveChanges();
        $('#filter-search-entries-btn').trigger('click');
    });

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
    });

    // $(document).on('ready pjax:success', function(e) {
    $(document).ready(function(){
        $('.dropdown-button').dropdown();

        var selectedInfo = getSelected();
        if($('#pa-select-entries-all').prop("checked") === true)  {
            $('#pa-select-entries-all').attr('checked','checked');
            $('.pa-entries-grid_select').attr('checked','checked');
            $('.pa-entries-grid_select').prop('checked',true);
            $(".pa-entries-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
        }else{
            $('#pa-select-entries-all').removeAttr('checked');
            $('.pa-entries-grid_select').prop('checked',false);
            $('.pa-entries-grid_select').removeAttr('checked');
            $("input:checkbox.pa-entries-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
        }
        // checkSelectAll();
    });

     // refreshes page on pjax success
    $(document).on('ready pjax:success','#design-search-entries-grid-pjax', function(e) {
        e.stopPropagation();
        e.preventDefault();

        // trigger select all button for every page
        $.ajax({
            url: '$getSelectedItemsUrl2',
            type: 'post',
            dataType:'json',
            async:false,
            data: {
                experimentDbId: '$experimentDbId'
            },
            success: function(response) {
                if(response['isSelectedAll']){
                    $('#pa-select-entries-all').trigger('click')
                }
            },
            error: function() {
            }
        });
    });

    function removeRow(id){
        for(var i=0; i<(uiGroups).length;i++){
            if(uiGroups[i]['group'] == id){
                uiGroups.splice(i, 1);
            }
        }

        if(uiGroups.length==0){
            var newRow = '<tr id="no-results-id"><td colspan="4">No results found.</td></tr>';
            var tableBody = $("#create-groups-table2 tbody");
            tableBody.append(newRow);
           
        } else {
            // $('#check-interval-field-id').removeAttr('disabled');
        }
    }
	$('#group-add-rows-btn').click(function(){
        //Add new rows
        var groupType = $('#group-type-selection-id').val();
        if(uiGroups.length < 10){
            var groupNo = (uiGroups.length > 0) ? uiGroups.length+1 : 1;
            if(groupNo > 1){
                var maxGrp = uiGroups[uiGroups.length-1]['group'];
                groupNo = parseInt(maxGrp)+1;
            } else {
                $("#no-results-id").closest('tr').remove();
            }

            var groupName = 'Entry Group '+groupNo;
            
            var removeBtn = '<td><a class="pa-remove-row-createGrp" data-id="'+groupNo+'" title="Remove row" style="cursor:pointer;"><i class="material-icons">delete</i></a></td>';

            var repnoStr = '<input type="number" id="repno-'+groupNo+'"" class="repno-group_class createGroup-validate numeric-input-validate pull-left" name="createGroup-apply" value="1" style="width:75px;" min="1">';
        	var tdStr = '<td>'+groupName+'</td><td>'+groupType+'</td><td id="group-'+groupNo+'">0</td><td>'+repnoStr+'</td>'+removeBtn;
           
            var tableBody = $("#create-groups-table2 tbody");
            
            var newRow = '<tr>'+tdStr+'</tr>';

            uiGroups.push({'name':groupName,'entryIds':[],'group':groupNo, 'repno':1, 'type':groupType});
            tableBody.append(newRow);
        } else {
            var hasToast = $('body').hasClass('toast');            

            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>The maximum number of groups is 10.</span>";
                Materialize.toast(notif, 5000);
            }
        }
    });

    $(document).on('change', '.repno-group_class', function(){
        var groupId = (this.id).split('-');

        for(var i=0; i<uiGroups.length; i++){
            if(uiGroups[i]['group'] == groupId[1]){
                uiGroups[i]['repno'] = this.value;
            }
        }
        saveChanges();
        checkFields();
    });

    // search entries
    $(document).on('click','#filter-search-entries-btn',function(e){
        e.preventDefault();
        var entryNumberFlag = false;
        // Close search filter
        if($('#search-filter').hasClass('active')) {
            $('#search-filter').trigger('click')
        }

        // Open result browser
        if(!$('#result-browser').hasClass('active')) {
            $('#result-browser').trigger('click')
        }

        var filters = {};

        // build query parameters
        $(".pa-filter-field").each(function( index ) {
            var obj = $(this);
            var attr = obj.data('attr');
            var isvar = obj.data('isvar');
            var values = [];

            // build query parameters
            if(attr == 'entryNumber' && entryNumberFlag == false){
                var from = $('.pa-filter-entryNumber-field-from').val();
                var to = $('.pa-filter-entryNumber-field-to').val();

                values = {operator:'range',from:from,to:to};
                entryNumberFlag = true;
                filters[[attr]] = values;
            }else if(attr != 'entryNumber'){
                if(attr == 'designation'){
                    $.each($('#pa_select_'+attr+' option:selected'),function(){
                        values.push($(this).text() );
                    });                    
                }else{
                    $('#pa_select_'+attr).select2('data').map(function(elem){ 
                        values.push(elem.text);
                    });
                }

                filters[[attr]] = values;
            }
        });

        var notIncludedEntries = [];
        for(var i=0; i<uiGroups.length; i++){
            notIncludedEntries = notIncludedEntries.concat(uiGroups[i]['entryIds']);
        }

        filters['notIncludedEntries'] = notIncludedEntries;

        $.ajax({
            url: '$saveFiltersUrl',
            type: 'post',
            data:{
                filters: JSON.stringify(filters)
            },
            success: function(response) {
                $('#select_group_type-id').val(null).trigger('change');
                $.pjax.reload({
                    type: 'POST',
                    url: '$assignEntriesUrl',
                    data: {pjax: true},
                    container: '#design-search-entries-grid-pjax', 
                    replace:false
                });
                
                $('#design-search-entries-grid').removeClass('hidden');
            },
            error: function() {
            }
        });
        resetSelection();
    });

    
    // reset filters
    $(document).on('click','#filter-search-entries-reset-btn', function(e){
        e.preventDefault();
        var entryNumberFlag = false;
        // Close search filter
        if($('#search-filter').hasClass('active')) {
            $('#search-filter').trigger('click')
        }

        // Open result browser
        if(!$('#result-browser').hasClass('active')) {
            $('#result-browser').trigger('click')
        }

        var filters = {};

        // build query parameters
        $(".pa-filter-field").each(function( index ) {
            var obj = $(this);
            var attr = obj.data('attr');
            var isvar = obj.data('isvar');
            var values = [];
            filters[[attr]] = values;

            if(attr == 'entryNumber'){
                $('.pa-filter-entryNumber-field-to').val('');
                $('.pa-filter-entryNumber-field-from').val('');
            }else{
                $('#pa_select_'+attr).val("-1").trigger("change");
            }
        });

        var notIncludedEntries = [];
        for(var i=0; i<uiGroups.length; i++){
            notIncludedEntries = notIncludedEntries.concat(uiGroups[i]['entryIds']);
        }

        filters['notIncludedEntries'] = notIncludedEntries;

        $.ajax({
            url: '$saveFiltersUrl',
            type: 'post',
            data:{
                filters: JSON.stringify(filters)
            },
            success: function(response) {
                $('#select_group_type-id').val(null).trigger('change');
                $.pjax.reload({
                    type: 'POST',
                    url: '$assignEntriesUrl',
                    data: {pjax: true},
                    container: '#design-search-entries-grid-pjax', 
                    replace:false
                });
                
                $('#design-search-entries-grid').removeClass('hidden');
            },
            error: function() {
            }
        });
        resetSelection();
    });

    // select all entries
    $(document).on('click','#pa-select-entries-all',function(e){
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.pa-entries-grid_select').attr('checked','checked');
            $('.pa-entries-grid_select').prop('checked',true);
            $(".pa-entries-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
        }else{
            $(this).removeAttr('checked');
            $('.pa-entries-grid_select').prop('checked',false);
            $('.pa-entries-grid_select').removeAttr('checked');
            $("input:checkbox.pa-entries-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
        }

        var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
        var unset = $(this).prop('checked') ? false : true;
        var url = '$storeSessionItemsUrl';

        $.ajax({
            url: url,
            type: 'post',
            data: {
                entryDbId: 0,
                unset: unset,
                storeAll: true,
                experimentDbId : '$experimentDbId'
            },
            success: function(response) {
                $('#selected-count').html(response);
                
                if(unset){
                    $('#selected-count').html('No');
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    });


    //select grid
    $(document).on('click','.pa-entries-grid_select',function(e){
  
        e.preventDefault();
        e.stopImmediatePropagation();

        var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
        var unset;
        var url = '$storeSessionItemsUrl';

        if($(this).prop('checked')){
            unset = false;
            $('#pa-add-to-list').removeClass('disabled');
        }else{
            unset = true;
            $('#pa-select-entries-all').prop('checked',false);
            $('#pa-select-entries-all').removeAttr('checked');
        }

        $.ajax({
            url: url,
            type: 'post',
            data: {
                entryDbId: $(this).prop('id'),
                unset: unset,
                storeAll: false,
                experimentDbId : '$experimentDbId'
            },
            success: function(response) {
                $('#selected-count').html(response);
                if(totalEntriesCount == response) {
                    $('#pa-select-entries-all').trigger('click');
                } else {
                    $('#pa-select-entries-all').prop('checked',false);
                    $('#pa-select-entries-all').removeAttr('checked');
                }

                response = response == 0 ? 'No': response;
                $('#selected-entry-count').html(response);
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

     // click row in browser
    $(document).on('click',"#design-search-entries-grid tbody tr",function(e){

        e.preventDefault();
        e.stopImmediatePropagation();
        var thisRow = $(this).find('input:checkbox')[0];

        $("#"+thisRow.id).trigger("click");
        if(thisRow.checked){
            $(this).addClass("grey lighten-4");
        }else{
            $(this).removeClass("grey lighten-4");
        }

       
    });

    // assign more entries
    $(document).on('click','#pa-add-to-list',function(e){
        
        var groupId = $('#select_group_type-id').val();
        var groupType = '';
        //search for group type
        for(var i=0; i<uiGroups.length; i++){
            if(uiGroups[i]['group'] == groupId){
                groupType = uiGroups[i]['type'];
            }
        }

        e.preventDefault();
        e.stopImmediatePropagation();

        var ids = [];
        var isSelectAll = false;

        $.ajax({
            url: '$getSelectedItemsUrl2',
            type: 'post',
            dataType:'json',
            async:false,
            data: {
                experimentDbId: '$experimentDbId',
                groupType: groupType
            },
            success: function(response) {
                if(response['selectedItemsCount'] > 0){
                    ids = response['selectedItems'];
                    isSelectAll = true;
                }
                if(response['removedEntries'] > 0){
                    var notif = "<i class='material-icons orange-text left'>warning</i> There are "+response['removedEntries']+" entries that are not assigned. Mismatched entry type value.";
                    Materialize.toast(notif,5000);
                }
            },
            error: function() {
            
            }
        });
       
        for(var i=0; i<uiGroups.length; i++){
            if(uiGroups[i]['group'] == groupId){
                uiGroups[i]['entryIds'] = uiGroups[i]['entryIds'].concat(ids);
                $('#group-'+groupId).html(uiGroups[i]['entryIds'].length);
            }
        }

        var url = '$storeSessionItemsUrl';
        var unset = true;
        $.ajax({
            url: url,
            type: 'post',
            data: {
                entryDbId: 0,
                unset: unset,
                storeAll: true,
                experimentDbId : '$experimentDbId'
            },
            success: function(response) {
                $('#selected-count').html(response);
                
                if(unset){
                    $('#selected-count').html('No');
                }
            },
            error: function(e) {
                console.log(e);
            }
        });


        statusEntries();
        $('#select_group_type-id').val(null).trigger('change');
        checkFields();
        $('#nFieldRow').val();
        saveChanges();
        $('#filter-search-entries-btn').trigger('click');
    });

    function populateGroupSelect(){
        $('#select_group_type-id').empty();
        $('#select_group_type-id').append('<option></option>');
        for(var i=0; i<uiGroups.length; i++){
            optionStr = '<option value="'+uiGroups[i]['group']+'">'+uiGroups[i]['name']+' ('+uiGroups[i]['type']+')</option>';
            $('#select_group_type-id').append(optionStr);
        }
    }

    //retrieve user's session selection
    function getSelected(){
        var userSelection;
        $.ajax({
            url: '$getSelectedItemsUrl2',
            type: 'post',
            dataType:'json',
            async:false,
            data: {
                experimentDbId: '$experimentDbId'
            },
            success: function(response) {
                userSelection = response;
            },
            error: function() {
            
            }
        });
        return userSelection;
    }

    function resetSelection(){
        var url = '$storeSessionItemsUrl';
        var unset = true;
        $.ajax({
            url: url,
            type: 'post',
            data: {
                entryDbId: 0,
                unset: unset,
                storeAll: true,
                experimentDbId : '$experimentDbId'
            },
            success: function(response) {
                $('#selected-count').html(response);
                
                if(unset){
                    $('#selected-count').html('No');
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    }

    function checkSelectAll(){
        //get summary
        var summaryElement = $('#design-search-entries-grid').find('.summary')[0];
        var checkAll = false;
        if(summaryElement != undefined){
            var summaryStr = summaryElement.innerText;
            if(summaryStr != undefined){
                var summaryArr = summaryStr.split(" of ");
                if(summaryArr[1] != undefined){
                    summaryArr = summaryArr[1].split(" items");
                    
                    if($('#selected-count').html() == summaryArr[0]){
                        checkAll = true;
                    } else {
                        checkAll = false;
                    }
                }
            }
        }

        if(!checkAll){
            $('#pa-select-entries-all').prop('checked',false);
            $('#pa-select-entries-all').removeAttr('checked');
        }else{
            $('#pa-select-entries-all').prop('checked',true);
            $('#pa-select-entries-all').attr('checked', 'checked');
        }
    }

JS
);
?>


<style>
    .tabs-left .tab-content {
        border-bottom: none !important;
    }
    .box-body {
        padding: 0px 15px 10px 15px;
        max-height: 800px;
        overflow-y: hidden;
        overflow-x: hidden;
        background-color: #f1f4f5;
    }
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
        opacity: 0;
    }

    #create-groups-table2 > tbody > tr > td {
    	 vertical-align: middle !important; 
    }
    #create-groups-table2 > tbody > tr > th {
    	 padding: 3px 3px !important; 
    }
    #create-groups-table2 > tbody > tr > td{
    	 line-height: : 15px !important; 
    	 padding: 3px 3px !important; 
    }
    .repno-group_class {
    	margin: 0 0 0px 0 !important;
    	height: 30px !important;
    }
    #select_group_type-id > span.select2-container {
        display: inline-block !important; 
        margin-left: 20px !important;
    }

</style>