<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders design step for experiment creation
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use app\models\Program;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;

use app\modules\experimentCreation\models\TransactionModel;
?>
    <div id="mod-notif" class="hidden"></div>
    <div class="row col col-md-12">

        <p>Select the parameter set that you want to be used for the new occurrences.</p>

    <div class="col col-md-12 pull-right view-entity-content" style="margin-top:10px;overflow: hidden !important;" id="parameter-sets-panel">

            <?php 
                $parameterSetCount = $parameterSetsProvider->getCount();
                echo $grid = GridView::widget([
                    'pjax' => true, // pjax is set to always true for this demo
                    'dataProvider' => $parameterSetsProvider,
                    'id' => 'parameter-sets-browser', 
                    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
                    'striped'=>false,
                    'showPageSummary'=>false,
                    'columns'=>$columns
                ]);
            ?>
        </div>
</div>