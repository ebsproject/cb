<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for the Design Controller
*/

namespace app\models;
namespace app\modules\occurrence\models;

use app\models\Entry;
use app\models\EntrySearch;
use app\models\EntryList;
use app\models\Experiment;
use app\models\Item;
use app\models\Occurrence;
use app\models\PlanTemplate;
use app\models\Scale;
use app\models\User;
use app\models\Variable;

use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;

use app\modules\experimentCreation\models\AnalysisModel;
use app\modules\experimentCreation\models\PlantingArrangement;
use app\modules\experimentCreation\models\VariableComponent;

use Yii;
class DesignModel {
    /**
     * Model constructor
     */
     public function __construct(
        protected AnalysisModel $analysisModel, 
        protected Entry $entry,  
        protected EntrySearch $entrySearch, 
        protected EntryList $entryList, 
        protected Experiment $experiment,
        protected Item $item,
        protected Occurrence $occurrence,
        protected PlanTemplate $planTemplate,
        protected PlantingArrangement $plantingArrangement,
        protected Scale $scale,
        protected User $user,
        protected Variable $variable,
        protected VariableComponent $variableComponent
    )
    {}

    /**
     * Get experiment design information of the selected occurrences
     *
     * @param string $occurrenceDbIds list of occurrence ids
     * @param integer $experimentDbId identifier of the experiment record
     * @param array $occurrenceDataArray information about the selected occurrences
     * @param string $program current program of the user
     * @param string $regenerate flag if new occurrence or regenerate
     */
    public function getDesignInformation($occurrenceDbIds, $experimentDbId, $occurrenceDataArray, $program, $regenerate = "true") {

        //get and build entry view
        $entryListId = $this->entryList->getEntryListId($experimentDbId);
        $entryListCheckArray =  $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals check'], '', true);
        $entryListEntryArray = $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals test'], 'limit=1', false);
        $entryListTotalArray = $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType','entryListDbId'=>"equals $entryListId"], 'limit=1', false);

        $model = $this->experiment->getExperiment($experimentDbId);

        //build entry information gridview
        $entryInfo  = array();
        $entryInfo['check'] = $entryListCheckArray['totalCount'];
        $entryInfo['test'] = $entryListEntryArray['totalCount'];
        $entryInfo['total'] = $entryListTotalArray['totalCount'];

        $designFields = array();
        $layoutFields = array();
        $rules = array();
        $paramValues = array();
        
        //POST-search call
        // will check if there's already a transaction for the selected occurrences
        $param = ['entityType'=>'occurrence', 'occurrenceApplied'=> "equals ".implode(";",$occurrenceDbIds)]; 
        $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param, 'sort=creationTimestamp:DESC');
        
        // if there's no transaction yet for the selected occurrences, retrieve experiment transaction and copy
        if($jsonInput == NULL){

            //create new plan template derived from EC
            $param = ['entityType'=>'experiment', 'entityDbId'=> "equals $experimentDbId"];
            $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);
            
            if($jsonInput == NULL) { //redirect user to EM browser
                $notif = 'error';
                $icon = 'times';
                $message = "There is no randomization transaction for the selected occurrences.";
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
                Yii::$app->response->redirect(['/occurrence','program'=>$program])->send();
                exit();
            }
            //create plan template record
            $experimentJson = [];
            $experimentJson['templateName'] = str_replace('#','_',(str_replace(' ', '_',$occurrenceDataArray[0]['occurrence']))).'_'.implode("_",$occurrenceDbIds);
            $experimentJson['entityType'] = "occurrence";
            $experimentJson['entityDbId'] = $occurrenceDbIds[0];
            $experimentJson['design'] = $jsonInput['design'] ?? $model['experimentDesignType'];
            $programDbId = $model['programDbId'];
            $experimentJson['programDbId'] = "$programDbId";
            $experimentJson['status'] = ($regenerate == "true") ? 'randomized':'draft';
            $experimentJson['occurrenceApplied'] = implode(";",$occurrenceDbIds);

            //update no. of occurrences value in the copied template
            $mandatoryInput = isset($jsonInput['mandatoryInfo']) ? json_decode($jsonInput['mandatoryInfo'], true) : [];

            if(!empty($mandatoryInput)){
                
                if($regenerate == "false"){
                    $mandatoryInput['totalOccurrences'] = count($occurrenceDbIds);
                } else if(isset($mandatoryInput['totalOccurrences'])){
                    $mandatoryInput['totalOccurrences'] = count($occurrenceDbIds);
                } else {
                    $searchIdx = array_search("nTrial",array_column($mandatoryInput['design_ui']['randomization'], 'code'));

                    if($searchIdx === false){
                        $searchIdx = array_search("nFarm",array_column($mandatoryInput['design_ui']['randomization'], 'code'));
                    }

                    if($searchIdx !== false){
                        $mandatoryInput['design_ui']['randomization'][$searchIdx]['value'] = count($occurrenceDbIds);
                    }
                }
            }

            $experimentJson['mandatoryInfo'] = json_encode($mandatoryInput);
            $experimentJson['randomizationInputFile'] = isset($jsonInput['randomizationInputFile']) ? json_encode($jsonInput['randomizationInputFile']) : "{}";
            
            //create new plan template for the selected occurrences
            $this->planTemplate->createPlanTemplate($experimentJson);

            //get and use the newly created template
            $param = ['entityType'=>'occurrence', 'occurrenceApplied'=> "equals ".implode(";",$occurrenceDbIds)]; 
            $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);
        }
   
        $designVariable = $this->variable->getVariableByAbbrev('DESIGN');
        
        $experimentDesign = isset($model['experimentDesignType'])? $model['experimentDesignType']:NULL;

        $designAF = NULL;
        $parameter = NULL;
        $showLayout = FALSE;
        $planStatus = NULL;
        $uploaded = false;
        $testList = false;
        $checkList = false;
        $filters = [];
        $uiTemplateInfo = [];
        $dataProvider = new \yii\data\ArrayDataProvider([]);
        $gridClass = '';
        $generateLayout = "false";
        $uiTemplate = 'none';
        $storedItems = [];

        //check if the transaction is randomization or upload
        if(isset($jsonInput) && $jsonInput != NULL){

            $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);
            if(isset($jsonArr['totalOccurrences'])){
                $uploaded = true;
                $designAF = isset($jsonArr['design_id']) ? $jsonArr['design_id'] : NULL;
            } else {
                $parameter = isset($jsonArr['design_ui']) ? $jsonArr['design_ui'] : NULL;
                $showLayout = isset($jsonArr['design_ui']['show_layout']) ? $jsonArr['design_ui']['show_layout'] : FALSE;
                $designAF = isset($jsonArr['design_id']) ? $jsonArr['design_id'] : NULL;
            }

            

            $planStatus = $jsonInput['status'];
        } else {
            $generateLayout = "true";
        }

        if(isset($jsonInput['status']) && (in_array($jsonInput['status'], ['randomized', 'uploaded']))){
            if($jsonInput['status'] == 'uploaded'){
                $uploaded = true;
            }
        }
        else if(isset($jsonInput['status']) && (in_array($jsonInput['status'],['randomization in progress','upload in progress']))){
            
            if($jsonInput['status'] == 'upload in progress'){
                $uploaded = true;
            }
        }

        // for randomization transaction
        if(!$uploaded){

            //get mandatory parameters for randomization
            if(isset($parameter['randomization'])){

                foreach($parameter['randomization'] as $key=>$var){
                    if($var['code'] == 'genLayout'){
                        $generateLayout = "true";
                    }
                    if($var['code'] == 'checkList'){
                        $checkList = true;
                    }else if($var['code'] == 'testList'){
                        $testList = true;
                    }
                    if(isset($var['ui']['visible']) && $var['ui']['visible']){
                        
                        if(in_array($var['code'], ['nTrial', 'nFarm'])){ //disable fields if nTrial or nFarm variable
                            $var['ui']['readonly'] = true;
                        }
                        $designFld = $this->variableComponent->generateDesignField($var, 'randomization', $key);
                        $designFields[] = $designFld;
                        $paramValues[$var['code']] = $designFld['defaultValue'];
                    }
                }
            }

            //get layout parameters for randomization
            if(isset($parameter['layout'])){

                foreach($parameter['layout'] as $key=>$var){
                    if(isset($var['ui']['visible']) && $var['ui']['visible']){
                        $layoutFld = $this->variableComponent->generateDesignField($var, 'layout', $key);
                        $layoutFields[] = $layoutFld;
                        $paramValues[$var['code']] = $layoutFld['defaultValue'];
                    }
                }
                if(count($parameter['layout']) == 0){
                    $showLayout = FALSE;
                    $generateLayout = "true";
                }
            }

            if(isset($parameter['rules'])){
                $rules = $parameter['rules'];
            }

            //check if the design selected uses the testList and checkList variable
            if($testList){
                $uiTemplate = 'testList';
            } else if($checkList){
                $uiTemplate = 'checkList';
            }

            // if design requires testList or checkList information
            if($uiTemplate != 'none'){
                
                $uiTemplateInfo = isset($parameter['groups']) ? $parameter['groups'] : [];

                //if UI is for checkList (Design: P-Rep with diagonal checks)
                if($uiTemplate == 'checkList' &&  empty($uiTemplateInfo)){
                    $arrayRep = array_fill(0, $entryInfo['check'], 2);
                    $checkEntryDbIds = array_column($entryListCheckArray['data'], 'entryDbId');
                    $checkRep = array_combine($checkEntryDbIds, $arrayRep);
                    $uiTemplateInfo = $checkRep;
                    $jsonArr['design_ui']['groups'] = $uiTemplateInfo;
                } elseif($uiTemplate == 'checkList' &&  !empty($uiTemplateInfo)){
                    $entryDbIds = array_keys($uiTemplateInfo);
                    $checkEntryDbIds = array_column($entryListCheckArray['data'], 'entryDbId');
                    $noReps = array_values(array_diff($checkEntryDbIds, $entryDbIds));
                    if(!empty($noReps)){
                        $arrayRep = array_fill(0, count($noReps), 2);
                        $checkRep = array_combine($noReps, $arrayRep);
                        $uiTemplateInfo = array_merge($uiTemplateInfo, $checkRep);
                        $jsonArr['design_ui']['groups'] = $uiTemplateInfo;
                    }
                }


                // prepare entry browser
                $userId = $this->user->getUserId();
                $filteredIdName = "ec"."_filtered_entry_ids_$experimentDbId";
                $selectedIdName = "ec"."_selected_entry_ids_$experimentDbId";
                
                //destroy selected items session
                if(((isset($_GET['reset']) && $_GET['reset'] == 'hard_reset' && !isset($_GET['grid-entry-list-grid-page'])) ||
                    (!isset($_GET['reset']) && !isset($_GET['grid-entry-list-grid-page']))) && isset($_SESSION[$selectedIdName])){
                    unset($_SESSION[$selectedIdName]);
                    if(isset($_SESSION[$filteredIdName])){
                        unset($_SESSION[$filteredIdName]);
                    }
                }

                // get params for data browser
                $paramPage = '';
                $paramSort = '';

                if (isset($_GET['sort'])) {
                    $paramSort = 'sort=';
                    $sortParams = $_GET['sort'];
                    $sortParams = explode('|', $sortParams);
                    $countParam = count($sortParams);
                    $currParam = 1;

                    $sortOrder = array();

                    foreach ($sortParams as $column) {
                        if ($column[0] == '-') {
                            $sortOrder[substr($column, 1)] = SORT_DESC;
                            $paramSort = $paramSort . substr($column, 1) . ':desc';
                        } else {
                            $sortOrder[$column] = SORT_ASC;
                            $paramSort = $paramSort . $column;
                        }
                        if ($currParam < $countParam) {
                            $paramSort = $paramSort . '|';
                        }
                        $currParam += 1;
                    }
                    $paramSort = '&'.$paramSort;
                }

                if (isset($_GET['dp-1-page'])){
                    $paramPage = '&page=' . $_GET['dp-1-page'];
                } else if (isset($_GET['page'])) {
                    $paramPage = '&page=' . $_GET['page'];
                } else if (isset($_GET['grid-entry-list-grid-page'])) {
                    $paramPage = '&page=' . $_GET['grid-entry-list-grid-page'];
                }
                $paramLimit = '?limit='.getenv('CB_DATA_BROWSER_DEFAULT_PAGE_SIZE');

                $pageParams = $paramLimit.$paramPage;

                // append sorting condition
                $pageParams .= empty($pageParams) ? '?'.$paramSort : $paramSort;

                $urlTemp = Yii::$app->request->queryParams;
                $urlParams = null;
                foreach($urlTemp as $key=>$val){
                    if(!in_array($key, ['occurrenceDbIds', 'regenerate'])){
                        $urlParams[$key] = $val;
                    }
                }
                $urlParams['id'] = $experimentDbId;
                
                //if UI uses testList (entry browser has separate filter form) and has pre-saved session
                if (Yii::$app->session->get('design-entries-filters-' . $experimentDbId) !== null && $uiTemplate == 'testList') {
                    $filters = Yii::$app->session->get('design-entries-filters-' . $experimentDbId);
                    Yii::$app->session->set('design-filtersForEntries-'.$experimentDbId, $filters); 
                    if(!empty($uiTemplateInfo)){
                        $entryIdFilters = [];
                        foreach ($uiTemplateInfo as $info) {
                            $entryIdFilters = array_merge($entryIdFilters, $info['entryIds']);
                        }
                        if(!empty($entryIdFilters)){
                            $filters['notIncludedEntries'] = $entryIdFilters;
                        }
                    }
                    $dataProvider = $this->plantingArrangement->searchEntries($experimentDbId, [], $filters, $urlParams, $this->entrySearch,$pageParams);

                    $gridClass = '';
                } else {
                    $storedFilter = 'empty';
                    //if UI uses testList (entry browser has separate filter form) and does not have pre-saved session
                    if(!empty($uiTemplateInfo) && $uiTemplate == 'testList'){
                        $entryIdFilters = [];
                        foreach ($uiTemplateInfo as $info) {
                            $entryIdFilters = array_merge($entryIdFilters, $info['entryIds']);
                        }
                        if(!empty($entryIdFilters)){
                            $storedFilter = [];
                            $storedFilter['notIncludedEntries'] = $entryIdFilters;
                        }
                    // UI defaults to checklist and will just show check entries
                    } else if($uiTemplate == 'checkList'){
                        $storedFilter = [];
                        $storedFilter['entryType'] = ['check'];
                    }
                    
                    $dataProvider = $this->plantingArrangement->searchEntries($experimentDbId, [], $storedFilter, $urlParams, $this->entrySearch,$pageParams);
                }

                $storedItems = Yii::$app->session->get($selectedIdName);
            }   
        } else {
            $designFields = $jsonArr;
            $generateLayout = "true";
        }
        
        $designArray = [];
        $designInfo = []; 
        $designDesc = [];

        //get designs from BA
        $analysisModel = new AnalysisModel();
        $designSG = $analysisModel->getDesignCatalogue();
       
        // if there's an error in the response, show a notification
        if(!isset($designSG['data']['designModel']['children'])){
            Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning"></i> The design catalogue cannot be accessed. Please try to refresh the page.');
            $designArray = [];
            $jsonInput = NULL;
            $designFields = [];
            $layoutFields = [];
            $rules = [];
        } else {
            //generate template json for selected design
            $designRecord = $this->variable->getVariableByAbbrev('DESIGN');
            $designVariable = $designRecord['variableDbId'];

            //get scale values for design
            $scaleRecords = $this->scale->getVariableScale($designVariable)['scaleValues'];
            $scaleValues = array_column($scaleRecords, 'value');
            
            $scaleValuesKey = array_map('strtoupper', $scaleValues);

            $designList = $designSG['data']['designModel']['children'];
            usort($designList,function($first,$second){
              return strcmp($first['name'], $second['name']);
            }); 

            //map design from BA to the designs from CB scale value
            $designDesc = [];
            foreach($designList as $design){
                $tempString = explode("(", $design['name']);

                $searchword = strtoupper(trim($tempString[0]));

                $matches = array_filter($scaleValuesKey, function($var) use ($searchword) { return preg_match("/($searchword)/i", $var); });


                if(!empty($matches) && count($matches) > 0){
                    $designArray[$design['id']] = $design['variableLabel'];
                    $designInfo[$design['id']]  = [
                        'designCode' => $design['code'],
                        'designName' => $design['name']
                    ];

                    $designDesc[$design['id']] = $design['description'];
                }
            }

            //Add Unspecified design
            $designArray[0] = "Unspecified";
            $designInfo[0] = [
                'designCode' => 'unspecified',
                'designName' => 'Unspecified'
            ];
            $designDesc[0] = 'This design is populated currently only via upload.';
            
            uasort($designArray,function($first,$second){
              return strcmp($first, $second);
            });
        }
        
        //check if there are plot records, for deletion
        $plotRecords = $this->occurrence->searchAllPlots($occurrenceDbIds[0], null, 'limit=1', false)['totalCount'];
        
        //create entry info data provider
        $entryInfoDataProvider = new ArrayDataProvider([
          'key'=>'total',
          'allModels' => [$entryInfo]
        ]);

        $configData = $this->variableComponent->getDesignConfig();

        $userModel = new User();
        $userId = $userModel->getUserId();

        return [
            'experimentDbId' => $experimentDbId,
            'model' => $model,
            'experimentDesign' => $designAF,
            'designFields' => $designFields,
            'layoutFields' => $layoutFields,
            'planDesign' => $jsonInput,
            'withChecks' =>  $entryInfo['check'] > 0 ? TRUE:FALSE,
            'showLayout' => $showLayout,
            'entryInfo' => json_encode($entryInfo),
            'designArray' => $designArray,
            'userId' =>$userId,
            'entryInfoDataProvider' => $entryInfoDataProvider,
            'deletePlots' => $plotRecords > 0 ? 'true' : 'false',
            'planStatus' => $planStatus,
            'rules' => json_encode($rules),
            'paramValues' => json_encode($paramValues),
            'designInfo'=> json_encode($designInfo),
            'design' => $experimentDesign,
            'uploaded' => $uploaded,
            'uiTemplate' => $uiTemplate,
            'uiTemplateInfo' => $uiTemplateInfo,
            'entryListId' => $entryListId,
            'dataProvider' => $dataProvider,
            'filters' => $filters,
            'gridClass' => $gridClass,
            'generateLayoutShow' => $generateLayout,
            'selectedItems' => $storedItems,
            'totalEntries' => $entryInfo['total'],
            'designDesc' => $designDesc,
            'configData' => $configData
        ];
    }
}