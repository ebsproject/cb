<?php
/* 
* This file is part of EBS-Core Breeding. 
* 
* EBS-Core Breeding is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* 
* EBS-Core Breeding is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
* GNU General Public License for more details. 
* 
* You should have received a copy of the GNU General Public License 
* along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

namespace app\modules\occurrence\models;

use Yii;
use app\controllers\Dashboard;
use app\models\BaseModel;
use app\models\DataBrowserConfiguration;
use app\models\Experiment;
use app\models\ExperimentProtocol;
use app\models\Item;
use app\models\Lists;
use app\models\ListMember;
use app\models\Program;
use app\models\UserDashboardConfig;
use app\models\Variable;
use app\modules\dashboard\models\DashboardModel;
use yii\helpers\Url;

/**
 * Methods for Experiment Manager
 */
class Manage extends BaseModel 
{
    public function __construct (
        public Dashboard $dashboard,
        public DataBrowserConfiguration $dataBrowserConfig,
        public Experiment $experiment,
        public ExperimentProtocol $experimentProtocol,
        public Item $item,
        public Lists $lists,
        public ListMember $listMember,
        public Program $program,
        public UserDashboardConfig $userDashboardConfig,
        public Variable $variable,
        $config = []
    )
    {
        parent::__construct($config);
    }
    
    /**
     * Save Occurrence browser filters
     *
     * @param array $data list of dashboard filters
     * @param array $texts list of occurrence text filters
     * @param integer $userId user identifier
     * @param string $currentUrl current URL
     * @return string $newUrl new URL
     */
    public function applyFilters($data, $texts, $userId, $currentUrl){
        $currentUrl = str_replace('""}', '$"}', $currentUrl);
        $currentUrl = str_replace('""', '"$', $currentUrl);
        $currentUrl = json_decode($currentUrl);

        $programId = 0;
        $dashboardAttrs = ['owned', 'program', 'season', 'site', 'stage', 'year'];
        $dashboardFilters = [];
        $occurrenceFilters = [];
        $occurrenceTextFilters = [];

        foreach ($data as $key => $value) {
            $key = key($value);
            $val = $value[key($value)];
            // if in dashboard filters
            if (in_array($key, $dashboardAttrs)) {
                $keyField = (in_array($key, ['year', 'owned'])) ? $key : $key . '_id';

                $dashboardFilters[$keyField] = $val;

                if ($key == 'program')
                    $programId = intval($val);
            } else {
                // else, if not in dashboard filters, save in data browser config
                $occurrenceFilters[$key] = $val;
            }
        }
        if(!empty($texts)){
            foreach ($texts as $k => $v) {
                if ($k != 'program'){
                    $key = key($v);
                    $val = $v[key($v)];
                    $occurrenceTextFilters[$key] = $val;
                }
            }
        }

        // save in dashboard filters
        $result = $this->userDashboardConfig->saveDashboardFilters($userId, $dashboardFilters);

        // if there is a problem saving dashboard config
        if($result=='error') {
            return Yii::$app->response->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl)->send();
            exit;
        }

        // save in user data browser extends BaseModel config
        $this->dataBrowserConfig->saveConfig('filter', $userId, 'occurrences-grid', $occurrenceFilters);
        $this->dataBrowserConfig->saveConfig('filter', $userId, 'occurrences-text-grid', $occurrenceTextFilters);

        // set occurence filters and text filters to session
        $session = Yii::$app->session;
        $session->set('dashboard-occurrence-filters', $occurrenceFilters);
        $session->set('dashboard-occurrence-text-filters', $occurrenceTextFilters);

        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully applied data filters.<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');

        $newProgram = $this->program->getProgramAttr('id', $programId, 'abbrev');
        //reset saved URL
        $this->dashboard->resetSavedUrl($newProgram);

        // redirect to tool's landing page
        $module = isset($_POST['module']) ? $_POST['module'] : null;
        $controller = isset($_POST['controller']) ? $_POST['controller'] : null;
        $action = isset($_POST['action']) ? $_POST['action'] : null;

        // get current url
        $url = Yii::$app->request->referrer;
        // get url parameters
        $url = explode('?', $url);
        $initUrl = (isset($url[0])) ? $url[0] : '';

        // if module is not empty
        if(!empty($module) && isset($url[0]) && $module != 'dashboard' && $controller != 'default'){
            $processedUrl = explode($module, $url[0]);

            // get initial page of the module
            $initialUrl = DashboardModel::getInitialPageByModule($module, $controller, $action);

            // if processed initial url is not empty
            if(!empty($initialUrl)){
                $initUrl = $processedUrl[0].$initialUrl;
            }
        }

        $otherParams = '';
        //append other url parameters
        if (!empty($currentUrl)) {
            foreach ($currentUrl as $key => $value) {
                if (strtolower($key) !== 'program') {
                    if (is_scalar($value)) {
                        $value = str_replace('$', '"', $value);

                        $otherParams .= '&' . $key . '=' . $value;
                    }
                }
            }
        }

        $newUrl = $initUrl . '?program=' . $newProgram . $otherParams;

        return $newUrl;
    }

    /**
     * Confirm make crosses by Experiment ID
     * Update experiment data process and status
     *
     * @param integer $id experiment identifier
     * @return integer $itemId data process identifier
     */
    public function confirmMakeCrosses($id)
    {
        $id = isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : 0;

        // check if all entry roles are specified for the entries of the experiment
        $params = [
            'experimentDbId' => (string) $id,
            'entryRole' => 'null'
        ];

        $entryModel = \Yii::$container->get('app\models\Entry');
        $result = $entryModel->searchAll($params);

        // if there are entry roles not specified
        if (isset($result['data']) && !empty($result['data'])) {
            $status = 'planted;draft';
        } else {
            $status = 'planted;entry list created';
        }

        // get cross parent nursery phase II ID
        $itemParam['abbrev'] = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS';
        $item = $this->item->searchAll($itemParam);

        $itemId = (isset($item['data'][0]['itemDbId'])) ? $item['data'][0]['itemDbId'] : null;

        if (!empty($itemId)) {
            // deactivate Phase I traits
            $listId = $this->experimentProtocol->getListId($id);

            if ($listId) {
                $listMembersInfo = $this->lists->searchAllMembers($listId);
                $listMembersId = array_column($listMembersInfo['data'], 'listMemberDbId');
                $this->listMember->updateMany($listMembersId, [
                    'isActive' => false
                ]);
                //load crossing_date trait by default
                $variable = $this->variable->getVariableByAbbrev('DATE_CROSSED');

                $requestData[] = [
                    'id' => $variable['variableDbId'],
                    'displayValue' => $variable['label'],
                ];
                
                $this->lists->createMembers($listId, $requestData);
            }

            // update data process and status of experiment
            $data['dataProcessDbId'] = (string) $itemId;
            $data['experimentStatus'] = $status;
            $this->experiment->updateOne($id, $data);
        }

        return json_encode($itemId);
    }

    /**
     * Retrieve experiment data by experiment ID and variable abbrevs
     *
     * @param integer $id experiment identifier
     * @param array $variableAbbrevs list of variables to be retrieved
     * @return list of experiment data
     */
    public function getDataByExperimentId($id, $variableAbbrevs){
        $method = 'POST';
        $path = 'experiments/'.$id.'/data-search';
        $abbrevs = implode('|', $variableAbbrevs);
        $params['abbrev'] = $abbrevs;

        $response = Yii::$app->api->getParsedResponse($method, $path, json_encode($params));

        if(
            isset($response['data'][0]['data']) && 
            !empty($response['data'][0]['data']) && 
            isset($response['data'][0]['data'])
        )
            return $response['data'][0]['data'];

        return null;
    }

    /**
     * Gets status of seeds harvested
     *
     * @param $locationId integer location identifier
     * @param $occurrenceId integer occurrence identifier
     * @return array status monitoring data for seeds harvested
     */
    public function getSeedsHarvestedStatus($locationId=null, $occurrenceId=null){
        $harvestedPlotCount = 0;
        $harvestedCrossCount = 0;
        $harvestedTotalCount = 0;
        $totalCount = 0;
        // harvest param
        $params = [
            'harvestStatus' => 'COMPLETED'
        ];

        // get seeds harvested status for Occurrence
        if($locationId == null){
            $path = "occurrences/$occurrenceId/harvest-summaries";

            $url = Url::toRoute([
                '/occurrence/view/plot', 
                'program' => Yii::$app->userprogram->get('abbrev'),
                'id' => $occurrenceId
            ]);
        }else{
            $path = "locations/$locationId/harvest-summaries";

            $url = Url::toRoute([
                '/location/view/plot', 
                'program' => Yii::$app->userprogram->get('abbrev'),
                'id' => $locationId
            ]);
        }

        $result = Yii::$app->api->getParsedResponse('GET', $path, filter: 'limit=1');
        if (!$result['success']) {
            Yii::error('Error in API call '. json_encode(['path'=>$path, 'locationDbId'=>$locationId,
                'occurrenceDbId'=>$occurrenceId]), __METHOD__);
            $totalCount = $harvestedTotalCount = 0;
        } else {
            if(isset($result['data'][0])){
                $harvestData = $result['data'][0];

                // Count all plots and crosses
                $plotCount = (int) $harvestData['totalPlotCount'];
                $crossCount = (int) $harvestData['totalCrossCount'];
                $totalCount = $plotCount + $crossCount;

                // Count all harvested seeds from plots and crosses
                $harvestedPlotCount = (int) $harvestData['totalSeedsFromPlots'];
                $harvestedCrossCount = (int) $harvestData['totalSeedsFromCrosses'];
                $harvestedTotalCount = $harvestedPlotCount + $harvestedCrossCount;
            }
        }

        // get status
        $occurrenceModel = \Yii::$container->get('app\models\Occurrence');
        $status = $occurrenceModel->getStatusPercentage($harvestedTotalCount, $totalCount);

        return [
            'label' => 'Seeds harvested',
            'allCount' => $totalCount,
            'doneCount' => $harvestedTotalCount,
            'status' => $status,
            'url' => $url,
            'urlTitle' => 'View plot data'
        ];

    }
}