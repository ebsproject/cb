<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace app\modules\occurrence\models;

/**
 * File export model for Experiment Manager used to handle naming convention
 */
class FileExport
{
    // File types
    const MAPPING_FILES = 'Mapping_Files';
    const PLANTING_INSTRUCTIONS_OCCURRENCE = 'Planting_Instructions';
    const PLANTING_INSTRUCTIONS_LOCATION = '{{location}}_Planting_Instructions';
    const TRAIT_DATA_COLLECTION_OCCURRENCE = 'Trait_Data_Collection';
    const TRAIT_DATA_COLLECTION_LOCATION = '{{location}}_Trait_Data_Collection';
    const TRAIT_DATA_COLLECTION_CROSS_LIST = '{{crossList}}_Trait_Data_Collection';
    const ENTRY_LIST = 'Entry_List';
    const PLOT_LIST = 'Plot_List';
    const PLOT_LIST_LOCATION = '{{location}}_Plot_List';
    const DESIGN_LAYOUT = 'Design_Layout';
    const FIELD_LAYOUT = 'Field_Layout';
    const CROSS_LIST = 'Cross_List';
    const VARIABLE_LIST = 'Variable_List';
    const PACKAGES = 'Seed_Packages';
    const PARENT_LIST = 'Parent_List';
    const DATA_COLLECTION_SUMMARIES = 'Data_Collection_Summaries';
    const METADATA_AND_PLOT_DATA = 'Metadata_and_Plot_Data';
    const OCCURRENCE_DATA_COLLECTION = 'Occurrence_Data_Collection';

    // File formats
    const CSV = 'csv';
    const JSON = 'json';
    const XLSX = 'xlsx';

    private string $fileType;
    private string $fileName;
    private string $timestamp;
    private array $experiments;
    private string $fileFormat;

    private string $location;
    private string $crossList;
    private bool $shouldUseIndicatedFileFormat;
    private string $suffix;

    public function __construct(string $fileType='', string $fileFormat='', int $time=null, string $location='', string $crossList='', bool $shouldUseIndicatedFileFormat = false, string $fileName='', string $suffix='') {
        if (!empty($fileType) && !in_array($fileType, $this->getFileTypes())) {
            throw new \TypeError('Unsupported file type: '.$fileType);
        }
        // Empty string file formats will return filenames with no extension
        if (!in_array($fileFormat, $this->getFileFormats()) && $fileFormat !== '') {
            throw new \TypeError('Unsupported file format: '.$fileFormat);
        }
        $this->fileType = $fileType;
        $this->fileName = $fileName; // main file name identifier
        $this->fileFormat = $fileFormat;
        $this->timestamp = self::formatTimestamp($time);
        $this->experiments = [];
        $this->location = $location;
        $this->crossList = $crossList;
        $this->shouldUseIndicatedFileFormat = $shouldUseIndicatedFileFormat;
        $this->suffix = $suffix; // text to be appended in file name
    }

    /**
     * Returns a string-formatted timestamp
     *
     * @param int time Unix timestamp
     * @return string timestamp in YYYYMMDD_HHmmss format
     */
    public static function formatTimestamp($time)
    {
        return date("Ymd_His", $time);
    }

    /**
     * Returns a string-formatted occurrence/location code
     *
     * @param array $codes string|int array of number-like values
     * @return array|string[] list of 3-character zero-padded numeral
     */
    public static function formatSerialCodes(array $codes) {
        return array_map(fn($n) => sprintf('%03d', (int)$n), $codes);
    }

    /**
     * Returns all valid file types for export
     *
     * @return string[] list of file types
     */
    public static function getFileTypes()
    {
        return [
            self::MAPPING_FILES,
            self::PLANTING_INSTRUCTIONS_OCCURRENCE,
            self::PLANTING_INSTRUCTIONS_LOCATION,
            self::TRAIT_DATA_COLLECTION_OCCURRENCE,
            self::TRAIT_DATA_COLLECTION_LOCATION,
            self::TRAIT_DATA_COLLECTION_CROSS_LIST,
            self::ENTRY_LIST,
            self::PLOT_LIST,
            self::PLOT_LIST_LOCATION,
            self::DESIGN_LAYOUT,
            self::FIELD_LAYOUT,
            self::CROSS_LIST,
            self::VARIABLE_LIST,
            self::PACKAGES,
            self::PARENT_LIST,
            self::DATA_COLLECTION_SUMMARIES,
            self::METADATA_AND_PLOT_DATA,
            self::OCCURRENCE_DATA_COLLECTION,
        ];
    }

    /**
     * Returns all valid file formats for export
     *
     * @return string[] list of file formats
     */
    public static function getFileFormats()
    {
        return [
            self::CSV,
            self::JSON,
            self::XLSX,
        ];
    }

    /**
     * Appends experiment code and corresponding (single or multiple) serial data to
     * experiments =
     * [
     *   0 => [ experimentCode_0 => [ filename_0 => data, filename_n => data ]],
     *   n => [ experimentCode_n => [ filename_0 => data, filename_n => data ]],
     * ]
     *
     * Serial codes are transformed into filenames.
     * If experiment code exists, corresponding data is appended to the existing.
     *
     * @param string $experimentCode experiment code
     * @param integer[]|string[] $serialCodes occurrence/location's serial number segment
     * @param array $serialData data associated with a list of serial codes
     * @return int number of experiments added
     */
    public function addExperimentData(string $experimentCode, array $serialCodes, array $serialData)
    {
        if (sizeof($serialCodes) !== sizeof($serialData)) {
            throw new \ValueError(
                'Serial codes and data array size mismatch: '.sizeof($serialCodes).' != '.sizeof($serialData)
            );
        }
        $oldSize = sizeof($this->experiments);

        $newData = [ $experimentCode => array_combine(
            $this->getFilenames($experimentCode, self::formatSerialCodes($serialCodes)),
            $serialData
        )];

        if (!in_array($experimentCode, $this->getExperimentCodes())) {
            $this->experiments[] = $newData;
        } else {
            foreach ($this->experiments as $index => $experiment) {
                if (array_key_exists($experimentCode, $experiment)) {
                    $this->experiments[$index] = array_merge_recursive($this->experiments[$index], $newData);
                }
            }
        }

        $newSize = sizeof($this->experiments);
        return $newSize - $oldSize;
    }

    /**
     * Returns all experiment codes of the added experiments
     *
     * @return string[] list of experiment codes
     */
    public function getExperimentCodes() {
        return array_map('array_key_first', $this->experiments);
    }

    /**
     * Returns a list of formatted file names based on experiment and serial codes
     *
     * @param string $experimentCode internal code assigned to an experiment
     * @param string[] occurrence/location's serial number segment
     * @return string[] list of file names
     */
    public function getFilenames(string $experimentCode, array $serialCodes)
    {
        $extension = $this->fileFormat === '' ? '' : ".{$this->fileFormat}";

        $fileType = in_array($this->fileType, [self::PLANTING_INSTRUCTIONS_LOCATION, self::TRAIT_DATA_COLLECTION_LOCATION])
            ? str_replace('{{location}}', $this->location, $this->fileType)
            : $this->fileType;

        // if file type is empty, use suffix
        if(empty($fileType)){
            $fileType = !empty($this->suffix) ? $this->suffix : '';
        }

        return array_map(
            fn($serialCode) =>
                preg_replace('/[^\w\-\ ]/','',$experimentCode).'_'.
                $serialCode.'_'.
                $fileType.'_'.
                $this->timestamp.
                $extension,
            $serialCodes
        );
    }

    /**
     * Returns all individual occurrence/location filenames and their respective data for export
     *
     * @return array list of occurrence/location filenames with file format extension as KEY and content as VALUE
     */
    public function getUnzipped()
    {
        $experiments = array_map(fn($experiment) => end($experiment), $this->experiments);

        // Special handling for JSON Mapping Files where all data are in one file needs only the filename
        if ($this->fileType === self::MAPPING_FILES && $this->fileFormat === self::JSON) {
            $experiments = array_map(
                fn($code) => ["{$code}_{$this->fileType}_{$this->timestamp}.{$this->fileFormat}"=>[]],
                $this->getExperimentCodes()
            );
        }

        $allFiles = [];
        foreach ($experiments as $experimentCode => $files) {
            $allFiles = array_merge($allFiles, $files);
        }
        return $allFiles;
    }

    /**
     * Returns a filename for exporting multiple experiments
     *
     * @return string a filename with .json extension if file format is JSON, .zip otherwise
     */
    public function getZippedFilename()
    {
        $extension = $this->getFileExtension();

        // Special handling for Trait Data Collection (location level)
        if ($this->fileType === self::TRAIT_DATA_COLLECTION_LOCATION) {
            $prefix = str_replace('{{location}}', $this->location, $this->fileType);
            return $prefix.'_'.$this->timestamp . '.' . $extension;
        }

        // Special handling for Trait Data Collection (cross list level)
        if ($this->fileType === self::TRAIT_DATA_COLLECTION_CROSS_LIST) {
            $prefix = str_replace('{{crossList}}', $this->crossList, $this->fileType);
            return $prefix.'_'.$this->timestamp . '.' . self::CSV;
        }

        // Special handling for Planting Instruction and Plot List (location level)
        if ($this->fileType === self::PLANTING_INSTRUCTIONS_LOCATION || $this->fileType === self::PLOT_LIST_LOCATION) {
            $prefix = str_replace('{{location}}', $this->location, $this->fileType);
            return $prefix.'_'.$this->timestamp.'.'.$extension;
        }

        return 'Multi-Experiments'.'_'.
            $this->fileType.'_'.
            strtoupper($this->fileFormat).'_'.
            $this->timestamp.'.'.
            $extension;
    }


    /**
     * Returns cleaned file name
     * 
     * @return string cleaned file name
     */
    public function getFileName()
    {

        $prefix = preg_replace('/[^\w\-\ ]/','', $this->fileName . '_' . $this->suffix);
        return $prefix. '_' . $this->timestamp.'.'.$this->fileFormat;
    }

    /**
     * Returns a file extension for exporting multiple experiments...
     *
     * @return string a file extension which could be either .csv, .json, .xlsx, or .zip
     */
    private function getFileExtension ()
    {
        if ($this->fileFormat === self::JSON) {
            return self::JSON;
        }

        return ($this->shouldUseIndicatedFileFormat) ? $this->fileFormat : 'zip';
    }
}