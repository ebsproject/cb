<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\occurrence\models;

use app\interfaces\models\IBackgroundJob;
use app\models\BaseModel;
use app\interfaces\models\ILocation;
use app\interfaces\models\ILocationOccurrenceGroup;
use app\interfaces\models\IOccurrence;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "api.background_job" for Experiment Manager application.
 */
class OccurrenceBackgroundJob extends BaseModel {
    public function __construct (
        public IBackgroundJob $backgroundJob,
        public ILocation $location,
        public ILocationOccurrenceGroup $locationOccurrenceGroup,
        public IOccurrence $occurrence,
        $config = []
    )
    { parent::__construct($config); }

    /**
     * Format the background process records into dropdown selections
     *
     * @param Array $backgroundProcesses List of background process records
     * @return String $transactions Formatted HTML elements for transactions
     */
    public function getBackgroundJobNotifications($backgroundProcesses){
        $transactions = [];

        foreach ($backgroundProcesses as $key => $record) {
            $status = strtolower($record['jobStatus']);
            $message = ucfirst($record['message']);

            if ($status === 'done') {
                $iconClass = 'green';
                $icon = 'check';
            } elseif (in_array($status, ['in_progress', 'in_queue'])) {
                $iconClass = 'grey';
                $icon = 'hourglass_empty';
            } elseif ($status === 'failed') {
                $iconClass = 'red';
                $icon = 'priority_high';
            } else {
                $iconClass = 'grey';
                $icon  = 'near_me';
            }

            $entityId = $record['entityDbId'];
            $endpointEntity = strtoupper($record['endpointEntity']);
            $application = strtoupper($record['application']);
            
            // if the transaction is saving list
            $isList = ($endpointEntity == 'LIST_MEMBER') ? true : false;

            $attributes = '';

            // if the transaction is from Harvest manager
            if($application == 'HARVEST_MANAGER'){
                $message = 'HM: ' . $record['message'];
                // get program
                $program = Yii::$app->userprogram->get('abbrev');
                
                // set URL depending on the status
                if($status == 'done'){
                    $baseUrl = '/harvestManager/management/index';
                }else if($status == 'failed'){
                    $baseUrl = '/harvestManager/harvest-data/plots';
                }else{
                    $baseUrl = '/harvestManager/creation/index';
                }

                $url =  Url::to([
                    $baseUrl,
                    'program' => $program,
                    'occurrenceId' => $entityId,
                    'source' => 'plots'
                ]);
                $attributes = 'data-url="'.$url.'" title="Go to the transaction"';
            }

            $timeDiff = $this->backgroundJob->ago($record['modificationTimestamp']);

            // if endoint entity is LIST_MEMBER
            if($isList){
                $modelClass = 'Lists';
                $fieldParam = 'list.display_name as displayName | list.id as listDbId | list.abbrev as abbrev';
                $filterFieldParam = 'listDbId';
                $displayField = 'displayName';
            }else{
                $modelClass = str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($record['endpointEntity']))));
                $fieldParam = '';
                $displayField = '';
                $filterFieldParam = '';
                if ($record['endpointEntity'] == 'EXPERIMENT') {
                    $fieldParam = 'experiment.experiment_name as experiment | experiment.id as experimentDbId';
                    $filterFieldParam = 'experimentDbId';
                    $displayField = 'experiment';
                } else if ($record['endpointEntity'] === 'LOCATION') {
                    $fieldParam = 'location.location_name as locationName | location.id as locationDbId';
                    $filterFieldParam = 'locationDbId';
                    $displayField = 'locationName';
                } else {
                    $fieldParam = 'occurrence.occurrence_name as occurrenceName | occurrence.id as occurrenceDbId';
                    $filterFieldParam = 'occurrenceDbId';
                    $displayField = 'occurrenceName';
                }
            }
            
            $model = '\\app\\models\\'.$modelClass;
            // Call container->get() to dynamically construct a model instance
            $modelInstance = \Yii::$container->get($model);
            $params = [
                'fields' => $fieldParam,
                $filterFieldParam => "equals $entityId"
            ];

            $description = '';

            if (str_contains($record['description'], 'Field Book'))
                $description = 'field book';
            if (str_contains($record['description'], 'Mapping File'))
                $description = 'mapping file';
            if ($record['workerName'] == 'CreateOccurrenceRecords')
                $description = $record['description'];

            $result = $modelInstance->searchAll($params, 'limit=1&isBasic=true', false);
            $entityName = isset($result['data'][0][$displayField]) ? $result['data'][0][$displayField] : '';
            $abbrev = isset($result['data'][0]['abbrev']) ? $result['data'][0]['abbrev'] : '';
            $entity = strtolower($record['entity'] ?? '');
            $workerName = strtolower($record['workerName'] ?? '');
            $jobRemarks = $record['jobRemarks'];
            $item = "<li class='bg_notif-btn' data-transaction_id='$entityId' data-abbrev='$abbrev' data-worker-name='$workerName' data-entity='$entity' data-description='$description' data-filename='$jobRemarks' $attributes>
                <a class='grey-text text-darken-2'>
                    <span class='material-icons icon-bg-circle small $iconClass'>$icon</span>
                    $message
                </a>";

            $timeElement = '<time class="media-meta">';
            $item .= $timeElement.' <b>'.$entityName.'</b></time>'.$timeElement.$timeDiff.'</time></li>';
            $transactions[] = $item;
        }

        return implode(' ', $transactions);

    }

    /**
     * Update Occurrence status based from bacgroud process status
     *
     * @param integer $userId user identifier
     */
    public function updateOccurrenceStatus($userId){
        $params = [
            'creatorDbId' => 'equals '.$userId,
            'application' => 'equals OCCURRENCES',
            'jobStatus' => 'equals DONE',
            'jobRemarks' => 'null'
        ];

        $result = $this->backgroundJob->searchAll($params);

        if($result['status'] == 200 && $result['totalCount'] > 0){
            foreach ($result['data'] as $key => $record) {
                $entityDbId = $record['entityDbId'];
                $description = strtolower($record['description']);
                // if background process is already done and the status is not yet updated and
                // if upload mapped plots or generate location, change the background job status to reflected
                if(
                    ($record['jobStatus'] == 'DONE' && $record['jobRemarks'] != 'REFLECTED') &&
                    ($description == 'generate location' || $description == 'upload planting arrays')){
                    if($description == 'generate location' || $description == 'upload planting arrays'){

                        // update background job transaction
                        $this->backgroundJob->updateOne($record['backgroundJobDbId'], ['jobRemarks' => 'REFLECTED']);
                    }
                }
            }
        }
    }

    /**
     * Push notifications from background process
     *
     * @param integer $userId user identifier
     * @return array list of notifications
     */
    public function pushNotifications($userId){

        // get new notifications
        $params = [
            'creatorDbId' => 'equals ' . $userId,
            'jobStatus' => 'equals DONE|equals IN_QUEUE|equals IN_PROGRESS|equals FAILED',
            'application' => '[OR] OCCURRENCES',
            'workerName' => '[OR] GenerateHarvestRecords',
            'isSeen' => 'equals false'
        ];

        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);

        $newNotifications = isset($result['data']) ? $result['data'] : [];
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->getBackgroundJobNotifications($newNotifications);
        
        // get old notifications
        $oldNotifParams = [
            'creatorDbId' => 'equals ' . $userId,
            'jobStatus' => 'equals DONE|equals IN_QUEUE|equals IN_PROGRESS|equals FAILED',
            'application' => '[OR] OCCURRENCES',
            'workerName' => '[OR] GenerateHarvestRecords',
            'isSeen' => 'equals true'
        ];
        $oldNotifResult = $this->backgroundJob->searchAll($oldNotifParams, $filter, false);
        $oldNotifications = isset($oldNotifResult['data']) ? $oldNotifResult['data'] : [];
        $seenNotifications = $this->getBackgroundJobNotifications($oldNotifications);

        // update unseen notifications to seen
        $updateParams = [
            'jobIsSeen' => 'true'
        ];

        $this->backgroundJob->update($newNotifications, $updateParams);

        return [
            'unseenNotifications' => $unseenNotifications,
            'seenNotifications' => $seenNotifications,
            'unseenCount' => $unseenCount
        ];
        
    }

}