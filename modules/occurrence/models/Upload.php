<?php
/* 
* This file is part of EBS-Core Breeding. 
* 
* EBS-Core Breeding is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* 
* EBS-Core Breeding is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
* GNU General Public License for more details. 
* 
* You should have received a copy of the GNU General Public License 
* along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

namespace app\modules\occurrence\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\models\Api;
use app\models\Config;
use app\models\DataBrowserConfiguration;
use app\models\Germplasm;
use app\models\LocationOccurrenceGroup;
use app\models\Occurrence;
use app\models\Plot;
use app\models\Person;
use app\models\Program;
use app\models\User;
use app\modules\download\models\DownloadModel;
use yii\web\UploadedFile;

/**
 * Upload model for occurrences
 */
class Upload
{
    public function __construct (
        public Config $configModel,
        public Api $api,
        public DataBrowserConfiguration $dataBrowserConfiguration,
        public DownloadModel $downloadModel,
        public Germplasm $germplasm,
        public LocationOccurrenceGroup $locationOccurrenceGroup,
        public Occurrence $occurrence,
        public Person $person,
        public Plot $plot,
        public Program $program,
        public UploadedFile $uploadedFile
    ) {}

    /**
     * Validate file to be uploaded for mapped plots
     *
     * @param string $fileName
     */
    public function validatePlantingArrays($fileName)
    {
        $error['success'] = false;

        try {
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads/';
            $file = $this->uploadedFile->getInstanceByName($fileName);

            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false,
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $tmpFileName = \Yii::$app->security->generateRandomString() . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $tmpFileName;

            $file->saveAs($tmpFileName);

            $csv = new \app\components\CsvReader($tmpFileName);
            $csv->setHasHeader(true);

            // valid required headers
            $requiredHeaders = [
                'occurrence_id',
                'plot_id',
                'pa_x',
                'pa_y',
                'field_x',
                'field_y'
            ];

            $count = 2;
            $filePlotIds = [];
            $plotIdField = 'plot_id';
            $occField = 'occurrence_id';
            $paArr = [];
            $paTemp = [];
            $fieldArr = [];
            $fieldTemp = [];
            $occurrenceDbIdArr = [];
            $fillerRecords = [];
            $borderRecords = [];
            $triggerBgProcess = false; // whether to trigger backgroung process or not
            $paCombStrCond= '';
            $fieldCombStrCond= '';
            $integerVars = [
                'pa_x',
                'pa_y',
                'field_x',
                'field_y'
            ];

            // get current program
            $program = Yii::$app->userprogram->get('abbrev');
            // get role and check if admin
            [
                $role,
                $isAdmin,
            ] = $this->person->getPersonRoleAndType($program);


            // check whether map to existing location
            $isExLoc = $this->getIsUploadToExistingLocSelection();

            Yii::info('Getting selection to map to existing location ' . json_encode(['isExLoc'=>$isExLoc]), __METHOD__);

            // get user ID
            $userModel = new User();
            $userId = $userModel->getUserId();

            // loop through content
            foreach ($csv as $line) {

                $isHeaderLine = ($count - 1 == 1) ? true : false;
                $paStrCond = '';
                $fieldStrCond = '';
                // validate required data
                foreach ($requiredHeaders as $key => $value) {

                    // check if occurrence_id exists, else use databasetrialid
                    if (strtolower($value) == 'occurrence_id' && !isset($line[strtolower($value)])){
                        $value = $occField = 'databasetrialid';
                    }

                    // if required column is missing
                    if(!isset($line[strtolower($value)]) && $isHeaderLine){
                        $error['error'] = 'Missing required column header ' . $value . '.';
                        return $error;
                    }

                    // if value is empty
                    if (
                        isset($line[strtolower($value)]) && empty($line[strtolower($value)])
                    ) {
                        $error['error'] = 'Value for ' . $value . ' at line ' . $count . ' could not be zero or empty.';
                        return $error;
                    }

                    // if value is not numeric and not float
                    if (
                        isset($line[strtolower($value)]) &&
                        in_array($value, $integerVars) &&
                        !is_numeric($line[strtolower($value)]) ||
                        strpos($line[strtolower($value)], '.')
                    ) {
                        $error['error'] = 'Invalid value for ' . $value . ' at line ' . $count . '.';
                        return $error;
                    }

                    // store pax and pay values in temp string for checking of duplicates
                    if (strtolower($value) == 'pa_x') {
                        $lineTemp['pa_x'] = $line[strtolower($value)];
                        $paStrCond = '"' . $line[strtolower($value)] . ',';
                    }

                    if (strtolower($value) == 'pa_y') {
                        $lineTemp['pa_y'] = $line[strtolower($value)];
                        $paStrCond .= $line[strtolower($value)] . '"';
                    }

                    // store fieldx and fieldy values in temp string for checking of duplicates
                    if (strtolower($value) == 'field_x') {
                        $lineTemp['field_x'] = $line[strtolower($value)];
                        $fieldStrCond = '"' . $line[strtolower($value)] . ',';
                    }

                    if (strtolower($value) == 'field_y') {
                        $lineTemp['field_y'] = $line[strtolower($value)];
                        $fieldStrCond .= $line[strtolower($value)] . '"';
                    }

                    // store plot id in needed info
                    if(strtolower($value) == 'plot_id'){
                        $lineTemp['plot_id'] = $line[strtolower($value)];
                    }

                }

                // check if the PA combination does not exist yet
                if(strpos($paCombStrCond, $paStrCond) !== false){
                    $error['error'] = 'Duplicate PA X and PA Y values at line ' . $count . '.';
                    return $error;
                }

                // check if the FIELD combination does not exist yet
                if(strpos($fieldCombStrCond, $fieldStrCond) !== false){
                    $error['error'] = 'Duplicate FIELD X and FIELD Y values at line ' . $count . '.';
                    return $error;
                }
              
                $paCombStrCond .= $paStrCond;
                $fieldCombStrCond .= $fieldStrCond;

                // store occurrence IDs in an array
                if (
                    isset($line[strtolower($occField)])
                    && !in_array($line[strtolower($occField)], $occurrenceDbIdArr)
                    && !in_array($line[strtolower($occField)], ['BORDER', 'FILLER'])
                ) {
                    $occurrenceDbId = $line[strtolower($occField)];
                    $occurrenceDbIdArr[] = $occurrenceDbId;
                }

                if (
                    isset($line[strtolower($plotIdField)]) &&
                    strtoupper(trim($line[strtolower($plotIdField)])) == 'BORDER'

                ) {
                    $borderRecords[] = $lineTemp;
                } else  if (
                    isset($line[strtolower($plotIdField)]) &&

                    strtoupper(trim($line[strtolower($plotIdField)])) == 'FILLER'

                ) {
                    $fillerRecords[] = $lineTemp;
                } else {
                    if (isset($line[strtolower($plotIdField)])) {
                        $plotId = intval($line[strtolower($plotIdField)]);
                        $filePlotIds[] = $line[$plotIdField] = $plotId;
                    }
                    $uploadedData[] = $lineTemp;
                }

                // check if "DO_NOT_PLANT", disregard the row
                if (isset($line[strtolower($plotIdField)]) && strtoupper(trim($line[strtolower($plotIdField)])) == 'DO_NOT_PLANT') {
                    continue;
                }

                $count++;
            }

            // get invalid values
            if (!empty($csv->getInvalids())) {
                $errorMessage = [];
                $errs = $csv->getInvalids();
                foreach ($errs as $err) {
                    $errorMessage[] = $err . '<br>';
                }

                $error['error'] = $errorMessage;
                return $error;
            }

            // if there are invalid fields
            if (empty($uploadedData)) {
                $error['error'] = 'There are invalid values. Ensure that values are specified.';
                return $error;
            }

            $plotsArr = [];
            $programId = null;
            $isMapped = false;

            // validate occurrences
            foreach ($occurrenceDbIdArr as $occurrenceId) {
                $searchParams = [
                    'occurrenceDbId' => "equals $occurrenceId",
                    'fields' => 'occurrence.id as occurrenceDbId|program.id AS programDbId|occurrence.occurrence_status AS occurrenceStatus'.'
                        |occurrence.occurrence_name AS occurrenceName|location.location_code AS locationCode|creator.id AS creatorDbId'
                ];

                $occurrenceResult = $this->occurrence->searchAll($searchParams,'limit=1',false);

                // if Occurrence information is not successfully retrieved
                if(empty($occurrenceResult['success'])){
                    $error['error'] = 'There was a problem validating the file.';
                    return $error;
                }
            
                // if occurrence does not exist
                if (!$occurrenceResult['totalCount']) {
                    $error['error'] = 'Occurrence with ID ' . $occurrenceId . ' does not exist.';
                    return $error;
                }

                // check if user has write access or the creator of the occurrence
                $creatorId = isset($occurrenceResult['data'][0]['creatorDbId']) ? $occurrenceResult['data'][0]['creatorDbId'] : null;

                // check if user has write permission to the occurrence
                $writeSearchParams = [
                    'occurrenceDbId' => "equals $occurrenceId",
                    'fields' => 'occurrence.id as occurrenceDbId'
                ];

                $writeOccurrenceResult = $this->occurrence->searchAll($writeSearchParams,'permission=write&limit=1',false);

                // if user has no write access and not the creator of the record
                if(!$writeOccurrenceResult['totalCount']){
                    // if user is not the creator of the record
                    if($creatorId != $userId){
                        $error['error'] = 'You do not have permission to upload mapped plots to occurrence with ID ' . $occurrenceId . '.';
                        return $error;
                    }
                }

                // check if there is an ongoing background processing
                $occurrenceStatus = isset($occurrenceResult['data'][0]['occurrenceStatus']) ? 
                    $occurrenceResult['data'][0]['occurrenceStatus']: '';

                // background processing statuses
                $bgProcessingStatuses = [
                    'upload mapped plots in progress',
                    'generate location in progress'
                ];

                // check if occurrence status has background processing ongoing
                if (in_array($occurrenceStatus, $bgProcessingStatuses)) {
                    $error['error'] = 'There is currently background processing ongoing for the Occurrence.';
                    return $error;
                }

                // check if occurrence is already mapped to a location and map to new location
                $occurrenceName = isset($occurrenceResult['data'][0]['occurrenceName']) ? 
                $occurrenceResult['data'][0]['occurrenceName']: '';

                $locationCode = isset($occurrenceResult['data'][0]['locationCode']) ? 
                $occurrenceResult['data'][0]['locationCode']: '';

                // if status is mapped and not use existing location, implement validation
                if (strpos($occurrenceStatus, 'mapped') !== false && $isExLoc != true) {
                    $error['error'] = 'Occurrence ' . $occurrenceName . ' is already mapped to Location <strong>' . $locationCode. '</strong>. Please select map to "Existing location" if you wish to proceed.';
                    return $error;
                }

                // get program of occurrence
                if(empty($programId)){
                    $programId = $occurrenceResult['data'][0]['programDbId'] ?? null;
                    $programDetail = $this->program->getProgram($programId);
                    $cropProgramId = $programDetail["cropProgramDbId"] ?? null;
                }

                $searchParams = [
                    'occurrenceDbId' => "equals $occurrenceId",
                    'fields' => 'log.occurrence_id as occurrenceDbId|log.location_id as locationDbId'
                ];

                // check that the occurrence has not been mapped in a location
                $logResult = $this->locationOccurrenceGroup->searchAll($searchParams,'limit=1',false);

                // if occurrence is already mapped in a location
                if ($logResult['totalCount']) {
                    // check status of Occurrence
                    $status = $occurrenceResult['data'][0]['occurrenceStatus'] ?? 'planted';

                    // if the Occurrence is already planted, return validation error message
                    if($status == 'planted' && $isExLoc != true){
                        $error['error'] = 'Occurrence with ID ' . $occurrenceId . ' is already mapped in a location and is already planted. Please select map to "Existing location" if you wish to proceed.';
                        return $error;
                    } else {
                        $mappedLocId = $logResult['data'][0]['locationDbId'] ?? 0;

                        $result['isMapped'] = true;
                        $result['mappedLocId'] = $mappedLocId;

                    }
                }

                // if non-admin user and collaborator role, apply permission
                if (!$isAdmin && strtoupper($role) == 'COLLABORATOR'){
                    $occCheckParams = [
                        'occurrenceDbId' => "equals $occurrenceId",
                        'fields' => 'occurrence.id as occurrenceDbId'
                    ];

                    $occCheckResult = $this->occurrence->searchAll($occCheckParams,'ownershipType=shared&limit=1',false);

                    if (!$occCheckResult['totalCount']) {
                        $error['error'] = 'You do not have permission in Occurrence with ID ' . $occurrenceId . '.';
                        return $error;
                    }
                }
            }

            $fillerData = [];

            if(!empty($fillerRecords)){
                
                // get default germplasm ID for filler
                $fillerConfig = $this->configModel->getConfigByAbbrev("DEFAULT_FILLER_GERMPLASM");

                $fillerGermplasmDbId = (!empty($cropProgramId)) ? $fillerConfig['' . $cropProgramId]["germplasm_id"] : null;

                // if there are no fillers found in the database
                if(empty($fillerGermplasmDbId)){
                    $error['error'] = 'There are no fillers found in the system.';
                    return $error;
                }

                $fillerParams = [
                    "germplasmDbId" => "equals $fillerGermplasmDbId",
                    "fields" => "germplasm.id AS germplasmDbId | germplasm.designation AS designation"
                ];

                $germplasmResult = $this->germplasm->searchAll($fillerParams, 'limit=1',false);
                $fillerGermplasmDetail = isset($germplasmResult['data']) ? $germplasmResult['data'][0] : [];
                $fillerGermplasmId = $fillerGermplasmDetail["germplasmDbId"];
                $fillerDesignation = $fillerGermplasmDetail["designation"];

                foreach ($fillerRecords as $key => $value) {
                    $fillerData[] = array_merge($value, [
                        "germplasmDbId" => $fillerGermplasmId,
                        "designation" => $fillerDesignation,
                        "occurrenceName" => null,
                        "occurrenceDbId" => null
                    ]);
                }
            }

            $borderData = [];
            
            if(!empty($borderRecords)){
                // get default germplasm ID for border
                $borderConfig = $this->configModel->getConfigByAbbrev("DEFAULT_BORDER_GERMPLASM");
                
                $borderGermplasmDbId = !empty($cropProgramId) ? $borderConfig['' . $cropProgramId]["germplasm_id"] : null;

                // if there are no borders found in the database
                if(empty($borderGermplasmDbId)){
                    $error['error'] = 'There are no borders found in the system.';
                    return $error;
                }

                $borderParams = [
                    "germplasmDbId" => "equals $borderGermplasmDbId",
                    "fields" => "germplasm.id AS germplasmDbId | germplasm.designation AS designation"
                ];

                $germplasmResult = $this->germplasm->searchAll($borderParams, 'limit=1', false);

                $borderGermplasmDetail = isset($germplasmResult['data']) ? $germplasmResult['data'][0] : [];
                $borderGermplasmId = $borderGermplasmDetail["germplasmDbId"];
                $borderDesignation = $borderGermplasmDetail["designation"];

                foreach ($borderRecords as $key => $value) {
                    
                    $borderData[] = array_merge($value, [
                        "germplasmDbId" => $borderGermplasmId,
                        "designation" => $borderDesignation,
                        "occurrenceName" => null,
                        "occurrenceDbId" => null
                    ]);
                }
            }

            // save data in data browser configuration as temporary storage
            $this->saveUploadedData(['plots' => $uploadedData, "borders" => $borderData, "fillers" => $fillerData], 'upload planting arrays');
            $result['success'] = true;

            $result['id'] = (count($occurrenceDbIdArr) > 1) ? $occurrenceDbIdArr : $occurrenceId;

            return $result;
        } catch (\Exception $e) {
            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $error['error'] = 'Please check if file is not empty.';
                return $error;
            }

            $error['error'] = $e->getMessage();
            return $error;
        }
    }

    /**
     * Save uploaded data to data browser configuration
     * as a temporary storage of the upload transaction
     *
     * @param array $uploadedData array list of data to be saved
     */
    public function saveUploadedData($uploadedData, $name)
    {
        // get upload transaction if exists
        if ($name == 'upload planting arrays') {
            $configData = $this->getUserUploadPlantingArrayTransaction();
        } elseif ($name == 'upload occurrence data collection') {
            $configData = $this->getUserUploadOccurrenceDataCollectionTransaction();
        }
        $config = $configData['config'];
        $postParams = $configData['postParams'];

        $requestData['name'] = $name;
        $requestData['data'] = ['data' => $uploadedData];
        $requestData = array_merge($requestData, $postParams);

        // if no config yet
        if (isset($config['totalCount']) && $config['totalCount'] == 0) {
            $request = [
                'records' => [
                    $requestData
                ]
            ];
            $this->dataBrowserConfiguration->create($request);
        } else {
            // if there is an existing config, update record

            if (isset($config['data'][0]['configDbId'])) {
                $configDbId = $config['data'][0]['configDbId'];

                $this->dataBrowserConfiguration->updateOne($configDbId, $requestData);
            }
        }
    }

    /**
     * Validate file to be uploaded for occurrence data
     *
     * @param string $fileName
     */
    public function validateOccurrenceData($fileName)
    {
        $error['success'] = false;

        try {
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads/';
            $file = $this->uploadedFile->getInstanceByName($fileName);

            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false,
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $tmpFileName = \Yii::$app->security->generateRandomString() . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $tmpFileName;

            $file->saveAs($tmpFileName);

            $csv = new \app\components\CsvReader($tmpFileName);
            $csv->setHasHeader(true);

            /* BEGIN Get config */
            $program = Yii::$app->session->get('program');

            [
                $role,
                $isAdmin,
            ] = $this->person->getPersonRoleAndType($program);
            
            $configUploadVariables = array_merge(
                $this->downloadModel->getConfigVariables(
                    'occurrence',
                    $isAdmin,
                    $role,
                    $program
                ),
                $this->downloadModel->getConfigVariables(
                    'management_protocol',
                    $isAdmin,
                    $role,
                    $program
                )
            );
            /* END Get config */

            $count = 2;

            // valid required headers
            $requiredHeaders = [
                'Occurrence ID',
            ];
            $occurrenceDbIdArr = [];
            $uploadedData = [];

            foreach ($csv as $line) {
                $isHeaderLine = ($count - 1 == 1) ? true : false;
                $excludeAttributes = ['occurrence id', 'location description'];

                // validate required data
                foreach ($requiredHeaders as $value) {
                    // if required column is missing, prompt error
                    if (!isset($line[strtolower($value)]) && $isHeaderLine) {
                        $error['error'] = "Missing required column header $value.";
                        return $error;
                    }

                    // check if occurrence id exists, else prompt error
                    if (
                        strtolower($value) == 'occurrence id' &&
                        isset($line[strtolower($value)]) &&
                        empty($line[strtolower(($value))])
                    ) {
                        $error['error'] = "Occurrence ID value for <strong>$value/strong> at line $count must not be empty.";
                        return $error;
                    }

                    // Validate Occurrence ID value
                    if (strtolower($value) == 'occurrence id') {
                        $searchParams = [
                            'occurrenceDbId' => "equals {$line[strtolower($value)]}",
                            'fields' => 'occurrence.id as occurrenceDbId'
                        ];

                        $occurrenceResult = $this->occurrence->searchAll($searchParams,'limit=1',false);

                        // If Occurrence ID is not successfully retrieved
                        if (!($occurrenceResult['success'])){
                            $error['error'] = "There was a problem retrieving the <strong>occurrence ID</strong> at line $count.";
                            return $error;
                        }
                    
                        // If Occurrence ID does not exist
                        if (!$occurrenceResult['totalCount']) {
                            $error['error'] = "Occurrence ID <strong>{$line[strtolower($value)]}</strong> at line $count does not exist.";
                            return $error;
                        }
                    }
                }

                // Store Occurrence IDs once validated
                array_push($occurrenceDbIdArr, $line['occurrence id']);

                // The first invalid value detected will halt...
                //...the validation process and prompt the user with...
                //...the appropriate error message and suggestion
                foreach ($line as $columnHeader => $dataValue) {
                    // Ignore excluded attributes and empty dataValues
                    if (in_array($columnHeader, $excludeAttributes) || !$dataValue) {
                        continue;
                    }

                    // Using $columnHeader, get variable abbrev
                    foreach ($configUploadVariables as $variable) {
                        // Column header from the uploaded CSV file...
                        //...should follow the corresponding label from...
                        //...the current config used.
                        $labelToLowerCased = strtolower($variable['label']);

                        if (
                            // Regular checking if column header corresponds to one of the configured variable labels
                            $columnHeader == $labelToLowerCased ||
                            // Checking if column header corresponds to one of the configured variable labels for Ecosystem...
                            // ...(Occurrence Ecosystem or Experiment Ecosystem)
                            (str_contains($columnHeader, 'ecosystem') && str_contains($labelToLowerCased, $columnHeader))
                        ) {
                            $abbrev = $variable['abbrev'];
                            $label = $variable['label'];

                            // Retrieve variable information
                            $variableInfo = $this->api->getApiResults(
                                'POST',
                                'variables-search',
                                json_encode([
                                    'abbrev' => "equals $abbrev",
                                ]),
                                'limit=1'
                            );

                            // If variable is not successfully retrieved
                            if (!($variableInfo['success'])) {
                                $error['error'] = "There was a problem retrieving the variable information of <strong>$label</strong> at line $count.";
                                return $error;
                            }

                            // If variable does not exist
                            if (!$variableInfo['totalCount']) {
                                $error['error'] = "Variable <strong>$label</strong> at line $count does not exist.";
                                return $error;
                            }

                            $expectedDataType = $variableInfo['data'][0]['dataType'];

                            // Check if uploaded value conforms to the expected data type
                            switch ($expectedDataType) {
                                case 'float':
                                case 'integer':
                                    if (!is_numeric($dataValue)) {
                                        $error['error'] = "Wrong format for uploaded value at line $count under the <strong>$label</strong> column.<br>
                                            Uploaded value should be <strong>numeric</strong>.";
                                        return $error;
                                    }
                                    break;
                                case 'date':
                                    if (!preg_match('/^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/i', $dataValue)) {
                                        $error['error'] = "Wrong format for uploaded value at line $count under the <strong>$label</strong> column.<br>
                                            Uploaded value should be in <strong>YYYY-MM-DD</strong> format.";
                                        return $error;
                                    }
                                    break;
                                case 'boolean':
                                    if (
                                        !is_bool($dataValue) ||
                                        !in_array(strtolower($dataValue), ['true', 'false'])
                                    ) {
                                        $error['error'] = "Wrong format for uploaded value at line $count under the <strong>$label</strong> column.<br>
                                            Uploaded value should be either <strong>true</strong> or <strong>false</strong>.";
                                        return $error;
                                    }
                                    break;
                                default: break;
                            }

                            $variableDbId = $variableInfo['data'][0]['variableDbId'];

                            // Retrieve scale value information
                            $scaleValueInfo = $this->api->getApiResults(
                                'POST',
                                "variables/$variableDbId/scale-values-search",
                                retrieveAll: true
                            );

                            // If scale values are not successfully retrieved
                            if (!($scaleValueInfo['success'])) {
                                $error['error'] = "There was a problem retrieving the scale values of the <strong>$label</strong> column at line $count.";
                                return $error;
                            }

                            $scaleValueInfo = $scaleValueInfo['data'][0]['scaleValues'];

                            $scaleValues = [];
                            
                            // Extract scale values, if there are any
                            foreach ($scaleValueInfo as $scaleValue) {
                                $scaleValues []= $scaleValue['value'];
                            }

                            // If there are scale values, check if uploaded value...
                            //...matches one of them
                            if (count($scaleValues) && !in_array($dataValue, $scaleValues)) {
                                $scaleValues = implode(', ', $scaleValues);

                                $error['error'] = "The value \"$dataValue\" at line $count is not a valid value for the <strong>$label</strong> column.<br>
                                    It should be one of the following: <strong>{$scaleValues}</strong>.";
                                return $error;
                            }
                        } else { // Else, this entire column will be ignored
                            continue;
                        }
                    }
                }

                // Store uploaded data once validated
                array_push($uploadedData, $line);

                $count++;
            }

            // save data in data browser configuration as temporary storage
            $this->saveUploadedData([ 'occurrences' => $uploadedData ], 'upload occurrence data collection');
            $result['success'] = true;

            $result['id'] = (count($occurrenceDbIdArr) > 1) ? implode('|', $occurrenceDbIdArr) : $occurrenceDbIdArr[0];

            return $result;
        } catch (\Exception $e) {
            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $error['error'] = 'Please check if file is not empty.';
                return $error;
            }

            $error['error'] = $e->getMessage();
            return $error;
        }
    }

    /**
     * Save whether to map to existing location or not
     * 
     * @param Integer $isExLoc Selection whether to map to existing location or not
     *   
     */
    public function saveIsUploadToExistingLoc($isExLoc){
        // get user ID
        $userModel = new User();
        $userId = $userModel->getUserId();

        $searchParams = [
            'userDbId' => "equals $userId",
            'dataBrowserDbId' => 'equals is-upload-to-existing-location',
            'type' => 'equals upload'
        ];

        // check if configuration already exists
        $config = $this->dataBrowserConfiguration->searchAll($searchParams, 'limit=1', false);

        // if no config yet
        if (isset($config['totalCount']) && $config['totalCount'] == 0) {
            $request = [
                'records' => [
                    [
                        'name' => 'is upload to existing location selection',
                        'data' => ['isExLoc' => $isExLoc],
                        'userDbId' => $userId,
                        'dataBrowserDbId' => 'is-upload-to-existing-location',
                        'type' => 'upload'
                    ]
                ]
            ];
            $this->dataBrowserConfiguration->create($request);
        } else {
            // if there is an existing config, update record

            if (isset($config['data'][0]['configDbId'])) {
                $configDbId = $config['data'][0]['configDbId'];

                $requestData['data'] = ['isExLoc' => $isExLoc];
                $this->dataBrowserConfiguration->updateOne($configDbId, $requestData);
            }
        }
    }

    /**
     * Get is upload to existing location selection
     * 
     * @return Boolean Whether to map to existing location or not 
     */
    public function getIsUploadToExistingLocSelection(){
        // get user ID
        $userModel = new User();
        $userId = $userModel->getUserId();

        $searchParams = [
            'userDbId' => "equals $userId",
            'dataBrowserDbId' => 'equals is-upload-to-existing-location',
            'type' => 'equals upload'
        ];

        // check if configuration already exists
        $config = $this->dataBrowserConfiguration->searchAll($searchParams, 'limit=1', false);

        // if no config yet
        if (isset($config['totalCount']) && $config['totalCount'] == 0) {
            return false;
        } else {
            // if there is an existing config, retrieve data
            if (isset($config['data'][0]['data']['isExLoc'])) {
                return $config['data'][0]['data']['isExLoc'];
            }
            return false;
        }
    }

    /**
     * Get preview upload mapped plots dataprovider
     *
     * @param string $action text feature rendering the preview
     * @param int|string $occurrenceId integer occurrence identifier
     * @param string $plotType string Plot type
     * @param boolean $useDesignLayout whether to use design layout or not
     * @return array $dataProvider array list of mapped plots
     */
    public function previewPlantingArrays (string $action = null, string $occurrenceId = null, string $plotType, string $useDesignLayout = null)
    {
        $plotCount = 0;
        // if generate location
        if ($action == 'generateLocation') {

            $layoutFields = '';

            if($useDesignLayout === 'true'){
                $layoutFields = ' | plot.design_x as pa_x | 
                    plot.design_y as pa_y | 
                    plot.design_x as field_x | 
                    plot.design_y as field_y';
            }

            $params = [
                'occurrenceDbId' => "equals $occurrenceId",
                'fields' => '
                    occurrence.id AS occurrenceDbId |
                    occurrence.occurrence_name AS occurrenceName |
                    plot.id AS id | 
                    plot.plot_number as plotno |
                    entry.entry_name as designation 
                    '. $layoutFields
            ];

            $plotsResult = Yii::$app->api->getParsedResponse(
                'POST',
                'plots-search',
                json_encode($params),
                'sort=occurrenceName|plotno',
                false
            );

            $dataArr = isset($plotsResult['data']) ? $plotsResult['data'] : [];
            $key = 'id';
            $plotCount = isset($plotsResult['totalCount']) ? $plotsResult['totalCount'] : 0;
        } else {
            // get upload transaction
            $config = $this->getUserUploadPlantingArrayTransaction();
            $data = $config['config'];
            $dataArr = [];
            $key = 'occurrenceDbId';

            if (isset($data['data'][0]['data']['data'])) {
                $dataArr = $data['data'][0]['data']['data'][$plotType];
                $plotCount = count($dataArr);

                // get only 100 records
                $dataArr = array_slice($dataArr, 0, 100);

                // if plots, add other needed info in preview
                if($plotType == 'plots'){
                    $plotIdsArr = array_column($dataArr, 'plot_id');

                    $plotIdsParam['plotDbId'] = 'equals ' . implode(' | equals ', $plotIdsArr);
                    $plotIdsParam['fields'] = 'plot.id AS plotDbId | ' .
                        'plot.plot_number AS plotno | '.
                        'germplasm.designation AS designation |' .
                        'plot.occurrence_id AS occurrenceDbId |' .
                        'occurrence.occurrence_name AS occurrenceName';

                    $plots = $this->plot->searchAll($plotIdsParam, 'sort=occurrenceDbId|plotno', false);
                    $plotsArr = $plots['data'] ?? [];

                    $combinedData = [];
                    $count = 0;
                    foreach ($plotsArr as $value) {
                        $combinedData[] = array_merge($value, $dataArr[$count]);
                        $count++;
                    }

                    $dataArr = $combinedData;
                }
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArr,
            'key' => $key,
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    $key, 'plotno', 'occurrenceDbId'
                ],
                'defaultOrder' => [
                    'occurrenceDbId' => SORT_ASC,
                    'plotno' => SORT_ASC
                ]
            ]
        ]);

        // if not borders and fillers, include total plot count
        if(!in_array($plotType,['borders','fillers'])){
            return [
                'dataProvider' => $dataProvider,
                'plotCount' => $plotCount
            ];
        } else {
            return $dataProvider;
        }
        
    }

    /**
     * Get preview location info dataprovider
     *
     * @return $dataProvider array location info
     */
    public function previewLocationInfo($locationInfo)
    {
        return new ArrayDataProvider([
            'allModels' => $locationInfo,
            'key' => 'locationName',
            'pagination' => false
        ]);
    }

    /**
     * Get user upload mapped plots transaction
     *
     * @return array $config mapped plots transaction
     */
    public function getUserUploadPlantingArrayTransaction()
    {
        // get user ID
        $userModel = new User();
        $userId = $userModel->getUserId();

        $searchParams = [
            'userDbId' => "equals $userId",
            'dataBrowserDbId' => 'equals planting-arrays',
            'type' => 'equals upload'
        ];

        $postParams = [
            'userDbId' => "$userId",
            'dataBrowserDbId' => 'planting-arrays',
            'type' => 'upload'
        ];

        // check if configuration already exists
        $config = $this->dataBrowserConfiguration->searchAll($searchParams, 'limit=1&sort=userDbId:asc', false);

        return [
            'searchParams' => $searchParams,
            'postParams' => $postParams,
            'config' => $config
        ];
    }

    /**
     * Get user upload occurrence data collection transaction
     *
     * @return array $config occurrence data collection transaction
     */
    public function getUserUploadOccurrenceDataCollectionTransaction()
    {
        // get user ID
        $userModel = new User();
        $userId = $userModel->getUserId();

        $searchParams = [
            'userDbId' => "equals $userId",
            'dataBrowserDbId' => 'equals occurrence-data-collection',
            'type' => 'equals upload'
        ];

        $postParams = [
            'userDbId' => "$userId",
            'dataBrowserDbId' => 'occurrence-data-collection',
            'type' => 'upload'
        ];

        // check if configuration already exists
        $config = $this->dataBrowserConfiguration->searchAll($searchParams, 'limit=1&sort=userDbId:asc', false);

        return [
            'searchParams' => $searchParams,
            'postParams' => $postParams,
            'config' => $config
        ];
    }

    /**
     * Get preview upload mapped plots dataprovider
     *
     * @return array $dataProvider array list of mapped plots
     */
    public function previewOccurrenceData ()
    {
        $occurrenceCount = 0;

        // get upload transaction
        $config = $this->getUserUploadOccurrenceDataCollectionTransaction();
        $data = $config['config'];
        $configDbId = '';
        $dataArr = [];
        $key = 'occurrenceDbId';

        if (isset($data['data'][0]['data']['data'])) {
            $dataArr = $data['data'][0]['data']['data']['occurrences'];
            $configDbId =$data['data'][0]['configDbId'];

            // Get occurrence count
            $occurrenceCount = count($dataArr);

            // get only 100 records
            $dataArr = array_slice($dataArr, 0, 100);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArr,
            'key' => 'occurrence id',
            'pagination' => false,
        ]);

        return [
            'dataProvider' => $dataProvider,
            'occurrenceCount' => $occurrenceCount,
            'configDbId' => $configDbId,
        ];
    }

    /**
     * Validate the file if does not contain duplicate
     * combination of paX-paY and fieldX-fieldY
     *
     * @param Integer $id Location identifier
     */
    public function validateMapToExistingLoc($id)
    {
        // get upload transaction if exists
        $configData = $this->getUserUploadPlantingArrayTransaction();

        $config = $configData['config'];
        $dataArr = [];
        $return['success'] = true;
        $errorMessage = 'combination already exists in the Location for plot ';

        // get experimental plots
        if (isset($config['data'][0]['data']['data']['plots'])) {
            $dataArr = $config['data'][0]['data']['data']['plots'];
        }

        // get borders
        if (isset($config['data'][0]['data']['data']['borders'])) {
            $dataArr = array_merge($dataArr, $config['data'][0]['data']['data']['borders']);
        }

        // get fillers
        if (isset($config['data'][0]['data']['data']['fillers'])) {
            $dataArr = array_merge($dataArr, $config['data'][0]['data']['data']['fillers']);
        }

        foreach ($dataArr as $key => $value) {

            $searchPaParams = [
                'paX' => 'equals ' . ($value['pa_x'] ?? '0'),
                'paY' => 'equals ' . ($value['pa_y'] ?? '0'),
            ];

            $path = 'locations/' . $id . '/plots-search';
            $result = Yii::$app->api->getParsedResponse(
                'POST',
                $path,
                json_encode($searchPaParams),
                'limit=1',
                false
            );

            // if there is an existing PA combinations
            if ($result['totalCount'] > 0) {
                $plotId = isset($value['plotDbId']) ? $value['plotDbId'] : $value['plot_id'];
                $return['success'] = false;
                $return['message'] = 'PA X-PA Y ' . $errorMessage . $plotId;

                return $return;
            }

            $searchFieldParams = [
                'fieldX' => 'equals ' . ($value['field_x'] ?? '0'),
                'fieldY' => 'equals ' . ($value['field_y'] ?? '0'),
            ];

            $path = 'locations/' . $id . '/plots-search';
            $result = Yii::$app->api->getParsedResponse(
                'POST',
                $path,
                json_encode($searchFieldParams),
                'limit=1',
                false
            );

            // if there is an existing PA combinations
            if ($result['totalCount'] > 0) {
                $plotId = isset($value['plotDbId']) ? $value['plotDbId'] : $value['plot_id'];
                $return['success'] = false;
                $return['message'] = 'FIELD X-FIELD Y ' . $errorMessage . $plotId;

                return $return;
            }
        }

        return $return;
    }
}
