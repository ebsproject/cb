<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\occurrence\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\models\BaseModel;
use app\models\UserDashboardConfig;

/**
 * Model for searching entries of an occurrence
 */
class Entry extends BaseModel 
{

    public $entryDbId;
    public $entryCode;
    public $entryNumber;
    public $entryName;
    public $entryType;
    public $entryRole;
    public $entryClass;
    public $description;
    public $germplasmName;
    public $germplasmState;
    public $germplasmType;
    public $seedName;
    public $packageLabel;
    public $packageUnit;
    public $packageQuantity;
    public $generation;
    public $occurrenceName;
    public $parentage;
    public $germplasmCode;

    /**
     * Set dependencies
     * This overrides construct
     */
    public function __construct (protected UserDashboardConfig $userDashboardConfig, $config = [])
    { parent::__construct($config); }

    /**
    * {@inheritdoc}
    */
    public function rules(){
        return [
            [[
                'entryCode',
                'entryName',
                'germplasmState',
                'germplasmType',
                'germplasmCode',
                'entryType',
                'entryRole',
                'entryClass',
                'description',
                'seedName',
                'packageLabel',
                'packageUnit',
                'generation',
                'occurrenceName',
                'parentage'], 'string'],
            [[
                'entryNumber',
                'packageQuantity'
            ], 'integer'],
        ];
    }

    /**
     * Retrieve dataprovider for occurrence entry browser
     *
     * @param array $params list of search parameters
     * @param array $attributes list of attributes to be retrieved from API 
     * 
     * @return array entry dataprovider
     */
    public function search ($params, $attributes = []) {
        $filters = [];
        $id = $params['id'];
        $dataProviderId = 'entries-browser';

        $paramLimit = '';
        $paramPage = '';
        $currPage = 1;
        $paramSort = '';
        $sortParams = '';

        if (
            isset($_GET["$dataProviderId-sort"]) &&
            !empty($_GET["$dataProviderId-sort"])
        ) {
            $paramSort = 'sort=';
            $sortParams = $_GET["$dataProviderId-sort"];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        } else { // set default order
            $paramSort = 'sort=entryNumber';
        }

        // get filters
        if (isset($params['Entry'])) {
            foreach ($params['Entry'] as $key => $value) {
                $val = trim($value);

                if ($val != '') {
                    $filters[$key] = (str_contains($val, '%') || str_contains($val, 'equals')) ? $val : "equals $val";
                }
            }
        }

        $filters = empty($filters) ? null : $filters;
        
        if (!empty($attributes)) {
            $filters['fields'] = $this->generateFields(array_unique([ ...$attributes,
                'entryDbId',
                'germplasmDbId',
                'packageDbId',
                'plantingInstructionDbId',
                'entryName',
            ]));
        }

        // get current page
        if (isset($_GET["$dataProviderId-page"])) {
            $currPage = intval($_GET["$dataProviderId-page"]);
            $paramPage = "&page=$currPage";
        }

        // Get page size from the preferences
        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $pagination = $paramLimit . $paramPage;

        $paramSortOriginal = $paramSort;

        // check if page is out of range
        $totalPagesArr = Yii::$app->api->getParsedResponse(
            'POST',
            "occurrences/$id/entries-search?$pagination&$paramSort",
            json_encode($filters)
        );

        // if out of range, redirect to page 1
        if ($currPage != 1 && count($totalPagesArr['data']) == 0) {
            $pagination = "$paramLimit&page=1";
            $_GET["$dataProviderId-page"] = 1; // return browser to page 1
        }
 
        $output = Yii::$app->api->getParsedResponse(
            'POST',
            "occurrences/$id/entries-search?$pagination&$paramSort",
            json_encode($filters)
        );

        $dataProvider = new ArrayDataProvider([
            'id' => $dataProviderId,
            'allModels' => $output['data'],
            'key' => 'entryDbId',
            'restified' => true,
            'totalCount' => $output['totalCount'],
            'sort' => [
                'attributes' => [
                    'entryCode', 'entryName', 'entryType', 'entryRole','entryClass', 'generation', 'parentage',
                    'seedName', 'packageLabel', 'packageUnit', 'entryNumber', 'packageQuantity',
                    'germplasmState', 'germplasmType', 'germplasmCode', 'occurrenceName', 'description'
                ],
                'defaultOrder' => [
                    'entryNumber' => SORT_ASC
                ]
            ],
        ]);

        $this->load($params);

        return [
            $dataProvider,
            $output['totalCount'],
            $paramSortOriginal,
            $filters,
        ];
    }

    /**
     * Generate string of fields
     *
     * @param array $attributes list of specified attributes
     * 
     * @return string generated string of fields
     */
    public function generateFields ($attributes)
    {
        $attributesMap = [
            'plantingInstructionDbId' => 'pi.id AS plantingInstructionDbId',
            'entryDbId' => 'pi.entry_id AS entryDbId',
            'entryCode' => 'pi.entry_code AS entryCode',
            'entryNumber' => 'pi.entry_number AS entryNumber',
            'entryName' => 'pi.entry_name AS entryName',
            'entryType' => 'pi.entry_type AS entryType',
            'entryRole' => 'pi.entry_role AS entryRole',
            'entryClass' => 'pi.entry_class AS entryClass',
            'entryStatus' => 'pi.entry_status AS entryStatus',
            'description' => 'e.description',
            'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
            'germplasmCode' => 'g.germplasm_code AS germplasmCode',
            'designation' => 'g.designation',
            'parentage' => 'g.parentage',
            'generation' => 'g.generation',
            'germplasmState' => 'g.germplasm_state AS germplasmState',
            'germplasmType' => 'g.germplasm_type AS germplasmType',
            'seedDbId' => 'pi.seed_id AS seedDbId',
            'seedCode' => 'seed.seed_code AS seedCode',
            'seedName' => 'seed.seed_name AS seedName',
            'packageDbId' => 'pi.package_id AS packageDbId',
            'packageCode' => 'package.package_code AS packageCode',
            'packageLabel' => 'package.package_label AS packageLabel',
            'packageQuantity' => 'package.package_quantity AS packageQuantity',
            'packageUnit' => 'package.package_unit AS packageUnit',
            'occurrenceDbId' => 'occurrence.id AS occurrenceDbId',
            'occurrenceName' => 'occurrence.occurrence_name AS occurrenceName',
            'occurrenceCode' => 'occurrence.occurrence_code AS occurrenceCode',
            'creationTimestamp' => 'pi.creation_timestamp AS creationTimestamp',
            'creatorDbId' => 'creator.id AS creatorDbId',
            'creator' => 'creator.person_name AS creator',
            'modificationTimestamp' => 'pi.modification_timestamp AS modificationTimestamp',
            'modifierDbId' => 'modifier.id AS modifierDbId',
            'modifier' => 'modifier.person_name AS modifier',
        ];

        $fields = '';

        foreach ($attributes as $value) {
            if (!array_key_exists($value, $attributesMap))
                continue;

            $fields .= "{$attributesMap[$value]} |";
        }

        $fields = rtrim($fields, '|');

        return $fields;
    }
}