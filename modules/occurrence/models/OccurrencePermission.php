<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\models;

use app\controllers\BrowserController;
use app\interfaces\models\IApi;
use app\models\BaseModel;

/**
 * This is the model class for table Occurrence Permissions.
 */
class OccurrencePermission extends BaseModel
{
    public function __construct(
        protected IApi $api,
        protected BrowserController $browserController,
        $config = []
    ) {
        parent::__construct($config);
    }

    /* Define fields to be validated and included in search filters */
    public function rules()
    {
        return [
            [[
                'experimentDbId',
                'occurrenceDbId',
                'siteDbId',
                'countryDbId',
            ], 'integer'],
            [[
                'experimentName',
                'experimentCode',
                'occurrenceName',
                'occurrenceCode',
                'siteCode',
                'siteName',
                'countryCode',
                'countryName',
                'entity',
                'collaborator',
                'permission',
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp',
            ], 'safe'],
        ];
    }

    /**
     * API endpoint for occurrence
     */
    public static function apiEndPoint()
    {
        return 'occurrence-permissions';
    }

    /**
     * Search for a Occurrence  Permissions given search parameters
     *
     * @param $filter search parameters
     * @param $category text current filter category
     * @param $categorySelectedId integer selected category item
     * @return $data array list of permissions given search parameters
     */
    public function searchOccurrencePermissions ($filter, $category, $categorySelectedId, $page)
    {
        $data = [];
        $totalCount = 0;
        $limit = 'limit=10';
        $page = !empty($page) ? '&page=' . $page : '';
        $sort = '&sort=creationTimestamp:desc';

        $validCategory = in_array($category, ['experiment']);

        // if there is a selected category
        if (!empty($categorySelectedId) && $categorySelectedId != 'all' && $validCategory) {
            $category = $category . 'DbId';
            $addtlParams[$category] = (string) $categorySelectedId;

            $params = $addtlParams;
        }

        $params = empty($params) ? null : $params;

        $response = $this->api->getApiResults(
            'POST',
            'occurrence-permissions-search',
            json_encode($params),
            $limit . $page . $sort,
            false,
            true
        );

        // check if successfully retrieved occurrences
        if (
            isset($response['success']) &&
            $response['success']
        ) {

            $data = ($response['data']) ?  $response['data'] : [];
            $totalCount = $response['totalCount'];
        }

        return [
            'dataProvider' => $data,
            'totalCount' => $totalCount
        ];
    }
}
