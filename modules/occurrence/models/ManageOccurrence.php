<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\models;

use app\dataproviders\ArrayDataProvider;
use app\models\Occurrence;
use app\models\UserDashboardConfig;
use app\modules\experimentCreation\models\BrowserModel;
use Yii;

/**
 * Model class for Manage occurrence data
 */
class ManageOccurrence extends Occurrence
{
    
    public $occurrenceName;
    public $site;
    public $field;
    public $description;
    public $remarks;
    private $dataVars = [];
    private $dynamicRules = [];
    private $dynamicFields = [];
    private $defaultColumns = [];

    public function __construct (
        public BrowserModel $browserModel,
        protected UserDashboardConfig $userDashboardConfig
    )
    {
        // get dynamic occurrence data variables
        $params = Yii::$app->request->queryParams;
        $usage = 'experiment_manager';

        if (str_replace('occurrence/manage/', '', Yii::$app->request->pathInfo) == 'management') {
            $usage = 'management';
        }

        // pass "management" usage if current URL is in management protocol tab
        $experimentId = isset($params['experimentId']) ? $params['experimentId'] : 0;

        $url = "experiments/$experimentId/occurrence-data-table-search?limit=1&usage=$usage";
        $response = Yii::$app->api->getParsedResponse('POST', $url, null);
        $data = $response['data'];

        $this->defaultColumns = [
            'occurrenceDbId', 'occurrenceName', 'occurrenceStatus', 'description', 'remarks',
            'experimentDbId', 'experimentCode', 'experimentName', 'siteDbId', 'siteCode', 'site',
            'fieldDbId', 'fieldCode', 'field', 'creationTimestamp', 'creator', 'creatorDbId',
            'modificationTimestamp', 'modifier', 'modifierDbId', 'locationName'
        ];

        $dataVars = [];
        if(!empty($data)){
            $variables = array_keys($data[0]);

            // This is to include only the occurrence data variables
            // 2nd parameter of array_diff contains the excluded columns
            $dataVars = array_diff($variables, $this->defaultColumns);
            $dataVars = array_values($dataVars);
        }

        $this->dynamicFields = array_fill_keys($dataVars, '');
        $this->dataVars = $dataVars;
        $this->dynamicRules = $dataVars;

    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes are the dynamic fields
     *
     * @param string $varName property name
     * @return mixed property value
     */
    public function __get($varName)
    {
        if (array_key_exists($varName, $this->dynamicFields)) {
            return $this->dynamicFields[$varName];
        } else {
            return parent::__get($varName);
        }
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes are the dynamic fields
     *
     * @param string $varName property name
     * @param mixed $value property value
     */
    public function __set($varName, $value)
    {
        if (array_key_exists($varName, $this->dynamicFields)) {
            $this->dynamicFields[$varName] = $value;
        } else {
            parent::__set($varName, $value);
        }
    }

    public function rules()
    {
        return [
            [[
                'occurrenceName', 'description', 'remarks', 'site', 'field', 'occurrenceStatus'
            ], 'string'],
            [$this->dynamicRules, 'safe']
        ];
    }

    /**
     * Retrieve manage occurrence level data of Experiment data provider
     *
     * @param Integer $experimentId Experiment identifier
     * @param String $experiment Experiment name
     * @param String $program Current program of user
     * @param String $usage Current program of user
     * @param String $function usage of the data
     *
     * @return Array $dataProvider Data provider for manage occurrence level data
     */
    public function search($experimentId, $params, $program, $usage = 'experiment_manager', $function = null){

        $dataProviderId = 'data-provider-occurrence-data';
        $dataProvider = [];
        
        $paramLimit = '';
        $paramSort = '';
        $paramPage = '';

        extract($params);

        $filters = null;

        // If the Controller retrieves only 1 occurrence
        if ($function == 'limit to 1') {
            $occurrenceDbIdFilter = [ 'occurrenceDbId' => "equals $occurrenceId" ];
    
            $filters = isset($ManageOccurrence) ? array_merge(
                $this->browserModel->formatFilters($ManageOccurrence),
                $occurrenceDbIdFilter) : $occurrenceDbIdFilter;
        } else { // Else if the Controller retrieves all occurrence within an experiment
            $filters = isset($ManageOccurrence) ? $this->browserModel->formatFilters($ManageOccurrence) : null;
        }

        if (isset($_GET["$dataProviderId-sort"])) {
            $paramSort = '&sort=';
            $sortParams = $_GET["$dataProviderId-sort"];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
            $tempOrder = ['occurrenceDbId' => SORT_ASC];
            $tempOrder = array_merge($tempOrder, $sortOrder);
        }

        $currPage = 1;
        if (isset($_GET["$dataProviderId-page"])) {
            $currPage = intval($_GET["$dataProviderId-page"]);
            $paramPage = "&page=$currPage";
        }

        // Get page size from the preferences
        $defaultPageSize = ($function == 'limit to 1') ? '1' : UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = "limit=$defaultPageSize";

        // Set pagination
        $pagination = "$paramLimit&$paramPage";

        // Add 'usage=' substring
        $usage = "usage=$usage";

        $filters = !empty($filters) ? $filters : null;

        $output = Yii::$app->api->getParsedResponse(
            'POST',
            "experiments/$experimentId/occurrence-data-table-search?$usage&$pagination&$paramSort",
            json_encode($filters)
        );

        $totalCount = $output['totalCount'];
        $attributes = array_merge($this->defaultColumns, $this->dataVars);

        $dataProvider = new ArrayDataProvider([
            'id' => $dataProviderId,
            'allModels' => $output['data'],
            'key' => 'occurrenceDbId',
            'restified' => true,
            'totalCount' => $totalCount,
            'sort' => [
                'attributes' => $attributes
            ],
        ]);

        $data = [
            'dataProvider' => $dataProvider,
            'dataVars' => $this->dataVars
        ];

        if (!($this->load($params) && $this->validate())) {
            //validate results
            return $data;
        }

        return $data;
    }
}