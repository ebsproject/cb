<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\occurrence\models;

use Yii;

use app\models\Api;
use app\models\Lists;
use app\models\Worker;

/**
 * Model for saving list in Experiment Manager
 */
class SaveList
{
    public function __construct (
        protected Api $api,
        protected Lists $lists,
        protected Worker $worker,
    ) {}

    /**
     * Save entries or plots into a new list
     * @param int $sourceId source identifier
     * @param array $attributes parameters for the API call to create a new list
     * and a data type to determine whether retrieve entries or plots
     * @param string $dbIds list of list member identifiers
     * @param string $selectAllMode identifier for way of handling tracked dbIds
     * @param object|array $filterObject API endpoint parameters for filtering retrieved data
     * @param string $sorting API endpoint parameters for sorting retrieved data
     *
     * @return array array-object containing success status and Toast message
     */
    public function saveAsList($dataLevel,$sourceId, $attributes, $dbIds, $selectAllMode, $filterObject = null, $sorting = ''){
        $dataLevel = isset($dataLevel) && !empty($dataLevel) ? $dataLevel : 'occurrence';
        
        /* Create a new list BEGIN */
        extract($attributes);
        $dbIdsArray = $dbIds ?? [];

        // Add "equals" condition to improve query runtime
        foreach ($dbIdsArray as $key => $dbId) {
            $dbIdsArray[$key] = ($selectAllMode === 'include') ? "equals $dbId" : "not equals $dbId";
        }

        $dbIdsString = ( !empty($dbIdsArray) ) ? implode('|', $dbIdsArray) : '';

        // Retrieve abbrev and name
        $responseAbbrev = $this->lists->searchAll(
            [
                'fields' => 'list.abbrev',
                'abbrev' => "equals $abbrev",
            ],
            '?limit=1'
        );

        $responseName = $this->lists->searchAll(
            [
                'fields' => 'list.name',
                'name' => "equals $name",
            ],
            '?limit=1'
        );

        // This checks if input abbrev and/or name is already existing in the database
        if ($responseAbbrev['success'] && $responseName['success']) {
            if ($responseAbbrev['totalCount'] > 0 || $responseName['totalCount'] > 0) {
                if (isset($responseAbbrev['data']) && isset($responseName['data'])) {
                    $retrievedAbbrev = $responseAbbrev['data'][0]['abbrev'];
                    $retrievedName = $responseName['data'][0]['name'];

                    if ($abbrev === $retrievedAbbrev && $name === $retrievedName) {
                        return [
                            'success' => false,
                            'message' => 'List Name and Abbreviation already exists. Please enter new ones.',
                        ];
                    } elseif ($abbrev !== $retrievedAbbrev && $name === $retrievedName) {
                        return [
                            'success' => false,
                            'message' => 'List Name already exists. Please enter a new one.',
                        ];
                    } elseif ($abbrev === $retrievedAbbrev && $name !== $retrievedName) {
                        return [
                            'success' => false,
                            'message' => 'List Abbreviation already exists. Please enter a new one.',
                        ];
                    }
                } else {
                    return [
                        'success' => false,
                        'message' => 'There was a problem in validating List Name and Abbreviation. You may file a report using the "Send feedback" form. Thank you.',
                    ];
                }
            }
        } else {
            return [
                'success' => false,
                'message' => 'There was a problem in validating List Name and Abbreviation. You may file a report using the "Send feedback" form. Thank you.',
            ];
        }

        $requestData = [
            'records' => [
                [
                    'abbrev' => $abbrev,
                    'name' => $name,
                    'description' => $description,
                    'displayName' => $displayName,
                    'remarks' => $remarks,
                    'type' => $listType,
                ]
            ]
        ];

        $output = $this->lists->create($requestData);
        /* Create a new list END */

        /* Insert data to new list BEGIN */
        $success = false;
        $message = '';
        $listId = null;

        if ($output['success'] && isset($output['data']) && !empty($output['data'])) {
            $success = true;
            $listId = $output['data'][0]['listDbId'];

            $method = 'POST';
            $params = null;

            if ($dataLevel == 'occurrence' && $dataType == 'entries') {
                $url = "occurrences/$sourceId/$dataType-search";
            } elseif ($dataLevel == 'occurrence' && $dataType == 'plots') {
                $url = "occurrences/$sourceId/plot-data-table-search?ownershipType=shared";
            } elseif ($dataLevel == 'location') {
                $url = "locations/$sourceId/plots-search";
            }

            // Set appropriate parameters
            if ($dataType === 'entries') {
                if ($listType === 'germplasm') {
                    $params = [
                        'fields' => 'pi.entry_id AS dbId | pi.entry_name AS displayValue | pi.germplasm_id AS id',
                    ];
                }
                elseif ($listType === 'package') {
                    $params = [
                        'fields' => 'pi.entry_id AS dbId | package.package_label AS displayValue | pi.package_id AS id',
                    ];
                }
            } elseif ($dataType === 'plots') {
                if ($listType === 'germplasm') {
                    $params = [
                        'fields' => 'plot.id AS dbId | pi.entry_name AS displayValue | pi.germplasm_id AS id',
                    ];
                }
                elseif ($listType === 'package') {
                    $params = [
                        'fields' => 'plot.id AS dbId | package.package_label AS displayValue | pi.package_id AS id',
                    ];
                }
                elseif ($listType === 'plot') {
                    $params = [
                        'fields' => 'plot.id AS dbId | plot.plot_code AS displayValue | plot.id AS id',
                    ];
                }
            }

            if ($dbIdsString != '') {
                $params['dbId'] = $dbIdsString;
            }

            // Check if $filterObject filters and fields are existing...
            // ...add filters and fields to $params
            if($filterObject){
                if(isset($filterObject['filters']['fields'])){
                    $params['fields'] .= " | {$filterObject['filters']['fields']}";
                    unset($filterObject['filters']['fields']);
                }

                if(isset($filterObject['additionalFields'])){
                    // Add additionalFields if not found in $params to prevent column ambiguity
                    if ((strpos($params['fields'], $filterObject['additionalFields'])) === false) {
                        $params['fields'] .= " | {$filterObject['additionalFields']}";
                        unset($filterObject['additionalFields']);
                    }
                }

                $params = array_merge(
                    $params,
                    $filterObject['filters']
                );

                $filters = $filterObject['filters'];

                // Concatenate trait abbrevs into "fields"
                foreach ($filters as $filterKey => $value) {
                    // If current filter key is in uppercase, it is a trait abbrev
                    // Add it to "fields"
                    if ($filterKey == strtoupper($filterKey)) {
                        $params['fields'] .= "| $filterKey";
                    }
                }
            }

            // Trigger background worker
            $workerName = 'CreateListMembers';
            $description = "Saving $dataType as $listType list";
            $entity = 'OCCURRENCE';
            $entityDbId = "$listId";
            $endpointEntity = 'LIST_MEMBER';
            $application = 'OCCURRENCES';
            $method = 'POST';
            $workerData = [
                'type' => $listType,
                'parameters' => [
                    'columnFilters' => $params,
                    'endpoint' => $url,
                    'idColumn' => 'id',
                    'listId' => "$listId",
                    'sorting' => $sorting
                ]
            ];

            $url = 'occurrences-search';
            $attributeFilter = $dataLevel.'DbId';
            $attributeColumn = $dataLevel.'Name';

            if ($dataLevel == 'location') {
                $entity = 'LOCATION';
                $url = 'locations-search';
            }

            try {
                $this->worker->invoke(
                    $workerName,
                    $description,
                    $entity,
                    $entityDbId,
                    $endpointEntity,
                    $application,
                    $method,
                    $workerData
                );
            } catch (\Exception $e) {
                $success = false;
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';

                $success = false;
                $message = "There was a problem in saving $dataType as a $listType list.";
                $this->lists->deleteOne($listId);
    
                return [
                    'success' => $success,
                    'message' => $message,
                ];
            }

            // get Source Name for BG processing notification
            $sourceData = $this->api->getApiResults(
                'POST',
                $url.'?limit=1',
                json_encode([
                    $attributeFilter => "equals $sourceId"
                ])
            );
            $sourceName = $sourceData['data'][0][$attributeColumn];

            $message = "You are now seeing the transaction for <strong>$sourceName</strong> is now <strong>saving $dataType as a $listType list ($abbrev)</strong>. If there are no valid IDs present, there will still be a list created but <strong>no list members</strong> will be saved in the list. You will be notified once done. Click the Reset Grid button to display all records.";

            // set background processing info to session
            Yii::$app->session->set('em-bg-processs-message', $message);
            Yii::$app->session->set('em-bg-processs', true);
            // assign current bg process task type and list abbrev to session variables
            Yii::$app->session->set('bg-process-task-type-' . $listId, 'saveListMembers');
            Yii::$app->session->set('bg-process-list-abbrev-' . $listId, $abbrev);

            $browserFilter = 'occurrenceDbId';
            $browserFilterValue = $sourceId;

            if ($dataLevel == 'location') {
                $browserFilter = 'location';
                $browserFilterValue = $sourceData['data'][0]['locationName'];
            }

            // redirect to Occurrence browser
            $program = Yii::$app->session->get($dataLevel. '-' . $dataType . '-program');
            Yii::$app->response->redirect(['/occurrence?program='.$program.'&OccurrenceSearch['.$browserFilter.']='.$browserFilterValue]);

            return;
        } else { // There was an error in creating a new list
            $success = false;

            if (strpos($output['message'], 'name already exists'))
                $message = 'Name or Abbrev already exists. Please enter another.';
            else
                $message = 'There was a problem in saving ' . $dataType . ' as ' . $listType . ' list.';

            return [
                'success' => $success,
                'message' => $message,
            ];
        }
	}
}