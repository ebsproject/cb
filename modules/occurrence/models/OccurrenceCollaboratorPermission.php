<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\models;

use app\controllers\BrowserController;
use app\interfaces\models\IApi;
use app\models\BaseModel;

/**
 * This is the model class for table Occurrence Collaborator Permissions.
 *
 * @property int $id Occurrrence ID: Database identifier of the occurrence [OCC_ID]
 * @property string $occurrence_code Occurrence Code: Textual identifier of the occurrence [OCC_CODE]
 * @property string $occurrence_name Occurrence Name: Full name of the occurrence [OCC_NAME]
 * @property string $occurrence_status Occurrence Status: Status of the occurrence with regard to its plots {draft, mapped} [OCC_STATUS]
 * @property string|null $description Occurrence Description: Additional information about the occurrence [OCC_DESC]
 * @property int $experiment_id Experiment ID: Reference to the experiment that is represented by the occurrence [OCC_EXPT_ID]
 * @property int|null $geospatial_object_id Geospatial Object ID: Reference to the site or field (geospatial object) where the occurrence is planned to be conducted [OCC_GEO_ID]
 * @property int $creator_id
 * @property string $creation_timestamp
 * @property int|null $modifier_id
 * @property string|null $modification_timestamp
 * @property bool $is_void
 * @property string|null $notes
 * @property string|null $access_data
 * @property string|null $event_log
 * @property int|null $rep_count Number of replication
 * @property string|null $occurrence_document Sorted list of distinct lexemes which are normalized; used in search query
 */
class OccurrenceCollaboratorPermission extends BaseModel
{
    public function __construct(
        protected IApi $api,
        protected BrowserController $browserController,
        $config = []
    ) {
        parent::__construct($config);
    }

    /* Define fields to be validated and included in search filters */
    public function rules()
    {
        return [
            [[
                'experimentDbId',
                'experimentStewardDbId',
                'occurrenceDbId',
                'programDbId',
                'siteDbId',
                'locationDbId',
                'locationStewardDbId',
            ], 'integer'],
            [[
                'experimentName',
                'experimentCode',
                'experimentSteward',
                'occurrenceName',
                'occurrenceCode',
                'occurrenceStatus',
                'programCode',
                'programName',
                'siteCode',
                'siteName',
                'countryCode',
                'countryName',
                'locationName',
                'locationCode',
                'locationSteward',
                'collaboratorPermissions',
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp',
            ], 'safe'],
        ];
    }

    /**
     * API endpoint for occurrence
     */
    public static function apiEndPoint()
    {
        return 'occurrence-collaborator-permissions';
    }

    /**
     * Search for a Occurrence Collaborator Permissions given search parameters
     *
     * @param $filter search parameters
     * @param $category text current filter category
     * @param $categorySelectedId integer selected category item
     * @return $data array list of collaborator permissions given search parameters
     */
    public function searchOccurrenceCollaboratorPermissions($filter, $category, $categorySelectedId, $page)
    {
        $data = [];
        $totalCount = 0;
        $limit = 'limit=10';
        $page = !empty($page) ? '&page=' . $page : '';
        $sort = '&sort=creationTimestamp:desc';

        // default parameter, exclude draft occurrences
        $params["occurrenceStatus"] = "not equals draft";

        // if filter is set, build params
        if (!empty($filter)) {
            // process filter for document
            $processedFilter = $this->browserController->processDocumentFilter($filter);

            $params["occurrenceDocument"] = $processedFilter;
        }

        $validCategory = in_array($category, ['experiment', 'location']);

        // if there is a selected category
        if (!empty($categorySelectedId) && $categorySelectedId != 'all' && $validCategory) {
            $category = $category . 'DbId';
            $addtlParams[$category] = (string) $categorySelectedId;

            $params = $addtlParams;
        }

        $params = empty($params) ? null : $params;

        $response = $this->api->getApiResults(
            'POST',
            'occurrence-collaborator-permissions-search',
            json_encode($params),
            $limit . $page . $sort,
            false,
            true
        );

        // check if successfully retrieved occurrences
        if (
            isset($response['success']) &&
            $response['success']
        ) {

            $data = ($response['data']) ?  $response['data'] : [];
            $totalCount = $response['totalCount'];
        }

        return [
            'dataProvider' => $data,
            'totalCount' => $totalCount
        ];
    }
}
