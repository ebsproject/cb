<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\models\BaseModel;
use app\models\UserDashboardConfig;

/**
 * Model for searching plots of an occurrence
 */
class Plot extends BaseModel
{
    public $plotDbId;
    public $plotNumber;
    public $entryNumber;
    public $entryName;
    public $germplasmState;
    public $germplasmType;
    public $parentage;
    public $entryCode;
    public $entryType;
    public $entryClass;
    public $entryRole;
    public $entryStatus;
    public $plotCode;
    public $rep;
    public $designX;
    public $designY;
    public $paX;
    public $paY;
    public $fieldX;
    public $fieldY;
    public $blockNumber;
    public $rowBlockNumber;
    public $colBlockNumber;
    public $occurrenceDbId;
    public $traits = [];
    public $germplasmCode;

    private $dynamicRules = [];
    private $dynamicFields = [];

    /**
     * Set data browser fields
     * This overrides construct
     * @param array $config Array with occurrenceDbId
     */
    public function __construct (protected UserDashboardConfig $userDashboardConfig, $config = [])
    {
        extract($config);
        
        if (str_contains(Yii::$app->request->url, 'occurrence/view/plot')) {
            $params = Yii::$app->request->queryParams;
            $occurrenceDbId = isset($params['id']) ? $params['id'] : 0;

            $data = [];

            if ($occurrenceDbId) {
                $url = 'occurrences/' . $occurrenceDbId . '/plot-data-table-search?limit=1';
                $response = Yii::$app->api->getResponse('POST', $url, null); 
                $data = $response['body']['result']['data']; 
            }
    
            $traits = [];

            if (!empty($data)) {
                $traits = array_keys($data[0]);
    
                // This is to include only the Traits in the $traits variable
                // 2nd Parameter of array_diff contains the excluded columns
                $traits = array_diff($traits, [
                    'plotDbId', 'entryDbId', 'entryCode', 'entryNumber',
                    'entryName', 'entryType', 'entryRole', 'entryClass', 'entryStatus', 'germplasmDbId',
                    'plotCode', 'plotNumber', 'plotType', 'rep', 'designX', 'designY', 'plotOrderNumber',
                    'paX', 'paY', 'fieldX', 'fieldY', 'blockNumber', 'plotStatus', 'plotQcCode',
                    'creationTimestamp', 'creator', 'creatorDbId', 'modificationTimestamp', 'modifier',
                    'modifierDbId', 'seedDbId', 'seedCode', 'seedName', 'packageDbId', 'packageCode', 'packageLabel', 'parentage', 'generation',
                    'germplasmState', 'germplasmType', 'rowBlockNumber', 'colBlockNumber', 'germplasmCode',
                ]);
                $traits = array_values($traits);
                $this->dynamicFields = array_fill_keys($traits, '');
    
                $this->traits = $traits;
                $this->dynamicRules = $traits;
            }    
        }

        parent::__construct($config);
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        // This function allows the included columns to display a text field (search filter)...
        // ...on the data browser (DynaGrid)
        return [
            [[
                'entryCode', 'entryNumber', 'entryName', 'entryType', 'entryRole',
                'entryClass', 'entryStatus', 'plotCode', 'parentage',
                'germplasmState', 'germplasmType', 'germplasmCode',
            ], 'string'],
            [[
                'entryNumber', 'plotNumber', 'rep', 'designX', 'designY', 'paX', 'paY',
                'fieldX', 'fieldY', 'blockNumber', 'occurrenceDbId', 'plotOrderNumber',
                'rowBlockNumber', 'colBlockNumber'
            ], 'integer'],

            [$this->dynamicRules,  'safe']
        ];
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes are the dynamic fields
     *
     * @param string $name property name
     * @return mixed property value
     * @see getAttribute()
     */
    public function __get($name)
    {

        if (array_key_exists($name, $this->dynamicFields))
            return $this->dynamicFields[$name];
        else
            return parent::__get($name);
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes are the dynamic fields
     *
     * @param string $name property name
     * @param mixed $value property value
     */
    public function __set($name, $value)
    {

        if (array_key_exists($name, $this->dynamicFields))
            $this->dynamicFields[$name] = $value;
        else
            parent::__set($name, $value);
    }

    /**
     * Search functionality for data browser
     * 
     * @param array $params contains the filter parameters
     * @param array $attributes list of attributes to be retrieved from API
     * 
     * @return ArrayDataProvider the data provider for the browser
     */
    public function search($params = null, $attributes = [])
    {
        $dataProviderId = 'occurrence-plot-browser';
        $this->load($params);

        $filters = [];
        $exportFilters = [];
        $totalPages = 0;
    
        if (isset($params['Plot'])) {
            unset($params['Plot']['occurrenceDbId']);
            foreach ($params['Plot'] as $key => $value) {
                if (isset($value)) {
                    $value = trim($value);
                    if (in_array($key, [
                        'entryCode', 'entryName', 'entryType', 'entryRole',
                        'entryClass', 'entryStatus', 'plotCode', 'plotType',
                        'plotStatus', 'plotQcCode', 'parentage',
                        'germplasmState', 'germplasmType', 'germplasmCode',
                    ])) {
                        if ($value !== '') {
                            $filters[$key] = (str_contains($value, '%') || str_contains($value, 'equals')) ? $value : "equals $value";
                            $exportFilters[$key] = (str_contains($value, '%') || str_contains($value, 'equals')) ? $value : "equals $value";
                        }
                    } else if (in_array($key, [ // If key is of integer type
                        'entryNumber', 'plotNumber', 'rep', 'designX', 'designY',
                        'paX', 'paY', 'fieldX', 'fieldY', 'blockNumber', 'rowBlockNumber', 'colBlockNumber'
                    ])) {
                        if ($value !== '') {
                            $filters[$key] = (str_contains($value, '%') || str_contains($value, 'equals')) ? $value : "equals $value";
                            $exportFilters[$key] = (str_contains($value, '%') || str_contains($value, 'equals')) ? $value : "equals $value";
                        }
                    } else if ($value !== '') {                        
                        $filters[$key] = ['dataValue' => (str_contains($value, '%') || str_contains($value, 'equals')) ? $value : "equals $value"];
                        $exportFilters[$key] = (str_contains($value, '%') || str_contains($value, 'equals')) ? $value : "equals $value";
                    }
                }
            }
        }

        $paramSort = '';
        if (isset($_GET["$dataProviderId-sort"])) {
            $paramSort = '&sort=';
            $sortParams = $_GET["$dataProviderId-sort"];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
            $tempOrder = ['plotOrderNumber' => SORT_ASC];
            $tempOrder = array_merge($tempOrder, $sortOrder);
        }
        $paramPage = '';
        $currPage = 1;
        if (isset($_GET["$dataProviderId-page"])) {
            $currPage = intval($_GET["$dataProviderId-page"]);
            $paramPage = "&page=$currPage";
        }

        $paramLimit = '';
        // Get page size from the preferences
        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $filters = empty($filters) ? null : $filters;
        $url = 'occurrences/' . $this->occurrenceDbId . '/plot-data-table-search?ownershipType=shared';
        $pagination = $paramLimit.$paramPage;

        if (!empty($attributes)) {
            $filters['fields'] = $this->generateFields(array_unique([ ...$attributes, 'plotDbId', 'germplasmDbId', ]));
        }

        $output = Yii::$app->api->getParsedResponse(
            'POST',
            "$url&$pagination&$paramSort",
            json_encode($filters)
        );

        if (isset($output['totalPages'])) {
            $totalPages = $output['totalPages'];
        }

        // check if page is out of range
        if ($currPage > $totalPages) {
            $pagination = $paramLimit.'&page=1';
            $_GET["$dataProviderId-page"] = 1; // return browser to page 1

            // get plot records from page 1
            $output = Yii::$app->api->getParsedResponse(
                'POST',
                "$url&$pagination&$paramSort",
                json_encode($filters)
            );
        }

        $paramSortOriginal = $paramSort;

        $data = $output['data'];

        // repackage data and update sort attributes
        $tempData = [];
        foreach ($data as $index => $d) {
            $tempData[$index] = $d;
            $tempData[$index]['plotOrderNumber'] = $index;
        }
        // This enables the included columns' sort functionality
        $sortAttributes = [
            'entryCode', 'entryNumber', 'entryName', 'entryType', 'entryRole', 'entryClass',
            'entryStatus', 'plotCode', 'plotNumber', 'plotType', 'rep',
            'designX', 'designY', 'paX', 'paY', 'fieldX', 'fieldY', 'blockNumber', 'parentage',
            'germplasmState', 'germplasmType', 'rowBlockNumber', 'colBlockNumber', 'rowBlockNumber',
            'colBlockNumber', 'germplasmCode',
            'plotOrderNumber' => [
                "asc" => ["plotOrderNumber" => SORT_ASC],
                'desc' => ["plotOrderNumber" => SORT_DESC]
            ]
        ];
        $sortAttributes = array_merge($sortAttributes, $this->traits);

        $dataProvider = new ArrayDataProvider([
            'id' => $dataProviderId,
            'allModels' => $tempData,
            'key' => 'plotDbId',
            'restified' => true,
            'totalCount' => $output['totalCount'],
            'sort' => [
                'attributes' => $sortAttributes,
                'defaultOrder' => [
                    'plotOrderNumber' => SORT_ASC
                ]
            ]
        ]);

        return [
            $dataProvider,
            $output['totalCount'],
            $paramSortOriginal,
            $filters,
            $exportFilters
        ];
    }

    /**
     * Generate string of fields
     *
     * @param array $attributes list of specified attributes
     * 
     * @return string generated string of fields
     */
    public function generateFields ($attributes)
    {
        $attributesMap = [
            'plotDbId' => 'plot.id AS plotDbId',
            'entryDbId' => 'plot.entry_id AS entryDbId',
            'entryCode' => 'pi.entry_code AS entryCode',
            'entryNumber' => 'pi.entry_number AS entryNumber',
            'entryName' => 'pi.entry_name AS entryName',
            'entryType' => 'pi.entry_type AS entryType',
            'entryRole' => 'pi.entry_role AS entryRole',
            'entryClass' => 'pi.entry_class AS entryClass',
            'entryStatus' => 'pi.entry_status AS entryStatus',
            'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
            'germplasmCode' => 'g.germplasm_code AS germplasmCode',
            'parentage' => 'g.parentage',
            'generation' => 'g.generation',
            'germplasmState' => 'g.germplasm_state AS germplasmState',
            'germplasmType' => 'g.germplasm_type AS germplasmType',
            'seedDbId' => 'pi.seed_id AS seedDbId',
            'seedCode' => 'seed.seed_code AS seedCode',
            'seedName' => 'seed.seed_name AS seedName',
            'packageDbId' => 'pi.package_id AS packageDbId',
            'packageCode' => 'package.package_code AS packageCode',
            'packageLabel' => 'package.package_label AS packageLabel',
            'plotCode' => 'plot_code AS plotCode',
            'plotNumber' => 'plot_number AS plotNumber',
            'plotType' => 'plot_type AS plotType',
            'rep' => 'rep',
            'designX' => 'design_x AS designX',
            'designY' => 'design_y AS designY',
            'plotOrderNumber' => 'plot_order_number AS plotOrderNumber',
            'paX' => 'pa_x AS paX',
            'paY' => 'pa_y AS paY',
            'fieldX' => 'field_x AS fieldX',
            'fieldY' => 'field_y AS fieldY',
            'plotStatus' => 'plot_status AS plotStatus',
            'plotQcCode' => 'plot_qc_code AS plotQcCode',
            'creationTimestamp' => 'pi.creation_timestamp AS creationTimestamp',
            'creatorDbId' => 'creator.id AS creatorDbId',
            'creator' => 'creator.person_name AS creator',
            'modificationTimestamp' => 'plot.modification_timestamp AS modificationTimestamp',
            'modifierDbId' => 'modifier.id AS modifierDbId',
            'modifier' => 'modifier.person_name AS modifier',
            'blockNumber' => 'block_number AS blockNumber',
            'rowBlockNumber' => 'descol1.block_value AS rowBlockNumber',
            'colBlockNumber' => 'descol2.block_value AS colBlockNumber',
        ];

        $fields = '';

        foreach ($attributes as $value) {
            if (!array_key_exists($value, $attributesMap))
                continue;

            $fields .= "{$attributesMap[$value]} |";
        }

        $fields = rtrim($fields, '|');

        return $fields;
    }
}
