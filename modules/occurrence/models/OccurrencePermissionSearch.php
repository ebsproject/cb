<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\models;

use app\controllers\BrowserController;
use app\dataproviders\ArrayDataProvider;
use app\modules\experimentCreation\models\BrowserModel;
use app\interfaces\models\IApi;
use Yii;

/**
 * Contains methods for searching an occurrence
 */
class OccurrencePermissionSearch extends OccurrencePermission
{
    private $data;

    public function __construct(
        protected IApi $api,
        protected BrowserController $browserController,
        protected BrowserModel $browserModel,
        $config = []
    ) {
        parent::__construct(
            $api,
            $browserController,
            $config
        );
    }

    public function __get($varName)
    {
        return $this->data[$varName] ?? '';
    }

    public function __set($varName, $value)
    {
        $this->data[$varName] = $value;
    }

    /**
     * Search for occurrence
     *
     * @param $filter array list of search parameters
     * @param $category text current filter category
     * @param $selectedCategoryId integer selected category item
     * @param $page integer page in api response to be displayed
     * @param $pageParams data browser parameters
     * @param $originTab name of EC tab
     * @return $dataProvider array list of occurrences
     */
    public function search ( $filter = null, $category = null, $selectedCategoryId = null, $page = null, $sortArray = null, $pageParams = '', $originTab = '')
    {
        $response = $this->searchOccurrencePermissions($filter, $category, $selectedCategoryId, $page);

        $totalCount = $response['totalCount'];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $response['dataProvider'],
            'key' => 'occurrenceDbId',
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'experimentDbId',
                    'experimentName',
                    'experimentCode',
                    'occurrenceDbId',
                    'occurrenceName',
                    'occurrenceCode',
                    'siteDbId',
                    'siteCode',
                    'siteName',
                    'countryDbId',
                    'countryCode',
                    'countryName',
                    'entity',
                    'collaborator',
                    'permission',
                ],
                'defaultOrder' => [
                    'creationTimestamp' => SORT_DESC
                ],
            ]
        ]);

        return [
            'dataProvider' => $dataProvider,
            'totalCount' => $totalCount
        ];
    }

    /**
     * Search Occurrence permissions
     *
     * @param array $params filter parameters
     * 
     * @return array $dataProvider occurrence records
     */
    public function searchOccurrencePermission ($params)
    {
        Yii::debug(
            'Searching for Occurrence Permissions ' .
                json_encode(['params' => $params]),
            __METHOD__
        );

        extract($params);

        $filters = [
            'occurrenceDbId' => $occurrenceDbId,
        ];

        if (isset($entityDbId) && !empty($entityDbId))
            $filters['entityDbId'] = $entityDbId;

        $columnFilters = isset($OccurrencePermissionSearch) ? $this->browserModel->formatFilters($OccurrencePermissionSearch) : [];

        $filters = array_merge($filters, $columnFilters);
        $paramLimit = 'limit=10';
        $paramPage = '';
        $paramSort = '';

        // get current page
        $currPage = 1;
        if (isset($_GET['dp-1-page'])) {
            $paramPage = '&page=' . $_GET['dp-1-page'];
            $currPage = $_GET['dp-1-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
            $currPage = $_GET['page'];
        }

        // get current sort
        $paramSort = 'sort=experimentDbId:desc|occurrenceDbId';
        $sortParams = '';
        if (isset($_GET['sort'])) {
            $sortParams = $_GET['sort'];
        } else if (isset($_GET['dp-1-sort'])) {
            $sortParams = $_GET['dp-1-sort'];
        }

        // for sorting
        if (!empty($sortParams)) {
            if ($sortParams[0] == '-') {
                $order = ':desc';
                $column = substr($sortParams, 1);
            } else {
                $order = '';
                $column = $sortParams;
            }

            $paramSort = 'sort=' . $column . $order;
        }

        // Get page size from cookies of dynagrid
        $cookies = Yii::$app->request->cookies;
        if (isset($cookies['occurrence-permissions-grid_'])) {
            $paramLimit = $cookies->getValue('occurrence-permissions-grid_');
            if (!is_array($paramLimit)) {
                $paramLimit = json_decode($paramLimit, true);
                $paramLimit = json_decode($paramLimit['grid'], true);
                $paramLimit = $paramLimit['page'];
                $paramLimit = 'limit=' . $paramLimit;
            } else {
                $paramLimit = 'limit=10';
            }
        }

        $url = 'occurrence-permissions-search?' . $paramSort;

        $output = Yii::$app->api->getParsedResponse(
            'POST',
            $url,
            json_encode($filters),
            '',
            true
        );

        if (isset($output['totalPages'])) {
            $totalPages = $output['totalPages'];
        }

        Yii::$app->session->set('occurrence-permission-search-occurrence-permissions-session-vars', [
            (int) $currPage ?? null,
            $paramLimit ?? '',
            $paramPage ?? '',
            $paramSort ?? '',
            $filters ?? null,
        ]);

        $attributes = [
            'experimentDbId',
            'experimentName',
            'experimentCode',
            'occurrenceDbId',
            'occurrenceName',
            'occurrenceCode',
            'siteDbId',
            'siteCode',
            'siteName',
            'countryDbId',
            'countryCode',
            'countryName',
            'entity',
            'collaborator',
            'permission',
        ];

        $this->dynamicRules = [[$attributes, 'safe']];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $output['data'],
            'key' => 'occurrenceDbId',
            'restified' => true,
            'totalCount' => $output['totalCount'],
            'sort' => [
                'defaultOrder' => [
                    'experimentDbId' => SORT_DESC,
                    'collaborator' => SORT_ASC,
                ],
                'attributes' => $attributes
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            // Validate results
            return $dataProvider;
        }
    }
}
