<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\controllers;

use app\components\B4RController;
use app\interfaces\models\IApi;
use app\models\BackgroundJob;
use app\models\GeospatialObject;
use app\models\Location;
use app\models\LocationOccurrenceGroup;
use app\models\Occurrence;
use app\models\Person;
use app\models\PlantingInstruction;
use app\models\Plot;
use app\models\Worker;
use app\modules\occurrence\models\Upload;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Yii;

/**
 * Controller for the upload mapped plots
 */
class UploadController extends B4RController
{
    const UPLOAD_OCCURRENCE_DATA_IN_PROGRESS = 'upload occurrence data in progress';
    const UPLOAD_OCCURRENCE_DATA_FAILED = 'upload occurrence data failed';

    public function __construct($id, $module,
        protected IApi $api,
        protected BackgroundJob $backgroundJob,
        protected GeospatialObject $geospatialObject,
        protected Location $location,
        protected LocationOccurrenceGroup $locationOccurrenceGroup,
        protected Occurrence $occurrence,
        protected Person $person,
        protected PlantingInstruction $plantingInstruction,
        protected Plot $plot,
        protected Upload $upload,
        protected Worker $worker,
        $config = []
    ) { parent::__construct($id, $module, $config); }

    /**
     * Render upload planting array web form
     *
     * @return text $htmlData web form
     */
    public function actionRenderUploadPlantingArraysForm()
    {
        // get logged in person display name and set as default
        $person = $this->person->getLoggedInPersonInfo();
        $personName = isset($person['personName']) ? $person['personName'] : null;
        $action = isset($_POST['action']) ? $_POST['action'] : null;

        $locationName =  [[
            'variable_abbrev' => 'LOCATION_NAME',
            'required' => 'required',
            'variable_type' => 'identification'
        ]];

        $location = [[
            'variable_abbrev' => 'LOCATION',
            'required' => 'required',
            'variable_type' => 'identification',
            'target_column' => 'locationDbId',
            'target_value' => 'locationCode',
            'secondary_target_value' => 'locationName',
            'api_resource_method' => 'POST',
            'api_resource_endpoint' => 'locations-search',
            'api_resource_filter' => [
                'locationDbId' => '0'
            ],
        ]];

        // retieve configuration
        $data = [
            [
                'variable_abbrev' => 'LOCATION_STEWARD',
                'required' => 'required',
                'default' => $personName,
                'variable_type' => 'identification',
                'target_column' => 'stewardDbId',
                'secondary_target_column' => 'personDbId',
                'target_value' => 'personName',
                'api_resource_method' => 'GET',
                'api_resource_endpoint' => 'persons',
                'api_resource_sort' => 'sort=personName',
                'api_resource_filter' => [
                    'personDbId' => '0'
                ],
            ],
            [
                'variable_abbrev' => 'DESCRIPTION',
                'variable_type' => 'identification'
            ],
        ];

        // if in generate location
        if($action=='generate-location'){
            $data = array_merge($locationName, $data);
        } else {
            $location = array_merge($locationName, $location);
            $data = array_merge($location, $data);
        }

        $widgetOptions = [
            'mainModel' => 'Location',
            'config' => $data,
            'noInstructions' => 'true',
            'columnClass' => ($action != 'generate-location') ? 'col-md-6 s6' : ''
        ];

        $viewFile = '@app/modules/occurrence/views/upload/planting_arrays.php';

        $htmlData = $this->renderAjax($viewFile, [
            'widgetOptions' => $widgetOptions,
            'action' => $action,
            'program' => Yii::$app->userprogram->get('abbrev'),
        ]);

        return json_encode($htmlData);
    }

    /**
     * Render Location options when mapping an
     * Occurrence to an existing Location
     *
     * @param Integer $id Occurrence identifier
     * @param Integer $mappedLocationId Location identifier if already mapped
     * @param Array $options list of Location options
     */
    public function actionRenderLocationOptions($id, $mappedLocationId = null)
    {
        $options = $this->location->getValidLocationsByOccurrenceId($id, $mappedLocationId);

        return json_encode($options);
    }

    /**
     * Render upload occurrence data collection
     *
     * @return text $htmlData UI content
     */
    public function actionRenderUploadOccurrenceDataCollection ()
    {
        return json_encode($this->renderAjax('occurrence_data_collection', [
            'program' => Yii::$app->userprogram->get('abbrev'),
        ]));
    }

    /**
     * Upload mapped plots in CSV format
     *
     * @return array $result successful or not, and if not, includes error message
     */
    public function actionPlantingArrays()
    {
        try {
            if (isset($_FILES['upload_planting_arrays'])) {
                // validate file
                $result = $this->upload->validatePlantingArrays('upload_planting_arrays');

                return json_encode($result);
            }
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'error' => 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.'
            ]);
        }
    }

    /**
     * Validate the file if does not contain duplicate
     * combination of paX-paY and fieldX-fieldY
     *
     * @return Array $result Whether success or not and return message
     */
    public function actionValidateMapToExistingLoc()
    {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $result = $this->upload->validateMapToExistingLoc($id);

        return json_encode($result);
    }

    /**
     * Upload occurrence data collection in CSV format
     *
     * @return array $result successful or not, and if not, includes error message
     */
    public function actionOccurrenceDataCollection()
    {
        try {
            if (isset($_FILES['upload_occurrence_data_collection'])) {
                // validate file
                $result = $this->upload->validateOccurrenceData('upload_occurrence_data_collection');

                if ($result['success']) {
                    // Store occurrence IDs detected in CSV input to session
                    Yii::$app->session->set('upload-occurrence-data-collection-occurrence-ids', $result['id']);
                }

                return json_encode($result);
            }
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'error' => 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.'
            ]);
        }
    }

    /**
     * Preview upload planting array
     */
    public function actionPreviewPlantingArrays()
    {
        // unset session
        unset(Yii::$app->session['upload-planting-arrays-is-loc-existing']);
        $action = (isset($_POST['action']) && !empty($_POST['action'])) ? $_POST['action'] : null;

        // get values in form
        $formData = Yii::$app->request->post();
        // form input values for Occurrence
        $formData = (isset($_POST['Location']) && !empty($_POST['Location'])) ? $_POST['Location'] : [];

        // occurrence ID
        $occurrenceId = (isset($_POST['occurrenceId']) && !empty($_POST['occurrenceId'])) ? $_POST['occurrenceId'] : null;
        $formattedOccurrenceId = $occurrenceId;

        if (is_array($occurrenceId)) {
            $formattedOccurrenceId = implode('|', $occurrenceId);
            $singleOccurrenceId = $occurrenceId[0];
        } else {
            $singleOccurrenceId = $occurrenceId;
        }

        // Whether to map to an existing location or not
        $isExLoc = (isset($_POST['isExLoc']) && !empty($_POST['isExLoc'])) ? $_POST['isExLoc'] : false;
        Yii::$app->session->set('upload-planting-arrays-is-loc-existing', $isExLoc);

        // Whether to use Design layout or not
        $useDesignLayout = isset($_POST['useDesignLayout']) ? $_POST['useDesignLayout'] : 'false';
        Yii::$app->session->set('generate-location-use-design-layout', $useDesignLayout);

        // get location information
        $identificationParams = [];
        foreach ($formData as $key => $value) {
            // to check if identification or metadata
            $type = array_keys($value);
            $typeVal = isset($type[0]) ? $type[0] : '';
            // get variable ID and value
            $variableId = array_keys($value[$typeVal]);
            $variableIdVal = isset($variableId[0]) ? $variableId[0] : null;
            $val = $value[$typeVal][$variableIdVal];
            $val = trim($val);
            $val = empty($val) ? null : $val;

            // if identification
            if ($typeVal == 'identification') {
                // format variable according to api attribute text case (e.g. occurrenceName)
                $attr = ucwords(strtolower($key), '_');
                $attribute = str_replace('_', '', $attr);
                $attribute = lcfirst($attribute);

                $identificationParams[$attribute] = $val;
            }
        }

        // if location is already existing
        if ($isExLoc == 'true') {
            // get location info
            $locationDbId = Yii::$app->session->get('upload-planting-arrays-location-id');
            $locParam['locationDbId'] = "equals $locationDbId";

            $loc = $this->location->searchAll($locParam, null, false);

            $locName = (isset($loc['data'][0]['locationName'])) ? $loc['data'][0]['locationName'] : '';
            $locCode = (isset($loc['data'][0]['locationCode'])) ? $loc['data'][0]['locationCode'] : '';
            $locationSteward = Yii::$app->session->get('upload-planting-arrays-steward-id');

            $locationName = $locName . ' (' . $locCode . ')';

            // get occurrences in the location
            $log['locationDbId'] = "equals $locationDbId";
            $logsData = $this->locationOccurrenceGroup->searchAll($log, 'sort=orderNumber:desc');
            $logsArr = (isset($logsData['data'])) ? $logsData['data'] : [];

            $occurrenceArray = [];

            // add occurrences already assigned to the selected location
            foreach ($logsArr as $key => $value) {
                $occurrenceArray[] = $value['occurrenceDbId'];
            }
            $occurrenceArray = array_merge($occurrenceArray, explode("|", $formattedOccurrenceId));
        } else {
            $occurrenceArray = explode("|", $formattedOccurrenceId);
            $locationName = $identificationParams["locationName"];
            $locationSteward = $identificationParams["locationSteward"];
            $locationDescription = $identificationParams["description"];
        }

        // retrieve sites of the occurrences already assigned to the selected location

        array_walk($occurrenceArray, function (&$value, $key) {
            $value = "equals $value";
        });

        $occurrenceParam['occurrenceDbId'] = implode('|', $occurrenceArray);
        $occurrence = $this->occurrence->searchAll($occurrenceParam, null, false);

        // get occurrence year and season
        $identificationParams['locationYear'] = (isset($occurrence['data'][0]['experimentYear'])) ? $occurrence['data'][0]['experimentYear'] : '';
        $identificationParams['seasonDbId'] = (isset($occurrence['data'][0]['experimentSeasonDbId'])) ? $occurrence['data'][0]['experimentSeasonDbId'] : '';
        $identificationParams['siteDbId'] = (isset($occurrence['data'][0]['siteDbId'])) ? $occurrence['data'][0]['siteDbId'] : '';
        $identificationParams['site'] = (isset($occurrence['data'][0]['site'])) ? $occurrence['data'][0]['site'] : '';
        $identificationParams['occurrenceId'] = $occurrenceId;
        $identificationParams['geospatialObjectDbId'] = (isset($occurrence['data'][0]['siteDbId'])) ? $occurrence['data'][0]['siteDbId'] : '';

        // include field if not null
        if (isset($occurrence['data'][0]['fieldDbId']) && !empty($occurrence['data'][0]['fieldDbId'])) {
            $identificationParams['fieldDbId'] = $occurrence['data'][0]['fieldDbId'];
        }

        /** store location info in session **/
        Yii::$app->session->set('location-info-upload-planting-arrays', $identificationParams);

        if (isset($occurrence['data'])) {
            $occurrenceSiteArray = [];
            $uploadedOccNames = [];
            foreach ($occurrence['data'] as $key => $value) {
                if (!in_array($value['siteDbId'], $occurrenceSiteArray)) {
                    $occurrenceSiteArray[] = $value['siteDbId'];
                }
                $uploadedOccNames[] = $value['occurrenceName'];

                $locationInfo["locationSteward"] = $locationSteward;
                $locationInfo["locationName"] = $locationName;
                $locationInfo["locationYear"] = $value['experimentYear'];
                $locationInfo["description"] = $locationDescription ?? '';
                $locationInfo['occurrenceName'] = $value["occurrenceName"];

                $locationInfo['seasonDbId'] = $value['experimentSeasonDbId'];
                $locationInfo['siteDbId'] = $value['siteDbId'];
                $locationInfo['site'] = $value['site'];
                $locationInfo['occurrenceDbId'] = $value['occurrenceDbId'];
                $locationInfo['geospatialObjectDbId'] = $value['geospatialObjectDbId'] ?? null;
                $locationArray[] = $locationInfo;
            }
        }
        // store the occurrence name(s) in session
        $occurrenceNames = implode(', ', $uploadedOccNames);
        Yii::$app->session->set('upload-planting-arrays-occurrence-name', $occurrenceNames);

        $locationDataProvider = $this->upload->previewLocationInfo($locationArray);

        // get preview dataprovider
        $plotData = $this->upload->previewPlantingArrays($action, $formattedOccurrenceId, "plots", $useDesignLayout);
        $plotDataProvider = $plotData['dataProvider'];
        $plotCount = $plotData['plotCount'];
        $borderDataProvider = $this->upload->previewPlantingArrays($action, $formattedOccurrenceId, "borders");
        $fillerDataProvider = $this->upload->previewPlantingArrays($action, $formattedOccurrenceId, "fillers");

        return Yii::$app->controller->renderAjax('preview_planting_arrays.php', [
            'plotDataProvider' => $plotDataProvider,
            'totalPlotCount' => $plotCount,
            'borderDataProvider' => $borderDataProvider,
            'fillerDataProvider' => $fillerDataProvider,
            'locationDataProvider' => $locationDataProvider,
            'occurrenceId' => $singleOccurrenceId,
            'action' => $action,
            'allowMultipleSiteNotification' =>
            count($occurrenceSiteArray) > 1 ?
                "There are <b>" . count($occurrenceSiteArray)
                . "</b> unique sites assigned to the location." : ""
        ]);
    }

    /**
     * Save selection whether to use existing location or not
     */
    public function actionSaveIsExLocSelection(){

        $isExLoc = (isset($_POST['isExLoc']) && $_POST['isExLoc'] == 'true') ? true : false;

        Yii::info('Saving selection to map to existing location ' . json_encode(['isExLoc'=>$isExLoc]), __METHOD__);

        $this->upload->saveIsUploadToExistingLoc($isExLoc);
    }

    /**
     * Confirm upload of mapped plots
     *
     * @param Integer $mappedLocationId Mapped Location identifier
     */
    public function actionConfirmUploadPlantingArrays($mappedLocationId = null)
    {

        $locationInfo = Yii::$app->session->get('location-info-upload-planting-arrays');
        $occurrenceIdArray = [];
        $occurrenceArray = [];
        // get occurrence ID

        $occurrenceIds = isset($locationInfo['occurrenceId']) ? $locationInfo['occurrenceId'] : 0;
        $formattedOccurrenceId = $occurrenceIds;
        $occurrenceIdsParam = "equals " . ((is_array($occurrenceIds)) ? implode('| equals', $occurrenceIds) : $occurrenceIds);

        $action = isset($_POST['action']) ? $_POST['action'] : null;
        if (is_array($occurrenceIds)) {
            // add equals condition
            $occurrenceIdsParam = 'equals ' . implode(' | equals ', $occurrenceIds);
            $formattedOccurrenceId = implode('|', $occurrenceIds);
            $singleOccurrenceId = $occurrenceIds[0];
            $occurrenceIdArray = $occurrenceIds;
        } else {
            $singleOccurrenceId = $occurrenceIds;
            $occurrenceIdArray[] = $occurrenceIds;
        }

        $occId = null; // one occurrence ID
        // get current occurrence statuses
        foreach ($occurrenceIdArray as $occurrenceId) {
            $response = $this->occurrence->searchAll(
                [
                    'fields' => 'occurrence.id AS occurrenceDbId | occurrence.occurrence_status AS occurrenceStatus',
                    'occurrenceDbId' => "equals $occurrenceId",
                ],
                '?limit=1',
                false
            );

            $occurrenceArray []= [
                'occurrenceId' => "$occurrenceId",
                'occurrenceStatus' => $response['data'][0]['occurrenceStatus'],
            ];

            if($occId == null){
                $occId = $occurrenceId;
            }
        }

        // whether to use design layout or not
        $useDesignLayout = Yii::$app->session->get('generate-location-use-design-layout');

        // if action is to generate location
        if ($action == 'generateLocation') {
            $layoutFields = '';

            if($useDesignLayout === 'true'){
                $layoutFields = ' | plot.design_x as pa_x |
                    plot.design_y as pa_y |
                    plot.design_x as field_x |
                    plot.design_y as field_y';
            }

            $plotParams = [
                'occurrenceDbId' => $occurrenceIdsParam,
                'fields' => '
                    occurrence.id AS occurrenceDbId |
                    plot.id AS plot_id |
                    plot.plot_number as plotno
                    '. $layoutFields
            ];

            $plotsResult = $this->api->getApiResults(
                'POST',
                'plots-search',
                json_encode($plotParams),
                'sort=plotno',
                true
            );
            if (empty($plotsResult['success'])) {
                $data = [ 'success' => 'false' ];
                return json_encode($data);
            }

            $dataArr = isset($plotsResult['data']) ? $plotsResult['data'] : [];
            $borderArr = [];
            $fillerArr = [];
        } else {

            $action = 'uploadPlantingArrays';

            $config = $this->upload->getUserUploadPlantingArrayTransaction();

            $data = $config['config'];
            $dataArr = [];

            if (isset($data['data'][0]['data']['data'])) {
                $dataArr = $data['data'][0]['data']['data']['plots'];
                $borderArr = $data['data'][0]['data']['data']['borders'];
                $fillerArr = $data['data'][0]['data']['data']['fillers'];
            }
        }

        $isExLoc = Yii::$app->session->get('upload-planting-arrays-is-loc-existing');

        try {
            // the location is existing
            if ($isExLoc == 'true') {
                $createdLocId = Yii::$app->session->get('upload-planting-arrays-location-id');
            } else {

                // check if occurrence is not yet mapped to a location
                $locOccGroupParams['occurrenceDbId'] = 'equals ' . $occId;
                $locOccGroupInfo = $this->locationOccurrenceGroup->searchAll($locOccGroupParams, 'limit=1', false);

                // use mapped location ID
                if (isset($locOccGroupInfo['data'][0]['locationDbId'])) {
                    $createdLocId = $locOccGroupInfo['data'][0]['locationDbId'];
                } else {

                    // BEGIN Create location record
                    // location type is planting area
                    $locationInfo['locationType'] = 'planting area';
                    // location status is draft
                    $locationInfo['locationStatus'] = 'draft';
                    // change steward key
                    $locationInfo['stewardDbId'] = $locationInfo['locationSteward'];
                    unset($locationInfo['locationSteward']);

                    // remove description if empty
                    if (empty($locationInfo['description'])) {
                        unset($locationInfo['description']);
                    }
                    // remove description if empty
                    if (empty($locationInfo['location'])) {
                        unset($locationInfo['location']);
                    }
                    // remove location name if empty
                    if (empty($locationInfo['locationName'])) {
                        unset($locationInfo['locationName']);
                    }

                    unset($locationInfo['occurrenceId']);

                    $locationInfo = array_map('strval', $locationInfo);

                    $createLocParams['records'] = [$locationInfo];

                    // create location record
                    $location = $this->location->create($createLocParams);

                    // get location code
                    if (isset($location['data'][0]['locationDbId'])) {
                        // get location info
                        $createdLocId = $location['data'][0]['locationDbId'];
                        $createdLocParams['locationDbId'] = 'equals ' . $createdLocId;
                        $createdLocInfo = $this->location->searchAll($createdLocParams, 'limit=1', false);

                        if (isset($createdLocInfo['data'][0]['locationCode'])) {
                            $locationCode = $createdLocInfo['data'][0]['locationCode'];

                            // create geospatial object
                            $geoObjParams = [
                                'geospatialObjectCode' => $locationCode,
                                'geospatialObjectName' => $createdLocInfo['data'][0]['locationName'],
                                'geospatialObjectType' => $locationInfo['locationType'],
                                'geospatialObjectSubtype' => 'breeding location'
                            ];

                            // if field_id is set, save as parent ID
                            if (isset($locationInfo['fieldDbId'])) {
                                $geoObjParams['parentGeospatialObjectId'] = $locationInfo['fieldDbId'];

                                // use field ID to get root geospatial object ID
                                $geospatialObjectParams['geospatialObjectDbId'] = 'equals ' . $locationInfo['fieldDbId'];
                            } else {
                                $geoObjParams['parentGeospatialObjectId'] = $locationInfo['siteDbId'];

                                // if no field ID, use site ID to get root geospatial object ID
                                $geospatialObjectParams['geospatialObjectDbId'] = 'equals ' . $locationInfo['siteDbId'];
                            }

                            // get geospatial object
                            $geospatialObject = $this->geospatialObject->searchAll($geospatialObjectParams, 'limit=1', false);
                            // set root geospatial object ID
                            if(!empty($geospatialObject['data'][0]['rootGeospatialObjectDbId'])){
                                $geoObjParams['rootGeospatialObjectId'] = (string) $geospatialObject['data'][0]['rootGeospatialObjectDbId'];
                            }

                            $createGeoObjParams['records'] = [$geoObjParams];
                            $geospatialObject = $this->geospatialObject->create($createGeoObjParams);

                            // Update geospatial object ID of the location
                            if (isset($geospatialObject['data'][0]['geospatialObjectDbId'])) {
                                $updateParams['geospatialObjectDbId'] = (string) $geospatialObject['data'][0]['geospatialObjectDbId'];
                                $this->location->updateOne($createdLocId, $updateParams);
                            }
                        }
                    }
                }
            }

            // if location exists
            if (!empty($createdLocId)) {

                $borderArrCount = (!empty($borderArr)) ? count($borderArr) : 0;
                $fillerArrCount = (!empty($fillerArr)) ? count($fillerArr) : 0;

                // get total number of plots
                $plotCount = count($dataArr) + $borderArrCount + $fillerArrCount;
                // get background processing threshold value
                // temporarily hardcode value while waiting for the updated threshold value
                $size = 0; // Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', $action);

                $requestData = []; // parameters for bakcground processing for plots

                // if plot count is less than threshold and occurrence is already mapped, reset mapping data
                if (!empty($mappedLocationId)) {
                    $this->resetMappingData($dataArr, $createdLocId);
                }

                if(!empty($mappedLocationId)){
                    // if occurrence is already mapped, delete borders and fillers
                    $this->deleteBordersAndFillers($createdLocId);
                }

                // location identifier parameter
                $params['locationDbId'] = (string) $createdLocId;
                // populate planting coordinates and new location ID

                // if less than the threshold, update the records
                if ($plotCount < $size) {
                    foreach ($dataArr as $key => $value) {

                        if (
                            (isset($value['plot_id']) || isset($value['databaseplotid']))
                        ) {

                            // search for the plot record
                            $plotId = isset($value['plot_id']) ? $value['plot_id'] : $value['databaseplotid'];
                            $params['plot_id'] = (string) $plotId;

                            if (
                                isset($value['pa_x']) &&
                                isset($value['pa_y']) &&
                                isset($value['field_x']) &&
                                isset($value['field_y'])
                            ) {
                                $params['paX'] = (string) $value['pa_x'];
                                $params['paY'] = (string) $value['pa_y'];
                                $params['fieldX'] = (string) $value['field_x'];
                                $params['fieldY'] = (string) $value['field_y'];
                            }

                            // update plot record if the plot count is not greater than the threshold value

                            $result = $this->plot->updateOne($plotId, $params);

                            // if there was a problem updating plot record
                            if(isset($result['status']) && $result['status'] != 200){
                                $data = [
                                    'success' => 'false'
                                ];
                                return json_encode($data);
                            }
                        }
                    }
                } else {
                    // build the background processing parameters
                    $requestData = $dataArr;
                }

                // create plots and planting instructions for borders
                if(!empty($borderArr)){
                    foreach ($borderArr as $key => $value) {

                        if (
                            (isset($value['plot_id']) || isset($value['databaseplotid']))
                        ) {

                            $plotId = isset($value['plot_id']) ? $value['plot_id'] : $value['databaseplotid'];

                            if (
                                isset($value['pa_x']) &&
                                isset($value['pa_y']) &&
                                isset($value['field_x']) &&
                                isset($value['field_y'])
                            ) {
                                $params['plotType'] = "border";
                                $params['plotStatus'] = "active";
                                $params['rep'] = "1";
                            }

                            // update plot record if the plot count is not greater than the threshold value
                            if ($plotCount < $size) {
                                // create plot for border
                                $plotResult = $this->plot->create(["records" => [$params]]);

                                $plotDbId = $plotResult['data'][0]["plotDbId"];

                                // update location ID, pa_x, pa_y, field_x, field_y of the plot
                                $this->plot->updateOne($plotDbId, [
                                    "locationDbId" => (string) $createdLocId,
                                    "paX" => (string) $value['pa_x'],
                                    "paY" => (string) $value['pa_y'],
                                    "fieldX" => (string) $value['field_x'],
                                    "fieldY" => (string) $value['field_y']
                                ]);

                                $plantingInstruction["germplasmDbId"] = (string) $value['germplasmDbId'];
                                $plantingInstruction["entryName"] = $value['designation'];
                                $plantingInstruction["entryType"] = "border";
                                $plantingInstruction["entryStatus"] = "active";
                                $plantingInstruction["plotDbId"] = (string) $plotDbId;

                                // create planting instruction record for border
                                $this->plantingInstruction->create(["records" => [$plantingInstruction]]);
                            }
                        }
                    }
                }
                // create plots and planting instructions for fillers
                if(!empty($fillerArr)){
                    foreach ($fillerArr as $key => $value) {

                        if (
                            (isset($value['plot_id']) || isset($value['databaseplotid']))
                        ) {

                            $plotId = isset($value['plot_id']) ? $value['plot_id'] : $value['databaseplotid'];

                            if (
                                isset($value['pa_x']) &&
                                isset($value['pa_y']) &&
                                isset($value['field_x']) &&
                                isset($value['field_y'])
                            ) {
                                $params['plotType'] = "filler";
                                $params['plotStatus'] = "active";
                                $params['rep'] = "1";
                            }
                            if ($plotCount < $size) {
                                // create plot for border
                                $plotResult = $this->plot->create(["records" => [$params]]);

                                $plotDbId = $plotResult['data'][0]["plotDbId"];

                                // update location ID, pa_x, pa_y, field_x, field_y of the plot
                                $this->plot->updateOne($plotDbId, [
                                    "locationDbId" => (string) $createdLocId,
                                    "paX" => (string) $value['pa_x'],
                                    "paY" => (string) $value['pa_y'],
                                    "fieldX" => (string) $value['field_x'],
                                    "fieldY" => (string) $value['field_y']
                                ]);

                                $plantingInstruction["germplasmDbId"] = (string) $value['germplasmDbId'];
                                $plantingInstruction["entryName"] = $value['designation'];
                                $plantingInstruction["entryType"] = "filler";
                                $plantingInstruction["entryStatus"] = "active";
                                $plantingInstruction["plotDbId"] = (string) $plotDbId;

                                // create planting instruction record for border
                                $this->plantingInstruction->create(["records" => [$plantingInstruction]]);
                            }
                        }
                    }
                }

                // if plot count is greater than or equal to the threshold value, use background processing
                if ($plotCount >= $size) {
                    $description = strtolower(preg_replace('/([a-z])([A-Z])/', '$1 $2', $action));

                    // get current program
                    $program = Yii::$app->userprogram->get('abbrev');
                    $userId = Yii::$app->session->get('user.id');

                    // create background job record
                    $backgroundJobParams = [
                        "workerName" => "UploadMappedPlots",
                        "description" => $description,
                        "entity" => "PLOT",
                        "entityDbId" => $singleOccurrenceId,
                        "endpointEntity" => "OCCURRENCE",
                        "application" => "OCCURRENCES",
                        "method" => "POST",
                        "jobStatus" => "IN_QUEUE",
                        "startTime" => "NOW()",
                        "creatorDbId" => $userId
                    ];

                    $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);

                    // if creating transaction is successful
                    if (isset($transaction['status']) && $transaction['status'] == 200) {
                        $occurrenceBgStatus = $description . ' in progress';
                        $occurrenceName = Yii::$app->session->get('upload-planting-arrays-occurrence-name');
                        $message = 'The mapping plots of <b>' . $occurrenceName . '</b> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.';
                        // set background processing info to session
                        Yii::$app->session->set('em-bg-processs-message', $message);
                        Yii::$app->session->set('em-bg-processs', true);

                        if ($action === 'generateLocation') {
                            $workerName = 'GenerateLocation';
                            $records = json_encode([
                                "backgroundJobDbId" => $transaction["data"][0]["backgroundJobDbId"],
                                "plotDbIds" => $requestData,
                                "locationDbId" => (string) $createdLocId,
                                "occurrences" => $occurrenceArray,
                                "bearerToken" => Yii::$app->session->get('user.b4r_api_v3_token'),
                                "url" => getenv('CB_API_V3_URL'),
                                "jobDescription" => $description,
                            ]);
                        } else {
                            $workerName = 'UploadMappedPlots';
                            $records = json_encode([
                                "backgroundJobDbId" => $transaction["data"][0]["backgroundJobDbId"],
                                "updatePlotData" => $requestData,
                                "fillerData" => $fillerArr,
                                "borderData" => $borderArr,
                                "locationDbId" => (string) $createdLocId,
                                "mappedLocationId" => (is_integer($mappedLocationId)) ? (string) $mappedLocationId : null,
                                "occurrences" => $occurrenceArray,
                                "bearerToken" => Yii::$app->session->get('user.b4r_api_v3_token'),
                                "url" => getenv('CB_API_V3_URL'),
                                "jobDescription" => $description,
                            ]);
                        }

                        $host = getenv('CB_RABBITMQ_HOST');
                        $port = getenv('CB_RABBITMQ_PORT');
                        $user = getenv('CB_RABBITMQ_USER');
                        $password = getenv('CB_RABBITMQ_PASSWORD');

                        $connection = new AMQPStreamConnection($host, $port, $user, $password);

                        $channel = $connection->channel();

                        $channel->queue_declare(
                            $workerName, //queue - Queue names may be up to 255 bytes of UTF-8 characters
                            false,  //passive - can use this to check whether an exchange exists without modifying the server state
                            true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                            false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                            false   //auto delete - queue is deleted when last consumer unsubscribes
                        );

                        $msg = new AMQPMessage(
                            $records,
                            array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                        );

                        $channel->basic_publish(
                            $msg,   //message
                            '', //exchange
                            $workerName  //routing key (queue)
                        );
                        $channel->close();
                        $connection->close();
                    } else {
                        $occurrenceBgStatus = $description . ' failed';
                        $notif = 'error';
                        $icon = 'times';
                        $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                        Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
                    }
                    foreach ($occurrenceIdArray as $occurrenceId) {
                        $occParams['occurrenceStatus'] = (string) $occurrenceBgStatus;
                        $this->occurrence->updateOne($occurrenceId, $occParams);
                    }

                    $this->redirect(['/occurrence?program=' . $program . '&OccurrenceSearch[occurrenceDbId]=' . $formattedOccurrenceId]);
                } else {

                    foreach ($occurrenceIdArray as $occurrenceId) {
                        // create record in location occurrence group
                        $locOccGroupParams['records'] = [[
                            'locationDbId' => (string) $createdLocId,
                            'occurrenceDbId' => (string) $occurrenceId
                        ]];

                        $this->locationOccurrenceGroup->create($locOccGroupParams);

                        // update the status of Occurrence and Location to "mapped"
                        $locParams['locationStatus'] = 'mapped';

                        // check if status has packing job status
                        $searchOccParams['occurrenceDbId'] = "equals $occurrenceId";

                        // get current occurrence status
                        $occData = $this->occurrence->searchAll($searchOccParams, 'limit=1', false);
                        $status = isset($occData['data'][0]['occurrenceStatus']) ? $occData['data'][0]['occurrenceStatus'] : '';

                        // explode statuses
                        $statusArr = explode(';', $status);
                        $packingStatus = '';

                        foreach ($statusArr as $key => $value) {
                            // get the packing job status
                            if (strpos($value, 'pack') !== false) {
                                $packingStatus = $value;
                            }
                        }

                        // append the packing job status
                        $occParams['occurrenceStatus'] = (empty($packingStatus)) ? 'mapped' : 'mapped;' . $packingStatus;

                        $this->occurrence->updateOne($occurrenceId, $occParams);

                        $this->location->updateOne($createdLocId, $locParams);

                        $this->occurrence->updateExperimentStatus($occurrenceId);
                    }

                    // update plot count
                    $result = Yii::$app->api->getParsedResponse('POST', 'locations/'.$createdLocId.'/plot-count-generations');

                }
            }
        } catch (\Exception $e) {
            $data = [
                'success' => 'false'
            ];
            return json_encode($data);
        }

        // if Occurrence information is not successfully retrieved
        if(!isset($occurrenceId)){
            $data = [
                'success' => 'false'
            ];
            return json_encode($data);
        }

        $data = [
            'success' => 'true',
            'id' => $occurrenceId
        ];

        return json_encode($data);
    }

    /**
     * Reset mapping data of plots of a Location
     *
     * @param Array $data List of plots
     * @param Integer $locationId Location identifier
     */
    public function resetMappingData($data, $locationId){

        foreach ($data as $key => $value) {

            if (
                (isset($value['plot_id']) || isset($value['databaseplotid']))
            ) {

                // search for the plot record
                $plotId = isset($value['plot_id']) ? $value['plot_id'] : $value['databaseplotid'];
                if (
                    isset($value['pa_x']) &&
                    isset($value['pa_y']) &&
                    isset($value['field_x']) &&
                    isset($value['field_y'])
                ) {
                    $params['paX'] = 'null';
                    $params['paY'] = 'null';
                    $params['fieldX'] = 'null';
                    $params['fieldY'] = 'null';

                    // update plot record
                    $this->plot->updateOne($plotId, $params);
                }
            }
        }
    }

    /**
     * Delete borders and fillers of a Location
     *
     * @param Integer $locationId Location identifier
     */
    public function deleteBordersAndFillers($locationId){
        // retrieve all borders and fillets of location
        $params = [
            'locationDbId' => "equals $locationId",
            'plotType' => 'border|filler',
            'fields' => 'plot.id AS plotDbId|plot.location_id AS locationDbId|plot.plot_type AS plotType'
        ];
        $plots = $this->plot->searchAll($params, null, false);

        $dataArr = isset($plots['data']) ? $plots['data'] : [];

        foreach($dataArr as $value){
            if(isset($value['plotDbId'])){
                // delete plot
                $result = $this->plot->deleteOne($value['plotDbId']);
            }
        }
    }

    /**
     * Get steward and description information by location ID
     *
     * @param integer $locationId location identifier
     * @return array steward and description info
     */
    public function actionGetLocationInfo()
    {
        $locationId = isset($_POST['locationId']) ? $_POST['locationId'] : 0;

        if(!empty($locationId)){
            $param['locationDbId'] = "equals $locationId";
            $location = $this->location->searchAll($param, 'limit=1', false);
        }

        $stewardId = (isset($location['data'][0]['stewardDbId'])) ? $location['data'][0]['stewardDbId'] : 0;
        $steward = (isset($location['data'][0]['steward'])) ? $location['data'][0]['steward'] : 0;
        $description = (isset($location['data'][0]['description'])) ? $location['data'][0]['description'] : '';

        // set steward info to session
        Yii::$app->session->set('upload-planting-arrays-steward-id', $stewardId);
        Yii::$app->session->set('upload-planting-arrays-steward', $steward);
        Yii::$app->session->set('upload-planting-arrays-location-id', $locationId);

        return json_encode([
            'stewardId' => $stewardId,
            'description' => $description
        ]);
    }


    /**
     * Preview upload occurrence data collection
     */
    public function actionPreviewOccurrenceDataCollection ()
    {
        // get preview dataprovider
        $occurrenceData = $this->upload->previewOccurrenceData();

        $occurrenceDataProvider = $occurrenceData['dataProvider'];
        $occurrenceCount = $occurrenceData['occurrenceCount'];
        $configDbId = $occurrenceData['configDbId'];

        // Store occurrence IDs detected in CSV input to session
        Yii::$app->session->set('upload-occurrence-data-collection-config-id', $configDbId);

        return json_encode(Yii::$app->controller->renderAjax('preview_occurrence_data_collection.php', [
            'occurrenceDataProvider' => $occurrenceDataProvider,
            'totalOccurrenceCount' => $occurrenceCount,
        ]));
    }


    /**
     * Confirm upload of occurrence data collection
     *
     */
    public function actionConfirmUploadOccurrenceDataCollection ()
    {
        // Get string of occurrence ID(s)
        $occurrenceIds = Yii::$app->session->get('upload-occurrence-data-collection-occurrence-ids');
        // Get config DB ID
        $configDbId = Yii::$app->session->get('upload-occurrence-data-collection-config-id');
        // Get current program
        $program = Yii::$app->userprogram->get('abbrev');
        // Get user ID
        $userId = Yii::$app->session->get('user.id');
        // Prep occurrence name for setFlash
        $occurrenceName = '';

        $singleOccurrenceId = str_contains($occurrenceIds, '|') ? explode('|', $occurrenceIds)[0] : $occurrenceIds;

        if (!str_contains($occurrenceIds, '|')) {
            // Get occurrence name
            $occurrenceName = $this->api->getApiResults(
                'POST',
                'occurrences-search',
                json_encode([
                    'occurrenceDbId' => "equals $occurrenceIds",
                ]),
                '?limit=1'
            )['data'][0]['occurrenceName'];
        }

        try {
            /* BEGIN Update occurrence status */
            $occurrenceIdsArray = explode('|', $occurrenceIds);

            foreach ($occurrenceIdsArray as $occurrenceDbId) {
                // Get current occurrence status
                $occurrenceStatus = $this->api->getApiResults(
                    'POST',
                    'occurrences-search',
                    json_encode([
                        'fields' => "occurrence.id AS occurrenceDbId | occurrence.occurrence_status AS occurrenceStatus",
                        'occurrenceDbId' => "equals $occurrenceDbId",
                    ]),
                    '?limit=1',
                    false
                )['data'][0]['occurrenceStatus'];

                if (!$occurrenceStatus) {
                    throw new \Exception('There was a problem in retrieving the occurrence status', 500);
                }

                // Append "upload occurrence data in progress" if it does not exist already
                if (!str_contains($occurrenceStatus, self::UPLOAD_OCCURRENCE_DATA_IN_PROGRESS)) {
                    // Change "upload occurrence data failed" to "upload occurrence data in progress"...
                    // ...if the former exists
                    if (str_contains($occurrenceStatus, self::UPLOAD_OCCURRENCE_DATA_FAILED)) {
                        $occurrenceStatus = str_replace(
                            self::UPLOAD_OCCURRENCE_DATA_FAILED,
                            self::UPLOAD_OCCURRENCE_DATA_IN_PROGRESS,
                            $occurrenceStatus
                        );
                    } else {
                        $occurrenceStatus .= ';upload occurrence data in progress';
                    }
                }

                // Update occurrence status
                $this->api->getApiResults(
                    'PUT',
                    "occurrences/$occurrenceDbId",
                    json_encode([
                        'occurrenceStatus' => $occurrenceStatus,
                    ])
                );
            }
            /* END Update occurrence status */

            $this->worker->invoke(
                'UploadOccurrenceDataProcessor',
                'Uploading occurrence data',
                'OCCURRENCE',
                $singleOccurrenceId,
                'OCCURRENCE',
                'OCCURRENCES',
                'GET',
                [
                    'configDbId' => $configDbId,
                    'program' => $program,
                    'userId' => $userId,
                    'processName' => 'upload-occurrence-data',
                    'occurrenceDbIds' => $occurrenceIds,
                ]
            );

            $subject = str_contains($occurrenceIds, '|') ? 'multiple occurrences' : $occurrenceName;

            $message = "The uploaded Occurrence Data records for <strong>$subject</strong>
            are being processed in the background. You may proceed with other tasks.
            You will be notified in this page once done.";

            $content = "<i class='fa fa-info'></i>
                        $message
                        <button data-dismiss='alert' class='close' type='button'>×</button>";

            // Load flash notification content to be displayed upon redirection
            Yii::$app->session->setFlash('info', $content);

            $this->redirect(["/occurrence?program=$program&OccurrenceSearch[occurrenceDbId]=$occurrenceIds"]);
        } catch (\Exception $e) {
            /* BEGIN Update occurrence status */
            $occurrenceIdsArray = explode('|', $occurrenceIds);

            foreach ($occurrenceIdsArray as $occurrenceDbId) {
                // Get current occurrence status
                $occurrenceStatus = $this->api->getApiResults(
                    'POST',
                    'occurrences-search',
                    json_encode([
                        'fields' => "occurrence.id AS occurrenceDbId | occurrence.occurrence_status AS occurrenceStatus",
                        'occurrenceDbId' => "equals $occurrenceDbId",
                    ]),
                    '?limit=1',
                    false
                )['data'][0]['occurrenceStatus'];

                if (!$occurrenceStatus) {
                    throw new \Exception('There was a problem in retrieving the occurrence status', 500);
                }

                // Change "upload occurrence data in progress" to "upload occurrence data failed" if the former exists
                if (str_contains($occurrenceStatus, self::UPLOAD_OCCURRENCE_DATA_IN_PROGRESS)) {
                    $occurrenceStatus = str_replace(
                        self::UPLOAD_OCCURRENCE_DATA_IN_PROGRESS,
                        self::UPLOAD_OCCURRENCE_DATA_FAILED,
                        $occurrenceStatus
                    );
                } else {
                    continue;
                }

                // Update occurrence status
                $this->api->getApiResults(
                    'PUT',
                    "occurrences/$occurrenceDbId",
                    json_encode([
                        'occurrenceStatus' => $occurrenceStatus,
                    ])
                );
            }
            /* END Update occurrence status */

            $data = [
                'success' => 'false'
            ];
            return json_encode($data);
        }

        // if Occurrence information is not successfully retrieved
        if(!isset($occurrenceId)){
            $data = [
                'success' => 'false'
            ];
            return json_encode($data);
        }

        $data = [
            'success' => 'true',
            'id' => $occurrenceIds
        ];

        return json_encode($data);
    }
}
