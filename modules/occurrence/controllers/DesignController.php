<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\controllers;

use app\components\B4RController;
use app\dataproviders\ArrayDataProvider;
use app\models\Api;
use app\models\BackgroundJob;
use app\models\Browser;
use app\models\Config;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\Occurrence;
use app\models\OccurrenceData;
use app\models\Person;
use app\models\PlanTemplate;
use app\models\User;
use app\models\Variable;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

use app\modules\occurrence\models\DesignModel;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\ProtocolModel;
use app\modules\experimentCreation\models\AnalysisModel;
use stdClass;
use Yii;

/**
 * Controller for the design of occurrence
 */
class DesignController extends B4RController
{
    public function __construct ($id, $module,
        protected AnalysisModel $analysisModel,
        protected Api $api,
        protected BackgroundJob $backgroundJob,
        protected Browser $browser,
        protected Config $configuration,
        protected DesignModel $designModel,
        protected Experiment $experiment,
        protected ExperimentData $experimentData,
        protected ExperimentModel $experimentModel,
        protected Occurrence $occurrence,
        protected OccurrenceData $occurrenceData,
        protected Person $person,
        protected PlanTemplate $planTemplate,
        protected ProtocolModel $protocolModel,
        protected User $user,
        protected Variable $variable,
        $config=[]
    )
    {
        parent::__construct($id, $module, $config);
    }
    /**
     * Render update design page
     * 
     * @param string $program Current program
     * @param string $occurrenceDbIds String of selected Occurrence IDs
     * @param boolean $regenerate Flag for whether generate of design is for newly added occurrences or old occurrences
     * 
     * @return string HTML content of Update Occurrences with layout (if applicable)
     */
    public function actionGenerateDesign ($program, $occurrenceDbIds = '', $regenerate = 'true')
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_REGENERATE_DESIGN', 'OCCURRENCES', usage: 'url');
        }

        //data for selected occurrence list
        if(str_contains($occurrenceDbIds, ';')){
            $occurrenceDbIdArray = explode(';', $occurrenceDbIds);
        } else if(str_contains($occurrenceDbIds, '|')){
            $occurrenceDbIdArray = explode('|', $occurrenceDbIds);
        } else {
            $occurrenceDbIdArray = [$occurrenceDbIds];
        }

        $occurrenceAttributesArray = [
            'ESTABLISHMENT',
            'PLANTING_TYPE',
            'PLOT_TYPE',
            'SEEDING_RATE',
            'PLANTING_INSTRUCTIONS',
            'POLLINATION_INSTRUCTIONS',
            'HV_METH_DISC',
            'HARVEST_INSTRUCTIONS',
        ];

        $totalOccurrenceCount = 0;
        $occurrenceDataArray = [];
        sort($occurrenceDbIdArray);

        foreach ($occurrenceDbIdArray as $index => $occurrenceDbId) {
            $response = $this->api->getApiResults(
                'POST',
                "occurrences/$occurrenceDbId/data-table-search"
            );
            
            if ( $response['success'] && count($response['data']) > 0) {
                $data = $response['data'][0];

                // Store occurrrence data
                $occurrenceDataArray[] = $data;
                $totalOccurrenceCount += 1;
            }
        }

        //get the experiment id of the selected occurrences
        $experimentDbId = null;
        if(!empty($occurrenceDataArray)){
            $experimentDbId = $occurrenceDataArray[0]['experimentDbId'];
        }

        //check if it's the old design or created via new design
        $planTemplateArray = $this->planTemplate->searchAll([
            'entityType' => 'experiment',
            'entityDbId' => "equals $experimentDbId"
        ]);
        if($planTemplateArray['totalCount'] > 0){

            $dataProvider = new ArrayDataProvider([
                'allModels' => $occurrenceDataArray,
                'key' => 'occurrenceDbId',
                'restified' => true,
                'totalCount' => $totalOccurrenceCount,
                'sort' => [
                    'attributes' => $occurrenceAttributesArray,
                ],
            ]);

            //retrieve design information from experiment level
            //add details from generation of design
            $params = $this->designModel->getDesignInformation($occurrenceDbIdArray, $experimentDbId, $occurrenceDataArray, $program, $regenerate);
            $params['occDataProvider'] = $dataProvider;
            $params['occurrenceAttributesArray'] = $occurrenceAttributesArray;
            $params['program'] = $program;
            $params['totalOccurrenceCount'] = $totalOccurrenceCount;
            $params['firstOccurrenceName'] = $occurrenceDataArray[0]['occurrence'];
            $params['occurrenceDbIds'] = implode(";", $occurrenceDbIdArray);
            $params['regenerate'] = $regenerate;
            $params['status'] = $occurrenceDataArray[0]['occurrenceStatus'];

            return $this->render('index', $params);
        } else {
            Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> The design used in the selected occurrence/s are not yet supported.'); 
            return $this->redirect(['/occurrence','program'=>$program]); 
        }
    }

     /**
     * Check if the plots are already generated
     *
     * @param $experimentDbId integer record id of the experiment
     * @param $program integer program where the experiment belongs
     * @param $occurrenceDbIds strings list of selected occurrences
     * @param $label text value to be shown in the viewer
     * @param $occurrenceDbId integer value of the current occurrence being shown in the layout
     *
     */
    public function actionCheckDesignResults($experimentDbId, $program, $occurrenceDbIds, $label='plotno',  $occurrenceDbId=null){

        $panelWidth = $_POST['panelWidth'];

        //check if there's existing json input for the experiment design

        // will check if there's already a transaction for the selected occurrences
        $param = ['entityType'=>'occurrence', 'occurrenceApplied'=> "equals ".$occurrenceDbIds]; 
        $occurrenceJsonArray = $this->planTemplate->searchAll($param);
        
        $occurrenceDbIds = explode(";", $occurrenceDbIds);
        $experiment = $this->experiment->getExperiment($experimentDbId);

        if($occurrenceJsonArray['totalCount'] > 0){
            $occurrenceJson = $occurrenceJsonArray['data'][0];
            
            if($occurrenceJson['status'] != 'deletion in progress'){

                //check if uploaded
                $uploaded = false;
                if(isset($occurrenceJson['mandatoryInfo']) && $occurrenceJson != NULL){
                    $jsonArr = json_decode($occurrenceJson['mandatoryInfo'],true);
                    if(isset($jsonArr['totalOccurrences'])){
                        $uploaded = true;
                    }
                }

                $designValue = $occurrenceJson['design'];
                if($uploaded){
                    $designValue = 'Unspecified';
                }

                //Get request status
                $analysisModel = new AnalysisModel();
                $requestStatus = $analysisModel->getRequestStatus($occurrenceJson['requestDbId'],  $designValue);

                //for on going randomization transaction
                if(isset($requestStatus['data']['findRequest']['status']) && in_array($requestStatus['data']['findRequest']['status'], ['new', 'in_progress', 'submitted', 'processing_custom_design', 'upload in progress'])){
                  
                    $statusMessage = 'Randomization in progress';
                    if(strpos($occurrenceJson['status'], 'upload') !== false){
                        $statusMessage = 'Upload in progress';
                    }

                    $htmlData = $this->renderPartial('_design_layout_preview.php',[
                        'experimentId' => $experimentDbId,
                        'data' => [],
                        'startingPlotNo' => 1,
                        'panelWidth' => $panelWidth,
                        'noOfRows' => 0,
                        'noOfCols' => 0,
                        'statusDesign' => 'in progress',
                        'deletePlots' => 'true',
                        'planStatus' => $occurrenceJson['status'],
                        'design'=> $experiment['experimentDesignType']
                    ],true,false);

                    return json_encode([
                        'status' => "design generation in progress",
                        'success'=> 'in progress',
                        'htmlData' => $htmlData,
                        'planStatus' => $occurrenceJson['status'],
                        'deletePlots' => 'true',
                        'message' => $statusMessage
                    ]);
                
                // for successful randomization transaction
                } else if(in_array($occurrenceJson['status'], ['randomized','uploaded']) || (isset($requestStatus['data']['findRequest']['status'] ) && in_array($requestStatus['data']['findRequest']['status'], ['completed']))){

                    $statusStr = 'randomized';

                    //check if randomization or upload transaction
                    if($occurrenceJson['status'] !== 'randomized' || $occurrenceJson['status'] !== 'uploaded'){
                        
                        if(strpos($occurrenceJson['status'], 'upload') !== false){
                            $statusStr = 'uploaded';
                        }
                        //update plan template transaction status
                        $this->planTemplate->updateOne($occurrenceJson['planTemplateDbId'], ['status'=>$statusStr]);
            
                        //Update experiment design
                        //check status
                        $occurrences = $this->occurrence->searchAll(['fields'=>'occurrence.id AS occurrenceDbId|occurrence.occurrence_status AS occurrenceStatus', 'occurrenceDbId' => 'equals '.implode("|equals ", $occurrenceDbIds)]);
                        if($occurrences['totalCount'] > 0){
                            $update = false;
                            foreach($occurrences['data'] as $occurrence){
                                if($occurrence['occurrenceStatus'] == 'design generation in progress' || $occurrence['occurrenceStatus'] == 'draft'){
                                    $update = true;
                                    break;
                                }
                            }
                            if($update){
                                $this->occurrence->updateMany($occurrenceDbIds, ['occurrenceStatus'=>'design generation done']);
                            }
                        }
                        
                    }

                    $occurrences = $this->occurrence->searchAll([
                        'fields'=>'occurrence.experiment_id as experimentDbId|occurrence.occurrence_name as occurrenceName|occurrence.id AS occurrenceDbId',
                        'occurrenceDbId' => "equals ".implode('|equals ', $occurrenceDbIds)
                    ],'sort=occurrenceName:ASC');
                    
                    $occurrencesList = array();
                    if($occurrences['totalCount'] > 0){

                        // create the occurrence list for layout viewing per occurrence
                        $occurrencesList = $occurrences['data'];
                        if($occurrenceDbId == NULL){
                            $occurrenceDbId = $occurrences['data'][0]['occurrenceDbId'];
                            $occurrenceName = $occurrences['data'][0]['occurrenceName'];
                        } else {
                            $occurrenceIdx = array_search($occurrenceDbId, array_column($occurrencesList, 'occurrenceDbId'));
                            $occurrenceName = $occurrencesList[$occurrenceIdx]['occurrenceName'];
                        }

                        //checked plot records and view layout
                        $plotRecords = $this->occurrence->searchAllPlots($occurrenceDbId, null,'sort=plotNumber:ASC');

                        if($plotRecords['totalCount'] > 0){

                            //Save FIRST_PLOT_POSITION_VIEW
                            $varAbbrev = 'FIRST_PLOT_POSITION_VIEW';
                            $variableInfo = $this->variable->searchAll(["abbrev"=>"equals $varAbbrev"]);
                            $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
                            
                            $expData = $this->experimentData->getExperimentData($experimentDbId, ["abbrev" => "equals "."$varAbbrev"]);
                            
                            if(isset($expData) && !empty($expData)){
                                if($expData[0]['dataValue'] == "Top Left"){
                                    $flipped = true;
                                } else {
                                    $flipped = false;
                                }   
                            } else {   //create variable

                                // if no saved variable in experiment data, get default first plot position in config
                                $config = $this->config->getConfigByAbbrev('FIRST_PLOT_POSITION_VIEW_DEFAULT');

                                $varValue = (isset($config['value']) && !empty($config['value'])) ? $config['value'] : 'Top Left';

                                $dataRequestBodyParam[] = [
                                    'variableDbId' => "$variableDbId",
                                    'dataValue' => "$varValue"
                                ];

                                $this->experiment->createExperimentData($experimentDbId,$dataRequestBodyParam);
                                $flipped = ($varValue == 'Bottom Left') ? false : true;
                            }
                            //get the layout information
                            $layoutArray = $this->experimentModel->generateLayout($occurrenceDbId, $experimentDbId);
                            
                            $htmlData = $this->renderPartial('_design_layout_preview.php',[
                                'experimentId' => $experimentDbId,
                                'data' => $layoutArray['dataArrays'],
                                'label'=> $label,
                                'startingPlotNo' => 1,
                                'panelWidth' => $panelWidth,
                                'noOfRows' => $layoutArray['maxRows'] == NULL ? 0:$layoutArray['maxRows'],
                                'noOfCols' => $layoutArray['maxCols'] == NULL ? 0:$layoutArray['maxCols'],
                                'statusDesign' => 'completed',
                                'deletePlots' => 'true',
                                'planStatus' => $statusStr,
                                'flipped' => $flipped,
                                'occurrencesList' => $occurrencesList,
                                'occurrenceName' => $occurrenceName,
                                'occurrenceDbId' => $occurrenceDbId,
                                'xAxis' => $layoutArray['xAxis'],
                                'yAxis' => $layoutArray['yAxis'],
                                'maxBlk' => $layoutArray['maxBlk'],
                                'design'=> $experiment['experimentDesignType']
                            ],true,false);
                
                            return json_encode([
                                'status' => "design generation done",
                                'success'=> 'success',
                                'htmlData' => $htmlData,
                                'deletePlots' => 'true',
                                'planStatus' => $statusStr,
                                'message' => 'Successfully '.$statusStr.'.'
                            ]);
                            
                        }
                    }

                // if transaction failed or has errors
                } else if(isset($requestStatus['errors']) || in_array($requestStatus['data']['findRequest']['status'], ['failed', 'expired'])){
                    $statusStr = 'randomization failed';
                    $statusMessage = 'Randomization Failed. Please try again.';
                    if(strpos($occurrenceJson['status'], 'upload') !== false){
                        $statusStr = 'upload failed';
                        $statusMessage = 'Upload Failed. Please try again.';
                    }

                    //update plan template transaction to failed
                    $this->planTemplate->updateOne($occurrenceJson['planTemplateDbId'], ['status'=>$statusStr]);

                    $status = explode(';',$experiment['experimentStatus']);
            
                    //check status
                    $entryStatus = "created";
                    $experimentStatus = $entryStatus;
                            
                    return json_encode([
                        'status' => "design deletion done",
                        'success'=> 'failed',
                        'htmlData' => [],
                        'planStatus' => $statusStr,
                        'deletePlots' => 'false',
                        'message' => $statusMessage
                    ]);
                }else { // for undefined errors from randomization request resource
                    $statusStr = 'randomization failed';
                    $statusMessage = 'Randomization Failed. Please try again.';
                    if(strpos($occurrenceJson['status'], 'upload') !== false){
                        $statusStr = 'upload failed';
                        $statusMessage = 'Upload Failed. Please try again.';
                    }
                    $updateTemplate = $this->planTemplate->updateOne($occurrenceJson['planTemplateDbId'], ['status'=>$statusStr]);

                    //check status
                    $entryStatus = "created";
                    $experimentStatus = $entryStatus;
                        
                    return json_encode([
                        'status' => "design deletion done",
                        'success'=> 'failed',
                        'htmlData' => [],
                        'planStatus' => $statusStr,
                        'deletePlots' => 'false',
                        'message' => $statusMessage
                    ]);
                }
            } else {
                /**** will enable in deletion task ***/
                //retrieve process background
                $bgAddtlParams = ["entityDbId"=>"equals ".$occurrenceDbIds[0],"entity"=>"equals PLOT", "endpointEntity"=>'equals PLOT',"application"=>"equals OCCURRENCES"];

                $response = $this->backgroundJob->searchAll($bgAddtlParams, 'sort=backgroundJobDbId:DESC');

                if (isset($response['success']) && $response['success'] == 200) {
                    if(strtolower($response['data'][0]['jobStatus']) == 'done'){

                        //update plan status
                        $planStatus = 'draft';
                        $updateTemplate = $this->planTemplate->updateOne($occurrenceJson['planTemplateDbId'], ['status'=>$planStatus]);

                        $update = $this->occurrence->updateMany($occurrenceDbIds, ['occurrenceStatus'=>'design deletion done']);

                        $params = [
                            "jobIsSeen" => "true",
                            "jobRemarks" => "REFLECTED"
                        ];
                        $updateBgProcess = $this->backgroundJob->updateOne($response['data'][0]['backgroundJobDbId'], $params);
                        $success = 'delete successful';
                        $deletePlots = 'false';
                        $status = "design deletion done";
                        $message = 'Plots successfully deleted. Please proceed in submitting your request.';
                    } else if(strtolower($response['data'][0]['jobStatus']) == 'failed'){

                        $planStatus = 'draft';
                        $updateTemplate = $this->planTemplate->updateOne($occurrenceJson['planTemplateDbId'], ['status'=>$planStatus]);

                        $success = 'deletion failed';
                        $deletePlots = 'true';
                        $message = 'Deletion of plot records failed. Please try again.';
                        $status = "created";

                        $params = [
                            "jobIsSeen" => "true",
                            "jobRemarks" => "REFLECTED"
                        ];
                        $updateBgProcess = $this->backgroundJob->updateOne($response['data'][0]['backgroundJobDbId'], $params);
                    } else {
                        $success = 'deletion in progress';
                        $deletePlots = 'true';
                        $message = '';
                        $planStatus = 'deletion in progress';
                        $status = "design deletion in progress";
                    }
                } else {
                    $planStatus = 'draft';
                    $updateTemplate = $this->planTemplate->updateOne($occurrenceJson['planTemplateDbId'], ['status'=>$planStatus]);

                    $success = 'deletion failed';
                    $deletePlots = 'true';
                    $message = 'Deletion of plot records failed. Please try again.';
                    $status = "created";
                }
                    
                //Update experiment status
                
                $htmlData = $this->renderPartial('_design_layout_preview.php',[
                    'experimentId' => $experimentDbId,
                    'data' => [],
                    'startingPlotNo' => 1,
                    'panelWidth' => $panelWidth,
                    'noOfRows' => 0,
                    'noOfCols' => 0,
                    'statusDesign' => $success,
                    'planStatus' =>$planStatus,
                    'deletePlots' => $deletePlots,
                    'design'=> $experiment['experimentDesignType']
                ],true,false);
                
                return json_encode([
                    'status' => $status,
                    'success'=> $success,
                    'htmlData' => $htmlData,
                    'planStatus' =>$planStatus,
                    'deletePlots' => $deletePlots,
                    'message' => $message
                ]);
            }
        }
    }

        /**
     * Generates json input format for analysis module
     *
     * @param string $program name of the current program
     * @param integer $experimentDbId record id of the experiment
     *
     */
    public function actionGenerateJsonInputAnalytics($experimentDbId, $program, $occurrenceDbIds){

        if(isset($_POST)){

            $valuesArray = $_POST;
            //check if there's existing json input for the experiment design
           
            $occurrenceIds = explode(";",$occurrenceDbIds);

            $occurrenceJsonArray = $this->planTemplate->searchAll(['entityType'=>'occurrence', 'occurrenceApplied'=> "equals ".$occurrenceDbIds]);

            $occurrenceJson = $occurrenceJsonArray['data'][0];

            $plantTemplateDbId = $occurrenceJson['planTemplateDbId'];

            $experiment = $this->experiment->getExperiment($experimentDbId);
            $jsonArray = json_decode($occurrenceJson['mandatoryInfo'], true);
            
            $jsonInput = array();

            $parameterRandArray = $jsonArray['design_ui']['randomization'];
            $indexRandIds = array_column($parameterRandArray, 'code');

            $parameterLayoutArray = $jsonArray['design_ui']['layout'];
            $indexLayoutIds = array_column($parameterLayoutArray, 'code');
            
            $occurrencesCount = 0;
            foreach($valuesArray['ExperimentDesign'] as $key=>$value){
              
                if($key != 'DESIGN'){
                    $indexRand = array_search($key,$indexRandIds);
                    $indexLayout = array_search($key,$indexLayoutIds);
                    
                    if($value == ""){
                        $value = 0;
                    }
                    if($indexRand !== false && isset($parameterRandArray[$indexRand])){
                      
                        $valuesArray['ExperimentDesign'][$key] = $value;
                        $parameterRandArray[$indexRand]['value'] = $value;
                    }
                    if($indexLayout !== false && isset($parameterLayoutArray[$indexLayout])){
                      
                        $valuesArray['ExperimentDesign'][$key] = $value;
                        $parameterLayoutArray[$indexLayout]['value'] = $value;
                    }
                    if($key == 'nTrial' || $key == 'nFarm'){
                        $occurrencesCount = intval($value);
                        $parameterRandArray[$indexRand]['value'] = intval($value);
                    }
                    $jsonInput[$key] = $value;
                } else {

                    $design = $value;
                }
            }

            if(isset($valuesArray['uiGroups'])){
                $jsonArray['design_ui']['groups'] = $valuesArray['uiGroups'];
            }
            $jsonArray['design_ui']['randomization'] = $parameterRandArray;
            $jsonArray['design_ui']['layout'] = $parameterLayoutArray;

            $generatedInput = $this->experimentModel->createJsonInputAnalytics($experimentDbId, $valuesArray['ExperimentDesign'], $design, $jsonInput, $jsonArray, $occurrencesCount, $occurrenceIds);

            //create record in experiment data
            $occurrenceJsonTemp = array();
            $occurrenceJsonTemp['randomizationInputFile'] = json_encode($generatedInput, JSON_UNESCAPED_SLASHES);
            $occurrenceJsonTemp['mandatoryInfo'] = json_encode($jsonArray);

            // Update experiment design
            $this->planTemplate->updateOne($plantTemplateDbId, $occurrenceJsonTemp);

            $analysisModel = new AnalysisModel();
            $submitRequest = $analysisModel->submitRequest($generatedInput);
            //get totalPlots
            
            if($submitRequest['success']){

                $occurrenceJsonTemp['status'] = 'randomization in progress';
                $occurrenceJsonTemp['randomizatDataResults'] = '{}';
                $occurrenceJsonTemp['randomizationResults'] = '{}';

                //store request id
                $occurrenceJsonTemp['requestDbId'] = $submitRequest['requestId'];

                $occurrenceJsonTemp['notes'] = '{}';

                // Update experiment design
                $this->planTemplate->updateOne($plantTemplateDbId, $occurrenceJsonTemp);
                
                
                //Update occurrence status
                $this->occurrence->updateMany($occurrenceIds, ['occurrenceStatus'=>'design generation in progress']);
                return json_encode('success');
                
            } else {
                $occurrenceJsonTemp['status'] = 'randomization failed';
                $occurrenceJsonTemp['randomizatDataResults'] = '{}';
                $occurrenceJsonTemp['randomizationResults'] = '{}';

                // Update experiment design
                $this->planTemplate->updateOne($plantTemplateDbId, $occurrenceJsonTemp);
                return json_encode('failed');
            }
            
        }
    }


    /**
     * Create background transaction and trigger worker for creation of occurrence related data
     * @param string $program Current program of user
     * @param int $experimentDbId Experiment identifier
     * @param string $occurrenceDbIds list Occurrence identifier
     * @param string $regenerate flag if regeneration or creation of new occurrences
     * 
     */
    public function actionSaveDesignChanges($experimentDbId, $program, $occurrenceDbIds, $regenerate = "true"){
        
        $occurrenceIds = explode(";", $occurrenceDbIds);
        $newOccurrences =  $this->occurrence->searchAll(['occurrenceDbId'=>"equals ".implode("|equals ",$occurrenceIds)]);
        $newOccurrenceIds = $occurrenceIds;
       
        // get user ID
        $userId = Yii::$app->session->get('user.id');

        $formattedOccurrenceId = implode('|', $occurrenceIds);
        $backgroundJobParams = [
            "workerName" => "CreateOccurrenceRecords",
            "description" => $formattedOccurrenceId,
            "entity" => "OCCURRENCE",
            "entityDbId" => $occurrenceIds[0],
            "endpointEntity" => "OCCURRENCE",
            "application" => "OCCURRENCES",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId
        ];
        
        $redirectUrl = ['/occurrence?program='.$program.'&OccurrenceSearch[occurrenceDbId]='.$formattedOccurrenceId];

        //create background transactions
        try {
            $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
        } catch (\Exception $e) {
            \ChromePhp::log($e);
            $notif = 'error';
            $icon = 'times';
            $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);         
        }

        // if creating transaction is successful
        try {
            if (isset($transaction['status']) && $transaction['status'] == 200) {
                $message = "The records are being generated in the background. You may proceed with other tasks. You will be notified in this page once done.";
                // set background processing info to session
                Yii::$app->session->set('em-bg-processs-message', $message);
                Yii::$app->session->set('em-bg-processs', true);
    
                $host = getenv('CB_RABBITMQ_HOST');
                $port = getenv('CB_RABBITMQ_PORT');
                $user = getenv('CB_RABBITMQ_USER');
                $password = getenv('CB_RABBITMQ_PASSWORD');

                $connection = new AMQPStreamConnection($host, $port, $user, $password);
    
                $channel = $connection->channel();
    
                $channel->queue_declare(
                    'CreateOccurrenceRecords', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                    false,  //passive - can use this to check whether an exchange exists without modifying the server state
                    true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                    false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                    false   //auto delete - queue is deleted when last consumer unsubscribes
                );
    
                $msg = new AMQPMessage(
                    json_encode([
                        'tokenObject' => [
                            'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                            'refreshToken' => ''
                        ],
                        'backgroundJobId' => $transaction["data"][0]["backgroundJobDbId"],
                        'description' => 'Creation of occurrence related records',
                        'newOccurrenceIds' => $occurrenceIds,
                        'oldOccurrenceId' => null,
                        'designGeneration' => true,
                        'regenerate' => $regenerate,
                        'experimentId' => $experimentDbId,
                        'personDbId' => $userId
                    ]),
                    array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                );
    
                $channel->basic_publish(
                    $msg,   //message 
                    '', //exchange
                    'CreateOccurrenceRecords'  //routing key (queue)
                );
                $channel->close();
                $connection->close();
                
                //update occurrence status to creation in progress
                $upd = $this->occurrence->updateMany($occurrenceIds, ['occurrenceStatus' => 'creation in progress']);

                //populate occurrence information
                $experimentRecord = $this->experiment->getOne($experimentDbId)['data'];

                //update experiment status to creation in progress
                $this->experiment->updateOne($experimentDbId, ["experimentStatus"=>"creation in progress"]);

                // if $regenerate is false (new occurrence for upload)
                if($regenerate == "false"){
                    $this->occurrence->populateOccurrenceInfo($occurrenceIds, $experimentRecord['programDbId'], $userId);
                    //Save the protocol in the occurrence data
                    $protocolData = $this->protocolModel->getExperimentProtocols($experimentDbId, $newOccurrences['data']);
                    if(count($protocolData) > 0){
                        $record['records'] = $protocolData;
                        $create = $this->occurrenceData->create($record);
                    }
                }

                // effectively exits this function (will trigger a (false) error response in the calling ajax)
                Yii::$app->response->redirect($redirectUrl);
            } else {
                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
            }
        } catch (\Exception $e) {
            \ChromePhp::log('e:', $e);
        }
    }
}   