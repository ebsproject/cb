<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\controllers;

use app\components\B4RController;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\models\ITerminalTransaction;
use app\interfaces\modules\dataCollection\models\ITransaction;
use app\models\Api;
use app\models\BackgroundJob;
use app\models\Browser;
use app\models\Config;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\FormulaParameters;
use app\models\Location;
use app\models\LocationOccurrenceGroup;
use app\models\Occurrence;
use app\models\OccurrenceData;
use app\models\OccurrenceTrait;
use app\models\Person;
use app\models\PlanTemplate;
use app\models\User;
use app\models\Variable;
use app\modules\experimentCreation\models\FormModel;
use app\modules\experimentCreation\models\ProtocolModel;
use app\modules\experimentCreation\models\CopyExperimentModel;
use app\modules\occurrence\models\Manage;
use app\modules\occurrence\models\ManageOccurrence;
use app\modules\occurrence\models\OccurrenceCollaboratorPermissionSearch;
use app\modules\occurrence\models\OccurrencePermissionSearch;
use app\modules\account\models\ListModel;
use app\modules\dataCollection\models\Upload;
use app\modules\harvestManager\models\OccurrenceDetailsModel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use stdClass;
use Yii;
use yii\caching\TagDependency;

/**
 * Controller for the manage occurrence
 */
class ManageController extends B4RController
{

    public function __construct ($id, $module,
        protected Api $api,
        protected BackgroundJob $backgroundJob,
        protected Browser $browser,
        protected Config $configuration,
        protected CopyExperimentModel $copyExperimentModel,
        protected Experiment $experiment,
        protected ExperimentData $experimentData,
        protected FormModel $formModel,
        protected FormulaParameters $formulaParameters,
        protected ITerminalTransaction $terminalTransaction,
        protected ITransaction $transaction,
        protected ListModel $listModel,
        protected Location $location,
        protected LocationOccurrenceGroup $locationOccurrenceGroup,
        protected Manage $manage,
        protected ManageOccurrence $manageOccurrence,
        protected Occurrence $occurrence,
        protected OccurrenceCollaboratorPermissionSearch $occurrenceCollaboratorPermissionSearch,
        protected OccurrenceData $occurrenceData,
        protected OccurrenceDetailsModel $occurrenceDetailsModel,
        protected OccurrencePermissionSearch $occurrencePermissionSearch,
        protected OccurrenceTrait $occurrenceTrait,
        protected Person $person,
        protected PlanTemplate $planTemplate,
        protected ProtocolModel $protocolModel,
        protected Upload $upload,
        protected User $user,
        protected Variable $variable,
        protected TagDependency $tagDependency,
        $config=[]
    )
    {
        parent::__construct($id, $module, $config);
    }
    /**
     * Save occurrence basic and data information
     *
     * @return boolean $result if successful or not
     */
    public function actionBasicInfo(){
        // get values in form
        $formData = Yii::$app->request->post();

        // occurrence ID
        $id = (isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : null;
        // form input values for Occurrence
        $formData = (isset($_POST['Occurrence']) && !empty($_POST['Occurrence'])) ? $_POST['Occurrence'] : [];

        // save basic and occurrence data information
        $result = $this->occurrence->saveBasicData($id, $formData);
        // return true if successful in saving, else return false
        return $result;
    }

    /**
     * Commit an occurrence and location record
     */
    public function actionCommitOccurrence(){

        // occurrence ID
        $id = (isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : null;

        // update occurrence and location status
        $locationParams['locationStatus'] = 'planted';

        // check if status has packing job status
        $searchOccParams['occurrenceDbId'] = "equals $id";

        // get current occurrence status
        $occData = $this->occurrence->searchAll($searchOccParams, 'limit=1', false);
        if (empty($occData['success'])) {
            return json_encode([
                'success' => false,
            ]);
        }
        $status = $occData['data'][0]['occurrenceStatus'] ?? '';

        // explode statuses
        $statusArr = explode(';', $status);
        $additionalStatus = '';
        
        foreach ($statusArr as $key => $value) {
            // get the packing job and and trait data collected statuses
            if ((strpos($value, 'pack') !== false && $value !== 'packed') || strpos($value, 'trait data collected') !== false){
                $additionalStatus .= ";$value";
            }
        }

        // append the packing job status
        $occurrenceParams['occurrenceStatus'] = "planted$additionalStatus";

        $this->occurrence->updateOne($id, $occurrenceParams);
        // get location of the occurrence
        $searchParams['occurrenceDbId'] = "equals $id";
        $locOccGroup = $this->locationOccurrenceGroup->searchAll($searchParams, 'limit=1', false);

        if(isset($locOccGroup['data'][0]['locationDbId'])) {
            $locationId = $locOccGroup['data'][0]['locationDbId'];

            $this->location->updateOne($locationId, $locationParams);
            $this->occurrence->updateExperimentStatus($id);
        }

        return json_encode([
            'success' => true,
        ]);
    }

    /**
     * Render update Occurrence Protocols page
     * 
     * @param string $program Current program
     * @param string $templateOccurrenceDbId Occurrence ID for template tabs
     * @param string $occurrenceDbIdsString String of selected Occurrence IDs
     * @param string $dataProcessAbbrevsString Data process abbreviation identifier
     * @param boolean $streamlined Flag for whether occurrence protocols page is in streamlined form or not
     * 
     * @return string HTML content of Update Occurrences with layout (if applicable)
     */
    public function actionOccurrenceProtocols ($program, $templateOccurrenceDbId = '', $occurrenceDbIdsString = '', $dataProcessAbbrevsString = '', $streamlined = false)
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        // Get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($occurrenceDbIdsString);
        $accessData = isset($occurrenceObj['accessData']) ? $occurrenceObj['accessData'] : null;
        $creatorDbId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;
        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess(
                'EXPERIMENT_MANAGER_UPDATE_OCC_PROTOCOL',
                'OCCURRENCES',
                $creatorDbId,
                $accessData,
                'write',
                usage: 'url'
            );
        }

        $program = isset($_POST['program']) ? $_POST['program'] : $program;
        $templateOccurrenceDbId = isset($_POST['templateOccurrenceDbId']) ? $_POST['templateOccurrenceDbId'] : $templateOccurrenceDbId;
        $occurrenceDbIdsString = isset($_POST['occurrenceDbIdsString']) ? $_POST['occurrenceDbIdsString'] : $occurrenceDbIdsString;
        $occurrenceDbIdArray = explode('|', $occurrenceDbIdsString);

        // retrieve occurrences where user has write permission to
        $writePermissionResponse = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'fields' => 'occurrence.id AS occurrenceDbId',
                'occurrenceDbId' => "equals " . implode('|equals ', $occurrenceDbIdArray),
            ]),
            'permission=write',
            true
        );

        // if no write permission to all, return empty occurrence IDs
        if($writePermissionResponse['totalCount'] == 0){
            $occurrenceDbIdArray = [];
            $occurrenceDbIdsString = '';
        } else {
            // else, retrieve only occurrences where user has write permission to
            $occurrenceDbIdArray = [];
            foreach($writePermissionResponse['data'] as $data){
                $occurrenceDbIdArray[] = $data['occurrenceDbId'];
            }
            $occurrenceDbIdsString = implode('|', $occurrenceDbIdArray);
        }

        $hasMultipleSelectedOccurrences = (count($occurrenceDbIdArray) > 1) ? true : false;
        $occurrenceDataArray = [];
        $totalOccurrenceCount = 0;
        $occurrenceAttributesArray = [
            'ESTABLISHMENT',
            'PLANTING_TYPE',
            'PLOT_TYPE',
            'SEEDING_RATE',
            'PLANTING_INSTRUCTIONS',
            'POLLINATION_INSTRUCTIONS',
            'HV_METH_DISC',
            'HARVEST_INSTRUCTIONS',
        ];
        $plantingProtocolVariablesColumns = [];
        $dataProcessAbbrevsString = isset($_POST['dataProcessAbbrevsString']) ? $_POST['dataProcessAbbrevsString'] : $dataProcessAbbrevsString;
        $dataProcessAbbrevsArray = explode('|', $dataProcessAbbrevsString);

        foreach ($occurrenceDbIdArray as $index => $occurrenceDbId) {
            $response = $this->api->getApiResults(
                'POST',
                "occurrences/$occurrenceDbId/data-table-search"
            );

            if (
                $response['success'] &&
                isset($response['data'])
            ) {
                $data = $response['data'][0];
                $attributes = array_keys($data);

                // Store occurrence data attributes
                foreach ($attributes as $index => $attribute) {
                    if (!in_array($attribute, $occurrenceAttributesArray)) {
                        array_push($occurrenceAttributesArray, $attribute);
                        if (gettype($data[$attribute]) === 'array') {
                            $protocolDbId = $data[$attribute]['protocolDbId'];
    
                            if ($protocolDbId === null) {
                                continue;
                            }
    
                            $response = $this->api->getApiResults(
                                'POST',
                                'protocols-search',
                                json_encode([
                                    'fields' => 'protocol.id AS protocolDbId | protocol.protocol_type AS protocolType',
                                    'protocolDbId' => "equals $protocolDbId",
                                ]),
                                'limit=1&sort=protocolDbId:asc',
                                true
                            );

                            $protocolType = $response['data'][0]['protocolType'];
    
                            if ($protocolType === 'planting') {
                                $plantingProtocolVariablesColumns[] = [
                                    'attribute' => $attribute,
                                    'value' => function ($model) use ($attribute) {
                                        return $model[$attribute]['dataValue'] ?? '';
                                    }
                                ];
                            }
                        }
                    }
                }

                // Store occurrrence data
                $occurrenceDataArray[] = $data;
                $totalOccurrenceCount += 1;
            }
        }

        $occurrenceAttributesColumns = [
            [
                'attribute' => 'ESTABLISHMENT',
                'label' => 'CROP ESTABLISHMENT',
                'value' => function ($model) {
                    return $model['ESTABLISHMENT']['dataValue'] ?? '';
                },
                'visible' => false,
            ],
            [
                'attribute' => 'PLANTING_TYPE',
                'value' => function ($model) {
                    return $model['PLANTING_TYPE']['dataValue'] ?? '';
                },
                'visible' => false,
            ],
            [
                'attribute' => 'PLOT_TYPE',
                'value' => function ($model) {
                    return $model['PLOT_TYPE']['dataValue'] ?? '';
                },
                'visible' => false,
            ],
            [
                'attribute' => 'SEEDING_RATE',
                'label' => 'SEEDING DENSITY',
                'value' => function ($model) {
                    return $model['SEEDING_RATE']['dataValue'] ?? '';
                },
                'visible' => false,
            ],
            [
                'attribute' => 'PLANTING_INSTRUCTIONS',
                'value' => function ($model) {
                    return $model['PLANTING_INSTRUCTIONS']['dataValue'] ?? '';
                },
                'visible' => false,
            ],
            [
                'attribute' => 'POLLINATION_INSTRUCTIONS',
                'value' => function ($model) {
                    return $model['POLLINATION_INSTRUCTIONS']['dataValue'] ?? '';
                },
                'visible' => false,
            ],
            [
                'attribute' => 'HV_METH_DISC',
                'label' => 'HARVEST METHOD',
                'value' => function ($model) {
                    return $model['HV_METH_DISC']['dataValue'] ?? '';
                },
                'visible' => false,
            ],
            [
                'attribute' => 'HARVEST_INSTRUCTIONS',
                'value' => function ($model) {
                    return $model['HARVEST_INSTRUCTIONS']['dataValue'] ?? '';
                },
                'visible' => false,
            ],
        ];

        foreach ($occurrenceDataArray as $occurrenceData) {
            foreach ($occurrenceData as $attribute => $value) {
                if ($attribute === 'ESTABLISHMENT') {
                    $occurrenceAttributesColumns[0]['visible'] = true;
                } else if ($attribute === 'PLANTING_TYPE') {
                    $occurrenceAttributesColumns[1]['visible'] = true;
                } else if ($attribute === 'PLOT_TYPE') {
                    $occurrenceAttributesColumns[2]['visible'] = true;
                } else if ($attribute === 'SEEDING_RATE') {
                    $occurrenceAttributesColumns[3]['visible'] = true;
                } else if ($attribute === 'PLANTING_INSTRUCTIONS') {
                    $occurrenceAttributesColumns[4]['visible'] = true;
                } else if ($attribute === 'POLLINATION_INSTRUCTIONS') {
                    $occurrenceAttributesColumns[5]['visible'] = true;
                } else if ($attribute === 'HV_METH_DISC') {
                    $occurrenceAttributesColumns[6]['visible'] = true;
                } else if ($attribute === 'HARVEST_INSTRUCTIONS') {
                    $occurrenceAttributesColumns[7]['visible'] = true;
                }
            }
        }

		$dataProvider = new ArrayDataProvider([
			'allModels' => $occurrenceDataArray,
			'key' => 'occurrenceDbId',
			'restified' => true,
			'totalCount' => $totalOccurrenceCount,
			'sort' => [
				'attributes' => $occurrenceAttributesArray,
			],
		]);

        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if (!$isAdmin && strtoupper($role) == 'COLLABORATOR')
            $role = 'COLLABORATOR';
        else if ($isAdmin)
            $role = 'ADMIN';
        else
            $role = 'DATA_OWNER';

        $params = $this->retrieveProtocols($program, $templateOccurrenceDbId, $hasMultipleSelectedOccurrences, $dataProcessAbbrevsArray);
        $params['templateOccurrenceDbId'] = $templateOccurrenceDbId;
        $params['occurrenceDbIdsString'] = $occurrenceDbIdsString;
        $params['hasMultipleSelectedOccurrences'] = $hasMultipleSelectedOccurrences;
        $params['dataProvider'] = $dataProvider;
        $params['occurrenceAttributesArray'] = $occurrenceAttributesArray;
        $params['plantingProtocolVariablesColumns'] = $plantingProtocolVariablesColumns;
        $params['streamlined'] = $streamlined;
        $params['isAdmin'] = $isAdmin;
        $params['role'] = $role;
        $params['occurrenceAttributesColumns'] = $occurrenceAttributesColumns;

        if ($streamlined)
            return $params;
        else
            return $this->render('occurrence_protocols', $params);
    }

    /**
     * Render generate Code Patterns page
     * 
     * @param string $program Current program
     * @param string $entity Entry or Plot identifier
     * @param string $level Experiment or Occurrence identifier
     * @param string $experimentDbIdsString string of select Experiment IDs 
     * @param string $occurrenceDbIdsString string of select Occurrence IDs
     * @param boolean $streamlined Flag for whether Code Patterns page is in streamlined form or not
     * 
     * @return string HTML content of Generate Code Patterns page with layout (if applicable)
     */
    public function actionCodePatterns ($program, $entity, $level, $experimentDbIdsString = '', $occurrenceDbIdsString = '', $streamlined = false)
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess(
                'UPDATE_OCCURRENCE',
                'OCCURRENCES',
                usage: 'url'
            );
        }

        $selectedIds = [];
        $experimentNames = [];


        if ($experimentDbIdsString) {
            // Get occurrence IDs of each parent experiment
            $experimentDbIdsArray = explode('|', $experimentDbIdsString);
            $experimentDbIdsArray = array_unique($experimentDbIdsArray);

            foreach ($experimentDbIdsArray as $key => $value) {
                $response = $this->api->getApiResults(
                    'POST',
                    'occurrences-search',
                    json_encode([
                        'experimentDbId' => "equals $value",
                        'fields' => 'occurrence.experiment_id AS experimentDbId | occurrence.id AS occurrenceDbId | experiment.experiment_name AS experiment',
                    ]),
                    'permission=write',
                    true
                );

                if($response['totalCount'] > 0){

                    $data = $response['data'];
                    $subArray = [];

                    foreach ($data as $k => $v) {
                        $subArray["$value"] []= "{$v['occurrenceDbId']}";
                    }

                    array_push($selectedIds, $subArray);

                    array_push($experimentNames, $data[0]['experiment'] ?? '');
                }
            }
        } else if ($occurrenceDbIdsString) {

            // Get experiment IDs of each occurrence
            $occurrenceDbIdsArray = explode('|', $occurrenceDbIdsString);

            foreach ($occurrenceDbIdsArray as $key => $value) {

                $response = $this->api->getApiResults(
                    'POST',
                    'occurrences-search',
                    json_encode([
                        'occurrenceDbId' => "equals $value",
                        'fields' => 'occurrence.experiment_id AS experimentDbId | occurrence.id AS occurrenceDbId',
                    ]),
                    '?limit=1&permission=write',
                    true
                );

                if($response['totalCount'] > 0){

                    $data = $response['data'];

                    $experimentDbId = (isset($data[0]['experimentDbId'])) ? "{$data[0]['experimentDbId']}" : '';

                    $selectedIds[0][$experimentDbId] []= "$value";
                }
            }
        }

        $params = [
            'program' => $program,
            'entity' => $entity,
            'level' => $level,
            'ids' => $selectedIds,
            'encodedIds' => json_encode($selectedIds),
            'experimentNames' => $experimentNames,
            'streamlined' => $streamlined,
        ];

        if ($streamlined)
            return $params;
        else
            return $this->render('code_patterns', $params);
    }

    /**
     * Generate code pattern
     *
     * @param string $program Current program
     * @param string $entity Entry or Plot identifier
     * @param string $level Experiment or Occurrence identifier
     * @param string $experimentDbIdsString string of select Experiment IDs
     * @param string $occurrenceDbIdsString string of select Occurrence IDs
     *
     * @return string HTML content of Generate Code Patterns page with layout (if applicable)
     */
    public function actionGenerateCodePattern ($level)
    {
        $entity = $_POST['entity'] ?? '';

        $userId = Yii::$app->session->get('user.id');
        $variableAbbrev = strtoupper("{$entity}_CODE_PATTERN");

        $startingSequenceNumberArray = json_decode( $_POST['startingSequenceNumberArray']);
        $numberOfDigits = $_POST['numberOfDigits'];
        $affixValues = (array) json_decode($_POST['affixValues']);
        // This converts this PHP object into an associative array (easier to use)
        $ids = (array) json_decode($_POST['ids']);
        $pattern = [];
        $prevAffixType = '';
        $i = 0;
        // This is all the info of the accepted variables in code generation
        $entityArray = [
            'EXPERIMENT_CODE' => [
                'value' => 'EXPERIMENT_CODE',
                'entity_table' => 'experiment.experiment',
                'entity_column' => 'experiment.experiment_code',
                'entity_reference_table' => 'experiment.experiment_id',
                'entity_reference_column' => 'id',
                'is_variable' => true,
            ],
            'EXPERIMENT_NAME' => [
                'value' => 'EXPERIMENT_NAME',
                'entity_table' => 'experiment.experiment',
                'entity_column' => 'experiment.experiment_name',
                'entity_reference_table' => 'experiment.experiment_id',
                'entity_reference_column' => 'id',
                'is_variable' => true,
            ],
            'EXPERIMENT_YEAR' => [
                'value' => 'EXPERIMENT_YEAR',
                'entity_table' => 'experiment.experiment',
                'entity_column' => 'experiment.experiment_year',
                'entity_reference_table' => 'experiment.experiment_id',
                'entity_reference_column' => 'id',
                'is_variable' => true,
            ],
            'SEASON_CODE' => [
                'value' => 'SEASON_CODE',
                'entity_table' => 'tenant.season',
                'entity_column' => 'season.season_code',
                'entity_reference_table' => 'experiment.season_id',
                'entity_reference_column' => 'id',
                'is_variable' => true,
            ],
            'OCCURRENCE_CODE' => [
                'value' => 'OCCURRENCE_CODE',
                'entity_table' => 'experiment.occurrence',
                'entity_column' => 'occurrence.occurrence_code',
                'entity_reference_table' => 'plot.occurrence_id',
                'entity_reference_column' => 'id',
                'is_variable' => true,
            ],
            'OCCURRENCE_NAME' => [
                'value' => 'OCCURRENCE_NAME',
                'entity_table' => 'experiment.occurrence',
                'entity_column' => 'occurrence.occurrence_name',
                'entity_reference_table' => 'plot.occurrence_id',
                'entity_reference_column' => 'id',
                'is_variable' => true,
            ],
            'ENTRY_NUMBER' => [
                'value' => 'ENTRY_NUMBER',
                'entity_table' => 'experiment.planting_instruction',
                'entity_column' => 'planting_instruction.entry_number',
                'entity_reference_table' => 'plot.id',
                'entity_reference_column' => 'plot_id',
                'is_variable' => true,
            ],
            'ENTRY_CODE' => [
                'value' => 'ENTRY_CODE',
                'entity_table' => 'experiment.planting_instruction',
                'entity_column' => 'planting_instruction.entry_code',
                'entity_reference_table' => 'plot.id',
                'entity_reference_column' => 'plot_id',
                'is_variable' => true,
            ],
            'ENTRY_CLASS' => [
                'value' => 'ENTRY_CLASS',
                'entity_table' => 'experiment.planting_instruction',
                'entity_column' => 'planting_instruction.entry_class',
                'entity_reference_table' => 'plot.id',
                'entity_reference_column' => 'plot_id',
                'is_variable' => true,
            ],
            'ENTRY_ROLE' => [
                'value' => 'ENTRY_ROLE',
                'entity_table' => 'experiment.planting_instruction',
                'entity_column' => 'planting_instruction.entry_role',
                'entity_reference_table' => 'plot.id',
                'entity_reference_column' => 'plot_id',
                'is_variable' => true,
            ],
            'ENTRY_REP' => [
                'value' => 'ENTRY_REP',
                'entity_table' => 'experiment.plot',
                'entity_column' => 'plot.rep',
                'is_variable' => true,
            ],
            'PLOT_NUMBER' => [
                'value' => 'PLOT_NUMBER',
                'entity_table' => 'experiment.plot',
                'entity_column' => 'plot.plot_number',
                'is_variable' => true,
            ],
            'SITE' => [
                'value' => 'SITE',
                'entity_table' => 'place.geospatial_object',
                'entity_column' => 'geospatial_object.geospatial_object_name',
                'entity_reference_table' => 'occurrence.site_id',
                'entity_reference_column' => 'id',
                'is_variable' => true,
            ],
            'SITE_CODE' => [
                'value' => 'SITE_CODE',
                'entity_table' => 'place.geospatial_object',
                'entity_column' => 'geospatial_object.geospatial_object_code',
                'entity_reference_table' => 'occurrence.site_id',
                'entity_reference_column' => 'id',
                'is_variable' => true,
            ],
        ];

        // If user has inputted '0' to Leading Zeroes field, remove zero_padding and digits props
        $counterInfo = ($numberOfDigits) ? [
            'value' => 'COUNTER',
            'zero_padding' => 'leading',
            'digits' => "$numberOfDigits",
        ] : [
            'value' => 'COUNTER',
        ];

        // Create code pattern here
        foreach ($affixValues as $key => $value) {
            $value = (array) $value;
            $affixType = array_key_first($value);
            $affixValue = $value[$affixType];

            if ($affixType == 'freeText') {
                array_push($pattern, [
                    'value' => $affixValue,
                    'is_variable' => false,
                ]);
            } elseif ($affixType == 'variable') {
                array_push($pattern, $entityArray[$affixValue]);
            } elseif ($affixType == 'sequence') {
                array_push($pattern, $counterInfo);
            }
        }

        $pattern = [ 'pattern' => $pattern ];
        $pattern = json_encode($pattern);

        foreach ($ids as $key => $value) {
            foreach ($value as $v) {
                // Check if code pattern exists
                $occurrenceDataResponse = $this->api->getApiResults(
                    'POST',
                    "occurrence-data-search",
                    json_encode([
                        'occurrenceDbId' => "equals $v",
                        'variableAbbrev' => "equals $variableAbbrev",
                    ]),
                    'limit=1'
                );

                
                // Remove all previous code patterns
                foreach ($occurrenceDataResponse['data'] as $occurrenceData) {
                    $this->api->getApiResults(
                        'DELETE',
                        "occurrence-data/{$occurrenceData['occurrenceDataDbId']}"
                    );
                }

                // Retrieve correct variableDbId for ENTRY_CODE_PATTERN or PLOT_CODE_PATTERN
                $variableResponse = $this->api->getApiResults(
                    'POST',
                    "variables-search",
                    json_encode([
                        'abbrev' => "equals $variableAbbrev",
                    ]),
                    '?limit=1'
                );

                $variableDbId = $variableResponse['data'][0]['variableDbId'];

                // Insert newly created code pattern to Occurrence Data
                $this->api->getApiResults(
                    'POST',
                    "occurrence-data",
                    json_encode([
                        'records' => [
                            [
                                'occurrenceDbId' => "$v",
                                'variableDbId' => "$variableDbId",
                                'dataValue' => $pattern,
                            ]
                        ]
                    ])
                );

                // Generate code pattern
                $response = $this->api->getApiResults(
                    'POST',
                    "experiments/$key/$entity-code-generations",
                    json_encode([
                        "$v" => [
                            'offset' => "{$startingSequenceNumberArray[$i]}",
                        ]
                    ])
                );

                if (str_contains($response['message'], 'value too long for type character varying')) {
                    return 0;
                }

                // Clear cached data for View Occurrence-level Plots data browser...
                // ...given an Occurrence ID $v
                $this->tagDependency->invalidate(
                    Yii::$app->cache,
                    "dynagrid-em-occurrence-plot-$v"
                );

                $i += 1;
            }
        }

        return 1;
    }

    /**
     * Retrieve protocols of an Experiment
     *
     * @param string $program Program identifier
     * @param integer $id Unique Occurrence identifier
     * @param boolean $hasMultipleSelectedOccurrences Flag to determine if user selected more than 1 Occurrence
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function retrieveProtocols ($program, $id, $hasMultipleSelectedOccurrences = false, $dataProcessAbbrevsArray = [])
    {
        $experimentDbId = '';
        $experimentType = '';
        $occurrenceName = '';
        $dataProcessDbId = '';
        $dataProcessAbbrev = '';

        // Retrieve experimentDbId and dataProcessDbId
        $response = Yii::$app->api->getParsedResponse(
            'POST',
            'occurrences-search',
            json_encode([
                'occurrenceDbId' => (string) $id,
            ]),
            'limit=1',
            true
        );

        if (
            $response['success'] &&
            isset($response['data']) &&
            !empty($response['data'])
        ) {
            $experimentDbId = $response['data'][0]['experimentDbId'];
            $experimentType = $response['data'][0]['experimentType'];
            $occurrenceName = $response['data'][0]['occurrenceName'];
            $dataProcessDbId = $response['data'][0]['dataProcessDbId'];
            $dataProcessAbbrev = $response['data'][0]['dataProcessAbbrev'];
        }

        if ($hasMultipleSelectedOccurrences) {
            // Render these input Protocols and their fields based on the Experiment Types of the selected Occurrences
            $protocolParams = [
                'viewArr' => [],
            ];

            if (
                in_array('BREEDING_TRIAL_DATA_PROCESS', $dataProcessAbbrevsArray) ||
                in_array('GENERATION_NURSERY_DATA_PROCESS', $dataProcessAbbrevsArray) ||
                in_array('OBSERVATION_DATA_PROCESS', $dataProcessAbbrevsArray) ||
                in_array('CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS', $dataProcessAbbrevsArray) ||
                in_array('INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS', $dataProcessAbbrevsArray) ||
                in_array('AGRONOMIC_TRIAL_DATA_PROCESS', $dataProcessAbbrevsArray)
            ) {
                array_push($protocolParams['viewArr'], [
                    'display_name' => "Planting",
                    'action_id' => "planting-protocols",
                    'item_icon' => "fa fa-pagelines"
                ]);
            }

            if (
                in_array('CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS', $dataProcessAbbrevsArray) ||
                in_array('CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS', $dataProcessAbbrevsArray) ||
                in_array('INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS', $dataProcessAbbrevsArray) ||
                in_array('GENERATION_NURSERY_DATA_PROCESS', $dataProcessAbbrevsArray)
            ) {
                array_push($protocolParams['viewArr'], [
                    'display_name' => "Pollination",
                    'action_id' => "pollination-protocol",
                    'item_icon' => "fa fa-crosshairs"
                ]);
            }

            // Harvest Protocols tab is always rendered regardless of Experiment Types
            array_push($protocolParams['viewArr'], [
                'display_name' => "Harvest",
                'action_id' => "harvest-protocol",
                'item_icon' => "fa fa-pied-piper"
            ]);
        } else {
            // Render these input Protocols and their fields based on the Experiment Type
            // Retrieve protocol parameters and config
            $protocolParams = $this->specifyProtocols(
                $experimentDbId,
                $experimentType,
                $dataProcessDbId,
                $dataProcessAbbrev,
                $program
            );
        }

        $protocolParams = array_merge($protocolParams, [
            'occurrenceDbId' => $id,
            'occurrenceName' => $occurrenceName,
            'experimentDbId' => $experimentDbId,
            'dataProcessDbId' => $dataProcessDbId,
            'program' => $program,
        ]);

        return $protocolParams;
    }

    /**
     * Specify Protocols of an Experiment
     *
     * @param integer $experimentDbId Unique Experiment identifier
     * @param string $experimentType Experiment type identifier
     * @param integer $dataProcessDbId Unique Data Process identifier
     * @param string $dataProcessAbbrev Data Process abbreviation identifier
     * @param string $program Program identifier
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function specifyProtocols ($experimentDbId, $experimentType, $dataProcessDbId, $dataProcessAbbrev, $program)
    {
        if (
            (
                $dataProcessAbbrev !== 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS' ||
                (
                    stripos($experimentType, 'nursery') === false &&
                    !empty($entryCountAssignedToBlock)
                )
            ) ||
            stripos($experimentType, 'trial') === false
        ) {
            $activities = $this->formModel->prepare(
                'specify-protocols',
                $program,
                $experimentDbId,
                $dataProcessDbId
            );

            $index = array_search(
                $activities['active'] ?? '',
                array_column($activities['children'] ?? [],'url')
            );
         
            $activityChildren = $activities['children'][$index]['children'] ?? [];
           
            $viewArr = [];
            
            if (!empty($activityChildren) && sizeof($activityChildren) > 0) {
               foreach ($activityChildren as $value) {
                    $viewArr []= [
                        'display_name' => $value['display_name'],
                        'action_id' => $value['action_id'],
                        'item_icon' => $value['item_icon']
                    ];
               }
            }

            return [
                'viewArr' => $viewArr,
                'protocolChildren' => $activityChildren,
            ];
        }
    }

    /**
     * Render manage occurrence level data of Experiment
     *
     * @param String $program Current program of user
     * @param Integer $experimentId Experiment identifier
     * @param Array $varAbbrevs List of abbrevs of added variables
     * @param Integer $occurrenceId Occurrence identifier
     * @param String @updateOccurrence flag for ui
     *
     * @return view file for manage occurrence level data
     */
    public function actionOccurrences (
        $program,
        $experimentId,
        $varAbbrevs = [],
        $occurrenceId = null,
        $updateOccurrence = false,
        $shouldLimitRowsToOne = null
    )
    {
        // get experiment info
        $experimentObj = $this->experiment->getOne($experimentId);
        $experiment = isset($experimentObj['data']['experimentName']) ? $experimentObj['data']['experimentName'] : '';
        $experimentType = isset($experimentObj['data']['experimentType']) ?
            $experimentObj['data']['experimentType'] : '';
        $experimentProgram = isset($experimentObj['data']['programCode']) ?
            $experimentObj['data']['programCode'] : '';
        $dataProcessAbbrev = isset($experimentObj['data']['dataProcessAbbrev']) ?
            $experimentObj['data']['dataProcessAbbrev'] : '';

        // get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($occurrenceId);
        $accessData = isset($occurrenceObj['accessData']) ? $occurrenceObj['accessData'] : null;
        $creatorId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;

        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if (!$isAdmin && strtoupper($role) == 'COLLABORATOR') {
            $role = 'COLLABORATOR';
        } elseif ($isAdmin) {
            $role = 'ADMIN';
        } else {
            $role = 'DATA_OWNER';
        }
       
        // If this page is accessed without specifying a value for this flag...
        // ...retrieve cached flag value, else set to 'true'
        if (!$shouldLimitRowsToOne) {
            $shouldLimitRowsToOne = Yii::$app->session->get('should-limit-rows-to-one') ?? 'true';
        }

        Yii::$app->session->set('should-limit-rows-to-one', "$shouldLimitRowsToOne");

        $limit = (strtolower($shouldLimitRowsToOne) == 'true') ? 'limit to 1' : '';

        if (!$updateOccurrence) {
            if (
                $isAdmin ||
                Yii::$app->access->renderAccess(
                    'UPDATE_OCCURRENCE',
                    'OCCURRENCES',
                    $creatorId,
                    $accessData,
                    'write',
                    'url'
                )
            ) {
                // get config
                $config = $this->getConfig('occurrence', $program, $dataProcessAbbrev);

                // get Occurrences data provider
                $params = Yii::$app->request->queryParams;

                $data = $this->manageOccurrence->search(
                    $experimentId,
                    $params,
                    $program,
                    'experiment_manager',
                    $limit
                );

                // data variables
                $dataVars = $data['dataVars'];

                // list of abbrevs of added variables
                $varAbbrevs = empty($varAbbrevs) ? [] : json_decode($varAbbrevs);
                // get all variable abbrevs from the config
                $configVarAbrevs = array_column($config, 'variable_abbrev');
                // combine data variables and selected variable abbrevs to exclude
                $excludedVars = array_merge($dataVars, array_merge($varAbbrevs, $configVarAbrevs));

                $dataProvider = $data['dataProvider'];
                $searchModel = $this->manageOccurrence;

                // build form columns
                $columns = $this->buildFormColumns($config, array_unique(array_merge($dataVars, $varAbbrevs)));

                return $this->render('occurrences',
                    compact(
                        'program',
                        'experimentId',
                        'experiment',
                        'dataProvider',
                        'columns',
                        'searchModel',
                        'excludedVars',
                        'varAbbrevs',
                        'occurrenceId',
                        'isAdmin',
                        'role',
                        'experimentType',
                        'experimentProgram',
                        'shouldLimitRowsToOne',
                        'creatorId',
                        'accessData'
                    )
                );
            }
        } else {
            $configAbbrev = '';
            $planTemplate = null;
            $paramSet = 0;
            if($experimentType == 'Breeding Trial' || $experimentType == 'Agronomic Trial' ){
                
                $configAbbrev = 'BREEDING_TRIAL_PLACE_ACT_VAL';
                if($experimentType == 'Agronomic Trial'){
                    $configAbbrev = 'AGRONOMIC_TRIAL_PLACE_ACT_VAL';
                }
                $planTemplateArray = $this->planTemplate->searchAll([
                    'entityType' => 'parameter_sets',
                    'entityDbId' => "equals $experimentId"
                ]);
                if($planTemplateArray['totalCount'] > 0){
                    $planTemplate = $planTemplateArray['data'][0];
                    $paramSet = 1; //parameter set was used
                } else {
                    $planTemplateArray = $this->planTemplate->searchAll([
                        'entityType' => 'experiment',
                        'entityDbId' => "equals $experimentId"
                    ]);

                    if($planTemplateArray['totalCount'] > 0){
                        $planTemplate = $planTemplateArray['data'][0];
                    }
                }
            } elseif($experimentType == 'Observation'){
                $configAbbrev = 'OBSERVATION_PLACE_ACT_VAL';
            }
            $configObj = $this->configuration->getConfigByAbbrev($configAbbrev);
            $config = isset($configObj['Values']) ? $configObj['Values'] : [];

            // get Occurrences data provider
            $params = Yii::$app->request->queryParams;
            $data = $this->manageOccurrence->search(
                $experimentId,
                $params,
                $program,
                null,
                'update occurrence',
                $limit
            );
            // data variables
            $dataVars = $data['dataVars'];

            // list of abbrevs of added variables
            $varAbbrevs = empty($varAbbrevs) ? [] : json_decode($varAbbrevs);
            // get all variable abbrevs from the config
            // check only default columns for occurrence creation
            $configVarAbrevs = array_column($config, 'variable_abbrev');
            // combine data variables and selected variable abbrevs to exclude
            $excludedVars = array_merge($dataVars, array_merge($varAbbrevs, $configVarAbrevs));

            $dataProvider = $data['dataProvider'];
            $totalOccurrenceCount = $dataProvider->totalCount;
            $searchModel = $this->manageOccurrence;

            // build form columns
            $columns = $this->buildFormColumns(
                $config,
                array_unique(array_merge($dataVars, $varAbbrevs)),
                'update occurrence'
            );

            // get IDs of DRAFT occurrences
            $drafts = $this->occurrence->searchAll([
                'fields'=>'occurrence.id as occurrenceDbId|occurrence.occurrence_status as occurrenceStatus|occurrence.experiment_id AS experimentDbId',
                'occurrenceStatus' => "equals draft",
                'experimentDbId' => "equals ".$experimentId
            ],'sort=occurrenceDbId:ASC');

            //get occurrences with incomplete required info
            $incDrafts = $this->occurrence->searchAll([
                'fields'=>'occurrence.id as occurrenceDbId|occurrence.occurrence_status as occurrenceStatus|occurrence.experiment_id AS experimentDbId|occurrence.site_id AS siteDbId',
                'occurrenceStatus' => "equals draft",
                'experimentDbId' => "equals ".$experimentId,
                'siteDbId' => "is null"
            ],'sort=occurrenceDbId:ASC');


            $draftOccurrenceIds = null;
            $incDraftOccCount = $incDrafts['totalCount'] ?? 0;
            $draftOccCount = $drafts['totalCount'] ?? 0;
            if($drafts['totalCount'] > 0){
                $draftOccurrenceIds = array_column($drafts['data'], 'occurrenceDbId');
                $draftOccurrenceIds = implode(";", $draftOccurrenceIds);

            }

            return $this->render('add_occurrences',
                compact(
                    'program',
                    'experimentId',
                    'experiment',
                    'dataProvider',
                    'columns',
                    'searchModel',
                    'excludedVars',
                    'varAbbrevs',
                    'occurrenceId',
                    'isAdmin',
                    'role',
                    'experimentType',
                    'totalOccurrenceCount',
                    'planTemplate',
                    'draftOccurrenceIds',
                    'incDraftOccCount',
                    'draftOccCount',
                    'shouldLimitRowsToOne',
                    'creatorId',
                    'accessData',
                    'paramSet'
                )
            );
        }
    }

    /**
     * Get configurtion by data level and program
     *
     * @param String $dataLevel Data level whether Experiment, Occurrence, etc.
     * @param String $program Program abbrev identifier
     * @param String $datProcessAbbrev Experiment data process abbrev identifier
     *
     * @return Array $config Configuration of form
     */
    public function getConfig($dataLevel, $program, $dataProcessAbbrev)
    {
        $config = [];

        if ($dataLevel == 'occurrence') {
            // check if there is a config for program
            $abbrev = 'MANAGE_OCCURRENCE_DATA_' . $program;

            // Get Program-level Manage Occurrence Data config
            $response = $this->configuration->searchAll(
                [ 'configurationAbbrev' => "equals $abbrev" ],
                'limit=1',
                false
            );
            $configObj = ($response['success'] && !empty($response['data'])) ? $response['data'][0]['configurationValue'] : [];

            // if there is a config for the program
            if(!empty($configObj)){
                // check if there is a config for the experiment type
                $configArr = isset($configObj['Values']) ? $configObj['Values'] : [];
                foreach ($configArr as $value) {
                    if (isset($value[$dataProcessAbbrev])) {
                        $config = $value[$dataProcessAbbrev];
                        break;
                    }

                    if (isset($value['BREEDING_TRIAL_DATA_PROCESS'])) {
                        $config = $value['BREEDING_TRIAL_DATA_PROCESS'];
                        break;
                    }

                    if (isset($value['AGRONOMIC_TRIAL_DATA_PROCESS'])) {
                        $config = $value['AGRONOMIC_TRIAL_DATA_PROCESS'];
                        break;
                    }
                }

                if (empty($config)) {
                    $config = $configArr;
                }
            }
        }

        // if there is no config for the program and experiment type, get default
        if(empty($config)){
            $abbrev = 'MANAGE_OCCURRENCE_DATA_DEFAULT';

            // Get Global-level/Default Manage Occurrence Data config
            $response = $this->configuration->searchAll(
                [ 'configurationAbbrev' => "equals $abbrev" ],
                'limit=1',
                false
            );
            $configObj = ($response['success'] && !empty($response['data'])) ? $response['data'][0]['configurationValue'] : [];

            $config = isset($configObj['Values']) ? $configObj['Values'] : [];
        }

        return $config;
    }

    /**
     * Build form columns
     *
     * @param Array $config Configuration of form
     * @param Array $dataVars Data variables
     * @param String $usage feature where the data will be used
     * @return Array Form columns
     */
    public function buildFormColumns($config, $dataVars, $usage=null){
        // get data variables not in configuration
        $varAbbrevs = array_column($config, 'variable_abbrev');
        $dataVars = array_diff($dataVars, $varAbbrevs);

        // transform list of variables to form config
        $dataVarsConfig = $this->browser->buildVarsAsFormConfig($dataVars);

        // merge default config to dynamic data variables
        $varsConfig = array_merge($config, $dataVarsConfig);

        // build form columns
        $columns = $this->browser->buildFormColumns($varsConfig, 'occurrence', $usage);

        return $columns;
    }

    /**
     * Save tabular form row value
     */
    public function actionSaveTabularFormRowValue(){

        if(isset($_POST['column']) && isset($_POST['dataValue']) && isset($_POST['entityDbId'])){
            extract($_POST);

            $model = isset($entity) ? $entity : 'occurrence';

            // if contact person, save Person name instead of ID
            if($column == 'CONTCT_PERSON_CONT' && !empty($dataValue)){
                $dataValue = $this->person->getContact($dataValue)['personName'];
            }

            // If edited column is location
            if($column == 'LOCATION') {
                $locationDbId = Yii::$app->api->getParsedResponse(
                    'POST',
                    'occurrences-search',
                    json_encode([
                        'fields' => "occurrence.id AS occurrenceDbId | location.id AS locationDbId",
                        'occurrenceDbId' => "equals $entityDbId"])
                )['data']['0']['locationDbId'] ?? '';

                // If retrieved location is empty, return false
                if(empty($locationDbId)){
                    return false;
                }

                // Update location
                Yii::$app->api->getParsedResponse(
                    'PUT',
                    'locations/'.$locationDbId,
                    json_encode([
                        'locationName' => "$dataValue"
                    ])
                );

                // Return success
                return true;
            }

            $column = ($column == 'EXPERIMENT_STEWARD') ? 'STEWARD' : $column;

            $formattedColumn = $this->browser->formatToCamelCase($column);

            // check if field is a column or data value
            $searchParams[$model.'DbId'] = "equals $entityDbId";
            $entityObj = $this->$model->searchAll($searchParams, 'limit=1', false);

            $isColumn = false;

            // if fields are foreign keys, set isColumn equals true
            $idColumnsArr = ['field', 'site', 'pipeline', 'project', 'stage', 'steward', 'season'];
            $formattedIdColumnsArr = [];
            if(in_array($formattedColumn, $idColumnsArr)){
                $isColumn = true;
                $formattedColumn .= 'DbId';
                $formattedIdColumnsArr[] = $formattedColumn;
            }

            if(isset($entityObj['data']) && !empty($entityObj['data']) && !$isColumn){
                if(array_key_exists($formattedColumn, $entityObj['data'][0])){
                    $isColumn = true;
                }
            }

            // if variable is a field, save in occurrence table
            if($isColumn){
                // if column is site, set fieldDbId to null
                if($formattedColumn == 'siteDbId'){
                    $params['fieldDbId'] = 'null';
                }

                // if column is stage, parse the value to get only the stage ID
                if($formattedColumn == 'stageDbId'){
                    $tempDataValue = explode('-', $dataValue);
                    $dataValue = end($tempDataValue);
                }

                $params[$formattedColumn] = (empty($dataValue) && in_array($formattedColumn, $formattedIdColumnsArr)) ? 'null' : (string) $dataValue;

                $this->$model->updateOne($entityDbId, $params);
            } else{
                // else, save in data table
                $dataParams['variableDbId'] = "$variableDbId";
                // get entity data ID
                $entityDataDbId = null;
                if($model == 'occurrence'){
                    $dataParams['occurrenceDbId'] = "$entityDbId";
                    $dataSearchParams = $dataParams;

                    $searchParams = [
                        'variableDbId' => "equals $variableDbId",
                        'occurrenceDbId' => "equals $entityDbId"
                    ];
                    $occurrenceDataObj = $this->occurrenceData->searchAll($searchParams, 'limit=1', false);

                    if(isset($occurrenceDataObj['data']) && !empty($occurrenceDataObj['data'])){
                        $entityDataDbId = $occurrenceDataObj['data'][0]['occurrenceDataDbId'];
                    }
                } else {
                    // search params for create
                    $dataSearchParams = [
                        'variableDbId' => "$variableDbId",
                        'experimentDbId' => "$entityDbId",
                    ];

                    // get data ID
                    $experimentDataObj = Yii::$app->api->getParsedResponse(
                        'POST',
                        'experiments/'.$entityDbId.'/data-search?limit=1',
                        json_encode($dataParams)
                    );

                    if(isset($experimentDataObj['data'][0]['data']) && !empty($experimentDataObj['data'][0]['data'])){
                        $entityDataDbId = $experimentDataObj['data'][0]['data'][0]['experimentDataDbId'];
                    }
                }

                $modelData = $model . 'Data';

                // Update variable data
                if(!empty($entityDataDbId)){
                    // if value is empty, void previous record
                    if(empty($dataValue)){
                        $this->$modelData->deleteOne($entityDataDbId);
                    }else {
                        if ($model == 'occurrence') { // Occurrence Data
                            // Retrieve old info of the variable to be updated
                            $response = $this->api->getApiResults(
                                'POST',
                                'occurrence-data-search',
                                json_encode([
                                    'occurrenceDataDbId' => "equals $entityDataDbId",
                                ]),
                                'limit=1',
                                false
                            );

                            // Temporarily store old info
                            $oldVar = $response['data'][0];

                            // Delete the old variable
                            $this->$modelData->deleteOne($entityDataDbId);

                            // Insert the old variable info + updated data_value
                            // Prepare the attributes needed for create()
                            $createDataParams['records'][] = [
                                'occurrenceDbId' => $entityDbId,
                                'variableDbId' => $oldVar['variableDbId'],
                                'dataValue' => $dataValue,
                            ];

                            // Add in protocolDbId if it has a non-null value
                            if ($oldVar['protocolDbId']) {
                                $createDataParams['records'][0]['protocolDbId'] = $oldVar['protocolDbId'];
                            }
                            
                            // Call POST /occurrence-data
                            $this->$modelData->create($createDataParams);
                        } else { // Experiment Data
                            // Retrieve old info of the variable to be updated
                            $response = $this->api->getApiResults(
                                'POST',
                                "experiments/$entityDbId/data-search",
                                json_encode([
                                    'experimentDataDbId' => "equals $entityDataDbId",
                                ]),
                                'limit=1',
                                false
                            );

                            // Temporarily store old info
                            $oldVar = $response['data'][0]['data'][0];

                            // Delete the old variable
                            $this->$modelData->deleteOne($entityDataDbId);

                            // Insert the old variable info + updated data_value
                            // Prepare the attributes needed for create()
                            $createDataParams['records'][] = [
                                'variableDbId' => "{$oldVar['variableDbId']}",
                                'dataValue' => $dataValue,
                                'dataQcCode' => $oldVar['dataQcCode'],
                            ];

                            // Add in protocolDbId if it has a non-null value
                            if ($oldVar['protocolDbId']) {
                                $createDataParams['records'][0]['protocolDbId'] = "{$oldVar['protocolDbId']}";
                            }

                            // Call POST /experiments/:id/data
                            $this->api->getApiResults(
                                'POST',
                                "experiments/$entityDbId/data",
                                json_encode($createDataParams),
                            );
                        }
                    }

                } else if (!empty($dataValue)){
                    $dataValueArr['dataValue'] = (string) $dataValue;
                    // if no record exists, create new record
                    $createDataParams['records'][] = array_merge($dataSearchParams, $dataValueArr);

                    $this->$modelData->create($createDataParams);
                }
            }

            return true;
        }
    }

    /**
     * Update open transactions that are using the occurrence data
     */
    public function actionUpdateTransaction() {

        extract($_POST);
        $transactionList = $this->terminalTransaction->searchAll([
            'status' => 'not equals committed',
            'occurrenceDbId' => "equals $occurrenceDbId"
        ])['data'];

        $occurrenceName = '';
        foreach ($transactionList as $transaction) {
            $occurrenceInfo = $transaction['occurrence'];
            $transactionDbId = $transaction['transactionDbId'];
            foreach ($occurrenceInfo as $occ) {

                if ($occ['occurrenceDbId'] == $occurrenceDbId) {
                    $originalOccVar = $occ['variableDbIds'];
                    $occ['variableDbIds'][] = $variableDbId;

                    $resultVariableDbId = $this->formulaParameters->searchAll([
                        'fields' => '
                            formula_parameter.param_variable_id AS paramVariableDbId|
                            formula_parameter.result_variable_id AS resultVariableDbId
                        ',
                        'paramVariableDbId' =>  "$variableDbId",
                    ])['data'][0]['resultVariableDbId'] ?? 0;
                    $formulaParam = $this->formulaParameters->searchAll([
                        'fields' => '
                            formula_parameter.formula_id AS formulaDbId|
                            formula_parameter.param_variable_id AS paramVariableDbId|
                            formula_parameter.result_variable_id AS resultVariableDbId
                        ',
                        'resultVariableDbId' => "equals $resultVariableDbId",
                    ])['data'] ?? [];
                    $variableDbIdList = array_column($formulaParam,'paramVariableDbId');
                    $formulaDbIdList = array_unique(array_column($formulaParam,'formulaDbId'));
                    $result = $this->terminalTransaction->variableComputations($transactionDbId, [
                        'variableDbId' => $occ['variableDbIds'],
                        'formulaDbId' => $formulaDbIdList,
                    ]);

                    if($result['success'] && !in_array($resultVariableDbId, $occ['variableDbIds'])){
                        // update terminal transaction occurrences record
                        array_push($originalOccVar, ''.$resultVariableDbId);

                        $requestData['occurrences'] = [[
                            'variableDbIds' => $originalOccVar,
                            'occurrenceDbId' => $occurrenceDbId,
                            'dataUnit' => 'plot',
                            'dataUnitCount' => ''.$occ['dataUnitCount'],
                            'formulaDbId' => array_merge($formulaDbIdList, $occ['formulaDbId'] ?? []),
                        ]];

                        $this->terminalTransaction->updateOne($transactionDbId, $requestData);
                    }
                    $occurrenceName = $occ['occurrenceName'];
                }
            }
        }

        $plotData =  $this->occurrenceTrait->searchAll([
            "occurrenceDbId" => "equals $occurrenceDbId",
            "collectedTraits" => "is not null",
        ]);

        if (empty($occurrenceName) && $plotData['totalCount']) {

            // retrieve traits
            $traitList = $plotData['data'][0]['collectedTraits'] ?? [];


            if (!empty($traitList)) {

                $traits = array_column($traitList, 'variableAbbrev');
                $abbrev = $this->variable->getOne($variableDbId)['data']['abbrev'] ?? '';
                $resultVariableAbbrev = "equals ".implode('|equals ', array_values($traits));

                $variableList = $this->formulaParameters->searchAll([
                    'fields' => '
                        formula_parameter.formula_id AS formulaDbId|
                        result_variable.abbrev AS resultVariableAbbrev|
                        parameter_variable.abbrev AS paramVariableAbbrev|
                        formula_parameter.result_variable_id AS resultVariableDbId|
                        formula_parameter.param_variable_id AS paramVariableDbId
                    ',
                    'paramVariableDbId' =>  "$variableDbId",
                    'resultVariableAbbrev' =>  $resultVariableAbbrev,
                ])['data'] ?? [];
                if (!empty($variableList)){

                    $variableAbbrev = $variableList['0']['resultVariableAbbrev'] ?? '';
                    $resultVarId = $variableList['0']['resultVariableDbId'] ?? '';
                    $formulaDbId = [];
                    $resultAbbrevFormatted = '';
                    $variableAbbrevList = $variableDbIdList = $param = [];
                    foreach ($variableList as $variable) {
                        $variableAbbrevList[$variable['paramVariableAbbrev']] = 1;
                        $variableAbbrevList[$variable['resultVariableAbbrev']] = 1;
                        $variableDbIdList[$variable['paramVariableDbId']] = ''.$variable['paramVariableDbId'];
                        $variableDbIdList[$variable['resultVariableDbId']] = ''.$variable['resultVariableDbId'];
                        $param[$variable['resultVariableAbbrev']] = [
                            'dataValue' => 'not null'
                        ];
                        $formulaDbId[] = $variable['formulaDbId'];
                        $resultAbbrevFormatted = !empty($variable['resultVariableAbbrev']) ? 
                            $resultAbbrevFormatted . '|'. $variable['resultVariableAbbrev'] : $resultAbbrevFormatted;
                    }

                    $param["fields"] = "plotDbId$resultAbbrevFormatted";
                    if (!empty($variableAbbrevList)){
                        $url = "occurrences/$occurrenceDbId/plot-data-table-search";
                        $plotList = Yii::$app->api->getParsedResponse(
                            'POST',
                            $url,
                            json_encode($param),
                            retrieveAll: true
                        );

                        if ($plotList['totalCount']) {
                            // insert a new transaction dataset
                            $locationId = $this->occurrenceDetailsModel->getLocationIdOfOccurrence($occurrenceDbId);
                            $requestBody = [
                                "records"=>[
                                    [
                                        "locationDbId" => "$locationId",
                                        "action" => "file upload",
                                    ]
                                ]
                            ];
                            $transactionDbId = $this->terminalTransaction->create($requestBody)['data'][0]['transactionDbId'] ?? 0;
                            
                            $requestBody = [
                                "measurementVariables" => array_keys($variableAbbrevList),
                                "records" => []
                            ];
                
                            foreach ($plotList['data'] as $plot) {
                                // If not empty, add to records
                                $requestBody["records"][] = [
                                    'plot_id' => $plot['plotDbId'],
                                    strtolower($variableAbbrev) => 0
                                ];
                            }

                            // upload data
                            $this->terminalTransaction->createDataset($transactionDbId, $requestBody);
                            $this->terminalTransaction->validateDataset($transactionDbId);

                            $requestData['occurrences'] = [[
                                'variableDbIds' => [''.$resultVarId],
                                'occurrenceDbId' => $occurrenceDbId,
                                'dataUnit' => 'plot',
                                'dataUnitCount' => ''.$plotList['totalCount'],
                            ]];
                            $this->terminalTransaction->updateOne($transactionDbId, $requestData);

                            $this->terminalTransaction->variableComputations($transactionDbId, [
                                'variableDbId' => array_keys($variableDbIdList), 
                                'formulaDbId' => $formulaDbId
                            ]);

                            $this->terminalTransaction->updateOne($transactionDbId, ['status' => "uploaded"]);

                            $occurrenceName = $this->terminalTransaction->searchAll(['occurrenceDbId' => "equals $occurrenceDbId"])['data'][0]['occurrence'][0]['occurrenceName'];
                        }
                    }
                }
            }
        }

        return $occurrenceName;
    }

    /**
     * Extract add variable option values
     *
     * @param String $q ID page filter
     * @param String $attr Attribute to be retrieved
     * @param Integer $page Page number to be retrieved
     * @param Integer $excludedVars List of variable abbrevs to be excluded
     * @param String $level Data level of variable
     * 
     * @return Array total count and select2 tags
     */
    public function actionGetVariableOptions(string $q=null, string $attr=null, int $page=null, string $excludedVars=null, $level='occurrence', $usage='experiment_manager'){

        $offset = ($page==null ? 0 : (($page-1)*5)+1);
        $page = ($page==null ? 1 : $page);
        $limit = 5;
        $orderCond = "text:asc";

        $params = [
            'fields' => 'variable.id AS id | variable.abbrev AS abbrev | variable.usage as usage | variable.data_level as dataLevel | variable.label as label | variable.display_name as displayName',
            'usage' => "%$usage%",
            'dataLevel' => '%'.$level.'%'
        ];

        // exclude already added variables
        $varAbbrevs = json_decode($excludedVars);
        if(!empty($varAbbrevs)){
            $varAbbrevsCond = 'not equals ' . implode(' | not equals ', $varAbbrevs);
            $params['abbrev'] = $varAbbrevsCond;
        }

        if($q != null && $q != ""){
            $outputColumn = $attr;
            $params[$outputColumn] = '%'.trim($q).'%'; 
        }

        $resultLimits = ($limit == NULL || $limit == 0 ? '?page='.($offset+1) : '?limit='.$limit.'&page='.(($offset/$limit)+1));
    
        $method = 'POST';
        $endPoint = 'variables-search';

        $apiCallResult = Yii::$app->api->getParsedResponse($method,$endPoint.$resultLimits, json_encode($params), 'sort=label');
    
        $trackArr = [];
        $result = [];

        if(!empty($apiCallResult['data'])){
            foreach($apiCallResult['data'] as $value){
                if (!empty($value['label']) && !in_array($value['label'], $trackArr)) {
                    $trackArr[] = $value['label'];
                    $result[] = ["id" => $value["abbrev"], "text" => $value['label'] . ' ('. $value['displayName'] .')'];
                }
            }
        }
        
        return json_encode(['totalCount'=> $apiCallResult['totalCount'],'items'=>$result]);    
    }

    /**
     * Manage experiment level information
     *
     * @param String $program Current program of user
     * @param Integer $experimentId Experiment identifier
     * @param Array $varAbbrevs List of abbrevs of added variables
     * @param Integer $occurrenceId Occurrence identifier
     *
     * @return view file for manage experiment level data
     */
    public function actionExperiment($program, $experimentId, $varAbbrevs = [], $occurrenceId = null){
        // if experiment ID is not set
        if(empty($experimentId)){
            $notif = 'error';
            $icon = 'times';
            $message = 'There was a problem retrieving experiment information.';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
            $this->redirect(['/occurrence','program'=>$program])->send();
            exit();
        }

        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if (!$isAdmin && strtoupper($role) == 'COLLABORATOR')
            $role = 'COLLABORATOR';
        else if ($isAdmin)
            $role = 'ADMIN';
        else
            $role = 'DATA_OWNER';

        // get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($occurrenceId);
        $accessData = isset($occurrenceObj['accessData']) ? $occurrenceObj['accessData'] : null;
        $creatorId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;

        if(
            $isAdmin ||
            Yii::$app->access->renderAccess(
                'UPDATE_OCCURRENCE',
                'OCCURRENCES',
                $creatorId,
                $accessData,
                'write',
                'url'
            )
        ){
            // get experiment info
            $model = $this->experiment->getOne($experimentId);
            $experiment = isset($model['data']['experimentName']) ? $model['data']['experimentName'] : '';
            $dataProcessId = isset($model['data']['dataProcessDbId']) ? $model['data']['dataProcessDbId'] : '';
            $dataProcessAbbrev = isset($model['data']['dataProcessAbbrev']) ? $model['data']['dataProcessAbbrev'] : '';
            $experimentType = isset($model['data']['experimentType']) ? $model['data']['experimentType'] : '';
            $program = isset($model['data']['programCode']) ? $model['data']['programCode'] : '';
            $remarks = isset($model['data']['remarks']) ? $model['data']['remarks'] : '';

            $configAbbrev = str_replace('DATA_PROCESS', 'BASIC_INFO_ACT_VAL' , $dataProcessAbbrev);
            // for cross parent nursery, render phase I config to enable non-required fields
            $configAbbrev = str_replace('PHASE_II', 'PHASE_I' , $configAbbrev);

            // get configuration
            $configObj = $this->configuration->getConfigByAbbrev($configAbbrev);
            $config = isset($configObj['Values']) ? $configObj['Values'] : [];

            // get all variable abbrevs from the config
            $configVarAbrevs = array_column($config, 'variable_abbrev');

            // get saved data variables
            $dataVars = $this->getExperimentDataVariables($experimentId);

            // list of abbrevs of added variables
            $varAbbrevs = empty($varAbbrevs) ? [] : json_decode($varAbbrevs);
            if(!empty($remarks)){
                $varAbbrevs = array_merge(['REMARKS'], $varAbbrevs);
            }

            // combine data variables and selected variable abbrevs to exclude
            $excludedVars = array_unique(array_merge($dataVars, array_merge($varAbbrevs, $configVarAbrevs)));

            // transform list of variables to form config
            $dataVarsConfig = $this->browser->buildVarsAsFormConfig(array_unique(array_merge($dataVars, $varAbbrevs)));

            // combine variable from config and added variables
            $config = array_merge($config, $dataVarsConfig);

            $widgetOptions = [
                'mainModel' => 'Experiment',
                'config' => $config,
                'templateId' => $dataProcessId,
                'type' => $experimentType,
                'rowClass' => 'editable-field form-fields',
                'processType' => 'experiment',
                'mainApiResourceMethod' => 'POST',
                'mainApiResourceEndpoint' => 'experiments/'.$experimentId.'/data-search',
                'mainApiSource' => 'metadata',
                'mainApiResourceFilter' => [
                    'experimentDbId' => (string)$experimentId
                ],
                'program' => $program,
                'id' => $experimentId
            ];

            return $this->render('experiment',
                compact (
                    'model',
                    'program',
                    'widgetOptions',
                    'experiment',
                    'experimentId',
                    'excludedVars',
                    'varAbbrevs',
                    'occurrenceId',
                    'isAdmin',
                    'role',
                    'creatorId',
                    'accessData'
                )
            );
        }
    }

    /**
     * Get experiment data variables given experiment ID
     *
     * @param Integer $experimentId Experiment identifier
     * @return Array $dataVars List of experiment data variable abbrevs
     */
    public function getExperimentDataVariables($experimentId){

        $experimentDataObj = Yii::$app->api->getParsedResponse(
            'POST',
            'experiments/'.$experimentId.'/data-search?usage=experiment_manager',
            null,
            '',
            true
        );

        $dataVars = isset($experimentDataObj['data'][0]['data']) ? $experimentDataObj['data'][0]['data'] : [];

        $dataVars = array_column($dataVars, 'abbrev');

        return $dataVars;
    }

    /**
     * Render manage Occurrence-level Management Protocol data of an Experiment
     *
     * @param string $program Current program of user
     * @param int $experimentId Experiment identifier
     * @param array $varAbbrevs Newly added variable tags passed from URL query
     * @param int $occurrenceId Occurrence identifier
     *
     * @return view file for manage occurrence level data
     */
    public function actionManagement ($program, $experimentId, $varAbbrevs = [], $occurrenceId = null)
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if (!$isAdmin && strtoupper($role) == 'COLLABORATOR') {
            $role = 'COLLABORATOR';
        } elseif ($isAdmin) {
            $role = 'ADMIN';
        } else {
            $role = 'DATA_OWNER';
        }

        // get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($occurrenceId);
        $accessData = isset($occurrenceObj['accessData']) ? $occurrenceObj['accessData'] : null;
        $creatorId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;

        if (
            $isAdmin ||
            Yii::$app->access->renderAccess(
                'UPDATE_OCCURRENCE',
                'OCCURRENCES',
                $creatorId,
                $accessData,
                'write',
                'url'
            )
        ){
            // get variable abbrevs from all occurrences' management protocol specified then combine to 1 data browser

            // get experiment info
            $experimentObj = $this->experiment->getOne($experimentId);
            $experiment = isset($experimentObj['data']['experimentName']) ?
                $experimentObj['data']['experimentName'] : '';
            $dataProcessAbbrev = isset($experimentObj['data']['dataProcessAbbrev']) ?
                $experimentObj['data']['dataProcessAbbrev'] : '';

            // get Occurrence info config
            $config = $this->getConfig('occurrence', $program, $dataProcessAbbrev);
            // Keep only Occurrence name and Site columns
            $config = (isset($config[0]) && isset($config[1])) ? [$config[0], $config[1]] : [];

            // Disable editing of Occurrence name and Site columns
            $config[0]['disabled'] = true;
            $config[1]['disabled'] = true;

            // get Management Attributes data provider
            $params = Yii::$app->request->queryParams;
            $data = $this->manageOccurrence->search(
                $experimentId,
                $params,
                $program,
                'management',
                'limit to 1'
            );
            // data variables
            $occurrenceDataTable = $data['dataProvider']->allModels;
            $dataVars = $data['dataVars'];
            $specifiedAbbrevs = [];

            foreach ($occurrenceDataTable as $occurrenceData) {
                $occurrenceDbId = $occurrenceData['occurrenceDbId'];

                // Retrieve this Occurrence's Management Protocol List ID
                $response = $this->api->getApiResults(
                    'POST',
                    'occurrence-data-search',
                    json_encode([
                        'occurrenceDbId' => "equals $occurrenceDbId",
                        'variableAbbrev' => 'equals MANAGEMENT_PROTOCOL_LIST_ID'
                    ]),
                    '?limit=1&sort=occurrenceDbId:desc',
                    false
                );
                $managementProtocolListId = isset($response['data'][0]['dataValue']) ?
                    $response['data'][0]['dataValue'] : null;

                if(!empty($managementProtocolListId)){
                    // Retrieve list members AKA specified Management Attribute Abbrevs
                    $response = $this->api->getApiResults(
                        'POST',
                        "lists/$managementProtocolListId/members-search",
                        null,
                        '',
                        true
                    );
                    foreach ($response['data'] as $record) {
                        $specifiedAbbrevs []= $record['abbrev'];
                    }
                }
            }

            // combine data variables and selected variable abbrevs to exclude
            $varAbbrevs = array_merge(
                            empty($varAbbrevs) ? [] : json_decode($varAbbrevs),
                            $specifiedAbbrevs
                        );
            $excludedVars = array_merge($dataVars, $varAbbrevs);

            $dataProvider = $data['dataProvider'];
            $searchModel = $this->manageOccurrence;

            // build form columns
            $columns = $this->buildFormColumns($config, array_unique(array_merge($dataVars, $varAbbrevs)));

            return $this->render('management',
                compact(
                    'program',
                    'experimentId',
                    'experiment',
                    'dataProvider',
                    'columns',
                    'searchModel',
                    'excludedVars',
                    'varAbbrevs',
                    'occurrenceId',
                    'isAdmin',
                    'role',
                    'creatorId',
                    'accessData'
                )
            );
        }
    }

    /**
     * Render entry point-streamlined manage Code Pattern page
     *
     * @param string $program Current program of user
     * @param int $experimentId Experiment identifier
     * @param array $varAbbrevs Newly added variable tags passed from URL query
     * @param int $occurrenceId Occurrence identifier
     * @param string $level Data level identifier
     *
     * @return view file for manage Code Pattern page with navigation tab
     */
    public function actionCodeGeneration ($program, $experimentId, $varAbbrevs = [], $occurrenceId = null, $level = 'experiment')
    {

        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if (!$isAdmin && strtoupper($role) == 'COLLABORATOR')
            $role = 'COLLABORATOR';
        else if ($isAdmin)
            $role = 'ADMIN';
        else
            $role = 'DATA_OWNER';

        // get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($occurrenceId);
        $accessData = isset($occurrenceObj['accessData']) ? $occurrenceObj['accessData'] : null;
        $creatorId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;

        if (
            $isAdmin ||
            Yii::$app->access->renderAccess(
                'UPDATE_OCCURRENCE',
                'OCCURRENCES',
                $creatorId,
                $accessData,
                'write',
                'url'
            )
        ) {
            // get experiment info
            $experimentObj = $this->experiment->getOne($experimentId);
            // get experiment instance
            $experiment = isset($experimentObj['data']['experimentName']) ? $experimentObj['data']['experimentName'] : '';

            $entryCodePatternsParams = $this->actionCodePatterns(
                $program,
                'entry',
                $level, 
                ($level === 'experiment') ? $experimentId : '',
                ($level === 'experiment') ? '' : $occurrenceId,
                true
            );

            $plotCodePatternsParams = $this->actionCodePatterns(
                $program,
                'plot',
                $level, 
                ($level === 'experiment') ? $experimentId : '',
                ($level === 'experiment') ? '' : $occurrenceId,
                true
            );

            return $this->render('code_patterns_streamlined',
                compact(
                    'program', 
                    'experimentId',
                    'experiment',
                    'occurrenceId',
                    'entryCodePatternsParams',
                    'plotCodePatternsParams',
                    'level',
                    'isAdmin',
                    'role',
                    'creatorId',
                    'accessData'
                )
            );
        }
    }

    /**
     * Render entry point-streamlined manage Occurrence Protocols page
     *
     * @param string $program Current program of user
     * @param int $experimentId Experiment identifier
     * @param array $varAbbrevs Newly added variable tags passed from URL query
     * @param int $occurrenceId Occurrence identifier
     *
     * @return view file for manage Occurrecence Protocols page with navigation tab
     */
    public function actionProtocols ($program, $experimentId, $varAbbrevs = [], $occurrenceId = null)
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if (!$isAdmin && strtoupper($role) == 'COLLABORATOR')
            $role = 'COLLABORATOR';
        else if ($isAdmin)
            $role = 'ADMIN';
        else
            $role = 'DATA_OWNER';

        // get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($occurrenceId);
        $accessData = isset($occurrenceObj['accessData']) ? $occurrenceObj['accessData'] : null;
        $creatorId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;

        if (
            $isAdmin ||
            (
                Yii::$app->access->renderAccess(
                    'EXPERIMENT_MANAGER_UPDATE_OCC_PROTOCOL',
                    'OCCURRENCES',
                    $creatorId,
                    $accessData,
                    'write',
                    'url'
                )
            )
        ) {

            // get experiment info
            $experimentObj = $this->experiment->getOne($experimentId);
            // get experiment instance
            $experiment = isset($experimentObj['data']['experimentName']) ? $experimentObj['data']['experimentName'] : '';
            // get data process abbrev
            $dataProcessAbbrev = isset($experimentObj['data']['dataProcessAbbrev']) ? $experimentObj['data']['dataProcessAbbrev'] : '';
            // get params for generating Update Occurrence Protocols view file
            $params = $this->actionOccurrenceProtocols($program, $occurrenceId, $occurrenceId, $dataProcessAbbrev, true);
            $params['streamlined'] = true;

            return $this->render('occurrence_protocols_streamlined',
                compact(
                    'experimentId',
                    'experiment',
                    'params',
                    'isAdmin',
                    'role',
                    'creatorId',
                    'accessData'
                )
            );
        }
    }

    /**
     * Render Experiment Permissions tool page
     * 
     * @param string $program program identifier
     * @param string $occurrenceIds string of Occurrence IDs
     * @param int|string $invalidOccurrenceCount counter for invalid Occurrences
     * 
     * @return view view file
     */
    public function actionExperimentPermissions ($program, $occurrenceIds, $invalidOccurrenceCount = null)
    {
        // For CB Logging
        Yii::info(
            'Rendering Experiment Permissions browser ' .
            json_encode([
                'program' => $program,
                'occurrenceIds' => $occurrenceIds,
                'invalidOccurrenceCount'=> $invalidOccurrenceCount,
            ]),
            __METHOD__
        );

        $searchModel = $this->occurrenceCollaboratorPermissionSearch;
        $params = Yii::$app->request->queryParams;
        $params['occurrenceDbId'] = $occurrenceIds;

        // retrieve browser dataProvider
        $data = $searchModel->searchOccurrenceCollaboratorPermission($params);
        $dataProvider = $data[0];
        $currPage = $data[1];
        $totalPages = $data[2];

        if ($invalidOccurrenceCount > 0) {
            $notif = 'warning';
            $icon = 'warning';
            $message = "$invalidOccurrenceCount <b>invalid</b> Occurrences were selected.";
            Yii::$app->session->setFlash($notif, "<i class='fa fa-$icon'></i> $message");
        }

        return $this->render('experiment_permissions/index',
            compact(
                'searchModel',
                'dataProvider',
                'currPage',
                'totalPages',
                'program',
                'occurrenceIds',
            )
        );
    }

    /**
     * Render Manage Permissions modal
     * 
     * @param string $program program identifier
     * @param string $occurrenceIds string of Occurrence IDs
     * 
     * @return view view file
     */
    public function actionPermissions ($program)
    {
        $occurrenceIds = $_POST['occurrenceIds'] ?? '';
        $userId = $this->user->getUserId();
        $programDbIds = [];
        $creatorDbIds = [];
        $experimentStewardDbIds = [];

        // For CB Logging
        Yii::info(
            'Rendering Manage Permissions modal ' .
            json_encode([
                'program' => $program,
                'occurrenceIds' => $occurrenceIds,
            ]),
            __METHOD__
        );

        // get program IDs, experiment creators, and experiment stewards of selected Occurrences to be hidden from Permissions data browser
        $response = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'occurrenceDbId' => $occurrenceIds,
                'fields' => 'occurrence.id AS occurrenceDbId|program.id AS programDbId|creator.id AS creatorDbId|experiment.steward_id AS experimentStewardDbId'
            ]),
            '',
            true
        );

        foreach ($response['data'] as $value) {
            $programDbIds []= $value['programDbId'];
            $creatorDbIds []= $value['creatorDbId'];
            $experimentStewardDbIds []= $value['experimentStewardDbId'];
        }

        $params = Yii::$app->request->queryParams;
        $params['occurrenceDbId'] = $occurrenceIds;
        $params['entityDbId'] = "not equals $userId|not equals " .
            implode('|not equals ', (array_values(array_unique($programDbIds)))) .
            "|not equals " . implode('|not equals ', (array_values(array_unique($creatorDbIds)))) .
            "|not equals " . implode('|not equals ', (array_values(array_unique($experimentStewardDbIds))));

        $programData = $this->listModel->getPrograms();
        // hide experiment creators and experiment stewards from available Users
        $userData = $this->listModel->getUsers(null, array_merge($creatorDbIds, $experimentStewardDbIds), null, 'tags');
        // retrieve browser dataProvider
        $searchModel = $this->occurrencePermissionSearch;
        $dataProvider = $searchModel->searchOccurrencePermission($params);

        return $this->renderAjax('experiment_permissions/manage_permissions/index',
            compact(
                'dataProvider',
                'program',
                'userId',
                'programData',
                'userData',
                'occurrenceIds'
            )
        );
    }

    /**
     * Add access to selected entities to given occurrences
     *
     * @return json if action is successful
    **/
    public function actionAddAccess ()
    {
        $occurrenceIds = explode('|', $_POST['occurrenceIds']) ?? '';
        $selectedPersons = $_POST['selectedPersons'] ?? [];
        $selectedTeams = $_POST['selectedTeams'] ?? [];
        $access = $_POST['access'] ?? 'write';
        $trimmedPersons = [];
        $trimmedTeams = [];
        $totalTrimmedPersonsCount = 0;
        $totalTrimmedTeamsCount = 0;
        $hasRedundantInput = true;
        $failedAllAddAttempts = true;

        /* For each selected Occurrence, add new set of permissions */
        foreach ($occurrenceIds as $occurrenceId) {
            /* Remove redundant inputs */
            $occurrencePermissions = $this->api->getApiResults(
                'POST',
                'occurrence-permissions-search',
                json_encode([
                    'occurrenceDbId' => "$occurrenceId",
                ]),
                '',
                true
            )['data'];

            // Trim Persons input
            $personIds = array_filter($occurrencePermissions, function ($v, $k) {
                return $v['entity'] == 'person';
            }, ARRAY_FILTER_USE_BOTH);
            $personIds = array_column($personIds, 'entityDbId');
            $trimmedPersons = array_diff($selectedPersons, $personIds);

            // Trim Teams/Programs input
            $programIds = array_filter($occurrencePermissions, function ($v, $k) {
                return $v['entity'] == 'program';
            }, ARRAY_FILTER_USE_BOTH);
            $programIds = array_column($programIds, 'entityDbId');
            $trimmedTeams = [ ...array_diff($selectedTeams, $programIds) ];

            // Update entity count
            $totalTrimmedPersonsCount += count($trimmedPersons);
            $totalTrimmedTeamsCount += count($trimmedTeams);

            // If inputs are redundant to given Occurrence
            if (count($trimmedPersons) == 0 && count($trimmedTeams) == 0)
                continue;

            $hasRedundantInput = false;
            $rawData = [ 'records' => [], ];

            // Add permissions for User/Person accounts
            foreach ($trimmedPersons as $personId) {
                array_push($rawData['records'], [
                    'entityDbId' => $personId,
                    'entity' => 'person',
                    'permission' => $access,
                ]);
            }

            // Add permissions for Programs/Teams
            foreach ($trimmedTeams as $programId) {
                array_push($rawData['records'], [
                    'entityDbId' => $programId,
                    'entity' => 'program',
                    'permission' => $access,
                ]);
            }

            // Call API Endpoint
            $response = $this->api->getApiResults(
                'POST',
                "occurrences/$occurrenceId/permissions",
                json_encode($rawData)
            );

            if ($response['success'])
                $failedAllAddAttempts = false;
                
        }

        if ($hasRedundantInput) {
            return json_encode([
                'success' => false,
                'message' => 'Redundant permissions found. Please add new permissions.',
                'color' => 'red',
                'icon' => 'close',
                'trimmedPersonsCount' => 0,
                'trimmedTeamsCount' => 0,
            ]);
        }

        if ($failedAllAddAttempts) {
            return json_encode([
                'success' => false,
                'message' => 'Failed to add new permissions',
                'color' => 'red',
                'icon' => 'close',
                'trimmedPersonsCount' => $totalTrimmedPersonsCount,
                'trimmedTeamsCount' => $totalTrimmedTeamsCount,
            ]);
        }

        return json_encode([
            'success' => true,
            'message' => 'Added new permissions.',
            'color' => 'green',
            'icon' => 'done',
            'trimmedPersonsCount' => $totalTrimmedPersonsCount,
            'trimmedTeamsCount' => $totalTrimmedTeamsCount,
        ]);
    }

    /**
     * Revoke access to selected entities to given occurrences
     *
     * @return json the number of remaining access to list
    **/
    public function actionRevokeAccess ()
    {
        $occurrenceIds = explode('|', $_POST['occurrenceIds']);
        $selectedPersons = (array) json_decode($_POST['personEntityIds']) ?? [];
        $selectedTeams = (array) json_decode($_POST['programEntityIds']) ?? [];
        $params = Yii::$app->request->queryParams;
        $count = 0;
        $writeId = $this->api->getApiResults(
            'POST',
            'roles-search',
            json_encode([ 'personRoleCode' => 'DATA_PRODUCER' ])
        )['data'][0]['roleDbId'];
        $readId = $this->api->getApiResults(
            'POST',
            'roles-search',
            json_encode([ 'personRoleCode' => 'DATA_CONSUMER' ])
        )['data'][0]['roleDbId'];
        $failedResponses = 0;
        
        // go through each Occurrence for deletion
        foreach ($occurrenceIds as $occurrenceId) {
            $params['occurrenceDbId'] = "$occurrenceId";
            $occurrencePermissions = $this->occurrencePermissionSearch->searchAll($params)['data'] ?? [];
            $count += count($occurrencePermissions);
            $rawData = [
                'accessData' => [
                    'person' => [],
                    'program' => [],
                ],
            ];
            $selectedPersonsSub = $selectedPersons["$occurrenceId"] ?? [];
            $selectedTeamsSub = $selectedTeams["$occurrenceId"] ?? [];

            // Repackage access_data
            foreach ($occurrencePermissions as $key => $value) {
                $entity = $value['entity'];
                $entityDbId = "{$value['entityDbId']}";
                $addedBy = $this->user->getUserId();
                $addedOn = date('c');
                $permission = $value['permission'];
                $dataRoleId = ($permission == 'write') ? $writeId : $readId;

                $rawData['accessData'][$entity][$entityDbId] = [
                    'addedBy' => $addedBy,
                    'addedOn' => $addedOn,
                    'dataRoleId' => $dataRoleId,
                ];
            }

            // Remove permissions for selected persons
            foreach ($selectedPersonsSub as $personId) {
                unset($rawData['accessData']['person']["$personId"]);
                $count -= 1;
            }

            if (count($rawData['accessData']['person']) == 0)
                unset($rawData['accessData']['person']);

            // Remove permissions for selected programs/teams
            foreach ($selectedTeamsSub as $programId) {
                unset($rawData['accessData']['program']["$programId"]);
                $count -= 1;
            }

            if (count($rawData['accessData']['program']) == 0)
                unset($rawData['accessData']['program']);

            if (count($rawData['accessData']) == 0)
                $rawData['accessData'] = new stdClass();

            // Update access_data of given Occurrence using the remaining permissions post-removal
            $response = $this->api->getApiResults(
                'PUT',
                "occurrences/$occurrenceId",
                json_encode($rawData)
            );

            if (!$response['success'])
                $failedResponses += 1;
        }

        return json_encode([ 
            'success' => true,
            'remaining' => $count,
            'failedResponses' => $failedResponses,
        ]);
    }

    /**
     * Reset manage permissions data grid
     *
     * @return view file
    **/
    public function actionResetManagePermissionsGrid ()
    {

        $occurrenceIds = $_POST['occurrenceIds'] ?? '';
        $searchModel = $this->occurrencePermissionSearch;
        $userId = $this->user->getUserId();
        $params = Yii::$app->request->queryParams;
        $params['occurrenceDbId'] = $occurrenceIds;
        $programDbIds = [];
        $creatorDbIds = [];
        $experimentStewardDbIds = [];

        // get program IDs, creators, and experiment stewards of selected Occurrences to be hidden from Permissions data browser
        $response = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'occurrenceDbId' => $occurrenceIds,
                'fields' => 'occurrence.id AS occurrenceDbId|program.id AS programDbId|creator.id AS creatorDbId|experiment.steward_id AS experimentStewardDbId'
            ]),
            '',
            true
        );

        foreach ($response['data'] as $value) {
            $programDbIds []= $value['programDbId'];
            $creatorDbIds []= $value['creatorDbId'];
            $experimentStewardDbIds []= $value['experimentStewardDbId'];
        }

        $params['entityDbId'] = "not equals $userId|not equals " .
            implode('|not equals ', (array_values(array_unique($programDbIds)))) .
            "|not equals " . implode('|not equals ', (array_values(array_unique($creatorDbIds)))) .
            "|not equals " . implode('|not equals ', (array_values(array_unique($experimentStewardDbIds))));

        $dataProvider = $searchModel->searchOccurrencePermission($params);
        
        return $this->renderAjax('experiment_permissions/manage_permissions/grid',[
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Create an occurrence record
     * @param string $program Current program of user
     * @param int $experimentId Experiment identifier
     * @param int $occurrenceId Occurrence identifier
     * 
     * @return boolean
    **/
    public function actionAddNewOccurrence ($experimentId, $program, $occurrenceId)
    {

        $occurrenceRecord =  $this->occurrence->searchAll(['occurrenceDbId'=>"equals $occurrenceId"]);
        $occurrenceRep = isset($occurrenceRecord['data'][0]['repCount']) ?$occurrenceRecord['data'][0]['repCount'] : null ;

        $experimentRecord = $this->occurrence->searchAll(['experimentDbId'=>"equals $experimentId"]);
        $programDbId = isset($experimentRecord['data'][0]['programDbId']) ?$experimentRecord['data'][0]['programDbId'] : null ;
        $records = [
            'occurrenceStatus' => 'draft',
            'experimentDbId' => "$experimentId"
        ];

        if(!empty($occurrenceRep)){
            $records['repCount'] = "$occurrenceRep";
        }

        $requestData['records'][] = $records;
        $createdRecord = $this->occurrence->create($requestData);

        // get IDs of DRAFT occurrences
        $drafts = $this->occurrence->searchAll([
            'fields'=>'occurrence.id as occurrenceDbId|occurrence.occurrence_status as occurrenceStatus|occurrence.experiment_id AS experimentDbId',
            'occurrenceStatus' => "equals draft",
            'experimentDbId' => "equals ".$experimentId
        ],'sort=occurrenceDbId:ASC');

        $draftOccurrenceIds = 0;
        if($drafts['totalCount'] > 0){
            $draftOccurrenceIds = array_column($drafts['data'], 'occurrenceDbId');
            $draftOccurrenceIds = implode("|", $draftOccurrenceIds);
        }

        if($createdRecord['status'] == 200){
            //populate occurrence access data
            $userModel = new User();
            $userId = $userModel->getUserId();
            $newOccurrenceId = isset($createdRecord['data'][0]['occurrenceDbId']) ?$createdRecord['data'][0]['occurrenceDbId'] : null ;
            $this->occurrence->populateOccurrenceInfo([$newOccurrenceId], $programDbId, $userId);
            return json_encode(['success' => TRUE, 'draftOccurrences'=>$draftOccurrenceIds]);
        } else {
            return json_encode(['success' => FALSE]);
        }
    }

    /**
     * Delete a draft occurrence record
     * @param string $program Current program of user
     * @param int $experimentId Experiment identifier
     * @param int $occurrenceId Occurrence identifier
     * 
     * @return boolean
    **/
    public function actionDeleteOccurrence ($experimentId, $program, $occurrenceId)
    {   
        if(isset($_POST['deleteOccurrenceId'])){

            // get IDs of DRAFT occurrences
            $drafts = $this->occurrence->searchAll([
                'fields'=>'occurrence.id as occurrenceDbId|occurrence.occurrence_status as occurrenceStatus|occurrence.experiment_id AS experimentDbId',
                'occurrenceStatus' => "equals draft",
                'experimentDbId' => "equals ".$experimentId
            ],'sort=occurrenceDbId:ASC');

            $draftOccurrenceIds = 0;
            if($drafts['totalCount'] > 0){
                $draftOccurrenceIds = array_column($drafts['data'], 'occurrenceDbId');
                $draftOccurrenceIds = implode("|", $draftOccurrenceIds);
            }

            $deleteRecord = $this->occurrence->deleteMany([$_POST['deleteOccurrenceId']]);

            if($deleteRecord['status'] == 200){
                return json_encode(['success' => TRUE, 'draftOccurrences'=>$draftOccurrenceIds]);
            } else {
                return json_encode(['success' => FALSE]);
            }
        }
    }

    /**
     * Create background transaction and trigger worker for creation of occurrence related data
     * @param string $program Current program of user
     * @param int $experimentId Experiment identifier
     * @param int $occurrenceId Occurrence identifier
     * 
     */
    public function actionPrepareOccurrenceCreation($experimentId, $program, $occurrenceId, $planTemplateDbId = null){
        
        $newOccurrences =  $this->occurrence->searchAll(['experimentDbId'=>"equals $experimentId", 'occurrenceStatus'=>'draft']);
        $newOccurrenceIds = isset($newOccurrences['data'][0]['occurrenceDbId']) ? array_column($newOccurrences['data'], 'occurrenceDbId') : [] ;
        $createdOccurrences =  $this->occurrence->searchAll(['experimentDbId'=>"equals $experimentId", 'occurrenceStatus'=>'not equals draft']);
        $createdOccurrenceIds = isset($createdOccurrences['data'][0]['occurrenceDbId']) ? array_column($createdOccurrences['data'], 'occurrenceDbId') : [] ;
        
        // get user ID
        $userId = Yii::$app->session->get('user.id');

        $formattedOccurrenceId = implode('|', $newOccurrenceIds);
        $backgroundJobParams = [
            "workerName" => "CreateOccurrenceRecords",
            "description" => $formattedOccurrenceId,
            "entity" => "OCCURRENCE",
            "entityDbId" => $newOccurrenceIds[0],
            "endpointEntity" => "OCCURRENCE",
            "application" => "OCCURRENCES",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId
        ];
        
        $redirectUrl = ['/occurrence?program='.$program.'&OccurrenceSearch[occurrenceDbId]='.$formattedOccurrenceId];

        //create background transactions
        try {
            $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);         
        }

        // if creating transaction is successful
        try {
            if (isset($transaction['status']) && $transaction['status'] == 200) {
                $message = "The records are being generated in the background. You may proceed with other tasks. You will be notified in this page once done.";
                // set background processing info to session
                Yii::$app->session->set('em-bg-processs-message', $message);
                Yii::$app->session->set('em-bg-processs', true);
    
                $host = getenv('CB_RABBITMQ_HOST');
                $port = getenv('CB_RABBITMQ_PORT');
                $user = getenv('CB_RABBITMQ_USER');
                $password = getenv('CB_RABBITMQ_PASSWORD');

                $connection = new AMQPStreamConnection($host, $port, $user, $password);
    
                $channel = $connection->channel();
    
                $channel->queue_declare(
                    'CreateOccurrenceRecords', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                    false,  //passive - can use this to check whether an exchange exists without modifying the server state
                    true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                    false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                    false   //auto delete - queue is deleted when last consumer unsubscribes
                );
                
                $messageParams = [
                        'tokenObject' => [
                            'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                            'refreshToken' => ''
                        ],
                        'backgroundJobId' => $transaction["data"][0]["backgroundJobDbId"],
                        'description' => 'Creation of occurrence related records',
                        'newOccurrenceIds' => $newOccurrenceIds,
                        'oldOccurrenceId' => $createdOccurrenceIds[0],
                        'experimentId' => $experimentId,
                        'personDbId' => $userId,

                ];
                if($planTemplateDbId != null){
                    $messageParams['planTemplateDbId'] = $planTemplateDbId;
                }
                $msg = new AMQPMessage(
                    json_encode($messageParams),
                    array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                );
    
                $channel->basic_publish(
                    $msg,   //message 
                    '', //exchange
                    'CreateOccurrenceRecords'  //routing key (queue)
                );
                $channel->close();
                $connection->close();
                
                //update occurrence status to creation in progress
                $this->occurrence->updateMany($newOccurrenceIds, ['occurrenceStatus' => 'creation in progress']);
                //populate occurrence information
                $experimentRecord = $this->experiment->getOne($experimentId)['data'];

                //update experiment status to creation in progress
                $this->experiment->updateOne($experimentId, ["experimentStatus"=>"creation in progress"]);

                $this->occurrence->populateOccurrenceInfo($newOccurrenceIds, $experimentRecord['programDbId'], $userId);
                //Save the protocol in the occurrence data
                $protocolData = $this->protocolModel->getExperimentProtocols($experimentId, $newOccurrences['data']);
                if(count($protocolData) > 0){
                    $record['records'] = $protocolData;
                    $create = $this->occurrenceData->create($record);
                }

                // effectively exits this function (will trigger a (false) error response in the calling ajax)
                Yii::$app->response->redirect($redirectUrl);
            } else {
                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
            }
        } catch (\Exception $e) {
            \ChromePhp::log('e:', $e);
        }
    }

    /**
     * Delete the occurrence and all its records
     *
     * @param string $program name of the current program
     */
    public function actionDeleteCreatedOccurrence($program){

        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        
        // get user ID
        $userId = Yii::$app->session->get('user.id');

        $backgroundJobParams = [
            "workerName" => "OccurrenceProcessor",
            "description" => "Delete occurrence related records of ".$id,
            "entity" => "OCCURRENCE",
            "entityDbId" => $id,
            "endpointEntity" => "OCCURRENCE",
            "application" => "OCCURRENCES",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId
        ];
        
        $redirectUrl = ['/occurrence?program='.$program];

        //create background transactions
        try {
            $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);         
        }

        // if creating transaction is successful
        try {
            if (isset($transaction['status']) && $transaction['status'] == 200) {
                $message = "The records are being deleted in the background. You may proceed with other tasks. You will be notified in this page once done.";
                // set background processing info to session
                Yii::$app->session->set('em-bg-processs-message', $message);
                Yii::$app->session->set('em-bg-processs', true);
    
                $host = getenv('CB_RABBITMQ_HOST');
                $port = getenv('CB_RABBITMQ_PORT');
                $user = getenv('CB_RABBITMQ_USER');
                $password = getenv('CB_RABBITMQ_PASSWORD');

                $connection = new AMQPStreamConnection($host, $port, $user, $password);
    
                $channel = $connection->channel();
    
                $channel->queue_declare(
                    'OccurrenceProcessor', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                    false,  //passive - can use this to check whether an exchange exists without modifying the server state
                    true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                    false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                    false   //auto delete - queue is deleted when last consumer unsubscribes
                );
    
                $msg = new AMQPMessage(
                    json_encode([
                        'tokenObject' => [
                            'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                            'refreshToken' => ''
                        ],
                        'backgroundJobId' => $transaction["data"][0]["backgroundJobDbId"],
                        'description' => 'Deletion of occurrence related records',
                        'occurrenceDbIds' => [$id],
                        'personDbId' => $userId,
                        'processName' => "delete-occurrences"
                    ]),
                    array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                );
    
                $channel->basic_publish(
                    $msg,   //message 
                    '', //exchange
                    'OccurrenceProcessor'  //routing key (queue)
                );
                $channel->close();
                $connection->close();
                
                //update occurrence status to deletion in progress
                $this->occurrence->updateOne($id, ['occurrenceStatus' => 'deletion in progress']);

                // effectively exits this function (will trigger a (false) error response in the calling ajax)
                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> ' . $message);        
                Yii::$app->response->redirect($redirectUrl);
            } else {
                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
            }
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $e);
        }
    }
    
    /**
     * Update experiment name and occurrence names upon confirmation when updating Experiment Stage, Experiment Year, and/or Evaluation Season fields
     * @param integer $id Experiment ID
     */
    public function actionUpdateExperimentName($id) {

        if(isset($_POST)){
            $inputStage = $_POST['stage'];
            $inputYear = $_POST['year'];
            $inputSeason = $_POST['season'];

            //split value to get id
            $stageValArray = explode('-', $inputStage);
            if(count($stageValArray) > 1){
                $inputStage = $stageValArray[1];
            }

            $param = [
                'generateExperimentName' => 'true',
                'stageDbId' => "$inputStage", 
                'experimentYear'=>"$inputYear",
                'seasonDbId'=>"$inputSeason"
            ];
            $method = 'PUT';
            $path = 'experiments/'.$id;
            
            // update experiment name
            $response = Yii::$app->api->getResponse($method, $path, json_encode($param));
            $experiment = $this->experiment->getExperiment($id);

            // search for occurrence under this experiment
            $searchOccurrences = Yii::$app->api->getParsedResponse(
                'POST',
                'experiments/'.$id.'/occurrence-data-table-search',
                json_encode([
                    'fields' => 'occurrence.id as occurrenceDbId | occurrence.occurrence_name as occurrence | occurrence.occurrence_number as occurrenceNumber | experiment.experiment_name as experiment',
                ])
            );

            if (isset($searchOccurrences['data']) && !empty($searchOccurrences['data'])) {
                $occurrenceData = $searchOccurrences['data'];

                foreach($occurrenceData as $occurrence) {
                    $occurrenceName = $experiment['experimentName']. '-' .str_pad($occurrence['occurrenceNumber'], 3, 0, STR_PAD_LEFT);

                    // Update occurrence names
                    $updateOccurrence = Yii::$app->api->getParsedResponse(
                        'PUT',
                        'occurrences/'. $occurrence['occurrenceDbId'],
                        json_encode([
                            'occurrenceName' => (string) $occurrenceName
                        ])
                    );
                }
            }

            return json_encode([
                'experimentName' => $experiment['experimentName']
            ]);
        }
    }

    /**
     * Get occurrence information of experiment
     * @param string $program Current program of user
     * @param int $experimentId Experiment identifier
     * @param int $occurrenceId Occurrence identifier
     * 
     * @return boolean
    **/
    public function actionGetOccurrencesInfo ($experimentId, $program)
    {
        // get IDs of DRAFT occurrences
        $drafts = $this->occurrence->searchAll([
            'fields'=>'occurrence.id as occurrenceDbId|occurrence.occurrence_status as occurrenceStatus|occurrence.experiment_id AS experimentDbId',
            'occurrenceStatus' => "equals draft",
            'experimentDbId' => "equals ".$experimentId
        ],'sort=occurrenceDbId:ASC');

        //get occurrences with incomplete required info
        $incDrafts = $this->occurrence->searchAll([
            'fields'=>'occurrence.id as occurrenceDbId|occurrence.occurrence_status as occurrenceStatus|occurrence.experiment_id AS experimentDbId|occurrence.site_id AS siteDbId',
            'occurrenceStatus' => "equals draft",
            'experimentDbId' => "equals ".$experimentId,
            'siteDbId' => "is null"
        ],'sort=occurrenceDbId:ASC');


        $draftOccurrenceIds = null;
        $incDraftOccCount = $incDrafts['totalCount'] ?? 0;
        $draftOccCount = $drafts['totalCount'] ?? 0;

        return json_encode([
            'incDraftOccCount' => $incDraftOccCount,
            'draftOccCount'=> $draftOccCount
        ]);
    }

    /**
     * Get parameter sets for experiment
     * @param int $experimentId Experiment identifier
     * @param string $program Current program of user
     * @param int $entityType Design parameters
     * 
     * @return array
    **/
    public function actionGetParameterSets ($experimentId, $program, $entityType)
    {   
        $model = $this->experiment->getExperiment($experimentId);
        $dataDesign = $this->copyExperimentModel->getDesignInformationParamSet($experimentId, $model);

        $htmlData = $this->renderPartial('/design/_select_parameter_set.php',$dataDesign,true,false);

        return json_encode([
            'htmlData' => $htmlData
        ]);
    }
}   