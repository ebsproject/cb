<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\controllers;

use app\components\B4RController;


use app\controllers\BrowserController;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\models\IPerson;
use app\interfaces\modules\seedInventory\models\IVariableFilterComponent;
use app\models\Api;
use app\models\BackgroundJob;
use app\models\Browser;
use app\models\Entry as EntryModel;
use app\models\PlantingJob;
use app\interfaces\models\IExperiment;
use app\interfaces\models\IConfig;
use app\models\CrossSearch;
use app\modules\crossManager\models\CrossModel;
use app\models\CrossParent;
use app\models\Location;
use app\interfaces\models\IOccurrence;
use app\models\Package;
use app\models\PlantingInstruction;
use app\models\PlantingJobEntry;
use app\interfaces\models\IProgram;
use app\models\ScaleValue;
use app\models\Seed;
use app\interfaces\models\IUser;
use app\interfaces\models\IVariable;
use app\modules\experimentCreation\models\FormModel;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\location\models\FieldLayout;
use app\modules\occurrence\models\Entry;
use app\modules\occurrence\models\FileExport;
use app\modules\occurrence\models\SaveList;
use app\modules\occurrence\models\LocationPlot;
use app\modules\occurrence\models\Plot;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Yii;
use yii\caching\TagDependency;
use app\models\Worker;


/**
 * Controller for the view occurrence
 */
class ViewController extends B4RController
{
    // k-v map of Entry browser attributes and fields params
    private $entryAttributes = [
        'entryDbId' => 'pi.entry_id AS entryDbId',
        'entryCode' => 'pi.entry_code AS entryCode',
        'entryNumber' => 'pi.entry_number AS entryNumber',
        'entryName' => 'pi.entry_name AS entryName',
        'entryType' => 'pi.entry_type AS entryType',
        'entryRole' => 'pi.entry_role AS entryRole',
        'entryClass' => 'pi.entry_class AS entryClass',
        'entryStatus' => 'pi.entry_status AS entryStatus',
        'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
        'parentage' => 'g.parentage',
        'generation' => 'g.generation',
        'description' => 'e.description',
        'germplasmState' => 'g.germplasm_state AS germplasmState',
        'germplasmType' => 'g.germplasm_type AS germplasmType',
        'seedDbId' => 'pi.seed_id AS seedDbId',
        'seedCode' => 'seed.seed_code AS seedCode',
        'seedName' => 'seed.seed_name AS seedName',
        'packageDbId' => 'pi.package_id AS packageDbId',
        'packageCode' => 'package.package_code AS packageCode',
        'packageLabel' => 'package.package_label AS packageLabel',
        'packageQuantity' => 'package.package_quantity AS packageQuantity',
        'packageUnit' => 'package.package_unit AS packageUnit',
    ];
    // k-v map of Plot browser attributes and fields params
    private $plotAttributes = [
        'plotDbId' => 'plot.id AS plotDbId',
        'entryDbId' => 'plot.entry_id AS entryDbId',
        'entryCode' => 'pi.entry_code AS entryCode',
        'entryNumber' => 'pi.entry_number AS entryNumber',
        'entryName' => 'pi.entry_name AS entryName',
        'entryType' => 'pi.entry_type AS entryType',
        'entryRole' => 'pi.entry_role AS entryRole',
        'entryClass' => 'pi.entry_class AS entryClass',
        'entryStatus' => 'pi.entry_status AS entryStatus',
        'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
        'parentage' => 'g.parentage',
        'generation' => 'g.generation',
        'germplasmCode' => 'g.germplasm_code AS germplasmCode',
        'germplasmState' => 'g.germplasm_state AS germplasmState',
        'germplasmType' => 'g.germplasm_type AS germplasmType',
        'seedDbId' => 'pi.seed_id AS seedDbId',
        'seedCode' => 'seed.seed_code AS seedCode',
        'seedName' => 'seed.seed_name AS seedName',
        'packageDbId' => 'pi.package_id AS packageDbId',
        'packageCode' => 'package.package_code AS packageCode',
        'packageLabel' => 'package.package_label AS packageLabel',
        'plotCode' => 'plot_code AS plotCode',
        'plotNumber' => 'plot_number AS plotNumber',
        'plotType' => 'plot_type AS plotType',
        'rep' => 'rep',
        'designX' => 'design_x AS designX',
        'designY' => 'design_y AS designY',
        'plotOrderNumber' => 'plot_order_number AS plotOrderNumber',
        'paX' => 'pa_x AS paX',
        'paY' => 'pa_y AS paY',
        'fieldX' => 'field_x AS fieldX',
        'fieldY' => 'field_y AS fieldY',
        'blockNumber' => 'block_number AS blockNumber',
        'plotStatus' => 'plot_status AS plotStatus',
        'plotQcCode' => 'plot_qc_code AS plotQcCode',
    ];


    public function __construct ($id, $module,
        protected Api $api,
        protected BackgroundJob $backgroundJob,
        protected Browser $browser,
        protected BrowserController $browserController,
        protected IConfig $configuration,
        protected CrossSearch $crossSearch,
        protected CrossModel $crossModel,
        protected CrossParent $crossParent,
        protected Entry $entry,
        protected EntryModel $entryModel,
        protected IExperiment $experiment,
        public ExperimentModel $expModel,
        protected FieldLayout $fieldLayout,
        protected FormModel $formModel,
        protected IPerson $person,
        protected Location $location,
        protected IVariableFilterComponent $variableFilterComponent,
        protected LocationPlot $locationPlot,
        protected IOccurrence $occurrence,
        protected Package $package,
        protected Plot $plot,
        protected PlantingInstruction $plantingInstruction,
        protected PlantingJob $plantingJob,
        protected PlantingJobEntry $plantingJobEntry,
        protected IProgram $program,
        protected SaveList $saveList,
        protected Seed $seed,
        protected ScaleValue $scaleValue,
        protected IUser $user,
        protected IVariable $variable,
        protected Worker $worker,

        $config=[]
    ) {
        parent::__construct($id, $module, $config);
    }

    /**
     * View occurrence basic information
     *
     * @param text $program program identifier
     * @param integer $id occurrence identifier
     */
    public function actionIndex($program, $id)
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);
 
        // Get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($id);

        if (!$occurrenceObj) {
            throw new \Exception('Occurrence not found.');
        }

        $accessData = isset($occurrenceObj['accessData']) ? $occurrenceObj['accessData'] : null;
        $creatorDbId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;

        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess(
                'EXPERIMENT_MANAGER_VIEW_OCCURRENCE',
                'OCCURRENCES',
                $creatorDbId,
                usage: 'url'
            );
        }

        $data = $this->occurrence->getDetailViewInfo($id);

        return $this->render('basic', [
            'program' => $program,
            'id' => $id,
            'data' => $data,
            'role' => $role,
            'accessData' => $accessData,
            'creatorDbId' => $creatorDbId,
        ]);
    }

    /**
     * View occurrence status monitoring information
     *
     * @param integer $id occurrence identifier
     */
    public function actionRenderOccurrenceStatusMonitoringInfo($id)
    {
        $data = '';
        $monitoringInfo = $this->occurrence->getMonitoringInfo($id);
        
        // Remove seed collected info
        unset($monitoringInfo['seeds']);
        
        if(!empty($monitoringInfo) && isset($monitoringInfo)){

            $data = '<div class="viewer-monitoring-info">';

                // loop through all status monitoring data
                foreach ($monitoringInfo as $key => $value) {

                    if(isset($value['label'])){
                        $doneCount = isset($value['doneCount']) ? $value['doneCount'] : 0;
                        $allCount = isset($value['allCount']) ? $value['allCount'] : 0;
                        $statusVal = isset($value['status']) ? $value['status'] : 0;
                        $url = isset($value['url']) && !empty($value['url']) ? $value['url'] : '';
                        $progressUrl = $progressUrlEnd = '';

                        if(!empty($url)){
                            $urlTitle = isset($value['urlTitle']) && !empty($value['urlTitle']) ? $value['urlTitle'] : '';

                            $progressUrl = '<a href="'.$url.'" title="'.$urlTitle.'">';
                            $progressUrlEnd = '</a>';
                        }

                        $data .= '<div class="col col-lg-6 status-monitoring">'.
                            '<div class="progress-label">'.
                                $value['label'] .
                            '</div>'.
                            $progressUrl.
                            '<div class="progress-info-panel">'.
                                '<div class="progress-summary text-muted">'.
                                    $doneCount . ' of ' . $allCount . ' (' . round($statusVal, 2) . '%)'.
                                '</div>'.
                                '<div class="progress">'.
                                    '<div class="determinate" style="width: '.$statusVal.'%"></div>'.
                                '</div>'.
                            '</div>'.
                            $progressUrlEnd.
                        '</div>';
                    }
                }

            $data .= '</div>';
        }

        return json_encode($data);
    }

    /**
     * View entries of an occurrence
     *
     * @param text $program program identifier
     * @param integer $id occurrence identifier
     */
    public function actionEntry($program, $id)
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        // Get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($id);
        $creatorDbId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;

        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess(
                'EXPERIMENT_MANAGER_VIEW_OCCURRENCE',
                'OCCURRENCES',
                $creatorDbId,
                usage: 'url'
            );
        }
        

        // Get entry columns
        $mainCols = $this->browserController->getColumns($program, 'VIEW_OCCURRENCE_DATA', 'VIEW_ENTRY_COLUMNS');
        [ $mainAttributes, ] = $this->browserController->getColumns($program, 'VIEW_OCCURRENCE_DATA', 'VIEW_ENTRY_COLUMNS', [], 'download');
        $searchModel = $this->entry;
        $params = Yii::$app->request->queryParams;
        $output = $searchModel->search($params, $mainAttributes);
        $browserId = 'dynagrid-em-occurrence-entry';

        // Get occurrence status and determine if it is Packed or not
        $occurrenceResponse = $this->occurrence->searchAll(
            [ 'occurrenceDbId' => "equals $id" ],
            'limit=1'
        );

        $isPacked = ($occurrenceResponse &&
            $occurrenceResponse['success'] &&
            count($occurrenceResponse['data']) > 0 &&
            str_contains($occurrenceResponse['data'][0]['occurrenceStatus'], 'packed')
        ) ? true : false;

        $experimentDbId = ($occurrenceResponse &&
            $occurrenceResponse['success'] &&
            count($occurrenceResponse['data']) > 0
        ) ? $occurrenceResponse['data'][0]['experimentDbId'] : throw new \Exception('Could not retrieve experimentDbId. Occurrence not found.');

        $occurrenceName = ($occurrenceResponse &&
            $occurrenceResponse['success'] &&
            count($occurrenceResponse['data']) > 0
        ) ? $occurrenceResponse['data'][0]['occurrenceName'] : throw new \Exception('Could not retrieve occurrenceName. Occurrence not found.');

        return $this->render('entry', [
            'program' => $program,
            'id' => $id,
            'experimentDbId' => $experimentDbId,
            'isPacked' => $isPacked,
            'searchModel' => $searchModel,
            'dataProvider' => $output[0],
            'totalCount' => $output[1],
            'paramSort' => $output[2],
            'filters' => $output[3],
            'saveListMembersThresholdValue' => $this->retrieveThresholdValue('saveListMembers'),
            'exportRecordsThresholdValue' => $this->retrieveThresholdValue('exportRecords'),
            'mainCols' => $mainCols,
            'occurrenceName' => $occurrenceName,
            'browserId' => $browserId,
        ]);
    }

    /**
     * Save entries or plots in a new list
     */
    public function actionSaveAsList()
    {
        $occurrenceId = isset($_POST['occurrenceId']) ? $_POST['occurrenceId'] : null;

        // check immediately if data type was specified
        if (!isset($_POST['dataType']) || empty($_POST['dataType']))
            return [
                'success' => false,
                'message' => 'The system could not determine which type of data it is going to save as a list.'
            ];

        $listType = isset($_POST['listType']) ? $_POST['listType'] : '';
        $dataType = isset($_POST['dataType']) ? $_POST['dataType'] : '';

        $attributes = [
            'dataType' => $dataType,
            'abbrev' => isset($_POST['abbrev']) ? $_POST['abbrev'] : '',
            'name' => isset($_POST['name']) ? $_POST['name'] : '',
            'listType' => $listType,
            'displayName' => isset($_POST['displayName']) ? $_POST['displayName'] : '',
            'description' => isset($_POST['description']) ? $_POST['description'] : '',
            'remarks' => isset($_POST['remarks']) ? $_POST['remarks'] : '',
            'isSelectAllChecked' => isset($_POST['isSelectAllChecked']) ? (($_POST['isSelectAllChecked'] === 'true') ? true : false) : false,
        ];

        $data = $this->saveAsList($occurrenceId, $attributes, $dataType);
        return json_encode($data);
    }

    /**
     * Save entries or plots into a new list
     * @param int $occurrenceId occurrence identifier
     * @param array $attributes parameters for the API call to create a new list
     * and a data type to determine whether retrieve entries or plots
     * @param string $dataType whether entry or plot
     *
     * @return array array-object containing success status and Toast message
     */
    private function saveAsList ($occurrenceId, $attributes, $dataType)
    {
        // create new list
        $dbIds = $_POST['dbIds'] ? json_decode($_POST['dbIds']) : [];
        $selectAllMode = $_POST['selectAllMode'] ?? 'include';
        $filterObject = null;

        $table = 'entry';
        $endpoint = 'entries';

        // retrieve sorting
        $sorting = Yii::$app->session->get('occurrence-'. $table .'-param-sort' . $occurrenceId);

        if ($dataType == 'plots') {
            $table = 'plot';
            $endpoint = 'plot-data-table';
            $sorting = '';
        }

        // retrieve filters from either isSelectAllChecked flag, session, or $_POST
        $filters = Yii::$app->session->get('occurrence-' . $table . '-filters' . $occurrenceId);

        if (!empty($filters)) {
            $filterObject = [
                'filters' => $filters,
                'additionalFields' => $this->generateFields(
                    array_keys($filters),
                    "occurrences/$occurrenceId/$endpoint-search"
                ),
            ];
        }

        return $this->saveList->saveAsList('occurrence', $occurrenceId, $attributes, $dbIds, $selectAllMode, $filterObject, $sorting);
    }

    /**
     * Retrieve threshold value for BG processing
     *
     * @param string $featureName identifier
     *
     * @return int $thresholdValue
     */
    public function retrieveThresholdValue ($featureName = null)
    {
        $featureName = ($featureName) ? $featureName : $_POST['featureName'];

        return Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', $featureName, '&sort=configurationDbId:asc');
    }

    /**
     * View plots of an occurrence
     *
     * @param text $program program identifier
     * @param integer $id occurrence identifier
     */
    public function actionPlot($program, $id)
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        // Get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($id);
        $creatorDbId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;

        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess(
                'EXPERIMENT_MANAGER_VIEW_OCCURRENCE',
                'OCCURRENCES',
                $creatorDbId,
                usage: 'url'
            );
        }
    
        $userId = $this->user->getUserId();
        $method = __METHOD__;
        $browserId = 'dynagrid-em-occurrence-plot';
        // Time limit value for the cached data
        $timeToLive = $this->configuration->getConfigByAbbrev('REDIS_DEFAULT_TIME_TO_LIVE')['value'];

        /* Exclude Attributes based on Design */
        // Get experimentDesignType and occurrenceName
        $occurrenceResponse = $this->occurrence->searchAll(
            [ 'occurrenceDbId' => "equals $id" ],
            'limit=1'
        );

        $design = strtolower($occurrenceResponse['data'][0]['occurrenceDesignType']) ?? '';

        if(empty($design)){
            $design = strtolower($occurrenceResponse['data'][0]['experimentDesignType']) ?? '';
        }

        $occurrenceName = (isset($occurrenceResponse['data'][0]['occurrenceName'])) ?
            $occurrenceResponse['data'][0]['occurrenceName'] :
            throw new \Exception('Could not retrieve occurrenceName. Occurrence not found.');

        $excludedAttributes = [];

        if ($design == 'row-column') {
            // For Row-Column design, display row and col block no.
            $excludedAttributes = ['blockNumber'];
        } elseif (in_array($design, ['alpha-lattice', 'augmented rcbd', 'unspecified'])) {
            // Display block number
            $excludedAttributes = ['rowBlockNumber', 'colBlockNumber'];
        } else {
            // Exclude block columns for other design types
            $excludedAttributes = ['rowBlockNumber', 'colBlockNumber','blockNumber'];
        }

        // Include attributes based on experiment type
        $expType = $occurrenceResponse['data'][0]['experimentType'] ?? '';
        if (strtolower($expType) === 'observation') {
            $excludedAttributes = array_diff($excludedAttributes, ['blockNumber']);
        }

        $searchModel = $this->plot;

        $queryParams = Yii::$app->request->queryParams;
        $queryParams['Plot']['occurrenceDbId'] = "$id";

        // Load query params so that data browser can display textfields with input
        $searchModel->load($queryParams);

        // Get cached params to compare to $queryParams
        $cachedParams = Yii::$app->cache->get("$userId:$method:$browserId:$id:params");
        $cachedParams = $cachedParams ? $cachedParams : [];

        if (
            !$cachedParams ||
            ($cachedParams && $cachedParams != $queryParams)
        ) {
            // Set/Overwrite cached params with new params
            Yii::$app->cache->set(
                "$userId:$method:$browserId:$id:params",
                $queryParams,
                $timeToLive
            );
            
            // Set/Overwrite cached data with newly retrieved data
            Yii::$app->cache->set(
                "$userId:$method:$browserId:$id",
                $searchModel->search($queryParams) ?? null,
                $timeToLive,
                new TagDependency(['tags' => "$browserId-$id"])
            );
        }

        // If cached data exists, Get data
        // If not, Set retrieved data from search() into cache...
        //...then return the newly retrieved data to $output
        $output = Yii::$app->cache->getOrSet(
            "$userId:$method:$browserId:$id",
            function () use ($searchModel, $queryParams) {
                return $searchModel->search($queryParams) ?? null;
            },
            $timeToLive,
            new TagDependency(['tags' => "$browserId-$id"])
        );

        $traits = $searchModel->traits;

        // Get plot columns
        $configCols = $this->browserController->getColumns(
            $program,
            'VIEW_OCCURRENCE_DATA',
            'VIEW_PLOT_COLUMNS',
            $excludedAttributes,
            'view'
        );

        // By default, set blockNumber to be visible for Observation experiments
        if (strtolower($expType) === 'observation') {
            foreach ($configCols as &$configCol) {
                if ($configCol['attribute'] === 'blockNumber') {
                    $configCol['visible'] = true;
                }
            }
        }

        return $this->render('plot', [
            'program' => $program,
            'id' => $id,
            'browserId' => $browserId,
            'searchModel' => $searchModel,
            'dataProvider' => $output[0],
            'traits' => $traits,
            'totalCount' => $output[1],
            'paramSort' => $output[2],
            'filters' => $output[3],
            'exportFilters' => $output[4],
            'saveListMembersThresholdValue' => $this->retrieveThresholdValue('saveListMembers'),
            'exportRecordsThresholdValue' => $this->retrieveThresholdValue('exportRecords'),
            'configCols' => $configCols,
            'occurrenceName' => $occurrenceName,
        ]);
    }

    /**
     * View protocols of an occurrence
     *
     * @param string $program - Program identifier
     * @param integer $id - Unique Occurrence identifier
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function actionProtocol ($program, $id)
    {        
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        // Get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($id);
        $creatorDbId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;
        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess('EXPERIMENT_MANAGER_VIEW_OCCURRENCE', 'OCCURRENCES', $creatorDbId, usage: 'url');
        }

        $experimentDbId = '';
        $experimentType = '';
        $dataProcessDbId = '';
        $dataProcessAbbrev = '';

        // Retrieve experimentDbId and dataProcessDbId
        $occurrenceResponse = $this->occurrence->searchAll(
            [ 'occurrenceDbId' => "equals $id" ],
            'limit=1'
        );

        if (
            $occurrenceResponse['success'] &&
            isset($occurrenceResponse['data']) &&
            !empty($occurrenceResponse['data'])
        ) {
            $experimentDbId = $occurrenceResponse['data'][0]['experimentDbId'];
            $experimentType = $occurrenceResponse['data'][0]['experimentType'];
            $dataProcessDbId = $occurrenceResponse['data'][0]['dataProcessDbId'];
            $dataProcessAbbrev = $occurrenceResponse['data'][0]['dataProcessAbbrev'];
            $occurrenceName = $occurrenceResponse['data'][0]['occurrenceName'];
        } else {
            throw new \Exception('Occurrence not found.');
        }

        // Retrieve protocol parameters and config
        $protocolParams = $this->specifyProtocols(
            $experimentDbId,
            $experimentType,
            $dataProcessDbId,
            $dataProcessAbbrev,
            $program
        );

        $protocolParams = array_merge($protocolParams, [
            'occurrenceDbId' => $id,
            'experimentDbId' => $experimentDbId,
            'dataProcessDbId' => $dataProcessDbId,
            'occurrenceName' => $occurrenceName,
            'program' => $program,
        ]);

        return $this->render('@app/modules/occurrence/views/view/protocol/protocol.php', $protocolParams);
    }

    /**
     * Specify Protocols of an Occurrence
     *
     * @param integer $experimentDbId Unique Experiment identifier
     * @param string $experimentType Experiment type identifier
     * @param integer $dataProcessDbId Unique Data Process identifier
     * @param string $dataProcessAbbrev Data Process abbreviation identifier
     * @param string $program Program identifier
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function specifyProtocols ($experimentDbId, $experimentType, $dataProcessDbId, $dataProcessAbbrev, $program)
    {
        if (
            (
                $dataProcessAbbrev !== 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS' ||
                (
                    stripos($experimentType, 'nursery') === false &&
                    !empty($entryCountAssignedToBlock)
                )
            ) ||
            stripos($experimentType, 'trial') === false
        ) {
            $activities = $this->formModel->prepare(
                'specify-protocols',
                $program,
                $experimentDbId,
                $dataProcessDbId
            );
            
            $index = array_search(
                $activities['active'] ?? '',
                array_column($activities['children'] ?? [],'url')
            );
         
            $activityChildren = $activities['children'][$index]['children'] ?? [];
           
            $viewArr = [];
            
            if ($dataProcessAbbrev == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS') {
                $viewArr []= [
                    'display_name' => 'Planting',
                    'action_id' => 'planting-protocols',
                    'item_icon' => 'fa fa-pagelines',
                ];
            }

            if (!empty($activityChildren) && sizeof($activityChildren) > 0) {
               foreach ($activityChildren as $value) {
                    $viewArr []= [
                        'display_name' => $value['display_name'],
                        'action_id' => $value['action_id'],
                        'item_icon' => $value['item_icon']
                    ];
               }
            }

            return [
                'viewArr' => $viewArr,
                'protocolChildren' => $activityChildren
            ];
        }
    }

    /**
     * View occurrence design layout
     *
     * @param text $program program identifier
     * @param integer $id occurrence identifier
     */
    public function actionLayout ($program, $id)
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        // Get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($id);
        $creatorDbId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;
        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess(
                'EXPERIMENT_MANAGER_VIEW_OCCURRENCE',
                'OCCURRENCES',
                $creatorDbId,
                usage: 'url'
            );
        }

        $occurrenceResponse = $this->occurrence->searchAll(
            [ 'occurrenceDbId' => "equals $id" ],
            'limit=1'
        );

        if (!$occurrenceResponse['success'] || !$occurrenceResponse['data']) {
            throw new \Exception('Occurrence not found.');
        }

        $occurrenceName = $occurrenceResponse['data'][0]['occurrenceName'];

        return $this->render('layout', [
            'program' => $program,
            'id' => $id,
            'occurrenceName' => $occurrenceName,
        ]);
    }

    /**
     * @param Integer $id Occurrence identifier
     * @param Text $label Field to be displayed
     * @param Boolean $flipped Starting position whether top or bottom
     * @param Integer $occurrenceDbId Occurrence identifier
     *
     * @return Array design layout of an Occurrence
     */
    public function actionRenderLayout($id, $label='plotno', $flipped=true, $occurrenceDbId=null){
        $panelWidth = $_POST['panelWidth'] ?? 0;

        // retrieve occurrence layout data
        $data = $this->fieldLayout->getLayoutDataByOccurrenceId($id);

        // if layout is invalid
        if(empty($data)){
            $htmlData = $this->renderPartial('_layout_preview.php',[
                'data' => 'invalid',
            ],true,false);

            return json_encode([
                'success'=> 'success',
                'htmlData' => $htmlData,
            ]);
        }

        $layoutArray = $data['layout'];
        $occurrenceName = $data['occurrenceName'];
        $occurrenceDbId = $data['occurrenceDbId'];
        $occurrencesList = $data['occurrencesList'];

        // check first plot position
        $flipped = $this->fieldLayout->getLayoutFirstPlotPosition($id);

        $htmlData = $this->renderPartial('_layout_preview.php',[
            'data' => $layoutArray['dataArrays'],
            'label'=> $label,
            'startingPlotNo' => 1,
            'panelWidth' => $panelWidth,
            'noOfRows' => $layoutArray['maxRows'] == NULL ? 0:$layoutArray['maxRows'],
            'noOfCols' => $layoutArray['maxCols'] == NULL ? 0:$layoutArray['maxCols'],
            'statusDesign' => 'completed',
            'deletePlots' => 'true',
            'planStatus' => 'randomized',
            'flipped' => $flipped,
            'occurrencesList' => $occurrencesList,
            'occurrenceName' => $occurrenceName,
            'occurrenceDbId' => $occurrenceDbId,
            'xAxis' => $layoutArray['xAxis'],
            'yAxis' => $layoutArray['yAxis'],
            'maxBlk' => $layoutArray['maxBlk']
        ],true,false);

        return json_encode([
            'success'=> 'success',
            'htmlData' => $htmlData,
        ]);
    }

    /**
     * Update the layout view based on the selected occurrence
     * 
     * @return array layout information
     */
    public function actionChangeOccurrenceLayout(){

        $occurrenceDbId = $_POST['occurrenceDbId'];

        $fullScreen = false;
        if($_POST['fullScreen'] == 1){
            $fullScreen = true;
        }

        $layoutArray = $this->expModel->generateLayout($occurrenceDbId, null, $fullScreen, 'design');

        return json_encode($layoutArray);
    }

    /**
     * View crosses of an occurrence
     *
     * @param text $program program identifier
     * @param integer $id occurrence identifier
     */
    public function actionCrosses ($program, $id)
    {
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        // Get access data and creator of the occurrence record
        $occurrenceObj = $this->occurrence->get($id);
        $creatorDbId = isset($occurrenceObj['creatorDbId']) ? $occurrenceObj['creatorDbId'] : null;

        // Check render access
        if (!$isAdmin) {
            Yii::$app->access->renderAccess(
                'EXPERIMENT_MANAGER_VIEW_OCCURRENCE',
                'OCCURRENCES',
                $creatorDbId,
                usage: 'url'
            );
        }

        $searchModel = $this->crossSearch;
        $paramFilter = Yii::$app->request->queryParams;

        $occurrenceResponse = $this->occurrence->searchAll(
            [ 'occurrenceDbId' => "equals $id" ],
            'limit=1'
        );

        if (!$occurrenceResponse['success'] || !$occurrenceResponse['data']) {
            throw new \Exception('Occurrence not found.');
        }

        $occurrenceName = $occurrenceResponse['data'][0]['occurrenceName'];

        $mainParam = [
            'occurrenceDbId' => "equals $id",
            'includeParentSource' => true,
            'retrieveDataFromPI' => true
        ];

        $dataProvider = $searchModel->search($mainParam, $paramFilter, true, null, true);

        return $this->render('crosses', [
            'program' => $program,
            'id' => $id,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'totalCount' => $dataProvider->totalCount,
            'exportCrossesThresholdValue' => $this->retrieveThresholdValue('exportCrossRecords'),
            'params' => $mainParam,
            'occurrenceName' => $occurrenceName,
        ]);
    }

    /**
     * Export crosses information in CSV format
     */
    public function actionExportCrosses($id)
    {
        $filters = Yii::$app->session->get('occurrence-cross-filters' . $id) ?? null;
        $params = Yii::$app->session->get('occurrence-cross-params' . $id) ?? null;
        $csvContent = [[]];
        $i = 1;

        // add trait CSV headers
        $csvHeaders = [
            'CROSS_ID',
            'CROSS NAME',
            'CROSS LIST NAME',
            'FEMALE PARENT',
            'FEMALE PARENTAGE',
            'MALE PARENT',
            'MALE PARENTAGE',
            'CROSS METHOD',
            'CROSS REMARKS',
            'FEMALE ENTRY NO.',
            'MALE ENTRY NO.',
            'FEMALE ENTRY CODE',
            'MALE ENTRY CODE',
            'FEMALE OCCURRENCE',
            'MALE OCCURRENCE',
            'FEMALE OCCURRENCE CODE',
            'MALE OCCURRENCE CODE',
            'FEMALE SEED SOURCE',
            'MALE SEED SOURCE',
            'FEMALE SOURCE PLOT',
            'MALE SOURCE PLOT',
            'FEMALE SOURCE PLOT ID',
            'MALE SOURCE PLOT ID',
            'HARVEST STATUS'
        ];

        $crossAttributes = [
            'crossDbId',
            'crossName',
            'entryListName',
            'crossFemaleParent',
            'femaleParentage',
            'crossMaleParent',
            'maleParentage',
            'crossMethod',
            'crossRemarks',
            'femaleParentEntryNumber',
            'maleParentEntryNumber',
            'femalePIEntryCode',
            'malePIEntryCode',
            'femaleOccurrenceName',
            'maleOccurrenceName',
            'femaleOccurrenceCode',
            'maleOccurrenceCode',
            'femaleParentSeedSource',
            'maleParentSeedSource',
            'femaleSourcePlot',
            'maleSourcePlot',
            'femaleSourcePlotDbId',
            'maleSourceEntryDbId',
            'harvestStatus'
        ];

        // retrieve all Entries of an Occurrence
        $crosses = $this->api->getApiResults(
            'POST',
            'crosses-search',
            ($params) ? json_encode($params) : null,
            '',
            true
        );

        if (!empty($crosses['data']) && isset($crosses['data'])) {
            foreach ($csvHeaders as $key => $value)
                array_push($csvContent[0], $value);

            foreach ($crosses['data'] as $key => $value) {
                $csvContent[$i] = [];

                foreach ($crossAttributes as $k => $v)
                    array_push($csvContent[$i], $value[$v]);

                $i += 1;
            }
        }

        // retrieve occurrence number for CSV filename
        $occurrenceParams = [
            'fields' => 'occurrence.id AS id |'.
                'occurrence.occurrence_name AS occurrenceName |'.
                'occurrence.occurrence_number AS occurrenceNumber |'.
                'experiment.experiment_code AS experimentCode',
            'id' => "equals $id",
        ];
        $occurrence = $this->api->getApiResults(
            'POST',
            'occurrences-search?limit=1',
            json_encode($occurrenceParams),
            '',
            true
        );

        $toExport = new FileExport(FileExport::CROSS_LIST, FileExport::CSV, time());

        $experimentCode = $this->fieldLayout->getValidFileName($occurrence['data'][0]['experimentCode']);
        $occurrenceNumber = $occurrence['data'][0]['occurrenceNumber'];
        $toExport->addExperimentData($experimentCode, [$occurrenceNumber], [$csvContent]);

        $buffer = $toExport->getUnzipped();
        $filename = array_key_first($buffer);
        $content = $buffer[$filename];

        header("Content-Disposition: attachment; filename={$filename}; Content-Type: application/csv;");

        $fp = fopen('php://output', 'wb');

        if ($fp === false) die('Failed to create CSV file.');

        foreach ($content as $line) fputcsv($fp, $line);

        fclose($fp);

        exit;
    }

    /**
     * Export plots information in CSV format on occurrence-level
     *
     * @param int $id occurrence identifier
     */
    public function actionExportPlots($program, $id)
    {
        Yii::info('Exporting file '.json_encode(['occurrenceDbId'=>$id]), __METHOD__);

        $traitLabels = Yii::$app->session->get('occurrence-plot-trait-labels' . $id) ?? [];
        $traitAttributes = Yii::$app->session->get('occurrence-plot-trait-attributes' . $id) ?? [];
        $exportFilters = Yii::$app->session->get('occurrence-plot-export-filters' . $id) ?? [];
        $paramSort = Yii::$app->session->get('occurrence-plot-param-sort' . $id) ?? '';
        $csvContent = [[]];
        $i = 1;

        // retrieve occurrence experiment design type and experiment type for column processing
        $occurrenceParams = [
            'fields' => 'occurrence.id AS id |
                experiment.experiment_design_type AS experimentDesignType |
                experiment.experiment_type as experimentType|
                occurrence.occurrence_design_type AS occurrenceDesignType',
            'id' => "equals $id",
        ];
        $occurrence = $this->api->getApiResults(
            'POST',
            'occurrences-search?limit=1',
            json_encode($occurrenceParams),
            '',
            true
        );

        $design = $occurrence['data'][0]['experimentDesignType'];
        $design = (is_null($design) || $design == '')? $occurrence['data'][0]['occurrenceDesignType'] : $design;
        $design = strtolower($design);
        $excludedAttributes = [];

        // Exclude Attributes based on Experiment Design
        if ($design == 'row-column') {
            // For Row-Column design, display row and col block no.
            $excludedAttributes = ['blockNumber'];
        } else if (in_array($design, ['alpha-lattice', 'augmented rcbd', 'unspecified'])) {
            // Display block number
            $excludedAttributes = ['rowBlockNumber', 'colBlockNumber'];
        } else {
            // Exclude block columns for other design types
            $excludedAttributes = ['rowBlockNumber', 'colBlockNumber', 'blockNumber'];
        }

        // Include attributes based on experiment type
        $expType = $occurrence['data'][0]['experimentType'] ?? '';
        if (strtolower($expType) === 'observation') {
            $excludedAttributes = array_diff($excludedAttributes, ['blockNumber']);
        }

        // Build CSV Attributes and Headers
        [
            $csvAttributes,
            $csvHeaders,
        ] = $this->browserController->getColumns($program, 'VIEW_OCCURRENCE_DATA', 'VIEW_PLOT_COLUMNS', $excludedAttributes, 'download');

        $csvAttributes = array_merge($csvAttributes, $traitAttributes);
        $csvHeaders = array_merge($csvHeaders, $traitLabels);

        // retrieve all Plots of an Occurrence
        $plots = $this->api->getApiResults(
            'POST',
            "occurrences/$id/plot-data-files-search?ownershipType=shared&$paramSort",
            ( empty($exportFilters) ) ? null : json_encode($exportFilters),
            '',
            true
        );

        $plotRows = $plots['data'] ?? [];

        // Include columns based on the configuration
        if (!empty($plotRows) && isset($plotRows)) {
            foreach ($csvHeaders as $key => $value)
                array_push($csvContent[0], $value);

            foreach ($plotRows as $key => $value) {
                $csvContent[$i] = [];

                foreach ($csvAttributes as $k => $v)
                    array_push($csvContent[$i], ($value[$v] ?? ''));

                $i += 1;
            }
        }

        // redirect to previous page if data retrieval fails
        if(empty($occurrence['success'])){
            $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl)->send();
            exit;
        }

        // get file name from config by occurrence ID
        $filename = $this->browserController->getFileNameFromConfigByEntityId($id, 'ENTRY_LIST_LOCATION', FileExport::CSV);

        header("Content-Disposition: attachment; filename={$filename};");
        header("Content-Type: application/csv;");

        $fp = fopen('php://output', 'wb');

        if ($fp === false) die('Failed to create CSV file.');

        foreach ($csvContent as $line) fputcsv($fp, $line);

        fclose($fp);

        exit;
    }

    /**
     * Export plots information in CSV format on location-level
     *
     * @param string $id ID of the current location
     * @param string $type file type to export file as
     * @param string $program program abbrev identifier
     * @param string $redirectUrl URL to redirect to in case API call fails
     */
    public function actionExportLocationPlots($id, $type='csv', $program='', $redirectUrl='')
    {
        // This is for export location level plots using BACKGROUND WORKERS
        $redirectUrl = empty($redirectUrl) ? "index?program={$program}" : $redirectUrl;

        Yii::info('Exporting file '.json_encode(['occurrenceDbId'=>$id, 'fileType'=>$type,
                'program'=>$program, 'redirectUrl'=>$redirectUrl]), __METHOD__);

        $traitAttributes = Yii::$app->session->get('location-plot-trait-attributes' . $id) ?? [];
        $traitLabels = Yii::$app->session->get('location-plot-trait-labels' . $id);
        $traitLabels = (!$traitLabels) ? [] : $traitLabels;

        $exportFilters = Yii::$app->session->get('location-plot-export-filters' . $id) ?? [];
        $exportFilters['userId'] = "{$this->user->getUserId()}";

        // Get location information
        $location = $this->api->getApiResults(
            'POST',
            'locations-search',
            json_encode([
                'locationDbId' => "equals $id",
            ]),
            '?limit=1',
            true
        )['data'][0];

        $locationName = $location['locationName'];

        // Build CSV Attributes and Headers
        [
            $csvAttributes,
            $csvHeaders,
        ] = $this->browserController->getColumns($program, 'VIEW_LOCATION_DATA', 'VIEW_PLOT_COLUMNS', [], 'download');

        $csvAttributes = array_merge($csvAttributes, $traitAttributes);
        $csvHeaders = array_merge($csvHeaders, $traitLabels);

        $description = 'Preparing location-level plot records for downloading';
        $timestamp = date("Ymd_His", time());
        $filename = 'View_Location_Plots_'.$timestamp;

        // Invoke worker
        $this->worker->invoke(
            'BuildCsvData',
            $description,
            'LOCATION',
            "$id",
            'LOCATION',
            'OCCURRENCES',
            'POST',
            [
                'url' => "locations/$id/plot-data-files-search?ownershipType=shared",
                'requestBody' => json_encode($exportFilters),
                'description' => $description,
                'csvHeaders' => $csvHeaders,
                'csvAttributes' => $csvAttributes,
                'fileName' => $filename,
                'httpMethod' => 'POST',
            ],
            [
                'remarks' => $filename,
            ]
        );

        // Reload page
        $program = Yii::$app->session->get('program');
        Yii::$app->response->redirect(["/occurrence?program=$program&OccurrenceSearch[locationDbId]=$id"]);
        $notif = 'info';
        $icon = 'info';
        $message = "The location-level plot records for <b>$locationName</b> are being prepared in the background for download. You may proceed with other tasks. You will be notified in this page once done.";
        Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);

    }

    /**
     * Export plots information in CSV format on location-level
     *
     * @param string $id ID of the current location
     * @param string $type file type to export file as
     * @param string $program program abbrev identifier
     * @param string $redirectUrl URL to redirect to in case API call fails
     */
    public function actionExportNonworkerLocationPlots($id, $type='csv', $program='', $redirectUrl='')
    {
        // This is for export location level plots without BACKGROUND WORKERS
        $redirectUrl = empty($redirectUrl) ? "index?program={$program}" : $redirectUrl;

        Yii::info('Exporting file '.json_encode(['occurrenceDbId'=>$id, 'fileType'=>$type,
                'program'=>$program, 'redirectUrl'=>$redirectUrl]), __METHOD__);

        $traitAttributes = Yii::$app->session->get('location-plot-trait-attributes' . $id) ?? [];
        $traitLabels = Yii::$app->session->get('location-plot-trait-labels' . $id);
        $traitLabels = (!$traitLabels) ? [] : $traitLabels;

        $exportFilters = Yii::$app->session->get('location-plot-export-filters' . $id) ?? [];
        $exportFilters['userId'] = "{$this->user->getUserId()}";
        $paramSort = Yii::$app->session->get('location-plot-param-sort' . $id);
        $csvContent = [[]];
        $i = 1;

        // Build CSV Attributes and Headers
        [
            $csvAttributes,
            $csvHeaders,
        ] = $this->browserController->getColumns($program, 'VIEW_LOCATION_DATA', 'VIEW_PLOT_COLUMNS', [], 'download');

        $csvAttributes = array_merge($csvAttributes, $traitAttributes);
        $csvHeaders = array_merge($csvHeaders, $traitLabels);

        // retrieve all Plots of the Location
        $plots = $this->api->getApiResults(
            'POST',
            'locations/' . $id . '/plot-data-files-search?ownershipType=shared&' . $paramSort,
            (empty($exportFilters)) ? null : json_encode($exportFilters),
            '',
            true
        );

        $plotRows = $plots['data'] ?? [];

        // Include columns based on the configuration
        if (!empty($plotRows) && isset($plotRows)) {
            foreach ($csvHeaders as $key => $value)
                array_push($csvContent[0], $value);

            foreach ($plotRows as $key => $value) {
                $csvContent[$i] = [];

                foreach ($csvAttributes as $k => $v)
                    array_push($csvContent[$i], $value[$v]);

                $i += 1;
            }
        }

        // get file name from config by location ID
        $filename = $this->browserController->getFileNameFromConfigByEntityId($id, 'PLOT_LIST_LOCATION', FileExport::CSV, 'location');

        header("Content-Disposition: attachment; filename={$filename};");
        header("Content-Type: application/csv;");

        $fp = fopen('php://output', 'wb');

        if ($fp === false) die('Failed to create CSV file.');

        foreach ($csvContent as $line) fputcsv($fp, $line);

        fclose($fp);

        exit;
    }

    /**
     * Export entries information in CSV format
     *
     * @param integer $id Occurrence identifier
     */
    public function actionExportEntries($program, $id)
    {
        $filters = Yii::$app->session->get('occurrence-entry-filters' . $id) ?? null;
        $paramSort = Yii::$app->session->get('occurrence-entry-param-sort' . $id) ?? '';
        $csvContent = [[]];
        $i = 1;

        [
            $entryAttributes,
            $csvHeaders,
        ] = $this->browserController->getColumns($program, 'VIEW_OCCURRENCE_DATA', 'VIEW_ENTRY_COLUMNS', [], 'download');

        $filters['fields'] = $this->generateFields($entryAttributes, "occurrences/$id/entries-search");

        // retrieve all Entries of an Occurrence
        $entries = $this->api->getApiResults(
            'POST',
            "occurrences/$id/entries-search?$paramSort",
            json_encode($filters),
            '',
            true
        );

        if (!empty($entries['data']) && isset($entries['data'])) {
            foreach ($csvHeaders as $key => $value)
                array_push($csvContent[0], $value);

            foreach ($entries['data'] as $key => $value) {
                $csvContent[$i] = [];

                foreach ($entryAttributes as $k => $v)
                    array_push($csvContent[$i], $value[$v]);

                $i += 1;
            }
        }

        // get file name from config by occurrence ID
        $fileName = $this->browserController->getFileNameFromConfigByEntityId($id, 'ENTRY_LIST_OCCURRENCE', FileExport::CSV);

        header("Content-Disposition: attachment; filename={$fileName}; Content-Type: application/csv;");

        $fp = fopen('php://output', 'wb');

        if ($fp === false) die('Failed to create CSV file.');

        foreach ($csvContent as $line) fputcsv($fp, $line);

        fclose($fp);

        exit;
    }

    /**
     * Search for entries and plots associated with an occurrence.
     *
     * @param int $occurrenceId Occurrence unique identifier
     * @param string $entity determines whether retrieve DB IDs for entry or plot
     * @param string $listType optional; list type identifier
     * 
     * @return array array of entry or plot DB IDs
     */
    public function actionRetrieveDbIds ($occurrenceId, $entity, $listType = null)
    {
        $listType = (!$listType and isset($_POST['listType'])) ? $_POST['listType'] : $listType;
        $paramSort = '';
        $filters = null;
        $defaultFieldsParams = '';
        $url = '';
        $checkedBoxesString = isset($_POST['checkedBoxesString']) ?  $_POST['checkedBoxesString'] : '';
        $filterAttribute = "{$entity}DbId";
        $attribute = "{$entity}DbId";
        $attributeMap = null;

        // Set map of attributes for buildFieldsParam()
        if ($entity == 'entry') {
            $attributeMap = $this->entryAttributes;
        } else if ($entity == 'plot') {
            $attributeMap = $this->plotAttributes;
        }

        // Set variables for buildFieldsParam() and API request
        $paramSort = Yii::$app->session->get("occurrence-$entity-param-sort$occurrenceId");
        $filters = Yii::$app->session->get("occurrence-$entity-filters$occurrenceId");

        // Add string of entryDbIds or plotDbIds into filters 
        if ($checkedBoxesString) {
            $idArray = explode('|', $checkedBoxesString);
            $checkedBoxesString = '';

            // Add "equals" condition to improve query speed
            foreach ($idArray as $id) {
                $checkedBoxesString .= "equals $id |";
            }

            $filters[$filterAttribute] = $checkedBoxesString;
        }

        if ($entity == 'entry') {
            $defaultFieldsParams = "pi.entry_id AS entryDbId";
            $url = 'occurrences/' . $occurrenceId . '/entries-search?' . $paramSort;
        } else if ($entity == 'plot') {
            $defaultFieldsParams = "plot.id AS plotDbId";
            $url = 'occurrences/' . $occurrenceId . '/plot-data-table-search?ownershipType=shared&' . $paramSort;
        }

        if ($listType == "package") {
            // Check if user selected at least 1 checkbox
            if (isset($filters["{$entity}DbId"])) {
                $defaultFieldsParams = "pi.package_id AS packageDbId";
            } else {
                $defaultFieldsParams = "$defaultFieldsParams | pi.package_id AS packageDbId";
            }

            $attribute = "packageDbId";
        }

        $fieldsParam = $this->browser->buildFieldsParam(
            $attributeMap,
            $defaultFieldsParams,
            $paramSort,
            $filters
        );

        // if there's a fields parameter
        if (!empty($fieldsParam)) {
            $filters['fields'] = $fieldsParam;
        }

        $response = $this->api->getApiResults(
            'POST',
            $url,
            json_encode($filters),
            '',
            true
        );

        $data = [];

        if (
            $response['success'] &&
            isset($response['data']) &&
            !empty($response['data'])
        )
            $data = $response['data'];
        $retrievedDbIdArray = [];

        // return only the DB IDs
        foreach ($data as $key => $value)
            array_push($retrievedDbIdArray, strval($value[$attribute]));

        return json_encode($retrievedDbIdArray);
    }

    /**
     * Delegate building of CSV data to the background worker
     * 
     * @param string $program current program of user
     * @param int|string $occurrenceId unique identifier of an Occurrence
     * @param string $entity name of the entity
     */
    public function actionPrepareDataForCsvDownload ($program, $occurrenceId, $entity = 'entry', $tool = null, $experimentId = null)
    {
        Yii::info('Preparing Data for CSV Download:' . json_encode(['program'=>$program, 'occurrenceId'=>$occurrenceId, 'entity'=>$entity]), __METHOD__);

        $occurrence = $this->api->getApiResults(
            'POST',
            "occurrences-search",
            json_encode([
                'occurrenceDbId' => "equals $occurrenceId",
            ]),
            '?limit=1',
            true
        )['data'][0];
        $occurrenceName = $occurrence['occurrenceName'];
        $url = '';
        $requestBody = null;
        $csvHeaders = [];
        $csvAttributes = [];
        $filename = '';
        // get user ID
        $userId = "{$this->user->getUserId()}";

        if ($entity == 'entry') {
            // Build URL
            $url = "occurrences/$occurrenceId/entries-search?" . Yii::$app->session->get("occurrence-$entity-param-sort" . $occurrenceId);

            // Build request body
            $requestBody = Yii::$app->session->get("occurrence-entry-filters$occurrenceId");
            if ($requestBody) $requestBody['userId'] = $userId;
            else $requestBody = [ 'userId' => $userId ];

            // Build CSV Attributes and Headers
            [
                $csvAttributes,
                $csvHeaders,
            ] = $this->browserController->getColumns($program, 'VIEW_OCCURRENCE_DATA', 'VIEW_ENTRY_COLUMNS', [], 'download');

            // get file name from config by occurrence ID
            $filename = $this->browserController->getFileNameFromConfigByEntityId($occurrenceId, 'ENTRY_LIST_OCCURRENCE', FileExport::CSV);

            // remove extension as required by worker
            $filename = substr($filename, 0, -4);

        } else if ($entity == 'plot') {
            // Get trait labels
            $traitLabels = Yii::$app->session->get('occurrence-plot-trait-labels' . $occurrenceId) ?? [];
            // Get trait attributes
            $traitAttributes = Yii::$app->session->get('occurrence-plot-trait-attributes' . $occurrenceId) ?? [];
            // Get filters
            $filters = Yii::$app->session->get('occurrence-plot-filters' . $occurrenceId);
            // Get sort param
            $paramSort = Yii::$app->session->get('occurrence-plot-param-sort' . $occurrenceId) ?? '';

            // Build URL
            $url = "occurrences/$occurrenceId/plot-data-files-search?ownershipType=shared&$paramSort";

            // Build request body
            $requestBody = $filters ?? null;
            if ($requestBody) $requestBody['userId'] = $userId;
            else $requestBody = [ 'userId' => $userId ];

            $design = $occurrence['experimentDesignType'];
            $design = (is_null($design) || $design == '')? $occurrence['occurrenceDesignType'] : $design;
            $design = strtolower($design);
            $excludedAttributes = [];
    
            if ($design == 'row-column') {
                // For Row-Column design, display row and col block no.
                $excludedAttributes = ['blockNumber'];
            } else if (in_array($design, ['alpha-lattice', 'augmented rcbd', 'unspecified'])) {
                // Display block number
                $excludedAttributes = ['rowBlockNumber', 'colBlockNumber'];
            } else {
                $excludedAttributes = ['rowBlockNumber', 'colBlockNumber','blockNumber'];
            }

            // Include attributes based on experiment type
            $expType = $occurrence['experimentType'] ?? '';

            if (strtolower($expType) === 'observation') {
                $excludedAttributes = array_diff($excludedAttributes, ['blockNumber']);
            }

            // Build CSV Attributes and Headers
            [
                $csvAttributes,
                $csvHeaders,
            ] = $this->browserController->getColumns($program, 'VIEW_OCCURRENCE_DATA', 'VIEW_PLOT_COLUMNS', $excludedAttributes, 'download');

            $csvAttributes = array_merge($csvAttributes, $traitAttributes);
            $csvHeaders = array_merge($csvHeaders, $traitLabels);

            // get file name from config by occurrence ID
            $filename = $this->browserController->getFileNameFromConfigByEntityId($occurrenceId, 'PLOT_LIST_OCCURRENCE', FileExport::CSV);

            // remove extension as required by worker
            $filename = substr($filename, 0, -4);

        } else if ($entity == 'cross') {
            // Build URL
            $url = 'crosses-search';

            // Build request body
            $requestBody = Yii::$app->session->get("occurrence-cross-params$occurrenceId") ?? null;

            // Build CSV Headers
            $csvHeaders = [
                'CROSS_ID',
                'CROSS NAME',
                'CROSS LIST NAME',
                'FEMALE PARENT',
                'FEMALE PARENTAGE',
                'MALE PARENT',
                'MALE PARENTAGE',
                'CROSS METHOD',
                'CROSS REMARKS',
                'FEMALE ENTRY NO.',
                'MALE ENTRY NO.',
                'FEMALE ENTRY CODE',
                'MALE ENTRY CODE',
                'FEMALE OCCURRENCE',
                'MALE OCCURRENCE',
                'FEMALE OCCURRENCE CODE',
                'MALE OCCURRENCE CODE',
                'FEMALE SEED SOURCE',
                'MALE SEED SOURCE',
                'FEMALE SOURCE PLOT',
                'MALE SOURCE PLOT',
                'FEMALE SOURCE PLOT ID',
                'MALE SOURCE PLOT ID',
            ];

            // Build CSV Attributes
            $csvAttributes = [
                'crossDbId',
                'crossName',
                'entryListName',
                'crossFemaleParent',
                'femaleParentage',
                'crossMaleParent',
                'maleParentage',
                'crossMethod',
                'crossRemarks',
                'femaleParentEntryNumber',
                'maleParentEntryNumber',
                'femalePIEntryCode',
                'malePIEntryCode',
                'femaleOccurrenceName',
                'maleOccurrenceName',
                'femaleOccurrenceCode',
                'maleOccurrenceCode',
                'femaleParentSeedSource',
                'maleParentSeedSource',
                'femaleSourcePlot',
                'maleSourcePlot',
                'femaleSourcePlotDbId',
                'maleSourceEntryDbId',
            ];

            // retrieve occurrence number for CSV filename
            $occurrenceParams = [
                'fields' => 'occurrence.id AS id |'.
                    'occurrence.occurrence_name AS occurrenceName |'.
                    'occurrence.occurrence_number AS occurrenceNumber |'.
                    'experiment.experiment_code AS experimentCode',
                'id' => "equals $occurrenceId",
            ];
            $occurrence = $this->api->getApiResults(
                'POST',
                'occurrences-search?limit=1',
                json_encode($occurrenceParams),
                '',
                true
            );

            $experimentCode = $this->fieldLayout->getValidFileName($occurrence['data'][0]['experimentCode']);
            $occurrenceNumber = $occurrence['data'][0]['occurrenceNumber'];

            $toExport = new FileExport(FileExport::CROSS_LIST, FileExport::CSV, time());
            $filename = str_replace(
                '.csv',
                '',
                $toExport->getFilenames(
                    $experimentCode,
                    $toExport->formatSerialCodes([$occurrenceNumber])
                )[0]
            );
        }

        $application = "OCCURRENCES";
        $endpointEntity = "OCCURRENCE";
        // redirect to tool browser
        $redirectUrl = ['/occurrence?program='.$program.'&OccurrenceSearch[occurrenceDbId]='.$occurrenceId];
        $entityDbId = $occurrenceId;
        $description = "Preparing $entity records for downloading";
        if($tool == 'experiment-creation'){
            $application = "EXPERIMENT_CREATION";
            $endpointEntity = "EXPERIMENT";
            $redirectUrl = ['experimentCreation/create/index?program='.$program.'&id='.$experimentId];
            $entityDbId = $experimentId;
        }

        $backgroundJobParams = [
            "workerName" => "BuildCsvData",
            "description" => $description,
            "entity" => strtoupper($entity),
            "entityDbId" => $entityDbId,
            "endpointEntity" => $endpointEntity,
            "application" => $application,
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId,
            'remarks' => $filename,
            'message' => $description,
        ];

        try {
            $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);         
        }

        // if creating transaction is successful
        try {
            if (isset($transaction['status']) && $transaction['status'] == 200) {
                $message = "The $entity records for <b>$occurrenceName</b> are being prepared in the background for download. You may proceed with other tasks. You will be notified in this page once done.";
                // set background processing info to session
                Yii::$app->session->set('em-bg-processs-message', $message);
                Yii::$app->session->set('em-bg-processs', true);
    
                $host = getenv('CB_RABBITMQ_HOST');
                $port = getenv('CB_RABBITMQ_PORT');
                $user = getenv('CB_RABBITMQ_USER');
                $password = getenv('CB_RABBITMQ_PASSWORD');

                $connection = new AMQPStreamConnection($host, $port, $user, $password);
    
                $channel = $connection->channel();
    
                $channel->queue_declare(
                    'BuildCsvData', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                    false,  //passive - can use this to check whether an exchange exists without modifying the server state
                    true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                    false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                    false   //auto delete - queue is deleted when last consumer unsubscribes
                );
    
                $msg = new AMQPMessage(
                    json_encode([
                        'tokenObject' => [
                            'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                            'refreshToken' => ''
                        ],
                        'backgroundJobId' => $transaction["data"][0]["backgroundJobDbId"],
                        'description' => $description,
                        'url' => $url,
                        'requestBody' => json_encode($requestBody),
                        'csvHeaders' => $csvHeaders,
                        'csvAttributes' => $csvAttributes,
                        'fileName' => $filename,
                        'userId' => $userId,
                    ]),
                    array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                );
    
                $channel->basic_publish(
                    $msg,   //message 
                    '', //exchange
                    'BuildCsvData'  //routing key (queue)
                );
                $channel->close();
                $connection->close();
                Yii::$app->session->set("bgp-export-$entity-filename-$occurrenceId", "$occurrenceName-$entity-records");

                if($tool == 'experiment-creation'){
                    $notif = 'info';
                    $icon = 'info';
                    Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
                }
                // effectively exits this function (will trigger a (false) error response in the calling ajax)
                Yii::$app->response->redirect($redirectUrl);
            } else {
                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
            }
        } catch (\Exception $e) {
            \ChromePhp::log('e:', $e);
        }
    }

    /**
     * Generate string of fields for API request body
     *
     * @param array $attributes list of specified attributes
     * @param string $endpoint API URL endpoint
     * 
     * @return string generated string of fields
     */
    public function generateFields ($attributes, $endpoint)
    {
        $occurrenceEntriesAttributes = [
            'entryDbId' => 'pi.entry_id AS entryDbId',
            'entryCode' => 'pi.entry_code AS entryCode',
            'entryNumber' => 'pi.entry_number AS entryNumber',
            'entryName' => 'pi.entry_name AS entryName',
            'entryType' => 'pi.entry_type AS entryType',
            'entryRole' => 'pi.entry_role AS entryRole',
            'entryClass' => 'pi.entry_class AS entryClass',
            'entryStatus' => 'pi.entry_status AS entryStatus',
            'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
            'germplasmCode' => 'g.germplasm_code AS germplasmCode',
            'designation' => 'g.designation',
            'description' => 'e.description',
            'parentage' => 'g.parentage',
            'generation' => 'g.generation',
            'germplasmState' => 'g.germplasm_state AS germplasmState',
            'germplasmType' => 'g.germplasm_type AS germplasmType',
            'seedDbId' => 'pi.seed_id AS seedDbId',
            'seedCode' => 'seed.seed_code AS seedCode',
            'seedName' => 'seed.seed_name AS seedName',
            'packageDbId' => 'pi.package_id AS packageDbId',
            'packageCode' => 'package.package_code AS packageCode',
            'packageLabel' => 'package.package_label AS packageLabel',
            'packageQuantity' => 'package.package_quantity AS packageQuantity',
            'packageUnit' => 'package.package_unit AS packageUnit',
            'occurrenceDbId' => 'occurrence.id AS occurrenceDbId',
            'occurrenceName' => 'occurrence.occurrence_name AS occurrenceName',
            'occurrenceCode' => 'occurrence.occurrence_code AS occurrenceCode',
            'creationTimestamp' => 'pi.creation_timestamp AS creationTimestamp',
            'creatorDbId' => 'creator.id AS creatorDbId',
            'creator' => 'creator.person_name AS creator',
            'modificationTimestamp' => 'pi.modification_timestamp AS modificationTimestamp',
            'modifierDbId' => 'modifier.id AS modifierDbId',
            'modifier' => 'modifier.person_name AS modifier',
        ];

        $occurrencePlotDataTableAttributes = [
            'plotDbId' => 'plot.id AS plotDbId',
            'entryDbId' => 'plot.entry_id AS entryDbId',
            'entryCode' => 'pi.entry_code AS entryCode',
            'entryNumber' => 'pi.entry_number AS entryNumber',
            'entryName' => 'pi.entry_name AS entryName',
            'entryType' => 'pi.entry_type AS entryType',
            'entryRole' => 'pi.entry_role AS entryRole',
            'entryClass' => 'pi.entry_class AS entryClass',
            'entryStatus' => 'pi.entry_status AS entryStatus',
            'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
            'germplasmCode' => 'g.germplasm_code AS germplasmCode',
            'parentage' => 'g.parentage',
            'generation' => 'g.generation',
            'germplasmState' => 'g.germplasm_state AS germplasmState',
            'germplasmType' => 'g.germplasm_type AS germplasmType',
            'seedDbId' => 'pi.seed_id AS seedDbId',
            'seedCode' => 'seed.seed_code AS seedCode',
            'seedName' => 'seed.seed_name AS seedName',
            'packageDbId' => 'pi.package_id AS packageDbId',
            'packageCode' => 'package.package_code AS packageCode',
            'packageLabel' => 'package.package_label AS packageLabel',
            'plotCode' => 'plot_code AS plotCode',
            'plotNumber' => 'plot_number AS plotNumber',
            'plotType' => 'plot_type AS plotType',
            'rep' => 'rep',
            'designX' => 'design_x AS designX',
            'designY' => 'design_y AS designY',
            'plotOrderNumber' => 'plot_order_number AS plotOrderNumber',
            'paX' => 'pa_x AS paX',
            'paY' => 'pa_y AS paY',
            'fieldX' => 'field_x AS fieldX',
            'fieldY' => 'field_y AS fieldY',
            'plotStatus' => 'plot_status AS plotStatus',
            'plotQcCode' => 'plot_qc_code AS plotQcCode',
            'creationTimestamp' => 'pi.creation_timestamp AS creationTimestamp',
            'creatorDbId' => 'creator.id AS creatorDbId',
            'creator' => 'creator.person_name AS creator',
            'modificationTimestamp' => 'plot.modification_timestamp AS modificationTimestamp',
            'modifierDbId' => 'modifier.id AS modifierDbId',
            'modifier' => 'modifier.person_name AS modifier',
            'blockNumber' => 'block_number AS blockNumber',
            'rowBlockNumber' => 'descol1.block_value AS rowBlockNumber',
            'colBlockNumber' => 'descol2.block_value AS colBlockNumber',
        ];

        $fields = '';

        if (preg_match('/occurrences\/.*\/entries-search.*/i', $endpoint)) {
            foreach ($attributes as $value) {
                if (!array_key_exists($value, $occurrenceEntriesAttributes))
                    continue;

                $fields .= "{$occurrenceEntriesAttributes[$value]} |";
            }
        } elseif (preg_match('/occurrences\/.*\/plot-data-.*-search.*/i', $endpoint)) {
            foreach ($attributes as $value) {
                if (!array_key_exists($value, $occurrencePlotDataTableAttributes))
                    continue;

                $fields .= "{$occurrencePlotDataTableAttributes[$value]} |";
            }
        }

        $fields = rtrim($fields, '|');

        return $fields;
    }

    /**
     * Render replace entry form
     *
     * @return Html Replace entry form
     */
    public function actionReplaceEntryForm ()
    {
        $occurrenceDbId = $_POST['id'];
        $entryDbId = $_POST['entryDbId'];

        // Call API
        $occurrenceEntryResponse = $this->api->getApiResults(
            'POST',
            "occurrences/$occurrenceDbId/entries-search",
            json_encode([
                'fields' => 'pi.entry_id AS entryDbId |
                    pi.entry_code AS entryCode |
                    pi.entry_number AS entryNumber |
                    pi.entry_name AS entryName |
                    g.parentage |
                    package.package_label AS packageLabel |
                    package.package_quantity AS packageQuantity |
                    package.package_unit AS packageUnit',
                'entryDbId' => "$entryDbId",
            ]),
            '?limit=1'
        );
        // Get returned row
        $occurrenceEntry = $occurrenceEntryResponse['data'][0];

        // Call API
        $plantingJobEntryResponse = $this->api->getApiResults(
            'POST',
            "planting-job-entries-search",
            json_encode([
                'fields' => 'o.id AS occurrenceDbId |
                    et.id AS entryDbId |
                    pj.planting_job_code AS plantingJobCode |
                    pje.required_package_quantity AS requiredPackageQuantity |
                    pje.required_package_unit AS requiredPackageUnit',
                'occurrenceDbId' => "equals $occurrenceDbId",
                'entryDbId' => "equals $entryDbId",
            ]),
            '?limit=1'
        );
        // Get returned row
        $plantingJobEntry = ( empty($plantingJobEntryResponse['data'])) ? [] : $plantingJobEntryResponse['data'][0];
        $isInAPackingJob = !empty($plantingJobEntry);

        $data = [
            'info' => [
                'isReplaced' => null,
                'replacementType' => null,
                'plantingJobCode' => $isInAPackingJob ? $plantingJobEntry['plantingJobCode'] : null,
            ],
            'entry' => [
                'entryNumber' => $occurrenceEntry['entryNumber'],
                'entryCode' => $occurrenceEntry['entryCode'],
                'entryName' => $occurrenceEntry['entryName'],
                'parentage' => $occurrenceEntry['parentage'],
            ],
            'quantity' => [
                'requiredPackageQuantity' => $isInAPackingJob ? number_format($plantingJobEntry['requiredPackageQuantity']) . ' ' . $plantingJobEntry['requiredPackageUnit'] : null,
                'available' => number_format($occurrenceEntry['packageQuantity']) . ' ' . $occurrenceEntry['packageUnit'],
            ],
            'package' => [
                'packageLabel' => $occurrenceEntry['packageLabel'],
                'packageQuantity' => number_format($occurrenceEntry['packageQuantity']) . ' ' . $occurrenceEntry['packageUnit'],
            ],
        ];
        // set summary to session
        Yii::$app->session->set('original-occurrence-entry-data', $data);

        // retrieve replacement type scale values
        $varRecord = $this->variable->getVariableByAbbrev('REPLACEMENT_TYPE');
        $scaleValues = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], null);

        $viewFile = '_replace_entry_form';

        return $this->renderAjax($viewFile,[
            'data' => $data,
            'scaleValues' => $scaleValues,
            'isPreview' => false,
            'isBulkReplacement' => false,
        ]);
    }

    /**
     * Render replace entry selection form
     * Depending on selected replacement option
     *
     * @return Html Entry selection form
     */
    public function actionReplaceEntrySelectionForm ()
    {
        $experimentId = $_POST['experimentId'];
        $replacementType = $_POST['replacementType'];
        $entryIdToReplace = $_POST['entryIdToReplace'];
        $germplasmIdToReplace = $_POST['germplasmIdToReplace'];
        $packageIdToReplace = isset($_POST['packageIdToReplace']) ? $_POST['packageIdToReplace'] : null;
        $plantingJobEntriesDataProvider = null;
        $germplasmName = $_POST['germplasmName'] ?? '';
        $parentage = '';

        $viewFile = '_replace_entry_selection_form';

        // Render view file with dropdown for Replace with Germplasm
        if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM') {
            $inputListFields = $this->variableFilterComponent->retrieveInputListConfig();
    
            $fieldTypeOptions = [];
            $fieldTypeOptionTooltip = [];
        
            if (isset($inputListFields) && !empty($inputListFields)) {
                foreach ($inputListFields as $key=>$option) {
                    $fieldTypeOptions[$key] = $option['text'];
                    $fieldTypeOptionTooltip[$key] = $option['description'];
                }
            }

            return json_encode([
                'data' => $this->renderAjax($viewFile, [
                    'plantingJobEntriesDataProvider' => $plantingJobEntriesDataProvider,
                    'germplasmName' => $germplasmName,
                    'parentage' => $parentage,
                    'entity' => 'germplasm',
                    'fieldTypeOptions' => $fieldTypeOptions,
                    'fieldTypeOptionTooltip' => $fieldTypeOptionTooltip,
                    'packageIdToReplace' => $packageIdToReplace,
                ])
            ]);
        }

        // get selection form data provider and columns
        $data = $this->getReplacementSelectionData(
            $replacementType,
            $experimentId,
            $entryIdToReplace,
            $germplasmIdToReplace,
            $packageIdToReplace
        );

        $columns = $this->getReplacementSelectionColumns($replacementType);

        // get replacement entity
        $entity = $this->getReplacementEntity($replacementType);

        $htmlData = $this->renderAjax($viewFile, [
            'plantingJobEntriesDataProvider' => $plantingJobEntriesDataProvider,
            'germplasmName' => $germplasmName,
            'parentage' => $parentage,
            'dataProvider' => $data['dataProvider'],
            'totalCount' => $data['totalCount'],
            'columns' => $columns,
            'entity' => $entity,
            'packageIdToReplace' => null,
        ]);

        $data = [
            'data' => $htmlData,
            'count' => $data['totalCount']
        ];

        return json_encode($data);
    }

    /**
     * Get germplasm replacement selection data provider
     *
     * @param string $attribute attribute identifier retrieved via $_POST[...]
     * @param string $searchValue specified data value retrieved via $_POST[...]
     * @param string $packageDbId unique package identifier retrieved via $_POST[...]
     * 
     * @return JSON contains HTML data and data count
     */
    public function actionGetGermplasmSelectionData ()
    {
        $attribute = $_POST['attribute'] ?? null;
        $searchValue = $_POST['searchValue'] ?? null;
        $packageDbId = $_POST['packageDbId'] ?? null;
        $packageDbId = json_decode($packageDbId);

        // If User searches for germplasm records using germplasm name/designation...
        // ...change "name" to "names" and use non-strict searching
        if ($attribute == 'name') {
            $attribute = 'names';
            $searchValue = "equals $searchValue";
        } else {
            $searchValue = "equals $searchValue";
        }

        $data = $this->seed->searchPackageRecords([
            'packageDbId' => "not equals " . ( gettype($packageDbId) == 'array' ? implode('|not equals ', $packageDbId) : $packageDbId ),
            "$attribute" => "$searchValue",
            'seedManager' => Yii::$app->userprogram->get('abbrev'), // filter by current user program
            'fields' => 'germplasm.id AS germplasmDbId |
                germplasm.designation AS name |
                germplasm.germplasm_code AS germplasmCode |
                germplasm.parentage AS parentage |
                seed.id AS seedDbId |
                seed.seed_name AS seedName |
                seed.seed_code AS seedCode |
                program.program_code AS seedManager |
                package.id AS packageDbId |
                package.package_label AS label |
                package.package_code AS packageCode |
                package.package_quantity AS packageQuantity |
                package.package_unit AS packageUnit
            ',
        ], 'all');

        $sort = [
            'defaultOrder' => [ 'germplasmDbId' => SORT_ASC ],
            'attributes' => ['germplasmDbId','name','germplasmCode','parentage','seedName','seedCode', 'label', 'packageCode', 'packageQuantity', 'packageUnit',],
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'germplasmDbId',
            'pagination' => false,
            'sort' => $sort,
        ]);

        $totalCount = $dataProvider->getTotalCount();

        $columns = [
            [
                'header' => false,
                'hAlign' => 'center',
                'value' => function ($data) {
                    return '
                    <input id="'.$data['packageDbId'].'" class="with-gap" name="selection-radio-button" value="'.$data['packageDbId'].'" type="radio" />
                    <label for="'.$data['packageDbId'].'" style="padding-left: 18px !important; margin-left: -5px !important;"></label>';
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('app', 'Germplasm name'),
                'enableSorting' => false,
                'value' => function ($data) {
                    return $data['name'];
                },
            ],
            [
                'attribute' => 'parentage',
                'enableSorting' => false,
                'format' => 'raw',
                'value' => function ($data) {
                    return "<div
                        title='{$data['parentage']}'
                        class='truncated'
                    > {$data['parentage']} </div>";
                },
            ],
            [
                'attribute' => 'seedName',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'label',
                'enableSorting' => false,
                'label' => Yii::t('app', 'Package label'),
                'value' => function ($data) {
                    return $data['label'];
                },
            ],
            [
                'attribute' => 'packageQuantity',
                'enableSorting' => false,
                'format' => 'raw',
                'value' => function ($data) {
                    return (isset($data['packageQuantity'])) ? number_format($data['packageQuantity']) . ' ' . $data['packageUnit'] : '<span class="not-set">(not set)</span>';
                },
            ],            
        ];

        return json_encode([
            'htmlData' => $this->renderAjax('_replace_entry_germplasm_selection_grid', compact(
                'dataProvider',
                'totalCount',
                'columns',
            )),
            'count' => $totalCount,
        ]);
    }


    /**
     * Get replacement selection data provider
     *
     * @param Text $replacementType Replacement type
     * @param Integer|Array $experimentId Experiment identifier
     * @param Integer|Array $entryIdToReplace Entry to be replaced identifier
     * @param Integer|Array $germplasmIdToReplace Germplasm to be replaced identifier
     * @param Integer|Array $packageIdToReplace Package to be replaced identifier
     * @return Array Entry replacement data provider
     */
    public function getReplacementSelectionData (
        $replacementType,
        $experimentId,
        $entryIdToReplace,
        $germplasmIdToReplace,
        $packageIdToReplace
    )
    {
        if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE') {
            $model = 'package';
            $sortParam = 'packageDbId';
            $entityId = 'packageDbId';

            $packageIdToReplace = !empty($packageIdToReplace) ? $packageIdToReplace : 0;
            // package parameters
            $params = [
                'germplasmDbId' => "equals " . ( gettype($germplasmIdToReplace) == 'array' ? implode('|equals ', $germplasmIdToReplace) : $germplasmIdToReplace ),
                'packageDbId' => 'not equals '. ( gettype($packageIdToReplace) == 'array' ? implode('|not equals ', $packageIdToReplace) : $packageIdToReplace ),
                'seedManager' => 'equals ' . Yii::$app->userprogram->get('abbrev'),
                'fields' => '
                    germplasm.id AS germplasmDbId |
                    seed.id AS seedDbId |
                    program.program_code AS seedManager |
                    package.id AS packageDbId |
                    package.package_label AS label |
                    package.package_quantity AS quantity |
                    package.package_unit AS unit |
                    seed.seed_name AS seedName
                '
            ];

            $sort = [
                'defaultOrder' => ['packageDbId'=>SORT_ASC],
                'attributes' => ['packageDbId','label','quantity','unit']
            
            ];

        } elseif ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER') {
            // get program of experiment
            $experimentParams['experimentDbId'] = 'equals ' . ( gettype($experimentId) == 'array' ? implode('|equals ', $experimentId) : $experimentId );
            $experiment = $this->experiment->searchAll($experimentParams, 'limit=1', false);
            $programId = isset($experiment['data'][0]['programDbId']) ? $experiment['data'][0]['programDbId'] : 0;

            // get crop program
            $program = $this->program->getProgram($programId);
            $cropProgramId = $program["cropProgramDbId"] ?? null;

            // get filler germplasm ID in config
            $fillerConfig = $this->configuration->getConfigByAbbrev("DEFAULT_FILLER_GERMPLASM");
            $fillerGermplasmId = (!empty($cropProgramId) && isset($fillerConfig['' . $cropProgramId]["germplasm_id"])) 
                ? $fillerConfig['' . $cropProgramId]["germplasm_id"] : 0;

            $model = 'package';
            $sortParam = 'packageDbId';
            $entityId = 'packageDbId';

            $params = [
                'germplasmDbId' => "equals $fillerGermplasmId",
                'packageDbId' => 'not equals '. ( gettype($packageIdToReplace) == 'array' ? implode('|not equals ', $packageIdToReplace) : $packageIdToReplace ),
                'seedManager' => 'equals ' . Yii::$app->userprogram->get('abbrev'),
                'fields' => '
                    germplasm.id AS germplasmDbId |
                    seed.id AS seedDbId |
                    program.program_code AS seedManager |
                    package.id AS packageDbId |
                    package.package_label AS label |
                    package.package_quantity AS quantity |
                    package.package_unit AS unit |
                    germplasm.designation AS designation |
                    seed.seed_name AS seedName 
                '
            ];

            $sort = [
                'defaultOrder' => ['packageDbId'=>SORT_ASC],
                'attributes' => ['packageDbId','label','packageQuantity','packageUnit','designation','seedName']
            
            ];

        } elseif ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK') {
            $model = 'entryModel';
            $sortParam = 'entryNumber';
            $entityId = 'entryDbId';
            $params = [
                'experimentDbId' => "equals " . ( gettype($experimentId) == 'array' ? implode('|equals ', $experimentId) : $experimentId ),
                'entryType' => 'equals check',
                'entryDbId' => 'not equals ' . ( gettype($entryIdToReplace) == 'array' ? implode('|not equals ', $entryIdToReplace) : $entryIdToReplace )
            ];
            $sort = [
                'defaultOrder' => ['entryNumber'=>SORT_ASC],
                'attributes' => ['entryDbId','entryNumber','packageLabel','packageQuantity','packageUnit']
            
            ];
        } elseif ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM') {
            $model = 'germplasm';
            $sortParam = 'germplasmDbId';
            $entityId = 'germplasmDbId';

            $params = [
                'germplasmDbId' => "not equals " . ( gettype($germplasmIdToReplace) == 'array' ? implode('|not equals ', $germplasmIdToReplace) : $germplasmIdToReplace ),
            ];

            $sort = [
                'defaultOrder' => ['germplasmDbId' => SORT_ASC],
                'attributes' => ['germplasmDbId','name','germplasmCode','parentage','seedName','seedCode', 'label', 'packageCode', 'packageQuantity', 'packageUnit',],
            ];

        }

        if($model == 'package') {
            // use POST seed-packages-search
            $data = $this->seed->searchPackageRecords($params, 'all', $sortParam);
        } else {
            $data = $this->$model->searchAll($params, 'sort='.$sortParam, true)['data'];
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => $entityId,
            'pagination'=>false,
            'sort' => $sort,
        ]);

        $totalCount = $dataProvider->getTotalCount();

        return [
            'dataProvider' => $dataProvider,
            'totalCount' => $totalCount
        ];
    }

    /**
     * Get replacement selection columns
     *
     * @return Array List of data browser columns
     */
    public function getReplacementSelectionColumns ($replacementType)
    {
        $columns = [];

        if($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE'){
            $entityId = 'packageDbId';

            $columns = [
                [
                    'attribute' => 'label',
                    'label' => Yii::t('app', 'Package label'),
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'quantity',
                    'label' => Yii::t('app', 'Package quantity'),
                    'enableSorting' => false,
                    'format' => 'raw',
                    'value' => function ($data){
                        return (isset($data['quantity'])) ? number_format($data['quantity']) . ' ' . $data['unit'] : '<span class="not-set">(not set)</span>';
                    },
                ],
                [
                    'attribute' => 'seedName',
                    'enableSorting' => false,
                ],
            ];

        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK'){
            $entityId = 'entryDbId';

            $columns = [
                [
                    'label' => Yii::t('app', 'Germplasm name'),
                    'value' => function ($data){
                        return $data['entryName'];
                    },
                    'format' => 'raw',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'entryCode',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'packageLabel',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'packageQuantity',
                    'enableSorting' => false,
                    'format' => 'raw',
                    'value' => function ($data){
                        return isset($data['packageQuantity']) ? number_format($data['packageQuantity']) . ' ' . $data['packageUnit'] : '<span class="not-set">(not set)</span>';
                    },
                ],
            ];
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER') {
            $entityId = 'packageDbId';

            $columns = [
                [
                    'label' => Yii::t('app', 'Germplasm name'),
                    'value' => function ($data){
                        return $data['designation'];
                    },
                    'format' => 'raw',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'label',
                    'label' => Yii::t('app', 'Package label'),
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'quantity',
                    'label' => 'Package quantity',
                    'enableSorting' => false,
                    'format' => 'raw',
                    'value' => function ($data){
                        return (isset($data['quantity'])) ? number_format($data['quantity']) . ' ' . $data['unit'] : '<span class="not-set">(not set)</span>';
                    },
                ],
                [
                    'attribute' => 'seedName',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'seedManager',
                    'enableSorting' => false,
                ],
            ];
        }

        $defaultCol = [
            [
                'header' => false,
                'hAlign' => 'center',
                'value' => function ($data) use ($entityId){
                    return '
                    <input id="'.$data[$entityId].'" class="with-gap" name="selection-radio-button" value="'.$data[$entityId].'" type="radio" />
                    <label for="'.$data[$entityId].'" style="padding-left: 18px !important; margin-left: -5px !important;"></label>';
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
        ];

        $columns = array_merge($defaultCol, $columns);

        return $columns;
    }

    /**
     * Render preview of replacement
     */
    public function actionRenderReplacementPreview ()
    {
        // replacement values
        $replacementType = $_POST['replacementType'];
        $replacementTypeText  = $_POST['replacementTypeText'];
        $replacementId  = $_POST['replacementId'];
        $checkedBoxes = $_POST['checkedBoxes'];

        $originalEntryData = null;
        $isBulkReplacement = $_POST['isBulkReplacement'] ?? false;
        $plantingJobEntriesDataProvider = null;
        $germplasmName = '';
        $replacementData = [];

        if (gettype($isBulkReplacement) != 'boolean') {
            $isBulkReplacement = filter_var($isBulkReplacement, FILTER_VALIDATE_BOOLEAN);
        }

        if (!$isBulkReplacement) { // get summary of original entry from session
            $originalEntryData = Yii::$app->session->get('original-occurrence-entry-data');
        }

        $viewFile = '_replace_entry_form';

        // replacement data
        if($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE') {
            $entityDbId = 'packageDbId';
            $modelClass = 'package';

            $params['fields'] = 'package.id AS packageDbId | package.package_quantity AS quantity | package.package_unit AS unit| package.package_label AS label | seed.id AS seedDbId | seed.seed_name AS seedName';

        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK') {
            $entityDbId = 'entryDbId';
            $modelClass = 'entryModel';

            $params['fields'] = 'entry.id AS entryDbId | package.package_quantity AS quantity | package.id AS packageDbId | package.package_unit AS unit | package.package_label AS label | germplasm.id AS germplasmDbId | germplasm.designation AS designation | germplasm.parentage AS parentage | seed.seed_name AS seedName';
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER') {
            $entityDbId = 'packageDbId';
            $modelClass = 'package';

            $params['fields'] = 'package.id AS packageDbId | package.package_quantity AS quantity | package.package_unit AS unit| package.package_label AS label | seed.seed_name AS seedName | germplasm.designation AS designation | germplasm.parentage AS parentage | germplasm.id AS germplasmDbId | program.program_code AS seedManager';
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM') {
            $entityDbId = 'packageDbId';
            $modelClass = 'package';

            $params['fields'] = 'germplasm.id AS germplasmDbId | germplasm.designation AS designation | germplasm.parentage AS parentage | seed.seed_name AS seedName | package.id AS packageDbId | package.package_quantity AS quantity | package.package_unit AS unit | package.package_label AS label';
        }

        $params[$entityDbId] = "equals $replacementId";

        $entity = $this->$modelClass->searchAll($params, 'limit=1', false);

        $data = isset($entity['data'][0]) ? $entity['data'][0] : '';

        $data['quantity'] = number_format($data['quantity']) . ' ' . $data['unit'];
   
        if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE') {
            $replacementData = [
                'replacementPackageDbId' => (string) $data['packageDbId'],
                'replacementSeedDbId' => (string) $data['seedDbId'],
                'replacementGermplasmDbId' => null,
            ];
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK') {
            $replacementData = [
                'replacementGermplasmDbId' => (string) $data['germplasmDbId'],
                'replacementEntryDbId' => (string) $data['entryDbId'],
            ];

            if (isset($data['packageDbId'])) {
                $replacementData['replacementPackageDbId'] = (string) $data['packageDbId'];
            }
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER') {
            $replacementData = [
                'replacementGermplasmDbId' => (string) $data['germplasmDbId'],
            ];

            if (isset($data['packageDbId'])) {
                $replacementData['replacementPackageDbId'] = (string) $data['packageDbId'];
            }
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM') {
            $replacementData = [
                'replacementGermplasmDbId' => "{$data['germplasmDbId']}",
                'replacementPackageDbId' => "{$data['packageDbId']}",
            ];
        }

        $replacementData['replacementType'] = $replacementTypeText;
        $replacementData['isReplaced'] = 'true';

        Yii::$app->session->set('replacement-occurrence-entry-data-' .
            ((gettype($checkedBoxes) == 'array') ? implode('_', $checkedBoxes) : $checkedBoxes), $replacementData);

        $htmlData = $this->renderAjax($viewFile,[
            'data' => $originalEntryData,
            'replacementType' => $replacementTypeText,
            'replacementData' => $data,
            'isPreview' => true,
            'isBulkReplacement' => $isBulkReplacement,
            'plantingJobEntriesDataProvider' => $plantingJobEntriesDataProvider,
            'germplasmName' => $germplasmName,
        ]);

        return json_encode($htmlData);
    }

    /**
     * Get replacement entity by replacement type
     *
     * @param Text $type Replacement type
     * @return Text Replacement entity
     */
    public function getReplacementEntity ($type)
    {
        if ($type == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE') {
            return 'package';
        }

        if ($type == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK') {
            return 'check';
        }

        if ($type == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER') {
            return 'filler package';
        }
    }

    /**
     * Confirm entry replacement
     *
     * @return Boolean $success Whether successfully updated or not
     */
    public function actionConfirmReplacement ()
    {
        $entryDbIds = $_POST['checkedBoxes'];
        $entryNumbers = $_POST['entryNumbers'] ?? [];
        $occurrenceDbId = $_POST['occurrenceDbId'];
        $plantingJobDbId = 0;
        $results = [
            'true' => 0,
            'false' => 0,
            'total' => 0,
        ];
        $count = 0;

        // Get replacement data
        $replacementData = Yii::$app->session->get('replacement-occurrence-entry-data-' .
            ((gettype($entryDbIds) == 'array') ? implode('_', $entryDbIds) : $entryDbIds));

        foreach ($entryNumbers as $entryNumber) {
            $plantingJobEntry = null;
            $params = [
                'entryNumber' => "equals $entryNumber",
                'occurrenceDbId' => "equals $occurrenceDbId",
            ];
            $filters = 'limit=1';

            // Retrieve planting job entry info (in case entry is in a packing job)
            $plantingJobEntryResponse = $this->plantingJobEntry->searchAll($params, $filters, false);
            $plantingJobEntry = ($plantingJobEntryResponse['success'] &&
                !empty($plantingJobEntryResponse['data'])) ? $plantingJobEntryResponse['data'][0] : null;

            // Try retrieving planting job DB ID and planting job entry DB ID
            $plantingJobDbId = ($plantingJobEntry) ? $plantingJobEntry['plantingJobDbId'] : 0;
            $plantingJobEntryDbId = ($plantingJobEntry) ? $plantingJobEntry['plantingJobEntryDbId'] : 0;

            // Attempt to update planting job entry first...
            // ...In case entry is included in a packing job...
            // ...Before the parent planting instruction
            if ($plantingJobEntryDbId) {
                $this->plantingJobEntry->updateOne($plantingJobEntryDbId, $replacementData);
            }

            // Retrieve planting instruction IDs of all the selected entry's replicates
            $plantingInstructionResponse = $this->plantingInstruction->searchAll(array_merge($params,
                [ 'fields' => 'plantingInstruction.id  AS plantingInstructionDbId | entry.id AS entryDbId |
                    entry.entry_number AS entryNumber | plot.occurrence_id AS occurrenceDbId'
                ]
            ));
            $plantingInstructionData = ($plantingInstructionResponse['success'] &&
                !empty($plantingInstructionResponse['data'])) ? $plantingInstructionResponse['data'] : null;
            
            // Planting instruction request parameters
            $reqParams = [];

            // If planting instruction records exist
            foreach ($plantingInstructionData as $plantingInstruction) {
                $plantingInstructionDbId = $plantingInstruction['plantingInstructionDbId'];

                if ($replacementData['replacementType'] == 'Replace with package') { // If Replace with package
                    $reqParams['packageDbId'] = (string) $replacementData['replacementPackageDbId'];
                    $reqParams['seedDbId'] = (string) $replacementData['replacementSeedDbId'];
                } elseif ($replacementData['replacementType'] === 'Replace with check') { // If Replace with check
                    // Get entry info
                    $replacementEntryResponse = $this->entryModel->searchAll(['entryDbId' => "equals {$replacementData['replacementEntryDbId']}",], 'limit=1', false);
                    $replacementEntry = ($replacementEntryResponse['success'] && !empty($replacementEntryResponse['data'])) ?
                        $replacementEntryResponse['data'][0] : null;

                    // Get entry information
                    if (isset($replacementData['replacementPackageDbId'])) {
                        $reqParams['packageDbId'] = "{$replacementData['replacementPackageDbId']}";
                    }

                    $reqParams['germplasmDbId'] = "{$replacementData['replacementGermplasmDbId']}";

                    // Get entry ID, entry name, entry class and seed ID
                    $reqParams['entryName'] = $replacementEntry['entryName'];
                    $reqParams['entryType'] = $replacementEntry['entryType'];
                    $reqParams['seedDbId'] = "{$replacementEntry['seedDbId']}";
                } elseif (
                    $replacementData['replacementType'] === 'Replace with filler' ||
                    $replacementData['replacementType'] === 'Replace with germplasm'
                ) { // If Replace with Filler OR Replace with Germplasm
                    // Get designation and seed of package
                    $packageParams = [];
                    $packageParams['fields'] = 'package.id AS packageDbId |
                        germplasm.designation AS designation |
                        seed.id AS seedDbId';

                    $reqParams['packageDbId'] = "{$replacementData['replacementPackageDbId']}";
                    $reqParams['germplasmDbId'] = "{$replacementData['replacementGermplasmDbId']}";

                    $packageParams['packageDbId'] = "equals {$replacementData['replacementPackageDbId']}";

                    $packageResponse = $this->package->searchAll($packageParams, 'limit=1', false);
                    $package = ($packageResponse['success'] && !empty($packageResponse['data'])) ?
                        $packageResponse['data'][0] : null;

                    if (isset($package['designation'])) {
                        $reqParams['entryName'] = "{$package['designation']}";
                    }

                    if (isset($package['seedDbId'])) {
                        $reqParams['seedDbId'] = "{$package['seedDbId']}";
                    }
                }
                
                $response = $this->plantingInstruction->updateOne($plantingInstructionDbId, $reqParams);
                $success = $response['success'] ? 'true' : 'false';
                $results[$success] += 1;
            }

            if ($replacementData['replacementType'] === 'Replace with germplasm') {
                // Reflect updates on Cross Names
                $this->renameEntryInvolvedCrossLists($occurrenceDbId, $entryDbIds[$count]);
            }

            $count++;
        }

        // If planting job DB ID exists and...
        // ...There is at least 1 successful replacement...
        if ($plantingJobDbId && $results['true']) {
            // ...Update planting job status (in case entry/entries are in a packing job)...
            // ...And isSubmitted to draft and false, respectively
            $plantingJobParam = [
                'isSubmitted' => 'false',
                'plantingJobStatus' => 'draft'
            ];

            $this->plantingJob->updateOne($plantingJobDbId, $plantingJobParam);
        }

        $results['total'] = $results['true'] + $results['false'];

        return json_encode($results);
    }

    /**
     * Redirect user back to the Occurrences browser
     *
     * @param string $program program code
     * @param string|int $id unique occurrence identifier
     */
    public function actionRedirectToOccurrencesBrowser ($program, $id)
    {
        $entity = $_POST['entity'] ?? '';
        $entity = ucwords(strtolower(str_replace('_', ' ', $entity)));

        $occurrenceName = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'occurrenceDbId' => "equals $id",
            ]),
            'limit=1',
            false
        )['data'][0]['occurrenceName'];
        $redirectUrl = ['/occurrence?program='.$program.'&OccurrenceSearch[occurrenceDbId]='.$id];
        $message = "The $entity records for <b>$occurrenceName</b> are being prepared in the background for download. You may proceed with other tasks. You will be notified in this page once done.";// set background processing info to session
        Yii::$app->session->set('em-bg-processs-message', $message);
        Yii::$app->session->set('em-bg-processs', true);

        Yii::$app->response->redirect($redirectUrl);
    }

    public function renameEntryInvolvedCrossLists($occurrenceDbId, $entryDbId) {
        $description = "Renaming of cross name records";

        // Invoke worker
        $rename = $this->worker->invoke(
            'UpdateExperimentCrossNameRecords',
            $description,
            'OCCURRENCE',
            "$occurrenceDbId",
            'OCCURRENCE',
            'OCCURRENCES',
            'POST',
            [
                'occurrenceDbId' => $occurrenceDbId,
                'entryDbId' => $entryDbId
            ]
        );

        return $rename;
    }

    /**
     * Check harvested plots on the selected occurrence for replace occurrence entry
     * 
     * @return array layout information
     */
    public function actionHarvestedChecker(){
        // Retrieve occurrence id
        $occurrenceDbId = $_POST['id'];

        // Get plots
        $data = Yii::$app->api->getResponse('POST', 'occurrences/'.$occurrenceDbId.'/plots-search')['body']['result']['data'][0] ?? null;
        
        // Catch if data returns no result
        if(!$data){
            return json_encode(['success' => false, 'state' => 'error', 'message' => 'Encountered error in retrieving occurrence plots']);
        }

        // Check each plots if its harvested
        foreach($data['plots'] as $temp){
            if($temp['harvestStatus'] == 'COMPLETED'){
                // If at least one plot is harvested
                return json_encode(['success' => false, 'state' => 'error', 'message' => 'Cannot replace this entry because a plot has been already harvested.']);
            }
        }

        // Return success as default
        return json_encode(['success' => true]);
    }
}