<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\occurrence\controllers;

use Yii;
use ZipArchive;
use app\components\B4RController;
use app\controllers\BrowserController;
use app\controllers\Dashboard;
use app\models\Api;
use app\models\Browser;
use app\models\BackgroundJob;
use app\models\DataBrowserConfiguration;
use app\models\Experiment;
use app\models\ExperimentDesignArray;
use app\models\OccurrenceSearch;
use app\models\Worker;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IPerson;
use app\models\ListMember;
use app\models\LocationOccurrenceGroup;
use app\models\User;
use app\modules\location\models\FieldLayout;
use app\modules\occurrence\models\FileExport;
use app\modules\occurrence\models\Manage as OccurrenceManage;
use app\modules\occurrence\models\OccurrenceBackgroundJob;
use Throwable;
use yii\helpers\Html;

/**
 * Default controller for the `occurrence` module
 */
class DefaultController extends B4RController
{
    public function __construct ($id, $module,
        protected Api $api,
        protected Browser $browser,
        protected BrowserController $browserController,
        protected BackgroundJob $backgroundJob,
        protected Dashboard $dashboard,
        protected DataBrowserConfiguration $dataBrowserConfiguration,
        protected Experiment $experiment,
        protected ExperimentDesignArray $experimentDesignArray,
        protected FieldLayout $fieldLayout,
        protected IOccurrence $occurrence,
        protected LocationOccurrenceGroup $locationOccurrenceGroup,
        protected OccurrenceBackgroundJob $occurrenceBackgroundJob,
        protected OccurrenceManage $occurrenceManage,
        protected OccurrenceSearch $occurrenceSearch,
        protected IPerson $person,
        protected User $user,
        protected Worker $worker,
        protected ZipArchive $zipArchive,
        protected ListMember $listMember,
        $config=[]
    )
    { parent::__construct($id, $module, $config); }

    /**
     * Renders the browser for the occurrences
     *
     * @param $program text current program of the user
     * @return mixed browser for occurrences
     */
    public function actionIndex($program, $filter = null)
    {
        $this->dataBrowserConfiguration->processCurrentDataBrowserState('dynagrid-em-occurrences', null, ['program' => $program], ['OccurrenceSearch' => ['occurrenceDbId']]);
        Yii::info('Rendering occurrence browser ' . json_encode(['program'=>$program, 'filter'=>$filter]), __METHOD__);
        $userId = $this->user->getUserId();
        $occurrenceModel = $this->occurrence;
        $searchModel = $this->occurrenceSearch;

        // get dashboard filters
        $dashboardFilters = $this->dashboard->getFilters();
        $defaultFilters = (array) $dashboardFilters;

        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $occurrenceTextFilters = [];

        if($session->get('dashboard-occurrence-text-filters') == NULL){

            $searchParams = [
                'userDbId' => (string)$userId,
                'dataBrowserDbId' => 'occurrences-grid',
                'type' => 'filter'
            ];

            // check if configuration already exists
            $config = $this->dataBrowserConfiguration->searchAll($searchParams);
            $occurrenceFilters = [];
            if (isset($config['totalCount']) && $config['totalCount'] > 0) {
                $occurrenceFilters = $config['data'][0]['data'];
            }
            // get text filters
            $searchParams = [
                'userDbId' => (string)$userId,
                'dataBrowserDbId' => 'occurrences-text-grid',
                'type' => 'filter'
            ];

            // check if configuration already exists
            $config = $this->dataBrowserConfiguration->searchAll($searchParams);
            $occurrenceTextFilters = [];
            if (isset($config['totalCount']) && $config['totalCount'] > 0) {
                $occurrenceTextFilters = $config['data'][0]['data'];
            }
        } else {

            $occurrenceFilters = $session->get('dashboard-occurrence-filters');
            $occurrenceTextFilters = $session->get('dashboard-occurrence-text-filters');
        }

        $defaultFilters = array_merge($defaultFilters, $occurrenceFilters);

        // retrieve browser dataProvider and export dataProvider
        $data = $searchModel->searchOccurrence($params, $defaultFilters);
        // get person ID
        $personId = $this->user->getUserId();
        // get program ID
        $programId = Yii::$app->userprogram->get('id');
        $role = 'DATA_OWNER';

        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if ($isAdmin && strtoupper($role) != 'COLLABORATOR') {
            $role = 'ADMIN';
        }

        // If role is COLLABORATOR, get other DC file names config for the role
        // Else, get DC file names per PROGRAM
        $dataCollectionOtherFiles = $this->api->getApiResults(
            'GET',
            'configurations?abbrev=DC_' . strtoupper((strtoupper($role) === 'COLLABORATOR') ? $role : $program) . '_OTHER_FILE_SETTINGS&limit=1',
            retrieveAll: false
        );

        // If config does not exist, get global config
        if($dataCollectionOtherFiles['totalCount'] == 0){
            $dataCollectionOtherFiles = $this->api->getApiResults(
                'GET',
                'configurations?abbrev=DC_GLOBAL_OTHER_FILE_SETTINGS&limit=1',
                retrieveAll: false
            );
        }

        $dataCollectionOtherFiles = (isset($dataCollectionOtherFiles['data'][0]['configValue'])) ? array_keys($dataCollectionOtherFiles['data'][0]['configValue']) : [];

        // Prepare HTML attributes and info
        $params = [];
        foreach ($dataCollectionOtherFiles as $value) {
            $params []= [
                'text' => $value,
                'options' => [
                    'class' => 'data-collection-other-files-btn hide-loading',
                    'data-hover' => 'hover',
                    'data-file-name' => $value,
                    'title' => "Download the $value file for a single Occurrence",
                ]
            ];
        }

        return $this->render('index', [
            'dataProvider' => $data[0],
            'program' => $program,
            'programId' => $programId,
            'searchModel' => $searchModel,
            'dashboardFilters' => $defaultFilters,
            'occurrenceTextFilters' => $occurrenceTextFilters,
            'occurrenceModel' => $occurrenceModel,
            'role' => $role,
            'personId' => $personId,
            'isAdmin' => Yii::$app->session->get('isAdmin'),
            'downloadDataCollectionDropdownItemParams' => $params,
            'thresholdValueForExportCsvMappingFiles' => Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportCsvMappingFiles'),
        ]);
    }

    /**
     * Retrieves the values for the filter columns of the EM browser.
     * @return json data for the Select2 widget
     */
    public function actionGetFiltersData() {
        $params = isset($_POST['filter']) ? $_POST['filter'] : '';
        $column = isset($_POST['column']) ? $_POST['column'] : '';

        $data = $this->occurrence->getColumnFilters($params, $column);

        return json_encode($data);
    }

    /**
     * Save occurrence filters
     */
    public function actionApplyFilters()
    {
        $data = isset($_POST['data']) ? $_POST['data'] : [];
        $texts = isset($_POST['texts']) ? $_POST['texts'] : [];
        $userModel = new User();
        $userId = $userModel->getUserId();
        $currentUrl = isset($_POST['curr_url_params']) ? $_POST['curr_url_params'] : null;

        // remove filter session
        $session = Yii::$app->session;
        $session->remove('dashboard-occurrence-filters');
        $session->remove('dashboard-occurrence-text-filters');

        return $this->occurrenceManage->applyFilters($data, $texts, $userId, $currentUrl);
    }

    /**
     * Given Occurrence ID(s), this function exports mapping files in
     * CSV (occurrences) or JSON (experiments)
     *
     * @param mixed $id Occurrence identifier(s)
     * @param string $type File format identifier
     * @param string $program program abbrev identifier
     * @param string $redirectUrl URL to redirect to in case API call fails
     */
    public function actionExportMappingFiles ($id='', $type='json', $program='', $redirectUrl='', $plotCount = '')
    {
        Yii::info('Exporting file '.json_encode(['occurrenceDbId'=>$id, 'fileType'=>$type,
            'program'=>$program, 'redirectUrl'=>$redirectUrl, 'plotCount' => $plotCount]), __METHOD__);
        $plotCount = $plotCount ?? intval($plotCount);
        $redirectUrl = empty($redirectUrl) ? "index?program={$program}" : $redirectUrl;

        if ($id === '' || is_array($id)) {
            exit;
        } else {
            // Replace safe URL character (-) with | to be API-accepted
            $idArray = explode('-', $id);
            $id = '';

            foreach ($idArray as $key => $value) {
                $id .= "equals $value |";
            }
        }

        if ($type === 'csv') {
            $occurrenceInstances = [];

            // This accepts both single and Array of occurrence IDs
            $params = [
                'occurrenceDbId' => "$id",
                'fields' => 'occurrence.id AS occurrenceDbId |'.
                    'occurrence.occurrence_number AS occurrenceNumber |'.
                    'experiment.experiment_code AS experimentCode |'.
                    'experiment.experiment_name AS experimentName |'.
                    'occurrence.occurrence_name AS occurrenceName |'.
                    'occurrence.occurrence_name AS occurrenceCode |'.
                    'location.location_name AS locationName |'.
                    'location.location_code AS locationCode |'.
                    'occurrence.plot_count AS plotCount',
            ];

            $response = $this->api->getApiResults(
                'POST',
                'occurrences-search',
                json_encode($params),
                '',
                true
            );

            if (empty($response['success'])) {
                $this->redirect($redirectUrl)->send();
            }

            $occurrenceInstances = $response['data'];
            $totalPlots = 0;

            if (sizeof($occurrenceInstances) > 1) {
                foreach ($occurrenceInstances as $index => $occurrenceInstance) {
                    $totalPlots += $occurrenceInstance['plotCount'];
                }

                if ($totalPlots >= 20000) {
                    Yii::$app->session->setFlash('error', 'The total Plot count exceeds 20,000. Please refrain from downloading Mapping Files in bulk that exceeds the Plot count limit.');
                    // Redirect back to the EM data browser
                    $this->redirect(urldecode($redirectUrl))->send();    
                    return;
                }
            }

            // Build CSV data
            $this->exportMappingFilesInCSV($program, $occurrenceInstances, $plotCount);
        } else if ($type === 'json') {
            // Get Experiment and Occurrence IDs
            $params = [
                'fields' => 'occurrence.id AS occurrenceDbId |'.
                    'experiment.id AS experimentDbId | ' .
                    'experiment.experiment_code AS experimentCode |'.
                    'experiment.experiment_name AS experimentName |'.
                    'occurrence.occurrence_name AS occurrenceName |'.
                    'occurrence.occurrence_name AS occurrenceCode |'.
                    'location.location_name AS locationName |'.
                    'location.location_code AS locationCode |'.
                    'occurrence.plot_count AS plotCount',
                'occurrenceDbId' => (string) $id,
            ];

            $response = $this->api->getApiResults(
                'POST',
                'occurrences-search',
                json_encode($params),
                '',
                true
            );
            if (empty($response['success'])) {
                $this->redirect($redirectUrl)->send();
            }

            // Verify if response contains the Experiment and Occurrence IDs
            if (
                isset($response) && 
                !empty($response['data'])
            ) { 
                $occurrences = $response['data'];
                $occurrenceDbId = $occurrences[0]['occurrenceDbId'];
                unset($response);
                $experimentDbIds = [];
                $experimentCodes = [];
                $experimentNames = [];
                $plotCount = 0;

                // get attribute to use in file name
                [$fileNameAttribute, $fileNameSuffix] = Yii::$app->config->getFileNameAttributeAndSuffix('MAPPING_FILES');

                foreach ($occurrences as $key => $occurrence) {
                    $experimentDbIds []= $occurrence['experimentDbId'];
                    $experimentCodes []= $occurrence['experimentCode'];
                    $experimentNames []= $occurrence['experimentName'];

                    $fileNameAttrs []= $occurrence[$fileNameAttribute];

                    // Get Experiment and Occurrence IDs
                    $params = [
                        'fields' => 'occurrence.id AS occurrenceDbId | occurrence.plot_count AS plotCount | experiment.id AS experimentDbId',
                        'experimentDbId' => "equals {$occurrence['experimentDbId']}",
                    ];

                    $response = $this->api->getApiResults(
                        'POST',
                        'occurrences-search',
                        json_encode($params),
                        '',
                        true
                    );

                    if (empty($response['success'])) {
                        $this->redirect($redirectUrl)->send();
                    }

                    if ($response['data']) {
                        $tempOccurrences = $response['data'];
                        unset($response['data']);

                        foreach ($tempOccurrences as $key => $tempOcc) {
                            $plotCount += $tempOcc['plotCount'];
                        }

                        unset($tempOccurrences);
                    }
                }
                unset($occurrences);
                $experimentDbIds = array_unique($experimentDbIds);
                $experimentCodes = array_unique($experimentCodes);
                $experimentNames = array_unique($experimentNames);
                $fileNameAttrs = array_unique($fileNameAttrs);
                $experimentCount = count($experimentDbIds);

                // Get background processing threshold
                $thresholdValue = Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportCsvMappingFiles');

                if (
                    isset($experimentDbIds) &&
                    count($experimentDbIds) > 0 &&
                    $plotCount >= $thresholdValue
                ) {
                    // Prepare file for export
                    $toExport = new FileExport(FileExport::MAPPING_FILES, FileExport::JSON, time());
                    array_map(fn($code) => $toExport->addExperimentData($code, [], []), $fileNameAttrs);

                    // Set headers and file name based on number of experiments to be exported
                    // $filename = str_replace('.json', '', array_key_first($toExport->getUnzipped()));
                    $filename = str_replace(
                        '.json',
                        '',
                        $experimentCount === 1 ? array_key_first($toExport->getUnzipped()) : $toExport->getZippedFilename()
                    );
                    $experimentDbId = $experimentDbIds[0];
                    $experimentName = $experimentNames[0];
                    $description = 'Preparing JSON Mapping File for downloading';
                    $experimentDbIdQuery = '';

                    foreach ($experimentDbIds as $experimentDbId) {
                        $experimentDbIdQuery .= "&experimentId=$experimentDbId";
                    }
                    $experimentNames = implode(', ', $experimentNames);
                    $experimentDbIds = implode('|', $experimentDbIds);

                    $result = $this->worker->invoke(
                        'BuildJsonData',
                        $description,
                        'PLOT',
                        $experimentDbId,
                        'EXPERIMENT',
                        'OCCURRENCES',
                        'POST',
                        [
                            'url' => "experiment-design-arrays?$experimentDbIdQuery",
                            'description' => $description,
                            'fileName' => $filename,
                            'httpMethod' => 'GET',
                        ],
                        [
                            'remarks' => $filename,
                        ]
                    );

                    // If background processing failed
                    if (!$result['success']) {
                        $message = "There was a problem in the background processing. Please try again or file a report for assistance.";

                        // set background processing info to session
                        Yii::$app->session->set('em-bg-processs-message', $message);
                        Yii::$app->session->set('em-bg-processs', true);

                        $program = Yii::$app->session->get('program');
                        Yii::$app->response->redirect(["/occurrence?program=$program&OccurrenceSearch[experimentDbId]=$experimentDbId"]);
                    } else {
                        $message = "The Mapping File records for <b>$experimentNames</b> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.";
        
                        // set background processing info to session
                        Yii::$app->session->set('em-bg-processs-message', $message);
                        Yii::$app->session->set('em-bg-processs', true);
        
                        $program = Yii::$app->session->get('program');
                        Yii::$app->response->redirect(["/occurrence?program=$program&OccurrenceSearch[experimentDbId]=$experimentDbIds"]);
                    }

                    return;
                }

                if (
                    isset($experimentDbIds) &&
                    count($experimentDbIds) > 0
                ) {
                    $tempString = '';

                    foreach ($experimentDbIds as $experimentDbId) {
                        $tempString .= '&experimentId=' . $experimentDbId;
                    }

                    $experimentDbIds = $tempString;

                    // Get Mapping Files of Experiment(s) via BSO-API
                    $response = Yii::$app->api->getResponse(
                        'GET',
                        'experiment-design-arrays',
                        null,
                        $experimentDbIds,
                        true,
                        'bso'
                    );

                    Yii::debug(substr('BSO-API response ' . json_encode($response), 0, length: 1000), __METHOD__);

                    if (isset($response) && $response['status'] < 400 && isset($response['body']) && !empty($response['body'])) {
                        $data = $response['body'];

                        // parse geospatialObjects to force display of empty object {} instead of []
                        foreach ($data["references"]["geospatialObjects"] as $gobject) {

                            if ($gobject["geojson"] != null) {
                                $geojson = [
                                    "type" => $gobject["geojson"]["type"],
                                    "features" => [
                                        [
                                            "type" => $gobject["geojson"]["features"][0]["type"],
                                            "properties" => (object)[],
                                            "geometry" => $gobject["geojson"]["features"][0]["geometry"]
                                        ]
                                    ]

                                ];
                            } else {
                                $geojson = $gobject["geojson"];
                            }
                            $newGObject = [
                                "code" => $gobject["code"],
                                "name" => $gobject["name"],
                                "type" => $gobject["type"],
                                "geojson" => $geojson,
                                "geospatialObjectId" => $gobject["geospatialObjectId"],
                                "parentGeospatialObjectId" => $gobject["parentGeospatialObjectId"]
                            ];
                            $geospatialObjects[] = $newGObject;
                        }
                        $data["references"]["geospatialObjects"]  = $geospatialObjects;

                        // Prepare file for export
                        $toExport = new FileExport(FileExport::MAPPING_FILES, FileExport::JSON, time());
                        array_map(fn($code) => $toExport->addExperimentData($code, [], []), $fileNameAttrs);

                        // Set headers and file name based on number of experiments to be exported
                        $filename = $experimentCount === 1 ? array_key_first($toExport->getUnzipped()) : $toExport->getZippedFilename();

                        header("Content-Disposition: attachment; filename={$filename};");
                        header('Content-Type: application/json');

                        if (isset($data) && !empty($data)) {
                            // Echo JSON content to be able to export
                            return json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
                        } else {
                            Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> There seems to be a problem in exporting Mapping Files in JSON format. Kindly contact the EBS Service Desk for assistance.', false);
                            // Redirect back to the EM data browser
                            $this->redirect('index?program=' . $program)->send();
                        }
                    } else {
                        Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> There seems to be a problem in exporting Mapping Files in JSON format. Kindly contact the EBS Service Desk for assistance.', false);
                        $this->redirect($redirectUrl)->send();
                    }
                } else {
                    Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> There seems to be a problem in exporting Mapping Files in JSON format. Kindly contact the EBS Service Desk for assistance.', false);
                    // Redirect back to the EM data browser
                    $this->redirect('index?program=' . $program)->send();
                }
            } else {
                Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> There seems to be a problem in exporting Mapping Files in JSON format. Kindly contact the EBS Service Desk for assistance.', false);
                // Redirect back to the EM data browser
                $this->redirect('index?program=' . $program)->send();
            }
        }
    }

    /**
     * Arranges Occurrence data into mapping file data,
     * and saves them in CSV format,
     * and exported to the web browser
     *
     * @param array $program Program identifier
     * @param array $occurrenceInstances Array of Occurrence data
     * @param int $plotCount Sum total of plot records to be exported 
     * 
     */
    public function exportMappingFilesInCSV ($program, $occurrenceInstances, $plotCount = null)
    {
        $occurrenceCount = count($occurrenceInstances);
        $experimentCodes = [];
        $occurrenceSerialNumbers = [];
        $mappingFiles = [];

        // get attribute to use in file name
        [$fileNameAttribute, $fileNameSuffix] = Yii::$app->config->getFileNameAttributeAndSuffix('MAPPING_FILES');

        foreach ($occurrenceInstances as $index => $occurrenceInstance) {
            // Add Experiment code
            $experimentCodes[$index] = $this->fieldLayout->getValidFileName($occurrenceInstance['experimentCode']);

            $fileNameAttr[$index] = $occurrenceInstance[$fileNameAttribute];

            // Get Occurrence name's serial number part
            $occurrenceSerialNumbers[$index] = $occurrenceInstance['occurrenceNumber'];

            // Retrieve attributes and headers
            [
                $mappingPlotsAttributes,
                $headers,
            ] = $this->browserController->getColumns($program, 'MAPPING_FILE', 'DOWNLOAD_MAPPING_FILE_CSV_COLUMNS', [], 'download');

            // Prepend system-defined columns
            array_unshift($mappingPlotsAttributes,
                'occurrenceDbId',
                'plotDbId',
                'paX',
                'paY',
                'fieldX',
                'fieldY'
            );
            array_unshift($headers,
                'OCCURRENCE_ID',
                'PLOT_ID',
                'PA_X',
                'PA_Y',
                'FIELD_X',
                'FIELD_Y'
            );

            // Get background processing threshold
            $thresholdValue = Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportCsvMappingFiles');

            if ($occurrenceCount === 1 && $plotCount && $plotCount >= $thresholdValue) {
                $toExport = new FileExport(FileExport::MAPPING_FILES, FileExport::CSV, time());
                $filename = str_replace('.csv', '', $toExport->getFilenames($fileNameAttr[$index], $toExport->formatSerialCodes([$occurrenceSerialNumbers[$index]]))[0]);
                $occurrenceId = $occurrenceInstance['occurrenceDbId'];
                $occurrenceName = $occurrenceInstance['occurrenceName'];
                $description = 'Preparing CSV Mapping Files for downloading';

                $result = $this->worker->invoke(
                    'BuildCsvData',
                    $description,
                    'PLOT',
                    $occurrenceId,
                    'OCCURRENCE',
                    'OCCURRENCES',
                    'POST',
                    [
                        'url' => "occurrences/$occurrenceId/mapping-plots",
                        'requestBody' => null,
                        'description' => $description,
                        'csvHeaders' => $headers,
                        'csvAttributes' => $mappingPlotsAttributes,
                        'fileName' => $filename,
                        'httpMethod' => 'GET',
                    ],
                    [
                        'remarks' => $filename,
                    ]
                );

                // If background processing failed
                if (!$result['success']) {
                    $message = "There was a problem in the background processing. Please try again or file a report for assistance.";

                    // set background processing info to session
                    Yii::$app->session->set('em-bg-processs-message', $message);
                    Yii::$app->session->set('em-bg-processs', true);

                    $program = Yii::$app->session->get('program');
                    Yii::$app->response->redirect(["/occurrence?program=$program&OccurrenceSearch[occurrenceDbId]=$occurrenceId"]);
                } else {
                    $message = "The Mapping File records for <b>$occurrenceName</b> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.";
    
                    // set background processing info to session
                    Yii::$app->session->set('em-bg-processs-message', $message);
                    Yii::$app->session->set('em-bg-processs', true);
    
                    $program = Yii::$app->session->get('program');
                    Yii::$app->response->redirect(["/occurrence?program=$program&OccurrenceSearch[occurrenceDbId]=$occurrenceId"]);
                }

                return;
            }

            $mappingFileData = [$headers];
            $i = 1;

            $response = $this->api->getApiResults(
                'GET',
                "occurrences/{$occurrenceInstance['occurrenceDbId']}/mapping-plots",
                null,
                '',
                true
            );
            $mappingPlots = $response['data'];

            if (
                isset($mappingPlots) &&
                !empty($mappingPlots)
            ) {
                foreach ($mappingPlots as $value) {
                    $mappingFileData[$i] = [];

                    foreach ($mappingPlotsAttributes as $attribute) {
                        array_push($mappingFileData[$i], $value[$attribute]);
                    }

                    $i++;
                }
            }

            // Add this set of mapping file data
            $mappingFiles[$index] = $mappingFileData;
        }

        // Determine which manner of file writing to use
        if ($occurrenceCount === 1) {
            $this->saveMappingFileAsCSVFile(
                $experimentCodes[0],
                $occurrenceSerialNumbers[0],
                $mappingFiles[0],
                $occurrenceInstances[0]['occurrenceDbId']
            );
        } else if ($occurrenceCount > 1) {
            $this->saveMappingFilesAsZIPFile(
                $experimentCodes,
                $occurrenceSerialNumbers,
                $mappingFiles,
                $fileNameAttr
            );
        }
    }

    /**
     * Writes a single set of mapping file data into a CSV file
     * and export it to the web browser
     *
     * @param string $experimentCode Experiment code
     * @param string $occurrenceSerialNumber Occurrence name's serial number segment
     * @param array $mappingFile A set of Occurrence data arranged
     *                             into mapping file data
     * @param integer $occurrenceDbId occurrence identifier
     */
    public function saveMappingFileAsCSVFile ($experimentCode, $occurrenceSerialNumber, $mappingFile, $occurrenceDbId=null)
    {

        // get file name from config by occurrence ID
        $filename = $this->browserController->getFileNameFromConfigByEntityId($occurrenceDbId, 'MAPPING_FILES', FileExport::CSV);

        // Set headers
        header('Content-Type: application/csv');
        header("Content-Disposition: attachment; filename={$filename};");
    
        // Write to output stream
        $fp = fopen('php://output', 'w');

        if (!$fp) {
            die('Failed to create CSV file.');
        }

        foreach ($mappingFile as $line) {
            fputcsv($fp, $line);
        }

        exit;
    }

    /**
     * Writes multiple sets of mapping file data into respective CSV files,
     * saves them into a single ZIP file,
     * and exports the ZIP file to the web browser
     *
     * @param array $experimentCodes Array of Experiment codes
     * @param array $occurrenceSerialNumbers Array of Occurrence names' serial number segment
     * @param array $mappingFiles Array of mapping file data
     * @param array $fileNameAttr Array of attributes to be used in file name
     */
    public function saveMappingFilesAsZIPFile ($experimentCodes, $occurrenceSerialNumbers, $mappingFiles, $fileNameAttr)
    {
        $toExport = new FileExport(FileExport::MAPPING_FILES, FileExport::CSV, time());
        
        array_map(fn($code, $serial, $data) => $toExport->addExperimentData($code, [$serial], [$data]),
            $fileNameAttr, $occurrenceSerialNumbers, $mappingFiles);
        $buffer = $toExport->getUnzipped();

        // Initialize variables for CSV files
        $csvFiles = [];
        $csvFileNames = [];

        // Create temporary directory
        $tempDir = sys_get_temp_dir();

        // For each set of mapping file data...
        // ...create a CSV file and...
        // ...temporarily store it within B4R container
        foreach ($buffer as $filename => $content) {
            // Create unique temporary file for CSV contents
            $tempFilePath = tempnam($tempDir, '');
            $csvFilePath = $tempDir . '/' . $filename;
            rename($tempFilePath, $csvFilePath);

            // Open temp file
            $fp = fopen($csvFilePath, 'w+');

            if ($fp === false) die('Failed to create a CSV file.');

            // Write CSV contents into file
            foreach ($content as $line) {
                fputcsv($fp, $line);
            }

            fclose($fp);
            $csvFiles[] = $csvFilePath;
            $csvFileNames[] = $filename;
        }

        // Set B4R container's present working directory (PWD) to...
        // .../var/www/html/files
        chdir('files');

        $zipName = $toExport->getZippedFilename();

        // Create an empty ZIP file
        // 1 is ZipArchive::CREATE's value
        $this->zipArchive->open($zipName, 1);

        // Add each valid CSV file
        foreach ($csvFiles as $index => $csvFile) {
            if (file_exists($csvFile) && is_readable($csvFile)) {
                $this->zipArchive->addFile($csvFile, $csvFileNames[$index]);
            }
        }

        $this->zipArchive->close();

        // Set HTTP headers
        header('Content-Type: application/zip');
        header('Content-Length: ' . filesize($zipName));
        header('Content-Disposition: attachment; filename=' . $zipName . ';');

        // Download ZIP file
        readfile($zipName);

        // Delete ZIP file from within B4R container's PWD
        unlink($zipName);

        // Reset PWD to /var/www/html
        chdir('../');

        exit;
    }

    /**
     * Export planting instructions
     *
     * @param string|int occurrence identifier
     * @param string $type file format
     * @param string $entity entity whether location or occurrence
     * @param string $program program abbrev identifier
     * @param string $redirectUrl URI to redirect the page to gracefully handle errors
     * @param string $retrieveExperimentData for experiment data variables
     */
    public function actionExportPlantingInstructions($id, $type = 'csv', $entity = 'occurrence', $program='', $redirectUrl='')
    {
        $redirectUrl = empty($redirectUrl) ? "index?program={$program}" : $redirectUrl;

        Yii::info('Exporting file '.json_encode(['occurrenceDbId'=>$id, 'fileFormat'=>$type, 'entity'=>$entity,
                'program'=>$program, 'redirectUrl'=>$redirectUrl]), __METHOD__);

        $noLoc = false;

        if ($entity == 'occurrence') {
            $model = 'Occurrence';
            $searchId = $id;
        } else {
            $model = 'Location';

            // get location by occurrence id
            $params = [
                'occurrenceDbId' => "$id",
                'fields' => "log.occurrence_id AS occurrenceDbId | log.location_id AS locationDbId",
            ];

            $locOccGroup = $this->locationOccurrenceGroup->searchAll($params);
            // if no location
            if (empty($locOccGroup['data'])) {
                $noLoc = true;
                $searchId = null;
            } else {
                $searchId = $locOccGroup['data'][0]['locationDbId'];
            }
        }

        // get occurrence or location by id
        $params = [
            "{$entity}DbId" => "$searchId",
            'fields' => "$entity.id AS {$entity}DbId",
        ];

        $model = "\\app\\models\\" . $model;
        $modelInstance = \Yii::$container->get($model);
        $entityData = $modelInstance->searchAll($params);

        // if occurrence exists
        if (!empty($entityData)) {
            // build csv data and export file
            $result = $this->plantingInstructions($program, $searchId, $entity, $noLoc);
            if ($result === false) {
                $this->redirect($redirectUrl)->send();
            }
        }

        exit;
    }

    /**
     * Build content in export planting instructions
     *
     * @param string $program prorgram identifier
     * @param integer $id occurrence identifier
     * @param string $entity occurrence | location
     * @param boolean $noLoc whether occurrence has location or not
     * @return boolean false if API response failed
     */
    public function plantingInstructions($program, $id, $entity = 'occurrence', $noLoc = false)
    {
        // Declare variables
        $retrieveExperimentData = false;
        $response = null;
        $modifier = "_$program";

        $plantingInsArr = [['No Location record found for the Occurrence.']];
        [
            $role,
        ] = $this->person->getPersonRoleAndType($program);

        // if the record exists
        if (!$noLoc) {
            [
                $csvAttributes,
                $csvHeaders,
            ] = $this->browserController->getColumns(
                    ($role == 'COLLABORATOR') ? $role : $program,
                    'DOWNLOAD_PLANTING_INSTRUCTION_FILES',
                    $entity,
                    mode: 'download'
                );

            // Section to retrieve experimentData config
            // Build config abbrev based on program and role
            if (strtoupper($role) == 'COLLABORATOR')
                $modifier = '_COLLABORATOR';

            $newAbbrev = "DOWNLOAD_PLANTING_INSTRUCTION_FILES$modifier";
                
            Yii::info('Get columns from configuration ' . json_encode(['program'=>$program,
                'configAbbrev'=>$newAbbrev]), __METHOD__);

            // Retrieve config
            $response = $this->api->getApiResults(
                'GET',
                'configurations',
                filter: "abbrev=$newAbbrev&limit=1",
                retrieveAll: true
            );

            // If specific config doesn't exist, retrieve the default
            if (empty($response['data'])) {
                $response = $this->api->getApiResults(
                    'GET',
                    'configurations',
                    filter: "abbrev=DOWNLOAD_PLANTING_INSTRUCTION_FILES_DEFAULT&limit=1",
                    retrieveAll: true
                );
            }

            // Realign api result for easy process
            $response = $response['data'][0]['configValue'][$entity];

            // Set retrieveExperimentData value based on config
            foreach ($response as $key => $value){
                foreach($value as $array => $val){
                    if($array == 'retrieveExperimentData') {
                        $retrieveExperimentData = $val;
                    }
                }
            } // End section to retrieve experimentData config

            // retrieve occurrence/location planting instructions
            $plantingIns = $this->api->getApiResults(
                'GET',
                $entity . 's/' . $id . '/planting-instructions?retrieveExperimentData='.$retrieveExperimentData,
                null,
                'sort=occurrenceName|plotNumber',
                true
            );

            $plantingInsArr = [[]];
            $i = 1;

            if (!empty($plantingIns['data']) && isset($plantingIns['data'])) {
                // headers
                foreach ($csvHeaders as $value) {
                    array_push($plantingInsArr[0], $value);
                }

                // loop through each plot records
                foreach ($plantingIns['data'] as $value) {
                    $plantingInsArr[$i] = [];

                    // all plot attributes
                    foreach ($csvAttributes as $v) {
                        if (isset($value[$v])) {
                            array_push($plantingInsArr[$i], $value[$v]);
                        } else {
                            array_push($plantingInsArr[$i], '');
                        }
                    }

                    $i += 1;
                }
            } else {
                $plantingInsArr = [['There are no planting instruction records found.']];
            }
        }

        // get attribute and suffix to be used in file name from config
        $uppercasedEntity = strtoupper($entity);
        $downloadFeature = "PLANTING_INSTRUCTIONS_$uppercasedEntity";

        // get file name from config by occurrence ID
        $fileName = $this->browserController->getFileNameFromConfigByEntityId($id, $downloadFeature, FileExport::CSV);

        header("Content-Disposition: attachment; filename={$fileName}; Content-Type: application/csv;");

        $fp = fopen('php://output', 'wb');

        if ($fp === false) {
            die('Failed to create CSV file.');
        }

        foreach ($plantingInsArr as $line) {
            fputcsv($fp, $line);
        }

        fclose($fp);
    }

    /**
     * Returns new notifications from background process
     */
    public function actionNewNotificationsCount(){
        $userId = $this->user->getUserId();

        $params = [
            'creatorDbId' => "equals $userId",
            'isSeen' => 'equals false',
            'jobStatus' => 'equals DONE|equals FAILED',
            'application' => '[OR] OCCURRENCES',
            'workerName' => '[OR] GenerateHarvestRecords'
        ];

        $sortParams = 'sort=modificationTimestamp:desc&limit=1';
        $result = $this->backgroundJob->searchAll($params, $sortParams);

        // if there were problems retrieving 
        if($result['status'] != 200){
            return json_encode([
                'success' => false,
                'error' => 'Error while retrieving background processes. Kindly contact Administrator.'
            ]);
        } else {
            // update the Occurrence status
           $this->updateOccurrenceStatus($userId);
        }

        return json_encode($result['totalCount']);
    }

    /**
     * Update Occurrence status based from bacgroud process status
     */
    public function updateOccurrenceStatus($userId){

        $this->occurrenceBackgroundJob->updateOccurrenceStatus($userId);
    }

    /**
     * Push notifications from background process
     */
    public function actionPushNotifications(){
        $userModel = new User();
        $userId = $userModel->getUserId();
        $notifs = $this->occurrenceBackgroundJob->pushNotifications($userId);

        return $this->renderAjax(
            'notifications',
            $notifs
        );
    }

    /**
     * Retrieve locationDbId given occurrenceDbId
     *
     * @return string $locationDbId
     */
    public function actionRetrieveLocationDbId($occurrenceDbId = null)
    {
        $occurrenceDbId = (!$occurrenceDbId) ? $_POST['occurrenceDbId'] : $occurrenceDbId;

        if ($occurrenceDbId) {
            $method = 'POST';
            $url = 'occurrences-search?limit=1';
            $params = [
                'occurrenceDbId' => $occurrenceDbId,
            ];

            $response = $this->api->getApiResults(
                $method,
                $url,
                json_encode($params),
                '',
                true
            );

            if (
                $response['success'] &&
                isset($response['data']) &&
                !empty($response['data'][0]['locationDbId'])
            )
                return ((string) $response['data'][0]['locationDbId']);

            return 'false';
        } else {
            return 'false';
        }
    }

    /**
     * Retrieve session variable "bg-process-task-type" value given transactionId
     * Retrieve additional (optional) data needed for the transaction
     *
     * @param string $transactionId ID for the current transaction clicked by the user
     *
     * @return object $bgProcessDetails
     */
    public function actionGetBgProcessDetails ($transactionId = null)
    {
        $transactionId = (!$transactionId) ? $_POST['transactionId'] : $transactionId;
        $taskType = Yii::$app->session->get('bg-process-task-type-' . $transactionId);
        $additionalData = null;

        if ($taskType == 'saveListMembers') {
            $additionalData = Yii::$app->session->get('bg-process-list-abbrev-' . $transactionId);
        }

        return json_encode([
            'taskType' => $taskType,
            'additionalData' => $additionalData,
        ]);
    }

    /**
     * Extract Occurrence advanced filter option values
     *
     * @param string $q id page filter
     * @param string $attr attribute to be retrieved
     * @param integer $page page number to be retrieved
     * @param string|integer $programId program identifier
     * 
     * @return array total count and filter tags
     */    
    public function actionGetFilterData (string $q=null, string $attr=null, int $page=null,$programId)
    {
        $offset = ($page==null ? 0 : (($page-1)*5)+1);
        $page = ($page==null ? 1 : $page);
        $limit = 5;
        $orderCond = "text:asc";
        $distinctOn = $attr."_id";
        $table = 'experiment';

        if($attr == 'year'){ // Get data for Year
            $orderCond = "id:desc";
            $distinctOn = "{$table}_{$attr}";
            $fields = "$distinctOn AS \"id\" | $distinctOn  AS \"text\" | $table.program_id AS \"programDbId\"";
            $attr = $distinctOn;
        }else if($attr == 'site'){ // Get data for Site
            $distinctOn = "id";
            $fields = "$attr.id AS \"id\" | $attr.geospatial_object_name  AS \"text\" | program.id AS \"programDbId\"";
        }else if(in_array($attr, ['season','stage'])){ // Get data for Season/Stage
            $fields = "$table.$distinctOn AS \"id\" | $attr.{$attr}_code AS \"text\" | $table.program_id AS \"programDbId\"";
        }else if ($attr == 'occurrence_name'){ // Get data for Occurrence Name
            $table = 'occurrence';
            $distinctOn = 'id';

            $fields = "$table.$distinctOn AS \"id\" | $table.{$attr} AS \"text\" | experiment.program_id AS \"programDbId\"";
        } else if ($attr == 'field_name') { // Get data for Field Name
            $table = 'field';
            $distinctOn = 'id';
            // Set $attr to 'field' so the API call (in $apiCallResult)...
            // ...recognizes the attribute to be requested
            $attr = 'field';

            // Include these fields to the "fields" request body parameter
            $fields = "$table.$distinctOn AS \"id\" | $table.geospatial_object_name AS \"text\" | experiment.program_id AS \"programDbId\"";
        } else if ($attr == 'location') { // Get data for Location Name
            $table = $attr;
            $distinctOn = 'id';

            // Include these fields to the "fields" request body parameter
            $fields = "$table.$distinctOn AS \"id\" | $table.location_name AS \"text\" | experiment.program_id AS \"programDbId\"";
        }else{
            $distinctOn = 'id';
            $fields = "$table.$distinctOn AS \"id\" | $table.{$attr} AS \"text\" | $table.program_id AS \"programDbId\"";
        }

        $params = [
            'fields' => $fields,
            'distinctOn' => 'id'
        ];

        // if non-admin user and collaborator role, apply permission
        $program = Yii::$app->userprogram->get('abbrev');
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        $ownershipTypeCond = '';
        // if non-admin and collaborator role, retrieve only shared occurrences
        if (!$isAdmin && strtoupper($role) == 'COLLABORATOR'){
            $ownershipTypeCond = '&ownershipType=shared';
        } else {
            $params['programDbId'] = (string) $programId;
        }

        if($q != null && $q != ""){
            $outputColumn = $attr;
            $params[$outputColumn] = '%'.trim($q).'%'; 
            $params['fields'] = $outputColumn.' |'.$params['fields'];
        }

        $resultLimits = ($limit == NULL || $limit == 0 ? '?page='.($offset+1) : '?limit='.$limit.'&page='.(($offset/$limit)+1));
    
        $method = 'POST';
        $endPoint = 'occurrences-search';

        $apiCallResult = Yii::$app->api->getParsedResponse($method,$endPoint.$resultLimits, json_encode($params), 'sort=text'.$ownershipTypeCond);
    
        $trackArr = [];
        $result = [];

        if(!empty($apiCallResult['data'])){
            foreach($apiCallResult['data'] as $value){
                if (!empty($value['text']) && !in_array($value['text'], $trackArr)) {
                    $trackArr[] = $value['text'];
                    $result[] = ["id" => $value["id"], "text" => $value['text']];
                }
            }
        }
        
        return json_encode(['totalCount'=> $apiCallResult['totalCount'],'items'=>$result]);    
    }

    /** 
     * Determine whether to access Harvest Manager with a Location ID or none
     * 
     * @param integer|string $occurrenceDbIds Currently selected Occurrence(s)
     *                                        from Experiment Manager data browser
     * 
     * @return array An array containing information to determine which Toast
     *               message to display and which manner of accessing Harvest
     *               Manager to use via javascript
    */
    public function actionDetermineHarvestManagerAccessingMethod ($occurrenceDbIds = '', $dataLevel = '')
    {
        $isAdmin = Yii::$app->session->get('isAdmin');

        $ERROR = 0;
        $ACCESS_HM_WITHOUT_LOCATION_ID = 1;
        $ACCESS_HM_WITH_LOCATION_ID = 2;
        $ACCESS_HM_AT_CROSS_LEVEL = 3;
        $ACCESS_HM_AT_PLOT_LEVEL = 4;
        $EXPERIMENT_TYPE_INVALID = 5;
        $EXPERIMENT_STATUS_NOT_PLANTED = 8;
        $NO_WRITE_ACCESS = 9;

        $SUCCESS_CASES = [
            '' => $ACCESS_HM_WITH_LOCATION_ID,
            'cross' => $ACCESS_HM_AT_CROSS_LEVEL,
            'plot' => $ACCESS_HM_AT_PLOT_LEVEL,
        ];

        // Set default response
        $response = [[
            'statusCode' => $ERROR,
            'occurrenceData' => null,
        ]];

        // Either assign a value from an ajax POST prop...
        // ...or a value from itself (i.e. either null or an actual ID value)
        $occurrenceDbIds = isset($_POST['occurrenceDbIds']) ?
            $_POST['occurrenceDbIds'] : $occurrenceDbIds;
        $dataLevel = isset($_POST['dataLevel']) ?
            $_POST['dataLevel'] : $dataLevel ;

        if ($occurrenceDbIds == '') {
            $response[0]['statusCode'] = $ACCESS_HM_WITHOUT_LOCATION_ID;

            return json_encode($response);
        }
        
        $occurrencesData = $this->occurrence->getOccurrenceExperimentInfoAndLocationDbId($occurrenceDbIds);
        $response = [];

        // If there was a problem in retrieving occurrences info
        if (empty($occurrencesData)) {
            $response['statusCode'] = $ERROR;
            return json_encode($response);
        }

        foreach ($occurrencesData as $occurrenceData) {
            // If parent Experiment's Type is NEITHER...
            // ...Breeding Trial, Generation Nursery,...
            // ...Cross Parent Nursery, NOR Intentional Crossing Nursery
            if (!in_array(
                    strtolower($occurrenceData['experimentType']),
                    [
                        'breeding trial',
                        'generation nursery',
                        'cross parent nursery',
                        'intentional crossing nursery',
                        'observation',
                    ]
            )) {
                array_push($response, [
                    'statusCode' => $EXPERIMENT_TYPE_INVALID,
                    'occurrenceData' => $occurrenceData,
                ]);

                continue;
            }

            // Check if user has write permission to occurrence
            $occurrence = $this->api->getApiResults(
                'POST',
                'occurrences-search',
                json_encode([
                    'occurrenceDbId' => 'equals ' . $occurrenceData['occurrenceDbId'],
                ]),
                '',
                false
            );

            $hasWritePermission = (!$isAdmin) ? Yii::$app->access->renderAccess(
                "HARVEST_OCCURRENCE",
                'HARVEST_MANAGER',
                $occurrence['data'][0]['creatorDbId'],
                $occurrence['data'][0]['accessData'],
                'write'
            ) : true;

            // If the user has no write access to occurrence...
            // ...deny the action to access Harvest Manager
            if (!$hasWritePermission) {
                $response[0]['statusCode'] = $NO_WRITE_ACCESS;

                return json_encode($response);
            }

            // If occurrence is NOT PLANTED and data level is either 'plot'...
            // ...or empty (''), deny the action to access Harvest Manager
            if (
                ($dataLevel == 'plot' || $dataLevel == '') &&
                !str_contains(strtolower(
                    $occurrenceData['occurrenceStatus']),
                    'planted')
            ) {
                array_push($response, [
                    'statusCode' => $EXPERIMENT_STATUS_NOT_PLANTED,
                    'occurrenceData' => $occurrenceData,
                ]);

                continue;
            }

            array_push($response, [
                'statusCode' => $SUCCESS_CASES[$dataLevel],
                'occurrenceData' => $occurrenceData,
            ]);
        }

        return json_encode($response);
    }

    /**
     * Check if user has write permission to occurrence by ID 
     * 
     */
    public function actionCheckWritePermission ()
    {
        $occurrenceIds = $_POST['occurrenceIds'] ?? [];

        // check if has write permissions in selected occurrences
        $response = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'fields' => 'occurrence.id AS occurrenceDbId',
                'occurrenceDbId' => "equals " . implode('|equals ', $occurrenceIds),
            ]),
            'permission=write',
            true
        );

        $writeTotalCount = $response['totalCount'];

        // if no permission to at least one selected occurrence
        if($writeTotalCount != count($occurrenceIds)){
            return json_encode([
                'noAccessCount' => (count($occurrenceIds) - $writeTotalCount)
            ]);
        }

        return true;
    }

    /**
     * Search for occurrences filtered by checkbox selection through HTTP POST.
     *
     * Note: Currently only used when all occurrences are selected.
     * 
     * @return array array of Occurrence DB IDs
     */
    public function actionRetrieveDbIds ()
    {
        // Set variables for buildFieldsParam() and API request
        [   $currPage,
            $paramLimit,
            $paramPage,
            $paramSort,
            $isOwnedCond,
            $filters,
            $selectOccurrenceThreshold,
        ] = Yii::$app->session->get('occurrence-search-occurrences-session-vars');

        $pageSize = ((int) substr($paramLimit, 6));

        // If $paramLimit is greater than $selectOccurrenceThreshold...
        // ...then set the $paramLimit to $selectOccurrenceThreshold
        if ($pageSize > $selectOccurrenceThreshold) {
            $paramLimit = "limit=$selectOccurrenceThreshold";
        }

        Yii::debug('Retrieving DB IDs for checkbox selection ' . json_encode(compact(
            'currPage',
            'paramLimit',
            'paramPage',
            'paramSort',
            'isOwnedCond',
            'filters',
            'selectOccurrenceThreshold'
        )), __METHOD__);

        // Aside from Occurrence ID, these are the default fields to be retrieved
        $defaultFieldsParams = 'experiment.steward_id AS experimentStewardDbId|creator.id AS creatorDbId|location.id AS locationDbId|location.location_code AS locationCode|occurrence.occurrence_status AS occurrenceStatus|occurrence.occurrence_number AS occurrenceNumber|experiment.experiment_code AS experimentCode|occurrence.plot_count AS plotCount';
        $url = '';
        $checkedBoxesString = isset($_POST['checkedBoxesString']) ?  $_POST['checkedBoxesString'] : '';
        $attributeMap = [
            'occurrenceDbId' => 'occurrence.id AS occurrenceDbId',
            'occurrenceCode' => 'occurrence.occurrence_code AS occurrenceCode',
            'occurrenceName' => 'occurrence.occurrence_name AS occurrenceName',
            'occurrenceNumber' => 'occurrence.occurrence_number AS occurrenceNumber',
            'occurrenceStatus' => 'occurrence.occurrence_status AS occurrenceStatus',
            'locationDbId' => 'location.id AS locationDbId',
            'location' => 'location.location_name AS location',
            'locationCode' => 'location.location_code AS locationCode',
            'plotCount' => 'occurrence.plot_count AS plotCount',
            'programDbId' => 'program.id AS programDbId',
            'programCode' => 'program.program_code AS programCode',
            'program' => 'program.program_name AS program',
            'projectDbId' => 'project.id AS projectDbId',
            'projectCode' => 'project.project_code AS projectCode',
            'project' => 'project.project_name AS project',
            'siteDbId' => 'site.id AS siteDbId',
            'siteCode' => 'site.geospatial_object_code AS siteCode',
            'site' => 'site.geospatial_object_name AS site',
            'fieldDbId' => 'field.id AS fieldDbId',
            'fieldCode' => 'field.geospatial_object_code AS fieldCode',
            'field' => 'field.geospatial_object_name AS field',
            'geospatialObjectId' => 'occurrence.geospatial_object_id AS geospatialObjectId',
            'geospatialObjectCode' => 'goi.geospatial_object_code AS geospatialObjectCode',
            'geospatialObject' => 'goi.geospatial_object_name AS geospatialObject',
            'geospatialObjectType' => 'goi.geospatial_object_type AS geospatialObjectType',
            'experimentDbId' => 'occurrence.experiment_id AS experimentDbId',
            'experimentCode' => 'experiment.experiment_code AS experimentCode',
            'experiment' => 'experiment.experiment_name AS experiment',
            'experimentType' => 'experiment.experiment_type AS experimentType',
            'experimentStatus' => 'experiment.experiment_status AS experimentStatus',
            'experimentStageDbId' => 'stage.id AS experimentStageDbId',
            'experimentStageCode' => 'stage.stage_code AS experimentStageCode',
            'experimentStage' => 'stage.stage_name AS experimentStage',
            'experimentYear' => 'experiment.experiment_year AS experimentYear',
            'experimentSeasonDbId' => 'season.id AS experimentSeasonDbId',
            'experimentSeasonCode' => 'season.season_code AS experimentSeasonCode',
            'experimentSeason' => 'season.season_name AS experimentSeason',
            'experimentDesignType' => 'experiment.experiment_design_type AS experimentDesignType',
            'experimentStewardDbId' => 'experiment.steward_id AS experimentStewardDbId',
            'experimentSteward' => 'steward.person_name AS experimentSteward',
            'locationSteward' => 'location_steward.person_name AS locationSteward',
            'dataProcessDbId' => 'experiment.data_process_id AS dataProcessDbId',
            'dataProcessAbbrev' => 'item.abbrev AS dataProcessAbbrev',
            'repCount' => 'occurrence.rep_count AS repCount',
            'creatorDbId' => 'creator.id AS creatorDbId',
        ];
        $totalIdsRetrieved = 0;

        $url = "occurrences-search?$paramPage&$paramLimit&$paramSort&$isOwnedCond";

        if ($checkedBoxesString) {
            $idArray = explode('|', $checkedBoxesString);
            $checkedBoxesString = '';

            // Add "equals" condition to improve query speed
            foreach ($idArray as $key => $id) {
                $checkedBoxesString .= "equals $id |";
            }

            $filters["occurrenceDbId"] = $checkedBoxesString;
        }

        $fieldsParam = $this->browser->buildFieldsParam(
            $attributeMap,
            $defaultFieldsParams,
            $paramSort,
            $filters
        );
        // Ensure that the fields parameter is unique
        $fieldsParam = implode('|', array_unique(explode('|', $fieldsParam)));

        // Check if fieldsParam has occurrenceDbId especially with sorting / filters
        if(!str_contains($fieldsParam,"occurrenceDbId")){
            $fieldsParam .= "| occurrence.id AS occurrenceDbId";
        }

        // if there's a fields parameter
        if (!empty($fieldsParam)) {
            $filters['fields'] = $fieldsParam;
        }

        $retrievedDbIdArray = [];
        $sliceSize = 0;
        do {
            $response = $this->api->getApiResults(
                'POST',
                $url,
                json_encode($filters)
            );
            
            if (
                $response['success'] &&
                isset($response['data']) &&
                !empty($response['data'])
            ) {
                $data = $response['data'];
            }

            // If $sliceSize is greater than 0, slice the data array...
            // ...so that the $totalIdsRetrieved will not exceed $selectOccurrenceThreshold
            if ($sliceSize) {
                $data = array_slice($data, 0, $sliceSize);
            }

            // retrieve the following occurrence info
            foreach ($data as $value) {
                array_push($retrievedDbIdArray, [
                    'id' => "{$value['occurrenceDbId']}",
                    'experimentStewardId' => "{$value['experimentStewardDbId']}",
                    'creatorId' => "{$value['creatorDbId']}",
                    'locationDbId' => "{$value['locationDbId']}",
                    'locationCode' => "{$value['locationCode']}",
                    'occurrenceStatus' => "{$value['occurrenceStatus']}",
                    'occurrenceNumber' => "{$value['occurrenceNumber']}",
                    'experimentCode' => "{$value['experimentCode']}",
                    'plotCount' => "{$value['plotCount']}",
                ]);
            }
    

            // Update retrieved IDs and current page
            $totalIdsRetrieved += count($response['data']);
            $nextTotalIdsRetrieved = $totalIdsRetrieved + $pageSize;

            // Get current total row count and next total count
            $currPage += 1;
            
            // If $nextTotalRowCount is greater than $selectOccurrenceThreshold...
            // ...then set the value for $sliceSize
            if ($nextTotalIdsRetrieved > $selectOccurrenceThreshold) {
                $sliceSize = $selectOccurrenceThreshold - $totalIdsRetrieved;
            }

            $paramPage = "page=$currPage";

            $url = "occurrences-search?$paramPage&$paramLimit&$paramSort&$isOwnedCond";

        } while ($currPage <= $response['totalPages'] && $totalIdsRetrieved < $selectOccurrenceThreshold);

        return json_encode($retrievedDbIdArray);
    }

    /**
     * Retrieves valid and invalid occurrences for creating a packing job (PIM)
     *
     * @return mixed object containing valid and invalid occurrences
    */
    public function actionRetrieveValidAndInvalidOccurrences ()
    {
        $occurrenceDbIds = $_POST['occurrenceDbIds'] ?? [];
        $validOccurrences = [];
        $invalidOccurrences = [];

        // Add "equals" modifier to occurrence IDs for faster query performance
        array_walk($occurrenceDbIds, function (&$value) {
            $value = "equals $value";
        });

        $response = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'fields' => 'occurrence.id AS occurrenceDbId | occurrence.occurrence_status AS occurrenceStatus | occurrence.occurrence_name AS occurrenceName',
                'occurrenceDbId' => implode('|', $occurrenceDbIds),
            ]),
            '',
            true
        );

        if ($response['success'] && !empty($response['data'])) {
            $data = $response['data'];
            $totalCount = $response['totalCount'];

            foreach ($data as $value) {
                $occurrenceStatus = $value['occurrenceStatus'];

                if (
                    str_contains($occurrenceStatus, 'planted') ||
                    str_contains($occurrenceStatus, 'packing') ||
                    str_contains($occurrenceStatus, 'packed')
                ) {
                    $invalidOccurrences []= $value['occurrenceName'];
                } else {
                    $validOccurrences []= $value['occurrenceDbId'];
                }
            }

            // check if has write permissions in selected occurrences
            $response = $this->api->getApiResults(
                'POST',
                'occurrences-search',
                json_encode([
                    'fields' => 'occurrence.id AS occurrenceDbId',
                    'occurrenceDbId' => implode('|', $occurrenceDbIds),
                ]),
                'permission=write',
                true
            );

            if($response['success']){
                $writeTotalCount = $response['totalCount'];

                if($totalCount != $writeTotalCount){
                    return json_encode([
                        'noAccessCount' => ($totalCount - $writeTotalCount)
                    ]);
                }
            }

            return json_encode([
                'invalidOccurrences' => $invalidOccurrences,
                'validOccurrences' => $validOccurrences,
            ]);
        }
    }

    /**
     * Retrieve experiment IDs given occurrence IDs
     * 
     * @return array array of experiment IDs
     */
    public function actionRetrieveExperimentDbIds ()
    {
        $occurenceDbIds = $_POST['occurrenceDbIds'] ?? null;
        $experimentDbIdArray = [];

        array_walk($occurenceDbIds, function (&$value, $key) {
            $value = "equals $value";
        });

        $response = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'fields' => 'occurrence.id AS occurrenceDbId | occurrence.experiment_id AS experimentDbId',
                'occurrenceDbId' => implode('|', $occurenceDbIds),
            ]),
            '',
            true
        );

        $totalCount = $response['totalCount'];

        // check if user has write access to all selected occurrences
        $writePermissionResponse = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'fields' => 'occurrence.id AS occurrenceDbId',
                'occurrenceDbId' => implode('|', $occurenceDbIds),
            ]),
            'permission=write',
            true
        );

        if($writePermissionResponse['success']){
            $writeTotalCount = $writePermissionResponse['totalCount'];

            if($totalCount != $writeTotalCount){
                return json_encode([
                    'noAccessCount' => ($totalCount - $writeTotalCount)
                ]);
            }
        }

        if ($response['success'] && !empty($response['data'])) {
            $data = $response['data'];

            foreach ($data as $key => $value) {
                $experimentDbId = $value['experimentDbId'];

                if (!in_array($experimentDbId, $experimentDbIdArray))
                    array_push($experimentDbIdArray, $experimentDbId);
            }
        }

        $experimentDbIdString = implode('|', $experimentDbIdArray);

        return json_encode([
            'experimentDbIdString' => $experimentDbIdString,
        ]);
    }

    /**
     * Retrieve data process abbrevs given occurrence IDs
     * 
     * @return array array of data process abbrevs
     */
    public function actionRetrieveDataProcessAbbrevs ()
    {
        $occurenceDbIds = $_POST['occurrenceDbIds'] ?? null;
        $dataProcessAbbrevArray = [];

        array_walk($occurenceDbIds, function (&$value, $key) {
            $value = "equals $value";
        });

        $response = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'fields' => 'occurrence.id AS occurrenceDbId | item.abbrev AS dataProcessAbbrev',
                'occurrenceDbId' => implode('|', $occurenceDbIds),
            ]),
            '',
            true
        );

        if ($response['success'] && !empty($response['data'])) {
            $data = $response['data'];

            foreach ($data as $key => $value) {
                $dataProcessAbbrev = $value['dataProcessAbbrev'];

                if (!in_array($dataProcessAbbrev, $dataProcessAbbrevArray))
                    array_push($dataProcessAbbrevArray, $dataProcessAbbrev);
            }
        }

        $dataProcessAbbrevString = implode('|', $dataProcessAbbrevArray);

        return json_encode([
            'dataProcessAbbrevString' => $dataProcessAbbrevString,
        ]);
    }

    /**
     * Download data (CSV or JSON format) from filepath 
     * 
     * @param int|string $entityId unique identifier of an entity
     * @param string $entity entity name
     * @param string $description description of the feature that triggered the background processor
     * @param string $filename file name identifier
     * @param string $dataFormat data format identifier
     */
    public function actionDownloadData ($filename, $dataFormat)
    {
        $filename = $_POST['filename'] ?? $filename;
        $filename = str_contains($filename, '.') ? $filename : "$filename.$dataFormat";

        $pathToFile = realpath(Yii::$app->basePath) . "/files/data_export/$filename";

        if (file_exists($pathToFile)) {
            // Set headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($pathToFile));

            // Flush system output buffer
            flush();

            // Read/Download CSV file
            readfile($pathToFile);

            die();
        }
    }

    /**
     * Export/Download Data collection summaries in CSV
     *
     * @param string|int $occurrenceDbId unique occurrence identifier
     * @param string $program program code identifier
     * @param string $columnHeaderType determinant of retrieved column headers and orientation of generated report
     * @param string $experimentCode experiment code
     * @param string $occurrenceNumber occurrence number
     * @param int $plotCount plot count
     */
    public function actionExportDataCollectionSummaries (
        $occurrenceDbId,
        string $program,
        string $columnHeaderType,
        string $experimentCode = '',
        string $occurrenceNumber = '',
        int $plotCount = 0
    )
    {
        Yii::info(
            'Exporting data collection summaries in CSV '.
            json_encode([
                'occurrenceDbId' => $occurrenceDbId,
                'program' => $program,
                'columnHeaderType' => $columnHeaderType,
                'experimentCode' => $experimentCode,
                'occurrenceNumber' => $occurrenceNumber,
                'plotCount' => $plotCount,
            ]),
            __METHOD__
        );        

        $occurrenceDbId = $_POST['occurrenceDbId'] ?? $occurrenceDbId;
        $columnHeaderType = strtolower($_POST['columnHeaderType'] ?? $columnHeaderType);
        $experimentCode = $_POST['experimentCode'] ?? $experimentCode;
        $occurrenceNumber = $_POST['occurrenceNumber'] ?? $occurrenceNumber;
        $plotCount = (int) ($_POST['plotCount'] ?? $plotCount);
        
        // get attribute and suffix to be used in file name from config
        [$fileNameAttribute, $fileNameSuffix] = Yii::$app->config->getFileNameAttributeAndSuffix('DATA_COLLECTION_SUMMARIES');

        if($fileNameAttribute == 'experimentCode'){
            $fileName = $experimentCode;
        } else {
            // retrieve occurrence info for CSV filename
            $response = $this->occurrenceSearch->getOccurrenceForFileNameById($occurrenceDbId);
            $fileName = isset($response['data'][0][$fileNameAttribute]) ? $response['data'][0][$fileNameAttribute] : $fileNameAttribute;
        }

        $toExport = new FileExport(
            fileFormat: FileExport::CSV,
            time: time(),
            shouldUseIndicatedFileFormat: true,
            fileName: $fileName,
            suffix: $fileNameSuffix
        );

        // if attribute has experiment, use occurrence number in file name
        if(strpos($fileNameAttribute,'experiment') !== false){
            $filename = $toExport->getFileNames(
                $fileName,
                $toExport->formatSerialCodes([ $occurrenceNumber ])
            )[0];
        } else{
            $filename = $toExport->getFileName();
        }

        // Get background processing threshold
        $dataCollectionSummariesWorkerThreshold = Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportDataCollectionSummaries');

        // Trigger worker if plot count is at least the worker threshold amount
        if ($plotCount >= $dataCollectionSummariesWorkerThreshold) {
            $description = 'Preparing Data Collection summaries for downloading';

            // If multiple occurrences were selected...
            // ...get "Multi-Experiments..." file name format
            if (substr_count($occurrenceDbId, ',') > 0) {
                $filename = $toExport->getZippedFilename();
            }

            $filename = str_replace('.csv', '', $filename);
            $result = $this->worker->invoke(
                'BuildCsvData',
                $description,
                'PLOT',
                explode(',', $occurrenceDbId)[0],
                'EXPERIMENT',
                'OCCURRENCES',
                'POST',
                [
                    'url' => "data-collection-summaries?occurrenceDbId=$occurrenceDbId&columnHeaderType=$columnHeaderType",
                    'description' => $description,
                    'fileName' => $filename,
                    'httpMethod' => 'GET',
                ],
                [
                    'remarks' => $filename,
                ]
            );

            // change delimiter from comma ',' to pipe '|'
            $occurrenceDbIdPiped = implode('|', explode(',', $occurrenceDbId));

            // If background processing failed
            if (!$result['success']) {
                $message = "There was a problem in the background processing. Please try again or file a report for assistance.";

                // set background processing info to session
                Yii::$app->session->set('em-bg-processs-message', $message);
                Yii::$app->session->set('em-bg-processs', true);

                // Get program
                $program = Yii::$app->session->get('program');
                Yii::$app->response->redirect(["/occurrence?program=$program&OccurrenceSearch[occurrenceDbId]=$occurrenceDbIdPiped"]);
            } else {
                $message = "The Data Collection summaries are being created in the background. You may proceed with other tasks. You will be notified in this page once done.";

                // set background processing info to session
                Yii::$app->session->set('em-bg-processs-message', $message);
                Yii::$app->session->set('em-bg-processs', true);

                // Get program
                $program = Yii::$app->session->get('program');
                Yii::$app->response->redirect(["/occurrence?program=$program&OccurrenceSearch[occurrenceDbId]=$occurrenceDbIdPiped"]);
            }

            return;
        }

        $csvHeaders = [];
        $dataCollectionSummaries = [];
        $response = $this->api->getApiResults(
            'GET',
            'data-collection-summaries',
            null,
            "occurrenceDbId=$occurrenceDbId&columnHeaderType=$columnHeaderType",
            true
        );

        $data = $response['data'];
        $csvHeaders = array_keys($response['data'][0] ?? []);

        foreach ($data as $i => $value) {
            $dataCollectionSummaries []= array_values($value);
        }

        // preprend CSV column headers into array
        array_unshift($dataCollectionSummaries, $csvHeaders);

        // Get appropriate filename based on how many occurrences were selected
        if (substr_count($occurrenceDbId, ',') > 0) {
            $filename = $toExport->getZippedFilename();
        }

        // Set headers
        header('Content-Type: application/csv');
        header("Content-Disposition: attachment; filename={$filename};");
    
        // Write to output stream
        $fp = fopen('php://output', 'w');

        if (!$fp) {
            die('Failed to create CSV file.');
        }

        foreach ($dataCollectionSummaries as $line) {
            fputcsv($fp, $line);
        }

        exit;
    }

    /**
     * Export/Download Data collection files in CSV (Zipped)
     *
     * @param array|object $selectedDbIds list of selected DbIds (Occurrence, Location, or both)
     * @param string $entity identifies whether data is of Occurrence or Location
     * @param boolean $hasHeaders flag for whether or not to include trait abbrevs as additional CSV headers
     */
    public function actionExportDataCollectionFiles ($selectedDbIds, $entity, $hasHeaders = '')
    {
        $idArray = $_POST['selectedDbIds'] ?? $selectedDbIds;
        $entity = $_POST['entity'] ?? $entity;
        $hasHeaders = (boolean) $_POST['hasHeaders'] ?? $hasHeaders;

        Yii::info(`Exporting data collection files in CSV (Zipped) \n` . json_encode(compact('idArray', 'entity', 'hasHeaders'), JSON_PRETTY_PRINT), __METHOD__);

        [
            $csvAttributes,
            $csvHeaders,
        ] = $this->browserController->getColumns(
            Yii::$app->session->get('program') ?? '',
            'DOWNLOAD_DATA_COLLECTION_FILES',
            'DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS',
            [],
            'download',
            'abbrev'
        );
        $entityDbId = null;
        $description = 'Preparing Data Collection files for downloading';
        $url = '';
        $filename = '';
        $zipFileName = '';
        $httpMethod = 'POST';
        $processName = 'buildTraitDataCollectionData';
        $entityId = array_keys($idArray)[0];
        $endpointEntity = strtoupper($entity);
        $application = 'OCCURRENCES';
        $workerName = 'BuildCsvData';
        $fileName = [];
        $serialCodes = [];

        if ($entity === 'occurrence') {
            $zipFileName = 'Multi_Occurrences_Trait_Data_Collection_CSV_' . date("Ymd_His", time());
        } else if ($entity === 'location') {
            $zipFileName = 'Multi_Locations_Trait_Data_Collection_CSV_' . date("Ymd_His", time());

            // Get all mapped Occurrence IDs for each Location
            if ($hasHeaders) {
                foreach ($idArray as $entityDbId => $entityInfo) {
                    $data = $this->api->getApiResults(
                        'POST',
                        'occurrences-search',
                        json_encode([
                            "fields" => "occurrence.id AS occurrenceDbId | location.id AS locationDbId",
                            "locationDbId" => "equals $entityDbId",
                        ])
                    )['data'];
    
                    foreach ($data as $value) {
                        if (!isset($idArray[$entityDbId]['occurrenceDbIds'])) {
                            $idArray[$entityDbId]['occurrenceDbIds'] = [ $value['occurrenceDbId'] ];
                        } else {
                            $idArray[$entityDbId]['occurrenceDbIds'] []= $value['occurrenceDbId'];
                        }
                    }
                }
            }
        }

        // Generate params and file names for respective Occurrences/Locations
        foreach ($idArray as $entityDbId => $entityInfo) {
            $occurrenceId = ($entity === 'location') ? '' : $entityDbId;
            $locationId = ($entity === 'location') ? $entityDbId : ($entityInfo['locationDbId'] ?? '');
            $entityCsvAttributes = $csvAttributes;
            $entityCsvHeaders = $csvHeaders;
            $method = 'POST';
            $path = '';
            $filter = '';
            $rawData = [];
            $excludeOccurrenceDbId = false;

            // Always include occurrenceDbId when retrieving either Occurrence- or Location-level plots
            if (!array_search('occurrenceDbId', $entityCsvAttributes)) {
                $entityCsvAttributes []= 'occurrenceDbId';
                $excludeOccurrenceDbId = true;
            }

            array_unshift($entityCsvAttributes, 'plotDbId');
            array_unshift($entityCsvHeaders, 'PLOT_ID');

            // If "GERMPLASM" is present, change it to "DESIGNATION"
            $index = array_search('GERMPLASM', $entityCsvHeaders);
            if ($index) {
                $entityCsvHeaders[$index] = 'DESIGNATION';
            }

            if ($locationId) {
                $method = 'POST';
                $path = "locations/$locationId/plots-search";
    
                // Add these sort parameters if corresponding csvAttributes exist
                $sortParams = [];
                if (array_search('occurrenceCode', $entityCsvAttributes))
                    $sortParams []= $entityCsvAttributes[array_search('occurrenceCode', $entityCsvAttributes)];
                if (array_search('plotNumber', $entityCsvAttributes))
                    $sortParams []= $entityCsvAttributes[array_search('plotNumber', $entityCsvAttributes)];
                $sortParams = implode('|', $sortParams);
                $filter = '';
                $filter = (empty($sortParams)) ? '' : "sort={$sortParams}";
    
                // Retrieve only plots of mapped/planted occurrences of a location
                $rawData['fields'] = $this->generateFields([ ...$entityCsvAttributes, 'occurrenceStatus'], $path);
                if ($occurrenceId) {
                    $rawData['occurrenceDbId'] = "equals $occurrenceId";
                }
                $rawData['occurrenceStatus'] = 'mapped%|planted%';
            } else if ($occurrenceId) {
                $method = 'POST';
                $path = "occurrences/$occurrenceId/plots-search";
                $filter = (array_search('plotNumber', $entityCsvAttributes)) ? '' : "sort=plotNumber";
                $rawData = [
                    'fields' => $this->generateFields($entityCsvAttributes, $path),
                ];
            }
        
            // Exclude occurrenceDbId from being written in the CSV file...
            // ...if current config doesn't include occurrenceDbId
            if ($excludeOccurrenceDbId) {
                unset($entityCsvAttributes[array_search('occurrenceDbId', $entityCsvAttributes)]);
            }

            $idArray[$entityDbId]['params'] = [
                'csvAttributes' => $entityCsvAttributes,
                'csvHeaders' => $entityCsvHeaders,
                'method' => $method,
                'path' => $path,
                'filter' => $filter,
                'rawData' => $rawData,
            ];

            // Generate file name
            $feature = 'TRAIT_DATA_COLLECTION_' . strtoupper($entity);

            $idArray[$entityDbId]['fileName'] = $this->browserController->getFileNameFromConfigByEntityId($entityDbId, $feature, FileExport::CSV, $entity);
        }

        try {
            $this->worker->invoke(
                $workerName,
                $description,
                strtoupper($entity),
                $entityId,
                $endpointEntity,
                $application,
                $httpMethod,
                compact(
                    'processName',
                    'description',
                    'zipFileName',
                    'httpMethod',
                    'idArray',
                    'entity',
                    'hasHeaders',
                ),
                [
                    'remarks' => $zipFileName,
                ]
            );
        } catch (\Exception $e) {
            return json_encode([ 
                'success' => false,
                'error' => [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                ],
            ]);
        }

        return json_encode([
            'success' => true,
        ]);
    }

    /**
     * Generate string of fields for API request body
     *
     * @param array $attributes list of specified attributes
     * @param string $endpoint API URL endpoint
     * 
     * @return string generated string of fields
     */
    public function generateFields ($attributes, $endpoint)
    {
        $locationsPlotsSearchAttributes = [
            'plotDbId' => 'plot.id AS plotDbId',
            'occurrenceDbId' => 'plot.occurrence_id AS occurrenceDbId',
            'occurrenceCode' => 'occurrence.occurrence_code AS occurrenceCode',
            'occurrenceName' => 'occurrence.occurrence_name AS occurrenceName',
            'occurrenceStatus' => 'occurrence.occurrence_status AS occurrenceStatus',
            'entryDbId' => 'plot.entry_id AS entryDbId',
            'entryCode' => 'pi.entry_code AS entryCode',
            'entryNumber' => 'pi.entry_number AS entryNumber',
            'entryName' => 'pi.entry_name AS entryName',
            'entryType' => 'pi.entry_type AS entryType',
            'entryRole' => 'pi.entry_role AS entryRole',
            'entryClass' => 'pi.entry_class AS entryClass',
            'entryStatus' => 'pi.entry_status AS entryStatus',
            'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
            'germplasmCode' => 'g.germplasm_code AS germplasmCode',
            'germplasmState' => 'g.germplasm_state AS germplasmState',
            'germplasmType' => 'g.germplasm_type AS germplasmType',
            'parentage' => 'g.parentage AS parentage',
            'seedDbId' => 'pi.seed_id AS seedDbId',
            'seedCode' => 'seed.seed_code AS seedCode',
            'seedName' => 'seed.seed_name AS seedName',
            'packageDbId' => 'pi.package_id AS packageDbId',
            'packageCode' => 'package.package_code AS packageCode',
            'packageLabel' => 'package.package_label AS packageLabel',
            'plotCode' => 'plot.plot_code AS plotCode',
            'plotNumber' => 'plot.plot_number AS plotNumber',
            'plotType' => 'plot.plot_type AS plotType',
            'rep' => 'plot.rep',
            'designX' => 'plot.design_x AS designX',
            'designY' => 'plot.design_y AS designY',
            'plotOrderNumber' => 'plot.plot_order_number AS plotOrderNumber',
            'paX' => 'plot.pa_x AS paX',
            'paY' => 'plot.pa_y AS paY',
            'fieldX' => 'plot.field_x AS fieldX',
            'fieldY' => 'plot.field_y AS fieldY',
            'blockNumber' => 'plot.block_number AS blockNumber',
            'harvestStatus' => 'plot.harvest_status AS harvestStatus',
            'plotStatus' => 'plot.plot_status AS plotStatus',
            'plotQcCode' => 'plot.plot_qc_code AS plotQcCode',
            'creationTimestamp' => 'pi.creation_timestamp AS creationTimestamp',
            'creatorDbId' => 'creator.id AS creatorDbId',
            'creator' => 'creator.person_name AS creator',
            'modificationTimestamp' => 'plot.modification_timestamp AS modificationTimestamp',
            'modifierDbId' => 'modifier.id AS modifierDbId',
            'modifier' => 'modifier.person_name AS modifier',
        ];

        $occurrencesPlotsSearchAttributes = [
            'plotDbId' => 'plot.id AS plotDbId',
            'entryDbId' => 'plot.entry_id AS entryDbId',
            'entryCode' => 'pi.entry_code AS entryCode',
            'entryNumber' => 'pi.entry_number AS entryNumber',
            'entryName' => 'pi.entry_name AS entryName',
            'entryType' => 'pi.entry_type AS entryType',
            'entryRole' => 'pi.entry_role AS entryRole',
            'entryClass' => 'pi.entry_class AS entryClass',
            'entryStatus' => 'pi.entry_status AS entryStatus',
            'germplasmDbId' => 'pi.germplasm_id AS germplasmDbId',
            'germplasmCode' => 'g.germplasm_code AS germplasmCode',
            'germplasmState' => 'g.germplasm_state AS germplasmState',
            'germplasmType' => 'g.germplasm_type AS germplasmType',
            'parentage' => 'g.parentage AS parentage',
            'seedDbId' => 'pi.seed_id AS seedDbId',
            'seedCode' => 'seed.seed_code AS seedCode',
            'seedName' => 'seed.seed_name AS seedName',
            'packageDbId' => 'pi.package_id AS packageDbId',
            'packageCode' => 'package.package_code AS packageCode',
            'packageLabel' => 'package.package_label AS packageLabel',
            'plotCode' => 'plot_code AS plotCode',
            'plotNumber' => 'plot_number AS plotNumber',
            'plotType' => 'plot_type AS plotType',
            'rep' => 'rep',
            'designX' => 'design_x AS designX',
            'designY' => 'design_y AS designY',
            'plotOrderNumber' => 'plot_order_number AS plotOrderNumber',
            'paX' => 'pa_x AS paX',
            'paY' => 'pa_y AS paY',
            'fieldX' => 'field_x AS fieldX',
            'fieldY' => 'field_y AS fieldY',
            'blockNumber' => 'block_number AS blockNumber',
            'harvestStatus' => 'harvest_status AS harvestStatus',
            'plotStatus' => 'plot_status AS plotStatus',
            'plotQcCode' => 'plot_qc_code AS plotQcCode',
            'creationTimestamp' => 'pi.creation_timestamp AS creationTimestamp',
            'creatorDbId' => 'creator.id AS creatorDbId',
            'creator' => 'creator.person_name AS creator',
            'modificationTimestamp' => 'plot.modification_timestamp AS modificationTimestamp',
            'modifierDbId' => 'modifier.id AS modifierDbId',
            'modifier' => 'modifier.person_name AS modifier',
        ];

        $fields = '';

        if (preg_match('/locations\/.*\/plots-search/i', $endpoint)) {
            foreach ($attributes as $value) {
                if (!array_key_exists($value, $locationsPlotsSearchAttributes))
                    continue;

                $fields .= "{$locationsPlotsSearchAttributes[$value]} |";
            }
        } else if (preg_match('/occurrences\/.*\/plots-search/i', $endpoint)) {
            foreach ($attributes as $value) {
                if (!array_key_exists($value, $occurrencesPlotsSearchAttributes))
                    continue;

                $fields .= "{$occurrencesPlotsSearchAttributes[$value]} |";
            }
        }

        $fields = rtrim($fields, '|');

        return $fields;
    }

    /**
     * Export/Download data collection files based on selected file name
     * in CSV format
     * 
     * @param string|int $occurrenceDbId unique occurrence identifier
     * @param string $dataCollectionFileName Data Collection file name
     * @param int $plotCount total plot count of an Occurrence
     * 
     * @return Html|string HTML dropdown items
     */
    public function actionExportOtherDataCollectionFiles ($occurrenceDbId, $dataCollectionFileName, $plotCount)
    {
        $occurrenceDbId = $_POST['occurrenceDbId'] ?? $occurrenceDbId;
        $dataCollectionFileName = $_POST['dataCollectionFileName'] ?? $dataCollectionFileName;
        $plotCount = $_POST['plotCount'] ?? $plotCount;
        $program = Yii::$app->session->get('program') ?? 'COLLABORATOR';

        Yii::info('Generating HTML dropdown items...' . json_encode(compact('program', 'occurrenceDbId', 'dataCollectionFileName')), __METHOD__);

        $dataCollectionFileNameAbbrevised = strtoupper(str_replace(' ', '_', $dataCollectionFileName));

        $occurrenceName = $this->api->getApiResults(
            'POST',
            'occurrences-search',
            json_encode([
                'fields' => 'occurrence.id AS occurrenceDbId | occurrence.occurrence_name AS occurrenceName',
                'occurrenceDbId' => "equals $occurrenceDbId",
            ]),
            '&limit=1',
            true
        )['data'][0]['occurrenceName'];

        $fileName = $this->browserController->getFileNameFromConfigByEntityId(
            $occurrenceDbId,
            "{$dataCollectionFileNameAbbrevised}_OCCURRENCE"
        );

        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if ($isAdmin && strtoupper($role) != 'COLLABORATOR') {
            $role = 'ADMIN';
        }

        // If role is COLLABORATOR, get other DC file names config for the role
        // Else, get DC file names per PROGRAM
        $config = $this->api->getApiResults(
            'GET',
            'configurations?abbrev=DC_' . strtoupper((strtoupper($role) === 'COLLABORATOR') ? $role : $program) . '_OTHER_FILE_SETTINGS&limit=1',
            retrieveAll: false
        );

        // If config does not exist, get global config
        if($config['totalCount'] == 0){
            $config = $this->api->getApiResults(
                'GET',
                'configurations?abbrev=DC_GLOBAL_OTHER_FILE_SETTINGS&limit=1',
                retrieveAll: false
            );
        }

        $config = (isset($config['data'][0]['configValue'])) ? $config['data'][0]['configValue']["$dataCollectionFileName"]['Export'] : [];

        $csvAttributes = [];
        $csvHeaders = [];
        $url = "occurrences/$occurrenceDbId/mapping-plots";

        foreach ($config as $column) {
            $attribute = $column['attribute'];

            // Rename certain retrieved config attributes
            if ($attribute === 'experimentSeason') {
                $attribute = 'seasonName';
            } else if ($attribute === 'programCode') {
                $attribute = 'programAbbrev';
            }

            $csvAttributes []= $attribute;
            $csvHeaders []= $column['header'];
        }

        // Get background processing threshold for exportCsvMappingFiles
        $thresholdValue = Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportCsvMappingFiles');

        if ($plotCount >= $thresholdValue) {
            $description = "Preparing $dataCollectionFileName data for downloading";
            $httpMethod = 'GET';

            $this->worker->invoke(
                'BuildCsvData',
                $description,
                'OCCURRENCE',
                $occurrenceDbId,
                'OCCURRENCE',
                'OCCURRENCES',
                'GET',
                compact(
                    'description',
                    'fileName',
                    'url',
                    'httpMethod',
                    'csvAttributes',
                    'csvHeaders'
                ),
                [
                    'remarks' => $fileName,
                ]
            );

            return json_encode([
                'success' => true,
            ]);
        } else {
            $response = $this->api->getApiResults(
                'GET',
                $url,
                retrieveAll: true
            );

            $mappingPlots = $response['data'];

            $csvData = [$csvHeaders];
    
            $i = 1;
    
            if (isset($mappingPlots) && !empty($mappingPlots)) {
                foreach ($mappingPlots as $value) {
                    $csvData[$i] = [];
    
                    foreach ($csvAttributes as $attribute) {
                        array_push($csvData[$i], $value[$attribute] ?? '');
                    }
    
                    $i++;
                }
            }

            // Set headers
            header('Content-Type: application/csv');
            header("Content-Disposition: attachment; filename={$fileName};");
        
            // Write to output stream
            $fp = fopen('php://output', 'w');
    
            if (!$fp) {
                die('Failed to create CSV file.');
            }
    
            foreach ($csvData as $line) {
                fputcsv($fp, $line);
            }
            
            exit;
        }
    }

    /**
     * Retrieve saved trait protocols
     * 
     * @return json array list of retrieved list IDs
     */
    public function actionGetTraitProtocols () {
        $occurrenceIds = Yii::$app->request->post('occurrenceId');
        
        // Retrieve data value
        $method = 'POST';
        $url = "occurrence-data-search";
        $listIds = [];

        foreach ($occurrenceIds as $occurrenceId) {
            $params = [
                "fields" => "occurrence_data.occurrence_id AS occurrenceDbId | variable.abbrev AS variableAbbrev | occurrence_data.data_value AS dataValue",
                "occurrenceDbId" => $occurrenceId,
                "variableAbbrev" => "equals TRAIT_PROTOCOL_LIST_ID"
            ];

            $response = $this->api->getApiResults(
                $method,
                $url,
                json_encode($params),
                '',
                true
            );

            if ($response['success'] && !empty($response['data']) && !empty($response['data'][0]['dataValue'])) {
                $listIds[] = $response['data'][0]['dataValue'];
			}
        }

        // Retrieve list ID 
        $totalCount = 0;
        $hasZeroCount = false;
        $listIdCount = count($listIds);

        foreach ($listIds as $listId) {
            $listMember = $this->listMember->search('', $listId);
            if (isset($listMember->totalCount)) {
                $totalCount += $listMember->totalCount;
                if ($listMember->totalCount === 0) {
                    $hasZeroCount = true;
                }
            }
        }

        return json_encode(['hasZeroCount' => $hasZeroCount, 'listIdCount' => $listIdCount]);
    }
}