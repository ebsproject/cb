<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\inventoryManager\controllers;

use Yii;

// Import interfaces
use app\interfaces\models\IUser;
use app\interfaces\models\IWorker;
use app\interfaces\modules\inventoryManager\models\IListsModel;
// Import models
use app\modules\inventoryManager\models\TemplatesModel;

/**
 * Controller class for template-related operations in Inventory Manager
 */
class TemplatesController extends \yii\web\Controller
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public IUser $user,
        public IWorker $worker,
        public IListsModel $listsModel,
        public TemplatesModel $templatesModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the list selection page for template generation
     * @param String program program code
     * @param String action - (optional) action File upload action type. Supports: create or update.
     */
    public function actionSelectList($program = null, $action = TemplatesModel::ACTION_CREATE, $permissions = '[]') {
        // Get user ID
        $userId = $this->user->getUserId();

        $permissions = json_decode($permissions, true);
        // Retrieve lists browser data
        $params = \Yii::$app->request->getQueryParams();
        $searchModel = $this->listsModel;
        $dataProvider = $this->listsModel->search($params, $action);

        // Template options
        $templateOptions = $this->templatesModel->getTemplateOptions($action);

        // Determine filter options for list type based on action
        $listTypeFilters = [];
        if($action == TemplatesModel::ACTION_CREATE) {
            $listTypeFilters = [
                'germplasm' => 'Germplasm',
                'seed' => 'Seed',
                'package' => 'Package'
            ];
        }
        else if($action == TemplatesModel::ACTION_UPDATE) {
            $listTypeFilters = [
                'seed' => 'Seed',
                'package' => 'Package'
            ];
        }

        // Render view file
        return $this->render('lists', [
            'program' => $program,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'userId' => $userId,
            'action' => $action,
            'templateOptions' => $templateOptions,
            'listTypeFilters' => $listTypeFilters,
            'permissions' => $permissions
        ]);
    }

    /**
     * Generates the template CSV file
     * @param String program - program code
     * @param String type - type of file. Supports: seed-package or package
     * @param Integer listDbId - (optional) list identifier
     * @param String action - (optional) action File upload action type. Supports: create or update.
     */
    public function actionDownloadTemplate($program, $type, $listDbId = null, $action = TemplatesModel::ACTION_CREATE)
    {
        // Get configs
        $config = $this->templatesModel->getConfigurations($program, $type, $action);
        // If no configs found, throw error
        if (empty($config)) {
            $errorMessage = 'No configurations found. '
                . json_encode([
                    'program'=>$program,
                    'type'=>$type,
                    'listDbId'=>$listDbId,
                    'action'=>$action
                ]);
            Yii::error($errorMessage);
            throw new \yii\base\ErrorException(\Yii::t('app', $errorMessage));
        }

        // Extract template headers based on config
        $headers = $this->templatesModel->extractTemplateHeaders($config);

        $data = [[]];
        $fileNamePrefix = "IM_" . strtoupper($action);
        // If list id is available, retrieve data and abbrev
        if(!empty($listDbId)) {
            $data = $this->templatesModel->getListData($listDbId, $type, $headers, $action);
            $fileNamePrefix = $this->listsModel->getListAbbrev($listDbId) . "_" . strtoupper($action);
        }
        
        // Build CSV
        if(!empty($headers)){
            $this->templatesModel->buildCSVFile(
                headers: $headers,
                programCode: $program,
                type: $type,
                data: $data,
                fileNamePrefix: $fileNamePrefix
            );
        }
        else{
            return false;
        }
    }

    /**
     * Controller action for invoking background workers. The file upload ID,
     * worker name, and description must be specified.
     */
    public function actionInvokeBackgroundWorker() {
        // Get germplasm file upload ID
        $germplasmFileUploadDbId = isset($_POST['germplasmFileUploadDbId']) ? $_POST['germplasmFileUploadDbId'] : 0;
        // Get worker name
        $workerName = isset($_POST['workerName']) ? $_POST['workerName'] : '';
        // description
        $description = isset($_POST['description']) ? $_POST['description'] : '';
        // user id
        $userId = $this->user->getUserId();
        $isAdmin = $this->user->isAdmin();

        // Invoke worker
        $result = $this->worker->invoke(
            $workerName,                                    // Worker name
            $description,                                   // Description
            'GERMPLASM_FILE_UPLOAD',                        // Entity
            $germplasmFileUploadDbId,                       // Entity ID
            'GERMPLASM_FILE_UPLOAD',                        // Endpoint Entity
            'INVENTORY_MANAGER',                            // Application
            'POST',                                         // Method
            [                                               // Worker data
                "germplasmFileUploadDbId" => $germplasmFileUploadDbId,
                "userId" => $userId,
                "isAdmin" => $isAdmin
            ]
        );

        return json_encode($result);
    }

}
