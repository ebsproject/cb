<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\inventoryManager\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;

// Import controllers
use app\controllers\Dashboard;

// Import interfaces
use app\interfaces\models\IBackgroundJob;
use app\interfaces\models\ILists;
use app\interfaces\models\IProgram;
use app\interfaces\models\IUser;
use app\interfaces\models\IVariable;
use app\interfaces\models\IWorker;

// Import models
use app\models\Package;
use app\modules\inventoryManager\models\SeedTransferModel;

/**
 * Seed transfer controller for the `inventoryManager` module
 */
class TransferController extends Controller
{
    CONST DEFAULT_PERMISSIONS = [
        'VIEW_CREATE_TAB', 
        'VIEW_UPDATE_TAB', 
        'VIEW_SEED_TRANSFER_TAB', 
        'CREATE_SEED_AND_PACKAGE', 
        'CREATE_PACKAGE', 
        'UPDATE_SEED', 
        'UPDATE_PACKAGE', 
        'TRANSFER_SEED'
    ];

    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public Dashboard $dashboard,
        public IBackgroundJob $backgroundJob,
        public ILists $lists,
        public IProgram $program,
        public IUser $user,
        public IVariable $variable,
        public IWorker $worker,
        public Package $packageModel,
        public SeedTransferModel $seedTransferModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     *
     * @param string $program program code
     */
    public function actionIndex($program=null)
    {
        // Check RBAC if has access
        $hasAccess = Yii::$app->access->renderAccess("VIEW_SEED_TRANSFER_TAB","INVENTORY_MANAGER");
        // Determine if user is ADMIN user
        $isAdmin = Yii::$app->session->get('isAdmin');
        // Check allowed actions to determine tabs to display
        $permissions = self::DEFAULT_PERMISSIONS;
        if (!$isAdmin && Yii::$app->session->get('enableRbac')) {
            $permissions = [];
            $rbacPermissions = Yii::$app->session->get('currentPermissions')['permission'] ?? [];

            foreach ($rbacPermissions as $rbacPermission) {
                $permissions[] = $rbacPermission;
            }
        }
        // If RBAC is disabled and user is not an admin, set permission to UPDATE and SEED_TRANSFER tabs only
        else if (!$isAdmin && !Yii::$app->session->get('enableRbac')) {
            $permissions = [
                'VIEW_UPDATE_TAB',
                'VIEW_SEED_TRANSFER_TAB',
                'UPDATE_PACKAGE', 
                'TRANSFER_SEED'
            ];
        }

        if (!$hasAccess) {
            Yii::$app->session->setFlash('error', "You have no permission to the page you are trying to access. Please contact your data administrator.");
            Yii::$app->response->redirect(Url::base().'/index.php');
            return;
        }

        $dashboardFilters = (array) $this->dashboard->getFilters();
        $programDbId = $dashboardFilters['program_id'];

        // Find the program code if not provided in the URL
        if (empty($program)) {
            // Get program info
            $programInfo = $this->program->getOne($programDbId);
            $programData = $programInfo['data'] ?? [];
            // Use program code as program
            $program = $programData['programCode'] ?? null;
        }

        // Set search parameters and filters
        $params = $this->seedTransferModel->getSearchParams(Yii::$app->request->get(), [$programDbId]);
        $filters = $this->seedTransferModel->getSearchFilters(Yii::$app->request->get());
        $dataProvider = $this->seedTransferModel->search($params, $filters);

        // Pre-load data browser column filters
        $this->seedTransferModel->load(Yii::$app->request->get());

        // Get current user id
        $userDbId = $this->user->getUserId();
        // Get user programs
        $userPrograms = (array) $this->user->getUserPrograms(
            userId: $userDbId,
            ignoreAdmin: true
        );
        // Get program ids array from result
        $userPrograms = array_column($userPrograms, 'id');

        return $this->render('index', [
            'program' => $program,
            'dataProvider' => $dataProvider,
            'searchModel' => $this->seedTransferModel,
            'statusOptions' => $this->seedTransferModel->getStatusOptions(),
            'userDbId' => $userDbId,
            'userPrograms' => $userPrograms,
            'permissions' => $permissions,
        ]);
    }

    /**
     * Renders the seed transfer receive modal
     *
     */
    public function actionRenderReceiveModal() {
        $seedTransferDbId = $_POST['seedTransferDbId'] ?? 0;

        // Render the modal content
        return $this->renderAjax('_receive_seed_transfer', [
            'seedTransferDbId' => $seedTransferDbId
        ]);
    }

    /**
     * Renders the seed transfer view modal
     *
     */
    public function actionRenderViewModal() {
        $seedTransferDbId = $_POST['seedTransferDbId'] ?? 0;

        // Get seed transfer info
        $result = $this->seedTransferModel->getOne($seedTransferDbId);
        $seedTransferInfo = $result['data'] ?? [];
        $seedTransferDetails = [
            "basic" => [],
            "audit" => [],
            "printout" => []
        ];

        // Get source list info
        $sourceListDbId = $seedTransferInfo["sourceListDbId"];
        $result = $this->lists->getOne($sourceListDbId);
        $sourceListInfo = $result['data'] ?? [];
        $sourceListDisplayName = $sourceListInfo['displayName'];

        // Get sender program info
        $senderProgramDbId = $seedTransferInfo["senderProgramDbId"];
        $senderProgramDisplayText = null;
        if(!empty($senderProgramDbId)) {
            $result = $this->program->getOne($senderProgramDbId);
            $senderProgramInfo = $result['data'] ?? [];
            $senderProgramDisplayText = $senderProgramInfo['programName'] . " (" . $senderProgramInfo['programCode'] . ")";
        }

        // Get receiver program info
        $receiverProgramDbId = $seedTransferInfo["receiverProgramDbId"];
        $receiverProgramDisplayText = null;
        $receiverProgramCode = null;
        if(!empty($receiverProgramDbId)) {
            $result = $this->program->getOne($receiverProgramDbId);
            $receiverProgramInfo = $result['data'] ?? [];
            $receiverProgramDisplayText = $receiverProgramInfo['programName'] . " (" . $receiverProgramInfo['programCode'] . ")";
            $receiverProgramCode = $receiverProgramInfo['programCode'];
        }

        // Package quantity
        $isSendWholePackage = $seedTransferInfo["isSendWholePackage"];
        if($isSendWholePackage) {
            $packageQuantityDisplayText = "whole packages";
        }
        else {
            if(!empty($seedTransferInfo["packageQuantity"]) && !empty($seedTransferInfo["packageUnit"])) {
                $packageQuantityDisplayText = $seedTransferInfo["packageQuantity"] . " " . $seedTransferInfo["packageUnit"] . '/package';
            }
            else $packageQuantityDisplayText = '?/package';
            
        }

        // Get other details
        $seedTransferStatus = $seedTransferInfo["seedTransferStatus"];
        $destinationListDbId = $seedTransferInfo["destinationListDbId"];
        $destinationListDisplayName = null;
        if(!empty($destinationListDbId)) {
            $result = $this->lists->getOne($destinationListDbId);
            $destinationListInfo = $result['data'] ?? [];
            $destinationListDbId = $destinationListInfo['listDbId'];
            $destinationListDisplayName = $destinationListInfo['displayName'];
        }

        // Add basic info
        $seedTransferDetails["basic"]["Source Package List"] = $sourceListDisplayName;
        $seedTransferDetails["basic"]["Sender Program"] = $senderProgramDisplayText;
        $seedTransferDetails["basic"]["Receiver Program"] = $receiverProgramDisplayText;
        $seedTransferDetails["basic"]["Package Quantity"] = $packageQuantityDisplayText;
        // Add audit info
        $seedTransferDetails["audit"]["Creator"] = $seedTransferInfo["creator"];
        $seedTransferDetails["audit"]["Creation Timestamp"] = $seedTransferInfo["creatorTimestamp"];
        $seedTransferDetails["audit"]["Sender"] = $seedTransferInfo["sender"];
        $seedTransferDetails["audit"]["Send Timestamp"] = $seedTransferInfo["sendTimestamp"];
        $seedTransferDetails["audit"]["Receiver"] = $seedTransferInfo["receiver"];
        $seedTransferDetails["audit"]["Receive Timestamp"] = $seedTransferInfo["receiveTimestamp"];
        // Add printout info
        $seedTransferDetails["printout"]["receiverProgramCode"] = $receiverProgramCode;
        $seedTransferDetails["printout"]["destinationListDbId"] = $destinationListDbId;
        $seedTransferDetails["printout"]["destinationListName"] = $destinationListDisplayName;

        // Render the modal content
        return $this->renderAjax('_view_seed_transfer', [
            "seedTransferDetails" => $seedTransferDetails,
            "sourceListDbId" => $sourceListDbId,
            "sourceListDisplayName" => $sourceListDisplayName,
            "seedTransferStatus" => $seedTransferStatus,
            "destinationListDbId" => $destinationListDbId,
            "destinationListDisplayName" => $destinationListDisplayName
        ]);
    }

    public function statusDisplay($status) {
        return match ($status) {
            'draft' => '<span title="Draft" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'DRAFT') . '</strong></span>',
            'creation in progress' => '<span title="Ongoing creation of records" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'CREATION IN PROGRESS') . '</strong></span>',
            'creation failed' => '<span title="Creation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'CREATION FAILED') . '</strong></span>',
            'created' => '<span title="Creation completed" class="new badge blue darken-2"><strong>' . \Yii::t('app', 'CREATED') . '</strong></span>',
            'sending in progress' => '<span title="Ongoing sending" class="new badge orange darken-2"><strong>' . \Yii::t('app', 'SENDING IN PROGRESS') . '</strong></span>',
            'sending failed' => '<span title="Sending failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'SENDING FAILED') . '</strong></span>',
            'sent' => '<span title="Sending completed" class="new badge blue darken-2"><strong>' . \Yii::t('app', 'SENT') . '</strong></span>',
            'receipt in progress' => '<span title="Ongoing receipt" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'RECEIPT IN PROGRESS') . '</strong></span>',
            'receipt failed' => '<span title="Receipt failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'RECEIPT FAILED') . '</strong></span>',
            'received' => '<span title="Transfer completed" class="new badge green darken-2"><strong>' . \Yii::t('app', 'RECEIVED') . '</strong></span>',
            default => '<span title="Status unknown" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>',
        };
    }

    /**
     * Renders the seed transfer creation modal
     *
     * @param string $program program code
     */
    public function actionRenderCreationModal($program) {
        $seedTransferDbId = $_POST['seedTransferDbId'] ?? 'null';

        // Retrieve package unit scale values
        $result = $this->variable->getVariableByAbbrev('PACKAGE_UNIT');
        $variableDbId = $result['variableDbId'];
        $result = $this->variable->getVariableScales($variableDbId);
        $scaleValues = $result['scaleValues'] ?? [];
        // Get value-display name pairs
        $packageUnitData = [];
        foreach($scaleValues as $sv) {
            $packageUnitData[$sv['value']] = $sv['displayName'];
        }

        if ($seedTransferDbId != 'null') {
            $seedTransfer = $this->seedTransferModel->getOne($seedTransferDbId);
            Yii::debug('Retrieved Seed Transfer record '.json_encode($seedTransfer));
            $sourceListDbId = $seedTransfer['data']['sourceListDbId'] ?? 0;

            // Get source package list data
            $result = $this->lists->getOne($sourceListDbId);
            $sourceListDisplayText = $result['data']['displayName'] ?? null;

            // Get sender program data
            $result = $this->getSenderProgram($sourceListDbId);
            $senderProgramDbId = $result['senderProgramDbId'];
            $senderDisplayText = $result['senderDisplayText'];

            // Get receiver program data
            $receiverProgramDbId = $seedTransfer['data']['receiverProgramDbId'] ?? 0;
            $result = $this->program->getOne($receiverProgramDbId);
            $receiverProgramDisplayText = $result['data']['programName'] ?? null;

            // Get package data
            $isSendWholePackage = $seedTransfer['data']['isSendWholePackage'];
            if ($isSendWholePackage === true) {
                $isSendWholePackage = 'true';
            } else if ($isSendWholePackage === false) {
                $isSendWholePackage = 'false';
            }
            $packageQuantity = $seedTransfer['data']['packageQuantity'];
            $packageUnit = $seedTransfer['data']['packageUnit'];
            if (!empty($packageUnit)) {
                $packageUnitDisplayText = $packageUnitData[$packageUnit];
            }
        }

        // Render the modal content
        return $this->renderAjax('_create_seed_transfer', [
            'program' => $program,
            "seedTransferDbId" => $seedTransferDbId,
            "packageUnitData" => $packageUnitData,
            'sourceListDbId' => $sourceListDbId ?? null,
            'sourceListDisplayText' => $sourceListDisplayText ?? null,
            'senderProgramDbId' => $senderProgramDbId ?? null,
            'senderDisplayText' => $senderDisplayText ?? null,
            'receiverProgramDbId' => $receiverProgramDbId ?? null,
            'receiverProgramDisplayText' => $receiverProgramDisplayText ?? null,
            'isSendWholePackage' => $isSendWholePackage ?? null,
            'packageQuantity' => $packageQuantity ?? null,
            'packageUnit' => $packageUnit ?? null,
            'packageUnitDisplayText' => $packageUnitDisplayText ?? null,
        ]);
    }

    /**
     * Get select2 options
     * @param string q - search value
     * @param string id - element id
     * @param integer page - pagination
     * @param array filters - search param filter configs
     * @param string paramType - type of param to be loaded (currently supports 'package-list' and 'receiver-program')
     * @param string optional - JSON encoded array containing optional parameters
     */
    public function actionGetFilterData($q = null, $id = null, $page = null, $filters = null, $paramType = null, $optional = '[]'){
        $limit = 5;
        $page = ($page==null ? 1 : $page);
        // Decode optional parameters
        $optional = (array) json_decode($optional);

        // Set default filters
        $filters = "limit=$limit&page=$page";

        // Retrieve package list options.
        // Only retrieve lists created by the current user,
        // or shared to the current user with read-write access
        if($paramType == 'package-list') {
            // Build params
            $params = [
                "type" => "equals package",
                "listUsage" => "equals final list",
                "status" => "equals created",
                "permission" => "equals read_write"
            ];
            $filters = "sort=creationTimestamp:DESC&" . $filters;
            // Add search term if specified
            if(!empty($q)) $params['displayName'] = "%$q%";
            // Retrieve data
            $result = $this->lists->searchAll(
                params: $params,
                filters: $filters
            );
            // Format data
            $listsArray = $result['data'];
            $formatted = [];
            foreach($listsArray as $list) {
                $listDbId = $list['listDbId'];
                $displayName = $list['displayName'];

                $formatted[] = [
                    "id" => $listDbId,
                    "text" => $displayName
                ];
            }

            // Return formatted data
            return json_encode(['totalCount' => $result['totalCount'], 'items' => $formatted]);
        }
        else if($paramType == 'receiver-program') {
            // Get sender program id. Options for receiver program should not include the sender program.
            $programDbId = $optional['senderProgramDbId'] ?? 0;
            // Build params
            $params = [
                "fields" => "program.id AS id|program.program_name AS text",
                "id" => "not equals $programDbId"
            ];
            // Add search term if specified
            if(!empty($q)) $params['text'] = "%$q%";
            // Retrieve data
            $result = $this->program->searchAll(
                params: $params,
                filters: $filters
            );
            // Return formatted data
            return json_encode(['totalCount' => $result['totalCount'], 'items' => $result['data']]);
        }
        else if($paramType == 'facility') {
            // Build params
            $params = [
                "fields" => "facility.id AS id|facility.facility_name AS text"
            ];
            $filters = "sort=text:ASC&" . $filters;
            // Add search term if specified
            if(!empty($q)) $params['text'] = "%$q%";
            // Retrieve data
            $result = Yii::$app->api->getParsedResponse('POST', 'facilities-search', json_encode($params), $filters, false);
            // Return formatted data
            return json_encode(['totalCount' => $result['totalCount'], 'items' => $result['data']]);
        }
        else if($paramType == 'sub-facility') {
            // Get parent facility id.
            $parentFacilityDbId = $optional['facilityDbId'] ?? 0;
            // Build params
            $params = [
                "fields" => "facility.id AS id|facility.facility_name AS text|facility.parent_facility_id AS parentFacilityDbId",
                "parentFacilityDbId" => "equals $parentFacilityDbId"
            ];
            $filters = "sort=text:ASC&" . $filters;
            // Add search term if specified
            if(!empty($q)) $params['text'] = "%$q%";
            // Retrieve data
            $result = Yii::$app->api->getParsedResponse('POST', 'facilities-search', json_encode($params), $filters, false);
            // Return formatted data
            return json_encode(['totalCount' => $result['totalCount'], 'items' => $result['data']]);
        }

        // If paramType is not supported, return empty result
        return json_encode(['totalCount' => 0, 'items' => []]);

    }

    /**
     * Validates a package list and returns validation flags
     */
    public function actionValidatePackageList() {
        $result = [
            'isOneSeedManager' => false,
            'atMostOnePackageUnit' => false,
            'isUserMember' => false,
            'packageUnit' => []
        ];

        $packageListDbId = $_POST['listDbId'] ?? 0;
        $data = $this->lists->searchAllMembers($packageListDbId,
            params: [ "fields" => "package.id AS packageDbId" ],
            retrieveAll: true)['data'] ?? [];

        if (!empty($data)) {
            $packageDbIds = array_column($data, 'packageDbId');
            $data = $this->packageModel->searchAll(params: [
                "fields" => "package.id AS packageDbId | program.id AS programDbId | program.program_code AS seedManager | package.package_unit AS unit",
                "packageDbId" => 'equals '.implode('| equals ', $packageDbIds)
            ], retrieveAll: true)['data'] ?? [];

            // Check if all packages in the list belong to 1 program
            if (sizeof(array_unique(array_column($data, 'seedManager'))) == 1) {
                $result['isOneSeedManager'] = true;
            }

            // Check if all packages in the list use at most one package unit
            if (sizeof(array_unique(array_column($data, 'unit'))) <= 1) {
                $result['atMostOnePackageUnit'] = true;
                $result['packageUnit'] = array_unique(array_column($data, 'unit'));
            }

            // Check if user has access to the program owning the packages of the list
            $userDbId = $this->user->getUserId();
            $userPrograms = $this->user->getUserPrograms($userDbId, ignoreAdmin: true);
            $userProgramDbIds = array_column($userPrograms, 'id');
            $packageProgramDbIds = array_unique(array_column($data, 'programDbId'));
            if (sizeof(array_intersect($userProgramDbIds, $packageProgramDbIds)) == sizeof($packageProgramDbIds)) {
                $result['isUserMember'] = true;
            }
        }

        return json_encode($result);
    }

    /**
     * Creates a seed transfer draft record.
     * Given the source list id, the sender program is determined.
     * NOTE: This assumes that all packages in the package list belong to a single program.
     */
    public function actionCreateSeedTransferDraft() {
        // Get source list id
        $sourceListDbId = $_POST['sourceListDbId'] ?? 0;
        $result = $this->getSenderProgram($sourceListDbId);
        $senderProgramDbId = $result['senderProgramDbId'];
        $senderDisplayText = $result['senderDisplayText'];

        // Create seed transfer record
        $result = $this->seedTransferModel->create(
            requestData: [
                "records" => [
                    [
                        "seedTransferStatus" => "draft",
                        "sourceListDbId" => "$sourceListDbId",
                        "senderProgramDbId" => "$senderProgramDbId"
                    ]
                ]
            ],
            backgroundProcess: false
        );

        // Parse result
        $data = $result['data'] ?? [];
        $seedTransfer = $data[0] ?? [];
        $seedTransferDbId = $seedTransfer['seedTransferDbId'] ?? 0;

        // Return data
        return json_encode([
            "seedTransferDbId" => $seedTransferDbId,
            "sourceListDbId" => $sourceListDbId,
            "senderProgramDbId" => $senderProgramDbId,
            "senderDisplayText" => $senderDisplayText
        ]);
    }

    /**
     * Updates a seed transfer record given the id and the record data.
     * When the source list is updated, the sender program is also updated
     * based on the program owning the packages in the list.
     * NOTE: This assumes that all packages in the package list belong to a single program.
     */
    public function actionUpdateSeedTransferRecord() {
        // Get request data
        $seedTransferDbId = $_POST['seedTransferDbId'] ?? 0;
        $recordData = isset($_POST['recordData']) ? (array) $_POST['recordData'] : [];
        $senderDisplayText = '';

        // Get seed transfer info
        $result = $this->seedTransferModel->getOne($seedTransferDbId);
        $data = $result['data'] ?? [];
        $currentSourceListDbId = $data['sourceListDbId'] ?? 0;

        // If source list was updated, change sender program as well
        if(!empty($recordData['sourceListDbId']) && $recordData['sourceListDbId'] != $currentSourceListDbId) {
            $sourceListDbId = $recordData['sourceListDbId'];
            $result = $this->getSenderProgram($sourceListDbId);
            $recordData['senderProgramDbId'] = $result['senderProgramDbId'];
            $senderDisplayText = $result['senderDisplayText'];
        }

        // Build request body
        $requestBody = [];
        foreach($recordData as $key => $value) {
            if(!empty($value)) $requestBody[$key] = "$value";
        }
        // Update seed transfer record
        $result = $this->seedTransferModel->updateOne($seedTransferDbId, $requestBody);

        // Return updated data
        return json_encode([
            "recordData" => $recordData,
            "senderDisplayText" => $senderDisplayText
        ]);
    }

    /**
     * Retrieves the sender program id and display text given the source list id.
     * NOTE: This assumes that all packages in the package list belong to a single program.
     *
     * @param Integer sourceListDbId package list identifier (platform.list.id
     * @return Array containing the sender program id and display text
     */
    public function getSenderProgram($sourceListDbId) {
        // Get sender program id from a list member
        $result = $this->lists->searchAllMembers(
            dbId: $sourceListDbId,
            params: [
                "fields" => "package.program_id AS programDbId|program.program_name AS programName|program.program_code AS programCode"
            ],
            filters: 'limit=1',
            retrieveAll: false
        );
        // Parse result
        $data = $result['data'] ?? [];
        $firstMember = $data[0] ?? [];
        $senderProgramDbId = $firstMember['programDbId'] ?? 0;
        $senderProgramName = $firstMember['programName'] ?? 0;
        $senderProgramCode = $firstMember['programCode'] ?? 0;
        // Set display name based on sender program name and code
        $senderDisplayText = "$senderProgramName ($senderProgramCode)";

        return [
            "senderProgramDbId" => $senderProgramDbId,
            "senderDisplayText" => $senderDisplayText
        ];
    }

    /**
     * Retrieves the seed transfer information.
     * This action also builds the sender and receiver program
     * text in the format: <program_name> (<program_code>)
     */
    public function actionGetSeedTransferInfo() {
        // Get seed transfer
        $seedTransferDbId = $_POST['seedTransferDbId'] ?? 0;
        $result = $this->seedTransferModel->getOne($seedTransferDbId);
        $seedTransferInfo = $result['data'] ?? [];

        // Get sender program
        $senderProgramDbId = $seedTransferInfo['senderProgramDbId'] ?? 0;
        $result = $this->program->getOne($senderProgramDbId);
        $senderProgramInfo = $result['data'] ?? [];
        $senderProgramText = !empty($senderProgramInfo) ?
            $senderProgramInfo['programName'] . " (" . $senderProgramInfo['programCode'] . ")"
            :
            'Unknown'
        ;

        // Ger receiver program
        $receiverProgramDbId = $seedTransferInfo['receiverProgramDbId'] ?? 0;
        $result = $this->program->getOne($receiverProgramDbId);
        $receiverProgramInfo = $result['data'] ?? [];
        $receiverProgramText = !empty($receiverProgramInfo) ?
            $receiverProgramInfo['programName'] . " (" . $receiverProgramInfo['programCode'] . ")"
            :
            'Unknown'
        ;
        // Get status
        $seedTransferStatus = $seedTransferInfo['seedTransferStatus'];

        return json_encode([
            "seedTransferInfo" => $seedTransferInfo,
            "senderProgramText" => $senderProgramText,
            "receiverProgramText" => $receiverProgramText,
            "seedTransferStatus" => $seedTransferStatus
        ]);
    }

    /**
     * Calls on background worker to process seed transfer transaction.
     */
    public function actionInvokeSeedTransferWorker() {
        Yii::debug(json_encode($_POST), __METHOD__);

        $entityDbId = $_POST['entityDbId'] ?? 0;
        $workerName = $_POST['workerName'] ?? '';
        $description = $_POST['description'] ?? '';
        $processName = $_POST['processName'] ?? '';
        $facilityDbId = isset($_POST['facilityDbId']) && !empty($_POST['facilityDbId']) ? $_POST['facilityDbId'] : null;
        $userDbId = $this->user->getUserId();

        $result = $this->worker->invoke(
            $workerName,
            $description,
            'SEED_TRANSFER',
            $entityDbId,
            'SEED_TRANSFER',
            'INVENTORY_MANAGER',
            'POST',
            [
                'seedTransferDbId' => $entityDbId,
                'processName' => $processName,
                'userDbId' => $userDbId,
                'facilityDbId' => $facilityDbId
            ]
        );

        return json_encode($result);
    }

    /**
     * Facilitates the retrieval of push notifications
     */
    public function actionPushNotifications()
    {
        $workerName = "equals SeedTransferProcessor";
        $userId = $this->user->getUserId();

        $params = [
            "creatorDbId" => "equals $userId",
            "workerName" => $workerName,
            "isSeen" => "equals false"
        ];
        $result = $this->backgroundJob->searchAll($params, "sort=modificationTimestamp:desc");

        $newNotifications = $result["data"];
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->backgroundJob->getGenericNotifications($newNotifications);

        $params = [
            "creatorDbId" => "equals $userId",
            "workerName" => $workerName,
            "isSeen" => "equals true"
        ];
        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);

        $notifications = $result["data"];
        $seenCount = count($notifications);
        $seenNotifications = $this->backgroundJob->getGenericNotifications($notifications);

        // update unseen notifications to seen
        $params = [
            "jobIsSeen" => "true"
        ];
        $result = $this->backgroundJob->update($newNotifications, $params);

        return $this->renderAjax(
            '/default/_notifications',
            [
                "unseenNotifications" => $unseenNotifications,
                "seenNotifications" => $seenNotifications,
                "unseenCount" => $unseenCount,
                "seenCount" => $seenCount,
            ]
        );
    }

    /**
     * Facilitates the retrieval of new notifications
     */
    public function actionNewNotificationsCount() {
        $workerName = "equals SeedTransferProcessor";
        $userId = $this->user->getUserId();

        // Parameters for background jobs search
        $params = [
            "creatorDbId" => "equals $userId",
            "workerName" => $workerName,
            "isSeen" => "equals false"
        ];
        $filter = 'sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter);

        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        }

        return json_encode($result["totalCount"]);
    }

    /**
     * Sends the seed transfer.
     */
    public function actionSendSeedTransfer() {
        // Get seed transfer id
        $seedTransferDbId = $_POST['seedTransferDbId'] ?? 0;

        // Get current user id
        $userDbId = $this->user->getUserId();

        // Build request body
        $requestBody = [
            'seedTransferStatus' => 'sent',
            'senderDbId' => "$userDbId"
        ];

        // Update seed transfer record
        $result = $this->seedTransferModel->updateOne($seedTransferDbId, $requestBody);
    }

    /**
     * Deletes the seed transfer.
     */
    public function actionDeleteSeedTransfer() {
        // Get seed transfer id
        $seedTransferDbId = $_POST['seedTransferDbId'] ?? 0;

        // Delete seed transfer record
        $result = $this->seedTransferModel->deleteOne($seedTransferDbId);
    }
}