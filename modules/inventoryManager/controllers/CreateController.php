<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\inventoryManager\controllers;

use Yii;
use app\components\B4RController;

// Import data providers
use app\dataproviders\ArrayDataProvider;

//Import interfaces
use app\interfaces\models\IGermplasmFileUpload;

/**
 * CreateController for the `inventoryManager` module
 * Contains methods for managing the creation of new seeds and packages
 */
class CreateController extends B4RController
{

    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public IGermplasmFileUpload $germplasmFileUploadModel,
        $config = []
        )
    {
        
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the creation summary in the confirmation modal
     */
    public function actionRenderCreationSummary() {
        $germplasmFileUploadDbId = isset($_POST['germplasmFileUploadDbId']) ? $_POST['germplasmFileUploadDbId'] : 0;
        $action = $_POST['action'] ?? 'create';
        Yii::info('Rendering seed/package creation summary ' . json_encode(['germplasmFileUploadDbId'=>$germplasmFileUploadDbId]), __METHOD__);
        $entities = ['seed', 'package'];

        // Retrieve file upload transaction record
        $requestBody = [
            // Add more fields in case there's an additional entity/needed info
            "fields" => "
                file_upload.id AS germplasmFileUploadDbId|
                file_upload.seed_count AS seedCount|
                file_upload.package_count AS packageCount
            ",
            "germplasmFileUploadDbId" => "equals $germplasmFileUploadDbId",
        ];
        $result = $this->germplasmFileUploadModel->searchAll($requestBody)['data'][0] ?? [];

        $creationSummaryObject = [];
        foreach($entities as $entity) {
            $count = $result[$entity . 'Count'] ?? 0;

            if($count) {
                $creationSummaryObject[] = [
                    'summaryItemId' => 0,
                    'entity' => ucfirst($entity),
                    'count' => $count
                ];
            }
        }

        // Assemble data provider
        $dataProvider = new ArrayDataProvider([
            'allModels' => $creationSummaryObject,
            'key' => 'summaryItemId',
            'restified' => true,
            'totalCount' => count($creationSummaryObject)
        ]);

        // Render summary
        return $this->renderAjax('@app/modules/germplasm/views/create/_creation_summary', [
            "dataProvider" => $dataProvider,
            "action" => $action,
        ]);
    }

}

?>