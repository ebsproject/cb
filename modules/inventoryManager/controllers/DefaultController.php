<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\inventoryManager\controllers;

use app\modules\inventoryManager\models\InventoryFileUpload;
use Yii;
use yii\web\Controller;
use yii\helpers\Url;

// Import controllers
use app\controllers\Dashboard;

// Import interfaces
use app\interfaces\models\IBackgroundJob;
use app\interfaces\models\IProgram;
use app\interfaces\models\IUser;

// Import models
use app\modules\inventoryManager\models\CreateModel;
use app\modules\inventoryManager\models\TemplatesModel;

/**
 * Default controller for the `inventoryManager` module
 */
class DefaultController extends Controller
{
    CONST DEFAULT_PERMISSIONS = [
        'VIEW_CREATE_TAB', 
        'VIEW_UPDATE_TAB', 
        'VIEW_SEED_TRANSFER_TAB', 
        'CREATE_SEED_AND_PACKAGE', 
        'CREATE_PACKAGE', 
        'UPDATE_SEED', 
        'UPDATE_PACKAGE', 
        'TRANSFER_SEED'
    ];

    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public CreateModel $createModel,
        public Dashboard $dashboard,
        public IBackgroundJob $backgroundJob,
        public IProgram $program,
        public IUser $user,
        public InventoryFileUpload $fileUploadModel,
        public TemplatesModel $templatesModel,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     *
     * @param string $program program code
     */
    public function actionIndex($program = null, $tab = 'create')
    {
        // Check RBAC if has access
        $hasAccess = Yii::$app->access->renderAccess('VIEW_'.strtoupper($tab).'_TAB',"INVENTORY_MANAGER");
        // Determine if user is ADMIN user
        $isAdmin = Yii::$app->session->get('isAdmin');

        // Check allowed actions to determine tabs to display
        $permissions = self::DEFAULT_PERMISSIONS;
        if (!$isAdmin && Yii::$app->session->get('enableRbac')) {
            $permissions = [];
            $rbacPermissions = Yii::$app->session->get('currentPermissions')['permission'] ?? [];

            foreach ($rbacPermissions as $rbacPermission) {
                $permissions[] = $rbacPermission;
            }
        }
        // If RBAC is disabled and user is not an admin, set permission to UPDATE and SEED_TRANSFER tabs only
        else if (!$isAdmin && !Yii::$app->session->get('enableRbac')) {
            $permissions = [
                'VIEW_UPDATE_TAB',
                'VIEW_SEED_TRANSFER_TAB',
                'UPDATE_PACKAGE', 
                'TRANSFER_SEED'
            ];
        }

        // If RBAC is disabled, check if user has access to the current tab.
        // Update the hasAccess variable to true if user has access to the tab.
        if(!\Yii::$app->session->get('enableRbac')) {
            $hasAccess = in_array('VIEW_'.strtoupper($tab).'_TAB', $permissions);
        }

        // Check access, and redirect when necessary
        $allowAccess = $this->checkAccess($isAdmin, $permissions, $hasAccess, $tab, $program);
        if(!$allowAccess) {
            Yii::$app->session->setFlash('error', "You have no permission to the page you are trying to access. Please contact your data administrator.");
            Yii::$app->response->redirect(Url::base().'/index.php');
            return;
        }

        $dashboardFilters = (array) $this->dashboard->getFilters();
        $programDbId = $dashboardFilters['program_id'];

        // Find the program code if not provided in the URL
        if(empty($program)) {
            // Get program info
            $programInfo = $this->program->getOne($programDbId);
            $programData = $programInfo['data'] ?? [];
            // Use program code as program
            $program = $programData['programCode'] ?? null;
        }

        // Initialize session variables
        Yii::$app->session->set($this->createModel->getKeyVariables(), null);
        Yii::$app->session->set($this->createModel->getKeyVariablesRequired(), null);
        Yii::$app->session->set($this->createModel->getKeyVariablesOptional(), null);

        $searchModel = $this->fileUploadModel;
        $dataProvider = $this->fileUploadModel->search(null, [
            "fileUploadOrigin" => "equals INVENTORY_MANAGER",  // IM abbrev
            "fileUploadAction" => "equals " . $tab
        ]);

        // Template options
        $action = $tab == 'create' ? TemplatesModel::ACTION_CREATE : TemplatesModel::ACTION_UPDATE;
        $templateOptions = $this->templatesModel->getTemplateOptions($action);
        
        return $this->render('index', [
            'program' => $program,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'action' => $action,
            'templateOptions' => $templateOptions,
            'permissions' => $permissions
        ]);
    }

    /**
     * Renders file upload modal content
     *
     * @param string $program program code
     * @param string $action TemplatesModel::ACTION_CREATE | TemplatesModel::ACTION_UPDATE
     */
    public function actionRenderFileUploadForm($program, $action)
    {
        extract($_POST);

        $htmlData = $this->renderAjax('_file_upload', [
            'program' => $program,
            'action' => $action,
            'uploadType' => $uploadType ?? '',
        ]);

        return json_encode($htmlData);
    }

    /**
     * Reads and performs initial validation of uploaded file using file upload form
     *
     * @param string $uploadType CreateModel::SEED_PACKAGE | CreateModel::PACKAGE | CreateModel::SEED
     * @param string $action TemplatesModel::ACTION_CREATE | TemplatesModel::ACTION_UPDATE
     */
    public function actionProcessFileUpload($uploadType, $action)
    {
        $dashboardFilters = (array) $this->dashboard->getFilters();
        $programDbId = $dashboardFilters['program_id'];

        // Set variables to session
        // If admin, find admin-specific config
        $isAdmin = $this->user->isAdmin();
        $configVars = null;
        if ($isAdmin) {
            $configVars = $this->createModel->getVariableConfig($programDbId, type: $uploadType, action: $action, admin: $isAdmin);
        }
        // If admin config does not exist, find other config available
        if(empty($configVars)) {
            $configVars = $this->createModel->getVariableConfig($programDbId, type: $uploadType, action: $action);
            $sysDefVars = $this->createModel->getVariableConfig($programDbId, systemDef: true, type: $uploadType, action: $action);
            $configVars = array_merge($configVars, $sysDefVars);
        }
        Yii::$app->session->set($this->createModel->getKeyVariables(), $configVars);

        $fileReference = 'inventory_file_upload';

        try {
            // Retrieve uploaded file
            $uploadedFileInfo = $_FILES[$fileReference];

            if (isset($uploadedFileInfo) && !empty($uploadedFileInfo)) {
                $variableConfig = Yii::$app->session->get($this->createModel->getKeyVariables());

                // Retrieve file data
                $uploadedFile = $this->createModel->retrieveUploadedFile($fileReference);

                if ($uploadedFile['success']) {
                    // Get file upload row count threshold
                    $uploadLimit = Yii::$app->config->getAppThreshold(
                        'INVENTORY_MANAGER',
                        'fileUploadRowCount'
                    );
                    // If row count exceed the limit, show error message.
                    if(count($uploadedFile['fileContent']) > $uploadLimit) {
                        $uploadLimitFormat = number_format($uploadLimit);
                        $result = [
                            'success' => false,
                            'error' => "The file exceeds the maximum number of rows allowed. ($uploadLimitFormat)"
                        ];
                    }
                    else {
                        // Validate file headers
                        $result = $this->createModel->validateUploadedFile($programDbId, $uploadedFile['headers'],
                        $variableConfig);
                    }

                    if ($result['success']) {
                        // Create new file upload record
                        $result = $this->createModel->saveUploadedFile($programDbId, $uploadedFile['fileName'],
                            $uploadedFile['fileContent'], origin: 'INVENTORY_MANAGER', remarks: $uploadType, action: $action);
                    }

                    return json_encode($result);
                } else {
                    $success = false;
                    $error = $uploadedFile['error'];
                }
            }
        } catch (\Exception $e) {
            $success = false;
            $error = 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.';
        }

        return json_encode([
            'success' => $success ?? true,
            'message' => $message ?? '',
            'error' => $error ?? '',
        ]);
    }

    /**
     * Facilitates the retrieval of push notifications
     */
    public function actionPushNotifications(){
        
        $workerName = "
            equals ValidateInventoryFileUpload|
            equals CreateSeedAndPackageRecords";

        $userId = $this->user->getUserId();

        $params = [
            "creatorDbId" => "equals $userId",
            "workerName" => $workerName,
            "isSeen" => "equals false"
        ];
        $result = $this->backgroundJob->searchAll($params, "sort=modificationTimestamp:desc");
        

        $newNotifications = $result["data"];
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->backgroundJob->getGenericNotifications($newNotifications);

        $params = [
            "creatorDbId" => "equals $userId",
            "workerName" => $workerName,
            "isSeen" => "equals true"
        ];
        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);

        $notifications = $result["data"];
        $seenCount = count($notifications);
        $seenNotifications = $this->backgroundJob->getGenericNotifications($notifications);

        // update unseen notifications to seen
        $params = [
            "jobIsSeen" => "true"
        ];
        $result = $this->backgroundJob->update($newNotifications, $params);

        return $this->renderAjax(
            '_notifications',
            [
                "unseenNotifications" => $unseenNotifications,
                "seenNotifications" => $seenNotifications,
                "unseenCount" => $unseenCount,
                "seenCount" => $seenCount,
            ]
        );
    }

    /**
     * Facilitates the retrieval of new notifications
     */
    public function actionNewNotificationsCount(){
        // Get worker name
        $workerName = "
            equals ValidateInventoryFileUpload|
            equals CreateSeedAndPackageRecords
        ";
        // Get user id
        $userId = $this->user->getUserId();

        // Parameters for background jobs search
        $params = [
            "creatorDbId" => "equals $userId",
            "workerName" => $workerName,
            "isSeen" => "equals false"
        ];
        $filter = 'sort=modificationTimestamp:desc';   
        $result = $this->backgroundJob->searchAll($params, $filter);

        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        }

        return json_encode($result["totalCount"]);
    }

    /**
     * Deletes a file upload record
     */
    public function actionDeleteFileUploadRecord()
    {
        $germplasmFileUploadDbId = $_POST['germplasmFileUploadDbId'] ?? 0;
        $result = $this->fileUploadModel->deleteOne($germplasmFileUploadDbId);
        Yii::debug('Deleted germplasm file upload '.json_encode($result), __METHOD__);
    }
    
    /**
     * Checks access permissions and redirects when necessary
     * @param bool $isAdmin if user is an admin
     * @param array $permissions user's access permissions
     * @param bool $hasAccess if user has access to the page
     * @param string $tab current tab
     * @param string $program program code
     */
    public function checkAccess($isAdmin, $permissions, $hasAccess, $tab, $program) {
        // If not an admin, check access permissions
        if (!$isAdmin) {
            // If seed transfer is the only allowed tab, redirect to seed transfer
            if (in_array('VIEW_SEED_TRANSFER_TAB',$permissions)
                && !in_array('VIEW_CREATE_TAB',$permissions)
                && !in_array('VIEW_UPDATE_TAB',$permissions)
            ) {
                // Redirect to the seed transfer
                return $this->redirect(
                    \yii\helpers\Url::base() . 'inventoryManager/transfer?program=' . $program,
                );
            }
            // If create tab is not accessible, redirect to an accessible tab
            else if (!$hasAccess && $tab == 'create') {
                if (in_array('VIEW_UPDATE_TAB', $permissions)) {
                    return $this->redirect(
                        \yii\helpers\Url::base() . 'inventoryManager?program=' . $program . '&tab=update',
                    );
                }
                else if (in_array('VIEW_SEED_TRANSFER_TAB', $permissions)) {
                    return $this->redirect(
                        \yii\helpers\Url::base() . 'inventoryManager/transfer?program=' . $program,
                    );
                }
            }
            // If update tab is not accessible, redirect to an accessible tab
            else if (!$hasAccess && $tab == 'update') {
                if (in_array('VIEW_CREATE_TAB', $permissions)) {
                    return $this->redirect(
                        \yii\helpers\Url::base() . 'inventoryManager?program=' . $program . '&tab=create',
                    );
                }
                else if (in_array('VIEW_SEED_TRANSFER_TAB', $permissions)) {
                    return $this->redirect(
                        \yii\helpers\Url::base() . 'inventoryManager/transfer?program=' . $program,
                    );
                }
            }
            // If the user does not have access to any actions, show error message
            if (!$hasAccess) {
                return false;
            }
        }
        return true;
    }
}
