<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders information for the selected seed transfer 
 */

use yii\helpers\Html;
use app\components\PrintoutsWidget2;

// Add link to display destination list if status is RECEIVED
$destinationListLink = '';
if($seedTransferStatus == 'received') {
    $destinationListLink = Html::a('<strong>View received packages</strong>',
        '#',
        [

            'title' => \Yii::t('app', 'View List'),
            'class' => 'btn btn-primary waves-effect waves-light modal-close teal',
            'id'=>'im-view-list-btn',
            'data-toggle' => 'modal',
            'data-target' => '#view-list-modal',
            'data-id' => $destinationListDbId,
            'data-display_name' => $destinationListDisplayName,
            'style' => null
        ]
    )
    . "<span align='left'>" . PrintoutsWidget2::widget([
        "product" => "Inventory manager",
        "program" => $seedTransferDetails['printout']['receiverProgramCode'],
        "entityName" => 'list',
        "entityDisplayName" => $seedTransferDetails['printout']['destinationListName'],
        "entityDbIds" => [ $seedTransferDetails['printout']['destinationListDbId'] ],
        "buttonStyle" => [
            "margin-left" => "10px"
        ]
    ]) . "</span>"
    ;
}

// Status display
$seedTransferDisplay = match ($seedTransferStatus) {
    'draft' => '<span title="Draft" class="new badge grey darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'DRAFT') . '</strong></span>',
    'creation in progress' => '<span title="Ongoing creation of records" class="new badge yellow darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'CREATION IN PROGRESS') . '</strong></span>',
    'creation failed' => '<span title="Creation failed" class="new badge red darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'CREATION FAILED') . '</strong></span>',
    'created' => '<span title="Creation completed" class="new badge blue darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'CREATED') . '</strong></span>',
    'sending in progress' => '<span title="Ongoing sending" class="new badge orange darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'SENDING IN PROGRESS') . '</strong></span>',
    'sending failed' => '<span title="Sending failed" class="new badge red darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'SENDING FAILED') . '</strong></span>',
    'sent' => '<span title="Sending completed" class="new badge blue darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'SENT') . '</strong></span>',
    'receipt in progress' => '<span title="Ongoing receipt" class="new badge yellow darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'RECEIPT IN PROGRESS') . '</strong></span>',
    'receipt failed' => '<span title="Receipt failed" class="new badge red darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'RECEIPT FAILED') . '</strong></span>',
    'received' => '<span title="Transfer completed" class="new badge green darken-2" style="margin-top: 0px;"><strong>' . \Yii::t('app', 'RECEIVED') . '</strong></span>',
    default => '<span title="Status unknown" class="new badge grey darken-2"><strong style="margin-top: 0px;">' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>',
};

?>

<!-- Basic information -->
<ul class="collapsible popout collapsible-accordion col col-md-12" data-collapsible="accordion" style="margin-top:-10px">
    <?php
    $col1Str = '';
    $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';
    foreach ($seedTransferDetails['basic'] as $k => $value) {
        if($k == "Source Package List") {
            $value = Html::a($value . ' ',
                '#',
                [

                    'title' => \Yii::t('app', 'View List'),
                    'id'=>'im-view-list-btn',
                    'data-toggle' => 'modal',
                    'data-target' => '#view-list-modal',
                    'data-id' => $sourceListDbId,
                    'data-display_name' => $sourceListDisplayName,
                    'style' => null
                ]
            );
        }
        $value = empty($value) ? $notSet : $value;

        $col1Str = $col1Str.'<dt title="'.$k.'" style="width: 170px !important;">'.$k.'</dt><dd style="margin-left: 190px !important;">'.$value.'</dd>';
    }
    ?>
    <li class="active">
        <div class="collapsible-header active"><?= \Yii::t('app', 'Basic information') ?></div>
        <div class="collapsible-body" style="display: block;margin-top:0px">
            <div class="viewer-status for-small-screen" style="text-align: center; margin-bottom: 10px;">
                <?= $seedTransferDisplay ?>
            </div>
            <dl class="dl-horizontal">
                <?= $col1Str ?>
            </dl>
            <div align="center">
                <?= $destinationListLink ?>
            </div>
        </div>
    </li>
</ul>

<!-- Audit information -->
<ul class="collapsible popout collapsible-accordion col col-md-12" data-collapsible="accordion" style="margin-top:-10px">
    <?php
    $col2Str = '';
    $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';
    foreach ($seedTransferDetails['audit'] as $k => $value) {
        $value = ($value == '') ? $notSet : $value;

        $col2Str = $col2Str.'<dt title="'.$k.'" style="width: 170px !important;">'.$k.'</dt><dd style="margin-left: 190px !important;">'.$value.'</dd>';
    }
    ?>
    <li class="active">
        <div class="collapsible-header active"><?= \Yii::t('app', 'Audit information') ?></div>
        <div class="collapsible-body" style="display: block;margin-top:0px">
            <dl class="dl-horizontal">
                <?= $col2Str ?>
            </dl>
        </div>
    </li>
</ul>