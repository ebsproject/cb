<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders information for receiving the seed transfer
 */

use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;

// Define URLs
$getSeedTransferInfoUrl = Url::to(['/inventoryManager/transfer/get-seed-transfer-info']);
$dataUrl = Url::to(['transfer/get-filter-data']);  // Generate search parameter select2


$pluginOptions = [
    'allowClear' => false,
    'minimumInputLength' => 0,
    'placeholder' => 'Search value',
    'language' => [
        'errorLoading' => new JSExpression("function(){ return 'Unable to fetch data at the moment.'; }"),
    ],
    'ajax' => [
        'url' => $dataUrl,
        'dataType' => 'json',
        'delay'=> 250,
        'data'=> new JSExpression(
            'function (params) {

                var id = $(this).attr("id");
                var paramType = $(this).data("param-type");
                var optional = {
                    facilityDbId: $(this).attr("data-facility-id")
                };

                return {
                    q: params.term, // search term
                    id: id,
                    page: params.page,
                    paramType: paramType,
                    optional: JSON.stringify(optional)
                }
            }

            '
        ),
        'processResults' => new JSExpression(
            'function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                    more: (params.page * 5) < data.totalCount
                    },
                };
            }'
        ),
        'cache' => true
    ],
    'templateResult' => new JSExpression(
        'function formatRepo (repo) {
            if (repo.loading) {
                return repo.text;
            }

            return repo.text;
        }'
    ),
    'templateSelection' => new JSExpression(
        'function formatRepoSelection (repo) {
            return repo.text;
        }'
    ),
];

// Facility select2
$facilitySelect2 = Select2::widget([
    'name' => 'im-create-st-facility-select',
    'id' => 'im-create-st-facility-select',
    'class' => 'im-create-st-input',
    'data' => [],
    'options' => [
        'placeholder' => 'Select facility',
        'data-param-type' => 'facility'
    ],
    'pluginOptions' => $pluginOptions,
]);

// SubFacility select2
$subFacilitySelect2 = Select2::widget([
    'name' => 'im-create-st-sub-facility-select',
    'id' => 'im-create-st-sub-facility-select',
    'class' => 'im-create-st-input',
    'data' => [],
    'options' => [
        'placeholder' => 'Select sub-facility',
        'data-param-type' => 'sub-facility',
        'data-facility-id' => []
    ],
    'pluginOptions' => $pluginOptions,
]);

?>

<div id='receive-message'></div>
<br>

<!-- Modal body -->
<div id="im-creat-st-default-input-div">
    <!-- Facility -->
    <div class="im-creat-st-default-input-sub-div" id = "im-create-st-facility-div">
        <label title="Facility">Facility <i>(optional)</i></label>
        <?= $facilitySelect2 ?>
        <div id = "im-create-st-facility-error-div" class="hidden"></div>
        <br>
    </div>

    <!-- Sub Facility -->
    <div class="im-creat-st-default-input-sub-div hidden" id = "im-create-st-sub-facility-div">
        <label title="Sub Facility">Sub-facility <i>(optional)</i></label>
        <?= $subFacilitySelect2 ?>
        <div id = "im-create-st-sub-facility-error-div" class="hidden"></div>
        <br>
    </div>
</div>

<?php
$this->registerJs(<<<JS

    $(document).ready(function() {
        $.ajax({
            url: '$getSeedTransferInfoUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                seedTransferDbId: $seedTransferDbId
            },
            success: function(response) {
                var parsedResponse = JSON.parse(response);
                // Get program names
                var senderProgramText = parsedResponse.senderProgramText;
                var receiverProgramText = parsedResponse.receiverProgramText;
                // Build message
                var message = "You are about to receive packages from <strong>" + senderProgramText + "</strong>."
                    + " After this operation, the packages will be available for use in <strong>" + receiverProgramText + "</strong>."
                    + "<br>"
                    + "Do you wish to receive?";
                // Add modal contents to the modal body
                $('#receive-message').html(message);
            },
            error: function(jqXHR, exception) {
                $('#im-create-transfer-modal-body').html('There was a problem while loading record information.');
            }
        });
    });

    // Facility select event
    $(document).off('change').on('change', "#im-create-st-facility-select", function() {
        // Get value
        let value = $(this).val();

        // Display sub facility select
        $('#im-create-st-sub-facility-select').attr('data-facility-id', value);
        $("#im-create-st-sub-facility-div").removeClass('hidden');
    });

JS);
?>