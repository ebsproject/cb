<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders Inventory Manager > Seed Transfer page
 */

use kartik\date\DatePicker;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

?>

<!-- Google Fonts style sheet -->
<!-- This is needed to properly render the buttons for SEND and RECEIVE -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />

<?php

echo Yii::$app->controller->renderPartial('@app/modules/inventoryManager/views/default/tabs.php', [
    'controller' => 'search',
    'program' => $program,
    'action' => 'transfer',
    'permissions' => $permissions
]);

// Define URLs
$resetUrl = Url::to(['/inventoryManager/transfer', 'program'=>$program]);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$renderCreationModalUrl = Url::to(['/inventoryManager/transfer/render-creation-modal', 'program'=>$program]);
$renderReceiveModalUrl = Url::to(['/inventoryManager/transfer/render-receive-modal']);
$renderViewModalUrl = Url::to(['/inventoryManager/transfer/render-view-modal']);
$getSeedTransferInfoUrl = Url::to(['/inventoryManager/transfer/get-seed-transfer-info']);
$invokeWorkerUrl = Url::to(['/inventoryManager/transfer/invoke-seed-transfer-worker']);
$notificationsUrl = Url::to(['/inventoryManager/transfer/push-notifications']);
$newNotificationsUrl = Url::toRoute(['/inventoryManager/transfer/new-notifications-count']);
$sendSeedTransferUrl = Url::to(['/inventoryManager/transfer/send-seed-transfer']);
$deleteSeedTransferUrl = Url::to(['/inventoryManager/transfer/delete-seed-transfer']);
$viewListUrl = Yii::$app->getUrlManager()->createUrl(['account/list/view', 'program' => $program]);

// Define constants

// Data browser ID
$browserId = 'dynagrid-im-seed-transfer';

// Disable buttons if the user does not have permission to transfer seeds
$disableCreateButton = in_array('TRANSFER_SEED', $permissions) ? '' : 'disabled';
$createButtonToolTip = in_array('TRANSFER_SEED', $permissions) ? Yii::t('app','Create a new internal seed transfer') : Yii::t('app','You do not have permission to create a new internal seed transfer');
$hideActionButtons = in_array('TRANSFER_SEED', $permissions) ? '' : 'hidden';

// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

$createButton = '<a 
    class="light-green darken-3 btn tooltipped ' . $disableCreateButton . ' " 
    style="margin-right:5px;"
    id="im-create-transfer-btn"
    data-position="top" data-delay="50" 
    data-tooltip="'.$createButtonToolTip.'">
    <strong>'.Yii::t('app','Create').'</strong>
</a>';

// Define constants
CONST VALID_EDIT_STATUS = ['draft'];
CONST VALID_VIEW_TRANSACTION_STATUS = ['creation in progress','created','sending in progress','sent','receipt in progress','received'];
CONST VALID_DELETE_STATUS_CREATOR = ['draft','creation failed'];
CONST VALID_DELETE_STATUS_SENDER = ['created','sending failed'];
CONST VALID_DELETE_STATUS_RECEIVER = ['sent','receipt failed'];
CONST VALID_SEND_STATUS = ['created'];
CONST VALID_RECEIVE_STATUS = ['sent'];

// Notifications
$notifButton = Html::a(
    '<i class="material-icons large">notifications_none</i><span id="notif-badge-id" class=""></span>',
    '#',
    [
        'id' => 'im-notification-btn',
        'class' => 'btn waves-effect waves-light im-notification-btn im-tooltipped',
        'title' => 'Notifications',
        'data-pjax' => 0,
        'style' => "overflow: visible !important;",
        "data-position" => "top",
        "data-tooltip" => Yii::t('app', 'Notifications'),
        "data-activates" => "notifications-dropdown",
        'disabled' => false,
    ]
);

// Alert notif display
echo "<div id='im-bg-alert-div'></div>";

// Columns in data browser
$defaultColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order' => kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ],
    [
       'class'=>'kartik\grid\ActionColumn',
       'template' => ' {send} {receive} {view} {edit} {delete} ',
       'buttons' => [
            'send' => function ($url, $model) use ($userPrograms, $hideActionButtons) {
                $seedTransferStatus = $model['seedTransferStatus'];
                // Determinte if action is allowed.
                // The SEND action should only be allowed for members of the sender program.
                $actionAllowed = false;
                $senderProgramDbId = $model['senderProgramDbId'];
                if(in_array($senderProgramDbId, $userPrograms)) $actionAllowed = true;

                if(in_array($seedTransferStatus, VALID_SEND_STATUS) && $actionAllowed) {
                    return Html::a('<span class="material-symbols-outlined">unarchive</span>',
                        '#',
                        [
                            'class' => 'im-seed-transfer-action-btn ' . $hideActionButtons,
                            'id' => 'im-seed-transfer-send-btn',
                            'title' => Yii::t('app', 'Send seed transfer'),
                            'data-id' => $model['seedTransferDbId']
                        ]
                    );
                }
            },
            'receive' => function ($url, $model) use ($userPrograms, $hideActionButtons) {
                $seedTransferStatus = $model['seedTransferStatus'];
                // Determinte if action is allowed.
                // The RECEIVE action should only be allowed for members of the receiver program.
                $actionAllowed = false;
                $receiverProgramDbId = $model['receiverProgramDbId'];
                if(in_array($receiverProgramDbId, $userPrograms)) $actionAllowed = true;

                if(in_array($seedTransferStatus, VALID_RECEIVE_STATUS) && $actionAllowed) {
                    return Html::a('<span class="material-symbols-outlined">archive</span>',
                        '#',
                        [
                            'class' => 'im-seed-transfer-action-btn ' . $hideActionButtons,
                            'id' => 'im-seed-transfer-receive-btn',
                            'title' => Yii::t('app', 'Receive seed transfer'),
                            'data-id' => $model['seedTransferDbId'],
                        ]
                    );
                }
            },
            'view' => function ($url, $model) use ($hideActionButtons) {
                $seedTransferStatus = $model['seedTransferStatus'];
                if(in_array($seedTransferStatus, VALID_VIEW_TRANSACTION_STATUS)) {
                    return Html::a('<i class="material-icons">visibility</i>',
                        '#',
                        [
                            'class' => 'im-seed-transfer-action-btn ' . $hideActionButtons,
                            'id' => 'im-seed-transfer-view-btn',
                            'title' => Yii::t('app', 'View seed transfer'),
                            'data-id' => $model['seedTransferDbId'],
                            'data-toggle' => 'modal',
                            'data-target' => '#im-view-transfer-modal',
                        ]
                    );
                }
            },
            'edit' => function ($url, $model) use ($userDbId, $hideActionButtons) {
                // Only show this to the creator of the seed transfer 
                // Status should be DRAFT
                $creatorDbId = $model['creatorDbId'];
                $seedTransferStatus = $model['seedTransferStatus'];

                if(in_array($seedTransferStatus, VALID_EDIT_STATUS) && $creatorDbId == $userDbId) {
                    return Html::a('<i class="material-icons">edit</i>',
                        '#',
                        [
                            'class' => 'im-seed-transfer-action-btn ' . $hideActionButtons,
                            'id' => 'im-seed-transfer-edit-btn',
                            'title' => Yii::t('app', 'Edit seed transfer'),
                            'data-id' => $model['seedTransferDbId'],
                        ]
                    );
                }
            },
            'delete' => function ($url, $model) use ($userDbId, $userPrograms, $hideActionButtons) {
                // Status should be included in array
                $seedTransferStatus = $model['seedTransferStatus'];

                // Determine if action is allowed for creator.
                // If the user id is equal to the creator id, DELETE action is allowed.
                $creatorDbId = $model['creatorDbId'];
                $actionAllowedCreator = false;
                if($creatorDbId == $userDbId) $actionAllowedCreator = true;

                // Determine if action is allowed for sender program.
                // If the user has access to the sender program, DELETE action is allowed.
                $senderProgramDbId = $model['senderProgramDbId'];
                $actionAllowedSender = false;
                if(in_array($senderProgramDbId, $userPrograms)) $actionAllowedSender = true;

                // Determine if action is allowed for receiver program
                // If the user has access to the receiver program, DELETE action is allowed.
                $receiverProgramDbId = $model['receiverProgramDbId'];
                $actionAllowedReceiver = false;
                if(in_array($receiverProgramDbId, $userPrograms)) $actionAllowedReceiver = true;

                if((in_array($seedTransferStatus, VALID_DELETE_STATUS_CREATOR) && $actionAllowedCreator)
                    || (in_array($seedTransferStatus, VALID_DELETE_STATUS_SENDER) && $actionAllowedSender)
                    || (in_array($seedTransferStatus, VALID_DELETE_STATUS_RECEIVER) && $actionAllowedReceiver)
                ) {
                    return Html::a('<i class="material-icons">delete</i>',
                        '#',
                        [
                            'class' => 'im-seed-transfer-action-btn ' . $hideActionButtons,
                            'id' => 'im-seed-transfer-delete-btn',
                            'title' => Yii::t('app', 'Delete seed transfer'),
                            'data-id' => $model['seedTransferDbId']
                        ]
                    );
                }
            },
        ],
       'vAlign' => 'top',
       'header' => false,
       'noWrap' => true,
       'visible' => true
    ],
    [
        'label' => 'Status',
        'attribute' => 'seedTransferStatus',
        'contentOptions' => [
            'class' => 'im-status-col'
        ],
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $statusOptions,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'multiple' => false,
                'allowClear' => true
            ],
        ],
        'filterInputOptions' => [
            'placeholder' => 'Select status'
        ],
        'value' => function($model) {
            return !empty($model['seedTransferStatus']) ? $model['seedTransferStatus'] : 'unknown';
        },
        'content' => function($model) {
            $status = $model['seedTransferStatus'];
            return match ($status) {
                'draft' => '<span title="Draft" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'DRAFT') . '</strong></span>',
                'creation in progress' => '<span title="Ongoing creation of records" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'CREATION IN PROGRESS') . '</strong></span>',
                'creation failed' => '<span title="Creation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'CREATION FAILED') . '</strong></span>',
                'created' => '<span title="Creation completed" class="new badge blue darken-2"><strong>' . \Yii::t('app', 'CREATED') . '</strong></span>',
                'sending in progress' => '<span title="Ongoing sending" class="new badge orange darken-2"><strong>' . \Yii::t('app', 'SENDING IN PROGRESS') . '</strong></span>',
                'sending failed' => '<span title="Sending failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'SENDING FAILED') . '</strong></span>',
                'sent' => '<span title="Sending completed" class="new badge blue darken-2"><strong>' . \Yii::t('app', 'SENT') . '</strong></span>',
                'receipt in progress' => '<span title="Ongoing receipt" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'RECEIPT IN PROGRESS') . '</strong></span>',
                'receipt failed' => '<span title="Receipt failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'RECEIPT FAILED') . '</strong></span>',
                'received' => '<span title="Transfer completed" class="new badge green darken-2"><strong>' . \Yii::t('app', 'RECEIVED') . '</strong></span>',
                default => '<span title="Status unknown" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>',
            };
        },
        'headerOptions'=> [
            'style' => 'text-align:left'
        ],
        'visible' => true
    ],
    [
        'label' => 'Package List Display Name',
        'attribute' => 'sourceListDisplayName',
        'contentOptions' => [
            'class' => 'im-list-name-col'
        ],
        'content' => function($model) {
            return !empty($model['sourceListDisplayName']) ? $model['sourceListDisplayName'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => true
    ],
    [
        'label' => 'Package Count',
        'attribute' => 'sourceListCount',
        'contentOptions'=> [
            'class' => 'im-package-count-col'
        ],
        'content' => function($model) {
            return !empty($model['sourceListCount']) ? number_format($model['sourceListCount']) : '<span class="not-set">(not set)</span>';
        },
        'visible' => true
    ],
    [
        'label' => 'Sender Program',
        'attribute' => 'senderProgramCode',
        'contentOptions'=> [
            'class' => 'im-sender-program-col'
        ],
        'content' => function($model) {
            return !empty($model['senderProgramCode']) ? $model['senderProgramCode'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => true
    ],
    [
        'label' => 'Sender',
        'attribute' => 'sender',
        'contentOptions' => [
            'class' => 'im-sender-col'
        ],
        'content' => function($model) {
            return !empty($model['sender']) ? $model['sender'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => true
    ],
    [
        'label' => 'Send Timestamp',
        'attribute' => 'sendTimestamp',
        'contentOptions' => [
            'class' => 'im-send-ts-col'
        ],
        'content' => function($model) {
            return !empty($model['sendTimestamp']) ? $model['sendTimestamp'] : '<span class="not-set">(not set)</span>';
        },
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterInputOptions' => [
            'autocomplete' => 'off'
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'startDate'=> date("1970-01-01"),
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
        'visible' => false
    ],
    [
        'label' => 'Receiver Program',
        'attribute' => 'receiverProgramCode',
        'contentOptions'=> [
            'class' => 'im-receiver-program-col'
        ],
        'content' => function($model) {
            return !empty($model['receiverProgramCode']) ? $model['receiverProgramCode'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => true
    ],
    [
        'label' => 'Receiver',
        'attribute' => 'receiver',
        'contentOptions' => [
            'class' => 'im-receiver-col'
        ],
        'content' => function($model) {
            return !empty($model['receiver']) ? $model['receiver'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => true
    ],
    [
        'label' => 'Receive Timestamp',
        'attribute' => 'receiveTimestamp',
        'contentOptions' => [
            'class' => 'im-receive-ts-col'
        ],
        'content' => function($model) {
            return !empty($model['receiveTimestamp']) ? $model['receiveTimestamp'] : '<span class="not-set">(not set)</span>';
        },
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterInputOptions' => [
            'autocomplete' => 'off'
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'startDate'=> date("1970-01-01"),
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
        'visible' => false
    ],
    [
        'label' => 'Creator',
        'attribute' => 'creator',
        'contentOptions' => [
            'class' => 'im-creator-col'
        ],
        'content' => function($model) {
            return !empty($model['creator']) ? $model['creator'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'label' => 'Creation Timestamp',
        'attribute' => 'creationTimestamp',
        'contentOptions' => [
            'class' => 'im-creation-ts-col'
        ],
        'content' => function($model) {
            return !empty($model['creationTimestamp']) ? $model['creationTimestamp'] : '<span class="not-set">(not set)</span>';
        },
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterInputOptions' => [
            'autocomplete' => 'off'
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => true,
                'todayHighlight' => true,
                'showButtonPanel' => true,
                'clearBtn' => true,
                'startDate'=> date("1970-01-01"),
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton' => true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],
        'visible' => false
    ],
];

// DynaGrid configuration
DynaGrid::begin([
    'columns' => $defaultColumns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'export' => [
            'showConfirmAlert' => false,
        ],
        'exportConfig' => [],
        'id' => $browserId.'-table-con',
        'tableOptions' => [
            'class' => $browserId.'-table'
        ],
        'options' => [
            'id' => $browserId.'-table-con'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [ 'id'=> $browserId ],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => [
            'before' => '<div>'.\Yii::t('app', 'This is the browser for all seed transfer transactions.').'</div>',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' => $createButton . $notifButton .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        $resetUrl,
                        [
                            'id' => 'reset-'.$browserId,
                            'class' => 'btn btn-default',
                            'title' => Yii::t('app','Reset grid'),
                            'data-position' => 'top',
                            'data-tooltip' => Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    ) . '{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'resizableColumns' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options'=> [
        'id' => $browserId,
    ]
]);
DynaGrid::end();

// Modal for creating a seed transfer
$createTransferModalTitle = Yii::t('app', 'Internal Seed Transfer: Create');
Modal::begin([
    'id' => 'im-create-transfer-modal',
    'header' => '<h4>'.$createTransferModalTitle.'</h4>',
    'footer' =>
        Html::a(Yii::t('app', 'Cancel'), '#', ['id'=>'im-create-transfer-modal-cancel-btn']) .  '&emsp;' .
        Html::a(Yii::t('app', 'Confirm'), '#', ['id'=>'im-create-transfer-modal-confirm-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']) . '&emsp;',
    'size' => 'modal-md',
    'options' => [
        'data-backdrop' => 'static'
    ],
    'closeButton' => [
        'class' => 'hidden'
    ],
]);
echo '<p>'.Yii::t('app','"<span class="required">*</span>" indicates required fields.').' Changes made are <strong>automatically saved</strong>.</p>';
echo '<div id="im-create-transfer-modal-body"></div>';
echo '<div id="im-create-transfer-modal-loading-indicator"></div>';
Modal::end();

// Modal for confirmation
Modal::begin([
    'id' => 'im-create-transfer-confirm-modal',
    'header' => '<h4><div id="im-st-confirm-modal-header">Confirmation</div></h4>',
    'footer' =>
        '<span class="im-ctm-confirm-cancel-btn-span" id="im-ctm-confirm-cancel-span">' . Html::a(Yii::t('app', 'Cancel'), '#', ['id'=>'im-create-transfer-modal-confirm-cancel-btn', 'data-dismiss'=> 'modal']) .  '&emsp; </span>' .
        '<span class="im-ctm-confirm-cancel-btn-span" id="im-ctm-confirm-back-span">' . Html::a(Yii::t('app', 'Back to creation'), '#', ['id'=>'im-create-transfer-modal-confirm-back-btn', 'data-dismiss'=> 'modal']) .  '&emsp; </span>' .
        '<span class="im-ctm-confirm-action-btn-span" id="im-ctm-confirm-exit-span">' . Html::a(Yii::t('app', 'Exit'), '#', ['id'=>'im-create-transfer-modal-confirm-exit-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']) . '&emsp; </span>' .
        '<span class="im-ctm-confirm-action-btn-span" id="im-ctm-confirm-send-span">' . Html::a(Yii::t('app', 'Send'), '#', ['id'=>'im-create-transfer-modal-confirm-send-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']) . '&emsp; </span>' .
        '<span class="im-ctm-confirm-action-btn-span" id="im-ctm-confirm-receive-span">' . Html::a(Yii::t('app', 'Receive'), '#', ['id'=>'im-create-transfer-modal-confirm-receive-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']) . '&emsp; </span>' .
        '<span class="im-ctm-confirm-action-btn-span" id="im-ctm-confirm-delete-span">' . Html::a(Yii::t('app', 'Confirm'), '#', ['id'=>'im-create-transfer-modal-confirm-delete-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']) . '&emsp; </span>'.
        '<span class="im-ctm-confirm-action-btn-span" id="im-ctm-confirm-proceed-span">' . Html::a(Yii::t('app', 'Proceed'), '#', ['id'=>'im-create-transfer-modal-confirm-proceed-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']) . '&emsp; </span>',
    'size' => 'modal-md',
    'options' => [
        'data-backdrop' => 'static'
    ],
    'closeButton' => [
        'class' => 'hidden'
    ],
]);
echo '<div id="im-create-transfer-confirm-modal-body"></div>';
Modal::end();

// Modal for viewing seed transfer information
$viewTransferModalTitle = Yii::t('app', 'Seed Transfer Details');
Modal::begin([
    'id' => 'im-view-transfer-modal',
    'header' => '<h4>'.$viewTransferModalTitle.'</h4>',
    'footer' =>
        Html::a(Yii::t('app', 'Close'), '#', ['id'=>'im-view-transfer-modal-close-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal', 'data-dismiss'=> 'modal']) . '&emsp;',
    'size' => 'modal-md',
    'options' => [
        'data-backdrop' => 'static',
        'style' => 'min-hight:550px;'
    ],
    'closeButton' => [
        'class' => 'hidden'
    ],
]);
echo '<div id="im-view-transfer-modal-body"></div>';
Modal::end();

// List view modal - close button
$closeButton = '<div class="row">' .
        \macgyer\yii2materializecss\widgets\Button::widget([
            'label' => 'Close',
            'options' => [
                'class' => 'btn pull-right',
                'data-dismiss' => 'modal'

            ],
        ])

        . '</div>';

?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<!-- View List Modal -->
<div id="view-list-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4><em class="material-icons">format_list_numbered</em> View List <span id="listmgt-view-name" style="color: green;font-weight: bold;font-size:x-large;"></span></h4>
            </div>
            <div class="modal-body" id="view-list-body">
            </div>
            <div class="modal-footer">

                <?php 
                    echo $closeButton;
                ?>

            </div>
        </div>
    </div>
</div>

<?php
// Variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    var notificationsUrl = '$notificationsUrl';
    var newNotificationsUrl = '$newNotificationsUrl';
    var currentSeedTransferDbId = null;
    
    // Check for new notifications every 7.5 seconds
    notificationInterval=setInterval(updateNotif, 7500);

    $(document).ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        $("#view-list-body").css('min-height','550px');
        
        notifDropdown();
        renderNotifications();
        updateNotif();
        
        refresh();
    });

    // Reload grid on pjax success
    $(document).on('ready pjax:success', function(e) {
        e.preventDefault();
        e.stopPropagation();
        
        notifDropdown();
        $('#notif-container').html(
            '<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>'
        );
        renderNotifications();
        updateNotif();
        
        refresh();
    });
    
    // Create seed transfer action
    $(document).on('click', '#im-create-transfer-btn', function() {
        // Add loading indicator
        $('#im-create-transfer-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#im-create-transfer-modal').modal('show');

        // Render modal contents
        $.ajax({
            url: '$renderCreationModalUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {},
            success: function(response) {
                // Add modal contents to the modal body
                $('#im-create-transfer-modal-body').html(response);
            },
            error: function(jqXHR, exception) {
                $('#im-create-transfer-modal-body').html('There was a problem while loading record information.');
            }
        });
    });

    // Send browser action button click event
    $(document).on('click', '#im-seed-transfer-send-btn', function() {
        // Modify header text
        $('#im-st-confirm-modal-header').html("Send Seed Transfer")
        // Add loading indicator
        $('#im-create-transfer-confirm-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#im-create-transfer-confirm-modal').modal('show');
        // Show send button
        $('.im-ctm-confirm-action-btn-span').addClass("hidden");
        $('.im-ctm-confirm-cancel-btn-span').addClass("hidden");
        $('#im-ctm-confirm-send-span').removeClass("hidden");
        $('#im-ctm-confirm-cancel-span').removeClass("hidden");

        // Get current seed transfer id
        currentSeedTransferDbId = $(this).data('id');

        $.ajax({
            url: '$getSeedTransferInfoUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                seedTransferDbId: currentSeedTransferDbId
            },
            success: function(response) {
                var parsedResponse = JSON.parse(response);
                // Get program names
                var senderProgramText = parsedResponse.senderProgramText;
                var receiverProgramText = parsedResponse.receiverProgramText;
                // Build message
                var message = "You are about to send this transaction on behalf of <strong>" + senderProgramText + "</strong>."
                    + "<br>"
                    + "Do you wish to proceed?";
                // Add modal contents to the modal body
                $('#im-create-transfer-confirm-modal-body').html(message);
            },
            error: function(jqXHR, exception) {
                $('#im-create-transfer-modal-body').html('There was a problem while loading record information.');
            }
        });
    });

    // Send button click event
    $(document).on('click', '#im-create-transfer-modal-confirm-send-btn', function() {
        $.ajax({
            url: '$sendSeedTransferUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                seedTransferDbId: currentSeedTransferDbId
            },
            success: function(response) {
                $('#im-create-transfer-confirm-modal').modal('hide');

                var notif = "<i class='material-icons green-text'>check</i>&nbsp;The seed transfer was successfully sent.";
                Materialize.toast(notif, 3500);

                reloadGrid();
            },
            error: function(jqXHR, exception) {
                $('#im-create-transfer-confirm-modal').modal('hide');
                var notif = "<i class='material-icons red-text'>close</i>&nbsp;An error occurred. The seed transfer was not sent.";
                Materialize.toast(notif, 3500);
            }
        });
    });

    // Receive browser action button click event
    $(document).on('click', '#im-seed-transfer-receive-btn', function() { 
        // Modify header text
        $('#im-st-confirm-modal-header').html("Receive Seed Transfer")
        // Add loading indicator
        $('#im-create-transfer-confirm-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#im-create-transfer-confirm-modal').modal('show');
        // Show receive button
        $('.im-ctm-confirm-action-btn-span').addClass("hidden");
        $('.im-ctm-confirm-cancel-btn-span').addClass("hidden");
        $('#im-ctm-confirm-receive-span').removeClass("hidden");
        $('#im-ctm-confirm-cancel-span').removeClass("hidden");

        // Get current seed transfer id
        currentSeedTransferDbId = $(this).data('id');

        $.ajax({
            url: '$renderReceiveModalUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                seedTransferDbId: currentSeedTransferDbId
            },
            success: function(response) {
                // Add modal contents to the modal body
                $('#im-create-transfer-confirm-modal-body').html(response);
            },
            error: function(jqXHR, exception) {
                $('#im-create-transfer-modal-body').html('There was a problem while loading record information.');
            }
        });
    });

    // View button click event
    $(document).on('click', '#im-seed-transfer-view-btn', function() {
        // Add loading indicator
        $('#im-view-transfer-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#im-view-transfer-modal').modal('show');
        // Get seed transfer id
        var transferId = $(this).data('id');

        // Render modal contents
        $.ajax({
            url: '$renderViewModalUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                seedTransferDbId: transferId
            },
            success: function(response) {
                // Add modal contents to the modal body
                $('#im-view-transfer-modal-body').html(response);
            },
            error: function(jqXHR, exception) {
                $('#im-view-transfer-modal-body').html('There was a problem while loading record information.');
            }
        });
    });

    // Receive button click event
    $(document).on('click', '#im-create-transfer-modal-confirm-receive-btn', function() {
        let modalId = 'im-create-transfer-confirm-modal';
        let modalBody = 'im-create-transfer-confirm-modal-body';
        let facilityDbId = null;
        if($('#im-create-st-facility-select').val()) facilityDbId = $('#im-create-st-facility-select').val();
        if($('#im-create-st-sub-facility-select').val()) facilityDbId = $('#im-create-st-sub-facility-select').val();

        invokeBackgroundWorker(
            currentSeedTransferDbId,
            'SeedTransferProcessor',
            'Receive seed transfer ID: ' + currentSeedTransferDbId,
            'receive-seed-transfer',
            modalId,
            modalBody,
            facilityDbId
        )
    });

    // Delete browser action button click event
    $(document).on('click', '#im-seed-transfer-delete-btn', function() {
        // Modify header text
        $('#im-st-confirm-modal-header').html("Delete Seed Transfer")
        // Add loading indicator
        $('#im-create-transfer-confirm-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#im-create-transfer-confirm-modal').modal('show');
        // Show delete button
        $('.im-ctm-confirm-action-btn-span').addClass("hidden");
        $('.im-ctm-confirm-cancel-btn-span').addClass("hidden");
        $('#im-ctm-confirm-delete-span').removeClass("hidden");
        $('#im-ctm-confirm-cancel-span').removeClass("hidden");

        // Get current seed transfer id
        currentSeedTransferDbId = $(this).data('id');

        // Build message
        var message = "This will delete the seed transfer and its contents. Click confirm to proceed.";

        // Add modal contents to the modal body
        $('#im-create-transfer-confirm-modal-body').html(message);
    });

    // Delete button click event
    $(document).on('click', '#im-create-transfer-modal-confirm-delete-btn', function() {
        $.ajax({
            url: '$deleteSeedTransferUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                seedTransferDbId: currentSeedTransferDbId
            },
            success: function(response) {
                $('#im-create-transfer-confirm-modal').modal('hide');

                var notif = "<i class='material-icons green-text'>check</i>&nbsp;The seed transfer was successfully deleted.";
                Materialize.toast(notif, 3500);

                reloadGrid();
            },
            error: function(jqXHR, exception) {
                $('#im-create-transfer-confirm-modal').modal('hide');
                var notif = "<i class='material-icons red-text'>close</i>&nbsp;An error occurred. The seed transfer was not deleted.";
                Materialize.toast(notif, 3500);
            }
        });
    });

    // Implement logic for viewing list members
    $('#view-list-modal').on('show.bs.modal', function(event){
        $("#view-list-body").html('<div class="progress"><div class="indeterminate"></div></div>');
        var id = $(event.relatedTarget).data("id");
        var displayName = $(event.relatedTarget).data("display_name");
        $('#listmgt-view-name').html(displayName);
        var viewUrl = "$viewListUrl" +'&id='+id;
        setTimeout(function(){
            $.ajax({
                type: 'GET',
                url: viewUrl,
                async: false,
                success: function(data) {
                    $("#view-list-body").html(data);
                }
            });	
        }, 500);
    });
    
    // Edit seed transfer action
    $(document).on('click', '#im-seed-transfer-edit-btn', function() {
        currentSeedTransferDbId = $(this).data('id');
        
        // Add loading indicator
        $('#im-create-transfer-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#im-create-transfer-modal').modal('show');

        // Render modal contents
        $.ajax({
            url: '$renderCreationModalUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                seedTransferDbId: currentSeedTransferDbId
            },
            success: function(response) {
                // Add modal contents to the modal body
                $('#im-create-transfer-modal-body').html(response);
            },
            error: function(jqXHR, exception) {
                $('#im-create-transfer-modal-body').html('There was a problem while loading record information.');
            }
        });
    });
    
    /**
    * Generic function for invoking a background worker
    * 
    * @param {int} entityDbId - seed transfer identifier
    * @param {string} workerName - name of the background worker to invoke
    * @param {string} description - describes what the worker does
    * @param {string} processName - create-seed-transfer-record
    * @param {string|null} [modalId=null] - modal element ID to be closed
    * @param {string|null} [modalBody=null] - modal body element ID to be used for displaying error messages
    * @param {int} facilityDbId - facility identifier
    */
    function invokeBackgroundWorker(entityDbId, workerName, description, processName, modalId=null, modalBody=null, facilityDbId=null) {
        if (modalBody != null) {
            $("#" + modalBody).html('<div class="progress"><div class="indeterminate"></div></div>');
        }

        // Invoke background worker
        $.ajax({
            url: '$invokeWorkerUrl',
            type: 'post',
            dataType: 'json',
            data: {
                entityDbId: entityDbId,
                workerName: workerName,
                description: description,
                processName: processName,
                facilityDbId: facilityDbId
            },
            success: function(response) {
                var success = response.success;
                var status = response.status;
                
                // Display flash message
                if (success) {
                    // Success message
                    displayCustomFlashMessage(
                        'info', 
                        'fa fa-info-circle',
                        'The inventory records are being created in the background. You may proceed with other tasks. You will be notified in this page once done.',
                        'im-bg-alert-div',
                        10
                    );
                } else {
                    // Failure message
                    let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status !== 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if (status == null) message += ' (#002)';

                    displayCustomFlashMessage(
                        'warning', 
                        'fa-fw fa fa-warning', 
                        message,
                        'im-bg-alert-div'
                    );
                }

                if (modalId != null) $("#" + modalId).modal('hide');
                if (modalBody != null) $("#" + modalBody).html('');
                
                reloadGrid();
            },
            error: function() {
                var errorMessage = '<i>There was a problem loading the content.</i>';
                if (modalBody != null) $("#" + modalBody).html(errorMessage);
            }
        });
    }
    
    /**
     * Displays custom flash message
     * 
     * @param {string} status - the status of the message (info, success, warning, error)
     * @param {string} iconClass - icon of flash message
     * @param {string} message - the message to display
     * @param {string} parentDivId - id of the div where the flash message is to be inserted
     * @param {number} [time=7.5] - time in seconds before the flash message is removed from the display. 7.5 seconds by default.
     */
    function displayCustomFlashMessage(status, iconClass, message, parentDivId, time=7.5) {
        // Add flash message to parent div
        $('#' + parentDivId).html(
            '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
                '<div class="card-panel">' +
                    '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                    message +
                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                '</div>' +
            '</div>'
        );

        // Remove flash message after the specified time in seconds
        setTimeout(function() {
            $('#w25-' + status + '-0').fadeOut(250);
            setTimeout(function() {
                $('#' + parentDivId).html('');
            }, 500);
        }, 1000 * time);
    }
    
    /**
     * Updates notifications
     */
    function updateNotif() {
        $.getJSON(newNotificationsUrl, 
            {},
            function(json){
                $('#notif-badge-id').html(json);

                if(parseInt(json)>0){
                    $('#notif-badge-id').addClass('notification-badge red accent-2');
                }else{
                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                }
        })
        .fail( function(jqXHR, textStatus, errorThrown) {
            console.log("Message: "+textStatus+" : "+errorThrown);
        });
    }

    /**
     * Initialize dropdown materialize for notifications
     */
    function renderNotifications() {
        $('#im-notification-btn').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            belowOrigin: true,
            alignment: 'right',
            stopPropagation: true
        });
    }
    
    /**
     * Renders the notification in dropdown UI
     */
    function notifDropdown() {
        $('#im-notification-btn').on('click',function(e){
            $.ajax({
                url: notificationsUrl,
                type: 'post',
                data: {},
                async:false,
                success: function(data) {

                    var pieces = data.split('||');
                    var height = pieces[1];

                    $('<style>#notifications-dropdown.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');

                    data = pieces[0];

                    renderNotifications();

                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                    $('#notifications-dropdown').html(data);

                    $('.bg_notif-btn').on('click', function(e){
                        var notifHTML = $(this).html();
                        reloadGrid()
                    });
                },
                error: function(xhr, status, error) {
                    var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');

                    Materialize.toast(toastContent, 'x', 'red');
                    $('.toast-error').collapsible();
                }
            });
        });
    }
    
    /**
    * Resize grid table upon loading 
    */
    function refresh() {
        var maxHeight = ($(window).height() - 370);
        $(".kv-grid-wrapper").css("height", maxHeight);
        $('.dropdown-trigger').dropdown();
    }
    
    /**
    * Reload data browser
    */
    function reloadGrid() {
        $.pjax.reload({
            container: `#${browserId}-pjax`,
            url: '$resetUrl'
        });
    }
JS);

?>

<style>
    #dynagrid-im-seed-transfer .kv-grid-wrapper {
        min-height: 500px;
    }

    h6 {
        font-size: 1rem;
        line-height: 110%;
        margin: 0.5rem 0 0.4rem 0;
    }

    body {
        overflow-x: hidden;
    }

    .material-symbols-outlined {
        font-variation-settings:
        'FILL' 0,
        'wght' 400,
        'GRAD' 0,
        'opsz' 48
    }
    
    .notification-badge {
        font-family: "Poppins", sans-serif;
        position: relative;
        right: 0px;
        top: -20px;
        color: #ffffff;
        background-color: #3f51b5;
        margin: 0 -.8em;
        border-radius: 50%;
        padding: 2px 5px;
    }

    #notifications-dropdown h5 {
        font-size: 1rem;
        text-transform: capitalize;
        font-weight: 500;
    }

    #notifications-dropdown li {
        padding: 8px 16px;
        font-size: 1rem;
    }

    #notifications-dropdown li > a {
        padding: 0;
        font-size: 1.1rem;
        font-weight: 300;
    }

    #notifications-dropdown li > a > span {
        display: inline-block;
        font-size: 1.2rem;
        position: relative;
        top: 4px;
        margin-right: 5px;
    }

    #notifications-dropdown li > time {
        font-size: 0.8rem;
        font-weight: 400;
        margin-left: 38px;
    }

    #notifications-dropdown li.divider {
        padding: 0;
    }

    .small {
        font-size: 1.0rem !important;
    }

    .icon-bg-circle {
        color: #fff;
        padding: .4rem;
        border-radius: 50%;
    }

    .round {
        display: inline-block;
        height: 30px;
        width: 30px;
        line-height: 30px;
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #222;
        color: #FFF;
        text-align: center;
    }

    .viewer-status .badge.new {
        line-height: 2rem;
        font-size: 1rem;
        height: 2rem;
        letter-spacing: 0.02rem;
        font-weight: 700;
    }

    .modal.fade {
        background: rgba(0, 0, 0, 0.5);
    }

    .modal-backdrop.fade {
        opacity: 0;
    }
    
</style>