<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders contents of Inventory Manager > Seed Transfer > Create modal
 */

use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;

// Define URLs
$resetUrl = Url::to(['/inventoryManager/transfer', 'program'=>$program]);
$dataUrl = Url::to(['transfer/get-filter-data']);  // Generate search parameter select2
$createSeedTransferDraftUrl = Url::to(['transfer/create-seed-transfer-draft']);
$updateSeedTransferRecordUrl = Url::to(['transfer/update-seed-transfer-record']);
$invokeWorkerUrl = Url::to(['/inventoryManager/transfer/invoke-seed-transfer-worker']);
$validatePackageListUrl = Url::to(['/inventoryManager/transfer/validate-package-list']);

// Data browser ID
$browserId = 'dynagrid-im-seed-transfer';

// Set select2 plugin options
// Copied from Harvest Manager:
// app\modules\harvestManager\models\SearchParametersModel.php::generateHTMLElements(...)
$pluginOptions = [
    'allowClear' => false,
    'minimumInputLength' => 0,
    'placeholder' => 'Search value',
    'language' => [
        'errorLoading' => new JSExpression("function(){ return 'Unable to fetch data at the moment.'; }"),
    ],
    'ajax' => [
        'url' => $dataUrl,
        'dataType' => 'json',
        'delay'=> 250,
        'data'=> new JSExpression(
            'function (params) {
    
                var filters = $(".im-create-st-form").serializeArray();
                var id = $(this).attr("id");
                var paramType = $(this).data("param-type");
                var optional = {
                    senderProgramDbId: $(this).attr("data-sender-id")
                };
    
                return {
                    q: params.term, // search term
                    id: id,
                    page: params.page,
                    filters: JSON.stringify(filters),
                    paramType: paramType,
                    optional: JSON.stringify(optional)
                }
            }
    
            '
        ),
        'processResults' => new JSExpression(
            'function (data, params) {
                params.page = params.page || 1;
    
                return {
                    results: data.items,
                    pagination: {
                    more: (params.page * 5) < data.totalCount
                    },
                };
            }'
        ),
        'cache' => true
    ],
    'templateResult' => new JSExpression(
        'function formatRepo (repo) {
            if (repo.loading) {
                return repo.text;
            }

            return repo.text;
        }'
    ),
    'templateSelection' => new JSExpression(
        'function formatRepoSelection (repo) {
            return repo.text;
        }'
    ),
];

// Package list select2
$packageListSelect2 = Select2::widget([
    'name' => 'im-create-st-package-list-select',
    'id' => 'im-create-st-package-list-select',
    'class' => 'im-create-st-input',
    'data' => [],
    'value' => $sourceListDisplayText,  // Should be DB ID but for some reason doesn't work well with plugin
    'options' => [
        'placeholder' => 'Select package list',
        'data-param-type' => 'package-list'
    ],
    'pluginOptions' => $pluginOptions,
]);

// Receiver program select2
$receiverProgramSelect2 = Select2::widget([
    'name' => 'im-create-st-receiver-program-select',
    'id' => 'im-create-st-receiver-program-select',
    'class' => 'im-create-st-input',
    'data' => [],
    'value' => $receiverProgramDisplayText,
    'options' => [
        'placeholder' => 'Select receiver program',
        'data-param-type' => 'receiver-program',
        'data-sender-id' => []
    ],
    'pluginOptions' => $pluginOptions
]);

// Send whole package radio input
$sendWholePackageRadio = 
'<input
    type="radio"
    id="im-create-st-send-whole-pkg-qty-radio-yes"
    class="im-create-st-input im-create-st-send-whole-pkg-qty-radio-input with-gap"
    title="Transfer the whole package quantity"
    name="whole-pkg-decision"
    data-send-whole="true"
    style="opacity:0"
/>'.
'<label for="im-create-st-send-whole-pkg-qty-radio-yes">'.\Yii::t('app','Yes').'</label>&emsp;&nbsp;'.
'<input
    type="radio"
    id="im-create-st-send-whole-pkg-qty-radio-no"
    class="im-create-st-input im-create-st-send-whole-pkg-qty-radio-input with-gap"
    title="Transfer a portion of the package quantity"
    name="whole-pkg-decision"
    data-send-whole="false"
    style="opacity:0"
/>'.
'<label for="im-create-st-send-whole-pkg-qty-radio-no">'.\Yii::t('app','No').'</label>&emsp;&nbsp;';

$packageQuantityInput =
'<input
    id="im-create-st-package-quantity-input" 
    class="im-create-st-input"
    min="0.001"
    max="999999999.999"
    step="0.001"
    placeholder="Input package quantity"
    onkeypress="return event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)" 
    type="number">
';

// Package list select2
$packageUnitSelect2 = Select2::widget([
    'name' => 'im-create-st-package-unit-select',
    'id' => 'im-create-st-package-unit-select',
    'class' => 'im-create-st-input',
    'data' => $packageUnitData,
    'value' =>  $packageUnit,
    'options' => [
        'placeholder' => 'Select package unit'
    ],
    'pluginOptions' => [
        'allowClear' => false
    ],
]);

$form = ActiveForm::begin([
    'action' => ['find'],
    'enableClientValidation' => false,
    'id' => 'im-create-st-form',
    'options' => [
        'class' => 'im-create-st-form'
    ],
]);


?>

<!-- Modal body -->
<div id="im-creat-st-default-input-div">
    <!-- Package List -->
    <div class="im-creat-st-default-input-sub-div" id = "im-create-st-package-list-div">
        <label title="Package List (required)">Package List <span class="required">*</span></label>
        <?= $packageListSelect2 ?>
        <div id = "im-create-st-package-list-error-div" class="hidden"></div>
    </div>

    <!-- Sender Program -->
    <div class="im-creat-st-default-input-sub-div im-create-st-hidden-div" id = "im-create-st-sender-program-div">
        <label title="Sender Program">Sender Program&nbsp;<i class="material-icons info-icon" title="Determined by the program that owns the packages in the selected package list.">info_outline</i></label>
        <br>
        <span id="im-create-st-sender-program-span" class="new badge green darken-2" title="Program"></span>
    </div>

    <!-- Receiver Program -->
    <div class="im-creat-st-default-input-sub-div im-create-st-hidden-div" id = "im-create-st-receiver-program-div">
        <label title="Receiver Program (required)">Receiver Program <span class="required">*</span></label>
        <?= $receiverProgramSelect2 ?>
        <div id="im-create-st-receiver-program-error-div" class="hidden"></div>
    </div>

    <!-- Send whole package qty -->
    <div class="im-creat-st-default-input-sub-div im-create-st-hidden-div" id = "im-create-st-send-whole-pkg-qty-div">
        <label title="Send the whole package quantity">Send the whole package quantity <span class="required">*</span></label>
        <br>
        <?= $sendWholePackageRadio ?>
        <div id="im-create-st-send-whole-pkg-qty-error-div" class="hidden"></div>
    </div>

    <!-- Package qty and unit -->
    <div class="im-creat-st-default-input-sub-div im-create-st-hidden-div" id = "im-create-st-pkg-qty-unit-div">
        <div class="col-md-6">
            <label title="Package quantity">Package Quantity <span class="required">*</span></label>
            <br>
            <?= $packageQuantityInput ?>
        </div>
        <div class="col-md-6">
            <label title="Package unit">Package Unit <span class="required">*</span></label>
            <br>
            <?= $packageUnitSelect2 ?>
        </div>
        <div id="im-create-st-pkg-qty-unit-error-div" class="hidden"></div>
    </div>
        
</div>

<?php

ActiveForm::end();

$this->registerJs(<<<JS
    var currentSeedTransferDbId = $seedTransferDbId;
    var sendWhole = () => {
        let sendWhole = '$isSendWholePackage';
        if (sendWhole == 'true') {
            return true;
        } else if (sendWhole == 'false') {
            return false;
        } else
            return null;
    }
    var data = {
        sourceListDbId: '$sourceListDbId',
        senderProgramDbId: '$senderProgramDbId',
        receiverProgramDbId: '$receiverProgramDbId',
        isSendWholePackage: sendWhole(),
        packageQuantity: '$packageQuantity',
        packageUnit: '$packageUnit'
    };
    
    // Variables for validation
    var isValid = true;
    
    // Toast notification variables
    var notif = '';
    var notifSuccess = "<i class='material-icons green-text'>check</i>&nbsp;";
    var notifInfo = "<i class='material-icons blue-text'>info</i>&nbsp;";
    var notifWarning = "<i class='material-icons orange-text left'>warning</i>&nbsp;";
    var notifError = "<i class='material-icons red-text left'>close</i>&nbsp;";
    
    $(document).ready(function() {
        flashTimeout();

        $(".im-create-st-hidden-div").hide();
        $("#im-create-transfer-modal-confirm-btn").addClass('disabled');
        
        // Display seed transfer fields depending on current data available
        if (isNotEmpty(data.sourceListDbId)) {
            
            // Sender program
            $("#im-create-st-sender-program-span").html('<strong>' + '$senderDisplayText' + '</strong>');
            $("#im-create-st-sender-program-div").show();
            
            // Receiver program
            $('#im-create-st-receiver-program-select').attr('data-sender-id', data.senderProgramDbId);
            $("#im-create-st-receiver-program-div").show();
            
            // Send whole package quantity
            if (data.isSendWholePackage === true) {
                $('#im-create-st-send-whole-pkg-qty-radio-yes').prop('checked', true);
            } else if (data.isSendWholePackage === false) {
                $('#im-create-st-send-whole-pkg-qty-radio-no').prop('checked', true);
            }
            if (isNotEmpty(data.receiverProgramDbId)) {
                $("#im-create-st-send-whole-pkg-qty-div").show();
            }
            
            // Package quantity and unit
            if (isNotEmpty(data.packageQuantity)) {
                $('#im-create-st-package-quantity-input').prop('value', data.packageQuantity);
            }
            if (data.isSendWholePackage === false) {
                $("#im-create-st-pkg-qty-unit-div").show();
            }
            
            toggleConfirmButton();
        }

        // Package list select event
        $(document).off('change').on('change', "#im-create-st-package-list-select", function() {
            // Add loading indicator
            $('#im-create-transfer-modal-loading-indicator').html('<p><em>Checking packages...</em></p><div class="progress"><div class="indeterminate"></div></div>');

            // Get value
            let value = $(this).val();

            // Wait 1 second before valiation
            setTimeout(function (){
                // Validate package list
                $.ajax({
                    url: '$validatePackageListUrl',
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    data: {
                        listDbId: value
                    },
                    success: function(response) {
                        // Check validation results for the selected package list
                        let isValid = true;
                        let validation = response;

                        if (validation.isUserMember == false) {
                            isValid = false;
                            notif = notifError + "You do not have access to a program that manages a package in this list."
                        }
                        if (validation.isOneSeedManager == false) {
                            isValid = false;
                            notif = notifWarning + "The selected package list contains items from 2 or more programs."              
                        }
                        if (validation.atMostOnePackageUnit == false) {
                            isValid = false;
                            notif = notifWarning + "The selected package list contains items with 2 or more package units."                
                        }
                        if (!isValid) {
                            $(this).parent().addClass('has-error');
                            $('.toast').css('display','none');
                            Materialize.toast(notif, 5000);
                            $('#im-create-transfer-modal-loading-indicator').html('');
                        }
                        else $(this).parent().removeClass('has-error');

                        // If not empty, show send whole package selection
                        if (isValid && value !== undefined && value !== null && value !== '') {
                            // If draft does not exist yet, create draft
                            if (currentSeedTransferDbId == null) {
                                // Create a draft seed transfer record (with toast message)
                                // Data: seedTransferStatus = 'draft', sourceListDbId, and senderProgramId*
                                $.ajax({
                                    url: '$createSeedTransferDraftUrl',
                                    type: 'post',
                                    datatType: 'json',
                                    cache: false,
                                    data: {
                                        sourceListDbId: value
                                    },
                                    success: function(response) {
                                        // Parse response
                                        var resultData = JSON.parse(response)
                                        var seedTransferDbId = resultData.seedTransferDbId;
                                        var sourceListDbId = resultData.sourceListDbId;
                                        var senderProgramDbId = resultData.senderProgramDbId;
                                        var senderDisplayText = resultData.senderDisplayText;
                                        // Update variables
                                        currentSeedTransferDbId = seedTransferDbId
                                        data.senderProgramDbId = senderProgramDbId
                                        data.sourceListDbId = sourceListDbId

                                        // Update sender id in the data variable
                                        $('#im-create-st-receiver-program-select').attr('data-sender-id',senderProgramDbId)

                                        // Update sender program display
                                        $("#im-create-st-sender-program-span").html('<strong>' + senderDisplayText + '</strong>');
                                        // Show sender program display and receiver program input
                                        $("#im-create-st-sender-program-div").show();
                                        $("#im-create-st-receiver-program-div").show();

                                        // If package unit array is not empty, set package unit input 
                                        // default value as package unit in array.
                                        // Disable input.
                                        var packageUnit = validation.packageUnit;
                                        if(packageUnit.length == 1) {
                                            let unitValue = packageUnit[0];
                                            $('#im-create-st-package-unit-select').val(unitValue).trigger("change");
                                            $('#im-create-st-package-unit-select').attr('disabled', true);
                                        }

                                        // Display toast notification
                                        var notif = "<i class='material-icons blue-text'>info</i>&nbsp;Seed transfer draft successfully saved.";
                                        Materialize.toast(notif, 3500);
                                        // Remove loading indicator
                                        $('#im-create-transfer-modal-loading-indicator').html('');
                                        // Enable/disable confirm button
                                        toggleConfirmButton();
                                        // Reset the data browser after creation
                                        resetBrowser();
                                    },
                                    error: function(jqXHR, exception) {
                                        var errorMessage = modalText('There was a problem while loading record information.', false, true);
                                        $('#im-create-transfer-modal-body').html(errorMessage);
                                    }
                                });
                            }
                            else if(currentSeedTransferDbId != null && value != data.sourceListDbId) {
                                // Update list db id and sender program id
                                // Also, update data-sender-program-id

                                // Update source list db id
                                data.sourceListDbId = value

                                // Perform update
                                updateSeedTransfer();
                            }
                        }
                        
                        // Enable/disable confirm button
                        toggleConfirmButton();
                    },
                    error: function(err) { }
                });   
            }, 1000);
        });

        // Receiver program select event 
        $(document).on('change', "#im-create-st-receiver-program-select", function() {
            // Get value
            let value = $(this).val();

            // If not empty, show sender program div and receiver program div
            if (value !== undefined && value !== null && value !== '') {
                $("#im-create-st-send-whole-pkg-qty-div").show();

                data.receiverProgramDbId = value

                // Perform update
                updateSeedTransfer();
            }
            // Enable/disable confirm button
            toggleConfirmButton();
        });

        // Radio button click event 
        $(document).on('click', ".im-create-st-send-whole-pkg-qty-radio-input", function() {
            // Get value
            let sendWhole = $(this).data('send-whole');

            // If send whole is true, hide package unit input. Otherwise, show.
            if (sendWhole) {
                $("#im-create-st-pkg-qty-unit-div").hide();
            }
            else {
                $("#im-create-st-pkg-qty-unit-div").show();
            }

            data.isSendWholePackage = sendWhole

            // Perform update
            updateSeedTransfer();
            // Enable/disable confirm button
            toggleConfirmButton();
        });

        // Package quantity input
        $(document).on('input', "#im-create-st-package-quantity-input", function() {
            // Get value
            let value = $(this).val();
            
            // Add constraints to the value
            if (isNotEmpty(value)) {
                value = value.toString();
                
                // Set max to length to 13 to avoid exponential values
                if (value.length > 13) {
                    value = value.slice(0, 13);    
                    if (value.endsWith('.')) value = value.slice(0, -1);
                    this.value = value;
                }
                
                // Set max decimal places to 3
                let regexp = /(\.\d{4,})$/
                if (regexp.test(value)) {
                    value = value.slice(0, value.indexOf('.')+4);
                    this.value = value;
                }
                            
                value = parseFloat(value);
            }

            // If not empty, enable confirm button
            if (value !== undefined && value !== null && value !== '' && value > 0) {
                data.packageQuantity = value
            }
            else {
                data.packageQuantity = null
            }

            // Perform update
            updateSeedTransfer();
            // Enable/disable confirm button
            toggleConfirmButton();
        });

        // Package unit input
        $(document).on('change', "#im-create-st-package-unit-select", function() {
            // Get value
            let value = $(this).val();

            // If not empty, show sender program div and receiver program div
            if (value !== undefined && value !== null && value !== '') {
                data.packageUnit = value

                // Perform update
                updateSeedTransfer();
            }
            else {
                data.packageUnit = null
            }
            // Enable/disable confirm button
            toggleConfirmButton();
        });

        // Confirm exit
        $(document).on('click', '#im-create-transfer-modal-cancel-btn', function() {
            // Modify header text
            $('#im-st-confirm-modal-header').html("Exit Seed Transfer Creation")
            // Add loading indicator
            $('#im-create-transfer-modal').modal('hide');
            $('#im-create-transfer-confirm-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
            $('#im-create-transfer-confirm-modal').modal('show');
            // Show back and proceed button
            $('.im-ctm-confirm-action-btn-span').addClass("hidden");
            $('.im-ctm-confirm-cancel-btn-span').addClass("hidden");
            $('#im-ctm-confirm-exit-span').removeClass("hidden");
            $('#im-ctm-confirm-back-span').removeClass("hidden");

            // If draft is not yet created, notify user
            var message = "";
            if (currentSeedTransferDbId == null) {
                message = "No draft has been created yet. Are you sure you want to exit?"
            }
            else {
                message = "Are you sure you want to cancel creation of a seed transfer? All data are saved and can be edited later."
            }

            $('#im-create-transfer-confirm-modal-body').html('<p>' + message + "</p>");
        });
        
        // Exit button click event
        $(document).on('click', '#im-create-transfer-modal-confirm-exit-btn', function() {
            $('#im-create-transfer-confirm-modal').modal('hide');
        });

        // Confirm cancel
        $(document).on('click', '#im-create-transfer-modal-confirm-back-btn', function() {
            // Add loading indicator
            $('#im-create-transfer-confirm-modal').modal('hide');
            $('#im-create-transfer-modal').modal('show');
        });
        
        // Confirm CREATE seed transfer
        $('#im-create-transfer-modal-confirm-btn').off('click').on('click', function() {
            let modalId = 'im-create-transfer-modal';
            let modalBody = 'im-create-transfer-modal-body';
            
            invokeBackgroundWorker(
                currentSeedTransferDbId,
                'SeedTransferProcessor',
                'Create inventory records for seed transfer ID: ' + currentSeedTransferDbId,
                'create-seed-transfer-record',
                modalId,
                modalBody
            )
        })
    });

    /** 
     * Enables or disables the CONFIRM button based on the current data.
     * If isSendWholePackage is TRUE, then the button is enabled when the seed transfer id,
     *      source list id, sender program id, and receiver program id are all specified.
     * If isSendWholePackage is FALSE, then the button is enabled when the seed transfer id,
     *      source list id, sender program id, receiver program id,
     *      package quantity, and package unit are all specified.
     * Else, the button is disabled.
     */
    function toggleConfirmButton() {
        // If isValid flag is set to FALSE, disable button
        if (!isValid) {
            $("#im-create-transfer-modal-confirm-btn").addClass('disabled');
            return;
        }

        // Case: isSendWholePackage = FALSE
        if (!valueIsEmpty(currentSeedTransferDbId)
            && !valueIsEmpty(data.sourceListDbId)
            && !valueIsEmpty(data.senderProgramDbId)
            && !valueIsEmpty(data.receiverProgramDbId)
            && !valueIsEmpty(data.packageQuantity)
            && !valueIsEmpty(data.packageUnit)
            && !data.isSendWholePackage
        ) {
            $("#im-create-transfer-modal-confirm-btn").removeClass('disabled');
        }
        // Case: isSendWholePackage = TRUE
        else if (!valueIsEmpty(currentSeedTransferDbId)
            && !valueIsEmpty(data.sourceListDbId)
            && !valueIsEmpty(data.senderProgramDbId)
            && !valueIsEmpty(data.receiverProgramDbId)
            && !valueIsEmpty(data.isSendWholePackage)
            && data.isSendWholePackage
        ) {
            $("#im-create-transfer-modal-confirm-btn").removeClass('disabled');
        }
        // Case: DEFAULT
        else {
            $("#im-create-transfer-modal-confirm-btn").addClass('disabled');
        }
        
    }
    
    /**
    * Performs validation on a package list and returns validation flags 
    */
    function validatePackageList(listDbId) {
        let validation = null;
        
        $.ajax({
            url: '$validatePackageListUrl',
            type: 'post',
            dataType: 'json',
            data: {
                listDbId: listDbId
            },
            async: false,
            success: function(response) {
                validation = response;
            },
            error: function(err) { }
        });
        
        return validation;
    }

    /**
     * Checks if the given value is an empty string, null or undefined.
     */
    function valueIsEmpty(value) {
        if (value == '' || value == null || value == undefined) return true;
        return false;
    }
    
    /**
    * Returns true whether value is none of 
    * . undefined
    * . null
    * . empty string
    */
    function isNotEmpty(value) {
        return (value !== undefined && value !== null && value !== '');
    }

    /**
     * Perform the seed transfer update
     */
    function updateSeedTransfer() {
        $.ajax({
            url: '$updateSeedTransferRecordUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                seedTransferDbId: currentSeedTransferDbId,
                recordData: data
            },
            success: function(response) {
                var parsedData = JSON.parse(response)
                // Parse boolean value
                var sendWhole = parsedData.recordData.isSendWholePackage
                if (sendWhole == 'true') sendWhole = true;
                else if (sendWhole == 'false') sendWhole = false;
                else sendWhole = null;
                parsedData.recordData.isSendWholePackage = sendWhole
                // Update data
                data = parsedData.recordData
                var senderDisplayText = parsedData.senderDisplayText

                // Update sender program display
                if (senderDisplayText !== '') {
                    $("#im-create-st-sender-program-span").html('<strong>' + senderDisplayText + '</strong>');
                    // Update sender id in the data variable
                    $('#im-create-st-receiver-program-select').attr('data-sender-id', data.senderProgramDbId)
                }

                resetBrowser();
            },
            error: function(jqXHR, exception) {
                var errorMessage = modalText('There was a problem while loading record information.', false, true);
                $('#im-create-transfer-modal-body').html(errorMessage);
            }
        });
    }
    
    /**
     * Add a timeout for flash messages
     */
    function flashTimeout() {
        time = 7.5;
        if ($("#w7-info-0") || $("#w7-warning-0")){
            // remove flash message after the specified time in seconds (7.5 seconds by default)
            setTimeout(
                function() 
                {
                    $('#w7-info-0').fadeOut(250);
                    $('#w7-warning-0').fadeOut(250);
                }
            , 1000 * time);
        }
    }

    /**
     * Reset the browser and clear selection after bulk update
     */
    function resetBrowser() {
        
        var pageurl = $('#dynagrid-im-seed-transfer li.active a').attr('href');
        $.pjax.reload(
            {
                container:'#dynagrid-im-seed-transfer-pjax',
                url: pageurl,
                replace: false,
                type: 'POST',
                data: {
                    forceReset:false,
                    bulkOperation:true
                }
            }
        );
    }

    /**
     * Format text for modals.
     * @param {string} string the text to be formatted
     * @param {boolean} strong whether or not to add a strong tag <strong> around the text
     * @param {boolean} emphasis whether or not to add an emphasis tag <em> around the text
     * @return {boolean} whether or not to tick the select all checkbox element
     */
    function modalText(string, strong = false, emphasis = false) {
        if(strong) string = '<strong>' + string + '</strong>';
        if(emphasis) string = '<em>' + string + '</em>';
        return '<p style="font-size:115%;">' + string + '</p>';
    }
    
     /**
    * Generic function for invoking a background worker
    * 
    * @param {int} entityDbId - seed transfer identifier
    * @param {string} workerName - name of the background worker to invoke
    * @param {string} description - describes what the worker does
    * @param {string} processName - create-seed-transfer-record
    * @param {string|null} [modalId=null] - modal element ID to be closed
    * @param {string|null} [modalBody=null] - modal body element ID to be used for displaying error messages
    */
    function invokeBackgroundWorker(entityDbId, workerName, description, processName, modalId=null, modalBody=null) {
        if (modalBody != null) {
            $("#" + modalBody).html('<div class="progress"><div class="indeterminate"></div></div>');
        }
        
        // Invoke background worker
        $.ajax({
            url: '$invokeWorkerUrl',
            type: 'post',
            dataType: 'json',
            data: {
                entityDbId: entityDbId,
                workerName: workerName,
                description: description,
                processName: processName
            },
            success: function(response) {
                var success = response.success;
                var status = response.status;
                
                // Display flash message
                if (success) {
                    // Success message
                    displayCustomFlashMessage(
                        'info', 
                        'fa fa-info-circle',
                        'The inventory records are being created in the background. You may proceed with other tasks. You will be notified in this page once done.',
                        'im-bg-alert-div',
                        10
                    );
                } else {
                    // Failure message
                    let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status !== 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if (status == null) message += ' (#002)';

                    displayCustomFlashMessage(
                        'warning', 
                        'fa-fw fa fa-warning', 
                        message,
                        'im-bg-alert-div'
                    );
                }

                if (modalId != null) $("#" + modalId).modal('hide');
                if (modalBody != null) $("#" + modalBody).html('');
                
                reloadGrid();
            },
            error: function() {
                var errorMessage = '<i>There was a problem loading the content.</i>';
                if (modalBody != null) $("#" + modalBody).html(errorMessage);
            }
        });
    }
    
    /**
     * Displays custom flash message
     * 
     * @param {string} status - the status of the message (info, success, warning, error)
     * @param {string} iconClass - icon of flash message
     * @param {string} message - the message to display
     * @param {string} parentDivId - id of the div where the flash message is to be inserted
     * @param {number} [time=7.5] - time in seconds before the flash message is removed from the display. 7.5 seconds by default.
     */
    function displayCustomFlashMessage(status, iconClass, message, parentDivId, time=7.5) {
        // Add flash message to parent div
        $('#' + parentDivId).html(
            '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
                '<div class="card-panel">' +
                    '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                    message +
                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                '</div>' +
            '</div>'
        );

        // Remove flash message after the specified time in seconds
        setTimeout(function() {
            $('#w25-' + status + '-0').fadeOut(250);
            setTimeout(function() {
                $('#' + parentDivId).html('');
            }, 500);
        }, 1000 * time);
    }
    
    /**
    * Reload data browser
    */
    function reloadGrid() {
        $.pjax.reload({
            container: `#${browserId}-pjax`,
            url: '$resetUrl'
        });
    }

JS);

Yii::$app->view->registerCss('

    div.im-creat-st-default-input-sub-div {
        margin-bottom: 15px;
    }

    i.info-icon {
        vertical-align: -32%;
        font-size: 18px;
    }

    i.info-icon-btn {
        vertical-align: -13%;
        font-size: 18px;
    }
');