<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Import Yii
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

// Import components
use app\components\FavoritesWidget;

/**
 * Renders step tabs in Inventory Manager
 */

// Initializes nav bar dropdowns
$currentUrl = '/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;

$tab = match ($action) {
    'update' => 'update',
    'create' => 'create',
    default => 'transfer',
};

// Define URLs
$createIndexUrl = Url::to(['/inventoryManager','program' => $program,'tab' => 'create']);
$updateIndexUrl = Url::to(['/inventoryManager','program' => $program,'tab' => 'update']);
$transferIndexUrl = Url::to(['/inventoryManager/transfer', 'program'=>$program]);

// Determine active tabs
$createTabStatus = str_contains($tab, 'create') ? 'active' : '';
$updateTabStatus = str_contains($tab, 'update') ? 'active' : '';
$transferTabStatus = str_contains($tab, 'transfer') ? 'active' : '';

$isAdmin = Yii::$app->session->get('isAdmin');
$disableTabs = '';

$createTabHTML = !in_array('VIEW_CREATE_TAB',$permissions) ? '' : '
    <li class="tab col step s3 hm-tool-tab" id="create-tab" link="' . $createIndexUrl . '" controller="create">
        <a class="create-tab a-tab '.$createTabStatus.'" href="' . $createIndexUrl . '">' . \Yii::t('app', 'Create') . '</a>
    </li>';
$updateTabHTML = !in_array('VIEW_UPDATE_TAB',$permissions) ? '' : '
    <li class="tab col step s3 hm-tool-tab'.$disableTabs.'" id="update-tab" link="' . $updateIndexUrl . '" controller="update">
        <a class="update-tab a-tab '.$disableTabs.' ' . $updateTabStatus . '" href="' . $updateIndexUrl . '">' . \Yii::t('app', 'Update') . '</a>
    </li>';
$transferTabHTML = !in_array('VIEW_SEED_TRANSFER_TAB',$permissions) ? '' : '
    <li class="tab col step s3 hm-tool-tab" id="transfer-tab" link="'.$transferIndexUrl.'" controller="transfer">
        <a class="transfer-tab a-tab '.$transferTabStatus.'" href="'.$transferIndexUrl.'">' . \Yii::t('app', 'Seed Transfer') . '</a>
    </li>
';

// Set tool name
$nameDisplay = Yii::t('app', 'Inventory Manager') .
FavoritesWidget::widget([
    'module' => 'inventoryManager'
]);

?>

<div class="col-md-9" style="margin:10px;">
    <h3><?= $nameDisplay ?></h3>
</div>

<!-- Tabs here -->
<div class="row" style="margin-top:0px; margin-bottom:0px">
    <ul id="tabs" class="tabs">
        <?php
            echo $createTabHTML . $updateTabHTML . $transferTabHTML;
        ?>
    </ul>
    <br />

</div>