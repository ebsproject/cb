<?php
/*
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders seed/package file upload transaction modal content
 */

use kartik\widgets\FileInput;
use yii\helpers\Url;

// Define constants
switch ($uploadType) {
    case 'seed-package':
        $uploadTypeName = 'Seed and Package';
        break;
    case 'seed':
        $uploadTypeName = 'Seed';
        break;
    case 'package':
        $uploadTypeName = 'Package';
        break;
    default:
        $uploadTypeName = 'Package';
        break;
}

// Define URLs
$resetUrl = Url::to(['/inventoryManager', 'program'=>$program, 'tab'=>$action]);
$processFileUploadUrl = Url::to(['/inventoryManager/default/process-file-upload', 'action'=>$action, 'uploadType'=>$uploadType]);
$invokeBGWorkerUrl = Url::to(['/inventoryManager/templates/invoke-background-worker']);

echo '<div class="col col-md-12 upload-panel" style="margin-top:10px">';

echo FileInput::widget([
    'name' => 'inventory_file_upload',
    'pluginOptions' => [
        'uploadAsync' => false,
        'uploadUrl' => $processFileUploadUrl,
        'allowedFileExtensions' => ['csv'],
        'browseClass' => 'btn waves-effect waves-light browse-btn pull-right',
        'showCaption' => false,
        'showRemove' => false,
        'browseLabel' => 'Browse',
        'removeLabel' => '',
        'uploadLabel' => 'Validate',
        'uploadTitle' => 'Validate uploaded file',
        'uploadClass' => 'btn',
        'removeClass' => 'btn btn-danger',
        'cancelClass' => 'btn grey lighten-2 black-text',
        'dropZoneTitle' => \Yii::t('app', 'Drag and drop file here or<br/>click Browse<br/><small>Use the downloaded '.$uploadTypeName.' File Upload template in CSV format.</small>'),
        'overwriteInitial' => true,
        'maxFileCount' => 1,
        'autoReplace' => true,
        'disabledPreviewTypes' => ['text'],
        'fileActionSettings' => [
            'showUpload' => false,
            'showZoom' => false,
            'removeIcon' => '<i class="fa fa-trash"></i>',
            'showRotate' => false,
            'indicatorNew' => '<i class="fa fa-plus-circle text-warning"></i>',
            'indicatorSuccess' => '<i class="fa fa-check-circle text-success"></i>',
            'indicatorError' => '<i class="fa fa-exclamation-circle text-danger"></i>',
            'indicatorLoading' => '<i class="fa fa-hourglass-half text-muted"></i>',
            'indicatorPaused' => '<i class="fa fa-pause-circle text-info"></i>'
        ],
        'theme' => 'fa',
    ],
    'pluginEvents' => [
        'fileclear' => 'function() { validate("clear"); }',
        'filereset' => 'function() { validate("reset"); }',
        'fileloaded' => 'function(data) { validate("upload",data); }',
        'fileuploaderror' => 'function() { validate("error"); }',
    ],
    'options' => [
        'multiple' => false,
        'accept' => 'csv/*',
        'id' => 'inventory-file-upload'
    ]
]);

echo '<div>';
?>

<?php
$this->registerJs(<<<JS
    var validationSuccess = false;

    // Initial file validation is successful (i.e. all required fields filled)
    $('#inventory-file-upload').on('filebatchuploadsuccess', function(e, data) {
        validationSuccess = true;
        
        let response = data.response;
        
        if (response.success) {
            let fileName = data.files[0].name;
            let fileDbId = response.id;

            var modalID = 'inventory-file-upload-modal';
            var modalBody = 'inventory-file-upload-modal-body';

            // Invoke validation background worker to validate the file data
            invokeBackgroundWorker(
                fileDbId,
                'ValidateInventoryFileUpload',
                'Create inventory records for file upload ID: ' + fileDbId,
                modalID,
                modalBody
            );
        }
    });

    /**
     * Generic function for invoking a background worker
     * @param Integer germplasmFileUploadDbId file upload identifier
     * @param String workerName name of the worker to invoke
     * @param String description general description of what the worker does
     * @param String description general description of what the worker does
     * @param String modalID (optional) modal ID to be closed (if any)
     * @param String modalBody (optional) modal body to be used for displaying error messages
     */
    function invokeBackgroundWorker(germplasmFileUploadDbId, workerName, description, modalID = null, modalBody = null) {
        if(modalBody != null) $("#" + modalBody).html('<div class="progress"><div class="indeterminate"></div></div>');

        // Invoke background worker
        $.ajax({
            url: '$invokeBGWorkerUrl',
            type: 'post',
            data:{
                germplasmFileUploadDbId: germplasmFileUploadDbId,
                workerName: workerName,
                description: description
            },
            success: function(response){
                var result = JSON.parse(response);
                var success = result.success;
                var status = result.status;
                
                // Display flash message
                if(success) {
                    // Success message
                    displayCustomFlashMessage(
                        'info', 
                        'fa fa-info-circle',
                        'The inventory records are being created in the background. You may proceed with other tasks. You will be notified in this page once done.',
                        'im-bg-alert-div',
                        10
                    );
                }
                else {
                    // Failure message
                    let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status != 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if(status == null) message += ' (#002)';

                    displayCustomFlashMessage(
                        'warning', 
                        'fa-fw fa fa-warning', 
                        message,
                        'im-bg-alert-div'
                    );
                }

                // refresh browser
                reloadGrid(modalID,modalBody);
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                if(modalBody != null) $("#" + modalBody).html(errorMessage);
            }
        });
    }

    /**
     * Displays custom flash message
     * @param {string} status the status of the message (info, success, warning, error)
     * @param {string} iconClass icon of flash message
     * @param {string} message the message to display
     * @param {string} parentDivId id of the div where the flash message is to be inserted
     * @param {float} time time in seconds before the flash message is removed from the display. 7.5 seconds by default.
     */
    function displayCustomFlashMessage(status, iconClass, message, parentDivId, time = 7.5) {
        // Add flash message to parent div
        $('#' + parentDivId).html(
            '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
                '<div class="card-panel">' +
                    '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                    message +
                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                '</div>' +
            '</div>'
        );

        // remove flash message after the specified time in seconds (7.5 seconds by default)
        setTimeout(
            function() 
            {
                $('#w25-' + status + '-0').fadeOut(250);
                setTimeout(
                    function() 
                    {
                        $('#' + parentDivId).html('');
                    }
                , 500);
            }
        , 1000 * time);
    }
    
    // Validate if required fields specified upon file upload
    function validate(action, data) {
        // File is cleared
        if (action == 'clear') {
            validationSuccess = false;
        }
    }
    
    /**
     * Reload data browser
     * @param String modalID (optional) modal ID to be closed (if any)
     * @param String modalBody (optional) modal body to be used for displaying error messages
    */
    function reloadGrid(modalID = null, modalBody = null) {
        // reload after 2 seconds
        setTimeout(
            function() 
            {
                if(modalID != null) $("#" + modalID).modal('hide');
                if(modalBody != null) $("#" + modalBody).html('');
                $.pjax.reload({
                    container: '#dynagrid-im-grid-pjax',
                    url: '$resetUrl'
                });
            }
        , 1000 * 2);
    }

JS);
?>
