<?php 
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders Inventory Manager landing page
 */

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

use app\components\FileUploadErrorLogsWidget;
use app\components\FileUploadSummaryWidget;
use app\components\DownloadFileDataCSV;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;
use app\modules\inventoryManager\models\TemplatesModel;

echo Yii::$app->controller->renderPartial('@app/modules/inventoryManager/views/default/tabs.php',[
    'controller' => 'search',
    'program' => $program,
    'action' => $action,
    'permissions' => $permissions
]);

// Define URLs
$resetUrl = Url::to(['/inventoryManager', 'program'=>$program, 'tab'=>$action]);
$renderFileUploadFormUrl = Url::to(['/inventoryManager/default/render-file-upload-form', 'program'=>$program, 'action'=>$action]);
$downloadTemplateUrl = Url::to(['templates/download-template','program'=>$program, 'action'=>$action]);
$selectListUrl = Url::to(['/inventoryManager/templates/select-list', 'program'=>$program, 'action'=>$action, 'permissions' => json_encode($permissions)]);
$notificationsUrl = Url::to(['/inventoryManager/default/push-notifications']);
$newNotificationsUrl = Url::toRoute(['/inventoryManager/default/new-notifications-count']);
$deleteFileUploadRecordUrl = Url::to(['/inventoryManager/default/delete-file-upload-record', 'program'=>$program]);
$renderCreationSummaryUrl = Url::to(['/inventoryManager/create/render-creation-summary']);
$invokeBGWorkerUrl = Url::to(['/inventoryManager/templates/invoke-background-worker']);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

// Define constants
CONST VALID_DELETE_STATUS = ['in queue', 'validation error', 'validated', 'creation failed'];
CONST VALID_COMPLETE_STATUS = ['validated'];
CONST VALID_SUMMARY_STATUS = ['completed'];
CONST VALID_ERROR_LOG_STATUS = ['validation error','creation failed','update failed'];
CONST VALID_DOWNLOAD_STATUS = ['validation error','completed'];

// Data browser ID
$browserId = 'dynagrid-im-grid';

// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// File Status filter for browser
$fileStatusFilter = [
    'IN QUEUE' => 'queued for validation',
    'VALIDATION IN PROGRESS' => 'validation in progress',
    'VALIDATED' => 'successfully validated',
    'VALIDATION ERROR' => 'failed validation'
];
$actionFilter = [
    'CREATION IN PROGRESS' => 'creation in progress',
    'CREATION FAILED' => 'failed creation of records',
    'COMPLETED' => 'completed creation of records'
];
if($action == 'update') {
    $actionFilter = [
        'UPDATE IN PROGRESS' => 'update in progress',
        'UPDATE FAILED' => 'failed update of records',
        'COMPLETED' => 'completed update of records'
    ];
}
$fileStatusFilter = array_merge($fileStatusFilter,$actionFilter);

// Columns in file upload transaction browser
$defaultColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'order' => kartik\dynagrid\DynaGrid::ORDER_FIX_LEFT,
        'header' => false
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'template' => ' {complete} {view} {error} {download} {delete} ',
        'buttons' => [
            'complete' => function ($url, $model) {
                if(in_array($model['fileStatus'], VALID_COMPLETE_STATUS)){
                    return Html::a('<i class="material-icons">check_circle</i>',
                        '#',
                        [
                            'class' => 'complete-inventory-file-upload',
                            'title' => Yii::t('app', 'Complete File Upload Transaction'),
                            'data-id' => $model['germplasmFileUploadDbId'],
                            'data-toggle' => 'modal',
                            'data-target' => '#im-complete-confirmation-modal',
                        ]
                    );
                }
            },
            'delete' => function ($url, $model) {
                if (in_array($model['fileStatus'], VALID_DELETE_STATUS)) {
                    return Html::a('<i class="material-icons">delete</i>',
                        '#',
                        [
                            'class' => 'delete-inventory-file-upload',
                            'title' => Yii::t('app', 'Delete File Upload Transaction'),
                            'data-id' => $model['germplasmFileUploadDbId'],
                            'data-toggle' => 'modal',
                            'data-target' => '#inventory-delete-modal',
                        ]
                    );
                }
            },
            'view' => function ($url, $model) use ($resetUrl) {
                if (in_array($model['fileStatus'], VALID_SUMMARY_STATUS)) {
                    $fileUploadAction = $model['fileUploadAction'];
                    // If action is not supported, return nothing
                    if(!in_array($fileUploadAction, TemplatesModel::FILE_UPLOAD_ACTIONS)) {
                        return;
                    }
                    // Append action string
                    $configAbbrevPrefix = 'IM_' . strtoupper($fileUploadAction) . "_";
                    
                    $type = $model['remarks'];
                    // If type is not specified, return nothing
                    if(empty($type)) {
                        return;
                    }
                    // Append type string
                    $configAbbrevPrefix .= TemplatesModel::getTypeString($type) . '_';

                    return FileUploadSummaryWidget::widget([
                        "sourceToolName" => "Inventory Manager",
                        "fileDbId" => $model['germplasmFileUploadDbId'],
                        "returnUrl" => $resetUrl,
                        "configAbbrevPrefix" => $configAbbrevPrefix
                    ]);
                }
            },
            'error' => function ($url, $model) use ($resetUrl) {
                if (in_array($model['fileStatus'], VALID_ERROR_LOG_STATUS)) {
                    return FileUploadErrorLogsWidget::widget([
                        "sourceToolName" => "Inventory Manager",
                        "fileDbId" => $model['germplasmFileUploadDbId'],
                        "returnUrl" => $resetUrl
                    ]);
                }
            },
            'download' => function ($url, $model) use ($resetUrl) {
                if(in_array($model['fileStatus'], VALID_DOWNLOAD_STATUS)){
                    $fileUploadAction = $model['fileUploadAction'];
                    // If action is not supported, return nothing
                    if(!in_array($fileUploadAction, TemplatesModel::FILE_UPLOAD_ACTIONS)) {
                        return;
                    }
                    // Append action string
                    $configAbbrevPrefix = 'IM_' . strtoupper($fileUploadAction) . "_";
                    
                    $type = $model['remarks'];
                    // If type is not specified, return nothing
                    if(empty($type)) {
                        return;
                    }
                    // Append type string
                    $configAbbrevPrefix .= TemplatesModel::getTypeString($type) . '_';

                    return DownloadFileDataCSV::widget([
                        "sourceToolName" => "Inventory Manager",
                        "fileDbId" => $model['germplasmFileUploadDbId'],
                        "returnUrl" => "none",
                        "configAbbrevPrefix" => $configAbbrevPrefix
                    ]);
                }
            },
        ],
        'vAlign' => 'top',
        'header' => false,
        'noWrap' => true,
        'visible' => false
    ],
    [
        'attribute' => 'fileStatus',
        'contentOptions' => [
            'class' => 'im-status-col'
        ],
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $fileStatusFilter,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'multiple' => false,
                'allowClear' => true
            ],
        ],
        'filterInputOptions' => [
            'placeholder' => 'Select status'
        ],
        'value' => function($model) {
            return !empty($model['fileStatus']) ? $model['fileStatus'] : 'unknown';
        },
        'content' => function($model) use ($action) {
            $fileStatus = strtoupper($model['fileStatus']);
            return match ($fileStatus) {
                'IN QUEUE' => '<span title="Queued for validation" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'IN QUEUE') . '</strong></span>',
                'VALIDATION IN PROGRESS' => '<span title="Ongoing validation of transaction" class="new badge orange darken-2"><strong>' . \Yii::t('app', 'VALIDATION IN PROGRESS') . '</strong></span>',
                'VALIDATED' => '<span title="Validation completed" class="new badge blue darken-2"><strong>' . \Yii::t('app', 'VALIDATED') . '</strong></span>',
                'VALIDATION FAILED', 'VALIDATION ERROR' => '<span title="Validation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'VALIDATION FAILED') . '</strong></span>',
                'CREATION IN PROGRESS' => '<span title="Ongoing creation of records" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'CREATION IN PROGRESS') . '</strong></span>',
                'UPDATE IN PROGRESS' => '<span title="Ongoing update of records" class="new badge yellow darken-2"><strong>' . \Yii::t('app', 'UPDATE IN PROGRESS') . '</strong></span>',
                'CREATION FAILED' => '<span title="Creation failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'CREATION FAILED') . '</strong></span>',
                'UPDATE FAILED' => '<span title="Update failed" class="new badge red darken-2"><strong>' . \Yii::t('app', 'UPDATE FAILED') . '</strong></span>',
                'COMPLETED' => '<span title="'.($action == 'create' ? 'Creation' : 'Update').' completed" class="new badge green darken-2"><strong>' . \Yii::t('app', 'COMPLETED') . '</strong></span>',
                default => '<span title="Status unknown" class="new badge grey darken-2"><strong>' . \Yii::t('app', 'STATUS UNKNOWN') . '</strong></span>',
            };
        },
        'headerOptions'=> [
            'style' => 'text-align:left'
        ],
        'visible' => false
    ],
    [
        'attribute' => 'fileName',
        'contentOptions'=> [
            'class' => 'im-file-name-col'
        ],
        'value' => function($model) {
            return !empty($model['fileName']) ? $model['fileName'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute' => 'seedCount',
        'contentOptions'=> [
            'class' => 'im-seed-count-col'
        ],
        'value' => function($model) {
            return !empty($model['seedCount']) ? $model['seedCount'] : 0;
        },
        'content' => function($model) {
            return !empty($model['seedCount']) ? $model['seedCount'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute' => 'packageCount',
        'contentOptions'=> [
            'class' => 'im-package-count-col'
        ],
        'value' => function($model) {
            return !empty($model['packageCount']) ? $model['packageCount'] : 0;
        },
        'content' => function($model) {
            return !empty($model['packageCount']) ? $model['packageCount'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute' => 'uploader',
        'contentOptions' => [
            'class' => 'im-uploader-col'
        ],
        'value' => function($model) {
            return !empty($model['uploader']) ? $model['uploader'] : 0;
        },
        'content' => function($model) {
            return !empty($model['uploader']) ? $model['uploader'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
    [
        'attribute' => 'uploadTimestamp',
        'contentOptions' => [
            'class' => 'im-upload-ts-col'
        ],
        'value' => function($model) {
            return !empty($model['uploadTimestamp']) ? $model['uploadTimestamp'] : 0;
        },
        'content' => function($model) {
            return !empty($model['uploadTimestamp']) ? $model['uploadTimestamp'] : '<span class="not-set">(not set)</span>';
        },
        'visible' => false
    ],
];

// TEMPLATE BUTTON
$templateButtonTitleText = Yii::t('app', 'Template');
$optionTemplateFromListText = Yii::t('app', 'Template from list');
$optionBlankTemplateText = Yii::t('app', 'Blank template');
$options = '';
$optionsAvailable = [];
$optionAllowed = [];
foreach($templateOptions as $to) {
    $displayText = $to['display_text'];
    $type = $to['type'];
    $typeModified = $type;
    if ($type == "seed-package") $typeModified = "seed_and_package";
    // Check if action is allowed based on RBAC permissions. If not, skip
    if(!in_array(strtoupper($action)."_".strtoupper($typeModified), $permissions)) {
        $optionAllowed[] = 'hidden';
        continue;
    }
    $optionAllowed[] = '';
    $optionsAvailable[] = $type;

    // Build options element
    $options .= "
        <li 
            id='im-blank-template-$type-btn'
            class='im-blank-template-option-btn hide-loading'
            title='$displayText template'
            type='$type'
        >
            <a
                data-template-type='$displayText'
                class='im-blank-template-option-a'
            >$displayText</a>
        </li>
    ";
}

$disableButtons = count($optionsAvailable) > 0 ? '' : 'disabled';
$uploadButtonTitleText = Yii::t('app', 'Upload');
$uploadSeedPackageButtonTitleText = Yii::t('app', 'Upload '.($action == 'create' ? 'seed and package' : 'seed').' file');
$uploadPackageButtonTitleText = Yii::t('app', 'Upload package file');
$uploadButton = "
    <a
        title='$uploadButtonTitleText'
        id='inventory-file-upload-btn'
        class='dropdown-trigger btn $disableButtons'
        style='margin-left:5px;  margin-right:10px;'
        data-activates='inventory-upload-actions-list'
        data-beloworigin='true'
        data-constrainwidth='false'
    >
        ".$uploadButtonTitleText."
        <span class='caret'></span>
    </a>
    
    <ul id='inventory-upload-actions-list' class='dropdown-content'>
        <!-- Upload seed and package file or seed only entry point -->
        <li
            id='inventory-upload-seed-package-btn'
            class='hide-loading " . $optionAllowed[0] . "'
            title='$uploadSeedPackageButtonTitleText'>" .
            Html::a(
                '<i class="material-icons inline-material-icon" style="font-size: 18px;">file_upload</i> '.($action == 'create' ? 'Seed and package' : 'Seeds')
            ) . "
        </li>
        <!-- Upload package entry point -->
        <li
            id='inventory-upload-package-btn'
            class='hide-loading " . $optionAllowed[1] . "'
            title='$uploadPackageButtonTitleText'>" .
            Html::a(
                '<i class="material-icons inline-material-icon" style="font-size: 18px;">file_upload</i> '.($action == 'create' ? 'Package only' : 'Packages')
            ) . "
        </li>
    </ul>
    
";


$templateButton = "
    <a
        title='$templateButtonTitleText'
        id='im-template-btn'
        class='dropdown-trigger btn $disableButtons'
        style='margin-left:5px;  margin-right:5px;'
        data-activates='im-template-actions-list'
        data-beloworigin='true'
        data-constrainwidth='false'
    >
        ".$templateButtonTitleText."
        <span class='caret'></span>
    </a>
    
    <ul id='im-template-actions-list' class='dropdown-content'>
        <!-- Template from list entry point -->
        <li
            id='im-template-from-list-btn'>" .
            Html::a(
                $optionTemplateFromListText
            ) . "
        </li>
        <!-- Blank template entry point -->
        <li
            id='im-blank-template-btn'>" .
            Html::a(
                $optionBlankTemplateText,
                null,
                [
                    'class' => 'im-blank-template-btn dropdown-button dropdown-trigger dropdown-button-pjax',
                    'data-activates' => 'im-blank-template-options',
                    'data-alignment' => 'right',
                    'data-hover' => 'hover',
                ]
            ) .
                "<ul
                    id='im-blank-template-options'
                    class='dropdown-content'
                    style='
                        max-width: 130px;
                        margin-left: 129px;
                    '
                >" .
                    $options
                    .
                "</ul>"
             . "
        </li>
    </ul>
    
";

echo "<div id='im-bg-alert-div'></div>";

// DynaGrid configuration
DynaGrid::begin([
    'columns' => $defaultColumns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'export' => [
            'showConfirmAlert' => false,
        ],
        'exportConfig' => [],
        'id' => 'im-grid-table-con',
        'tableOptions' => [
            'class' => 'im-grid-table'
        ],
        'options' => [
            'id' => 'im-grid-table-con'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [ 'id'=> 'im-grid-id' ],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => [
            'before' => '<div>'.\Yii::t('app', 'This is the browser for all uploaded seed and package '.($action == 'create' ? 'creation' : 'update').' transactions. <b>Transactions across all programs are displayed.</b>').'</div>',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' => $templateButton . $uploadButton . 
                    Html::a(
                        '<i class="material-icons large">notifications_none</i>
                        <span id="notif-badge-id" class=""></span>',
                        '#',
                        [
                            'id' => 'im-notification-btn',
                            'class' => 'btn waves-effect waves-light im-notification-btn im-tooltipped',
                            'title' => 'Notifications',
                            'data-pjax' => 0,
                            'style' => "overflow: visible !important;",
                            "data-position" => "top",
                            "data-tooltip" => \Yii::t('app', 'Notifications'),
                            "data-activates" => "notifications-dropdown",
                            'disabled' => false,
                        ]
                    ) .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        $resetUrl,
                        [
                            'id' => 'reset-im-grid',
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app','Reset grid'),
                            'data-position' => 'top',
                            'data-tooltip' => \Yii::t('app', 'Reset grid'),
                            'data-pjax' => true
                        ]
                    ) . '{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'resizableColumns' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options'=> [
        'id' => $browserId,
    ]
]);
DynaGrid::end();

// File upload modal
Modal::begin([
    'id' => 'inventory-file-upload-modal',
    'header' => '<h4 id="inventory-file-upload-modal-header"></h4>',
    'footer' => Html::a('Cancel', [''], [
            'id' => 'inventory-file-upload-cancel-btn',
            'class' => 'modal-close-button',
            'title' => \Yii::t('app', 'Cancel'),
            'data-dismiss' => 'modal'
        ]) . '&emsp;&nbsp;',
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin'
    ],
    'closeButton' => ['class'=>'hidden'],
]);
echo '<div class="parent-panel" id="inventory-file-upload-modal-body"></div>';
Modal::end();

// Confirm delete transaction modal
Modal::begin([
    'id' => 'inventory-delete-modal',
    'header' => '<h4>Delete File Upload Transaction</h4>',
    'footer' =>
        Html::a(\Yii::t('app','Cancel'), '#', [
            'id' => 'inventory-delete-modal-cancel-btn',
            'data-dismiss' => 'modal']).'&emsp;'.
        Html::a(\Yii::t('app', 'Confirm'), '#', [
            'class' => 'btn btn-primary waves-effect waves-light modal-close',
            'id' => 'inventory-delete-modal-confirm-btn',
            'title' => \Yii::t('app', 'Proceed')]).'&emsp;',
    'options' => [
        'data-backdrop' => 'static',
        'style' => 'z-index: 2000;'
    ],
    'closeButton' => ['class'=>'hidden'],
]);
echo '<div id="inventory-delete-modal-body"></div>';
Modal::end();

// Confirm complete transaction modal
Modal::begin([
    'id' => 'im-complete-confirmation-modal',
    'header' => '<h4>Confirm '.($action == 'create' ? 'Creation' : 'Update').'</h4>',
    'footer' =>
    Html::a(\Yii::t('app','Cancel'), ['#'], ['class'=>'im-modal-close-btn', 'id'=>'im-cancel-creation-btn', 'data-dismiss'=>'modal']) . '&emsp;' .
    Html::a(\Yii::t('app', 'Proceed'), '#',
        [
            'class' => 'btn btn-primary waves-effect waves-light modal-close im-modal-proceed-creation-btn',
            'id' => 'im-modal-proceed-creation-btn',
            'title' => \Yii::t('app', 'Proceed')
        ]
    ) . '&emsp;'
    . '&emsp;',
    'options' => [
        'data-backdrop' => 'static',
        'style' => 'z-index: 2000;'
    ]
]);
echo '<div id="im-complete-confirmation-modal-body"></div>';
Modal::end();
?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<?php

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
    var notificationsUrl = '$notificationsUrl';
    var newNotificationsUrl = '$newNotificationsUrl';
    var action = '$action'
    var selectedId = null;

    // Check for new notifications every 7.5 seconds
    notificationInterval=setInterval(updateNotif, 7500);

    $(document).ready(function() {
        
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        
        flashTimeout();
        notifDropdown();
        renderNotifications();
        updateNotif();

        // Reload grid on pjax success
        $(document).on('ready pjax:success', function(e) {
            e.preventDefault();
            e.stopPropagation();

            notifDropdown();

            $('#notif-container').html(
                '<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>'
                );
                
            renderNotifications();
            updateNotif();
            
            refresh();
        });
        
        // Seed and Package upload modal entry point
        $(document).on('click', '#inventory-upload-seed-package-btn', function(e) {
            e.preventDefault();
            e.stopPropagation();
            
            // Set modal display
            var modalHeader = '<h4><i class="material-icons" style="vertical-align:bottom">file_upload</i> '+(action == 'create' ? 'Seed and Package' : 'Seed')+' File Upload</h4>'
            $('#inventory-file-upload-modal-header').html(modalHeader);
            
            $('#inventory-file-upload-modal').modal('show');
            
            var modalBodyId = '#inventory-file-upload-modal-body';
            $(modalBodyId).html(loading);
            
            $.ajax({
                url: '$renderFileUploadFormUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    uploadType: action == 'create' ? 'seed-package' : 'seed'
                },
                success: function(response) {
                    $(modalBodyId).html(response);
                },
                error: function() {
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBodyId).html(errorMessage);
                }
            });
        });

        // blank template btn click event
        $(document).on('click', '.im-blank-template-option-btn', function(e) {
            // Get template type
            var type = $(this).attr('type');

            // Build template download url
            let downloadTemplateUrl = '$downloadTemplateUrl'
                + '&type=' + type;

            // Build complete URL
            let templateFileUrl = window.location.origin + downloadTemplateUrl +
            '&redirectUrl=' + encodeURIComponent(window.location.href);

            window.location.assign(templateFileUrl);
        })

        // display and hide loading indicator
        $(document).on('click','.hide-loading',function(){
            $('#system-loading-indicator').css('display','block');
            setTimeout(function() {
                $('#system-loading-indicator').css('display','none');
            },1700);
        });

        // template from list btn click event
        $(document).on('click', '#im-template-from-list-btn', function(e) {
            selectListUrl = '$selectListUrl'
            window.location.assign(selectListUrl);
        });
            
        // Package upload modal entry point
        $(document).on('click', '#inventory-upload-package-btn', function(e) {
            e.preventDefault();
            e.stopPropagation();
            
            // Set modal display
            var modalHeader = '<h4><i class="material-icons" style="vertical-align:bottom">file_upload</i> Package File Upload</h4>'
            $('#inventory-file-upload-modal-header').html(modalHeader);
            
            $('#inventory-file-upload-modal').modal('show');
            
            var modalBodyId = '#inventory-file-upload-modal-body';
            $(modalBodyId).html(loading);
            
            $.ajax({
                url: '$renderFileUploadFormUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    uploadType: 'package'
                },
                success: function(response) {
                    $(modalBodyId).html(response);
                },
                error: function() {
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBodyId).html(errorMessage);
                }
            });
        });

        /**
         * On complete button click event
         */
        $(document).on('click', '.complete-inventory-file-upload', function(e) {
            germplasmFileUploadDbId = $(this).attr('data-id');
            // Enable proceed button
            $('#im-modal-proceed-creation-btn').removeClass('disabled');
            var modalBody = '#im-complete-confirmation-modal-body';

            $(modalBody).html('<div class="progress"><div class="indeterminate"></div></div>');

            // Render summary
            $.ajax({
                url: '$renderCreationSummaryUrl',
                type: 'post',
                data:{
                    germplasmFileUploadDbId: germplasmFileUploadDbId,
                    action: action
                },
                success: function(response){
                    $(modalBody).html(response);
                },
                error: function(e){
                    var errorMessage = 'There was a problem loading the content. See details below: <br>' + e.responseText;
                    $(modalBody).html(errorMessage);
                }
            });

        });

        /**
         * On delete button click event
         */
        $(document).on('click', '.delete-inventory-file-upload', function(e) {
            selectedId = $(this).attr('data-id');
            $('#inventory-delete-modal-body').html('<p style="font-size:115%;">This will delete the file upload and its contents. Click confirm to proceed.</p>');
            $('#inventory-delete-modal-confirm-btn').removeClass('disabled');
        });
        
        /**
         * On confirm delete button click event
         */
        $(document).on('click', '#inventory-delete-modal-confirm-btn', function(e) {
            $(this).addClass('disabled');
            
            // Add progress bar
            var modalBodyId = '#inventory-delete-modal-body';
            $(modalBodyId).html('<div class="progress"><div class="indeterminate"></div></div>');
            
            $.ajax({
                url: '$deleteFileUploadRecordUrl',
                type: 'post',
                data: {
                    germplasmFileUploadDbId: selectedId
                },
                success: function(response) {
                    $('#inventory-delete-modal').modal('hide');
                    
                    var notif = "<i class='material-icons green-text'>check</i>&nbsp;The file upload transaction was successfully deleted.";
                    Materialize.toast(notif, 3500);
                    
                    reloadGrid();
                },
                error: function(){
                    $("#inventory-delete-modal").modal('hide');
                    
                    var notif = "<i class='material-icons red-text'>close</i>&nbsp;An error occurred. The file upload transaction was not deleted.";
                    Materialize.toast(notif, 3500);
                }
            });
        });

        refresh();
    });

    /**
     * On proceed creation of seeds/packages button click event
     */
    $(document).on('click', '#im-modal-proceed-creation-btn', function(e) {
        // Disable button to prevent double transactions
        $(this).addClass('disabled');
        var modalID = 'im-complete-confirmation-modal';
        var modalBody = 'im-complete-confirmation-modal-body';

        invokeBackgroundWorker(
            germplasmFileUploadDbId,
            'CreateSeedAndPackageRecords',
            'Create seeds and packages for germplasm file upload ID: ' + germplasmFileUploadDbId,
            'The seed/package records are being created in the background. You may proceed with other tasks. You will be notified in this page once done.',
            modalID,
            modalBody
        );
    });

    /**
     * Reload data browser
     * @param String modalID (optional) modal ID to be closed (if any)
     * @param String modalBody (optional) modal body to be used for displaying error messages
    */
    function reloadGrid(modalID = null, modalBody = null) {
        if (modalID !== null && modalBody !== null) {
            // reload after 2 seconds
            setTimeout(
                function() 
                {
                    if(modalID != null) $("#" + modalID).modal('hide');
                    if(modalBody != null) $("#" + modalBody).html('');
                    $.pjax.reload({
                        container: "#$browserId-pjax",
                        url: '$resetUrl'
                    });
                }
            , 1000 * 2);
        }
        else {
            $.pjax.reload({
                container: "#$browserId-pjax",
                url: '$resetUrl'
            });
        }   
    }
    
    /**
    * Resize grid table upon loading 
    */
    function refresh() {
        var maxHeight = ($(window).height() - 370);
        $(".kv-grid-wrapper").css("height", maxHeight);
        $('.dropdown-trigger').dropdown();
    }
    
    /**
     * Add a timeout for flash messages
     */
    function flashTimeout() {
        time = 7.5;
        if ($("#w7-info-0") || $("#w7-warning-0")){
            // remove flash message after the specified time in seconds (7.5 seconds by default)
            setTimeout(
                function() 
                {
                    $('#w7-info-0').fadeOut(250);
                    $('#w7-warning-0').fadeOut(250);
                }
            , 1000 * time);
        }
    }

    /**
     * Updates notifications
     */
    function updateNotif() {
        $.getJSON(newNotificationsUrl, 
            {},
            function(json){
                $('#notif-badge-id').html(json);

                if(parseInt(json)>0){
                    $('#notif-badge-id').addClass('notification-badge red accent-2');
                }else{
                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                }
        })
        .fail( function(jqXHR, textStatus, errorThrown) {
            console.log("Message: "+textStatus+" : "+errorThrown);
        });
    }

    /**
     * Initialize dropdown materialize  for notifications
     */
    function renderNotifications(){
        $('#im-notification-btn').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            belowOrigin: true,
            alignment: 'right',
            stopPropagation: true
        });
    }

    /**
     * Renders the notification in dropdown UI
     */
    function notifDropdown(){
        $('#im-notification-btn').on('click',function(e){
            $.ajax({
                url: notificationsUrl,
                type: 'post',
                data: {},
                async:false,
                success: function(data) {

                    var pieces = data.split('||');
                    var height = pieces[1];

                    $('<style>#notifications-dropdown.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');

                    data = pieces[0];

                    renderNotifications();

                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                    $('#notifications-dropdown').html(data);

                    $('.bg_notif-btn').on('click', function(e){
                        var notifHTML = $(this).html();

                        // refresh browser
                        $.pjax.reload({
                            container: "#$browserId-pjax",
                            url: '/index.php/inventoryManager?program=$program&tab=$action'
                        });
                    });
                },
                error: function(xhr, status, error) {
                    var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');

                    Materialize.toast(toastContent, 'x', 'red');
                    $('.toast-error').collapsible();
                }
            });
        });
    }


    /**
     * Generic function for invoking a background worker
     * @param {integer} germplasmFileUploadDbId file upload identifier
     * @param {string} workerName name of the worker to invoke
     * @param {string} description general description of what the worker does
     * @param {string} description general description of what the worker does
     * @param {string} successMsg notification for successful background worker creation
     * @param {string} modalID (optional) modal ID to be closed (if any)
     * @param {string} modalBody (optional) modal body to be used for displaying error messages
     */
    function invokeBackgroundWorker(germplasmFileUploadDbId, workerName, description, successMsg, modalID = null, modalBody = null) {
        if(modalBody != null) $("#" + modalBody).html('<div class="progress"><div class="indeterminate"></div></div>');

        // Invoke background worker
        $.ajax({
            url: '$invokeBGWorkerUrl',
            type: 'post',
            data:{
                germplasmFileUploadDbId: germplasmFileUploadDbId,
                workerName: workerName,
                description: description
            },
            success: function(response){
                var result = JSON.parse(response);
                var success = result.success;
                var status = result.status;
                
                // Display flash message
                if(success) {
                    // Success message
                    displayCustomFlashMessage(
                        'info', 
                        'fa fa-info-circle',
                        successMsg,
                        'im-bg-alert-div',
                        10
                    );
                }
                else {
                    // Failure message
                    let message = 'A problem occurred while starting the background process. Please try again at a later time or <a href="https://ebsproject.atlassian.net/servicedesk/customer/portals" target="_blank">report an issue</a>.';
                    // If status is not 200, the problem occurred while creating the background job record (code: #001)
                    if (status != null && status != 200) message += ' (#001)';
                    // If status is 200, the problem occurred while connecting to the background worker (code: #002)
                    else if(status == null) message += ' (#002)';

                    displayCustomFlashMessage(
                        'warning', 
                        'fa-fw fa fa-warning', 
                        message,
                        'im-bg-alert-div'
                    );
                }

                // refresh browser
                reloadGrid(modalID,modalBody);
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                if(modalBody != null) $("#" + modalBody).html(errorMessage);
            }
        });
    }

    /**
     * Displays custom flash message
     * @param {string} status the status of the message (info, success, warning, error)
     * @param {string} iconClass icon of flash message
     * @param {string} message the message to display
     * @param {string} parentDivId id of the div where the flash message is to be inserted
     * @param {float} time time in seconds before the flash message is removed from the display. 7.5 seconds by default.
     */
    function displayCustomFlashMessage(status, iconClass, message, parentDivId, time = 7.5) {
        // Add flash message to parent div
        $('#' + parentDivId).html(
            '<div id="w25-' + status + '-0" class="alert ' + status + '">' +
                '<div class="card-panel">' +
                    '<i class="' + iconClass + ' hm-bg-info-icon"></i>&nbsp;&nbsp;' +
                    message +
                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                '</div>' +
            '</div>'
        );

        // remove flash message after the specified time in seconds (7.5 seconds by default)
        setTimeout(
            function() 
            {
                $('#w25-' + status + '-0').fadeOut(250);
                setTimeout(
                    function() 
                    {
                        $('#' + parentDivId).html('');
                    }
                , 500);
            }
        , 1000 * time);
    }
JS);

?>

<style>
    #dynagrid-im-grid .kv-grid-wrapper {
        min-height: 500px;
    }
    .small {
        font-size: 1.0rem !important;
    }
    .icon-bg-circle {
        color: #fff;
        padding: .4rem;
        border-radius: 50%;
    }
    h6 {
        font-size: 1rem;
        line-height: 110%;
        margin: 0.5rem 0 0.4rem 0;
    }
    .round {
        display: inline-block;
        height: 30px;
        width: 30px;
        line-height: 30px;
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #222;
        color: #FFF;
        text-align: center;
    }
    body {
        overflow-x: hidden;
    }
    .notification-badge {
        font-family: "Poppins", sans-serif;
        position: relative;
        right: 0px;
        top: -20px;
        color: #ffffff;
        background-color: #3f51b5;
        margin: 0 -.8em;
        border-radius: 50%;
        padding: 2px 5px;
    }

    #notifications-dropdown h5 {
        font-size: 1rem;
        text-transform: capitalize;
        font-weight: 500;
    }

    #notifications-dropdown li {
        padding: 8px 16px;
        font-size: 1rem;
    }

    #notifications-dropdown li > a {
        padding: 0;
        font-size: 1.1rem;
        font-weight: 300;
    }

    #notifications-dropdown li > a > span {
        display: inline-block;
        font-size: 1.2rem;
        position: relative;
        top: 4px;
        margin-right: 5px;
    }

    #notifications-dropdown li > time {
        font-size: 0.8rem;
        font-weight: 400;
        margin-left: 38px;
    }

    #notifications-dropdown li.divider {
        padding: 0;
    }
    .small {
        font-size: 1.0rem !important;
    }
    .icon-bg-circle {
        color: #fff;
        padding: .4rem;
        border-radius: 50%;
    }
    h6 {
        font-size: 1rem;
        line-height: 110%;
        margin: 0.5rem 0 0.4rem 0;
    }
    .round {
        display: inline-block;
        height: 30px;
        width: 30px;
        line-height: 30px;
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #222;    
        color: #FFF;
        text-align: center;  
    }
</style>