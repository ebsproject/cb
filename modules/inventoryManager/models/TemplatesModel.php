<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\inventoryManager\models;

// Import dependencies
use app\interfaces\models\IConfig;
use app\interfaces\models\IProgram;
use app\interfaces\models\ICropProgram;
use app\interfaces\models\IUser;
use app\interfaces\modules\inventoryManager\models\IListsModel;
use app\modules\germplasm\models\FileExport;

/**
 * Model class for Templates
 */
class TemplatesModel {

    // Define class constants
    CONST SEED_PACKAGE_TEMPLATE = "seed-package";
    CONST SEED_TEMPLATE = "seed";
    CONST PACKAGE_TEMPLATE = "package";
    // Define entity fields
    CONST GERMPLASM_FIELDS = "germplasm.germplasm_code AS germplasmCode|germplasm.designation AS designation";
    CONST GERMPLASM_DATA = [ "germplasmCode", "designation" ];
    CONST SEED_FIELDS = "seed.seed_code AS seedCode|seed.seed_name AS seedName";
    CONST SEED_DATA = [ "seedCode", "seedName" ];
    CONST PACKAGE_FIELDS = "package.package_code AS packageCode|package.package_label AS packageLabel";
    CONST PACKAGE_DATA = [ "packageCode", "packageLabel" ];
    // Define action fields
    CONST ACTION_CREATE_CONFIG = [
        self::SEED_PACKAGE_TEMPLATE => [
            "fields" => self::GERMPLASM_FIELDS,
            "data" => self::GERMPLASM_DATA,
        ],
        self::PACKAGE_TEMPLATE => [
            "fields" => self::SEED_FIELDS,
            "data" => self::SEED_DATA,
        ]
    ];
    CONST ACTION_UPDATE_CONFIG = [
        self::SEED_TEMPLATE => [
            "fields" => self::SEED_FIELDS,
            "data" => self::SEED_DATA,
        ],
        self::PACKAGE_TEMPLATE => [
            "fields" => self::PACKAGE_FIELDS,
            "data" => self::PACKAGE_DATA,
        ]
    ];
    // Define action constants
    CONST ACTION_CREATE = "create";
    CONST ACTION_UPDATE = "update";
    CONST FILE_UPLOAD_ACTIONS = [
        self::ACTION_CREATE,
        self::ACTION_UPDATE
    ];

    /**
     * Class constructor
     */
    public function __construct(
        public IConfig $configurations,
        public IProgram $program,
        public ICropProgram $cropProgram,
        public IUser $user,
        public IListsModel $listsModel
    )
    { }

    /**
     * Retrieves the crop code given the program code
     * @param String programCode code of the program
     * @return String crop code
     */
    public function getCropCodeGivenProgramCode($programCode) {
        // Get crop program ID of program
        $result = $this->program->searchAll(
            params: [
                "programCode" => "equals $programCode",
                "fields" => "program.program_code AS programCode|program.crop_program_id AS cropProgramDbId"
            ],
            filters: "limit=1",
            retrieveAll: false
        );
        $data = $result['data'] ?? [];
        $programInfo = $data[0] ?? [];
        $cropProgramDbId = $programInfo['cropProgramDbId'] ?? 0;

        // Get crop code
        $result = $this->cropProgram->searchAll(
            params: [
                "cropProgramDbId" => "equals $cropProgramDbId",
                "fields" => "cropProgram.id AS cropProgramDbId|crop.crop_code AS cropCode",
            ],
            filters: "limit=1",
            retrieveAll: false
        );
        $data = $result['data'] ?? [];
        $cropProgramInfo = $data[0] ?? [];
        return $cropProgramInfo['cropCode'] ?? "";
    }

    /**
     * Builds the type string of the config abbrev
     * given the template type. Returns an empty string
     * if the type is unsupported.
     * 
     * @param String $type type of the template. Supports: seed, package, seed-package
     * @return String type string
     */
    public static function getTypeString($type) {
        // Build type string
        if ($type === self::SEED_PACKAGE_TEMPLATE) {
            return 'SEED_PACKAGE';
        }
        if ($type === self::PACKAGE_TEMPLATE) {
            return 'PACKAGE';
        }
        if ($type === self::SEED_TEMPLATE) {
            return 'SEED';
        }
        
        return '';
    }

    /**
     * Retrieves the configuration based on program code and type
     * @param String $programCode code of the program
     * @param String $type Type of file. Supports: seed-package or package.
     *  Defaults to `package` if other than `seed-package` is specified.
     * @param string $action File upload action type. Supports: create or update.
     */
    public function getConfigurations($programCode, $type, $action='create') {
        if (!in_array($action, self::FILE_UPLOAD_ACTIONS)) {
            return [];
        }

        // Initialize
        $baseConfigAbbrev = 'IM_'.strtoupper($action).'_';
        $typeString = TemplatesModel::getTypeString($type).'_';

        $isAdmin = $this->user->isAdmin();

        // If user is an admin, check for admin specific configs
        if($isAdmin) {
            $adminConfigAbbrev = $baseConfigAbbrev . $typeString . "ROLE_ADMIN";
            $adminConfig = $this->configurations->getConfigByAbbrev($adminConfigAbbrev) ?? [];
            $adminConfigValues = $adminConfig['values'] ?? [];
            if(!empty($adminConfigValues)) {
                return $adminConfigValues;
            }
        }

        // Get system default config
        $systemDefaultConfigAbbrev = $baseConfigAbbrev . $typeString . "SYSTEM_DEFAULT";
        $systemDefaultConfig = $this->configurations->getConfigByAbbrev($systemDefaultConfigAbbrev) ?? [];
        $systemDefaultConfigValues = $systemDefaultConfig['values'] ?? [];

        // Get crop code
        $cropCode = $this->getCropCodeGivenProgramCode($programCode);

        // Get config for Program and crop
        $configAbbrev = $baseConfigAbbrev . $typeString . $cropCode . "_" . $programCode;
        $config = $this->configurations->getConfigByAbbrev($configAbbrev) ?? [];
        // If config does not exist, retrieve default config for the crop
        if(empty($config)) {
            $configAbbrev = $baseConfigAbbrev . $typeString . $cropCode . "_DEFAULT";
            $config = $this->configurations->getConfigByAbbrev($configAbbrev) ?? [];
        }
        $configValues = $config['values'] ?? [];

        return array_merge($systemDefaultConfigValues, $configValues);
    }

    /**
     * Extracts the column headers from the given config
     * @param Array config configuration array
     * @param Array entityOrderArr array containing the order of entities
     * @param Boolean $useColumnHeader indicator to use designated column header instead of abbrev
     * @return Array template headers
     */
    public function extractTemplateHeaders($config, $entityOrderArr = ['seed','package'],$useColumnHeader = false) {
        $templateHeaders = [];
    
        foreach($entityOrderArr as $entity){
            $configArr = array_filter($config,function($config) use ($entity){
                return isset($config['entity']) && $config['entity'] == $entity;
            });

            // Get headers
            $configArr = $useColumnHeader && !empty(array_column($configArr,'column_header')) ? 
                        array_column($configArr,'column_header') : array_column($configArr,'abbrev');

            // Combine with main array
            $templateHeaders = array_merge($templateHeaders, $configArr);
        }

        return $templateHeaders;
    }

    /**
     * Retrieves list data given the list id and template type
     * @param Integer listDbId list identifier
     * @param String templateType type of template
     * @return Array file data for the template to be generated
     * @param String action file upload action type. Supports: create or update.
     */
    public function getListData($listDbId, $templateType, $headers, $action = self::ACTION_CREATE) {
        // Set fields and data variables based on template type
        $fields = "";
        $dataVariables = [];
        $actionConfig = [];

        // Get action config
        if($action == self::ACTION_CREATE) {
            $actionConfig = self::ACTION_CREATE_CONFIG;
        }
        else if($action == self::ACTION_UPDATE) {
            $actionConfig = self::ACTION_UPDATE_CONFIG;
        }

        // Get fields and data variables
        $templateConfig = $actionConfig[$templateType] ?? [];
        if(!empty($templateConfig)) {
            $fields = $templateConfig['fields'];
            $dataVariables = $templateConfig['data'];
        }

        // Get list members
        $result = $this->listsModel->searchAllMembers($listDbId,[
            "fields" => $fields,
            "isActive" => "equals true"
        ]);
        $listMembers = $result['data'] ?? [];

        // Get number of headers
        $headerCount = count($headers);

        // Build file data array
        $fileData = [];
        foreach($listMembers as $member) {
            $rowData = [];

            // Loop through data variables N times where N = number of headers.
            // This will fill other columns not that are not part of
            // the data variables with an empty string to ensure
            // that the file will be properly read once uploaded.
            for($i = 0; $i < $headerCount; $i += 1) {
                // If ith element in data variables is set,
                // get the list member value for that variable to row data.
                if(!empty($dataVariables[$i])) $rowData[] = $member[$dataVariables[$i]];
                // If ith element in data variables is NOT set,
                // add empty string to row data.
                else $rowData[] = '';
            }

            // Append row data to the main file data array
            $fileData[] = $rowData;
        }

        return $fileData;
    }

    /**
     * Facilitates the creation of CSV files
     * 
     * @param Array headers array of column headers for the file
     * @param String programCode program code
     * @param String type type of file. Supports: seed-package or package
     * @param Array data file data
     * @param String fileNamePrefix prefix for the name of the file
     * @param String fileName (optional) specific file name to be used
     */
    public function buildCSVFile($headers, $programCode, $type, $data = [[]], $fileNamePrefix = 'IM_UPLOAD', $fileName = null) {
        // Add headers to template file content
        $content = [
            $headers
        ];
        // Add data to content
        $content = array_merge($content, $data);

        // Initialize CSV
        $generateTemplate = new FileExport(FileExport::FILE_UPLOAD_TEMPLATE, FileExport::CSV, time());

        // Build file name if file name is not specified
        if(empty($fileName)) {
            $cropCode = $this->getCropCodeGivenProgramCode($programCode);
            $typeString = str_replace('-','_',strtoupper($type)) . '_';
            $identifier = !empty($cropCode) ? ( !empty($programCode) ? $cropCode . '_' . $programCode : $cropCode ) : '';
            $fileName = $fileNamePrefix . '_' . $typeString . 'TEMPLATE_'. $identifier .'.csv';
        }
        else $fileName .= ".csv";
        

        // File export operations
        $generateTemplate->addFileData($content,$fileName);
        $buffer = $generateTemplate->getUnzipped();
        $content = $buffer[$fileName];

        // Set headers
        header('Content-Type: application/csv');
        header("Content-Disposition: attachment; filename={$fileName};");
    
        // Write to output stream
        $fp = fopen('php://output', 'w');
        
        if (!$fp) {
            die('Failed to create CSV file.');
        }

        foreach ($content as $field) {
            fputcsv($fp, $field);
        }

        exit;
    }

    /**
     * Determines the applicable template options
     * based on the action.
     * 
     * @param String action upload action. Suports: create, update
     * @return Array applicable template option info, including display_text and type
     */
    public function getTemplateOptions($action) {
        if($action == self::ACTION_CREATE) {
            return [
                [
                    "display_text" => "Seed and package",
                    "type" => self::SEED_PACKAGE_TEMPLATE
                ],
                [
                    "display_text" => "Package only",
                    "type" => self::PACKAGE_TEMPLATE
                ]
            ];
        }
        else if($action == self::ACTION_UPDATE) {
            return [
                [
                    "display_text" => "Seeds",
                    "type" => self::SEED_TEMPLATE
                ],
                [
                    "display_text" => "Packages",
                    "type" => self::PACKAGE_TEMPLATE
                ]
            ];
        }

        return [];
    }
}