<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\inventoryManager\models;

use Yii;

/**
 * Model for seed and/or package creation processes
 */
class CreateModel extends \app\modules\germplasm\models\CreateModel
{
    // Reference keys to store data
    CONST KEY_VARIABLES = 'im-variables';
    CONST KEY_VARIABLES_REQUIRED = 'im-variables-required';
    CONST KEY_VARIABLES_OPTIONAL = 'im-variables-optional';

    // Creation types
    CONST SEED_PACKAGE = 'seed-package';
    CONST PACKAGE = 'package';
    CONST SEED = 'seed';

    // File upload action types
    CONST FUA_CREATE = 'create';
    CONST FUA_UPDATE = 'update';

    // Getter methods
    public function getKeyVariables() { return self::KEY_VARIABLES; }
    public function getKeyVariablesRequired() { return self::KEY_VARIABLES_REQUIRED; }
    public function getKeyVariablesOptional() { return self::KEY_VARIABLES_OPTIONAL; }

    /**
     * Returns configuration for seed/package variables per program
     *
     * @param int $programId current dashboard program identifier
     * @param bool $systemDef [optional] flag to return system-defined variables instead
     * @param string $type CreateModel::SEED_PACKAGE | CreateModel::PACKAGE | CreateModel::SEED
     * @param string $action CreateModel::FUA_CREATE | CreateModel::FUA_UPDATE
     * @param bool $admin [optional] flag to retrieve admin-specific configs
     */
    public function getVariableConfig($programId, $systemDef=false, $type='', $action=self::FUA_CREATE, $admin = false)
    {
        if (empty($programId) || !isset($type) || !isset($action)) return [];

        $programInfo = $this->programModel->getProgram($programId);

        // Build the config abbrev
        if (!empty($programInfo)) {
            $result = $this->cropProgramModel->searchAll([
                "cropProgramDbId" => "equals ".$programInfo['cropProgramDbId'],
                "fields" => "cropProgram.id AS cropProgramDbId | crop.crop_code AS cropCode",
            ], retrieveAll: false);

            $cropCode = $result['data'][0]['cropCode'] ?? '';
            $identifier = $systemDef ? 'SYSTEM' : strtoupper($cropCode);

            // If admin flag is set to true, retrieve admin-specific configurations
            if($admin) {
                if ($identifier !== '' && $type === self::SEED_PACKAGE) {
                    $abbrev = join('_', ['IM', strtoupper($action), 'SEED_PACKAGE_ROLE_ADMIN']);
                } else if ($identifier !== '' && $type === self::PACKAGE) {
                    $abbrev = join('_', ['IM', strtoupper($action), 'PACKAGE_ROLE_ADMIN']);
                } else if ($identifier !== '' && $type === self::SEED) {
                    $abbrev = join('_', ['IM', strtoupper($action), 'SEED_ROLE_ADMIN']);
                }
            }
            else {
                if ($identifier !== '' && $type === self::SEED_PACKAGE) {
                    $abbrev = join('_', ['IM', strtoupper($action), 'SEED_PACKAGE', $identifier, 'DEFAULT']);
                } else if ($identifier !== '' && $type === self::PACKAGE) {
                    $abbrev = join('_', ['IM', strtoupper($action), 'PACKAGE', $identifier, 'DEFAULT']);
                } else if ($identifier !== '' && $type === self::SEED) {
                    $abbrev = join('_', ['IM', strtoupper($action), 'SEED', $identifier, 'DEFAULT']);
                }
            }
        }

        if (isset($abbrev)) {
            $config = $this->configModel->getConfigByAbbrev($abbrev);
            Yii::debug('Variable configuration ' . json_encode($config), __METHOD__);

            if (!empty($config) && !empty($config['values'])) {
                return $this->validateConfigVariables($config['values']);
            }
        }

        return [];
    }
}