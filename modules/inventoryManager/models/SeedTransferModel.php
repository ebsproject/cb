<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\inventoryManager\models;

// Import dependencies
use Yii;
use app\dataproviders\ArrayDataProvider;

// Import interfaces
use app\interfaces\models\IScaleValue;
use app\interfaces\models\ISeedTransfer;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\models\IVariable;

// Import models
use app\models\BaseModel;

/**
 * Model class for Seed Transfers
 */
class SeedTransferModel extends BaseModel implements ISeedTransfer
{
    // Displayed columns
    public $seedTransferDbId;
    public $seedTransferStatus;
    public $sourceListDisplayName;
    public $sourceListCount;
    public $senderProgramCode;
    public $sender;
    public $receiverProgramCode;
    public $receiver;

    // Other columns
    public $sendTimestamp;
    public $receiveTimestamp;
    public $creator;
    public $creationTimestamp;

    /**
     * Model constructor
     */
    public function __construct(
        public IScaleValue $scaleValue,
        public IUserDashboardConfig $userDashboardConfig,
        public IVariable $variable)
    { }

    public static function apiEndPoint()
    {
        return 'seed-transfers';
    }

    /**
     * Validation for search text filters
     */
    public function rules()
    {
        return [
            [[
                'seedTransferStatus',
                'sourceListDisplayName',
                'senderProgramCode',
                'sender',
                'receiverProgramCode',
                'receiver',
                'creator',
            ], 'string'],
            [[
                'sourceListCount',
            ], 'integer'],
            [[
                'sendTimestamp',
                'receiveTimestamp',
                'creationTimestamp',
            ], 'safe'],
        ];
    }

    /**
     * Returns a unique identifier for a seed transfer browser
     */
    public function getDataBrowserId()
    {
        return 'dynagrid-im-seed-transfer';
    }

    /**
     * Returns model name to be used by seed transfer browser
     */
    public function getModelName()
    {
        return 'SeedTransferModel';
    }

    /**
     * Retrieves data to be displayed in a seed transfers browser
     *
     * @param array $params search fields
     * @param array $filters limit, sort, page options
     */
    public function search($params=null, $filters='')
    {
        // Retrieve data from API
        $result = $this->searchAll($params, $filters, retrieveAll: false);
        if ($result['status'] == 200 && isset($result['data'])) {
            $data = $result['data'];
        }
        $totalCount = $result['totalCount'] ?? 0;
        $totalPages = $result['totalPages'] ?? 0;

        // Retrieve correct records depending on page
        if (str_contains($filters, 'page')) {
            $updatedFilters = '';
            foreach(explode('&', $filters) as $filter) {
                if (str_contains($filter, 'page')) {
                    $page = explode('=', $filter)[1];
                } else {
                    $updatedFilters = $updatedFilters . $filter . '&';
                }
            }
            // Reset page to 1 on page size change
            if (isset($page) && $page > $totalPages) {
                $result = $this->searchAll($params, $updatedFilters.'page=1', retrieveAll: false);
                $data = $result['data'] ?? [];
                $totalCount = $result['totalCount'] ?? 0;
                $browserPageParam = $this->getDataBrowserId().'-page';
                $_GET[$browserPageParam] = 1;
            }
        }

        return new ArrayDataProvider([
            'allModels' => $data ?? [],
            'key' => 'seedTransferDbId',
            'sort' => [
                'attributes' => [
                    'seedTransferDbId',
                    'senderProgramDbId',
                    'senderProgramCode',
                    'receiverProgramDbId',
                    'receiverProgramCode',
                    'sourceListDbId',
                    'sourceListDisplayName',
                    'sourceListCount',
                    'destinationListDbId',
                    'destinationListDisplayName',
                    'seedTransferStatus',
                    'creator',
                    'creationTimestamp',
                    'sender',
                    'sendTimestamp',
                    'receiver',
                    'receiveTimestamp',
                ],
                'defaultOrder' => [
                    'creationTimestamp' => SORT_DESC,
                    'seedTransferDbId' => SORT_DESC,
                ],
            ],
            'restified' => true,
            'id' => $this->getDataBrowserId(),
            'totalCount' => $totalCount
        ]);
    }

    /**
     * Returns data filters for searching
     *
     * @param array $params user browser request data
     * @param array $programDbIds program identifiers to filter sender/receiver program
     */
    public function getSearchParams($params, $programDbIds=[])
    {
        $fields = [];

        if (isset($params[$this->getModelName()])) {
            // Exclude fields that are empty
            $fieldsRaw = array_filter($params[$this->getModelName()], 'strlen');

            foreach ($fieldsRaw as $field => $value) {
                // Optimize search call with exact values
                if (!str_contains($value, '%')) {
                    $fields[$field] = 'equals ' . $value;
                }
                else $fields[$field] = strtolower($value);
            }
        }

        $defaultFields = [
            "senderProgramDbId" => "[OR] " . join('|', $programDbIds),
            "receiverProgramDbId" => "[OR] " . join('|', $programDbIds),
        ];

        if (!empty($fields)) {
            return array_merge($defaultFields, $fields);
        } else {
            return $defaultFields;
        }
    }

    /**
     * Returns sort, page, and limit filters for searching
     *
     * @param array $params user browser request data
     */
    public function getSearchFilters($params)
    {
        // Set defaults
        $paramSort = '&sort=seedTransferDbId:DESC';
        $paramPage = '&page=1';
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();;

        // Arrange sorting
        $elementId = '#'.$this->getDataBrowserId().'-pjax';
        if (isset($params['sort'], $params['_pjax']) && $params['_pjax'] == $elementId) {
            if (str_contains($params['sort'], '-')) {
                $sortField = str_replace('-', '', $params['sort']);
                $paramSort = "&sort=$sortField:DESC";
            } else {
                $paramSort = '&sort='.$params['sort'];
            }
        }

        // Arrange pagination
        $elementId = $this->getDataBrowserId().'-page';
        if (!empty($params[$elementId])) {
            $paramPage = '&page='.$params[$elementId];
        }

        return $paramLimit.$paramPage.$paramSort;
    }

    /**
     * Returns a list of supported seed transfer status
     */
    public function getStatusOptions()
    {
        $abbrev = 'SEED_TRANSFER_STATUS';
        $variableDbId = $this->variable->searchAll([
                'abbrev' => "equals $abbrev",
                'fields' => 'variable.id AS variableDbId|variable.abbrev as abbrev'
            ])['data'][0]['variableDbId'] ?? 0;

        $scaleValues = $this->scaleValue->getVariableScaleValues($variableDbId, []);

        return $this->variable->getVariableScaleTags($scaleValues);
    }

}