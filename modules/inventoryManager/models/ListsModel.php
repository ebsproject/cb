<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\inventoryManager\models;

// Import dependencies
use yii\data\Sort;
use app\models\Lists;
use app\dataproviders\ArrayDataProvider;

// Import interfaces
use app\interfaces\models\IUser;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\modules\harvestManager\models\IHarvestManagerModel;
use app\interfaces\modules\inventoryManager\models\IListsModel;

/**
 * Model class for Lists
 */
class ListsModel extends Lists implements IListsModel {

    // Define sort parameters
    CONST SORT_PARAMETERS = [
        'creationTimestamp'=>[
            "asc" => ["creationTimestamp"=>SORT_ASC],
            'desc' => ["creationTimestamp"=>SORT_DESC]
        ],
        'listDbId'=>[
            "asc" => ["listDbId"=>SORT_ASC],
            'desc' => ["listDbId"=>SORT_DESC]
        ],
        'type'=>[
            "asc" => ["type"=>SORT_ASC],
            'desc' => ["type"=>SORT_DESC]
        ],
        'name'=>[
            "asc" => ["name"=>SORT_ASC],
            'desc' => ["name"=>SORT_DESC]
        ],
        'abbrev'=>[
            "asc" => ["abbrev"=>SORT_ASC],
            'desc' => ["abbrev"=>SORT_DESC]
        ],
        'displayName'=>[
            "asc" => ["displayName"=>SORT_ASC],
            'desc' => ["displayName"=>SORT_DESC]
        ],
        'creator'=>[
            "asc" => ["creator"=>SORT_ASC],
            'desc' => ["creator"=>SORT_DESC]
        ]
    ];

    public $creator;
    public $creationTimestamp;
    public $displayName;
    public $listDbId;
    public $permission;

    /**
     * Class constructor
     */
    public function __construct(
        public IUser $user,
        public IUserDashboardConfig $userDashboardConfig,
        public IHarvestManagerModel $hmModel
    )
    { }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creatorDbId','entityId','listDbId','modifierDbId','shareCount'], 'integer'],
            [['creatorDbId','name','type'], 'required'],
            [['abbrev','creationTimestamp','creator','description','displayName','listUsage','memberCount','modificationTimestamp','modifier','name','notes','permission','remarks','status','stubType','type'], 'string'],
            [['creatorDbId','entityId','listDbId','modifierDbId','shareCount','abbrev','creationTimestamp','creator','description','displayName','listUsage','memberCount','modificationTimestamp','modifier','name','notes','permission','remarks','status','stubType','type'], 'safe'],
            [[], 'boolean']
        ];
    }

    /**
     * Retreves data to be displayed in the lists browser
     * @param Array params browser parameters
     * @return ArrayDataProvider browser data provider object
     */
    public function search($params, $action = null) {
        // Check if browser pagination and filters will be reset
        $userId = $this->user->getUserId();
        $reset = $this->hmModel->browserReset($userId, $params, 'ListsModel');
        $resetPage = isset($reset['resetPage']) ? $reset['resetPage'] : false;
        $resetFilters = isset($reset['resetFilters'])  ? $reset['resetFilters'] : false;

        // If resetFilters = true, set params to null
        if($resetFilters) {
            $params = null;
        }
        // Load params into the browser search model
        $this->load($params);

        // Build URL params
        $urlParams = $this->assembleUrlParameters($resetPage);
        // Build request body
        $requestBody = $this->assembleBrowserFilters($params, $action);

        // Extract data
        $result = $this->searchAll($requestBody, $urlParams, false);
        $lists = $result['data'] ?? [];
        $totalCount = $result['totalCount'] ?? 0;

        // If no lists are retrieved, reset the pagination.
        if(empty($lists)){
            // Assemble URL parameters
            $urlParams = $this->assembleUrlParameters(true);

            // Extract data
            $result = $this->searchAll($requestBody, $urlParams, false);
            $lists = $result['data'] ?? [];
            $totalCount = $result['totalCount'] ?? 0;
        }

        // Build data provider
        $dataProvider = new ArrayDataProvider([
            'allModels' => $lists,
            'key' => 'listDbId',
            'restified' => true,
            'totalCount' => $totalCount
        ]);
        // Initialize sort
        $sort = new Sort();
        $sort->attributes = ListsModel::SORT_PARAMETERS;
        // Set default order
        $sort->defaultOrder = [
            'creationTimestamp' => SORT_DESC,
            'listDbId' => SORT_DESC
        ];
        // Apply sort to data provider
        $dataProvider->setSort($sort);

        return $dataProvider;
    }

    /**
     * Assemble the URL parameters for 
     * pagination and sorting of the browser
     * @param Boolean resetPage to reset pagination or not
     * @return String URL parameters in string format
     */
    public function assembleUrlParameters($resetPage = false) {
        $gridId = 'dynagrid-im-template-lists-grid';

        // Sorting
        $paramSort = '';
        $currentUrl = \Yii::$app->request->getUrl();
        $getParams = \Yii::$app->request->getQueryParams();
        $defaultSortString = "sort=creationTimestamp:DESC|listDbId:DESC";
        // Check sort settings
        if(strpos($currentUrl,"$gridId-pjax")!==false) {
            if(isset($getParams['sort'])) {
                $paramSort = 'sort=';
                $sortParams = $getParams['sort'];
                $sortParams = explode('|', $sortParams);
                $countParam = count($sortParams);
                $currParam = 1;

                foreach($sortParams as $column) {
                    if($column[0] == '-') {
                        $paramSort = $paramSort . substr($column, 1) . ':DESC';
                    } else {
                        $paramSort = $paramSort . $column . ':ASC';
                    }
                    if($currParam < $countParam) {
                        $paramSort = $paramSort . '|';
                    }
                    $currParam += 1;
                }

            }
            else $paramSort = $defaultSortString;
        } else $paramSort = $defaultSortString;

        // Pagination
        $paramPage = '';
        if($resetPage) {
            $paramPage = 'page=1';
            if (isset($getParams['page'])){
                $getParams['page'] = 1;
            }
            \Yii::$app->request->setQueryParams($getParams);
        }
        else if (isset($getParams['page'])) {
            $paramPage = 'page=' . $getParams['page'];
        }
        
        
        // Page limit
        $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

        $queryParams = [];
        if($paramLimit!='') $queryParams[] = $paramLimit;
        if($paramPage!='') $queryParams[] = $paramPage;
        if($paramSort!='') $queryParams[] = $paramSort;

        return implode("&", $queryParams);
    }

    /**
     * Assembles the browser filters for API retrieval
     * @param Array params query parameters
     * @return Array containing filters for API retrieval
     * @param String action file upload action type. Supports: create or update.
     */
    public function assembleBrowserFilters($params, $action = null) {
        $browserFilters = [];
        $filters = isset($params['ListsModel']) ? $params['ListsModel'] : [];

        // Loop through filters
        foreach($filters as $key => $val) {
            if(!empty($val)) {
                // Special case: permission
                if($key == "permission") {
                    $userId = $this->user->getUserId();
                    $notStr = "";
                    if($val == "shared") $notStr = "not ";
                    $browserFilters["creatorDbId"] = $notStr . "equals $userId";
                }
                else if(str_contains($key,"Timestamp")) {
                    $browserFilters[$key] = $val . "%";
                }
                // Check if wildcard was used. If used, do not use 'equals'
                else if(str_contains($val,"%")) $browserFilters[$key] = $val;
                else $browserFilters[$key] = "equals $val";
            }
        }
        
        // If type filter not set, use default type filter
        if(empty($browserFilters['type'])) {
            if ($action == 'create') $browserFilters['type'] = "equals germplasm|equals seed|equals package";
            else $browserFilters['type'] = "equals seed|equals package";
        }

        return $browserFilters;
    }

    /**
     * Retrieves the list abbrev given its ID
     * @param Integer listDbId list identifier
     * @return String list abbrev
     */
    public function getListAbbrev($listDbId) {
        $result = $this->getOne($listDbId);
        $data = $result['data'] ?? [];
        return $data['abbrev'] ?? '';
    }
}