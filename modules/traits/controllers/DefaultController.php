<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\traits\controllers;

use Yii;
use app\components\B4RController;
use app\models\Lists;
use app\models\TraitsSearch;
use app\dataproviders\ArrayDataProvider;
use app\controllers\Dashboard;

/**
 * Controller for the `traits` module
 * Contains methods for managing traits
 */
class DefaultController extends B4RController
{
    protected $lists;
    protected $dashboard;

    public function __construct($id, $module,
        Lists $lists,
        Dashboard $dashboard,
        $config=[]) {
        
        $this->lists = $lists;
        $this->dashboard = $dashboard;
        parent::__construct($id, $module, $config); 
    }
    /**
     * Renders the data browser for traits catalog
     * @param String $program Current program code being accessed
     * @return mixed
     */
    public function actionIndex($program=null) {
        $searchModel = $model = new TraitsSearch();
        $params = Yii::$app->request->queryParams;
        $queryParams = [];
        $sessionName = 'query-tool-traits';
        if (isset($_POST['Query'])) {
            $queryParams = $_POST['Query'];
            Yii::$app->session->set($sessionName, $queryParams);

            // Initialize cache variables for Select All
            Yii::$app->session->set('trait-select_all_flag', 'include mode');   // (string: "exclude mode"/"include mode")
            Yii::$app->session->set('trait-selected_ids', []);                  // (string array of ids)
            Yii::$app->session->set('trait-total_results_count', 0);            // (integer)
            Yii::$app->session->set('trait-api_filter', []);                    // (json)
        } else if (isset($_POST['reset'])) {
            if (isset(Yii::$app->session[$sessionName])) {
                unset(Yii::$app->session[$sessionName]);
            }
            // return programID
            $defaultFilters = $this->dashboard->getFilters();
            $dashProgramId = $defaultFilters->program_id;

            return $dashProgramId;
        }else {
            if (isset(Yii::$app->session[$sessionName]) && !empty(Yii::$app->session[$sessionName])) {
                $queryParams = Yii::$app->session[$sessionName];
                $dataProvider = $searchModel->search($params, $queryParams);
            } else {
                // if no filters are set, return empty dataprovider
                $dataProvider = new ArrayDataProvider([
                    'allModels' => []
                ]);
            }

            $selectedIds = isset(Yii::$app->session['trait-selected_ids'])? Yii::$app->session['trait-selected_ids']: [];
            $selectAllMode = isset(Yii::$app->session['trait-select_all_flag'])? Yii::$app->session['trait-select_all_flag']: 'include mode';

            $listsModel = $this->lists;
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'searchModel' => $searchModel,
                'sessionName' => $sessionName,
                'listsModel' => $listsModel,
                'selectedIds' => $selectedIds,
                'selectAllMode' => $selectAllMode,
                'program' => $program
            ]);
        }
    }
}