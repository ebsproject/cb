<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Renders browser for traits
*/
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\date\DatePicker;
use yii\helpers\Html;
use app\components\FavoritesWidget;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\components\ManageSortsWidget;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use app\models\Lists;
use app\components\QueryWidget;
use yii\web\JsExpression;
use app\widgets\VariableInfo;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

$resetGridUrl = Url::toRoute(['/traits/default/index']);
$listUrl = Url::toRoute(['/account/list','program'=>Yii::$app->userprogram->get('abbrev')]);
$selectAllButtonChecked = ($selectAllMode == 'exclude mode' && empty($selectedIds))? 'checked': '';
$totalResultsCount = $dataProvider->totalCount;
$selectedItemsCount = 'No';
if($selectAllMode == 'include mode') {
    $selectedItemsCount = count($selectedIds) > 0? count($selectedIds): 'No';
} else {    // exclude mode
    $selectedItemsCount = count($selectedIds) > 0? $totalResultsCount - count($selectedIds): $totalResultsCount;
}

// Get config value for minimum search characters
$getSearchCharMin = Yii::$app->config->getAppThreshold('GERMPLASM_MANAGER', 'searchCharMin')?? 3;

echo QueryWidget::widget([
    'configAbbrev' => 'TRAITS_QUERY_FIELDS',
    'sessionName' => $sessionName,
    'gridId' => 'dynagrid-traits-grid',
    'url' => Url::to(['/traits/default/index','program'=>$program]),
    'totalCount' => $dataProvider->totalCount,
    'clearSelectionCb' => "true",
    'tool' => 'trait_browser',
    'searchCharMin' => $getSearchCharMin
]);

//all columns of traits catalog browser
$defaultColumns = [
    [
        'label'=> "Checkbox",
        'header'=>'
            <input type="checkbox" class="filled-in" id="traits-select-all-id" ' . $selectAllButtonChecked . '/>
            <label for="traits-select-all-id"></label>
        ',
        'content'=>function($model) use ($selectedIds, $selectAllMode) {
            $checked = '';
            if($selectAllMode == 'include mode') {
                if(in_array($model['variableDbId'], $selectedIds)) $checked = 'checked';
            } else {    // exclude mode
                if(!in_array($model['variableDbId'], $selectedIds)) $checked = 'checked';
            }
            return '
                <input class="dynagrid-traits-grid_select filled-in" type="checkbox" id="'.$model['variableDbId'].'" '.$checked.'/>
                <label for="'.$model['variableDbId'].'"></label>
            ';
        },
        'hAlign' => 'center',
        'vAlign' => 'top',
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => ' ',
        'template' => '{view}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<i class="material-icons">remove_red_eye</i>',
                    '#',
                    [
                        'class' => 'view-trait',
                        'title' => 'View trait',
                        'data-id' => $model['variableDbId'],
                        'data-label' => $model['label'],
                        'data-target' => '#view-trait-modal',
                        'data-toggle' => 'modal'
                    ]
                );
            },
        ],
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'abbrev',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'label',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'name',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'displayName',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'dataType',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'type',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'usage',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'dataLevel',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'status',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
];

$otherColumns = [
    [
        'attribute'=>'description',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'creator',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'creationTimestamp',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ],
        'visible' => false,
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton'=>true,
                'todayHighlight' => true,
                'showButtonPanel'=>true,
                'clearBtn' => true,
                'options'=>[
                'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton'=>true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ]
    ],
    [
        'attribute'=>'modifier',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'modificationTimestamp',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ],
        'visible' => false,
        'format' => 'dateTime',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton'=>true,
                'todayHighlight' => true,
                'showButtonPanel'=>true,
                'clearBtn' => true,
                'options'=>[
                'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton'=>true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ]
    ],
];

// set up columns for export and grid view
$otherGridColumns = [];
foreach ($otherColumns as $key => $value) {
    $visibleFalse = ['visible'=>false];

    $defaultOtherCols = array_merge($value,$visibleFalse);
    $otherGridColumns[] = $defaultOtherCols;
}

$gridColumns = array_merge($defaultColumns, $otherGridColumns);

//dynagrid configuration
$dynagrid = DynaGrid::begin([
    'columns' => $gridColumns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'export' => [
            'showConfirmAlert' => false,
        ],
        'exportConfig' => [
            GridView::CSV => [
                'label' => 'CSV',
                'icon' => '',
                'showHeader' => true,
                'showPageSummary' => false,
                'showFooter' => false,
                'showCaption' => false,
                'filename' => 'Traits',
                'config' => [
                    'colDelimiter' => ","
                ],
            ],
            GridView::EXCEL => [
                'label' => 'Excel',
                'icon' => '',
                'showHeader' => true,
                'showPageSummary' => false,
                'showFooter' => false,
                'showCaption' => false,
                'filename' => 'Traits',
            ],
        ],
        'id' => 'dynagrid-traits-grid-id',
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>['id'=>'dynagrid-traits-grid-id'],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            'heading'=>'<h3>'.\Yii::t('app', 'Traits') .FavoritesWidget::widget([
                'module' => 'traits',
                'controller' => 'default'
                ]).'</h3>',
            'before' => '<br/>'.\Yii::t('app', 'This is the browser for all the traits. Click the eye icon to view trait information.').
            '<p id="total-selected-text" class="pull-right" style="margin-right: 5px;"><b id = "selected-count">'.$selectedItemsCount.'</b> selected items.</p>',
            'after' => false,
        ],
        'toolbar' =>  [
            [
                'content'=> ""
                    . Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'id' => 'reset-dynagrid-traits-grid', 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid')]).'{dynagridFilter}{dynagridSort}{dynagrid}'
                    . Html::a('<i class="material-icons widget-icons">add_shopping_cart</i>', '#!', [ 'class' => 'btn pull-right', 'id' => 'traits-save-new-list-btn', 'title'=>\Yii::t('app',' Save as new list')]).' '
                    . ManageSortsWidget::widget([
                        'dataBrowserId' => 'traits',
                        'gridId' => 'dynagrid-traits-grid',
                        'searchModel' => 'TraitsSearch',
                        'btnClass' => 'sort-action-button',
                        'resetGridUrl' => $resetGridUrl.'?'
                    ])
            ],

        ],
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'resizableColumns'=>true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options'=>['id'=>'dynagrid-traits-grid']
]);

DynaGrid::end();

//view trait modal
Modal::begin([
    'id' => 'view-trait-modal',
    'header' => '<h4 id="view-entity-label"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
]);
echo '<div class="view-trait-modal-body"></div>';
Modal::end();

// modal for saving as new list
Modal::begin([
    'id' => 'traits-save-list-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">add</i> Save as new list</h4>',
    'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
        .Html::a('Submit',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light disabled',
                'url' => '#',
                'id' => 'traits-save-list-confirm-btn'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);

$form = ActiveForm::begin([
    'enableClientValidation'=>false,
    'id'=>'create-saved-list-form',
    'fieldConfig' => function($model,$attribute){
        if(in_array($attribute, ['abbrev','display_name','name','type'])){
            return ['options' => ['class' => 'input-field form-group col-md-6 required']];
        }else{
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    }
]);

$model = $listsModel;
$listType = [
    'trait' => 'Trait'
];
$listTypeOptions = [
    'placeholder' => Yii::t('app','Select type'),
    'id' => 'traits-list-type',
    'class' => 'text-field-var',
    'required' => 'required',
    'data-name' => 'Type',
    'value' => 'trait'
];

echo $form->field($model, 'name')->textInput([
    'maxlength' => true,
    'id'=>'traits-list-name',
    'title'=>'Name identifier of the list',
    'autofocus' => 'autofocus',
    'onkeyup' => 'js: $("#traits-list-display-name").val(this.value.charAt(0).toUpperCase() + this.value.slice(1)); $(".field-traits-list-display-name > label").addClass("active");'
]).
$form->field($model, 'abbrev')->textInput([
    'maxlength' => true,
    'id'=>'traits-list-abbrev',
    'title'=>'Short name identifier or abbreviation of the list',
    'oninput' => 'js: $("#traits-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());'
]).
$form->field($model, 'display_name')->textInput(['maxlength' => true, 'title'=>'Name of the list to show to users', 'id'=>'traits-list-display_name',]).

$form->field($model, 'type')->widget(
    Select2::class, [
        'data' => $listType,
        'options' => $listTypeOptions
    ])->label(
        Yii::t('app','Type'),['class'=>'control-label', 'style' => 'top: -95%;']
    ).
$form->field($model, 'description')->textarea([ 'class'=>'materialize-textarea', 'title'=>'Description about the list', 'id'=>'traits-list-description']).
$form->field($model, 'remarks')->textarea(['class'=>'materialize-textarea', 'title'=>'Additional details about the list','row'=>2, 'id'=>'traits-list-remarks']);

ActiveForm::end();

echo '<div class="traits-save-list-preview-entries">Number of traits to be saved as a list: <b id = "traits-count"></b></div>';

Modal::end();

$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

$viewVariableUrl = Url::to(['/variable/default/view-info']);
$retrieveListPreviewUrl = Url::to(['/account/list/retrieve-list-preview']);
$getSelectedFromSessionUrl = Url::to(['/account/list/get-selected-from-session']);
$saveListUrl = Url::to(['/account/list/save-list']);
$selectRowUrl = Url::to(['/account/list/select-row']);
$toggleSelectAllUrl = Url::to(['/account/list/toggle-select-all']);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings(['dynagrid-traits-grid']);    // saves current browser settings to db

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = 'dynagrid-traits-grid',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    var resetGridUrl='$resetGridUrl';
    var retrieveListPreviewUrl = "$retrieveListPreviewUrl";
    var saveListUrl = "$saveListUrl";
    var selectRowUrl = "$selectRowUrl";
    var toggleSelectAllUrl = "$toggleSelectAllUrl";
    var totalResultsCount = "$totalResultsCount";
    var selectAllMode = "$selectAllMode";
    var getSelectedFromSessionUrl = '$getSelectedFromSessionUrl';

    var selectedFromSession = null;

    $(document).on('input', '#traits-list-name', function(e) {
        e.preventDefault();
        $('#traits-list-abbrev').val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());
        $('#traits-list-abbrev').trigger('change');
        $('#traits-list-display_name').val(this.value);
        $('#traits-list-display_name').trigger('change');
    });

    // view information about the trait
    $(document).on('click', '.view-trait', function(e) {
        var obj = $(this);
        var label = obj.data('label');
        var id = obj.data('id');

        $('#view-entity-label').html('<i class="fa fa-info-circle"></i> ' + label + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        $('.view-trait-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        //load content of view variable information
        setTimeout(function(){
            $.ajax({
                url: '$viewVariableUrl',
                data: {
                    id: id
                },
                type: 'POST',
                async: false,
                success: function(data) {
                    $('.view-trait-modal-body').html(data);
                },
                error: function(){
                    $('.view-trait-modal-body').html('<i>There was a problem while loading record information.</i>');
                }
            });
        }, 300);
    });

    function disableSelect2Filters(state) {
        $('#variableFilterId').prop('disabled', state);
        $('#parentageFilterId').prop('disabled', state);
        $('#generationFilterId').prop('disabled', state);
    }

    // click row in browser
    $(document).on('click', '#dynagrid-traits-grid-id tbody tr:not(a)',function(e){
    e.stopPropagation();
    e.preventDefault();

    this_row=$(this).find('input:checkbox')[0];
    var id = this_row.id
        $.ajax({
            type: 'POST',
            url: selectRowUrl,
            data: {
                id: id,
                type: 'trait'
            },
            async: true,
            dataType: 'json',
            success: function(response) {
                if(this_row.checked){
                    this_row.checked=false;
                    $('#traits-select-all-id').prop("checked", false);
                    $(this).removeClass("grey lighten-4");
                } else {
                    $(this).addClass("grey lighten-4");
                    this_row.checked=true;
                    $("#"+this_row.id).prop("checked");
                }

                // get total selected
                var count = 0
                if(selectAllMode == 'include mode')
                    count = response > 0? response: 'No';
                else    // exclude mode
                    count = response > 0? totalResultsCount - response: totalResultsCount;
                // toggle select all button
                if(totalResultsCount == count)
                    $('#traits-select-all-id').prop('checked',true);
                else
                    $('#traits-select-all-id').prop('checked',false);
                $('#selected-count').html(count);
            },
            error: function(){
                var notif = "<i class='material-icons red-text'>close</i> There was a problem in selecting item.";
                Materialize.toast(notif,5000);
            }
        });
    });

    // select all traits
    $(document).on('click', '#traits-select-all-id',function(e){
        var mode = 'include mode';
        var count = 'No';
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.dynagrid-traits-grid_select').attr('checked','checked');
            $('.dynagrid-traits-grid_select').prop('checked',true);
            $(".dynagrid-traits-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
            mode = 'exclude mode';
            count = totalResultsCount > 0? totalResultsCount : count;
        }else{
            $(this).removeAttr('checked');
            $('.dynagrid-traits-grid_select').prop('checked',false);
            $('.dynagrid-traits-grid_select').removeAttr('checked');
            $("input:checkbox.dynagrid-traits-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
            mode = 'include mode';
        }

        $.ajax({
            type: 'POST',
            url: toggleSelectAllUrl,
            data: {
                type: 'trait',
                mode: mode
            },
            async: false,
            dataType: 'json',
            success: function(response) {
                $('#selected-count').html(count);
                selectAllMode = mode;
            },
            error: function(){
                var notif = "<i class='material-icons red-text'>close</i> There was a problem in selecting all items.";
                Materialize.toast(notif,5000);
            }
        });
    });

    function refresh(){
        var total = $('#dynagrid-traits-grid-id').find('.summary').children().eq(1).html();
        totalResultsCount = total !== undefined? parseInt(total.replace(/,/g, '')): 0;
        var maxHeight = ($(window).height() - 280);
        $(".kv-grid-wrapper").css("height",maxHeight);

        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });

        $('#reset-dynagrid-traits-grid').on('click', function(e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: toggleSelectAllUrl,
                data: {
                    type: 'trait',
                    mode: 'include mode'
                },
                async: false,
                dataType: 'json',
                success: function(response) {
                    $('#selected-count').html('No');
                    selectAllMode = 'include mode'
                },
                error: function(){
                }
            });

            $.pjax.reload({
                container: '#dynagrid-traits-grid-pjax',
                replace: true,
                url: resetGridUrl
            });
        });

        $('#traits-save-new-list-btn').on('click', function(e) {
            e.preventDefault();
            var count = $('#selected-count').html().replace(/,/g, '');

            if(parseInt(count) > 0) {
                $('#traits-save-list-modal').modal('show');
                $('#traits-save-list-modal').on('shown.bs.modal', function () {
                    $('#traits-list-name').focus();
                })

                $('#traits-count').html(count);

                $.ajax({
                    type: 'POST',
                    url: getSelectedFromSessionUrl,
                    data: {
                        type: 'trait'
                    },
                    async: false,
                    dataType: 'json',
                    success: function(response) {
                        selectedFromSession = response;
                    },
                    error: function(){
                        var notif = "<i class='material-icons red-text'>close</i> There was a problem while loading content.";
                        Materialize.toast(notif,5000);
                    }
                });
            } else {
                var notif = "<i class='material-icons orange-text'>warning</i> Please select a trait.";
                Materialize.toast(notif,5000);
            }
        });

        // validate if all required fields are specified
        $('.form-control').bind("change keyup input",function() {
            var abbrev = $('#traits-list-abbrev').val();
            var name = $('#traits-list-name').val();
            var display_name = $('#traits-list-display_name').val();

            if(abbrev != '' && name != '' && display_name != ''){
                $('#traits-save-list-confirm-btn').removeClass('disabled');
            } else {
                $('#traits-save-list-confirm-btn').addClass('disabled');
            }
        });

        // confirm save trait list
        $('#traits-save-list-confirm-btn').on('click', function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            var abbrev = $('#traits-list-abbrev').val();
            var name = $('#traits-list-name').val();
            var displayName = $('#traits-list-display_name').val();
            var listType = $('#traits-list-type').select2('data')[0].id;
            var description = $('#traits-list-description').val();
            var remarks = $('#traits-list-remarks').val();

            $.ajax({
                type: 'POST',
                url: saveListUrl,
                data: {
                    abbrev: abbrev,
                    name: name,
                    displayName: displayName,
                    type: listType,
                    description: description,
                    remarks: remarks,
                    selectedFromSession: selectedFromSession
                },
                dataType: 'json',
                async: false,
                success: function(response) {
                    if(response['result']){
                        if(response['isBgProcess']) {
                            let exportEntriesUrl = '$listUrl'
                            let expEntUrl = window.location.origin + exportEntriesUrl
                            window.location.replace(expEntUrl)
                        }
                        var notif = "<i class='material-icons green-text'>done</i> " + response['message'];
                        $('#traits-save-list-modal').modal('hide');

                        $.pjax.reload({
                            container: '#dynagrid-traits-grid-pjax'
                        });

                        $.pjax.xhr = null;
                    } else {
                        var notif = "<i class='material-icons red-text'>close</i> "+ response['message'];
                    }
                    Materialize.toast(notif,5000);
                },
                error: function() {
                    var notif = "<i class='material-icons red-text'>close</i> There seems to be a problem while saving the list.";
                    Materialize.toast(notif,5000);
                    $('#traits-save-list-confirm-btn').addClass('disabled');
                }
            });
        });
    }

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();

        refresh();
        disableSelect2Filters(false);
    });

    $(document).on('ready pjax:complete', function(e) {
        e.stopPropagation();
        e.preventDefault();

        refresh();
    });

    $(document).ready(function() {
    /**
    * Move the rows to prevent being covered by the select2 filter row
    * There is a weird behavior of dynagrid that uses select2 as a filter for columns where
    * the row of the select2 widgets cover the results
    */
        var height = $('#dynagrid-traits-grid-id-filters').parent().outerHeight();
        $('#dynagrid-traits-grid-id-container').find('thead').children().eq(0).height(height);
        refresh();
        // disableSelect2Filters(true);

        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
    });

JS
);


Yii::$app->view->registerCss('
    body{
        overflow-x: hidden;
    }
    .dropdown-content {
       overflow-y: visible;
    }
    .dropdown-content .dropdown-content {
       margin-left: -100%;
    }
    .grid-view td {
        white-space: nowrap;
    }
    #query-input-list-btn {
        display: none;
    }
');

?>