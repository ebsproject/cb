<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for metrics widget
*/

namespace app\models;
namespace app\modules\dashboard\models;

use Yii;
use yii\data\SqlDataProvider;
use ChromePhp;

class Sites extends \yii\db\ActiveRecord
{
    /**
     * Get all locations of study
    * @param $filters array list of dashboard filters
    * @return $result array list of location information
    */
    public static function getLocationsofStudies($filters){
        extract($filters);

        $params = [
            "distinctOn" => "siteDbId"
        ];

        $results = Yii::$app->api->getResponse('POST', 'occurrences-search', json_encode($params));
        
        if(isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'])) {
            $results = $results['body']['result']['data'];

            foreach ($results as $occurrences) {
                $siteDbId[] = $occurrences['siteDbId'];
            }
            $siteDbId = implode('|', $siteDbId);
        }

        $param = [
            'geospatialObjectDbId' => $siteDbId
        ];

        $data = Yii::$app->api->getResponse('POST','geospatial-objects-search',json_encode($param), null, true);

        if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
            $geospatial = $data['body']['result']['data'];

            $count = $data['body']['metadata']['pagination']['totalCount'];

            foreach ($geospatial as $geo) {
                $coordinates = explode(",", $geo['geospatialObjectCoordinates']);
                $longitude = (isset($coordinates[0])) ? trim($coordinates[0],"(") : null; 
                $latitude = (isset($coordinates[1])) ? trim($coordinates[1],")") : null;
                $result[] = [
                    'abbrev' => $geo['geospatialObjectCode'],
                    'name' => $geo['geospatialObjectName'],
                    'country' => $geo['parentGeospatialObjectName'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'elevation' => $geo['geospatialObjectCoordinates'],
                    'count' => $count
                ];
            }
        }
        return $result;
    }
}