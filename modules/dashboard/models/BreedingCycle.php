<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for breeding cycle widget
*/

namespace app\models;
namespace app\modules\dashboard\models;

use Yii;
use yii\data\SqlDataProvider;
use app\models\Application;
use app\models\Config;

class BreedingCycle extends \yii\db\ActiveRecord
{
    public $user;
    public $application;

    public function __construct($id, $module,
        Application $application,
        $config=[]) {
        $this->application = $application;

        parent::__construct($id, $module, $config);
    }

	/**
	 * Retrieves tools for a breeding activity in platform.config
	 *
	 * @param $activity text breeding acivity identifier
	 * @return $data array list of applications for a breeding activity
	 */
	public function getAppsByBreedingActivity($activity){
		$applications = []; //list of applications
		$apps = []; //list of application ids from config

		//get mapping of applications per activity
		$configModel = new Config();
		$appArr = $configModel->getConfigByAbbrev('BREEDING_CYCLE_TOOLS');

		if(isset($appArr) && !empty($appArr)){ //if there is saved configuration
			$res = json_encode($appArr);
			$res = json_decode($res);

			foreach ($res->activities as $value) {
				if($value->name == $activity){
					$apps = (isset($value->application_abbrevs)) ? $value->application_abbrevs : [];
				} 
			}

			foreach ($apps as $value) {
				$app = $this->application->getAppInfoByAbbrev($value);

				if(!empty($app) && isset($app)){
					$applications[] = $app;
				}
			}
		}

		return $applications;
	}

	/**
	 * Builds breeding cycle data for sunburst chart
	 *
	 * @return $dataArr array list of formatted breeding cycle activities
	 */
	public static function buildBreedingCycleData(){
		//initialize array
		$dataArr = [];

		//get data in platform.config
		$configModel = new Config();
		$results = $configModel->getConfigByAbbrev('BREEDING_CYCLE_TOOLS');
		$encodedData = json_encode($results);
		$encodedData = json_decode($encodedData);

		//set title
		$title = (isset($encodedData->title)) ? \Yii::t('app',$encodedData->title) : \Yii::t('app','Breeding Cycle');
		$dataArr[] = [
			'id'=>'0',
			'parent'=>'',
			'name'=> $title
		];

		if(isset($encodedData->activities)){
			$activities = $encodedData->activities;
			$count = 1;
			foreach ($activities as $value) {

				$dataArr[] = [
					'id'=>(string)$count,
					'parent'=>'0',
					'value'=>10,
					'name'=> (isset($value->label)) ? \Yii::t('app',$value->label) : \Yii::t('app','Not set'),
					'activity'=> (isset($value->name)) ? $value->name : 'not_set'
				];
				$count++;
			}
		}

		return json_encode($dataArr);
	}
}