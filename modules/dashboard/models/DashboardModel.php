<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for dashboard
*/

namespace app\models;
namespace app\modules\dashboard\models;

use app\controllers\Dashboard;
use Yii;
use app\models\User;
use app\models\UserDashboardConfig;
use yii\data\SqlDataProvider;
use ChromePhp;

class DashboardModel extends \yii\db\ActiveRecord
{
	/**
	 * Returns filter tags given attribute
	 *
	 * @param $attr text attibute
	 * @return $tags array list of filter tags
	 */
	public static function getStudyDataFilterTags($attr){
		//get all programs user have access to
		$userModel = new User();
		$userId = $userModel->getUserId();
		$tags = [];

		$progId = Yii::$app->userprogram->get('id');
		$progId = empty($progId) ? '0' : $progId;

		
		$distinctOn = $attr."_id";
		$table = 'experiment';
		if($attr == 'year'){
			$orderCond = "id:desc";
			$distinctOn = "{$table}_{$attr}";
			$fields = "$distinctOn AS \"id\" | $distinctOn  AS \"text\" | $table.program_id AS \"programDbId\"";
		}else if($attr == 'site'){
			$distinctOn = "id";
			$fields = "$attr.id AS \"id\" | $attr.geospatial_object_name  AS \"text\" | program.id AS \"programDbId\"";
		}else if(in_array($attr, ['season','stage'])){
			$fields = "$table.$distinctOn AS \"id\" | $attr.{$attr}_code AS \"text\" | $table.program_id AS \"programDbId\"";
		}else if ($attr == 'occurrence_name'){
			$distinctOn = 'id';
			$table = 'occurrence';
			$fields = "$table.$distinctOn AS \"id\" | $table.{$attr} AS \"text\" | experiment.program_id AS \"programDbId\"";
		}else{
			$distinctOn = 'id';
			$fields = "$table.$distinctOn AS \"id\" | $table.{$attr} AS \"text\" | $table.program_id AS \"programDbId\"";
		}

		$params = [
			'fields' => $fields,
			'distinctOn' => 'id',
			'programDbId' => (string) $progId
		];

		// retrieve dashboard configuration of the user
		$data = Yii::$app->api->getResponse('POST','occurrences-search',json_encode($params),'sort=text',true);
		if($data['status'] == 200 && isset($data['body']['result']['data'])){
			$tags = $data['body']['result']['data'];
		}

		return $tags;
	}

	/**
	 * Retrieves saved dashboard filters excluding currently selected
	 * @param $selected text currently selected filter
	 * @return $filtersArrs array saved filters tags
	 */
	public static function getSavedDashboardFilterTags($selected){
		$userModel = new User();
		$userId = $userModel->getUserId();

		$data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
		$filtersArrs = [];

        if(isset($data[0]) && !empty($data[0])){ //if there is saved data in config
            $valArr = json_decode($data[0],true);
            $filtersArr = (isset($valArr['saved_dashboard_filters'])) ? $valArr['saved_dashboard_filters'] : [];
        }

        if(!empty($filtersArr)){
	        foreach ($filtersArr as $key => $value) {
	        	if($selected == $key){} //do not inlclude already selected
	    		else{
	        		$filtersArrs[] = ['id'=>$key,'text'=>$key];
	        	}
	        }
	    }
        return $filtersArrs;
	}


	/**
	 * Retrieves saved dashboard filters excluding currently selected
	 * @param $selected text currently selected filter
	 * @return $filtersArrs array saved filters tags
	 */
	public static function getSavedDashboardFilters($selected){
		$userModel = new User();
		$userId = $userModel->getUserId();

		$data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
		$filtersArr = [];

        if(isset($data[0]) && !empty($data[0])){ //if there is saved data in config
            $valArr = json_decode($data[0],true);
            $filtersArr = (isset($valArr['saved_dashboard_filters'])) ? $valArr['saved_dashboard_filters'] : [];
        }

        unset($filtersArr[$selected]);

        ksort($filtersArr,2); //sort asc
        return $filtersArr;
	}

	/**
	 * Get initial page by module, controller, action
	 *
	 * @param $module text module identifier
	 * @param $controller text controller identifier
	 * @param $action text action identifier
	 * @return $initialUrl inital tool url
	 */
	public static function getInitialPageByModule($module, $controller, $action){
		$initUrl = '';

		$params['module'] = $module;

		// if controller is set
		if(!empty($controller)){
			$params['controller'] = $controller;

			// if action is set
			if(!empty($action)){
				$params['action'] = $action;
			}
		}

		$response = Yii::$app->api->getParsedResponse(
			'POST',
			'application-actions-search?applicationDbId:ASC&limit=1',
			json_encode($params)
		);

		// check only module and controller
		if(empty($response['data'][0])){
			unset($params['action']);

			$response = Yii::$app->api->getParsedResponse(
				'POST',
				'application-actions-search?applicationDbId:ASC&limit=1',
				json_encode($params)
			);
		}

		if(empty($response['data'][0])){
			unset($params['controller']);

			$response = Yii::$app->api->getParsedResponse(
				'POST',
				'application-actions-search?applicationDbId:ASC&limit=1',
				json_encode($params)
			);
		}

		if(!empty($response['data'][0]) && isset($response['data'][0])){
			$res = $response['data'][0];
			$module = (isset($res['module'])) ? $res['module'] : '';
			$controller = (isset($res['controller'])) ? $res['controller'] : '';
			$action = (isset($res['action'])) ? $res['action'] : '';

			$controller = !empty($controller) ? '/'. $controller : '';
			$action = !empty($action) ? '/'. $action : '';

			$initUrl = $module . $controller . $action;
		}
		
		return $initUrl;
	}
}