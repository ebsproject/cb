<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for studies widget
*/

namespace app\models;
namespace app\modules\dashboard\models;

use Yii;
use app\modules\dashboard\models\Metrics;
use yii\data\ArrayDataProvider;
use app\models\Application;
use app\models\Experiment;

class Study extends \yii\db\ActiveRecord
{

    /**
     * Returns studies array data provider
     *
     * @param $studyType text study type of study
     * @param $p text whether in configuration page or not
     * @return $dataProvider array product catalog data provider
     */
    public static function getStudyArrayDataProvider($filters,$studyType,$p=null){
        
        extract($filters);
    	//get study condition of user
        $studyFilters = Metrics::getStudyParams($filters);
        $delimiter = "|";

        //get study type condition if any
		if (isset($studyType) && !empty($studyType)){
			$studyType = implode($delimiter, $studyType);
			$studyFilters['experimentType'] = $studyType;
        }
        
        $isOwned = (isset($owned) && ($owned=='true')) ? '&isOwned' : '';
        $studyFilters['experimentStatus'] = 'not equals draft';
        $params = $studyFilters;

        $results = Yii::$app->api->getResponse('POST', 'experiments-search?ownershipType=shared'.$isOwned, json_encode($params), null, true);
        
        $data = [];

		// check if successfully retrieved experiments
		if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'])){
			$data = $results['body']['result']['data'];
        } 

        $pagination = [
            'pageSize' => 10,
        ];

        $sort = [
            'defaultOrder' => ['experimentDbId'=>SORT_DESC],
            'attributes' => ['experimentName', 'experimentYear', 'seasonName', 'stageCode', 'experimentDesignType', 'studyType', 'experimentDbId']
        ];

        if($p == 'configure'){
            $pagination = false;
            $sort = false;
        }

        return new ArrayDataProvider([
            'key' => 'experimentDbId',
            'allModels' => $data,
            'pagination' => $pagination,
            'sort' => $sort
        ]);


    }


	/**
	 * Get study info given study parameters
	 *
	 * @param $studyCond array study parameters
	 * @param $attribute text attribute to be retrieved
	 *
	 * @return $studyInfo array list of study info
	 */
	public static function getStudyInfo($studyCond, $attr = null){

		$data = Yii::$app->api->getResponse('POST','studies-search',json_encode($studyCond));

		// get study info
		if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){

			return $data['body']['result']['data'];


		}
	}

}