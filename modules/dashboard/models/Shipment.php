<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for shimpment widget
*/

namespace app\models;
namespace app\modules\dashboard\models;

use Yii;
use yii\data\SqlDataProvider;
use ChromePhp;

class Shipment extends \yii\db\ActiveRecord
{
	/**
     * Returns shipment data provider
     *
     * @param $condStr text product catalog data browser additional query conditions
     * @return $dataProvider array product catalog data provider
     */
    public static function getShipmentDataProvider($filters,$p=null){
    	extract($filters);
    	//get study condition of user

        $programAbbrev = Shipment::getProgramAbbrevById($program_id);

        $exStatus = " and s.status not in ('DRAFT', 'CREATED')";
        
        $addlCond = ''; //additional condition based from program
        if($programAbbrev == 'ES'){ //ES program
            $addlCond = " and st.abbrev in ('PROGRAM-TO-ES_THRESHING','ES_THRESHING-TO-PROGRAM')";
        }else if($programAbbrev == 'SW'){
            $addlCond = " and st.abbrev in ('PROGRAM-TO-OUTSIDE','OUTSIDE-TO-PROGRAM')
                and (upper(s.status) <> 'DRAFT' or (upper(s.status) = 'DRAFT' and s.shipper_program_id = (select id from master.program where abbrev = 'GRC')))
            ";
        }else if($programAbbrev == 'SHU'){
            $addlCond = " and st.abbrev in ('PROGRAM-TO-OUTSIDE','OUTSIDE-TO-PROGRAM')
                and upper(s.status) not in ('DRAFT','CREATED','WORKBOOK GENERATED','RECEIVED')
            ";
        }

        $fromStr = "
                from
                    seed_warehouse.shipment s
                left join
                    master.user u
                on
                    u.id = s.shipper_id
                    and u.is_void = false
                left join
                    seed_warehouse.shipment_metadata place_meta
                on
                    place_meta.shipment_id = s.id
                    and place_meta.is_void = false
                    and place_meta.variable_id = (select id from master.variable where abbrev = 'PLACE')
                left join
                    master.place place
                on
                    place.id = place_meta.value::integer
                    and place.is_void = false,
                    seed_warehouse.shipment_type st,
                    seed_warehouse.shipment_category sc,
                    master.program pr
                where
                    st.id = s.shipment_type_id
                    and s.category_id = sc.id
                    and s.is_void = false
                    and sc.abbrev != 'DEPOSIT' --not deposit
                    and pr.id = s.shipper_program_id
                    and (s.shipper_program_id = {$program_id} or (s.receiver_program_id = {$program_id}) {$exStatus})
                    {$addlCond}
                ";

        $sql = "select 
                    s.id,
                    s.status,
                    s.transaction_no,
                    s.name,
                    u.display_name as user_sender,
                    (
                        select count(si.id) from seed_warehouse.shipment_item si where shipment_id = s.id and is_void = false
                    ) as no_of_seeds,
                    st.abbrev as type,
                    sc.abbrev as category,
                    pr.abbrev as sender,
                    place.display_name as place,
                    (
                        select value from seed_warehouse.shipment_metadata sm where shipment_id = s.id and is_void = false and sm.variable_id = (select id from master.variable where abbrev = 'SEND_DATE') limit 1
                    ) as date_sent,
                    (
                        select value from seed_warehouse.shipment_metadata sm where shipment_id = s.id and is_void = false and sm.variable_id = (select id from master.variable where abbrev = 'RECEPTION_DATE') limit 1
                    ) as date_received,
                    (
                        select abbrev from master.program where id = s.receiver_program_id and is_void = false
                    ) receiver ". $fromStr ;

        $countSql = 'select count(*) '.$fromStr;

        $count = Yii::$app->db->createCommand($countSql)->queryScalar();

        $pagination = [
            'pageSize' => 10,
        ];
        $sort = [
            'defaultOrder' => ['id'=>SORT_DESC],
            'attributes' => ['id','status','transaction_no','name','no_of_seeds','type','category','user_sender', 'sender','place','receiver','date_sent','date_received']
        ];

        if($p == 'configure'){
            $pagination = false;
            $sort = false;
        }

        return new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            'pagination' => $pagination,
            'sort' => $sort
        ]);


    }

    /**
     * Get program abbrev by program id
     * 
     * @param $programId integer program identifier
     * @return $result text program abbreviation 
     */
    public static function getProgramAbbrevById($programId){
        $sql = "select abbrev from master.program where id = {$programId}";
        $result = Yii::$app->db->createCommand($sql)->queryColumn();
        return (isset($result) && !empty($result)) ? $result[0] : null;
    }

    /**
     * Returns formatted status of shipment
     * 
     * @param $status text status of shipment
     * @return $formattedStatus text formatted status of shipment
     */
    public static function formatStatus($status){
        $status = strtoupper($status);
        switch ($status) {
            case 'DRAFT':
            case 'PENDING':
            case 'CREATED':
            $class = 'orange darken-2';
            break;

            case 'FOR EVALUATION':
            case 'SENT':
            case 'SMTA GENERATED':
            case 'WORKBOOK GENERATED':
            case 'SUBMITTED':
            case 'PROCESSED':
            case 'FOR PROCESSING':
            $class = 'blue darken-2';
            break;

            case 'ACTIVE':
            case 'RECEIVED SHIPMENT':
            case 'SHIPMENT RECEIVED':
            case 'MOVED TO SEED STORAGE':
            case 'FOR SHIPMENT':
            case 'INCOMING SHIPMENT':
            case 'APPROVED':
            case 'RECEIVED':
            case 'DONE':
            case 'COMMITTED':
            case 'COMPLETED':
            $class = 'green darken-2';
            break;

            case 'INCOMPLETE':
            case 'INCOMPLETE SEED LOT DATA';
            case 'NO SEED LOTS SPECIFIED';
                $class = 'red';
            break;
            
            default:
            $class = 'grey';
            break;
        }

        return '<span class="badge new ' . $class . '" style="margin: 1px">' . $status . '</span>';

    }
    
    /**
     * Retrieves seed lots info of shipment
     *
     * @param $id integer shipment identifier
     * @param mixed list seed lots information
     */
    public static function getSeedLotsById($id){
        $checkSortOrderSql = "
            select 
                count(1) 
            from 
                seed_warehouse.shipment_item 
            where 
                sort_order is not null and 
                shipment_id = {$id} and 
                is_void = false";

        $checkSortOrder = Yii::$app->db->createCommand($checkSortOrderSql)->queryScalar();

        if($checkSortOrder && $checkSortOrder != 0){
            $defaultOrder = 'sort_order ASC';    
        }else{
            $defaultOrder = 'code ASC';
        }
        $fromStr = "
            from 
                seed_warehouse.shipment s,
                seed_warehouse.shipment_item si 
                    left join operational.seed_storage ss on ss.id=si.seed_storage_id
                    left join master.product p on p.id = ss.product_id
            where
                s.id = si.shipment_id
                and si.is_void = false
                and s.id = {$id}
                ";

        $sql = "
            select 
                case when p.designation is null then si.designation else p.designation end designation,
                case when ss.gid is null then si.gid else ss.gid end as gid,
                case when ss.label is null then si.label else ss.label end as label,
                case when ss.volume is null then si.volume || ' ' || si.unit else ss.volume || ' ' || ss.unit end as volume,
                (select value from operational.seed_storage_metadata where seed_storage_id = ss.id and variable_id = (
                    select id from master.variable where abbrev = 'SOURCE_STUDY'
                )) as source_study,
                (select value from operational.seed_storage_metadata where seed_storage_id = ss.id and variable_id = (
                    select id from master.variable where abbrev = 'SOURCE_HARV_YEAR'
                )) as source_year
            {$fromStr}
                order by si.{$defaultOrder}
        ";

        $countSql = 'select count(1) '.$fromStr;

        $count = Yii::$app->db->createCommand($countSql)->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            'pagination' => false
        ]);

        return [
            'dataProvider' => $dataProvider,
            'count' => $count
        ];
    }
}