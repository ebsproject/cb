<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for metrics widget
*/

namespace app\models;
namespace app\modules\dashboard\models;

use Yii;
use yii\data\SqlDataProvider;
use ChromePhp;

class Metrics extends \yii\db\ActiveRecord
{

	/**
	 * Get study parameters given dashboard filters
	 *
	 * @param $filters array dashboard filters
	 *
	 * @return $postParams array list of study parameters
	 */
	public static function getStudyParams($filters){
		extract($filters);

		$postParams = [];

		// if admin user, add condition for program
		if(Yii::$app->session->get('isAdmin') !== null && Yii::$app->session->get('isAdmin')){
			$postParams = [
				"programCode" => Yii::$app->userprogram->get('abbrev')
			];
		}

		$delimiter = "|";

		//get year condition if any
		if (isset($year) && !empty($year)){
			$yearCond = implode($delimiter, $year);
			$postParams['experimentYear'] = $yearCond;
		}

		//get season condition if any
		if (isset($season_id) && !empty($season_id)){
			$seasonCond = implode($delimiter, $season_id);
			$postParams['seasonDbId'] = $seasonCond;
		}

		//get stage condition if any
		if (isset($stage_id) && !empty($stage_id)){
			$stageCond = implode($delimiter, $stage_id);
			$postParams['stageDbId'] = $stageCond;
		}

		//get place condition if any
		if (isset($place_id) && !empty($place_id)){
			$placeCond = implode($delimiter, $place_id);
			$postParams['placeDbId'] = $placeCond;			
		}

		return $postParams;

	}

	/**
	 * Get study ids given study parameters
	 *
	 * @param $studyCond array study parameters
	 * @param $attribute text attribute to be retrieved
	 *
	 * @return $studyIds array list of study identifiers
	 */
	public static function getStudyIdsByParams($studyCond, $attr = null){

		$data = Yii::$app->api->getResponse('POST','studies-search',json_encode($studyCond));

		// if get only total count
		if($attr == 'count'){
			$count = 0;
			if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['metadata']['pagination']['totalCount'])){
				$count = $data['body']['metadata']['pagination']['totalCount'];
			}
			
			return $count;
		}

		// get study identifiers
		if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){

			$studies = $data['body']['result']['data'];
			return array_column($studies, 'studyDbId');


		}
	}

	/**
	 * Returns operational studies count
	 *
	 * @param $filters array list of data filters
	 * @return $res integer operational studies count
	 */
	public static function getOperationalStudiesCount($filters){
		extract($filters);

		// get study post params
		$params = Metrics::getStudyParams($filters);

		return Metrics::getStudyIdsByParams($params, 'count');
		

	}

	/**
	 * Returns entries count
	 *
	 * @param $filters array list of data filters
	 * @return $res integer entries count
	 */
	public static function getEntriesCount($filters){
		extract($filters);
		$count = 0;
		
		// get study post params
		$params = Metrics::getStudyParams($filters);
		$studyIds = Metrics::getStudyIdsByParams($params);

		// build entries search params
		$studyIds = (!empty($studyIds)) ? implode("|", $studyIds) : "equals 0";
		
		$studyCond = [
			"studyDbId" => $studyIds
		];

		$data = Yii::$app->api->getResponse('POST','entries-search?limit=1',json_encode($studyCond));

		if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['metadata']['pagination']['totalCount'])){
			$count = $data['body']['metadata']['pagination']['totalCount'];
		}

		return $count;
	}

	/**
	 * Returns observations count
	 *
	 * @param $filters array list of data filters
	 * @return $res integer observations count
	 */
	public static function getObservationsCount($filters){
		extract($filters);

		$count = 0;
		
		// get study post params
		$params = Metrics::getStudyParams($filters);
		$studyIds = Metrics::getStudyIdsByParams($params);

		// build entries search params
		$studyIds = (!empty($studyIds)) ? implode("|", $studyIds) : "equals 0";
			
		$studyCond = [
			"studyDbId" => $studyIds
		];

		$data = Yii::$app->api->getResponse('POST','plots-data-search?limit=1',json_encode($studyCond));

		if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['metadata']['pagination']['totalCount'])){
			$count = $data['body']['metadata']['pagination']['totalCount'];
		}

		return $count;
	}
}