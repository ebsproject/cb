<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use app\components\B4RController;
use app\modules\dashboard\models\Metrics;
use app\models\Study;
use app\controllers\Dashboard;
use ChromePhp;

/**
 * Contains methods for metrics widget
 */
class MetricsController extends B4RController
{

    /**
     * Renders metrics widget
     */
    public function actionIndex($col,$key,$p=null,$timestamp=null,$layout='two',$program){

        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';

        $filters = Dashboard::getFilters(); //get dashboard filters
        $filters = (array) $filters;

        return $this->render('@app/modules/dashboard/views/widgets/metrics.php',[
            'filters' => json_encode($filters),
            'layout' => $layout,
            'timestamp' => preg_replace('/[^A-Za-z0-9\-]/', '',$timestamp)
        ]);

    }

    /**
     * Get summary count of an attribute
     * 
     * @return mixed count and attribute
     */
    public function actionGetsummary(){
        $filters = $_POST['filters'];
        $attr = $_POST['attr'];
        $filters = json_decode($filters);
        $filters = (array) $filters;

        //to make unique metrics pero filter
        $filtstr = '';
        foreach ($filters as $key => $value) {
            if(is_array($value)){
                $value = implode('-', $value).'-';
            }
            if(empty($filtstr)){
                $filtstr = $key . '-'. $value;
            }else{
                $filtstr = $filtstr . $key .'-'. $value;
            }
        }

        $currtime = strtotime("now");
        switch ($attr) {
            case 'study':

                if(Yii::$app->session->get('metrics-time') != null && ($currtime - Yii::$app->session->get('metrics-time')) < 3000 && Yii::$app->session->get('metrics-study-count'.$filtstr) !== null) {
                    $count =  Yii::$app->session->get('metrics-study-count'.$filtstr);
                }else{
                    $count = Metrics::getOperationalStudiesCount($filters);
                    $time = strtotime("now");

                    Yii::$app->session->set('metrics-time',$time);
                    Yii::$app->session->set('metrics-study-count'.$filtstr,$count);
                }

                break;

            case 'entry':

                if(Yii::$app->session->get('metrics-time') != null && ($currtime - Yii::$app->session->get('metrics-time')) < 3000 && Yii::$app->session->get('metrics-entry-count'.$filtstr) !== null) {
                    $count =  Yii::$app->session->get('metrics-entry-count'.$filtstr);
                }else{
                    $count = Metrics::getEntriesCount($filters);
                    $time = strtotime("now");

                    Yii::$app->session->set('metrics-time',$time);
                    Yii::$app->session->set('metrics-entry-count'.$filtstr,$count);
                }
                break;

            case 'observation':

                if(Yii::$app->session->get('metrics-time') != null && ($currtime - Yii::$app->session->get('metrics-time')) < 3000 && Yii::$app->session->get('metrics-observation-count'.$filtstr) !== null) {
                    $count =  Yii::$app->session->get('metrics-observation-count'.$filtstr);
                }else{
                    $count = Metrics::getObservationsCount($filters);
                    $time = strtotime("now");

                    Yii::$app->session->set('metrics-time',$time);
                    Yii::$app->session->set('metrics-observation-count'.$filtstr,$count);
                }
                break;

            default:
                if(Yii::$app->session->get('metrics-time') != null && ($currtime - Yii::$app->session->get('metrics-time')) < 3000 && Yii::$app->session->get('metrics-family-count'.$filtstr) !== null) {
                    $count =  Yii::$app->session->get('metrics-family-count'.$filtstr);
                }else{
                    $count = Metrics::getFamiliesCount($filters);
                    $time = strtotime("now");

                    Yii::$app->session->set('metrics-time',$time);
                    Yii::$app->session->set('metrics-family-count'.$filtstr,$count);
                }
                break;
        }

        return json_encode(['count'=>number_format($count),'attr'=>$attr]);
    }

	/**
	 * Load number of studies per attribute
     *
     * @return $data array list of metrics for number of studies per attribute
	 */
	public function actionLoadnoofstudiesperattr(){
		$field = $_POST['field'];
		$filters = (array) json_decode($_POST['filters']);

		return Metrics::loadNoOfStudiesPerAttr($field,$filters);


	}

    /**
     * Load number of entries per attribute
     *
     * @return $data array list of metrics for number of entries per attribute
     */
    public function actionLoadnoofentriesperattr(){
    	$field = $_POST['field'];
    	$filters = (array) json_decode($_POST['filters']);

		return Metrics::loadNoOfEntriesPerAttr($field,$filters);


    }

    /**
     * Load number of families per attribute
     *
     * @return $data array list of metrics for number of families per attribute
     */
    public function actionLoadnooffamiliesperattr(){
    	$field = $_POST['field'];
    	$filters = (array) json_decode($_POST['filters']);

		return Metrics::loadNoOfFamiliesPerAttr($field,$filters);


    }

    /**
     * Load number of observations per attribute
     *
     * @return $data array list of metrics for number of observations per attribute
     */
    public function actionLoadnoofobservationsperattr(){
    	$field = $_POST['field'];
    	$filters = (array) json_decode($_POST['filters']);

		return Metrics::loadNoOfObservationsPerAttr($field,$filters);


    }

    /**
     * Retrieves list of families
     * 
     * @return $mixed browser of families
     */
    public function actionLoadfamilies(){
        $filters = (array) json_decode($_POST['filters']);

        $data = Metrics::getFamiliesByFilters($filters);

        return Yii::$app->controller->renderPartial('@app/modules/dashboard/views/widgets/families_modal.php',[
            'data' => $data
        ]);
    }

    /**
     * Retrieves list of progenies by gpid1
     * 
     * @return $mixed browser of families
     */
    public function actionLoadprogenies(){
        $gpid1 = $_POST['gpid1'];

        $data = Metrics::getProgeniesByGpid1($gpid1);

        return Yii::$app->controller->renderPartial('@app/modules/dashboard/views/widgets/progenies_modal.php',[
            'data' => $data,
            'gpid1' => $gpid1
        ]);
    }

    /**
     * Export families
     */
    public function actionExportfamilies(){
        $filters = Dashboard::getFilters(); //get dashboard filters
        $filters = (array) $filters;
        $header = "Cross Name,Parentage,Female,Male,Year,No. of Progenies\r\n";

        $dataProvider = Metrics::getFamiliesByFilters($filters,true);

        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="Families-'.date('YmdHi') .'.csv"');
        
        // csv header
        echo $header;
        // csv data
        foreach ($dataProvider as $data) {
            $rows = $data['cross_name'].','.$data['parentage'].','.$data['female'].','.$data['male'].','.$data['year'].','.$data['count'];
            echo $rows."\r\n";
        }

        exit; 
    }

    /**
     * Export progenies
     * @param $gpid1 integer female identifier
     */
    public function actionExportprogenies($gpid1){
        $header = "Designation,Derivative Name,Product Type,Year,Generation\r\n";

        $dataProvider = Metrics::getProgeniesByGpid1($gpid1,true);

        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="Progenies-'.date('YmdHi') .'.csv"');
        
        // csv header
        echo $header;
        // csv data
        foreach ($dataProvider as $data) {
            $rows = $data['designation'].','.$data['derivative_name'].','.$data['product_type'].','.$data['year'].','.$data['generation'];
            echo $rows."\r\n";
        }

        exit; 
    }

    /**
     * Displays list of studies with basic information
     */
    public function actionGetlistofstudies(){
       $studyIds = $_POST['studyIds']; 
       $count = $_POST['count'];
       $label = $_POST['label'];
       $hideBack = (isset($_POST['hideBack'])) ? $_POST['hideBack'] : null;

       $data = Study::getStudyListByIds($studyIds,$count);

       return Yii::$app->controller->renderPartial('@app/modules/dashboard/views/widgets/studies_modal.php',[
            'data' => $data,
            'label' => $label,
            'hideBack' => $hideBack
        ]);

    }
}