<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use app\components\B4RController;
use app\controllers\Dashboard;
use app\modules\dashboard\models\Sites;
use ChromePhp;

/**
 * Contains methods for sites widget
 */
class SitesController extends B4RController
{

	/**
	 * Renders sites widget
	 */
    public function actionIndex(){
        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';

        $dashboard = new Dashboard();
        $filters = $dashboard->getFilters(); //get dashboard filters
        $filters = (array) $filters;

        $data = Sites::getLocationsofStudies($filters); //get locations
        
        $locations = [];
        foreach ($data as $value) {
            $locations[] = [
               doubleval($value['latitude']),
               doubleval($value['longitude']),
               $value['name'] . ' (' .$value['abbrev'] .')'.
               ' <br/>No. of occurrences: '.$value['count'].
               ' <br/>Country: '.$value['country'] .
               ' <br/>Latitude: '.$value['latitude'] .
               '<br/>Longitude: '.$value['longitude']
            ];   
        }

        return $this->render('@app/modules/dashboard/views/widgets/sites.php',[
            'filters' => json_encode($filters),
            'locations' => $locations
        ]);

    }
}