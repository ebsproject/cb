<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use app\components\B4RController;
use app\modules\dashboard\models\BreedingCycle;
use app\controllers\Dashboard;
use ChromePhp;

/**
 * Contains methods for breeding cycle widget in dashboard
 */
class BreedingcycleController extends B4RController
{
    public $breedingCycle;

    public function __construct($id, $module,
    BreedingCycle $breedingCycle,
        $config=[]) {
        $this->breedingCycle = $breedingCycle;

        parent::__construct($id, $module, $config);
    }

	/**
	 * Renders breeding cycle widget
     * @param $col text column identifier
     * @param $key integer widget identifier
     * @param $program text current program abbrev of user
	 */
	public function actionIndex($col,$key,$program){
		//use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';
        //builds breeding cycle data for sunburst chart
        $data = BreedingCycle::buildBreedingCycleData();

        return $this->render('@app/modules/dashboard/views/widgets/breeding_cycle.php',[
            'data' => $data,
            'col' => $col,
            'key' => $key,
            'program' => $program
        ]);
    }

    /**
     * Renders applications for a breeding cycle activity 
     */
    public function actionBreedingcycleselecttool($program){
    	$activity = $_POST['activity'];

    	$applications = $this->breedingCycle->getAppsByBreedingActivity($activity);

    	return Yii::$app->controller->renderPartial('@app/modules/dashboard/views/widgets/select_tool_modal.php',[
			'applications' => $applications,
            'program' => $program
		]);

    }
}
