<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use app\components\B4RController;
use app\controllers\Dashboard;

/**
 * Controller for received service request widget
 */
class ReceivedservicerequestsController extends B4RController
{

    /**
    * Renders received service requests
    * @param $col text column identifier
    * @param $key integer widget identifier
    * @param $p text whether in configuration page or not
    */
    public function actionIndex($col,$key,$p=null){
    
        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';

        $filters = Dashboard::getFilters(); //get dashboard filters
        $filters = (array) $filters;

        //get shipments data provider
        $dataProvider = \app\modules\service\models\ServiceRequest::getDataProvider($filters,$p, 'provider', $_GET);
        
        if(isset($_GET['filter'])){
            $filter=$_GET['filter'];
        }else{
            $filter="show_new";
        }

        $unreadMsgsCount = \app\modules\service\models\ServiceRequest::getUnreadMsgCount($filters,false,$serviceType='provider',$filter);

        return $this->render('@app/modules/dashboard/views/widgets/service_request.php', [
            'dataProvider' => $dataProvider,
            'col' => $col,
            'key' => $key,
            'p'=>$p,
            'serviceType'=>'provider',
            'unreadMsgsCount'=>$unreadMsgsCount,
            'program'=>\app\models\Program::findOne(["id"=>$filters['program_id']])->abbrev,
            'filter' =>$filter
        ]);
    }

}