<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use app\components\B4RController;
use app\controllers\Dashboard;

/**
 * Controller for created service request widget
 */
class CreatedservicerequestsController extends B4RController
{

    /**
    * Renders created service requests widget
    * @param $col text Column identifier
    * @param $key integer Widget identifier
    * @param $p text Whether in configuration page or not
    */
    public function actionIndex($col,$key,$p=null){
    
        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';

        $filters = Dashboard::getFilters(); //get dashboard filters
        $filters = (array) $filters;

        //get shipments data provider
        $dataProvider = \app\modules\service\models\ServiceRequest::getDataProvider($filters,$p,'customer', $_GET);
        if(isset($_GET['filter'])){
            $filter=$_GET['filter'];
        }else{
            $filter="show_new";
        }
        $unreadMsgsCount = \app\modules\service\models\ServiceRequest::getUnreadMsgCount($filters, false,$serviceType='customer', $filter);

        return $this->render('@app/modules/dashboard/views/widgets/service_request.php', [
            'dataProvider' => $dataProvider,
            'col' => $col,
            'key' => $key,
            'p'=>$p,
            'serviceType'=>'customer',
            'unreadMsgsCount'=>$unreadMsgsCount,
            'program'=>\app\models\Program::findOne(["id"=>$filters['program_id']])->abbrev,
            'filter' =>$filter
        ]);
    }

    /**
     * Retrieve all messages in the message thread of a service request
     * @return html Message thread
     */
    public function actionGetMessages() {

        $threadId = $_POST['threadId'];
        echo \app\modules\service\models\ServiceRequest::getMessages($threadId);
    }

    /**
     * Retrieve count of unread messages in the program
     * @param  string $serviceType Customer or provider
     * @param  string $filter Condition for new or all records (show_all,show_new)
     * @return integer|html Count of unread messages
     */
    public function actionGetUnreadMessagesCount($serviceType, $filter) {

        $filters = Dashboard::getFilters(); //get dashboard filters
        $filters = (array) $filters;

        $unreadMsgsCount = \app\modules\service\models\ServiceRequest::getUnreadMsgCount($filters,true, $serviceType, $filter);
        echo $unreadMsgsCount;
    }
}