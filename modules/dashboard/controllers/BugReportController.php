<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use yii\web\Controller;
use app\controllers\Dashboard;
use app\modules\dashboard\models\BugReport;

use yii\web\Request;
use app\models\User;
use yii\web\UploadedFile;
use app\components\B4RController;
use app\modules\dashboard\models\MultipleUploadForm;

/**
 * Contains methods for bug report tool
 */
class BugReportController extends B4RController
{
    /**
    * Renders bug report page
    *
    */
    public function actionIndex($program = ''){
        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';
   
        $userModel = new User();
        $userId = $userModel->getUserId();
        $url = Yii::$app->request->referrer;
        $parsedUrl = explode('?',$url);
        $yii2Url = $parsedUrl[0];
        $yii1Url = $yii2Url;

        //if no logged in user
        if($userId == 0) {
            $email = null;
        }else{
            $email = User::getUserInfo('email');
        }
        
        $type = [
            [
                'id' => 'bug',
                'display_name' => 'Bug'
            ],
            [
                'id' => 'new_feature',
                'display_name' => 'New Feature'
            ],
            [
                'id' => 'improvement',
                'display_name' => 'Improvement'
            ],
            [
                'id' => 'inquiry',
                'display_name' => 'Inquiry'
            ],
            [
                'id' => 'data_curation',
                'display_name' => 'Data Curation and Content'
            ]
        ];
        $type = json_encode($type);
        $form = new MultipleUploadForm();
        return Yii::$app->controller->render('@app/modules/dashboard/views/bug-report/index.php',[
            'email' => $email,
            'type' => $type,
            'yii1Url' => $yii1Url,
            'yii2Url' => $yii2Url,
            'uploadForm' => $form,
            'renderedInYii1' => true
        ]);
    }

    /**
    * Renders bug report widget
    *
    */
    public function actionLoadfields($program = ''){
        $url = parse_url(Yii::$app->request->referrer);
        $userModel = new User();
        $userId = $userModel->getUserId();
        $yii2Url = $_POST['yii2url'];

        if(isset($url['query'])){
            $idxUrl = $url['path'].'?'.$url['query'];
        } else  $idxUrl = $url['path'];

        

        if(isset(Yii::$app->session['userId-'.$userId.'-program-'.$program][$idxUrl]) && Yii::$app->session['userId-'.$userId.'-program-'.$program][$idxUrl] != null){
            $yii1Url = Yii::$app->session['userId-'.$userId.'-program-'.$program][$idxUrl];
        } else { 
            $yii1Url = $yii2Url;
        }

        //if no logged in user
        if($userId == 0) {
            $email = null;
        }else{
            $email = User::getUserInfo('email');
        }

        $type = [
            [
                'id' => 'bug',
                'display_name' => 'Bug'
            ],
            [
                'id' => 'new_feature',
                'display_name' => 'New Feature'
            ],
            [
                'id' => 'improvement',
                'display_name' => 'Improvement'
            ],
            [
                'id' => 'inquiry',
                'display_name' => 'Inquiry'
            ],
            [
                'id' => 'data_curation',
                'display_name' => 'Data Curation and Content'
            ]
        ];
        $type = json_encode($type);
        $form = new MultipleUploadForm();
        return Yii::$app->controller->renderAjax('@app/modules/dashboard/views/bug-report/index.php',[
            'email' => $email,
            'type' => $type,
            'yii1Url' => $yii1Url,
            'yii2Url' => $yii2Url,
            'uploadForm' => $form
        ]);
    }

    /**
     * Creates feedback issue
     * 
     */
    public function actionCreateIssue(){
        if($_POST['request_type'] != NULL){
            $requestArr=[
                'bug'=>5,
                'new_feature'=>6,
                'improvement'=>8,
                'inquiry'=>4,
                'data_curation'=>9
            ];
            $request_type = $requestArr[$_POST['request_type']];
        } else{
            $request_type = 0;
        }
        $reporter = $_POST['reporter'];
        $summary = $_POST['summary'];
        $description = $_POST['description']."

            Yii1 URL: ".$_POST['yii1Url']."
            Yii2 URL: ".$_POST['yii2Url']." 
        ";
        $dataArr =[ 
                'serviceDeskId'=>'1', 
                'requestTypeId'=>$request_type, 
                'requestFieldValues'=> [ 
                    'summary' => $summary, 
                    'description'=> $description, 
                ],
                'raiseOnBehalfOf' => $reporter
        ]; 

        $dataStr = json_encode($dataArr); 

        $curl = curl_init(); 
        curl_setopt_array($curl, array( 
            CURLOPT_URL => 'https://riceinfo.atlassian.net/rest/servicedeskapi/request', 
            CURLOPT_RETURNTRANSFER => true, 
            CURLOPT_ENCODING => "", 
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $dataStr, 
            CURLOPT_USERPWD => getenv('JIRA_EMAIL'). ':' . getenv('JIRA_PWD'),
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json", 
                "Content-type: application/json",
            ), 
        )); 

        $output = curl_exec($curl); 
        $err = curl_error($curl); 
 
        curl_close($curl); 
         
        if ($err) { 
            Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> Failed to create the issue. '.$err );
            echo json_encode('error');
        }
        $decodedOut = json_decode($output, false); 
        
        
        $issueKey = $decodedOut->issueKey; 
        $webLink = $decodedOut->_links->web;

        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Your feedback has been successfully sent. View the details <b><a id="issue-link" href="'.$webLink.'" target="_blank">here.</a></b>');
        echo json_encode($issueKey);
    }

    /**
     * Creates the attachment for the initially created feedback issue
     * @param string $issueKey key of the issue created
     */
    public function actionUploadAttachment($issueKey=''){
        //copy temporary uploaded files
        $attachmentArr = [];
        if (isset($_GET['files']) && isset($_FILES)) {
            $files = $_FILES;
            if(!empty($_FILES)){
                foreach ($files as $key=>$value) {
                    $fileName  = preg_replace('/\s+/', '_', $files[$key]['name']);
                    $sourcePath = $files[$key]['tmp_name'];
                    $targetPath =  Yii::getAlias('@webroot').'/files/attachments/'.$fileName;
                    copy($sourcePath, $targetPath);
                    chmod($targetPath, 0777);
                    $attachmentArr[]=$targetPath;
                }
            }
        }

         $target_url = 'https://riceinfo.atlassian.net/rest/api/2/issue/'.trim($issueKey, '"').'/attachments';   
         
        foreach($attachmentArr as $file){
            //Initialise the cURL var 
            $ch = curl_init();     
            $data = array( 
                'file' => curl_file_create($file), 
            ); 
            
            curl_setopt_array($ch, array( 
                CURLOPT_URL => $target_url, 
                CURLOPT_RETURNTRANSFER => true, 
                CURLOPT_FOLLOWLOCATION => true, 
                CURLOPT_USERPWD => getenv('JIRA_EMAIL'). ':' . getenv('JIRA_PWD'),
                CURLOPT_VERBOSE => 1, 
                CURLOPT_POST => 1, 
                CURLOPT_HTTPHEADER => array( 
                    "Content-Type: multipart/form-data", 
                    "X-Atlassian-Token : nocheck", 
                ), 
                CURLOPT_POSTFIELDS => $data, 
            )); 
            
            $err = curl_error($ch); 
    
            curl_close($ch); 
            
            if ($err) { 
                Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> Problem attaching the file. '.$err );
                echo json_encode('error');
            }
            chmod($file, 0777);
            unlink($file);
  
        }
         echo json_encode("OK"); 
    }
}