<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use app\components\B4RController;
use app\models\User;
use app\models\UserDashboardConfig;
use app\models\Application;
use ChromePhp;

/**
 * Contains methods for tools widget in dashboard
 */
class ToolsController extends B4RController
{

    public $application;

    public function __construct ($id, $module,
        Application $application, 
        $config=[]
    )
    {
        $this->application = $application;
        parent::__construct($id, $module, $config);
    }

	/**
	 * Renders tools widget
     * @param $col text column identifier
     * @param $key integer widget identifier
     * @param $p text whether in configuration page or not
     * @param $timestamp timestamp when the tool widget was added
     * @param $layout text dashboard layout
	 */
	public function actionIndex($col, $key, string $p=null, string $timestamp=null, $layout, $program){
        $layout = (empty($layout) || !isset($layout)) ? 'two' : $layout;
        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';
        //get user id
        $userModel = new User();
        $userId = $userModel->getUserId();

        //builds favorites data
        $data = UserDashboardConfig::buildToolsData($userId,$timestamp);

        return $this->render('@app/modules/dashboard/views/widgets/tools.php',[
            'data' => $data,
            'col' => $col,
            'key'=> $key,
            'userId' => $userId,
            'p'=>$p,
            'timestamp'=>$timestamp,
            'layout'=>$layout,
            'program'=>$program
        ]);
    }

    /**
     * Reorders tools in tools widget in dashboard
     */
    public function actionReorderintoolswidget(){
        $userId = $_POST['user_id'];
        $appIds = (isset($_POST['app_ids'])) ? $_POST['app_ids'] : [];
        $timestamp = $_POST['timestamp'];
     
        UserDashboardConfig::reorderTools($userId,$appIds,$timestamp);
    }

    /**
     * Remove tool in tools widget
     */
    public function actionRemovetoolinwidget(){
        $appId = $_POST['app_id'];
        $timestamp = $_POST['timestamp'];

        UserDashboardConfig::removeToolInWidget($appId,$timestamp);
    }

    /**
     * Retieves all tool tags user has access to
     */
    public function actionGettoolstags(){
        
        //get user id
        $userId = $_POST['user_id'];
        $timestamp = $_POST['timestamp'];
        
        $filterTags = $this->application->getApplications($userId,$timestamp);

        return json_encode($filterTags);
    }

    /**
     * Add tools in tools widget
     */
    public function actionAddtoolsinwidget(){
        $timestamp = $_POST['timestamp'];
        $userId = $_POST['user_id'];
        $appId = $_POST['app_id'];

        UserDashboardConfig::addToolsInWidget($userId,$timestamp,$appId);
    }
}
