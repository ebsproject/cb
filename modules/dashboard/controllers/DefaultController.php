<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\dashboard\controllers;

use app\components\B4RController;
use app\controllers\Dashboard;
use app\models\DashboardWidget;
use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;
use app\modules\dashboard\models\DashboardModel;
use app\models\Program;
use app\models\User;
use Yii;

/**
 * Default controller for the `dashboard` module
 */
class DefaultController extends B4RController
{

    public function __construct($id, $module,
        public Dashboard $dashboard,
        public User $user,
        public UserDashboardConfig $userDashboardConfig,
        $config=[]) {

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionPrototype()
    {
        return $this->render('index_prototype');
    }

    /**
     * Renders dashboard page
     */
    public function actionIndex($program){
        //get user id
        $userId = $this->user->getUserId();
        $firstName = $this->user->getUserInfo('first_name');

    	$widgets = DashboardWidget::getWidgets($userId);
        $layout = UserDashboardConfig::getLayout($userId);

    	return $this->render('index',[
    		'widgets' => $widgets,
            'userId' => $userId,
            'layout' => $layout,
            'program' => $program,
            'firstName' => $firstName,
    	]);
    }

    /**
     * Renders dashboard configuration page
     */
    public function actionConfigure($program){
        //get user id
        $userId = $this->user->getUserId();

        $widgets = DashboardWidget::getWidgets($userId);

        if (isset($_POST['hasEditable'])) {
        // use Yii's response format to encode output as JSON
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['output'=>'', 'message'=>''];
        }

        $layout = UserDashboardConfig::getLayout($userId);

        return $this->render('configure',[
            'widgets' => $widgets,
            'userId' => $userId,
            'layout' => $layout,
            'program' => $program
        ]);
    }

    /**
     * Loads all available widgets information
     */
    public function actionLoadallwidgetsdata(){
        //get user id
        $userId = $this->user->getUserId();

        $data = DashboardWidget::getAllWidgetsData();
        $addedWidgets = UserDashboardConfig::getAddedWidgets($userId);
        
        return Yii::$app->controller->renderPartial('@app/modules/dashboard/views/default/add_widgets_modal.php',[
            'data' => $data,
            'addedWidgets' => $addedWidgets
        ]);
    }

    /**
     * Loads all favorites data
     */
    public function actionLoadallfavoritesdata($program){

        $userId = $this->user->getUserId();

        //builds favorites data
        $data = UserDashboardConfig::buildFavoritesData($userId);
        
        return Yii::$app->controller->renderPartial('@app/modules/dashboard/views/default/favorites.php',[
            'data' => $data,
            'program' => $program
        ]);
    }

    /**
     * Loads all recently used data
     */
    public function actionLoadallrecentlyuseddata($program){

        $userId = $this->user->getUserId();

        //builds favorites data
        $data = UserDashboardConfig::buildRecentlyUsedData($userId);
        
        return Yii::$app->controller->renderPartial('@app/modules/dashboard/views/default/recently_used_tools.php',[
            'data' => $data,
            'program' => $program
        ]);
    }

    /**
     * Saves adding of a widget
     */
    public function actionAddwidget(){
        $widgetId = $_POST['widget_id'];
        $userId = $_POST['user_id'];
        

        UserDashboardConfig::addWidget($widgetId,$userId);
    }

    /**
     * Saves dashboard layout
     */
    public function actionEditlayout(){
        $userId = $_POST['user_id'];
        $layout = $_POST['layout'];

        UserDashboardConfig::saveLayout($userId,$layout);
        
        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully updated layout of dashboard.<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');

        return 'success';
    }

    /**
     * Checks whether user set to disable prompt when removing widget or not
     */
    public function actionCheckifshowprompt(){
        $userId = $_POST['user_id'];

        $disablePrompt = UserDashboardConfig::checkIfShowRemoveWidgetPrompt($userId);

        return json_encode($disablePrompt);
    }

    /**
     * Removes widget in dashboard
     */
    public function actionRemovewidget(){
        $userId = $_POST['user_id'];
        $col  = $_POST['col'];
        $key  = $_POST['key'];

        UserDashboardConfig::removeWidget($userId,$col,$key);

        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully removed widget.<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');
    }

    /**
     * Disables remove widget prompt in dashboard
     */
    public function actionDisableremovewidgetprompt(){
        $userId = $_POST['user_id'];

        UserDashboardConfig::disableRemoveWidgetPrompt($userId);
    }

    /**
     * Reorders widget in dashboard
     */
    public function actionReorderwidget(){
        $userId = $_POST['user_id'];
        $col1 = (isset($_POST['col1'])) ? $_POST['col1'] : [];
        $col2 = (isset($_POST['col2'])) ? $_POST['col2'] : [];

        UserDashboardConfig::reorderWidget($userId,$col1,$col2);
    }

    /**
     * Add custom label of a widget
     */
    public function actionUpdatewidgetlabel(){
        $userId = $_POST['user_id'];
        $col = $_POST['col'];
        $key = $_POST['key'];
        $val = $_POST['val'];

        UserDashboardConfig::updateWidgetLabel($userId,$col,$key,$val);
    }

    /**
     * Minimize or expand a widget
     */
    public function actionCollapse(){
        $userId = $_POST['user_id'];
        $col = $_POST['col'];
        $key = $_POST['key'];
        $collapse = $_POST['collapse'];

        UserDashboardConfig::collapseWidget($userId,$col,$key,$collapse);
    }

    /**
     * Retrieves filter tags given attribute
     * @param $attr text attribute
     * @return $tags array list of filter tags
     */
    public function actionGetstudyattrtags($attr) {
        //retrieves filter tags given attribute
        $tags = DashboardModel::getStudyDataFilterTags($attr);

        return json_encode($tags);
    }

    /**
     * Retrieves language tags
     * @return $languagesArr array list of language tags
     */
    public function actionGetlanguagetags() {
        $languagesArr = [            
            ['id'=>'en','text'=>'English'],
            /**
            Temporarily hide other languages
            ['id'=>'zh-CN','text'=>'Chinese (Traditional)'],
            ['id'=>'tl','text'=>'Filipino'],
            ['id'=>'fi','text'=>'Finnish'],
            ['id'=>'fr','text'=>'French'],
            ['id'=>'ko','text'=>'Korean'],
            ['id'=>'es','text'=>'Spanish'],
            */
        ];

        return json_encode($languagesArr);
    }

    /**
     * Retrieves filter tags for saved filters
     * @return $tags array list of filter tags for saved filters
     */
    public function actionGetsavedfiltersurl() {
        $selected = $_POST['selected'];
        //retrieves filter tags given attribute
        $tags = DashboardModel::getSavedDashboardFilterTags($selected);

        return json_encode($tags);
    }

    /**
     * Retieves all program tags user has access to
     */
    public function actionGetprogramtags(){
        $session = Yii::$app->session;
        
        if($session->get('dashboard-program-tags') == NULL){
            //get user id
            $userId = $this->user->getUserId();
            $filterTags = $this->user->getUserPrograms($userId);

             // save program tags to session
            $session->set('dashboard-program-tags', $filterTags);
        }else{
            $filterTags = $session->get('dashboard-program-tags');
        }

        return json_encode($filterTags);
    }

    /**
     * Saves specified dashboard filters
     */
    public function actionSavefilters(){
        
        $texts = $_POST['texts'];
        //get user id
        $data = [
            'program_id' => $_POST['program_id'],
            'year' => isset($_POST['year']) ? $_POST['year'] : null,
            'season_id' => isset($_POST['season']) ? $_POST['season'] : null,
            'stage_id' => isset($_POST['stage']) ? $_POST['stage'] : null,
            'site_id' => isset($_POST['site_id']) ? $_POST['site_id'] : null,
            'owned' => isset($_POST['owned']) ? $_POST['owned'] : false,
        ];

        $curr_url_params = isset($_POST['curr_url_params']) ? $_POST['curr_url_params'] : null;

        $curr_url_params = str_replace('""}', '$"}', $curr_url_params);
        $curr_url_params = str_replace('""', '"$', $curr_url_params);
        $curr_url_params = json_decode($curr_url_params);

        $module = isset($_POST['module']) ? $_POST['module'] : null;
        $controller = isset($_POST['controller']) ? $_POST['controller'] : null;
        $action = isset($_POST['action']) ? $_POST['action'] : null;

        $userId = $this->user->getUserId();
        UserDashboardConfig::saveDashboardFilters($userId,$data);

        $searchParams = [
            'userDbId' => (string)$userId,
            'dataBrowserDbId' => 'occurrences-text-grid',
            'type' => 'filter'
        ];

        // check if configuration already exists
        $textsConfig = DataBrowserConfiguration::searchAll($searchParams);

        $valArr = [];

        if(isset($textsConfig['data'][0]) && !empty($textsConfig['data'][0])){
            $valArr = $textsConfig['data'][0]['data'];
        }

        // Ssave texts in dashboard config
        

        if(!empty($texts)){
            foreach ($texts as $v) {
                $key = key($v);
                $val = $v[key($v)];
                $valArr[$key] = ($val == 'empty') ? [] : $val;
            }
        }else{
            //remove dashboard filters
            $dashboardAttrs = ['season', 'site', 'stage', 'year'];
            foreach ($dashboardAttrs as $key) {
                $attr = $key . '-text';
                $valArr[$attr] = [];
            }
        }

        DataBrowserConfiguration::saveConfig('filter', $userId, 'occurrences-text-grid', $valArr);

        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully applied data filters.<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');

        $newProgram = Program::getProgramAttr('id',$_POST['program_id'],'abbrev');
        //reset saved URL
        Dashboard::resetSavedUrl($newProgram);

        // get current url
        $url = Yii::$app->request->referrer; 

        // get url parameters
        $url = explode('?', $url);
        $initUrl = (isset($url[0])) ? $url[0] : '';

        // if module is not empty
        if(!empty($module) && isset($url[0]) && $module != 'dashboard' && $controller != 'default'){
            $processedUrl = explode($module, $url[0]);

            // get initial page of the module
            $initialUrl = DashboardModel::getInitialPageByModule($module, $controller, $action);

            // if processed initial url is not empty
            if(!empty($initialUrl)){
                $initUrl = $processedUrl[0].$initialUrl;
            }
        }

        $otherParams = '';
        //append other url parameters
        if(!empty($curr_url_params)){
            foreach ($curr_url_params as $key => $value) {
                if(strtolower($key) !== 'program'){
                    if(is_scalar($value)){
                        $value = str_replace('$', '"', $value);

                        $otherParams .= '&'.$key.'='.$value;
                    }
                }
            }
        }

        return $initUrl .'?program='.$newProgram.$otherParams;

 
    }

    /**
     * Mark on un-mark a tool as favorite
     */
    public function actionMarkunmarkfavorites(){
        $appId = $_POST['app_id'];

        return UserDashboardConfig::markUnmarkFavorites($appId);

    }

    /**
     * Un-mark a tool as favorite
     */
    public function actionUnmarkfavorites(){
        $appId = $_POST['app_id'];

        UserDashboardConfig::unmarkFavorites($appId);
    }

    /**
     * Save to session the Yii1 URL
     *
     */
    public function actionSaveyii1url($program){

        $this->user->refreshUserSession(); //renews user's session

        $newUrl = $_POST['newUrl'];
        $linkName = $_POST['linkName'].'?program='.$program;
        $userId = $this->user->getUserId();
        
        $sessionArr = Yii::$app->session['userId-'.$userId.'-program-'.$program];
        $sessionArr[$linkName] = $newUrl;
        Yii::$app->session['userId-'.$userId.'-program-'.$program] = $sessionArr;
        return json_encode($newUrl);

    }

    public function actionResetyii1url($program){
        $newUrl = $_POST['actionLink'];
        $userId = $this->user->getUserId();
        
        $sessionArr = Yii::$app->session['userId-'.$userId.'-program-'.$program];
        $sessionArr[$newUrl] = null;
        Yii::$app->session['userId-'.$userId.'-program-'.$program] = $sessionArr;
        return json_encode('OK');
    }

    /**
     * Saves current filter
     */
    public function actionSavecurrentfilters(){
        $filterName = $_POST['filter_name'];
        $originalFilterName = isset($_POST['original_filter_name']) ? $_POST['original_filter_name'] : null;

        $filters = Yii::$app->session->get('dashboardFilters');

        $result = UserDashboardConfig::saveCurrentFilters($filterName, $filters, $originalFilterName);

        if($result == 'success'){
            Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully saved <b>'.$filterName.'</b> data filter.<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');
        }

        return json_encode($result);
    }

    /**
     * Deletes filter by name
     */
    public function actionDeletefilter(){
        $filterName = $_POST['filter_name'];

        $this->userDashboardConfig->deleteFilter($filterName);

        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully deleted <b>'.$filterName.'</b> data filter.<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');
    }

    /**
     * Apply selected data filter
     */
    public function actionApplyselectedfilter(){
        $selected = $_POST['selected'];

        $newProgram = UserDashboardConfig::applySelectedFilter($selected);

        Dashboard::resetSavedUrl($newProgram);

        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully applied <b>'.$selected.'</b> data filter.<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');

        $url = Yii::$app->request->referrer;
        $url = explode('?', $url);

        return $url[0].'?program='.$newProgram;


    }

    /**
     * Apply selected data filter GET
     */
    public function actionApplyselectedfilterget($selected){
        $newProgram = $this->userDashboardConfig->applySelectedFilter($selected);

        $this->dashboard->resetSavedUrl($newProgram);

        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully applied <b>'.$selected.'</b> data filter.<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');
        
        $url = Yii::$app->request->referrer;
        $url = explode('?', $url);

        return $this->redirect($url[0].'?program='.$newProgram);
    }

    /**
     * Preview selected filter data
     */
    public function actionPreviewselectedfilter(){
        //builds data of saved filter
        $selected = $_POST['selected'];
        $data = UserDashboardConfig::buildDashboardFilterData($selected);
        
        return Yii::$app->controller->renderPartial('@app/modules/dashboard/views/default/filter_preview.php',[
            'filters' => $data
        ]);
    }

    /**
     * Set data browser default page size
     */
    public function actionSetDefaultPageSize() {
        if(isset($_POST['newDefaultPageSize'])) {
            $newDefaultPageSize = $_POST['newDefaultPageSize'];
            $browserId = isset($_POST['browserId']) ? $_POST['browserId']: 'default';
            UserDashboardConfig::saveDefaultPageSizePreferences($newDefaultPageSize, $browserId);
        }
    }
}
