<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use app\components\B4RController;
use app\modules\dashboard\models\Study;
use app\controllers\Dashboard;
use ChromePhp;

/**
 * Contains methods for trials widget
 */
class TrialsController extends B4RController
{

    /**
    * Renders trials widget
    * @param $col text column identifier
    * @param $key integer widget identifier
    */
    public function actionIndex($col,$key,$p=null){
        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';

        $dashboard = new Dashboard();
        $filters = $dashboard->getFilters(); //get dashboard filters
        $filters = (array) $filters;
        $studyType =  ['Breeding Trial'];

        $dataProvider = Study::getStudyArrayDataProvider($filters,$studyType,$p);

        $model = new Study();
        return $this->render('@app/modules/dashboard/views/widgets/study.php', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'col' => $col,
            'key' => $key,
            'study_type' => 'trial',
            'plural_label' => 'trials',
            'p'=>$p
        ]);
    }
}