<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use ChromePhp;
use app\components\B4RController;
use app\models\User;
use app\models\UserDashboardConfig;

/**
 * Contains methods for recently used widget
 */
class RecentlyusedController extends B4RController
{

    /**
     * Renders recently used widget
     * @param $col text column identifier
     * @param $key integer widget identifier
     * @param $p whether in configuration page or not
     */
    public function actionIndex($col,$key,$p=null){
        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';

        //get user id
        $userModel = new User();
        $userId = $userModel->getUserId();

        //builds recently used data
        $data = UserDashboardConfig::buildRecentlyUsedData($userId);

        return $this->render('@app/modules/dashboard/views/widgets/recently_used.php',[
            'data' => $data,
            'col' => $col,
            'key'=> $key,
            'p' => $p
        ]);

    }
}