<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use app\components\B4RController;
use app\models\User;
use app\models\UserDashboardConfig;
use ChromePhp;

/**
 * Contains methods for favorites widget in dashboard
 */
class FavoritesController extends B4RController
{
	/**
	 * Renders favorites widget
     * @param $col text column identifier
     * @param $key integer widget identifier
     * @param $p text whether in configuration page or not
     * @param $layout text dashboard layout
	 */
	public function actionIndex($col, $key, string $p=null, $layout, $program){
        $layout = (isset($layout) && !empty($layout)) ? $layout : 'two';
        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';
        //get user id
        $userModel = new User();
        $userId = $userModel->getUserId();
        //builds favorites data
        $data = UserDashboardConfig::buildFavoritesData($userId);

        return $this->render('@app/modules/dashboard/views/widgets/favorites.php',[
            'data' => $data,
            'col' => $col,
            'key'=> $key,
            'userId' => $userId,
            'p'=>$p,
            'layout' => $layout,
            'program' => $program
        ]);
    }

    /**
     * Reorders widget in dashboard
     */
    public function actionReorderinfavoriteswidget(){
        $userId = $_POST['user_id'];
        $appIds = (isset($_POST['app_ids'])) ? $_POST['app_ids'] : [];

        UserDashboardConfig::reorderFavorites($userId,$appIds);
    }
}
