<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\dashboard\controllers;

use Yii;
use app\components\B4RController;
use app\modules\dashboard\models\Shipment;
use app\controllers\Dashboard;
use ChromePhp;

/**
 * Contains methods for shipments widget
 */
class ShipmentController extends B4RController
{

    /**
    * Renders shipments widget
    * @param $col text column identifier
    * @param $key integer widget identifier
    * @param $p text whether in configuration page or not
    */
    public function actionIndex($col,$key,$p=null){
        //use dashboard layout
        $this->layout = '@app/views/layouts/dashboard';

        $filters = Dashboard::getFilters(); //get dashboard filters
        $filters = (array) $filters;

        //get shipments data provider
        $dataProvider = Shipment::getShipmentDataProvider($filters,$p);

        return $this->render('@app/modules/dashboard/views/widgets/shipment.php', [
            'dataProvider' => $dataProvider,
            'col' => $col,
            'key' => $key,
            'p'=>$p
        ]);
    }

    /**
     * Renders view more information of a shipment
     */
    public function actionViewshipment(){
        $shipmentId = $_POST['shipment_id'];

        $data = Shipment::getShipmentDataById($shipmentId);
        $seedLots = Shipment::getSeedLotsById($shipmentId);
        $count = $seedLots['count'];

        return Yii::$app->controller->renderPartial('@app/modules/dashboard/views/widgets/shipment_info_modal.php',[
            'data' => isset($data[0]) ? $data[0] : null,
            'seedLots' => $seedLots['dataProvider'],
            'count' => $count,
            'shipmentId' => $shipmentId
        ]);
    }
}