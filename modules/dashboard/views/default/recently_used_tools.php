<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Displays recently used tools
 */
use yii\helpers\Url;
use app\models\Application;
use app\controllers\Dashboard;

echo '<div class="modal-notif"></div>';
if(empty($data)){
	echo '<p><br/>'.\Yii::t('app','There are no recently used tools yet.').'</p>';	
}else{

	foreach ($data as $k => $value) {
		$id = $value['applicationDbId'];
		$label = $value['label'];
		$actionLabel = $value['actionLabel'];
		$icon = $value['icon'];
		$timestamp = (isset($value['timestamp'])) ? $value['timestamp'] : null;

		$datetime1 = new DateTime($timestamp);
		$datetime2 = new DateTime();
		$dashboardModel = new Dashboard();
		$int = $dashboardModel->getFormattedTimeInterval($datetime1,$datetime2);
		
		$applicationModel = new Application();
		$url = $applicationModel->geturlbyappid($id,$program); //process url of application

		$item = '<div class="col col-md-4 recently-used-tool" id="recently-used-'.$id.'">

			<a class="yii1-faverecent-link" href="'. Url::to([$url]) .'">
				<div class="card-panel row">
					<div class="col col-md-3">
						<i class="material-icons metrics-icon">'.$icon.'</i>
					</div>
					<div class="col col-md-9 spec-recently-used-tool" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
						<h5 style="margin:8px 0px 5px 0px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;">'.\Yii::t('app', $label).'</h5>
						<span class="text-muted" title="'.$int.' ago">'.$int.' ago</span>
					</div>
				</div>
			</a>
		</div>	
		';
		echo $item;
	}

}
?>