<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\bootstrap\Modal;
use app\controllers\SiteController;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use kartik\select2\Select2;

$layoutObj = SiteController::getLayout(); //authenticate access token
extract($layoutObj);
$url = '/site/sample';
?>

<h3>Dashboard
<span class="pull-right">
	<span class="text-muted" style="font-size:50%">Welcome back, John!&emsp;</span>
	<a href="#" class="fixed-color dropdown-button waves-effect waves-light btn grey-text text-darken-2 btn-flat" style="padding:1px 5px;background-color: #fdfdfd !important;" data-constrainwidth="false" data-activates="dashboard-actions" data-beloworigin="true" title="More actions"><em class="material-icons widget-icons">more_vert</em></a>&nbsp;&nbsp;&nbsp;</span>
</h3>


<ul id="dashboard-actions" class="dropdown-content"  style="width: 120px !important">
<li><a href="#">Configure</a></li>
<li><a href="#">Download as PDF</a></li>
<li><a href="#">Refresh data</a></li>
</ul>


<div class="col col-md-6">

	<div class="card-panel">
	<span style="font-size: 140%">Breeding cycle</span>
		<span class="pull-right">
			<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">close</em></a>
			<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">more_vert</em></a></span>
		<div class="row">
		<p style="text-align:center;margin-bottom: -35px;margin-top:-2px;" class="breeding-cycle">
			<a href="#" style="position: absolute;height: 9%;width:12%;left:34%" title="Advancement"> &emsp;&emsp;</a>
			<a href="#" style="position: absolute;height: 9%;width:12%;left:52%" title="Parental selection"  data-target="#parental-selection-modal" data-toggle="modal"> &emsp;&emsp;</a>
			<a href="#" style="position: absolute;height: 9%;width:12%;left:25%;top:13%" title="Analysis"> &emsp;&emsp;</a>
			<a href="#" style="position: absolute;height: 9%;width:12%;left:55%;top:13%" title="Nurseries" data-target="#nurseries-modal" data-toggle="modal"> &emsp;&emsp;</a>
			<img src="<?= Yii::getAlias('@web/images/breeding_cycle.png')?>" style="width:75%;">
		</p>
		</div>
	</div>

	<div class="card-panel">
	<span style="font-size: 140%">Metrics</span>
	<span class="pull-right">
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">loop</em></a>
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">close</em></a>
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">more_vert</em></a></span>
		<div class="row">
			<br/>
			<a href="#" data-target="#studies-modal" data-toggle="modal" class=""><div class="col col-md-6">
				<div class="col col-md-3">
				<em class="material-icons <?= $themeColor ?>-text text-<?= $darken ?> metrics-icon">settings</em>
				</div>
				<div class="col col-md-9">
					<h5 style="color:#333">163</h5>
					<span class="text-muted">Operational studies</span>
				</div>
			</div></a>

		 	<a href="#" data-target="#entries-modal" data-toggle="modal" class=""><div class="col col-md-6">
				<div class="col col-md-3">
				<em class="material-icons <?= $themeColor ?>-text text-<?= $darken ?> metrics-icon">view_list</em>
				</div>
				<div class="col col-md-9">
					<h5 style="color:#333">782,406</h5>
					<span class="text-muted">Entries</span>
				</div>
			</div></a>

		  	<div class="col col-md-6">
				<div class="col col-md-3">
				<em class="material-icons <?= $themeColor ?>-text text-<?= $darken ?> metrics-icon">storage</em>
				</div>
				<div class="col col-md-9">
					<h5>134,889</h5>
					<span class="text-muted">Families</span>
				</div>
			</div>

		 	<div class="col col-md-6">
				<div class="col col-md-3">
				<em class="material-icons <?= $themeColor ?>-text text-<?= $darken ?> metrics-icon">star_half</em>
				</div>
				<div class="col col-md-9">
					<h5>1,082,229</h5>
					<span class="text-muted">Observations</span>
				</div>
			</div>
		</div>
	</div>

	<div class="card-panel">
	<span style="font-size: 140%">Tasks</span>
	<span class="pull-right">
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">loop</em></a>
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">close</em></a>
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">more_vert</em></a></span>
		<div class="row">
			<br/>
			<div class="with-padding">
				<em class="material-icons <?= $themeColor ?>-text text-<?= $darken ?>">check</em> 
				<span style="font-size:140%;line-height:2">Seeding done</span> <br/>&emsp;&emsp;&nbsp;<span class="text-muted">Test Study<span class="text-muted pull-right">Jan. 20, 2018 by John doe</span></span>
				<br/>
			</div>
			<div class="with-padding">
				<em class="material-icons <?= $themeColor ?>-text text-<?= $darken ?>">check</em> 
				<span style="font-size:140%;line-height:2">Transplanting done</span> <br/>&emsp;&emsp;&nbsp;<span class="text-muted">Foo bar Study<span class="text-muted pull-right">Jan. 29, 2018 by John doe</span></span>
			</div>
			<div class="with-padding">
				<em class="material-icons <?= $themeColor ?>-text text-<?= $darken ?>">check</em> 
				<span style="font-size:140%;line-height:2">Harvest done</span> <br/>&emsp;&emsp;&nbsp;<span class="text-muted">Demo Study<span class="text-muted pull-right">Jan. 30, 2018 by John doe</span></span>
			</div>
		</div>
	</div>

	<div class="card-panel">
		<span style="font-size: 140%">Shipments</span>
		<span class="pull-right">
			<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">loop</em></a>
			<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">close</em></a>
			<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">more_vert</em></a></span>
		<div class="row" style="margin: 0 -1.2rem -2.5rem;">
			<br/>
			
			<table class="table table-bordered white "><thead>
		<tr><th>#</th><th><span title="Trait observation variable">Status</span></th><th><span title="Data level whether entry or plot">Transaction No.</span></th><th>No. Of Seed lots</th><th><span title="Minimum value of the observation">Name</span></th><th><span title="Maximum value of the observation">Sent by</span></th><th><span title="Average value of the observation">Received by</span></th></tr>
		</thead>
		<tbody>
		<tr data-key="0"><td>1</td><td><span title="Actual Plot Yield In Grams"><span class="new badge blue">RECEIVED</span></span></td><td>A-10045</td><td>35</td><td>Name 1</td><td>John Doe</td><td>B4R User</td></tr>
		<tr data-key="1"><td>2</td><td><span class="new badge orange">DRAFT</span></td><td>A-10012</td><td>176</td><td>Name 2</td><td>B4R User</td><td>John Doe</td></tr>
		<tr data-key="2"><td>3</td><td><span title="Grain moisture content after drying">
		<span class="new badge green darken-2">DONE</span>
		</span></td><td>A-10013</td><td>176</td><td>Name 3</td><td>User</td><td>John Doe</td></tr>
		<tr data-key="3"><td>4</td><td><span title="Grain moisture factor at 14% moisture content"><span class="new badge green darken-2">DONE</span></span></td><td>A-1222</td><td>176</td><td>Name 4</td><td>B4R User</td><td>John Doe</td></tr>
		<tr data-key="4"><td>5</td><td><span title="Plot yield in kg/ha without MC"><span class="new badge orange">DRAFT</span></span></td><td>A-10015</td><td>167</td><td>Name 5</td><td>User</td><td>User</td></tr>
		</tbody></table>
		</div>
	</div>

</div>


<div class="col col-md-6">
	<div class="card-panel">
	<span style="font-size: 140%">Draft studies stats</span>
	<span class="pull-right">
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">loop</em></a>
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">close</em></a>
		<a href="#" class="text-muted fixed-color dropdown-button waves-effect waves-light" data-constrainwidth="false" data-activates="widget-actions" data-beloworigin="true" title="Actions"><em class="material-icons widget-icons">more_vert</em></a></span>
	<div class="row">
		<br/>
		
		<div class="with-padding">
			<span class="text-muted">Demo study<span class="pull-right">70%</span></span>
		 	<div class="progress">
	      	<div class="determinate" style="width: 70%"></div>
	  	</div>
	  	</div>

		<div class="with-padding">
	      	<span class="text-muted">Foo bar study<span class="pull-right">50%</span></span>
		 	<div class="progress">
	      	<div class="determinate" style="width: 50%"></div>
	  	</div>
	  	</div>

	  	<div class="with-padding">
	      	<span class="text-muted">Test study<span class="pull-right">20%</span></span>
		 	<div class="progress">
	      	<div class="determinate" style="width: 20%"></div>
	  	</div>
	  	</div>

	  	<div class="with-padding">
	      	<span class="text-muted">My study<span class="pull-right">95%</span></span>
		 	<div class="progress">
	      	<div class="determinate" style="width: 95%"></div>
	  	</div>

	  </div>
	</div>
	</div>

	<div class="card-panel">
	<span style="font-size: 140%">Favorites</span>
	<span class="pull-right">
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">loop</em></a>
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">close</em></a>
		<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">more_vert</em></a></span>
	<div class="row">
		<br/>
		
		<div class="col col-md-3">
		<a href="<?= Url::to([$url,'tool'=>'Create parent list','enableNav'=>'t']) ?>">
			<div class="card-panel <?= $themeColor . ' '. $darken ?> white-text" style="text-align:center">
			<em class="material-icons metrics-icon">subject</em>
			<br/>
			Create parent lists
			</div>
		</a>
		</div>

		<div class="col col-md-3">
		<a href="<?= Url::to([$url,'tool'=>'Create cross list','enableNav'=>'t']) ?>">
			<div class="card-panel <?= $themeColor . ' '. $darken ?> white-text" style="text-align:center">
			<em class="material-icons metrics-icon">shuffle</em>
			<br/>
			Create cross lists
			</div>
		</a>
		</div>

		<div class="col col-md-3">
		<a href="<?= Url::to([$url,'tool'=>'Design trials','enableNav'=>'t']) ?>">
			<div class="card-panel <?= $themeColor . ' '. $darken ?> white-text" style="text-align:center">
			<em class="material-icons metrics-icon">grid_on</em>
			<br/>
			Design trials
			</div>
		</a>
		</div>

		<div class="col col-md-3">
		<a href="<?= Url::to([$url,'tool'=>'Browse nurseries','enableNav'=>'t']) ?>">
			<div class="card-panel <?= $themeColor . ' '. $darken ?> white-text" style="text-align:center">
			<em class="material-icons metrics-icon">inbox</em>
			<br/>
			Browse nurseries
			</div>
		</a>
		</div>
	<br/>
		<div class="col col-md-3">
		<a href="<?= Url::to([$url,'tool'=>'Manage lists','enableNav'=>'t']) ?>">
			<div class="card-panel <?= $themeColor . ' '. $darken ?> white-text" style="text-align:center">
			<em class="material-icons metrics-icon">view_list</em>
			<br/>
			Manage lists
			</div>
		</a>
		</div>

		<div class="col col-md-3">
		<a href="<?= Url::to([$url,'tool'=>'QC Data','enableNav'=>'t']) ?>">
			<div class="card-panel <?= $themeColor . ' '. $darken ?> white-text" style="text-align:center">
			<em class="material-icons metrics-icon">content_paste</em>
			<br/>
			QC data
			</div>
		</a>
		</div>

	</div>
</div>


<div class="card-panel">

<span style="font-size: 140%">Experimental designs</span>

<span class="pull-right">
	<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">loop</em></a>
	<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">close</em></a>
	<a href="#" class="text-muted fixed-color"><em class="material-icons widget-icons">more_vert</em></a></span>
<div class="row">
	<br/>
	<?= 
	    Highcharts::widget([
	        'scripts' => [
	            'modules/exporting',
	        ],
	        'options' => [
	            'title' => null,//['text' => 'Number of entries per phase'],
	            'plotOptions' => [
	                'pie' => [
	                    'cursor' => 'pointer',
	                    'showInLegend' => true,
	                    'allowPointSelect' => true,
	                    'dataLabels' => [
			                'enabled' => false
			            ],
	                ],
	            ],
	            'series' => [
	                [
	                    'type'=>'pie',                                                             
	                    'name'=>'Studies',
	                    'colorByPoint' => true,
	                    'data' => [
	                        [
	                            'name' => 'Augmented RCBD',
	                            'y' => 17
	                        ],
	                        [
	                            'name' => 'Systematic arrangement',
	                            'y' => 83
	                        ],
	                        [
	                            'name' => 'Row-column',
	                            'y' => 19
	                        ],
	                        [
	                            'name' => 'RCBD',
	                            'y' => 11
	                        ],
	                        [
	                            'name' => 'P-REP',
	                            'y' => 51
	                        ],
	                        [
	                            'name' => 'Pedigree order',
	                            'y' => 5
	                        ],
	                    ],
	                ]
	            ],
	        ],
	    ]);
	?>
</div>
</div>

</div>
<div class="row"></div>

<?php
Modal::begin([
    'id' => 'my-modal',
]);
Modal::end();
?>

<!-- modal for an entries -->
<div id="entries-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div style="float:left"><h4>Entries </h4></div><div style="float:right;padding-right:20px;margin-top:-10px;">
		    
		<?php
		echo Select2::widget([
		    'name' => 'entries_per_phase_select',
		    'value' => ['Entries per phase'], // initial value
		    'data' => ['Entries per phase','Entries per location','Entries per year'],
		    'id' => 'entries_per_phase_select',
		    'maintainOrder' => true,
		    'toggleAllSettings' => [
		        'selectLabel' => '<i class="glyphicon glyphicon-ok-circle"></i> Select All',
		        'unselectLabel' => '<i class="glyphicon glyphicon-remove-circle"></i> Unselect All',
		        'selectOptions' => ['class' => 'text-success'],
		        'unselectOptions' => ['class' => 'text-danger'],
		    ],
		    'options' => ['placeholder' => '', 'class' => 'select2-Drop',],
		    'pluginOptions' => [
		        'tags' => true,
		        'maximumInputLength' => 10
		    ],
		]);
		?>

		</div>
		</div>
		<div class="modal-body">
		   
		<?= 
		    Highcharts::widget([
		        'scripts' => [
		            'modules/exporting',
		        ],
		        'options' => [
		            'title' => ['text' => 'Number of entries per phase'],
		            'chart' => [
		            	'backgroundColor' => null
		            ],
		            'plotOptions' => [
		                'pie' => [
		                    'cursor' => 'pointer',
		                    'showInLegend' => true,
		                    'allowPointSelect' => true,
		                    'dataLabels' => [
				                'enabled' => false
				            ],
		                ],
		            ],
		            'series' => [
		                [
		                    'type'=>'pie',                                                             
		                    'name'=>'Entries',
		                    'colorByPoint' => true,
		                    'data' => [
		                        [
		                            'name' => 'OYT',
		                            'y' => 17234
		                        ],
		                        [
		                            'name' => 'PYT',
		                            'y' => 83345
		                        ],
		                        [
		                            'name' => 'HB',
		                            'y' => 19342
		                        ],
		                        [
		                            'name' => 'F3',
		                            'y' => 11112
		                        ],
		                        [
		                            'name' => 'F1',
		                            'y' => 51856
		                        ],
		                        [
		                            'name' => 'AYT',
		                            'y' => 5976
		                        ],
		                    ],
		                ]
		            ],
		        ],
		    ]);
		?>
		</div>
		<div class="modal-footer">
		<a class="btn btn-primary waves-effect waves-light modal-close" href="#" data-dismiss="modal">Close</a>&emsp;
		</div>
    </div>
  </div>
</div> 



<!-- modal for studies -->
<div id="studies-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div style="float:left"><h4>Operational studies </h4></div><div style="float:right;padding-right:20px;margin-top:-10px;">
		    <?php
			echo Select2::widget([
			    'name' => 'studies_per_phase_select',
			    'value' => ['Studies per design'], // initial value
			    'data' => ['Studies per design','Studies per phase','Studies per location','Studies per year'],
			    'id' => 'studies_per_phase_select',
			    'maintainOrder' => true,
			    'toggleAllSettings' => [
			        'selectLabel' => '<i class="glyphicon glyphicon-ok-circle"></i> Select All',
			        'unselectLabel' => '<i class="glyphicon glyphicon-remove-circle"></i> Unselect All',
			        'selectOptions' => ['class' => 'text-success'],
			        'unselectOptions' => ['class' => 'text-danger'],
			    ],
			    'options' => ['placeholder' => '', 'class' => 'select2-Drop',],
			    'pluginOptions' => [
			        'tags' => true,
			        'maximumInputLength' => 10
			    ],
			]);
			?>

		</div>
		</div>
		<div class="modal-body">
		   
		<?= 
		    Highcharts::widget([
		        'scripts' => [
		            'modules/exporting',
		        ],
		        'options' => [
		            'title' => null,//['text' => 'Number of entries per phase'],
		            'chart' => [
		            	'backgroundColor' => null
		            ],
		            'plotOptions' => [
		                'pie' => [
		                    'cursor' => 'pointer',
		                    'showInLegend' => true,
		                    'allowPointSelect' => true,
		                    'dataLabels' => [
				                'enabled' => false
				            ],
		                ],
		            ],
		            'series' => [
		                [
		                    'type'=>'pie',                                                             
		                    'name'=>'Studies',
		                    'colorByPoint' => true,
		                    'data' => [
		                        [
		                            'name' => 'Augmented RCBD',
		                            'y' => 17
		                        ],
		                        [
		                            'name' => 'Systematic arrangement',
		                            'y' => 83
		                        ],
		                        [
		                            'name' => 'Row-column',
		                            'y' => 19
		                        ],
		                        [
		                            'name' => 'RCBD',
		                            'y' => 11
		                        ],
		                        [
		                            'name' => 'P-REP',
		                            'y' => 51
		                        ],
		                        [
		                            'name' => 'Pedigree order',
		                            'y' => 5
		                        ],
		                    ],
		                ]
		            ],
		        ],
		    ]);
		?>

		</div>
		<div class="modal-footer">
		<a class="btn btn-primary waves-effect waves-light modal-close" href="#" data-dismiss="modal">Close</a>&emsp;
		</div>
    </div>
  </div>
</div> 