<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders add more widgets modal
 *
 */
use app\controllers\SiteController;
use yii\helpers\Url;
use yii\helpers\Html;

$layoutObj = SiteController::getLayout();
extract($layoutObj);
$addTitle = 'Click to add widget';
$inArr = false; //whether a widget was already added or not
$addedLabel = ''; //label whether added or not

echo '<div id="add-modal-top"></div><div class="modal-notif"></div>';

foreach ($data as $key => $value) {
	$label = (isset($value['label'])) ? $value['label'] : 'Not set';
	$description = (isset($value['description'])) ? $value['description'] : 'Not set';
	$icon = (isset($value['icon'])) ? $value['icon'] : 'settings';

	$inArr = in_array($value['widgetDbId'], $addedWidgets);

	$addedLabel = ($inArr) ? '<span class="badge new" style="float: none;text-transform: uppercase">'.\Yii::t('app','Added').'</span> ' : '';
?>
<div class="card-panel spec-add-widgets" id="spec-widget-<?= $value['widgetDbId'] ?>">
	<div class="row spec-widget" style="margin-bottom:0px">
			<div class="col col-md-1">
				<em class="material-icons <?= $themeColor ?>-text text-<?= $darken ?> metrics-icon"><?= $icon ?></em>
			</div>
			<div class="col col-md-9">
				<h5 style="color:#333"><?= $addedLabel . \Yii::t('app', $label); ?></h5>
				<span class="text-muted">
					<?= \Yii::t('app', $description); ?>
				</span>
			</div>
			<div class="col col-md-1">
				<?= Html::a(\Yii::t('app','Add'), '#add-modal-top', [
					'class'=>'btn btn-primary waves-effect waves-light add-widget',
					'id'=> $value['widgetDbId'],
					'label'=> $value['label'],
					'style'=>'margin-top:10px;',
					'title'=>\Yii::t('app', $addTitle)
				]); ?>
			</div>
	</div>
</div>

<?php
}
?>