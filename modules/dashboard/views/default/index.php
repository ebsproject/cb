<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders dashboard page
 */
use marekpetras\yii2ajaxboxwidget\Box;
use app\models\UserDashboardConfig;
use yii\helpers\Url;
use yii\bootstrap\Modal;

$session = Yii::$app->session;
$welcomeMessage = !empty($firstName) ? \Yii::t('app', 'Welcome back, ')."$firstName!" : '';

$this->registerJsFile('https://code.highcharts.com/highcharts.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://code.highcharts.com/modules/sunburst.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,visualization&key=AIzaSyAKUE_cXkmZjA5QQvA2QUlBNrw2MhmWGiU&callback=Function.prototype',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://www.gstatic.com/charts/loader.js',['position' => \yii\web\View::POS_END]);
?>
<h3><?= \Yii::t('app', 'Dashboard') ?>
<span class="pull-right">
	<span class="text-muted hide-in-mobile" style="font-size:50%"><?= $welcomeMessage ?>&nbsp;</span>
	<a href="<?= Url::to(['default/configure','program'=>$program]);?>" id="configure-dashboard-btn" class="waves-effect waves-light btn grey-text text-darken-2 btn-flat tooltipped" style="padding:1px 10px;margin-right:-3px;background-color: #fdfdfd !important;" data-tooltip="<?= \Yii::t('app', 'Configure dashboard') ?> (<?= \Yii::t('app', 'Alt') ?>+C)"><em class="material-icons widget-icons">settings</em></a>
	
	<a href="#!" class="waves-effect waves-light btn grey-text text-darken-2 btn-flat tooltipped" onclick="location.reload()" style="padding:1px 10px;margin-right:15px;background-color: #fdfdfd !important;" data-tooltip="<?= \Yii::t('app', 'Refresh') ?>"><em class="material-icons widget-icons">refresh</em></a>

	<!-- <a href="#" class="fixed-color dropdown-button waves-effect waves-light btn grey-text text-darken-2 btn-flat" style="padding:1px 5px;background-color: #fdfdfd !important;" data-constrainwidth="false" data-activates="dashboard-actions" data-beloworigin="true" title="More actions"><i class="material-icons widget-icons">more_vert</i></a>&nbsp;&nbsp;&nbsp;</span> -->
</h3>

<ul id="dashboard-actions" class="dropdown-content" style="width: 120px !important">
<li><a href="<?= Url::to(['default/configure']);?>"><?= \Yii::t('app', 'Configure') ?></a></li>
<li><a href="#!" onclick="location.reload()"><?= \Yii::t('app', 'Refresh') ?></a></li>
<li><a href="#!"><?= \Yii::t('app', 'Download') ?></a></li>
</ul>

<?php
Modal::begin([
    'id' => 'generic-modal',
]);
Modal::end();

$col1 = '';
$col2 = '';
//render dashboard widgets
foreach ($widgets as $k => $value) {
	foreach ($value as $key => $value) {
		//set icon if minimized or not
		$collapseIcon = (isset($value['is_minimized'])) ? 'plus' : 'minus';

		//check whether have timestamp
		$timestamp = (isset($value['timestamp'])) ? str_replace(' ', '+', $value['timestamp']) : '';
		$procTimestamp = (isset($value['timestamp'])) ? str_replace(' ', '_', $value['timestamp']) : '';
		$procTimestamp = (isset($value['timestamp'])) ? str_replace(':', '_', $procTimestamp) : '';

		$toolsButtons = [
			'others' => function() use ($key, $value){ //if there are other buttons defined in the widget
				if(isset($value['other_buttons']) && !empty($value['other_buttons'])){
					return $value['other_buttons'];
				}
			},
			'reload' => function() use ($key, $k, $procTimestamp){
				return '
					&nbsp;&nbsp;<a href="#!" title="'. \Yii::t('app', 'Refresh data').'" class="text-muted fixed-color" id="refresh-widget-'.$procTimestamp.'" onclick="$(&quot;#dashboard-widget-'.$k.'-'.$key.'&quot;).box(&quot;reload&quot;);"><i class="material-icons widget-icons">loop</i></a>
					';
			},
			'collapse' => function() use ($key, $collapseIcon, $k){
				return '
					&nbsp;<a href="#!" id="collapse-'.$k.'-'.$key.'" class="text-muted fixed-color collapse-widget" title="'. \Yii::t('app', 'Collapse').'" data-widget="collapse"><i class="fa fa-'.$collapseIcon.'" style="vertical-align: 30%;font: normal normal normal 12px/1 FontAwesome;"></i></a>
					';
			}
		];

		//initialize default classes
		$defaultClasses = ['box', 'box-flat', 'box-init', $value['abbrev']];

		//if widget is minimized
		$classes = (isset($value['is_minimized'])) ?  array_merge($defaultClasses,['collapsed-box']) : $defaultClasses;
		
		//check whether have custom label
		$label = (isset($value['custom_label'])) ? $value['custom_label'] : $value['label'];

		$info = ($value['abbrev'] == 'FAVORITES') ? ' <i class="text-muted fa fa-question-circle-o tiny" title="'.\Yii::t('app','To favorite an app, go to the application and click the star icon').'."></i>' : '';
		$options = [
			'id'=>'dashboard-widget-'.$k.'-'.$key,
			'title'=>'<span title="'. \Yii::t('app', $value['description']).'"><i class="material-icons" style="vertical-align:bottom;font-size:110%">'.$value['icon'].'</i> '. \Yii::t('app', $label).$info.' </span>',
			'bodyLoad'=> ['/dashboard/'.$value['name'].'?col='.$k.'&key='.$key.'&timestamp='.$timestamp.'&layout='.$layout.'&program='.$program],
			'toolsTemplate' => '{others} {collapse} {reload}',
			'toolsButtons' => $toolsButtons,
			'classes' => $classes,
		];

		if($k == "col_1"){
			$col1 .= Box::widget($options);
		}else{
			$col2 .= Box::widget($options);
		}
	}

}
if($layout == 'two'){ //two columns
	$col1Class = '6';
	$col2Class = '6';
}else if($layout == 'one'){ //one column
	$col1Class = '12';
	$col2Class = '12';
}else if($layout == 'big-left'){ //bigger left
	$col1Class = '8';
	$col2Class = '4';
}else if($layout == 'big-right'){ //right left
	$col1Class = '4';
	$col2Class = '8';
}
echo '<div class="col col-md-'.$col1Class.'">'.$col1.'</div>';
echo '<div class="col col-md-'.$col2Class.'">'.$col2.'</div>';

if(empty($widgets['col_2']) && empty($widgets['col_1'])){
	echo \Yii::t('app', 'You have not added any widgets in your dashboard. Go to the configuration page to add widgets.') .'<br/><br/>';
}

echo '<div class="row"></div>';
?>

<style>
.box {
	position: relative;
	background: #ffffff;
	-webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2); 
	box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2); 
	border-top:none;
	margin: .5rem 0 1rem 0;
	border-radius: 2px;
	width: 100%;
}
.box-header.with-border {
    border-bottom: none; 
}
.box-header > .box-tools {
    top: 10px;
}
.box-header {
    padding: 15px;
}
.box-body {
    padding: 0px 15px 10px 15px;
    max-height: 800px;
    overflow-y: auto;
    overflow-x: hidden;
}
.panel-default {
    border-color: transparent;
}
.panel {
    margin-bottom: -10px;
    background-color: transparent;
    border: 1px solid transparent;
}
.panel-default > .panel-heading {
    margin-bottom: -15px;
    color: #333;
    background-color: transparent;
    border-color: transparent;
}
.panel-footer {
    padding: 10px 0px;
    background-color: transparent;
}
.table {
    margin-bottom: 8px;
}
.summary{
	margin-top: -5px;
}
</style>

<?php
$firstLogin = 'false';
//if first time to view dashboard page
if($session->get('first_login') == null){

	$firstLogin = 'true';

	//check if user dashboard config is empty, meaning it's the first time user visits dashboard
	$userConfig = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

	if(!empty($userConfig)){
		$firstLogin = 'false';
	}
	//set not first time login
	$session->set('first_login','false');
}


$collapseUrl = Url::to(['/dashboard/default/collapse']);

$this->registerJs(<<<JS
//display welcome message to first time users
$(document).ready(function() {
	var first_login = '$firstLogin';
	if(first_login == 'true'){
		$('#welcome-modal-btn').click();
	}
});

//hide welcome modal
$(document).on('click', '.welcome-modal-close', function(e) {
	$('#welcome-modal').modal('hide');
});

//collapse a widget
$(document).on('click','.collapse-widget', function(e){
	var obj = $(this);
	var id = obj.attr('id');

	var parentDivId = obj.closest('div.box').attr('id');

	var idStrArr = id.split("-");

	var col = idStrArr[1];
	var key = idStrArr[2];

	var collapse = true;
	if($("#"+parentDivId).hasClass("collapsed-box")){
		collapse = false;
	}

	//check if user will be prompted
	$.ajax({
        url: '$collapseUrl',
        type: 'POST',
        async: false,
        dataType: 'json',
        data: {
        	user_id: $userId,
        	col: col,
        	key: key,
        	collapse: collapse
        },
        success: function(data) {
        	if(data === true){
        		disablePrompt = true;
        	}
        },
        error: function(){
        }
	});
});
JS
);