<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders preview of selected filter
 */
use app\models\Program;
use app\models\Season;
$filters = (array) $filters;
extract($filters);
?>

<table class="table-dashboard-filters">
    <?php
        //displays current filters
        if(isset($program_id) && !empty($program_id)){
            echo '<tr><th class="filter-header">Program</th><td>'.
                '<span class="new badge green darken-2">'.Program::getProgramNameById($program_id).'</span>'
            .'</td></tr>';
        }
        if(isset($year) && !empty($year)){
            echo '<tr><th class="filter-header">Year</th><td>';
            foreach ($year as $key => $value) {
                echo '<span class="new badge cyan darken-1">'.$value.'</span>';
            }
            echo '</td></tr>';
        }
        if(isset($season_id) && !empty($season_id)){
            echo '<tr><th class="filter-header">Season</th><td>';
            foreach ($season_id as $key => $value) {
                echo '<span class="new badge indigo darken-1">'.Season::getRecordByIdAttribute($value,'abbrev','season').'</span>';
            }
            echo '</td></tr>';
        }
        if(isset($phase_id) && !empty($phase_id)){
            echo '<tr><th class="filter-header">Phase</th><td>';
            foreach ($phase_id as $key => $value) {
                echo '<span class="new badge deep-purple">'.Season::getRecordByIdAttribute($value,'abbrev','phase').'</span>';
            }
            echo '</td></tr>';
        }
        if(isset($place_id) && !empty($place_id)){
            echo '<tr><th class="filter-header">Location</th><td>';
            foreach ($place_id as $key => $value) {
                echo '<span class="new badge light-blue darken-3">'.Season::getRecordByIdAttribute($value,null,'place').'</span>';
            }
            echo '</td></tr>';
        }
    ?>
</table>