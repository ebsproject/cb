<?php 
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Contains sample list of tools per act
 */
use yii\helpers\Url;

$url = '/site/sample'; 
?>
<div class="col col-md-4 dashboard-tool" id="create-parent-list">
<a href="<?= Url::to([$url,'tool'=>'Create parent list','enableNav'=>'t']) ?>">
	<div class="card-panel row">
		<div class="col col-md-3">
		<em class="material-icons  metrics-icon">subject</em>
		</div>
		<div class="col col-md-9 spec-dashboard-tool">
			<h5>Create parent list</h5>
			<span class="text-muted">Create new parent list</span>
		</div>
	</div>
</a>
</div>

<div class="col col-md-4 dashboard-tool" id="browse-parent-list">
<a href="<?= Url::to([$url,'tool'=>'Parent lists','enableNav'=>'t']) ?>">
	<div class="card-panel row">
		<div class="col col-md-3">
		<em class="material-icons  metrics-icon">subject</em>
		</div>
		<div class="col col-md-9 spec-dashboard-tool">
			<h5>Browse parent lists</h5>
			<span class="text-muted">Browse all parent lists</span>
		</div>
	</div>
</a>
</div>

<div class="col col-md-4 dashboard-tool" id="create-cross-list">
<a href="<?= Url::to([$url,'tool'=>'Create cross list','enableNav'=>'t']) ?>">
	<div class="card-panel row">
		<div class="col col-md-3">
		<em class="material-icons  metrics-icon">shuffle</em>
		</div>
		<div class="col col-md-9 spec-dashboard-tool">
			<h5>Create cross list</h5>
			<span class="text-muted">Create new cross list</span>
		</div>
	</div>
</a>
</div>

<div class="col col-md-4 dashboard-tool" id="browse-cross-list">
<a href="<?= Url::to([$url,'tool'=>'Browse parent list','enableNav'=>'t']) ?>">
	<div class="card-panel row">
		<div class="col col-md-3">
		<em class="material-icons  metrics-icon">shuffle</em>
		</div>
		<div class="col col-md-9 spec-dashboard-tool">
			<h5>Browse cross lists</h5>
			<span class="text-muted">Browse all cross lists</span>
		</div>
	</div>
</a>
</div>

<div class="col col-md-4 dashboard-tool" id="create-entry-list">
<a href="<?= Url::to([$url,'tool'=>'Create entry list','enableNav'=>'t']) ?>">
	<div class="card-panel row">
		<div class="col col-md-3">
		<em class="material-icons  metrics-icon">format_list_bulleted</em>
		</div>
		<div class="col col-md-9 spec-dashboard-tool">
			<h5>Create entry list</h5>
			<span class="text-muted">Create new entry list</span>
		</div>
	</div>
</a>
</div>

<div class="col col-md-4 dashboard-tool" id="browse-entry-list">
<a href="<?= Url::to([$url,'tool'=>'Browse list','enableNav'=>'t']) ?>">
	<div class="card-panel row">
		<div class="col col-md-3">
		<em class="material-icons  metrics-icon">format_list_bulleted</em>
		</div>
		<div class="col col-md-9 spec-dashboard-tool">
			<h5>Browse entry lists</h5>
			<span class="text-muted">Browse entry lists</span>
		</div>
	</div>
</a>
</div>
