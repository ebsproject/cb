<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders dashboard configuration page
 */
use app\components\DashboardWidget;
use marekpetras\yii2ajaxboxwidget\Box;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use kartik\sortable\Sortable;
use kartik\editable\Editable;
use app\components\FavoritesWidget;


$this->registerJsFile('https://code.highcharts.com/highcharts.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://code.highcharts.com/modules/sunburst.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,visualization&key=AIzaSyAKUE_cXkmZjA5QQvA2QUlBNrw2MhmWGiU&callback=Function.prototype',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://www.gstatic.com/charts/loader.js',['position' => \yii\web\View::POS_END]);
$p = Yii::$app->controller->action->id;
?>

<h3 style="margin: 1rem 0 1.1rem 0px;"><?= \Yii::t('app', 'Configure Dashboard') ?>
<?= 
  FavoritesWidget::widget([
    'module' => 'dashboard',
    'controller' => 'default',
    'action' => 'configure'
  ]) 
?>
<span class="pull-right">
	<a href="#" data-backdrop="static" data-keyboard="false" data-target="#add-widgets-modal" data-toggle="modal" class="tooltipped add-more-widgets-modal waves-effect waves-light btn grey-text text-darken-2 btn-flat" style="background-color: #fdfdfd !important;" data-constrainwidth="false" data-activates="dashboard-actions" data-tooltip="<?= \Yii::t('app', 'Add widget') ?>"><?= \Yii::t('app', 'Add widget') ?></a>
	<a href="#" data-target="#edit-layout-modal" data-toggle="modal" class="tooltipped edit-layout-modal waves-effect waves-light btn grey-text text-darken-2 btn-flat" style="background-color: #fdfdfd !important;" data-constrainwidth="false" data-activates="dashboard-actions" data-tooltip="<?= \Yii::t('app', 'Edit layout') ?>"><span id="edit-layout-btn"><?= \Yii::t('app', 'Edit layout') ?></span></a>
	
	<a href="<?= Url::to(['/dashboard','program'=>$program]);?>" class="tooltipped waves-effect waves-light btn grey-text text-darken-2 btn-flat" style="background-color: #fdfdfd !important;" data-tooltip="<?= \Yii::t('app', 'Go back to dashboard') ?>"><?= \Yii::t('app', 'Go to Dashboard') ?></a>&nbsp;&nbsp;&nbsp;</span>
</h3>

<p>Changes made are <strong>automatically saved</strong>.</p>
<!-- add widgets modal -->
<div id="add-widgets-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close close-add-widgets" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div style="float:left"><h4><?= \Yii::t('app','Add widgets') ?></h4></div><div style="float:right;">
		    <input placeholder="<?= \Yii::t('app','Filter') ?>" id="filter-add-widgets" style="margin-top:-10px;"></input>
		</div>
		</div>
		<div class="modal-body add-widgets-modal-body">
		</div>
		<div class="modal-footer" style="padding: 18px 16px;">
		<a class="modal-close close-add-widgets" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Close') ?></a>&emsp;
		</div>
    </div>
  </div>
</div> 

<!-- edit layout modal -->
<div id="edit-layout-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div style="float:left"><h4><?= \Yii::t('app','Edit layout') ?></h4></div><div style="float:right;">
		</div>
		</div>
		<div class="modal-body edit-layout-modal-body">
			<?= \Yii::t('app', 'Choose dashboard layout.') ?>
			<div style="margin: 15px 0px 15px 20px">
				<a href="#" class="choose-layout" data-layout="one">
					<div class="col z-depth-2 white pull-left" style="width:23%;margin-right: 10px;padding-right: 0;" id="dashboard-layout-one">
						<div>
							<div class="grey lighten-1" style="height: 100px; margin: 7px;">

							</div>
						</div>
					</div>
				</a>

				<a href="#" class="choose-layout" data-layout="two">
					<div class="col col-md-3 z-depth-2 white pull-left" style="width:23%;margin-right: 10px;padding-right: 0;" id="dashboard-layout-two">
						<div>
							<div class="grey lighten-1 pull-left" style="height: 100px;margin: 7px; width: 44%">
							</div>
							<div class="grey lighten-1 pull-right" style="height: 100px;margin: 7px 7px 7px 0px;width:44%">
							</div>
						</div>
					</div>
				</a>

				<a href="#" class="choose-layout" data-layout="big-right">
					<div class="col col-md-3 z-depth-2 white pull-left" style="width:23%;margin-right: 10px;padding-right: 0;" id="dashboard-layout-big-right">
						<div>
							<div class="grey lighten-1 pull-left" style="height: 100px;margin: 7px; width: 28%">
							</div>
							<div class="grey lighten-1 pull-right" style="height: 100px;margin: 7px 7px 7px 0px;width:60%">
							</div>
						</div>
					</div>
				</a>

				<a href="#" class="choose-layout" data-layout="big-left">
					<div class="col col-md-3 z-depth-2 white pull-left" style="width:23%;margin-right: 10px;padding-right: 0;" id="dashboard-big-left">
						<div>
							<div class="grey lighten-1 pull-left" style="height: 100px;margin: 7px; width: 60%">
							</div>
							<div class="grey lighten-1 pull-right" style="height: 100px;margin: 7px 7px 7px 0px;width:28%">
							</div>
						</div>
					</div>
				</a>

			<div class="clearfix"></div>
			</div>
		</div>
		<div class="modal-footer" style="padding: 18px 16px;">
		<a class="modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Close') ?></a>&emsp;
		</div>
    </div>
  </div>
</div> 

<?php
    Modal::begin([
        'id' => 'remove-widget-modal',
        'header' => '<h4>Remove widget</h4>',
        'footer' => '<div class="pull-left" style="margin: 10px 0px 10px 15px;"><input type="checkbox" id="dont-ask-me-again" name="Form[dont_ask_again]" style="opacity:0"/>
            <label for="dont-ask-me-again">'.\Yii::t('app',"Don't ask me again.").'</label></div>'.Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;</label>&emsp;&nbsp;'.
            Html::a(\Yii::t('app','Confirm'),
                '#',
                [
                    'class'=>'btn btn-primary waves-effect waves-light save-remove-btn',
                    'url'=>'#',
                ]).'&emsp;',
        'options' => ['data-backdrop'=>'static']
    ]);

	echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to remove <b><span class="widget-label"></span></b> widget. Click confirm to proceed.';    

    Modal::end();
?>

<?php
$col1 = '';
$col2 = '';

Pjax::begin(['id'=>'dashboard-form']);
echo Html::a("Refresh", '#', ['class' => 'btn refresh-dashboard-btn hidden']);

$itemsCol1 = [];
$itemsCol2 = [];
//render dashboard widgets
foreach ($widgets as $k => $value) {

	foreach ($value as $key => $value) {
		//set icon if minimized or not
		$collapseIcon = (isset($value['is_minimized'])) ? 'plus' : 'minus';

		//initialize default classes
		$defaultClasses = ['box', 'box-flat', 'box-init'];

		//if widget is minimized
		$classes = (isset($value['is_minimized'])) ?  array_merge($defaultClasses,['collapsed-box']) : $defaultClasses;
		
		//check whether have custom label
		$label = (isset($value['custom_label'])) ? $value['custom_label'] : $value['label'];

		//check whether have timestamp
		$timestamp = (isset($value['timestamp'])) ? str_replace(' ', '+', $value['timestamp']) : '';

		$toolsButtons = [
			'collapse' => function() use ($key, $collapseIcon, $k){
				return '
					&nbsp;<a href="#!" id="collapse-'.$k.'-'.$key.'" class="text-muted fixed-color collapse-widget" title="'.\Yii::t('app', 'Collapse') .'" data-widget="collapse"><i class="fa fa-'.$collapseIcon.'" style="vertical-align: 30%;font: normal normal normal 12px/1 FontAwesome;"></i></a>
					';
			},
			'remove' => function() use ($key, $label, $k){
				return '
					&nbsp;<a href="#!" title="'.\Yii::t('app', 'Remove widget').'" class="text-muted fixed-color remove-widget" data-label="'.$label.'" data-target="remove-widget-modal" id="'.$k.'-'.$key.'"><i class="material-icons widget-icons">close</i></a>
					';
			}
		];

		$options = [
			'id'=>'dashboard-widget-'.$k.'-'.$key,
			'title'=>'<i class="material-icons" style="vertical-align:bottom;font-size:115%">'.$value['icon'].'</i> <span title="'.\Yii::t('app','Click to edit label').'">'.Editable::widget([
			    'name'=>'custom_label',
			    'id' => 'widget_label-'.$k.'-'.$key,
			    'asPopover' => true,
			    'pluginEvents' => [
			    	"editableSubmit"=>"function(event, val, form, jqXHR, data) {
			    		var id = $(this).attr('id');
			    		var idArr = id.split('-');

			    		var col = idArr[1];
			    		var key = idArr[2];

			    		//update custom label
			    		$.ajax({
					        url: 'updatewidgetlabel',
					        type: 'POST',
					        async: true,
					        data: {
					        	col: col,
					        	key: key,
					        	val: val,
					        	user_id: $userId
					        },
					        success: function(data) {
					        },
					        error: function(){
					            console.log('There was a problem while saving.'); 
					        }
						}); 
			    	}",
			    ],
			    'value' => $label,
			    'header' => 'widget label',
			    'size'=>'md',
			    'options' => ['placeholder'=>\Yii::t('app','Enter widget custom label')]
			]).'</span>',
			'bodyLoad'=> ['/dashboard/'.$value['name'].'?col='.$k.'&key='.$key.'&p='.$p.'&timestamp='.$timestamp.'&layout='.$layout.'&program='.$program],
			'toolsTemplate' => '{collapse} {remove}',
			'toolsButtons' => $toolsButtons,
			'classes' => $classes
		];

		$itemOptions = ['id'=>$value['widgetDbId'].'-'.$key.'-'.$k];
		if(isset($value['is_minimized'])){
			$itemOptions = array_merge($itemOptions,['data-is_minimized'=>'true']);
		}
		if(isset($value['custom_label'])){
			$itemOptions = array_merge($itemOptions,['data-custom_label'=>$value['custom_label']]);
		}
		if(isset($value['timestamp'])){
			$itemOptions = array_merge($itemOptions,['data-timestamp'=>$value['timestamp']]);
		}

		if($k == "col_1"){
			$itemsCol1[] = [
				'content'=>Box::widget($options),
				'options'=> $itemOptions
			];
		}else{
			$itemsCol2[] = [
				'content'=>Box::widget($options),
				'options'=> $itemOptions
			];
		}
	}
}

$pluginEvents = [
    'sortupdate' => "function() { 
    	var col1Arr = [];
		var col2Arr = [];
		$('#widgets-col_1').find('li').each(function(e){
			var obj = $(this);
			var idStr = obj.attr('id');

			if(idStr !== undefined){
				var idStr = idStr.split('-');
				var id = idStr[0];

				var custom_label = obj.data('custom_label');
				var is_minimized = obj.data('is_minimized');
				var timestamp = obj.data('timestamp');

				var data = {widget_id:id,custom_label:custom_label,is_minimized:is_minimized,timestamp:timestamp};
				col1Arr.push(data);
			}
		});
		$('#widgets-col_2').find('li').each(function(e){
			var obj = $(this);
			var idStr = obj.attr('id');

			if(idStr !== undefined){
				var idStr = idStr.split('-');
				var id = idStr[0];

				var custom_label = obj.data('custom_label');
				var is_minimized = obj.data('is_minimized');
				var timestamp = obj.data('timestamp');

				var data = {widget_id:id,custom_label:custom_label,is_minimized:is_minimized,timestamp:timestamp};
				col2Arr.push(data);
			}
		});

		$.ajax({
	        url: 'reorderwidget',
	        type: 'POST',
	        async: true,
	        data: {
	        	col1: col1Arr,
	        	col2: col2Arr,
	        	user_id: $userId
	        },
	        success: function(data) {
	        	$('.widgets').css('display','');
	        },
	        error: function(){
	            console.log('There was a problem while loading content.'); 
	        }
		}); 
    }",
];

$col1 = Sortable::widget([ 
    'connected'=>'connected', 
    'id'=>'widgets-col_1',
    'itemOptions' => ['class'=>'widgets','title'=>\Yii::t('app', 'Drag and drop to reorder widgets.')],
    'pluginEvents' => $pluginEvents,
    'items'=>$itemsCol1
]); 

$col2 = Sortable::widget([ 
    'connected'=>'connected', 
    'id'=>'widgets-col_2',
    'itemOptions' => ['class'=>'widgets'],
    'pluginEvents' => $pluginEvents,
    'items'=>$itemsCol2
]); 
if($layout == 'two'){ //two columns
	$col1Class = '6';
	$col2Class = '6';
}else if($layout == 'one'){ //one column
	$col1Class = '12';
	$col2Class = '12';
}else if($layout == 'big-left'){ //bigger left
	$col1Class = '8';
	$col2Class = '4';
}else if($layout == 'big-right'){ //right left
	$col1Class = '4';
	$col2Class = '8';
}
echo '<div class="col col-md-'.$col1Class.'">'.$col1.'</div>';
echo '<div class="col col-md-'.$col2Class.'">'.$col2.'</div>';

Pjax::end();
?>
<div class="row">
</div>
<style>
.box {
	position: relative;
	background: #ffffff;
	-webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2); 
	box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2); 
	border-top:none;
	margin: 0;
	border-radius: 2px;
	width: 100%;
}
.box-header.with-border {
    border-bottom: none; 
}
.box-header > .box-tools {
    top: 10px;
}
.box-header {
    padding: 15px;
}
.box-body {
    padding: 0px 15px 10px 15px;
    max-height: 600px;
    overflow-y: auto;
    overflow-x: hidden;
}
.kv-editable-link {
    color: #444;
}
.pagination > li {
	border: none; 
	margin: 0px; 
	padding: 0px;
}
.panel-default {
    border-color: transparent;
}
.panel {
    margin-bottom: -10px;
    background-color: transparent;
    border: 1px solid transparent;
}
.panel-default > .panel-heading {
    margin-bottom: -15px;
    color: #333;
    background-color: transparent;
    border-color: transparent;
}
.panel-footer {
    padding: 10px 0px;
    background-color: transparent;
}
.table {
    margin-bottom: 8px;
}
.summary{
	margin-top: -5px;
}
</style>

<?php
$loadAllWidgetsUrl = Url::to(['/dashboard/default/loadallwidgetsdata']);
$checkIfShowPromptUrl = Url::to(['/dashboard/default/checkifshowprompt']);
$collapseUrl = Url::to(['/dashboard/default/collapse']);
$disableRemovePromptUrl = Url::to(['/dashboard/default/disableremovewidgetprompt']);
$removeWidgetUrl = Url::to(['/dashboard/default/removewidget']);
$addWidgetUrl = Url::to(['/dashboard/default/addwidget']);
$editLayoutUrl = Url::to(['/dashboard/default/editlayout']);

$this->registerJs(<<<JS
//whether to disable prompt or not
var disablePrompt = false;
var objR = '';

//remove a widget
$(document).on('click','.remove-widget', function(e){
	objR = $(this);
	var label = objR.data('label');
	idStr = objR.attr('id');

	$('.widget-label').html(label);

	//check if user will be prompted
	$.ajax({
        url: '$checkIfShowPromptUrl',
        type: 'POST',
        async: false,
        dataType: 'json',
        data: {
        	user_id: $userId
        },
        success: function(data) {
        	if(data === true){
        		disablePrompt = true;
        	}
        },
        error: function(){
        }
	});

	var liId = objR.closest('li').attr('id');
	var ulId = objR.closest('ul').attr('id');

	var listItem = $( "#"+liId );
	var key = $("ul#"+ulId+" > li").index(listItem);

	var str = ulId.split("-");
	var col = str[1]; //column

	//whether to show prompt or not
	if(disablePrompt){
		removeWidget(col, key);
	}else{
		$('.save-remove-btn').attr('id', idStr);
		$('#remove-widget-modal').modal('show');
	}
});

//collapse a widget
$(document).on('click','.collapse-widget', function(e){
	var obj = $(this);
	var id = obj.attr('id');

	var parentDivId = obj.closest('div.box').attr('id');

	var idStrArr = id.split("-");

	var col = idStrArr[1];
	var key = idStrArr[2];

	var collapse = true;
	if($("#"+parentDivId).hasClass("collapsed-box")){
		collapse = false;
	}

	//check if user will be prompted
	$.ajax({
        url: '$collapseUrl',
        type: 'POST',
        async: false,
        dataType: 'json',
        data: {
        	user_id: $userId,
        	col: col,
        	key: key,
        	collapse: collapse
        },
        success: function(data) {
        	if(data === true){
        		disablePrompt = true;
        	}
        },
        error: function(){
        }
	});
});

//confirm remove widget
$(document).on('click','.save-remove-btn', function(e){
	var obj = $(this);

	var thisIdStr = obj.attr('id');
	var str = thisIdStr.split("-");
	var col = str[0]; //column
	var key =  str[1]; // key

	var liId = objR.closest('li').attr('id');
	var ulId = objR.closest('ul').attr('id');

	var listItem = $( "#"+liId );
	var k = $("ul#"+ulId+" > li").index(listItem);

	var s = ulId.split("-");
	var c = s[1]; //column

	if($('#dont-ask-me-again').prop('checked')){
		//if checked, saved in user config
		$.ajax({
	        url: '$disableRemovePromptUrl',
	        type: 'POST',
	        async: false,
	        dataType: 'json',
	        data: {
	        	user_id: $userId
	        },
	        success: function(data) {
	        },
	        error: function(){
	        }
		});
	}
	removeWidget(c, k);
	$('#remove-widget-modal').modal('hide');
	$('#dashboard-widget-'+col+'-'+key).box('hide');

});

//remove widget
function removeWidget(col, key){
	$.ajax({
		url: '$removeWidgetUrl',
		type: 'POST',
		async: true,
		data: {
			user_id: $userId,
			col: col,
			key: key
		},
		success: function(data) {
			location.reload();
		},
		error: function(){
			console.log('Problem while removing widget');
		}
	});
}

//edit layout
$(document).on('click','.choose-layout', function(e){
	obj = $(this);
	var layout = obj.data('layout');

	//check if user will be prompted
	$.ajax({
        url: '$editLayoutUrl',
        type: 'POST',
        async: true,
        data: {
        	user_id: $userId,
        	layout: layout
        },
        success: function(data) {
        	location.reload();
        	$('#edit-layout-modal').modal('hide');
        },
        error: function(){
        }
	});

});

//load add widgets modal
$(document).on('click', '.add-more-widgets-modal', function(e) {
	$('.modal-notif').html('');
    $('.add-widgets-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    
    //load content of add more widgets
    setTimeout(function(){ 
        $.ajax({
	        url: '$loadAllWidgetsUrl',
	        type: 'POST',
	        async: true,
	        success: function(data) {
	            $('.add-widgets-modal-body').html(data);          
	        },
	        error: function(){
	            $('.add-widgets-modal-body').html('<i>There was a problem while loading content.</i>'); 
	        }
    	});
    }, 300);
});

//add widget
$(document).on('click', '.add-widget', function(e) {

	var widgetId = $(this).attr('id');
	var label = $(this).attr('label');

    //load content of add more widgets
    setTimeout(function(){ 
        $.ajax({
	        url: '$addWidgetUrl',
	        type: 'POST',
	        async: true,
	        data: {
	        	widget_id: widgetId,
	        	user_id: $userId,
	        	label: label
	        },
	        success: function(data) {
	            $('.modal-notif').html('<div class="alert success"><div class="card-panel"><i class="fa fa-check"></i> Successfully added <b>'+ label +'</b> widget in the dashboard.</b><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button></div></div>');

	            $.pjax.reload('#dashboard-form');
	        },
	        error: function(){
	            $('.add-widgets-modal-body').html('<i>There was a problem while loading content.</i>'); 
	        }
    	});
    }, 300);
});

$('.close-add-widgets').click(function(){
	location.reload();
});

//filter widgets
$('#filter-add-widgets').on('change keyup paste click', function () {
	searchTerm = $(this).val();

	$('.add-widgets-modal-body .spec-add-widgets').each(function (index) {
		toolId = '#' + $(this).attr('id');

		if(searchTerm.length == 0){
			$(toolId).show();
		}else if ($(toolId + ' > .spec-widget:containsCaseInsensitive(' + searchTerm + ')' ).length > 0 && searchTerm.length > 0) {
			$(toolId).show();
		}else{
			$(toolId).hide();
		}
	});
});

$.expr[':'].containsCaseInsensitive = function (a, i, m) {
	return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};

JS
);


?>