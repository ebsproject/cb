<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Contains sample list of tools per act (nurseries)
 */
use yii\helpers\Url;

$url = '/site/sample'; 
?>
<div class="col col-md-4 nurseries-tool" id="nurseries-create-nurserise">
<a href="<?= Url::to([$url,'tool'=>'Create nurseries','enableNav'=>'t']) ?>">
	<div class="card-panel row">
		<div class="col col-md-3">
		<em class="material-icons  metrics-icon">subject</em>
		</div>
		<div class="col col-md-9 spec-nurseries-tool">
			<h5>Create nurseries</h5>
			<span class="text-muted">Create nurseries</span>
		</div>
	</div>
</a>
</div>

<div class="col col-md-4 nurseries-tool" id="nurseries-browse-nurseries">
<a href="<?= Url::to([$url,'tool'=>'Browse nurseries','enableNav'=>'t']) ?>">
	<div class="card-panel row">
		<div class="col col-md-3">
		<em class="material-icons  metrics-icon">subject</em>
		</div>
		<div class="col col-md-9 spec-nurseries-tool">
			<h5>Browse nurseries</h5>
			<span class="text-muted">Browse all nurseries</span>
		</div>
	</div>
</a>
</div>

<div class="col col-md-4 nurseries-tool" id="nurseries-browse-cross-list">
<a href="<?= Url::to([$url,'tool'=>'Manage tasks','enableNav'=>'t']) ?>">
	<div class="card-panel row">
		<div class="col col-md-3">
		<em class="material-icons  metrics-icon">check_box</em>
		</div>
		<div class="col col-md-9 spec-nurseries-tool">
			<h5>Manage tasks</h5>
			<span class="text-muted">Manage tasks</span>
		</div>
	</div>
</a>
</div>
