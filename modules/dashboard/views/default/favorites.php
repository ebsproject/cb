<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders favorites
 */
use yii\helpers\Url;
use app\models\Application;

echo '<div class="modal-notif"></div>';

if(empty($data)){
	echo '<p><br/>'.\Yii::t('app','There are no applications marked as favorite yet.').'</p>';	
}else{
	foreach ($data as $k => $value) {
		$id = $value['applicationDbId'];
		$label = $value['label'];
		$actionLabel = $value['actionLabel'];
		$icon = $value['icon'];
		$abbrev = $value['abbrev'];

		$application = new Application();
		$url = $application->geturlbyappid($id,$program); //process url of application

		$item = '<div class="col col-md-4 favorites-tool" id="favorites-'.$id.'">

			<a href="#" class="close remove-a-tool-icon" type="button" style="margin:10px 10px -5px 0px" data-appid="'.$id.'" title="Unmark as favorite">×</a>
			<a class="yii1-faverecent-link" href="'. Url::to([$url]) .'">
				<div class="card-panel row">
					<div class="col col-md-3">
						<i class="material-icons metrics-icon">'.$icon.'</i>
					</div>
					<div class="col col-md-9 spec-favorites-tool" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
						<h5 style="margin:8px 0px 5px 0px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;">'.\Yii::t('app', $label).'</h5>
						<span class="text-muted">'.\Yii::t('app', $actionLabel).'</span>
					</div>
				</div>
			</a>
		</div>	
		';
		echo $item;
	}
}
?>
