<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders bug report widgets modal
 *
 */
use app\controllers\SiteController;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use kartik\widgets\FileInput;
use kartik\widgets\ActiveForm;
?>
    <div id="loading-text">
    </div>
    <div class="col s12">
   
      <div class="row row-small">
        <div class="input-field col s12">
          <input id="summary_text" type="text" class="validate" data-length="80">
          <label for="summary_text" ><?=Yii::t('app', 'Summary')?> *</label>
        </div>

       
      </div>
    <div class="row row-small">
        <div class="input-field col s12">
            <textarea id="description_text" class="materialize-textarea validate" ></textarea>
            <label for="description_text" ><?=Yii::t('app', 'Description')?> *</label>
        </div>
      </div>
        <div class="row row-small">
            <div class="input-field col s6">
                <input id="reporter_field" type="text" class="validate" <?= (!empty($email)) ? 'readonly disabled' : '' ?> >
                <label class="active" for="reporter_field" style="transform: translateY(-14px) scale(0.8);transform-origin: 0 0;"> <?=Yii::t('app', 'Reporter email address')?> *</label>
            </div>
            <div class="col s6">
                <?php
                    
                    echo '<label class="control-label">'.\Yii::t('app', 'Request type').'</label>';
                    echo Select2::widget([
                        'name' => 'bug_request_type_id',
                        'id' => 'bug_request_type_id',
                        // 'data' => $data,
                        'options' => [
                            'placeholder' => 'Select request type',
                            'tags' => true,
                        ]
                    ]);
                ?>
            </div>
            
        </div>
    </div>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
            <!-- <form> -->
            <div class="file-field input-field">
            <div class="btn">
                <span>Attachment</span>
                <input name="file_attachment" id="report_attachment_id" type="file" multiple>
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" id="report_attachment_id_btn"  type="text" placeholder="Attach one or more files">
            </div>
            </div>

            <?php ActiveForm::end(); ?>
    <?php if(isset($renderedInYii1)){ ?>
        <a id="bug-report-create-btn" class="btn btn-primary waves-effect waves-light pull-right" href="#" url="#"><?= \Yii::t('app', 'Submit') ?><em class="material-icons right">send</em></i></a>
    <?php } ?>

    
            


<?php
$createIssue =  Url::to(['bug-report/create-issue']);
$uploadAttachment =  Url::to(['bug-report/upload-attachment']);
$this->registerJsFile("@web/js/materialize.min.js", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
 ], 'materialize');
$this->registerJs(<<<JS
    $("document").ready(function(){
        //enable submit button when send feedback modal is opened
        $('#bug-report-btn').on('click',function(){ 
            $('#bug-report-create-btn').removeAttr('disabled');
        });

        $('selector').select2();
        $('#bug_request_type_id').trigger('click');
        $("#bug_request_type_id").get(0).options.length = 0;
        //populate request type select 2
        var option = JSON.parse('$type');
        var s1 = document.getElementById('bug_request_type_id');
        for(var i=0; i<option.length;i++){
            var newOption = document.createElement("option");
            newOption.value = option[i]['id'];
            newOption.innerHTML = option[i]['display_name'];
            s1.options.add(newOption);
        }

        //if user not yet logged in, set inquiry as default request type
        var email = '$email';
        if(email == ''){
            $('#bug_request_type_id').val('inquiry');
        }

        //check if valid email
        function validateEmail(email) {

            filter = /^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/;
            if (filter.test(email)) {
              // Yay! valid
              return true;
            }
            else
              {return false;}
        }

        $("#reporter_field").val('$email');

        $('#bug-report-create-btn').unbind().bind('click',function(){
            var reporter = $("#reporter_field").val();
            var summary = $("#summary_text").val();
            var description = $("#description_text").val();
            var request_type = $("#bug_request_type_id").val();

            var emailValid = validateEmail(reporter);
            $('#reporter_field').removeClass('invalid');
            
            var missingFlag = 0;
            if(reporter == '' || reporter == null || reporter == undefined ){
                $("#reporter_field").addClass('invalid');
                missingFlag = 1;
            }
            if(summary == '' || summary == null || summary == undefined ){
                $("#summary_text").addClass('invalid');
                missingFlag = 1;
            }
            if(description == '' || description == null || description == undefined ){
                $("#description_text").addClass('invalid');
                missingFlag = 1;
            }
            if(request_type == '' || request_type == null || request_type == undefined ){
                $("#bug_request_type_id").addClass('invalid');
                missingFlag = 1;
            }
           
            var files = $("#report_attachment_id")[0].files;
            var data = new FormData();
            jQuery.each(files, function(i, file) {
                data.append(i, file);
            });

            //if with missing required fields and invalid email address
            if(!emailValid && missingFlag == 1){
                $('#reporter_field').addClass('invalid');
                $('#loading-text').html('<br/><span class="red-text text-darken-2">Please specify valid email address and required fields.</span>');
            }
            else if(!emailValid){
                $('#reporter_field').addClass('invalid');
                $('#loading-text').html('<br/><span class="red-text text-darken-2">Please specify valid email address.</span>');
            }
            else if(missingFlag == 0){
                $("#bug_request_type_id").attr('disabled', 'disabled');
                $("#reporter_field").attr('disabled', 'disabled');
                $("#summary_text").attr('disabled', 'disabled');
                $("#description_text").attr('disabled', 'disabled');
                $("#report_attachment_id").attr('disabled', 'disabled');
                $("#bug-report-create-btn").attr('disabled', 'disabled');
                $('#loading-text').html('<br/><span class="teal-text text-darken-2"><i id="loading-text">Submitting feedback...</i></span><div class="progress"><div class="indeterminate"></div></div>');
                setTimeout(function(){
                    $.ajax({ 
                        url: '$createIssue', 
                        type: 'POST', 
                        data: { 
                            reporter: reporter,
                            summary: summary,
                            description: description,
                            request_type: request_type,
                            yii1Url: '$yii1Url',
                            yii2Url: '$yii2Url',
                        }, 
                        async: false, 
                        success: function(issueKey) { 
                            if(files.length > 0 && issueKey != 'error'){
                                $.ajax({ 
                                    url: '$uploadAttachment'+'?files&issueKey='+issueKey, 
                                    type: 'POST', 
                                    data:data,
                                    processData: false,
                                    contentType: false,
                                    success: function(data) { 
                                        location.reload();        
                                    }, 
                                    error: function(){ 
                                        $('.bug-report-modal-body').html('<i>There was a problem while loading content.</i>');  
                                    } 
                                });    
                            } else {
                                location.reload();
                            }
                                
                        }, 
                        error: function(){ 
                            $('.bug-report-modal-body').html('<i>There was a problem while loading content.</i>');  
                        } 
                    });
                }, 500);

            } 
            else {
                $('#loading-text').html('<br/><span class="red-text text-darken-2">Please specify the required fields.</span>');
            }
        });
    });
JS
);
?>
<style>
.collapsible span.badge {
    margin-top: 0;
}
html{
    width: 98%;
}
</style>