<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders metrics widget in dashboard
 *
 */
use app\controllers\SiteController;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;

$layoutObj = SiteController::getLayout();
extract($layoutObj);
$moreInfo = 'Click to view more information';

$col = ($layout == 'one') ? '3' : '6';
?>
<div class="row">
<!-- operational studies count -->
<a href="#" data-target="#metrics-modal" data-toggle="modal" class="metrics-item" data-url="<?= Url::to(['/dashboard/metrics/loadnoofstudiesperattr']) ?>" data-attr="studies" title="<?= \Yii::t('app', $moreInfo); ?>">
	<div class="col col-md-<?= $col?>">
		<div class="col col-md-3">
			<em class="material-icons text-<?= $darken ?> metrics-icon">settings</em>
		</div>
		<div class="col col-md-9">
			<h5 style="color:#333" id="<?= $timestamp?>-study" class="count"><em class="fa fa-refresh fa-spin"></em></h5>
			<span class="text-muted">
				<?= \Yii::t('app', 'Operational studies'); ?>
			</span>
		</div>
	</div>
</a>

<!-- entries count -->
<a href="#" data-target="#metrics-modal" data-toggle="modal" class="metrics-item" data-url="<?= Url::to(['/dashboard/metrics/loadnoofentriesperattr']) ?>" data-attr="entries" title="<?= \Yii::t('app', $moreInfo); ?>">
	<div class="col col-md-<?= $col?>">
		<div class="col col-md-3">
			<em class="material-icons text-<?= $darken ?> metrics-icon">view_list</em>
		</div>
		<div class="col col-md-9">
			<h5 style="color:#333" id="<?= $timestamp?>-entry" class="count"><em class="fa fa-refresh fa-spin"></em></h5>
			<span class="text-muted">
				<?= \Yii::t('app', 'Entries'); ?>
			</span>
		</div>
	</div>
</a>

<!-- families count -->
<a href="#" data-target="#metrics-families-modal" data-toggle="modal" class="metrics-families" data-url="<?= Url::to(['/dashboard/metrics/loadnooffamiliesperattr']) ?>" data-attr="families" title="<?= \Yii::t('app', $moreInfo); ?>">
	<div class="col col-md-<?= $col?>">
		<div class="col col-md-3">
			<em class="material-icons text-<?= $darken ?> metrics-icon">storage</em>
		</div>
		<div class="col col-md-9">
			<h5 style="color:#333" id="<?= $timestamp?>-family" class="count"><em class="fa fa-refresh fa-spin"></em></h5>
			<span class="text-muted">
				<?= \Yii::t('app', 'Families'); ?>
			</span>
		</div>
	</div>
</a>

</div>

<?php
Modal::begin([
    'id' => 'my-modal',
]);
Modal::end();
?>

<style type="text/css">
.text-muted{	
    overflow: hidden;
    white-space:  nowrap;
    text-overflow:  ellipsis;
}
</style>
<!-- renders modals for metrics widget -->
<?= Yii::$app->controller->renderPartial('@app/modules/dashboard/views/widgets/metrics_modals.php'); ?>

<?php
$date = date("M j, Y");
$this->registerJsFile('https://code.highcharts.com/highcharts.js',['position' => \yii\web\View::POS_END]);
$viewFamiliesUrl = Url::to(['/dashboard/metrics/loadfamilies']);
$exportUrl = Url::to(['/dashboard/metrics/exportfamilies']);
$viewProgeniesUrl = Url::to(['/dashboard/metrics/loadprogenies']);
$exportProgeniesUrl = Url::to(['/dashboard/metrics/exportprogenies']);
$getSummaryUrl = Url::to(['/dashboard/metrics/getsummary']);
$getListOfStudiesUrl = Url::to(['/dashboard/metrics/getlistofstudies']);

//view study modal
Modal::begin([
    'id' => 'view-study-modal',
    'header' => '<h4 id="entity-label"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;'],
]);
echo '<div class="view-study-modal-body"></div>';
Modal::end();
//css
$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

$viewEntityUrl = Url::to(['/search/default/viewentity']);

$this->registerJs(<<<JS
$(document).on('click', '.view-study', function(e) {
    var obj = $(this);
    var entity_label = obj.data('entity_label');
    var entity_id = obj.data('entity_id');
    var entity_type = obj.data('entity_type');

    $('#entity-label').html('<i class="fa fa-info-circle"></i> ' + entity_label + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $('.view-study-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    
    //load content of view more information
    setTimeout(function(){ 
        $.ajax({
        url: '$viewEntityUrl',
        data: {
            entity_id: entity_id,
            entity_type: entity_type,
            entity_label: entity_label
        },
        type: 'POST',
        async: true,
        success: function(data) {
            $('.view-study-modal-body').html(data);          
        },
        error: function(){
            $('.view-study-modal-body').html('<i>There was a problem while loading record information.</i>'); 
        }
    });
    }, 300);
    
});

//retrieve summary count
$.ajax({
	url: '$getSummaryUrl',
	async: true,
	type: 'post',
	dataType: 'json', 
	data:{
		filters: '$filters',
		attr: 'study'
	},
	success: function(data) {
		$('#{$timestamp}-study').text(data.count);
	},
	error: function() {
	}
});

$.ajax({
	url: '$getSummaryUrl',
	async: true,
	type: 'post',
	dataType: 'json', 
	data:{
		filters: '$filters',
		attr: 'entry'
	},
	success: function(data) {
		$('#{$timestamp}-entry').text(data.count);
	},
	error: function() {
	}
});

$.ajax({
	url: '$getSummaryUrl',
	async: true,
	type: 'post',
	dataType: 'json', 
	data:{
		filters: '$filters',
		attr: 'family'
	},
	success: function(data) {
		$('#{$timestamp}-family').text(data.count);
	},
	error: function() {
	}
});

$.ajax({
	url: '$getSummaryUrl',
	async: true,
	type: 'post',
	dataType: 'json', 
	data:{
		filters: '$filters',
		attr: 'observation'
	},
	success: function(data) {
		$('#{$timestamp}-observation').text(data.count);
	},
	error: function() {
	}
});

//metrics families view more info
$(document).on('click','.metrics-families',function(e){
	$('.modal-notif').html('<div class="progress"><div class="indeterminate"></div></div>');
	
	setTimeout(function(){ 
		$.ajax({
			url: '$viewFamiliesUrl',
			async: true,
			type: 'post',
			data:{
				filters: '$filters'
			},
			success: function(data) {
				$('.modal-notif').html('');
				$('.metrics-families-modal-body').html(data); 
			},
			error: function() {
				$('.metrics-families-modal-body').html('<i>There was a problem while loading content.</i>');
			}
		});
	}, 100);	
});

//go back to families table
$(document).on('click','.back-to-families',function(e){
    setTimeout(function(){ 
      $('.families-table').css(
        "display",'block'
      );

      $('.progenies-table').css(
        "display",'none'
      );

    }, 300);

    $('.families-table').addClass('removes-active');
    $('.progenies-table').addClass('removes-active');
    $('.families-table').removeClass('remove-active');
    $('.progenies-table').removeClass('remove-active');
});

//view progenies
$(document).on('click','.view-progenies-from-families',function(e){
	$('.modal-notif').html('<div class="progress"><div class="indeterminate"></div></div>');

	var gpid1 = $(this).data('gpid1');
	var cross = $(this).data('cross');
	
	setTimeout(function(){ 
		$.ajax({
			url: '$viewProgeniesUrl',
			async: true,
			type: 'post',
			data:{
				gpid1: gpid1
			},
			success: function(data) {
				$('.modal-notif').html('');
				$('.progenies-table').html(data);
				$('.viewed-cross-name').html(cross);
			},
			error: function() {
				$('.metrics-families-modal-body').html('<i>There was a problem while loading content.</i>');
			}
		});
	}, 100);

	setTimeout(function(){ 
      $('.families-table').css(
        "display",'none'
      );

      $('.progenies-table').css(
        "display",'block'
      );
    }, 300);

    $('.families-table').addClass('remove-active');
    $('.progenies-table').addClass('remove-active');
    $('.families-table').removeClass('removes-active');
    $('.progenies-table').removeClass('removes-active');

});

//export families data
$(document).on('click', '.export-families', function(e) {
    window.location = '$exportUrl';
    $('#system-loading-indicator').hide();
});

//export progenies data
$(document).on('click', '.export-progenies', function(e) {
	var gpid1 = $(this).data('gpid1');
    window.location = '$exportProgeniesUrl' + '?gpid1=' + gpid1;
    $('#system-loading-indicator').hide();
});

//define initial values for variables
var pieChart = null;
var fieldAttr = 'studies';
var selectedAttr = 'experimental design';
var url = '/dashboard/metrics/loadnoofstudiesperattr';
var attr = 0;
var field = 'design';
var title = 'Number of studies per experimental design as of $date';
var modalTitle = 'Studies';

//update modal and pie chart upon clicking metrics item
$(document).on('click', '.metrics-item', function(e) {
    url = $(this).attr('data-url');
    fieldAttr = $(this).attr('data-attr');
    title = 'Number of '+ fieldAttr +' per '+ selectedAttr +' as of $date';
    modalTitle = fieldAttr[0].toUpperCase() + fieldAttr.slice(1);
    
	$('#metrics-modal-title').html(modalTitle);

	renderChart(url);
});

//exclude transparent in pie colors
var colors = Highcharts.getOptions().colors;
var pieColors = colors.filter(e=>e !== 'transparent');

Highcharts.setOptions({
    lang: {
        thousandsSep: ','
    }
});

//initial chart options
var chartOptions = {
	chart: {
    	backgroundColor: null,
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Number of '+ fieldAttr +' per '+ selectedAttr +' as of $date'
    },
    options: {
    	backgroundColor: null,
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            point: {
            	events: {
	                click: function () {
	                	// if studies, display list of studies
	                    if(fieldAttr == 'studies'){
	                    	//display list of studies
	                    	getListOfStudies(this.name,this.study_ids,this.y);
	                    }
	                }
	            }
            },
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b><br>{point.y:,.0f} ({point.percentage:.1f}%)',
                distance: 20
            },
            colors: pieColors,
            showInLegend: false
        }
    },
    series: [{
        name: 'No. of studies',
        colorByPoint: true,
        data: null
    }]
};

//display list of studies
function getListOfStudies(label,study_ids,count){
	$('.modal-notif').html('<div class="progress"><div class="indeterminate"></div></div>');

	$.ajax({
		url: '$getListOfStudiesUrl',
		async: true,
		type: 'post',
		data:{
			label: label,
			studyIds: study_ids,
			count: count
		},
		success: function(data) {
			$('.modal-notif').html('');
			$('.progenies-table').html(data);
		},
		error: function() {
			$('.progenies-table').html('<i>There was a problem while loading content.</i>');
		}
	});

	setTimeout(function(){ 
      $('.families-table').css(
        "display",'none'
      );

      $('.progenies-table').css(
        "display",'block'
      );
    }, 300);

    $('.families-table').addClass('remove-active');
    $('.progenies-table').addClass('remove-active');
    $('.families-table').removeClass('removes-active');
    $('.progenies-table').removeClass('removes-active');
}

//define pie chart
pieChart = new Highcharts.chart('metrics-chart', 
    chartOptions
);

//updates pie chart when selecting attributes
$('.metrics-select2').on('change', function(){
	var attr = $(this).val();

	if(attr == 0){ //attribute per design
		selectedAttr = 'experimental design';
		title = 'Number of ' + fieldAttr +' per '+ selectedAttr +' as of $date';
		field = 'design';
	}
	else if(attr == 1){ //attribute per phase
		selectedAttr = 'phase';
		title = 'Number of ' + fieldAttr +' per '+ selectedAttr +' as of $date';
		field = 'phase';
	}
	else if(attr == 2){ //attribute per location
		selectedAttr = 'location';
		title = 'Number of ' + fieldAttr +' per '+ selectedAttr +' of $date';
		field = 'place';
	}
	else if(attr == 3){ //attribute per year
		selectedAttr = 'year';
		title = 'Number of ' + fieldAttr +' per '+ selectedAttr +' as of $date';
		field = 'year';
	}

	renderChart(url);

});

//renders pie chart
function renderChart(url){

	$('.families-table').addClass('removes-active');
    $('.progenies-table').addClass('removes-active');
    $('.families-table').removeClass('remove-active');
    $('.progenies-table').removeClass('remove-active');

	$('.families-table').css(
		"display",'block'
	);

	$('.progenies-table').css(
		"display",'none'
	);

	$('.modal-notif').html('<div class="progress"><div class="indeterminate"></div></div>');
	$('#metrics-chart').css('display','none');
	
	setTimeout(function(){ 
		$.ajax({
			url: url,
			async: true,
			type: 'post',
			dataType: 'json', 
			data:{
				field: field,
				filters: '$filters'
			},
			success: function(data) {
				$('.modal-notif').html('');
				if(pieChart.series != null){
					pieChart.series[0].remove();
					pieChart.addSeries({name: 'Number of ' + fieldAttr,data:data});
					pieChart.setTitle({text:title});

	    			$('#metrics-chart').css('display','block');
				}
			},
			error: function(jqXHR, exception) {
				alert('There was a problem encountered while loading data.');
			}
		});
	}, 100);
}
JS
);
?>