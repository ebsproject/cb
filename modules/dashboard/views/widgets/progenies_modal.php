<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders list of families based from defined data filters
 */

use yii\grid\GridView;
?>
<a href="#" class="back-to-families" title="<?= \Yii::t('app', 'Go back') ?>"><em class="material-icons">arrow_back</em></a>

<p><?= \Yii::t('app', 'Below are the progenies of the cross') ?> <strong class="viewed-cross-name"></strong>.</p>
<a class="btn-floating waves-effect waves-light grey pull-right export-progenies" data-gpid1="<?= $gpid1 ?>" title="<?= \Yii::t('app', 'Export all data to csv file') ?>" style="margin-top: -32px;">
    <em class="material-icons">file_download</em>
</a>

<?= GridView::widget([
    'dataProvider' => $data,
    'id' => 'view-progenies-table-from-families',
    'layout' => '{summary}{items}',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'designation',
        [
            'attribute' => 'derivative_name',
            'format' => 'raw',
            'value' => function ($model) {
                return $model['derivative_name'];
            }
        ],
        [
            'attribute' => 'product_type',
            'format' => 'raw',
            'value' => function ($model) {
                $product_type = $model['product_type'];
                $class = 'blue'; 
                if($product_type == 'progeny'){
                    $class = 'green';
                }

                return '<span class="new badge '.$class.' darken-2">'.strtoupper($product_type).'</span>';
            }
        ],
        'year',
        'generation'
    ]
]) ?>
<?php
//display remaining count 
$remaining = ($data->totalCount - 20000);

if($remaining > 0){ //if more than 20,000
    echo '<i>'.number_format($remaining).' more...</i>';
}
?>