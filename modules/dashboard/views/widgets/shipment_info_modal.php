<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders more information about the shipment
 */

use yii\grid\GridView;
use app\models\User;
?>

<p><?= \Yii::t('app', 'Below displays more information about the shipment and lists the seed lots that belong to it.') ?></p>

<ul class="collapsible popout collapsible-accordion col col-md-6" data-collapsible="accordion" style="margin-top:-10px">
    <?php
    //sender information
    $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';
    ?>  
    <li class="active">
        <div class="collapsible-header active"><em class="material-icons">send</em><?= \Yii::t('app', 'Sender') ?></div>
        <div class="collapsible-body" style="display: block;margin-top:15px">
            <dl class="dl-horizontal">
                <dt><?= \Yii::t('app', 'Shipper program') ?></dt>
                    <dd><?= (!empty($data['shipper_program'])) ? $data['shipper_program'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Transaction No.') ?></dt>
                    <dd><?= (!empty($data['transaction_no'])) ? $data['transaction_no'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Name') ?></dt>
                    <dd><?= (!empty($data['name'])) ? $data['name'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Shipper') ?></dt>
                    <dd><?= (!empty($data['sent_by'])) ? $data['sent_by'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'OU Request No.') ?></dt>
                    <dd><?= (!empty($data['ou_request_number'])) ? $data['ou_request_number'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Date dispatched') ?></dt>
                    <dd><?= (!empty($data['date_dispatched'])) ? $data['date_dispatched'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Remarks') ?></dt>
                    <dd><?= (!empty($data['remarks'])) ? $data['remarks'] : $notSet ?></dd>
            </dl>
        </div>
    </li>
</ul>

<ul class="collapsible popout collapsible-accordion col col-md-6" data-collapsible="accordion" style="margin-top:-10px">
    <?php
    //recipient information
    if($data['category'] == 'OUTBOUND' && !empty($data['recipient_id'])){
        $recipient = User::getDmsPersonDisplayName($data['recipient_id']);
    }else if (!empty($data['recipient_id'])){
        $recipient = User::getUserInfoById('display_name',$data['recipient_id']);
    }
    ?>  
    <li class="active">
        <div class="collapsible-header active"><em class="material-icons">call_received</em><?= \Yii::t('app', 'Recipient') ?></div>
        <div class="collapsible-body" style="display: block;margin-top:15px;min-height: 140px;">
            <dl class="dl-horizontal">
                <dt><?= \Yii::t('app', 'Receiver program') ?></dt>
                    <dd><?= (!empty($data['receiver_program'])) ? $data['receiver_program'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Recipient') ?></dt>
                    <dd><?= (!empty($data['recipient_id'])) ? $recipient : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Recipient Institution') ?></dt>
                    <dd><?= (!empty($data['recipient_institution'])) ? $data['recipient_institution'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Recipient Address') ?></dt>
                    <dd><?= (!empty($data['recipient_address'])) ? $data['recipient_address'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Recipient Country') ?></dt>
                    <dd><?= (!empty($data['recipient_country'])) ? $data['recipient_country'] : $notSet ?></dd>
                <dt><?= \Yii::t('app', 'Recipient type') ?></dt>
                    <dd><?= (!empty($data['recipient_type'])) ? $data['recipient_type'] : $notSet ?></dd>
            </dl>
        </div>
    </li>
</ul>

<ul class="collapsible popout collapsible-accordion col col-md-12" data-collapsible="accordion" style="margin-top:-1px">
    <?php
    //recipient information
    ?>  
    <li class="active">
        <div class="collapsible-header active"><em class="material-icons">list</em><?= \Yii::t('app', 'Seed lots') . ' ('.$count.')' ?></div>
        <div class="collapsible-body" style="display: block;margin:0px -15px -10px -15px">
            <?= 
                GridView::widget([
                    'dataProvider' => $seedLots,
                    'id' => 'view-shipment-widget',
                    'layout' => '{items}',
                    'tableOptions' => ['class' => 'table table-bordered white','style'=>'margin-bottom:-1px'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'designation',
                        'gid',
                        'source_study',
                        'source_year',
                        'label',
                        'volume'
                    ]
                ])
            ?>
        </div>
    </li>
</ul>
