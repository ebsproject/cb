<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\modules\service\models\ServiceRequest;

/**
 * Renders service request widget in dashboard
 */

Yii::$app->view->registerCss('
    .notification-badge {
        font-family: "Poppins", sans-serif;
        position: relative;
        top: -20px;
        right: 5px;
        margin: 0 -.8em;
        padding: 2px 5px;
        color: #fff;
        border-radius: 50%;
    }
    .chat-wrapper .chat-message {
        position: relative;
        float: left;
        clear: both;
        margin: 2px 0px 2px 2px;
        padding: 8px;
        border-radius: 8px;
        line-height: 22px;
        background-color: #ddd;
    }
    .chat-wrapper .chat-message.right {
        background-color: #448AFF;
        color: #fff;
    }
    .chat-wrapper .chat-message-sender.right {
         clear: both;
    }
    .switch label>.lever{
        margin:7px;
    }
');

$serviceRequestMsgClass='service_request-msg-'.$col.'-'.$key.'-'.$serviceType;
$columns = [
    [
        'header'=>\Yii::t('app','Inbox'),
        'format' =>'raw',
        'value'=> function($model) use ($serviceRequestMsgClass){
            if($model["count_unread_message"]>0){
                return Html::a('<i class="material-icons" style="width:25px;">messages</i><span class="notif-messages-'.$model['id'].
                    '"><small class="notification-badge red accent-2">'.$model["count_unread_message"].'</small><span>',
                    '#',
                    [
                        'class' => $serviceRequestMsgClass,
                        'title' => \Yii::t('app','View messages'),
                        'data-service_request_id' => $model['id'],
                        'data-service_request_thread_id' => $model['thread_id'],
                        'data-service_request_label' => $model['study_name'],
                        'data-target' => '#view-service_request-modal',
                        'data-toggle' => 'modal'
                    ]
                );
            }else if($model["count_all_message"]>0){
                return Html::a('<i class="material-icons" style="width:25px;">messages</i>',
                    '#',
                    [
                        'class' => $serviceRequestMsgClass,
                        'title' => \Yii::t('app','View messages'),
                        'data-service_request_id' => $model['id'],
                        'data-service_request_thread_id' => $model['thread_id'],
                        'data-service_request_label' => $model['study_name'],
                        'data-target' => '#view-service_request-modal',
                        'data-toggle' => 'modal'
                    ]
                );
            }else{
                return '';
            }
        }
    ],
    [
    	'attribute'=>'status',
    	'format'=>'raw',
    	'value'=> function($data){
    		return '<span title="'.$data['status'].'">'.ServiceRequest::formatStatus($data['status']).'</span>';
    	}
    ],
    'study',
    'study_name',
    [
        'attribute'=>($serviceType==='provider')?'requestor_program':'provider_program',
        'label'=>($serviceType==='provider')?\Yii::t("app","Program"):\Yii::t("app","Provider"),
    ],
    [
        'attribute'=>'service',
        'format'=>'raw',
        
    ],
    'requestor',
    [
        
        'label'=>\Yii::t("app","Updated on"),
        'attribute'=>'updated_on',
        'format'=>'raw',
        'value'=> function($data){
            return Yii::$app->formatter->asDatetime($data['updated_on']);
        }
    ],
    [
        
        'label'=> \Yii::t("app","Updated by"),
        'attribute'=>'modifier',
        'format'=>'raw',
        
    ],
    [
        
        'attribute'=>'date_requested',
        'format'=>'raw',
        'value'=> function($data){
            return Yii::$app->formatter->asDatetime($data['date_requested']);
        }
    ],
    [
        
        'header'=> \Yii::t("app","Last Activity"),
        'attribute'=>'last_activity',
        'format'=>'raw',
        
    ],
    
];

$gotoStr= \Yii::t("app","Go to Service Requests");
$goto= <<<HTML
    <button class="btn-link right" style="height: 25px;line-height: 25px; text-transform: none;padding: 0px 9px;clear:right;" id="goto-btn-$col-$key-$serviceType" >
        $gotoStr
        <i class="material-icons right" style="margin-left: 2px;font-size:20px;" >arrow_forward</i>
    </button>
    <br>
HTML;

$notification= '';
$showAll= \Yii::t("app","Show all");
$showNew= \Yii::t("app","Show new");

$switchButton= <<<HTML
    <div class="switch">
        <label>
            $showAll
            <input type="checkbox" id="filter-switch-id-$col-$key-$serviceType">
            <span class="lever"></span>
            $showNew
        </label>
    </div>
HTML;
if($unreadMsgsCount==1){

    $notification = '<div class="unread-msg-count-'.$serviceType.' " style="clear:both;"><span class="badge green  white-text">'.$unreadMsgsCount." ".\Yii::t('app','unread message')."</span>"."</div>"."";
}else if($unreadMsgsCount>1){

   $notification = '<div class="unread-msg-count-'.$serviceType.' " "><span class="badge  white-text green">'.$unreadMsgsCount." ".\Yii::t('app','unread messages')."</span>"."</div>";
}

$notification = $notification.$goto; 

if($serviceType==='provider'){
    $notification = $notification.'<span class="left" style="clear:both;">'.$switchButton.'</span>';
}

//dynagrid configuration
$panel = [
    'after' => false,
];

if($p=='configure'){
    $footer = ['footer'=>false];

    $panel = array_merge($panel,$footer);
}

$layout = <<< HTML
    {custom}    
    
    <div class="right" style="clear:both;">
        {summary}
    </div>

    <div class="clearfix"></div>
    {items}
    {pager}
HTML;

$dataBrowserId='service_request-widget-'.$col.'-'.$key.'-'.$serviceType; // data browser identifier
$filterWidgetId='-'.$col.'-'.$key.'-'.$serviceType; // identifier on which widget should apply the filter

$dynagrid = DynaGrid::begin([
	'options'=>['id'=>$dataBrowserId],
	'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
    	'dataProvider'=>$dataProvider,
    	'pjax'=>true,        
        'pjaxSettings'=>[
                'neverTimeout'=>true,
                'options'=>['id'=>$dataBrowserId,'enablePushState'=>false],
                'beforeGrid'=>'',
                'afterGrid'=>'',
            ],
    	'panel'=> false,
        'layout' => $layout,
        'replaceTags' => [
            '{custom}' => $notification,
        ],
    	'responsiveWrap'=>false,
	    'toolbar' =>false,
    ]
]);

DynaGrid::end();
echo '</div>';

// Message thread modal
Modal::begin([
    'id' => 'view-service_request-modal',
    'header' => '<h4 id="service_request-label"></h4>',
    'footer' => Html::a(\Yii::t('app',\Yii::t('app','Ok')),'#',
        [
            'data-dismiss'=>'modal',
            'class'=>'btn btn-primary waves-effect waves-light modal-close ok-btn-'.$serviceType
        ]).'&emsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    'options' => ['data-backdrop'=>'static'],
]);
echo 
'<div class="view-service_request-modal-body">

</div>';
Modal::end();

//css
$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

extract($_GET);

//view service_request url
$viewServiceRequestUrl = Url::to(['/dashboard/createdservicerequests/get-messages']);

$getServiceRequestUrl = Url::to(["/".Yii::$app->controller->getRoute(),"col"=>$col,"key"=>$key,"timestamp"=>$timestamp,"layout"=>$layout,"program"=>$program]);

$getUnreadMsgsCountUrl = Url::to(['/dashboard/createdservicerequests/get-unread-messages-count','serviceType'=>$serviceType,'filter'=>$filter,'filterWidget'=>$filterWidgetId]);

$gotoUrl= Url::toRoute(["/service/requests","program"=>$program]);

$this->registerJs(<<<JS

var filter="show_new";

//reinitialize event bindings after pjax reload
$(document).on('ready pjax:success', function(e) {

    if(filter=='show_all'){    

        $('#filter-switch-id'+"$filterWidgetId").prop("checked",false); 
    }else if(filter=='show_new'){    

        $('#filter-switch-id'+"$filterWidgetId").prop("checked",true); 
    }

    $('#goto-btn-$col-$key-$serviceType').on('click',function(e){
        window.location="$gotoUrl";
    });
});

if(filter=='show_all'){    

    $('#filter-switch-id'+"$filterWidgetId").prop("checked",false); 
}else if(filter=='show_new'){    

    $('#filter-switch-id'+"$filterWidgetId").prop("checked",true); 
}

//event to load new or all service requests
$(document).on('change',"#filter-switch-id"+"$filterWidgetId",function() {
    var filterStatus = $(this).prop('checked');
    
    if(filterStatus){
        filter='show_new';
    }else{
        filter='show_all';
    }

    $.pjax.reload({
        container: '#$dataBrowserId-pjax',
        replace: false,
        url: "$getServiceRequestUrl"+"&filter="+filter+"&filterWidget=$filterWidgetId"
    });
});

//redirects to service requests tool
$('#goto-btn-$col-$key-$serviceType').on('click',function(e){
    window.location="$gotoUrl";
});

//view modal for the message thread
$(document).on('click', ".$serviceRequestMsgClass", function(e) {
    var obj = $(this);
    var serviceRequestLabel = obj.data('service_request_label');
    var serviceRequestId = obj.data('service_request_id');
    var serviceRequestThreadId = obj.data('service_request_thread_id');

    $('#service_request-label').html('<i class="material-icons">forum</i> ' + serviceRequestLabel);
    $('.view-service_request-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    $('.view-service_request-modal-body')
    
    // load content of view more information
    setTimeout(function(){ 
        $.ajax({
            url: '$viewServiceRequestUrl',
            data: {
                threadId: serviceRequestThreadId,
            },
            type: 'POST',
            async: true,
            success: function(data) {
                $('.view-service_request-modal-body').html(data);  
                $('.notif-messages-'+serviceRequestId).html('');        
            },
            error: function(){
                 $('.view-service_request-modal-body').html('<i>There was a problem while loading record information.</i>'); 
            }
        });
    }, 100);

    setTimeout(function(){ 
        $.ajax({
            url: '$getUnreadMsgsCountUrl',
            async: false,
            success: function(count) {
                $('.unread-msg-count-$serviceType').html(count);        
                
            }
        });
    }, 100);
    
});

JS
);