<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders metrics widget modals in dashboard
 *
 */
use kartik\select2\Select2;
?>

<!-- modal for metrics view more info -->
<div id="metrics-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div style="float:left"><h4 id="metrics-modal-title"><?= \Yii::t('app', 'Operational studies')?></h4></div><div style="float:right;padding-right:20px;margin-top:-10px;">
		    <?php
				echo Select2::widget([
				    'name' => 'sudies_per_attribute',
				    'value' => 'per design', // initial value
				    'data' => [\Yii::t('app', 'per design'),\Yii::t('app', 'per phase'),\Yii::t('app', 'per location'),\Yii::t('app', 'per year')],
				    'id' => 'studies_per_attribute_select',
				    'maintainOrder' => true,
				    'options' => ['class' => 'metrics-select2'],
				    'pluginOptions' => [
				        'tags' => true,
				        'maximumInputLength' => 10,
				        'width' => '100%'
				    ],
				]);
			?>
		</div>
		</div>
		<div class="modal-body metrics-modal-body">
			<div class="modal-notif"></div>
			<div id="metrics-chart" class="families-table"></div>
			<div class="progenies-table"></div>
		</div>
		<div class="modal-footer">
		<a class="btn btn-primary waves-effect waves-light modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Close') ?></a>&emsp;
		</div>
    </div>
  </div>
</div> 


<!-- modal for families view more info -->
<div id="metrics-families-modal" class="fade modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<div style="float:left"><h4 id="metrics-modal-title"><?= \Yii::t('app', 'Families') ?></h4></div>
		</div>

		<div class="modal-body metrics-families-modal-body">
			<div class="modal-notif"></div>

		</div>
		<div class="modal-footer">
		<a class="btn btn-primary waves-effect waves-light modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app', 'Close') ?></a>&emsp;
		</div>
    </div>
  </div>
</div> 
