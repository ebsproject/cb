<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders favorites widget
 */
use yii\helpers\Url;
use app\models\Application;
use app\controllers\SiteController;
use kartik\sortable\Sortable;

$layoutObj = SiteController::getLayout(); //authenticate access token
extract($layoutObj);

if(empty($data)){
	echo '<p><br/>'.\Yii::t('app','There are no applications marked as favorite yet.').'</p>';
}else{

	echo '<div class="row search-favorites-filter-'.$col.'-'.$key.'" style="margin:0px;display:none;"><div class="col col-md-11" style="padding-left:0"><input placeholder="Search favorites" id="search_favorites_widget_'.$col.'_'.$key.'" style="margin-top:-20px;" autofocus></input></div>
		<div class="col col-md-1" style="padding-left:0">
		<a href="#!" title="Hide search" class="text-muted fixed-color hide-favorites-filter-'.$col.'-'.$key.'" style="display:none"><i class="material-icons widget-icons">close</i></a></div></div>
	';
	echo '<div class="favorites-content-'.$col.'-'.$key.'" style="max-height:215px;overfloy-y:auto">';
	$items = [];
	//reorder favorites
	$reorderUrl = Url::to(['/dashboard/favorites/reorderinfavoriteswidget']);
	$pluginEvents = [
	    'sortupdate' => "function() { 
	    	var itemsArr = [];
			$('#favorites-tools-sort').find('li').each(function(e){
				var obj = $(this);
				var idStr = obj.attr('id');

				if(idStr !== undefined){
					var idStr = idStr.split('_');
					var id = idStr[1];

					var data = id;
					itemsArr.push(data);
				}
			});

			$.ajax({
		        url: '$reorderUrl',
		        type: 'POST',
		        async: true,
		        data: {
		        	app_ids: itemsArr,
		        	user_id: $userId
		        },
		        success: function(data) {
		        },
		        error: function(){
		            console.log('There was a problem while loading content.'); 
		        }
			}); 
	    }",
	];

	$itemsStr='';
	$colClass = (!empty($layout) && $layout == 'one') ? '2' : '3';
	$class=(empty($p)) ? 'grid-item ' : 'col col-md-'.$colClass;
	$hidden=(empty($p)) ? '' : 'hidden';
	//loop through applications
	foreach ($data as $k => $value) {

		$id = $value['applicationDbId'];
		$label = $value['label'];
		$actionLabel = $value['actionLabel'];
		$icon = $value['icon'];

		$application = new Application();
		$url = $application->geturlbyappid($id,$program); //process url of application

		$item = '<div class="'.$class.' my-favorite-tools" title="'.\Yii::t('app', 'Click to go to').' '.\Yii::t('app', $label).'" style="white-space: nowrap;text-overflow: ellipsis;margin-bottom:-5px" id="favorites-widget-'.$id.'-'.$k.'-'.$col.'-'.$key.'">

			<a href="#!" class="close remove-a-tool-from-favorites '.$hidden.'" id="favorites-close-'.$id.'-'.$k.'-'.$col.'-'.$key.'" type="button" style="margin:0px 4px;opacity:0;color:#fff !important;font-weight:normal;" data-appid="'.$id.'" title="'.\Yii::t('app', 'Unmark as favorite').'">×</a>

			<a href="'. Url::to([$url]) .'" class="favorites-tool-item yii1-favorites-link dashboard-widget-item-theme-color" data-close_id="'.$id.'-'.$k.'-'.$col.'-'.$key.'">
				<div class="card-panel dashboard-widget-item-theme-color '. $darken .' white-text" style="text-align:center; padding: 5px 10px 15px 9px;">
					<i class="material-icons metrics-icon">'.$icon.'</i><br/>
					<span class="favorites-spec-tool">'.\Yii::t('app', $label).'</span>
				</div>
			</a>
		</div>';

		$itemsStr = $itemsStr.$item;

		$items[] = [
			'options'=>['id'=>'favorites-widget-item_'.$id,'style'=>'margin:0px'],
			'content'=>$item
		];
	}

	if(!empty($p)){
		echo $itemsStr;
	}else{
		echo Sortable::widget([ 
			'type'=>'grid',
		    'id'=>'favorites-tools-sort',
		    'itemOptions' => ['class'=>'widgets','title'=>\Yii::t('app', 'Drag and drop to reorder items.')],
		    'pluginEvents' => $pluginEvents,
		    'items'=>$items
		]);
	}
	echo '</div>';
}

$removeToolInFavUrl = Url::to(['/dashboard/default/unmarkfavorites']);
$resetYii1Url = Url::to(['/dashboard/default/resetyii1url','program'=>$program]);
$this->registerJs(<<<JS

//remove favorites
$(document).on('click', '.remove-a-tool-from-favorites', function(e) {
	var obj = $(this);
	var app_id = obj.data('appid');

	if(document.getElementById("favorites-widget-item_"+app_id) !== null){
		document.getElementById("favorites-widget-item_"+app_id).remove();
	}	

	$.ajax({
		url: '$removeToolInFavUrl',
		type: 'POST',
		async: true,
		data: {
			app_id: app_id
		},
		success: function(data) {
		},
		error: function(){
		}
	});
});

//hover on an item in favorites
$(".favorites-tool-item").mouseover(function(){
	var obj = $(this);
	var close_id = obj.data('close_id');
    $('#favorites-close-'+close_id).css("opacity", "0.3");
});

$(".remove-a-tool-from-favorites").mouseover(function(){	
    $(this).css("opacity", "0.3");
});
//remove hover on an element in favorites
$(".favorites-tool-item").mouseout(function(){
	var obj = $(this);
	var close_id = obj.data('close_id');
    $('#favorites-close-'+close_id).css("opacity", "0");
});

//filter favorites
$('#search_favorites_widget_{$col}_$key').on('change keyup paste click', function () {
	searchTerm = $(this).val();

	$('.favorites-content-$col-$key .my-favorite-tools').each(function (index) {
		toolId = '#' + $(this).attr('id');

		if(searchTerm.length == 0){
			$(toolId).closest('li').show();
		}else if ($(toolId + ' div.card-panel > .favorites-spec-tool:containsCaseInsensitive(' + searchTerm + ')' ).length > 0 && searchTerm.length > 0) {
			$(toolId).closest('li').show();
		}else{
			$(toolId).closest('li').hide();
		}
	});
});

$.expr[':'].containsCaseInsensitive = function (a, i, m) {
	return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};

//show filter input
$('.search-favorites-widget').on('click', function () {
	$('.search-favorites-filter-$col-$key, .hide-favorites-filter-$col-$key').css('display','block');
});

//hide filter input
$('.hide-favorites-filter-$col-$key').on('click', function () {
	$('.search-favorites-filter-$col-$key, .hide-favorites-filter-$col-$key').css('display','none');
	$('#search_favorites_widget_{$col}_$key').val('');
	$( "#search_favorites_widget_{$col}_$key" ).trigger("change");
});

$(".yii1-favorites-link").on("click", function (e) {
	e.preventDefault();
	var actionLink = $(this).attr("href");
	if(actionLink != undefined && actionLink != '#'){
	  $.ajax({
		url: '$resetYii1Url',
		type: 'post',
		dataType: 'json',
		async:true,
		data: {
			actionLink: actionLink
		},
		success: function(response) {
		  window.location.href = actionLink; 
		},
	  });
	}
});
JS
);
?>

<style type="text/css">
.favorites-spec-tool{
	display: block;
	text-overflow: ellipsis;
	overflow: hidden;
}
#favorites-tools-sort.sortable{
	border:none;
}
#favorites-tools-sort.sortable li{
    margin: 4px;
    padding: 0px 5px;
    border: 1px dashed #eee;
}

#favorites-tools-sort.sortable li{
	    width: 12.68%;
	}

@media only screen and (min-width: 992px){
    #favorites-tools-sort.sortable li{
	    width: 15.68%;
	}
}

@media only screen and (min-width: 1100px){
    #favorites-tools-sort.sortable li{
	    width: 20.99%;
	}
}

@media only screen and (min-width: 1200px){
    #favorites-tools-sort.sortable li{
	    width: 22.99%;
	}
}
@media only screen and (min-width: 1366px){
    #favorites-tools-sort.sortable li{
	    width: 23.35%;
	}
}

@media only screen and (min-width: 1700px){
    #favorites-tools-sort.sortable li{
	    width: 23.79%;
	}
}
</style>