<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders breeding cycle widget in dashboard
 *
 */
use yii\bootstrap\Modal;
use yii\helpers\Url;
?>

<div id="breeding-cycle-<?= $col.'-'.$key ?>" class="breeding-cycle"></div>

<div id="select-tool-modal" class="fade modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div style="float:left"><h4><?= \Yii::t('app','Select tool') ?></h4></div><div style="float:right"><input placeholder="<?= \Yii::t('app','Filter') ?>" id="bc_select_tool_search_bar" style="margin-top:-10px;"></input></div>
			</div>
			<div class="modal-body select-tool-modal-body bc-select-tools">
			</div>
			<div class="modal-footer">
				<a class="btn btn-primary waves-effect waves-light modal-close" href="#" data-dismiss="modal"><?= \Yii::t('app','Close') ?></a>&emsp;
			</div>
		</div>
	</div>
</div>

<style type="text/css">
.breeding-cycle{
    margin: auto;
    width: 100%;
}
@media only screen and (min-width: 1400px){
    .breeding-cycle{
        width: 80%;
    }
}
@media only screen and (min-width: 1700px){
    .breeding-cycle{
        width: 70%;
    }
}

</style>

<?php
Modal::begin([
    'id' => 'my-modal',
]);
Modal::end();
?>

<?php
$selectToolUrl = Url::to(['/dashboard/breedingcycle/breedingcycleselecttool','program'=>$program]);
$this->registerJsFile('https://code.highcharts.com/highcharts.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://code.highcharts.com/modules/sunburst.js',['position' => \yii\web\View::POS_END]);

$this->registerJs(<<<JS

// for select tool filter
$('#bc_select_tool_search_bar').on('change keyup paste click', function () {
	searchTerm = $(this).val();
	$('.bc-select-tools > .bc-select-tool').each(function (index) {
		toolId = '#' + $(this).attr('id');

		if(searchTerm.length == 0){
			$(toolId).show();
		}else if ($(toolId + ' div.card-panel > div.spec-bc-select-tool:containsCaseInsensitive(' + searchTerm + ')' ).length > 0 && searchTerm.length > 0) {
			$(toolId).show();
		}else{
			$(toolId).hide();
		}
	});
});

$.expr[':'].containsCaseInsensitive = function (a, i, m) {
	return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};
//end select tool filter

//initialize breeding cycle data
var data = '$data';

// set colors
var pieColors = ["transparent", "#4fc3f7", "#4db6ac", "#434348", "#7e57c2", "#d4e157", "#e57373"];

Highcharts.chart('breeding-cycle-$col-$key', {
    chart: {
        height: '100%'
    },
    title: {
        text: null
    },
    series: [{
        type: "sunburst",
        data: JSON.parse(data),
        allowDrillToNode: false,
        cursor: 'pointer',
        events: {
            click: function (event) {
                if(event.point.id != 0){ //show select tool modal
        			$('#select-tool-modal').modal('show');
        			renderSelectTool(event.point.activity);
    			}
            }
        },
        dataLabels: {
            format: '{point.name}'
        },
        levels: [{
            level: 1,
            levelIsConstant: false,
            dataLabels: {
                rotationMode: 'parallel',
                style: {
                    fontSize: '13px',
                    textOutline: 0,
                    color: 'black'
                }
            }
        }, {
            level: 2,
            colorByPoint: true,
            dataLabels: {
                rotation: 0,
                style: {
                    fontSize: '13px',
                    textOverflow: 'ellipsis',
                }
            }
        }]
    }],
    tooltip: {
        headerFormat: "",
        pointFormat: '<b>{point.name}</b>'
    },
    colors: pieColors
});

//renders modal that displays tools of an activity
function renderSelectTool(activity){
	$.ajax({
		url: '$selectToolUrl',
		async: false,
		type: 'post',
		data:{
			activity: activity
		},
		success: function(data) {
			$('.select-tool-modal-body').html(data);  
		},
		error: function(jqXHR, exception) {
			alert('There was a problem encountered while loading data.');
		}
	});
}

JS
);
?>

<style type="text/css">
.BREEDING_CYCLE > .box-body{
    padding:0px !important;
}</style>