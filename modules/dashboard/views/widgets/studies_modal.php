<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders list of studies with basic information
 */

use yii\helpers\Html;
use yii\grid\GridView;
?>
<?php if(empty($hideBack)){ //show back button?>
    <a href="#" class="back-to-families" title="<?= \Yii::t('app', 'Go back') ?>"><em class="material-icons">arrow_back</em></a>
    <p><strong><?= $label ?></strong></p>
<?php } ?>
<?= GridView::widget([
    'dataProvider' => $data,
    'id' => 'view-studies-from-metrics',
    'layout' => '{summary}{items}',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class'=>'yii\grid\ActionColumn',
            'template' => '{view}',
            'contentOptions' => ['class' => 'text-center'],
            'buttons' => [
                'view' => function ($url,$model) {  
                    return Html::a('<i class="fa fa-eye"></i>',
                        '#',
                        [
                            'class' => 'view-study',
                            'title' => 'View study',
                            'data-entity_id' => $model['studyDbId'],
                            'data-entity_label' => $model['study'],
                            'data-entity_type' => 'study',
                            'data-target' => '#view-study-modal',
                            'data-toggle' => 'modal'
                        ]
                    );
                }
            ]
        ],
        'study',
        'place',
        [
            'attribute'=>'phaseAbbrev',
            'header' => 'Phase',
        ],
        'year',
        'season',
        'design'
    ]
]) ?>
