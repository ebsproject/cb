<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders favorites widget
 */
use yii\helpers\Url;
use app\models\Application;
use app\controllers\SiteController;
use app\controllers\Dashboard;
use app\components\FavoritesWidget;

$program = Yii::$app->userprogram->get('abbrev'); //current program

if(empty($data)){
	echo '<p><br/>'.\Yii::t('app','There are no recently used applications.').'</p>';
}else{

	echo '<div class="row search-recentlyused-filter-'.$col.'-'.$key.'" style="margin:0px;display:none;"><div class="col col-md-11" style="padding-left:0"><input placeholder="Search recently used" id="search_recentlyused_widget_'.$col.'_'.$key.'" style="margin-top:-20px;" autofocus></input></div>
		<div class="col col-md-1" style="padding-left:0">
		<a href="#!" title="Hide search" class="text-muted fixed-color hide-recentlyused-filter-'.$col.'-'.$key.'" style="display:none"><i class="material-icons widget-icons">close</i></a></div></div>
	';
	echo '<div class="recentlyused-content-'.$col.'-'.$key.' recently-used-tools-widget" style="max-height: 165px;overflow-y: auto;">';
	//loop through applications
	echo '<table>';
	foreach ($data as $k => $value) {

		$id = $value['applicationDbId'];
		$label = $value['label'];
		$actionLabel = $value['actionLabel'];
		$icon = $value['icon'];
		$timestamp = (isset($value['timestamp'])) ? $value['timestamp'] : null;

		$datetime1 = new DateTime($timestamp);
		$datetime2 = new DateTime();
		$dashboardModel = new Dashboard();
		$int = $dashboardModel->getFormattedTimeInterval($datetime1,$datetime2);

		$applicationModel = new Application();
		$url = $applicationModel->geturlbyappid($id,$program); //process url of application
		$ops = $applicationModel->getOptionsAppId($id); //process url of application
		$ops['iconStyle'] = 'font-size:135%';
		$favoritesString = ($p == 'configure') ? '' : FavoritesWidget::widget($ops);

		$item = '<tr id="tr-recenltyused-'.$id.'-'.$k.'-'.$col.'-'.$key.'"><td style="padding:0px 0px 3px 0px;width:25px">'.$favoritesString.'</td><td style="padding:0px 0px 3px 0px"><a class="yii1-recent-link" href="'. Url::to([$url]) .'" title="'.\Yii::t('app', 'Click to go to').' '.\Yii::t('app', $label).'"><span class="my-recentlyused-tools" style="display:block;margin:0px 5px 10px 0px"id="recenltyused-'.$id.'-'.$k.'-'.$col.'-'.$key.'">'.
			'<i class="material-icons pull-left">'.
					
			$icon.'</i>&nbsp;
			<span style="font-size:110%;line-height:1.5;color:#333" class="recentlyused-spec-tool">'.\Yii::t('app', $label).'</span>
			<span class="text-muted pull-right time-interval" title="'.$int.'">'.$int.' ago</span></span>

		</span></a></td></tr>';

		echo $item;
	}
	echo '</table></div>';
}

$this->registerJs(<<<JS

//filter recently used
$('#search_recentlyused_widget_{$col}_$key').on('change keyup paste click', function () {
	searchTerm = $(this).val();

	$('.recentlyused-content-$col-$key .my-recentlyused-tools').each(function (index) {
		toolId = '#' + $(this).attr('id');
		trToolId = '#tr-' + $(this).attr('id');

		if(searchTerm.length == 0){
			$(trToolId).show();
		}else if ($(toolId + ' > .recentlyused-spec-tool:containsCaseInsensitive(' + searchTerm + ')' ).length > 0 && searchTerm.length > 0) {
			$(trToolId).show();
		}else{
			$(trToolId).hide();
		}
	});
});

$.expr[':'].containsCaseInsensitive = function (a, i, m) {
	return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};

//show filter input
$('.search-recently-used-widget').on('click', function () {
	$('.search-recentlyused-filter-$col-$key, .hide-recentlyused-filter-$col-$key').css('display','block');
});

//hide filter input
$('.hide-recentlyused-filter-$col-$key').on('click', function () {
	$('.search-recentlyused-filter-$col-$key, .hide-recentlyused-filter-$col-$key').css('display','none');
	$('#search_recentlyused_widget_{$col}_$key').val('');
	$( "#search_recentlyused_widget_{$col}_$key" ).trigger("change");
});

JS
);
?>

<style type="text/css">
.with-padding {
    margin: 10px 0px 10px 0px;
}

.text-muted{
	overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}

@media only screen and (max-width: 1600px){
	.time-interval{
		max-width: 150px;
	}
}
@media only screen and (max-width: 1366px){
	.time-interval{
		max-width: 120px;
	}
}

</style>