<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders selection tool
 */
use yii\helpers\Url;
use app\models\Application;

if(empty($applications)){
	echo '<p><br/>'.\Yii::t('app','There are no applications yet in this activity.').'</p>';
}else{

	//loop through applications
	foreach ($applications as $key => $value) {
		$id = $value['applicationDbId'];
		$label = $value['label'];
		$actionLabel = $value['actionLabel'];
		$icon = $value['icon'];

		$application = new app\models\Application();
		$url = $application->geturlbyappid($id,$program); //process url of application

		$item = '<div class="col col-md-4 bc-select-tool" id="bc-select-tool'.$id.'">
			<a href="'.Url::to([$url]).'">
				<div class="card-panel row">
					<div class="col col-md-3">
						<i class="material-icons  metrics-icon">'.$icon.'</i>
					</div>
					<div class="col col-md-9 spec-bc-select-tool">
						<h5>'.\Yii::t('app', $label).'</h5>
						<span class="text-muted">'.\Yii::t('app', $actionLabel).'</span>
					</div>
				</div>
			</a>
		</div>';

		echo $item;
	}
}
?>