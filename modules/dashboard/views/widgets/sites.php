<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders sites widget in dashboard
 *
 */

?>
<div id="map_div"></div>
<script>
//show study locations on map
var data = JSON.parse('<?php echo json_encode($locations);?>');

if(data.length){
	google.charts.load("current", {packages:["map", "corechart", "orgchart"]});
	google.charts.setOnLoadCallback(drawMapChart);

	function drawMapChart() {
		var $arr = [];
		$arr.push(['Lat', 'Long', 'Name']);

		$.each(data, function(index){
			$arr.push(data[index]);
		});

		var datag = google.visualization.arrayToDataTable(
			$arr
		);

		var map = new google.visualization.Map(document.getElementById('map_div'));
		map.draw(datag, {
			showInfoWindow: true
		});
	}
}else{
	$('.SITES > .box-body').html('&emsp;&emsp;<i>There are no studies.</i>');
}
</script>