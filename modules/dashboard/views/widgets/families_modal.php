<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders list of families based from defined data filters
 */

use yii\grid\GridView;
?>
<div id="modal-top"></div>
<div class="families-table">
    <p><?= \Yii::t('app', 'Below are the crosses and their progenies in your program.') ?></p>
    <a class="btn-floating waves-effect waves-light grey pull-right export-families" title="<?= \Yii::t('app', 'Export all data to csv file') ?>" style="margin-top: -32px;">
        <em class="material-icons">file_download</em>
    </a>

    <?= GridView::widget([
        'dataProvider' => $data,
        'id' => 'view-families-table',
        'layout' => '{summary}{items}',
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'cross_name',
            'parentage',
            'female',
            'male',
            'year',
            [
                'attribute' => 'count',
                'header' => \Yii::t('app', 'No. of Progenies'),
                'format' => 'raw',
                'value' => function ($model) {
                    $count = $model['count'];
                    if($count > 0 && isset($model['gpid1']) && !empty($model['gpid1'])){
                        return '<a href="#modal-top" data-gpid1="'.$model['gpid1'].'" data-cross="'.$model['cross_name'].'" class="view-progenies-from-families" title="'.\Yii::t('app', 'View progenies').'"><b>'.$model['count'].'</b></a>';
                    }else{
                        return $count;
                    }
                }
            ]
        ]
    ]) ?>
</div>

<div class="progenies-table">

</div>
<?php
//display remaining count 
$remaining = ($data->totalCount - 20000);

if($remaining > 0){ //if more than 20,000
    echo '<i>'.number_format($remaining).' more...</i>';
}
?>