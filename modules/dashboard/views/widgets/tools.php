<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders tools widget
 */
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Application;
use app\controllers\SiteController;
use kartik\sortable\Sortable;
use kartik\select2\Select2;

$layoutObj = SiteController::getLayout(); //authenticate access token
extract($layoutObj);

$procTimestamp = str_replace(' ', '_', $timestamp);
$procTimestamp = str_replace(':', '_', $procTimestamp);

echo '<div style="width:60%;margin-bottom:5px">'.Select2::widget([
    'name' => 'data_filter_tool',
    'id' => 'tool_select_'.$procTimestamp,
    'maintainOrder' => true,
    'options' => [
        'placeholder' => \Yii::t('app', 'Select to add tool'),
        'tags' => true
    ]
]).'</div>';
    
if(empty($data)){
	echo '<p><br/>'.\Yii::t('app', 'There are no applications added yet.').'</p>';
}else{	
	echo '<div class="row search-tools-filter-'.$col.'-'.$key.'-'.$procTimestamp.'" style="margin:0px;display:none;"><div class="col col-md-11" style="padding-left:0"><input placeholder="'.\Yii::t('app', 'Search').'" id="search_tools_widget_'.$col.'_'.$key.'-'.$procTimestamp.'" style="margin-top:-20px;" autofocus></input></div>
		<div class="col col-md-1" style="padding-left:0">
		<a href="#!" title="'.\Yii::t('app', 'Hide search').'" class="text-muted fixed-color hide-tools-filter-'.$col.'-'.$key.'-'.$procTimestamp.'" style="display:none"><i class="material-icons widget-icons">close</i></a></div></div>
	';
	echo '<div class="tools-content-'.$col.'-'.$key.'-'.$procTimestamp.'" style="max-height:217px;overflow-y:auto">';
	$items = [];
	//reorder tools
	$reorderUrl = Url::to(['/dashboard/tools/reorderintoolswidget']);
	$pluginEvents = [
	    'sortupdate' => "function() { 
	    	var itemsArr = [];
			$('#tools-tools-sort-{$procTimestamp}').find('li').each(function(e){
				var obj = $(this);
				var idStr = obj.attr('id');

				if(idStr !== undefined){
					var idStr = idStr.split('_');
					var id = idStr[1];

					var data = id;
					itemsArr.push(data);
				}
			});

			$.ajax({
		        url: '$reorderUrl',
		        type: 'POST',
		        async: true,
		        data: {
		        	app_ids: itemsArr,
		        	user_id: $userId,
		        	timestamp: '$timestamp'
		        },
		        success: function(data) {
		        },
		        error: function(){
		            console.log('There was a problem while loading content.'); 
		        }
			}); 
	    }",
	];

	$itemsStr='';
	$colClass = (!empty($layout) && $layout == 'one') ? '2' : '3';
	$addClass = (!empty($layout) && $layout == 'one') ? 'one' : '';
	$class=(empty($p)) ? 'grid-item ' : 'col col-md-'.$colClass;
	$hidden=(empty($p)) ? '' : 'hidden';
	//loop through applications
	foreach ($data as $k => $value) {

		$id = $value['applicationDbId'];
		$label = $value['label'];
		$actionLabel = $value['actionLabel'];
		$icon = $value['icon'];

		$application = new Application();
		$url = $application->geturlbyappid($id,$program); //process url of application

		$item = '<div class="'.$class.' my-tools-tools" title="'.\Yii::t('app', 'Click to go to').' '.\Yii::t('app', $label).'" style="white-space: nowrap;text-overflow: ellipsis;margin-bottom:-5px" id="tools-widget-'.$id.'-'.$k.'-'.$col.'-'.$key.'-'.$procTimestamp.'">

			<a href="#!" class="close remove-a-tool-from-tools-'.$procTimestamp.' '.$hidden.'" id="tools-close-'.$id.'-'.$k.'-'.$col.'-'.$key.'-'.$procTimestamp.'" type="button" style="margin:0px 4px;opacity:0;color:#fff !important;font-weight:normal;" data-appid="'.$id.'" data-timestamp="'.$timestamp.'" title="'.\Yii::t('app', 'Remove application').'">×</a>

			<a href="'. Url::to([$url]) .'" class="tools-tool-item yii1-tools-link dashboard-widget-item-theme-color" data-close_id="'.$id.'-'.$k.'-'.$col.'-'.$key.'-'.$procTimestamp.'">
				<div class="card-panel '. $darken .' white-text" style="text-align:center; padding: 5px 10px 15px 9px;">
					<i class="material-icons metrics-icon">'.$icon.'</i><br/>
					<span class="tools-spec-tool">'.\Yii::t('app', $label).'</span>
				</div>
			</a>
		</div>';

		$itemsStr = $itemsStr.$item;

		$items[] = [
			'options'=>['id'=>'tools-widget-item_'.$id.'_'.$procTimestamp,'style'=>'margin:0px'],
			'content'=>$item
		];
	}

	if(!empty($p)){
		echo $itemsStr;
	}else{
		echo Sortable::widget([ 
			'type'=>'grid',
		    'id'=>'tools-tools-sort-'.$procTimestamp,
		    'options'=>['class'=>'tools-tools-sort '.$addClass],
		    'itemOptions' => ['class'=>'widgets','title'=>\Yii::t('app', 'Drag and drop to reorder items.')],
		    'pluginEvents' => $pluginEvents,
		    'items'=>$items
		]);
	}
	echo '</div>';
}

$removeToolInWidgetUrl = Url::to(['/dashboard/tools/removetoolinwidget']);
$getFilterTagsTool = Url::to(['/dashboard/tools/gettoolstags']);
$addToolsUrl = Url::to(['/dashboard/tools/addtoolsinwidget']);
$resetYii1Url = Url::to(['/dashboard/default/resetyii1url','program'=>$program]);
$this->registerJs(<<<JS
//remove tools
$(document).on('click', '.remove-a-tool-from-tools-{$procTimestamp}', function(e) {
	var obj = $(this);
	var app_id = obj.data('appid');
	var timestamp = obj.data('timestamp');

	if(document.getElementById("tools-widget-item_"+app_id+"_"+"{$procTimestamp}") !== null){
		document.getElementById("tools-widget-item_"+app_id+"_"+"{$procTimestamp}").remove();
	}	

	$.ajax({
		url: '$removeToolInWidgetUrl',
		type: 'POST',
		async: true,
		data: {
			app_id: app_id,
			timestamp: timestamp
		},
		success: function(data) {
			// Refresh upon removing tool
			$("#dashboard-widget-{$col}-{$key}").box("reload");
		},
		error: function(){
		}
	});
});

//hover on an item in tools
$(".tools-tool-item").mouseover(function(){
	var obj = $(this);
	var close_id = obj.data('close_id');
    $('#tools-close-'+close_id).css("opacity", "0.3");
});

$(".remove-a-tool-from-tools-{$procTimestamp}").mouseover(function(){	
    $(this).css("opacity", "0.3");
});
//remove hover on an element in tools
$(".tools-tool-item").mouseout(function(){
	var obj = $(this);
	var close_id = obj.data('close_id');
    $('#tools-close-'+close_id).css("opacity", "0");
});

//filter tools
$('#search_tools_widget_{$col}_$key-$procTimestamp').on('change keyup paste click', function () {
	searchTerm = $(this).val();

	$('.tools-content-$col-$key .my-tools-tools').each(function (index) {
		toolId = '#' + $(this).attr('id');

		if(searchTerm.length == 0){
			$(toolId).closest('li').show();
		}else if ($(toolId + ' div.card-panel > .tools-spec-tool:containsCaseInsensitive(' + searchTerm + ')' ).length > 0 && searchTerm.length > 0) {
			$(toolId).closest('li').show();
		}else{
			$(toolId).closest('li').hide();
		}
	});
});

$.expr[':'].containsCaseInsensitive = function (a, i, m) {
	return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};

//show filter input
$('.search-tools-widget').on('click', function () {
	$('.search-tools-filter-$col-$key-$procTimestamp, .hide-tools-filter-$col-$key').css('display','block');
});

//hide filter input
$('.hide-tools-filter-$col-$key').on('click', function () {
	$('.search-tools-filter-$col-$key-$procTimestamp, .hide-tools-filter-$col-$key').css('display','none');
	$('#search_tools_widget_{$col}_$key-$procTimestamp').val('');
	$( "#search_tools_widget_{$col}_$key-$procTimestamp" ).trigger("change");
});

$(".yii1-tools-link").on("click", function (e) {
	e.preventDefault();
	var actionLink = $(this).attr("href");
	if(actionLink != undefined && actionLink != '#'){
	  $.ajax({
		url: '$resetYii1Url',
		type: 'post',
		dataType: 'json',
		async:true,
		data: {
			actionLink: actionLink
		},
		success: function(response) {
		  window.location.href = actionLink; 
		},
	  });
	}
       
});

$(document).ready(function(){
	//get tool tags
    $.ajax({
        url: '$getFilterTagsTool',
        type: 'post',
        dataType: 'json',
        data: {
        	timestamp: '$timestamp',
        	user_id: $userId
        },
        async: true,
        success: function(response) {
            option = response;
            var lookup = {};
            var stool = document.getElementById('tool_select_{$procTimestamp}');

            for(var i=0; i<option.length;i++){
                var newToolOption = document.createElement("option");

                if (!(parseInt(option[i]['id']) in lookup)) {

                    lookup[parseInt(option[i]['id'])] = 1;

                    newToolOption.value = parseInt(option[i]['id']);
                    newToolOption.innerHTML = option[i]['text'];
                    stool.options.add(newToolOption);
                }
            }
        },
        error: function() {
            console.log("ERROR");
        }
    });

    //upon selecting tool
    $( "#tool_select_{$procTimestamp}" ).change(function(){
    	var val = this.value;
    	$.ajax({
	        url: '$addToolsUrl',
	        type: 'post',
	        async: false,
	        data: {
				app_id: val,
				user_id: $userId,
				timestamp: '$timestamp'
			},
	        success: function(response) {
	        	refreshed = true;
	        },
	        error: function() {
	        }
	    });
  		$("#dashboard-widget-{$col}-{$key}").box("reload");
	});
});
JS
);
?>

<style type="text/css">
.tools-spec-tool{
	display: block;
	text-overflow: ellipsis;
	overflow: hidden;
}
.tools-tools-sort.sortable,.tools-tools-sort.one.sortable{
	border:none;
}
.tools-tools-sort.sortable li,.tools-tools-sort.one.sortable li{
    margin: 4px;
    padding: 0px 5px;
    border: 1px dashed #eee;
}

.tools-tools-sort.sortable li{
    width: 12.68%;
}

.tools-tools-sort.one.sortable li{
    width: 15.68%;
}

@media only screen and (min-width: 992px){
    .tools-tools-sort.sortable li{
	    width: 15.68%;
	}
}

@media only screen and (min-width: 992px){
    .tools-tools-sort.one.sortable li{
	    width: 15.68%;
	}
}

@media only screen and (min-width: 1100px){
    .tools-tools-sort.sortable li{
	    width: 20.99%;
	}
}

@media only screen and (min-width: 1100px){
    .tools-tools-sort.one.sortable li{
	    width: 15.99%;
	}
}

@media only screen and (min-width: 1200px){
    .tools-tools-sort.sortable li{
	    width: 22.99%;
	}
}

@media only screen and (min-width: 1200px){
    .tools-tools-sort.one.sortable li{
	    width: 15.99%;
	}
}

@media only screen and (min-width: 1366px){
    .tools-tools-sort.sortable li{
	    width: 23.35%;
	}
}

@media only screen and (min-width: 1366px){
    .tools-tools-sort.one.sortable li{
	    width: 16.35%;
	}
}

@media only screen and (min-width: 1700px){
    .tools-tools-sort.sortable li{
	    width: 23.79%;
	}
}

@media only screen and (min-width: 1700px){
    .tools-tools-sort.one.sortable li{
	    width: 16.29%;
	}
}
</style>