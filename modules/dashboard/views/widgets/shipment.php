<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders shipment widget in dashboard
 */
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\modules\dashboard\models\Shipment;

$columns = [
    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => '',
        'template' => '{view}',
        'buttons' => [
            'view' => function ($url,$model) {  
                return Html::a('<i class="fa fa-eye"></i>',
                    '#',
                    [
                        'class' => 'view-shipment-widget',
                        'title' => \Yii::t('app','View shipment'),
                        'data-shipment_id' => $model['id'],
                        'data-shipment_label' => $model['transaction_no'],
                        'data-target' => '#view-shipment-modal',
                        'data-toggle' => 'modal'
                    ]
                );
            }
        ]
    ],
    [
    	'attribute'=>'status',
    	'format'=>'raw',
    	'value'=> function($data){
    		return '<span title="'.$data['status'].'">'.Shipment::formatStatus($data['status']).'</span>';
    	}
    ],
    [
        'attribute'=>'transaction_no',
        'format'=>'raw',
        'value'=> function($data){
            $no = $data['transaction_no'];
            return '<span title="'.$data['transaction_no'].'">'.$no.'</span>';
        }
    ],
    'no_of_seeds',
    'category',
    [
        'attribute' => 'user_sender',
        'label' => 'Sender',
    ],
    [
        'attribute' => 'sender',
        'label' => 'Sender Program'
    ],
    'place',
    'receiver',
    [
        'attribute'=>'date_sent',
        'contentOptions' => [
            'style' => 'min-width:85px'
        ]
    ],
    'date_received'
];

echo '<div style="">';
//dynagrid configuration
$panel = [
    'after' => false
];
if($p=='configure'){
    $footer = ['footer'=>false];

    $panel = array_merge($panel,$footer);
}

$dynagrid = DynaGrid::begin([
	'options'=>['id'=>'shipments-widget-'.$col.'-'.$key],
	'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
    	'dataProvider'=>$dataProvider,
    	'pjax'=>true,
    	'panel'=> $panel,
    	'responsiveWrap'=>false,
	    'toolbar' =>  false
    ]
]);

DynaGrid::end();
echo '</div>';

//view shipment modal
Modal::begin([
    'id' => 'view-shipment-modal',
    'header' => '<h4 id="shipment-label"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
]);
echo '<div class="view-shipment-modal-body"></div>';
Modal::end();

//css
$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

//view shipment url
$viewShipmentUrl = Url::to(['/dashboard/shipment/viewshipment']);
$this->registerJs(<<<JS
$(document).on('click', '.view-shipment-widget', function(e) {
    var obj = $(this);
    var shipment_label = obj.data('shipment_label');
    var shipment_id = obj.data('shipment_id');

    $('#shipment-label').html('<i class="fa fa-info-circle"></i> ' + shipment_label);
    $('.view-shipment-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    
    //load content of view more information
    setTimeout(function(){ 
        $.ajax({
        url: '$viewShipmentUrl',
        data: {
            shipment_id: shipment_id,
        },
        type: 'POST',
        async: true,
        success: function(data) {
            $('.view-shipment-modal-body').html(data);          
        },
        error: function(){
            $('.view-shipment-modal-body').html('<i>There was a problem while loading record information.</i>'); 
        }
    });
    }, 100);
    
});

JS
);