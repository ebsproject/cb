<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders studies widget in dashboard
 */
use app\widgets\ExperimentInfo;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

$columns = [
    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => '',
        'template' => '{view}',
        'buttons' => [
            'view' => function ($url,$model) {
                $linkAttrs =  [
                    'id' => 'view-experiment-widget-'.$model['experimentDbId'],
                    'title' => \Yii::t('app', 'Click to view more information'),
                    'data-label' => $model['experimentName'],
                    'data-id' => $model['experimentDbId'],
                    'data-target' => '#view-experiment-widget-modal',
                    'data-toggle' => 'modal'
                ];
                return ExperimentInfo::widget([
                    'id' => $model['experimentDbId'],
                    'entityLabel' => '<i class="material-icons">remove_red_eye</i>',
                    'linkAttrs' => $linkAttrs
                ]);

            }
        ]
    ],
    [
    	'attribute'=>'experimentName',
    	'format'=>'raw',
    	'value'=> function($data){
            return '<span title="'.$data['experimentCode'].'">'.$data['experimentName'].'</span>';
    	}
    ],
    [
        'attribute'=>'experimentYear',
        'label' => 'year',
    ],
    [
        'attribute'=>'seasonName',
        'label' => 'season',
    ],
    [
        'attribute'=>'stageCode',
        'label' => 'stage',
    ],
    [
        'attribute'=>'experimentDesignType',
        'label' => 'design type',
    ],
];

echo '<div style="">';

//dynagrid configuration
$panel = [
    'after' => false
];

if($p=='configure'){
    $footer = ['footer'=>false];

    $panel = array_merge($panel,$footer);
}

$dynagrid = DynaGrid::begin([
	'options'=>['id'=>$study_type.'-'.$col.'-'.$key],
	'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
    	'dataProvider'=>$dataProvider,
    	'pjax'=>true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
            ]
        ],
    	'panel'=> $panel,
    	'responsiveWrap'=>false,
	    'toolbar' =>  false
    ]
]);

DynaGrid::end();
echo '</div>';

//view study modal
Modal::begin([
    'id' => 'view-study-modal',
    'header' => '<h4 id="entity-label"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);

echo '<div class="view-study-modal-body"></div>';
Modal::end();

//css
$this->registerCssFile("@web/css/search_tool.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-search-tool');

$viewEntityUrl = Url::to(['/search/default/viewentity']);
$this->registerJs(<<<JS
$(document).on('click', '.view-study', function(e) {
    var obj = $(this);
    var entity_label = obj.data('entity_label');
    var entity_id = obj.data('entity_id');
    var entity_type = obj.data('entity_type');

    $('#entity-label').html('<i class="fa fa-info-circle"></i> ' + entity_label + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $('.view-study-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    
    //load content of view more information
    setTimeout(function(){ 
        $.ajax({
        url: '$viewEntityUrl',
        data: {
            entity_id: entity_id,
            entity_type: entity_type,
            entity_label: entity_label
        },
        type: 'POST',
        async: true,
        success: function(data) {
            $('.view-study-modal-body').html(data);          
        },
        error: function(){
            $('.view-study-modal-body').html('<i>There was a problem while loading record information.</i>'); 
        }
    });
    }, 300);
    
});

//tabs
$(document).on('click', '#li-1', function(e) {
    $('#li-1').addClass('active');
    $('#li-2').removeClass('active');
    $('#li-3').removeClass('active');
    $('#li-4').removeClass('active');
    $('#li-5').removeClass('active');
    $('#li-6').removeClass('active');
    $('#li-7').removeClass('active');

    $('#view-entity-1').removeClass('hidden').addClass('active');
    $('#view-entity-2').removeClass('active').addClass('hidden');
    $('#view-entity-3').removeClass('active').addClass('hidden');
    $('#view-entity-4').removeClass('active').addClass('hidden');
    $('#view-entity-5').removeClass('active').addClass('hidden');
    $('#view-entity-6').removeClass('active').addClass('hidden');
    $('#view-entity-7').removeClass('active').addClass('hidden');
});
$(document).on('click', '#li-2', function(e) {
    $('#li-2').addClass('active');
    $('#li-1').removeClass('active');
    $('#li-3').removeClass('active');
    $('#li-4').removeClass('active');
    $('#li-5').removeClass('active');
    $('#li-6').removeClass('active');
    $('#li-7').removeClass('active');

    $('#view-entity-2').removeClass('hidden').addClass('active');
    $('#view-entity-1').removeClass('active').addClass('hidden');
    $('#view-entity-3').removeClass('active').addClass('hidden');
    $('#view-entity-4').removeClass('active').addClass('hidden');
    $('#view-entity-5').removeClass('active').addClass('hidden');
    $('#view-entity-6').removeClass('active').addClass('hidden');
    $('#view-entity-7').removeClass('active').addClass('hidden');
});
$(document).on('click', '#li-3', function(e) {
    $('#li-2').removeClass('active');
    $('#li-1').removeClass('active');
    $('#li-3').addClass('active');
    $('#li-4').removeClass('active');
    $('#li-5').removeClass('active');
    $('#li-6').removeClass('active');
    $('#li-7').removeClass('active');

    $('#view-entity-3').removeClass('hidden').addClass('active');
    $('#view-entity-1').removeClass('active').addClass('hidden');
    $('#view-entity-2').removeClass('active').addClass('hidden');
    $('#view-entity-4').removeClass('active').addClass('hidden');
    $('#view-entity-5').removeClass('active').addClass('hidden');
    $('#view-entity-6').removeClass('active').addClass('hidden');
    $('#view-entity-7').removeClass('active').addClass('hidden');
});
JS
);