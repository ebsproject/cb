<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file for the saved lists browser rendered in query tool
 */
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$actionColumns = [
    ['class' => 'yii\grid\SerialColumn'], 
    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => Yii::t('app', 'Actions'),
        'template' => '{select}',
        'buttons' => [
            'select' => function ($url, $model, $key) {
                return Html::a('<i class="material-icons">exit_to_app</i>',
                    '#',
                    [
                        'class'=>'select-list',
                        'title' => Yii::t('app', 'Select saved list'),
                        'data-saved_list_id' => $model["listDbId"],
                        'data-list_type' => $model["type"]
                    ]
                );
            },
        ]
    ],
];
// all the columns that can be viewed in seasons browser
$columns = [
    'abbrev',
    'name',
    [
        'header' => Yii::t('app', 'List Type'),
        'value' => function ($data) {
            return ucfirst($data["type"]);
        },
        'format' => 'raw'
    ],
    [
        'header' => Yii::t('app', 'No. of Members'),
        'value' => function ($data) {
            return '<span class="new badge">'.$data["memberCount"].'</span>';
        },
        'format' => 'raw'
    ],
    'remarks'
];  
$gridColumns = array_merge($actionColumns,$columns);
echo 
'<p>Duplicate values from lists will be removed.</p>'.
'<div style="background: white; height: 400px; position: relative; overflow-y: scroll">'.
$grid = GridView::widget([
    'pjax' => true, // pjax is set to always true for this demo
    'dataProvider' => $dataProvider,
    'id' => 'list-grid',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => $gridColumns,
    'toggleData'=>true,
]). '</div>';


$getItemsUrl = Url::to(['/query/default/get-items-from-lists']);

$this->registerJs(<<<JS
    var field = "$field";
    var getItemsUrl = "$getItemsUrl";
    var addlFldsOptions = $addlFldsOptions;

    $('.select-list').on('click', function(e) {
        e.preventDefault();
        $('#loading-area').html('<div class="progress"><div class="indeterminate"></div></div>');
        var listIds = [];
        var listId = $(this).attr('data-saved_list_id');
        var listType = $(this).attr('data-list_type');
        listIds.push(listId);

        var currentSelected = $('#query-column-name-list').val();
        var fldOptions = addlFldsOptions[currentSelected];
        var field = fldOptions['list_filter_field'];

        // render designation from saved lists
        $.ajax({
            url: getItemsUrl,
            data: {listIds: listIds, field:field, listType: listType},
            type: 'POST',
            dataType: 'json',
            success: function(response) {
                var data = response.data;
                var inputValues = data.replace(/,/g, '\\n');

                $('#loading-area').html('');
                $('#query-txt-area').val(inputValues).trigger('change');
                $('#current-list-count').val(response.count);
            }
        });
    })
JS
);

?>