<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\query\controllers;

use app\models\Config;
use app\models\PlatformList;
use app\modules\query\models\QueryModel;

use app\components\B4RController;

use yii\helpers\Url;
use Yii;

/**
 * Default controller for the `query` module
 */
class DefaultController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        public Config $configModel,
        public QueryModel $queryModel,
        $config = []
        )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Retrieve the filter data option for given target column
     * 
     * @param String $q the term that is searched for
     * @param String $targetColumn the target attribute for filtering
     * @param String $page the current page of the results
     * @param String $variable the variable information for the given column
     * @param String $paramType specifies if the target column is a basic or additional parameter
     * @return JSON the filter data in json format 
     */
    public function actionGetFilterData() {
       //$q=null,$page=null,$entity=null,$variable=null,$paramType='basic', $currentFld=null
        $entity = $_REQUEST['entity'];
        $variable = $_REQUEST['variable'];
        $currentFld = json_decode(json_encode($_REQUEST['currentFld']));
        $q = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';
        $config = $this->configModel->getConfigByAbbrev($entity);
        $data = $this->queryModel->buildDataOption($config, $variable, $currentFld, $q);
        return json_encode($data);
    }

    /**
     * Renders the select2 HTML field for each parameter
     * @return view file for the select 2 field
     */
    public function actionGetSelectField () {

        $columnName = $_POST['columnName'];
        $selectedOperator = $_POST['selectedOperator'];
        $selectedFldOptions = $_POST['selectedFldOptions'];
        $entity = $_POST['entity'];
        $currentValue = isset($_POST['currentValue']) && !empty($_POST['currentValue']) ? $_POST['currentValue'] : '';
        $dataUrl = Url::to(['/query/default/get-filter-data']);

        $defaultValue = $currentValue;
        $inputFld = $this->queryModel->generateHtmlElements((object)$selectedFldOptions, 'col-md-5', $dataUrl, $entity, 'additional', false, $defaultValue);

        return $this->renderAjax('select_field', [
            'inputFld' => $inputFld
        ], true);
    }

    /**
     * Retrieves the saved lists of the current user filtered by the given list type
     * @return view file containing the data browser for all the lists
     */
    public function actionRenderLists() {

        $type = $_POST['type'];
        $field = $_POST['field'];
        $addlFldsOptions = json_encode($_POST['addlFldsOptions']);
        $platformListModel = \Yii::$container->get('app\models\PlatformList');

        $typeParam = 'equals '. str_replace('|','|equals ',$type);

        $params = [];
        $params['MyList']['type']=$typeParam;

        $dataProvider = $platformListModel->search($params, "ownershipType=combined&sort=creationTimestamp:desc", false);

        return $this->renderAjax('saved_lists', [
            'dataProvider' => $dataProvider,
            'field' => $field,
            'addlFldsOptions' => $addlFldsOptions
        ], true);
    }

    /**
     * Retrieves the items from the given lists IDs
     * @return JSON contains the items and the total count
     */
    public function actionGetItemsFromLists() {

        $listIds = $_POST['listIds'];
        $listIds = implode(',', $listIds);
        $field = $_POST['field'];
        $listType = $_POST['listType'];
        $items = $this->queryModel->getItemsFromLists($listIds, $field, $listType);
        $count = count($items);
        $items = implode(',', $items);
        return json_encode(['data'=>$items, 'count'=>$count]);
    }
}
