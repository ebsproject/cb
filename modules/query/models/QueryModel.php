<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\query\models;
use Yii;

use app\models\Config;
use app\models\Variable;

use kartik\widgets\Select2;
use yii\data\SqlDataProvider;
use yii\helpers\Url;
use yii\web\JsExpression;


/**
 * Model class for Query tool
 */

class QueryModel 
{
    /**
     * Retrieves the corresponding standard data type for the given data type
     * 
     * @param String $dataType the specified data type
     * @return String the standard data type
     */
    public static function getStandardDataType ($dataType) {
        $dataType = strtolower($dataType);

        switch ($dataType) {
            case 'timestamp' :
            case 'date':
                return 'timestamp';
            break;
            case 'smallint':
            case 'float':
            case 'bigint':
            case 'integer':
            case 'numeric':
                return 'number';
            break;
            default:
                return 'string';
            break;
        }
    }

    /**
     * Generates the HTML input field for the given attribute from the configuration file
     * 
     * @param Object $fld the object information for the current field attribute
     * @param String $colDiv specified the class for the div element
     * @param String $dataUrl if the input field is select2, this is the link to where the data of the select2 will come from
     * @param String $entity the given entity for the specified browser
     * @param String $paramType if the field is basic or additional
     * @param String $displayLabel specifies whether to display the label
     * @param String $defaultValue default value for the input field
     * @param String string in HTML format
     */
    public static function generateHtmlElements($fld, $colDiv, $dataUrl, $entity, $paramType='basic', $displayLabel = true, $defaultValue='') {
        $initSelectionUrl = Url::to(['/query/default/init-selection']);
        $isMultiple = ($fld->input_type == 'multiple') ? true : false;
        $fldName = 'Query[' . $fld->fieldName . ']';
        $fldLabel = $fld->label;
        $fldClass = 'query-flds query-'.$paramType.'-flds';
        
        $label = ($displayLabel) ? '<label>'.$fldLabel.'</label>' : '';
        $colDiv = ($paramType == 'basic') ? 'col ' . $colDiv : $colDiv;
        
        $fldDiv = '<div class="'.$colDiv.'" style="max-width:50%">'.$label;
        $defaultValueId = isset($defaultValue) ? $defaultValue : '';
        $defaultValueText = '';
        if ($fld->input_field == 'selection') {
            if (!empty($defaultValueId)) {
                $defaultValueArr = QueryModel::initSelection($fld, $entity, $fld->variable_abbrev, $defaultValueId, $isMultiple);
                if (isset($defaultValueArr['defaultValueId']) && isset($defaultValueArr['defaultValueText'])) {
                    $defaultValueId = $defaultValueArr['defaultValueId'];
                    $defaultValueText = $defaultValueArr['defaultValueText'];
                }
            }

            $select2Options = [
                'name' => $fldName,
                'data' => null,
                'size' => Select2::SMALL,
                'value' => $defaultValueId,
                'options' => [
                    'id' => 'query-fld-'.$fld->variable_abbrev,
                    'multiple' => $isMultiple,
                    'class' => $fldClass,
                    // 'data-target-column' => $fld->target_column,
                    'data-variable-abbrev' => $fld->variable_abbrev,
                    'data-column-name' => $fld->fieldName
                ],
                'pluginOptions' => [
                    'allowClear' => ($fld->required) ? false :  true,
                    'minimumInputLength' => 0,
                    'placeholder' => 'Search value',
                    'language' => [
                        'errorLoading' => new JsExpression("function(){ return 'Something went wrong...'; }")
                    ],
                    'ajax' => [
                        'url' => $dataUrl,
                        'dataType' => 'json',
                        'delay' => 150,
                        'type'=>'POST',
                        'data' => new JsExpression(
                            'function (params) {

                                var variableAbbrev = $(this).attr("data-variable-abbrev");
                                return {
                                    q: params.term,
                                    page: params.page,
                                    entity: '."'" . $entity. "'".',
                                    variable: variableAbbrev,
                                    paramType: '."'".$paramType."'".',
                                    currentFld: '.json_encode($fld).'
                                }
                            }'
                        ),
                        'processResults' => new JSExpression(
                            'function (data, params) {
                                
                                params.page = params.page || 1;
        
                                return {
                                    results: data.items,
                                    // pagination: {
                                    //     more: (params.page * 5) < data.totalCount
                                    // },
                                };
                            }'
                        ),
                        'cache' => true
                    ],
                    'templateResult' => new JSExpression(
                        'function formatRepo (repo) {
                            if (repo.loading) {
                                return repo.text;
                            }
        
                            return repo.text;
                        }'
                    ),
                    'templateSelection' => new JSExpression(
                        'function formatRepoSelection (repo) {
                            return repo.text;
                        }'
                    )
                ]
            ];

            if (!empty($defaultValueText)) {
                $select2Options['initValueText'] = $defaultValueText;
            }
            $fld = Select2::widget($select2Options, true);
        } else {
            $defaultValueId = str_replace('|', ',', $defaultValueId);
            $fld= '<input 
                name="'.$fldName.'" 
                data-variable-abbrev="'.$fld->variable_abbrev.'"
                data-column-name="'.$fld->fieldName.'"
                id="query-fld-'.$fld->variable_abbrev.'"
                class="'.$fldClass.'"
                value="'.$defaultValueId.'"
                placeholder=""
                type="text">
                Separate values with comma (,) eg. value1, value2. You may use the wildcard %
            ';
        }

        $fld = $fldDiv . $fld. '</div>';
        return $fld;
    }

    /**
     * Initializes the selection for the current filters that are saved in session
     * 
     * @param String $targetColumn the specified column
     * @param String $entity the given entity for the specified browser
     * @param String $variable the variable abbrev for the given column
     * @param Integer $id the ID of the currently selected value
     * @param Boolean $isMultiple specifies whether the input field accepts multiple values
     * @return Array contains the data selection for the target column
     */
    public static function initSelection($currentConfigFld,$entity,$variable,$id, $isMultiple) {
        $config = new Config();
        $config = $config->getConfigByAbbrev($entity);
        $data = QueryModel::buildDataOption($config, $variable, $currentConfigFld, "", $id);
        $returnVal = '';
        $defaultValueId = [];
        $defaultValueTxt = [];

        if (!empty($data['items'])) {
            if ($isMultiple) {

                foreach ($data['items'] as $item) {
                    $defaultValueId[] = $item['id'];
                    $defaultValueTxt[] = $item['text'];
                }

                $returnVal = ['defaultValueId' => $defaultValueId, 'defaultValueText' => $defaultValueTxt];
            } else {
                $returnVal = ['defaultValueId' => $data['items'][0]['id'], 'defaultValueText' => $data['items'][0]['text']];
            }
        }
        return $returnVal;
    }

    /**
     * Method for building data option for the dropdown UI in query tool
     * @param String $config abbrev of the configuration record
     * @param String $variable abbrev of the variable
     * @param JSON $currentConfigFld json object for the current config
     * @param String $term lookup term for the dropdown inputted by the user
     * @param String $currentSelected if session is not empty, this is the default value
     * @return Array contains the items and the total count
     */
    public static function buildDataOption($config, $variable,$currentConfigFld, $term, $currentSelected=null) {
        $apiResourceMethod = $currentConfigFld->api_resource_method;
        $apiResourceEndpoint = $currentConfigFld->api_resource_endpoint;
        $apiResourceOutput = $currentConfigFld->api_resource_output;
        $apiResourceBody = isset($currentConfigFld->api_resource_body) ? $currentConfigFld->api_resource_body : '';

        $output = QueryModel::processApiCall($apiResourceMethod, $apiResourceEndpoint, $apiResourceOutput, $apiResourceBody);

        if (isset($currentConfigFld->secondary_resource_endpoint) && !empty($currentConfigFld->secondary_resource_endpoint)) {
            $apiResourceMethod = $currentConfigFld->secondary_resource_endpoint_method;
            $apiResourceEndpoint = $currentConfigFld->secondary_resource_endpoint;
            $apiResourceOutput = $currentConfigFld->secondary_resource_output;
            $apiResourceBody = isset($currentConfigFld->secondary_resource_body) ? $currentConfigFld->secondary_resource_body : '';
            if (isset($currentConfigFld->secondary_resource_replacements) && !empty($currentConfigFld->secondary_resource_replacements)) {
                foreach ($currentConfigFld->secondary_resource_replacements as $key => $value) {
                    if ($value == 'api_resource_output') {
                        $apiResourceEndpoint = str_replace($key, $output, $apiResourceEndpoint);
                    } else {
                        $apiResourceEndpoint = str_replace($key, $value, $apiResourceEndpoint);
                    }
                }
            }

            $output = QueryModel::processApiCall($apiResourceMethod, $apiResourceEndpoint, $apiResourceOutput, $apiResourceBody);
        }

        $items = [];
        $term = strtolower($term);
        $currentSelected = strtolower($currentSelected);
        foreach ($output as $o) {
            if (!empty($currentSelected)) {
                // check for filters from session upon 1st load
                if(preg_match('/\b('.$currentSelected.')\b/', strtolower($o)) !== 1) { 
                    continue;
                }
            }
            if (!empty($term)) {
                // if dropdown is filtered, check if current value contains the searched word
                if(strpos(strtolower($o), $term) === false){
                    continue;
                }
            }

            $items[] = [
                'id' => $o,
                'text' => $o
            ];
        }
        $totalCount = count($output);

        $returnVal = [
            'totalCount' => $totalCount,
            'items' => $items
        ];
        return $returnVal;
    }

    /**
     * Method for making API calls based on the configuration
     * @param String $method indicates the method for making the API request
     * @param String $path the specific API endpoint
     * @param Array/String $requestBody the request body for the API; empty string by default
     * @param Array $output the expected output from the API
     */
    public static function processApiCall($method, $path, $output, $requestBody=null) {

        $requestBody = empty($requestBody) ? '' : $requestBody;
        $response = Yii::$app->api->getResponse($method, $path, json_encode($requestBody), '', true);
        if ($response['status'] !== 200) {
            return [
                'output' => []
            ];
        }
        $responseBody = $response['body'];
        $responseBody = (isset($responseBody['result']['data'])) ? $responseBody['result']['data'] : [];
        if (empty($responseBody)) {
            return [
                'output' => []
            ];
        }
        $outputArr = explode('->', $output);
        $tempResponseBody = [];
        $output = [];
        foreach ($outputArr as $outputKey) {
            if (isset($responseBody[$outputKey])) {
                $responseBody = $responseBody[$outputKey];
            } else {
                $tempResponseBody = [];
                foreach ($responseBody as $arr) {
                    if (!array_key_exists($outputKey, $arr)) {
                        $responseBody = [];
                        $output = [];
                        break;
                    }
                    if (trim($arr[$outputKey]) !== '') {
                        $tempResponseBody[] = $arr[$outputKey];
                    }
                }
                $responseBody = $tempResponseBody;
            }
        }

        if (!empty($responseBody)) {
            $output = $responseBody;
        }

        return $output;
    }

    /**
     * Retrieves the list members from the given lists IDs
     * 
     * @param String $field specifies what column should be returned
     * @param String $listType the type of the lists to be retrieved
     * @return Array $results the distinct items from the lists
     */
    public static function getItemsFromLists($listDbId, $field, $listType) {
        $url = 'lists/'.$listDbId.'/members-search';
        $params = [
            "fields"=>"$field|listMember.order_number AS orderNumber"
        ];
        
        $results = [];
        $response = Yii::$app->api->getResponse('POST', $url, json_encode($params), 'sort=orderNumber', true);
        if ($response['status'] == 200) {
            $data = $response['body'];
            $totalPages = (isset($data['metadata']['pagination']['totalPages'])) ? $data['metadata']['pagination']['totalPages'] : null;
            $data = (isset($data['result']['data'])) ? $data['result']['data'] : [];
        } else {
            $totalPages = 0;
            $data = [];
        }

        $fld = explode('.', $field);
        $fld = isset($fld[1]) ? $fld[1] : $fld[0];
        foreach ($data as $d) {
            // Commented out to allow duplicates
            // if(!in_array($d[$fld], $results)) {
            $results[] = $d[$fld];
            // }
        }
        return $results;
    }
}