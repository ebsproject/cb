<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* Renders browser and manage sorts page
*/
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\sortable\Sortable;

\yii\jui\JuiAsset::register($this);

?>

<div class="container sort-action-modal-container">
    <div class="row sort-modal-row">
        <div class="col s4">
            <div class="row">
                <h6>Sort Name</h6>
                <label>Select sorts for this data browser.</label>
                <!-- Select2 widget -->
                <?php
                    echo Select2::widget([
                        'name' => 'sort_select',
                        'id' => 'sort_select',
                        'data' => [],
                        'options' => [
                            'placeholder' => 'Select Sort Configuration'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => new yii\web\JsExpression('$("#sort-action-modal")')
                        ]
                    ]);
                ?>
            </div>
            <!-- Buttons -->
            <div class="row btn-container">
                <button id="save-sort-btn" class="btn manage-sorts-btn btn-default" style="display: none;"><span>Save</span> <i class="material-icons">save</i></button>
                <button id="delete-sort-btn" class="btn manage-sorts-btn btn-danger" style="display: none;" data-toggle="modal" data-target="#deleteSortModal"><span>Delete</span> <i class="material-icons">delete_forever</i></button>
            </div>
        </div>
        <div class="col s8">
            <div class="row sort-modal-row">
                <!-- Sort Order -->
                <div>
                    <h6>Sort Order</h6>
                    <label>Select and arrange the column/s that you want to sort.</label>
                    <div class="sort-container">
                        <div class="container sort-action-modal-container sort-view-container" style="background-color: white; height: 275px; position: relative; overflow-y: scroll">
                            <div style="max-height: 100%;">
                                <ul id="sort-order-list">
                                </ul>
                            </div>
                        </div>
                        <button id="sort-add-column-btn" class="btn manage-sorts-btn btn-default"><span>Add column</span></button>
                        <button id="sort-clear-all-column-btn" class="btn manage-sorts-btn btn-default" disabled><span>Clear all</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade custom-modal" id="deleteSortModal" tabindex="-1" role="dialog" aria-labelledby="deleteSortModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteSortModalLabel">Delete <span id="deleteSortModalTitle">Sort</span>?</h5>
            </div>
            <div class="modal-body">
                <p><span id="deleteSortModalContent">Sort</span> will be deleted from your saved sort configurations. This action cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <a class="close-cancel-dialog-btn">Cancel</a> &nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-danger" id="delete-sort-final-btn">Delete</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade custom-modal" id="saveSortModal" tabindex="-1" role="dialog" aria-labelledby="saveSortModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="saveSortModalLabel">Save sort</h5>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                    <input type="text" class="form-control" id="new-sort-name-input" placeholder="Enter name of sort">
                </form>
            </div>
            <div class="modal-footer">
                <a class="close-cancel-dialog-btn">Cancel</a> &nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-primary" id="save-sort-final-btn" disabled>Save</button>
            </div>
        </div>
    </div>
</div>


<?php
// Get the dataBrowserId and columnNames from the data browser
$dataBrowserId = json_encode($dataBrowserId);
$columnNames = json_encode($columnNames);

$getSortsUrl = Url::to(['/userSort/data-browser-sort/get-sorts']);
$saveSortsUrl = Url::to(['/userSort/data-browser-sort/save-sorts']);
$deleteSortsUrl = Url::to(['/userSort/data-browser-sort/delete-sorts']);

$this->registerJs(<<<JS
var savedUserSortConfigs = [];
var currentUserSortConfig = [];
var currentIsUnsaved = false;
var currentSelectedIndex = -1;
var columnNames = JSON.parse('$columnNames');
var sortSelected = {};
var sortColumnCurrentCount = 0;
var currentlySavingSort = false;
var currentlyDeletingSort = false;
var filtersInUrl = '';

/**
* Retrieves the current sort configurations based on the user's data browser.
*/
function retrieveCurrentConfig() {
    // Use url to get value and order of each sort column
    url = window.location.search;
    // catch case where a space (' ') is replaced by a '+' by the browser
    url = url.startsWith('?')? url.slice(1): url;
    url = url.replace(/\+/g, '%20');

    // Get filtersInUrl
    var filtersUrl = url.split('&');
    for(var val of filtersUrl) {
        if(val.indexOf('sort') === -1 && '$browserUrl'.indexOf(val) === -1) {
            filtersInUrl += '&'+val;
        }
    }
    filtersInUrl = filtersInUrl.slice(1);

    url = decodeURIComponent(url);
    url = url.split('&');
    for(var val of url) {
        if(val.indexOf('sort') !== -1) {
            val = val.split('=');
            val = val[1].split('|');
            for(var columnName of val) {
                currentUserSortConfig.push(columnName);
            }
        }
    }
}

/**
* Sets the Select2 widget's options to the sortNames parameter
*
* @param sortNames array containing the names of saved (and unsaved) sorts
*/
function setSortSelection(sortNames) {
    // Clear Select2 widget
    $('#sort_select').empty();

    // Populate Select2 by appending sortNames
    for(var index in sortNames) {
        $('#sort_select').append(new Option(sortNames[index], index));
    }

    // Apply Changes
    $('#sort_select').val('').trigger('change');
    currentSelectedIndex = $('#sort_select').val();
}

/**
* Initialize each column in sortSelected to false.
*/
function buildSortSelected() {
    for(var col of columnNames) {
        sortSelected[col] = false;
    }
}

/**
* Enables/disables the options of all sort column name select widget.
*/
function updateDisabledSortNamesInSelect() {
    $('#sort-order-list li').each(function(listIndex, listElement) {
        var selectElement = $(listElement).find('.sort-column-name-select');
        for(var colIndex in columnNames) {
            if(sortSelected[columnNames[colIndex]] == true) {
                selectElement.children().eq(colIndex).prop('disabled', true);
            } else {
                selectElement.children().eq(colIndex).prop('disabled', false);
            }
        }
    });
}

/**
* Adds a row to the sort order list.
*
* @param sortConditionColumn object containing the properties (isNew, columnName, columnOrder, value, element) of the sort
*/
function addSortColumn(sortConditionColumn) {
    sortColumnCurrentCount += 1;

    $('#sort-order-list'). append('<li>'
        +'<div class="container sort-action-modal-container sort-view-container">'
            +'<div class="row sort-modal-row">'
                +'<div class="col s1">'
                    +'<button class="btn btn-danger remove-sort-button"><i class="material-icons">remove</i></button>'
                +'</div>'
                +'<div class="col s5">'
                    +'<select class="sort-column-name-select">'
                    +'</select>'
                +'</div>'
            +'</div>'
        +'</div>'
    +'</li>');

    // Automatically selects the last element because it is newly appended
    var listElement = $('#sort-order-list');
    var listItemIndex = listElement.children().length - 1;
    var selectElement = listElement.children().eq(listItemIndex).find('.sort-column-name-select');

    var selectData = [];
    for(var colName of columnNames) {
        if (colName.toUpperCase() === colName){
            selectData.push({
                id: colName,
                text: colName.endsWith('Abbrev')? colName.slice(0, -6).toUpperCase(): colName.replace( /([A-Z])/, " $1" ).toUpperCase()
            });
        }
        else{
            selectData.push({
                id: colName,
                text: colName.endsWith('Abbrev')? colName.slice(0, -6).toUpperCase(): colName.replace( /([A-Z])/g, " $1" ).toUpperCase()
            });
        }
    }

    selectElement.select2({
        data: selectData,
        dropdownParent: $('#sort-action-modal')
    });

    if(sortConditionColumn['isNew'] == true) {
        // Auto select first non-disabled option
        for(var colIndex in columnNames) {
            if(sortSelected[columnNames[colIndex]] == false) {
                selectElement.val(columnNames[colIndex]).trigger('change');
                sortSelected[columnNames[colIndex]] = true;
                break;
            }
        }

        // Disable other (already) selected options
        updateDisabledSortNamesInSelect();

        // Then add possible conditions (new or from saved sort configs)
        addSortColumnOrderSelect({
            'isNew': true,
            'element': selectElement
        });

    } else {
        var colName = sortConditionColumn['columnName'];
        var colOrder = sortConditionColumn['columnOrder'];
        selectElement.val(colName).trigger('change');
        sortSelected[colName] = true;

        // Disable other (already) selected options
        updateDisabledSortNamesInSelect();

        // Then add possible conditions (new or from saved sort configs)
        addSortColumnOrderSelect({
            'isNew': false,
            'element': selectElement,
            'columnOrder': colOrder
        });
    }

    // Disable the add button if all column names are selected.
    if(sortColumnCurrentCount == columnNames.length) {
        $(sortConditionColumn['element']).prop('disabled', true);
    }
    // enable clear all button
    $('#sort-clear-all-column-btn').prop('disabled', false);

}

/**
* Adds a sort order select element to the sort row.
*
* @param sortConditionColumnOrder object containing the properties (isNew, columnCondition, columnValue, element) of the sort column name
*/
function addSortColumnOrderSelect(sortConditionColumnOrder) {
    var columnNameSelectElement = sortConditionColumnOrder['element'];

    // Remove column order select elements to the right if they exist
    if($(columnNameSelectElement).parent().next().length) {
        $(columnNameSelectElement).parent().nextAll().remove();
    }

    var chosenColumnName = $(columnNameSelectElement).select2('data')[0].id;

    $(columnNameSelectElement).parents().eq(1).append('<div class="col s5">'
        +'<select class="sort-column-order-select">'
        +'</select>'
    +'</div>'
    +'<div class="col s1">'
        +'<i class="material-icons sort-order-icon">drag_handle</i>'
    +'</div>');
    var listElement = $('#sort-order-list');
    var listItemIndex = columnNameSelectElement.parents().eq(3).index();
    var columnConditionSelectElement = listElement.children().eq(listItemIndex).find('.sort-column-order-select');

    var columnConditionChoices = ['ascending', 'descending'];

    var selectData = [];
    for(var colCondition of columnConditionChoices) {
        selectData.push({
            id: colCondition,
            text: colCondition
        });
    }

    columnConditionSelectElement.select2({
        data: selectData,
        minimumResultsForSearch: -1, // Removes Select2 search field
        dropdownParent: $('#sort-action-modal')
    });
    columnConditionSelectElement.next().css('width', '100%');
    if(sortConditionColumnOrder['isNew'] == true) {
        columnConditionSelectElement.val('ascending').trigger('change');

        currentUserSortConfig.push(chosenColumnName);
    } else {
        var colOrder = sortConditionColumnOrder['columnOrder'];

        columnConditionSelectElement.val(colOrder).trigger('change');
    }
}

/**
* Compares if two arrays are equal in length, order, and values
*
* @param json curr The array containing the data of the current sort configuration
* @param json rowData The array containing the data of the current row the current sort configuration is being compared to
* @return boolean True or False Whether both arrays are the same or not
*/
function sortDataIsEqual(curr, rowData) {
    // Checks if both variables are arrays
    if(!Array.isArray(curr) || !Array.isArray(rowData)) {
        return false;
    }

    // Compares if the two arrays are equal in length
    if(curr.length != rowData.length) {
        return false;
    }

    // Compares the order or the columns and their respective orders (ascending/descending)
    if(curr.toString() != rowData.toString()) {
        return false;
    }

    return true;
}

/**
* Finds the index of the current sort configuration in the user's saved sort configurations
*
* @param json currentConfig The array containing the current sort configuration
* @param array sortConfig The array containing the user's saved sort configurations
* @return integer The index of the current sort configuration in user's saved sort configurations, if not found then -1
*/
function findCurrentConfigInSavedConfigs(currentConfig, savedConfigs) {
    for(var index in savedConfigs) {
        // if found, return index
        if(sortDataIsEqual(currentConfig, savedConfigs[index]['data']['__sort__'])) {
            return index;
        }
    }

    // if not found, return -1
    return -1;
}

/**
* Sets the Select2 widget to the index of a selected sort
*
* @param integer index The index of the sort
*/
function updateSortSelection(index) {
    // Set Select2 widget value to index and apply change
    $('#sort_select').val(index).trigger('change');
    currentSelectedIndex = $('#sort_select').val();
}

/**
* Add *unsaved at the sort selection if it does not exist and select it whenever a user modifies a sort configuration.
*/
function addUnsavedSortSelectionForSorts() {
    var index = $('#sort_select').val();

    if(currentIsUnsaved) {
        if(index == 0) {
            // Use currentUserSortConfig
        } else {
            if(index == null) { // No sort is selected
                currentUserSortConfig = [];
            } else {
                if (typeof [...savedUserSortConfigs][index - 1] === 'undefined') {
                    currentUserSortConfig = [];
                } else {
                    currentUserSortConfig = [...savedUserSortConfigs[index - 1]['data']['__sort__']];
                }
            }
            // Select *unsaved in select2
            updateSortSelection(0);
            $('#save-sort-btn').show();
            $('#delete-sort-btn').hide();
        }
    } else {
        if(index == null) { // No sort is selected
            currentUserSortConfig = [];
        } else {
            if (typeof [...savedUserSortConfigs][index] === 'undefined') {
                currentUserSortConfig = [];
            } else {
                currentUserSortConfig = [...savedUserSortConfigs[index]['data']['__sort__']];
            }
        }
        // Get sort names of savedUserSortConfigs
        var sortNames = [];
        for(var savedSort of savedUserSortConfigs) {
            sortNames.push(savedSort['name'])
        }
        // Create *unsaved and select it in select2
        sortNames.unshift('*unsaved');
        setSortSelection(sortNames);
        updateSortSelection(0);
        $('#save-sort-btn').show();
        $('#delete-sort-btn').hide();
    }
    currentIsUnsaved = true;
}

/**
* Updates the currentUserSortConfig with the current sort order.
*/
function updateCurrentUserSortConfigAfterSort() {
    // Clear the currentUserSortConfig because it will be updated
    currentUserSortConfig = [];

    $('#sort-order-list li').each(function(listIndex, listElement) {
        var colNameSelectElement = $(listElement).find('.sort-column-name-select');
        var colName = colNameSelectElement.select2('data')[0].id;

        var colOrderSelectElement = $(listElement).find('.sort-column-order-select');
        var colOrder = colOrderSelectElement.select2('data')[0].id;

        var colNameAndOrder = colOrder == 'descending'? '-'+colName: colName;
        currentUserSortConfig.push(colNameAndOrder);
    });
}

/**
* Refreshes the Select2 widget and updates the sortView.
*/
function refreshSortSelection() {

    setTimeout(function(){
        $.ajax({
            url: '$getSortsUrl',
            data: {
                dataBrowserId: $dataBrowserId
            },
            type: 'POST',
            async: true,
            success: function(data) {
                data = JSON.parse(data);

                // Store parsed data to savedUserSortConfigs
                savedUserSortConfigs = data;

                // Get sort names of savedUserSortConfigs
                var sortNames = [];
                for(var savedSort of savedUserSortConfigs) {
                    sortNames.push(savedSort['name'])
                }

                // If currentUserSortConfig is not empty
                if(Array.isArray(currentUserSortConfig) && currentUserSortConfig.length) {
                    // Get index of currentUserSortConfig in savedUserSortConfigs if found
                    var index = findCurrentConfigInSavedConfigs(currentUserSortConfig, savedUserSortConfigs);
                    var selectedSortConfig = {};
                    if(index == -1) {
                        currentIsUnsaved = true;
                        selectedSortConfig = currentUserSortConfig;
                        sortNames.unshift('*unsaved');
                        index += 1
                        $('#save-sort-btn').show();
                    } else {
                        currentIsUnsaved = false;
                        selectedSortConfig = savedUserSortConfigs[index]['data']['__sort__'];
                        $('#delete-sort-btn').show();
                    }
                    refreshSorts(selectedSortConfig);
                    setSortSelection(sortNames);
                    updateSortSelection(index);
                } else {
                    currentIsUnsaved = false;
                    setSortSelection(sortNames);
                    updateSortSelection('');
                }
                // enable Select2 widget
                $('#sort_select').prop('disabled', false);
                if(currentlyDeletingSort) {
                    $('#sort_select').trigger('select2:unselect');
                    $('#sort_select').val(null).trigger('change');
                    $('#sort-add-column-btn').prop('disabled', false);
                    currentlyDeletingSort = false;
                }
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading sort configuration. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });
    }, 300);
}

/**
* This function is only called when a sort selection is selected. It builds the sorts.
*
* @param sortConfig object of the selected sort configuration
*/
function refreshSorts(sortConfig) {
    // Reset sorts (remove from list)
    sortColumnCurrentCount = 0;
    $('#sort-order-list').empty();
    // Reset selected column values
    buildSortSelected();

    $('#sort-add-column-btn').prop('disabled', false);

    for(var colName of sortConfig) {
        var colOrder = colName.startsWith('-')? 'descending': 'ascending';
        colName = colName.startsWith('-')? colName.substr(1): colName;
        addSortColumn({
            'isNew': false,
            'element': $('#sort-add-column-btn'),
            'columnName': colName,
            'columnOrder': colOrder
        });
    }
}

/**
* When the user unselects the currently selected option in the Select2 widget,
* the currentSelectedIndex is set to -1, the buttons are hidden and the sort view is cleared.
*/
$('#sort_select').on('select2:unselect', function(e) {
    currentSelectedIndex = -1;
    buildSortSelected();
    $('#save-sort-btn').hide();
    $('#delete-sort-btn').hide();
    // clearing the sort conditions list
    $('#sort-order-list').empty();
    // add column button, and clear all button
    $('#sort-add-column-btn').prop('disabled', false);
    $('#sort-clear-all-column-btn').prop('disabled', true);
});

/**
* When the user selects an option in the Select2 widget, the sort view is updated and
* the save and delete buttons are shown or hidden based on the index of the selected option
* and if the current sort configuration is unsaved.
*/
$('#sort_select').on('select2:select', function(e) {
    $('#sort-add-column-btn').prop('disabled', false);
    currentSelectedIndex = $('#sort_select').val();
    var index = $('#sort_select').val();
    var selectedSortConfig = [];
    if(currentIsUnsaved) {
        if(index == 0) {
            selectedSortConfig = currentUserSortConfig;
            $('#save-sort-btn').show();
            $('#delete-sort-btn').hide();
        } else {
            selectedSortConfig = savedUserSortConfigs[index - 1]['data']['__sort__'];
            $('#save-sort-btn').hide();
            $('#delete-sort-btn').show();
        }
    } else {
        selectedSortConfig = savedUserSortConfigs[index]['data']['__sort__'];
        $('#save-sort-btn').hide();
        $('#delete-sort-btn').show();
    }
    refreshSorts(selectedSortConfig);
    $('#sort-clear-all-column-btn').prop('disabled', false);
});

/**
* When the column name select is opened, enable selecting of currently selected column name.
*/
$('#sort-order-list').on('select2:open', '.sort-column-name-select', function(e) {
    var currentlySelectedColumnName = $(this).select2('data')[0].id;
    $(this).children().eq(columnNames.indexOf(currentlySelectedColumnName)).prop('disabled', false);
});

/**
* Before the column name select event is fired, update the previously selected column name to false
* in the sortSelected array.
*/
$('#sort-order-list').on('select2:selecting', '.sort-column-name-select', function(e) {
    var currentlySelectedColumnName = $(this).select2('data')[0].id;
    sortSelected[currentlySelectedColumnName] = false;
    updateDisabledSortNamesInSelect();
    var index = currentUserSortConfig.indexOf(currentlySelectedColumnName);
    if(index > -1) {
        currentUserSortConfig.splice(index, 1);
    } else {
        index = currentUserSortConfig.indexOf('-'+currentlySelectedColumnName);
        if(index > -1) {
            currentUserSortConfig.splice(index, 1);
        }
    }
});

/**
* When the user selects a column name, the column order select widget is added by calling
* addSortColumnOrderSelect() and the selected sort names are updated.
*/
$('#sort-order-list').on('select2:select', '.sort-column-name-select', function(e) {
    addUnsavedSortSelectionForSorts();
    var currentlySelectedColumnName = $(this).select2('data')[0].id;
    sortSelected[currentlySelectedColumnName] = true;
    addSortColumnOrderSelect({'isNew': true, 'element': $(this)});
    updateCurrentUserSortConfigAfterSort();
    updateDisabledSortNamesInSelect();
});

/**
* When the user selects an order, the input element is added by calling the
* addUnsavedSortSelectionForSorts() and the currentUserSortConfig is updated.
*/
$('#sort-order-list').on('select2:select', '.sort-column-order-select', function(e) {
    addUnsavedSortSelectionForSorts();

    var columnNameSelectElement = $(this).parent().prev().find('.sort-column-name-select');
    var columnName = columnNameSelectElement.select2('data')[0].id;
    var columnOrder = $(this).select2('data')[0].id;
    var previousOrder = columnName;
    if(columnOrder == 'descending') {
        columnName = '-'+columnName;
    } else {
        columnName = columnName;
        previousOrder = '-'+columnName;
    }
    currentUserSortConfig[currentUserSortConfig.indexOf(previousOrder)] = columnName;
});

/**
* When the user types on the save sort input, the save button is enabled if the input
* is not empty and disabled if the input is empty.
*/
$('#new-sort-name-input').on('input', function(e) {
    if(!$('#new-sort-name-input').val().trim()) {
        $('#save-sort-final-btn').prop('disabled', true);
    } else {
        $('#save-sort-final-btn').prop('disabled', false);
    }
});

/**
* When the user presses the ENTER key on the save sort input, trigger the save button.
*/
$('#new-sort-name-input').on('keypress', function(e) {
    var keyCode = (e.keyCode? e.keyCode : e.which);
    if(keyCode == 13) { // Enter key is pressed
        e.preventDefault();
        if(!currentlySavingSort) {
            $('#save-sort-final-btn').trigger('click');   // The save button is triggered
        }
    }
});

/**
* The close or cancel button hides the save or delete modal depending on which one
* is currently shown.
*/
$('.close-cancel-dialog-btn').on('click', function(e) {
    if($('#saveSortModal').hasClass('in')) {
        $('#saveSortModal').modal('hide');
    }

    if($('#deleteSortModal').hasClass('in')) {
        $('#deleteSortModal').modal('hide');
    }
});

/**
* When the save modal is shown, this function sets the focus on the input.
*/
$('#saveSortModal').on('shown.bs.modal', function(e) {
    $(this).find('#new-sort-name-input').focus();
});

/**
* Shows the save sort modal when the save button is clicked if the currentUserSortConfig
* is not empty.
*/
$('#save-sort-btn').on('click', function (e) {
    if(!$.isEmptyObject(currentUserSortConfig)) {
        $('#saveSortModal').modal('show');
    } else {
        var notif = '<i class="material-icons orange-text">warning</i> There is no sort configuration to save. Please add a column to be sorted.';
        Materialize.toast(notif, 5000);
    }
});

/**
* When the final save button is clicked, this function checks if the input is not empty
* and proceeds to save the current sorts.
*/
$('#save-sort-final-btn').on('click', function(e) {
    // if input is not an empty string
    if($('#new-sort-name-input').val().trim() !== "" && !currentlySavingSort) {
        currentlySavingSort = true;
        $('#sort_select').prop('disabled', true);
        sortConfigToSave = {'__sort__':currentUserSortConfig}

        setTimeout(function(){
            $.ajax({
                url: '$saveSortsUrl',
                data: {
                    name: $('#new-sort-name-input').val().trim(),
                    data: JSON.stringify(sortConfigToSave),
                    dataBrowserId: $dataBrowserId
                },
                type: 'POST',
                async: true,
                success: function(data) {
                    if(data == true) {  // Sort configuration was saved successfully
                        var notif = '<i class="material-icons green-text">check</i> Successfully saved ' + $('#new-sort-name-input').val().trim() + ' sort.';
                        Materialize.toast(notif, 5000);
                        $('#save-sort-btn').hide();
                        refreshSortSelection();
                    } else {    // Sort configuration was not saved successfully
                        var notif = '<i class="material-icons red-text">close</i> There was a problem while saving sort configuration. Please try again.';
                        Materialize.toast(notif, 5000);
                        $('#sort_select').prop('disabled', false);
                    }
                    $('#saveSortModal').modal('hide');
                    $('#new-sort-name-input').val('');
                    $('#save-sort-final-btn').prop('disabled', true);
                    currentlySavingSort = false;
                },
                error: function(){
                    var notif = '<i class="material-icons orange-text">warning</i> There was a problem while saving sort configuration. Please try again.';
                    Materialize.toast(notif, 5000);
                    $('#sort_select').prop('disabled', false);
                    currentlySavingSort = false;
                }
            });
        }, 300);
    }
});

/**
* When the delete button is clicked, this function gets the name and index of the
* currently selected sort and sets the title and content of the delete modal.
*/
$('#delete-sort-btn').on('click', function(e) {
    // find index of selected sort
    var index = $('#sort_select').val();
    if(currentIsUnsaved) {
        index -= 1;
    }

    $('#deleteSortModalTitle').html('<b>'+savedUserSortConfigs[index]['name']+'</b>');
    $('#deleteSortModalContent').html('<b>'+savedUserSortConfigs[index]['name']+'</b>');
});

/**
* When the final delete button is clicked, this function proceeds to delete the selected sort.
*/
$('#delete-sort-final-btn').on('click', function(e) {
    $('#sort_select').prop('disabled', true);

    // find index of selected sort
    var index = $('#sort_select').val();
    if(currentIsUnsaved) {
        index -= 1;
    }

    setTimeout(function(){
        $.ajax({
            url: '$deleteSortsUrl',
            data: {
                sortId: savedUserSortConfigs[index]['configDbId'],
            },
            type: 'POST',
            async: true,
            success: function(data) {
                if(data == true) {  // Sort configuration was deleted successfully
                    var notif = '<i class="material-icons green-text">check</i> Successfully deleted ' + savedUserSortConfigs[index]['name'] + ' sort.';
                    Materialize.toast(notif, 5000);
                    currentlyDeletingSort = true;
                    refreshSortSelection();   // update sort view to currently selected
                } else {    // Sort configuration was not deleted successfully
                    var notif = '<i class="material-icons red-text">close</i> There was a problem while deleting sort configuration. Please try again.';
                    Materialize.toast(notif, 5000);
                    $('#sort_select').prop('disabled', false);
                }
                $('#deleteSortModal').modal('hide');
                $('#deleteSortModalTitle').text('Sort');  // reset the delete modal title text to 'Sort'
                $('#deleteSortModalContent').text('Sort');    // reset the delete modal content text to 'Sort'
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while deleting sort configuration. Please try again.';
                Materialize.toast(notif, 5000);
                $('#sort_select').prop('disabled', false);
            }
        });
    }, 300);
});

/**
* When the add sort column button is clicked, add a sort row.
*/
$('#sort-add-column-btn').on('click', function(e) {
    addUnsavedSortSelectionForSorts();
    addSortColumn({'isNew': true, 'element': this});
    $('#sort-clear-all-column-btn').prop('disabled', false);
});

/**
* Adds listener for all the remove-sort-button descendants of
* sort-order-list. When the user clicks the remove button on the
* sort row, this function searches for the index of the selected
* sort and removes the list item.
*/
$('#sort-order-list').on('click', '.remove-sort-button', function(e) {
    addUnsavedSortSelectionForSorts();
    sortColumnCurrentCount -= 1;
    // enable option
    var columnName = $(this).parent().next().find('select').select2('data')[0].id;
    var index = currentUserSortConfig.indexOf(columnName);
    if(index > -1) {
        currentUserSortConfig.splice(index, 1);
    } else {
        index = currentUserSortConfig.indexOf('-'+columnName);
        if(index > -1) {
            currentUserSortConfig.splice(index, 1);
        }
    }
    if(sortColumnCurrentCount == 0 && Array.isArray(currentUserSortConfig)) {
        $('#sort-clear-all-column-btn').prop('disabled', true);
    }
    $('#sort-add-column-btn').prop('disabled', false);
    sortSelected[columnName] = false;
    updateDisabledSortNamesInSelect();
    $('#sort-order-list li').eq($(this).parents().eq(3).index()).remove();
});

/**
* When the user rearranges the order of the sort configuration.
*/
$('#sort-order-list').on('sortupdate', function(e) {
    addUnsavedSortSelectionForSorts();

    updateCurrentUserSortConfigAfterSort();
});

/**
* When the user clicks the clear all sorts button, this function clears the
* sort-order-list list.
*/
$('#sort-clear-all-column-btn').on('click', function(e) {
    $('#sort_select').val(null).trigger('change');
    $('#sort_select').trigger('select2:unselect');
    $('#sort-add-column-btn').prop('disabled', false);
    sortColumnCurrentCount = 0;
});

/**
* When the user clicks the apply button, this function gets the selected sort
* configuration (except for the currently applied sorts) and applies it.
*/
$('#sort-action-modal-apply-btn').off('click').on('click', function(e) {
    $(this).prop('disabled', false);
    // find index of selected sort
    var index = currentSelectedIndex;
    var noSelectedSort = false;

    if(index == -1 || index == null) {
        noSelectedSort = true;
    }

    var browserUrl = '$browserUrl';
    if(filtersInUrl !== '') {
        browserUrl += '&'+filtersInUrl;
    }
    if(!noSelectedSort) {
        if(currentIsUnsaved) {
            index -= 1;
        }

        var sortConfigToApply = [];
        if(index > -1) {
            sortConfigToApply = savedUserSortConfigs[index]['data']['__sort__'];
        } else {
            sortConfigToApply = currentUserSortConfig;
        }

        var sortsInUrl = '';
        for(var index in sortConfigToApply) {
            var i = index > 0? index.toString(): '';
            sortsInUrl += '|' + sortConfigToApply[index];
        }
        sortsInUrl = sortsInUrl.slice(1);

        if(sortsInUrl !== '') {
            browserUrl += '&sort='+sortsInUrl;
        }
    }

    // Refresh page for entry list browser (needed for selection of sorted results per page)
    if($dataBrowserId == 'entry-list') window.location.href = browserUrl;

    // Apply new sort values by reloading the data browser
    $.pjax.reload({
        container: '$browserPjaxId',
        replace: true,
        url: browserUrl
    });

    // close modal
    $('.sort-action-modal-body').empty();
});

/**
* When the user clicks the cancel button, this function just closes the modal.
*/
$('#sort-action-modal-cancel-btn').on('click', function(e) {
    $('.sort-action-modal-body').empty();
});

/**
* When DOM has loaded
*/
$(document).ready(function() {
    $('#sort-action-modal-apply-btn').prop('disabled', true);
    $('#sort_select').prop('disabled', true);
    $('#save-sort-btn').hide();
    $('#delete-sort-btn').hide();
    retrieveCurrentConfig();

    refreshSortSelection();

    buildSortSelected();
    $('#sort-order-list').sortable({
        cursor: 'move'
    });
});

JS
);

Yii::$app->view->registerCss('
    body{
        overflow-x: hidden;
    }
    .dropdown-content {
       overflow-y: visible;
    }
    .dropdown-content .dropdown-content {
       margin-left: -100%;
    }
    .grid-view td {
        white-space: nowrap;
    }
    #sort-action-modal {
        z-index: 1040 !important;
    }
    .sort-name-item-selected {
        background-color: silver;
    }
    .dialog-modal {
        position: fixed;
        z-index: 1050 !important;
    }
    #sort_select {
        width: 100%;
    }
    .sort-action-modal-container {
        padding: 10px 15px 20px 15px !important;
    }
    .btn-container {
        display: flex;
        justify-content: flex-end;
        height: 40px;
    }
    .material-icons {
        display: inline-flex;
        vertical-align: top;
    }
    .modal-body {
        padding: 10px 0px 0px 0px;
    }
    .form-group {
        padding: 0px 30px;
    }
    a:hover {
        cursor: pointer;
    }
    .modal-body > p {
        padding: 0px 30px;
    }
    #sort-view-list > li {
        margin: 5px 0 5px 0;
    }
    .sort-container {
        padding: 0 0;
    }
    .sort-column-name-select {
        display: block;
    }
    .sort-view-container {
        padding: 5px 5px !important;
        margin: 5px 0;
    }
    .sort-modal-row {
        margin-bottom: 5px;
    }
    .sort-modal-row > .col {
        padding: 0 5px;
    }
    .sort-modal-row > .col > .select2 {
        margin: 3px 0;
    }
    .remove-sort-button {
        padding: 0 10px;
    }
    .sort-order-icon {
        margin: 4px;
    }
    .sort-order-icon:hover {
        cursor: grab;
    }
');
?>