<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Contains methods for managing data browser sort configurations
 */

namespace app\modules\userSort\controllers;

use Yii;
use app\models\User;
use app\models\DataBrowserSortConfig;
use app\components\B4RController;
use yii\helpers\Json;

class DataBrowserSortController extends B4RController
{

    /**
     * Retrieves the user's sort configurations based on the user's data browser.
     *
     * @param integer $dataBrowserId The id of the current data browser
     * @return array $result The user's saved sort configurations
     */
    public function actionIndex() {
        $userModel = new User();
        $userId = $userModel->getUserId(); // get currently logged in user
        $dataBrowserId = $_POST['dataBrowserId'];
        $columnNames = $_POST['columnNames'];
        $browserUrl = $_POST['browserUrl'];
        $browserModel = $_POST['browserModel'];
        $browserPjaxId = $_POST['browserPjaxId'];

        return $this->renderAjax('index', [
            'dataBrowserId' => $dataBrowserId,
            'columnNames' => $columnNames,
            'browserUrl' => $browserUrl,
            'browserModel' => $browserModel,
            'browserPjaxId' => $browserPjaxId
        ], true);
    }

    /**
     * Retrieves the user's sort configurations based on the user's data browser.
     *
     * @param integer $dataBrowserId The id of the current data browser
     * @return array $result The user's saved sort configurations
     */
    public function actionGetSorts() {
        $dataBrowserId = $_POST['dataBrowserId'];

        $searchModel = new DataBrowserSortConfig();

        $result = $searchModel->getSorts($dataBrowserId);

        return Json::encode($result);
    }

    /**
     * Saves sort configuration to database.
     *
     * @param string $name The name of the sort configuration
     * @param json $data The sort configuration
     * @param string $dataBrowserId The id of the current data browser
     * @return string $result Indicates if sql command executed successfully
     */
    public function actionSaveSorts() {
        $name = $_POST['name'];
        $data = $_POST['data'];
        $dataBrowserId = $_POST['dataBrowserId'];

        $searchModel = new DataBrowserSortConfig();

        $result = $searchModel->saveSort($name, $data, $dataBrowserId);

        return $result;
    }

    /**
     * Deletes sort configuration from database.
     *
     * @param integer $sortId The id of the sort configuration to be deleted
     * @return string $result Indicates if sql command executed successfully
     */
    public function actionDeleteSorts() {
        $sortId = $_POST['sortId'];

        $searchModel = new DataBrowserSortConfig();

        $result = $searchModel->deleteSort($sortId);

        return $result;
    }

}