<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Entry;
use app\models\EntrySearch;
use app\models\EntryList;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\ExperimentBlock;
use app\models\Item;
use app\models\User;
use app\models\Cross;
use app\modules\experimentCreation\models\CrossPatternModel;

use Yii;
use yii\helpers\ArrayHelper;
use app\dataproviders\ArrayDataProvider;
use yii\web\Controller;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\PlantingArrangement;
use app\modules\experimentCreation\models\EntryOrderModel;

/**
 * Controller for entry order arrangement
 */
class EntryOrderController extends Controller
{

    protected $entry;
    protected $entrySearch;
    protected $entryList;
    protected $experiment;
    protected $experimentData;
    protected $experimentBlock;
    protected $item;
    protected $experimentModel;
    protected $plantingArrangement;
    protected $entryOrderModel;
    protected $user;
    protected $cross;
    protected $crossPatternModel;

    public function __construct($id, $module,
        Entry $entry, 
        EntrySearch $entrySearch, 
        EntryList $entryList, 
        Experiment $experiment,
        ExperimentData $experimentData,
        ExperimentBlock $experimentBlock,
        ExperimentModel $experimentModel,
        Item $item, 
        PlantingArrangement $plantingArrangement, 
        EntryOrderModel $entryOrderModel,
        User $user,
        Cross $cross,
        CrossPatternModel $crossPatternModel,
        $config = [])
    {
        $this->entry = $entry;
        $this->entrySearch = $entrySearch;
        $this->entryList = $entryList;
        $this->experiment = $experiment;
        $this->experimentData = $experimentData;
        $this->experimentBlock = $experimentBlock;
        $this->experimentModel = $experimentModel;
        $this->item = $item;
        $this->plantingArrangement = $plantingArrangement;
        $this->entryOrderModel = $entryOrderModel;
        $this->user = $user;
        $this->cross = $cross;
        $this->crossPatternModel = $crossPatternModel;
        parent::__construct($id, $module, $config);
    }

    /**
     * Checks if there are previously generated records to be deleted
     * 
     * @param integer $id experiment identifier
     */
    public function actionCheckDesignType($id){
        
        //Get experiment record
        $confirmFlag = $this->entryOrderModel->checkDesignType($id, $_POST);

        return json_encode(['confirmFlag'=> $confirmFlag]);
    }

    /**
     * Update records related to changes in design
     * 
     * @param integer $id experiment identifier
     */
    public function actionChangeDesignType($id){

        extract($_POST);

        //Get experiment record
        $experiment = $this->experiment->getOne($id)['data'];

        $experimentDesignType = $experiment['experimentDesignType'] ?? '';
        if($designType == 'design-add-blocks'){
            if(isset($experiment['experimentDesignType']) && $experiment['experimentDesignType'] != 'Systematic Arrangement'){

                $this->experiment->updateOne($id, ["experimentDesignType" => 'Systematic Arrangement']);
                $experimentDesignType = 'Systematic Arrangement';
            }
        } else if($designType == 'design-entry-order'){
            if(isset($experiment['experimentDesignType']) && $experiment['experimentDesignType'] != 'Entry Order'){

                $this->experiment->updateOne($id, ["experimentDesignType" => 'Entry Order']);
                $experimentDesignType = 'Entry Order';
            }
        }

        // clear entry list replicate data (set all reps to null)
        \Yii::$container->get('app\modules\experimentCreation\models\PAEntriesModel')->updateEntryListReplicate($id, null, [null], 'overwrite');

        //delete all records of experiment blocks
        $params = [
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.order_number AS orderNumber',
            "experimentDbId" => "equals $id"
        ];
        $experimentBlockData = $this->experimentBlock->searchExperimentBlocks($params);

        $experimentBlockCount = isset($experimentBlockData[0]['orderNumber']) ? count($experimentBlockData) : 0;

        if($experimentBlockCount > 0){
            if($experimentDesignType == 'Systematic Arrangement'){
                if($experimentBlockCount == 1){
                    $this->experimentBlock->DeleteOne($experimentBlockData[0]['experimentBlockDbId']);
                } else {
                    $blockData = ArrayHelper::map($experimentBlockData, 'experimentBlockDbId', 'experimentBlockDbId');
                    $listOfExptBlockIds = array_keys($blockData);

                    $this->experimentBlock->deleteMany($listOfExptBlockIds);
                }
            } else {
                if($experimentBlockCount == 1){
                    if(!strpos($experimentBlockData[0]['experimentBlockName'], 'Entry order')){
                        $this->experimentBlock->DeleteOne($experimentBlockData[0]['experimentBlockDbId']);
                        $this->plantingArrangement->addNewRows($id, 1, 'entry order');
                    }
                } else {
                    $blockData = ArrayHelper::map($experimentBlockData, 'experimentBlockDbId', 'experimentBlockDbId');
                    $listOfExptBlockIds = array_keys($blockData);

                    $this->experimentBlock->deleteMany($listOfExptBlockIds);
                    $this->plantingArrangement->addNewRows($id, 1, 'entry order');
                }
            }
            //Check if there are checks metadata
            $abbrevArray = [
                'CHECK_GROUPS',
                'BEGINS_WITH_CHECK_GROUP',
                'ENDS_WITH_CHECK_GROUP',
                'CHECK_GROUP_INTERVAL',
                'ENTRY_LIST_TIMES_REP',
                'PA_ENTRY_WORKING_LIST'
            ];
            $abbrevStr = 'equals '.implode('|equals ', $abbrevArray);
            $expData = $this->experimentData->getExperimentData($id, ["abbrev" => $abbrevStr]);

            if(isset($expData) && count($expData) > 0){
                $existingRec = array_column($expData, 'experimentDataDbId');
                $this->experimentData->deleteMany($existingRec);
            }
  
        }

        $this->plantingArrangement->checkExperimentPAUpdate($id);

        return json_encode(['success'=>true]);
    }

    /**
     * Entry order
     * @param $tab string present activity tab
     * @param $program string current program
     * @param $id integer Experiment ID
     * @param $processId integer Data Process ID 
     * @param $sessionCheckFlag boolean flag for hard reload checking of session saved
     * @param $savingFlag boolean flag for checking of session saved
     */
    public function actionManageEntryOrder(string $tab = null, $program, $id, $processId, $sessionCheckFlag = false, $savingFlag = false)
    {
        $tab = isset($tab) ? $tab : 'entry_order';
        $this->layout = '@app/views/layouts/dashboard';

        //get all records of experiment blocks
        $params = [
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.entry_list_id_list AS entryListIdList',
            "experimentDbId" => "equals $id"
        ];
        $experimentBlockData = $this->experimentBlock->searchExperimentBlocks($params,'?limit=1&sort=experimentBlockDbId:desc')[0];

        if(isset($experimentBlockData) && !empty($experimentBlockData)){

            //Get experiment record
            $experiment = $this->experiment->getOne($id);
            $experimentType = $experiment['data']['experimentType'];

            //populate entryListIdList, layoutOrderData
            $blockData = $this->plantingArrangement->getBlockInformation($experimentBlockData['experimentBlockDbId'], $experimentType);
            if($experimentBlockData['entryListIdList'] == NULL){
                $this->entryOrderModel->saveBlockParams($id, $experimentBlockData['experimentBlockDbId'], $blockData);
                $blockData = $this->plantingArrangement->getBlockInformation($experimentBlockData['experimentBlockDbId'], $experimentType);
            }

            $checkInformation = [];
            //For Observation experiment type
            if($experimentType == 'Observation'){

                //Get occurrence count
                $varAbbrev = 'OCCURRENCE_COUNT';
                $expData = $this->experimentData->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);
                $blockData['no_of_occurrences'] = (isset($expData) && !empty($expData))?$expData[0]['dataValue']:1;

                //get check information
                $entryListId = $this->entryList->getEntryListId($id);
                $checkRecords = $this->entry->searchAll(['fields'=>"entry.entry_list_id as entryListDbId|entry.id as entryDbId|entry.entry_number as entryNumber|germplasm.designation|entry.entry_type as entryType",'entryListDbId'=>"equals ".$entryListId, 'entryType'=>'check'], 'sort=entryNumber:asc');
                
                $checkArray = array();
                if($checkRecords['totalCount'] > 0){

                    foreach($checkRecords['data'] as $checks){
                        $checkArray[$checks['entryDbId']] =  $checks['entryNumber'].': '.$checks['designation'];
                    }
                }
                $checkInformation = $this->entryOrderModel->getCheckInformation($id);
                $checkInformation['check_array'] = $checkArray;
            }
            
            $startingCornerValues = $this->plantingArrangement->getScaleValuesByAbbrev('STARTING_CORNER');
            $plotNumberingOrderValues = $this->plantingArrangement->getScaleValuesByAbbrev('PLOT_NUMBERING_ORDER');

            // If experiment type is ICN and Cross Pattern is used
            $isCrossPattern = false;
            if($experimentType == 'Intentional Crossing Nursery'){
                $params = ["experimentDbId" => "equals $id"];
                $data = \Yii::$container->get('app\models\CrossAttribute')->searchAll($params, 'limit=1&sort=crossAttributeDbId:desc', false);

                if(!empty($data['data'])) {
                    $isCrossPattern = true;
                }
            }

            $htmlData =  $this->renderPartial($isCrossPattern? '_entry_order_cross_pattern' : '_entry_order', [
                'id' => $id,
                'program' => $program,
                'processId' => $processId,
                'startingCornerValues' => $startingCornerValues,
                'plotNumberingOrderValues' => $plotNumberingOrderValues,
                'experimentType' => $experimentType,
                'experiment' => $experiment['data'],
                'blockData' => $blockData,
                'subBlockId' => $experimentBlockData['experimentBlockDbId'],
                'subBlockName' => $experimentBlockData['experimentBlockName'],
                'totalPlots' => $blockData['total_no_of_plots_in_block'],
                'checkInformation' => $checkInformation
            ], true, false);

            return json_encode($htmlData);
        } else {
            $this->experiment->updateOne($id, ["experimentDesignType" => 'Systematic Arrangement']);
            Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning"></i> Reset the arrangement since there are no block records found.');
            return $this->redirect(['create/specify-cross-design?id='.$id.'&program='.$program.'&processId='.$processId]);
        }
    }

    /**
     * Renders the preview of the block layout
     * @param integer $id Experiment ID
     */
    public function actionPreviewLayoutOfBlock($id)
    {
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : 0;
        $panelWidth = isset($_POST['panelWidth']) ? $_POST['panelWidth'] : 0;

        $data = $this->plantingArrangement->getBlockLayout($blockId);

        // get starting plot no of a block
        $startingPlotNo = $this->plantingArrangement->getStartingPlotNo($id, $blockId);

        $startingCorner = $this->experimentBlock->searchAll([
            'fields'=>'experimentBlock.id as experimentBlockDbId|experimentBlock.starting_corner AS startingCorner',
            'experimentBlockDbId' => "$blockId"
        ]);
        $startingCorner = $startingCorner['data'][0]['startingCorner'];

        return $this->renderAjax('/planting-arrangement/_preview_block_layout', [
            'blockId' => $blockId,
            'data' => json_decode($data['notes']),
            'startingPlotNo' => $startingPlotNo,
            'panelWidth' => $panelWidth,
            'noOfRows' => $data['no_of_rows_in_block'],
            'noOfCols' => $data['no_of_cols_in_block'],
            'startingCorner' => $startingCorner,
            'isRefreshLayout' => false,
            'isEntryOrder' => true
        ]);
    }
    /**
     * Manage entries in a block
     * 
     * @param integer $id Experiment ID
     * @param integer $blockId Experiment block ID
     * @param string $experimentType Identification type of experiment
     */
    public function actionManageEntriesInBlock($id, $blockId, $experimentType)
    {
        $filters = [];

        if(!isset($_POST['pjax']) || !isset($_POST['_pjax'])){
            Yii::$app->session->set('pa-entries-filters-' . $id, $filters);
        }
        
        $filteredIdName = "ec"."_filtered_entry_ids_$id";
        $selectedIdName = "ec"."_selected_entry_ids_$id";
        Yii::$app->session->get($selectedIdName);

        //destroy selected items session
        if(((isset($_GET['reset']) && $_GET['reset'] == 'hard_reset' && !isset($_GET['page'])) ||
            (!isset($_GET['reset']) && !isset($_GET['page']))) && isset($_SESSION[$selectedIdName])){
            unset($_SESSION[$selectedIdName]);
            if(isset($_SESSION[$filteredIdName])){
                unset($_SESSION[$filteredIdName]);
            }
        }

        // get params for data browser
        $paramPage = '';
        $paramSort = 'sort=entryNumber:asc';

        if (isset($_GET['dp-1-page'])){
            $paramPage = '&page=' . $_GET['dp-1-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        } else if (isset($_GET['grid-entry-list-grid-page'])) {
            $paramPage = '&page=' . $_GET['grid-entry-list-grid-page'];
        }
        $paramLimit = '?limit='.getenv('CB_DATA_BROWSER_DEFAULT_PAGE_SIZE');

        $pageParams = $paramLimit.$paramPage;
        // append sorting condition
        $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;

        $urlParams = Yii::$app->request->queryParams;

        $dataProvider = $this->entrySearch->search($urlParams, null, null, false, null, $pageParams);
        $dataProvider->sort = false;

        //get block information
        $blockInformation = $this->experimentBlock->searchAll([
            'fields' => 'experimentBlock.id as experimentBlockDbId|experimentBlock.layout_order_data AS layoutOrderData|experimentBlock.entry_list_id_list AS entryListIdList',
            'experimentBlockDbId' => "equals ".$blockId
        ]);

        $entryIdAndRepno = [];

        $entryIdsData = $blockInformation['data'][0]['entryListIdList'];
        $entryIdsArr = array_column($entryIdsData, 'entryDbId');
        $entryRepVal = array_column($entryIdsData, 'repno');
        $entryIdAndRepno = array_combine($entryIdsArr, $entryRepVal);
        $entryListId = $this->entryList->getEntryListId($id);
        
        $experiment = $this->experiment->getOne($id);
        
        $experimentType = $experiment['data']['experimentType'];

        $checksUsed = [];
        if($experimentType == 'Observation'){
            $checksUsed = $this->entryOrderModel->getChecksUsed($id);
        }

        return $this->renderAjax('_entry_order_entries.php',[
            'subBlockId' => $blockId,
            'experimentType' => $experimentType,
            'data' => $dataProvider->allModels,
            'id'=>$id,
            'entryIdAndRepno' => $entryIdAndRepno,
            'dataProvider' => $dataProvider,
            'filterModel'=>$this->entrySearch,
            'entryListId'=>$entryListId,
            'checksUsed' => $checksUsed
        ]);
    }

    /**
     * Update information for a block
     * 
     * @param integer $experimentId Experiment ID
     */
    public function actionUpdateBlockInformation($experimentId){

        extract($_POST);
  
        $entries = [];
        if($selectAll == "true"){
            $urlParams['entryListDbId']="equals $entryListId";
            $entries = $this->entry->searchAll($urlParams);
            
            $entryDbFiltered =  array_column($entries['data'], 'entryDbId');
            if(isset($excludeEntryDbIds)){

                $entryDbIds = array_diff($entryDbFiltered, $excludeEntryDbIds);
            } else {
                $entryDbIds = $entryDbFiltered;
            }
        }
        
        //get entries
        $blockInformation = $this->experimentBlock->searchAll([
            'fields' => 'experimentBlock.id as experimentBlockDbId|experimentBlock.entry_list_id_list AS entryListIdList|experimentBlock.no_of_cols as noOfCols',
            'experimentBlockDbId' => "$blockId"
        ]);

        // Update block
        if(!isset($updates) && $column == 'repno'){
            $entryIdData = $blockInformation['data'][0]['entryListIdList'];
            $entryIdsArr = array_column($entryIdData, 'entryDbId');

            $newEntryListData = $entryIdData;
            foreach($entryDbIds as $entryId){
                $index = array_search($entryId, $entryIdsArr);
                $newEntryListData[$index]['repno'] = $value;
            }
            $param = [
                'entryListIdList' => json_encode($newEntryListData),
                'layoutOrderData' =>  "[]",
                'noOfRanges'=>'0',
                'noOfCols'=>'0',
                'plotNumberingOrder'=>'Column serpentine',
                'startingCorner' => 'Top Left'
            ];
        } else {
            $entryIdData = $blockInformation['data'][0]['entryListIdList'];
            $entryIdsArr = array_column($entryIdData, 'entryDbId');

            $newEntryListData = $entryIdData;

            for($i=0; $i<count($entryDbIds); $i++){
                $index = array_search($entryDbIds[$i], $entryIdsArr);
                $newEntryListData[$index]['repno'] = $value[$i];
            }

            $param = [
                'entryListIdList' => json_encode($newEntryListData),
                'layoutOrderData' =>  "[]",
                'noOfRanges'=>'0',
                'noOfCols'=>'0',
                'plotNumberingOrder'=>'Column serpentine',
                'startingCorner' => 'Top Left'
            ];
        }

        $this->experimentBlock->updateOne($blockId, $param);

        $this->plantingArrangement->updateTemporaryPlotsOfExperiment($experimentId, [$blockId]);

        // Retain no of cols then recompute for no of rows
        $param = [];
        $prevNoOfCols = $blockInformation['data'][0]['noOfCols'];
        if($prevNoOfCols > 0) { // only update if previous no. of cols is not 0
            $param['noOfCols'] = strval($prevNoOfCols);
            // get total plots
            $blockInformation = $this->experimentBlock->searchAll([
                'fields' => 'experimentBlock.id as experimentBlockDbId|experimentBlock.layout_order_data AS layoutOrderData',
                'experimentBlockDbId' => "$blockId"
            ]);
            $totalPlotsInBlock = count($blockInformation['data'][0]['layoutOrderData']);

            // compute new no of rows
            $newNoOfRows = ceil($totalPlotsInBlock/$prevNoOfCols);
            $param['noOfRanges'] = strval($newNoOfRows);

            $this->experimentBlock->updateOne($blockId, $param);
        }

        // update status of experiment
        $this->plantingArrangement->updateExperimentStatus($experimentId);

        $this->plantingArrangement->checkExperimentPAUpdate($experimentId);
        return json_encode(['success'=>true]);
    }

    /**
     * Shows browser for cross pattern in a block
     *
     * @param integer $id Experiment ID
     * @param string $program string current program
     * @param integer $processId integer Data Process ID
     * @param integer $blockId Experiment block ID
     * @param string $experimentType Identification type of experiment
     */
    public function actionManageCrossPatternPlotsInBlock($id, $program, $processId, $blockId, $experimentType) {
        $dataProvider = $this->crossPatternModel->getPlotsFromCrossPatternInBlock($blockId);

        return $this->renderAjax('_entry_order_cross_pattern_entries.php',[
            'id' => $id,
            'program' => $program,
            'processId' => $processId,
            'subBlockId' => $blockId,
            'experimentType' => $experimentType,
            'data' => $dataProvider->allModels,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Deletes the planting arrangement and all the created crosses
     *
     * @param integer $id Experiment ID
     * @param string $program string current program
     * @param integer $processId integer Data Process ID
     */
    public function actionDeleteCrossPatternPlotsInBlock($id, $program, $processId) {
        // delete cross pattern info
        $this->crossPatternModel->deleteCrossPatternInfo($id);

        // delete created crosses
        $this->cross->deleteCrosses($id);

        // update status, remove "crosses created; design generated"
        $experiment = $this->experiment->getOne($id)['data'];
        $status = explode(';', $experiment['experimentStatus']);

        if (in_array('cross list created', $status) || in_array('design generated', $status)) {
            $index = array_search('cross list created', $status);
            if ($index != false && isset($status[$index])) {
                unset($status[$index]);
            }

            $index = array_search('design generated', $status);
            if ($index != false && isset($status[$index])) {
                unset($status[$index]);
            }

            $updatedStatus = implode(';', $status);

            $this->plantingArrangement->saveExperimentStatus($id, $updatedStatus);
            $this->experiment->updateOne($id, ['notes' => null]);
        }

        // redirect to crosses tab
        Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> The planting arrangement and all the created crosses have been successfully deleted.');
        return $this->redirect(['create/manage-crosses?id='.$id.'&program='.$program.'&processId='.$processId]);
    }
}