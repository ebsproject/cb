<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Api;
use app\models\BackgroundJob;
use app\models\Entry;
use app\models\EntryData;
use app\models\EntryList;
use app\models\EntrySearch;
use app\models\Experiment;
use app\models\ExperimentBlock;
use app\models\ExperimentData;
use app\models\GeospatialObject;
use app\models\Occurrence;
use app\models\Item;
use app\models\User;
use app\models\Variable;
use app\models\PlanTemplate;
use app\models\Scale;
use app\models\UserDashboardConfig;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;


use app\modules\experimentCreation\models\AnalysisModel;
use app\modules\experimentCreation\models\DesignModel;
use app\modules\experimentCreation\models\EntryModel;
use app\modules\experimentCreation\models\EntryOrderModel;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\ExperimentEntryDataModel;
use app\modules\experimentCreation\models\ExperimentEntryModel;
use app\modules\experimentCreation\models\PlantingArrangement;

/**
 * Controller for planting arrangement
 */
class DesignController extends Controller
{
    public function __construct($id, $module,
        protected AnalysisModel $analysisModel,
        protected Api $api,
        protected BackgroundJob $backgroundJob,
        protected DesignModel $designModel,
        protected Entry $entry,
        protected EntryModel $entryModel,
        protected EntryData $entryData,
        protected EntryList $entryList,
        protected EntrySearch $entrySearch,
        protected Experiment $experiment,
        protected ExperimentData $experimentData,
        protected ExperimentEntryModel $experimentEntryModel,
        protected ExperimentEntryDataModel $experimentEntryDataModel,
        protected GeospatialObject $geospatialObject,
        protected Item $item,
        protected Occurrence $occurrence,
        protected User $user,
        protected ExperimentModel $experimentModel,
        protected Scale $scale,
        protected UserDashboardConfig $userDashboardConfig,
        protected Variable $variable,
        protected PlanTemplate $planTemplate,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * set the currently selected package data in session to null.
     * @param integer $id Experiment ID
     * @return array
     */
    public function actionEmptyUserSelection($id){
        Yii::$app->session->set('selectedItems-'.$id, null);
        return json_encode(['success'=> true]);
    }

    /**
     * get the currently selected package data in session.
     * @param integer $id Experiment ID
     * @return array
    */
    public function actionGetUserSelection($id){
        return json_encode(Yii::$app->session['selectedItems-'.$id]);
    }

    /**
     * Set selected items in the grid
     */
    public function actionStoreSessionItems(){
        $count = 0;
        $items = [];
        $isAll = false;
        
        if(isset($_POST['unset'], $_POST['entryDbId'], $_POST['storeAll'], $_POST['experimentDbId'])){
            extract($_POST);


            $filteredIdName = "ec"."_filtered_entry_ids_$experimentDbId";
            $selectedIdName = "ec"."_selected_entry_ids_$experimentDbId";  
            $isSelectedAll = "ec"."_is_selected_all_$experimentDbId";
        
            $items = (Yii::$app->session->get($selectedIdName) !== null) ? Yii::$app->session->get($selectedIdName) : [];
            
            if($unset == "true"){
                if($storeAll == "true"){
                    $items = [];
                }
                else{
                    foreach($items as $key => $value){
                        if($value == $entryDbId){
                            unset($items[$key]);
                        }
                    }
                    $items = array_values($items);
                }
            }
            else{

                if($storeAll == "true"){
                    $items = Yii::$app->session->get($filteredIdName);
                    $isAll = true;
                }
                else if(!in_array($entryDbId, $items)){     
                    $items[] = $entryDbId;
                }
            }

            Yii::$app->session->set($selectedIdName, $items);
            Yii::$app->session->set($isSelectedAll, $isAll);
            
            $count = !empty($items) ? count($items) : 0;
        }

        return json_encode($count);

    }

    /**
     * Retrieve selected items stored in session
     */
    public function actionGetSelectedItems(){

        extract($_POST);


        $selectedIdName = "ec"."_selected_entry_ids_$experimentDbId";
        $selectedItems = (Yii::$app->session->get($selectedIdName) !== null) ? Yii::$app->session->get($selectedIdName) : [];
        $isSelectedAll = (Yii::$app->session->get("ec"."_is_selected_all_$experimentDbId") !== null) ? Yii::$app->session->get("ec"."_is_selected_all_$experimentDbId") : false;

        $removedEntries = 0;
        //get entry role for checking when assigning to group
        if(isset($groupType)){
            $entries = $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId','entryDbId'=>"equals ".implode('|equals ', $selectedItems), 'entryType'=>'equals '.$groupType], '', true);
            $newSelectedItems = array_column($entries['data'], 'entryDbId');

            if(count($selectedItems) != $entries['totalCount']){
                $removedEntries = count($selectedItems) - $entries['totalCount'];
            }
            $selectedItems = $newSelectedItems;
        }

        $response = [
            'selectedItems' => $selectedItems,
            'selectedItemsCount' => count($selectedItems),
            'isSelectedAll' => $isSelectedAll,
            'removedEntries' => $removedEntries
        ];

        return json_encode($response);
    }

    /**
     * Resets the session filter for assign entries
     * @param integer $id Experiment ID
     */
    public function actionResetSessionFilter($id){
        $filters = [];
        Yii::$app->session->set('design-entries-filters-' . $id, $filters);
        Yii::$app->session->set("ec"."_is_selected_all_$id", false);

        return json_encode(['success'=> true]);
    }

    /**
     * Save changes in the group
     * @param integer $id Experiment ID
     */
    public function actionSaveGroupChanges($id){
        if(isset($_POST)){
            extract($_POST);

            $param = ['entityType'=>'experiment', 'entityDbId'=> "equals $id"];

            $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);
            $plantTemplateDbId = $jsonInput['planTemplateDbId'];
            $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);
            $jsonArr['design_ui']['groups'] = json_decode($uiGroups, true);
            
            $update = $this->planTemplate->updateOne($plantTemplateDbId, ['mandatoryInfo' => json_encode($jsonArr)]);
            
            return json_encode(['success'=> true]);
        } else {
            return json_encode(['success'=> false]);
        }
    }

    /**
     * Update design parameters
     * @param integer $id Experiment ID
     */
    public function actionUpdateDesignParameters($id){
        
        if(isset($_POST)){
            extract($_POST);

            if(in_array($fieldId, ['entryRole', 'entryClass'])){
                $requestedData =[
                    "$fieldId" => "$fieldValue",
                ];
                foreach ($entryDbIds as $entryId) {
                    $this->entry->updateOne($entryId, $requestedData);
                }
            }

            $param = ['entityType'=>'experiment', 'entityDbId'=> "equals $id"];

            $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);
            $plantTemplateDbId = $jsonInput['planTemplateDbId'];
            $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);
            $jsonArr['design_ui']['groups'] = $uiGroups;

            $update = $this->planTemplate->updateOne($plantTemplateDbId, ['mandatoryInfo' => json_encode($jsonArr)]);
            
            return json_encode(['success'=> true, 'uiGroups'=>$uiGroups]);
        } else {
            return json_encode(['success'=> false]);
        }
    }

    /**
     * Bulk update design fields
     * @param integer $id Experiment ID
     */
    public function actionBulkUpdateFields($id){
        
        if(isset($_POST)){
            $update = FALSE;
            extract($_POST);
            if($updateAll=='true'){
                $entryListCheckArray =  $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId','entryListDbId'=>"equals $entryListId", 'entryType'=>'check'], '', true);
                $entryDbIds = array_column($entryListCheckArray['data'], 'entryDbId');
            }
            if(in_array($fieldId, ['ENTRY_ROLE', 'ENTRY_CLASS'])){

                if($fieldId == 'ENTRY_ROLE'){
                    $fieldName = 'entryRole';
                } else {
                    $fieldName = 'entryClass';
                }
                $requestedData =[
                    "$fieldName" => "$fieldValue",
                ];
                foreach ($entryDbIds as $entryId) {
                    $this->entry->updateOne($entryId, $requestedData);
                }

                if($fieldId == 'ENTRY_ROLE'&& $fieldValue == 'repeated check'){
                    $update = TRUE;
                    $fieldValue = 2;
                }
            } else {
                $update = TRUE;
            }

            if($update){
                $param = ['entityType'=>'experiment', 'entityDbId'=> "equals $id"];
                $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);
                $plantTemplateDbId = $jsonInput['planTemplateDbId'];
                $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);
                $uiGroups = $jsonArr['design_ui']['groups'];

                foreach ($entryDbIds as $entryId) {
                    $uiGroups[$entryId] = $fieldValue;
                }
                $jsonArr['design_ui']['groups'] = $uiGroups;

                $update = $this->planTemplate->updateOne($plantTemplateDbId, ['mandatoryInfo' => json_encode($jsonArr)]);
            }
            
            return json_encode(['success'=> true, 'uiGroups'=>$uiGroups]);
        } else {
            return json_encode(['success'=> false]);
        }
    }

    /**
     * Save across environment variable
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     */
    public function actionSaveEnvValue($id, $program, $processId){

        if(isset($_POST)){
            extract($_POST);
            $designRecord = $this->variable->getVariableByAbbrev('ACROSS_ENV_DESIGN');
            $variableDbId = $designRecord['variableDbId'];

            $acrossEnvDesign = $this->experimentData->getExperimentData($id,[
                'variableDbId' => 'equals '.$variableDbId
            ]);

            if(count($acrossEnvDesign)== 0 && $designValue == 'random'){
                $dataRequestBodyParam[] = [
                    'variableDbId' => "$variableDbId",
                    'dataValue' => "$designValue"
                ];

                $data = $this->experimentModel->createExperimentData($id,$dataRequestBodyParam);  

            } else if(count($acrossEnvDesign) > 0 && $designValue == 'none'){
                //delete data record
                $acrossEnvDesignId = $acrossEnvDesign[0]['experimentDataDbId'];
                $this->experimentData->deleteMany([$acrossEnvDesignId]);

            }

            //delete all template records
            $planTemplates = $this->planTemplate->searchAll(['entityDbId'=>"equals $id"]);
            
            if($planTemplates['totalCount'] > 0){
                $planTemplateIds = array_column($planTemplates['data'],'planTemplateDbId');
                $this->planTemplate->deleteMany($planTemplateIds);
            }

            return json_encode('success');
        }
        
    }

    /**
     * Renders view for entry list management for the experiment
     *
     * @param string $program code of the current program
     * @param integer $id unique experiment identifier
     * @param integer $processId integer process identifier
     * @param string $entryType type of entries for filtering
     *
     * @return string render result
     */
    public function actionAssignNumberOfReplicationsToEntries ($program, $id, $processId, $entryType = '')
    {
        Yii::info('Rendering data browser for experiment entry data with arguments: ' . json_encode(compact('program', 'id', 'processId')), __METHOD__);

        $acrossEnvDesign = $this->designModel->getAcrossEnvDesign($id);
        $params = Yii::$app->request->queryParams;
        $model = $this->experiment->getExperiment($id);

        if($acrossEnvDesign == 'random'){
            $view = 'assign_number_of_replications_to_entries';
            $searchModel = $this->experimentEntryDataModel;
            $params = Yii::$app->request->queryParams;
            $browserFilters = isset($params['ExperimentEntryDataModel']) ? $params['ExperimentEntryDataModel'] : [];
            $params['ExperimentEntryDataModel']['variableAbbrev'] = 'equals TIMES_REP';

            $entryTypeSet = 0; //flag if entry type filter is already set by default
            if($entryType != ''){
                $params['ExperimentEntryDataModel']['entryType'] = "equals ".$entryType;
                $entryTypeSet = 1;
            }

            $params['ExperimentEntryDataModel']['fields'] = 'entryData.id AS entryDataDbId | entry.entry_number AS entryNumber | entry.entry_name AS entryName | entry.entry_type AS entryType | entry.entry_class AS entryClass | entry.entry_role AS entryRole | variable.abbrev AS variableAbbrev | entryData.data_value AS dataValue';
            $output = $searchModel->search($params, $id);

            $browserFilters = array_values($browserFilters);
            $browserFilters = implode('', $browserFilters);
            if($output[1] == 0 && empty($browserFilters)) {
                $this->designModel->populateEntryData($id, true, $output[1], null);
                $output = $searchModel->search($params, $id);
            } else if($output[1] > 0){
                $this->designModel->populateEntryData($id, false, $output[1], null);
            }

            return $this->render($view, [
                'program' => $program,
                'id' => $id,
                'processId' => $processId,
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $output[0],
                'totalCount' => $output[1],
                'paramSort' => $output[2],
                'filters' => $output[3],
                'isNextButtonDisabled' => 'disabled="disabled"',
                'entryTypeSet' => $entryTypeSet,
                'entryType' => $entryType
            ]);
        } else {
            $view = 'assign_numreps_non_sparse';
            $searchModel = $this->experimentEntryModel;
            $params = Yii::$app->request->queryParams;
            $browserFilters = isset($params['ExperimentEntryModel']) ? $params['ExperimentEntryModel'] : [];

            $entryTypeSet = 0; //flag if entry type filter is already set by default
            $repType = 'repTest';
            $repInitial = 1;
            if($entryType != ''){
                $params['ExperimentEntryModel']['entryType'] = "equals ".$entryType;
                if($entryType == 'check'){
                    $repType = 'repCheck';
                    $repInitial = 2;
                    $params['ExperimentEntryModel']['entryRole'] = "not equals local check|is null";
                }
                $entryTypeSet = 1;
            }

            $params['ExperimentEntryModel']['fields'] = 'entry.id AS entryDbId | entry.entry_number AS entryNumber | entry.entry_name AS entryName | entry.entry_type AS entryType | entry.entry_class AS entryClass | entry.entry_role AS entryRole|entry_list.experiment_id as experimentDbId';
            $output = $searchModel->search($params, $id);

            $browserFilters = array_values($browserFilters);
            $browserFilters = implode('', $browserFilters);
            
            //retrieve plan template and check if there's existing rep arrays
            $param = ['entityType'=>'parameter_sets', 'entityDbId'=> "equals $id", 'status'=>'equals incomplete'];
            $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);
            $plantTemplateDbId = $jsonInput['planTemplateDbId'];
            $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);

            //check if there are changes in $repType
            $entryListId = $this->entryList->getEntryListId($id);
            $filters = ['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId| entry.entry_number AS entryNumber| entry.entry_role AS entryRole','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals '.$entryType];

            if($entryType == 'check'){
                $filters['entryRole'] = "not equals local check|is null";
            }
            $entryListArray =  $this->entry->searchAll($filters, 'sort=entryNumber:ASC', true);
            $entryDbIds = array_column($entryListArray['data'], 'entryDbId');
            $diff = $diff2 = [];
            if(isset($jsonArr['design_ui'][$repType])){
                $idKeys = array_keys($jsonArr['design_ui'][$repType]);
                $diff = array_diff($idKeys, $entryDbIds);
                $diff2 = array_diff($entryDbIds, $idKeys);
            }
            
            if(!isset($jsonArr['design_ui'][$repType]) || !empty($diff) || !empty($diff2)){
                //get mapping of data
                
                $repArray = array_fill(0,  $output[1], $repInitial);
                
                $jsonArr['design_ui'][$repType] = array_combine($entryDbIds, $repArray);
                $this->planTemplate->updateOne($plantTemplateDbId, ['mandatoryInfo' => json_encode($jsonArr)]);
            }

            $repsArray = $jsonArr['design_ui'][$repType];
            return $this->render($view, [
                'program' => $program,
                'id' => $id,
                'processId' => $processId,
                'model' => $model,
                'repsArray' => $repsArray,
                'repType' => $repType,
                'searchModel' => $searchModel,
                'dataProvider' => $output[0],
                'totalCount' => $output[1],
                'paramSort' => $output[2],
                'filters' => $output[3],
                'isNextButtonDisabled' => 'disabled="disabled"',
                'entryTypeSet' => $entryTypeSet,
                'entryType' => $entryType
            ]);
        }
    }


    /**
     * Renders view for selection of entries
     *
     * @param string $program code of the current program
     * @param integer $id unique experiment identifier
     * @param integer $processId integer process identifier
     * @param string $selectEntryType entries to be selected
     *
     * @return string render result
     */
    public function actionSelectionEntries ($program, $id, $processId, $selectEntryType)
    {
        $searchModel = $this->entryModel;
        $params = Yii::$app->request->queryParams;
        $params['EntryModel']['fields'] = 'entry.entry_number AS entryNumber | entry.entry_name AS entryName | entry.entry_type AS entryType | entry.entry_class AS entryClass | entry.entry_role AS entryRole | entry.id AS entryDbId| experiment.id AS experimentDbId';
        $params['EntryModel']['entryType'] = $selectEntryType;
        $params['EntryModel']['experimentDbId'] = 'equals '.$id;
        $params['EntryModel']['entryRole'] = 'not equals local check|is null';
        $output = $searchModel->search($params, $id);
        
        $model = $this->experiment->getExperiment($id);

        //get selected entries if already exist
        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id", 'status'=>'incomplete'], 'sort=planTemplateDbId:ASC');

        $experimentJson = $experimentJsonArray['data'][0];

        $planTemplateDbId = $experimentJson['planTemplateDbId'];

        $jsonArray = json_decode($experimentJson['mandatoryInfo'], true);
        $parameterRandArray = $jsonArray['design_ui']['randomization'];
        $indexRandIds = array_column($parameterRandArray, 'code');

        $indexRand = array_search('nCheckListSet',$indexRandIds);
        $selectedIds = isset($parameterRandArray[$indexRand]['value']) ? $parameterRandArray[$indexRand]['value'] : [];

        return $this->render('selection_entries_view', [
            'program' => $program,
            'id' => $id,
            'processId' => $processId,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $output[0],
            'totalCount' => $output[1],
            'paramSort' => $output[2],
            'filters' => $output[3],
            'isNextButtonDisabled' => 'disabled="disabled"',
            'selectEntryType' => $selectEntryType,
            'selectedIds' => $selectedIds,
            'planTemplateId' => $planTemplateDbId
        ]);
    }

    /**
     * Save a specific field update
     */
    public function actionSaveRowField ()
    {
        extract($_POST);

        Yii::info(
            'Saving changes in row field with arguments: ' .
            json_encode(compact(
                'entryDataDbId',
                'value'
            )),
            __METHOD__
        );

        if (isset($entryDataDbId, $value)) {
            if ($entryDataDbId) { // Record exists
                $this->entryData->updateRecord(
                    $entryDataDbId,
                    ['dataValue' => $value]
                );

                return;
            }
        }

        Yii::$app->session->setFlash(
            'error',
            '<i class="fa fa-times"></i> Failed to save the changes. Kindly report this in the support portal.'
        );
    }

    /**
     * Save a specific field update
     */
    public function actionSaveRowFieldNonsparse ()
    {
        extract($_POST);

        Yii::info(
            'Saving changes in row field with arguments: ' .
            json_encode(compact(
                'entryDbId',
                'value'
            )),
            __METHOD__
        );

        if (isset($entryDbId, $value, $repType)) {

            $param = ['entityType'=>'parameter_sets', 'entityDbId'=> "equals $id", 'status'=>'equals incomplete'];
            $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);
            $plantTemplateDbId = $jsonInput['planTemplateDbId'];
            $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);

            $tempArr = $jsonArr['design_ui'][$repType];
            $tempArr[$entryDbId] = $value;

            $jsonArr['design_ui'][$repType] = $tempArr;
            $this->planTemplate->updateOne($plantTemplateDbId, ['mandatoryInfo' => json_encode($jsonArr)]);
            return;
        }

        Yii::$app->session->setFlash(
            'error',
            '<i class="fa fa-times"></i> Failed to save the changes. Kindly report this in the support portal.'
        );
    }


    /**
     * Save bulk update for nonsparse
     */
    public function actionBulkUpdateTotalNrepsNonsparse ()
    {
        extract($_POST);

        $selectedIds = $selectedIds === '[]' ? [] : $selectedIds;

        Yii::info(
            'Bulk updating Total NReps with arguments: ' .
            json_encode(compact(
                'program',
                'id',
                'selectedIds',
                'selectAllMode',
                'mode',
                'value'
            )),
            __METHOD__
        );

        if (isset(
                $program,
                $id,
                $selectedIds,
                $selectAllMode,
                $mode,
                $value
        )) {

            $param = ['entityType'=>'parameter_sets', 'entityDbId'=> "equals $id", 'status'=>'equals incomplete'];
            $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);
            $plantTemplateDbId = $jsonInput['planTemplateDbId'];
            $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);

            $tempArr = $jsonArr['design_ui'][$repType];
            if ($mode != 'all') {

                for($i = 0; $i < count($selectedIds); $i++){
                    $tempArr[$selectedIds[$i]] = $value;
                }
                $jsonArr['design_ui'][$repType] = $tempArr;
                $this->planTemplate->updateOne($plantTemplateDbId, ['mandatoryInfo' => json_encode($jsonArr)]);
                
                $entriesMessage = $mode == 'current-page' ? 'in current page' : 'across pages';
                return json_encode([
                    'success' => true,
                    'message' => '<span>Successfully updated Total NReps of <strong>selected entries '.$entriesMessage.'</strong>!</span>',
                ]);
                
            } else {

                foreach($tempArr as $key => $val){
                    $tempArr[$key] = $value;
                }
                
                $jsonArr['design_ui'][$repType] = $tempArr;
                $this->planTemplate->updateOne($plantTemplateDbId, ['mandatoryInfo' => json_encode($jsonArr)]);
                
                return json_encode([
                    'success' => true,
                    'message' => '<span>Successfully updated Total NReps of <strong>all entries</strong>!</span>',
                ]);
            }
            
        }


        Yii::$app->session->setFlash(
            'error',
            '<i class="fa fa-times"></i> Failed to save the changes. Kindly report this in the support portal.'
        );

    }



    /**
     * Bulk update Total NReps for selected/all entries
     */
    public function actionBulkUpdateTotalNreps ()
    {
        extract($_POST);

        $selectedIds = $selectedIds === '[]' ? [] : $selectedIds;
        $limit = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'updateEntries')) ?
            Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'updateEntries') : 500;
        $isBackgroundProcess = false;
        $additionalParams = [
            'description' => 'Updating entry data records',
            'entity' => 'ENTRY_DATA',
            'entityDbId' => "$experimentDbId",
            'endpointEntity' => 'ENTRY_DATA',
            'application' => 'EXPERIMENT_CREATION',
        ];

        Yii::info(
            'Bulk updating Total NReps with arguments: ' .
            json_encode(compact(
                'program',
                'experimentDbId',
                'selectedIds',
                'selectAllMode',
                'mode',
                'value'
            )),
            __METHOD__
        );

        if (isset(
                $program,
                $experimentDbId,
                $selectedIds,
                $selectAllMode,
                $mode,
                $value
        )) {
            if ($mode === 'current-page') {
                $isBackgroundProcess = count($selectedIds) > $limit;

                // Update each selected entry within the current page
                $this->entryData->updateMany(
                    $selectedIds,
                    ['dataValue' => $value],
                    $isBackgroundProcess,
                    ($isBackgroundProcess) ? $additionalParams : []
                );

                if ($isBackgroundProcess) {
                    $experimentName = $this->experiment->getOne($experimentDbId)['data']['experimentName'];

                    Yii::$app->session->setFlash('info', "<i class='fa fa-info-circle'></i> The entry data records for <strong>$experimentName</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.");
                    Yii::$app->response->redirect(["experimentCreation/create/index?program=$program&id=$experimentDbId"]);
                } else {
                    return json_encode([
                        'success' => true,
                        'message' => '<span>Successfully updated Total NReps of <strong>selected entries in current page</strong>!</span>',
                    ]);
                }
            } elseif ($mode === 'selected' || $mode === 'all') {
                if ($mode === 'selected' && $selectAllMode === 'include') {
                    // Update each selected entry
                    $isBackgroundProcess = count($selectedIds) > $limit;

                    // Update each selected entry within the current page
                    $this->entryData->updateMany(
                        $selectedIds,
                        ['dataValue' => $value],
                        $isBackgroundProcess,
                        ($isBackgroundProcess) ? $additionalParams : []
                    );
    
                    if ($isBackgroundProcess) {
                        $experimentName = $this->experiment->getOne($experimentDbId)['data']['experimentName'];
    
                        Yii::$app->session->setFlash('info', "<i class='fa fa-info-circle'></i> The entry data records for <strong>$experimentName</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.");
                        Yii::$app->response->redirect(["experimentCreation/create/index?program=$program&id=$experimentDbId"]);
                    }
                } else if ($mode === 'selected' && $selectAllMode === 'exclude') {
                    // Get all entry IDs of an experiment, EXCLUDING select entry IDs in the data browser
                    $filter = Yii::$app->session['experiment-'.$experimentDbId.'-entry-data-api_filter'];
                    $notIncluded = null;
                    if(!empty($selectedIds)) $notIncluded = 'not equals '.implode(' | not equals ', $selectedIds);
                    if(!is_null($notIncluded)) $filter['entryDataDbId'] = $notIncluded;

                    $response = $this->api->getApiResults(
                        'POST',
                        "experiments/$experimentDbId/entry-data-search",
                        json_encode($filter),
                        retrieveAll: true,
                    );

                    $entryDataDbIds = array_column($response['data'], 'entryDataDbId');

                    if (count($selectedIds) > 0) {
                        foreach ($entryDataDbIds as $key => $_) {
                            settype($entryDataDbIds[$key], 'string');
                        }

                        $entryDataDbIds = array_diff($entryDataDbIds, $selectedIds);
                    }

                    // Update each selected entry
                    $isBackgroundProcess = count($entryDataDbIds) > $limit;

                    $this->entryData->updateMany(
                        $entryDataDbIds,
                        ['dataValue' => $value],
                        $isBackgroundProcess,
                        ($isBackgroundProcess) ? $additionalParams : []
                    );
    
                    if ($isBackgroundProcess) {
                        $experimentName = $this->experiment->getOne($experimentDbId)['data']['experimentName'];
    
                        Yii::$app->session->setFlash('info', "<i class='fa fa-info-circle'></i> The entry data records for <strong>$experimentName</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.");
                        Yii::$app->response->redirect(["experimentCreation/create/index?program=$program&id=$experimentDbId"]);
                    }
                } else {
                    // Get all entry IDs of an experiment, EXCLUDING select entry IDs in the data browser
                    $response = $this->api->getApiResults(
                        'POST',
                        "experiments/$experimentDbId/entry-data-search",
                        json_encode([
                            'distinctOn' => 'entryDbId',
                            'variableAbbrev' => 'equals TIMES_REP',
                            'fields' => 'entry.id AS entryDbId | entryData.id AS entryDataDbId | variable.abbrev AS variableAbbrev',
                        ]),
                        retrieveAll: true,
                    );

                    $entryDataDbIds = array_column($response['data'], 'entryDataDbId');
                    
                    if (count($selectedIds) > 0) {
                        foreach ($entryDataDbIds as $key => $_) {
                            settype($entryDataDbIds[$key], 'string');
                        }
    
                        $entryDataDbIds = array_diff($entryDataDbIds, $selectedIds);
                    }

                    // Update each selected entry
                    $isBackgroundProcess = count($entryDataDbIds) > $limit;

                    $this->entryData->updateMany(
                        $entryDataDbIds,
                        ['dataValue' => $value],
                        $isBackgroundProcess,
                        ($isBackgroundProcess) ? $additionalParams : []
                    );
    
                    if ($isBackgroundProcess) {
                        $experimentName = $this->experiment->getOne($experimentDbId)['data']['experimentName'];
    
                        Yii::$app->session->setFlash('info', "<i class='fa fa-info-circle'></i> The entry data records for <strong>$experimentName</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.");
                        Yii::$app->response->redirect(["experimentCreation/create/index?program=$program&id=$experimentDbId"]);
                    }
                }

                $entriesMessage = ($mode === 'selected') ? '<strong>selected entries across pages</strong>' : '<strong>all entries</strong>';

                return json_encode([
                    'success' => true,
                    'message' => "<span>Successfully updated Total NReps of $entriesMessage!</span>",
                ]);
            }

            return;
        }

        Yii::$app->session->setFlash(
            'error',
            '<i class="fa fa-times"></i> Failed to save the changes. Kindly report this in the support portal.'
        );
    }

    /**
     * Retrieves config json for ui
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     *
     */
    public function actionChangeDesign($program,$id){
        
        if(isset($_POST)){

            $designInfo = json_decode($_POST['designInfo'], true);
            $designId = $_POST['designId'];
            $acrossEnvDesign = $_POST['acrossEnvDesign'];

            $design = $designInfo[$designId]['designCode'];
            $model = $this->experiment->getExperiment($id);

            //generate template json for selected design
            $designRecord = $this->variable->getVariableByAbbrev('DESIGN');
            $designVariable = $designRecord['variableDbId'];

            //get scale values for design
            $scaleRecords = $this->scale->getVariableScale($designVariable)['scaleValues'] ?? [];
            $scaleValues = array_column($scaleRecords, 'value');

            $searchword = trim($design);
            $experimentDesign = "";
            foreach($scaleValues as $valDesign){
                if($valDesign == $searchword){
                    $experimentDesign = $valDesign;
                    break;
                }
            }

            //Update experiment design
            $oldDesign = $model['experimentDesignType'];
          
            // $this->experiment->updateOne($id, ['experimentDesignType'=>$experimentDesign]);

            $entryListId = $this->entryList->getEntryListId($id);

            //check if there's existing json input for the experiment design
            //POST search plan-templates
            $planTemplateArray = $this->planTemplate->searchAll(['entityDbId'=>"equals $id"]);

            $designUiSwitch = $this->designModel->getDesignUiInfo($id);

            if($planTemplateArray['totalCount'] > 0 && $designUiSwitch == 'old' && $acrossEnvDesign == 'none'){
                //delete created designs and revert back to old UI, saving design to experiment level

                $planTemplateIds = array_column($planTemplateArray['data'],'planTemplateDbId');

                $this->planTemplate->deleteMany($planTemplateIds);
            }

            $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id", 'status'=>'incomplete']);

            $planTemplateArrayInc = $this->planTemplate->searchAll(['entityDbId'=>"equals $id"]);

            $isSparse = ($acrossEnvDesign == 'none') ? false : true;

            //get fields for design
            $designFieldsArray = $this->designModel->getDesignFields($designId, $isSparse);

            if($planTemplateArray['totalCount'] == 0){

                //create plan template record
                $experimentJson = [];
                $randName = bin2hex(random_bytes(8));
                $experimentJson['templateName'] = str_replace('#','_',(str_replace(' ', '_',$model['experimentCode']))).'_'.$model['experimentDbId']."_$randName";
                $experimentJson['entityDbId'] = $id;

                if($designUiSwitch == 'old' && $acrossEnvDesign == 'none'){   //return to old design

                    $experimentJson['status'] = 'draft';
                    $experimentJson['entityType'] = "experiment";

                    $updateExp = $this->experiment->updateOne($id, ['experimentDesignType'=>$experimentDesign]);
                    
                    $designId = $this->designModel->getOldDesignInformation($experimentDesign);
                    
                    //get fields for design
                    $parameterArray = $this->experimentModel->getJsonConfig($designId, 'ui');

                    //get fields for metadata
                    $parameterMetadata = $this->experimentModel->getJsonConfig($designId, 'metadata');

                    //get dummy for json input
                    $parameterJson = $this->experimentModel->getJsonConfig($designId, 'json');

                } else {

                    //get fields for design
                    $parameterArray = $this->designModel->getDesignParameters($designId, 'ui', $designFieldsArray, $isSparse);

                    $parameterArray['randomization'] = $this->analysisModel->convertDesignParameters($parameterArray['randomization'], 'randomization');

                    $parameterArray['layout'] = $this->analysisModel->convertDesignParameters($parameterArray['layout'], 'layout');

                    // //get fields for metadata
                    $parameterMetadata = $this->designModel->getDesignParameters($designId, 'metadata', $designFieldsArray, $isSparse);

                    //get dummy for json input
                    $parameterJson = $this->designModel->getDesignParameters($designId, 'json', $designFieldsArray, $isSparse);

                    $jsonArray['design_code'] = $experimentDesign;
                    $experimentJson['status'] = 'incomplete';
                    $experimentJson['entityType'] = "parameter_sets";
                }

                $jsonArray['design_ui'] = $parameterArray;
                $jsonArray['design_id'] = $designId;
                $jsonArray['design_name'] = $design;
               
                $jsonArray['design_ui']['show_layout'] = FALSE;

                $jsonArray['metadata'] = $parameterMetadata;
                $jsonArray['json_file']=$parameterJson;

                //create record in experiment data
                $experimentJson['design'] = $experimentDesign;
                $experimentJson['programDbId'] = $model['programDbId']."";
               
                $experimentJson['mandatoryInfo'] = json_encode($jsonArray);
                $experimentJson['entryCount'] = "0";
                $experimentJson['plotCount'] = "0";
                $experimentJson['checkCount'] = "0";
                $experimentJson['repCount'] = "0";
                $experimentJson['tempFinalCabinetDbId'] = "0";

                $result = $this->planTemplate->createPlanTemplate($experimentJson);
            } else {
                $experimentJson = $planTemplateArray['data'][0];
                
                if($designUiSwitch == 'old' && $acrossEnvDesign == 'none'){   //return to old design

                    $experimentJson['status'] = 'draft';
                    $experimentJson['entityType'] = "experiment";

                    $updateExp = $this->experiment->updateOne($id, ['experimentDesignType'=>$experimentDesign]);
                    
                    $designId = $this->designModel->getOldDesignInformation($experimentDesign);
                    
                    //get fields for design
                    $parameterArray = $this->experimentModel->getJsonConfig($designId, 'ui');

                    //get fields for metadata
                    $parameterMetadata = $this->experimentModel->getJsonConfig($designId, 'metadata');

                    //get dummy for json input
                    $parameterJson = $this->experimentModel->getJsonConfig($designId, 'json');

                } else {
                    //get fields for design
                    $parameterArray = $this->designModel->getDesignParameters($designId, 'ui', $designFieldsArray, $isSparse);

                    //get fields for metadata
                    $parameterMetadata = $this->designModel->getDesignParameters($designId, 'metadata', $designFieldsArray, $isSparse);

                    //get dummy for json input
                    $parameterJson = $this->designModel->getDesignParameters($designId, 'json', $designFieldsArray, $isSparse);

                    $jsonArray['design_code'] = $experimentDesign;
                    $experimentJson['status'] = 'incomplete';
                    $experimentJson['entityType'] = "parameter_sets";
                }

                
                $parameterArray['randomization'] = $this->analysisModel->convertDesignParameters($parameterArray['randomization'], 'randomization');
                $parameterArray['layout'] = $this->analysisModel->convertDesignParameters($parameterArray['layout'], 'layout');
                $jsonArray['design_ui'] = $parameterArray;
                $jsonArray['design_ui']['show_layout'] = FALSE;
                $jsonArray['design_id'] = $designId;
                $jsonArray['design_name'] = $design;

                $jsonArray['metadata'] = $parameterMetadata;

                $jsonArray['json_file']=$parameterJson;

                //update plan template
                $experimentJsonTemp = array();

                $experimentJsonTemp['design'] = $experimentDesign;
                $experimentJsonTemp['mandatoryInfo'] = json_encode($jsonArray);

                // Update experiment design
                $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], $experimentJsonTemp);

            }

            $valuesArray['Additional'] = $_POST['additionalData'];

            if($designUiSwitch == 'new' && $acrossEnvDesign == 'none'){   
                $this->designModel->saveAdditionalInfo($valuesArray, $id, $acrossEnvDesign);
            }

            //Update experiment design
            $experimentStatus = 'entry list created';
            $statusArray = explode(';', $model['experimentStatus']);
            if(in_array("entry list created", $statusArray)){
                $experimentStatus = 'entry list created';
            }else if(in_array("entry list incomplete seed sources", $statusArray)){
                 $experimentStatus = 'entry list incomplete seed sources';
            }
            
            $this->experiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
            
            /**ADD deletion of succeeding records**/

            return json_encode('ok');
        }

    }


    /**
     * Generates json input format for analysis module
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     * @param boolean $switchInput flag if data is just for boolean fields
     *
     */
    public function actionSaveParameters($id, $program, $processId, $switchInput = false){

        if(isset($_POST)){

            $valuesArray = $_POST;
            //check if there's existing json input for the experiment design
            $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id", 'status'=>'incomplete']);

            $experimentJson = $experimentJsonArray['data'][0];

            $plantTemplateDbId = $experimentJson['planTemplateDbId'];

            $experiment = $this->experiment->getExperiment($id);
            $jsonArray = json_decode($experimentJson['mandatoryInfo'], true);
            
            $jsonInput = array();

            $parameterRandArray = $jsonArray['design_ui']['randomization'];
            $indexRandIds = array_column($parameterRandArray, 'code');

            $parameterLayoutArray = $jsonArray['design_ui']['layout'];
            $indexLayoutIds = array_column($parameterLayoutArray, 'code');
            

            $occurrencesCount = 0;
            foreach($valuesArray['ExperimentDesign'] as $key=>$value){
              
                if($key != 'DESIGN'){
                    $indexRand = array_search($key,$indexRandIds);
                    $indexLayout = array_search($key,$indexLayoutIds);
                    
                    if($value == ""){
                        $value = 0;
                    }
                    if($indexRand !== false && isset($parameterRandArray[$indexRand])){
                      
                        $valuesArray['ExperimentDesign'][$key] = $value;
                        $parameterRandArray[$indexRand]['value'] = $value;
                    }
                    if($indexLayout !== false && isset($parameterLayoutArray[$indexLayout])){
                      
                        $valuesArray['ExperimentDesign'][$key] = $value;
                        $parameterLayoutArray[$indexLayout]['value'] = $value;
                    }
                    if($key == 'nTrial' || $key == 'nFarm'){
                        $occurrencesCount = intval($value);
                        $parameterRandArray[$indexRand]['value'] = intval($value);
                    }
                    $jsonInput[$key] = $value;
                } else {

                    $design = $value;
                }
            }

            if(isset($valuesArray['uiGroups'])){
                $jsonArray['design_ui']['groups'] = $valuesArray['uiGroups'];
            }
            $jsonArray['design_ui']['randomization'] = $parameterRandArray;
            $jsonArray['design_ui']['layout'] = $parameterLayoutArray;

            $experimentJsonTemp['mandatoryInfo'] = json_encode($jsonArray);
            
            if(!$switchInput){
                $experimentJsonTemp['status'] = "draft";
            }
            
            // Update experiment design
            $this->planTemplate->updateOne($plantTemplateDbId, $experimentJsonTemp);

            // Update experiment remarks for workflow
            $this->experiment->updateOne($id, ["remarks"=>"generate"]);

            return json_encode('success');
            
        }
    }


    /**
     * Save additional details for sparse testing
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     */
    public function actionSaveAddtlDetails($id, $program, $processId){
         
        if(isset($_POST)){

            $valuesArray = $_POST;

            $this->designModel->saveAdditionalInfo($valuesArray, $id);
            return json_encode('success');
        }
    }

    /**
     * Delete parameter set
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     */
    public function actionDeleteParamSet($id, $program, $processId){
         
        if(isset($_POST)){

            if(isset($_POST['all'])){
                //delete all template records
                $planTemplates = $this->planTemplate->searchAll(['entityDbId'=>"equals $id"]);
                
                if($planTemplates['totalCount'] > 0){
                    $planTemplateIds = array_column($planTemplates['data'],'planTemplateDbId');
                    $this->planTemplate->deleteMany($planTemplateIds);
                }
            } else {
                $this->planTemplate->deleteOne($_POST['planTemplateId']);
            }
            return json_encode('success');
        }
    }

    /**
     * Save selection of entries
     *
     */
    public function actionSaveSelectedEntries(){
         
        if(isset($_POST)){
            extract($_POST);
            $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id", 'status'=>'incomplete'], 'sort=planTemplateDbId:ASC');

            $experimentJson = $experimentJsonArray['data'][0];

            $planTemplateDbId = $experimentJson['planTemplateDbId'];

            $experiment = $this->experiment->getExperiment($id);
            $jsonArray = json_decode($experimentJson['mandatoryInfo'], true);
            
            $jsonInput = array();

            $parameterRandArray = $jsonArray['design_ui']['randomization'];
            $indexRandIds = array_column($parameterRandArray, 'code');

            $indexRand = array_search('nCheck',$indexRandIds);
            $parameterRandArray[$indexRand]['value'] = count($selectedIds);

            $indexRand = array_search('methodReplicateCheckEntries',$indexRandIds);
            $parameterRandArray[$indexRand]['value'] = 'manual';

            //search for methodReplicatesChecks
            $parameterRandArray[]  = [
                  "id" => "1",
                  "code" => "nCheckListSet",
                  "name" => "CHECK_LIST_SET",
                  "type" => "input",
                  "dataType" => "string",
                  "description" => "List of selected checks",
                  "variableLabel" => "Check List Set",
                  "orderNumber" => 1,
                  "parentPropertyId" => 9,
                  "layoutVariable" => false,
                  "value" => $selectedIds,
                  "ui"=> [
                      "id"=> "1",
                      "visible"=> false,
                      "minimum"=> null,
                      "maximum"=> null,
                      "multiple"=> false,
                      "defaultValue"=> "1",
                      "disabled"=> false,
                      "catalogue"=> false
                  ],
                  "rules"=> []
              ];

            $jsonArray['design_ui']['randomization'] = $parameterRandArray;
            $experimentJsonTemp['mandatoryInfo'] = json_encode($jsonArray);
            
            // Update experiment design
            $this->planTemplate->updateOne($planTemplateDbId, $experimentJsonTemp);

            return json_encode('success');
        }
    }

    /**
     * View layout of parameter set
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     * @param integer $planTemplateDbId record id of the plan template
     * @param string $label label to be used for viewing layout
     */
    public function actionViewLayout($id, $program, $processId, $planTemplateDbId, $label='plotno'){
    
        $model = $this->experiment->getExperiment($id);

        //get layout information
        $layoutData = $this->designModel->getLayoutParamSet($id, $planTemplateDbId, $label);
        $layoutData['processId'] = $processId;
        $layoutData['id'] = $id;
        $layoutData['program'] = $program;
        $layoutData['model'] = $model;
        return $this->render('_design_layout_view.php', $layoutData);
    }

    /**
     * Submit randomization request
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     * */
    public function actionSubmitRandomization($id, $program, $processId){

        //generate request parameters
        $generatedRequest = $this->designModel->generateRequestParameters($id);
        
        return json_encode($generatedRequest);
    }

    /**
     * Check randomization request
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     * */
    public function actionCheckDesignResults($id, $program, $processId){
        
        //get all draft parameter sets
        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id"], 'sort=planTemplateDbId:DESC');
        $templates = $experimentJsonArray['data'];
        $plantTemplateDbIds = array_column($templates, 'planTemplateDbId');

        $experimentJson = $experimentJsonArray['data'][0];
        $experiment = $this->experiment->getExperiment($id);

        //Get request status
        $requestStatus = $this->analysisModel->getRequestStatus($experimentJson['requestDbId'], 'sparse');
        $plantTemplateDbIds = array_column($experimentJsonArray['data'], 'planTemplateDbId');

        if($experimentJson['status'] != 'deletion in progress'){
            if(isset($requestStatus['data']['findRequest']['status']) && in_array($requestStatus['data']['findRequest']['status'], ['new', 'in_progress', 'submitted', 'processing_custom_design'])){
            
                $occurrences = $this->occurrence->searchAll([
                    'fields'=>'occurrence.experiment_id as experimentDbId|occurrence.occurrence_name as occurrenceName|occurrence.id AS occurrenceDbId',
                    'experimentDbId' => "equals ".$id
                ],'sort=occurrenceName:ASC');

                //checked plot records
                $plotsPopulatedFlag = true;
                if($occurrences['totalCount'] > 0){   //check if there are plot records already
                    foreach($occurrences['data'] as $occurrence){
                        $plotRecords = $this->occurrence->searchAllPlots($occurrence['occurrenceDbId'], null,'limit=1&sort=plotNumber:ASC');
                        if($plotRecords['totalCount'] == 0){
                            $plotsPopulatedFlag = false;
                            break;
                        }
                    }
                }
                
                if($plotsPopulatedFlag){
                    $response = $this->designModel->getDesignResponse($id, 'finished', $experimentJson, $experiment, $plantTemplateDbIds);
                    return json_encode($response);

                } else {
                    $response = $this->designModel->getDesignResponse($id, 'in progress', $experimentJson, $experiment, $plantTemplateDbIds);
                }
               
            } else if(in_array($experimentJson['status'], ['randomized','uploaded']) || (isset($requestStatus['data']['findRequest']['status']) && in_array($requestStatus['data']['findRequest']['status'], ['completed']))){

                $response = $this->designModel->getDesignResponse($id, 'finished', $experimentJson, $experiment, $plantTemplateDbIds);

            } else if(isset($requestStatus['errors']) || (isset($requestStatus['data']['findRequest']['status']) && in_array($requestStatus['data']['findRequest']['status'], ['failed', 'expired']))){

                $response = $this->designModel->getDesignResponse($id, 'failed', $experimentJson, $experiment, $plantTemplateDbIds);
                $response['message'] = $requestStatus['errors'] ?? $response['message'];
                $this->planTemplate->updateMany($plantTemplateDbIds, ['remarks'=>$response['message']]);
            } else {
                $response = $this->designModel->getDesignResponse($id, 'failed', $experimentJson, $experiment, $plantTemplateDbIds);
                $response['message'] = $requestStatus['errors'] ?? $response['message'];
                $this->planTemplate->updateMany($plantTemplateDbIds, ['remarks'=>$response['message']]);
            }

        }  else {

            //retrieve process background
            $bgAddtlParams = ["entityDbId"=>"equals ".$id,"entity"=>"equals PLOT", "endpointEntity"=>'equals PLOT',"application"=>"equals EXPERIMENT_CREATION"];

            $response = $this->backgroundJob->searchAll($bgAddtlParams, 'sort=backgroundJobDbId:DESC');
            
            if (isset($response['success']) && $response['success'] == 200) {
                if(strtolower($response['data'][0]['jobStatus']) == 'done'){

                    //update plan status
                    $planStatus = 'draft';
                    
                    //update plan template status
                    $this->planTemplate->updateMany($plantTemplateDbIds, ['status'=>$planStatus]);

                    //if upload workflow, delete all templates
                    if($experiment['remarks'] == 'upload'){
                        $this->planTemplate->deleteMany($plantTemplateDbIds);
                    }

                    //update experiment status
                    $status = explode(';',$experiment['experimentStatus']);
        
                    //Update experiment design
                    $experimentStatus = 'entry list created';
                    
                    $update = $this->experiment->updateOne($id, ['experimentStatus'=>$experimentStatus, 'notes'=>null]);

                    $params = [
                        "jobIsSeen" => "true",
                        "jobRemarks" => "REFLECTED"
                    ];
                    $updateBgProcess = $this->backgroundJob->updateOne($response['data'][0]['backgroundJobDbId'], $params);

                    $response['expStatus'] = $experimentStatus;
                    $response['success'] = 'delete successful';
                    $response['planStatus'] = $planStatus;
                    $response['deletePlots'] = 'false';
                    $response['message'] = 'Plots successfully deleted. Please proceed in submitting your request.';
                } else if(strtolower($response['data'][0]['jobStatus']) == 'failed'){

                    $planStatus = 'draft';
                    //update plan template status
                    $this->planTemplate->updateMany($plantTemplateDbIds, ['status'=>$planStatus]);

                    $response['expStatus'] = $experiment['experimentStatus'];
                    $response['success'] = 'deletion failed';
                    $response['planStatus'] = $planStatus;
                    $response['deletePlots'] = 'true';
                    $response['message'] = 'Deletion of plot records failed. Please try again.';

                    $params = [
                        "jobIsSeen" => "true",
                        "jobRemarks" => "REFLECTED"
                    ];
                    $updateBgProcess = $this->backgroundJob->updateOne($response['data'][0]['backgroundJobDbId'], $params);
                } else {

                    $response['expStatus'] = $experiment['experimentStatus'];
                    $response['success'] = 'deletion in progress';
                    $response['planStatus'] = 'deletion in progress';
                    $response['deletePlots'] = 'true';
                    $response['message'] = '';
                }
            } else {
                $planStatus = 'draft';
                //update plan template status
                $this->planTemplate->updateMany($plantTemplateDbIds, ['status'=>$planStatus]);

                $response['expStatus'] = $experiment['experimentStatus'];
                $response['success'] = 'deletion failed';
                $response['planStatus'] = $planStatus;
                $response['deletePlots'] = 'true';
                $response['message'] = 'Deletion of plot records failed. Please try again.';
            }
        }
        
        return json_encode($response);
    }

    /**
     * Update parameter set
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     */
    public function actionUpdateParamSet($id, $program, $processId){
         
        if(isset($_POST)){
            
            $this->planTemplate->updateOne($_POST['planTemplateId'], ['status'=>'incomplete']);
            return json_encode('success');
        }
    }

    /**
     * Perform additional validation rules
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     */
    public function actionValidateList($id, $program, $processId){
         
        if(isset($_POST)){
            
            $validation = json_decode($_POST['addtlValidation'], true);
            $checkEntries = (int) $_POST['checkEntries'];
            $testEntries = (int) $_POST['testEntries'];
            $passValidation = true;
            $validationMessage = '';
            $compOptr = [
                '>=' => 'greater than or equal to ',
                '<=' => 'less than or equal to ',
                '<' => 'less than ',
                '>' => 'greater than ',
                '==' => 'equals ',
            ];
            foreach($validation as $key=>$var){
                $entryType = 'test';
                if($key == 'checkList'){
                    $entryType = 'check';
                }

                if($var['variable'] == 'nRep'){
                    //check nRep values for check entries
                    $params['variableAbbrev']='equals TIMES_REP';
                    $params['dataValue'] = $compOptr[$var['condition']].(int) $var['value'];
                    $params['entryType'] = $entryType;
                    $params['fields'] = 'entryData.id AS entryDataDbId | entry.entry_type AS entryType|variable.abbrev AS variableAbbrev | entryData.data_value AS dataValue';

                    $exptData = $this->experiment->searchAllEntryData($id,$params, 'limit=1',false);
                    
                    if($key == 'checkList'){
                        if(isset($exptData['totalCount']) && $exptData['totalCount'] != $checkEntries){
                            $passValidation = false;
                            $validationMessage .= 'No. of Replicates for all check entries should be '.$compOptr[$var['condition']].(int) $var['value'].".<br>";
                        }
                    }
                    if($key == 'testList'){
                        if(isset($exptData['totalCount']) && $exptData['totalCount'] != $testEntries){
                            $passValidation = false;
                            $validationMessage .= 'No. of Replicates for all test entries should be '.$compOptr[$var['condition']].(int) $var['value'].".<br>";
                        }
                    }
                }
            }

            return json_encode([
                'success' => $passValidation,
                'message' => $validationMessage
            ]);
        }
    }

    /**
     * Change workflow for design, generate or upload
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     */
    public function actionUpdateWorkflow($id, $program, $processId){
         
        if(isset($_POST)){
            $this->experiment->updateOne($id, ['remarks'=>$_POST['workflowVal']]);
            return json_encode('success');
        }
    }

    /**
     * Change workflow for design, generate or upload
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     */
    public function actionGetRepsInfo($id, $program){

        $param = ['entityType'=>'parameter_sets', 'entityDbId'=> "equals $id", 'status'=>'equals incomplete'];
        $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);

        $repTest = 0;
        $repCheck = 0;
        if(!empty($jsonInput)){
            $plantTemplateDbId = $jsonInput['planTemplateDbId'];
            $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);

            $repTest = isset($jsonArr['design_ui']['repTest']) ? array_sum(array_values($jsonArr['design_ui']['repTest'])) : 0;
            $repCheck = isset($jsonArr['design_ui']['repCheck']) ? array_sum(array_values($jsonArr['design_ui']['repCheck'])) : 0;
        }
        return json_encode([
            'repTest' => $repTest,
            'repCheck' => $repCheck
        ]);
    }


    /**
     * Change workflow for design, generate or upload
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     */
    public function actionUpdateDesignUi($id, $program, $processId){
         
        if(isset($_POST)){
            $entryListId = $this->entryList->getEntryListId($id);
            $this->entryList->updateOne($entryListId, ['description'=>$_POST['designUiSwitch']]);

            //delete all template records
            $planTemplates = $this->planTemplate->searchAll(['entityDbId'=>"equals $id"]);
            
            if($planTemplates['totalCount'] > 0){
                $planTemplateIds = array_column($planTemplates['data'],'planTemplateDbId');
                $this->planTemplate->deleteMany($planTemplateIds);
            }
            
            return json_encode('success');
        }
    }


}