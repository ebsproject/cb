<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use app\models\BackgroundJob;
use app\models\User;

use app\modules\experimentCreation\models\BrowserModel;

use app\components\B4RController;
use app\controllers\Dashboard;

use Yii;

use function GuzzleHttp\json_decode;

class BrowseController extends B4RController
{
    protected $browserModel;
    protected $backgroundJob;
    protected $dashboard;
    protected $user;

    public function __construct(
        $id,
        $module,
        BrowserModel $browserModel,
        BackgroundJob $backgroundJob,
        Dashboard $dashboard,
        $config = [])
    {
        $this->browserModel = $browserModel;
        $this->backgroundJob = $backgroundJob;
        $this->dashboard = $dashboard;
        parent::__construct($id,$module,$config);
        
    }

    /**
     * Returns new notifications from background processes
     */
    public function actionNewNotificationsCount()
    {
        $this->user = new User();
        $userId = $this->user->getUserId();
        

        //New updates once new api params are in place
        $params = [
            'creatorDbId' => (string) $userId,
            'application' => 'equals EXPERIMENT_CREATION',
            'distinctOn' => 'entityDbId',
            'isSeen' => 'false'
        ];
        
        $filter = 'sort=modificationTimestamp:desc';
        
        $result = $this->backgroundJob->searchAll($params, $filter);

        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        } else{
            //Update the status
            $this->browserModel->updateExptBackgroundStatus($userId);
        }

        return json_encode($result["totalCount"]);
    }

    /** 
     * Renders notifications from background processes 
     */
    public function actionPushNotifications()
    {
        $this->user = new User();
        $userId = $this->user->getUserId();
        
        $params = [
            "creatorDbId" => "equals ".$userId,
            "application" => "equals EXPERIMENT_CREATION",
            "distinctOn"=> "entityDbId",
            "isSeen" => "false",
            "workerName" => "not equals BuildCsvData"
        ];
        
        $result = $this->backgroundJob->searchAll($params, "sort=modificationTimestamp:desc");
        
        //get export transactions
        $paramsExport = [
            "creatorDbId" => "equals ".$userId,
            "application" => "equals EXPERIMENT_CREATION",
            "isSeen" => "false",
            "workerName" => "equals BuildCsvData"
        ];
        $resultExport = $this->backgroundJob->searchAll($paramsExport, "sort=modificationTimestamp:desc");

        $exportNotifs = $resultExport["data"] ?? [];

        $newNotifications = array_merge($result["data"], $exportNotifs);
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->backgroundJob->getNotifications($newNotifications);
        
        //get old notifications
        $params = [
            "creatorDbId" => "equals ".$userId,
            "isSeen" => "true",
            "application" => "equals EXPERIMENT_CREATION"
        ];

        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);
        
        $notifications = $result["data"];
        $seenNotifications = $this->backgroundJob->getNotifications($notifications);
        
        // update unseen notifications to seen
        $params = [
            "jobIsSeen" => "true"
        ];
        
        $result = $this->backgroundJob->update($newNotifications, $params);
        
        return $this->renderAjax(
            'notifications',
            [
                "unseenNotifications" => $unseenNotifications,
                "seenNotifications" => $seenNotifications,
                "unseenCount" => $unseenCount
            ]
        );
    }

    /**
     * Retrieve the status of the transaction
     * @return string transaction status 
     */
    public function actionGetTransactionStatus()
    {
        $url = 'background-jobs-search';
        $params = [
            "backgroundJobDbId" => "" . $_POST['backgroundJobDbId'],
        ];
        $data = Yii::$app->api->getResponse('POST', $url, json_encode($params), '', true);
        
        if ($data['status'] == 200) {
            $data = $data['body'];
           
            return isset($data['result']['data']) ? $data['result']['data'][0]['status'] : '';
        }
    }

    /**
     * Returns new notifications from background processes to check the deletion of plots and/or planting instruction records
     */
    public function actionBackgroundOccurrenceCheck($id)
    {
        //New updates once new api params are in place
        $params = [
            "application" => "equals EXPERIMENT_CREATION",
            'distinctOn'=> 'entityDbId',
            'endpointEntity' => "equals OCCURRENCE",
            'method' => "equals DELETE",
            'entityDbId' => "equals $id"
        ];
        $result = $this->backgroundJob->searchAll($params,"sort=modificationTimestamp:desc");
        
        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        } else{ 
           $this->browserModel->bgOccurrenceStatusUpdate($id, $result);
        }
    }

     /**
     * Returns a notif for the creation of plot records
     * @param integer $id Experiment ID
     */
    public function actionCheckPlotRecord($id)
    {
        //New updates once new api params are in place
        $params = [
            "application" => "equals EXPERIMENT_CREATION",
            'distinctOn'=> 'entityDbId',
            'endpointEntity' => "equals OCCURRENCE",
            'method' => "equals POST",
            'entityDbId' => "equals $id"
        ];
        $result = $this->backgroundJob->searchAll($params,"sort=modificationTimestamp:desc");
        
        if ($result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        } else{
            $return = $this->browserModel->plotRecordStatusUpdate($id, $result);
            $return = ($return != null) ? json_decode($return, true) : [];
         
            if(isset($return['success']) && isset($return['createPlantingInstruction'])){
                return json_encode([
                    'success' => $return['success'],
                    'error' => $return['message'],
                    'createPlantingInstruction' => $return['createPlantingInstruction']
                ]);
            }else{
                return json_encode(true);
            }
        }
    }
}