<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace app\modules\experimentCreation\controllers;
// NOTE: add classes here in alphabetical order
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\ExperimentProtocol;
use app\models\ListMember;
use app\models\OccurrenceData;
use app\models\Protocol;
use app\models\Variable;

use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\ProtocolModel;

use app\components\B4RController;
use app\controllers\Dashboard;

use Yii;

/**
 * Controller for Protocol Tab
 */
class ProtocolController extends B4RController
{
    protected $protocolModel;
    protected $dashboard;
    protected $experimentModel;
    protected $experimentData;
    protected $experimentProtocol;
    protected $listMember;
    protected $experiment;
    protected $occurrenceData;
    protected $protocol;
    protected $variable;

    public function __construct(
        $id, 
        $module, 
        ProtocolModel $protocolModel,
        Dashboard $dashboard,
        ExperimentModel $experimentModel,
        ExperimentData $experimentData,
        ExperimentProtocol $experimentProtocol,
        ListMember $listMember,
        Experiment $experiment,
        OccurrenceData $occurrenceData,
        Protocol $protocol,
        Variable $variable,
        $config = [])
    {
        $this->protocolModel = $protocolModel;
        $this->dashboard = $dashboard;
        $this->experimentModel = $experimentModel;
        $this->experimentData = $experimentData;
        $this->experimentProtocol = $experimentProtocol;
        $this->listMember = $listMember;
        $this->experiment = $experiment;
        $this->occurrenceData = $occurrenceData;
        $this->protocol = $protocol;
        $this->variable = $variable;
        parent::__construct($id, $module, $config);
    }

    /**
     * Render the planting protocol view
     * 
     * @param Integer $id experiment identifier
     * @param String $program program identifier
     * @param String $processId program identifier
     * @param String $isViewOnly flag for view-only mode
     * @param Integer $occurrenceDbId occurrence record id
     * @param String $previewOnly flag for view-only mode
     */
     public function actionPlantingProtocols($id, $program, $processId, $isViewOnly = '', $occurrenceDbId = null, $previewOnly = '', $shouldUpdateOccurrenceProtocols = '', $occurrenceDbIdsString = '', $hasMultipleSelectedOccurrences = ''){
        $model = $this->findModel($id);
        $this->layout = '@app/views/layouts/dashboard';
         
        // This will determine if there is no protocol record yet...
        // ...for this new experiment
        $response = $this->experiment->searchAll(
            [
                'fields' => 'experiment.id AS experimentDbId | experiment.experiment_code AS experimentCode',
                'experimentDbId' => $id,
            ],
            'limit=1&sort=experimentDbId:desc',
            true
        );
        $data = $response['data'][0] ?? [];

        $experimentCode = $data['experimentCode'] ?? '';
        $protocolCode = 'PLANTING_PROTOCOL_' . $experimentCode;
    
        $protocol = $this->protocol->searchAll([
            'protocolCode' => "$protocolCode"
        ]);
        
        $protocolId = isset($protocol['data'][0]['protocolDbId']) ? $protocol['data'][0]['protocolDbId'] : null;

        // If there is no protocol yet, create an experiment protocol record...
        // Else, continue to the next code block
        if (!$protocolId) {
            $protocolId = $this->protocol->createExperimentProtocols($id, 'planting');
        }

        if(isset($_POST) && !empty($_POST)){
            $formData = Yii::$app->request->post();
            $shouldUpdateOccurrenceProtocols = isset($formData['shouldUpdateOccurrenceProtocols']) ? $formData['shouldUpdateOccurrenceProtocols'] : $shouldUpdateOccurrenceProtocols;
            $occurrenceDbIdsString = isset($formData['occurrenceDbIdsString']) ? $formData['occurrenceDbIdsString'] : $occurrenceDbIdsString;

            if ($shouldUpdateOccurrenceProtocols) {
                return $this->occurrenceData->saveOccurrenceData($formData, $occurrenceDbIdsString, 'planting');

            } else {
                // Save experiment
                $id = $this->experimentModel->saveExperiment(Yii::$app->request->post(), $program, 'experiment', $model['experimentDbId'],'protocol','planting');
                return true;
            }
        }
        $defaultFilters = $this->dashboard->getFilters();
        $dashProgram = $defaultFilters->program_id;
        $data = $this->protocolModel->getPlantingProtocolData($id, $program, $processId, $model, $dashProgram, $isViewOnly, $occurrenceDbId, $previewOnly, $shouldUpdateOccurrenceProtocols, $hasMultipleSelectedOccurrences); 

        // Determine whether Planting section is in View Only mode or not
        $data['cssDisplaySaveButton'] = (($isViewOnly === '1' || $previewOnly === '1') ? 'display: none;' : '');
        $data['isViewOnly'] = $isViewOnly;
        $data['shouldUpdateOccurrenceProtocols'] = $shouldUpdateOccurrenceProtocols;
        $data['occurrenceDbId'] = $occurrenceDbId;
        $data['occurrenceDbIdsString'] = $occurrenceDbIdsString;
        $data['hasMultipleSelectedOccurrences'] = $hasMultipleSelectedOccurrences;
        $data['previewOnly'] = $previewOnly;

        return $this->render('_planting-protocols', $data);
     }

     /**
     * Render the harvest protocol view
     * 
     * @param Integer $id experiment identifier
     * @param String $program program identifier
     * @param String $processId program identifier
     * @param String $isViewOnly flag for view-only mode
     * @param Integer $occurrenceDbId occurrence record id
     * @param String $previewOnly flag for view-only mode
    */
     public function actionHarvestProtocol($id, $program, $processId, $isViewOnly = '', $occurrenceDbId = null, $previewOnly = '', $shouldUpdateOccurrenceProtocols = '', $occurrenceDbIdsString = '', $hasMultipleSelectedOccurrences = ''){
        $model = $this->findModel($id);
        $this->layout = '@app/views/layouts/dashboard';
         
        // This will determine if there is no protocol record yet...
        // ...for this new experiment
        $response = $this->experiment->searchAll(
            [
                'fields' => 'experiment.id AS experimentDbId | experiment.experiment_code AS experimentCode',
                'experimentDbId' => $id,
            ],
            'limit=1&sort=experimentDbId:desc',
            true
        );
        $data = $response['data'][0] ?? [];

        $experimentCode = $data['experimentCode'] ?? '';
        $protocolCode = 'HARVEST_PROTOCOL_' . $experimentCode;
    
        $protocol = $this->protocol->searchAll([
            'protocolCode' => "$protocolCode"
        ]);
        
        $protocolId = isset($protocol['data'][0]['protocolDbId']) ? $protocol['data'][0]['protocolDbId'] : null;

        // If there is no protocol yet, create an experiment protocol record...
        // Else, continue to the next code block
        if (!$protocolId) {
            $protocolId = $this->protocol->createExperimentProtocols($id, 'harvest');
        }

        if(isset($_POST) && !empty($_POST)){
            $formData = Yii::$app->request->post();
            $shouldUpdateOccurrenceProtocols = isset($formData['shouldUpdateOccurrenceProtocols']) ? $formData['shouldUpdateOccurrenceProtocols'] : $shouldUpdateOccurrenceProtocols;
            $occurrenceDbIdsString = isset($formData['occurrenceDbIdsString']) ? $formData['occurrenceDbIdsString'] : $occurrenceDbIdsString;

            if ($shouldUpdateOccurrenceProtocols) {
                return $this->occurrenceData->saveOccurrenceData($formData, $occurrenceDbIdsString, 'harvest');

            } else {
                // Save experiment
                $id = $this->experimentModel->saveExperiment(Yii::$app->request->post(), $program, 'experiment', $model['experimentDbId'],'protocol','harvest');
                return true;
            }
        }
   
        $data = $this->protocolModel->getExperimentProtocolData($id, $program, $processId, $model,'HARVEST_PROTOCOL', $isViewOnly, $occurrenceDbId, $previewOnly, $shouldUpdateOccurrenceProtocols, $hasMultipleSelectedOccurrences); 
        
        // Determine whether Planting section is in View Only mode or not
        $data['cssDisplaySaveButton'] = (($isViewOnly === '1' || $previewOnly === '1') ? 'display: none;' : '');
        $data['shouldUpdateOccurrenceProtocols'] = $shouldUpdateOccurrenceProtocols;
        $data['occurrenceDbIdsString'] = $occurrenceDbIdsString;
        $data['hasMultipleSelectedOccurrences'] = $hasMultipleSelectedOccurrences;

        return $this->render('_harvest-protocol', $data);
     }

    /**
     * Render the pollination protocol view
     * 
     * @param Integer $id experiment identifier
     * @param String $program program identifier
     * @param String $processId program identifier
     * @param String $isViewOnly flag for view-only mode
     * @param Integer $occurrenceDbId occurrence record id
     * @param String $previewOnly flag for view-only mode
    */
    public function actionPollinationProtocol($id, $program, $processId, $isViewOnly = '', $occurrenceDbId = null, $previewOnly = '', $shouldUpdateOccurrenceProtocols = '', $occurrenceDbIdsString = '', $hasMultipleSelectedOccurrences = ''){
        $model = $this->findModel($id);
        $this->layout = '@app/views/layouts/dashboard';
         
        // This will determine if there is no protocol record yet...
        // ...for this new experiment
        $response = $this->experiment->searchAll(
            [
                'fields' => 'experiment.id AS experimentDbId | experiment.experiment_code AS experimentCode',
                'experimentDbId' => $id,
            ],
            'limit=1&sort=experimentDbId:desc',
            true
        );
        $data = $response['data'][0] ?? [];

        $experimentCode = $data['experimentCode'] ?? '';
        $protocolCode = 'POLLINATION_PROTOCOL_' . $experimentCode;
    
        $protocol = $this->protocol->searchAll([
            'protocolCode' => "$protocolCode"
        ]);
        
        $protocolId = isset($protocol['data'][0]['protocolDbId']) ? $protocol['data'][0]['protocolDbId'] : null;

        // If there is no protocol yet, create an experiment protocol record...
        // Else, ignore this and then continue to the next code block
        if (!$protocolId) {
            $protocolId = $this->protocol->createExperimentProtocols($id, 'pollination');
        }

        if(isset($_POST) && !empty($_POST)){
            $formData = Yii::$app->request->post();
            $shouldUpdateOccurrenceProtocols = isset($formData['shouldUpdateOccurrenceProtocols']) ? $formData['shouldUpdateOccurrenceProtocols'] : $shouldUpdateOccurrenceProtocols;
            $occurrenceDbIdsString = isset($formData['occurrenceDbIdsString']) ? $formData['occurrenceDbIdsString'] : $occurrenceDbIdsString;

            if ($shouldUpdateOccurrenceProtocols) {
                return $this->occurrenceData->saveOccurrenceData($formData, $occurrenceDbIdsString, 'pollination');

            } else {
                // Save experiment
                $id = $this->experimentModel->saveExperiment(Yii::$app->request->post(), $program, 'experiment', $model['experimentDbId'],'protocol','pollination');
                return true;
            }
        }
   
        $data = $this->protocolModel->getExperimentProtocolData($id, $program, $processId, $model,'POLLINATION_PROTOCOL', $isViewOnly, $occurrenceDbId, $previewOnly, $shouldUpdateOccurrenceProtocols, $hasMultipleSelectedOccurrences); 
    
        // Determine whether Planting section is in View Only mode or not
        $data['cssDisplaySaveButton'] = (($isViewOnly === '1' || $previewOnly === '1') ? 'display: none;' : '');
        $data['shouldUpdateOccurrenceProtocols'] = $shouldUpdateOccurrenceProtocols;
        $data['occurrenceDbIdsString'] = $occurrenceDbIdsString;
        $data['hasMultipleSelectedOccurrences'] = $hasMultipleSelectedOccurrences;

        return $this->render('_pollination-protocol', $data);
     }


    /**
     * Render traits protocol view
     * 
     * @param Integer $id experiment identifier
     * @param String $program program identifier
     * @param String $processId program identifier
     * @param String $isViewOnly flag for view-only mode
     * @param String $previewOnly flag for view-only mode
     * @param string $shouldUpdateOccurrenceProtocols flag for manage protocol mode
     * @param string $occurrenceDbIdsString list of occurrence identifiers, separated by `|`
     */
     public function actionTraitsProtocols($id, $program, $processId, $isViewOnly = '', $previewOnly = '', $occurrenceDbId = null, $shouldUpdateOccurrenceProtocols = '', $occurrenceDbIdsString = '', $hasMultipleSelectedOccurrences = ''){

        //Get the configuration
        $this->layout = '@app/views/layouts/dashboard';

        $occurrenceDbIdsString = empty($occurrenceDbIdsString) ? (string)$occurrenceDbId : $occurrenceDbIdsString;

        // Get list Id
        if ($isViewOnly) {
            // Experiment Manager - View
            $listId = $this->experimentProtocol->getListId($id,'TRAITS',$shouldUpdateOccurrenceProtocols,$occurrenceDbIdsString,'OCCURRENCE');
        } else {
            // Experiment Creation
            $listId = $this->experimentProtocol->getListId($id,'TRAITS',$shouldUpdateOccurrenceProtocols,$occurrenceDbIdsString,'EXPERIMENT');
        }

        // Get all trait variables tags
        $traitVariableTags = $this->experimentProtocol->getTraitVariablesTags();

        // Get all trait lists tags
        $traitProtocolTags = $this->experimentProtocol->getExperimentProtocolTags($listId, 'TRAITS');

        // get current sort
        $paramSort = '';
        if(isset($_GET['sort'])) {
            $sort = $_GET['sort'][0] != '-' ? $_GET['sort'] : substr($_GET['sort'], 1) . ':desc';
            $paramSort = 'sort=' . $sort;
        } else if (isset($_GET['dp-1-sort'])) {
            $sort = $_GET['dp-1-sort'][0] != '-' ? $_GET['dp-1-sort'] : substr($_GET['dp-1-sort'], 1) . ':desc';
            $paramSort = 'sort=' . $sort;
        }

        // Get all existing traits
        $traitList = $this->experimentProtocol->getExperimentProtocolProvider($listId, null, $paramSort);

        if ($isViewOnly || $previewOnly) {
            $display = $this->render('@app/modules/occurrence/views/view/protocol/_trait_list.php',[
                'id' => $id,
                'dataProvider' => $traitList,
            ],true, false);
        } else {
            $display = $this->render('traits/_traits',[
                'id' => $id,
                'listId' => $listId,
                'program' => $program,
                'processId' => $processId,
                'traitVariableTags' => $traitVariableTags,
                'traitProtocolTags' => $traitProtocolTags,
                'dataProvider' => $traitList,
                'shouldUpdateOccurrenceProtocols' => $shouldUpdateOccurrenceProtocols,
                'occurrenceDbIdsString' => $occurrenceDbIdsString,
                'hasMultipleSelectedOccurrences' => $hasMultipleSelectedOccurrences,
            ],true, false);
        }

        return $display;
    }


    /**
     * Render management protocol view
     * 
     * @param Integer $id experiment identifier
     * @param String $program program identifier
     * @param String $processId program identifier
     * @param String $isViewOnly flag for view-only mode
     * @param String $previewOnly flag for view-only mode
     */
    public function actionManagementProtocol($id, $program, $processId, $isViewOnly = '', $previewOnly = '', $occurrenceDbId = null, $shouldUpdateOccurrenceProtocols = '', $occurrenceDbIdsString = '', $hasMultipleSelectedOccurrences = ''){
        $targetLevelValue = '';
        //Get the configuration
        $this->layout = '@app/views/layouts/dashboard';

        $occurrenceDbIdsString = empty($occurrenceDbIdsString) ? (string)$occurrenceDbId : $occurrenceDbIdsString;

        // Get list Id
        if ($isViewOnly) {
            // Experiment Manager - View
            $listId = $this->experimentProtocol->getListId($id,'MANAGEMENT',$shouldUpdateOccurrenceProtocols,$occurrenceDbIdsString,'OCCURRENCE');
        } else {
            // Experiment Creation
            $listId = $this->experimentProtocol->getListId($id,'MANAGEMENT',$shouldUpdateOccurrenceProtocols,$occurrenceDbIdsString,'EXPERIMENT');
        }

        // Get all management variables tags
        $mgtVariableTags = $this->experimentProtocol->getManagementVariablesTags();

        // Get all management protocols tags
        $mgtProtocolTags = $this->experimentProtocol->getExperimentProtocolTags($listId,'MANAGEMENT');

        // get current sort
        $paramSort = '';
        if(isset($_GET['sort'])) {
            $sort = $_GET['sort'][0] != '-' ? $_GET['sort'] : substr($_GET['sort'], 1) . ':desc';
            $paramSort = 'sort=' . $sort;
        } else if (isset($_GET['dp-1-sort'])) {
            $sort = $_GET['dp-1-sort'][0] != '-' ? $_GET['dp-1-sort'] : substr($_GET['dp-1-sort'], 1) . ':desc';
            $paramSort = 'sort=' . $sort;
        }

        // Get all existing management
        $mgtList = $this->experimentProtocol->getExperimentProtocolProvider($listId, null, $paramSort);
        
        //check if there's a target level saved in the experiment data
        $mgtVariableObj = \Yii::$container->get('app\models\Variable')->searchAll(['abbrev' => 'equals PROTOCOL_TARGET_LEVEL']);
        $mgtVariable = $mgtVariableObj['data'][0]['variableDbId'] ?? null;

        $mgtDataParam = ['variableDbId'=>"equals $mgtVariable"];
        $experimentDataRes = ExperimentData::searchAllRecords($id,$mgtDataParam);
        if(!empty($experimentDataRes)){
            $targetLevelValue = $experimentDataRes[0]['dataValue'];
        }

        if ($isViewOnly || $previewOnly) {
            $display = $this->render('@app/modules/occurrence/views/view/protocol/_management_list.php',[
                'id' => $id,
                'dataProvider' => $mgtList,
            ],true, false);
        } else {
            $display = $this->render('_management',[
                'id' => $id,
                'listId' => $listId,
                'program' => $program,
                'processId' => $processId,
                'mgtVariableTags' => $mgtVariableTags,
                'mgtProtocolTags' => $mgtProtocolTags,
                'dataProvider' => $mgtList,
                'targetLevelValue' => $targetLevelValue,
                'shouldUpdateOccurrenceProtocols' => $shouldUpdateOccurrenceProtocols,
                'occurrenceDbIdsString' => $occurrenceDbIdsString,
                'hasMultipleSelectedOccurrences' => $hasMultipleSelectedOccurrences,
            ],true, false);
        }

        return $display;
    }

    /**
     * Render the page for management attributes/protocol
     * 
     * @param $id Integer experiment identifier
     * @param $program String program identifier
     * @param $processId String program identifier
     */
    public function actionManagement($id, $program, $processId, $occurrenceDbId = null, $shouldUpdateOccurrenceProtocols = '', $occurrenceDbIdsString = '', $hasMultipleSelectedOccurrences = ''){
        //Get the configuration
        $this->layout = '@app/views/layouts/dashboard';

        // Get list Id
        $listId = $this->experimentProtocol->getListId($id,'MANAGEMENT',$shouldUpdateOccurrenceProtocols,$occurrenceDbIdsString);

        // Get all management variables tags
        $mgtVariableTags = $this->experimentProtocol->getManagementVariablesTags();

        // Get all management protocols tags
        $mgtProtocolTags = $this->experimentProtocol->getExperimentProtocolTags($listId,'MANAGEMENT');
        
        // Get all existing management
        $mgtList = $this->experimentProtocol->getExperimentProtocolProvider($listId);

        //check if there's a target level saved in the experiment data
        $mgtVariable = $this->variable->searchAll(['abbrev' => 'equals PROTOCOL_TARGET_LEVEL'])['data'][0]['variableDbId'];
        $mgtDataParam = ['variableDbId'=>"equals $mgtVariable"];
        $experimentDataRes = $this->experimentData->searchAllRecords($id,$mgtDataParam);
        $targetLevelValue = null;
        if(!empty($experimentDataRes)){
            $targetLevelValue = $experimentDataRes[0]['dataValue'];
        }

          if(isset($_POST['subtab']) && $_POST['subtab'] == 'mgt-var'){
              return $this->renderAjax('_management_variables',[
                 'id' => $id,
                 'listId' => $listId,
                 'program' => $program,
                 'processId' => $processId,
                 'mgtVariableTags' => $mgtVariableTags,
                 'mgtProtocolTags' => $mgtProtocolTags,
                 'dataProvider' => $mgtList,
                 'targetLevelValue' => $targetLevelValue,
                 'shouldUpdateOccurrenceProtocols' => $shouldUpdateOccurrenceProtocols,
                 'occurrenceDbIdsString' => $occurrenceDbIdsString,
                 'hasMultipleSelectedOccurrences' => $hasMultipleSelectedOccurrences,
              ]);
          }else{
              return $this->renderAjax('_management_protocol',[
                  'id' => $id,
                  'listId' => $listId,
                  'program' => $program,
                  'processId' => $processId,
                  'mgtVariableTags' => $mgtVariableTags,
                  'mgtProtocolTags' => $mgtProtocolTags,
                  'dataProvider' => $mgtList,
                  'targetLevelValue' => $targetLevelValue,
                  'shouldUpdateOccurrenceProtocols' => $shouldUpdateOccurrenceProtocols,
                  'occurrenceDbIdsString' => $occurrenceDbIdsString,
                  'hasMultipleSelectedOccurrences' => $hasMultipleSelectedOccurrences,
              ]);
          }
    }

    /**
     * Remove one or more variables in the list
     */
    public function actionRemoveVariables(){
        $idList = isset($_POST['idList']) ? $_POST['idList'] : [];

        if(!empty($idList) && isset($idList)){
            $result = $this->listMember->deleteMany($idList);

            $successfulCount = 0;
            $failedCount = 0;
            $noPermissionCount = 0;

            if(empty($result['success'])){
                return json_encode(['success'=>false]);
            }

            $failedResults = $result['data'][0]['failed'];
            $successfulCount = count($result['data'][0]['successful']);
            $failedCount = 0;
            $noPermissionCount = 0;

            // Distinctly count failed results depending on "failed" message
            foreach ($failedResults as $key => $value) {
                // If irrelevant result was encountered here, ignore it
                if (str_contains($value['response'][0]['message'], 'function')) {
                    continue;
                }
                
                if (str_contains($value['response'][0]['message'], 'does not have read and write access')) {
                    $noPermissionCount += 1;
                } else {
                    $failedCount += 1;
                }
            }
        }

        return json_encode([
            'success' => true,
            'successfulCount' => $successfulCount,
            'failedCount' => $failedCount,
            'noPermissionCount' => $noPermissionCount,
        ]);
    }

    /**
     * Add selected variables to platform.list_member table
     * 
     * @return $response string toast message
     */
    public function actionAddVariables(){
        $response = 'Failed to add variables.';
        $protocolType = '';

        if(isset($_POST)){
            extract($_POST);
            if(isset($_POST['protocolType'])){
                $protocolType = $_POST['protocolType'];
            }
            $response = $this->experimentProtocol->addVariables($listId, $id, $type, $operation,$protocolType, $experimentId);
        }

        return json_encode($response);
    }

    /**
     * Preview variables when saving new list
     */
    public function actionRetrieveVariablesPreview(){
        $id = isset($_POST['id']) ? $_POST['id'] : [];
        $idList = isset($_POST['idList']) ? $_POST['idList'] : [];


        // there are variables selected
        $data = $this->experimentProtocol->getExperimentProtocolProvider($id, $idList);
        
        $htmlData = $this->renderPartial('traits/_trait_list_preview', ['dataProvider'=>$data['dataProvider']]);

        return json_encode([
            'htmlData' => $htmlData,
            'count' => $data['count']
        ]);
    }

    /**
     * Preview management attributes/variables when saving new list
     */
    public function actionRetrieveMgtVariablesPreview(){
        $id = isset($_POST['id']) ? $_POST['id'] : [];
        $idList = isset($_POST['idList']) ? $_POST['idList'] : [];
        
        // Get list Id
        $listId = $this->experimentProtocol->getListId($id,'MANAGEMENT');

        // there are variables selected
        $data = $this->experimentProtocol->getExperimentProtocolProvider($listId, $idList);
        
        $htmlData = $this->renderPartial('_management_list_preview', ['dataProvider'=>$data]);
       
        return json_encode([
            'htmlData' => $htmlData,
            'count' => count($data->allModels)
        ]);
    }

    /**
     * Preview variables to be added from list
     */
    public function actionAddVarList(){
        $listId = isset($_POST['listId']) ? $_POST['listId'] : null;
         // there are variables selected
         $data = $this->experimentProtocol->getExperimentProtocolProvider($listId);

        $htmlData = $this->renderPartial('traits/_trait_list_preview', ['dataProvider' => $data]);

        return json_encode([
            'htmlData' => $htmlData
        ]);
    }

     /**
     * Preview Management variables to be added from list
     */
    public function actionAddMgtVarList(){
        $listId = isset($_POST['listId']) ? $_POST['listId'] : null;
         // there are variables selected
        if(!empty($listId)){
            $data = $this->experimentProtocol->getExperimentProtocolProvider($listId);

            $htmlData = $this->renderPartial('_management_list_preview', ['dataProvider' => $data]);
            
            return json_encode([
                'htmlData' => $htmlData
            ]);
        }else{
            return json_encode(true);
        }
    }

    /**
     *  Save remarks in platform.list_member table
    */
    public function actionSaveRemarks(){

        if(isset($_POST['listMemberId'])){
            $listMemberId = $_POST['listMemberId'];
            $remarks = $_POST['remarks'];

            $this->listMember->updateOne($listMemberId, [
                'remarks' => $remarks
            ]);
        }
    }

    /**
     *  Bulk save remarks in platform.list_member table
    */
    public function actionSaveBulkRemarks(){
        $response = "<i class='material-icons orange-text left'>warning</i> Post information not found.";
        if(isset($_POST)){
            extract($_POST);
 
            $result = $this->experimentProtocol->saveBulkRemarks($listId, $listMemberId, $remarks, $operation);

            if(empty($result['success'])){
                return json_encode([
                    'success'=> false,
                ]);
            }

            $response = "<i class='material-icons green-text left'>done</i> Successfully saved.";
        }

        return json_encode([
            'success'=> true,
            'message'=> $response
        ]);

        return json_encode($response);
    }

    /**
     * Render plot types
     * 
     * @param Integer $id experiment identifier
     * @param String $program program identifier
     */
    public function actionLoadPlotTypeFields($id, $program){
        // This flag determines whether to retrieve Occurrence-level data or Experiment-level data
        $isViewOnly = isset($_POST['isViewOnly']) ? $_POST['isViewOnly'] : '';
        $previewOnly = isset($_POST['previewOnly']) ? $_POST['previewOnly'] : '';

        if(isset($_POST) && !empty($_POST['selected'])){
            $data = $this->protocolModel->getPlotType($id, $program, Yii::$app->request->post(), $isViewOnly, $previewOnly);
            return $this->renderAjax('_plot_types', $data);
        }else{
            return '';
        }
    }

    /**
     * Compute plot field values
     * 
     * @param Integer $id experiment identifier
     */
    public function actionComputeFormula($id){
        
        $computedValue = 0;
        if(isset($_POST) && !empty($_POST)){
            $computedValue = $this->protocolModel->computePlotFieldValue(Yii::$app->request->post());
        }
        
        return json_encode($computedValue);
    }

    /**
     * Render plot variable values
     * 
     * @param Integer $id experiment identifier
     */
    public function actionLoadVariableValue($id){
        $result = [];
        // This flag determines whether to retrieve Occurrence-level data or Experiment-level data
        $isViewOnly = isset($_POST['isViewOnly'])? $_POST['isViewOnly'] : '';
        $shouldUpdateOccurrenceProtocols = isset($_POST['shouldUpdateOccurrenceProtocols'])? $_POST['shouldUpdateOccurrenceProtocols'] : '';
        
        if(isset($_POST['defaultVariables']) && !empty($_POST['defaultVariables'])){
            $defaultVariables = json_decode($_POST['defaultVariables'], true);
            $previewOnly = isset($_POST['previewOnly']) ? $_POST['previewOnly'] : '';

            $result = $this->protocolModel->getPlotVariableValue($id, $defaultVariables, $isViewOnly, $previewOnly, $shouldUpdateOccurrenceProtocols);
        }
        
        return json_encode($result);
    }
    /**
     * Render protocol tab notification preview
     */
    public function actionValidateProtocols($id, $program, $processId){

        $flag = false;
        $htmlData = null;
        if(isset($_POST['protocolChildren']) && !empty($_POST['protocolChildren'])){
            $protocolChildren = json_decode($_POST['protocolChildren'], true);
            $data = $this->protocolModel->validateProtocol($id, $program, $processId, $protocolChildren);
            if(sizeof($data['records']) > 0){
                $flag = true;
                $htmlData = $this->renderPartial('notif_preview',['listOfNoRecordVal'=>$data['records']]);
            }
        }
        
        return json_encode([
            'htmlData' => $htmlData,
            'flag' => $flag
        ]);
    }

    /**
     * Render planting protocol filter tags
     * 
     * @param Integer $id experiment identifier
     */
    public function actionUpdateFilterTags($id){
        
        $tags = [];
        if(isset($_POST)){
            $tags = $this->protocolModel->getPlotFieldFilterTag($id, Yii::$app->request->post());
        }

        return json_encode($tags);
    }

    /**
     * Finds the Experiment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param Integer $id
     * @return Experiment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = $this->experiment->getExperiment($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
        

    // /**
    //  * TODO: To be refactored
    //  * Render process path view
    //  * 
    //  * @param $id integer experiment identifier
    //  * @param $program integer program identifier
    //  * @param $processId integer processId identifier
    //  */
    // public function actionProcessPathProtocols($id, $program, $processId){

    // //Get the configuration
    //     $this->layout = '@app/views/layouts/dashboard';
            
    //     $processPathTags = $this->protocolModel->getProcessPathTags($program,$id,$processId);
    //     $processPathCount = count($processPathTags);
    //     $saveDisabled = ($processPathCount == 1) ? true : false;
            
    //     if($saveDisabled && isset($processPathTags)){
    //         $this->protocolModel->saveProcessPath($id, key($processPathTags));
    //     }
                    
    //     $processPathDataId = $this->protocolModel->getProcessPathData($id);
                    
    //     //set default value
    //     $defaultValue = !empty($processPathDataId['value'])? $processPathDataId['value'] : '';
                        
    //     $display = $this->render('_process_path',[
    //         'id'=>$id,
    //         'program'=>$program,
    //         'processId'=>$processId,
    //         'processPathTags'=>$processPathTags,
    //         'defaultValue'=>$defaultValue,
    //         'saveDisabled'=> $saveDisabled,
    //     ],true, false);
                        
    //     return $display;
    // }
                        
    // /**
    //  * TODO: To be refactored
    //  * Save process path value
    //  * 
    //  */
    // public function actionSaveProcessPath(){
                            
    //     $id = isset($_POST['id']) ? intval($_POST['id']) : [];
    //     $processPathId = isset($_POST['processPathId']) ? intval($_POST['processPathId']) : [];
    //     $message = $this->protocolModel->saveProcessPath($id, $processPathId);
                            
    //     return json_encode($message);
    // }

    // /**
    //  * TODO: To be refactored
    //  * Reorder variables in the list
    //  */
    // public function actionReorderVariables(){
    //     $id = isset($_POST['id']) ? $_POST['id'] : '';
    //     $idList = isset($_POST['idList']) ? $_POST['idList'] : [];

    //     $idList = implode(',', $idList);

    //     $this->protocolModel->reorderVariables($id,$idList);

    //     return json_encode(['success'=>true]);
    // }


    /**
     * Save as new list of type variable
     */
    public function actionSaveVariableList(){
        $id = isset($_POST['id']) ? $_POST['id'] : [];
        $idList = isset($_POST['idList']) ? $_POST['idList'] : [];
        $protocolType = isset($_POST['protocolType']) ? strtoupper($_POST['protocolType']) : '';

        $attributes = [
            'abbrev' => isset($_POST['abbrev']) ? $_POST['abbrev'] : '',
            'name' => isset($_POST['name']) ? $_POST['name'] : '',
            'displayName' => isset($_POST['displayName']) ? $_POST['displayName'] : '',
            'description' => isset($_POST['description']) ? $_POST['description'] : '',
            'remarks' => isset($_POST['remarks']) ? $_POST['remarks'] : ''
        ];

        $data = $this->protocolModel->saveVariableList($id, $idList, $attributes,$protocolType);

        return json_encode(['msg'=>$data]);
    }
                                                                            
    // /**
    //  * TODO: Unused function - remove when refactoring is done
    //  * Contruct the tags of the other select2 
    //  * Get the relationship of Crop establishment, planting type and plot type
    //  */
    // public function actionGetTags2($id){
    //     $tags = [];
    //     if(isset($_POST)){
            
    //         $variable = strtoupper($_POST['varAbbrev']);
    //         $value = $_POST['selectedVal'];
    //         $establishment = strtoupper($_POST['establishment']);
    //         $activities = '';

    //         $experimentModel = $this->experiment->find()->where(['id'=>$id,'is_void'=>false])->one();
    //         $crop = '';
    //         $activCheck = $variable;
            
    //         if($variable == 'ESTABLISHMENT'){
    //             $activCheck = 'PLANTING_TYPE';
    //             $activities = 'planting_type_abbrevs';
    //         }else if($variable == 'PLANTING_TYPE' && !empty($value)){
    //             $activCheck = 'PLOT_TYPE';
    //             $activities = 'plot_type_abbrevs';
            
    //             //Get the establishment abbrev
    //             if(!empty($establishment)){
    //                 $scaleValueMod = ScaleValue::find()->where(['abbrev'=>$establishment, 'is_void'=>false])->one();
    //                 $establishmentAbbrev = $scaleValueMod->abbrev;
    //             }else if(empty($establishment)){
    //                 $establishmentAbbrev = $value;
    //             }
    //         }elseif($variable == 'PLANTING_TYPE' && empty($value)){
    //             $activCheck = 'PLOT_TYPE';
    //             $activities = 'plot_type_abbrevs';
    //         }else if($variable == 'PLOT_TYPE'){
    //             $activCheck = $variable;
    //             $activities = 'plot_type_abbrevs';
    //         }
                
    //         if(!empty($experimentModel->crop_id)){
    //             $cropModel = Crop::find()->where(['id'=>$experimentModel->crop_id, 'is_void'=>false])->one();
    //             $crop = $cropModel->abbrev;
    //             $configAbbrev = '';
                
    //             if(strtoupper($crop) == 'RICE' && ($variable == 'PLANTING_TYPE' && !empty($value))){
    //                 $activityAbbrev = $establishmentAbbrev.'_'.'PLOT_TYPE';
                    
    //             }else if($variable == 'PLANTING_TYPE' && !empty($value)){ //For Wheat and Maize
                    
    //                 $scaleValueMod = ScaleValue::find()->where(['abbrev'=>strtoupper($value), 'is_void'=>false])->one();
    //                 $activityAbbrev = $crop.'_'.strtoupper($scaleValueMod->value).'_'.'PLOT_TYPE';
    //             }else{
                    
    //                 $activityAbbrev = $crop.'_'.$activCheck;
    //             }      
    //             $requiredConfig = Config::find()->where(['abbrev'=>$activityAbbrev, 'is_void'=>false])->one();
                
    //             $requiredFields = $requiredConfig['config_value'];
                
    //             $activities_config = $requiredFields['Values'][0][$activities];
                
                
    //             foreach($activities_config as $val){
    //                 $scaleValueMod = ScaleValue::find()->where(['abbrev'=>$val, 'is_void'=>false])->one();
    //                 $tags[$val] = $scaleValueMod->value;
    //             }
    //         }
    //     }
        
    //     return json_encode($tags);
    // }

    // /**
    //  * TODO: Unused function - remove when refactoring is done
    //  * Contruct the tags of the other select2 
    //  * Get the relationship of Crop establishment, planting type and plot type
    //  */
    // public function actionGetTags($id){
        
    //     $tags = [];
    //     if(isset($_POST)){
            
    //         $variable = strtoupper($_POST['varAbbrev']);
    //         $establishment = strtoupper($_POST['establishment']);
    //         $plantingType = strtoupper($_POST['plantingType']);
    //         $plotType = strtoupper($_POST['plotType']);
            
    //         $activities = '';
    //         $experimentModel = $this->experiment->find()->where(['id'=>$id,'is_void'=>false])->one();
    //         $varModel = Variable::find()->where(['abbrev'=>$variable, 'is_void'=>false])->one();
            
    //         if($variable == 'PLOT_TYPE'){
    //             //check establishment 1st 
    //             if(!empty($establishment) && empty($plantingType)){
    //                 $activityAbbrev = $establishment.'_'.$variable;
    //             }elseif(empty($establishment) && !empty($plantingType)){
    //                 $plantingType = ($plantingType != 'PLANTING_TYPE_FLAT') ? 'PLANTING_TYPE_FLAT' : $plantingType;
    //                 $activityAbbrev = $plantingType.'_'.$variable;
    //             }elseif(empty($establishment) && empty($plantingType)){
    //                 $cropModel = Crop::find()->where(['id'=>$experimentModel->crop_id, 'is_void'=>false])->one();
    //                 $crop = $cropModel->abbrev;
    //                 $activityAbbrev = $crop.'_'.$variable;
    //             }
            
    //             $activities = 'plot_type_abbrevs';
                
    //             $requiredConfig = Config::find()->where(['abbrev'=>$activityAbbrev, 'is_void'=>false])->one();
    //             $requiredFields = $requiredConfig['config_value'];
    //             $activities_config = $requiredFields['Values'][0][$activities];
                                
    //             if(!empty($activities_config) && isset($activities_config)){
    //                 foreach($activities_config as $val){
                    
    //                     $scaleValueMod = ScaleValue::find()->where(['abbrev'=>$val, 'is_void'=>false])->one();
    //                     $tags[$val] = $scaleValueMod->value;
    //                 }
    //             }
    //         }else{
    //             //Check if the variable has a scale value
    //             if(!empty($varModel->scale_id)){
    //                 $scaleValues = $this->protocolModel->getVariableScaleValueById($varModel->scale_id);
    //                 $tags = $scaleValues;
    //             }
    //         }
    //     }
        
    //     return json_encode($tags);
    // }

    // /**
    //  * TODO: Unused function - remove when refactoring is done
    //  * This checks if the protocol required values are saved in the database
    //  */
    // public function actionCheckProtocol($id){
        
    //     $noRecordFlag = false;
        
    //     if(isset($_POST)){
    //         $type = $_POST['type'];  //planting, traits, or process path protocol
    //         $activity = $_POST['activity'];

    //         if($type == 'planting'){
            
    //             $config = Config::find()->where(['abbrev'=>$activity, 'is_void'=>false])->one();
    //             $value = $config['config_value']['Values'];
            
    //             foreach($config['config_value']['Values'] as $valItem){
                   
    //                 $itemAbbrev = isset($valItem['variable_abbrev']) ? $valItem['variable_abbrev'] : '';
                   
    //                 //Check first the plot type
    //                 if(!empty($itemAbbrev) && strpos($itemAbbrev,'PLOT_TYPE') !== false){
                        
    //                     //Check it in the Experiment data and get the saved value
                        
    //                     $varModel = Variable::find()->where(['abbrev'=>$itemAbbrev,'is_void'=>false])->one();
    //                     $exptDataModel = ExperimentData::find()->where(['experiment_id'=>$id,'variable_id'=>$varModel->id,'is_void'=>false])->one();
                        
    //                     if(isset($exptDataModel['value']) && !empty($exptDataModel['value'])){
                            
    //                         $scaleValueMod = ScaleValue::find()->where(['value'=>$exptDataModel['value'],'is_void'=>false])->one();
    //                         $plotTypeConfig =  Config::find()->where(['abbrev'=>$scaleValueMod->abbrev, 'is_void'=>false])->one();
                            
    //                         foreach($plotTypeConfig['config_value']['Values'] as $reqValItem){
                                            
    //                             if(isset($reqValItem['required']) && strtolower($reqValItem['required']) == 'required'){
                                                         
    //                                 //Check experiment data for saved values
    //                                 $varModel = Variable::find()->where(['abbrev'=>$reqValItem['variable_abbrev'],'is_void'=>false])->one();
    //                                 $exptDataModel = ExperimentData::find()->where(['experiment_id'=>$id,'variable_id'=>$varModel->id,'is_void'=>false])->one();
                                    
    //                                 if(isset($exptDataModel['value']) && !empty($exptDataModel['value'])){
    //                                     //This is ok 
    //                                 }else{
    //                                     //No record found in the experiment data table
    //                                     //check if field label has value, if none then get the label in the variable table
    //                                    $noRecordFlag = true;
    //                                 }
    //                             }
    //                         }
    //                     }else{
    //                         $noRecordFlag = true;
    //                     }
    //                 }
    //             }
    //         }
    //         return json_encode($noRecordFlag);
    //     }
    // }


    /**
     * Save Management Target Level -- Occurrence or Location
     * 
     * @param $id Integer Experiment ID
     */
    public function actionSaveMgtTargetLevel($id){
        if(isset($_POST)){
            extract($_POST);

            //Save the updates
            $this->protocolModel->saveMgtTargetLevel($id, $targetLevel);
        }
        
        return json_encode(true);
    }

}

