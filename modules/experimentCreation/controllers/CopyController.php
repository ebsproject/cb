<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use app\models\Config;
use app\models\BackgroundJob;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\Occurrence;
use app\models\User;
use app\models\Variable;


use app\modules\experimentCreation\models\FormModel;
use app\modules\experimentCreation\models\CopyExperimentModel;
use app\modules\experimentCreation\models\ExperimentModel;

use yii\web\Controller;
// use app\components\B4RController;

use Yii;

class CopyController extends Controller
{
    protected $configModel;
    protected $copyExperiment;
    protected $experiment;
    protected $experimentData;
    protected $experimentModel;
    protected $formModel;
    protected $mainVariable;
    protected $occurrence;
    protected $user;

    public function __construct($id, $module,
        Config $configModel,
        CopyExperimentModel $copyExperiment,
        ExperimentModel $experimentModel,
        ExperimentData $experimentData,
        Experiment $experiment,
        FormModel $formModel,
        Variable $mainVariable,
        Occurrence $occurrence,
        User $user,
        $config = []
        )
    {
        $this->configModel = $configModel;
        $this->copyExperiment = $copyExperiment;
        $this->experiment = $experiment;
        $this->formModel = $formModel;
        $this->experimentData = $experimentData;
        $this->experimentModel = $experimentModel;
        $this->mainVariable = $mainVariable;
        $this->occurrence = $occurrence;
        $this->user = $user;
        parent::__construct($id,$module,$config);
    }

    /**
     * Get preview of copy experiment
     * 
     * @param integer $id record id of the experiment
     * @param string $program name of the current program
     */
    public function actionPreviewExperiment($id, $program){
        $copyInformation = [];
        if(isset($_POST['copyInformation']) && $_POST['copyInformation'] != null){
            $copyInformation = $_POST['copyInformation'];
        } else {
            Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-error"></i> The session has expired. Kindly try again');
            $this->redirect(['create/index?program='.$program.'&id='.$id]);
        }
        
        $userId = $this->user->getUserId();
        Yii::$app->session['copyInformation-'.$id.'-'.$userId] = $copyInformation;

        $model = $this->experiment->getExperiment($id);

        if(Yii::$app->access->renderAccess("EXPERIMENT_CREATION_COPY","EXPERIMENT_CREATION", $model['creatorDbId'], usage:'url')){
        
            $this->redirect(['copy/copy-experiment?program='.$program.'&id='.$id]);
        }
    }

    /**
     * Show preview of copy experiment
     * 
     * @param integer $id record id of the experiment
     * @param string $program name of the current program
     * @param array $copyInformation list of selected experiment steps
     */
    public function actionCopyExperiment($id, $program){

        $userId = $this->user->getUserId();
        $sessionName = 'copyInformation-'.$id.'-'.$userId;
        $copyInformation = isset(Yii::$app->session[$sessionName]) ? Yii::$app->session[$sessionName] : [];

        if(empty($copyInformation)){
            Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-error"></i> The session has expired. Kindly try it again.');
            $this->redirect(['create/index?program='.$program.'&id='.$id]);
        } else {
        
            $model = $this->experiment->getExperiment($id);
              
            $data = $this->copyExperiment->getPreviewInformation($id, $program,$copyInformation);

            return $this->render('index.php',[
                'model' => $model,
                'copyInformation' => $copyInformation,
                'program' => $program,
                'id' => $id,
                'activities' => $data['activities'],
                'dataProcessDbId' => isset($model['dataProcessDbId']) ? $model['dataProcessDbId'] :"",
                'experimentDesignType' => isset($model['experimentDesignType']) ? $model['experimentDesignType']:"",
                'experimentType' => isset($model['experimentType']) ? $model['experimentType']:"",
                'basicInformation' => isset($data['basicInformation']) ? $data['basicInformation'] : [],
                'copyDesign' => $data['copyDesign']
            ]);
        }
    }

    /**
     * Start copying of experiment
     *
     * @param $id integer record id of the experiment
     * @param $program integer program where the experiment belongs
     **/
    public function actionStartCopyExperiment($id, $program){
        if(isset($_POST['copyInformation'])){
            $copyInformation = json_decode($_POST['copyInformation'], false);
        }
        if(isset($_POST['copyExperimentDbId'])){
            $copyExperimentDbId = $_POST['copyExperimentDbId'];
        }
        if(isset($_POST['copyDesign'])){
            $copyDesign = $_POST['copyDesign'];
        }
        $model = $this->experiment->getExperiment($id);
        $oldModel = $this->experiment->getExperiment($copyExperimentDbId);
        //create experiment record
        $experimentDbId = $this->copyExperiment->copyNewExperiment($id, $model);

        if(!$copyDesign){
            $idx = array_search('specify-design', $copyInformation);
            if($idx !== false){
                $copyInformation = array_splice($copyInformation, 0, $idx);
            }
        }

        if($experimentDbId != null){
            //send request
            $addtlParams = ["description"=>"Copying of experiment","entity"=>"EXPERIMENT","entityDbId"=>$experimentDbId,"endpointEntity"=>"EXPERIMENT", "application"=>"EXPERIMENT_CREATION"];
            $dataArray = [
                'records'=>$model,
                'copyExperimentDbId' => $copyExperimentDbId,
                'copyInformation' => $copyInformation,
                'experimentDbId' => $experimentDbId.""
            ];
            $this->experiment->create($dataArray, true, $addtlParams);

            //update experiment
            if($oldModel['experimentDesignType'] == 'Entry Order'){
                $this->experiment->updateOne($experimentDbId, ["experimentDesignType"=>"Systematic Arrangement"]);
            }
            $this->experiment->updateOne($experimentDbId, ["experimentStatus"=>"draft;copying of experiment in progress"]);
            Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The copying of records for <strong>'.$oldModel['experimentName'].'</strong> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.');
            $this->redirect(['create/index?program='.$program.'&id='.$experimentDbId]);

        } else {
            Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-error"></i> The new experiment draft is not created. Please try again');
            $this->redirect(['create/index?program='.$program.'&id='.$id]);
        }
        
    }

    /**
     * Check if the plots are already generated
     *
     * @param $id integer record id of the experiment
     * @param $program integer program where the experiment belongs
     * @param $label text value to be shown in the viewer
     * @param $flipped boolean flag for changing the origin of the layout
     * @param $occurrenceDbId integer value of the current occurrence being shown in the layout
     *
     */
    public function actionGetLayout($id, $program, $label='plotno', $occurrenceDbId = NULL){

        $panelWidth = $_POST['panelWidth'];

        $occurrences = $this->occurrence->searchAll([
            'fields'=>'occurrence.experiment_id as experimentDbId|occurrence.occurrence_name as occurrenceName|occurrence.id AS occurrenceDbId',
            'experimentDbId' => "$id"
        ],'sort=occurrenceName:ASC');

        

        $occurrencesList = array();
        if($occurrences['totalCount'] > 0){

            $occurrencesList = $occurrences['data'];
            if($occurrenceDbId == NULL){
                $occurrenceDbId = $occurrences['data'][0]['occurrenceDbId'];
                $occurrenceName = $occurrences['data'][0]['occurrenceName'];
            } else {
                $occurrenceIdx = array_search($occurrenceDbId, array_column($occurrencesList, 'occurrenceDbId'));
                $occurrenceName = $occurrencesList[$occurrenceIdx]['occurrenceName'];
            }

            //checked plot records and view layout
            $plotRecords = $this->occurrence->searchAllPlots($occurrenceDbId, null,'sort=plotNumber:ASC');

            if($plotRecords['totalCount'] > 0){

                //Save FIRST_PLOT_POSITION_VIEW
                $varAbbrev = 'FIRST_PLOT_POSITION_VIEW';
                $this->mainVariable->searchAll(["abbrev"=>"$varAbbrev"]);
                
                $expData = $this->experimentData->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);
                $flipped = false;
                if(isset($expData) && !empty($expData)){
                    if($expData[0]['dataValue'] == "Top Left"){
                        $flipped = true;
                    } else {
                        $flipped = false;
                    }   
                } 
                $layoutArray = $this->experimentModel->generateLayout($occurrenceDbId, $id);
                
                $htmlData = $this->renderPartial('/copy/layout_view.php',[
                    'experimentId' => $id,
                    'data' => $layoutArray['dataArrays'],
                    'label'=> $label,
                    'startingPlotNo' => 1,
                    'panelWidth' => $panelWidth,
                    'noOfRows' => $layoutArray['maxRows'] == NULL ? 0:$layoutArray['maxRows'],
                    'noOfCols' => $layoutArray['maxCols'] == NULL ? 0:$layoutArray['maxCols'],
                    'statusDesign' => 'completed',
                    'deletePlots' => 'true',
                    'planStatus' => 'randomized',
                    'flipped' => $flipped,
                    'occurrencesList' => $occurrencesList,
                    'occurrenceName' => $occurrenceName,
                    'occurrenceDbId' => $occurrenceDbId,
                    'xAxis' => $layoutArray['xAxis'],
                    'yAxis' => $layoutArray['yAxis'],
                    'maxBlk' => $layoutArray['maxBlk']
                ],true,false);
    
                return json_encode([
                    'success'=> 'success',
                    'htmlData' => $htmlData,
                    'deletePlots' => 'true',
                    'planStatus' => 'randomized',
                    'message' => 'Successfully randomized.'
                ]);
                
            }
        }
    }
}
