<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Entry;
use app\models\EntrySearch;
use app\models\EntryList;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\ExperimentBlock;
use app\models\Item;
use app\models\User;
use app\models\UserDashboardConfig;

use Yii;
use yii\helpers\ArrayHelper;
use app\dataproviders\ArrayDataProvider;
use yii\web\Controller;

use app\components\B4RController;
use app\modules\experimentCreation\models\EntryOrderModel;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\FemaleListSearch;
use app\modules\experimentCreation\models\MaleListSearch;
use app\modules\experimentCreation\models\PlantingArrangement;

/**
 * Controller for entry order arrangement
 */
class CrossPatternController extends B4RController
{

    public function __construct($id, $module,
        public Entry $entry, 
        public EntrySearch $entrySearch, 
        public EntryList $entryList, 
        public Experiment $experiment,
        public ExperimentData $experimentData,
        public ExperimentBlock $experimentBlock,
        public ExperimentModel $experimentModel,
        public FemaleListSearch $femaleSearchModel,
        public Item $item, 
        public MaleListSearch $maleSearchModel,
        public PlantingArrangement $plantingArrangement, 
        public EntryOrderModel $entryOrderModel,
        public User $user,
        public UserDashboardConfig $userDashboardConfig,
        $config = [])
    {
        
        parent::__construct($id, $module, $config);
    }

        /**
    * Manage crosses for the nurseries
    *
    * @param String $program name of the current program
    * @param Integer $id record id of the experiment
    * @param Integer $processId process identifier
    * @param String $type input type for pairing
    *
    */
    public function actionManageCrossPattern($program, $id, $processId, $type = 'designation') {
        $userModel = new User();
        $userId = $userModel->getUserId();
        $browserIdFemale = 'dynagrid-ec-cross-pattern-female';
        $browserIdMale = 'dynagrid-ec-cross-pattern-male';

        // $model = $this->findModel($id);
        $model = $this->experiment->getOne($id)['data'];
        $maxCrossCount = 1500;

        $femaleSearchModel = $this->femaleSearchModel;
        $maleSearchModel = $this->maleSearchModel;
        $configData = $this->experimentModel->retrieveConfiguration('specify-entry-list', $program, $id, $processId);

        $params = [];
        $fields = 'entry.id AS entryDbId|experiment.id as experimentDbId|entry.entry_role AS entryRole|entry.entry_number as entryNumber|entry.entry_code as entryCode| germplasm.designation|femaleCountField|maleCountField|germplasm.parentage|germplasm.id as germplasmDbId';
        $cookies = Yii::$app->request->cookies;
        $paramPage = '';
        $paramSort = '';
        //set female browser parameters and filters
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();

        if (isset($_GET['female-grid-page'])){
            $paramPage = '&page=' . $_GET['female-grid-page'];
        }else if (isset($_GET[$browserIdFemale.'-page'])){
            $paramPage = '&page=' . $_GET[$browserIdFemale.'-page'];
        }

        //sorting
        if(isset($_GET['sort'], $_GET['_pjax']) && $_GET['_pjax'] == '#'.$browserIdFemale.'-pjax'){
            if(strpos($_GET['sort'],'-') !== false){
                $sort = str_replace('-', '', $_GET['sort']);
                $paramSort = "&sort=$sort:DESC";
            }
            else{
                $paramSort = '&sort='.$_GET['sort'];
            }
        }
        $pageParams = $paramLimit.$paramPage.$paramSort;

        if(isset($_GET['FemaleListSearch'])){
            $params = array_filter($_GET['FemaleListSearch'],'strlen');
        }
        $currentPage = isset($_GET[$browserIdFemale.'-page']) ? $_GET[$browserIdFemale.'-page'] : null;
        $currentPage = (is_null($currentPage) && isset($_GET['female-grid-page'])) ? $_GET['female-grid-page'] : null;

        if(!isset($params['entryRole']) || $params['entryRole'] == null){
            $params['entryRole'] = 'female|equals female-and-male';
        }
        
        $femaleOrigParams = Yii::$app->session->get('female_entry_ids_params');
        if(isset($femaleOrigParams['includeParentCount'])) unset($femaleOrigParams['includeParentCount']);  // exclude this property because it is added in the model
        $params['entryRole'] = 'equals '.$params['entryRole'];
        $params['experimentDbId'] = "equals $id";
        $params['fields'] = $fields;
        $femaleDataProvider = $femaleSearchModel->search(['FemaleListSearch' => $params,'userId'=>$userId, 'id'=>$id], $pageParams, $currentPage, $type);

        $femaleReset = false;
        if(($femaleOrigParams != $params)) {
            $femaleReset = true;
        }
        Yii::$app->session->set('resetFemaleSelection', $femaleReset);

        $params = [];
        $pageParams = $paramPage = $paramSort = null;
        //set male browser parameters and filters
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();

        if (isset($_GET['male-grid-page'])){
            $paramPage = '&page=' . $_GET['male-grid-page'];
        }else if (isset($_GET[$browserIdMale.'-page'])){
            $paramPage = '&page=' . $_GET[$browserIdMale.'-page'];
        }

        //sorting
        if(isset($_GET['sort'], $_GET['_pjax']) && $_GET['_pjax'] == '#'.$browserIdMale.'-pjax'){
            if(strpos($_GET['sort'],'-') !== false){
                $sort = str_replace('-', '', $_GET['sort']);
                $paramSort = "&sort=$sort:DESC";
            }
            else{
                $paramSort = '&sort='.$_GET['sort'];
            }
        }
        $pageParams = $paramLimit.$paramPage.$paramSort;

        if(isset($_GET['MaleListSearch'])){
            $params = array_filter($_GET['MaleListSearch'], 'strlen');
        }
        $currentPage = isset($_GET[$browserIdMale.'-page']) ? $_GET[$browserIdMale.'-page'] : null;
        $currentPage = (is_null($currentPage) && isset($_GET['male-grid-page'])) ? $_GET['male-grid-page'] : null;

        if(!isset($params['entryRole']) || $params['entryRole'] == null){
            $params['entryRole'] = 'male|equals female-and-male';
        }

        $maleOrigParams = Yii::$app->session->get('male_entry_ids_params');
        if(isset($maleOrigParams['includeParentCount'])) unset($maleOrigParams['includeParentCount']);  // exclude this property because it is added in the model
        $params['entryRole'] = 'equals '.$params['entryRole'];
        $params['experimentDbId'] = "equals $id";
        $params['fields'] = $fields;
        $maleDataProvider = $maleSearchModel->search(['MaleListSearch' => $params,'userId'=>$userId, 'id'=>$id], $pageParams, $currentPage,$type);

        $maleReset = false;
        if(($maleOrigParams != $params)) {
            $maleReset = true;
        }
        Yii::$app->session->set('resetMaleSelection', $maleReset);

        if($processId == 'undefined'){
            $processId = $_GET['processId'];
        }

        //Temporary config value for crosses since no config is set yet
        $varConfigValues[] = [
            'disabled' => false,
            'is_shown' => true,
            'required' => "required",
            'variable_type' => 'identification',
            'variable_abbrev'=> "CROSS_METHOD",
            'target_value' => 'value',
            'allowed_values' => [
                "SINGLE CROSS",
                "THREE-WAY CROSS",
                "DOUBLE CROSS",
                "FEMALE COMPLEX TOP CROSS",
                "BACKCROSS",
                "COMPLEX CROSS",
                "INDUCED MUTATION POPULATION",
                "HYBRID FORMATION"
            ]
        ];

        return $this->render('/create/crosses/_cross_pattern',[
            'id' => $id,
            'model' => $model,
            'femaleSearchModel' => $femaleSearchModel,
            'maleSearchModel' => $maleSearchModel,
            'program' => $program,
            'processId'=> $processId,
            'femaleDataProvider' => $femaleDataProvider,
            'maleDataProvider' => $maleDataProvider,
            'maxCrossCount' => $maxCrossCount,
            'configValues' => $varConfigValues,
            'type' => $type
        ]);

    }
   
}