<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

// NOTE: add classes here in alphabetical order
use app\models\EntrySearch;
use app\models\Experiment;
use app\models\Occurrence;
use app\models\PlantingInstruction;
use app\models\Plot;
use app\models\User;
use app\models\UserDashboardConfig;

use app\modules\experimentCreation\models\ExperimentModel;

use app\components\B4RController;

use yii\helpers\ArrayHelper;

use Yii;

/**
 * Remove/delete functions of the experiment creation
 */
class RemoveController extends B4RController
{

    public function __construct( $id,
        $module,
        protected Experiment $mainExperiment, 
        protected ExperimentModel $mainExperimentModel, 
        protected EntrySearch $entrySearch,
        protected Occurrence $occurrence,
        protected PlantingInstruction $plantingInstruction,
        protected Plot $plot,
        protected User $userModel,
        protected UserDashboardConfig $userDashboardConfig,
        $config = [],
    )
    {
        parent::__construct($id, $module, $config);
    }

    
    /**
     * Nullifies the previously generated occurrence records of an experiment
     * @param integer $id record id of the experiment
     * 
     */
    public function removeOccurrenceRecords($id, $program="", $processId = ""){

        //Check if the experiment notes has a value isPlantingArrangementChange
        $experiment = $this->mainExperiment->searchAll(["fields"=>"experiment.id as experimentDbId|experiment.notes as notes|experiment.experiment_status AS experimentStatus|experiment.experiment_type AS experimentType|experiment.experiment_name as experimentName","experimentDbId"=>"equals ".$id,"notes"=> "plantingArrangementChanged"]);
        $experimentNotes = isset($experiment['data'][0]['notes']) ? $experiment['data'][0]['notes'] : null;
        $experimentType = isset($experiment['data'][0]['experimentType']) ? $experiment['data'][0]['experimentType'] : null;
        $experiment = isset($experiment['data'][0]) ? $experiment['data'][0] : [];
        $bgJobId = null;
        $deleteFlag = false;

        $occurrenceRecord = $this->occurrence->searchAll(['experimentDbId' => "equals ".$id]);
        $occurrenceIds = [];
        if($occurrenceRecord['totalCount'] > 0){
            $occurrenceIds = array_column($occurrenceRecord['data'], 'occurrenceDbId');
        }
        //Not empty, which means to execute deletion of previously generated plot and planting instruction records
        if(!empty($experimentNotes) && !empty($occurrenceIds)){
    
            $limit = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'deleteOccurrences')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'deleteOccurrences') : 250;
  
            //Retrieve all plot records of the occurrence
            $plotRecord = $this->plot->searchAll(["fields"=>"plot.occurrence_id AS occurrenceDbId|plot.id AS plotDbId", "occurrenceDbId"=>"equals ".implode('|equals ', $occurrenceIds)], 'limit=1&sort=occurrenceDbId:desc',false);

            if($plotRecord['totalCount'] >= $limit){ //Use the background process
                $token = Yii::$app->session->get('user.b4r_api_v3_token');
                $refreshToken = Yii::$app->session->get('user.refresh_token');
                //call background process for deletion
                $requestDataParams = [
                    'occurrenceIds'=>$occurrenceIds,
                    'processName' => 'delete-occurrences',
                    'experimentDbId' => $id.""
                ];

                $params = [
                    'httpMethod' => 'DELETE',
                    'endpoint' => 'v3/plot',
                    'entity' => 'PLOT',
                    'entityDbId' => ''.$id,
                    'endpointEntity' => 'PLOT',
                    'application' => 'EXPERIMENT_CREATION',
                    'dbIds' => ''.$id,
                    'descpription' => 'Deletion of plot records',
                    'requestData' => $requestDataParams,
                    'tokenObject' => [
                        "token" => $token,
                        "refreshToken" => $refreshToken
                    ],
                ];

                $deletePlots = Yii::$app->api->getParsedResponse('POST', 'processor', json_encode($params));

                if(isset($deletePlots['data'][0])){
                    $bgJobId = isset($deletePlots['data'][0]['backgroundJobDbId']) ? $deletePlots['data'][0]['backgroundJobDbId'] : null;
                    $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($deletePlots['data'][0]['backgroundJobDbId']);
                    
                    $experimentStatus = isset($experiment['experimentStatus']) ? $experiment['experimentStatus'] : '';
                    
                    if(!empty($experimentStatus)){
                        $status = explode(';',$experimentStatus);
                        $status = array_filter($status);
                    }else{
                        $status = [];
                    }

                    if($bgStatus !== 'done' && !empty($bgStatus)){
                        $updatedStatus = implode(';', $status);
                        $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
                    
                        $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);

                        //Update the experiment notes to remove plantingArrangementChanged, empty the notes
                        $this->mainExperiment->updateOne($id, ["notes" => null]);
                    }else if($bgStatus == 'done'){

                        //Update the experiment notes to remove plantingArrangementChanged, empty the notes
                        $this->mainExperiment->updateOne($id, ["notes" => null]);
                        $deleteFlag = false;
                    }

                    //update experiment
                    $message = '<i class="fa fa-info-circle"></i> The plot and planting instruction records for <strong>'.$experiment['experimentName'].'</strong> are being deleted in the background. You may proceed with other tasks. You will be notified in this page once done.';
                  
                        return ['success' => true, 'redirect' => true, 'message'=>$message];
                    }else{
                        return ['success' => false, 'redirect' => redirect, 'message'=>'Unable to complete request. Please try again.'];
                    }
            } else {
                //Retrieve all plot records of the occurrence
                $plotRecord = $this->plot->searchAll(["fields"=>"plot.occurrence_id AS occurrenceDbId|plot.id AS plotDbId|plot.entry_id AS entryDbId", "occurrenceDbId"=>"equals ".implode('|equals ', $occurrenceIds)], 'includeVoidedEntries=true',true);
                $plotIdsArr = ($plotRecord['totalCount'] > 0) ? ArrayHelper::map($plotRecord['data'],'plotDbId','plotDbId'):[];
                $plotIdsKey = (!empty($plotIdsArr)) ? array_keys($plotIdsArr) :  [];
                $entryDbIds = ($plotRecord['totalCount'] > 0) ? array_column($plotRecord['data'], 'entryDbId'):[];

                // Normal deletion, remove planting instruction first then the plot records
                // Retrieve all the planting instruction records of the occurrence
                $plantingInsRecord = $this->plantingInstruction->searchAll(["fields"=>"plantingInstruction.id AS plantingInstructionDbId|plot.occurrence_id as occurrenceDbId", "occurrenceDbId"=>"equals ".implode('|equals ', $occurrenceIds)], null, true);
              
                if(isset($plantingInsRecord['data']) && $plantingInsRecord['totalCount'] > 0){
                    $plantingInsIdsArr = ArrayHelper::map($plantingInsRecord['data'],'plantingInstructionDbId','plantingInstructionDbId');
                    $plantingInsIdsKey = array_keys($plantingInsIdsArr);
                    $plantingDelRecord = $this->plantingInstruction->deleteMany($plantingInsIdsKey);
                }

                $plantingInsRecordEntries = (!empty($entryDbIds) > 0) ? $this->plantingInstruction->searchAll(["fields"=>"plantingInstruction.id AS plantingInstructionDbId|plantingInstruction.entry_id AS entryDbId", "entryDbId"=>"equals ".implode('|equals ', $entryDbIds)], null, true) : [];

                if(isset($plantingInsRecordEntries['data']) && $plantingInsRecordEntries['totalCount'] > 0){
                    $plantingInsIdsArr = ArrayHelper::map($plantingInsRecordEntries['data'],'plantingInstructionDbId','plantingInstructionDbId');
                    $plantingInsIdsKey = array_keys($plantingInsIdsArr);
                    $plantingDelRecord = $this->plantingInstruction->deleteMany($plantingInsIdsKey);
                }
                
                if(isset($plantingDelRecord['success']) && $plantingDelRecord['success']){
                    $plotDelRecord = $this->plot->deleteMany($plotIdsKey);
                    
                    if($plotDelRecord['success']){
                        //set to false means that there's no need to check the background job status
                        $deleteFlag = false;
                        //Update the experiment notes to remove plantingArrangementChanged, empty the notes
                        $this->mainExperiment->updateOne($id, ["notes" => null]);

                        //update status
                        $experimentStatus = isset($experiment['experimentStatus']) ? $experiment['experimentStatus'] : '';
                        $status = explode(';', $experimentStatus);
                        $indexSearch = array_search('occurrences created', $status);
                        if($indexSearch !== false){
                            unset($status[$indexSearch]);
                        }

                        $newStatus = implode(';', $status);
                        //delete excess occurrences
                        if($experimentType == 'Observation'){
                            $deleteRecords = $this->occurrence->deleteMany($occurrenceIds);
                        }

                        $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);
                    }
                }
                $this->mainExperiment->updateOne($id, ["notes" => null]);
            }

            return ['success' => true, 'redirect' => false, 'message'=>'Successfully deleted the previously generated records.'];
        } else {
            //update status
            $this->mainExperiment->updateOne($id, ["notes" => null]);

            //update status
            $experimentStatus = isset($experiment['experimentStatus']) ? $experiment['experimentStatus'] : '';
            $status = explode(';', $experimentStatus);
            $indexSearch = array_search('occurrences created', $status);
            if($indexSearch !== false){
                unset($status[$indexSearch]);
            }

            $newStatus = implode(';', $status);

            $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);

            return ['success' => true, 'redirect' => false, 'message'=>'Successfully deleted the previously generated records.'];
        }
    }

    /**
     * Get the entries of the entries' current page 
     * @param integer $id record id of the experiment
     * @param string $program name of the current program
     * @param integer $processId integer process identifier
     */
    public function actionEntriesCurrentPage($id, $program, $processId){
        // retrieve entry list variables
        $entryListVars = $this->mainExperimentModel->retrieveConfiguration('specify-entry-list', $program, $id, $processId);
        
        $userId = $this->userModel->getUserId();
        
        // get params for data browser
        $paramPage = '';
        $paramSort = '';

        $queries = array();
        $urlParams = $_POST['urlParams'];
        $urlParams = explode('?',$urlParams);   // removes the first part of the url
        $urlParams = $urlParams[1];
        parse_str($urlParams, $queries);

        $urlParams = Yii::$app->request->queryParams;
        if(isset($queries['EntrySearch']) && $queries['EntrySearch'] != null){
            $urlParams['EntrySearch'] = $queries['EntrySearch'];
        }
        
        if (isset($_GET['sort'])) {
            $paramSort = 'sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }
        
        
        //Get the page to retrieve
        $currentPage = 0;
        if(isset($_POST['page']) && $_POST['page'] != null){
            $paramPage = '&page=' . $_POST['page'];
            $currentPage = $_POST['page'];
        }

        //default
        $paramLimit = '?limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();

        $pageParams = $paramLimit.$paramPage;

        //append sorting condition
        $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;
        $entryModel = $this->entrySearch;
        
        $dataProvider = $entryModel->search($urlParams, $entryListVars, null, false, null, $pageParams, $currentPage);
        
        //Get all the entry ids 
        $entryIds = [];
        $entryIds = array_column($dataProvider->allModels,'entryDbId');
        
        //List of checked/selected entries
        $selectedIdName = "ec_$userId"."_selected_entry_ids_$id";
        
        $selectedItems = (isset($_SESSION[$selectedIdName]) && $_SESSION[$selectedIdName] !== null) ? $_SESSION[$selectedIdName] : [];
        $selectedItems = isset($_POST['selectedItems']) ? $_POST['selectedItems'] : $selectedItems;
        
        //Get the selected items in the current page only
        $entryIds = array_filter(array_intersect($selectedItems, $entryIds));
        return json_encode(['entryIds' => $entryIds,'entryIdsCount'=>count($entryIds)]);
    }
}