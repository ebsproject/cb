<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

// NOTE: add classes here in alphabetical order
use app\models\BackgroundJob;
use app\models\Config;
use app\models\Cross;
use app\models\CrossAttribute;
use app\models\CrossSearch;
use app\models\CrossList;
use app\models\CrossingList;
use app\models\CrossParent;
use app\models\Entry;
use app\models\EntryData;
use app\models\EntryList;
use app\models\EntryListData;
use app\models\EntrySearch;
use app\models\Experiment;
use app\models\ExperimentBlock;
use app\models\ExperimentData;
use app\models\ExperimentDesign;
use app\models\ExperimentGroup;
use app\models\ExperimentGroupExperiment;
use app\models\ExperimentSearch;
use app\models\Facility;
use app\models\FileCabinet;
use app\models\GeospatialObject;
use app\models\Item;
use app\models\Occurrence;
use app\models\OccurrenceData;
use app\models\OccurrenceSearch;
use app\models\Person;
use app\models\Place;
use app\models\PlanTemplate;
use app\models\PlatformList;
use app\models\PlatformListMember;
use app\models\PlatformModule;
use app\models\PlantingInstruction;
use app\models\Plot;
use app\models\Program;
use app\models\RandomizationTransaction;
use app\models\Scale;
use app\models\Seed;
use app\models\Study;
use app\models\TempEntry;
use app\models\TempPlot;
use app\models\TempStudy;
use app\models\TempStudyData;
use app\models\User;
use app\models\Variable;
use app\models\UserDashboardConfig;
use app\models\Worker;
use app\modules\occurrence\models\FileExport;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

use app\modules\crossManager\models\CrossListModel;
use app\modules\crossManager\models\EntryListProtocol;
use app\modules\occurrence\models\Manage;
use app\modules\account\models\ListModel;
use app\modules\experimentCreation\controllers\RemoveController;
use app\modules\experimentCreation\models\AnalysisModel;
use app\modules\experimentCreation\models\BrowserModel;
use app\modules\experimentCreation\models\CopyExperimentModel;
use app\modules\experimentCreation\models\CrossPatternModel;
use app\modules\experimentCreation\models\CrossingMatrix;
use app\modules\experimentCreation\models\DesignModel;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\EntryOrderModel;
use app\modules\experimentCreation\models\FemaleListSearch;
use app\modules\experimentCreation\models\FormModel;
use app\modules\experimentCreation\models\GenerateRecordModel;
use app\modules\experimentCreation\models\MaleListSearch;
use app\modules\experimentCreation\models\PlantingArrangement;
use app\modules\experimentCreation\models\ProtocolModel;
use app\modules\experimentCreation\models\PackageModel;
use app\modules\experimentCreation\models\VariableComponent;


use app\components\B4RController;

use app\controllers\Dashboard;
use GuzzleHttp\Utils;
use yii\db\Expression;

use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use yii\web\NotFoundHttpException;

use function GuzzleHttp\json_decode;

use Yii;

/**
 * Main controller for the `Experiment Creation` module
 */
class CreateController extends B4RController
{

    public function __construct(
        $id,
        $module,
        public CopyExperimentModel $copyExperiment,
        public Cross $cross,
        public CrossAttribute $crossAttribute,
        public CrossPatternModel $crossPatternModel,
        public CrossListModel $crossListModel,
        public CrossParent $crossParent,
        public CrossSearch $crossSearch,
        public BackgroundJob $backgroundJob,
        public BrowserModel $browserModel,
        public Dashboard $dashboard,
        public DesignModel $designModel,
        public Entry $entryModel,
        public EntryData $entryDataModel,
        public EntryOrderModel $entryOrderModel,
        public EntryList $entryListModel,
        public EntryListData $entryListDataModel,
        public EntryListProtocol $entryListProtocolModel,
        public EntrySearch $entrySearchModel,
        public Experiment $mainExperiment,
        public ExperimentBlock $experimentBlock,
        public ExperimentData $mainExperimentData,
        public ExperimentDesign $mainExperimentDesign,
        public ExperimentModel $mainExperimentModel,
        public ExperimentSearch $experimentSearch,
        public FormModel $formModel,
        public GeospatialObject $geospatialObjectModel,
        public Item $mainItemModel,
        public Manage $manage,
        public Config $mainConfigModel,
        public Variable $mainVariableModel,
        public VariableComponent $variableComponent,
        public UserDashboardConfig $userDashboardConfig,
        public Scale $mainScaleModel,
        public Occurrence $occurrenceModel,
        public OccurrenceData $occurrenceDataModel,
        public OccurrenceSearch $occurrenceSearch,
        public PlanTemplate $planTemplate,
        public Person $personModel,
        public PlantingInstruction $plantingInstruction,
        public PlantingArrangement $plantingArrangement,
        public PlatformList $platformList,
        public Plot $plotModel,
        public ProtocolModel $protocolModel,
        public PackageModel $packageModel,
        public RemoveController $remove,
        public User $userModel,
        public FemaleListSearch $femaleSearchModel,
        public MaleListSearch $maleSearchModel,
        public Worker $worker,
        $config = []
        )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Browser for the created experiments within the program
     *
     * @param string $program name of the current program
     */
    public function actionIndex($program){

        $dashboardFilters = (array) $this->dashboard->getFilters();
        $params = Yii::$app->request->queryParams;

        foreach($dashboardFilters as $key => $value){
            $params['ExperimentSearch'][$key] = $value;
        }

        // if program is empty, add invalid program id value
        if(empty($program)){
            $params['ExperimentSearch']['program_id'] = "0";
        }

        if(isset($_GET['id']) && !empty($_GET['id'])){
            $params['ExperimentSearch']['experimentDbId'] = $_GET['id'];
        }

        // get params for data browser
        $paramPage = '';
        $pageParams = '';
        $paramSort = '';
        
        // get current sorting
        if (isset($_GET['sort'])) {
            $paramSort = 'sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }

        $paramLimit = '?limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        
      
        if (isset($_GET['dynagrid-experiment-browser-page'])){
            $paramPage = '&page=' . $_GET['dynagrid-experiment-browser-page'];
        }

        $currentPage = !empty($params) ? isset($_GET['dynagrid-experiment-browser-page']) ? $_GET['dynagrid-experiment-browser-page'] : null : null;
            
        $pageParams = $paramLimit.$paramPage;
        $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;
                
        $dataProvider = $this->experimentSearch->search($params, $pageParams, $currentPage);
   
        $statusOptions = $this->mainExperimentModel->getExperimentStatus();
        $statusOptions = array_map('trim', $statusOptions);
        $statusOptions = array_filter($statusOptions);  // remove whitespace values
        $statusOptions = array_unique($statusOptions);  // remove duplicates

        return $this->render('/browse/index',[
            'dataProvider' => $dataProvider,
            'searchModel' => $this->experimentSearch,
            'program' => $program,
            'statusOptions' => $statusOptions
        ]);
    }

    /**
     * Delete the experiment and all its records
     *
     * @param string $program name of the current program
     */
    public function actionDeleteExperiment($program){

        $id = isset($_POST['id']) ? $_POST['id'] : 0;
      
        $result = $this->browserModel->deleteDraftExperiment($id);
        if(isset($result['success'])){
            Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully deleted experiment and all related records.');
        }     

        return $this->redirect(['create/index?program='.$program]);
    }

    /**
     * Delete the experiment group and all its records
     *
     * @param integer $id record id of the experiment
     * @param string $program name of the current program
     */
    public function actionDeleteExperimentGroup($id, $program, $processId){

        if(isset($_POST)){

            $experimentGroupId = $_POST['experiment_group_id'];

            $experimentGroup = ExperimentGroup::findOne($experimentGroupId);

            //find all temp study
            $temporaryStudies = TempStudy::find()->where(['in', 'experiment_group_id',[$experimentGroupId]])->all();
            $tempStudyIds = array_column($temporaryStudies, 'id');
            TempStudyData::deleteAll(['in', 'temp_study_id',$tempStudyIds]);

            TempStudy::deleteAll(['in', 'experiment_group_id',[$experimentGroupId]]);
            ExperimentGroupExperiment::deleteAll(['in', 'experiment_group_id', [$experimentGroupId]]);
            $experimentGroup->delete();

            ExperimentModel::deleteExistingStudies($id);

            Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully deleted experiment group and all related records.');
        }

        return $this->redirect(['create/specify-experiment-groups?id='.$id.'&program='.$program.'&processId='.$processId]);

    }

   /**
     * Renders view for specifying basic information of the experiment
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     *
     */
    public function actionSpecifyBasicInfo(int $id = NULL, $program, int $processId=null, string $type=null){
        $experimentTemplate = '';
        $experimentTemplateId = '';
        $template = '';
        $experimentType = '';


        $defaultFilters = $this->dashboard->getFilters();
        $dashProgram = $defaultFilters->program_id;

        if($id != NULL){
            // This is for the refactored retrieval of basic info
            $model = $this->findModel($id);
        } else {
            $model = $this->mainExperiment;
        
            if(isset($_POST)){
                $defProcId = isset($_GET['processId']) ? $_GET['processId'] : '';
                $template = isset($_POST['template']) ? $_POST['template'] : '';
                $experimentTemplateId = isset($_POST['template']) && !empty($_POST['template']) ? $_POST['template'] : $defProcId;
                $experimentType = isset($_POST['exptType']) ? $_POST['exptType'] : '';
                $processId = $experimentTemplateId;
            }
        }
     
        if(isset($_POST) && !empty($_POST)){
            // Save experiment
            $formData = Yii::$app->request->post();
        
            $savedData = $this->mainExperimentModel->saveExperiment(Yii::$app->request->post(), $program, 'experiment', $id, null, null, $model, Yii::$app->controller->action->id);
            $id = $savedData['experimentId'];
            $savedExptModel = $this->mainExperiment->getExperiment($id);
           
            if(empty($savedData['errorMsg'])){
                if($savedExptModel['dataProcessAbbrev'] !== 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
                    Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully created '.$savedExptModel['experimentName'].'.');
                }
            }else{
                Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning"></i>' . $savedData['errorMsg']);
            }
            
            if(isset($formData['nextUrl']) && !empty($id)){
                $url = explode('?', $formData['nextUrl']);
                $urlProc = explode('experimentCreation/', $url[0]);
                return $this->redirect([$urlProc[1].'?id='.$id.'&program='.$program.'&processId='.$processId]);
            } else{
                return $this->redirect(['create/index?program='.$program]);
            }
        }

        $activities = $this->formModel->prepare(Yii::$app->controller->action->id, $program, $id, $processId);

        if(!empty($activities['children'])){
            $index = array_search($activities['active'], array_column($activities['children'], 'action_id'));
            $activityAbbrev = $activities['children'][$index]['abbrev'].'_VAL';
      
            //Get All required variables for basic info
            $requiredConfig = $this->mainConfigModel->getConfigByAbbrev($activityAbbrev); 
            
            $item = $this->mainItemModel->searchAll(["itemDbId"=>"equals $processId"]);
            $itemData = isset($item['data'][0]) ? $item['data'][0] : '';
            
            if(strpos($itemData['name'],'Cross Parent Nursery') !== false){
                $experimentTypeTempArr = explode('(Phase',$itemData['name']);
                $itemName = rtrim($experimentTypeTempArr[0]);
            }else{
                $itemName = $itemData['name'];
            }

            $experimentType = !empty($experimentType) ? $experimentType : $itemName;
            $defaultOps = ['experiment_template_id'=>$processId, 'experiment_template'=>$template,'experiment_type'=>$experimentType];
            
            $variableYear = $this->mainVariableModel->getVariableByAbbrev('YEAR');
            $yearScale = $this->mainScaleModel->getVariableScale($variableYear['variableDbId']);  
            $yearMinValue = $yearScale['minValue'];
            $yearMaxValue = $yearScale['maxValue'];
            
            if(!empty($id)){
                $itemEntry = $this->mainItemModel->searchItemRecord(['itemDbId'=>"$processId"]);
                $template = $itemEntry['data']['name'];
                
                $widgetOptions = [
                    'mainModel' => 'Experiment',
                    'config' => $requiredConfig['Values'],
                    'templateId' => $processId,
                    'type' => $experimentType,
                    'processType' => 'experiment',
                    'mainApiResourceMethod' => 'POST',
                    'mainApiResourceEndpoint' => 'experiments/'.$id.'/data-search',
                    'mainApiSource' => 'metadata',
                    'mainApiResourceFilter' => ["experimentDbId"=>"$id"],
                    'program' => $dashProgram,
                    'id' => $id
                ];
            }else{
                $widgetOptions = [
                    'mainModel' => 'Experiment',
                    'config' => $requiredConfig['Values'],
                    'templateId' => $processId,
                    'type' => $experimentType,
                    'processType' => 'experiment',
                    'program' => $dashProgram,
                    'id' => $id
                ];
            }

            return $this->render('basic',[
                'model' => $model,
                'program' => $program,
                'hasExperimentGroups' =>true,
                'processId' => $processId,
                'experiment_template' => $experimentTemplate,
                'experiment_template_id' => $experimentTemplateId,
                'template' => $template,
                'experiment_type' => $experimentType,
                'year_min_value' => $yearMinValue,
                'year_max_value' => $yearMaxValue,
                'widgetOptions' => $widgetOptions,
                'experimentType' => $experimentType
            ]);
        } else{
            Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> Entered an invalid Experiment Template.');
            return $this->redirect(['default/index?program='.$program]);
        }
    }

    /**
     * Renders view for entry list management for the experiment
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId integer process identifier
     *
     */
    public function actionSpecifyEntryList($program, $id, $processId){

        $userModel = new User();
        $userId = $userModel->getUserId();
        $filteredIdName = "ec_$userId"."_filtered_entry_ids_$id";
        $selectedIdName = "ec_$userId"."_selected_entry_ids_$id";
        //destroy selected items session
        if(((isset($_GET['reset']) && $_GET['reset'] == 'hard_reset' && !isset($_GET['grid-entry-list-grid-page'])) ||
            (!isset($_GET['reset']) && !isset($_GET['grid-entry-list-grid-page']))) && isset($_SESSION[$selectedIdName])){
            unset($_SESSION[$selectedIdName]);
            if(isset($_SESSION[$filteredIdName])){
                unset($_SESSION[$filteredIdName]);
            }
        }

        // get params for data browser
        $paramPage = '';
        $paramSort = '';
    
        // get current sorting
        if (isset($_GET['sort'])) {
            $paramSort = 'sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $sortAttribute = substr($column, 1) == 'parentRole' ? 'entryRole' : substr($column, 1);

                    $paramSort = $paramSort . $sortAttribute . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $sortAttribute = $column == 'parentRole' ? 'entryRole' : $column;

                    $paramSort = $paramSort . $sortAttribute;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }

        if (isset($_GET['dp-1-page'])){
            $paramPage = '&page=' . $_GET['dp-1-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        } else if (isset($_GET['grid-entry-list-grid-page'])) {
            $paramPage = '&page=' . $_GET['grid-entry-list-grid-page'];
        }
        $paramLimit = '?limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        $pageParams = $paramLimit.$paramPage;
        // append sorting condition
        $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;

        $model = $this->mainExperiment->getExperiment($id);
        $searchModel = $this->entrySearchModel;
        $params = Yii::$app->request->queryParams;
        
        $storedItems = Yii::$app->session->get($selectedIdName);
        //retrieve configuration
        $configData = $this->mainExperimentModel->retrieveConfiguration(Yii::$app->controller->action->id, $program, $id, $processId);
        //retrieve static columns configuration
        $entryListColumnsConfig = $this->mainConfigModel->getConfigByAbbrev('EC_ENTRY_LIST_BROWSER_STATIC_COLUMNS_CONFIG');
        $entryListColumnsConfig = isset($entryListColumnsConfig['values']) ? $entryListColumnsConfig['values'] : [];
        $dataProvider = $searchModel->search($params, $configData, null, false, null, $pageParams);

        $origParams = Yii::$app->session->get('ec-entry-list_params');
        $parameters = [
            'experimentDbId' => $params['id'],
            'params' => isset($params['EntrySearch']) ? $params['EntrySearch'] : []
        ];
        Yii::$app->session->set('ec-entry-list_params', $parameters);  // for selection

        $paramsReset = false;
        if(($origParams != $parameters)) {
            $paramsReset = true;
        }
        Yii::$app->session->set('resetEntryListParamsSelection', $paramsReset);

        $entryListId = $this->entryListModel->getEntryListId($id);
        
        if(!empty($entryListId)) {

            $entryRecords = $this->entryModel->searchAll([
                "fields" => "entry.entry_list_id AS entryListDbId|entry.germplasm_id as germplasmDbId",
                "entryListDbId"=>"equals $entryListId"
            ], 'limit=1&sort=germplasmDbId:desc', false);

            //Check if all entries has seeds, then add entry list created to status
            $filterNoPackages = [
                "fields" => "entry.entry_list_id AS entryListDbId|entry.seed_id as seedDbId|entry.package_id as packageDbId",
                "packageDbId" => "is null",
                "entryListDbId"=>"equals $entryListId",
            ];

            $noPackageRecords = $this->entryModel->searchAll($filterNoPackages, 'limit=1&sort=seedDbId:desc', false);
            
            $filterNoGIDs = [
                "fields" => "entry.entry_list_id AS entryListDbId|entry.id as entryDbId|entry.germplasm_id as germplasmDbId",
                "germplasmDbId" => "null",
                "entryListDbId"=>"equals $entryListId",
            ];

            $noGIDRecords = $this->entryModel->searchAll($filterNoGIDs, 'limit=1&sort=entryDbId:desc', false);

            if($entryRecords['totalCount'] > 0 && $noPackageRecords['totalCount'] == 0 && $noGIDRecords['totalCount'] == 0){
               //This part will be validated in the view part
            } elseif($entryRecords['totalCount'] == 0){
                $newStatus = 'draft';
                $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);
            }
        } else {
            $newStatus = 'draft';
            $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);
        }

        $displayName = '';
        // Get display name for filename of export
        if(!empty($model)){
            $parentActAbbrev = str_replace('_DATA_PROCESS', '', $model['dataProcessAbbrev']);
            $entryListActAbbrev = $parentActAbbrev.'_ENTRY_LIST_ACT';
            $entryListAct = $this->mainItemModel->getAll("abbrev=$entryListActAbbrev");
            $displayName = isset($entryListAct['data'][0]['displayName']) ? strtolower($entryListAct['data'][0]['displayName']) : 'entry list';
        }   
        

        return $this->render('entry_list',[
            'model' => $model,
            'displayName' => $displayName,
            'program' => $program,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $entryListColumnsConfig,
            'id' => $id,
            'processId' => $processId,
            'requiredFields' => $configData,
            'parentages' => [],
            'selectedItems' => $storedItems,
            'entryListId' => $entryListId,
            'urlParams' => $params,
            'entryListBrowserSort' => isset($_GET['sort'])? $_GET['sort'] : null,
            'exportRecordsThresholdValue' => Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportRecords')
        ]);
    }

    /**
    * Check entry list browser selection if it needs to be reset or not
    */
    public function actionCheckEntryListSelectionStatus() {
        $result = [
            'resetEntryListParamsSelection' => $_SESSION['resetEntryListParamsSelection'] ?? []
        ];

        Yii::$app->session->set('resetEntryListParamsSelection', false);

        return json_encode($result);
    }

    /**
     * Export entries information in CSV format
     *
     * @param integer $id Experiment identifier
     * @param $entity string entity identifier
     */
    public function actionExportEntity($id, $entity)
    {
        $entities = [];
        $columns = [];
        $csvContent = [[]];
        $i = 1;

        // retrieve experiment number for CSV filename
        $experiment = $this->mainExperiment->getOne($id);

        // redirect to previous page if data retrieval fails
        if(empty($experiment['success'])){
            $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl)->send();
            exit;
        }

        if($entity == 'entry') {
            $filters = Yii::$app->session->get('experiment-entry-list-filters-' . $id);
            $paramSort = '?sort=entryNumber';
            $entities['data'] = \Yii::$container->get('app\models\Entry')->getEntries($id, $filters, null, $paramSort, null, true)['entries'];

            $columns = [
                "entryNumber" => "ENTRY NO.",
                "entryCode" => "ENTRY CODE",
                "seedName" => "Seed Name",
                "packageLabel" => "Package Label",
                "germplasmCode" => "GERMPLASM CODE",
                "designation" => "GERMPLASM NAME",
                "parentage" => "PARENTAGE",
                "entryType" => "Entry Type",
            ];

            if($experiment['data']['experimentType'] == 'Cross Parent Nursery' || $experiment['data']['experimentType'] == 'Intentional Crossing Nursery') {
                $columns['entryRole'] = "Parent Role";
            }

            $fileType = FileExport::ENTRY_LIST;
        } else if ($entity == 'cross') {
            $params = ["experimentDbId"=>"equals $id","includeParentSource"=>true];
            $entities = \Yii::$container->get('app\models\Cross')->searchAll($params);

            $columns = [
                "crossName" => "Cross Name",
                "crossFemaleParent" => "Cross Female Parent",
                "femaleParentage" => "Female Parentage",
                "crossMaleParent" => "Cross Male Parent",
                "maleParentage" => "Male Parentage",
                "crossMethod" => "Cross Method",
                "crossRemarks" => "Cross Remarks",
                "femaleParentEntryNumber" => "Female Parent Entry Number",
                "maleParentEntryNumber" => "Male Parent Entry Number",
                "femaleParentSeedSource" => "Female Parent Seed Source",
                "maleParentSeedSource" => "Male Parent Seed Source",
            ];

            $fileType = FileExport::CROSS_LIST;
        }

        $csvAttributes = array_keys($columns);
        $csvHeaders = array_values($columns);

        if (!empty($entities['data']) && isset($entities['data'])) {
            foreach ($csvHeaders as $key => $value)
                array_push($csvContent[0], $value);

            foreach ($entities['data'] as $key => $value) {
                $csvContent[$i] = [];

                foreach ($csvAttributes as $k => $v)
                    array_push($csvContent[$i], $value[$v]);

                $i += 1;
            }
        }

        $toExport = new FileExport($fileType, FileExport::CSV, time());

        $experimentCode = $experiment['data']['experimentCode'];
        $toExport->addExperimentData($experimentCode, ['001'], [$csvContent]);

        $buffer = $toExport->getUnzipped();
        $filename = array_key_first($buffer);
        $content = $buffer[$filename];

        header("Content-Disposition: attachment; filename={$filename}; Content-Type: application/csv;");

        $fp = fopen('php://output', 'wb');

        if ($fp === false) die('Failed to create CSV file.');

        foreach ($content as $line) fputcsv($fp, $line);

        fclose($fp);

        exit;
    }

    /**
     * Delegate building of CSV data to the background worker
     *
     * @param integer $id Experiment identifier
     * @param string $entity name of the entity
     */
    public function actionPrepareDataForCsvDownload ($id, $program, $entity = 'entry')
    {
        $experiment = $this->mainExperiment->getOne($id);
        $experimentName = $experiment['data']['experimentName'];

        $url = '';
        $requestBody = null;
        $csvHeaders = [];
        $csvAttributes = [];
        $filename = '';

        if ($entity == 'entry') {
            // Build URL
            $url = "entries-search?" . 'sort=entryNumber';

            // Build request body
            $requestBody = Yii::$app->session->get('experiment-entry-list-filters-' . $id);

            $entryListId = \Yii::$container->get('app\models\EntryList')->getEntryListId($id);
            $requestBody['entryListDbId'] = "equals ".$entryListId;

            // Build CSV Attributes and Headers
            $columns = [
                "entryNumber" => "ENTRY NO.",
                "entryCode" => "ENTRY CODE",
                "seedName" => "Seed Name",
                "packageLabel" => "Package Label",
                "germplasmCode" => "GERMPLASM CODE",
                "designation" => "GERMPLASM NAME",
                "parentage" => "PARENTAGE",
                "entryType" => "Entry Type",
            ];

            if($experiment['data']['experimentType'] == 'Cross Parent Nursery' || $experiment['data']['experimentType'] == 'Intentional Crossing Nursery') {
                $columns['entryRole'] = "Parent Role";
            }

            $fileType = FileExport::ENTRY_LIST;
        }else if ($entity == 'cross') {
            // Build URL
            $url = "crosses-search?";

            // Build request body
            $requestBody = ["experimentDbId"=>"equals $id","includeParentSource"=>true];

            // Build CSV Attributes and Headers
            $columns = [
                "crossName" => "Cross Name",
                "crossFemaleParent" => "Cross Female Parent",
                "femaleParentage" => "Female Parentage",
                "crossMaleParent" => "Cross Male Parent",
                "maleParentage" => "Male Parentage",
                "crossMethod" => "Cross Method",
                "crossRemarks" => "Cross Remarks",
                "femaleParentEntryNumber" => "Female Parent Entry Number",
                "maleParentEntryNumber" => "Male Parent Entry Number",
                "femaleParentSeedSource" => "Female Parent Seed Source",
                "maleParentSeedSource" => "Male Parent Seed Source",
            ];

            $fileType = FileExport::CROSS_LIST;
        }

        $csvAttributes = array_keys($columns);
        $csvHeaders = array_values($columns);

        $experimentCode = $experiment['data']['experimentCode'];

        $toExport = new FileExport($fileType, FileExport::CSV, time());
        $filename = str_replace(
            '.csv',
            '',
            $toExport->getFilenames(
                $experimentCode,
                $toExport->formatSerialCodes(['001'])
            )[0]
        );

        // get user ID
        $userId = Yii::$app->session->get('user.id');

        $backgroundJobParams = [
            "workerName" => "BuildCsvData",
            "description" => "Preparing $entity records for downloading",
            "entity" => "EXPERIMENT",
            "entityDbId" => $id,
            "endpointEntity" => "EXPERIMENT",
            "application" => "EXPERIMENT_CREATION",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId,
            'remarks' => $filename,
        ];

        try {
            $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
        }

        // if creating transaction is successful
        try {
            if (isset($transaction['status']) && $transaction['status'] == 200) {
                $message = "The $entity records for <b>$experimentName</b> are being prepared in the background for download. You may proceed with other tasks. You will be notified in this page once done.";
                // set background processing info to session
                Yii::$app->session->set('em-bg-processs-message', $message);
                Yii::$app->session->set('em-bg-processs', true);

                $host = getenv('CB_RABBITMQ_HOST');
                $port = getenv('CB_RABBITMQ_PORT');
                $user = getenv('CB_RABBITMQ_USER');
                $password = getenv('CB_RABBITMQ_PASSWORD');

                $connection = new AMQPStreamConnection($host, $port, $user, $password);

                $channel = $connection->channel();

                $channel->queue_declare(
                    'BuildCsvData', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                    false,  //passive - can use this to check whether an exchange exists without modifying the server state
                    true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                    false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                    false   //auto delete - queue is deleted when last consumer unsubscribes
                );

                $msg = new AMQPMessage(
                    json_encode([
                        'tokenObject' => [
                            'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                            'refreshToken' => ''
                        ],
                        'backgroundJobId' => $transaction["data"][0]["backgroundJobDbId"],
                        'description' => "The $entity records have been written successfully. The file is now ready for download.",
                        'url' => $url,
                        'requestBody' => json_encode($requestBody),
                        'csvHeaders' => $csvHeaders,
                        'csvAttributes' => $csvAttributes,
                        'fileName' => $filename,
                    ]),
                    array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                );

                $channel->basic_publish(
                    $msg,   //message
                    '', //exchange
                    'BuildCsvData'  //routing key (queue)
                );
                $channel->close();
                $connection->close();
                Yii::$app->session->set("bgp-export-$entity-filename-$id", "$experimentName-$entity-records");

                // redirect to Experiments browser
                // effectively exits this function (will trigger a (false) error response in the calling ajax)
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$id]);
            } else {
                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
            }
        } catch (\Exception $e) {
            \ChromePhp::log('e:', $e);
        }
    }

     /**
     * Check the status of the experiment
     * @param integer $id record id of the experiment
     * @param string $action rendered view to perform the checking
     */
    public function actionCheckStatus($id, $action=''){
        $status = '';
        $model = $this->mainExperiment->getExperiment($id);
        $entryIdList = [];

        if($action == 'specify-entry-list'){
            $entryListId = $this->entryListModel->getEntryListId($id);
            if(!empty($entryListId)){
                
                $entryRecords = Entry::searchRecords(["fields"=>"entry.id as entryDbId|entry.entry_list_id AS entryListDbId",
                                "entryListDbId"=>"equals ".$entryListId]);

                //Check if all entries has packages, then add entry list created to status
                $filterNoPackages = [
                    "fields" => "entry.id as entryDbId|entry.entry_list_id AS entryListDbId|entry.package_id AS packageDbId",
                    "packageDbId" => "is null",
                    "entryListDbId"=>"equals ".$entryListId
                ];

                $noPackageRecords = Entry::searchRecords($filterNoPackages);
                
                $filterNoGIDs = [
                    "fields" => "entry.id as entryDbId|entry.entry_list_id AS entryListDbId|entry.germplasm_id AS germplasmDbId",
                    "germplasmDbId" => "null",
                    "entryListDbId"=>"equals ".$entryListId
                ];

                $noGIDRecords = Entry::searchRecords($filterNoGIDs);

                $status = explode(';', $model['experimentStatus']);
                $status = array_filter($status);
                
                if(in_array('draft', $status)){
                    $index = array_search('draft', $status);
                    unset($status[$index]);
                }

                if(!empty($entryRecords)){
                    $entryIdList = ArrayHelper::map($entryRecords, 'entryDbId','entryDbId');
                    $entryIdListKeys = array_keys($entryIdList);
                    
                    $experimentModel = $this->mainExperiment->searchAll(["fields"=>"experiment.id AS experimentDbId|program.program_code AS programCode|experiment.data_process_id AS dataProcessDbId",'experimentDbId'=>"equals ".$id]);   
  
                    $configDataTemp = $this->mainExperimentModel->retrieveConfiguration('specify-entry-list', $experimentModel['data'][0]['programCode'], $id, $experimentModel['data'][0]['dataProcessDbId']);
                    $finalConfigDataVal = isset($_POST['configValues']) && !empty($_POST['configValues']) ? json_decode($_POST['configValues'], true) : $configDataTemp;
                    $configData = !empty($finalConfigDataVal) ? $finalConfigDataVal : null;
                    
                    $apiParams = [
                        'apiResourceEndpoint' => 'entries-search',
                        'apiResourceMethod' => 'POST',
                        'apiResourceFilter' => ["fields"=>"entry.id as entryDbId|experiment.id AS experimentDbId|entry.entry_role AS entryRole|entry.entry_number AS entryNumber|
                                                entry.entry_code AS entryCode|entry.entry_name AS entryName|entry.entry_type AS entryType|entry.entry_class AS entryClass|
                                                entry.entry_status AS entryStatus","experimentDbId"=>"equals ".$id],
                        'apiResourceSort' => null,
                        'entryIdList' => $entryIdListKeys
                    ];
    
                    //Check if the required variables have been filled before experiment status is changed
                    $checkExptStatus = $this->mainExperimentModel->genericCheckRequiredData($id,$configData, $apiParams);

                    if(!empty($checkExptStatus) && $checkExptStatus != null){
                        //To update the status of the experiment
                        if(in_array("entry list created", $status)){
                            $index = array_search('entry list created', $status);
                            
                            if(isset($status[$index])){
                                unset($status[$index]);
                                
                                if(empty($status)){
                                    $newStatus = "draft";
                                }else{
                                    $newStatus = implode(';', $status);
                                }
                            }
                        }else{
                            $newStatus = implode(';',$status);
                        }

                        //To update the status of the experiment
                        if(in_array("entry list incomplete seed sources", $status)){
                            $index = array_search('entry list incomplete seed sources', $status);
                            
                            if(isset($status[$index])){
                                unset($status[$index]);
                                
                                if(empty($status)){
                                    $newStatus = "draft";
                                }else{
                                    $newStatus = implode(';', $status);
                                }
                            }
                        }else{
                            $newStatus = implode(';',$status);
                        }
                        $model['experimentStatus'] = $newStatus;
                    } else if(count($noPackageRecords) > 0){
                        //To update the status of the experiment
                        if(in_array("entry list created", $status)){
                           
                            $index = array_search('entry list created', $status);

                            if(isset($status[$index])){
                                $status[$index] = 'entry list incomplete seed sources';
                                
                                if(empty($status)){
                                    $newStatus = "draft";
                                }else{
                                    $newStatus = implode(';', $status);
                                }
                            }
                        }else if(!in_array("entry list incomplete seed sources", $status)){
                            $newStatus = implode(';',$status);
                            if(empty($status)){
                                $newStatus = "entry list incomplete seed sources";
                            }else{
                                $newStatus = "entry list incomplete seed sources;".implode(';', $status);
                            }
                        } else if(in_array("entry list incomplete seed sources", $status)){
                            $newStatus = implode(';', $status);
                        }
                        $model['experimentStatus'] = $newStatus;
                       
                    } else {
                        //Reset the experiment statsus
                        if(!in_array('entry list created', $status)){
                            
                            if(in_array('entry list incomplete seed sources', $status)){
                                $index = array_search('entry list incomplete seed sources', $status);
                                
                                if(isset($status[$index])){
                                    $status[$index] = 'entry list created';
                                    
                                    if(empty($status)){
                                        $newStatus = "draft";
                                    }else{
                                        $newStatus = implode(';', $status);
                                    }
                                    $model['experimentStatus'] = $newStatus;
                                }
                            } else {
                                $updatedStatus = implode(';', $status);
                                $model['experimentStatus'] = $updatedStatus;
                                $newStatus = !empty($model['experimentStatus']) ? $model['experimentStatus'].';'."entry list created" : "entry list created";
                                $model['experimentStatus'] = $newStatus;
                            }
                        }
                    }

                    if($model['notes'] == 'updated entries'){
                        // //check status
                        $newStatus = "entry list created";
                        if(in_array("entry list created", $status)){
                            $newStatus = "entry list created";
                        } else if(in_array("entry list incomplete seed sources", $status)){
                            $newStatus = "entry list incomplete seed sources";
                        }

                        $model['experimentStatus'] = $newStatus;
                    }
                } else {
                    $model['experimentStatus'] = 'draft';
                }
            } else {
                $model['experimentStatus'] = 'draft';
            }

            $status = !empty($model['experimentStatus']) ? $model['experimentStatus'] : 'draft';
            $this->mainExperiment->updateOne($id, ["experimentStatus"=>$status]);
        } else if($action == 'manage-crosses'){
            $configData = isset($_POST['configValues']) && !empty($_POST['configValues']) ? json_decode($_POST['configValues'], true) : null;
      
            $status = explode(';', $model['experimentStatus']);
            $status = array_filter($status);
            
            $apiParams = [
                'apiResourceEndpoint' => 'crosses-search',
                'apiResourceMethod' => 'POST',
                'apiResourceFilter' => ["experimentDbId"=>"$id"],
                'apiResourceSort' => null
            ];

            //Check if the required variables have been filled before experiment status is changed
            $checkExptStatus = $this->mainExperimentModel->genericCheckRequiredData($id,$configData, $apiParams);

            //Check if there's a cross record
            $crossRecordCheck = $this->cross->searchAll(["fields"=>"germplasmCross.id as crossDbId|experiment.id AS experimentDbId","experimentDbId"=>"equals ".$id]);

            if(isset($crossRecordCheck['data'][0]) && $crossRecordCheck['totalCount'] > 0){
              if(!empty($checkExptStatus)){
                //To update the status of the experiment
                if(in_array("cross list created", $status)){
                    
                    $index = array_search('cross list created', $status);
                    
                    if(isset($status[$index])){
                        unset($status[$index]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                    }
                }else{
                    $newStatus = implode(';', $status);
                }
                $model['experimentStatus'] = $newStatus;
              }else{
                  //Reset the experiment statsus
                  if(!in_array('cross list created', $status)){
                      $updatedStatus = implode(';', $status);
                      $model['experimentStatus'] = $updatedStatus;
                      $newStatus = !empty($model['experimentStatus']) ? $model['experimentStatus'].';'."cross list created" : "cross list created";
                      $model['experimentStatus'] = $newStatus;
                  }
              }

              $status = $model['experimentStatus'];
              $this->mainExperimentModel->updateOne($id, ["experimentStatus"=>"$status"]);
            } else {
                //To update the status of the experiment
                $newStatus = implode(';', $status);
                if(in_array("cross list created", $status)){
                    $index = array_search('cross list created', $status);
                    
                    if(isset($status[$index])){
                        unset($status[$index]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = 'entry list created';
                        }
                    } 
                }
                
                $this->mainExperimentModel->updateOne($id, ["experimentStatus"=>"$newStatus"]);
            }
        }else if($action == 'cross-design'){ //planting arrangement

            $isEmptyFlag = false;
            $status = explode(';', $model['experimentStatus']);
            $status = array_filter($status);

            $entryListId = $this->entryListModel->getEntryListId($id);
            $entryIdListKeys = [];
            $entryCount = 0;

            if(!empty($entryListId)){
                $entryRecords = $this->entryModel->searchAll(["fields"=>"entry.id AS entryDbId|entry.entry_list_id AS entryListDbId","entryListDbId"=>"equals $entryListId"], 'limit=1&sort=entryDbId:asc', false);
                $entryCount = $entryRecords['totalCount'];
            }
      
            //Check if there are block records existing
            $params = ["fields"=>"experimentBlock.id as experimentBlockDbId|experimentBlock.entry_list_id_list as entryListIdList|experimentBlock.is_active AS isActive|experiment.id AS experimentDbId|experimentBlock.no_of_ranges as noOfRanges",
                       "experimentDbId" => "$id", "isActive"=>"true", "entryListIdList" => "is not null"];
            $experimentBlockData = $this->experimentBlock->searchAll($params);

            $assignedEntriesToBlocks = [];
            $noOfRangesFlag = false; 
            foreach($experimentBlockData['data'] as $experimentBlocks){
                $blockEntries = ArrayHelper::map($experimentBlocks['entryListIdList'], 'entryDbId','entryDbId');
                $blockEntryKeys = array_keys($blockEntries);
                
                if(!empty($assignedEntriesToBlocks)){
                    $arrayDiff = array_diff($blockEntryKeys, $assignedEntriesToBlocks);
                    if(count($arrayDiff) > 0){
                        $assignedEntriesToBlocks = array_merge($assignedEntriesToBlocks,$arrayDiff);
                    }
                }else{
                    $assignedEntriesToBlocks = $blockEntryKeys;
                }
                if($model['experimentDesignType'] == "Systematic Arrangement" && $experimentBlocks['noOfRanges'] == 0){
                    $noOfRangesFlag = true;
                }
            }
            $assignedEntriesToBlocks = array_unique($assignedEntriesToBlocks);
        
            if($entryRecords['totalCount'] != count($assignedEntriesToBlocks) || $noOfRangesFlag){
                $isEmptyFlag = true;
            }

            if($model['experimentDesignType'] == "Systematic Arrangement") {    // validation using ENTRY_LIST_TIMES_REP experiment data
                $expData = $this->mainExperimentData->getExperimentData($id, ["abbrev" => "equals ENTRY_LIST_TIMES_REP"]);
                if(isset($expData[0]) && !empty(json_decode($expData[0]['dataValue'],true))){
                    if(str_contains($expData[0]['dataValue'], 'null')) {    // there are still reps that are not assigned to a block
                        $isEmptyFlag = true;
                    }
                } else {   //entry list data doesn't exist yet
                    $isEmptyFlag = true;
                }
            }

            //Check if the blocks has sub-blocks and if these blocks have entries
            $params2 = ["fields"=>"experimentBlock.id AS experimentBlockDbId|experiment.id AS experimentDbId|experimentBlock.is_active AS isActive|
                        experimentBlock.block_type AS blockType|experimentBlock.no_of_blocks AS noOfBlocks|experimentBlock.entry_list_id_list AS entryListIdList|
                        parentExperimentBlock.id AS parentExperimentBlockDbId", "experimentDbId" => "$id", "isActive"=>"true", 'blockType'=>'parent'];
            
            $experimentBlockData2 = $this->experimentBlock->searchAll($params2);
            foreach($experimentBlockData2['data'] as $parentData){
                //check for sub-blocks
                if($parentData['noOfBlocks'] != 0){
                    //block with sub-blocks
                    $blockId = $parentData['experimentBlockDbId'];
                    $params = ["fields"=>"experimentBlock.id AS experimentBlockDbId|experiment.id AS experimentDbId|experimentBlock.is_active AS isActive|
                                experimentBlock.block_type AS blockType|experimentBlock.no_of_blocks AS noOfBlocks|experimentBlock.entry_list_id_list AS entryListIdList|
                                parentExperimentBlock.id AS parentExperimentBlockDbId","experimentDbId" => "$id", "isActive"=>"true", 'parentExperimentBlockDbId'=>"$blockId", "entryListIdList"=> "is null"];
                    
                    $subBlockData = $this->experimentBlock->searchAll($params);
                    
                    if(!empty($subBlockData['data'][0])){
                        $isEmptyFlag = true;
                    }
                }else{
                    //parent blocks with no sub-blocks
                    if(empty($parentData['entryListIdList'])){
                        $isEmptyFlag = true;
                    }
                }
            }
            
            if($isEmptyFlag){
                if(in_array("design generated", $status)){
                    
                    $index = array_search('design generated', $status);
                    
                    if(isset($status[$index])){
                        unset($status[$index]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                    }
                }else{
                    
                    $newStatus = implode(';', $status);
                }
                $model['experimentStatus'] = $newStatus;
            }else{
                if(!in_array('design generated', $status)){
                    $updatedStatus = implode(';', $status);
                    $model['experimentStatus'] = $updatedStatus;
                    $newStatus = !empty($model['experimentStatus']) ? $model['experimentStatus'].';'."design generated" : "design generated";
                    $model['experimentStatus'] = $newStatus;
                }
            }

            $status = $model['experimentStatus'];
            $this->mainExperimentModel->updateOne($id, ["experimentStatus"=>"$status"]);
        }
        return json_encode($status);
    }

    /**
     * Save a specific field update
     */
    public function actionSaveRowField(){
        //TODO: For improvement, create a dynamic function for saving row input fields
        $baseModel = isset($_POST['baseModel']) ? $_POST['baseModel'] : 'Entry';

        if(isset($_POST['entryDbId'], $_POST['column'], $_POST['targetTable'], $_POST['value'], $_POST['experimentDbId'])){
            extract($_GET);
            extract($_POST);
            Yii::info('Saving changes in row field with arguments: ' . json_encode(compact('entryDbId', 'column', 'targetTable', 'value', 'experimentDbId')), __METHOD__);

            if(empty($value)){
                $value = "null";
            }
            if($targetTable === 'entry'){
                Entry::updateRecord($entryDbId, [$column => $value]);
                $entry = $this->entryModel->searchAll(['entryDbId'=>'equals '.$entryDbId]);
                $experiment = $this->mainExperiment->getExperiment($experimentDbId);

                if($column == 'entryRole' && $experiment['dataProcessAbbrev'] == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS' && $entry['data'][0]['entryClass'] !== 'imported'){
                    //update planting instruction records
                    Entry::updatePlantingInstruction($experimentDbId, [$entryDbId], $column, $value);
                }

                /**** will be enabled when needed ***/
                // if(in_array($column, ['entryType', 'entryRole'])){
                //     //add notes for BT experiment
                //     $this->mainExperimentModel->addDesignNoteForUpdate($experimentDbId, "updated entries");
                // }
            }
            else{
                //Get entry data id
                $entryData = $this->entryDataModel->getEntryData($entryDbId, ['variableAbbrev' => strtoupper($column)]);

                $entryDataId = isset($entryData[0]['entryDataDbId']) ? $entryData[0]['entryDataDbId'] : 0;
                
                if($entryDataId){//record exists
                    $this->entryDataModel->updateRecord($entryDataId, ['dataValue' => $value]);
                }
                else{//add entry data to the database

                    $variable = $this->mainVariableModel->getVariableByAbbrev(strtoupper($column));

                    if(isset($variable['variableDbId'])){

                        $record['records'][] =
                            [
                                'entryDbId' => $entryDbId,
                                'variableDbId' => $variable['variableDbId'],
                                'dataValue' => $value,
                            ];
                        
                        $this->entryDataModel->create($record);
                    }
                }
            }
        }
        else if(isset($_POST['fields'], $_POST['targetColumn'], $_POST['value'])){
            
            extract($_POST);
            $field = explode('-',$fields);
            $variableAbbrev = $field[0];
            $dataType = $field[1];
            $variableDbId = $field[2];
            $dataId = $field[3];

            $occurrenceRecord = Occurrence::searchAll([
                'occurrenceDbId' => "$dataId"
            ]);
            $occurrence = $occurrenceRecord['data'][0];
            $experiment = Experiment::getOne($occurrence['experimentDbId']);
            
            $model = $experiment['data'];
            $requestData = [
                $targetColumn => "$value",
            ];

            if($dataType == 'identification'){
                $baseModel = "\\app\\models\\".$baseModel;

                if($variableAbbrev == 'OCCURRENCE_NAME'){
                    if(!isset($value) || empty($value)){
                        $requestData[$targetColumn] = $model['experimentName'].'#OCC-'.$occurrence['occurrenceNumber'];
                    }
                }
                else if($variableAbbrev == 'SITE'){
                    $fieldRecord = GeospatialObject::searchAll([
                        'geospatialObjectType' => 'field',
                        'rootGeospatialObjectDbId' => "$value"
                    ]);
                    $fieldDbId = ($fieldRecord['totalCount'] == 1) ? $fieldRecord['data'][0]['geospatialObjectDbId'].'' : 'null';
                    $requestData = [
                        $targetColumn => "$value",
                        'fieldDbId' => $fieldDbId,
                    ];
                }
                else if($variableAbbrev == 'FIELD'){
                    $value = !empty($value) ? "$value" : 'null';
                    $fieldRecord = GeospatialObject::searchAll([
                        'geospatialObjectDbId' => "$value"
                    ]);
                    $siteDbId = $fieldRecord['data']['0']['rootGeospatialObjectDbId'];
                    $requestData = [
                        $targetColumn => $value,
                    ];
                    if(isset($siteDbId)){
                        $requestData['siteDbId'] = "$siteDbId";
                    }
                }

                $baseModel::updateOne($dataId, $requestData);
            }
            else{// fetch records in occurrence data
                $baseModel = "\\app\\models\\$baseModel".'Data';

                $occurrenceDataRecord = $baseModel::searchAll([
                    'occurrenceDbId' => "$dataId",
                    'variableDbId' => "$variableDbId"
                ]);

                if($occurrenceDataRecord['totalCount'] > 0){//record exists
                    $requestData = [
                        "dataValue" => $value,
                    ];
                    $occurrenceDataDbId = $occurrenceDataRecord['data'][0]['occurrenceDataDbId'];
                    $updatedRecord = $baseModel::updateOne($occurrenceDataDbId, $requestData);
                }
                else{//insert new record
                    $requestData['records'][] = [
                        "occurrenceDbId" => "$dataId",
                        "variableDbId" => "$variableDbId",
                        "dataValue" => "$value",
                    ];
                    $baseModel::create($requestData);
                }
            }
        }
        else{
            Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> Failed to save the changes. Kindly report this in the support portal.');
        }
    }

    /**
     * Renders view for single selection of package
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     *
     */
    public function actionRenderPackages($id, $program, $processId){

        if(isset($_POST)){

            $entryId = $_POST['entryId'];
            $germplasmId = $_POST['germplasmId'];

            //get package data
            $filters = [
                "germplasmDbId" => "equals ".$germplasmId
            ];
            $packagesArray = $this->packageModel->getPackages([$germplasmId], ['programFilter'=>$program]);
            
            $entry = Entry::searchRecords(['entryDbId'=>"equals ".$entryId]);

            // get the package of the entry if any
            $currentPackageDbId = NULL;

            if(!empty($entry[0]['packageDbId'])){

                $currentPackageDbId = $entry[0]['packageDbId'];
                $packageDbIds = array_column($packagesArray, 'packageDbId');

                if(!in_array($currentPackageDbId, $packageDbIds)){
                  
                    $currentPackage = $this->packageModel->getPackages([$currentPackageDbId], ['programFilter'=>"equals ".$program], 'packageDbId');
                  
                    $packagesArray[] = $currentPackage[0];
                }
            }

            $data = new ArrayDataProvider([
                'key'=>'packageDbId',
                'pagination'=>false,
                'allModels' => $packagesArray,
                'sort' => [
                    'attributes' => ['seedDbId', 'germplasmDbId', 'designation','programCode', 'packageCount', 'packageDbId'],
                    'defaultOrder' => [
                        'packageDbId'=>SORT_DESC
                    ]
                ]
            ]);

            $htmlData = $this->renderPartial('single_packages',[
                'data' => $data,
                'id' => $id,
                'program' => $program,
                'currentPackageDbId' => $currentPackageDbId,
                'processId' => $processId,
                'entryId' => $entryId,
                'entry' => $entry
            ],true,false);

            return json_encode([
                'htmlData' => $htmlData
            ]);
        }
    }

    /**
     * Save selected package for the entry/entries
     * Similar to CreateController::actionSavePackage but this retains the entry list browser state
     *
     */
    public function actionSaveSinglePackage(){

        extract($_GET);

        if(isset($_POST)){

            extract($_POST);

            //update seed_id of the entry
            Entry::updateRecord($entryId, ["seedDbId"=>"$selectedSeedId", "packageDbId"=>"$selectedPackageId"]);
            $this->plantingInstruction->updateForEntry($entryId, ["seedDbId"=>"$selectedSeedId", "packageDbId"=>"$selectedPackageId"]);

        }

        $prevCols = Yii::$app->session['EntryListSearchSession'.$id];
        $params_temp = '';
        $pagesParam = '';

        if(!empty($prevCols)){
            foreach ($prevCols as $key => $value) {
                $entities = trim(urlencode('EntryListSearch'.'['.$key.']'));
                $entity_value = $entities.'='.$value;
                $params_temp = empty($params_temp) ?  $entity_value : $params_temp. '&'.$entity_value;
            }
        }


        if(!empty($_POST['page']) && !empty($_POST['perPage'])){
            $pagesParam = '&page='.$_POST['page'].'&per-page='.$_POST['perPage'];
        }

        if(!empty($_POST['dp1_page']) && !empty($_POST['dp1_perPage'])){
            $pagesParam = !empty($pagesParam) ? $pagesParam.'&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'] : '&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'];
        }

        return json_encode('success');
        
    }

    /**
     * Renders view for saved list
     */
    public function actionRenderSavedList(){

        $param = ["type"=>"equals germplasm|equals package","memberCount"=>"not equals 0"];
        $listData = $this->platformList->searchList($param, true);
        
        $dataProvider = new ArrayDataProvider([
            'key'=>'listDbId',
            'allModels' => $listData,
            'sort' => [
                'attributes' => ['listDbId', 'abbrev', 'name', 'type'],
                'defaultOrder' => [
                    'listDbId'=>SORT_DESC
                ]
            ],
            'pagination' => false,
       ]);
        $htmlData = $this->renderPartial('saved_list',[
            'data' => $dataProvider,
        ],true,false);

        return json_encode($htmlData);
    }

    /**
     * Adds saved list members to the entry list
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId Experiment type identifier
     */
    public function actionImportSavedList($id,$program,$processId){
        extract($_GET);
        $success = '';
        if(isset($_POST)){
            $savedListId = $_POST['savedListId'];
            $configData = $this->mainExperimentModel->retrieveConfiguration('specify-entry-list', $program, $id, $processId);
            $this->mainExperimentModel->populateEntryViaList($_POST['experimentId'], $savedListId, $configData,$program);
            $this->platformList->searchList(["listDbId"=>"equals ".$savedListId]);
            $success = 'success';
        }
        return json_encode($success);
    }

    /**
     * Remove all or selected entries from the entry list
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     *
     */
    public function actionRemoveEntries($id, $program, $processId){

        $model = $this->mainExperiment->getExperiment($id);
        $message = '<i class="fa fa-times"></i> There seems to be a problem with deleting entries.';
        if(!empty($model)){
            extract($_POST);

            if($type == 'all'){
                $entryListId = $this->entryListModel->getEntryListId($id);
                $entryParams = ["fields"=>"entry.id AS entryDbId|entry.entry_list_id AS entryListDbId","entryListDbId"=>"equals ".$entryListId];
                $entries = $this->entryModel->searchAll($entryParams);
               
                if($entries['totalCount'] > 0){
                    $entryIds = array_column($entries['data'], 'entryDbId');
                }
            }
            $entryIds = (gettype($entryIds)== 'string') ? json_decode($entryIds, true) : $entryIds;
            $entryCount = !empty($entryIds) ? count($entryIds) : 0;
            $message = '<i class="fa fa-times"></i> There seems to be a problem with deleting entries.';
            $usedEntryIds = [];
            $designFlag = false;

            //check if entries are used in design
            if(strpos($model['dataProcessAbbrev'], '_TRIAL_DATA_PROCESS') !== false){

                $ref = $this->plotModel->searchAll(['entryDbId'=> 'equals '.implode('|equals ', $entryIds),
                    'experimentDbId'=> "equals $id",
                ], 'limit=1&sort=entryDbId:desc', false);
                if($ref['totalCount'] > 0){
                    //add notes in experiment notes to remove the design
                    $designFlag = true;
                    $usedEntryIds = $entryIds;
                }
                $tab = 'experiment design. Kindly delete the generated design first';

            }
            else{ //nursery
                $tab = 'crosses';
                $parentActAbbrev = str_replace('_DATA_PROCESS', '', $model['dataProcessAbbrev']);
                $crossesActAbbrev = $parentActAbbrev.'_CROSSES_ACT';
                $isCrossedArr = $this->mainItemModel->getAll("abbrev=$crossesActAbbrev");
                $isCrossed = isset($isCrossedArr['data'][0]) ? true : false;

                $crossUsedEntryIds = $paUsedEntryIds = [];
                //check if entries are used in crosses
                if($isCrossed){
                    $ref = $this->crossParent->searchAll([
                        'entryDbId'=> 'equals '.implode('|equals ', $entryIds),
                        'distinctOn'=> 'entryDbId',
                        'experimentDbId'=> "equals $id",
                    ]);
                    $crossUsedEntryIds = array_column($ref['data'], 'entryDbId');
                }
                $ref = $this->experimentBlock->searchAll(["experimentDbId" => "equals $id"]);
                $refEntryIds = array_column($ref['data'], 'entryListIdList');
                foreach($refEntryIds as $refEntryId){
                    if(!empty($refEntryId)){
                        $paUsedEntryIds = array_merge($paUsedEntryIds, array_column($refEntryId,'entryDbId'));
                    } 
                }

                if($model['dataProcessAbbrev'] != 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
                    $usedEntryIds = $crossUsedEntryIds;
                    if(count($paUsedEntryIds) > 0){
                        //add notes in experiment notes to remove the design
                        $this->mainExperiment->updateOne($id, ["notes"=>"updatedEntryList"]);
                        $designFlag = true;
                    }
                } else {
                    $tab = 'crosses/planting arrangement';
                    $usedEntryIds = array_unique(array_merge($crossUsedEntryIds, $paUsedEntryIds));
                }

            }

            $unusedEntries = !empty($entryIds) ? array_diff($entryIds, $usedEntryIds) : null;
            if(!empty($entryIds) && count($usedEntryIds) > 0){
                $unusedEntries = array_diff($entryIds, $usedEntryIds);
            }else if(!empty($entryIds) && count($usedEntryIds) == 0){
                $unusedEntries = $entryIds;
            }else{
                $unusedEntries = null;
            }

            $diffCount = !empty($unusedEntries) ? count($unusedEntries) : 0;
            
            if($diffCount == 0){//all entries have already been used
                $used = ($type == 'all') ? 'All':'Selected';
                if($type == 'all'){
                    $used = 'All';
                }else if($type == 'selected'){
                    $used = 'Selected';
                }else if($type == 'current-page'){
                    $used = 'Selected in Page';
                }
             
                $message = "<i class='material-icons orange-text left'>warning</i> Unable to remove. $used entries have already been used in $tab.";
            }
            else if((!empty($unusedEntries) || count($unusedEntries) > 0) && $diffCount > 0){

                $chunkLimit = 100;
                //delete entry data first, if any
                $entryData = Yii::$app->api->getParsedResponse('POST', 'experiments/'.$id.'/entry-data-search', json_encode([
                    'entryDbId'=> 'equals '.implode('|equals ', $unusedEntries),
                ]), '',true)['data'];
                $entryDataIds = array_column($entryData, 'entryDataDbId');
                if(!empty($entryDataIds)){
                    $entryDataChunk = array_chunk($entryDataIds, $chunkLimit);
                    foreach($entryDataChunk as $chunkIds){
                        $deletedEntryData = $this->entryDataModel->deleteMany($chunkIds);
                    }
                }
                
                if((isset($deletedEntryData['success']) && $deletedEntryData['success']) || empty($entryDataIds)){
                    
                    $userModel = new User();
                    $userId = $userModel->getUserId();
                    $deleteAddtlParams = ["entityDbId"=>$id,"entity"=>"EXPERIMENT", "endpointEntity"=>'ENTRY',"application"=>"EXPERIMENT_CREATION"];
        
                    $limit = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'deleteEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'deleteEntries') : 500;
        
                    if($diffCount > $limit){// use processor

                        $deletedEntries = $this->entryModel->deleteMany($unusedEntries,true, $deleteAddtlParams);
                        
                        $bgStatus = isset($deletedEntries['data'][0]) ? $this->mainExperiment->formatBackgroundWorkerStatus($deletedEntries['data'][0]['backgroundJobDbId']) : null;
                    
                        if($bgStatus == 'done'){
                            if($diffCount !== $entryCount){
                                $used = $entryCount - $diffCount;
                                $message = "<i class='material-icons green-text left'>check</i> Total of $diffCount entries have been deleted. The other $used entries have already been used in $tab.";
                            }
                            else{
                                $message = "<i class='material-icons green-text left'>check</i> Total of $diffCount entries have been deleted.";
                            }
                        }elseif(!empty($bgStatus)){
                            $experimentModel = $this->mainExperiment->searchAll(["fields"=>"experiment.id AS experimentDbId| program.program_code AS programCode",'experimentDbId'=>"equals ".$id]);   

                            $experimentStatus = isset($experimentModel['data'][0]['experimentStatus']) ? $experimentModel['data'][0]['experimentStatus'] : '';
              
                            if(!empty($experimentStatus)){
                                $status = explode(';',$experimentStatus);
                                $status = array_filter($status);
                            }else{
                                $status = [];
                            }

                            $updatedStatus = ['draft','entry list created'];
                            $newStatus = !empty($updatedStatus) ? implode(';',$updatedStatus).';'. $bgStatus : $bgStatus;
                            $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);

                            $this->redirect(['create/index?program='.$model['programCode'].'&id='.$id]);
                        }

                    }
                    else{// use broker
                        $chunkLimit = 300;
                        $entryChunk = array_chunk($unusedEntries, $chunkLimit);
                        foreach($entryChunk as $chunkIds){
                            $deletedEntries = $this->entryModel->deleteMany($chunkIds);
                        }
            
                        if($deletedEntries['success']){
                            if($diffCount !== $entryCount){
                                $used = $entryCount - $diffCount;
                                $message = "<i class='material-icons green-text left'>check</i> Total of $diffCount entries have been deleted. The other $used entries have already been used in $tab.";
                            }
                            else{
                                $message = "<i class='material-icons green-text left'>check</i> Total of $diffCount entries have been deleted.";
                            }

                            $experimentModel = $this->mainExperiment->searchAll(["fields"=>"experiment.id AS experimentDbId| program.program_code AS programCode",'experimentDbId'=>"equals ".$id]);   

                            $experimentStatus = isset($experimentModel['data'][0]['experimentStatus']) ? $experimentModel['data'][0]['experimentStatus'] : '';
              
                            if(!empty($experimentStatus)){
                                $status = explode(';',$experimentStatus);
                                $status = array_filter($status);
                            }else{
                                $status = [];
                            }

                            $newStatus = 'draft;entry list created';
                            $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);
                        }
                    }
                }
                if($designFlag){
                    $message .= "<b>The design for this experiment needs to be updated.</b>";
                } 
                //remove selected entries
                $userId = $this->userModel->getUserId();
                $selectedIdName = "ec_$userId"."_selected_entry_ids_$id";
                if(isset($_SESSION[$selectedIdName])){
                    unset($_SESSION[$selectedIdName]);
                }
            }
        }
        return json_encode($message);

    }

    /**
     * Revert status of the experiment if entry list was changed; delete all created records
     *
     * @param integer $id record id of the experiment
     */
    public function afterChangeEntryList($id){

        $experimentRecord = $this->mainExperiment->getExperiment($id);
        
        $experimentStatus = !empty($experimentRecord) ? $experimentRecord['experimentStatus'] : "";
        //remove design created status
        $status = explode(';', $experimentStatus);

        if (($key = array_search('design generated', $status)) !== false) {
            unset($status[$key]);
            $model['experimentStatus'] = implode(';', $status);
            $this->mainExperiment->updateOne($id, ["experimentStatus"=>$model['experimentStatus']]);
        }
    }

    /**
     * Updates the information of the record
     *
     * @param string $type name of the field to be updated
     * @param integer $id record id of the experiment
     * @param integer $experimentGroup record id of the experiment group
     *
     */
    public function actionUpdateRecordColumn($type, $id, $expGrp = NULL, $experimentId = NULL, $variable=NULL, $dataType = NULL, $minVal = NULL, $maxVal = NULL){

        if(isset($_POST)){
            $column = strtolower($type);

            //Check variable if column in there entry list table
            $checkColumn = Experiment::checkIfColumnExist('operational','entry_list',$column);
            $entryList = EntryList::findOne($id);

            if($checkColumn){

                if($variable == 'entry_type'){

                    $entryList->entry_type = ucwords($_POST[$column.'_value']);
                    //check if design is augmented; remove saved settings in the design part
                    $designVariable = Variable::find()->where(['abbrev'=>'DESIGN'])->one()['id'];
                    $designValue = ExperimentData::find()->where(['experiment_id'=>$experimentId,'variable_id'=>$designVariable,'is_void'=>'false'])->one()['value'];//volume
                    if(strtolower($designValue) == 'augmented rcbd'){
                        PlanTemplate::deleteAll("entity_id = ".$experimentId." and entity_type='experiment'");
                        ExperimentData::deleteAll('experiment_id = '.$experimentId);
                    }
                }else{
                    $entryList->$column = $_POST[$column.'_value'];
                }
                $entryList->save();
            }else{

                $variableCheck = Variable::find()->where(['abbrev'=>strtoupper($variable), 'is_void'=>false])->one();

                //Update the existing entry list data if any

                ExperimentModel::updateEntryListData($experimentId, $id, $variableCheck->id, $_POST[$column.'_value']);

            }
            return json_encode('success');
        }
    }

    /**
     * Updates the information of the record
     *
     * @param string $type name of the field to be updated
     * @param integer $id record id of the experiment
     * @param integer $experimentGroup record id of the experiment group
     *
     */
    public function actionUpdateRecordColumnExptGrp($type, $id, $expGrp = NULL, $experimentId = NULL, $variable=NULL, $dataType = NULL, $minVal = NULL, $maxVal = NULL){

        if(isset($_POST)){

            if(in_array($type, ['experiment_role'])){
                $experimentGroupExperiment = ExperimentGroupExperiment::find()->where(['experiment_group_id'=>(int) $expGrp, 'experiment_id'=>(int) $id, 'is_void'=>false])->one();

                if($type == 'experiment_role'){

                    $experimentGroupExperiment->experiment_role = $_POST['experiment_role_value'];
                }

                $experimentGroupExperiment->save();

            }
            else if(in_array($type, ['experiment_group_name'])){

                $experimentGroup = ExperimentGroup::findOne($id);

                if($type == 'experiment_group_name'){

                    $experimentGroup->name = $_POST['experiment_group_name_value'];
                }

                $experimentGroup->save();

            }
            return json_encode('success');
        }
    }

    /**
     * Get information specified in seed lot defaults activity -jp.ramos
     *
     * @param integer $experimentId record id of the experiment
     */
    public function getSeedLotDefaults($experimentId){

        $volumeVariable = Variable::find()->where(['abbrev'=>'VOLUME'])->one()['id'];
        $volumeValueObject = ExperimentData::find()->where(['experiment_id'=>$experimentId,'variable_id'=>$volumeVariable,'is_void'=>'false'])->one();//volume
        $volume = !empty($volumeValueObject) ? $volumeValueObject->value : '0';

        $unitVariable = Variable::find()->where(['abbrev'=>'UNIT'])->one()['id'];
        $unitValueObject = ExperimentData::find()->where(array('experiment_id'=>$experimentId,'variable_id'=>$unitVariable,'is_void'=>'false'))->one();//unit
        $unit = !empty($unitValueObject) ? $unitValueObject->value : 'g';

        $volumeOption = Variable::find()->where(array('abbrev'=>'VOLUME_OPTIONS'))->one();
        $volumeId = !empty($volumeOption) ? $volumeOption->id : NULL;

        $optionsValueObject = ExperimentData::find()->where(array('experiment_id'=>$experimentId,'variable_id'=>$volumeId,'is_void'=>'false'))->one();//volume options
        $option = !empty($optionsValueObject) ? $optionsValueObject->value : 'per entry';

        return array('vol' => $volume,'unit' => $unit,'opt' => $option);

    }

    /**
     * Save filters
     * @param integer $id Experiment ID
     */
    public function actionSaveFilters($id)
    {
        $filters = isset($_POST['filters']) ? json_decode($_POST['filters'], true) : '';
        //Set to null the selected entries to be added to a block
         Yii::$app->session->set('selectedItems-design-'.$id, null);

        Yii::$app->session->set('design-entries-filters-' . $id, $filters);
    }

    /**
     * Renders view for the design step of the experiment creation
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     *
     */
    public function actionSpecifyDesign($program, $id, $processId){
        
        //check if there's saved across env design variable, if none proceed with old UI
        $designRecord = $this->mainVariableModel->getVariableByAbbrev('ACROSS_ENV_DESIGN');
       
        $designVariable = $designRecord['variableDbId'] ?? null;

        $acrossEnvDesign = $this->mainExperimentData->getExperimentData($id,[
            'variableDbId' => 'equals '.$designVariable
        ]);
        
        $designUiSwitch = $this->designModel->getDesignUiInfo($id);
        $experiment = $this->mainExperiment->getExperiment($id);
        
        if(empty($acrossEnvDesign) || count($acrossEnvDesign) == 0){
            if($designUiSwitch == 'new'){
                //delete created templates
                $planTemplateArray = $this->planTemplate->searchAll(['entityType'=>'equals experiment', 'entityDbId'=> "equals $id"]);
                $planTemplateIds = array_column($planTemplateArray['data'],'planTemplateDbId');
                $deleteTemplates = $this->planTemplate->deleteMany($planTemplateIds);

                $designInfo1 = $this->generateInfoForOldDesign($id, $program, $processId, true);
                $designInfo = $this->designModel->generateDesignInfo($id, $program, $processId, 'none');

                $analysisModel = new AnalysisModel();
                
                //limited first since the other designs will be migrated
                if($experiment['remarks'] == 'upload'){
                    $designValue = 'Unspecified';
                }
                
                if($experiment['stageCode'] == 'OFT'){
                    $availDesigns = $analysisModel->getDesignScripts(null, $experiment['stageCode']);
                } else $availDesigns = $analysisModel->getDesignScripts();
                
                $designInfo1['designArray'] = $availDesigns['designArray'];
                $designInfo1['designInfo']= json_encode($availDesigns['designInfo']);
                $designInfo1['designDesc'] = $availDesigns['designDesc'];

                $designInfo['designUiSwitch'] = $designUiSwitch;
                return $this->render('/design/normal_designs',array_merge($designInfo, $designInfo1));
            } else {
                $designInfo = $this->generateInfoForOldDesign($id, $program, $processId);
                $designInfo['designUiSwitch'] = $designUiSwitch;
                return $this->render('design/design', $designInfo);
            }
        } else {
            $designInfo1 = $this->generateInfoForOldDesign($id, $program, $processId, true);
            $designInfo = $this->designModel->generateDesignInfo($id, $program, $processId);

            $analysisModel = new AnalysisModel();
            
            //limited first since the other designs will be migrated
            $designValue = 'Partially Replicated';
            if($experiment['remarks'] == 'upload'){
                $designValue = 'Unspecified';
            }
            $availDesigns = $analysisModel->getDesigns($designValue);
            
            $designInfo1['designArray'] = $availDesigns['designArray'];
            $designInfo1['designInfo']= json_encode($availDesigns['designInfo']);
            $designInfo1['designDesc'] = $availDesigns['designDesc'];
            $designInfo['designUiSwitch'] = $designUiSwitch;
            
            return $this->render('/design/design',array_merge($designInfo, $designInfo1));
        }
    }

    /**
     * Prepare data for old design UI
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId record id of the data process
     * @param boolean $parameterSet flag for new UI
     * 
     * */
    public function generateInfoForOldDesign($id, $program, $processId, $parameterSet = false){
        $model = $this->mainExperiment->getExperiment($id);

        $this->formModel->checkDataProcess($processId);
        
        $entryListId = $this->entryListModel->getEntryListId($id);
        $entryListCheckArray =  $this->entryModel->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals check'], '', true);
        $entryListEntryArray = $this->entryModel->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals test'], 'limit=1', false);
        $entryListTotalArray = $this->entryModel->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType','entryListDbId'=>"equals $entryListId"], 'limit=1', false);

        $configData = $this->variableComponent->getDesignConfig();

        $entryInfo  = array();
        
        $entryInfo['check'] = $entryListCheckArray['totalCount'];
        $entryInfo['test'] = $entryListEntryArray['totalCount'];
        $entryInfo['total'] = $entryListTotalArray['totalCount'];
        $designFields = array();
        $layoutFields = array();
        $rules = array();
        $paramValues = array();

        //POST-search call
        if($parameterSet){ //filter plan template records for new UI
            $param = ['entityType'=>'equals parameter_sets', 'entityDbId'=> "equals $id", 'status' => 'incomplete'];
        } else {
            $param = ['entityType'=>'equals experiment', 'entityDbId'=> "equals $id"];
        }
        $jsonInput = PlanTemplate::getPlanTemplateByEntity($param);
        
        $designVariable = $this->mainVariableModel->getVariableByAbbrev('DESIGN');
        
        $experimentDesign = isset($model['experimentDesignType'])? $model['experimentDesignType']:NULL;
        $designAF = '';
        $parameter = array();
        $fileList = NULL;
        $showLayout = FALSE;
        $planStatus = NULL;
        $uploaded = false;
        $testList = false;
        $checkList = false;
        $filters = [];
        $uiTemplateInfo = [];
        $dataProvider = new \yii\data\ArrayDataProvider([]);
        $gridClass = '';
        $generateLayout = "false";
        $generateLayoutInc = "false";
        $uiTemplate = 'none';
        $storedItems = [];
        $incompleteSet = 0;

        if(isset($jsonInput['mandatoryInfo']) && $jsonInput != NULL){

            $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);
            $parameter = isset($jsonArr['design_ui']) ? $jsonArr['design_ui'] : NULL;
            $showLayout = isset($jsonArr['design_ui']['show_layout']) ? $jsonArr['design_ui']['show_layout'] : FALSE;
            $designAF = isset($jsonArr['design_id']) ? $jsonArr['design_id'] : NULL;

            if(isset($jsonArr['totalOccurrences'])){
                $uploaded = true;
            }

            $planStatus = $jsonInput['status'];

            if($parameterSet){
                $incompleteSet = 1;
            }
        } else {
            $generateLayout = "false";
        }

        $status = explode(';', $model['experimentStatus']);
        $entryStatus = 'entry list created';
        //To update the status of the experiment
        if(in_array("entry list created", $status)){
            $entryStatus = "entry list created";
            $index = array_search('entry list created', $status);
            
            if(isset($status[$index])){
                unset($status[$index]);
                
                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
            }
        }else if(in_array("entry list incomplete seed sources", $status)){
            $entryStatus = "entry list incomplete seed sources";
        }
        if(isset($jsonInput['status']) && (in_array($jsonInput['status'], ['randomized', 'uploaded']))){
            if($jsonInput['status'] == 'uploaded'){
                $uploaded = true;
            }
            if(in_array('design requested', $status)){

                $index = array_search('design requested', $status);
                $status[$index] = 'design generated';
                $experimentStatus = implode(';', $status); //should be changed
            } else if($model['experimentStatus'] == 'entry list created' || $model['experimentStatus'] == 'entry list incomplete seed sources'){
                $experimentStatus = $entryStatus;
            }
            else{
                $experimentStatus = !in_array('design generated', $status) ? ($model['experimentStatus'].';design generated') : $model['experimentStatus'];
            }
            //Update experiment design
            $this->mainExperiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
        }
        else if(isset($jsonInput['status']) && (in_array($jsonInput['status'],['randomization in progress','upload in progress']))){
            $experimentStatus = $entryStatus.';design requested';
            
            $this->mainExperiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
            
            if($jsonInput['status'] == 'upload in progress'){
                $uploaded = true;
            }
        }
       
        if(!$uploaded){
            if(isset($parameter['randomization'])){
                foreach($parameter['randomization'] as $key=>$var){
                   
                    if($var['code'] == 'genLayout'){
                        $generateLayoutInc = "true";
                        $generateLayout = isset($var['value']) ? $var['value']:$var['ui']['defaultValue'];
                    }
                    if($var['code'] == 'checkList'){
                        $checkList = true;
                    }else if($var['code'] == 'testList'){
                        $testList = true;
                    }
                    if(isset($var['ui']['visible']) && $var['ui']['visible']){
                        $designFld = $this->variableComponent->generateDesignField($var, 'randomization', $key);
                        $designFields[] = $designFld;
                        $paramValues[$var['code']] = $designFld['defaultValue'];
                    }
                }
            }

            if(isset($parameter['layout'])){

                foreach($parameter['layout'] as $key=>$var){
                    if(isset($var['ui']['visible']) && $var['ui']['visible']){
                        $layoutFld = $this->variableComponent->generateDesignField($var, 'layout', $key);
                        $layoutFields[] = $layoutFld;
                        $paramValues[$var['code']] = $layoutFld['defaultValue'];
                    }
                }
                if(count($parameter['layout']) == 0){
                    $showLayout = FALSE;
                    $generateLayout = "false";
                }
            }

            if(isset($parameter['rules'])){
                $rules = $parameter['rules'];
            }

            
            if($testList){
                $uiTemplate = 'testList';
            } else if($checkList){
                $uiTemplate = 'checkList';
            }
            if($uiTemplate != 'none'){
                
                $uiTemplateInfo = isset($parameter['groups']) ? $parameter['groups'] : [];
                if($uiTemplate == 'checkList' &&  empty($uiTemplateInfo)){
                    $arrayRep = array_fill(0, $entryInfo['check'], 2);
                    $checkEntryDbIds = array_column($entryListCheckArray['data'], 'entryDbId');
                    $checkRep = array_combine($checkEntryDbIds, $arrayRep);
                    $uiTemplateInfo = $checkRep;
                    $jsonArr['design_ui']['groups'] = $uiTemplateInfo;
                    $this->planTemplate->updateOne($jsonInput['planTemplateDbId'], ['mandatoryInfo' => json_encode($jsonArr)]);
                } elseif($uiTemplate == 'checkList' &&  !empty($uiTemplateInfo)){
                    $entryDbIds = array_keys($uiTemplateInfo);
                    $checkEntryDbIds = array_column($entryListCheckArray['data'], 'entryDbId');
                    $noReps = array_values(array_diff($checkEntryDbIds, $entryDbIds));
                    if(!empty($noReps)){
                        //update entry role to spatial checks

                        $this->plantingArrangement->updateEntryRole(null, "spatial check", $noReps);
                        $arrayRep = array_fill(0, count($noReps), 2);
                        $checkRep = array_combine($noReps, $arrayRep);
                        $uiTemplateInfo = array_merge($uiTemplateInfo, $checkRep);
                        $jsonArr['design_ui']['groups'] = $uiTemplateInfo;
                        $this->planTemplate->updateOne($jsonInput['planTemplateDbId'], ['mandatoryInfo' => json_encode($jsonArr)]);
                    }
                }

                $userId = $this->userModel->getUserId();
                $filteredIdName = "ec"."_filtered_entry_ids_$id";
                $selectedIdName = "ec"."_selected_entry_ids_$id";
                
                //destroy selected items session
                if(((isset($_GET['reset']) && $_GET['reset'] == 'hard_reset' && !isset($_GET['grid-entry-list-grid-page'])) ||
                    (!isset($_GET['reset']) && !isset($_GET['grid-entry-list-grid-page']))) && isset($_SESSION[$selectedIdName])){
                    unset($_SESSION[$selectedIdName]);
                    if(isset($_SESSION[$filteredIdName])){
                        unset($_SESSION[$filteredIdName]);
                    }
                }

                // get params for data browser
                $paramPage = '';
                $paramSort = '';

                if (isset($_GET['sort'])) {
                    $paramSort = 'sort=';
                    $sortParams = $_GET['sort'];
                    $sortParams = explode('|', $sortParams);
                    $countParam = count($sortParams);
                    $currParam = 1;

                    $sortOrder = array();

                    foreach ($sortParams as $column) {
                        if ($column[0] == '-') {
                            $sortOrder[substr($column, 1)] = SORT_DESC;
                            $paramSort = $paramSort . substr($column, 1) . ':desc';
                        } else {
                            $sortOrder[$column] = SORT_ASC;
                            $paramSort = $paramSort . $column;
                        }
                        if ($currParam < $countParam) {
                            $paramSort = $paramSort . '|';
                        }
                        $currParam += 1;
                    }
                    $paramSort = '&'.$paramSort;
                }

                if (isset($_GET['dp-1-page'])){
                    $paramPage = '&page=' . $_GET['dp-1-page'];
                } else if (isset($_GET['page'])) {
                    $paramPage = '&page=' . $_GET['page'];
                } else if (isset($_GET['grid-entry-list-grid-page'])) {
                    $paramPage = '&page=' . $_GET['grid-entry-list-grid-page'];
                }
                $paramLimit = '?limit='.getenv('CB_DATA_BROWSER_DEFAULT_PAGE_SIZE');

                $pageParams = $paramLimit.$paramPage;

                // append sorting condition
                $pageParams .= empty($pageParams) ? '?'.$paramSort : $paramSort;

                $urlParams = Yii::$app->request->queryParams;
               
                if (Yii::$app->session->get('design-entries-filters-' . $id) !== null && $uiTemplate == 'testList') {
                    $filters = Yii::$app->session->get('design-entries-filters-' . $id);
                    Yii::$app->session->set('design-filtersForEntries-'.$id, $filters); 
                    if(!empty($uiTemplateInfo)){
                        $entryIdFilters = [];
                        foreach ($uiTemplateInfo as $info) {
                            $entryIdFilters = array_merge($entryIdFilters, $info['entryIds']);
                        }
                        if(!empty($entryIdFilters)){
                            $filters['notIncludedEntries'] = $entryIdFilters;
                        }
                    }
                    $dataProvider = $this->plantingArrangement->searchEntries($id, [], $filters, $urlParams, $this->entrySearchModel,$pageParams);

                    $gridClass = '';
                } else {
                    $storedFilter = 'empty';
                    if(!empty($uiTemplateInfo) && $uiTemplate == 'testList'){
                        $entryIdFilters = [];
                        foreach ($uiTemplateInfo as $info) {
                            $entryIdFilters = array_merge($entryIdFilters, $info['entryIds']);
                        }
                        if(!empty($entryIdFilters)){
                            $storedFilter = [];
                            $storedFilter['notIncludedEntries'] = $entryIdFilters;
                        }
                    } else if($uiTemplate == 'checkList'){
                        $storedFilter = [];
                        $storedFilter['entryType'] = ['check'];
                    }
                    
                    $dataProvider = $this->plantingArrangement->searchEntries($id, [], $storedFilter, $urlParams, $this->entrySearchModel,$pageParams);
                    if($uiTemplate == 'checkList'){
                    }
                }

                $getItemDataProc = $this->mainItemModel->searchItemRecord(['itemDbId' => "equals $processId"]);
                $dataProcessName = $getItemDataProc['data']['abbrev'];

                $storedItems = Yii::$app->session->get($selectedIdName);
            }   
        } else {
            $designFields = $jsonArr;
            $generateLayout = "false";
        }
        
        $designArray = [];
        $designInfo = []; 
        $designDesc = [];
        $analysisModel = new AnalysisModel();
        $designSG = $analysisModel->getDesignCatalogue();
       
        if(!isset($designSG['data']['designModel']['children'])){
            Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning"></i> The design catalogue cannot be accessed. Please try to refresh the page.');
            $designArray = [];
            $jsonInput = NULL;
            $designFields = [];
            $layoutFields = [];
            $rules = [];
        } else {
            //generate template json for selected design
            $designRecord = $this->mainVariableModel->getVariableByAbbrev('DESIGN');
            $designVariable = $designRecord['variableDbId'];

            //get scale values for design
            
            $scaleRecords = $this->mainScaleModel->getVariableScale($designVariable)['scaleValues'] ?? [];
            $scaleValues = array_column($scaleRecords, 'value');
            
            $scaleValuesKey = array_map('strtoupper', $scaleValues);

            $designList = $designSG['data']['designModel']['children'];
            usort($designList,function($first,$second){
              return strcmp($first['name'], $second['name']);
            }); 

            $temporaryDesigns = ["Incomplete Block Design","On-Farm Trial RCBD"];

            $designDesc = [];
            foreach($designList as $design){
                $tempString = explode("(", $design['name']);

                $searchword = strtoupper(trim($tempString[0]));

                $matches = array_filter($scaleValuesKey, function($var) use ($searchword) { return preg_match("/($searchword)/i", $var); });


                if(!empty($matches) && count($matches) > 0){
                    $designArray[$design['id']] = $design['variableLabel'];
                    $designInfo[$design['id']]  = [
                        'designCode' => $design['code'],
                        'designName' => $design['name']
                    ];

                    $designDesc[$design['id']] = $design['description'];
                }
            }

            //Add Unspecified design
            $designArray[0] = "Unspecified";
            $designInfo[0] = [
                'designCode' => 'unspecified',
                'designName' => 'Unspecified'
            ];
            $designDesc[0] = 'This design is populated currently only via upload.';

            uasort($designArray,function($first,$second){
              return strcmp($first, $second);
            });
        }
        
        //check if there are plot records
        $occurrences = $this->occurrenceModel->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "equals $id"]);
             
        $firstOccurrenceId = isset($occurrences['data'][0]['occurrenceDbId']) ? $occurrences['data'][0]['occurrenceDbId'] : 0;


       
        $plotRecords = ($firstOccurrenceId != 0) ? $this->occurrenceModel->searchAllPlots($firstOccurrenceId, null, 'limit=1', false)['totalCount'] : 0;

        //create entry info data provider
        $entryInfoDataProvider = new ArrayDataProvider([
          'key'=>'total',
          'allModels' => [$entryInfo]
        ]);

        $userModel = new User();
        $userId = $userModel->getUserId();
        
        //genLayout param is not present in design parameters, layoutfields should show
        if($generateLayoutInc == "false" && count($layoutFields) > 0){ 
            $generateLayout = "true";
        }

        $response = [
            'model' => $model,
            'program' => $program,
            'id' => $id,
            'experimentDesign' => $designAF,
            'processId' => $processId,
            'designFields' => $designFields,
            'layoutFields' => $layoutFields,
            'planDesign' => $jsonInput,
            'withChecks' =>  $entryInfo['check'] > 0 ? TRUE:FALSE,
            'fileList' => $fileList,
            'showLayout' => $showLayout,
            'designArray' => $designArray,
            'userId' =>$userId,
            'deletePlots' => $plotRecords > 0 ? 'true' : 'false',
            'rules' => json_encode($rules),
            'paramValues' => json_encode($paramValues),
            'designInfo'=> json_encode($designInfo),
            'design' => $experimentDesign,
            'uploaded' => $uploaded,
            'uiTemplate' => $uiTemplate,
            'uiTemplateInfo' => $uiTemplateInfo,
            'entryListId' => $entryListId,
            'dataProvider' => $dataProvider,
            'filters' => $filters,
            'gridClass' => $gridClass,
            'generateLayoutShow' => $generateLayout,
            'selectedItems' => $storedItems,
            'configData' => $configData,
            'totalEntries' => $entryInfo['total'],
            'updateDesignFromEntry' => $model['notes'],
            'designDesc' => $designDesc
        ];

        if(!$parameterSet){
            $response['entryInfo'] = json_encode($entryInfo);
            $response['entryInfoDataProvider'] = $entryInfoDataProvider;
            $response['planStatus'] = $planStatus;
        } else {
            $response['incompleteSet'] = $incompleteSet;
        }
        return $response;
    }

    /**
     * Saves design and create config json for ui
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     *
     */
    public function actionChangeDesign($program,$id){
        
        if(isset($_POST)){

            $designInfo = json_decode($_POST['designInfo'], true);
            $designId = $_POST['designId'];

            $design = $designInfo[$designId]['designName'];
            $model = $this->mainExperiment->getExperiment($id);

            $parameterArray = $this->mainExperimentModel->getJsonConfig($designId, 'ui');

            //generate template json for selected design
            $designRecord = $this->mainVariableModel->getVariableByAbbrev('DESIGN');
            $designVariable = $designRecord['variableDbId'];

            //get scale values for design
            $scaleRecords = $this->mainScaleModel->getVariableScale($designVariable)['scaleValues'];

            $scaleValues = array_column($scaleRecords, 'value');

            $searchword = trim($design);
            $matches = array_filter($scaleValues, function($var) use ($searchword) { return preg_match("/($searchword)/i", $var); });

            $matches = array_values($matches);
            $experimentDesign = $matches[0];
            

            //Update experiment design
            $oldDesign = $model['experimentDesignType'];
            $this->mainExperiment->updateOne($id, ['experimentDesignType'=>$experimentDesign]);
            $entryListId = $this->entryListModel->getEntryListId($id);
            //if design is Augmented Design
            if($experimentDesign == 'Augmented Design'){
                //update Entry role for all checks
                $this->plantingArrangement->updateEntryRole($entryListId, "spatial check", []);
            } elseif($oldDesign == 'Augmented Design'){
                //update entry role for all checks
                $this->plantingArrangement->updateEntryRole($entryListId, "null", []);
            }

            //Remove filters
            $filters = [];
            Yii::$app->session->set('design-entries-filters-' . $id, $filters);
            Yii::$app->session->set("ec"."_is_selected_all_$id", false);

            //check if there's existing json input for the experiment design
            //POST search plan-templates
            $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'equals experiment', 'entityDbId'=>"equals $id"]);

            if($planTemplateArray['totalCount'] == 0){

                //create plan template record
                $experimentJson = [];
                $experimentJson['templateName'] = str_replace('#','_',(str_replace(' ', '_',$model['experimentCode']))).'_'.$model['experimentDbId'];
                $experimentJson['entityType'] = "experiment";
                $experimentJson['entityDbId'] = $id;

                //get fields for design
                $parameterArray = $this->mainExperimentModel->getJsonConfig($designId, 'ui');
                $jsonArray['design_ui'] = $parameterArray;
                $jsonArray['design_id'] = $designId;
                $jsonArray['design_name'] = $design;
                $jsonArray['design_ui']['show_layout'] = FALSE;

                //get fields for metadata
                $parameterMetadata = $this->mainExperimentModel->getJsonConfig($designId, 'metadata');
                $jsonArray['metadata'] = $parameterMetadata;

                //get dummy for json input
                $parameterJson = $this->mainExperimentModel->getJsonConfig($designId, 'json');
                $jsonArray['json_file']=$parameterJson;

                //create record in experiment data
                $experimentJson['design'] = $experimentDesign;
                $programDbId = $model['programDbId'];
                $experimentJson['programDbId'] = "$programDbId";
                $experimentJson['status'] = 'draft';
                $experimentJson['mandatoryInfo'] = json_encode($jsonArray);
                $experimentJson['entryCount'] = "0";
                $experimentJson['plotCount'] = "0";
                $experimentJson['checkCount'] = "0";
                $experimentJson['repCount'] = "0";
                $experimentJson['tempFinalCabinetDbId'] = "0";
                
                $this->planTemplate->createPlanTemplate($experimentJson);

            } else {
                $experimentJson = $planTemplateArray['data'][0];
                
                 //get fields for design
                $parameterArray = $this->mainExperimentModel->getJsonConfig($designId, 'ui');
                $jsonArray['design_ui'] = $parameterArray;
                $jsonArray['design_ui']['show_layout'] = FALSE;
                $jsonArray['design_id'] = $designId;
                $jsonArray['design_name'] = $design;

                //get fields for metadata
                $parameterMetadata = $this->mainExperimentModel->getJsonConfig($designId, 'metadata');
                
                $jsonArray['metadata'] = $parameterMetadata;

                //get dummy for json input
                $parameterJson = $this->mainExperimentModel->getJsonConfig($designId, 'json');
                $jsonArray['json_file']=$parameterJson;

                //update plan template
                $experimentJsonTemp = array();

                $experimentJsonTemp['design'] = $experimentDesign;
                $experimentJsonTemp['mandatoryInfo'] = json_encode($jsonArray);
                $experimentJsonTemp['status'] = 'draft';

                // Update experiment design
                $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], $experimentJsonTemp);

            }
            
            //Update experiment design
            $experimentStatus = 'entry list created';
            $statusArray = explode(';', $model['experimentStatus']);
            if(in_array("entry list created", $statusArray)){
                $experimentStatus = 'entry list created';
            }else if(in_array("entry list incomplete seed sources", $statusArray)){
                 $experimentStatus = 'entry list incomplete seed sources';
            }
            
            $this->mainExperiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
            
            /**ADD deletion of succeeding records**/

            return json_encode('ok');
        }

    }

    /**
     * Generates json input format for analysis module
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     *
     */
    public function actionGenerateJsonInputAnalytics($id, $program){

        if(isset($_POST)){

            $valuesArray = $_POST;
            //check if there's existing json input for the experiment design
            $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals experiment', 'entityDbId'=>"equals $id"]);

            $experimentJson = $experimentJsonArray['data'][0];

            $plantTemplateDbId = $experimentJson['planTemplateDbId'];

            $experiment = $this->mainExperiment->getExperiment($id);
            $jsonArray = json_decode($experimentJson['mandatoryInfo'], true);
            
            $jsonInput = array();

            $parameterRandArray = $jsonArray['design_ui']['randomization'];
            $indexRandIds = array_column($parameterRandArray, 'code');

            $parameterLayoutArray = $jsonArray['design_ui']['layout'];
            $indexLayoutIds = array_column($parameterLayoutArray, 'code');
            

            $occurrencesCount = 0;
            foreach($valuesArray['ExperimentDesign'] as $key=>$value){
              
                if($key != 'DESIGN'){
                    $indexRand = array_search($key,$indexRandIds);
                    $indexLayout = array_search($key,$indexLayoutIds);
                    
                    if($value == ""){
                        $value = 0;
                    }
                    if($indexRand !== false && isset($parameterRandArray[$indexRand])){
                      
                        $valuesArray['ExperimentDesign'][$key] = $value;
                        $parameterRandArray[$indexRand]['value'] = $value;
                    }
                    if($indexLayout !== false && isset($parameterLayoutArray[$indexLayout])){
                      
                        $valuesArray['ExperimentDesign'][$key] = $value;
                        $parameterLayoutArray[$indexLayout]['value'] = $value;
                    }
                    if($key == 'nTrial' || $key == 'nFarm'){
                        $occurrencesCount = intval($value);
                        $parameterRandArray[$indexRand]['value'] = intval($value);
                    }
                    $jsonInput[$key] = $value;
                } else {

                    $design = $value;
                }
            }

            if(isset($valuesArray['uiGroups'])){
                $jsonArray['design_ui']['groups'] = $valuesArray['uiGroups'];
            }
            $jsonArray['design_ui']['randomization'] = $parameterRandArray;
            $jsonArray['design_ui']['layout'] = $parameterLayoutArray;

            $generatedInput = $this->mainExperimentModel->createJsonInputAnalytics($id, $valuesArray['ExperimentDesign'], $design, $jsonInput, $jsonArray, $occurrencesCount);

            //create record in experiment data
            $experimentJsonTemp = array();
            $experimentJsonTemp['randomizationInputFile'] = json_encode($generatedInput, JSON_UNESCAPED_SLASHES);
            $experimentJsonTemp['mandatoryInfo'] = json_encode($jsonArray);

            // Update experiment design
            $this->planTemplate->updateOne($plantTemplateDbId, $experimentJsonTemp);

            $analysisModel = new AnalysisModel();
            $submitRequest = $analysisModel->submitRequest($generatedInput);
            //get totalPlots
            
            if($submitRequest['success']){

                $experimentJsonTemp['status'] = 'randomization in progress';
                $experimentJsonTemp['randomizatDataResults'] = '{}';
                $experimentJsonTemp['randomizationResults'] = '{}';

                //store request id
                $experimentJsonTemp['requestDbId'] = $submitRequest['requestId'];

                $experimentJsonTemp['notes'] = '{}';

                // Update experiment design
                $this->planTemplate->updateOne($plantTemplateDbId, $experimentJsonTemp);
                
                
                //Update experiment design
                $entryStatus = "entry list created";
                $statusArray = explode(';', $experiment['experimentStatus']);
                if(in_array("entry list created", $statusArray)){
                    $entryStatus = "entry list created";
                } else if(in_array("entry list incomplete seed sources", $statusArray)){
                    $entryStatus = "entry list incomplete seed sources";
                }
                $experimentStatus = $entryStatus.';design requested';
                
                $this->mainExperiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
                return json_encode('success');
                
            } else {
                $experimentJsonTemp['status'] = 'randomization failed';
                $experimentJsonTemp['randomizatDataResults'] = '{}';
                $experimentJsonTemp['randomizationResults'] = '{}';

                // Update experiment design
                $this->planTemplate->updateOne($plantTemplateDbId, $experimentJsonTemp);
                return json_encode('failed');
            }
            
        }
    }

    /**
     * Delete the previously generated plots
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     */
    public function actionDeletePlots($id, $program, $occurrenceDbIds = null){
        
        $response['message'] = 'Failed to delete plots. Please try again.';
        $response['success'] = false;

        $application = 'EXPERIMENT_CREATION';
        $entityId = $id;
        $occurrenceIds = [];
        if($occurrenceDbIds == null){
            $occurrences = $this->occurrenceModel->searchAll(['fields'=>'occurrence.experiment_id as experimentDbId|occurrence.id as occurrenceDbId','experimentDbId'=>"equals $id"]);
            if($occurrences['totalCount'] > 0){
                $occurrenceIds = array_column($occurrences['data'], 'occurrenceDbId');
            }
        } else {
            $occurrenceIds = explode(";",$occurrenceDbIds);
            $application = 'OCCURRENCES';
            $entityId = $occurrenceIds[0];
        }
        $dependents = [];
        if(!empty($occurrenceIds)){
            
            $token = Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = Yii::$app->session->get('user.refresh_token');
            //call background process for deletion
            $requestDataParams = [
                'occurrenceIds'=>$occurrenceIds,
                'processName' => 'delete-design-records',
                'experimentDbId' => $id.""
            ];

            $params = [
                'httpMethod' => 'DELETE',
                'endpoint' => 'v3/plot',
                'entity' => 'PLOT',
                'entityDbId' => ''.$entityId,
                'endpointEntity' => 'PLOT',
                'application' => $application,
                'dbIds' => ''.$entityId,
                'descpription' => 'Deletion of plot records',
                'requestData' => $requestDataParams,
                'tokenObject' => [
                    "token" => $token,
                    "refreshToken" => $refreshToken
                ],
            ];

            $deletePlots = Yii::$app->api->getParsedResponse('POST', 'processor', json_encode($params));

            if(isset($deletePlots['data'][0])){

                if($application == 'EXPERIMENT_CREATION'){
                
                    $experiment = $this->mainExperiment->searchAll(["fields"=>"experiment.id as experimentDbId|experiment.notes as notes|experiment.experiment_status AS experimentStatus|experiment.experiment_type AS experimentType|experiment.experiment_name as experimentName","experimentDbId"=>"equals ".$id]);
                    $bgJobId = isset($deletePlots['data'][0]['backgroundJobDbId']) ? $deletePlots['data'][0]['backgroundJobDbId'] : null;
                    $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($deletePlots['data'][0]['backgroundJobDbId']);
                    
                    $experimentStatus = isset($experiment['data'][0]['experimentStatus']) ? $experiment['data'][0]['experimentStatus'] : '';

                    if(!empty($experimentStatus)){
                        $status = explode(';',$experimentStatus);
                        $status = array_filter($status);
                    }else{
                        $status = [];
                    }

                    if($bgStatus !== 'done' && !empty($bgStatus)){
                        $updatedStatus = implode(';', $status);
                        $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
                    
                        $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);

                        //Update the experiment notes to remove plantingArrangementChanged, empty the notes
                        $this->mainExperiment->updateOne($id, ["notes" => null]);
                    }else if($bgStatus == 'done'){

                        //Update the experiment notes to remove plantingArrangementChanged, empty the notes
                        $this->mainExperiment->updateOne($id, ["notes" => null]);
                        $deleteFlag = false;
                    }

                    $response['message'] = $deletePlots['status'];
                    $response['success'] = true;

                    if(isset($_POST['parameterSet'])){
                        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id"]);
                    } else {
                        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals experiment', 'entityDbId'=>"equals $id"]);
                    }
                
                    if($experimentJsonArray['totalCount'] > 0){
                        $experimentJson = $experimentJsonArray['data'];
                        $plantTemplateDbIds = array_column($experimentJson, 'planTemplateDbId');
                        //update plan template status
                        $this->planTemplate->updateMany($plantTemplateDbIds, ['status'=>'deletion in progress']);
                    }
                    return json_encode($response);
                } else {
                    $bgJobId = isset($deletePlots['data'][0]['backgroundJobDbId']) ? $deletePlots['data'][0]['backgroundJobDbId'] : null;
                    $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($deletePlots['data'][0]['backgroundJobDbId']);

                    if($bgStatus !== 'done' && !empty($bgStatus)){
                        $newStatus = "design deletion in progress";
                    
                        $this->occurrenceModel->updateMany($occurrenceIds, ["occurrenceStatus"=>"$newStatus"]);

                        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals occurrence', 'occurrenceApplied'=>"equals ".$occurrenceDbIds]); 
                    }else if($bgStatus == 'done'){

                        $this->occurrenceModel->updateMany($occurrenceIds, ["occurrenceStatus" => "design deletion done"]);
                        $deleteFlag = false;
                    }

                    $response['message'] = $deletePlots['status'];
                    $response['success'] = true;

                    $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals occurrence', 'occurrenceApplied'=>"equals ".$occurrenceDbIds]); 
                
                    if($experimentJsonArray['totalCount'] > 0 ){// && $bgStatus != 'done'){
                        $experimentJson = $experimentJsonArray['data'][0];
                        //update plan template status
                        $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], ['status'=>'deletion in progress']);
                    }

                    //update experiment status
                    $this->mainExperiment->updateOne($id, ["experimentStatus"=>"creation in progress"]);
                    return json_encode($response);
                }
               
            } 
           
        }

        return json_encode($response);

    }

    /**
     * Check if the plots are already generated
     *
     * @param $id integer record id of the experiment
     * @param $program integer program where the experiment belongs
     * @param $label text value to be shown in the viewer
     * @param $flipped boolean flag for changing the origin of the layout
     * @param $occurrenceDbId integer value of the current occurrence being shown in the layout
     *
     */
    public function actionCheckDesignResults($id, $program, $label='plotno', $occurrenceDbId = NULL){

        $panelWidth = $_POST['panelWidth'];
        //check if there's existing json input for the experiment design
        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals experiment', 'entityDbId'=>"equals $id"]);
        $experiment = $this->mainExperiment->getExperiment($id);

        $userModel = new User();
        $userId = $userModel->getUserId();

        if($experimentJsonArray['totalCount'] > 0){
            $experimentJson = $experimentJsonArray['data'][0];
            
            if($experimentJson['status'] != 'deletion in progress'){

                //check if uploaded
                $uploaded = false;
                if(isset($experimentJson['mandatoryInfo']) && $experimentJson != NULL){
                    $jsonArr = json_decode($experimentJson['mandatoryInfo'],true);
                    if(isset($jsonArr['totalOccurrences'])){
                        $uploaded = true;
                    }
                }

                //Get request status
                $analysisModel = new AnalysisModel();

                $designValue = $experimentJson['design'];
                if($uploaded){
                    $designValue = 'Unspecified';
                }

                $requestStatus = $analysisModel->getRequestStatus($experimentJson['requestDbId'], $designValue);
                
                if(isset($requestStatus['data']['findRequest']['status']) && in_array($requestStatus['data']['findRequest']['status'], ['new', 'in_progress', 'submitted', 'processing_custom_design', 'upload in progress'])){
                  
                    $statusMessage = 'Randomization in progress';
                    if(strpos($experimentJson['status'], 'upload') !== false){
                        $statusMessage = 'Upload in progress';
                    }
                    //check status
                    $entryStatus = "entry list created";
                    $statusArray = explode(';', $experiment['experimentStatus']);
                    if(in_array("entry list created", $statusArray)){
                        $entryStatus = "entry list created";
                    } else if(in_array("entry list incomplete seed sources", $statusArray)){
                        $entryStatus = "entry list incomplete seed sources";
                    }
                    $experimentStatus = $entryStatus.';design requested';

                    $htmlData = $this->renderPartial('design/_design_layout_preview.php',[
                        'experimentId' => $id,
                        'data' => [],
                        'startingPlotNo' => 1,
                        'panelWidth' => $panelWidth,
                        'noOfRows' => 0,
                        'noOfCols' => 0,
                        'statusDesign' => 'in progress',
                        'deletePlots' => 'true',
                        'planStatus' => $experimentJson['status'],
                        'design'=> $experiment['experimentDesignType']
                    ],true,false);

                    return json_encode([
                        'status' => $experimentStatus,
                        'success'=> 'in progress',
                        'htmlData' => $htmlData,
                        'planStatus' => $experimentJson['status'],
                        'deletePlots' => 'true',
                        'message' => $statusMessage
                    ]);
                    
                } else if(in_array($experimentJson['status'], ['randomized','uploaded']) || in_array($requestStatus['data']['findRequest']['status'], ['completed'])){

                    //check status
                    $entryStatus = "entry list created";
                    $statusArray = explode(';', $experiment['experimentStatus']);
                    if(in_array("entry list created", $statusArray)){
                        $entryStatus = "entry list created";
                    } else if(in_array("entry list incomplete seed sources", $statusArray)){
                        $entryStatus = "entry list incomplete seed sources";
                    }
                    $experimentStatus = $entryStatus.';design generated';

                    $statusStr = 'randomized';

                    if($experimentJson['status'] !== 'randomized' || $experimentJson['status'] !== 'uploaded'){
                        //check if randomization or upload
                        
                        if(strpos($experimentJson['status'], 'upload') !== false){
                            $statusStr = 'uploaded';
                        }
                        $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], ['status'=>$statusStr]);
                        
                        $status = explode(';',$experiment['experimentStatus']);
            
                        //Update experiment design
                        //check status
                        $entryStatus = "entry list created";
                        $statusArray = explode(';', $experiment['experimentStatus']);
                        if(in_array("entry list created", $statusArray)){
                            $entryStatus = "entry list created";
                        } else if(in_array("entry list incomplete seed sources", $statusArray)){
                            $entryStatus = "entry list incomplete seed sources";
                        }
                        $experimentStatus = $entryStatus.';design generated';

                        //Update experiment status
                        $this->mainExperiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
                    }

                    $occurrences = $this->occurrenceModel->searchAll([
                        'fields'=>'occurrence.experiment_id as experimentDbId|occurrence.occurrence_name as occurrenceName|occurrence.id AS occurrenceDbId',
                        'experimentDbId' => "equals $id"
                    ],'sort=occurrenceName:ASC');
                    
                    $occurrencesList = array();

                    //get entry count
                    //double check for incomplete seed sources
                    $entryListId = $this->entryListModel->getEntryListId($id);

                    //Check if all entries has seeds, then add entry list created to status
                    $filterRecords = [
                        "fields" => "entry.entry_list_id AS entryListDbId|entry.id AS entryDbId",
                        "entryListDbId"=>"equals $entryListId",
                    ];

                    $entryRecords = $this->entryModel->searchAll($filterRecords, 'limit=1&sort=entryDbId:asc', false);
                    if($occurrences['totalCount'] > 0){

                        $occurrencesList = $occurrences['data'];
                        if($occurrenceDbId == NULL){
                            $occurrenceDbId = $occurrences['data'][0]['occurrenceDbId'];
                            $occurrenceName = $occurrences['data'][0]['occurrenceName'];
                        } else {
                            $occurrenceIdx = array_search($occurrenceDbId, array_column($occurrencesList, 'occurrenceDbId'));
                            $occurrenceName = $occurrencesList[$occurrenceIdx]['occurrenceName'];
                        }

                        //checked plot records and view layout
                        $plotRecords = $this->occurrenceModel->searchAllPlots($occurrenceDbId, null,'limit=1&sort=plotNumber:ASC', false);
                        
                        if($plotRecords['totalCount'] > 0){

                            $occurrenceDataProvider = null;
                            if($plotRecords['data'][0]['plots'][0]['designX'] == null){ //no layout

                                // Download occurrences
                                $occurrenceDataProvider = \Yii::$container->get('app\models\Occurrence')->getDataProvider($id, null, [], true, ['occurrenceName'=>SORT_ASC]);
                            }
                            //Save FIRST_PLOT_POSITION_VIEW
                            $varAbbrev = 'FIRST_PLOT_POSITION_VIEW';
                            $variableInfo = $this->mainVariableModel->searchAll(["abbrev"=>"equals $varAbbrev"]);
                            $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
                            
                            $expData = $this->mainExperimentData->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);
                            
                            if(isset($expData) && !empty($expData)){
                                if($expData[0]['dataValue'] == "Top Left"){
                                    $flipped = true;
                                } else {
                                    $flipped = false;
                                }   
                            } else {   //create variable

                                // if no saved in experiment data, get default first plot position in config
                                $config = $this->mainConfigModel->getConfigByAbbrev('FIRST_PLOT_POSITION_VIEW_DEFAULT');

                                $varValue = (isset($config['value']) && !empty($config['value'])) ? $config['value'] : 'Top Left';

                                $dataRequestBodyParam[] = [
                                    'variableDbId' => "$variableDbId",
                                    'dataValue' => "$varValue"
                                ];

                                $this->mainExperiment->createExperimentData($id,$dataRequestBodyParam);
                                $flipped = ($varValue == 'Bottom Left') ? false : true;
                            }
                            $layoutArray = $this->mainExperimentModel->generateLayout($occurrenceDbId, $id);
                            
                            $htmlData = $this->renderPartial('design/_design_layout_preview.php',[
                                'experimentId' => $id,
                                'data' => $layoutArray['dataArrays'],
                                'label'=> $label,
                                'startingPlotNo' => 1,
                                'panelWidth' => $panelWidth,
                                'noOfRows' => $layoutArray['maxRows'] == NULL ? 0:$layoutArray['maxRows'],
                                'noOfCols' => $layoutArray['maxCols'] == NULL ? 0:$layoutArray['maxCols'],
                                'statusDesign' => 'completed',
                                'deletePlots' => 'true',
                                'planStatus' => $statusStr,
                                'flipped' => $flipped,
                                'occurrencesList' => $occurrencesList,
                                'occurrenceName' => $occurrenceName,
                                'occurrenceDbId' => $occurrenceDbId,
                                'xAxis' => $layoutArray['xAxis'],
                                'yAxis' => $layoutArray['yAxis'],
                                'maxBlk' => $layoutArray['maxBlk'],
                                'design'=> $experiment['experimentDesignType'],
                                'entryCount' => $entryRecords['totalCount'],
                                'plotCount' =>$plotRecords['totalCount'],
                                'program' => $program,
                                'occurrenceDataProvider' => $occurrenceDataProvider

                            ],true,false);
                
                            return json_encode([
                                'status' => $experimentStatus,
                                'success'=> 'success',
                                'htmlData' => $htmlData,
                                'deletePlots' => 'true',
                                'planStatus' => $statusStr,
                                'message' => 'Successfully '.$statusStr.'.'
                            ]);
                            
                        }
                    }

                } else if(isset($requestStatus['errors']) || in_array($requestStatus['data']['findRequest']['status'], ['failed', 'expired'])){
                    $statusStr = 'randomization failed';
                    $statusMessage = 'Randomization Failed. Please try again.';
                    if(strpos($experimentJson['status'], 'upload') !== false){
                        $statusStr = 'upload failed';
                        $statusMessage = 'Upload Failed. Please try again.';
                    }
                    $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], ['status'=>$statusStr]);

                    $status = explode(';',$experiment['experimentStatus']);
            
                    //check status
                    $entryStatus = "entry list created";
                    $statusArray = explode(';', $experiment['experimentStatus']);
                    if(in_array("entry list created", $statusArray)){
                        $entryStatus = "entry list created";
                    } else if(in_array("entry list incomplete seed sources", $statusArray)){
                        $entryStatus = "entry list incomplete seed sources";
                    }
                    $experimentStatus = $entryStatus;
                            
                    //Update experiment status
                    $this->mainExperiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
                    return json_encode([
                        'status' => $experimentStatus,
                        'success'=> 'failed',
                        'htmlData' => [],
                        'planStatus' => $statusStr,
                        'deletePlots' => 'false',
                        'message' => $statusMessage
                    ]);
                } else {
                    $statusStr = 'randomization failed';
                    $statusMessage = 'Randomization Failed. Please try again.';
                    if(strpos($experimentJson['status'], 'upload') !== false){
                        $statusStr = 'upload failed';
                        $statusMessage = 'Upload Failed. Please try again.';
                    }
                    $updateTemplate = $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], ['status'=>$statusStr]);

                    //check status
                    $entryStatus = "entry list created";
                    $statusArray = explode(';', $experiment['experimentStatus']);
                    if(in_array("entry list created", $statusArray)){
                        $entryStatus = "entry list created";
                    } else if(in_array("entry list incomplete seed sources", $statusArray)){
                        $entryStatus = "entry list incomplete seed sources";
                    }
                    $experimentStatus = $entryStatus;
                            
                    //Update experiment status
                    $update = $this->mainExperiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
                    return json_encode([
                        'status' => $experimentStatus,
                        'success'=> 'failed',
                        'htmlData' => [],
                        'planStatus' => $statusStr,
                        'deletePlots' => 'false',
                        'message' => $statusMessage
                    ]);
                }
            } else {

                //retrieve process background
                $bgAddtlParams = ["entityDbId"=>"equals ".$id,"entity"=>"equals PLOT", "endpointEntity"=>'equals PLOT',"application"=>"equals EXPERIMENT_CREATION"];
    
                $response = $this->backgroundJob->searchAll($bgAddtlParams, 'sort=backgroundJobDbId:DESC');
                
                if (isset($response['success']) && $response['success'] == 200) {
                    if(strtolower($response['data'][0]['jobStatus']) == 'done'){

                        //update plan status
                        $planStatus = 'draft';
                        $updateTemplate = $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], ['status'=>$planStatus]);

                        //update experiment status
                        $status = explode(';',$experiment['experimentStatus']);
            
                        //Update experiment design
                        $experimentStatus = 'entry list created';
                        
                        $update = $this->mainExperiment->updateOne($id, ['experimentStatus'=>$experimentStatus, 'notes'=>null]);

                        $params = [
                            "jobIsSeen" => "true",
                            "jobRemarks" => "REFLECTED"
                        ];
                        $updateBgProcess = $this->backgroundJob->updateOne($response['data'][0]['backgroundJobDbId'], $params);
                        $success = 'delete successful';
                        $deletePlots = 'false';
                        $message = 'Plots successfully deleted. Please proceed in submitting your request.';

                    } else if(strtolower($response['data'][0]['jobStatus']) == 'failed'){

                        $planStatus = 'draft';
                        $updateTemplate = $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], ['status'=>$planStatus]);

                        $success = 'deletion failed';
                        $deletePlots = 'true';
                        $message = 'Deletion of plot records failed. Please try again.';
                        $status = $experiment['experimentStatus'];

                        $params = [
                            "jobIsSeen" => "true",
                            "jobRemarks" => "REFLECTED"
                        ];
                        $updateBgProcess = $this->backgroundJob->updateOne($response['data'][0]['backgroundJobDbId'], $params);
                    } else {
                        $success = 'deletion in progress';
                        $deletePlots = 'true';
                        $message = '';
                        $planStatus = 'deletion in progress';
                        $status = $experiment['experimentStatus'];
                    }
                } else {
                    $planStatus = 'draft';
                    $updateTemplate = $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], ['status'=>$planStatus]);

                    $success = 'deletion failed';
                    $deletePlots = 'true';
                    $message = 'Deletion of plot records failed. Please try again.';
                    $status = $experiment['experimentStatus'];
                }
                    
                //Update experiment status
                
                $htmlData = $this->renderPartial('design/_design_layout_preview.php',[
                    'experimentId' => $id,
                    'data' => [],
                    'startingPlotNo' => 1,
                    'panelWidth' => $panelWidth,
                    'noOfRows' => 0,
                    'noOfCols' => 0,
                    'statusDesign' => $success,
                    'planStatus' =>$planStatus,
                    'deletePlots' => $deletePlots,
                    'design'=> $experiment['experimentDesignType']
                ],true,false);
                
                return json_encode([
                    'status' => $status,
                    'success'=> $success,
                    'htmlData' => $htmlData,
                    'planStatus' =>$planStatus,
                    'deletePlots' => $deletePlots,
                    'message' => $message
                ]);
            }
        }
    }

    /**
     * Update/save first plot position
     *
     * @param $id integer record id of the experiment
     **/
    public function actionChangeFirstPlotPos($id){

        $flipped = $_POST['flipped'];
        
        //Save FIRST_PLOT_POSITION_VIEW
        $varAbbrev = 'FIRST_PLOT_POSITION_VIEW';
        $variableInfo = $this->mainVariableModel->searchAll(["abbrev"=>"equals $varAbbrev"]);
        $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
        
        $expData = $this->mainExperimentData->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);
        
        if($flipped == "true"){
            $valueLabel = 'Top Left';
        } else {
            $valueLabel = 'Bottom Left';
        } 
        if(isset($expData) && !empty($expData)){
            if($valueLabel != $expData[0]['dataValue']){
                $varValue = $valueLabel;
            
                $dataRequestBodyParam = [
                    'dataValue' => "$varValue"
                ];
                $this->mainExperimentData->updateExperimentData($expData[0]['experimentDataDbId'], $dataRequestBodyParam);
            }
        } else {   //create variable

            // if no saved in experiment data, get default first plot position in config
            $config = $this->mainConfigModel->getConfigByAbbrev('FIRST_PLOT_POSITION_VIEW_DEFAULT');

            $varValue = (isset($config['value']) && !empty($config['value'])) ? $config['value'] : 'Top Left';

            $dataRequestBodyParam[] = [
                'variableDbId' => "$variableDbId",
                'dataValue' => "$varValue"
            ];
            
            $this->mainExperimentData->createExperimentData($id,$dataRequestBodyParam);
        }
        return json_encode('success');
    }

    /*
     * Update the layout view of the design based on the selected occurrence
     * 
     */
    public function actionChangeOccurrenceLayout(){

        $occurrenceDbId = $_POST['occurrenceDbId'];

        $fullScreen = false;
        if($_POST['fullScreen'] == 1){
            $fullScreen = true;
        }

        $layoutArray = $this->mainExperimentModel->generateLayout($occurrenceDbId, $_POST['experimentId'], $fullScreen);

        return json_encode($layoutArray);
    }

    /**
     * Download the analytical results
     *
     * @param integer $planId record id of plan template created for the randomization
     * @param string $fileName name of the file to be downloaded
     *
     */
    public function actionDownloadResults($planId, $fileName, $labelType) {

        $transaction = PlanTemplate::getPlanTemplateById($planId);

        $dataArray = $transaction['randomizationInputFile'];
        $url = Yii::getAlias('@webroot') . '/analytics/b4r-trial-designs/' . $dataArray['parameters']['outputFile'].'/';
        
        $outputFiles = $transaction['randomizationResults'];
        $source = $url . $fileName;

        $labelArray = ['StatisticalDesignArray', 'TrmtLayout'];
        //batch download this file
        if(in_array($labelType, $labelArray) && ((int) $dataArray['parameters']['nTrial'] > 1 || (int) $dataArray['parameters']['nFarm'] > 1)){
            $searchword = $labelType;
            $matches = array_filter($outputFiles['fileListResource'], function($var) use ($searchword) {return strpos($var, $searchword) !== false; });

            array_walk($matches, function(&$value, $key)use($url) { $value = $url.$value; } );
            $stringFiles = implode(' ', $matches);
            $filename = $dataArray['parameters']['outputFile'].'_'.$labelType.'.tar.gz';
            $zipFile = $url.$filename;
            $currentDirectory = exec('pwd');

            exec('tar czf '.$url.'/'.$filename.' '.$stringFiles);
            Yii::$app->response->sendFile($url.$filename);
            unlink($url.$filename);
        } else {
            if (file_exists($source)) {
                chmod($source, 0777);
                Yii::$app->response->sendFile($source);
                return json_encode('ok');
            } else {

                throw new \yii\web\NotFoundHttpException("{$fileName} is not found!");
            }
        }
    }

    /**
     * Export and downloads the all the data for the experiement
     *
     * @param string $fileName name of the file to be downloaded
     *
     */
    public function actionDownloadExperiment($filename) {

        $source = Yii::getAlias('@webroot') . '/files/data_export/'.$filename;

        if (file_exists($source)) {

            chmod($source, 0777);
            Yii::$app->response->sendFile($source);
        } else {

            throw new \yii\web\NotFoundHttpException("{$filename} is not found!");
        }
    }

    /*
    * Get background process transactions
    * @return $transactionsStr text list of transactions
    */
   public function actionGetTransactions(){
       extract($_POST);
       $userId = isset($_POST['userId']) ? $_POST['userId'] : null;

       $transactionsStr = Transaction::getActiveGearmanTransactions($userId, $id, $program);

       return json_encode($transactionsStr);
   }

    /**
     * Renders view for specifying experiment groups
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     *
     */
    public function actionSpecifyExperimentGroups($program, $id, $processId){
        
        $model = Experiment::findOne($id);

        //create new experiment group if post
        if(isset($_POST) && $_POST != []){

            $form = $_POST;
        }

        //get all the experiment groups that includes the experiment
        $experimentGroups = ExperimentGroup::find()->where(['experiment_id'=>$id, 'is_void'=>false])->orderBy('id DESC')->all();
        $status = explode(';', $model->experiment_status);

        if(count($experimentGroups) > 0){

            $model->experiment_status = !in_array('experiment group specified', $status) ? ($model->experiment_status.';experiment group specified') : $model->experiment_status;
            $model->save();
        } else {

            if (($key = array_search('experiment group specified', $status)) !== false) {

                unset($status[$key]);
                $model->experiment_status = implode(';', $status);
                $model->save();
            }
        }

        $experimentTables=array();

        foreach($experimentGroups as $experimentGroup){
            $experimentTables[] = VariableComponent::formExptGrpTable($experimentGroup->id, $id);
        }

        $experimentGroupModel = new ExperimentGroup();
        $userModel = new User();
        $userId = $userModel->getUserId();

        if ($experimentGroupModel->load(Yii::$app->request->post())) {

            $locationReps = $model->location_rep_count;

            if(isset($locationReps) && (int) $locationReps > count($experimentGroups)){

                //save new experiment group
                $success = false;
                $error = [];
                $experimentGroupModel->creator_id = $userId;
                $experimentGroupModel->experiment_id = $id;
                !$experimentGroupModel->is_void;

                if($experimentGroupModel->save()){

                    ExperimentModel::deleteExistingStudies($id);
                    $model->experiment_status = 'entry list created;design requested;experiment group specified';
                    $model->save();

                    $success = true;
                    //automatically create experiment group experiment record
                    $experimentGroupExperiment = new ExperimentGroupExperiment();
                    $experimentGroupExperiment->experiment_group_id = $experimentGroupModel->id;
                    $experimentGroupExperiment->experiment_id = $id;

                    $expGrpsInGrp = ExperimentGroupExperiment::find()->where(['experiment_group_id'=>$experimentGroupExperiment->id, 'is_void'=>false])->all();
                    $experimentGroupExperiment->order_number=count($expGrpsInGrp) + 1;
                    $experimentGroupExperiment->creator_id=$userId;
                    $experimentGroupExperiment->save();

                    Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully saved <b>'.$experimentGroupModel->name.'</b>.');
                    return json_encode(['success' => $success, 'name'=>$experimentGroupModel->name, 'error' => $error, 'create_another' => "0"]);

                } else{

                    $error = $experimentGroupModel->getErrors();
                    Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> Failed to save <b>'.$experimentGroupModel->name.'</b>.'.$error);
                    return json_encode(['success' => true, 'name'=>$experimentGroupModel->name, 'error' => $error, 'create_another' => "0"]);
                }

            } else {

                Yii::$app->session->setFlash('error', '<i class="fa fa-times"></i> Reached the maximum no. of limit defined in location reps for this experiment.');
                return json_encode(['success' => true, 'name'=>$experimentGroupModel->name, 'error' => '', 'create_another' => "0"]);
            }

        } else {

            //renders experiment groups tab
            return $this->render('experiment_group',[
                'model' => $model,
                'program' => $program,
                'id' => $id,
                'processId'=>$processId,
                'experimentTables'=>$experimentTables,
                'experimentGroupModel' => $experimentGroupModel
            ]);
        }
    }

    /**
     * Retrieves all experiments not included in the specified experiment group
     *
     */
    public function actionGetExperiments(){

        if(isset($_POST)){

            $experimentGroupId = $_POST['exptGrpId'];
            $notIncludedRecords = ExperimentGroupExperiment::find()->where(['experiment_group_id'=>$experimentGroupId, 'is_void'=>false])->all();
            $notIncludedIds = array_column($notIncludedRecords, 'experiment_id');
            $query = Experiment::find();
            $query->where(['not in', 'id', $notIncludedIds]);

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_ASC
                    ]
                ]
            ]);

            $htmlData = $this->renderPartial('add_experiment_to_group.php',[
                'data' => $dataProvider,
                'experimentGroupId' => $experimentGroupId
            ],true,false);

            return json_encode([
                'htmlData' => $htmlData
            ]);
        }

    }

    /**
     * Updates experiment group members
     *
     * @param integer $id record id of the experiment
     *
     */
    public function actionAddMovedToGroup($id){

        if(isset($_POST)){

            $method = $_POST['toDo'];
            $dataString = $_POST['dataStr'];
            $dataArray = explode('-', $dataString);
            $userModel = new User();
            $userId = $userModel->getUserId();

            if($method == 'add'){

                $experimentGroupExperiment = new ExperimentGroupExperiment();
                $experimentGroupExperiment->experiment_group_id = $dataArray[0];
                $experimentGroupExperiment->experiment_id = $dataArray[1];

                $experimentGroups = ExperimentGroupExperiment::find()->where(['experiment_group_id'=>$dataArray[0], 'is_void'=>false])->all();
                $experimentGroupExperiment->order_number=count($experimentGroups) + 1;
                $experimentGroupExperiment->creator_id=$userId;
                $experimentGroupExperiment->save();
            } else {

                $experimentGroupExperiment = ExperimentGroupExperiment::find()->where(['experiment_group_id'=>$dataArray[0], 'experiment_id'=>$dataArray[1], 'is_void'=>false])->one();
                $experimentGroupExperiment->delete();
            }
        }
    }

    /**
     * Add selected experiment to experiment group
     *
     */
    public function actionAddSelectedToGroup(){

        if(isset($_POST)){

            $experimentGroupId = $_POST['exptGrpId'];
            $experimentIds = array_map('intval', $_POST['selectedIds']);
            $userModel = new User();
            $userId = $userModel->getUserId();

            foreach($experimentIds as $id){

                $experimentGroupExperiment = new ExperimentGroupExperiment();
                $experimentGroupExperiment->experiment_group_id = $experimentGroupId;
                $experimentGroupExperiment->experiment_id = $id;

                $experimentGroups = ExperimentGroupExperiment::find()->where(['experiment_group_id'=>$experimentGroupId, 'is_void'=>false])->all();
                $experimentGroupExperiment->order_number=count($experimentGroups) + 1;
                $experimentGroupExperiment->creator_id=$userId;
                $experimentGroupExperiment->save();
            }

            return json_encode('success');
        }
    }

    /**
     * Renders view for specifying location reps for the experiment
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     *
     */
    public function actionSpecifyOccurrences($program, $id, $processId){  
        $userModel = new User();
        $userId = $userModel->getUserId();
        $selectedIdName = "occ_$userId"."_selected_occurrences"."_$id";
        //destroy selected items session
        if(((isset($_GET['reset']) && $_GET['reset'] == 'hard_reset' && !isset($_GET['page'])) ||
            (!isset($_GET['reset']) && !isset($_GET['page']))) && isset($_SESSION[$selectedIdName])){
            unset($_SESSION[$selectedIdName]);
        }

        $model = $this->mainExperiment->getOne($id)['data'];

        $status = $model['experimentStatus'];
        $experimentType = $model['experimentType'];
        $experimentNotes = $model['notes'];

        $occurrences = $this->occurrenceModel->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "equals $id"]);

        $firstOccurrenceId = isset($occurrences['data'][0]['occurrenceDbId']) ? $occurrences['data'][0]['occurrenceDbId'] : 0;

        $plantingInsRecord = 0;
        $plotRecord = 0;
        if($firstOccurrenceId != 0){
            $plotRecord = $this->occurrenceModel->searchAllPlots($firstOccurrenceId, null, 'limit=1', false)['totalCount'];
            $plantingInsRecord = $this->plantingInstruction->searchAll(["fields"=>"plantingInstruction.id AS plantingInstructionDbId|plot.occurrence_id as occurrenceDbId",'occurrenceDbId' => "equals $firstOccurrenceId"], 'limit=1&sort=plantingInstructionDbId:desc', false)['totalCount'];
        }

        $createPlantingInstruction = false;
        
        if($plotRecord > 0 && $plantingInsRecord > 0){
            $statusArr = explode(';',$status);
            $statusArr = array_filter($statusArr);
            
            if(!in_array('occurrences created',$statusArr)){
                $status = "$status;occurrences created";
            }
        }else{
            if($plotRecord > 0 && $plantingInsRecord == 0 && strpos($model['dataProcessAbbrev'], '_TRIAL_DATA_PROCESS') === false){
                $createPlantingInstruction = true;
            }
            $status = str_replace(';occurrences created', '', $status);
        }
        
        $this->mainExperiment->updateOne($id, ["experimentStatus" => "$status"]);

        $searchModel = $this->occurrenceSearch;
        $filter['params'] = Yii::$app->request->queryParams;
        $storedItems = Yii::$app->session->get('selected_items');

        //retrieve configuration
        $configData = $this->mainExperimentModel->retrieveConfiguration(Yii::$app->controller->action->id, $program, $id, $processId);
        $filter['configData'] = $configData;
     
        $totalCount = $occurrences['totalCount'];

        //Check if the experiment notes has a value plantingArrangementChanged
        $experiment = $this->mainExperiment->searchAll(["fields"=>"experiment.id as experimentDbId|experiment.notes as notes","experimentDbId"=>"equals ".$id]);

        $experimentNotes = isset($experiment['data'][0]['notes']) && ($experiment['data'][0]['notes'] == 'plantingArrangementChanged')  ? $experiment['data'][0]['notes'] : null;
        $deleteFlag = !empty($experimentNotes) ? true : false;

        if($deleteFlag){
            $removeInfo = $this->remove->removeOccurrenceRecords($id, $program, $processId);
            Yii::$app->session->setFlash('info', $removeInfo['message']);
            if($removeInfo['redirect'] == true){
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$id]);
            }
        }
        $model = $this->mainExperiment->getOne($id)['data'];
        $occurrences = $this->occurrenceModel->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "equals ".$id]);
        $totalCount = $occurrences['totalCount'];

        if($totalCount == 0){ // record does not exist
            $siteSpecified = false;
            foreach($configData as $key => $value){

                if($value['variable_abbrev'] == 'SITE' && isset($value['default']) && !empty($value['default'])){
                    $siteInfo = GeospatialObject::searchAll([
                        "fields" => "geospatialObject.id AS geospatialObjectDbId|geospatialObject.geospatial_object_code AS geospatialObjectCode|
                                     geospatialObject.geospatial_object_name AS geospatialObjectName",
                        "geospatialObjectCode" => "equals ".$value['default']
                    ]);

                    $siteDbId = null;
                    if(isset($siteInfo['data'][0]['geospatialObjectDbId'])){
                        $siteDbId = $siteInfo['data'][0]['geospatialObjectDbId'];
                        $siteSpecified = true;
                        $configData[$key]['default'] = $siteInfo['data'][0]['geospatialObjectName'];
                    }
                } 
            }

            $designActivity = $this->formModel->checkDataProcess($processId);
            if($experimentType == 'Observation'){

                $abbrevArray = [
                    'OCCURRENCE_COUNT'
                ];
                $abbrevStr = 'equals '.implode('|equals ', $abbrevArray);
                $expData = ExperimentData::getExperimentData($id, ["abbrev" => $abbrevStr]);
                
                $occurrenceCount = (isset($expData) && !empty($expData)) ? intval($expData[0]['dataValue']) : 1;
                $experimentBlockArray = $this->experimentBlock->searchAll(['experimentDbId' => "equals ".$id]);
                $repCount = ($experimentBlockArray['totalCount'] > 0) ? $experimentBlockArray['data'][0]['noOfReps'] : 1;
                
                for($i=0; $i<$occurrenceCount; $i++){
                    $occNum = $totalCount + ($i+1);
                    $occName = ($occNum < 10) ? '0'.$occNum : $occNum; 
                    
                    $occurrenceStatus = 'draft';
                    if($siteSpecified){
                        $records = [
                            'occurrenceStatus' => $occurrenceStatus,
                            'siteDbId' => "$siteDbId",
                            'experimentDbId' => "$id",
                            'repCount' => "$repCount",
                        ];
                    } else {
                        $records = [
                            'occurrenceStatus' => $occurrenceStatus,
                            'experimentDbId' => "$id",
                            'repCount' => "$repCount"
                        ];
                    }
                    $requestData['records'][] = $records;
                }
            } else {
                $repCount = count($designActivity) == 0 ? 1 : 0;
                $occurrenceName = $model['experimentName'].'#OCC-'.($totalCount + 1);
                $occurrenceStatus = 'draft';
                $records = [
                    'occurrenceStatus' => $occurrenceStatus,
                    'experimentDbId' => "$id",
                    'repCount' => "$repCount",
                ];

                if($siteSpecified){
                    $records = [
                        'occurrenceStatus' => $occurrenceStatus,
                        'siteDbId' => "$siteDbId",
                        'experimentDbId' => "$id",
                        'repCount' => "$repCount",
                    ];
                }
                $requestData['records'][] = $records;
            }

            if(count($requestData['records']) > 20) {
                //call background process for creating occurrences
                $addtlParams = ["description"=>"Creating of occurrence records","entity"=>"OCCURRENCE","entityDbId"=>$id,"endpointEntity"=>"OCCURRENCE", "application"=>"EXPERIMENT_CREATION"];
                $dataArray = [
                    'processName' => 'create-occurrence-records',
                    'experimentDbId' => $id."",
                    'records' => $requestData['records']
                ];

                $create = $this->occurrenceModel->create($dataArray, true, $addtlParams);

                $experimentStatus = $model['experimentStatus'];

                if(!empty($experimentStatus)){
                    $status = explode(';',$experimentStatus);
                    $status = array_filter($status);
                }else{
                    $status = [];
                }

                if(!empty($create['data'][0]) && isset($create['data'][0]['backgroundJobDbId'])) {
                    
                    $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($create['data'][0]['backgroundJobDbId']);

                    if($bgStatus !== 'done' && !empty($bgStatus)){

                        $updatedStatus = implode(';', $status);
                        $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;

                        $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);

                        //update experiment
                        Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The occurrence records for <strong>'.$model['experimentName'].'</strong> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.');
                        Yii::$app->response->redirect(['experimentCreation/create/index?program='.$model['programCode'].'&id='.$id]);
                    }  else {
                        //update worker status; seen and reflected
                        $params = [
                            "jobIsSeen" => "true",
                            "jobRemarks" => "REFLECTED"
                        ];
                        \Yii::$container->get('app\models\BackgroundJob')->update(['backgroundJobDbId'=>$create['data'][0]], $params);

                        // update status of experiment
                        if(!array('occurrences created', $status)){
                            $updatedStatus = implode(';', $status);
                            $status = "$updatedStatus;occurrences created";
                        }
                        $this->mainExperiment->updateOne($id, ["experimentStatus" => $status]);
                    }
                } else {
                    Yii::$app->response->redirect(['experimentCreation/create/index?program='.$model['programCode'].'&id='.$id]);
                }
            } else {
                $this->occurrenceModel->create($requestData);
            }

            if($experimentNotes == 'plantingArrangementChanged') {
                $this->mainExperiment->updateOne($id, ["notes" => null]);
            }
        } else {
            if(strpos($model['dataProcessAbbrev'], '_TRIAL_DATA_PROCESS') === false && $totalCount > 1 && $experimentType != 'Observation'){
                $this->occurrenceModel->deleteOne($firstOccurrenceId);
            }
        }

        // get params for data browser
        $paramPage = '';
        $paramSort = '';
    
        // get current sorting
        if (isset($_GET['sort'])) {
            $paramSort = 'sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }

        if (isset($_GET['dp-1-page'])){
            $paramPage = '&page=' . $_GET['dp-1-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        } else if (isset($_GET['dynagrid-ec-site-list-grid-page'])) {
            $paramPage = '&page=' . $_GET['dynagrid-ec-site-list-grid-page'];
        }
        $paramLimit = '?limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        $pageParams = $paramLimit.$paramPage;
        // append sorting condition
        $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;

        $dataProvider = $searchModel->search($filter, 'experiment creation', $id, null, ['occurrenceName'=>SORT_ASC], $pageParams, 'site tab');

        return $this->render('occurrence',[
            'id' => $id,
            'model' => $model,
            'program' => $program,
            'processId' => $processId,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'selectedItems' => $storedItems,
            'configData' => $configData,
            'deleteFlag' => false,
            'createPlantingInstruction' => $createPlantingInstruction,
            'requiredFields' => $configData,
            'occurrenceCount' => $dataProvider->getTotalCount()
        ]);
    }


    /**
     * Gets the list of facilities
     *
     */
    public function getProcessedFacilityList(){

        $query=NULL;
        $facilities = Place::getFacilityList($query);
        $id = array_column($facilities, 'id');
        $name = array_column($facilities, 'text');
        return array_combine($id, $name);

    }

     /**
     * Gets the list of facilities
     *
     * @param string $q string condition
     * @param integer $i record id of the study
     *
     */
    public function actionGetFacilityList($q=NULL, $id=NULL){

        $tempStudy = TempStudy::findOne($id);
        $site = NULL;
        if($tempStudy['place_id'] != NULL){
            $site = $tempStudy['place_id'];
        }
        $facilities = Place::getFacilityList($q, $site);

        $response['results'] = $facilities;
        return json_encode($response);
    }

    /**
     * Delete created studies from the operational.temp_study and operational.study when related experiment information are changed
     *
     * @param integer $id record id of the experiment
     *
     */
    public function deleteExistingStudies($id, $parentStudy=NULL){

        $getTempStudyIdSql = "with u as (
                    select
                        id,
                        experiment_id
                    from
                        operational.temp_study
                    where
                            experiment_id = {$id}
                   ) select string_agg(u.id::text,',') as temp_study_ids from u";

        $tempStudyIds = Yii::$app->db->createCommand($getTempStudyIdSql)->queryScalar();

        if(!empty($tempStudyIds)){
            $tempStudyDataSql = "DELETE from operational.temp_study_data where temp_study_id in ({$tempStudyIds})";
            Yii::$app->db->createCommand($tempStudyDataSql)->execute();

        }

        //check if there are existing studies created
        $temporaryStudies = TempStudy::find()->where(['experiment_id' => $id])->orderBy(['study_number' => SORT_ASC])->all();
        ExperimentDesign::deleteAll('experiment_id = '.$id);
        $oldStudies = array_column($temporaryStudies, 'operational_study_id');
     
        foreach($temporaryStudies as $temporaryStudy){

            if($temporaryStudy->operational_study_id != NULL || $temporaryStudy->operational_study_id != ''){

                $studyOld = Study::findOne($temporaryStudy->operational_study_id);

                if($studyOld != NULL){

                    FileCabinet::deleteAll("study_id = ".$studyOld->id." and source='randomization'");
                    
                    RandomizationTransaction::deleteAll("study_id = ".$studyOld->id);
                    
                    $crossList = CrossList:: find()->where(['study_id'=>$studyOld->id])->one();
                    
                    //check if there are cross lists referred to the study
                    if($crossList != NULL){
                        //delete temp_entry and temp_plot of old studies
                        if($oldStudies != NULL){
                            TempPlot::deleteAll('study_id IN ('.implode(',', $oldStudies).')');
                            TempEntry::deleteAll('study_id IN ('.implode(',', $oldStudies).')');
                        }
                        GenerateRecordModel::deleteCrossList($crossList['id']);
                    }
                    $studyOld->delete();
                }

            }

            TempStudyData::deleteAll('temp_study_id = '.$temporaryStudy->id);
        }

        //Delete all operational studies
        Study::deleteAll('experiment_id = '.$id);
        

        TempStudy::deleteAll('experiment_id = '.$id);
    }

    /**
     * View members of experiment groups
     *
     */
    public function actionGetExperimentGroup(){

        if(isset($_POST)){

            $experimentGroupId = $_POST['exptGrpId'];
            $includedRecord = ExperimentGroupExperiment::find()->where(['experiment_group_id'=>$experimentGroupId, 'is_void'=>false])->all();
            $includedIds = array_column($includedRecord, 'experiment_id');
            $query = Experiment::find();
            $query->where(['in', 'id', $includedIds]);

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_ASC
                    ]
                ]
            ]);

            $htmlData = $this->renderPartial('view_experiment_group.php',[
                'data' => $dataProvider,
                'experimentGroupId' => $experimentGroupId
            ],true,false);

            return json_encode([
                'htmlData' => $htmlData
            ]);
        }

    }

    /**
     * Check if the occurrences are properly filled out
     *
     * @param Integer $id record id of the experiment
     * @param String $program name of the current program
     *
     */
    public function actionCheckOccurrences($id, $program){
        $getExperimentTotalPlots = 0;
        $response['success'] = true;
        $deleteFlag = false;
        // check if site has been specified in all occurrences
        $occurrenceRecord = $this->occurrenceModel->searchAll(['experimentDbId' => "equals $id", 'siteDbId' => 'is null']);
        if($occurrenceRecord['totalCount'] > 0){

            Yii::$app->session->setFlash('error', '<i class="fa-fw fa fa-times"></i> Please specify the site for each occurrences.');
            $response['success'] = false;
    
        } else {
            $experiment = $this->mainExperiment->getOne($id)['data'];
            $status = $experiment['experimentStatus'];
            $dataProcessAbbrev = $experiment['dataProcessAbbrev'];
            $getExperimentTotalPlots = $this->plantingArrangement->getExperimentTotalPlots($id);
            
            if($dataProcessAbbrev == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
                $statusArr = explode(';', $status);
                $statusArr = array_filter($statusArr);
                if(!array('occurrences created', $statusArr)){
                    $status = "$status;occurrences created";
                }
                $this->mainExperiment->updateOne($id, ["experimentStatus" => $status]);
                $response['proceedNext'] = true;
            }
            else{
                $occurrenceRecord = $this->occurrenceModel->searchAll(['experimentDbId' => "equals ".$id]);
                $occurrenceIds = [];
                if($occurrenceRecord['totalCount'] > 0){
                    $occurrenceIds = array_column($occurrenceRecord['data'], 'occurrenceDbId');
                }
                
                $firstOccurrenceId = isset($occurrenceRecord['data'][0]['occurrenceDbId']) ? $occurrenceRecord['data'][0]['occurrenceDbId'] : 0;
                //Retrieve all plot records of the occurrence
                $plotRecord = $this->occurrenceModel->searchAllPlots($firstOccurrenceId, null, 'limit=1&sort=plotDbId:desc', false)['totalCount'];
                // Retrieve all the planting instruction records of the occurrence
                $plantingInsRecord = $this->plantingInstruction->searchAll(["occurrenceDbId"=>"equals ".implode('|equals ', $occurrenceIds)], 'limit=1&sort=occurrenceDbId:desc', false)['totalCount'];
                
    
                //Check if the experiment notes has a value plantingArrangementChanged
                $experimentCheck = $this->mainExperiment->searchAll(["fields"=>"experiment.id as experimentDbId|experiment.notes as notes|experiment.experiment_status as experimentStatus","experimentDbId"=>"equals ".$id,"notes"=> "plantingArrangementChanged"]);
                $experimentNotes = isset($experimentCheck['data'][0]['notes']) ? $experimentCheck['data'][0]['notes'] : null;
                $deleteFlag = !empty($experimentNotes) ? true : false;
               
                if(strpos($experiment['experimentStatus'], 'occurrences created') === false || $plotRecord == 0 || $plantingInsRecord == 0){
    
                    $response['proceedNext'] = false;

                    if($plotRecord == 0){
                        $response['saveProcess'][] =[
                            'name' => 'plots',
                            'message' => 'Generating Plots...',
                        ];
                    }
                    if($plantingInsRecord == 0){
                        $response['saveProcess'][] =[
                            'name' => 'planting instructions',
                            'message' => 'Generating Planting Intructions...',
                        ];
                    }
                    $response['saveProcess'][] = [
                        'name' => 'experiment status',
                        'message' => 'Updating experiment status...'
                    ];
                } else {
                    $response['proceedNext'] = true;
                }
            }
        }
        
        return json_encode(["deleteFlag"=>$deleteFlag,"response"=>$response,"getExperimentTotalPlots"=>$getExperimentTotalPlots]);
    }


    /**
     * Update facility for location reps
     *
     * @param integer $id record id of the study
     * @param string $program name of the current program
     */
    public function actionUpdateLocationRepFacility($id, $program){

        extract($_POST);

        if(isset($locationRepId)){
            TempStudy::findOne((int)$locationRepId);
        }

        $facility = Facility::findOne((int) $facilityId);
        $place = Place::findOne($facility['place_id']);
        return json_encode(array(
            'placeName' => $place['display_name'],
            'placeId' => $place['id']
        ));
    }

   /**
     * Renders view for location list
     */
    public function actionGetLocationList($id, $program, $processId){

        $userModel = new User();
        $userId = $userModel->getUserId();

        $savedList = ExperimentModel::getSavedList($userId);
        $query = PlatformList::find();
        $query->where(['in', 'id', $savedList]);
        $query->andFilterWhere(['type'=>'location']);

        $data = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'abbrev' => SORT_ASC,
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $data->pagination  = false;

        $htmlData = $this->renderPartial('location_list',[
            'data' => $data,
            'id' => $id,
            'program' => $program,
            'processId' => $processId
        ],true,false);

        return json_encode($htmlData);
    }

    /**
     * Saved selected location to a location list
     */
    public function actionSaveLocationList($id, $program, $processId){

        if(isset($_POST)){

            extract($_POST);

            $experimentName = $nameVal;
            $abbrev = $abbrevVal;

            //Save location list
            $savedList = new PlatformList();
            $savedList->abbrev = $abbrev;
            $savedList->name = $experimentName;
            $savedList->display_name = $experimentName;
            $savedList->type = 'location';
            $savedList->creator_id = User::getUserId();

            $entityId = ListModel::getEntityIdByListType($savedList->type);
            $savedList->entity_id = $entityId;

            $savedList->save();

            $locationString = implode(',', $location);

            //delete existing locations for the
            PlatformListMember::deleteAll("list_id = ".$savedList->id);

            //Save location list members
            ExperimentModel::saveLocationList($savedList->id, $locationString);

            Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully created location saved list.');
        }

        extract($_GET);
        return $this->redirect(['create/specify-occurrences?id='.$id.'&program='.$program.'&processId='.$processId]);
    }

    /**
     * Render location list members
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     */
    public function actionRenderLocationListMembers($id, $program, $processId){

        if(isset($_POST)){

            extract($_POST);

            $savedList = PlatformList::findOne($savedListId);
            $experiment = Experiment::findOne($currentExperimentId);

            if($type == 'selection'){

                $query = PlatformListMember::find()->where(['list_id'=>$savedListId, 'is_void'=>false]);
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    'sort' => [
                        'defaultOrder' => [
                            'order_number' => SORT_ASC
                        ]
                    ]
                ]);
                $dataProvider->pagination  = false;

                $tempStudies = TempStudy::find()->where('experiment_id = '.$id.' and place_id is NULL and is_void=false')->all();
                $htmlData = $this->renderPartial('location_members', [
                    'dataProvider' => $dataProvider,
                    'savedList' => $savedList,
                    'experiment' => $experiment,
                    'id' => $id,
                    'program' => $program,
                    'woLocation' => count($tempStudies),
                    'processId' =>$processId
                ]);

            } else {

                $sql = "
                    select data_id from platform.list_member where list_id = {$savedListId} and is_void = false
                ";
                $listIdArr = Yii::$app->db->createCommand($sql)->queryColumn();

                $query = Place::find();
                $query->where(['not in', 'id', $listIdArr]);
                $query->andFilterWhere(['is_void'=>false]);
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    'sort' => [
                        'defaultOrder' => [
                            'id' => SORT_DESC
                        ]
                    ]
                ]);

                $dataProvider->pagination  = false;

                $htmlData = $this->renderPartial('_excluded_locations', [
                    'dataProvider' => $dataProvider,
                    'savedList' => $savedList,
                    'experiment' => $experiment,
                    'id' => $id,
                    'program' => $program,
                    'processId' =>$processId
                ]);
            }

            return json_encode($htmlData);
        }

    }

     /**
     * Render creation of location list
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     */
    public function actionCreateLocationList($id, $program, $processId){

        $query = Place::find();
        $query->where(['is_void'=>false]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $dataProvider->pagination  = false;

        $htmlData = $this->renderPartial('_create_location_list', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'program' => $program,
            'processId' =>$processId
        ]);

        return json_encode($htmlData);
    }

    /**
     * Save selected locations to location reps
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     */
    public function actionSaveLocationToReps($program, $id){

        if(isset($_POST)){

            extract($_POST);

            $allLocationReps = TempStudy::find()->where(['experiment_id'=>$id, 'is_void'=>false])->orderBy(['study_number' => SORT_ASC])->all();

            if($populateRecord == 'populate-empty'){

                $locationReps = TempStudy::find()->where('experiment_id = '.$id.' and place_id is NULL and is_void=false')->orderBy(['study_number' => SORT_ASC])->all();
            } else{

                $locationReps = TempStudy::find()->where(['experiment_id'=>$id, 'is_void'=>false])->orderBy(['study_number' => SORT_ASC])->all();
            }

            $index = 0;

            foreach($locationReps as $locationRecord){

                if($index != count($selectedArray)){

                    $locationRecord->place_id = $selectedArray[$index++];
                    $locationRecord->save();
                } else break;
            }

            Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully updated <b>'.$index.'/'.count($allLocationReps).' Location Rep records </b>.');

        }
        extract($_GET);
        return $this->redirect(['create/specify-occurrences?id='.$id.'&program='.$program.'&processId='.$processId]);
    }

     /**
     * Save selected locations to location reps
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     */
    public function actionSaveLocationToList($program, $id, $processId){

        if(isset($_POST)){

            extract($_POST);

            $locationString = implode(',', $selectedArray);

            //Save location list members
            ExperimentModel::saveLocationToList($savedListId, $locationString);

            Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully updated Location list.');
        }

        return $this->redirect(['create/specify-occurrences?id='.$id.'&program='.$program.'&processId='.$processId]);
    }

    /**
     * Generate the following experiment records:
     * - plots
     * - planting instructions
     * 
     * @param String $program name of the current program
     * @param Integer $id record identifier of the experiment
     * 
     */
    public function actionGenerateRecords($program, $id){
        $model = Experiment::getOne($id)['data'];
        $status = $model['experimentStatus'];
        $plotRecords = [];

        $saveProcess = isset($_GET['saveProcess']) ? $_GET['saveProcess'] : '';
        $occurrenceRecords = Occurrence::searchAll(['experimentDbId' => "$id"]);
        $occurrences = $occurrenceRecords['data'];
        $success = true;

        $statusArr = explode(';', $status);
        $statusArr = array_filter($statusArr);
        if(!array('occurrences created', $statusArr)){
            foreach($occurrences as $occurrence){
                $occurrenceDbId = $occurrence['occurrenceDbId'];
                //saving plots
                if($saveProcess == 'plots'){
                    try {
                      
                        //check if experiment is trial
                        if(strpos($model['dataProcessAbbrev'], '_TRIAL_DATA_PROCESS') !== false){
                            $plotRecords['success'] = true;
                        } else {
                            $plotRecords = ExperimentModel::generatePlots($occurrenceDbId, $id);
                        }
                        if(!$plotRecords['success']){
                            $return = [
                                'success' => false,
                                'message' => 'Problem in generating plots. Missing layout order data (e.g. design_x, design_y, etc).',
                                'currentProcess' => $saveProcess
                            ];

                            return json_encode($return);

                        }else{
                            $return = [
                                'success' => $success,
                                'message' => 'Successfully generated plots.',
                                'currentProcess' => $saveProcess
                            ];
                        }

                    } catch (Exception $error){

                        $return = [
                            'success' => false,
                            'message' => 'Problem in generating plots.<br> <em>Additional Information: ' .$error.'</em>',
                            'currentProcess' => $saveProcess
                        ];

                        return json_encode($return);
                    }   
                }
                //saving planting instructions
                if($saveProcess == 'planting instructions'){
                    try {
                        $plotRecords = Occurrence::searchAllPlots($occurrenceDbId, null, '', true)['data'][0]['plots'];        
                        ExperimentModel::generatePlantingInstructions($plotRecords, $id);
                        $return = [
                            'success' => $success,
                            'message' => "Successfully generated $saveProcess.",
                            'currentProcess' => $saveProcess
                        ];

                    } catch (Exception $error){

                        $return = [
                            'success' => false,
                            'message' => "Problem in generating $saveProcess.<br> <em>Additional Information: $error</em>",
                            'currentProcess' => $saveProcess
                        ];

                        return json_encode($return);
                    }
                }

                if($saveProcess == 'experiment status'){
                    try{
                        $plotRecord = Occurrence::searchAllPlots($occurrenceDbId, null, 'limit=1&sort=plotDbId:desc', false)['totalCount'];        
                        $plantingInsRecord = PlantingInstruction::searchAll(['occurrenceDbId' => "equals $occurrenceDbId"], 'limit=1&sort=occurrenceDbId:desc', false)['totalCount'];

                        if($plotRecord > 0 && $plantingInsRecord > 0){

                            Experiment::updateOne($id, ["experimentStatus" => $status.';occurrences created']);
                            $return = [
                                'success' => $success,
                                'message' => 'done',
                                'currentProcess' => $saveProcess
                            ];

                        }else{
                            $plot = $plotRecord == 0 ? 'plot' : '';
                            $plantingIns = $plantingInsRecord == 0 ? 'planting instruction' : '';
                            $and = $plotRecord == 0 && $plantingInsRecord == 0 ? ' and ' : '';
                            $return = [
                                'success' => false,
                                'message' => "Problem in updating experiment status.<br> <em>Additional Information: Missing $plot".$and."$plantingIns records.</em>",
                                'currentProcess' => $saveProcess
                            ];

                            return json_encode($return);

                        }

                    } catch (Exception $error){
    
                        $return = [
                            'success' => false,
                            'message' => 'Problem in updating experiment status.<br> <em>Additional Information: ' .$error.'</em>',
                            'currentProcess' => $saveProcess
                        ];
    
                        return json_encode($return);
                    }
                }
            }
        }else{
            $return = [
                'success' => $success,
                'message' => 'done',
                'currentProcess' => $saveProcess
            ];
        }

        return json_encode($return);
    }

    /**
     * Renders preview for the experiment creation
     *
     * @param String $program name of the current program
     * @param Integer $id record id of the experiment
     * @param Integer $process process record identifier
     *
     */
    public function actionReview($program, $id, $processId){

        $model = $this->mainExperiment->getOne($id)['data'];

        if($model['experimentStatus'] != 'created'){
            $dataProcessAbbrev = $model['dataProcessAbbrev'];
            $parentActAbbrev = str_replace('_DATA_PROCESS', '', $dataProcessAbbrev);
            $entryListActAbbrev = $parentActAbbrev.'_ENTRY_LIST_ACT';
            $crossesActAbbrev = $parentActAbbrev.'_CROSSES_ACT';
        

             // get occurrences
            $occurrenceRecords = $this->occurrenceModel->searchAll([
                'experimentDbId' => "equals $id",
                'fields' => 'occurrence.id AS occurrenceDbId|experiment.id AS experimentDbId|occurrence.occurrence_code AS occurrenceCode|occurrence.occurrence_name AS occurrenceName',
            ]);
            $idList = array_column($occurrenceRecords['data'], 'occurrenceDbId');

            //populate entry and plot count
            $userModel = new User();
            $userId = $userModel->getUserId();
            
            $occurrenceDataProvider = $this->occurrenceModel->getDataProvider($id, null, [], true, ['occurrenceName'=>SORT_ASC]);
            $occurrenceDbId = $occurrenceRecords['data'][0]['occurrenceDbId'];
            $occurrenceCount = $occurrenceDataProvider['dataProvider']->getTotalCount();
            $occurrenceWarning = $occurrenceCount ? '' : 'Occurrence';

            // get entries/parents
            $entryListAct = $this->mainItemModel->getAll("abbrev=$entryListActAbbrev");
            $displayName = isset($entryListAct['data'][0]['displayName']) ? strtolower($entryListAct['data'][0]['displayName']) : 'entry list';
            $entryListActValues = $this->mainConfigModel->getAll("abbrev=$entryListActAbbrev".'_VAL');
            $varConfigValues = $entryListActValues['data'][0]['configValue']['Values'];

            //For the entry/parent list browser 
            // get params for data browser
            $paramPage = '';
            $paramSort = '';
            if (isset($_GET['dp-1-page'])){
                $paramPage = '&page=' . $_GET['dp-1-page'];
            } else if (isset($_GET['page'])) {
                $paramPage = '&page=' . $_GET['page'];
            } else if (isset($_GET['grid-entry-list-grid-page'])) {
                $paramPage = '&page=' . $_GET['grid-entry-list-grid-page'];
            }
            $paramLimit = '?limit='.getenv('CB_DATA_BROWSER_DEFAULT_PAGE_SIZE');

            //sorting
            if(isset($_GET['sort'], $_GET['_pjax']) && $_GET['_pjax'] == '#grid-review-entry-list-pjax'){
                if(strpos($_GET['sort'],'-') !== false){
                    $sort = str_replace('-', '', $_GET['sort']);
                    $paramSort = "&sort=$sort:DESC";
                }
                else{
                    $paramSort = '&sort='.$_GET['sort'];
                }
            }

            $pageParams = $paramLimit.$paramPage.$paramSort;
            $entryDataProvider = $this->entryModel->getDataProvider($id, $varConfigValues, null, true, null,$pageParams);
            
            $configReviewColumns = $this->copyExperiment->getReviewColumns($varConfigValues);
            $entryCount = $entryDataProvider['dataProvider']->getTotalCount();
            $entryWarning = $entryCount ? '' : 'Entry';

            // get crosses
            $crossesActArr = $this->mainItemModel->getAll("abbrev=$crossesActAbbrev");
            $crossesAct = isset($crossesActArr['data'][0]) ? true : false;
            
            $crossDataProvider = null;
            $crossWarning = '';
           
            if($crossesAct){
                //check if there are missing seedDbIds for cross parents
                $crossParentCount = $this->mainExperimentModel->updateCrossParents($id);
                
                $searchModel = $this->crossSearch;
                $paramFilter = Yii::$app->request->queryParams;

                $mainParam = ["experimentDbId"=>"equals $id"];

                $paramSort = '';
                //sorting
                if(isset($_GET['sort'], $_GET['_pjax']) && $_GET['_pjax'] == '#grid-review-cross-list-pjax'){
                    if(strpos($_GET['sort'],'-') !== false){
                        $sort = str_replace('-', '', $_GET['sort']);
                        $paramSort = "&sort=$sort:DESC";
                    }
                    else{
                        $paramSort = '&sort='.$_GET['sort'];
                    }
                }

                $pageParams = $paramSort;

                $crossDataProvider = $searchModel->search($mainParam, $paramFilter, true, null, true);
                $crossCount = $crossDataProvider->getTotalCount();
                $crossWarning = $crossCount ? '' : 'Cross';
                if(!empty($entryWarning) && !empty($crossWarning)){
                    $occurrenceWarning = ', Cross';
                }
            }

            //check if complete package sources(for copied experiment)
            if(strpos($model['experimentStatus'],'entry list incomplete seed sources') === false ){
                $entryListId = $this->entryListModel->getEntryListId($id);
                $filterNoPackages = [
                    "fields" => "entry.id as entryDbId|entry.entry_list_id AS entryListDbId|entry.package_id AS packageDbId",
                    "packageDbId" => "is null",
                    "entryListDbId"=>"equals ".$entryListId
                ];
                $noPackageRecords = Entry::searchRecords($filterNoPackages);
                
                if(count($noPackageRecords) > 0 ){
                    $status = explode(';', $model['experimentStatus']);
                    $status = array_filter($status);
                    
                    if(in_array('draft', $status)){
                        $index = array_search('draft', $status);
                        unset($status[$index]);
                    }
                    if(in_array('entry list created', $status)){
                        $index = array_search('entry list created', $status);
                        
                        if(isset($status[$index])){
                            $status[$index] = 'entry list incomplete seed sources';
                            
                            if(empty($status)){
                                $newStatus = "draft";
                            }else{
                                $newStatus = implode(';', $status);
                            }
                            $model['experimentStatus'] = $newStatus;
                        }
                        
                        $status = !empty($model['experimentStatus']) ? $model['experimentStatus'] : 'draft';
                        $this->mainExperiment->updateOne($id, ["experimentStatus"=>$status]);
                    }
                }
            }
            
            if((!empty($entryWarning) || !empty($crossWarning)) && !empty($occurrenceWarning)){
                $occurrenceWarning = ', Occurrence';
            }

            // get plots
            $plotCount = $this->occurrenceModel->searchAllPlots($occurrenceDbId, null,'limit=1&sort=plotDbId:asc', false)['totalCount'];
            $plotWarning = $plotCount ? '' : 'Plot';
            if((!empty($entryWarning) || !empty($crossWarning) || !empty($occurrenceWarning)) && !empty($plotWarning)){
                $plotWarning = ', Plot';
            }

            //get planting arrangement count
            // Retrieve all the planting instruction records of the occurrence
            $plantingInsRecord = $this->plantingInstruction->searchAll(["fields"=>"plantingInstruction.id AS plantingInstructionDbId|plot.occurrence_id as occurrenceDbId", "occurrenceDbId"=>"equals ".$occurrenceDbId], 'limit=1', false)['totalCount'];

            $plotPlantRecordNote = '';
            if($plotCount != $plantingInsRecord){
               $plotPlantRecordNote = '<i class="fa-fw fa fa-warning txt-color-orangeDark font-md"></i> There is a problem with the generated plots and planting instruction. Kindly check.';
            }

            $warning = ($dataProcessAbbrev == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS') ? '' : $entryWarning.$occurrenceWarning.$plotWarning;

            //double check for incomplete seed sources
            $entryListId = $this->entryListModel->getEntryListId($id);

            //Check if all entries has seeds, then add entry list created to status
            $filterNoPackages = [
                "fields" => "entry.entry_list_id AS entryListDbId|entry.seed_id as seedDbId|entry.package_id as packageDbId",
                "packageDbId" => "is null",
                "entryListDbId"=>"equals $entryListId",
            ];

            $noPackageRecords = $this->entryModel->searchAll($filterNoPackages, 'limit=1&sort=seedDbId:asc', false);

            if($noPackageRecords['totalCount'] > 0 && strpos($model['experimentStatus'],'entry list incomplete seed sources') !== false){ //update entry list status
                $status = explode(';', $model['experimentStatus']);
                $index = array_search('entry list created', $status);
                            
                if(isset($status[$index])){
                    $status[$index] = 'entry list incomplete seed sources';
                }
                $model['experimentStatus'] = implode(';', $status);
                $this->mainExperiment->updateOne($id, ["experimentStatus"=>$model['experimentStatus']]);
            }

            $packageSourceNote = '';
            if(strpos($model['experimentStatus'],'entry list incomplete seed sources') !== false ){
                $packageSourceNote = '<i class="fa-fw fa fa-warning txt-color-orangeDark font-md"></i> Seed source records are missing.';
            }

            //update planting instruction records when there's no seed_id and package_id
            $updateRecords = $this->plantingInstruction->updatePopulatedRecords($idList, $id);
            if($updateRecords['success'] == false){

                Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning txt-color-orangeDark font-md"></i>'." Problem in updating the planting instruction records.");

            } else {
                if($updateRecords['message'] == 'redirect'){
                    Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The planting instruction records for <strong>'.$model['experimentName'].'</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.');
                    $this->redirect(['create/index?program='.$program.'&id='.$id]);
                }
            }

            return $this->render('review',[
                'model' => $model,
                'program' => $program,
                'id' => $id,
                'processId' => $processId,
                'entryDataProvider' => $entryDataProvider['dataProvider'],
                'crossDataProvider' => $crossDataProvider,
                'occurrenceDataProvider' => $occurrenceDataProvider['dataProvider'],
                'configReviewColumns' => $configReviewColumns,
                'displayName' => $displayName,
                'warning' => $warning,
                'packageSourceNote' => $packageSourceNote,
                'plotCount' => $plotCount,
                'exportRecordsThresholdValue' => Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportRecords'),
                'plotPlantRecordNote' => $plotPlantRecordNote
            ]);

        }else{
            Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning txt-color-orangeDark font-md"></i>'." The experiment that you're trying to access is already finalized.");
            $this->redirect(['create/index?program='.$program.'&id='.$id]);
        }
    }


    /**
     * Finalize experiment to prevent further changes
     *
     * @param Integer $id record id of the experiment
     * @param String $program name of the current program
     *
     */
    public function actionFinalizeExperiment($id, $program){

        $experiment = $this->findModel($id);
        $dataProcessAbbrev = $experiment['dataProcessAbbrev'];
        
        if($dataProcessAbbrev == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
            $status = 'planted;crossed';
        }
        else{
            $status = 'created';
            //update entry list status
            $entryListDbId = $this->entryListModel->searchAll(['experimentDbId' => "equals $id"])['data'][0]['entryListDbId'];

            $this->entryListModel->updateOne($entryListDbId, ['entryListStatus' => 'completed']);

            if($dataProcessAbbrev == 'GENERATION_NURSERY_DATA_PROCESS'){
                $this->cross->createSelfCrosses(null, $id);
            }
        }

        // update occurrence status
        $occurrenceRecords = $this->occurrenceModel->searchAll([
            'experimentDbId' => "equals $id",
            'fields' => 'occurrence.id AS occurrenceDbId|experiment.id AS experimentDbId|occurrence.occurrence_code AS occurrenceCode|occurrence.occurrence_name AS occurrenceName|occurrence.site_id as siteDbId',
        ]);
        if($dataProcessAbbrev == 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS'){
            // populate experiment_year, season_id, site_id, cross_count
            $this->entryListModel->updateOne($entryListDbId, ['experimentYear'=>$experiment['experimentYear']."", 'seasonDbId' => $experiment['seasonDbId']."", 'siteDbId'=>$occurrenceRecords['data'][0]['siteDbId']."", 'entryListStatus' => 'cross list specified']);
            $this->crossListModel->updateCrossCount($entryListDbId);
        }

        // Save entry list-level protocol for ICN cross lists
        if ($dataProcessAbbrev === 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') {
            $listId = $this->entryListProtocolModel->addList($entryListDbId);

            // Only Trait protocol in EC
            $traits = $this->protocolModel->getSavedTraits($id);
            if (count($traits) === 0) {
                // Use default from trait configuration, if no trait specified
                $traits = $this->entryListProtocolModel->getTraitsFromConfig($program);
            }
            foreach ($traits as $trait) {
                $this->entryListProtocolModel->addListMember($listId, $trait['id']);
            }

            $requestData['records'][] = $this->entryListProtocolModel->getEntryListProtocols($entryListDbId, $listId);
            $result = $this->entryListDataModel->create($requestData);
            Yii::debug('Created entry list data ' . json_encode($result), __METHOD__);
        }


        // update experiment status
        $this->mainExperiment->updateOne($id, ['experimentStatus' => $status]);

        //----START for background job worker
        $idList = array_column($occurrenceRecords['data'], 'occurrenceDbId');

        if(count($idList) >= 50){
            $addtlParams = ["description"=>"Finalizing experiment creation","entity"=>"EXPERIMENT","entityDbId"=>$id,"endpointEntity"=>"EXPERIMENT", "application"=>"EXPERIMENT_CREATION"];
            $dataArray = [
                'occurrenceIds'=>$idList,
                'processName' => 'finalize-experiment',
                'experimentDbId' => $id.""
            ];
            $insertedRecord = $this->mainExperiment->create($dataArray, true, $addtlParams);

            //Redirect to main page
            $experimentStatus = isset($experiment['experimentStatus']) ? $experiment['experimentStatus'] : '';
          
            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }

            $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($insertedRecord['data'][0]['backgroundJobDbId']);
            
            if($bgStatus !== 'done' && !empty($bgStatus)){
                $updatedStatus = implode(';', $status);
                $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
            
                $this->mainExperiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);

                //update experiment
                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> Finalizing <strong>'.$experiment['experimentName'].'</strong> Records are being created in the background. You may proceed with other tasks. You will be notified in this page once done.');
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$id]);
            } else {
                Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully finalized experiment. You may now view the experiments in the '.Html::a('Experiment Manager browser', ['/occurrence', 'program' => $program]).'.');
            }
            
        } else {

            //Save the protocol in the occurrence data
            $protocolData = $this->protocolModel->getExperimentProtocols($id, $occurrenceRecords['data']);

            if(count($protocolData) > 0){
                $record['records'] = $protocolData;
                $this->occurrenceDataModel->create($record);
            }

            $this->occurrenceModel->updateMany($idList, ['occurrenceStatus' => $status]);

            if(!empty($experiment['experimentDesignType'])){
                $this->occurrenceModel->updateMany($idList, ["occurrenceDesignType"=>$experiment['experimentDesignType']]);
            }
            Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully finalized experiment. You may now view the experiments in the '.Html::a('Experiment Manager browser', ['/occurrence', 'program' => $program]).'.');
        }
        //---END for background job worker
        $this->redirect(['create/index?program='.$program.'&id='.$id]);
    }

    /**
     * View for protocols tab
     *
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     *
     */
    public function actionSpecifyProtocols($id, $program, $processId){

        $model = $this->findModel($id);

        $dataProcessAbbrev = isset($model['dataProcessAbbrev']) ? $model['dataProcessAbbrev'] : '';

        $experimentType = $model['experimentType'];
        /**
         * TODO: This condition should be updated: ALL entries should be assigned to at least 1 block before allowing the user to the next step
         */
        if(($dataProcessAbbrev !== 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS' || (stripos($experimentType,'nursery') === false && !empty($entryCountAssignedToBlock))) || stripos($experimentType,'trial') === false){

            $activities = $this->formModel->prepare(Yii::$app->controller->action->id,$program,$id,$processId);
            
            $index = array_search($activities['active'], array_column($activities['children'],'url'));    

            $activityChildren = $activities['children'][$index]['children'];
           
            $viewArr = [];
            
            if(!empty($activityChildren) && sizeof($activityChildren) > 0){
               
               foreach($activityChildren as $value){
                    $viewArr[] = [
                        'display_name'=> $value['display_name'],
                        'action_id'=>$value['action_id'], 
                        'item_icon' => $value['item_icon']
                    ];
               }
            }
            
            return $this->render('/protocol/specify_protocols',[
                'id'=> $id,
                'program' => $program,
                'processId' => $processId,
                'viewArr' => $viewArr,
                'model' => $model,
                'protocolChildren' => $activityChildren
            ]);
        }else{
            $this->redirect(['create/specify-cross-design?id='.$id.'&program='.$program.'&processId='.$processId]);
        }
    
    }

    /**
     * Save entries from input list to experiment's entry list
     *
     */
    public function actionAddEntriesFromList(){

        extract($_GET);

        if(isset($_POST)){

            extract($_POST);
            
            $productList = str_replace('\\', '$', $productList);
            $productList = json_decode($productList, true);

            // retrieve default values
            // TODO: make this more dynamic to support future variables
            $configData = $this->mainExperimentModel->retrieveConfiguration('specify-entry-list', $program, $experimentId, $processId);

            foreach($configData as $config){
                if(isset($config['default']) && !empty($config['default'])){
                    switch($config['variable_abbrev']){
                        case 'ENTRY_TYPE':
                            $entryType = $config['default'];
                            break;
                        case 'ENTRY_CLASS':
                            $entryClass = $config['default'];
                            break;
                        case 'ENTRY_ROLE':
                            $entryRole = $config['default'];
                            break;
                        default:
                            break;
                    }
                }
            }

            //Add entries
            //search for entry list
          
            $entryListId = $this->entryListModel->getEntryListId($experimentId,true);
            $createRecords = NULL;
            foreach($productList as $product){
                $germplasmId = $product['germplasmDbId'];
                $temp = [
                  'entryName' => $product['designation'],
                  'entryType' => !empty($entryType) ? $entryType : 'test',
                  'entryStatus' => 'active',
                  'entryListDbId' => "$entryListId",
                  'germplasmDbId' => "$germplasmId"
                ];
                
                if(!empty($entryClass)){
                    $temp['entryClass'] = $entryClass;
                }
                if(!empty($entryRole)){
                    $temp['entryRole'] = $entryRole;
                }
                $createRecords[] = $temp;
            }
            
             //Code Preparation
            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries') : 500;
            $createAddtlParams = ["description"=>"Creation of entries","entity"=>"EXPERIMENT","entityDbId"=>$experimentId,"endpointEntity"=>"ENTRY", "application"=>"EXPERIMENT_CREATION"];

            //add notes for BT experiment
            $this->mainExperimentModel->addDesignNoteForUpdate($experimentId, "updated entries");

            if(count($createRecords) >= $bgThreshold){
                $userModel = new User();
                $userId = $userModel->getUserId();

                $worker = $this->worker->invoke(
                    'CreateEntryRecords',               // Worker name
                    'Creation of entries',              // Description
                    'EXPERIMENT',                       // Entity
                    $id,                                // Entity ID
                    'ENTRY',                            // Endpoint Entity
                    'EXPERIMENT_CREATION',              // Application
                    'POST',                             // Method
                    [                                   // Worker data
                        'requestData' => [
                            'experimentDbId' => $id,
                            'processName' => 'create-entry-records',
                            'entityId' => '$id',
                            'entity' => 'inputlist',
                            'createRecords' => $createRecords,
                            'defaultValues' => ['entryListDbId' => $entryListId],
                        ],
                        'personDbId'=>$userId
                    ]
                );

                $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($worker['backgroundJobId']);

                if($bgStatus !== 'done' && !empty($bgStatus)){
                    $this->redirect(['create/index?program='.$program.'&id='.$id]);
                }
            }else{
                $createdEntryIds = $this->entryModel->create(["records"=>$createRecords]);

                $this->entryOrderModel->entryListAfterUpdate($experimentId, $createdEntryIds);

                $this->mainExperimentModel->autoSelectSinglePackage($entryListId, $experimentId);
            }
        }

        $prevCols = Yii::$app->session['EntryListSearchSession'.$id];

        $params_temp = '';

        if(!empty($prevCols)){
            foreach ($prevCols as $key => $value) {
                $entities = trim(urlencode('EntryListSearch'.'['.$key.']'));
                $entity_value = $entities.'='.$value;
                $params_temp = empty($params_temp) ?  $entity_value : $params_temp. '&'.$entity_value;
            }
        }
        return json_encode(true);
    }

    /**
     * Save entries from input list to experiment's entry list
     *
     */
    public function actionAddEntriesFromPackageList(){

        extract($_GET);

        if(isset($_POST)){

            extract($_POST);
            
            $productList = str_replace('\\', '$', $productList);
            $productList = json_decode($productList, true);

            // retrieve default values
            // TODO: make this more dynamic to support future variables
            $configData = $this->mainExperimentModel->retrieveConfiguration('specify-entry-list', $program, $experimentId, $processId);

            foreach($configData as $config){
                if(isset($config['default']) && !empty($config['default'])){
                    switch($config['variable_abbrev']){
                        case 'ENTRY_TYPE':
                            $entryType = $config['default'];
                            break;
                        case 'ENTRY_CLASS':
                            $entryClass = $config['default'];
                            break;
                        case 'ENTRY_ROLE':
                            $entryRole = $config['default'];
                            break;
                        default:
                            break;
                    }
                }
            }

            //Add entries
            //search for entry list
          
            $entryListId = $this->entryListModel->getEntryListId($experimentId,true);
            $createRecords = NULL;

            foreach($productList as $product){
                $germplasmId = $product['germplasmDbId'];
                $seedDbId = $product['seedDbId'];
                $packageDbId = $product['packageDbId'];
                $temp = [
                  'entryName' => $product['designation'],
                  'entryType' => !empty($entryType) ? $entryType : 'test',
                  'entryStatus' => 'active',
                  'entryListDbId' => "$entryListId",
                  'germplasmDbId' => "$germplasmId",
                  'seedDbId' => "$seedDbId",
                  'packageDbId' => "$packageDbId"
                ];
                
                if(!empty($entryClass)){
                    $temp['entryClass'] = $entryClass;
                }
                if(!empty($entryRole)){
                    $temp['entryRole'] = $entryRole;
                }
                $createRecords[] = $temp;
            }
            
             //Code Preparation
            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries') : 500;
            $createAddtlParams = ["description"=>"Creation of entries","entity"=>"EXPERIMENT","entityDbId"=>$experimentId,"endpointEntity"=>"ENTRY", "application"=>"EXPERIMENT_CREATION"];

            //add notes for BT experiment
            $this->mainExperimentModel->addDesignNoteForUpdate($experimentId, "updated entries");

            if(count($createRecords) >= $bgThreshold){
                $userModel = new User();
                $userId = $userModel->getUserId();
                
                $worker = $this->worker->invoke(
                    'CreateEntryRecords',               // Worker name
                    'Creation of entries',              // Description
                    'EXPERIMENT',                       // Entity
                    $id,                                // Entity ID
                    'ENTRY',                            // Endpoint Entity
                    'EXPERIMENT_CREATION',              // Application
                    'POST',                             // Method
                    [                                   // Worker data
                        'requestData' => [
                            'experimentDbId' => $id,
                            'processName' => 'create-entry-records',
                            'entityId' => '$id',
                            'entity' => 'inputlist',
                            'createRecords' => $createRecords,
                            'defaultValues' => ['entryListDbId' => $entryListId],
                        ],
                        'personDbId'=>$userId
                    ]
                );

                $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($worker['backgroundJobId']);

                if($bgStatus !== 'done' && !empty($bgStatus)){
                    $this->redirect(['create/index?program='.$program.'&id='.$id]);
                }
            }else{
                $createdEntryIds = $this->entryModel->create(["records"=>$createRecords]);

                $this->entryOrderModel->entryListAfterUpdate($experimentId, $createdEntryIds);

                $this->mainExperimentModel->autoSelectSinglePackage($entryListId, $experimentId);
            }
        }

        $prevCols = Yii::$app->session['EntryListSearchSession'.$id];

        $params_temp = '';

        if(!empty($prevCols)){
            foreach ($prevCols as $key => $value) {
                $entities = trim(urlencode('EntryListSearch'.'['.$key.']'));
                $entity_value = $entities.'='.$value;
                $params_temp = empty($params_temp) ?  $entity_value : $params_temp. '&'.$entity_value;
            }
        }
        return json_encode(true);
    }

    /**
     * Search products from input list
     *
     */
    public function actionSearchProductList(){

        if(isset($_POST)){

            extract($_GET);

            extract($_POST);

            $inputListSplitArr = array_map('trim', array_filter(preg_split("/[\r\n|,|\n]/", $productList)));

            $inputListSplitArr = array_map('strtoupper', $inputListSplitArr);
            $exceededValidationLimitMessage = '';
            $inputListLimitBgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'inputListLimit')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'inputListLimit') : 1000;
            if(count($inputListSplitArr) > $inputListLimitBgThreshold) {
                $inputListSplitArr = array_slice($inputListSplitArr, 0, $inputListLimitBgThreshold);
                $exceededValidationLimitMessage = "You have exceeded the maximum $inputListLimitBgThreshold records for validation. Only the first $inputListLimitBgThreshold records are validated.";
            }

            $experiment = $this->mainExperiment->getExperiment($experimentId);
            $sqlData = [
              'normInputProductNameResults' => $inputListSplitArr,
              'dataProcessAbbrev' => $experiment['dataProcessAbbrev']
            ];

            $germplasmResults = $this->mainExperimentModel->searchProductList('search', $sqlData);

            $germplasmCounterMax = 0;
            //check to show notification of duplicate germplasm
            if(isset($germplasmResults['germplasmCounter']) && count($germplasmResults['germplasmCounter']) > 0){
                $germplasmCounterMax = max(array_values($germplasmResults['germplasmCounter']));
            }
            
            $dataProvider = new ArrayDataProvider([
                'key'=>'germplasmDbId',
                'allModels' => $germplasmResults['arrangedArray'],
                'pagination'=>false,
                'sort' => [
                    'attributes' => ['germplasmDbId', 'germplasmNormalizedName', 'inputProductName','designation', 'orderNumber'],
                    'defaultOrder' => [
                        'orderNumber'=>SORT_ASC
                    ]
                ]
            ]);
            
            $htmlData = $this->renderPartial('input_list_products', [
                'dataProvider'=>$dataProvider,
                'id'=>$id,
                'program'=>$program,
                'validProducts'=>$germplasmResults['foundProductArray'],
                'invalidProducts' => $germplasmResults['notFoundProductArray'],
                'germplasmCounter' => $germplasmResults['germplasmCounter'],
                'germplasmCounterMax' => $germplasmCounterMax,
                'processId'=>$processId,
                'exceededValidationLimitMessage'=>$exceededValidationLimitMessage
            ]);

            return json_encode($htmlData);
        }

    }

        /**
     * Search products from input list
     *
     */
    public function actionSearchPackageLabels(){

        if(isset($_POST)){

            extract($_GET);

            extract($_POST);

            $inputListSplitArr = array_map('trim', array_filter(preg_split("/[\r\n|,|\n]/", $productList)));

            $exceededValidationLimitMessage = '';
            $inputListLimitBgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'inputListLimit')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'inputListLimit') : 1000;
            if(count($inputListSplitArr) > $inputListLimitBgThreshold) {
                $inputListSplitArr = array_slice($inputListSplitArr, 0, $inputListLimitBgThreshold);
                $exceededValidationLimitMessage = "You have exceeded the maximum $inputListLimitBgThreshold records for validation. Only the first $inputListLimitBgThreshold records are validated.";
            }

            $experiment = $this->mainExperiment->getExperiment($experimentId);
            $sqlData = [
              'packageLabels' => $inputListSplitArr,
              'dataProcessAbbrev' => $experiment['dataProcessAbbrev']
            ];

            $packageSearch = $this->mainExperimentModel->searchPackageLabels($sqlData, $experiment['programDbId']);

            $duplicatePackageFlag = false;
            //check to show notification of duplicate package labels
            if(isset($packageSearch['duplicatePackage']) && count($packageSearch['duplicatePackage']) > 0){
                $duplicatePackageFlag = true;
            }
            
            $dataProvider = new ArrayDataProvider([
                'key'=>'packageDbId',
                'allModels' => $packageSearch['arrangedArray'],
                'pagination'=>false,
                'sort' => [
                    'attributes' => ['germplasmDbId','packageDbId','seedDbId','designation',
                    'packageLabel','orderNumber'],
                    'defaultOrder' => [
                        'orderNumber'=>SORT_ASC
                    ]
                ]
            ]);
            
            $htmlData = $this->renderPartial('import/_input_package_label', [
                'dataProvider'=>$dataProvider,
                'id'=>$id,
                'program'=>$program,
                'validProducts'=>$packageSearch['foundProductArray'],
                'invalidProducts' => $packageSearch['notFoundProductArray'],
                'duplicatePackageFlag' => $duplicatePackageFlag,
                'processId'=>$processId,
                'exceededValidationLimitMessage'=>$exceededValidationLimitMessage
            ]);

            return json_encode($htmlData);
        }

    }

    /**
     * Search products from input list
     *
     */
    public function actionSearchPackageCodes(){

        if(isset($_POST)){

            extract($_GET);

            extract($_POST);

            $inputListSplitArr = array_map('trim', array_filter(preg_split("/[\r\n|,|\n]/", $productList)));

            $exceededValidationLimitMessage = '';
            $inputListLimitBgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'inputListLimit')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'inputListLimit') : 1000;
            if(count($inputListSplitArr) > $inputListLimitBgThreshold) {
                $inputListSplitArr = array_slice($inputListSplitArr, 0, $inputListLimitBgThreshold);
                $exceededValidationLimitMessage = "You have exceeded the maximum $inputListLimitBgThreshold records for validation. Only the first $inputListLimitBgThreshold records are validated.";
            }

            $experiment = $this->mainExperiment->getExperiment($experimentId);
            $sqlData = [
              'packageCodes' => $inputListSplitArr,
              'dataProcessAbbrev' => $experiment['dataProcessAbbrev']
            ];

            $packageSearch = $this->mainExperimentModel->searchPackageCodes($sqlData, $experiment['programDbId']);

            $duplicatePackageFlag = false;
            //check to show notification of duplicate package code
            if(isset($packageSearch['duplicatePackage']) && count($packageSearch['duplicatePackage']) > 0){
                $duplicatePackageFlag = true;
            }

            $dataProvider = new ArrayDataProvider([
                'key'=>'packageDbId',
                'allModels' => $packageSearch['arrangedArray'],
                'pagination'=>false,
                'sort' => [
                    'attributes' => ['germplasmDbId','packageDbId','seedDbId','designation',
                    'packageCode','orderNumber'],
                    'defaultOrder' => [
                        'orderNumber'=>SORT_ASC
                    ]
                ]
            ]);

            $htmlData = $this->renderPartial('import/_input_package_code', [
                'dataProvider'=>$dataProvider,
                'id'=>$id,
                'program'=>$program,
                'validProducts'=>$packageSearch['foundProductArray'],
                'invalidProducts' => $packageSearch['notFoundProductArray'],
                'duplicatePackages' => $packageSearch['duplicatePackage'],
                'duplicatePackageFlag' => $duplicatePackageFlag,
                'processId'=>$processId,
                'exceededValidationLimitMessage'=>$exceededValidationLimitMessage
            ]);

            return json_encode($htmlData);
        }

    }

    /**
     * Renders view for input list option
     */
    public function actionRenderInputList(){

        extract($_GET);

        $inputListLimitBgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'inputListLimit')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'inputListLimit') : 1000;

        $htmlData = $this->renderPartial('input_list', [
            'id'=>$id,
            'program'=>$program,
            'processId'=>$processId,
            'inputListLimitBgThreshold'=>$inputListLimitBgThreshold
        ]);

        return json_encode($htmlData);
    }

    /**
     * Renders view for the copy experiment option
     */
    public function actionRenderExperimentEntries(){

        if(isset($_POST)){

            $experimentId = $_POST['experimentId'];
            $currentExperimentId = $_POST['currentExperimentId'];
            $program = $_POST['program'];

            $experiment = $this->mainExperiment->getExperiment($experimentId);
            $dataProvider = $this->entryModel->getSimpleDataProvider($experimentId);

            $paramProcId = (isset($_GET['processId'])  && !empty($_GET['processId']))? $_GET['processId'] : '';
            $processId = isset($_POST['processId']) ? $_POST['processId'] : $paramProcId;
            
            $htmlData = $this->renderPartial('experiment_entries', [
                'dataProvider'=>$dataProvider,
                'experiment'=>$experiment,
                'currentExperimentId'=>$currentExperimentId,
                'processId' => $processId,
                'program' => $program,
                'experimentId' => $experimentId
            ]);

            return json_encode($htmlData);
        }

    }

    /**
     * Renders view for adding entries from plots option
     */
    public function actionAddEntriesFromPlots(){

        extract($_GET);

        if(isset($_POST)){

            extract($_POST);

            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries') : 500;

            $experiment = $this->mainExperiment->getExperiment($experimentId);
            $experimentType = !empty($experiment['experimentType']) ? $experiment['experimentType'] : "";
            $configData = $this->mainExperimentModel->retrieveConfiguration('specify-entry-list', $program, $experimentId, $processId);
            if(!empty($experiment) && $experiment['dataProcessAbbrev'] == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
                $this->entryModel->addEntriesFromOccurrence($occurrenceDbId, $experimentId,$bgThreshold, $program);

                $entryListId = $this->entryListModel->getEntryListId($id);
            } else {
                $entries = isset($entries) ? json_decode($entries, true): [];
                $totalCount = isset($totalCount) ? $totalCount : 0;
                $selectedExperimentId = isset($selectedExperimentId) ? $selectedExperimentId : NULL;
                $this->mainExperimentModel->createEntriesFromNurseries($selectedExperimentId, $experimentId, $entries, $configData, $bgThreshold, $program, $totalCount, $experimentType);
            }
           
            $this->afterChangeEntryList($experimentId);

        }
        return json_encode('success');
        
    }


    /**
     * Renders view for searching packages
     */
    public function actionRenderSearchPackages(){

      if(isset($_POST)){

          extract($_GET);

          $filters = $_POST;
          // Visible packages should only be within the user's program
          $filters['programFilter'] = $program;

          //get entry list ids
          $packageDataProvider = $this->packageModel->getFilterBulkRecords($id, $filters, limit: 500);
          extract($packageDataProvider);

          // Ensure entryIdArray value is correctly parsed downstream
          $filters['entryIdArray'] = !empty($filters['entryIdArray']) ? Utils::jsonDecode($filters['entryIdArray']) : null;

          $htmlData = $this->renderAjax('bulk_package_modal', [
              'data'=>$dataProvider,
              'id'=>$id,
              'program'=>$program,
              'processId'=>$processId,
              'filters'=>$filters,
              'entryNumbers'=>$entryNumbers,
              'partitionView' => $partitionView
          ]);

          return json_encode($htmlData);
      }
    }

    /**
     * Updates the filter for bulk package search
     *
     */
    public function actionChangeFilter(){

      if(isset($_POST)){

          extract($_GET);

          $filters = $_POST;
          // Visible packages should only be within the user's program
          $filters['programFilter'] = $program;

          $filterArray = $this->packageModel->getFilterBulkPackage($id, $filters);
          
          return json_encode($filterArray);

      }
  }

    /**
     * Save a selected package to an Entry
     */
    public function actionSavePackage() {
        if (isset($_POST)) {
            extract($_POST);
            if (isset($packageDbId)) {
                $requestBody = ['packageDbId' => "$packageDbId"];
            }

            if (isset($seedDbId)) {
                $requestBody['seedDbId'] = "$seedDbId";
            }

            Entry::updateRecord($entryDbId, $requestBody);
            $this->plantingInstruction->updateForEntry($entryDbId, $requestBody);
        }

        return json_encode(true);
    }

    /**
     * Bulk saves the selected packages
     *
     */
    public function actionSaveBulkPackages(){

        extract($_GET);
        $pagesParam = '';
        if(isset($_POST)){

            $entryPackageMapping = Utils::jsonDecode($_POST['entryPackageArray'], true);

            foreach($entryPackageMapping as $entry){
                if(isset($entry['packageDbId'])){
                    $entryDbId = $entry['entryDbId'];
                    $packageDbId = $entry['packageDbId'];
                    $requestBody = ['packageDbId' => "$packageDbId"];

                    if(isset($entry['seedDbId'])) {
                        $seedDbId = $entry['seedDbId'];
                        $requestBody['seedDbId'] = "$seedDbId";
                    }

                    Entry::updateRecord($entryDbId, $requestBody);
                    $this->plantingInstruction->updateForEntry($entryDbId, $requestBody);
                }
            }

            $prevCols = Yii::$app->session['EntryListSearchSession'.$id];

            $params_temp = '';
            $pagesParam = '';


            if(!empty($prevCols)){
                foreach ($prevCols as $key => $value) {
                    $entities = trim(urlencode('EntryListSearch'.'['.$key.']'));
                    $entity_value = $entities.'='.$value;
                    $params_temp = empty($params_temp) ?  $entity_value : $params_temp. '&'.$entity_value;
                }
            }

            if(!empty($_POST['page']) && !empty($_POST['perPage'])){
                $pagesParam = '&page='.$_POST['page'].'&per-page='.$_POST['perPage'];
            }

            if(!empty($_POST['dp1_page']) && !empty($_POST['dp1_perPage'])){
                $pagesParam = !empty($pagesParam) ? $pagesParam.'&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'] : '&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'];
            }
        }

        return json_encode(true);
        
    }

    /**
     * Renders view for the bulk package search
     *
     */
    public function actionRenderBulkPackage(){

        if(isset($_POST)){
            
            extract($_GET);

            $filters = $_POST;
            // Visible packages should only be within the user's program
            $filters['programFilter'] = $program;

            $seedInfoDefault = $this->packageModel->getFilterBulkPackage($id, $filters);
            $entryIdArray = isset($_POST['entryIdArray'])? $_POST['entryIdArray'] : '';

            $htmlData = $this->renderAjax('bulk_package', [
                'id'=>$id,
                'program'=> $program,
                'experimentFilter'=> $seedInfoDefault['experiment'],
                'yearFilter'=> $seedInfoDefault['year'],
                'seasonFilter'=> $seedInfoDefault['season'],
                'stageFilter'=> $seedInfoDefault['stage'],
                'entryIdArray' => $entryIdArray,
                'processId'=>$processId
            ]);

            return json_encode($htmlData);
        }

    }

    /**
     * Renders view for the harvested plots
     *
     */
    public function actionRenderHarvestedPlots(){

        if(isset($_POST)){

            extract($_GET);

            $experimentId = $_POST['experimentId'];
            $currentExperimentId = $_POST['currentExperimentId'];

            $selectedExperiment = $this->mainExperiment->getExperiment($experimentId);
            $experiment = $this->mainExperiment->getExperiment($currentExperimentId);

            $data = $this->mainExperimentModel->getHarvestedPlots($experimentId, $experiment['programDbId']);

            $dataProvider = new ArrayDataProvider([
                'allModels' => $data['entries'],
                'key' => 'germplasmDbId',
                'totalCount' => $data['count'],
                'sort' => [
                    'attributes' => ['germplasmDbId', 'generation', 'designation','label', 'packageDbId','seedDbId', 'packageCode', 'parentage', 'seedSourceEntryNumber','seedSourcePlotNumber']
                 
                ],
                'id' => 'from-entries-data-provider',
                'pagination' => [
                    'pagesize' => 100
                ]
            ]);

            $htmlData = $this->renderPartial('harvested_plots', [
                'dataProvider'=>$dataProvider,
                'entries'=>$data['entries'],
                'totalCount'=>$data['count'],
                'selectedExperiment'=>$selectedExperiment,
                'experiment'=>$experiment,
                'id'=>$id,
                'program'=>$program,
                'processId'=> isset($processId) ? $processId : ""
            ]);

            return json_encode($htmlData);
        }

    }

    /**
     * Renders view for the planting instruction records
     */
    public function actionRenderPlantingInstructions(){
        $htmlData = 'There seems to be a problem in loading the planting instruction records';
        
        if(isset($_POST, $_GET)){
            $htmlData = '';
            extract($_GET);
            extract($_POST);

            $plantingIns =  $this->plantingInstruction->searchAll([
                'fields'=>'"plantingInstruction".id as "plantingInstructionDbId"|
                    "plantingInstruction".entry_name AS "entryName"|
                    "plantingInstruction".entry_type AS "entryType"|
                    "plantingInstruction".entry_number AS "entryNumber"|
                    "plantingInstruction".entry_role AS "entryRole"|
                    "plantingInstruction".entry_class AS "entryClass"|
                    "plantingInstruction".entry_status AS "entryStatus"|
                    "plantingInstruction".entry_code AS "entryCode"|
                    germplasm.id AS "germplasmDbId"|
                    "plantingInstruction".seed_id AS "seedDbId"|
                    plot.occurrence_id AS "occurrenceDbId"|
                    germplasm.designation AS "designation"|
                    germplasm.parentage AS "parentage"|
                    entry.id AS "entryDbId"',
                'occurrenceDbId' => "equals $occurrenceDbId",
                'distinctOn' => 'entryDbId',
            ])['data'];
            
            $dataProvider = new ArrayDataProvider([
                'key' => 'plantingInstructionDbId',
                'allModels' => $plantingIns,
                'sort' => [
                    'attributes' => [
                        'plantingInstructionDbId',
                        'entryCode',
                        'entryNumber',
                        'entryName',
                        'entryType',
                        'entryRole',
                        'entryClass',
                        'entryStatus',
                        'germplasmDbId',
                        'seedDbId',
                        'parentage',
                        'designation'
                    ],
                    'defaultOrder' => [
                        'entryNumber' => SORT_ASC,
                    ],
                ],
                'pagination' => false,
            ]);
    
            $htmlData = $this->renderPartial('_planting_instructions', [
                'dataProvider' => $dataProvider,
                'entries' => $plantingIns,
                'id'=> $id,
                'program' => $program,
                'processId' => $processId,
                'experimentName' => $experimentName,
                'occurrenceDbId' => $occurrenceDbId,
                'gridName' => $gridName,
            ]);
        }

        return json_encode($htmlData);
    } 


    /**
     * Renders view for from trial entry list option
     */
    public function actionRenderFromTrial(){

        extract($_GET);

        $currentExperimentId = $_GET['id'];
        $dataProvider = ExperimentModel::getTrials($currentExperimentId);
        $dataProvider->pagination  = false;

        $htmlData = $this->renderPartial('from_trial',[
            'dataProvider' => $dataProvider,
            'currentExperimentId'=> $currentExperimentId,
            'id'=>$id,
            'program'=>$program,
            'processId'=>$processId
        ],true, false);

        return json_encode($htmlData);
    }

    /**
     * Renders view for from nursery entry list option
     */
    public function actionRenderFromNursery(){

        extract($_GET);
        
        $currentExperimentId = $_GET['id'];
        $model = $this->mainExperiment->getExperiment($currentExperimentId);
        
        $processId = !empty($model) ? $model['dataProcessDbId']:null;
     
        $parentList = false;
        $dataProviderArray = $this->mainExperimentModel->getNurseries($currentExperimentId);

        $partitionView = $dataProviderArray['partitionView'];
        $dataProvider = $dataProviderArray['dataProvider'];
        $dataProvider->pagination  = false;

        $htmlData = $this->renderPartial('from_nursery',[
            'dataProvider' => $dataProvider,
            'currentExperimentId'=> $currentExperimentId,
            'id'=>$id,
            'program'=>$program,
            'processId'=>$processId,
            'parentList'=>$parentList,
            'partitionView'=> $partitionView
        ],true, false);

        return json_encode($htmlData);
    }

    /**
     * Renders view for from other occurrences entry list option
     */
    public function actionRenderOccurrenceList(){

        extract($_GET);
     
        $occurrenceInfo = $this->mainExperimentModel->getOccurrences($id, $entryListType);
        $dataProvider = $occurrenceInfo['dataProvider'];
        $partitionView = $occurrenceInfo['partitionView'];

        $model = $this->mainExperiment->getExperiment($id);
        $dataProcessAbbrev = $model['dataProcessAbbrev'];

        $htmlData = $this->renderPartial('from_occurrences',[
            'dataProvider' => $dataProvider,
            'id' => $id,
            'program' => $program,
            'processId' => $processId,
            'gridName' => $gridName,
            'dataProcessAbbrev' => $dataProcessAbbrev,
            'partitionView' => $partitionView
        ],true, false);

        return json_encode($htmlData);
    }


    /**
     * Renders view for copy experiment entry list option
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param integer $processId Experiment type identifier
     */
    public function actionRenderCopyExperiment($id, $program, $processId){
        
        $currentExperimentId = $_GET['id'];
        $model = $this->mainExperiment->getExperiment($currentExperimentId);
        $programId = !empty($model) ? $model['programDbId'] : null;

        $dataProviderArray = $this->mainExperimentModel->getExistingExperimentsDataProvider($currentExperimentId, $programId);
        $dataProvider = $dataProviderArray['dataProvider'];
        $dataProvider->pagination  = false;
        
        $paramProcId = (isset($_GET['processId'])  && !empty($_GET['processId']))? $_GET['processId'] : '';
        $processId = isset($_POST['processId']) ? $_POST['processId'] : $paramProcId;

        $htmlData = $this->renderPartial('copy_experiment',[
            'dataProvider' => $dataProvider,
            'currentExperimentId'=> $currentExperimentId,
            'processId' => $processId,
            'program' => $program,
            'partitionView' => $dataProviderArray['partitionView']
        ],true, false);

        return json_encode($htmlData);
    }


    /**
     * Render the reorder modal window
     *
     * @param id integer
     * @param program string
     */
    public function actionRenderReorder($id, $program,$processId){
        if(isset($_POST)){

            $reorderType = $_POST['reorderType'];
            $experimentId = $_POST['experimentDbId'];

            $reorderAllThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'reorderAllEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'reorderAllEntries') : 200;
            // function checks if experiment is ICN, if so it will retrieve the existing data
            $existingExpData = $this->actionRetrieveGeneratedRecords($experimentId) ?? [];

            $htmlData = $this->renderAjax('reorder', [
                'reorderType'=> $reorderType,
                'parentages'=> (isset($parentages))? $parentages : NULL,
                'experimentDbId'=>$experimentId,
                'threshold' => $reorderAllThreshold,
                'program' => $program,
                'id' => $id,
                'processId' => $processId,
                'existingExpData' => $existingExpData
            ],true, false);

            return json_encode([
                'htmlData' => $htmlData
            ]);
        }

    }

    /**
     * Render the reorder selected modal window
     *
     * @param id integer
     * @param program string
     */
    public function actionRenderReorderSelected($id, $program, $processId){

        if(isset($_POST)){

            $entryIds = $_POST['entryIdArray'];

            $dataProvider = ExperimentModel::getEntriesByIdArray($entryIds);

            $htmlData = $this->renderAjax('reorder_selected', [
                'dataProvider' => $dataProvider,
                'id'=>$id,
                'program'=>$program,
                'entryIds' => $entryIds,
                'processId'=>$processId
            ]);

            return json_encode([
                'htmlData' => $htmlData
            ]);
        }

    }

    /**
     * Update order of selected entries
     *
     * @param id integer
     * @param program string
     */
    public function actionReorderSelectedEntries($id, $program, $processId){

        if(isset($_POST)){

            $entryValueArray = $_POST['jsonArray'];

            //sort the entries
            $selectedArray = array_keys($entryValueArray);

            foreach($selectedArray as $s){

                //calls the reorder entry handler function
                $value = $entryValueArray[$s]['value'];
                $entryId = $entryValueArray[$s]['id'];

                $this->reorderEntry($entryId,$value);
            }

            //delete existing studies
            ExperimentModel::deleteExistingStudies($id);

            Yii::$app->session->setFlash('success', '<i class="fa fa-check"></i> Successfully reordered selected entries according to specified entry list numbers. </b>.');

            $this->redirect(['create/specify-entry-list?id='.$id.'&program='.$program.'&processId='.$processId]);
        }
    }

    /**
     * Reorders a single entry number of an entry
     *
     * Gets the selected draft entry and
     * reorders the selected entry
     *
     * @param integer $entryId Id of the entry record
     * @param integer $value Order value of the entry
     * @return integer $studyId Id of the study record
     */
    public function reorderEntry($entryId,$value){

        //get the entry
        $sqlData = [
            'entryId' => $entryId
        ];
        $originalEntry = ExperimentModel::reorderEntry('select', $sqlData);

        $studyId = $originalEntry['experiment_id'];

        $origId = $originalEntry['id'];

        if($originalEntry['entlistno'] > $value){

            //adjust all entries above
            $endPoint = $originalEntry['entlistno'] - 1;

            //get the entry
            $sqlData = [
                'studyId' => $studyId,
                'value' => $value,
                'endPoint' => $endPoint
            ];
            $entries = ExperimentModel::reorderEntry('select_all', $sqlData);

            //update all concerned entries
            foreach($entries as $entry){

                $newEntno = $entry['entlistno'] + 1;
                $entId = $entry['id'];

                //get the entry
                $sqlData = [
                    'newEntno' => $newEntno,
                    'entId' => $entId
                ];
                ExperimentModel::reorderEntry('update_entries', $sqlData);
            }

            //change the entno of the originalEntry
            $sqlData = [
                'value' => $value,
                'origId' => $origId
            ];
            ExperimentModel::reorderEntry('update', $sqlData);


        }
        else if ($originalEntry['entlistno'] < $value){

            //adjust all entries above
            $endPoint = $originalEntry['entlistno'] + 1;

            //select all entries for this study
            $sqlData = [
                'studyId' => $studyId,
                'value' => $value,
                'endPoint' => $endPoint
            ];
            $entries = ExperimentModel::reorderEntry('select_adjust', $sqlData);

            //update all concerned entries
            foreach($entries as $entry){

                $newEntno = $entry['entlistno'] - 1;
                $entId = $entry['id'];

                //get the entry
                $sqlData = [
                    'newEntno' => $newEntno,
                    'entId' => $entId
                ];
                ExperimentModel::reorderEntry('update_entries', $sqlData);
            }

            //change the entno of the originalEntry
            $sqlData = [
                'value' => $value,
                'origId' => $origId
            ];
            ExperimentModel::reorderEntry('update', $sqlData);
        }

        return $studyId;

    }

    public function actionUpdateEntryNumber($id){

        return json_encode('success');

    }

    /**
     * Get families' information
     *
     * @param experimentId integer record id of the experiment
     */
    public function getAllFamilies($experimentId){

        $sqlData = [
            'experimentId' => $experimentId
        ];
        return ExperimentModel::getAllFamilies('select', $sqlData);

    }


    /**
     * Modify preferred designations
     *
     */
    public function actionRenderModifyPreferredDesignations($id,$program,$processId){

        if(isset($_POST)){

            extract($_GET);

            $type = isset($_POST['type']) ? $_POST['type'] : 'all'; //whether selected or all
            $totalEntryCount = $_POST['totalEntCount']; //total entry count

            $nameType = isset($_POST['name_type']) && !empty($_POST['name_type']) ? $_POST['name_type'] : '';
            $showAll = isset($_POST['showAll']) && !empty($_POST['showAll']) ? $_POST['showAll'] : 'false'; //whether to show al lor not
            $experimentId = isset($_POST['experimentId']) && !empty($id = $_POST['experimentId']) ? $_POST['experimentId'] : 1;

            if($type=='selected'){
                 //if selected entries
                $idArr = $_POST['idArray']; //selected entry ids
                if(is_array($idArr)){
                    $id = implode(',',$idArr);
                }
                else{
                    $id = $idArr;
                }
            }
            else if($type == 'all'){ //if all entries
                    $id = $_POST['experimentId']; //study id
            }

            $data = ExperimentModel::getPreferredNameDpFromConfig($id, $type, $program, $nameType, $showAll, $experimentId); //loads dataprovider of preview browser
            $dataAll = ExperimentModel::getPreferredNameDpFromConfig($id, $type, $program, $nameType, 'true', $experimentId); //loads dataprovider of preview browser

            if(isset($_POST['view']) && $_POST['view'] == 'grid'){

                $view = 'grid_name_type';
            }else{

                $view = 'update_product_name';
            }

            $page = '';
            $perPage = '';

            $prevCols = Yii::$app->session['EntryListSearchSession'.$experimentId];

            $params_temp = '';
            $pagesParam = '';

            if(!empty($prevCols)){
                foreach ($prevCols as $key => $value) {
                    $entities = trim(urlencode('EntryListSearch'.'['.$key.']'));
                    $entity_value = $entities.'='.$value;
                    $params_temp = empty($params_temp) ?  $entity_value : $params_temp. '&'.$entity_value;
                }
            }

            if(!empty($_POST['page']) && !empty($_POST['perPage'])){
                $page = $_POST['page'];
                $perPage = $_POST['perPage'];
            }

            if(!empty($_POST['dp1_page']) && !empty($_POST['dp1_perPage'])){
               $pagesParam = !empty($pagesParam) ? $pagesParam.'&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'] : '&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'];
            }

            if($processId == 'undefined'){
                $processId = $_GET['processId'];
            }

            $results['data']  = $this->renderAjax($view,
            [
                'dataProvider'=>$data['dataProvider'],
                'dataAll'=>$dataAll['dataProvider'],
                'nameTypes'=>$data['nameTypes'],
                'type'=>$type,
                'id' => $experimentId,
                'module'=>$program,
                'program'=>$program,
                'totalEntryCount' => $totalEntryCount,
                'idArray' => ($type == 'selected') ? $idArr : [],
                'experimentId' => $experimentId,
                'page' => $page,
                'perPage' => $perPage,
                'pagesParam' => $pagesParam,
                'params_temp' => $params_temp,
                'processId'=>$processId
            ],true,false);

            $results['count'] = $data['dataProvider']->getTotalCount();

            //delete existing studies
            ExperimentModel::deleteExistingStudies($id);

            return json_encode($results);
        }
    }

    /**
     * Render preview for the preferred designations
     *
     *
     */
    public function actionRenderPreferredDesignationsPreview(){

        if(isset($_POST)){

            $productIdArray = json_decode($_POST['productIdArr']);

            $viewData = ExperimentModel::renderPreferredDesignationsPreview($_GET, $productIdArray, $_POST['nameType']);

            extract($_GET);

            extract($viewData);

            $htmlData = $this->renderPartial('update_product_name_preview', [
                'dataProvider'=>$dataProvider,
                'id'=>$id,
                'program'=>$program,
            ]);

            return json_encode([
                'htmlData' => $htmlData,
                'count' => $count,
                'productIds'=> $productIds
            ]);
        }

    }

    /**
     * Save and update preferred designations
     *
     */
    public function actionUpdatePreferredDesignations(){

        ExperimentModel::updatePreferredDesignations($_GET);

        extract($_GET);

        $pagesParam = '';
        $params_temp = '';

        if(!empty($_POST['pagesParam'])){
            $pagesParam = $_POST['pagesParam'];
        }

        if(!empty($_POST['page']) && !empty($_POST['perPage'])){
            $pagesParam = $pagesParam.'&page='.$_POST['page'].'&per-page='.$_POST['perPage'];
        }

        if(!empty($_POST['params_temp'])){
            $params_temp = $_POST['params_temp'];
        }


        Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully update preferred product names.');

        $this->redirect(['create/specify-entry-list?'.$params_temp.'&id='.$id.'&program='.$program.'&processId='.$processId.$pagesParam]);

    }


    /**
     * Reorder entno by the parentages
     *
     */
    public function actionReorderEntriesFamily($id,$program,$processId){

        ExperimentModel::reorderEntriesFamily($_GET);

        extract($_GET);

        Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully reordered and updated entry list number according to parentage ordering.');

        if($processId == 'undefined'){
            $processId = $_GET['processId'];
        }
        $this->redirect(['create/specify-entry-list?id='.$id.'&program='.$program.'&processId='.$processId]);

    }

    /**
     * Copy entry list from other experiment
     */
    public function actionAddEntriesExperiment(){

        if(isset($_POST)){

            $experimentId = $_POST['experimentId'];
            $currentExperimentId = $_POST['currentExperimentId'];
            $program = $_POST['program'];

            $model = $this->findModel($currentExperimentId);
            $programCode = !empty($model) ? $model['programCode'] : null;
            $dataProcessDbId = !empty($model) ? $model['dataProcessDbId'] : null;

            $configData =  $this->mainExperimentModel->retrieveConfiguration('specify-entry-list', $programCode, $currentExperimentId, $dataProcessDbId);
            $data = [
                'experimentId' => $experimentId,
                'currentExperimentId' => $currentExperimentId,
                'configData' => $configData,
                'program' => $program,
                'experimentType' => !empty($model) ? $model['experimentType'] : null
            ];

            $createdRecords = $this->mainExperimentModel->addEntriesExperiment($data);
        
            return json_encode($createdRecords['success']);
        }
    }

    /**
     * Reorder entno by the current sorting
     *
     */
    public function actionReorderEntriesSort($id, $program, $processId){

        ExperimentModel::reorderEntriesSort($_GET);

        extract($_GET);

        //delete existing studies
        ExperimentModel::deleteExistingStudies($id);

        Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully reordered and updated entry list no according to current sorting.');

        $this->redirect(['create/specify-entry-list?id='.$id.'&program='.$program.'&processId='.$processId]);

    }

    /**
     * Reorder entno within the selected entries
     *
     * @param Integer $id record id of the experiment
     * 
     */
    public function actionReorderEntriesSelected($id){

        if(isset($_POST)){
            $selectedEntries = $this->entryModel->searchAll(['entryDbId'=>'equals '.implode('|equals ', $_POST['entryIdArray'])], 'sort=entryNumber:ASC');
            
            $options = [];
            if($selectedEntries['totalCount'] > 0){
                $options = array_column($selectedEntries['data'], 'entryNumber');
            }
            $dataProvider = new ArrayDataProvider([
                'key'=>'entryDbId',
                'allModels' => $selectedEntries['data'],
                'sort' => [
                    'attributes' => ['entryDbId', 'entryNumber', 'entryName', 'parentage'],
                    'defaultOrder' => [
                        'entryNumber'=>SORT_ASC
                    ]
                ],
                'pagination' => false,
            ]);
            $reorderAction = $_POST['reorderAction'];
            $experimentId = $_POST['experimentId'];
            $entryListId = $this->entryListModel->getEntryListId($experimentId);

            $entryCount = $this->entryModel->searchAll([
                "fields" => "entry.entry_list_id AS entryListDbId|entry.id as entryDbId",
                "entryListDbId"=>"equals $entryListId"
            ], 'limit=1&sort=entryDbId:desc', false);
           
            $entryCount = $entryCount['totalCount'];
            if($reorderAction == 'selected'){
                $view = 'entryList/_reorder_entries.php';
                $options =  array_combine($options, $options);
            } else if($reorderAction == 'reorder-individual'){
                $view = 'entryList/_reorder_individual.php';
                $tempValues = range(1,$entryCount);
                $options = array_combine($tempValues, $tempValues);
            } else if($reorderAction == 'reorder-by-group'){
                $view = 'entryList/_reorder_by_group.php';
                $tempValues = range(1,($entryCount-count($_POST['entryIdArray'])+1));
                $options = array_combine($tempValues, $tempValues);
            }

            // function checks if experiment is ICN, if so it will retrieve the existing data
            $existingExpData = $this->actionRetrieveGeneratedRecords($experimentId) ?? [];
            $htmlData = $this->renderAjax($view ,[
                'selectedEntries' => $selectedEntries,
                'entryIdArray' => $_POST['entryIdArray'],
                'dataProvider' => $dataProvider,
                'id' => $id,
                'options' => $options,
                'entryCount'=> $entryCount,
                'selectedCount' => count($_POST['entryIdArray']),
                'existingExpData' => $existingExpData
            ], true, false);

            return json_encode($htmlData);
        }
    }

    /**
     * Save new order of entries
     *
     * @param Integer $id record id of the experiment
     * 
     */
    public function actionSaveReorderedEntries($id){

        if(isset($_POST)){
            $updatedEntries = [];
            $entryInfo = $_POST['entryInfo'];
            
            $entryDbIds = array_column($entryInfo, 'entryDbId');
            $entryNumbers = array_column($entryInfo, 'entryNumber');
            $oldEntries = array_combine($entryNumbers, $entryDbIds);
            
            $entryListId = $this->entryListModel->getEntryListId($id);
            $entryRecords = $this->entryModel->searchAll([
                "fields" => "entry.entry_list_id AS entryListDbId|entry.entry_number as entryNumber",
                "entryListDbId"=>"$entryListId"
            ], 'sort=entryNumber:DESC&limit=1', false);

            if(!isset($entryRecords) || empty($entryRecords) || (!empty($entryRecords) && $entryRecords['status'] == 500)){
                Yii::$app->getSession()->setFlash('error', \Yii::t('app',' A server error occurred while retrieving entry records. Please try again or file a report for assistance.'));
                return json_encode(false);
            }

            $maxEntryNumber = $entryRecords['data'][0]['entryNumber'] + 1;
            foreach($entryInfo as $entry){
                if($entry['entryNumber'] != $entry['targetEntryNumber']){
                    //change entryNumber
                    if(!in_array($oldEntries[$entry['targetEntryNumber']], $updatedEntries)){ //change to max value
                        $update = $this->entryModel->updateOne($oldEntries[$entry['targetEntryNumber']], ['entryNumber'=>"$maxEntryNumber",'entryCode'=>"$maxEntryNumber"]);
                        $maxEntryNumber++;
                    }

                    if(!isset($update) || empty($update) || (!empty($update) && $update['status'] == 500)){
                        Yii::$app->getSession()->setFlash('error', \Yii::t('app', 'Error encountered while updating entry record. Please try again or file a report for assistance.'));
                    }

                    //update to targetEntryNumber
                    $update = $this->entryModel->updateOne($entry['entryDbId'], ['entryNumber'=>"".$entry['targetEntryNumber'],'entryCode'=>"".$entry['targetEntryNumber']]);

                    if(!isset($update) || empty($update) || (!empty($update) && $update['status'] == 500)){
                        Yii::$app->getSession()->setFlash('error', \Yii::t('app', 'Error encountered while updating entry record. Please try again or file a report for assistance.'));
                    }

                    if($update['success']){
                        $updatedEntries[] = $entry['entryDbId'];
                    }
                }
            }
            
 
            //Get experiment record
            $experiment = $this->mainExperiment->getOne($id);

            $experimentDesignType = isset($experiment['data']['experimentDesignType']) && !empty($experiment['data']['experimentDesignType']) ? $experiment['data']['experimentDesignType'] : '';
            $status = isset($experiment['data']['experimentStatus']) && !empty($experiment['data']['experimentStatus']) ? explode(';',$experiment['data']['experimentStatus']) : [];

            //check status
            $entryStatus = "entry list created";
            if(in_array("entry list created", $status)){
                $entryStatus = "entry list created";
            } else if(in_array("entry list incomplete seed sources", $status)){
                $entryStatus = "entry list incomplete seed sources";
            }
            $experimentStatus = $entryStatus;

            //update experiment status
            $this->mainExperiment->updateOne($id, ["experimentStatus"=>$experimentStatus]);
            //check if design is already generated
            if(in_array('design generated', $status)){
                //check if nursery
                if(in_array($experimentDesignType, ['Entry Order', 'Systematic Arrangement'])){
                    $this->mainExperiment->updateOne($id, ["experimentDesignType"=>"Systematic Arrangement"]);
                    //delete blocks
                    $this->entryOrderModel->deleteBlocksUpdate($id);
                    $this->mainExperiment->updateOne($id, ['notes' => 'plantingArrangementChanged']);
                }
            }

            // function checks if experiment is ICN, if so it will retrieve the existing data
            // clean-up records if ICN
            $generatedRecords = $this->actionRetrieveGeneratedRecords($id);
            if (!empty($generatedRecords)) {
                $this->actionReorderDeleteRecord($id);
            }
            return json_encode(true);
        }
    }

    /**
     * Render the modal form
     *
     * @param $id integer experiment identifier
     * @param $program string program identifier
     *
     */
    public function actionRenderConfigForm($id, $program=null){

        $config = $widgetOptions = [];
        $dataEndpoint = '';

        if(isset($_POST)){
            $variableAbbrev = isset($_POST['selected']) ? strtoupper($_POST['selected']) : '';
            $formType = isset($_POST['formType']) ? strtoupper($_POST['formType']) : '';
            $configVars = json_decode($_POST['configVars'], true);
            
            foreach($configVars as $value){
                if($value['variable_abbrev'] == $variableAbbrev && !(isset($value['is_hidden']) && $value['is_hidden'])){
                    $config[] = $value;
                }
            }

            if(isset($config[0]['api_resource_endpoint'])){
                $dataEndpoint = $config[0]['api_resource_endpoint'];
            }

            $widgetOptions = [
                'config' => $config,
                'formType' => $formType
            ];

            if(isset($config[0]['target_column']) && $config[0]['target_column'] == "contactPerson"){
                //get data
                $dataResults = $this->personModel->getPersonContacts();
                $widgetOptions['presetData'][$config[0]['variable_abbrev']] = $dataResults; 

            }
            
            $htmlData = $this->renderAjax('modal_generic_form.php',[
                'widgetOptions' => $widgetOptions,
                'dataEndpoint' => $dataEndpoint,
            ]);

            return json_encode($htmlData);
        }
    }

    /**
     * Get all entries for the browsers in the Crosses tab
     */
    public function actionGetAllEntries(){
        $items = [];

        if(isset($_POST['sessionName'], $_POST['switchInputType'])) {
            $sessionName = $_POST['sessionName'];
            $switchInputType = $_POST['switchInputType'];
            $params = Yii::$app->session->get($sessionName.'_params');
            $entries = $this->entryModel->searchAll($params, '', true);

            if(isset($switchInputType) && $switchInputType == 'entryCode'){
                foreach($entries['data'] as $entry){
                    $key = $entry['entryDbId'];
                    $items[$key] = $entry['entryCode'];
                }
            } else {
                foreach($entries['data'] as $entry){
                    $key = $entry['entryDbId'];
                    $items[$key] = $entry['designation'];
                }
            }
        }

        return json_encode($items);
    }

    /**
     * Get all entries for the browsers in the Crosses tab
     */
    public function actionGetAllEntryListEntries(){
        $items = [];

        if(isset($_POST['sessionName'])) {
            $sessionName = $_POST['sessionName'];
            $parameters = Yii::$app->session->get($sessionName.'_params');
            $params = [];
            foreach ($parameters['params'] as $key => $value) {
                if(isset($value) && $value != '') {
                    $params[$key] = $value == '(not set)' ? 'null' : $value;
                }
            }
            $entries = $this->entryModel->getEntries($parameters['experimentDbId'], $params, null, '', null, true);

            foreach($entries['entries'] as $entry){
                $key = $entry['entryDbId'];
                $items[$key] = ''.$entry['entryDbId'];
            }
        }

        return json_encode($items);
    }

    /**
     * Set selected items in the grid
     */
    public function actionStoreSessionItems(){

        $count = 0;
        $items = [];

        if(isset($_POST['unset'], $_POST['entryDbId'], $_POST['storeAll'], $_POST['experimentDbId'])){
            extract($_POST);

            $sessionName = isset($sessionName) ? $sessionName : 'entry_ids';
            $userModel = new User();
            $userId = $userModel->getUserId();
            $filteredIdName = "ec_$userId"."_filtered_$sessionName"."_$experimentDbId";
            $selectedIdName = "ec_$userId"."_selected_$sessionName"."_$experimentDbId";        
              
            $items = (isset($_SESSION[$selectedIdName]) && $_SESSION[$selectedIdName] !== null) ? $_SESSION[$selectedIdName] : [];
           
            if($unset == "true"){
                if($storeAll == "true"){
                    $items = [];
                }
                else{
                    foreach($items as $key => $value){
                        if(isset($tab) && $tab == 'crosses'){
                            $index = (gettype($value) == 'string') ? $key : $value;
                        }else{
                            $index = $value;
                        }
                        if($index == $entryDbId){
                            unset($items[$key]);
                        }
                    }
                }
            }
            else{
                if(isset($tab) && $tab == 'crosses'){
                    if($storeAll == "true"){
                        $items = [];
                        $params = Yii::$app->session->get($sessionName.'_params');
                        $entries = $this->entryModel->searchAll($params, '', true);
                        
                        if(isset($switchInputType) && $switchInputType == 'entryCode'){
                            foreach($entries['data'] as $entry){
                                $key = $entry['entryDbId'];
                                $items[$key] = $entry['entryCode'];
                            }
                        } else {
                            foreach($entries['data'] as $entry){
                                $key = $entry['entryDbId'];
                                $items[$key] = $entry['designation'];
                            }
                        }
                        
                    }else if(!isset($items[$entryDbId])){
                        $items[$entryDbId] = $designation;
                    }
                }else{
                    if($storeAll == "true"){
                        $items = Yii::$app->session->get($filteredIdName);
                    }
                    else if(!in_array($entryDbId, $items)){     
                        $items[] = $entryDbId;
                    }
                }
            }

            $_SESSION[$selectedIdName] = $items;
            $count = count($items);
        }

        return json_encode($count);

    }

    /**
     * Retrieve selected items stored in session
     */
    public function actionGetSelectedItems(){

        extract($_POST);
        $entryRecords = $this->entryModel->searchAll(["experimentDbId"=>"equals ".$experimentDbId], 'limit=1&sort=entryDbId:desc', false);
        $userModel = new User();
        $userId = $userModel->getUserId();
        $selectedIdName = "ec_$userId"."_selected_entry_ids_$experimentDbId";
        $selectedItems = (Yii::$app->session->get($selectedIdName) !== null) ? Yii::$app->session->get($selectedIdName) : [];
        $response = [
            'selectedItems' => $selectedItems,
            'selectedItemsCount' => count($selectedItems),
            'totalCount'=> $entryRecords['totalCount']
        ];

        return json_encode($response);
    }


    /**
     * set the currently selected package data in session to null.
     * @param integer $id Experiment ID
     * @return array
     */
    public function actionEmptyUserSelection($id){
        Yii::$app->session->set('selectedItems-'.$id, null);
        return json_encode(['success'=> true]);
    }

    /**
     * get the currently selected package data in session.
     * @param integer $id Experiment ID
     * @return array
    */
    public function actionGetUserSelection($id){
        return json_encode(Yii::$app->session['selectedItems-'.$id]);
    }

    /**
     * Bulk update fields in the grid
     */
    public function actionBulkUpdateFields(){
        $selectedItems = $insertList = $updateList = [];
        $isInsertFlag = false;
     
        if(isset($_POST['mode'], $_POST['variable'], $_POST['dataValue'], $_POST['experimentDbId'])){
            $limit = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'updateEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'updateEntries') : 500;
            
            extract($_POST);
            $selectedItemsInPage = isset($_POST['selectedItems']) && !empty($_POST['selectedItems']) ? $_POST['selectedItems'] : '';
            $model = $this->findModel($experimentDbId);
            $userModel = new User();
            $userId = $userModel->getUserId();

            $variableInfo = $this->mainVariableModel->searchAll(["abbrev"=>"equals $variable"]);
            $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
            $variableType = isset($variableInfo['data']['0']['type']) ? $variableInfo['data']['0']['type'] : '';
            $variableType = ($variable == 'DESCRIPTION') ? 'identification' : $variableInfo['data']['0']['type'];
            $endpoint = isset($endpoint) ? $endpoint : null;
            if($endpoint == 'entry-data'){//you may add more endpoints here
                if($mode == 'selected'){
                    $selectedIdName = "ec_$userId"."_selected_entry_ids_$experimentDbId";
                    $selectedItems = isset($_POST['selectedItems']) ? $_POST['selectedItems'] : Yii::$app->session->get($selectedIdName);
                }else if($mode == 'current-page'){
                    $selectedItems = $selectedItemsInPage;
                }else{
                    $entries = $this->mainExperiment->searchAllEntries($experimentDbId);
                    $selectedItems = isset($entries['data']) ? array_column($entries['data'], 'entryDbId') : []; 
                }
    
                $selectedItemsCount = count($selectedItems);
                
                $updateAddtlParams = ["entityDbId"=>$experimentDbId,"entity"=>"EXPERIMENT", "endpointEntity"=>'ENTRY',"application"=>"EXPERIMENT_CREATION"];
    
            
                $requestedData = [
                    'dataValue' => $dataValue
                ];
                
                foreach($selectedItems as $entryDbId){
                    $entryData = $this->entryModel->searchAllData($entryDbId, ['fields' => 'entry_data.id AS entryDataDbId|variable.abbrev AS variableAbbrev', "variableAbbrev" => "equals $variable"], null, false);
            
                    if(isset($entryData['data'][0]['data'][0]['entryDataDbId'])){
                        $updateList[] = $entryData['data'][0]['data'][0]['entryDataDbId'];
                    }
                    else{// insert a new entry data
                        $insertList[] = [
                            "entryDbId" => "$entryDbId",
                            "variableDbId" => "$variableDbId",
                            "dataValue" => "$dataValue",
                        ];
                    }
                }

                if(!empty($insertList)){
                    //*Code preparation for the bulk insert in the background processor
                    if($selectedItemsCount > $limit){ //more than the set threshold, use background process
                        $insertTaskId = "EC-INSERT_ENTRIES-$experimentDbId-$userId";

                        $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries') : 500;
                        $createAddtlParams = ["description"=>"Creation of entries","entity"=>"EXPERIMENT","entityDbId"=>$experimentDbId,"endpointEntity"=>"ENTRY","application"=>"EXPERIMENT_CREATION"];

                        $insertedList = $this->entryDataModel->create(['records'=> $insertList], true, $createAddtlParams); 
                        $isInsertFlag = true;
                        
                        $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($insertedList['data'][0]['backgroundJobDbId']);
                    
                        if($bgStatus !== 'done' && !empty($bgStatus)){
                            $this->redirect(['create/index?program='.$model['programCode'].'&id='.$experimentDbId]);
                        }
                    }else{
                        $insertedList = $this->entryDataModel->create(['records'=> $insertList]); 
                        $isInsertFlag = true;
                    }
                    
                }
                
                if(!empty($updateList)){
                    if($selectedItemsCount > $limit){
                        $updatedList = $this->entryDataModel->updateMany($updateList, $requestedData, true, $updateAddtlParams);

                        $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($updatedList['data'][0]['backgroundJobDbId']);
                        
                        if($bgStatus !== 'done' && !empty($bgStatus)){
                            $this->redirect(['create/index?program='.$model['programCode'].'&id='.$experimentDbId]);
                        }
                    }
                    else{
                        $updatedList = $this->entryDataModel->updateMany($updateList, $requestedData);
                    }
                }

                $variable = strtolower($variable);
                
            }else{
                // $variable = strToLower($variable);
                 //SITE tab
                 $variableInfo = $this->mainVariableModel->searchAll(["abbrev"=>"equals $variable"]);
                 $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
                 $variableType = isset($variableInfo['data']['0']['type']) ? $variableInfo['data']['0']['type'] : '';
                 $variableType = ($variable == 'DESCRIPTION') ? 'identification' : $variableInfo['data']['0']['type'];

                if(isset($_POST['renderedView']) && $_POST['renderedView'] == 'specify-occurrences'){
                    $updateAddtlParams = ["entityDbId"=>$experimentDbId,"entity"=>"EXPERIMENT", "endpointEntity"=>'OCCURRENCE',"application"=>"EXPERIMENT_CREATION"];
    
                    if($_POST['mode'] == 'selected'){
                        $selectedIdName = "occ_$userId"."_selected_occurrences_$experimentDbId";
                        $selectedItems = Yii::$app->session->get($selectedIdName);
                    }else{
                        $occurrenceRecords = $this->occurrenceModel->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "equals $experimentDbId"]);
                        $occurrences = $occurrenceRecords['data'];
                        $selectedItems = isset($occurrences) ? array_column($occurrences, 'occurrenceDbId') : []; 
                    }
                    
                    $requestedData =[
                        "$targetColumn" => "$dataValue"
                    ];

                    if($variable == 'CONTCT_PERSON_CONT'){
                        $dataValue = $this->personModel->getContact($dataValue)['personName'];
                    }
                  
                    $selectedItemsCount = count($selectedItems);
                    if($variableType == 'identification'){
                        $baseModel = \Yii::$container->get('\app\models\Occurrence');
                    }else{ 
                        $baseModel = \Yii::$container->get('\app\models\OccurrenceData');
                    }
    
                    if($selectedItemsCount > $limit){  
                        $updatedList = $baseModel->updateMany($selectedItems, $requestedData, true,$updateAddtlParams);
                        
                        $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($updatedList['data'][0]['backgroundJobDbId']);
                        
                        if($bgStatus !== 'done' && !empty($bgStatus)){
                            $this->redirect(['create/index?program='.$model['programCode'].'&id='.$experimentDbId]);
                        }
                    }else{
                        if($variableType == 'identification'){
                            if($variable == 'SITE'){
                                $select2Data = [];
                                $fieldRecord = $this->geospatialObjectModel->searchAll([
                                    'geospatialObjectType' => 'equals field',
                                    'rootGeospatialObjectDbId' => "equals $dataValue"
                                ]);
                                $fieldDbId = ($fieldRecord['totalCount'] == 1) ? $fieldRecord['data'][0]['geospatialObjectDbId'].'' : 'null';
                                $requestData = [
                                    $targetColumn => "$dataValue",
                                    'fieldDbId' => $fieldDbId,
                                ];
                                
                                foreach($selectedItems as $occurrenceDbId){
                                    $occurrenceData = $this->occurrenceModel->searchAll(['fields'=>'occurrence.experiment_id AS experimentDbId|occurrence.site_id AS siteDbId|occurrence.field_id AS fieldDbId|
                                                     occurrence.id AS occurrenceDbId',"occurrenceDbId"=> "equals $occurrenceDbId"],'limit=1&sort=occurrenceDbId:desc',false);
                                    $occSavedSiteId = !empty($occurrenceData['data'][0]['siteDbId']) ? $occurrenceData['data'][0]['siteDbId'] : null;
                                
                                    if(!empty($occSavedSiteId) && $occSavedSiteId != null){
                                        $checkSavedSite = $this->geospatialObjectModel->searchAll([
                                            "fields" => "geospatialObject.id AS geospatialObjectDbId|geospatialObject.geospatial_object_code AS geospatialObjectCode|
                                                         geospatialObject.geospatial_object_name AS geospatialObjectName|
                                                         geospatialObject.geospatial_object_type AS geospatialObjectType|
                                                         root.id AS rootGeospatialObjectDbId",
                                            "geospatialObjectDbId" => "equals $occSavedSiteId", "geospatialObjectType" => "equals site"]);
                                        
                                        if(isset($checkSavedSite['data'][0]) && $checkSavedSite['data'][0]['geospatialObjectDbId'] != $dataValue && !empty($dataValue)){            
                                            $requestData['siteDbId'] = "$dataValue";
                                        }
                                    }else{
                                        if(!empty($dataValue)){ 
                                            $requestData['siteDbId'] = "$dataValue";
                                        }
                                    }
                                }
                                //Update
                                if(!empty($requestData)){
                                    $updatedRecord = $baseModel->updateMany($selectedItems, $requestData);
                                }

                            }else{
                                $requestData = [
                                    "$targetColumn" => "$dataValue"
                                ];
                                
                                $updatedRecord = $baseModel->updateMany($selectedItems, $requestData);
                            }
                        }else{
                            $toCreateRequestData = [];
                            foreach($selectedItems as $occurrenceDbId){
                                $occurrenceDataRecord = $baseModel->searchAll([
                                    'occurrenceDbId' => "equals $occurrenceDbId",
                                    'variableDbId' => "equals $variableDbId"
                                ],'limit=1&sort=occurrenceDbId:desc',false);
        
                                if($occurrenceDataRecord['totalCount'] > 0){//record exists
                                    $requestData = [
                                        "dataValue" => $dataValue,
                                    ];
                                    $occurrenceDataDbId = $occurrenceDataRecord['data'][0]['occurrenceDataDbId'];
                                    $updatedRecord = $baseModel->updateOne($occurrenceDataDbId, $requestData);
                                }
                                else{//insert new record
                                    $toCreateRequestData['records'][] = [
                                        "occurrenceDbId" => "$occurrenceDbId",
                                        "variableDbId" => "$variableDbId",
                                        "dataValue" => "$dataValue",
                                    ];
                                }
                            }
                            //Create new record here if needed
                            if(!empty($toCreateRequestData)){
                                $insertedRecord = $baseModel->create($toCreateRequestData);
                            }
                        }
                    }

                }else{
                    //ENTRY LIST tab
                    $variable = $this->mainExperimentModel->setIndex($variable);

                    $requestedData =[
                        "$variable" => "$dataValue",
                    ];
                    $filters = [];

                    if($mode == 'selected'){
                        $selectedIdName = "ec_$userId"."_selected_entry_ids_$experimentDbId";
                        $selectedItems = isset($_POST['selectedItems']) ? $_POST['selectedItems'] : Yii::$app->session->get($selectedIdName);
                        $selectedItemsCount = isset($_POST['selectedItemsCount']) ? $_POST['selectedItemsCount'] : count($selectedItems);
                    }else if($mode == 'current-page'){
                        $selectedItems = $selectedItemsInPage;
                        $selectedItemsCount = count($selectedItems);
                    }else{
                        $entryLists = $this->entryListModel->searchAll(['experimentDbId' => "equals ".$experimentDbId]);
                        $entryListDbId = isset($entryLists['data'][0]['entryListDbId']) ? $entryLists['data'][0]['entryListDbId'] : 0;
                        $filters = ["fields"=>"entry.id as entryDbId|entry.entry_list_id as entryListDbId",'entryListDbId' => "equals ".$entryListDbId];

                        $entry = \Yii::$container->get('app\models\Entry')->searchAll($filters,'limit=1&sort=entryDbId:desc',false);
                        $selectedItemsCount = $entry['totalCount'];
                        $selectedItems = [];

                        $filters = json_encode($filters);
                    }

                    $updateAddtlParams = ["entityDbId"=>$experimentDbId,"entity"=>"EXPERIMENT", "endpointEntity"=>'ENTRY',"application"=>"EXPERIMENT_CREATION"];

                    // Will be enabled when needed
                    // if(in_array($variable, ['entryType', 'entryRole'])){
                    //     //add notes for BT experiment
                    //     $this->mainExperimentModel->addDesignNoteForUpdate($experimentDbId, "updated entries");
                    // }
                    
                    if($selectedItemsCount > $limit){

                        //update planting intruction records for cross nusery experiment phase II
                        if($variable == 'entryRole' && $model['dataProcessAbbrev'] == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
                            if(empty($selectedItems)) {
                                $entries = $this->mainExperiment->searchAllEntries($experimentDbId);
                                $selectedItems = isset($entries['data']) ? array_column($entries['data'], 'entryDbId') : []; 
                            }
                            $entry = $this->entryModel->searchAll([
                                'fields' => 'entry.id as entryDbId|entry.entry_class as entryClass',
                                'entryDbId' => 'equals '.implode('|equals ', $selectedItems),
                                'entryClass' => 'is null',
                            ]);
                            if($entry['totalCount']){
                                $piEntryIdList = array_column($entry['data'], 'entryDbId');
                                //update planting instruction records
                                $this->entryModel->updatePlantingInstruction($experimentDbId, $piEntryIdList, $variable, $dataValue);
                            }
                        }
                        if(!empty($selectedItems)) {
                            $filters = ["fields"=>"entry.id as entryDbId",'entryDbId' => 'equals '.implode('|equals ', $selectedItems)];
                            $filters = json_encode($filters);
                        }

                        $dataArray = [
                            'searchPath' => 'entries-search',
                            'filters' => $filters,
                            'updatePath' => 'entries',
                            'updateField' => $variable,
                            'updateValue' => (string) $dataValue,
                            'idName' => 'entryDbId',
                            'processName' => 'update-filter-records'
                        ];

                        $updatedList = $this->entryModel->updateMany(null, $dataArray, true, $updateAddtlParams);

                        $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($updatedList['data'][0]['backgroundJobDbId']);

                        if($bgStatus !== 'done' && !empty($bgStatus)){
                            $this->redirect(['create/index?program='.$model['programCode'].'&id='.$experimentDbId]);
                        }
                    }else{
                        if(empty($selectedItems)) {
                            $entryListDbId = $this->entryListModel->searchAll(['experimentDbId' => "equals $experimentDbId"])['data'][0]['entryListDbId'];
                            $entries = $this->entryModel->searchAll(['fields'=>'entry.id as entryDbId|entry.entry_list_id as entryListDbId', 'entryListDbId'=>'equals '.$entryListDbId]);
                            $selectedItems = isset($entries['data']) ? array_column($entries['data'], 'entryDbId') : [];
                        }
                        
                        $updatedList = $this->entryModel->updateMany($selectedItems, $requestedData);

                        //update planting intruction records for cross nusery experiment phase II
                        if($updatedList['success'] && $variable == 'entryRole' && $model['dataProcessAbbrev'] == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
                            $entry = $this->entryModel->searchAll([
                                'fields' => 'entry.id as entryDbId|entry.entry_class as entryClass',
                                'entryDbId' => 'equals '.implode('|equals ', $selectedItems),
                                'entryClass' => 'is null',
                            ]);
                            if($entry['totalCount']){
                                $piEntryIdList = array_column($entry['data'], 'entryDbId');
                                //update planting instruction records
                                $this->entryModel->updatePlantingInstruction($experimentDbId, $piEntryIdList, $variable, $dataValue);
                            }
                        }
                    }
                }
            }
        }

        $result = [
            'count' => $selectedItemsCount,
            'selectedItems' => $selectedItems,
            'variable' => $variable,
            'variableDbId' => $variableDbId,
            'variableType' => $variableType,
            'isInsertFlag' => $isInsertFlag 
        ];

        return json_encode($result);

    }

    /**
    *  Update the dynamic configuration
    * @param $id integer record id of the experiment
    * @param $program string program identifier
    */
    public function actionUpdateConfigVariable($id, $program, $processId){
        extract($_GET);
        $pagesParam = '';
        $boxFlag = false;
        
        if(isset($_POST)){
            // $entryIdArray = isset($_POST['entryIdArray'])? $_POST['entryIdArray'] : [];
            if(isset($_POST['entryIdArray']) && !empty($_POST['entryIdArray'])){
                if(strtolower(gettype($_POST['entryIdArray'])) == 'string'){
                     $entryIdArray = explode(',',$_POST['entryIdArray']);
                }else{
                    $entryIdArray = $_POST['entryIdArray'];
                }
            }else{
                $entryIdArray = [];
            }


            //Get the active tab useful for page redirection
            $activeTab = isset($_POST['activeTab']) ? 'specify-'.$_POST['activeTab'] : 'specify-entry-list';
            if(isset($_POST['activeTab']) && !empty($_POST['activeTab'])){
                $boxFlag = true;
            }

            $input_value = $_POST['inputVal'];

            $variable_abbrev = strtoupper($_POST['variable_abbrev']);
            $varEntry = Variable::find()->where(['abbrev'=>$variable_abbrev, 'is_void'=>false])->one();

            $updateType = $_POST['updateType'];

            $experimentId = $_POST['experimentId'];

            $prevCols = Yii::$app->session['EntryListSearchSession'.$experimentId];
            $dataMemtype = isset($_POST['dataMemtype']) ? $_POST['dataMemtype'] : '';

            $view = isset($_POST['renderedView']) ? $_POST['renderedView'] : 'specify-entry-list';
            
            //Retrieve Configuration
            $configData = ExperimentModel::retrieveConfiguration($view, $program, $id, $processId);
            
            //update entry class of this
            if(isset($_POST['mode'])){
                ExperimentModel::updateEntry($id, $input_value, $_POST['mode'], 'entry_role', $experimentId, $prevCols, strtolower($variable_abbrev), $updateType, $dataMemtype, $configData);
            } else {
                ExperimentModel::updateEntry($entryIdArray, $input_value, NULL, 'entry_role', $experimentId, $prevCols,strtolower($variable_abbrev), $updateType, $dataMemtype, $configData);
            }
            Yii::$app->session->setFlash('success', '<i class="fa-fw fa fa-check"></i> Successfully updated the entry/parent.');
            $pageSession = isset(Yii::$app->session['paginationSession']) ? Yii::$app->session['paginationSession'] : '';
            $params_temp = '';

            if(!empty($prevCols)){
                foreach ($prevCols as $key => $value) {
                    $entities = trim(urlencode('EntryListSearch'.'['.$key.']'));
                    $entity_value = $entities.'='.$value;
                    $params_temp = empty($params_temp) ?  $entity_value : $params_temp. '&'.$entity_value;
                }
            }


            if(!empty($_POST['page']) && !empty($_POST['perPage'])){
                $pagesParam = '&page='.$_POST['page'].'&per-page='.$_POST['perPage'];
            }

            if(!empty($_POST['dp1_page']) && !empty($_POST['dp1_perPage'])){
                $pagesParam = !empty($pagesParam) ? $pagesParam.'&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'] : '&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'];
            }

        }

        $experiment = Experiment::find()->where(['id'=> $id, 'is_void'=> false])->one();
        $processId = isset($experiment->data_process_id) ? $experiment->data_process_id : '';
        
        //delete existing studies
        ExperimentModel::deleteExistingStudies($id);

        if(isset($_POST['isRow']) && $_POST['isRow']){
             // Do nothing, page will not reload
        }else{
            if(!$boxFlag){
                $this->redirect(['create/specify-entry-list?'.$params_temp.'&id='.$id.'&program='.$program.'&processId='.$processId.$pageSession.$pagesParam]);
            }else{
                return json_encode(true);
            }
        }
    }

    /**
     * Render bulk update form
     * 
     * @param integer $id experiment identifier
     * @param text $program program identifier
     */
    public function actionRenderBulkUpdate($id, $program){
        if(isset($_POST)){
            $configVars = json_decode($_POST['configVars'], true);

            $fields = [];

            foreach($configVars as $value){
                if(!(isset($value['is_hidden']) && $value['is_hidden']) && !(isset($value['disabled']) && $value['disabled'])){  
                    if($value['variable_abbrev'] != 'OCCURRENCE_NAME' && $value['variable_abbrev'] != 'FIELD'){
                        $varInfo = $this->mainVariableModel->getVariableByAbbrev($value['variable_abbrev']);
                        $header = isset($value['display_name']) && !empty($value['display_name']) ? $value['display_name'] : $varInfo['label'];
                        $fields[$varInfo['abbrev']] = ucwords(strtolower($header));
                    }
                }
            }

            $htmlData = $this->renderAjax('bulk_update_form.php',[
               'data' => $fields,
               'id' => $id,
               'program' => $program,
               'configVars' => $configVars
            ], true, false);
          
            return json_encode($htmlData);
        }
    }
    

    /**
    * Manage crosses for the nurseries
    *
    * @param String $program name of the current program
    * @param Integer $id record id of the experiment
    * @param Integer $processId process identifier
    * @param String $type input type for pairing
    *
    */
    public function actionManageCrosses($program, $id, $processId, $type = 'designation') {
        $userModel = new User();
        $userId = $userModel->getUserId();
        $browserIdFemale = 'dynagrid-ec-add-crosses-female';
        $browserIdMale = 'dynagrid-ec-add-crosses-male';

        $model = $this->findModel($id);
        $maxCrossCount = 1500;

        $femaleSearchModel = $this->femaleSearchModel;
        $maleSearchModel = $this->maleSearchModel;
        $configData = $this->mainExperimentModel->retrieveConfiguration('specify-entry-list', $program, $id, $processId);

        //TODO: to be refactored next sprint
        //For checking parent/entry list tab
        // $options = ['configValues'=>$configData,'type'=>'entry-list'];
        // $validateTabFlag = $this->actionValidateRequired($id,'cross-validate-tab',$options);
        // if($validateTabFlag == true || $validateTabFlag == 1){
        //     $this->redirect(['create/specify-entry-list?id='.$id.'&program='.$program.'&processId='.$processId]);
        // }

        $params = [];
        $fields = 'entry.id AS entryDbId|experiment.id as experimentDbId|entry.entry_role AS entryRole|entry.entry_number as entryNumber|entry.entry_code as entryCode| germplasm.designation|femaleCountField|maleCountField|germplasm.parentage|germplasm.id as germplasmDbId';
        $paramPage = '';
        $paramSort = '';
        //set female browser parameters and filters
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();

        if (isset($_GET['female-grid-page'])){
            $paramPage = '&page=' . $_GET['female-grid-page'];
        }else if (isset($_GET[$browserIdFemale.'-page'])){
            $paramPage = '&page=' . $_GET[$browserIdFemale.'-page'];
        }

        //sorting
        if(isset($_GET['sort'], $_GET['_pjax']) && $_GET['_pjax'] == '#'.$browserIdFemale.'-pjax'){
            if(strpos($_GET['sort'],'-') !== false){
				$sort = str_replace('-', '', $_GET['sort']);
				$paramSort = "&sort=$sort:DESC";
			}
			else{
				$paramSort = '&sort='.$_GET['sort'];
			}
        }
        $pageParams = $paramLimit.$paramPage.$paramSort;

        if(isset($_GET['FemaleListSearch'])){
            $params = array_filter($_GET['FemaleListSearch'],'strlen');
        }
        $currentPage = isset($_GET[$browserIdFemale.'-page']) ? $_GET[$browserIdFemale.'-page'] : null;
        $currentPage = (is_null($currentPage) && isset($_GET['female-grid-page'])) ? $_GET['female-grid-page'] : null;

        if(!isset($params['entryRole']) || $params['entryRole'] == null){
            $params['entryRole'] = 'female|equals female-and-male';
        }
        
        $femaleOrigParams = Yii::$app->session->get('female_entry_ids_params');
        if(isset($femaleOrigParams['includeParentCount'])) unset($femaleOrigParams['includeParentCount']);  // exclude this property because it is added in the model
        $params['entryRole'] = 'equals '.$params['entryRole'];
        $params['experimentDbId'] = "equals $id";
        $params['fields'] = $fields;
        $femaleDataProvider = $femaleSearchModel->search(['FemaleListSearch' => $params,'userId'=>$userId, 'id'=>$id], $pageParams, $currentPage, $type);

        $femaleReset = false;
        if(($femaleOrigParams != $params)) {
            $femaleReset = true;
        }
        Yii::$app->session->set('resetFemaleSelection', $femaleReset);

        $params = [];
        $pageParams = $paramPage = $paramSort = null;
        //set male browser parameters and filters
        $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();

        if (isset($_GET['male-grid-page'])){
            $paramPage = '&page=' . $_GET['male-grid-page'];
        }else if (isset($_GET[$browserIdMale.'-page'])){
            $paramPage = '&page=' . $_GET[$browserIdMale.'-page'];
        }

        //sorting
        if(isset($_GET['sort'], $_GET['_pjax']) && $_GET['_pjax'] == '#'.$browserIdMale.'-pjax'){
            if(strpos($_GET['sort'],'-') !== false){
				$sort = str_replace('-', '', $_GET['sort']);
				$paramSort = "&sort=$sort:DESC";
			}
			else{
				$paramSort = '&sort='.$_GET['sort'];
			}
        }
        $pageParams = $paramLimit.$paramPage.$paramSort;

        if(isset($_GET['MaleListSearch'])){
            $params = array_filter($_GET['MaleListSearch'], 'strlen');
        }
        $currentPage = isset($_GET[$browserIdMale.'-page']) ? $_GET[$browserIdMale.'-page'] : null;
        $currentPage = (is_null($currentPage) && isset($_GET['male-grid-page'])) ? $_GET['male-grid-page'] : null;

        if(!isset($params['entryRole']) || $params['entryRole'] == null){
            $params['entryRole'] = 'male|equals female-and-male';
        }

        $maleOrigParams = Yii::$app->session->get('male_entry_ids_params');
        if(isset($maleOrigParams['includeParentCount'])) unset($maleOrigParams['includeParentCount']);  // exclude this property because it is added in the model
        $params['entryRole'] = 'equals '.$params['entryRole'];
        $params['experimentDbId'] = "equals $id";
        $params['fields'] = $fields;
        $maleDataProvider = $maleSearchModel->search(['MaleListSearch' => $params,'userId'=>$userId, 'id'=>$id], $pageParams, $currentPage,$type);

        $maleReset = false;
        if(($maleOrigParams != $params)) {
            $maleReset = true;
        }
        Yii::$app->session->set('resetMaleSelection', $maleReset);

        if($processId == 'undefined'){
            $processId = $_GET['processId'];
        }

        //Temporary config value for crosses since no config is set yet
        $varConfigValues[] = [
            'disabled' => false,
            'is_shown' => true,
            'required' => "required",
            'variable_type' => 'identification',
            'variable_abbrev'=> "CROSS_METHOD",
            'target_value' => 'value',
            'allowed_values' => [
                "SINGLE CROSS",
                "THREE-WAY CROSS",
                "DOUBLE CROSS",
                "FEMALE COMPLEX TOP CROSS",
                "BACKCROSS",
                "COMPLEX CROSS",
                "INDUCED MUTATION POPULATION",
                "HYBRID FORMATION"
            ]
        ];

        return $this->render('manage_crosses',[
            'id' => $id,
            'model' => $model,
            'femaleSearchModel' => $femaleSearchModel,
            'maleSearchModel' => $maleSearchModel,
            'program' => $program,
            'processId'=> $processId,
            'femaleDataProvider' => $femaleDataProvider,
            'maleDataProvider' => $maleDataProvider,
            'maxCrossCount' => $maxCrossCount,
            'configValues' => $varConfigValues,
            'type' => $type
        ]);

    }

    /**
    * Check crosses browsers selection if they need to be reset or not
    */
    public function actionCheckCrossesSelectionStatus(){
        $result = [
            'resetFemaleSelection' => $_SESSION['resetFemaleSelection'],
            'resetMaleSelection' => $_SESSION['resetMaleSelection']
        ];

        Yii::$app->session->set('resetFemaleSelection', false);
        Yii::$app->session->set('resetMaleSelection', false);

        return json_encode($result);
    }

    /**
    * Manage Cross list for the nursery experiment type
    *
    * @param string $program name of the current program
    * @param integer $id record id of the experiment
    * @param integer $processId process identifier
    *
    */
    public function actionManageCrosslist($program, $id, $processId){

        $searchModel = $this->crossSearch;
        $paramFilter = Yii::$app->request->queryParams;
        

        if($processId == 'undefined'){
            $processId = $_GET['processId'];
        }

        $mainParam = ["experimentDbId"=>"equals $id","includeParentSource"=>true];
        
        $dataProvider = $searchModel->search($mainParam,$paramFilter,true,null,true);
        
        //Temporary config value for crosses since no config is set yet
        $varConfigValues[] = [
            'disabled' => false,
            'fixed' => true,
            'required' => "required",
            'variable_type' => 'identification',
            'variable_abbrev'=> "CROSS_METHOD",
            'target_value' => 'value',
            'allowed_values' => [
                "SINGLE CROSS",
                "THREE-WAY CROSS",
                "DOUBLE CROSS",
                "FEMALE COMPLEX TOP CROSS",
                "BACKCROSS",
                "COMPLEX CROSS",
                "INDUCED MUTATION POPULATION",
                "HYBRID FORMATION"
            ]
        ];

        $model = $this->findModel($id);
        
        $methodTags = $this->mainExperimentModel->getTags();

        //check if there are missing seedDbIds for cross parents
        $crossParentCount = $this->mainExperimentModel->updateCrossParents($id);

        //get cross attributes, if any
        $crossAttributes = $this->crossPatternModel->getCrossAttributes($id);

        return $this->render('manage_crosses_cross_list',[
            'program' => $program,
            'id' => $id,
            'processId' => $processId,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $model,
            'methodTags' => $methodTags,
            'varConfigValues' => $varConfigValues,
            'crossAttributes' => $crossAttributes,
            'exportRecordsThresholdValue' => Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'exportCrossListLimit'),
            'deleteCrossThresholdValue' => Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'deleteCrossList')
        ]);

    }

    /**
     * Get all crosses for the browser in the Crosses tab
     * @param integer $id record id of the experiment
     */
    public function actionGetAllCrossListCrosses($id){
        $items = [];

        $params = [
            "fields" => "germplasmCross.id AS crossDbId|experiment.id AS experimentDbId",
            "experimentDbId" => "equals $id"
        ];
        $result = $this->crossSearch->searchAll($params);

        foreach($result['data'] as $cross){
            $key = $cross['crossDbId'];
            $items[$key] = ''.$cross['crossDbId'];
        }

        return json_encode($items);
    }

    /**
    * Render crossing matrix
    *
    * @param string $program name of the current program
    * @param integer $id record id of the experiment
    * @param integer $processId process identifier
    *
    */
    public function actionManageCrossingMatrix($program, $id, $processId){
        $model = Experiment::findOne($id);

        if($processId == 'undefined'){
            $processId = $_GET['processId'];
        }

        return $this->render('manage_crossing_matrix',[
            'id'=>$id,
            'program' => $program,
            'model' => $model,
            'processId'=> $processId
        ]);

    }
    
    /**
     * Add Crosses for the nursery experiment type
     * //post params:
     * @param Integer $id record id of the experiment
     * @param Array $crosses list of input crosses
     *
     */
    public function actionAddCrosses(){
        extract($_POST);
        $message = '';
        $crossList = array_filter(preg_split("/[\r\n]/", $crosses));
        $totalCrossCount = count($crossList);
        $validCrossCount = 0;
        $crosses = $duplicateList = $crossesEntryCode = $entryInfoCode = [];
        if(isset($switchInputType) && $switchInputType == 'entryCode'){
            //get convertion of entryCode to designation
            $convertedArray = $this->mainExperimentModel->getEntryDesignation($crossList, $experimentId);
            $entryDesignation = $convertedArray['combinedArray'];
            foreach($crossList as $key => $value){
                $cross = explode('|', $value);
                $femaleCode = isset($cross[0]) ? trim($cross[0]) : '';
                $maleCode = isset($cross[1]) ? trim($cross[1]) : '';

                $female = isset($entryDesignation[$femaleCode]) ? $entryDesignation[$femaleCode] : '';
                $male = isset($entryDesignation[$maleCode]) ? $entryDesignation[$maleCode] : '';
                $crossName = "$female|$male";
                $crossCode = "$femaleCode|$maleCode";

                $crosses[$key] = [
                    'female' => $female,
                    'male' => $male,
                    'crossName' => $crossName,
                    'crossInput' => $value,
                ];

                $crossesEntryCode[$key] = [
                    'female' => $female,
                    'male' => $male,
                    'crossName' => $crossName,
                    'crossInput' => $value,
                    'femaleCode' => $femaleCode,
                    'maleCode' => $maleCode,
                    'crossCode' => $crossCode
                ];
                $entries[$female] = true;
                $entries[$male] = true;
            }
            $params = [
                'fields' => 'entry.id AS entryDbId|entry.entry_number AS entryNumber|entry.entry_code AS entryCode|germplasm.designation|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|experiment.id AS experimentDbId|germplasm.generation|entry.notes',
                'experimentDbId' => "equals $experimentId",
                'entryCode'=> 'equals '.implode('|equals ', $convertedArray['entryCodeArray'])
            ];
        } else {
            foreach($crossList as $key => $value){
                $cross = explode('|', $value);
                $female = isset($cross[0]) ? trim($cross[0]) : '';
                $male = isset($cross[1]) ? trim($cross[1]) : '';
                $crossName = "$female|$male";

                $crosses[$key] = [
                    'female' => $female,
                    'male' => $male,
                    'crossName' => $crossName,
                    'crossInput' => $value,
                ];
                $entries[$female] = true;
                $entries[$male] = true;
            }
            $params = [
                'fields' => 'entry.id AS entryDbId|entry.entry_number AS entryNumber|entry.entry_code AS entryCode|germplasm.designation|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|experiment.id AS experimentDbId|germplasm.generation|entry.notes',
                'experimentDbId' => "equals $experimentId",
                'designation' => 'equals '.implode('|equals ', array_keys($entries)),
            ];
        }
        

        $entries = $this->entryModel->searchAll($params)['data'];

        //check if there's existing cross pattern data
        $variable = $this->mainVariableModel->getVariableByAbbrev('CROSSING_PATTERN');
        $sqlData = [
            'experimentDbId'=> "equals $experimentId",
            'variableDbId' => "equals ".$variable['variableDbId']
        ];
        $crossAttributes = $this->crossAttribute->searchAll($sqlData); 
        if(isset($crossPattern) && $crossAttributes['totalCount'] == 0){
            //delete existing crosses
            $this->cross->deleteCrosses($experimentId);
            Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The previously created cross records for this experiment was deleted to accommodate the cross pattern.');
        }
        
        $visitedCrossCode = [];
        foreach($entries as $key => $value){
            $des = $value['designation'];
            if(isset($designationList[$des]['count'])){
                $count = $designationList[$des]['count'] + 1;
                $temp = $designationList[$des]['entno'];
                $temp[$value['entryDbId']] = $value['entryNumber'];
                $designationList[$des]['entno'] = $temp;
            }else{
                $count = 1;
                $designationList[$des]['entno'][$value['entryDbId']] = $value['entryNumber'];
            }
            $designationList[$des]['key'] = isset($designationList[$des]['key']) ? $designationList[$des]['key'] : $key;
            $designationList[$des]['count'] = $count;

            if($switchInputType == 'entryCode'){
                $entryInfoCode[$value['entryCode']] = $value; 
            }
        }
        foreach($crosses as $key => $value){
            $female = $value['female'];
            $male = $value['male'];
            $crossName = $value['crossName'];
            $crosses[$key]['status'] = 'invalid';
            
            if($female == $male){// selfing not allowed in text area
                $crosses[$key]['remarks'] = 'Same parent germplasm. Please use SELF feature for selfing.';
            }else{
                $femaleExists = isset($designationList[$female]) ? true : false;
                $maleExists = isset($designationList[$male]) ? true : false;
                
                if($femaleExists && !$maleExists){
                    $crosses[$key]['remarks'] = 'Male parent does not exist in the Parent List!';
                }else if(!$femaleExists && $maleExists){
                    $crosses[$key]['remarks'] = 'Female parent does not exist in the Parent List!';
                }else if(!$femaleExists && !$maleExists){
                    $crosses[$key]['remarks'] = 'Both parents do not exist in the Parent List!';
                }else{
                    //retrieve entry info:
                    $femaleEntryKey = $designationList[$female]['key'];
                    $maleEntryKey = $designationList[$male]['key'];
                    $fCount = $designationList[$female]['count'];
                    $mCount = $designationList[$male]['count'];

                    if($switchInputType == 'entryCode'){

                        if(!in_array($crossesEntryCode[$key]['crossCode'], $visitedCrossCode)){// valid cross name
                            $femaleEntryDbId = $entryInfoCode[$crossesEntryCode[$key]['femaleCode']]['entryDbId'];
                            $maleEntryDbId = $entryInfoCode[$crossesEntryCode[$key]['maleCode']]['entryDbId'];

                            $crosses[$key]['status'] = 'valid';
                            $parents["$femaleEntryDbId,$maleEntryDbId"] = $key;
                            $femaleTemp = [
                                'germplasmDbId' => $entryInfoCode[$crossesEntryCode[$key]['femaleCode']]['germplasmDbId'].'',
                                'parentRole' => 'female',
                                'orderNumber' => '1',
                                'experimentDbId' => "$experimentId",
                                'entryDbId' => $femaleEntryDbId.''
                            ];
                            if(!empty($entryInfoCode[$crossesEntryCode[$key]['femaleCode']]['seedDbId'])){
                                $femaleTemp['seedDbId'] = $entryInfoCode[$crossesEntryCode[$key]['femaleCode']]['seedDbId'].'';
                            }
                            if(!empty($entryInfoCode[$crossesEntryCode[$key]['femaleCode']]['notes'])){
                                $femaleTemp['notes'] = $entryInfoCode[$crossesEntryCode[$key]['femaleCode']]['notes'];
                            }
                            $crossParent[$crossName][] = $femaleTemp;
                            $maleTemp  = [
                                'germplasmDbId' => $entryInfoCode[$crossesEntryCode[$key]['maleCode']]['germplasmDbId'].'',
                                'parentRole' => 'male',
                                'orderNumber' => '2',
                                'experimentDbId' => "$experimentId",
                                'entryDbId' => $maleEntryDbId.''
                            ];
                            if(!empty($entryInfoCode[$crossesEntryCode[$key]['maleCode']]['seedDbId'])){
                               $maleTemp['seedDbId'] = $entryInfoCode[$crossesEntryCode[$key]['maleCode']]['seedDbId'].'';
                            }
                            if(!empty($entryInfoCode[$crossesEntryCode[$key]['maleCode']]['notes'])){
                                $maleTemp['notes'] = $entryInfoCode[$crossesEntryCode[$key]['maleCode']]['notes'];
                            }
                            $crossParent[$crossName][] = $maleTemp;
                            $visitedCrossCode[] = $crossesEntryCode[$key]['crossCode'];
                        }else{// build duplicate germplasm list
                            $crosses[$key]['status'] = 'invalid';
                            if($fCount > 1 && $mCount == 1){   
                                $crosses[$key]['remarks'] = 'Duplicate female germplasm';    
                            }else if($fCount == 1 && $mCount > 1){
                                $crosses[$key]['remarks'] = 'Duplicate male germplasm';
                            }else if($fCount > 1 && $mCount > 1){
                                $crosses[$key]['remarks'] = 'Both parents have duplicate germplasm';
                            }

                            $duplicateList[] = [
                                'crossName' => $crossName,
                                'femaleCount' => $fCount,
                                'maleCount' => $mCount, 
                                'femaleNo' => $designationList[$female]['entno'],
                                'femaleEntryId' => $femaleEntryDbId,
                                'maleNo' => $designationList[$male]['entno'],
                                'femaleGen' => $entries[$femaleEntryKey]['generation'],
                                'maleGen' => $entries[$maleEntryKey]['generation'],
                                'maleEntryId' => $maleEntryDbId,
                                'parentDbId' => "$femaleEntryDbId,$maleEntryDbId",
                            ];
                        }
                    } else {

                        $femaleEntryDbId = $entries[$femaleEntryKey]['entryDbId'];
                        $maleEntryDbId = $entries[$maleEntryKey]['entryDbId'];

                        if($fCount == 1 && $mCount == 1){// valid cross name
                            $crosses[$key]['status'] = 'valid';
                            $parents["$femaleEntryDbId,$maleEntryDbId"] = $key;
                            $crossParent[$crossName][] = [
                                'germplasmDbId' => $entries[$femaleEntryKey]['germplasmDbId'].'',
                                'parentRole' => 'female',
                                'orderNumber' => '1',
                                'experimentDbId' => "$experimentId",
                                'entryDbId' => $femaleEntryDbId.''
                            ];
                            if(!empty($entries[$femaleEntryKey]['seedDbId'])){
                               $crossParent[$crossName][0]['seedDbId'] = $entries[$femaleEntryKey]['seedDbId'].'';
                            }
                            if(!empty($entries[$femaleEntryKey]['notes'])){
                                $crossParent[$crossName][0]['notes'] = $entries[$femaleEntryKey]['notes'];
                            }
                            $crossParent[$crossName][] = [
                                'germplasmDbId' => $entries[$maleEntryKey]['germplasmDbId'].'',
                                'parentRole' => 'male',
                                'orderNumber' => '2',
                                'experimentDbId' => "$experimentId",
                                'entryDbId' => $maleEntryDbId.''
                            ];
                            if(!empty($entries[$maleEntryKey]['seedDbId'])){
                               $crossParent[$crossName][1]['seedDbId'] = $entries[$maleEntryKey]['seedDbId'].'';
                            }
                            if(!empty($entries[$maleEntryKey]['notes'])){
                                $crossParent[$crossName][1]['notes'] = $entries[$maleEntryKey]['notes'];
                            }
                        }else{// build duplicate germplasm list
                            $crosses[$key]['status'] = 'invalid';
                            if($fCount > 1 && $mCount == 1){   
                                $crosses[$key]['remarks'] = 'Duplicate female germplasm';    
                            }else if($fCount == 1 && $mCount > 1){
                                $crosses[$key]['remarks'] = 'Duplicate male germplasm';
                            }else if($fCount > 1 && $mCount > 1){
                                $crosses[$key]['remarks'] = 'Both parents have duplicate germplasm';
                            }

                            $duplicateList[] = [
                                'crossName' => $crossName,
                                'femaleCount' => $fCount,
                                'maleCount' => $mCount, 
                                'femaleNo' => $designationList[$female]['entno'],
                                'femaleEntryId' => $femaleEntryDbId,
                                'maleNo' => $designationList[$male]['entno'],
                                'femaleGen' => $entries[$femaleEntryKey]['generation'],
                                'maleGen' => $entries[$maleEntryKey]['generation'],
                                'maleEntryId' => $maleEntryDbId,
                                'parentDbId' => "$femaleEntryDbId,$maleEntryDbId",
                            ];
                        }
                    }
                }
            }
        }

        if(isset($parents)){ // if there is a valid cross
            if($operation == 'replace'){
                $this->crossPatternModel->deleteCrossPatternInfo($experimentId);
                $this->cross->deleteCrosses($experimentId);
            }else{
                $crossRecords = $this->cross->searchAll([
                    'fields' => 'germplasmCross.id AS crossDbId|experiment.id AS experimentDbId|parentDbId',
                    'experimentDbId' => "equals $experimentId",
                    'parentDbId' => 'equals '.implode('|equals ', array_keys($parents)),
                ])['data'];
                
                $existingCrosses = array_flip(array_column($crossRecords, 'parentDbId'));    
            }
            
            //retrieve entry list
            $entryListDbId = $this->entryListModel->searchAll(['experimentDbId' => "$experimentId"])['data'][0]['entryListDbId'];
            foreach($parents as $key => $value){
                if($operation == 'add' && isset($existingCrosses[$key])){
                    $crosses[$value]['status'] = 'invalid';
                    $crosses[$value]['remarks'] = 'Already exists in the Cross List!';
                }else{ //build insert lists
                    $insertCrossList[] = [
                        'crossName' => $crosses[$value]['crossName'].'',
                        'crossMethod' => '',
                        'experimentDbId' => "$experimentId",
                        'entryListDbId' => "$entryListDbId"
                    ];
                }
            }
            if(!empty($insertCrossList)){
                $newCrossRecords = $this->cross->create(['records' => $insertCrossList]);
                $validCrossCount = $newCrossRecords['totalCount'];
                if($newCrossRecords['status'] == 200 && count($newCrossRecords['data']) > 0){
                    foreach($insertCrossList as $key => $value){
                        $crossDbId = $newCrossRecords['data'][$key]['crossDbId'].'';
                        $crossName = $value['crossName'];

                        $indexKey = key($crossParent[$crossName]);
                        $crossParent[$crossName][$indexKey]['crossDbId'] = $crossDbId;
                        $crossParent[$crossName][$indexKey+1]['crossDbId'] = $crossDbId;
                        $insertCrossParentList[] = $crossParent[$crossName][$indexKey];
                        $insertCrossParentList[] = $crossParent[$crossName][$indexKey+1];
                        unset($crossParent[$crossName][$indexKey]);
                        unset($crossParent[$crossName][$indexKey+1]);
                    }
                    $this->crossParent->create(['records' => $insertCrossParentList]);
                    
                    if(isset($crossPattern) || $crossAttributes['totalCount'] > 0){
                        $crossPattern = isset($crossPattern) ? $crossPattern : null;
                        $this->crossPatternModel->saveCrossPatternRecords($experimentId, $newCrossRecords, $crossPattern, $insertCrossList, $crossAttributes);

                    }
                }
            }
        }
        $invalidCrossCount = $totalCrossCount - $validCrossCount;
        $duplicateGermplasmCount = count($duplicateList);
        $note = '';
        if($duplicateGermplasmCount && $switchInputType != 'entryCode'){

            //insert duplicate to session
            $userModel = new User();
            $userId = $userModel->getUserId();
            $sessionName = "ec_$userId"."_duplicate_germplasm"."_$experimentId";
            Yii::$app->session->set($sessionName, $duplicateList);
             
            $here = "<a href='#!' id = 'load-duplicate-btn' data-session = '$sessionName' data-target = '#designation-modal' data-toggle = 'modal' data-operation = 'add'>here to load the germplasm form</a>";
            $note = "<br/><br/> <i class='orange-text smaller fa fa-exclamation-circle'></i> <b> NOTICE: </b>
             Some crosses have parent/s with duplicate germplasm. You may select the right entry 
             before adding it to the cross list. Click <u>$here</u>.";
             $userModel = new User();
             $userId = $userModel->getUserId();
             $sessionName = "ec_$userId"."_duplicate_germplasm"."_$experimentId";
     
        }

        $message = "
            <ul>
                <li><i class='green-text smaller fa fa-check-circle'></i> Total of valid crosses: <b> " .number_format($validCrossCount). "</b></li>
                <li><i class='red-text smaller fa fa-question-circle'></i> Total of invalid crosses: <b>  " .number_format($invalidCrossCount). "</b></li>
            </ul>
            Valid crosses are automatically added in the cross list. Invalid crosses are left out in the text area for checking - hover the icon located on each row for more details.
            $note";

        $results = [
            'crosses' => $crosses,
            'invalidCount' => $invalidCrossCount,
            'message' => $message,
        ];
        
        return json_encode($results);
    }

    /**
     * Update crossing matrix
     *
     * @param $experimentId integer experiment identifier
     */
    public function actionUpdateCrossingMatrix($experimentId){
        $type = isset($_POST['type']) ? $_POST['type'] : 0;
        $pos = isset($_POST['pos']) ? $_POST['pos'] : 0;
        $femaleId = isset($_POST['femaleId']) ? $_POST['femaleId'] : 0;
        $maleId = isset($_POST['maleId']) ? $_POST['maleId'] : 0;
        $method = isset($_POST['method']) ? $_POST['method'] : 'single';
        $value = isset($_POST['value']) ? $_POST['value'] : '';

        CrossingMatrix::updateCrossingMatrix($experimentId, $type, $pos, $femaleId, $maleId, $method, $value);
    } 

    /**
     * Retrieve crossing matrix data
     * @param $experimentId integer experiment identifier
     * @return $result array crossing matrix data
     */
    public function actionretrieveCrossingMatrixTempData($experimentId){
        $result = CrossingMatrix::retrieveCrossingMatrixTempData($experimentId);

        return json_encode($result);
    }

    /**
     * Crossing matrix
     *
     * @param $program string program identifier
     * @param $id integer experiment identifier
     * @param $processId string process identifier
     *
     */
    public function actionCrossingMatrix($program, $id, $processId){

        $this->layout = '@app/views/layouts/dashboard';
        $time_start = microtime(true); 
        $session = Yii::$app->session;

        $reload = $session->get('reload-crossing-matrix-'.$id);
        $reset = $session->get('reset-crossing-matrix-'.$id);

        if($reload){
            $data = CrossingMatrix::getCrossingMatrixData($id,'reload');
        }else if($reset){
            $data = CrossingMatrix::getCrossingMatrixData($id,'reset');
        }else{
            $data = CrossingMatrix::getCrossingMatrixData($id);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);

        return $this->render('crossing_matrix_box',[
            'program' => $program,
            'id' => $id,
            'processId' => $processId,
            'data' => $data
        ]);
    }

    /**
     * Reload crossing matrix
     */
    public function actionReloadCrossingMatrix(){
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $session = Yii::$app->session;
        $session->set('reload-crossing-matrix-'.$id, true);
    }

    /**
     * Reset crossing matrix
     */
    public function actionResetCrossingMatrix(){
        $userModel = new User();
        $userId = $userModel->getUserId();        
        $id = isset($_POST['id']) ? $_POST['id'] : 0;

        $session = Yii::$app->session;
        $session->set('reset-crossing-matrix-'.$id, true);

        // update tempotary data to reset
        CrossingMatrix::resetCrossingMatrix($id, $userId);
    }

    /**
     *  Save the changes in the cross table
     *
     * @param $id integer experiment identifier
     *
    */
    public function actionSaveCrosses($id){

        if(isset($_POST)){
            $mode = $_POST['mode'];

            if($mode == 'single'){
                $cross_no = intval($_POST['crossNo']);
                $input = $_POST['input'];
                $column = $_POST['column'];

                if(strtolower($column) == 'method'){
                    $column = 'crossMethod';
                }

                $id = $cross_no;
                $requestBody = ["$column" => "$input"];
                $this->cross->updateOne($cross_no, $requestBody);
            }elseif($mode == 'all'){

            }
        }
    }

    /**
     * Render Cross Bulk Update
     *
     * @param $id integer experiment identifier
     * @param $program string program identifier
     *
     */
    public function actionCrossBulkUpdate($id,$program){
        $variableArr = [];
        $configVars = [];

        if(isset($_POST)){
            $variableArr = [
                'cross_method' => 'CROSS_METHOD',
                'remarks' => 'REMARKS'
            ];
            $count = 1;
            foreach($variableArr as $key => $value){
                if($value == 'CROSS_METHOD'){
                    //get scale values for cross method
                    $methodTags = $this->mainExperimentModel->getTags();

                    $configVars[] = [
                        'disabled' => false,
                        'field_description' => 'Cross Method',
                        'field_label' => 'Cross Method',
                        'order_number'=>$count,
                        'required' => 'required',
                        'variable_abbrev' => $value,
                        'target_value' => 'value',
                        'allowed_values' => $methodTags
                        
                    ];
                }else{
                    $configVars[] = [
                        'disabled' => false,
                        'field_description' => 'Remarks',
                        'field_label' => 'Remarks',
                        'order_number'=>$count,
                        'variable_abbrev' => strtoupper($value)
                    ];
                }

                $count++;
            }

            $htmlData = $this->renderPartial('cross_update_form.php',[
                'data' => $variableArr,
                'id' => $id,
                'configVars' => $configVars,
                'program' => $program
            ], true, false);

            return json_encode($htmlData);

        }
    }

    /**
     * Single update of cross variables(methods and remarks)
     *
     * @param $id experiment identifier
     * @param $program string program identifier
     *
     */
    public function actionUpdateCrossVariable($id, $program){
        
        extract($_GET);
        $pagesParam = '';
        
        
        if(isset($_POST)){
          
            $input_value = $_POST['inputVal'];

            $variable_abbrev = strtoupper($_POST['variable_abbrev']);
            $varEntry = $this->mainVariableModel->getVariableByAbbrev(strtoupper($variable_abbrev));

            $prevCols = Yii::$app->session['EntryListSearchSession'.$id];

            //update entry class of this
            if($_POST['mode'] == 'selected'){
                $crossJson = isset($_POST['idsArr'])? $_POST['idsArr'] : [];
                $crossIdArray = [];
                if(!empty($crossJson)){    
                    $crossIdArray = json_decode($crossJson);
                }
                
                $selectedStr = implode(',', $crossIdArray);
                
                $this->mainExperimentModel->saveBulkCrosses($id, $variable_abbrev, $input_value, 'update',$_POST['mode'],$crossIdArray);
            } elseif($_POST['mode'] == 'all') {
                $this->mainExperimentModel->saveBulkCrosses($id, $variable_abbrev, $input_value, 'update',$_POST['mode']);
            }
          
            $pageSession = isset(Yii::$app->session['paginationSession']) ? Yii::$app->session['paginationSession'] : '';
            $params_temp = '';

            if(!empty($prevCols)){
                foreach ($prevCols as $key => $value) {
                    $entities = trim(urlencode('EntryListSearch'.'['.$key.']'));
                    $entity_value = $entities.'='.$value;
                    $params_temp = empty($params_temp) ?  $entity_value : $params_temp. '&'.$entity_value;
                }
            }

            if(!empty($_POST['page']) && !empty($_POST['perPage'])){
                $pagesParam = '&page='.$_POST['page'].'&per-page='.$_POST['perPage'];
            }

            if(!empty($_POST['dp1_page']) && !empty($_POST['dp1_perPage'])){
                $pagesParam = !empty($pagesParam) ? $pagesParam.'&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'] : '&dp-1-page='.$_POST['dp1_page'].'&dp-1-per-page='.$_POST['dp1_perPage'];
            }

        }
 
        if(isset($_POST['isRow']) && $_POST['isRow'] == true){
             // Do nothing, page will not reload
        }
        else{

            return json_encode(TRUE);
        }
    }

    /**
     * Get crosses batch count
     *
     * @param $id integer experiment identifier
     *
     */
    public function actionGetCrossesBatchCount($id){
        $idsArr = isset($_POST['idsArr'])? json_decode($_POST['idsArr']) : [];

        $batchCount = $this->crossPatternModel->getCrossesBatchCount($id, $idsArr);

        return json_encode($batchCount);
    }

    /**
     * Delete crosses 
     *
     * @param $id integer experiment identifier
     * @param $program string program identifier
     *
     */
    public function actionRemoveCrosses($id, $program){
        extract($_GET);
        $pagesParam = '';

        if(isset($_POST)){

            $prevCols = Yii::$app->session['EntryListSearchSession'.$id];

            //update entry class of this
            if($_POST['mode'] == 'selected'){
                $crossJson = isset($_POST['idsArr'])? $_POST['idsArr'] : [];
                $crossIdArray = json_decode($crossJson);
                $selectedStr = implode(',', $crossIdArray);
                $this->mainExperimentModel->saveBulkCrosses($id, NULL, NULL, 'delete',$_POST['mode'],$crossIdArray);
            } elseif($_POST['mode'] == 'all') {
                $this->mainExperimentModel->saveBulkCrosses($id, NULL, NULL, 'delete',$_POST['mode']);

                //To update the status of the experiment
                $experimentModel = $this->findModel($id);
                $status = explode(';', $experimentModel['experimentStatus']);

                if(in_array('cross list created', $status)){
                    $index = array_search('cross list created', $status);
                    if(isset($status[$index])){
                        unset($status[$index]);

                        $updatedStatus = implode(';', $status);
                        $experimentModel['experimentStatus'] = $updatedStatus;
                    }

                   $this->mainExperimentModel->updateOne($id, ["experimentStatus"=>"$updatedStatus"]);
                }
            }

            $pageSession = isset(Yii::$app->session['paginationSession']) ? Yii::$app->session['paginationSession'] : '';
            $params_temp = '';

            if(!empty($prevCols)){
                foreach ($prevCols as $key => $value) {
                    $entities = trim(urlencode('EntryListSearch'.'['.$key.']'));
                    $entity_value = $entities.'='.$value;
                    $params_temp = empty($params_temp) ?  $entity_value : $params_temp. '&'.$entity_value;
                }
            }

        }

        if(isset($_POST['isRow']) && $_POST['isRow'] == true){
             // Do nothing, page will not reload
        }
        else{
            return json_encode(TRUE);
        }
    }

    /**
     * Create pairs of male and female parent
     */
    public function actionPairParents(){
        $results = null;
        $crosses = [];
        $invalidCross = 0;
        $userModel = new User();
        $userId = $userModel->getUserId();
        $experimentDbId = $_GET['id'];
        $femaleDesignations = isset($_POST['femaleDesignations']) ? json_decode($_POST['femaleDesignations'], true) : null;
        $maleDesignations = isset($_POST['maleDesignations']) ? json_decode($_POST['maleDesignations'], true) : null;

        if(isset($femaleDesignations, $maleDesignations)){

            foreach($femaleDesignations as $femaleDesignation){
                foreach($maleDesignations as $maleDesignation){

                    $crossName = "$femaleDesignation|$maleDesignation";
                    if($femaleDesignation !== $maleDesignation){
                        //valid cross name
                        $crosses[] = $crossName;
                    }else{//count invalid crosses
                        $invalidCross++;
                    }
                }
            }
        }

        $crossString = join("\n", $crosses);

        $results = [
            'invalidCross' => $invalidCross,
            'crossString' => $crossString
        ];

        return json_encode($results);
    }

    /**
     * Create self crosses
     */
    public function actionCreateSelfCrosses(){

        $newCrossCount = 0;
        $crosses = [];
        if(isset($_POST['experimentDbId'], $_POST['designations'])){
            extract($_POST);
            
            $userModel = new User();
            $userId = $userModel->getUserId();
            $designations = json_decode($_POST['designations'], true);
            $designations = isset($designations) ? array_unique($designations) : [];//retrieve first germplasm in case of duplicates

            foreach($designations as $key => $value){
                $crosses[$key] = $value;
            }

            $params = [
                'distinctOn' => 'germplasmDesignation',
                'germplasmDesignation' => 'equals '.implode("|equals ", $crosses),
                'experimentDbId' => "equals $experimentDbId",
                'crossParentRole' => 'equals female-and-male',
            ];
          
            $existingCrosses = $this->crossParent->searchAll($params)['data'];
            $entryDbIds = array_flip($crosses);

            foreach($existingCrosses as $existingCross){
                $designation = $existingCross['germplasmDesignation'];
                if(isset($entryDbIds[$designation])){
                    unset($entryDbIds[$designation]);
                }
            }
            if(count($entryDbIds)>0){
                $newCrossCount =  $this->cross->createSelfCrosses($entryDbIds, $experimentDbId);
                // Entry::updateMany($entryDbIds, ['entryRole' => 'female-and-male']);//removed for B4R-7915 but preserving this as the requirement might still change in the future
            }
        }

        return json_encode($newCrossCount);
    }

    /**
     * Checks when to enable the next btn for function crosses
     *
     * @param $id integer experiment identifier
     */
    public function actionEnableNext($id){

       $param = ["experimentDbId"=>"$id"];
       $apiEndPoint = 'crosses-search';
       $crossData = Cross::searchAllData($apiEndPoint, $param);
       $countOfCrosses = $crossData['body']['metadata']['pagination']['totalCount'];

       return json_encode($countOfCrosses);

    }

    /**
     * Specify the design of the nursery experiment's plant selection
     * 
     * @param $program string program identifier
     * @param $id integer experiment identifier
     * @param $processId string process identifier
     */
    public function actionSpecifyCrossDesign($program,$id,$processId){
        $model = $this->findModel($id);
        $experiment = $this->mainExperiment->getOne($id)['data'];
        $experimentType = $experiment['experimentType'];
        $refreshManageBlocks = false;

        if($model['experimentDesignType'] == 'Systematic Arrangement'){
            $tabDesign = "design-add-blocks";
            $tab = 'add-blocks';
        } else{
            $tabDesign = "design-entry-order";
            $tab = 'entry-order';
        }

        if($model['notes'] == 'updatedEntryList'){
            //delete blocks
            $this->entryOrderModel->resetDesign($id);
            if($tab == 'entry-order' && $experimentType == 'Observation'){
                $changeToEntryOrder = 'true';
                $tabDesign = "design-add-blocks";
            }
        }

        $params = ["experimentDbId" => "$id", "parentExperimentBlockDbId" => "is null"];
        $data = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params);
        $changeToEntryOrder = 'false';
        if(count($data['data']) == 0) {    // add default block
            $this->plantingArrangement->addNewRows($id, 1);
            $data = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params);

            if($model['experimentDesignType'] == 'Entry Order' && count($data['data']) == 1 && $experimentType != 'Observation'){
                $this->mainExperiment->updateOne($id, ["experimentDesignType" => 'Systematic Arrangement']);
                $model = $this->findModel($id);
                $changeToEntryOrder = 'true';
                $tabDesign = "design-add-blocks";
                $tab = 'add-blocks';
            }
        }

        $experimentUsesCrossPattern = false;
        // If experiment type is ICN and Cross Pattern is used
        if($experimentType == 'Intentional Crossing Nursery'){
            $params = ["experimentDbId" => "equals $id"];
            $data = \Yii::$container->get('app\models\CrossAttribute')->searchAll($params, 'limit=1&sort=crossAttributeDbId:desc', false);

            if(!empty($data['data'])) {
                $changeToEntryOrder = $model['experimentDesignType'] == 'Entry Order'? 'false' : 'true';
                $tabDesign = "design-entry-order";
                $experimentUsesCrossPattern = true;
            }
        }

        //check if there's first plot pos variable, create if none
        $this->entryOrderModel->checkFirstPlotPos($id);

        // Initialize entry order for Observation experiment and ICN with cross pattern so no need to trigger the refresh of the page
        if(($experimentType == 'Observation' || $experimentUsesCrossPattern) && (isset($experiment['experimentDesignType']) && $experiment['experimentDesignType'] != 'Entry Order')) {
            $tabDesign = "design-entry-order";
            $tab = 'entry-order';
            $changeToEntryOrder = 'false';

            $this->mainExperiment->updateOne($id, ["experimentDesignType" => 'Entry Order']);

            // clear entry list replicate data (set all reps to null)
            \Yii::$container->get('app\modules\experimentCreation\models\PAEntriesModel')->updateEntryListReplicate($id, null, [null], 'overwrite');

            //delete all records of experiment blocks
            $params = [
                'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                    experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.order_number AS orderNumber',
                "experimentDbId" => "equals $id"
            ];
            $experimentBlockData = $this->experimentBlock->searchExperimentBlocks($params);

            $experimentBlockCount = isset($experimentBlockData[0]['orderNumber']) ? count($experimentBlockData) : 0;

            if($experimentBlockCount > 0){
                if($experimentBlockCount == 1){
                    if(!strpos($experimentBlockData[0]['experimentBlockName'], 'Entry order')){
                        $this->experimentBlock->DeleteOne($experimentBlockData[0]['experimentBlockDbId']);
                        $this->plantingArrangement->addNewRows($id, 1, 'entry order');
                    }
                } else {
                    $blockData = ArrayHelper::map($experimentBlockData, 'experimentBlockDbId', 'experimentBlockDbId');
                    $listOfExptBlockIds = array_keys($blockData);

                    $this->experimentBlock->deleteMany($listOfExptBlockIds);
                    $this->plantingArrangement->addNewRows($id, 1, 'entry order');
                }
                //Check if there are checks metadata
                $abbrevArray = [
                    'CHECK_GROUPS',
                    'BEGINS_WITH_CHECK_GROUP',
                    'ENDS_WITH_CHECK_GROUP',
                    'CHECK_GROUP_INTERVAL',
                    'ENTRY_LIST_TIMES_REP',
                    'PA_ENTRY_WORKING_LIST'
                ];
                $abbrevStr = 'equals '.implode('|equals ', $abbrevArray);
                $expData = $this->mainExperimentData->getExperimentData($id, ["abbrev" => $abbrevStr]);

                if(isset($expData) && count($expData) > 0){
                    $existingRec = array_column($expData, 'experimentDataDbId');
                    $this->mainExperimentData->deleteMany($existingRec);
                }

            }

            $this->plantingArrangement->checkExperimentPAUpdate($id);
        }

        return $this->render('specify_cross_design',[
            'program' => $program,
            'model' => $model,
            'id' => $id,
            'processId' => $processId,
            'tabDesign' => $tabDesign,
            'tab' => $tab,
            'experimentType' => $experimentType,
            'changeToEntryOrder' => $changeToEntryOrder,
            'experimentUsesCrossPattern' => $experimentUsesCrossPattern
        ]);
    }

    /**
     * Assign entries
     */
    public function actionAssignEntries($program,$id,$processId){
        $model = Experiment::findOne($id);

        return $this->render('assign_entries',[
            'program' => $program,
            'id' => $id,
            'processId' => $processId,
            'model' => $model
        ]);
    }

  /**
   * Update the experiment variable
   * 
   * @param $id string experiment identifier
   * @param $program string program identifier
   */

  public function actionUpdateExptVariable($id,$program){
    
      if(isset($_POST)){
        $column = $_POST['memtype'];
        $inputVal = $_POST['val'];

        if($column == 'location-rep'){
            $column = 'location_rep_count';
        }

        $experimentModel = Experiment::findOne($id);
        $existtingLocRep = $experimentModel->location_rep_count;
        if($existtingLocRep != $inputVal){
            $experimentModel->$column = $inputVal;
            $experimentModel->save();
        }

        //delete existing studies
        ExperimentModel::deleteExistingStudies($id);
      }
      return json_encode(true); 
  }

    /**
     * Preview crossing matrix
     * @return $htmlData html data browser for preview crossing matrix
     */
    public function actionPreviewCrossingMatrix(){
        $userModel = new User();
        $userId = $userModel->getUserId();
        
        $id = isset($_POST['id']) ? $_POST['id'] : [];

        $data = CrossingMatrix::proccessCrossingMatrixDataPreview($id, $userId);

        $htmlData = Yii::$app->controller->renderPartial('_preview_crossing_matrix',[
            'data' => $data
        ]);

        return json_encode($htmlData);
    }

    /**
     * Get duplicate designation
     * @return $htmlData html data browser for preview crossing matrix
     */
    public function actionGetDuplicateList(){
        extract($_POST);
        
        $duplicateList = Yii::$app->session->get($session);
        $data = $this->mainExperimentModel->getDuplicateListData($duplicateList);

        $htmlData = Yii::$app->controller->renderPartial('_entry_number_form',[
            'data' => $data
        ]);

        return json_encode($htmlData);
    }

    /**
     * Add stored crosses
     */
    public function actionAddStoredCrosses(){
        
        extract($_POST);
        $userModel = new User();
        $userId = $userModel->getUserId();
        $sessionName = "ec_$userId"."_duplicate_germplasm"."_$experimentId";
        $duplicateList = Yii::$app->session->get($sessionName);
        $entryListId = $this->entryListModel->getEntryListId($experimentId);
        //entry records
        $entries = array_flip(array_flip(array_merge(array_column($duplicateList, 'femaleEntryId'),array_column($duplicateList, 'maleEntryId'))));
        
        $params = [
            'fields' => 'entry.id AS entryDbId|germplasm.designation|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|experiment.id AS experimentDbId|entry.notes as notes',
            'experimentDbId' => "equals $experimentId",
            'entryDbId' => 'equals '.implode('|equals ', $entries),
        ];

        $entries = $this->entryModel->searchAll($params)['data'];
        $entryList = [];
        foreach($entries as $key => $value){
            $entryList[$value['entryDbId']] = $value;
        }

        //check if there's existing cross pattern data
        $variable = $this->mainVariableModel->getVariableByAbbrev('CROSSING_PATTERN');
        $sqlData = [
            'experimentDbId'=> "equals $experimentId",
            'variableDbId' => "equals ".$variable['variableDbId']
        ];
        $crossAttributes = $this->crossAttribute->searchAll($sqlData); 
        if(isset($crossPattern) && $crossAttributes['totalCount'] == 0){
            //delete existing crosses
            $this->cross->deleteCrosses($experimentId);
            Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The previously created cross records for this experiment was deleted to accommodate the cross pattern.');
        }

        $parents = array_flip(array_flip(array_column($duplicateList, 'parentDbId')));
        if($operation == 'replace'){
            $this->crossPatternModel->deleteCrossPatternInfo($experimentId);
            $this->cross->deleteCrosses($experimentId);
        }else{
            $crossRecords = $this->cross->searchAll([
                'fields' => 'germplasmCross.id AS crossDbId|experiment.id AS experimentDbId|parentDbId',
                'experimentDbId' => "equals $experimentId",
                'parentDbId' => 'equals '.implode('|equals ', $parents),
            ])['data'];
            
            $existingCrosses = array_flip(array_column($crossRecords, 'parentDbId'));    
        }
        $invalidCount = 0;

        foreach($parents as $key => $value){

            if($operation == 'add' && isset($existingCrosses[$value])){
                $invalidCount++;
            }else{ //build insert lists
                $par = explode(',',$value);
                $female = $entryList[$par[0]];
                $male = $entryList[$par[1]];

                $crossName = $female['designation'].'|'.$male['designation'];
                $crossParent[$crossName][] = [
                    'germplasmDbId' => $female['germplasmDbId'].'',
                    'seedDbId' => $female['seedDbId'].'',
                    'parentRole' => 'female',
                    'orderNumber' => '1',
                    'experimentDbId' => "$experimentId",
                    'entryDbId' => $female['entryDbId'].'',
                ];
                
                if($female['notes'] != null){
                    $crossParent[$crossName][0]['notes'] = $female['notes'];
                }

                $crossParent[$crossName][] = [
                    'germplasmDbId' => $male['germplasmDbId'].'',
                    'seedDbId' => $male['seedDbId'].'',
                    'parentRole' => 'male',
                    'orderNumber' => '2',
                    'experimentDbId' => "$experimentId",
                    'entryDbId' => $male['entryDbId'].'',
                ];
                if($male['notes'] != null){
                    $crossParent[$crossName][1]['notes'] = $male['notes'];
                }
                $insertCrossList[] = [
                    'crossName' => $crossName,
                    'crossMethod' => '',
                    'experimentDbId' => "$experimentId",
                    'entryListDbId' => "$entryListId"
                ];
            }
        }
        if(!empty($insertCrossList)){
            $newCrossRecords = $this->cross->create(['records' => $insertCrossList]);
            $validCrossCount = $newCrossRecords['totalCount'];

            foreach($insertCrossList as $key => $value){
                $crossDbId = $newCrossRecords['data'][$key]['crossDbId'].'';
                $crossName = $value['crossName'];
                $crossParent[$crossName][0]['crossDbId'] = $crossDbId;
                $crossParent[$crossName][1]['crossDbId'] = $crossDbId;
                $insertCrossParentList[] = $crossParent[$crossName][0];
                $insertCrossParentList[] = $crossParent[$crossName][1];
            }
            
            $newCrossParentRecord = $this->crossParent->create(['records' => $insertCrossParentList]);

            if(isset($crossPattern) || $crossAttributes['totalCount'] > 0){
                        $crossPattern = isset($crossPattern) ? $crossPattern : null;
                        $this->crossPatternModel->saveCrossPatternRecords($experimentId, $newCrossRecords, $crossPattern, $insertCrossList, $crossAttributes);
                    }
        }

        $result = [
            'totalCount' => count($duplicateList),
            'invalidCount' => $invalidCount
        ];

        return json_encode($result);
    }

    /**
     * Update valid list array session value
     */
    public function actionUpdateSession(){
        extract($_POST);
        $userModel = new User();
        $userId = $userModel->getUserId();
        $sessionName = "ec_$userId"."_duplicate_germplasm"."_$experimentId";
        $_SESSION[$sessionName][$key]['femaleEntryId'] = $femaleEntryDbId;
        $_SESSION[$sessionName][$key]['maleEntryId'] = $maleEntryDbId;
        $_SESSION[$sessionName][$key]['parentDbId'] = $parentDbId;

        return json_encode('updated parentDbId');
    }

    /**
     * Confirm update crossing matrix
     *
     * @return $count total number of crosses
     */
    public function actionConfirmUpdateCrossingMatrix(){
        $userModel = new User();
        $userId = $userModel->getUserId();
        $id = isset($_POST['id']) ? $_POST['id'] : [];
        $session = Yii::$app->session;

        $session->set('reload-crossing-matrix-'.$id, false);
        $session->set('reset-crossing-matrix-'.$id, false);

        // retrieve crossing matrix data
        $data = CrossingMatrix::retrieveCrossingMatrixTempData($id, true);
        $res = CrossingMatrix::updateCrossingListByMatrix($data, $id, $userId);

        $count = count($data);

        $tempData = CrossingMatrix::retrieveCrossingMatrixTempData($id);
        $session->set('dataArr-'.$id, $tempData);
        return $count;
    }

    /**
     * Get number of selected crosses in crossing matrix
     *
     * @param $id integer experiment identifier
     * @return $count integer total nof of selected crosses
     */
    public function actionGetNoOfSelectedCrosses(){
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $count = CrossingMatrix::getNoOfSelectedCrosses($id);

        return $count;
    }

    /**
     * Generic checking of required columns
     * @param $id integer exeperiment identifier
     * @return $data json encoded list of required variables with no values
     */
    public function actionGenericCheckRequired($id){
        $data = '';
        if(isset($_POST)){
            $reqVarConfig = json_decode($_POST['reqVarConfig'], true);
            
            $data = '';
            $count = 1;
            $data = ExperimentModel::genericCheckRequiredData($id, $reqVarConfig);
        }
        return json_encode($data);
    }

    /**
     * Checks if there are any unused parents within the experiment
     * @param $id integer experiment identifier
     * @return $result boolean true if there are unused parents 
     */
    public function actionCheckParents($id){

        $params = [
            'experimentDbId' => "equals $id",
            'femaleCount' => 'equals 0',
            'maleCount' => 'equals 0',
            'fields' => 'experiment.id as experimentDbId|femaleCountField|maleCountField|entry.id AS entryDbId',
            'includeParentCount' => true,
        ];
        $filters = 'limit=1&sort=entryDbId:asc';
        $entries = $this->entryModel->searchAll($params, $filters, false);

        return json_encode($entries['totalCount']);
    }

    /**
     * Check the experiment status to enable or disable the next button
     * @param $id integer experiment identifier
     * @param $program Program entry point
     * @param $processId integer process path identifier 
     * @return $return boolean  true/false if the experiment is equal to the required status 
     */
    public function actionCheckExperimentStatus($id, $program, $processId){

        if(isset($_POST)){
            $view = $_POST['view'];
    
            $activities = $this->formModel->prepare($view,$program,$id,$processId);
            $index = array_search($activities['active'], array_column($activities['children'],'url'));    
            $activityAbbrev = $activities['children'][$index]['abbrev'];
          
            $activityMod = $activityAbbrev.'_MOD';
            
            $moduleReqStatus = PlatformModule::find()->where(['abbrev'=>$activityMod,'is_void'=>false])->one();
            $requiredStatus = $moduleReqStatus['required_status'];

            //Check existing status of the experiment
            $experimentModel = Experiment::find()->where(['id'=>$id, 'is_void'=>false])->one();
            $experiment_status = isset($experimentModel->experiment_status) ? strtolower($experimentModel->experiment_status) : '';
            
            
            if(strpos($experiment_status,strtolower($requiredStatus)) !== false){
                $return = false;
            }else{
                $return = true;
            }

            return json_encode($return);
        }
    }

    /**
     * Validation of next step 
     * @param $id integer Experiment Id 
    */
    public function actionValidateRequired($id,$action=null,$options=null){
        if(isset($_POST) || !empty($action)){
            if(!empty($options)){
                extract($options);
            }else{
                $configValues = json_decode($_POST['configValues'], true);
                $type = $_POST['type'];
            }
          
            $noRecordArr = [];
            $finalArr = [];
            $noRecordFlag = false;
   
            $apiParams = [
                'apiResourceEndpoint' => 'entries-search',
                'apiResourceMethod' => 'POST',
                'apiResourceFilter' => ["experimentDbId"=>"$id"],
                'apiResourceSort' => ''
            ];

            //Check if the required variables have been filled before experiment status is changed
            $checkExptStatus = ExperimentModel::genericCheckRequiredData($id,$configValues, $apiParams);
    
            $finalArr[] = ['display_name'=>'Required Fields', 'value'=>$noRecordArr];
            $htmlData = $this->renderPartial('/protocol/notif_preview',['listOfNoRecordVal'=>$finalArr]);
            
            if($action == 'cross-validate-tab'){
                return $noRecordFlag;
            }else{
                return json_encode([
                    'htmlData' => $htmlData,
                    'flag' => $noRecordFlag
                ]);
            }
        }
    }

    /**
     * Redirect user to experiment browser
     * 
     * @param Integer $id experiment record identifier
     * @param String $program program record identifier
     * @param String $latestStatus latest experiment status
     */
    public function actionRedirectToIndex($id, $program, $latestStatus){

        $model = $this->findModel($id);

        if(strpos($latestStatus,'in progress') !== false || strpos($latestStatus,'in queue') !== false){
            $message = ucFirst($latestStatus).' for experiment <b>'.$model['experimentName'].'</b>. You will be notified once the background processing is done.';
        } else if(strpos($latestStatus,'design requested') !== false){ //check if design requested
            $message = 'Design generation for experiment <b>'.$model['experimentName'].'</b> is in progress. You are redirected to the design page to prevent any modification.';
            Yii::$app->session->setFlash('warning', '<i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> '. $message);

            return $this->redirect(['create/specify-design?program='.$program.'&id='.$id.'&processId='.$model['dataProcessDbId']]);
        } 
        else{
            $latestStatus = 'created' ? 'finalized' : $latestStatus;
            $message = 'Unable to update <b>'.$model['experimentName']."</b>. This experiment is already $latestStatus.";
        }

        Yii::$app->session->setFlash('warning', '<i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> '. $message);


        return $this->redirect(['create/index?program='.$program]);
    }

    /**
     * Redirect user to experiment browser
     * 
     * @param Integer $id experiment record identifier
     * @param String $program program record identifier
     * @param String $latestStatus latest experiment status
     */
    public function actionRedirectCurrentStep($id, $program, $processId){

        $model = $this->findModel($id);

        if(Yii::$app->access->renderAccess("EXPERIMENT_CREATION_UPDATE","EXPERIMENT_CREATION", $model['creatorDbId'], usage:'url')){
            $experimentStatus =!empty($model) ? $model['experimentStatus'] : "" ;
            
            $currentUrl = !empty($currentUrl) ? $currentUrl : null;
            $activities = $this->formModel->prepare($currentUrl, $program, $id, $processId);
            
            $parsedExperimentStatus = explode(';',$experimentStatus);
            $latestStatus = end($parsedExperimentStatus);
            if($latestStatus == '' && !empty($parsedExperimentStatus) && count($parsedExperimentStatus) > 2){
                $latestStatus = $parsedExperimentStatus[count($parsedExperimentStatus)-2];
            }
            
            $activeUrlTab = '';
            if(!empty($activities)){
                for($i=0; $i<count($activities['children']); $i++){
                    if(trim($latestStatus) == $activities['children'][$i]['status']){
                        $activeUrlTab = $activities['children'][$i]['url'];
                    }
                }
            }
            if($latestStatus == 'design requested'){
                $activeUrlTab = '/experimentCreation/create/specify-design';
            } else if($latestStatus == 'draft'){
                $activeUrlTab = '/experimentCreation/create/specify-entry-list';
            }else if($activeUrlTab == ''){
                $activeUrlTab = '/experimentCreation/create/specify-basic-info';
            }
            $url = Url::to([$activeUrlTab,'id'=>$id,'program'=>$program, 'processId'=>$processId]);
            
            return $this->redirect($url);
        }
    }


    /**
     * Finds the Experiment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Experiment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = $this->mainExperiment->getExperiment($id)) !== null) {
            return $model;
        } else {
            return [];
        }
    }

    /**
     * Retrieves the values for the filter columns of the entry list browser.
     * @param integer $id entry list identifier
     * @param boolean $designTab flag if filter will be used in design tab
     * 
     * @return json data for the Select2 widget
     */
    public function actionGetFilterData($id, $designTab=false) {
        $searchModel = $model = $this->entrySearchModel;
        $params = isset($_POST['filter']) ? $_POST['filter'] : '';
        $column = isset($_POST['column']) ? $_POST['column'] : '';
        $page = isset($_POST['page']) ? $_POST['page'] : '';

        $data = $searchModel->getEntriesFilter($id, $params, $column, $page, $designTab);
        
        return json_encode($data);
    }

    /**
     * Retrieves the values for the filter columns of the occurrence browser.
     * @param integer $id entry list identifier
     * @return json data for the Select2 widget
     */
    public function actionGetFilterOccurrences($id) {
        $searchModel = $model = $this->occurrenceSearch;
        $params = isset($_POST['filter']) ? $_POST['filter'] : '';
        $column = isset($_POST['column']) ? $_POST['column'] : '';

        $data = $searchModel->getOccurrencesFilter($id, $params, $column);

        return json_encode($data);
    }

    /**
     * Check if the process threshold has been met
     * @param Integer $id Experiment ID
     */
    public function actionCheckThresholdCount($id){
        $userModel = new User();
        $userId = $userModel->getUserId();
        $flag = false;
           
        if(isset($_POST)){
            $toolStep = $_POST['toolStep'];
            $action = $_POST['action'];
            
            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', $action)) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', $action) : 500;

            if($toolStep == 'entries'){
                $method = $_POST['method'];

                //Verify if the update of entries will use the background process
                if($method == 'update' || $method == 'delete'){
                    $dataCount = $_POST['selectedItemsCount'];

                    if($dataCount != null && $dataCount != 0){
                        if(isset($_POST['mode']) && $_POST['mode'] == 'current-page'){
                            $selectedItemsCount = $dataCount;
                        } else {
                            $selectedIdName = "ec_$userId"."_selected_entry_ids_$id";
                            $selectedItems = Yii::$app->session->get($selectedIdName);
                            $selectedItemsCount = isset($_POST['selectedItemsCount']) ? $_POST['selectedItemsCount'] : count($selectedItems);
                        }
                    }else{
                        $entryParams = ["fields"=>"entry.id AS entryDbId|experiment.id AS experimentDbId","experimentDbId"=>"equals ".$id];
                        $entriesData = $this->entryModel->searchAll($entryParams, 'limit=1&sort=entryDbId:desc', false);
                       
                        $selectedItemsCount = !empty($entriesData['totalCount']) ? $entriesData['totalCount'] : 0;
                    }
                    
                    if($selectedItemsCount >= $bgThreshold){
                        $flag = true;   
                    }
                }else if($method="create"){
                    if(isset($_POST['mode']) && $_POST['mode'] == 'copy'){
                        $entryParams = ["fields"=>"entry.id AS entryDbId|experiment.id AS experimentDbId","experimentDbId"=>"equals ".$id];
                        $entriesData = $this->entryModel->searchAll($entryParams, null, true);
                       
                        $dataCount = !empty($entriesData['totalCount']) ? $entriesData['totalCount'] : 0;
                        if($dataCount >= $bgThreshold){
                            $flag = true;
                        }
                    }else if(isset($_POST['mode']) && $_POST['mode'] == 'savedList'){
                        $savedListId = $_POST['savedListId'];
        
                        //get platformlist members
                        $list = PlatformListMember::getListMembers($savedListId, true);
                        $listMembers = isset($list[0]['members']) ? $list[0]['members'] : [];
                        $dataCount = !empty($listMembers) ? count($listMembers) : 0;
                        if($dataCount >= $bgThreshold){
                            $flag = true;
                        }
                    }else{
                        $dataCount = $_POST['selectedItemsCount'];
                        if($dataCount >= $bgThreshold){
                            $flag = true;
                        }
                    }
                }
            }elseif($toolStep == 'occurrences'){
                $method = $_POST['method'];
                //Verify if the update of entries will use the background process
                if($method == 'update'){
                    $dataCount = $_POST['selectedItemsCount'];

                    if($dataCount != null && $dataCount != 0){
                        $selectedIdName = "occ_$userId"."_selected_occurrences_$id";
                        $selectedItems = Yii::$app->session->get($selectedIdName);
                    }else{
                        $occurrenceRecords = $this->occurrenceModel->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "$$id"]);
                        $occurrences = $occurrenceRecords['data'];
                        $selectedItems = isset($occurrences) ? array_column($occurrences, 'occurrenceDbId') : []; 
                    }
                    
                    $selectedItemsCount = !empty($selectedItems) ? count($selectedItems) : 0;
                    if($selectedItemsCount >= $bgThreshold){
                        $flag = true;   
                    }
                }
            }
        }
        return json_encode($flag);
    }

    /**
     * Create Occurrence Record
     * @param integer $id Experiment ID
     * @param string $program Program Abbrev
     */
    public function actionCreateOccurrenceRecord($id, $program){
        //Check if the experiment notes has a value plantingArrangementChanged
        $experiment = $this->mainExperiment->searchAll(["fields"=>"experiment.id as experimentDbId|experiment.notes as notes","experimentDbId"=>"equals ".$id,"notes"=> "plantingArrangementChanged"]);
        $experimentNotes = isset($experiment['data'][0]['notes']) ? $experiment['data'][0]['notes'] : null;
        $deleteFlag = !empty($experimentNotes) ? true : false;

        $getExperimentTotalPlots = isset($_POST['getExperimentTotalPlots']) ? $_POST['getExperimentTotalPlots'] : 0;
        $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createOccurrences')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createOccurrences') : 500;
        //Ready to generate the records
        if($deleteFlag == false){
            $model = $this->mainExperiment->getOne($id)['data'];
            $status = $model['experimentStatus'];
            $plotRecords = $plantingInsRecords = [];
    
            $occurrenceRecords = $this->occurrenceModel->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' =>  "equals ".$id]);
            $occurrences = $occurrenceRecords['data'];
            $success = true;
            
            $statusArr = explode(';', $status);
            $statusArr = array_filter($statusArr);
            
            if(!in_array('occurrences created', $statusArr)){
                
                if(strpos($model['dataProcessAbbrev'], '_TRIAL_DATA_PROCESS') === false){
                    try {
                        //Generate Plot and Planting Instruction records
                        $plotRecords = $this->mainExperimentModel->generatePlots($id, $getExperimentTotalPlots);
                        
                        if($plotRecords['success'] == false){
                            $return = [
                                'success' => false,
                                'message' => 'Problem in generating plots. Missing layout order data (e.g. design_x, design_y, etc).',
                                'currentProcess' => 'plots and/or planting instructions'
                            ];

                            return json_encode($return);

                        }else{
                            if($plotRecords['message'] != 'redirect'){
                                if(isset($plotRecords['createPlantingInstruction']) && $plotRecords['createPlantingInstruction'] == true){
                                    $return = [
                                        'success' => $success,
                                        'createPlantingInstruction' => $plotRecords['createPlantingInstruction'],
                                        'currentProcess' => 'plots and/or planting instructions'
                                    ];
                                }else{
                                    $return = [
                                        'success' => $success,
                                        'message' => 'Successfully generated plots.',
                                        'currentProcess' => 'plots and/or planting instructions'
                                    ];
                                }
                            
                                return json_encode($return);
                            } else {
                                 $this->redirect(['create/index?program='.$program.'&id='.$id]);
                            }
                        }

                    } catch (Exception $error){

                        $return = [
                            'success' => false,
                            'message' => 'Problem in generating plots.<br> <em>Additional Information: ' .$error.'</em>',
                            'currentProcess' => 'plots and/or planting instructions'
                        ];

                        return json_encode($return);
                    }   
                }else{
                    //Normal insert
                    //saving planting instructions
                    try {
                        
                        $plantingInsRecords = $this->mainExperimentModel->generatePlantingInstructions($id);
                        
                        $return = [
                            'success' => $success,
                            'message' => "Successfully generated plots. Successfully generated planting instructions.",
                            'currentProcess' => 'planting instructions'
                        ];
                        
                        return json_encode($return);

                    } catch (Exception $error){

                        $return = [
                            'success' => false,
                            'message' => "Problem in generating planting instructions.<br> <em>Additional Information: $error</em>",
                            'currentProcess' => 'planting instructions'
                        ];

                        return json_encode($return);
                    }
                }
            }else{
                return json_encode(true);
            }
        }
      
    }

    /**
     * Checks the planting arrangement flag
     * @param integer $id Experiment ID
     */
    public function actionCheckPAFlag($id){
        //Check if the experiment notes has a value plantingArrangementChanged
        $experiment = Experiment::searchAll(["fields"=>"experiment.id as experimentDbId|experiment.notes as notes","experimentDbId"=>"equals ".$id,"notes"=> "plantingArrangementChanged"]);
        $experimentNotes = isset($experiment['data'][0]['notes']) ? $experiment['data'][0]['notes'] : null;
        $deleteFlag = !empty($experimentNotes) ? true : false;

        return json_encode($deleteFlag);
    }


    /**
     * Update the site and field column filters of the occurrence browser
     * @param integer $id Experiment ID
     */
    public function actionUpdateOccFilter($id){
        if(isset($_POST)){
            extract($_POST);    

            $field = explode('-',$rowId);
            $variableAbbrev = $field[0];
            $dataType = $field[1];
            $variableDbId = $field[2];
            $dataId = $field[3];
            $select2Data = [];

            $submem = '';
            $datamem = isset($datamem) ? $datamem : '';
            if($datamem == 'SITE'){
                $varModel = $this->mainVariableModel->searchAll(["fields"=>'variable.id AS variableDbId|variable.type as type|variable.abbrev AS abbrev','abbrev'=>"equals FIELD"],'limit=1&sort=variableDbId:desc',false);
                $submem = 'FIELD-'.$varModel['data'][0]['type'].'-'.$varModel['data'][0]['variableDbId'].'-'.$dataId; 
            }else  if($datamem == 'FIELD'){
                $varModel = $this->mainVariableModel->searchAll(["fields"=>'variable.id AS variableDbId|variable.type as type|variable.abbrev AS abbrev','abbrev'=>"equals SITE"],'limit=1&sort=variableDbId:desc',false);
                $submem = 'SITE-'.$varModel['data'][0]['type'].'-'.$varModel['data'][0]['variableDbId'].'-'.$dataId; 
            }
            $occurrenceRecord = $this->occurrenceModel->searchAll([
                'occurrenceDbId' => $dataId.""
            ]);
         
            $requestData = [
                $targetColumn => "$value",
            ];

            if($dataType == 'identification'){
                $baseModel = \Yii::$container->get("\\app\\models\\".$baseModel);

                //Save Value Selected for Site Column
                //Check if selected value is the same with the selected value
                $occurrenceData = $this->occurrenceModel->searchAll(['fields'=>'occurrence.experiment_id AS experimentDbId|occurrence.site_id AS siteDbId|occurrence.field_id AS fieldDbId|
                                                occurrence.id AS occurrenceDbId',"occurrenceDbId"=> "equals $dataId"],'limit=1&sort=occurrenceDbId:desc',false);
                $occSavedSiteId = !empty($occurrenceData['data'][0]['siteDbId']) ? $occurrenceData['data'][0]['siteDbId'] : null;
                $occSavedFieldId = !empty($occurrenceData['data'][0]['fieldDbId']) ? $occurrenceData['data'][0]['fieldDbId'] : null;

                if($datamem == 'SITE'){
                    $isChanged = false;

                    if(!empty($value) && $value != '' && $value != null) {
                        $fieldRecord = $this->geospatialObjectModel->searchAll([
                            'geospatialObjectType' => 'field',
                            'rootGeospatialObjectDbId' => "$value"
                        ]);

                        $fieldDbId = ($fieldRecord['totalCount'] == 1) ? $fieldRecord['data'][0]['geospatialObjectDbId'].'' : 'null';
                        $requestData = [
                            $targetColumn => "$value",
                            'fieldDbId' => $fieldDbId,
                        ];

                        if(!empty($occSavedSiteId) && $occSavedSiteId != null){
                            $checkSavedSite = $this->geospatialObjectModel->searchAll([
                                "fields" => "geospatialObject.id AS geospatialObjectDbId|geospatialObject.geospatial_object_code AS geospatialObjectCode|
                                            geospatialObject.geospatial_object_name AS geospatialObjectName|
                                            geospatialObject.geospatial_object_type AS geospatialObjectType|
                                            root.id AS rootGeospatialObjectDbId",
                                "geospatialObjectDbId" => "equals $occSavedSiteId", "geospatialObjectType" => "equals site|equals country"]);

                            if(isset($checkSavedSite['data'][0]) && $checkSavedSite['data'][0]['geospatialObjectDbId'] != $value && !empty($value)){
                                $requestData['siteDbId'] = "$value";
                                $updatedRecord = $baseModel->updateOne($dataId, $requestData);
                                $isChanged = true;
                            }
                        }else{
                            $requestData['siteDbId'] = "$value";
                            $updatedRecord = $baseModel->updateOne($dataId, $requestData);

                            $isChanged = true;
                        }

                        //For the select2 options if field column
                        $siteInfo = $this->geospatialObjectModel->searchAll([
                            "fields" => "geospatialObject.id AS geospatialObjectDbId|geospatialObject.geospatial_object_code AS geospatialObjectCode|
                                            geospatialObject.geospatial_object_name AS geospatialObjectName|
                                            geospatialObject.geospatial_object_type AS geospatialObjectType|
                                            parent.id AS parentGeospatialObjectDbId",
                            "parentGeospatialObjectDbId" => "equals $value", "geospatialObjectType" => "equals field"
                        ]);

                        foreach($siteInfo['data'] as $siteVal){
                            if(isset($siteVal['geospatialObjectDbId'])){
                                $select2Data[$siteVal['geospatialObjectDbId']] = $siteVal['geospatialObjectName'];
                            }
                        }
                    }

                    return json_encode(['data'=>$select2Data,'isChanged'=>$isChanged,'submem'=>$submem]);
                }
                else if($datamem == 'FIELD'){   // Saving field
                    $isChanged = false;
                    $value = !empty($value) ? "$value" : null;
                    $siteDbId = null;
                    $siteObjName = null;
                    if(isset($value)) { // New field to be saved
                        if(!empty($occSavedFieldId) && $occSavedFieldId != null){   // There is existing saved field
                            $fieldRecCheck = $this->geospatialObjectModel->searchAll([
                                "fields" => "geospatialObject.id AS geospatialObjectDbId|geospatialObject.geospatial_object_code AS geospatialObjectCode|
                                            geospatialObject.geospatial_object_name AS geospatialObjectName|
                                            geospatialObject.geospatial_object_type AS geospatialObjectType|
                                            root.id AS rootGeospatialObjectDbId",
                                "geospatialObjectDbId" => "equals $occSavedFieldId", "geospatialObjectType" => "equals field"
                            ],'limit=1&sort=geospatialObjectDbId:desc',false);

                            if($fieldRecCheck['data'][0]['geospatialObjectDbId'] != $value){
                                $fieldRecord = $this->geospatialObjectModel->searchAll([
                                    "fields" => "geospatialObject.id AS geospatialObjectDbId|root.id AS rootGeospatialObjectDbId|
                                                parent.id AS parentGeospatialObjectDbId|
                                                geospatialObject.geospatial_object_name AS geospatialObjectName",
                                    'geospatialObjectDbId' => "equals $value"
                                ],'limit=1&sort=geospatialObjectDbId:desc',false);

                                $siteDbId = $fieldRecord['data'][0]['parentGeospatialObjectDbId'];

                                if(!empty($siteDbId)){
                                    $siteCheck = $this->geospatialObjectModel->searchAll([
                                        "fields" => "geospatialObject.id AS geospatialObjectDbId|
                                                    geospatialObject.geospatial_object_name AS geospatialObjectName",
                                        'geospatialObjectDbId' => "$siteDbId"
                                    ],'limit=1&sort=geospatialObjectDbId:desc',false);

                                    $siteObjName = $siteCheck['data'][0]['geospatialObjectName'];
                                }
                                $requestData = [
                                    $targetColumn => $value,
                                ];
                                if(isset($siteDbId) && $siteDbId != $occSavedSiteId){
                                    $requestData['siteDbId'] = "$siteDbId";
                                    $requestData['fieldDbId'] = "$value";
                                    $updatedRecord = $baseModel->updateOne($dataId, $requestData);
                                    $isChanged = true;
                                }else{
                                    $requestData['fieldDbId'] = "$value";
                                    $updatedRecord = $baseModel->updateOne($dataId, $requestData);
                                }
                            }
                        } else {    // there is no existing saved field
                            $fieldRecord = $this->geospatialObjectModel->searchAll([
                                "fields" => "geospatialObject.id AS geospatialObjectDbId|root.id AS rootGeospatialObjectDbId|
                                            parent.id AS parentGeospatialObjectDbId|
                                            geospatialObject.geospatial_object_name AS geospatialObjectName|
                                            geospatialObject.geospatial_object_type AS geospatialObjectType",
                                'geospatialObjectDbId' => "equals $value", "geospatialObjectType" => "equals field"
                            ],'limit=1&sort=geospatialObjectDbId:desc',false);

                            $siteDbId = $fieldRecord['data'][0]['parentGeospatialObjectDbId'];

                            if(!empty($siteDbId)){
                                //Get the object name of the parent geospatial id of the field id
                                $siteCheck = $this->geospatialObjectModel->searchAll([
                                    "fields" => "geospatialObject.id AS geospatialObjectDbId|
                                                geospatialObject.geospatial_object_name AS geospatialObjectName|
                                                geospatialObject.geospatial_object_type AS geospatialObjectType",
                                    'geospatialObjectDbId' => "$siteDbId"
                                ],'limit=1&sort=geospatialObjectDbId:desc',false);

                                $geospatialObjectType = $siteCheck['data'][0]['geospatialObjectType'];

                                while($geospatialObjectType != 'site') {    // Keep retrieving the geospatial object until the Site is returned
                                    $siteCheck = $this->geospatialObjectModel->searchAll([
                                        "fields" => "geospatialObject.id AS geospatialObjectDbId|
                                                    parent.id AS parentGeospatialObjectDbId|
                                                    geospatialObject.geospatial_object_name AS geospatialObjectName|
                                                    geospatialObject.geospatial_object_type AS geospatialObjectType",
                                        'geospatialObjectDbId' => "$siteDbId"
                                    ],'limit=1&sort=geospatialObjectDbId:desc',false);

                                    $siteDbId = $siteCheck['data'][0]['parentGeospatialObjectDbId'];
                                    $geospatialObjectType = $siteCheck['data'][0]['geospatialObjectType'];

                                    if(is_null($siteDbId)) break;
                                }

                                $siteObjName = $siteCheck['data'][0]['geospatialObjectName'];
                            }

                            $requestData = [
                                $targetColumn => $value,
                            ];
                            if(isset($siteDbId) && $siteDbId != $occSavedSiteId){
                                $requestData['siteDbId'] = "$siteDbId";
                                $requestData['fieldDbId'] = "$value";
                                $updatedRecord = $baseModel->updateOne($dataId, $requestData);
                                $isChanged = true;
                            }else{
                                $requestData['fieldDbId'] = "$value";
                                $updatedRecord = $baseModel->updateOne($dataId, $requestData);
                            }
                        }
                    } else {    // field will be cleared
                        $requestData['fieldDbId'] = "null";
                        $updatedRecord = $baseModel->updateOne($dataId, $requestData);
                    }

                    return json_encode(['isChanged'=>$isChanged,'submem'=>$submem,'newVal' => $siteDbId,'newText'=>$siteObjName]);
                } else{
                    $requestData['records'] = [
                        "$targetColumn" => "$value"
                    ];
                    $insertedRecord = $baseModel->updateOne($dataId, $requestData);
                    return json_encode(true);
                }
            }
            else{// fetch records in occurrence data
                if($updateMode != 'bulk-update'){
                    $baseModel = \Yii::$container->get("\\app\\models\\$baseModel".'Data');
                    
                    $occurrenceDataRecord = $baseModel->searchAll([
                        'occurrenceDbId' => "$dataId",
                        'variableDbId' => "$variableDbId"
                    ],'limit=1&sort=occurrenceDbId:desc',false);
                    
                    if($occurrenceDataRecord['totalCount'] > 0){//record exists
                        $requestData = [
                            "dataValue" => $value,
                        ];
                        $occurrenceDataDbId = $occurrenceDataRecord['data'][0]['occurrenceDataDbId'];
                        $updatedRecord = $baseModel->updateOne($occurrenceDataDbId, $requestData);
                    }
                    else{//insert new record
                        $requestData['records'][] = [
                            "occurrenceDbId" => "$dataId",
                            "variableDbId" => "$variableDbId",
                            "dataValue" => "$value",
                        ];
                        $insertedRecord = $baseModel->create($requestData);
                    }
                }
                return json_encode(true);
            }
        }
    }

    /**
     * Retrieve entries count for experiment
     * @param integer $id Experiment ID
     */
    public function actionGetEntriesCount($id){

        if(isset($_POST) && isset($_POST['experimentId'])){
            $experimentId = $_POST['experimentId'];
            $params = "?limit=1&sort=entryDbId:desc";

            $entriesData = $this->entryModel->getEntries($experimentId,[],null,$params);

            $entriesCount = $entriesData && $entriesData['totalCount'] ? $entriesData['totalCount'] : 0;
            
            return json_encode([ 'entryCount'=>$entriesCount ]);
        }
        else{
            return json_encode([ 'entryCount'=>0 ]);
        }
    }

    /**
     * Reorder all entries according to current sorting
     * @param integer $id Experiment ID
     */
    public function actionReorderAllBySort($id){
        if(isset($_POST['sort'])){
            // process current sorting
            $paramSort = '?sort=';

            $sortParams = $_POST['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();
            if(!empty($sortParams) && !empty($sortParams[0])){
                foreach ($sortParams as $column) {
                    if (substr($column,0,1) == '-') {
                        $sortOrder[substr($column, 1)] = SORT_DESC;
                        $sortAttribute = substr($column, 1) == 'parentRole' ? 'entryRole' : substr($column, 1);
    
                        $paramSort = $paramSort . $sortAttribute . ':desc';
                    } else {
                        $sortOrder[$column] = SORT_ASC;
                        $sortAttribute = $column == 'parentRole' ? 'entryRole' : $column;
    
                        $paramSort = $paramSort . $sortAttribute;
                    }
                    if ($currParam < $countParam) {
                        $paramSort = $paramSort . '|';
                    }
                    $currParam += 1;
                }
            }
            else{
                $paramSort .= 'entryNumber:asc';
            }
        }      
        
        // retrieve entries with sorting
        $entriesData = $this->entryModel->getEntries($id,[],null,$paramSort);
        
        $pageCount = ($entriesData['totalCount'] % 100) > 0 ? (($entriesData['totalCount'] / 100)+1) : ($entriesData['totalCount'] / 100);
        $newData = null;

        if(count($entriesData['entries']) < $entriesData['totalCount']){
            for($currPage = 1;$currPage<=$pageCount;$currPage += 1){
                $newData = $this->entryModel->getEntries($id,[],null,$paramSort.'&page='.$currPage+1);
                $entriesData['entries'] = array_merge($entriesData['entries'],$newData['entries']);
            }
        }
        
        if(!empty($entriesData['entries'])){
            $this->reorderAllByEntryNumberSwap($entriesData['entries']);

            //Get experiment record
            $experiment = $this->mainExperiment->getExperiment($id);
            $experimentDesignType = isset($experiment['experimentDesignType']) && !empty($experiment['experimentDesignType']) ? $experiment['experimentDesignType'] : '';
            $status = isset($experiment['experimentStatus']) && !empty($experiment['experimentStatus']) ? explode(';',$experiment['experimentStatus']) : [];

            //check status
            $entryStatus = "entry list created";
            if(in_array("entry list created", $status)){
                $entryStatus = "entry list created";
            } else if(in_array("entry list incomplete seed sources", $status)){
                $entryStatus = "entry list incomplete seed sources";
            }
            $experimentStatus = $entryStatus;

            //update experiment status
            $this->mainExperiment->updateOne($id, ["experimentStatus"=>$experimentStatus]);
            //check if design is already generated
            if(in_array('design generated', $status)){
                //check if nursery
                if(in_array($experimentDesignType, ['Entry Order', 'Systematic Arrangement'])){
                    $this->mainExperiment->updateOne($id, ["experimentDesignType"=>"Systematic Arrangement"]);
                    //delete blocks
                    $this->entryOrderModel->deleteBlocksUpdate($id);
                    $this->mainExperiment->updateOne($id, ['notes' => 'plantingArrangementChanged']);
                }
            }

            // function checks if experiment is ICN, if so it will retrieve the existing data
            // clean-up records if ICN
            $generatedRecords = $this->actionRetrieveGeneratedRecords($id);
            if (!empty($generatedRecords)) {
                $this->actionReorderDeleteRecord($id);
            }
        }
        return json_encode(true);
    }

    /**
     * Reorder all entries by swapping entry numbers
     * @param Array $entryRecords entry records
     */
    public function reorderAllByEntryNumberSwap($entryRecords){
        $updatedEntries = [];

        $entryDbIds = array_column($entryRecords, 'entryDbId');
        $entryNumbers = array_column($entryRecords, 'entryNumber'); 
        $oldEntries = array_combine($entryNumbers, $entryDbIds); 
        $maxEntryNumber = count($entryRecords) + 1;

        $failedUpdates = [];
        $update  = null;
        $entryCount = 1;
        foreach($entryRecords as $entry){
            //change entryNumber
            if(isset($oldEntries[$entryCount]) && !in_array($oldEntries[$entryCount], $updatedEntries)){
                
                // check failed updates
                $update = $this->entryModel->updateOne($oldEntries[$entryCount], ['entryNumber'=>"$maxEntryNumber",'entryCode'=>"$maxEntryNumber"]);
                
                if(!$update['success']){
                    $failedUpdates[$oldEntries[$entryCount]] = ['entryDbId'=>$oldEntries[$entryCount],'entryNumber'=>$entryCount,'targetEntryNumber'=>$maxEntryNumber];
                }
                $maxEntryNumber++;
            }

            // update to targetEntryNumber
            $update = $this->entryModel->updateOne($entry['entryDbId'], ['entryNumber'=>"".$entryCount,'entryCode'=>"".$entryCount]);
            if($update['success']){
                $updatedEntries[] = $entry['entryDbId'];
            }
            else{
                $failedUpdates[$entry['entryDbId']] = ['entryDbId'=>$entry['entryDbId'],'entryNumber'=>$entry['entryNumber'],'targetEntryNumber'=>$entryCount];
            }

            $entryCount += 1;
        }

        if(!empty($failedUpdates)){
            foreach($failedUpdates as $failedUpdate){
                if(!($failedUpdate['targetEntryNumber'] > ($maxEntryNumber-1))){
                    $update = $this->entryModel->updateOne($failedUpdate['entryDbId'], ['entryNumber'=>"".$failedUpdate['targetEntryNumber'],'entryCode'=>"".$failedUpdate['targetEntryNumber']]);
                
                    if($update['success']){
                        $updatedEntries[] = $failedUpdate['entryDbId'];
                    }
                }
            }
        }

        return true;
    }

    /**
     * Reorder all entries in background process
     * @param Integer $experimentId experiment id
     */
    public function actionReorderAllBgProcess($experimentId){
        $model = $this->findModel($experimentId);

        if(isset($_POST['sort'])){
            // process current sorting
            $paramSort = '?sort=';

            $sortParams = $_POST['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();
            if(!empty($sortParams) && !empty($sortParams[0])){
                foreach ($sortParams as $column) {
                    if (substr($column,0,1) == '-') {
                        $sortOrder[substr($column, 1)] = SORT_DESC;
                        $sortAttribute = substr($column, 1) == 'parentRole' ? 'entryRole' : substr($column, 1);
    
                        $paramSort = $paramSort . $sortAttribute . ':desc';
                    } else {
                        $sortOrder[$column] = SORT_ASC;
                        $sortAttribute = $column == 'parentRole' ? 'entryRole' : $column;
    
                        $paramSort = $paramSort . $sortAttribute;
                    }
                    if ($currParam < $countParam) {
                        $paramSort = $paramSort . '|';
                    }
                    $currParam += 1;
                }
            }
            else{
                $paramSort .= 'entryNumber:asc';
            }
        }

        if(isset($experimentId) && !empty($paramSort)){
            $bgProcess = $this->mainExperimentModel->reorderAllEntriesBgProcess($experimentId,$paramSort);
            // function checks if experiment is ICN, if so it will retrieve the existing data
            // clean-up records if ICN
            $generatedRecords = $this->actionRetrieveGeneratedRecords($experimentId);
            if (!empty($generatedRecords)) {
                $this->actionReorderDeleteRecord($experimentId);
            }
        }

        $this->redirect(['create/index?program='.$model['programCode'].'&id='.$experimentId]);
    }

    /**
     * Reorder all entries in background process
     * @param Integer $experimentId experiment id
     */
    public function actionSaveReorderByInsert($experimentId){
        $model = $this->findModel($experimentId);

        if(isset($_POST['entryInfo'])){
            
            $entryInfo = $_POST['entryInfo'];
            $entryDbIds = array_column($entryInfo, 'entryDbId');
            $targetEntryNumber = array_column($entryInfo, 'targetEntryNumber');
            $newSortingArray = array_combine($targetEntryNumber, $entryDbIds);

            $newEntryInfo = [
                'newSortingArray' => $newSortingArray,
                'newSortIds' => $entryDbIds
            ];
            if(isset($experimentId) && !empty($entryInfo)){
                $bgProcessId = $this->mainExperimentModel->reorderAllEntriesBgProcess($experimentId,null,$newEntryInfo);

                // function checks if experiment is ICN, if so it will retrieve the existing data
                // clean-up records if ICN
                $generatedRecords = $this->actionRetrieveGeneratedRecords($experimentId);
                if (!empty($generatedRecords)) {
                    $this->actionReorderDeleteRecord($experimentId);
                }

                $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($bgProcessId);
                
                if($bgStatus !== 'done' && !empty($bgStatus)){
                    $this->redirect(['create/index?program='.$model['programCode'].'&id='.$experimentId]);
                } else {
                    //update status
                    // update experiment status
                    $experiment = $this->mainExperiment->getExperiment($experimentId);
                    $experimentStatus = $experiment['experimentStatus'];

                    if(!empty($experimentStatus)){
                        $status = explode(';',$experimentStatus);
                        $status = array_filter($status);
                    }else{
                        $status = [];
                    }
                    
                    if(in_array('updating of entries in progress', $status)){
                        $index = array_search('updating of entries in progress', $status);
                        unset($status[$index]);
                    }
                    $updatedStatus = implode(';', $status);
                    $update = $this->mainExperiment->updateOne($experimentId, ['experimentStatus'=>$updatedStatus]);

                    $params = [
                        "jobIsSeen" => "true",
                        "jobRemarks" => "REFLECTED"
                    ];
                    $updateBgProcess = $this->backgroundJob->updateOne($bgProcessId, $params); 

                    //Get experiment record
                    $experimentDesignType = isset($experiment['experimentDesignType']) && !empty($experiment['experimentDesignType']) ? $experiment['experimentDesignType'] : '';
                    $status = isset($experiment['experimentStatus']) && !empty($experiment['experimentStatus']) ? explode(';',$experiment['experimentStatus']) : [];

                    //check status
                    $entryStatus = "entry list created";
                    if(in_array("entry list created", $status)){
                        $entryStatus = "entry list created";
                    } else if(in_array("entry list incomplete seed sources", $status)){
                        $entryStatus = "entry list incomplete seed sources";
                    }
                    $experimentStatus = $entryStatus;

                    //update experiment status
                    $this->mainExperiment->updateOne($experimentId, ["experimentStatus"=>$experimentStatus]);
                    //check if design is already generated
                    if(in_array('design generated', $status)){
                        //check if nursery
                        if(in_array($experimentDesignType, ['Entry Order', 'Systematic Arrangement'])){
                            $this->mainExperiment->updateOne($experimentId, ["experimentDesignType"=>"Systematic Arrangement"]);
                            //delete blocks
                            $this->entryOrderModel->deleteBlocksUpdate($experimentId);
                            $this->mainExperiment->updateOne($experimentId, ['notes' => 'plantingArrangementChanged']);
                        }
                    }
                    
                    return json_encode(true);
                }
            }
            
        }   
    }

    /**
     * Checker for generated experiment-related records 
     * @param Integer $experimentId experiment id
     * Notes: Used in re-ordering entry lists
     */
    public function actionRetrieveGeneratedRecords($experimentId) {
        // Initial quick check just to see if there are any records
        $dataArray = [];

        // Check if experiment is ICN
        $experiment = $this->mainExperiment->getExperiment($experimentId);
        $experimentType = $experiment['experimentType'] ?? '';

        if ($experimentType != 'Intentional Crossing Nursery') {
            return $dataArray;
        }
        
        // Retrieve crosses
        $crossRecords = $this->cross->searchAll(
            [
                'fields'=>'germplasmCross.id as crossDbId|experiment.id as experimentDbId',
                'experimentDbId' => 'equals '.$experimentId
            ], 'limit=1', true) ?? [];
        $crossCount = $crossRecords['totalCount'] ?? 0;
        if ($crossCount > 0) {
            array_push($dataArray, 'Crosses');
        }

        // Retrieve occurrences
        $occurrences = $this->occurrenceModel->searchAll(
            [
                "fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",
                'experimentDbId' => "equals $experimentId"
            ], 'limit=1', true) ?? [];
        $occurrenceCount = $occurrences['totalCount'] ?? 0;
        if ($occurrenceCount > 0) {
            array_push($dataArray, 'Occurrences');
        }

        // Extract first occurrence ID for reference
        $firstOccurrenceId = isset($occurrences['data'][0]['occurrenceDbId']) ? 
            $occurrences['data'][0]['occurrenceDbId'] : 0;

        // Retrieve plot records
        $plotRecords = ($firstOccurrenceId != 0) ? 
            $this->occurrenceModel->searchAllPlots(
                $firstOccurrenceId, null, 
                'limit=1', false) : [];
        $plotCount = $plotRecords['totalCount'] ?? 0;
        if ($plotCount > 0) {
            array_push($dataArray, 'Plots');
        }
    

        // Retrieve planting instruction records
        $plantingInsRecord = ($firstOccurrenceId != 0) ? $this->plantingInstruction->searchAll(
            [
                "fields"=>"plantingInstruction.id AS plantingInstructionDbId|plot.occurrence_id as occurrenceDbId",
                'occurrenceDbId' => "equals $firstOccurrenceId"
            ], 'limit=1', false) : [];
        $plantInsCount = $plantingInsRecord['totalCount'] ?? 0;
        if ($plantInsCount > 0) {
            array_push($dataArray, 'Planting Instructions');
        }
        return $dataArray;
    }

    /**
     * Deletion of experiment-generated records by background worker
     * @param Integer $experimentId experiment id
     * Notes: Used in re-ordering entry lists
     */
    public function actionReorderDeleteRecord($experimentDbId) {
        $userId = Yii::$app->session->get('user.id');

        // Create background processs
        $backgroundJobParams = [
            "workerName" => "EcGeneratedRecordsProcessor",
            "description" => "Delete experiment-generated records of ".$experimentDbId,
            "entity" => "EXPERIMENT",
            "entityDbId" => $experimentDbId,
            "endpointEntity" => "EXPERIMENT",
            "application" => "EXPERIMENT_CREATION",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId
        ];

        try {
            $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
        }

        // if creating transaction is successful
        try {
            if (isset($transaction['status']) && $transaction['status'] == 200) {

                $message = "The records are being deleted in the background. You may proceed with other tasks. You will be notified in this page once done.";
                // set background processing info to session
                Yii::$app->session->set('em-bg-processs-message', $message);
                Yii::$app->session->set('em-bg-processs', true);

                $host = getenv('CB_RABBITMQ_HOST');
                $port = getenv('CB_RABBITMQ_PORT');
                $user = getenv('CB_RABBITMQ_USER');
                $password = getenv('CB_RABBITMQ_PASSWORD');

                $connection = new AMQPStreamConnection($host, $port, $user, $password);

                $channel = $connection->channel();

                $channel->queue_declare(
                    'EcGeneratedRecordsProcessor', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                    false,  //passive - can use this to check whether an exchange exists without modifying the server state
                    true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                    false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                    false   //auto delete - queue is deleted when last consumer unsubscribes
                );

                $msg = new AMQPMessage(
                    json_encode([
                        'tokenObject' => [
                            'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                            'refreshToken' => ''
                        ],
                        'backgroundJobId' => $transaction["data"][0]["backgroundJobDbId"],
                        'description' => 'Deletion of experiment-generated records',
                        'experimentDbId' => $experimentDbId,
                        'personDbId' => $userId,
                        'processName' => 'delete-experiment-generated-data'
                    ]),
                    array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                );

                $channel->basic_publish(
                    $msg,   //message
                    '', //exchange
                    'EcGeneratedRecordsProcessor'  //routing key (queue)
                );
                $channel->close();
                $connection->close();
            } else {
                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
            }
        } catch (\Exception $e) {
            $notif = 'error';
            $icon = 'times';
            Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $e);
        }
    }
}