<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Experiment;
use app\models\Occurrence;
use app\models\PlatformList;
use app\models\PlatformListMember;
use app\models\User;

use Yii;
use yii\helpers\ArrayHelper;
use app\dataproviders\ArrayDataProvider;
use yii\web\Controller;

/**
 * Controller for entry order arrangement
 */
class OccurrenceController extends Controller
{
    public function __construct($id, $module,
        public Experiment $experiment,
        public Occurrence $occurrence, 
        public PlatformList $platformList,
        public PlatformListMember $platformListMember,
        public User $user,
        $config = [])
    {
        
        parent::__construct($id, $module, $config);
    }



    /**
     * Renders view for site list
     * 
     * @param integer $id record id of the experiment
     * @param string $program name of the current program
     * @param integer $occurrenceCount no. of occurrences within the experiment
     */
    public function actionRenderSiteList($id, $program, $occurrenceCount){

        $param = ["type"=>"equals sites","memberCount"=>"not equals 0"];
        $listData = $this->platformList->searchList($param, true);

        $dataProvider = new ArrayDataProvider([
            'key'=>'listDbId',
            'allModels' => $listData,
            'sort' => [
                'attributes' => ['listDbId', 'abbrev', 'name', 'type'],
                'defaultOrder' => [
                    'listDbId'=>SORT_DESC
                ]
            ],
            'pagination' => false,
        ]);
        $htmlData = $this->renderPartial('/create/occurrences/site_list',[
            'data' => $dataProvider,
            'experimentId' => $id,
            'occurrenceCount' => $occurrenceCount,
            'program' => $program
        ],true,false);

        return json_encode([
            'htmlData' => $htmlData
        ]);
    }

    /**
     * Populate site values of the occurrences
     * 
     * @param integer $id record id of the experiment
     * 
     */
    public function actionPopulateSites($id){
        if(isset($_POST)){
            $savedListId = $_POST['listDbId'];

            //get platformlist members
            $list = $this->platformListMember->getListMembers($savedListId, true);
            
            $listMembers = $list[0]['members'];

            //get occurrence records
            $occurrencesRecords = $this->occurrence->searchAll([
                'fields'=>'occurrence.experiment_id as experimentDbId|occurrence.occurrence_name as occurrenceName|occurrence.id AS occurrenceDbId',
                'experimentDbId' => "equals $id"
            ],'sort=occurrenceName:ASC');
                    

            $occurrenceDbIds = isset($occurrencesRecords['data'][0]['occurrenceDbId']) ? array_column($occurrencesRecords['data'], 'occurrenceDbId') : [];
            $occIdx = 0;

            foreach($listMembers as $member){
                $memberValue = $member['geospatialObjectDbId'];
                $this->occurrence->updateOne($occurrenceDbIds[$occIdx++], ["siteDbId"=>"$memberValue"]);

            }
            return json_encode('success');
        }
    }
}