<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use app\models\Cross;
use app\models\CrossSearch;
use app\models\Experiment;
use app\models\ExperimentBlock;
use app\models\Variable;
use app\models\Item;
use app\models\Config;
use app\models\Occurrence;
use app\models\PlanTemplate;
use app\models\ScaleValue;
use app\models\User;

use Yii;

use yii\web\Controller;
use app\modules\experimentCreation\models\FormModel;
use app\components\GenericFormWidget;

/**
 * Default controller for the `experimentCreation` module
 */
class DefaultController extends Controller
{
    protected $cross;
    protected $crossSearch;
    protected $experiment;
    protected $experimentBlock;
    protected $variable;
    protected $item;
    protected $configModel;
    protected $scaleValue;
    protected $formModel;
    protected $genericFormWidget;
    protected $occurrence;
    protected $planTemplate;

    public function __construct($id, $module,
        Cross $cross,
        CrossSearch $crossSearch,
        Experiment $experiment, 
        ExperimentBlock $experimentBlock, 
        Variable $variable, 
        Item $item, 
        Config $configModel,
        ScaleValue $scaleValue,
        FormModel $formModel, 
        GenericFormWidget $genericFormWidget,
        Occurrence $occurrence,
        PlanTemplate $planTemplate,
        $config = [])
    {
        $this->cross = $cross;
        $this->crossSearch = $crossSearch;
        $this->experiment = $experiment;
        $this->experimentBlock = $experimentBlock;
        $this->variable = $variable;
        $this->item = $item;
        $this->configModel = $configModel;
        $this->scaleValue = $scaleValue;
        $this->formModel = $formModel;
        $this->genericFormWidget = $genericFormWidget;
        $this->occurrence = $occurrence;
        $this->planTemplate = $planTemplate;
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($program)
    {
        $activities = $this->formModel->prepare(Yii::$app->controller->action->id,$program);
        $activityAbbrev = '';

        if(Yii::$app->access->renderAccess("EXPERIMENT_CREATION_CREATE","EXPERIMENT_CREATION", usage:'url')){
            if(!empty($activities)){
                $index = array_search($activities['active'], array_column($activities['children'],'action_id'));
                $activityAbbrev = $activities['children'][$index]['abbrev'].'_VAL';
            }
            $requiredFields = [];

            //Get All required variables for basic info
            $requiredConfig = $this->configModel->getConfigByAbbrev($activityAbbrev);
            
            $genericForm = $this->genericFormWidget;
            $configValues = [];

            if(!empty($requiredConfig)){
                $configValues = $requiredConfig['Values'];

            }
            $genericForm->options = [
                'config' => $configValues,
            ];
            $requiredFields = $genericForm->generateFields()['requiredFields'];

            if(count($requiredFields) == 0){
                Yii::$app->session->setFlash('error','<i class="fa-fw fa fa-times"></i> Invalid experiment type');
                return $this->redirect(['create/index?program='.$program]);
            }
            return $this->render('default_basic',[
                'model' => $this->experiment,
                'program' => $program,
                'requiredFields' => $requiredFields
            ]);
        }
    }
    
    /**
    * Renders view more information of a experiment in widget
    * @return mixed experiment information
    */
    public function actionViewInfo(){
        $id = isset($_POST['id']) ? $_POST['id'] : null;

        $crossSearchModel = $this->crossSearch;
        $data = $this->experiment->getExperimentInfo($id,$crossSearchModel);
        // get crosses
        $model = $this->findModel($id);
        $crossesAct = false;
        $experimentType = "";
        if(!empty($model)){
            $dataProcessAbbrev = $model['dataProcessAbbrev'];
            $experimentType = $model['experimentType'];
            $parentActAbbrev = str_replace('_DATA_PROCESS', '', $dataProcessAbbrev);
            $crossesActAbbrev = $parentActAbbrev.'_CROSSES_ACT';
            $crossesActArr = $this->item->getAll("abbrev=$crossesActAbbrev");
            $crossesAct = isset($crossesActArr['data'][0]) ? true : false;

        }
         
        
        return Yii::$app->controller->renderAjax('@app/widgets/views/experiment/tabs.php',[
            'data' => $data,
            'id' => $id,
            'crossesAct' => $crossesAct,
            'experimentType' => $experimentType
        ]);
    }


    /**
     * Redirect to Basic Info after selecting an Experiment Type
     * @param String $type Experiment Type
     */
    public function actionGetExptTypeProcessPath($type){
        
        $variable = $this->variable->searchAll(["abbrev"=>"equals EXPERIMENT_TYPE"]);
        $processId = '';
        if(!empty($variable['data'])){
            $scaleValueTemp = $this->scaleValue->getVariableScaleValues($variable['data'][0]['variableDbId'], ["value"=>"equals $type"]);

            $scaleValueAbbrev = isset($scaleValueTemp[0]['abbrev']) ? $scaleValueTemp[0]['abbrev'] : '';
            $scaleValueAbbrevArr = explode('EXPERIMENT_TYPE_',$scaleValueAbbrev);
            
            if(strpos($scaleValueAbbrevArr[1],'CROSS_PARENT_NURSERY') !== false){
                $scaleVal = $scaleValueAbbrevArr[1].'_PHASE_I'; 
            }else{
                $scaleVal = $scaleValueAbbrevArr[1];
            }
            $scaleVal = $scaleVal.'_DATA_PROCESS';

            $item = $this->item->searchAll(["abbrev"=>"equals $scaleVal"]);
            $processId = isset($item['data'][0]['itemDbId']) ? $item['data'][0]['itemDbId'] : '';
        }

        return json_encode(["processId"=>$processId]);
    }
    
    /**
     * Store dynamic sessions
     */
    public function actionStoreSelectedSession(){
        $count = 0;
        $items = [];

        if(isset($_POST['unset'], $_POST['storeAll'], $_POST['id'])){
            extract($_POST);

            if(isset($_POST['occurrenceDbId'])){
                $sessionName = 'occurrences';
            }

            $userModel = new User();
            $userId = $userModel->getUserId();
            $selectedIdName = "occ_$userId"."_selected_$sessionName"."_$id";        

            if(!empty($_SESSION[$selectedIdName])){
                $items = ($_SESSION[$selectedIdName] !== null) ? $_SESSION[$selectedIdName] : [];
            }

            if($unset == "true"){
                if($storeAll == "true"){
                    $items = [];
                }
                else{
                    foreach($items as $key => $value){
                        $index = (isset($tab) && $tab == 'crosses') ? $key : $value;
                        if($index == $occurrenceDbId){
                            unset($items[$key]);
                        }
                    }
                }
            }
            else{
                if($storeAll == "true"){
                    $occurrenceRecords =  $this->occurrence->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "equals $id"]);
                    $occurrences = $occurrenceRecords['data'];
                    foreach($occurrences as $value){
                        if(!in_array($value['occurrenceDbId'], $items)){     
                            $items[] = $value['occurrenceDbId'];
                        }
                    }
                }
                else if(!in_array($occurrenceDbId, $items)){     
                    $items[] = $occurrenceDbId;
                }
            }

            $_SESSION[$selectedIdName] = $items;

            $count = count($items);
        }

        return json_encode($count);
    }

    /**
     * Retrieve dynamic sessions
     */
    public function actionGetSelectedSession(){
        extract($_POST);

        $userModel = new User();
        $userId = $userModel->getUserId();
        $selectedItems = [];

        if(isset($_POST['action']) && ($_POST['action'] == 'occurrences') || $_POST['action'] == 'specify-occurrences'){
            $selectedIdName = "occ_$userId"."_selected_occurrences_$experimentDbId";          
        }
        $selectedItems = (Yii::$app->session->get($selectedIdName) !== null) ? Yii::$app->session->get($selectedIdName) : [];

        $response = [
            'selectedItems' => $selectedItems,
            'selectedItemsCount' => count($selectedItems)
        ];

        return json_encode($response);
    }
    
    /**
     * Checks if there are cross method entries that are null proceeding to the next step
     * @param Integer $id Experiment ID
     */
    public function actionCrossStepValidation($id){
        $nextFlag = false;
        $crossesRec = $this->cross->searchAll(['experimentDbId'=> "equals $id","crossMethod"=>"null"]);
        if($crossesRec['data'] != null){
            $nextFlag = false;
        }else{
            $nextFlag = true;
        }

        return json_encode($nextFlag);

    }

    /**
     * Destroy Session
     * @param Integer $id Experiment ID
     */
    public function actionDestroySession($id){
        //destroy selected items session
        $userModel = new User();
        $userId = $userModel->getUserId();
        $siteSessionName = "occ_$userId"."_selected_occurrences_$id";

        if(!isset($_POST['pjax']) || !isset($_POST['_pjax'])){
            unset($_SESSION[$siteSessionName]);   
        }

        return json_encode(true);
    }


    /**
     * Finds the Experiment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param Integer $id
     * @return Experiment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = $this->experiment->getExperiment($id)) !== null) {
            return $model;
        } else {
            return [];
        }
    }
        

    /**
     * Get experiment steps to copy
     * @param String  $program name of the current program
     */
    public function actionGetExperimentSteps($program){
        $flowArr = [];
        if(isset($_POST['id'])){
            //Id of the experiment to copy
            $id = $_POST['id'];

            //Get the data process id of the experiment
            $model = $this->findModel($id);

            $dataProcessDbId = !empty($model) ? $model['dataProcessDbId'] : "";

            $dataResponse = $this->formModel->getActivities($dataProcessDbId);
            $result = !empty($dataResponse) ? $dataResponse['children'] : [];

            $default = false;

            if(strpos($model['dataProcessAbbrev'], '_TRIAL_DATA_PROCESS') !== false){
                //check if there's plan template for experiment
                $param = ['entityType'=>'equals experiment|equals parameter_sets', 'entityDbId'=> "equals $id"];
                $designInfo = $this->planTemplate->getPlanTemplateByEntity($param);

            } else {
                //check if there's experiment block
                $designInfo = $this->experimentBlock->searchAll(["experimentDbId" => "equals $id"]);
                $designInfo = isset($designInfo['data'][0]) ? $designInfo['data'][0] : null;
            }

            $design = "false";
            if($designInfo != null){
                $design = "true";
            } 
            
            foreach($result as $val){
                if($val['display_name'] != 'Review' && $val['display_name'] != 'Experiment Groups'){
                    if($val['display_name'] == 'Basic'){
                        $default = true;
                    }else{
                        $default = false;
                    }

                    if($val['display_name'] != 'Protocol' && $val['display_name'] == 'Basic'){
                        $flowArr[] = [
                            'actionId' => $val['action_id'],
                            'hasDependency' => false,
                            'default' => $default,
                            'displayName' => $val['display_name'],
                            'dataMem' => strtolower(str_replace(' ','_',$val['display_name']))
                        ];
                    } else{
                        $flowArr[] = [
                            'actionId' => $val['action_id'],
                            'hasDependency' => true,
                            'default' => $default,
                            'displayName' => $val['display_name'],
                            'dataMem' => strtolower(str_replace(' ','_',$val['display_name']))
                        ];
                    }
                   
                }
               
            }
            $htmlData = $this->renderAjax('copy_experiment_modal.php',[
                'data' => $flowArr,
                'id' => $id,
                'program' => $program,
                'design' => $design
            ]);

            return json_encode($htmlData);
        }
        
    }

    /**
     * Get the currently selected cross parents in session.
     * @param integer $id Experiment ID
     * @return array
    */
    public function actionGetUserSelection($id){
        $userModel = new User();
        $userId = $userModel->getUserId();
        if(isset($_POST)){
            $sessionName = $_POST['sessionName'];
            $sessionCount = isset($_SESSION["ec_$userId"."_selected_".$sessionName."_".$id]) ? count($_SESSION["ec_$userId"."_selected_".$sessionName."_".$id]) : 0;
            
            return json_encode($sessionCount);
        }else{
            return json_encode(false);
        }
    }

    /**
     * Check if the saved experiment name is changed
     * @param integer $id Experiment ID
     */
    public function actionCheckExperimentName($id){
        if(isset($_POST)){
            
            $inputStage = json_decode($_POST['stage'], true);
            $inputYear = json_decode($_POST['year'], true);
            $inputSeason = json_decode($_POST['season'],true);
            $inputExperimentName = $_POST['inputExperimentName'];

            $experiment =  $this->experiment->getExperiment($id);
            $updateFlag = false;

            $nameConfig = '';
            
            if((!empty($experiment['stageDbId'] ) && $experiment['stageDbId'] != $inputStage) || (!empty($experiment['experimentYear']) && $experiment['experimentYear'] != $inputYear) || (!empty($experiment['seasonDbId']) && $experiment['seasonDbId'] != $inputSeason)){
                $updateFlag = true;

                if($experiment['stageDbId'] != $inputStage){
                    $nameConfig = empty($nameConfig) ? 'Stage' : $nameConfig.' , Stage';
                }

                if($experiment['experimentYear'] != $inputYear){
                    $nameConfig = empty($nameConfig) ? 'Experiment Year' : $nameConfig.' , Experiment Year';
                }

                if($experiment['seasonDbId'] != $inputSeason){
                    $nameConfig = empty($nameConfig) ? 'Evaluation Season' : $nameConfig.' , Evaluation Season';
                }
            }

            //check if experiment name has duplicate
            $duplicateFlag = false;
            $checkFlag = false;
            if(!empty($inputExperimentName) && (!empty($experiment['experimentName']))){ //existing experiment, user changed the experiment name
                if($experiment['experimentName'] != $inputExperimentName){
                    $checkFlag = true;
                }
            } elseif(!empty($inputExperimentName)){ //new experiment with user's input
                $checkFlag = true;
            }
            if($checkFlag){
                $experiments =  $this->experiment->searchAll(['fields'=>'experiment.experiment_name as experimentName', 'experimentName'=>"equals $inputExperimentName"], 'limit=1', false);
                if($experiments['totalCount'] > 0){
                    $duplicateFlag = true;
                }
            }
            
            return json_encode(['updateFlag'=>$updateFlag,'message'=>$nameConfig, 'duplicateFlag'=>$duplicateFlag]);

        }
    }

    /**
     * Update experiment name
     * @param integer $id Experiment ID
     */
    public function actionUpdateExperimentName($id){
        if(isset($_POST)){
            $inputStage = $_POST['stage'];
            $inputYear = $_POST['year'];
            $inputSeason = $_POST['season'];

            //split value to get id
            $stageValArray = explode('-', $inputStage);
            if(count($stageValArray) > 1){
                $inputStage = $stageValArray[1];
            }
            

            $param = ['generateExperimentName' => 'true','stageDbId' => "$inputStage", 'experimentYear'=>"$inputYear",'seasonDbId'=>"$inputSeason"];
            $method = 'PUT';
            $path = 'experiments/'.$id;
            
            $response = Yii::$app->api->getResponse($method, $path, json_encode($param));
           
            $experiment = $this->experiment->getExperiment($id);
               
            return json_encode($experiment['experimentName']);
        }
    }

}
