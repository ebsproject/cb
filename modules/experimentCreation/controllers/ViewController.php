<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use Yii;
use app\dataproviders\ArrayDataProvider;
use yii\web\Controller;


use app\models\Entry;
use app\models\Experiment;
use app\models\ExperimentBlock;
use app\models\Occurrence;
use app\models\UserDashboardConfig;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\PackageModel;

/**
 * Controller for viewing of records
 */
class ViewController extends Controller
{
    protected $entry;
    protected $experiment;
    protected $experimentBlock;
    protected $experimentModel;
    protected $experimentSearch;
    protected $occurrence;
    protected $packageModel;
    protected $userDashboardConfig;

    public function __construct($id, $module,
        Entry $entry,  
        Experiment $experiment,
        ExperimentBlock $experimentBlock,
        ExperimentModel $experimentModel,
        Occurrence $occurrence,
        PackageModel $packageModel,
        UserDashboardConfig $userDashboardConfig,
        $config = [])
    {
        $this->entry = $entry;
        $this->experiment = $experiment;
        $this->experimentBlock = $experimentBlock;
        $this->experimentModel = $experimentModel;
        $this->occurrence = $occurrence;
        $this->packageModel = $packageModel;
        $this->userDashboardConfig = $userDashboardConfig;
        parent::__construct($id, $module, $config);
    }

    /**
     * View copy experiment records
     * 
     * @param Integer $id record id of the experiment
     * @param Integer $program current program being used
     * @param Integer $processId process identifier
     * 
     */
    public function actionViewAllCopyExperiments($id, $program, $processId){
        $params = Yii::$app->request->queryParams;
        $model = $this->experiment->getExperiment($id);
        $params['ExperimentSearch']['experimentDbId'] = "not equals $id";
        $params['ExperimentSearch']['experimentStatus'] = "in: ('created', 'planted', 'planted;crossed', 'mapped','completed')";
        $params['ExperimentSearch']['programDbId'] = $model['programDbId']."";

        // get params for data browser
        $paramPage = '';
        $pageParams = '';
        $paramSort = '';
        
        // get current sorting
        if (isset($_GET['sort'])) {
            $paramSort = 'sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }

        $paramLimit = '?limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        
      
        if (isset($_GET['dynagrid-copyexperiment-browser-page'])){
            $paramPage = '&page=' . $_GET['dynagrid-copyexperiment-browser-page'];
        }

        $currentPage = !empty($params) ? isset($_GET['dynagrid-copyexperiment-browser-page']) ? $_GET['dynagrid-copyexperiment-browser-page'] : null : null;
            
        $pageParams = $paramLimit.$paramPage;
        if($paramSort != ''){
            $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;
        }
        
        $searchModel = \Yii::$container->get('app\models\ExperimentSearch');
        $dataProvider = $searchModel->search($params, $pageParams, $currentPage, true);

        return $this->render('view_copy_experiments',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'program' => $program, 
            'id' => $id,
            'experiment' => $model,
            'processId' => $processId
        ]);

    }

    /**
     * View all available package records of an experiment
     *
     * @param int $id experiment record identifier
     * @param string $program current program being used
     * @param int $processId process identifier
     */
    public function actionViewAllPackages($id, $program, $processId) {
        $params = Yii::$app->request->queryParams;

        if (isset($_GET)) {

            // Get pagination info that can alternatively be used to query API
            $pagination['limit'] = $this->userDashboardConfig->getDefaultPageSizePreferences();
            // Get page number from DynaGrid cookies
            if (empty($_GET['dynagrid-package-browser-page'])) {
                $_GET['dynagrid-package-browser-page'] = 1;
            }
            $pagination['page'] = $_GET['dynagrid-package-browser-page'];

            $filter = [
                'yearFilter' => $_GET['yearFilter'],
                'seasonFilter' => $_GET['seasonFilter'],
                'experimentFilter' => $_GET['experimentFilter'],
                'stageFilter' => $_GET['stageFilter'],
                'programFilter' => $program,
            ];

            $defaultFilterStr = http_build_query($filter);;

            if(isset($params['PackageModel'])){
                foreach($params['PackageModel'] as $key=>$value){
                    if($value != ''){
                        $filter[$key] = $value;
                    }
                }
            }

            // Get all package records
            $searchModel = $this->packageModel;
            // NOTE: this will store records client-side and may be slow
            $packageDataProvider = $searchModel->search($id, $filter, $params);
            extract($packageDataProvider);
            $htmlData = $this->render('view_packages', [
                'id' => $id,
                'program' => $program,
                'processId' => $processId,
                'dataProvider' => $dataProvider,
                'entryNumbers' => $entryNumbers,
                'partitionView' => $partitionView,
                'searchModel' => $searchModel,
                'defaultFilterStr' => $defaultFilterStr
            ]);
            return $htmlData;
        }
    }

    /***
     * View fullscreen layout for planting arrangement
     *
     * @param Integer $blockDbId experiment block record identifier
     * */
    public function actionViewBlockLayout($blockDbId) {
        $xAxis = [];
        $yAxis = [];
        $data = [];

        $blockRecord = $this->experimentBlock->searchAll(['experimentBlockDbId' =>"$blockDbId"]);
        if(isset($blockRecord['data'][0])){
            $maxCol = $blockRecord['data'][0]['noOfCols'];
            $maxRows = $blockRecord['data'][0]['noOfRanges'];
            $xAxis = range(0,$maxCol, 1);
            $yAxis = range(0,$maxRows, 1);

            $record = $blockRecord['data'][0]['layoutOrderData'];
            if ($record != NULL) {
                usort($record, function ($first, $second) {
                    return $first['plotno'] > $second['plotno'] ? 1 : -1;
                });
               
                foreach($record as $plot){
                    $temp = [];
                    $temp['x'] = $plot['field_x'];
                    $temp['y'] = $plot['field_y'];
                    $temp['value'] = $plot['plotno'];
                    $temp['entNo'] = $plot['entno'];
                    $data[] = $temp;
                }

            }
        }

        return json_encode([
            'xAxis'=>$xAxis,
            'yAxis'=>$yAxis,
            'data'=>$data,
        ]);
    }

    /**
     * View nursery experiment records
     * 
     * @param Integer $id record id of the experiment
     * @param Integer $program current program being used
     * @param Integer $processId process identifier
     * 
     */
    public function actionViewAllNurseryExperiments($id, $program, $processId){
        $params = Yii::$app->request->queryParams;
        $model = $this->experiment->getExperiment($id);
        $params['ExperimentSearch']['experimentDbId'] = "not equals $id";
        $params['ExperimentSearch']['experimentStatus'] = "equals completed|equals planted|equals planted;crossed";
        $params['ExperimentSearch']['experimentType'] = "%nursery%";
        $params['ExperimentSearch']['programDbId'] = $model['programDbId']."";

        // get params for data browser
        $paramPage = '';
        $pageParams = '';
        $paramSort = '';
        
        // get current sorting
        if (isset($_GET['sort'])) {
            $paramSort = 'sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }

        $paramLimit = '?limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();
        
      
        if (isset($_GET['dynagrid-copyexperiment-browser-page'])){
            $paramPage = '&page=' . $_GET['dynagrid-copyexperiment-browser-page'];
        }

        $currentPage = !empty($params) ? isset($_GET['dynagrid-copyexperiment-browser-page']) ? $_GET['dynagrid-copyexperiment-browser-page'] : null : null;
            
        $pageParams = $paramLimit.$paramPage;
        if($paramSort != ''){
            $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;
        }
        
        $searchModel = \Yii::$container->get('app\models\ExperimentSearch');
        $dataProvider = $searchModel->search($params, $pageParams, $currentPage, true);

        return $this->render('view_nursery_experiments',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'program' => $program, 
            'id' => $id,
            'experiment' => $model,
            'processId' => $processId
        ]);
    }

    /**
     * Get harvested information
     * 
     * @param Integer $id record id of the experiment
     * 
     */
    public function actionGetHarvestedPlots($id){
        $data = [
            'entries' => [],
            'count' => 0
        ];
        $data = $this->experimentModel->getHarvestedPlots($id);

        return json_encode($data);
    }
}