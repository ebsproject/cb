<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\dataproviders\ArrayDataProvider;
use yii\web\Controller;


use app\models\Entry;
use app\models\Experiment;
use app\models\Occurrence;
use app\models\PlanTemplate;
use app\models\User;
use app\models\Worker;

use app\modules\experimentCreation\models\AnalysisModel;
use app\modules\experimentCreation\models\DesignModel;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\UploadModel;

/**
 * Controller for design array upload
 */
class UploadController extends Controller
{
    protected $analysisModel;
    protected $designModel;
    protected $entry;
    protected $experiment;
    protected $occurrence;
    protected $planTemplate;
    protected $user;
    protected $upload;
    protected $worker;

    public function __construct($id, $module,
        AnalysisModel $analysisModel,  
        DesignModel $designModel,  
        Entry $entry,  
        Experiment $experiment,
        Occurrence $occurrence,
        PlanTemplate $planTemplate,
        User $user,
        UploadModel $upload,
        Worker $worker,
        $config = [])
    {
        $this->analysisModel = $analysisModel;
        $this->designModel = $designModel;
        $this->entry = $entry;
        $this->experiment = $experiment;
        $this->occurrence = $occurrence;
        $this->planTemplate = $planTemplate;
        $this->user = $user;
        $this->upload = $upload;
        $this->worker = $worker;
        parent::__construct($id, $module, $config);
    }

    /**
     * Render file upload for design array
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param array $occurrenceDbIds id list of occurrences
     * @param string $entityType type of plan template
     */
    public function actionRenderFileUploadForm($id, $program, $occurrenceDbIds = null, $entityType = null){
        $viewFile = '@app/modules/experimentCreation/views/create/design/upload.php';

        if($entityType != null){
            $planTemplateArray = $this->planTemplate->searchAll(['entityType' => $entityType, 'entityDbId'=>"equals $id"]);
            $viewFile = '@app/modules/experimentCreation/views/design/upload.php';
        }
        else if($occurrenceDbIds == null){
            $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'experiment', 'entityDbId'=>"equals $id"]);
        }
        else {
            $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'occurrence', 'occurrenceApplied'=>"equals $occurrenceDbIds"]);
        }

        $design = null;
        $acrossEnvDesign = $this->designModel->getAcrossEnvDesign($id);
        if($planTemplateArray['totalCount'] > 0){
            $experimentJson = $planTemplateArray['data'][0];
            $uiInfo = json_decode($experimentJson['mandatoryInfo'], true);
            $designId = $uiInfo['design_id'];
            $design = $uiInfo['design_name'];
        } 
        
        $htmlData = $this->renderAjax($viewFile,[
            'id' => $id,
            'program' => $program,
            'designInfo' => $_POST['designInfo'],
            'design' => $design,
            'occurrenceDbIds' => explode(';',$occurrenceDbIds),
            'acrossEnvDesign'=>$acrossEnvDesign
        ]);

        return json_encode($htmlData);
    }

    /**
     * Upload design array in CSV format
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @return array $result successful or not, and if not, includes error message
     */
    public function actionDesignArray($id, $program, $occurrenceDbIds = null){
        try {
            if(isset($_FILES['upload_design_array'])){

                $experiment = $this->experiment->getOne($id);
                $experimentDesign = $experiment['data']['experimentDesignType'];

                if(in_array(strtolower($experimentDesign), ['rcbd', 'augmented rcbd', 'partially replicated', 'alpha-lattice', 'unspecified', 'row-column'])){
                    $result = $this->upload->preValidateFile('upload_design_array', $id, $experimentDesign,$occurrenceDbIds);
                } 

                return json_encode($result);
            }
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'error' => 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.'
            ]);
        }
    }

    /**
     * Upload planting arrangement in CSV format
     *
     * @param integer $id record id of the experiment
     * @param string $program name of the current program
     *
     * @return array $result successful or not, and if not, includes error message
     */
    public function actionPaFile($id, $program){
        try {
            if(isset($_FILES['upload_pa_file'])){
                $experiment = $this->experiment->getOne($id);
                $experimentType = strtolower($experiment['data']['experimentType']);

                $result = $this->upload->preValidatePaFile('upload_pa_file', $experimentType);

                if($result['success']) {
                    $result = $this->upload->validatePaFile('upload_pa_file', $result, $id);
                }

                return json_encode($result);
            }
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'error' => 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.'
            ]);
        }
    }

    /**
     * Submit file to AF
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     */
    public function actionSubmitFile($id, $program, $occurrenceDbIds = null){
        try {
            extract($_POST);
            
            //get needed information
            $uploadInfo = $this->upload->getUploadInformation($id, $occurrences, $occurrenceDbIds);
            $uploadInfo['designFileName'] = $tmpfilename;
            
            //check if there's existing json input for the experiment design
            //POST search plan-templates
            $model = $this->experiment->getExperiment($id);

            $designInfo = json_decode($designInfo, true);

            $designId = 0;
            $design = '';

            if($occurrenceDbIds == null){
                $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'experiment', 'entityDbId'=>"equals $id"]);
            } else {
                $occurrenceDbIds = str_replace("|",";", $occurrenceDbIds);
                $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'occurrence', 'occurrenceApplied'=>"equals $occurrenceDbIds"]);
            }

            if($planTemplateArray['totalCount'] > 0){
                $experimentJson = $planTemplateArray['data'][0];
                $uiInfo = json_decode($experimentJson['mandatoryInfo'], true);
                $designId = $uiInfo['design_id'];
                $design = $uiInfo['design_name'];

            } else {
                foreach($designInfo as $key => $designRec){
                    if(strpos($designRec['designCode'],'ALPHALATTICE')){
                        $designId = $key;
                        $design = $designRec['designName'];
                        break;
                    }
                }
            }

            $plantTemplateDbId = $planTemplateArray['data'][0]['planTemplateDbId'];

            if($occurrenceDbIds == null){
                $plantTemplateDbId = $this->upload->createPlanTemplate($id, $designId, $design, $information);
                $status = explode(';', $model['experimentStatus']);
                $experimentStatus = '';

                
                //Update experiment design
                $experimentStatus = 'entry list created';
                
                $this->experiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
                
                if(strtolower($design) != 'unspecified'){
                    $submitRequest = $this->analysisModel->submitUnspecifiedDesign($id, $uploadInfo);
                } else {
                    $submitRequest = $this->analysisModel->submitUnspecifiedDesign($id, $uploadInfo);
                }
                
                
                if($submitRequest['success']){
                
                    $experimentJsonTemp['status'] = 'upload in progress';
                    $experimentJsonTemp['randomizatDataResults'] = '{}';
                    $experimentJsonTemp['randomizationResults'] = '{}';

                    //store request id
                    $experimentJsonTemp['requestDbId'] = $submitRequest['requestId'];

                    $experimentJsonTemp['notes'] = '{}';

                    // Update experiment design
                    $experiment = $this->experiment->getExperiment($id);
                    $this->planTemplate->updateOne($plantTemplateDbId, $experimentJsonTemp);
                    $status = explode(';',$experiment['experimentStatus']);
                
                    //Update experiment design
                    $experimentStatus = 'entry list created;design requested';
                    
                    $update = $this->experiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
                    return json_encode('success');
                    
                } else {
                    $experimentJsonTemp['status'] = 'upload failed';
                    $experimentJsonTemp['randomizatDataResults'] = '{}';
                    $experimentJsonTemp['randomizationResults'] = '{}';

                    // Update experiment design
                    $this->planTemplate->updateOne($plantTemplateDbId, $experimentJsonTemp);
                    return json_encode('failed');
                }

                return json_encode("OK");
            } else {

                $this->experiment->updateOne($id, ['experimentStatus'=>'creation in progress']);
                
                if(strtolower($design) != 'unspecified'){
                    $submitRequest = $this->analysisModel->submitUnspecifiedDesign($id, $uploadInfo);
                } else {
                    $submitRequest = $this->analysisModel->submitUnspecifiedDesign($id, $uploadInfo);
                }

                if($submitRequest['success']){
                
                    $experimentJsonTemp['status'] = 'upload in progress';
                    $experimentJsonTemp['randomizatDataResults'] = '{}';
                    $experimentJsonTemp['randomizationResults'] = '{}';

                    //store request id
                    $experimentJsonTemp['requestDbId'] = $submitRequest['requestId'];

                    $experimentJsonTemp['notes'] = '{}';

                    // Update plan template design
                    $this->planTemplate->updateOne($plantTemplateDbId, $experimentJsonTemp);

                    // Update occurrences status
                    $occurrenceIds = explode(";", $occurrenceDbIds);
                    $this->occurrence->updateMany($occurrenceIds, ['occurrenceStatus'=>'design generation in progress']);
                  
                    return json_encode('success');
                    
                } else {
                    $experimentJsonTemp['status'] = 'upload failed';
                    $experimentJsonTemp['randomizatDataResults'] = '{}';
                    $experimentJsonTemp['randomizationResults'] = '{}';

                    // Update experiment design
                    $this->planTemplate->updateOne($plantTemplateDbId, $experimentJsonTemp);

                    return json_encode('failed');
                }

                return json_encode("OK");
            }
            
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'error' => 'Error in submitting the file. Try uploading the file again. If error persists, submit a bug report.'
            ]);
        }
    }

    /**
     * Submit file to BA-API new workflow
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     */
    public function actionSubmitFileNew($id, $program, $occurrenceDbIds = null){
        try {

            extract($_POST);
            
            //get needed information
            $uploadInfo = $this->upload->getUploadInformation($id, $occurrences, $occurrenceDbIds);
            $uploadInfo['designFileName'] = $tmpfilename;

            //check if there's existing json input for the experiment design
            //POST search plan-templates
            $model = $this->experiment->getExperiment($id);
           
            $plantTemplateDbIds = $this->upload->createPlanTemplateForParamSet($id, $information, $uploadInfo, $paramDesignStr);
            $status = explode(';', $model['experimentStatus']);
            $experimentStatus = '';

            //Update experiment design
            $experimentStatus = 'entry list created';
            
            $this->experiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
            
            $submitRequest = $this->analysisModel->submitDesignNew($id, $uploadInfo);
            
            if($submitRequest['success']){
            
                $experimentJsonTemp['status'] = 'upload in progress';
                $experimentJsonTemp['randomizationResults'] = '{}';

                //store request id
                $experimentJsonTemp['requestDbId'] = $submitRequest['requestId'];

                $experimentJsonTemp['notes'] = '{}';

                // Update experiment design
                $experiment = $this->experiment->getExperiment($id);
                $this->planTemplate->updateMany($plantTemplateDbIds, $experimentJsonTemp);
                $status = explode(';',$experiment['experimentStatus']);
            
                //Update experiment design
                $experimentStatus = 'entry list created;design requested';
                
                $update = $this->experiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
                $this->experiment->updateOne($id, ['experimentDesignType'=>'']);
                return json_encode('success');
                
            } else {
                $experimentJsonTemp['status'] = 'upload failed';
                $experimentJsonTemp['randomizationResults'] = '{}';

                // Update experiment design
                $this->planTemplate->updateMany($plantTemplateDbIds, $experimentJsonTemp);
                return json_encode('failed');
            }

            return json_encode("OK");
    
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'error' => 'Error in submitting the file. Try uploading the file again. If error persists, submit a bug report.'
            ]);
        }
    }

    /**
     * Submit PA file to worker
     *
     * @param integer $id record id of the experiment
     * @param string $program name of the current program
     * @param integer $blockId id record of the block
     */
    public function actionSubmitPaFile($id, $program, $blockId){
        $fileContent = $_POST['fileContent'];

        // Trigger background worker
        $this->worker->invoke(
            'CreateExperimentPARecords',              // Worker name
            'Creating planting arrangement records',  // Description
            'EXPERIMENT_BLOCK',                       // Entity
            $id,                                      // Entity ID
            'EXPERIMENT_BLOCK',                       // Endpoint Entity
            'EXPERIMENT_CREATION',                    // Application
            'POST',                                   // Method
            [                                         // Worker data
                'requestData' => [
                    'processName' => 'save-pa-file-records',
                    'experimentDbId' => $id."",
                    'blockIds' => [$blockId],
                    'fileContent' => $fileContent
                ]
            ]
        );

        // Update experiment status and notes
        $this->experiment->updateOne($id, ['experimentStatus'=>'generation of planting arrangement in progress','notes' => 'plantingArrangementChanged']);

        // Flash message
        $experiment = $this->experiment->getOne($id);
        $flashMessage = Yii::t('app', 'The planting arrangement records for <b>'.$experiment['data']['experimentName'].'</b> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.');
        Yii::$app->session->setFlash('info', '<i class="fa fa-fw fa-info-circle  font-md"></i> '. $flashMessage);

        // Redirect to main Experiment Browser
        $this->redirect(['create/index?program='.$program.'&id='.$id]);
    }

    /** 
     * Download sample file
     * 
     * @param String $filename name of the sample file
    */
    public function actionCheckFile($filename){

        $design = isset($_POST['design']) ? $_POST['design']:"";
        $oldDesign = $design;
        $design = strtoupper(str_replace(' ', '', $design));
        $path = realpath(Yii::$app->basePath) . '/modules/experimentCreation/views/create/design/';

       
        if($oldDesign == 'Sparse Testing'){
            $path = realpath(Yii::$app->basePath) . '/modules/experimentCreation/views/design/';
            $design = "_".$design;
        } elseif((!empty($design) && $design != 'Sparse Testing') || empty($design)){
            $design = "";
        } else {
            $design = "_".$design;
        }
       
        if($filename == 'template1file'){
            $filename = 'Template1FieldBook'.$design.'.csv';
        } else if($filename == 'requiredheaders'){
            $filename = 'RequiredHeaders.csv';
        } else {
            $filename = 'Template1Fieldbook'.$design.'.csv';
        }

        if(file_exists($path.$filename)){
            return json_encode(
                [   
                    'success'=> true,
                    'path'=>$path,
                    'filename'=>$filename
                ]
            );
            
        } else {
            return json_encode( ['success'=> false]);
        }
    }

    public function actionDownloadFile($filename, $path){
        $file_contents = file_get_contents($path.$filename);
        $filepath = $path.$filename;
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Disposition: attachment; filename=$filename");
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path.$filename));
        flush(); // Flush system output buffer
        readfile($filepath);
        die();
    }

     /**
     * Pre-validate csv file
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @return array $result successful or not, and if not, includes error message
     */
    public function actionPrevalidateFile($id, $program, $occurrenceDbIds = null){
        try {
            
            if(isset($_FILES['upload_design_array'])){

                $experiment = $this->experiment->getOne($id);
                $experimentDesign = 'Unspecified';
                
                $result = $this->upload->prevalidateUploadFile('upload_design_array', $id, $experimentDesign,$occurrenceDbIds);

                return json_encode($result);
            }
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'error' => 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.'
            ]);
        }
    }

     /**
     * Validate csv file
     * 
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @return array $result successful or not, and if not, includes error message
     */
    public function actionValidateViaApi($id, $program, $occurrenceDbIds = null){
        try {
            
            if(isset($_POST)){
                $error['success'] = false;
                $experiment = $this->experiment->getOne($id);
                $experimentDesign = strtolower($experiment['data']['experimentDesignType']);
                
                $designStr = $_POST['designStr'];
                
                $result = $this->upload->validateViaApi($_POST['tmpFileName'], $designStr, $_POST['type'], $id);
        
                extract($result);
                if ($err) {
                    $error['error'] = $err;
                    return json_encode($error);
                } else {
                    $response = json_decode($response, true);
                    
                    if(isset($response['result']['data'][0]) && $response['metadata']['additional']['isValid']){

                        $error['success'] = true;
                        return json_encode($error);
                    } else {
                  
                        $error['error'] = 'Encountered problems during validation. Kindly check the uploaded file.';
                        
                        if(isset($response['metadata']['additional']['failedRules'])){
                            
                            $rules = $response['metadata']['additional']['failedRules'];
                            $error['error'] = "Uploaded file failed in the following validation rules: <br />";
                            
                            foreach($rules as $rule){
                                foreach($rule as $r){
                                    $msg = $r['description'];
                                    unset($r['description']);
                                    
                                    $errStr = [];
                                    foreach($r as $key=>$e){
                                        $errStr[] = $key.":".json_encode($e, true);
                                    }
                                    $error['error'] .= "<b>&bull; ".$msg."</b> (".implode(", ", $errStr).")<br />";
                                }
                            }
                        } else if(isset($response['detail'])){

                            $rules = $response['detail'];
                            $error['error'] = "Uploaded file failed in the following validation rules: <br />";
                            
                            foreach($rules as $rule){
                                $msg = $rule['msg'];
                                unset($rule['msg']);
                                
                                $errStr = [];
                                foreach($rule as $key=>$e){
                                    $errStr[] = $key.":".json_encode($e, true);
                                }
                                $error['error'] .= "<b>&bull; ".$msg."</b> (".implode(", ", $errStr).")<br />";
                            }
                        }
                        return json_encode($error);
                    }
                }
                

            }
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'error' => 'Error in uploading file. Try uploading the file again. If error persists, submit a bug report.'
            ]);
        }
    }

}
