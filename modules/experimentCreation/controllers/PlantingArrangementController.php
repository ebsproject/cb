<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\experimentCreation\controllers;

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Entry;
use app\models\EntryList;
use app\models\EntrySearch;
use app\models\Experiment;
use app\models\ExperimentBlock;
use app\models\ExperimentData;
use app\models\Item;
use app\models\User;
use app\models\Variable;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\modules\experimentCreation\models\EntryOrderModel;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\PlantingArrangement;
use app\modules\experimentCreation\models\PAEntriesModel;
use app\modules\experimentCreation\models\PAWorkingListModel;
use app\dataproviders\ArrayDataProvider;

/**
 * Controller for planting arrangement
 */
class PlantingArrangementController extends Controller
{

    protected $entry;
    protected $entryList;
    protected $entrySearch;
    protected $entryOrderModel;
    protected $experiment;
    protected $experimentBlock;
    protected $experimentData;
    protected $item;
    protected $user;
    protected $experimentModel;
    protected $paEntriesModel;
    protected $paWorkingListModel;
    protected $plantingArrangement;
    protected $variable;

    public function __construct($id, $module,
        Entry $entry, 
        EntryList $entryList, 
        EntrySearch $entrySearch,
        EntryOrderModel $entryOrderModel, 
        Experiment $experiment,
        ExperimentBlock $experimentBlock,
        ExperimentData $experimentData,
        Item $item, 
        User $user,
        ExperimentModel $experimentModel,
        PAEntriesModel $paEntriesModel, 
        PAWorkingListModel $paWorkingListModel, 
        PlantingArrangement $plantingArrangement, 
        Variable $variable, 
        $config = [])
    {
        $this->entry = $entry;
        $this->entryList = $entryList;
        $this->entrySearch = $entrySearch;
        $this->entryOrderModel = $entryOrderModel;
        $this->experiment = $experiment;
        $this->experimentBlock = $experimentBlock;
        $this->experimentData = $experimentData;
        $this->item = $item;
        $this->user = $user;
        $this->experimentModel = $experimentModel;
        $this->paEntriesModel = $paEntriesModel;
        $this->paWorkingListModel = $paWorkingListModel;
        $this->plantingArrangement = $plantingArrangement;
        $this->variable = $variable;
        parent::__construct($id, $module, $config);
    }
    /**
     * Add blocks
     * @param $tab string present activity tab
     * @param $program string current program
     * @param $id integer Experiment ID
     * @param $processId integer Data Process ID 
     * @param $sessionCheckFlag boolean flag for hard reload checking of session saved
     * @param $savingFlag boolean flag for checking of session saved
     */
    public function actionAddBlock(string $tab = null, $program, $id, $processId, $sessionCheckFlag = false, $savingFlag = false)
    {
        $tab = is_null($tab)? 'add-blocks': $tab;

        $this->layout = '@app/views/layouts/dashboard';

        // check if to hard reload
        $sessionCheckFlag = (Yii::$app->session->get('pa-add-blocks-reload-' . $id) !== null) ? true : $sessionCheckFlag;
        $params = ["fields" => "experiment.id AS experimentDbId|parentExperimentBlock.id AS parentExperimentBlockDbId|experimentBlock.id AS experimentBlockDbId|experimentBlock.experiment_block_code AS experimentBlockCode|experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.no_of_blocks AS noOfBlocks|experimentBlock.block_type AS blockType|experimentBlock.order_number AS orderNumber|experimentBlock.is_active AS isActive",
            "experimentDbId" => "$id", "parentExperimentBlockDbId" => "is null"];
        $data = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data['data'],
            'key' => 'experimentBlockCode',
            'pagination' => false,
            'sort' => [
                'attributes' => ['orderNumber'],
                'defaultOrder' => ['orderNumber' => SORT_ASC]
            ]
        ]);
        // update status of experiment
        return $this->render('_add_blocks', [
            'program' => $program,
            'id' => $id,
            'processId' => $processId,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Load information per block in Manage Blocks tab
     */
    public function actionManageBlockTab() {
        $blockData = $_POST['blockData'];
        $blockData = json_decode($blockData, true);

        $currentBlockId = $blockData['subBlockId'];

        $sBlockParams = $this->plantingArrangement->getBlockInformation($currentBlockId);

        $blockParams = $blockData['params'];
        $blockParams['subBlockParams'] = $sBlockParams;

        return Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/planting-arrangement/_manage_blocks_pages.php',[
            'subBlockId'=>$blockData['subBlockId'],
            'totalPlots'=>$blockData['totalPlots'],
            'code'=>$blockData['code'],
            'params'=>$blockParams,
            'startingCornerValues'=>$blockData['startingCornerValues'],
            'plotNumberingValues'=>$blockData['plotNumberingValues'],
            'varCols'=>$blockData['varCols'],
            'id'=>$blockData['id'],
            'activeTabBlockId'=>$blockData['activeTabBlockId'],
            'experimentType'=>$blockData['experimentType'],
        ]);
    }

    /**
     * Assign Entries
     * @param $program string current program
     * @param $id integer experiment ID
     * @param $processId integer Data Process ID 
     */
    public function actionAssignEntries($program, $id, $processId)
    {
        $this->layout = '@app/views/layouts/dashboard';
        $filters = [];
        // retrieve entry list variables
        $entryListVars = $this->experimentModel->retrieveConfiguration('specify-entry-list', $program, $id, $processId);

        $userId = $this->user->getUserId();
        $filteredIdName = "ec"."_filtered_entry_ids_$id";
        $selectedIdName = "ec"."_selected_entry_ids_$id";
        //destroy selected items session
        if(((isset($_GET['reset']) && $_GET['reset'] == 'hard_reset' && !isset($_GET['grid-entry-list-grid-page'])) ||
            (!isset($_GET['reset']) && !isset($_GET['grid-entry-list-grid-page']))) && isset($_SESSION[$selectedIdName])){
            unset($_SESSION[$selectedIdName]);
            if(isset($_SESSION[$filteredIdName])){
                unset($_SESSION[$filteredIdName]);
            }
        }
        $storedItems = Yii::$app->session->get($selectedIdName);

        // get params for data browser
        $paramPage = '';
        $paramSort = '';

        if (isset($_GET['grid-entry-list-grid-sort'])) {
            $paramSort = 'sort=';
            $sortParams = $_GET['grid-entry-list-grid-sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }

        if (isset($_GET['dp-1-page'])){
            $paramPage = '&page=' . $_GET['dp-1-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        } else if (isset($_GET['grid-entry-list-grid-page'])) {
            $paramPage = '&page=' . $_GET['grid-entry-list-grid-page'];
        }
        $paramLimit = '?limit='.getenv('CB_DATA_BROWSER_DEFAULT_PAGE_SIZE');

        //This will be used once the showPersonalize issue previously found gets fixed, pls do not remove yet
        // if(isset($cookies['pa-search-entries-grid_'])) {  // Get page size from cookies of dynagrid
        //     $paramLimit = $cookies->getValue('pa-search-entries-grid_');
        //     if (!is_array($paramLimit)) {
        //         $paramLimit = json_decode($paramLimit, true);
        //         $paramLimit = json_decode($paramLimit['grid'], true);
        //         $paramLimit = $paramLimit['page'];
        //         $paramLimit = '?limit=' . $paramLimit;
        //     } else {
        //         $paramLimit = '?limit=10';
        //     }
        // }
        $pageParams = $paramLimit.$paramPage;
        // append sorting condition
        $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;

        $urlParams = Yii::$app->request->queryParams;
        
        if (Yii::$app->session->get('pa-entries-filters-' . $id) !== null) {
            $filters = Yii::$app->session->get('pa-entries-filters-' . $id);
            $dataProvider = $this->plantingArrangement->searchEntries($id, $entryListVars, $filters, $urlParams, $this->entrySearch,$pageParams);
            $gridClass = '';
        } else {
            $gridClass = 'hidden';
            $dataProvider = $this->plantingArrangement->searchEntries($id, $entryListVars, 'empty', $urlParams, $this->entrySearch,$pageParams);
        }
        $attributes=['entryDbId', 'entryNumber', 'designation','entryClass','entryRole'];
        $dataProvider->sort =[
            'attributes' => $attributes,
            'defaultOrder' => [
                'entryNumber' => SORT_ASC
            ]
        ];

        $getItemDataProc = $this->item->searchItemRecord(['itemDbId' => "$processId"]);
        $dataProcessName = $getItemDataProc['data']['abbrev'];

        //entry working list
        $blockCheck = $this->plantingArrangement->getBlocksByExperimentId($id);
        $blockFlag = count($blockCheck->allModels);

        if($blockFlag == 0){
            $workingListDataProvider = new \yii\data\ArrayDataProvider([]);
        }else{
            if(!empty(Yii::$app->session->get('workListDataProvider'))){
                $data1 = Yii::$app->session->get('workListDataProvider');
                $workingListDataProvider = $data1;
            }else{
                $workingListDataProvider = new \yii\data\ArrayDataProvider([]);
            }
        }
           
        $params = Yii::$app->request->queryParams;
        $workingListDataProvider2 = $this->paWorkingListModel->search($params, $id);

        $entryListId = $this->entryList->getEntryListId($id);

        //get entry replicates information
        $entryReplicates = $this->paEntriesModel->getEntryReplicatesInfo($id, $entryListId);
        
        return $this->render('_assign_entries', [
            'program' => $program,
            'id' => $id,
            'processId' => $processId,
            'dataProvider' => $dataProvider,
            'entryListVars' => isset($entryListVars) ? $entryListVars : [],
            'filters' => $filters,
            'gridClass' => $gridClass,
            'processName' => $dataProcessName,
            'workingListDataProvider' => $workingListDataProvider,
            'workingListFilterModel' => $this->paWorkingListModel,
            'selectedItems' => $storedItems,
            'entryListId' => $entryListId,
            'entryReplicates' => $entryReplicates
        ]);
    }

    /**
     * Render manage blocks
     *
     * @param $program text program identifier
     * @param $id integer experiment identifier
     * @param $processId integer process activity identifier
     */
    public function actionManageBlocks($program, $id, $processId)
    {
        // retrieve block data
        $blocksData = $this->plantingArrangement->getBlocksDataByExperimentId($id);

        $startingCornerValues = $this->plantingArrangement->getScaleValuesByAbbrev('STARTING_CORNER');
        $plotNumberingOrderValues = $this->plantingArrangement->getScaleValuesByAbbrev('PLOT_NUMBERING_ORDER');
        // retrieve entry list variables
        $entryListVars = $this->experimentModel->retrieveConfiguration('specify-entry-list', $program, $id, $processId);

        $activeTab = null;
        if (Yii::$app->session->get('pa-manage-block-active-tab-' . $id) !== null) {
            $activeTab = Yii::$app->session->get('pa-manage-block-active-tab-' . $id);
        }

        $experiment = $this->experiment->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experiment.experiment_type AS experimentType',
            'experimentDbId' => 'equals '.$id
        ]);
        $experimentType = $experiment['data'][0]['experimentType'];

        $htmlData = $this->renderPartial('_manage_blocks', [
            'id' => $id,
            'program' => $program,
            'processId' => $processId,
            'blocksData' => $blocksData,
            'startingCornerValues' => $startingCornerValues,
            'plotNumberingOrderValues' => $plotNumberingOrderValues,
            'entryListVars' => isset($entryListVars['varConfigValues']) ? $entryListVars['varConfigValues'] : [],
            'activeTab' => $activeTab,
            'experimentType' => $experimentType
        ], true, false);

        return $htmlData;
    }

    /**
     * Save active tab to session
     *
     * @param $id integer experiment identifier
     */
    public function actionManageBlockSaveActiveTab($id)
    {
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : null;

        Yii::$app->session->set('pa-manage-block-active-tab-' . $id, $blockId);
        Yii::$app->session->set('pa-manage-block-removed-plot-' . $id . '-' . $blockId, true);
    }

    /**
     * Render palnting arrangement overview
     * 
     * @param $program text program identifier
     * @param $id integer experiment identifier
     * @param $processId integer process activity identifier
     */
    public function actionOverview($program, $id, $processId)
    {

        $dataProvider = $this->plantingArrangement->getBlocksByExperimentId($id, 'sort=parentExperimentBlockDbId:asc&orderNumber:asc');
        $replicateCountAssigned = $this->plantingArrangement->getTotalReplicateCountAssignedToBlock($id);
        $plotCountAssigned = $this->plantingArrangement->getTotalPlotCountAssignedToBlock($id);
        $replicateCount = $this->plantingArrangement->getTotalReplicateCount($id);

        $htmlData = $this->renderPartial('_overview', [
            'id' => $id,
            'program' => $program,
            'processId' => $processId,
            'dataProvider' => $dataProvider,
            'replicateCountAssigned' => $replicateCountAssigned,
            'plotCountAssigned' => $plotCountAssigned,
            'replicateCount' => $replicateCount
        ], true, false);
        return $htmlData;
    }

    /**
     * Save active planting arrangement tab to session
     *
     * @param $id integer experiment identifier
     */
    public function actionPaSaveActiveTab($id)
    {
        $tab = isset($_POST['tab']) ? $_POST['tab'] : null;

        Yii::$app->session->set('pa-active-tab-' . $id, $tab);
    }

    /**
     * Save filters
     * @param integer $id Experiment ID
     */
    public function actionSaveFilters($id)
    {
        $filters = isset($_POST['filters']) ? json_decode($_POST['filters'], true) : '';
        
        //Set to null the selected entries to be added to a block
         Yii::$app->session->set('selectedItems-'.$id, null);
         Yii::$app->session->set('workListDataProvider', null);

        Yii::$app->session->set('pa-entries-filters-' . $id, $filters);
    }

    /**
     * Add new rows
     * @param $id integer experiment identifier
     */
    public function actionAddNewRows($id)
    {
        $noOfRows = $_POST['rows'];
        $hardReload = isset($_POST['reload']) ? true : false;

        // if to reload page and there are added new rows
        if ($hardReload) {
            Yii::$app->session->set('pa-add-blocks-reload-' . $id, 'reload');
        }

        $this->plantingArrangement->addNewRows($id, $noOfRows);
    }

    /**
     * Remove block in add blocks
     * @param $id integer experiment identifier
     */
    public function actionRemoveRow($id)
    {
        $rowId = $_POST['id'];

        // delete sub blocks first
        $parentBlock = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => '
                experimentBlock.id AS experimentBlockDbId|
                parentExperimentBlock.id as parentExperimentBlockDbId',
                'parentExperimentBlockDbId' => "equals $rowId",
        ]);
        foreach ($parentBlock['data'] as $subBlock) {
            $this->plantingArrangement->deleteBlock($id, $subBlock['experimentBlockDbId']);
        }

        // delete parent block
        $this->plantingArrangement->deleteBlock($id, $rowId);
    }

    /**
     * Save blocks
     * @param $id integer save blocks
     */
    public function actionSaveBlocks($id)
    {
        $this->plantingArrangement->saveBlocks($id);
        $this->plantingArrangement->checkExperimentPAUpdate($id);
    }

    /**
     * Add sub block in experiment
     *
     * @param $id integer experiment identifier
     */
    public function actionAddSubBlockSingle($id)
    {
        $savedVal = $_POST['savedVal'];
        $rowId = $_POST['rowId'];
        $newVal = $_POST['newVal'];

        $this->plantingArrangement->actionAddSubBlockSingle($id, $rowId, $newVal);
    }

    /**
     * Get search tags for assign entries
     * @param $id integer experiment identifier
     */
    public function actionSearchTags($id)
    {
        $attr = $_POST['attr'];
        $variableAbbrev = $_POST['attr'];

        if (strpos($attr, '_') !== false) {
            $column = ucwords(strtolower($attr), '_');
            $column = str_replace('_', '', $column);
            $column = lcfirst($column);
        } else {
            $column = strtolower($attr);
        }

        $filterTags = $this->plantingArrangement->getSearchTags($column, $id, $variableAbbrev);
        return json_encode($filterTags);
    }

    /**
     * Extract variable filter option values
     * @param array q id page filter
     * @return json totalCount items
     */    
    public function actionGetFilterData($q=null,$attr=null,$page=null,$listId=null,$id=null)
    {   
        $offset = ($page==null ? 0 : (($page-1)*5)+1);
        $page = ($page==null ? 1 : $page);
        $limit = null;

        if (strpos($attr, '_') !== false) {
            $column = ucwords(strtolower($attr), '_');
            $column = str_replace('_', '', $column);
            $column = lcfirst($column);
        } else {
            $column = strtolower($attr);
        }

        $index = 'entryDbId';
        if($attr == 'entryType'){
            $params['distinctOn'] = 'entry_type';
            $params['fields'] = "entry.id|entry.entry_list_id|germplasm.designation|experiment.id|entry.entry_type|entry.entry_class";
            $params['entry_list_id'] = "equals ".$listId;
            $index = 'entry_type';
            $column = 'entry_type';
        } else {
            $params = ["fields"=> "entry.id as entryDbId|entry.entry_list_id as entryListDbId|germplasm.designation as designationVal|experiment.id as experimentDbId|entry.entry_type as entrytype|entry.entry_class as entryclass","entryListDbId" => "equals ".$listId, 'experimentDbId'=> "equals ".$id
            ];
        }

        if($q != null && $q != ""){
            $outputColumn = 'designation';
            $params[$outputColumn] = '%'.$q.'%'; 
            $params['fields'] = $outputColumn.' |'.$params['fields'];
        }
     

        $resultLimits = ($limit == NULL || $limit == 0 ? '?page='.($offset+1) : '?limit='.$limit.'&page='.(($offset/$limit)+1));

        $method = 'POST';
        $endPoint = 'entries-search';
        
        $apiCallResult = Yii::$app->api->getParsedResponse($method,$endPoint.$resultLimits, json_encode($params));

        if($column == 'designation'){
            $column = $column.'Val';
        }
        
        $trackArr = [];
        $result = [];

        foreach($apiCallResult['data'] as $value){
            if (!empty($value[$column]) && !in_array($value[$column], $trackArr)) {
                $trackArr[] = $value[$column];
                $result[] = ["id" => $value[$index], "text" => $value[$column]];
            }
        }
        
        return json_encode(['totalCount'=> $apiCallResult['totalCount'],'items'=>$result]);    
    }

    /**
     * Manage assign entries Working List
     *
     * @param $program string current program
     * @param $id integer Experiment ID
     * @param $processId integer Data Process ID
     */
    public function actionManageWorkingList($program, $id, $processId) {
        $reset = isset($_POST['reset'])? $_POST['reset'] : false;

        $paWlSelectedIndex = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_SELECTED_INDEX');
        if(is_null($paWlSelectedIndex) || $reset) {
            $this->paWorkingListModel->saveToWorkingListExperimentData($id, [], 'PA_WL_SELECTED_INDEX');
            $this->paWorkingListModel->saveToWorkingListExperimentData($id, [], 'PA_WL_ALL_FILTERED_INDEX');
        }

        $params = Yii::$app->request->queryParams;
        $workingListDataProvider = $this->paWorkingListModel->search($params, $id);

        $selectedIds = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_SELECTED_INDEX');

        return $this->renderAjax('entry_working_list.php',[
            'workingListDataProvider' => $workingListDataProvider,
            'workingListFilterModel' => $this->paWorkingListModel,
            'program' => $program,
            'id' => $id,
            'processId' => $processId,
            'selectedRepIds' => $selectedIds,
        ]);
    }

    /**
     * Select single item in working list
     * @param $id integer Experiment ID
     **/
    public function actionSelectRowInWorkingList($id) {
        if(isset($_POST['index'])) {
            $index = $_POST['index'];

            $allFilteredWorkingListIndex = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_ALL_FILTERED_INDEX');
            $currentSelectedIndex = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_SELECTED_INDEX');

            if(in_array($index, $currentSelectedIndex)) {  // remove from selected_index
                $currentSelectedIndex = array_diff($currentSelectedIndex, [$index]);
            } else {    // add to selected_index
                $currentSelectedIndex[] = $index;
                usort($currentSelectedIndex, function($a, $b) {
                    return intval($a) <=> intval($b);
                });
            }

            if(!empty($currentSelectedIndex)) {
                $currentSelectedIndex = implode(',',$currentSelectedIndex);
                $currentSelectedIndex = array_map('intval', explode(',', $currentSelectedIndex));
            }
            $currentSelectedIndex = array_values($currentSelectedIndex);
            $this->paWorkingListModel->saveToWorkingListExperimentData($id, $currentSelectedIndex, 'PA_WL_SELECTED_INDEX');
            $result = [
                'selectedCount' => count(array_values($currentSelectedIndex)),
                'totalFilteredCount' => count(array_values($allFilteredWorkingListIndex))
            ];
            return json_encode($result);
        }
    }

    /**
     * Select all items in working list
     * @param $id integer Experiment ID
     **/
    public function actionToggleWorkingListSelectAll($id) {
        if(isset($_POST['selectAll']) && isset($_POST['totalWorkingListCount'])) {
            $selectAll = $_POST['selectAll'];
            $allFilteredWorkingListIndex = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_ALL_FILTERED_INDEX');
            $totalFilteredCount = 0;

            if($selectAll == 'true') {
                $this->paWorkingListModel->saveToWorkingListExperimentData($id, $allFilteredWorkingListIndex, 'PA_WL_SELECTED_INDEX');
                $totalFilteredCount = count(array_values($allFilteredWorkingListIndex));
            } else {    // deselect all
                $this->paWorkingListModel->saveToWorkingListExperimentData($id, [], 'PA_WL_SELECTED_INDEX');
            }

            return $totalFilteredCount;
        }
    }

    /**
     * Select all items in working list
     * @param $id integer Experiment ID
     **/
    public function actionRemoveSelectedFromWorkingList($id) {
        $selectedIds = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_SELECTED_INDEX');
        $this->paWorkingListModel->removeFromWorkingList($id, $selectedIds);

        $this->paWorkingListModel->saveToWorkingListExperimentData($id, [], 'PA_WL_SELECTED_INDEX');    // clear selected in session

        return true;
    }

    /**
     * Render working list modal
     * @param $id integer Experiment ID
     **/
    public function actionManageReplicateOrderInWorkingList($id) {
        $result = [ // default values
            'success' => false,
            'data' => '',
            'message' => 'There was a problem in loading content. Please report using "Feedback".'
        ];

        // get working list
        $expData = $this->experimentData->getExperimentData($id, ['abbrev' => "equals PA_ENTRY_WORKING_LIST"]);
        $reps = json_decode($expData[0]['dataValue'], true);

        // get selected reps from working list using index
        $selectedIndex = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_SELECTED_INDEX');

        // get reps from working list and add index column to each rep
        $selectedReps = [];
        foreach ($selectedIndex as $index) {
            $i = intval($index);
            $r = $reps[$i];
            $r['index'] = $i;
            $selectedReps[] = $r;
        }

        // get data provider for selected reps in working list
        $dataProvider = $this->paWorkingListModel->getSelectedWorkingListProvider($selectedReps);

        // return as rendered view file
        $result['success'] = true;
        $result['data'] = $this->renderAjax('_pa_reorder_replicates_modal', [
            'dataProvider' => $dataProvider,
        ]);

        return json_encode($result);
    }

    /**
     * Reorder selected replicates in working list
     * @param $id integer Experiment ID
     **/
    public function actionReorderSelectedInWorkingList($id) {
        $result = [ // default values
            'success' => false,
            'data' => '',
            'message' => 'There was a problem in loading content. Please report using "Feedback".'
        ];

        if(isset($_POST['reorderMethod'])
            && isset($_POST['originalOrderNo'])
            && isset($_POST['newOrderNo'])
        ) {
            $reorderMethod = $_POST['reorderMethod'];
            $originalOrderNo = $_POST['originalOrderNo'];
            $newOrderNo = $_POST['newOrderNo'];

            $result = $this->paWorkingListModel->reorderSelectedInWorkingList($id, $reorderMethod, $originalOrderNo, $newOrderNo);
        }

        return json_encode($result);
    }

    /**
     * Render assign entries modal
     * @param $id integer Experiment ID
     */
    public function actionRenderAssignEntriesForm($id) {
        // get entry list
        $expData = $this->experimentData->getExperimentData($id, ['abbrev' => "equals ENTRY_LIST_TIMES_REP"]);
        $ents = json_decode($expData[0]['dataValue'], true);

        // get working list
        $expData = $this->experimentData->getExperimentData($id, ['abbrev' => "equals PA_ENTRY_WORKING_LIST"]);
        $reps = json_decode($expData[0]['dataValue'], true);

        // get selected reps from working list using index
        $selectedIndex = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_SELECTED_INDEX');

        // get $entryIds and $entryListIdList
        $entryIds = [];
        $entryListIdList = [];
        $initialLayoutOrderData = [];

        // get reps from working list and add index column to each rep
        foreach ($selectedIndex as $index) {
            $rep = $reps[intval($index)];
            // Double check if rep is actually unused from entry list. If rep is already used then skip adding rep.
            // This is to prevent the bug of adding the exact same replicates when the rep had already been added
            if(!is_null($ents[$rep['entryDbId']]['replicates'][$rep['replicate']])) continue;
            $entryIds[] = $rep['entryDbId'];
            $entryDbIdColumn = array_column($entryListIdList, 'entryDbId');
            if(!in_array($rep['entryDbId'], $entryDbIdColumn)) {    // add to entryListIdList
                // $entryListIdList format specifically for Systematic Arrangement
                $entryListIdList[] = [
                    'entryDbId' => $rep['entryDbId'],
                    'repno' => 1,
                    'replicates' => json_encode([$rep['replicate']])
                ];
            } else {    // update entryListIdList record
                $i = array_search($rep['entryDbId'], $entryDbIdColumn);
                $entryListIdList[$i] = [
                    'entryDbId' => $rep['entryDbId'],
                    'repno' => $entryListIdList[$i]['repno'] + 1,
                    'replicates' => json_encode(array_merge(json_decode($entryListIdList[$i]['replicates'], true), [$rep['replicate']]))
                ];
            }
            // make initial layout order data
            $initialLayoutOrderData[] = [
                'entry_id' => $rep['entryDbId'],
                'repno' => $rep['replicate'],
            ];
        }

        $dataProvider = $this->plantingArrangement->getBlocksByExperimentId($id, 'sort=parentExperimentBlockDbId:asc&orderNumber:asc');
        $blockFlag = count($dataProvider->allModels);
        $variables =  $this->plantingArrangement->getBlockVariables();

        // add remarks in variables
        $variables[] = [
            'abbrev' => 'REMARKS',
            'dataType' => 'text',
            'scaleValues' => ''
        ];

        $totalPlotsInBlock = $this->plantingArrangement->getTotalPlotsInBlock($entryListIdList);

        $expData = $this->experimentData->getExperimentData($id, ['abbrev' => "equals ENTRY_LIST_TIMES_REP"]);
        $expData = is_string($expData[0]['dataValue'])? $expData[0]['dataValue'] : json_encode($expData[0]['dataValue']);
        $isSelectedRemainingReps = substr_count($expData, 'null') <= count($selectedIndex) ? true : false;

        if($blockFlag == null){
            return json_encode(['blockFlag' => 0]);
        }else{
            return $this->renderAjax('_assign_entries_modal', [
                'entryIdsCount' => count($entryIds),
                'totalPlotsInBlock' => $totalPlotsInBlock,
                'entryListIdList' => $entryListIdList,
                'initialLayoutOrderData' => $initialLayoutOrderData,
                'entryIds' => $entryIds,
                'dataProvider' => $dataProvider,
                'variables' => $variables,
                'id' => $id,
                'isSelectedRemainingReps' => $isSelectedRemainingReps
            ]);
        }
    }

    /**
     * Save block parameters
     * @param integer $id Experiment ID
     */
    public function actionSaveBlockParams($id)
    {
        $data = $_POST['params'];
        $entryIds = json_decode($_POST['entryIds'], true);
        $blockIds = json_decode($_POST['blockIds'], true);
        $entryListIdList = $_POST['entryListIdList'];
        $initialLayoutOrderData = $_POST['initialLayoutOrderData'];
        $assignBlockStatus = $_POST['assignBlockStatus'];

        $selectedIds = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_SELECTED_INDEX');
        $this->paWorkingListModel->removeFromWorkingList($id, $selectedIds);

        $this->paWorkingListModel->saveToWorkingListExperimentData($id, [], 'PA_WL_SELECTED_INDEX');    // clear selected in session
        $paWlAllFilteredIndex = $this->paWorkingListModel->getWorkingListExperimentData($id, 'PA_WL_ALL_FILTERED_INDEX');
        $newAllFilteredIndexValue = array_values(array_diff($paWlAllFilteredIndex, $selectedIds));  // remove selected from total
        $this->paWorkingListModel->saveToWorkingListExperimentData($id, $newAllFilteredIndexValue, 'PA_WL_ALL_FILTERED_INDEX');

        //Set to null the selected entries to be added to a block
        Yii::$app->session->set('selectedItems-'.$id, null);
        Yii::$app->session->set('workListDataProvider', null);

        $this->plantingArrangement->saveInitialBlockParams($id, $data, $blockIds, $entryListIdList, $initialLayoutOrderData, $assignBlockStatus);
        $this->plantingArrangement->checkExperimentPAUpdate($id);
    }

    /**
     * Update block parameters
     * @param integer $id Experiment ID
     */
    public function actionUpdateBlockParams($id)
    {
        $data = $_POST['params'];
        $blockId = $_POST['blockId'];

        $experiment = $this->experiment->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experiment.experiment_type AS experimentType',
            'experimentDbId' => 'equals '.$id
        ]);
        $experimentType = $experiment['data'][0]['experimentType'];

        if($experimentType == 'Observation'){
            $abbrevArray = [
                'CHECK_GROUPS',
                'BEGINS_WITH_CHECK_GROUP',
                'ENDS_WITH_CHECK_GROUP',
                'CHECK_GROUP_INTERVAL'
            ];
            
            $abbrevStr = 'equals '.implode('|equals ', $abbrevArray);
            $variablesInfo = $this->variable->searchAll([
                'fields' => 'variable.id AS variableDbId|variable.abbrev',
                'abbrev' => 'equals '.$abbrevStr
            ]);
            $variablesInfo = $variablesInfo['data'];

            $expData = $this->experimentData->getExperimentData($id, ["abbrev" => $abbrevStr]);
            
            $checksUsed = $this->entryOrderModel->getChecksUsed($id);
            //Update entryListIdList for the used checks
            if(!empty($checksUsed)){
                //get entries
                $blockInformation = $this->experimentBlock->searchAll([
                    'fields' => 'experimentBlock.experiment_id as experimentDbId|experimentBlock.id as experimentBlockDbId|experimentBlock.entry_list_id_list AS entryListIdList|experimentBlock.no_of_reps AS noOfReps',
                    'experimentDbId' => "equals ".$id
                ]);
                
                $entryIdData = $blockInformation['data'][0]['entryListIdList'];
                $entryIdData = !is_null($entryIdData) ? $entryIdData : [];
                
                $entryIdsArr = array_column($entryIdData, 'entryDbId');
                $noOfReps = ($blockInformation['data'][0]['noOfReps'] > 0) ? $blockInformation['data'][0]['noOfReps'] : 1;
                foreach($checksUsed as $check){
                    $index = array_search($check, $entryIdsArr);
                    $entryIdData[$index]['repno'] = $noOfReps;
                }
                $updatedData = [
                    "entryListIdList" => json_encode($entryIdData)
                ];
                
                $this->experimentBlock->updateOne($blockInformation['data'][0]['experimentBlockDbId'], $updatedData);
            }
            $existingRec = [];
            if(isset($expData) && !empty($expData)){
                $existingRec = array_column($expData, 'abbrev');
            }

            $dataRequestBodyParam = [];
            $dataRequestBodyParamUpdate = [];
            
            if(isset($_POST['check_groups'])){
                $checkGroups = $_POST['check_groups'];
            } else {
                $checkGroups = [];
            }
            $varIndex = array_search('CHECK_GROUPS', array_column($variablesInfo, 'abbrev'));
            $variableId = $variablesInfo[$varIndex]['variableDbId'];
            if(!in_array('CHECK_GROUPS',$existingRec)){
                //create
                $dataRequestBodyParam[] = [
                    'variableDbId' => "$variableId",
                    'dataValue' => json_encode($checkGroups)
                ];
            } else { //update
                $dataRequestBodyParamUpdate = [
                    'dataValue' => json_encode($checkGroups)
                ];
                $varIndex = array_search($variableId, array_column($expData, 'variableDbId'));
                $exptDbId = $expData[$varIndex]['experimentDataDbId'];
                $this->experimentData->updateExperimentData($exptDbId, $dataRequestBodyParamUpdate);
            }

            if(isset($_POST['check_group_interval'])){
                $checkGroupInterval = $_POST['check_group_interval'];
            } else {
                $checkGroupInterval = 0;
            }
            $varIndex = array_search('CHECK_GROUP_INTERVAL', array_column($variablesInfo, 'abbrev'));
            $variableId = $variablesInfo[$varIndex]['variableDbId'];
            if(!in_array('CHECK_GROUP_INTERVAL',$existingRec)){
                //create
                $dataRequestBodyParam[] = [
                    'variableDbId' => "$variableId",
                    'dataValue' => $checkGroupInterval
                ];
            } else { //update
                $dataRequestBodyParamUpdate = [
                    'dataValue' => $checkGroupInterval
                ];
                $varIndex = array_search($variableId, array_column($expData, 'variableDbId'));
                $exptDbId = $expData[$varIndex]['experimentDataDbId'];
                $dataUpdate = $this->experimentData->updateExperimentData($exptDbId, $dataRequestBodyParamUpdate);
            }

            if(isset($_POST['ends_with_check_group'])){
                $endsCheckGroups = $_POST['ends_with_check_group'];
            } else {
                $endsCheckGroups = [];
            }
            $varIndex = array_search('ENDS_WITH_CHECK_GROUP', array_column($variablesInfo, 'abbrev'));
            $variableId = $variablesInfo[$varIndex]['variableDbId'];
            if(!in_array('ENDS_WITH_CHECK_GROUP',$existingRec)){
                //create
                $dataRequestBodyParam[] = [
                    'variableDbId' => "$variableId",
                    'dataValue' => json_encode($endsCheckGroups)
                ];
            } else { //update
                $dataRequestBodyParamUpdate = [
                    'dataValue' => json_encode($endsCheckGroups)
                ];
                $varIndex = array_search($variableId, array_column($expData, 'variableDbId'));
                $exptDbId = $expData[$varIndex]['experimentDataDbId'];
                $dataUpdate = $this->experimentData->updateExperimentData($exptDbId, $dataRequestBodyParamUpdate);
            }

            if(isset($_POST['begins_with_check_group'])){
                $beginsCheckGroups = $_POST['begins_with_check_group'];
            } else {
                $beginsCheckGroups = [];
            }
            $varIndex = array_search('BEGINS_WITH_CHECK_GROUP', array_column($variablesInfo, 'abbrev'));
            $variableId = $variablesInfo[$varIndex]['variableDbId'];
            if(!in_array('BEGINS_WITH_CHECK_GROUP',$existingRec)){
                //create
                $dataRequestBodyParam[] = [
                    'variableDbId' => "$variableId",
                    'dataValue' => json_encode($beginsCheckGroups)
                ];
            } else { //update
                $dataRequestBodyParamUpdate = [
                    'dataValue' => json_encode($beginsCheckGroups)
                ];
                $varIndex = array_search($variableId, array_column($expData, 'variableDbId'));
                $exptDbId = $expData[$varIndex]['experimentDataDbId'];
                $dataUpdate = $this->experimentData->updateExperimentData($exptDbId, $dataRequestBodyParamUpdate);
            }

            if(!empty($dataRequestBodyParam)){
                $this->experiment->createExperimentData($id,$dataRequestBodyParam);
            }

        }

        $this->plantingArrangement->updateBlockParams($data, $blockId, $id);
        //get entries
        $blockInformation = $this->experimentBlock->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.no_of_ranges AS noOfRanges|
                experimentBlock.no_of_cols AS noOfCols|experimentBlock.layout_order_data AS layoutOrderData',
            'experimentDbId' => 'equals '.$id
        ]);
        $blockData = $blockInformation['data'][0];
        unset($blockInformation);

        $this->plantingArrangement->checkExperimentPAUpdate($id);
        return json_encode([
            'noOfRows' => $blockData['noOfRanges'],
            'noOfCols' => $blockData['noOfCols'],
            'totalPlots' => empty($blockData['layoutOrderData']) ? 0 : count($blockData['layoutOrderData'])
        ]);
    }

    /**
     * Remove a block
     * @param integer $id Experiment ID
     */
    public function actionDeleteBlock($id)
    {
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : 0;

        $this->plantingArrangement->deleteBlock($id, $blockId);
        unset(Yii::$app->session['pa-manage-block-active-tab-' . $id]);
        Yii::$app->session->set('pa-manage-block-active-tab-' . $id, null);

        $this->plantingArrangement->checkExperimentPAUpdate($id);
    }

    /**
     * Remove entries in a block
     * @param integer $id Experiment ID
     */
    public function actionRemoveEntryInBlock($id)
    {
        $entryIds = isset($_POST['entryIds']) ? $_POST['entryIds'] : 0;
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : 0;

        $this->plantingArrangement->removeEntryInBlock($id, $blockId, $entryIds);

        $this->plantingArrangement->checkExperimentPAUpdate($id);
    }

    /**
     * Reorder entries in a block
     * @param integer $id Experiment ID
     */
    public function actionReorderEntriesInBlock($id)
    {
        $entryIds = isset($_POST['entryIds']) ? $_POST['entryIds'] : 0;
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : 0;

        $this->plantingArrangement->reorderEntriesInBlock($id, $blockId, $entryIds);

        $this->plantingArrangement->checkExperimentPAUpdate($id);
    }

    /**
     * Manage plots in a block
     */
    public function actionManagePlotsInBlock()
    {
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : 0;
        $subBlockName = isset($_POST['subBlockName']) ? $_POST['subBlockName'] : '';
        $experimentType = isset($_POST['experimentType']) ? $_POST['experimentType'] : '';
        $varCols = isset($_POST['varCols']) && !empty($_POST['varCols']) ? json_decode($_POST['varCols']) : [];

        return Yii::$app->controller->renderAjax('@app/modules/experimentCreation/views/planting-arrangement/_manage_blocks_plots.php',[
            'subBlockId' => $blockId,
            'subBlockName' => $subBlockName,
            'experimentType' => $experimentType,
            'varCols'=>$varCols
        ]);
    }

    /**
     * Remove plots in a block
     * @param integer $id Experiment ID
     */
    public function actionRemovePlotsInBlock($id)
    {
        $plotNos = isset($_POST['plotNos']) ? $_POST['plotNos'] : 0;
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : 0;

        $this->plantingArrangement->removePlotsInBlock($id, $blockId, $plotNos);

        $this->plantingArrangement->checkExperimentPAUpdate($id);
    }

    /**
     * Reorder plots in a block
     * @param integer $id Experiment ID
     */
    public function actionReorderPlotsInBlock($id)
    {
        $plotNos = isset($_POST['plotNos']) ? $_POST['plotNos'] : 0;
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : 0;

        $this->plantingArrangement->reorderPlotsInBlock($id, $blockId, $plotNos);

        $this->plantingArrangement->checkExperimentPAUpdate($id);
    }

    /**
     * Renders the preview of the block layout
     * @param integer $id Experiment ID
     */
    public function actionPreviewLayoutOfBlock($id)
    {
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : 0;
        $panelWidth = isset($_POST['panelWidth']) ? $_POST['panelWidth'] : 0;

        $activeBlock = Yii::$app->session->get('pa-manage-block-active-tab-' . $id);
        if ($activeBlock == $blockId) {
            $data = $this->plantingArrangement->getBlockLayout($blockId);

            // get starting plot no of a block
            $startingPlotNo = $this->plantingArrangement->getStartingPlotNo($id, $blockId);

            $startingCorner = $this->experimentBlock->searchAll([
                'fields'=>'experimentBlock.id as experimentBlockDbId|experimentBlock.starting_corner AS startingCorner',
                'experimentBlockDbId' => 'equals '.$blockId
            ]);
            $startingCorner = $startingCorner['data'][0]['startingCorner'];

            return $this->renderAjax('_preview_block_layout', [
                'blockId' => $blockId,
                'data' => json_decode($data['notes']),
                'startingPlotNo' => $startingPlotNo,
                'panelWidth' => $panelWidth,
                'noOfRows' => $data['no_of_rows_in_block'],
                'noOfCols' => $data['no_of_cols_in_block'],
                'startingCorner' => $startingCorner,
                'isRefreshLayout' => false
            ]);
        }
    }

    /**
     * Refresh the preview of the block layout
     * @param integer $id Experiment ID
     */
    public function actionRefreshPreviewLayoutOfBlock($id)
    {   
        $values = isset($_POST['values']) ? $_POST['values'] : null;
        $blockId = isset($_POST['blockId']) ? $_POST['blockId'] : null;
        $panelWidth = isset($_POST['panelWidth']) ? $_POST['panelWidth'] : 50;
        $startingCorner = isset($values['starting_corner']) ? $values['starting_corner'] : 'Top Left';

        $startingPlotNo = $this->plantingArrangement->getStartingPlotNo($id, $blockId);

        $dataArr = $this->plantingArrangement->getTempLayoutByBlock($id, $blockId, $values);

        $data = $dataArr['data'];
        $rows = $dataArr['rows'];
        $cols = $dataArr['cols'];

        if(isset($_POST['isEntryOrder'])){
            return $this->renderAjax('_preview_block_layout', [
                'blockId' => $blockId,
                'data' => $data,
                'startingPlotNo' => $startingPlotNo,
                'panelWidth' => $panelWidth,
                'noOfRows' => $rows,
                'noOfCols' => $cols,
                'startingCorner' => $startingCorner,
                'isRefreshLayout' => true,
                'isEntryOrder' => true
            ]);
        } else {
            return $this->renderAjax('_preview_block_layout', [
                'blockId' => $blockId,
                'data' => $data,
                'startingPlotNo' => $startingPlotNo,
                'panelWidth' => $panelWidth,
                'noOfRows' => $rows,
                'noOfCols' => $cols,
                'startingCorner' => $startingCorner,
                'isRefreshLayout' => true
            ]);       
        }
    }

    /**
     * Get the current and active block tab
     * @param integer $id Experiment ID
     */
    public function actionGetActiveBlockTab($id)
    {
        $activeTab = null;
        if (Yii::$app->session->get('pa-manage-block-active-tab-' . $id) !== null) {
            $activeTab = Yii::$app->session->get('pa-manage-block-active-tab-' . $id);
        }

        return $activeTab;
    }

    /**
     * Assign entries to list
     * @param integer $id Experiment ID
     * @return list html data
     */
    public function actionAddEntriesToList($id)
    {
        if (isset($_POST['ids']) && !empty($_POST['ids'])) {
            $ids = json_decode($_POST['ids'], true);
        
            if (!empty($ids)) {
                $idStr = implode(',', $ids);
                $entryIds = "equals " . str_replace(",", "| equals  ", $idStr);

                //get entry list id
                $entryListId = $this->entryList->getEntryListId($id);

                $entryData['data'] = [];
                $rawData = ([
                    'fields'=>'entry.id AS entryDbId|entry.entry_list_id AS entryListDbId|entry.entry_number AS entryNumber|germplasm.designation',
                    'entryListDbId' => "equals $entryListId",
                    'entryDbId' => "$entryIds"
                ]);

                $entryArray = $this->entry->searchAll($rawData, 'sort=entryNumber:asc');

                //get entry replicates info
                $entryReplicates = $this->paEntriesModel->getEntryReplicatesInfo($id, $entryListId);
                $entryData = $this->paEntriesModel->generateWorkingList($id, $entryListId, $entryArray, $entryReplicates);
                
                //update or save working list
                if(!empty($entryData['data'])){
                    $this->paEntriesModel->saveWorkingList($id, $entryData['data']);
                }
                $dataProvider = $this->paWorkingListModel->getWorkingListProvider($entryData);
                Yii::$app->session->set('workListDataProvider',$dataProvider);

                $dataProvider = $this->plantingArrangement->getBlocksByExperimentId($id);
                $blockFlag = count($dataProvider->allModels);
          
                return json_encode(['response'=>true,'blockFlag'=>$blockFlag]);
            }
        }
    }

    /**
     * Remove an entry from the working list
     * @param integer $id Experiment ID
     * @return list html data
     */
    public function actionRemoveWorkingListEntry($id)
    {
        $entryIdsArr = Yii::$app->session['selectedItems-'.$id];    // Retrieves the entry added to the working list
        if (isset($entryIdsArr) && !empty($entryIdsArr)) {
            $selectedArr = isset($_POST['selectedArr'])? json_decode($_POST['selectedArr']): [];

            $entryIdsArr = array_diff($entryIdsArr, $selectedArr);
            $entryIdsArr = array_values($entryIdsArr);
            $newIdStr = implode(',', $entryIdsArr);
            if(!empty($newIdStr)){
                $entryIds = "equals " . str_replace(",", "| equals  ", $newIdStr);
            }else{
                $entryIds = "null";
            }

            //get entry list id
            $entryListId = $this->entryList->getEntryListId($id);

            $rawData = (['entryListDbId' => "equals $entryListId", 'entryDbId' => "$entryIds"]);
            $entryData = $this->entry->searchAll($rawData);

            $dataProvider = $this->paWorkingListModel->getWorkingListProvider($entryData);

            Yii::$app->session->set('workListDataProvider',$dataProvider);
            Yii::$app->session->set('selectedItems-'.$id, $entryIdsArr);

            return json_encode(true);
        }
    }


    /**
     * Reorder an entry from the working list
     * @param integer $id Experiment ID
     * @return list html data
     */
    public function actionReorderWorkingListEntry($id)
    {
        if (isset($_POST['ids']) && !empty($_POST['ids'])) {
            $entryIdsArr = (empty(Yii::$app->session->get('working_list_items'))) ? json_decode($_POST['ids'], true) : Yii::$app->session->get('working_list_items');
            $selectedArr = json_decode($_POST['selectedArr'], true);

            $selectedArr = str_replace('entry-', '', $selectedArr);

            $newArr = array_diff($entryIdsArr, $selectedArr);

            if (!empty($newArr)) {
                $newIdStr = implode(',', $newArr);
                $entryIds = "equals " . str_replace(",", "| equals  ", $newIdStr);

                //get entry list id
                $entryListId = $this->entryList->getEntryListId($id);
                $rawData = (['entryListDbId' => "equals $entryListId", 'entryDbId' => "$entryIds"]);

                $entryData = $this->entry->searchAll($rawData);

                $dataProvider = $this->paWorkingListModel->getWorkingListProvider($entryData);

                $htmlData = $this->renderAjax('entry_working_list', [
                    'dataProvider' => $dataProvider,
                    'workingListFilterModel' => $this->paWorkingListModel,
                    'entryIdsArr' => $newArr,
                    'experimentId' => $id
                ]);

                return json_encode($htmlData);
            }
        }
    }

    /**
     * set the currently selected package data in session.
     * @param integer $id Experiment ID
     * @return array
     */
    public function actionSaveUserSelection($id){
        $selectedItems = json_decode($_POST['selectedItems'],true);
        
        Yii::$app->session->set('selectedItems-'.$id, $selectedItems);
        return json_encode(['success'=> true]);
    }

    /**
     * set the currently selected package data in session to null.
     * @param integer $id Experiment ID
     * @return array
     */
    public function actionEmptyUserSelection($id){
        Yii::$app->session->set('selectedItems-'.$id, null);
        return json_encode(['success'=> true]);
    }

    /**
     * get the currently selected package data in session.
     * @param integer $id Experiment ID
     * @return array
    */
    public function actionGetUserSelection($id){
        return json_encode(Yii::$app->session['selectedItems-'.$id]);
    }

    /**
     * Set selected items in the grid
     */
    public function actionStoreSessionItems(){
        $count = 0;
        $items = [];
        $isAll = false;
        
        if(isset($_POST['unset'], $_POST['entryDbId'], $_POST['storeAll'], $_POST['experimentDbId'])){
            extract($_POST);


            $filteredIdName = "ec"."_filtered_entry_ids_$experimentDbId";
            $selectedIdName = "ec"."_selected_entry_ids_$experimentDbId";     
            $isSelectedAll = "ec"."_is_selected_all_$experimentDbId";
        
            $items = (Yii::$app->session->get($selectedIdName) !== null) ? Yii::$app->session->get($selectedIdName) : [];
            
            if($unset == "true"){
                if($storeAll == "true"){
                    $items = [];
                }
                else{
                    foreach($items as $key => $value){
                        if($value == $entryDbId){
                            unset($items[$key]);
                        }
                    }
                }
            }
            else{
         
                if($storeAll == "true"){
                    $items = Yii::$app->session->get($filteredIdName);
                    $isAll = true;
                }
                else if(!in_array($entryDbId, $items)){     
                    $items[] = $entryDbId;
                }
            }
            
            Yii::$app->session->set($selectedIdName, $items);
            Yii::$app->session->set($isSelectedAll, $isAll);
            
            $count = !empty($items) ? count($items) : 0;
        }

        return json_encode($count);

    }

    /**
     * Retrieve selected items stored in session
     */
    public function actionGetSelectedItems(){

        extract($_POST);


        $selectedIdName = "ec"."_selected_entry_ids_$experimentDbId";
        $selectedItems = (Yii::$app->session->get($selectedIdName) !== null) ? Yii::$app->session->get($selectedIdName) : [];
        $isSelectedAll = (Yii::$app->session->get("ec"."_is_selected_all_$experimentDbId") !== null) ? Yii::$app->session->get("ec"."_is_selected_all_$experimentDbId") : false;
        $response = [
            'selectedItems' => $selectedItems,
            'selectedItemsCount' => count($selectedItems),
            'isSelectedAll' => $isSelectedAll
        ];

        return json_encode($response);
    }

    /**
     * Resets the session filter for assign entries
     * @param integer $id Experiment ID
     */
    public function actionResetSessionFilter($id){
        $filters = [];
        Yii::$app->session->set('pa-entries-filters-' . $id, $filters);
        Yii::$app->session->set("ec"."_is_selected_all_$id", false);

        return json_encode(['success'=> true]);
    }

    /**
     * Checks if there are succeeding experiment status/es which means that
     * the Planting Arrangement tab has been updated
     * @param integer $id experiment identifier
     */
    public function actionCheckPaUpdated($id) {
        // $this->plantingArrangement->checkExperimentPAUpdate($id);
        return true;
    }

    /**
     * Update entry replicate
     * 
     * @param integer $id experiment identifier
     */
    public function actionUpdateEntryRep($id) {
        $response = false;

        if(isset($_POST)){
            extract($_POST);

            $varAbbrev = 'ENTRY_LIST_TIMES_REP';
            $variableInfo = $this->variable->searchAll([
                'fields' => 'variable.id AS variableDbId|variable.abbrev',
                'abbrev' => 'equals '.$varAbbrev
            ]);
            $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
            $expData = $this->experimentData->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);
            
            if(isset($expData[0])){
                $entryReplicates = json_decode($expData[0]['dataValue'],true);

                if(isset($entryDbId)){
                    $entryInformation['selectedItems'][] = $entryDbId;
                }
                
                foreach ($entryInformation['selectedItems'] as $key => $value) {
                    
                    if(isset($entryInformation['selectedItems'][$key])){
                        $entryDbId = $entryInformation['selectedItems'][$key];
                        $entryReplicates[$entryDbId]["repno"] = (int)$varValue;

                        $tempReplicates = [];
                        //update replicates
                        for($i = 1; $i <= (int)$varValue; $i++){
                            if(!isset($entryReplicates[$entryDbId]['replicates'][$i])){
                                $tempReplicates[$i] = null;
                            } else {
                                $tempReplicates[$i] = $entryReplicates[$entryDbId]['replicates'][$i];
                            }
                        }
                        $entryReplicates[$entryDbId]["replicates"] = $tempReplicates;
                    }
                }   
                
                $dataRequestBodyParamUpdate = [
                    'dataValue' => json_encode($entryReplicates)
                ];

                $exptDataDbId = $expData[0]['experimentDataDbId'];
                $dataUpdate = $this->experimentData->updateExperimentData($exptDataDbId, $dataRequestBodyParamUpdate);

                if($dataUpdate['status']){
                    return json_encode(true);
                }
            }
            
        }

        return json_encode(false);
    }

    /**
     * Update entry replicate
     * 
     * @param integer $id experiment identifier
     * @param integer $program program identifier
     */
    public function actionBulkUpdateReplicates($id, $program) {
        if(isset($_POST)){
            extract($_POST);

            $varAbbrev = 'ENTRY_LIST_TIMES_REP';
            $variableInfo = $this->variable->searchAll([
                'fields' => 'variable.id AS variableDbId|variable.abbrev',
                'abbrev' => 'equals '.$varAbbrev
            ]);
            $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
            $expData = $this->experimentData->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);

            if(isset($expData[0])){
                $entryReplicates = json_decode($expData[0]['dataValue'],true);
                
                $dataRequestBodyParamUpdate = [
                    'dataValue' => json_encode($entryReplicates)
                ];

                $exptDataDbId = $expData[0]['experimentDataDbId'];
                $dataUpdate = $this->experimentData->updateExperimentData($exptDataDbId, $dataRequestBodyParamUpdate);

                if($dataUpdate['status']){
                    return json_encode(true);
                }
            }
        }

        return json_encode(false);
    }

    /**
     * Render file upload for planting arrangement
     *
     * @param integer $id record id of the experiment
     * @param string $program name of the current program
     * @param integer $blockId id record of the block
     * @param string $experimentType type of the experiment
     */
    public function actionRenderFileUploadForm($id, $program, $blockId, $experimentType){
        $viewFile = '@app/modules/experimentCreation/views/planting-arrangement/file_upload.php';

        $sampleFileDirectory = realpath(Yii::$app->basePath);

        $htmlData = $this->renderAjax($viewFile,[
            'id' => $id,
            'program' => $program,
            'blockId' => $blockId,
            'experimentType' => $experimentType,
            'sampleFileDirectory' => $sampleFileDirectory
        ]);

        return json_encode($htmlData);
    }
}
