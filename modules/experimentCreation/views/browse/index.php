<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders browser for experiment Creation
 */
use yii\bootstrap\Modal;
use app\components\FavoritesWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

$params = Yii::$app->request->queryParams;
$filteredNotification = '';
$browserInst = "This is the browser for the experiments. You may access your experiments here.";
if (isset($params['ExperimentSearch']['experimentDbId']) || (isset($_GET['id']) && !empty($_GET['id']))) {
    $index = (isset($params['ExperimentSearch']['experimentDbId'])) ?? array_search($params['ExperimentSearch']['experimentDbId'], array_column($dataProvider->getModels(),'experimentDbId'));
    $statusTemp = !empty($dataProvider->getModels()[$index]['experimentStatus']) ? explode(';',$dataProvider->getModels()[$index]['experimentStatus']) : '';

    if(!empty($statusTemp)){
        $maxIndex = count($statusTemp);
        $browserInst = "This is the browser for the experiments. You may access your experiments here.Click the Reset Grid button to display all transactions.";
    }
}
?>

<div class="col col-sm-12 view-entity-content">
    
    <!-- Browse Experiments -->
    <?php
        $browserId = 'dynagrid-ec-experiment-browser';
        // action columns
        $actionColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'header' => false,
                'noWrap' => true,
                'template' => '{copy}{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $data, $key) {
                        $render = Yii::$app->access->renderAccess("EXPERIMENT_CREATION_VIEW","EXPERIMENT_CREATION", $data['creatorDbId']);                   
                        return !$render ? '':Html::a('<i class="material-icons">remove_red_eye</i>',
                            '#',
                            [
                                'class' => 'view-experiment',
                                'title' => Yii::t('app', 'View Experiment'),
                                'data-id' => $data['experimentDbId'],
                                'data-label' => $data['experimentName'],
                                'data-target' => '#experiment-view-modal',
                                'data-toggle' => 'modal'
                            ]
                        );
                    },
                    'update' => function ($url, $data, $key) {

                        if(strpos($data['experimentStatus'], 'in progress') == false && strpos($data['experimentStatus'], 'in queue') == false 
                           && (strpos($data['experimentStatus'], 'failed') !== false || strpos($data['experimentStatus'], 'draft') !== false 
                           || strpos($data['experimentStatus'], 'entry list created') !== false || strpos($data['experimentStatus'], 'occurrences created') !== false 
                           || strpos($data['experimentStatus'], 'protocol specified') !== false || strpos($data['experimentStatus'], 'entry list incomplete seed sources') !== false)){

                            $render = Yii::$app->access->renderAccess("EXPERIMENT_CREATION_UPDATE","EXPERIMENT_CREATION", $data['creatorDbId']);
                            return !$render ? '':Html::a('<i class="material-icons">edit</i>', 
                                '#',
                                [
                                    'class' => 'update-experiment',
                                    'title' => Yii::t('app', 'Update experiment'),
                                    'data-id' => $data['experimentDbId'],
                                    'data-processid' => $data['dataProcessDbId']
                                ]
                            );
                        }
                    },
                    'delete' => function ($url, $data, $key) {
                        if(strpos($data['experimentStatus'], 'in progress') == false && strpos($data['experimentStatus'], 'in queue') == false && strpos($data['experimentStatus'], 'planted') == false 
                           && strpos($data['experimentStatus'], 'design requested') == false  && (strpos($data['experimentStatus'], 'failed') !== false || strpos($data['experimentStatus'], 'draft') !== false 
                           || strpos($data['experimentStatus'], 'entry list created') !== false || strpos($data['experimentStatus'], 'design generated') !== false 
                           || strpos($data['experimentStatus'], 'occurrences created') !== false || strpos($data['experimentStatus'], 'protocol specified') !== false || strpos($data['experimentStatus'], 'entry list incomplete seed sources') !== false)){
                           
                            $render = Yii::$app->access->renderAccess("EXPERIMENT_CREATION_DELETE","EXPERIMENT_CREATION", $data['creatorDbId']);
                            return !$render ? '':Html::a('<i class="material-icons">delete</i>',
                                '#',
                                [
                                    'class'=>'delete-experiment',
                                    'title' => Yii::t('app', 'Delete experiment'),
                                    'data-id' => $data['experimentDbId'],
                                    'data-experiment_name' => $data['experimentName']
                                ]
                            );
                        }
                    },
                    'copy' => function ($url,$data,$key) {
                        if(($data['experimentStatus'] == 'created' || strpos($data['experimentStatus'], 'planted') !== false 
                           || strpos($data['experimentStatus'], 'mapped') !== false) && $data['dataProcessAbbrev'] !== 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){

                            $render = Yii::$app->access->renderAccess("EXPERIMENT_CREATION_COPY_EXPERIMENT","EXPERIMENT_CREATION", $data['creatorDbId']);
                            return !$render ? '':Html::a('<i class="material-icons">content_copy</i>',
                                '#',
                                [
                                    'class'=>'copy-experiment',
                                    'title' => Yii::t('app', 'Copy experiment'),
                                    'data-id' => $data['experimentDbId'],
                                    'data-experiment_name' => $data['experimentName']
                                ]
                            );
                        }
                    }
                ],
                'vAlign'=> 'top',
                'order' => DynaGrid::ORDER_FIX_LEFT,
            ],
        ];

        // all the columns that can be viewed in experiment browser
        $columns = [
            [
                'attribute'=>'experimentStatus',
                'label' => 'Status',
                'format' => 'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $statusOptions,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                        // 'id' => 'select-level-data-browser-id-status'
                    ],
                    'options'=>[
                        'id'=>'select-level-data-browser-id'
                    ]
                ],
                'filterInputOptions' => [
                    'placeholder' => '',
                    // 'id' => 'select-level-data-browser-id'
                ],
                'value' => function($data){
                    return \Yii::$container->get('app\models\Experiment')->formatExperimentStatus($data['experimentStatus']);
                },
                'contentOptions' => [
                    'style'=>'min-width: 150px;'
                ],
            ],
            [
                'attribute' => 'experimentName',
                'label' => 'Experiment',
                'contentOptions' => [
                    'style'=>'min-width: 150px;'
                ],
            ],
            [
                'attribute' => 'experimentType',
                'label' => 'Type',
                'contentOptions' => [
                    'style'=>'min-width: 150px;'
                ],
            ],
            [
                'attribute'=>'occurrenceCount',
                'value' => function ($data) {
                  return number_format($data['occurrenceCount']);
                }
            ],
            [
                'attribute'=>'entryCount',
                'value' => function ($data) {
                    return number_format($data['entryCount']);
                }
            ],
            [
                'attribute' => 'projectName',
                'label' => 'Project',
            ],
            [
                'attribute' => 'seasonName',
                'label' => 'Season',
            ],
            [
                'attribute' => 'stageCode',
                'label' => 'Stage',
            ],
            [
                'attribute' => 'experimentYear',
                'label' => 'Year',
            ],
            [
                'attribute' => 'experimentDesignType',
                'label' => 'Design Type',
            ],
            'creator',
            [
                'attribute'=>'creationTimestamp',
                'format' => 'date',
                'visible' => true,
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => [
                    'autocomplete' => 'off'
                ],
                'filterWidgetOptions' => [
                    'pluginOptions'=> [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'clearBtn' => true,
                        'todayHighlight' => true,
                    ],
                    'pluginEvents' => [
                        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                    ],
                ],
            ],
            [
                'attribute'=>'programCode',
                'visible'=>false,
            ],
            [
                'attribute'=>'experimentCode',
                'visible'=>false,
            ],
            [
                'attribute'=>'programName',
                'label'=>'Program',
                'visible'=>false,
            ],
            [
                'attribute'=>'pipelineName',
                'label'=>'Pipeline',
                'visible'=>false,
            ],
            [
                'attribute'=>'stageName',
                'visible'=>false,
            ],
            [
                'attribute'=>'cropName',
                'label'=>'Crop',
                'visible'=>false,
            ],
            [
                'attribute'=>'plantingSeason',
                'visible'=>false,
            ],
            [
                'attribute'=>'experimentObjective',
                'label'=>'Objective',
                'visible'=>false,
            ],
            [
                'attribute'=>'experimentSubType',
                'label'=>'Sub Type',
                'visible'=>false,
            ],
            [
                'attribute'=>'experimentSubSubType',
                'label'=>'Sub Sub Type',
                'visible'=>false,
            ],
            [
                'attribute'=>'description',
                'visible'=>false,
            ],
            [
                'attribute'=>'steward',
                'visible'=>false,
            ],
        ];

        $gridColumns = array_merge($actionColumns,$columns);
        // export configuration
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'fontAwesome' => true,
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false,
            ],
            'showColumnSelector' => false
        ]);

        $renderUpload = Yii::$app->access->renderAccess("EXPERIMENT_CREATION_CREATE","EXPERIMENT_CREATION"); 

        $createButton = '<a class="light-green darken-3 btn btn-success waves-effect waves-light tooltipped" style="margin-right:5px;" id="create-expt-btn" data-position="top" data-delay="50" data-tooltip="'.Yii::t('app','Create Experiments').'"><b>'.\Yii::t('app','Create').'</b></a>';
        //dynagrid configuration
        $dynagrid = DynaGrid::begin([
            'columns' => $gridColumns,
            'theme'=>'simple-default',
            'showPersonalize'=>true,
            'storage' => DynaGrid::TYPE_SESSION,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'id' => 'grid-experiment-list',
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'showPageSummary'=>false,
                'pjax'=>true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => ['id' =>'grid-experiment-list'],
                    'beforeGrid' => '',
                    'afterGrid' => ''
                ],
                'responsiveWrap'=>false,
                'panel'=>[
                    'heading' => '<h3>'.
                        Yii::t('app', 'Experiments').
                        FavoritesWidget::widget([
                            'module' => 'experimentCreation',
                            'controller' => 'create'
                        ])
                    .'</h3>' .$filteredNotification .'<br>',
                    'before' => \Yii::t('app', $browserInst),
                    'after' => false,
                ],
                'toolbar' => [
                    [
                        'content'=> 
                            !$renderUpload ? "":$createButton." ".
                            Html::a('<i class="material-icons large">notifications_none</i>
                                <span id="notif-badge-id" class=""></span>',
                                '#',
                                [
                                    'id' => 'notif-btn-id',
                                    'class' => 'btn waves-effect waves-light experiment-notification-button tooltipped',
                                    'data-pjax' => 0,
                                    'style' => "margin-right:5px;overflow: visible !important;",
                                    "data-position" => "top",
                                    "data-tooltip" => \Yii::t('app', 'Notifications'),
                                    "data-activates" => "notifications-dropdown"
                                ]).
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['create/index?program='.$program], ['data-pjax'=>true, 'class' => 'btn btn-default', 'id'=>'reset-'.$browserId,'title'=>\Yii::t('app','Reset grid')]).'{dynagridFilter}{dynagridSort}{dynagrid}',
                    ],
                ],
                'floatHeader' =>true,
                'floatOverflowContainer'=> true,
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
            ],                                         
            
            'submitButtonOptions' => [
                'icon' => 'glyphicon glyphicon-ok',
            ],
            'deleteButtonOptions' => [
                'icon' => 'glyphicon glyphicon-remove',
                'label' => Yii::t('app', 'Remove'),
            ],
            'options'=>['id'=>$browserId] // a unique identifier is important
        ]);
        DynaGrid::end();
    ?>    
</div>
<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>
<?php
    Modal::begin([
        'id' => 'experiment-view-modal',
        'header' => '<h4 id="view-experiment-label"></h4>',
        'footer' => Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'size' =>'modal-xl',
        'options' => ['data-backdrop'=>'static']
    ]);

    Modal::end();

    //Copy experiemnt modal
    Modal::begin([
        'id' => 'experiment-copy-modal',
        'header' => '<h4 id="copy-experiment-label"></h4>',
        'footer' =>
            Html::a(\Yii::t('app','Cancel'),['#'],['data-dismiss'=>'modal']).'&nbsp;&nbsp'.
            Html::a(\Yii::t('app','Preview'),['#'],['class'=>'btn btn-primary waves-effect waves-light copy-experiment-btn','url'=>'#','id'=>'copy-btn']).'&nbsp;&nbsp',
        'options' => ['data-backdrop'=>'static']
    ]);

    Modal::end();

    //delete experiment modal
    Modal::begin([
        'id' => 'experiment-delete-modal',
        'header' => '<h4 id="delete-experiment-label"></h4>',
        'footer' =>
            // Temporarily remove confirmation button
            Html::a(\Yii::t('app','Cancel'),['#'],['data-dismiss'=>'modal']).'&nbsp;&nbsp'.
            Html::a(\Yii::t('app','Confirm'),['#'],['class'=>'btn btn-primary waves-effect waves-light delete-experiment-btn','url'=>'#','id'=>'delete-btn']).'&nbsp;&nbsp',
        'options' => ['data-backdrop'=>'static']
    ]);
?>

<?= \Yii::t('app', '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete <span id="experiment-delete"></span>. This action will delete all records within the experiment. Click confirm to proceed.' ) ?>
<?= '<div class = "row"><div id="mod-progress" class="hidden"></div><div id="mod-notif" class="hidden"></div></div>';?> 

<?php Modal::end(); 

//show existing experiment data process
Modal::begin([
    'id' => 'experiment-data-process-modal',
    'header' => '<h4><i class="fa fa-check-square-o"></i> '.\Yii::t('app','Select Tool').'</h4>',
    'footer' => Html::a(\Yii::t('app','Close'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static']
]);

Modal::end();
?>

<?php
$experimentId = isset($_GET['id']) && !empty($_GET['id']) ? $_GET['id'] : null;
$nextUrl = Url::to(['create/redirect-current-step', 'program'=>$program]);
$downloadUrl = Url::to(['create/download-experiment']);
$viewUrl = Url::to(['/experimentCreation/default/view-info']);
$deleteUrl = Url::to(['/experimentCreation/create/delete-experiment','program'=>$program]);
$defaultProcessUrl = Url::to(['default/index', 'program'=>$program]);

$resetGridUrl = Url::toRoute(['/experimentCreation/create/index', 'program' => $program]);
$newNotificationsUrl = Url::toRoute(['/experimentCreation/browse/new-notifications-count']);
$notificationsUrl = Url::toRoute(['/experimentCreation/browse/push-notifications']);
$getTransactionStatusUrl = Url::toRoute(['/experimentCreation/browse/get-transaction-status']);
$copyUrl = Url::to(['default/get-experiment-steps','program'=>$program]);
$previewExperiment =  Url::to(['copy/preview-experiment', 'program'=>$program]);
$errorUrl =  Url::to(['/dashboard', 'program'=>$program]);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS

    var resetGridUrl = "$resetGridUrl";
    var newNotificationsUrl='$newNotificationsUrl';
    var notificationsUrl='$notificationsUrl';
    var getTransactionStatusUrl = '$getTransactionStatusUrl';
    var experimentId = "$experimentId";
    var selectedExperimentId = '';
    var experimentToCopy = '';
    var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';

    notifDropdown();
    renderNotifications();
    updateNotif();

    notificationInterval=setInterval(updateNotif, 15000);
   
    $(document).on('ready pjax:success', function(e) {
        e.preventDefault();
        refresh();
        $('#notif-btn-id').tooltip();
        notifDropdown();
        $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>');
        renderNotifications();
        updateNotif();
    });

    refresh();

    //reload the grid view
    if(experimentId !== '' && experimentId !== null){
        $.pjax.reload({
            container: '#$browserId-pjax',
            replace: false,
            url: resetGridUrl+"&id="+experimentId
        });
    }
  
    $(document).on('click','#create-expt-btn', function() {
        window.location = '$defaultProcessUrl';
    });

    $(document).on('click','.update-experiment', function() {
        
        window.location = '$nextUrl'+'&id='+$(this).data('id')+'&processId='+$(this).data('processid');
    });

    $(document).on('click','.view-experiment', function() {
        var viewModal = '#experiment-view-modal > .modal-dialog > .modal-content > .modal-body';
        var experimentId = $(this).data('id');
        var label = $(this).data('label');

        $('#view-experiment-label').html('<i class="fa fa-info-circle"></i> ' + label);

        $.ajax({
            url: '$viewUrl',
            data:{
                id : experimentId
            },
            type: 'post',
            async: false,
            success: function(response) {
                $(viewModal).html(response);

            },
            error: function() {
                $('#view-experiment-widget-modal-body').html('<i>There was a problem while loading record information.</i>');
            }
        });
    });

    // delete experiment
    $(document).on('click','.delete-experiment', function() {
        var obj = $(this);
        var experimentName = obj.data('experiment_name');
        var experimentId = obj.data('id');
        var deleteModal = '#experiment-delete-modal > .modal-dialog > .modal-content > .modal-body';
        selectedExperimentId = experimentId;

        $('#delete-experiment-label').html('<i class="fa fa-trash"></i> Delete ' + experimentName);
        $('#experiment-delete').html(experimentName);
  
        $('#experiment-delete-modal').modal('show');
    });

    $(document).on('click', '.delete-experiment-btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var url = '$deleteUrl';

        //Send to background job indicator
        $('#mod-progress').removeClass('hidden');
        var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
        $('#mod-progress').html(loading);
        $('#mod-notif').removeClass('hidden');
        $('#mod-notif').html('<div class="alert ">'+ 
                            '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                '<i class="fa fa-info-circle"></i>'+
                                    ' A background job is being created.' +
                            '</div></div>');

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                id: selectedExperimentId
            },
            async: false,
            success: function(response) {

            }
        });
    });
    //end delete experiment

    //Render Modal for steps to copy in an experiment
    $(document).on('click','.copy-experiment', function(e){
        var obj = $(this);
        var experimentName = obj.data('experiment_name');
        var experimentDbId = obj.data('id');
        experimentToCopy = experimentDbId;
        var copyModal = '#experiment-copy-modal > .modal-dialog > .modal-content > .modal-body';
        var url = '$copyUrl';
        
        //Get the experiment steps to copy
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                id: experimentDbId
            },
            async: false,
            success: function(response) {
                $('#copy-experiment-label').html('<i class="fa fa-copy"></i> Copy ' + experimentName);
                $(copyModal).html(response);
        
                $('#experiment-copy-modal').modal('show');
            }
        });
    });

    //Set the preview of the copy experiment
    $(document).on('click','.copy-experiment-btn', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        $('.copy-experiment-btn').attr('disabled', 'disabled');
        //list of experiment steps
        //i.e.["specify-basic-info", "specify-protocols"]
        var exptSteps = [];
        var experimentDbId = '';
        $.each($("[class^=experiment-steps"), function(){            
            var checkboxName = $(this).attr('name');
            experimentDbId = $(this).data('id');
            if($(this).prop('checked')){
                exptSteps.push(checkboxName);
            }
        });
        var update_modal = '#experiment-copy-modal > .modal-dialog > .modal-content > .modal-body';
            
        $(update_modal).html(loadingIndicator);
        //Get the experiment steps to copy
        $.ajax({
            url: '$previewExperiment'+'&id='+experimentDbId,
            type: 'post',
            dataType: 'json',
            async:false,
            data: {
                copyInformation:exptSteps
            },
            success: function() {
                $('#experiment-copy-modal').modal('hide');
            },
            error: function() {
            }
        });
    });

    function refresh(){
        var maxHeight = ($(window).height() - 240);

        $(".kv-grid-wrapper").css("height",maxHeight);
    }

    /**
     * Initialize dropdown materialize  for notifications
     */
    function renderNotifications(){
        $('.experiment-notification-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            belowOrigin: true,
            alignment: 'right',
            stopPropagation: true
        });
    }
    /**
     * Shows new notifications
     */
    function updateNotif() {
        $.getJSON(newNotificationsUrl, 
        function(json){
            $('#notif-badge-id').html(json);
            if(parseInt(json)>0){
                $('#notif-badge-id').addClass('notification-badge red accent-2');
            }else{
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
            }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log("Message: "+textStatus+" : "+errorThrown);
            
        });
    }

    /**
    * Displays the notification in dropdown UI
    */
    function notifDropdown(){
        $('.experiment-notification-button').on('click',function(e){
            $.ajax({
                url: notificationsUrl,
                type: 'get',
                async:false,
                success: function(data) {
                    renderNotifications();
                    $('#notif-badge-id').html('');
                    $('#notif-badge-id').removeClass('notification-badge red accent-2');
                    $('#notifications-dropdown').html(data);

                    $('.bg_notif-btn').on('click', function(e){
                        e.preventDefault()

                        var obj = $(this);
                        var id = obj.data('transaction_id');
                        var abbrev = obj.data('abbrev');
                        var workerName = obj.data('worker-name');
                        var entity = obj.data('entity');
                        var description = obj.data('description');
                        var filename = obj.data('filename');

                        if(workerName == 'buildcsvdata' || workerName == 'buildjsondata') {
                            // Create temporary entry point to downloading CSV data
                            var a = document.createElement('a');
                            var dataFormat = workerName.includes('csv') ? 'csv' : 'json';

                            a.setAttribute('hidden', '');

                            var message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> File is not yet ready for download.';

                            if(($(this)[0].innerHTML).includes('successful')){
                                a.setAttribute('href', `/index.php/occurrence/default/download-data?filename=\${filename}&dataFormat=\${dataFormat}`);
                                message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.';
                            }
                            
                            // Temporarily insert element to document
                            document.body.appendChild(a);

                            // Trigger download
                            a.click();

                            // Remove element
                            document.body.removeChild(a);
                            
                            $('.toast').css('display','none');
                            Materialize.toast(message, 5000);

                            $('#system-loading-indicator').html('')
                        } else {
                            var obj = $(this);
                            var transaction_id = obj.data('transaction_id');
                            $.pjax.reload({
                                container: '#$browserId-pjax',
                                replace: false,
                                url: resetGridUrl+"&id="+transaction_id
                            });
                        }
                    });
                },
                error: function(xhr, status, error) {
                    var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                        '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                        '<i class="material-icons red toast-close-btn">close</i></button>' +
                        '<ul class="collapsible toast-error grey lighten-2">' +
                        '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                        '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                        '</li> </ul>');
                    Materialize.toast(toastContent, 'x', 'red');
                    $('.toast-error').collapsible();
                }
            });
        });
    }

    $(document).ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
    });
JS
);

/**
 * View file for experiment creation browser
 */
Yii::$app->view->registerCss('
    .notification-badge {
        font-family: "Poppins", sans-serif;
        position: relative;
        right: 0px;
        top: -20px;
        color: #ffffff;
        background-color: #3f51b5;
        margin: 0 -.8em;
        border-radius: 50%;
        padding: 2px 5px;
    }

    #notifications-dropdown h5 {
        font-size: 1rem;
        text-transform: capitalize;
        font-weight: 500;
    }

    #notifications-dropdown li {
        padding: 8px 16px;
        font-size: 1rem;
    }

    #notifications-dropdown li > a {
        padding: 0;
        font-size: 1.1rem;
        font-weight: 300;
    }

    #notifications-dropdown li > a > span {
        display: inline-block;
        font-size: 1.2rem;
        position: relative;
        top: 4px;
        margin-right: 5px;
    }

    #notifications-dropdown li > time {
        font-size: 0.8rem;
        font-weight: 400;
        margin-left: 38px;
    }

    #notifications-dropdown li.divider {
        padding: 0;
    }
    .small {
        font-size: 1.0rem !important;
    }
    .icon-bg-circle {
        color: #fff;
        padding: .4rem;
        border-radius: 50%;
    }
    h6 {
        font-size: 1rem;
        line-height: 110%;
        margin: 0.5rem 0 0.4rem 0;
    }
    .round {
        display: inline-block;
        height: 30px;
        width: 30px;
        line-height: 30px;
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #222;    
        color: #FFF;
        text-align: center;  
    }
    body{
        overflow-x: hidden;
    }
');
?>
