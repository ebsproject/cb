<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders browser for experiment Creation
 */
use ChromePhp;
use yii\bootstrap\Modal;
?>
<h3>
    <a class="waves-effect waves-light btn pull-left tooltipped" style="margin-right:13px;" id="create-expt-btn" data-position="top" data-delay="50" data-tooltip="<?= \Yii::t('app','Create Experiment') ?>"><i class="material-icons pull-right">add</i>&nbsp;<span><?= \Yii::t('app','Create') ?></span></a>
</h3>
<?php
Modal::begin([
    'id' => 'generic-modal',
]);
Modal::end();
?>