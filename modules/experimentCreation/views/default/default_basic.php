<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders browser for experiment Creation
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use app\models\Program;
use yii\helpers\Url;

	$disabled = '';

    echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
        'processId' => null,
        'model'=>$model,
        'program'=>$program,
        'saveBtnLabel'=>'Next',
        'nextBtn' => '',
        'btnName' => 'next-btn',
        'disabled' => 'disabled'
    ]);

    echo '<div class="col col-sm-12 view-entity-content"><div class="main-panel">';
    
    $form = ActiveForm::begin([
      'enableClientValidation'=>false,
      'id'=>'default-experiment-form'
    ]);
?>

      <div class="col-md-6 s6 parent-box">
        <div id="basic-info1">
          <?php
              $countFields = count($requiredFields);

              for($i = 0; $i < $countFields; $i++){
                echo $requiredFields[$i]['value'];
              }
          ?>
          <div class="clearfix"></div>
          <div id="expt_type"></div>
          <div id="info-container" class="row col-md-12">
             <div class="control-group col-sm-1" style="float: right; margin-top: -45px;" id="temp">
                
             </div>
          </div>
          <div class="clearfix"></div>
        </div> <!--End of basic-info1-->
        
    </div>  <!--End of the 1st parent box-->
  <!-- </div> End of Card- -->
  <?php ActiveForm::end(); ?>
</div></div> <!--End of view entity content--> 
 


<?php
     //Modal for the template description
  Modal::begin([
    'id'=> 'template-modal',
    'header' => '<h4><i class="fa fa-list"></i>  '.\Yii::t('app','Experiment Templates').'</h4>',
    'footer' => Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
  ]);
  echo '<p id="instructions">Below is the list of all experiment templates.</p>';
  echo '<div id="template-panel" style="margin-left:30px;"></div>';

  Modal::end();
 ?>

<?php
  $renderBasicInfoUrl = Url::to(["create/specify-basic-info", "program"=>$program]);
  $getExptTypeProcessPathUrl = Url::to(["default/get-expt-type-process-path"]);
  
$this->registerJs(<<<JS
    var type = '';
    var variableArr = [];
    var template = '';
    $("document").ready(function(){
    
        $('#next-btn').attr('disabled','disabled');

    })

    $(document).on('change','.select2-input-validate', function(){
        var id = $(this).attr('id');
        var dataLabel = $(this).attr('data-mem');
        var type = $('#' + id).val();

        if((typeof type != "undefined") && type != null && type != "" && dataLabel == 'EXPERIMENT_TYPE'){
            $.ajax({
                url: '$getExptTypeProcessPathUrl'+'?type='+type,
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function(response){
                    window.location = '$renderBasicInfoUrl'+'&processId='+response.processId;
                },
                error: function(response){
                    $('#next-btn').attr('disabled','disabled');
                }
            });
        }
    });   
JS
);
?>
<style>
.required{
    color:red;
}
label{
    font-size: .8rem;
    color: #333333;
  }
.readonly{
    cursor: not-allowed;
    user-select: none;
    -webkit-user-modify: read-only;
    pointer-events: none;
    border: 1px solid grey;
    background-color: lightgrey;
}
.div-class{
    margin-bottom: 10px;
}
.my-container{
    min-width: 50%;
    max-width: 90%
}
.card{
    height: 100%;
    margin-left:-10px;
    margin-top:-10px;
    padding: 30px;
}
.form-inline input{
    flex-direction: column;
    align-items: stretch;
    vertical-align: middle;
}
.form-inline { 
    display: flex;
    flex-flow: row wrap;    
    align-items: center;
}
.view-entity-content{
    background: #ffffff;
}
#basic-info1{
    margin-left:auto;
    margin-right:auto;
    margin-top:15px;
    margin-bottom: 10px;
    display: block;
}
#basic-info2{
    margin-left:auto;
    margin-right:auto;
    margin-top:20px;
    display: block;
}
.parent-box{
    position: relative;
    margin-top: 1rem;
}
.main-panel{
    margin-left: 5%;
}
.control-group{
  margin-bottom: inherit;
}
.select2 {
  width:100%!important;
} 
</style>