<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders browser for experiment template description
 */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;

$columns = [
    [
        'attribute' => 'template',
        'format'=> 'raw',
        'value' => function($data){
            return !empty($data['template']) ? $data['template'] :  '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' =>'type',
        'format' => 'raw',
        'value' => function($data){
            return !empty($data['type']) ? $data['type'] :  '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' =>'description',
        'format' => 'raw',
        'value' => function($data){
            return !empty($data['description']) ? $data['description'] :  '<span class="not-set">(not set)</span>';
        }
    ]
];

DynaGrid::begin([
    'options' => ['id'=>'template-desc'],
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage'=>'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'showPageSummary' => false,
        'pjax' => false,
        'responsiveWrap' => false,
        'panel' => [
            'heading' => false,
            'before' => false,
            'after' => false,
        ],
        'toolbar' => false,
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'resizableColumns'=>true,
    ]
]);
DynaGrid::end();
?>