<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders the modal for copy experiment
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;

$designNote = '';
if($design === "false"){
    $designNote = '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i>'.\Yii::t('app', 'This experiment is generated outside EBS. There is no design record found for this experiment.');
}
?>

<p><?= \Yii::t('app', 'Check the boxes below to copy from the selected experiment.') ?></p>
<p><span><?php echo $designNote;?></span></p>
<?php 
   foreach($data as $val){
       $newClass = '';

       if($val['hasDependency']){
          $newClass = 'has-dependency';
       }
       if($val['default']){
            echo '<input
                type="checkbox"
                data-id="'.$id.'"
                id= "'.$val['actionId'].'-chkbx"
                class="experiment-steps '.$newClass.' '.$val['dataMem'].' filled-in"
                name="'.$val['actionId'].'"
                style="opacity:0"
                checked = '.$val['default'].',
                disabled = disabled,
                data-mem = '.$val['dataMem'].'
            />'.
            '<label for="'.$val['actionId'].'-chkbx">'.\Yii::t('app',$val['displayName']).'</label>&emsp;&nbsp;<br/>';
       }else{
            echo '<input
                type="checkbox"
                data-id="'.$id.'"
                id= "'.$val['actionId'].'-chkbx"
                class="experiment-steps '.$newClass.' '.$val['dataMem'].' filled-in"
                name="'.$val['actionId'].'"
                style="opacity:0"
                data-dependency = '.$val['hasDependency'].',
                data-mem = '.$val['dataMem'].'
            />'.
            '<label for="'.$val['actionId'].'-chkbx">'.\Yii::t('app',$val['displayName']).'</label>&emsp;&nbsp;<br/>';
       }
   
   }


?>
 <script>
    $(document).ready(function(){

        if('<?=$design;?>' === "false"){
            $('.design').attr('disabled','disabled');
            $('.planting_arrangement').attr('disabled','disabled');
            $('.site').attr('disabled','disabled');

            //Check parent list
            $('.parent_list').attr('checked','checked');
            $('.parent_list').prop('checked',true);

            //Check entry list
            $('.entry_list').attr('checked','checked');
            $('.entry_list').prop('checked',true);
        }
    });
    $(document).on('click','.has-dependency',function(e){
        var dataMem = $(this).attr('data-mem');
        
        if(dataMem == 'site'){
            if($(this).prop("checked") === true)  {
                $(this).attr('checked','checked');
                //Check parent list
                $('.parent_list').attr('checked','checked');
                $('.parent_list').prop('checked',true);

                //Check entry list
                $('.entry_list').attr('checked','checked');
                $('.entry_list').prop('checked',true);

                //Check design
                $('.design').attr('checked','checked');
                $('.design').prop('checked',true);

                //Check crosses
                $('.crosses').attr('checked','checked');
                $('.crosses').prop('checked',true);

                //Check planting arrangement
                $('.planting_arrangement').attr('checked','checked');
                $('.planting_arrangement').prop('checked',true);
            }else{
                $(this).removeAttr('checked');
                //Check parent list
                $('.parent_list').attr('checked',false);
                $('.parent_list').prop('checked',false);
                $('.parent_list').removeAttr('checked');

                //Check entry list
                $('.entry_list').attr('checked',false);
                $('.entry_list').prop('checked',false);
                $('.entry_list').removeAttr('checked');

                //Check design
                $('.design').attr('checked',false);
                $('.design').prop('checked',false);
                $('.design').removeAttr('checked');

                //Check crosses
                $('.crosses').attr('checked',false);
                $('.crosses').prop('checked',false);
                $('.crosses').removeAttr('checked');

                //Check planting arrangement
                $('.planting_arrangement').attr('checked',false);
                $('.planting_arrangement').prop('checked',false);
                $('.planting_arrangement').removeAttr('checked');
            }
        }
          
        if(dataMem == 'design' || dataMem == 'planting_arrangement'){
            if($(this).prop("checked") === true)  {
                $(this).attr('checked','checked');

                //Check design
                $('.design').attr('checked','checked');
                $('.design').prop('checked',true);

                //Check planting arrangement
                $('.planting_arrangement').attr('checked','checked');
                $('.planting_arrangement').prop('checked',true);

                //Check parent list
                $('.parent_list').attr('checked','checked');
                $('.parent_list').prop('checked',true);

                //Check entry list
                $('.entry_list').attr('checked','checked');
                $('.entry_list').prop('checked',true);


                //Check crosses
                $('.crosses').attr('checked','checked');
                $('.crosses').prop('checked',true);
            }else{
                $(this).removeAttr('checked');
              
                //Check parent list
                $('.parent_list').attr('checked',false);
                $('.parent_list').prop('checked',false);
                $('.parent_list').removeAttr('checked');

                //Check entry list
                $('.entry_list').attr('checked',false);
                $('.entry_list').prop('checked',false);
                $('.entry_list').removeAttr('checked');

                //Check Site 
                $('.site').attr('checked',false);
                $('.site').prop('checked',false);
                $('.site').removeAttr('checked');

                //Check crosses
                $('.crosses').attr('checked',false);
                $('.crosses').prop('checked',false);
                $('.crosses').removeAttr('checked');
            }
        }
        
        if(dataMem == 'crosses'){
            if($(this).prop("checked") === true)  {
                $(this).attr('checked','checked');
                //Check parent list
                $('.parent_list').attr('checked','checked');
                $('.parent_list').prop('checked',true);
            }else{
                $(this).removeAttr('checked');
                //Check parent list
                $('.parent_list').attr('checked',false);
                $('.parent_list').prop('checked',false);
                $('.parent_list').removeAttr('checked');

                //Check Site 
                $('.site').attr('checked',false);
                $('.site').prop('checked',false);
                $('.site').removeAttr('checked');
            }
        }

        if(dataMem == 'entry_list' || dataMem == 'parent_list'){
            if($(this).prop("checked") === true)  {
                $(this).attr('checked','checked');
            
                //Check parent list
                $('.parent_list').attr('checked','checked');
                $('.parent_list').prop('checked',true);

                //Check entry list
                $('.entry_list').attr('checked','checked');
                $('.entry_list').prop('checked',true);
            }else{
            
                $(this).removeAttr('checked');
                //Check parent list
                $('.parent_list').attr('checked',false);
                $('.parent_list').prop('checked',false);
                $('.parent_list').removeAttr('checked');

                //Check entry list
                $('.entry_list').attr('checked',false);
                $('.entry_list').prop('checked',false);
                $('.entry_list').removeAttr('checked');

                //Check Site 
                $('.site').attr('checked',false);
                $('.site').prop('checked',false);
                $('.site').removeAttr('checked');

                //Check Design 
                $('.design').attr('checked',false);
                $('.design').prop('checked',false);
                $('.design').removeAttr('checked');

                //Check planting arrangement
                $('.planting_arrangement').attr('checked',false);
                $('.planting_arrangement').prop('checked',false);
                $('.planting_arrangement').removeAttr('checked');
            }
        }
      });
 </script>
<style>
[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {
    position: relative;
    padding-left: 15px;
    cursor: pointer;
    display: inline-block;
    height: 15px;
    line-height: 35px;
    font-size: 0.5rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
label{
    font-family: 'Roboto Medium', sans-serif;
    font-size: 16px;
    color: #000;
    font-weight: 500;
}
</style>