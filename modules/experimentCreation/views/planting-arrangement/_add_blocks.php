<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting arrangement add blocks
*/
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\builder\TabularForm;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

unset(Yii::$app->session['pa-add-blocks-reload-'.$id]);
Pjax::begin(['id'=>'pa-add-blocks-id-pjax','timeout' => false, 'enablePushState' => false]);
  
  echo TabularForm::widget([
    'dataProvider'=>$dataProvider,
    'formName'=>'PAAddBlock',
    'attributeDefaults'=>[
          'type'=>TabularForm::INPUT_TEXT,
      ],
      'actionColumn' => [
      'class' => '\kartik\grid\ActionColumn',
      'template' => '{delete}',
      'header'=>'',
      'buttons' => [
        'delete' => function ($url,$model) {
          $styleCss = '';
          if($model['orderNumber'] == 1 || \Yii::$container->get('app\modules\experimentCreation\models\PlantingArrangement')->blockHasAssignReplicates($model['experimentBlockDbId'])){
            $styleCss = 'display:none';
          }
          return Html::a(
            '<i class="material-icons">delete</i>',
            false, 
            [
              'class'=>'pa-remove-block',
              'title' => 'Remove row',
              'style'=>"cursor:pointer;".$styleCss,
              'data-id'=>$model['experimentBlockDbId']
            ]
          );
        }
      ],
    ],
    'serialColumn' => [
      'class' => '\kartik\grid\SerialColumn',
      'header'=>'',
    ],
      'attributes'=>[
          'id' => [
          'label'=>'ID',
          'type'=>TabularForm::INPUT_HIDDEN_STATIC,
          'visible'=>false
      ],
      'experimentBlockCode' => [
        'label' => 'Block code',
        'options' => [
          'disabled' => 'disabled'
        ]
      ],
      'experimentBlockName' => [
          'label'=>'Block Name',
          'options' => [
          'disabled' => 'disabled'
        ]
      ],
      'noOfBlocks' => [
          'label'=>'No. of Sub-blocks',
          'type'=>TabularForm::INPUT_TEXT,
          'options'=> function($model, $key, $index, $widget) {
                  return
                  [ 
                      'type'=>'number',
                      'min'=>$model['noOfBlocks'],
                      'max'=>50,
                      'class'=>'pa-no-of-blocks',
                      'data-id'=>$model['experimentBlockDbId'],
                      'data-saved'=>$model['noOfBlocks']
                  ];
              },
      ],
      'blockType' => [
        'type'=>TabularForm::INPUT_DROPDOWN_LIST,
        'items'=>['Parent block','Cross block','Sub-block'],
        'options' => [
          'disabled' => 'disabled'
        ]
      ]
      ],
      'gridSettings'=>[
        'showPageSummary'=>false,
        'summary'=>'',
        'panel'=>[
          'heading'=>false,
          'before'=>\Yii::t('app',"Add Nursery blocks and sub-blocks."),
          'beforeOptions'=>['class'=>'pa-add-blocks-before-heading'],
          'footer'=>false,
          'floatHeader'=>true,
          'floatOverflowContainer' => true,
          'after'=> 
            '<div class="preloader-wrapper small active hidden" style="width:20px;height:20px;">
              <div class="spinner-layer spinner-green-only">
                <div class="circle-clipper left">
                  <div class="circle"></div>
                </div>
                <div class="gap-patch">
                  <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                  <div class="circle"></div>
                </div>
              </div>
            </div>&nbsp; Add '.
            Html::input('number', 'pa-addl-blocks', 1, ['max'=>10, 'min'=>1, 'class' => 'add-blocks','id'=>'pa-addl-blocks-input','style'=>'width:40px']). \Yii::t('app'," more Nursery blocks").'&nbsp;'.
            Html::a('Add','#',['style'=>'margin-right:5px;','type'=>'button', 'title'=>\Yii::t('app',"Add Nursery blocks"), 'class'=>'tabular-btn btn black-text grey lighten-2 waves-effect waves-light','id'=>'pa-add-rows-btn']).
            Html::a('Continue','#',['style'=>'margin-left:5px;','type'=>'button', 'title'=>\Yii::t('app',"Continue to Assign Entries"), 'class'=>'tabular-btn btn pa-add-blocks-continue-btn waves-effect waves-light'])
        ]
      ]
  ]);

Pjax::end();

// delete value modal confirmation
Modal::begin([
    'id' => 'delete-blocks-confirmation-modal',
    'header' => '<h4><i class="fa fa-trash"></i> Delete value</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#',
            [
                'data-dismiss'=>'modal',
                'id' => 'cancel-delete-form-value-btn',
                'title' => 'Cancel deletion'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm').'<i class="material-icons right">send</i>',
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light delete-form-value-btn',
                'id'=>'confirm-delete-blocks-btn',
                'data-dismiss'=>'modal',
                'title' => 'Confirm deletion of value'
            ]
        ) .
        '&nbsp;&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<div id="delete-block-progress" class="progress hidden"><div class="indeterminate"></div></div>';
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete <b id="blockCount">1</b> empty block/s. This action cannot be undone. Click confirm to proceed.';

Modal::end();

$addNewRows = Url::to(['/experimentCreation/planting-arrangement/add-new-rows','id'=>$id]);
$addNewRowsUpdatedUrl = Url::to(['/experimentCreation/planting-arrangement/add-block','id'=>$id,'program'=>$program,'processId'=>$processId,'u'=>true]);
$removeRow = Url::to(['/experimentCreation/planting-arrangement/remove-row','id'=>$id]);
$saveNurseryBlocks = Url::to(['/experimentCreation/planting-arrangement/save-blocks','id'=>$id]);
$addNewRowsSaved = Url::to(['/experimentCreation/planting-arrangement/add-block','id'=>$id,'s'=>true,'program'=>$program,'processId'=>$processId]);
$addSubBlock = Url::to(['/experimentCreation/planting-arrangement/add-sub-block-single','id'=>$id]);
$addBlock = Url::to(['/experimentCreation/planting-arrangement/add-block','id'=>$id,'program'=>$program,'processId'=>$processId]);
$assignParents = Url::to(['/experimentCreation/planting-arrangement/assign-parents','id'=>$id,'program'=>$program,'processId'=>$processId]);
$searchParents = Url::to(['/experimentCreation/planting-arrangement/assigns-parents','id'=>$id,'program'=>$program,'processId'=>$processId]);

$js=<<<JS

refresh();

// reload grid on pjax
$(document).on('ready pjax:success', function(e) {
    e.stopPropagation();
    e.preventDefault();
    refresh();
    $('#change-design-switch-id').removeAttr('disabled')
});


$(document).ready(function() {
    $('#change-design-switch-id').removeAttr('disabled')
});
var error = false;
var ifHasError = [];

function refresh(){
  // add more rows
  $(document).on('click','#pa-add-rows-btn',function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    $('#pa-add-rows-btn').addClass('disabled');

    // get number of rows
    var newRowsCount = $('#pa-addl-blocks-input').val();
    $('.red-text').remove();
    if(newRowsCount > 10){
      $('#pa-addl-blocks-input').addClass('invalid');
      $('#pa-addl-blocks-input').focus();
      $('#pa-addl-blocks-input').after("<span class='red-text'>Value must be <= 10</span>");
    }else if(newRowsCount < 1){
      $('#pa-addl-blocks-input').addClass('invalid');
      $('#pa-addl-blocks-input').focus();
      $('#pa-addl-blocks-input').after("<span class='red-text'>Value must be at least 1</span>");
    }else{
      $('.preloader-wrapper').removeClass('hidden');
      $('.red-text').remove();
      $('#pa-addl-blocks-input').removeClass('invalid');
      $('#pa-add-rows-btn, .pa-add-blocks-continue-btn').addClass('disabled');
      
      $.ajax({
        url: '$addNewRows',
        type: 'POST',
        async: false,
        data: {
          rows: newRowsCount
        },
        success: function(data){
          $.pjax.reload({
            container: '#pa-add-blocks-id-pjax',
            url: "$addNewRowsUpdatedUrl",
            replace: false
          });
          $('#pa-add-rows-btn').removeClass('disabled');
        }
      });
    }

    return false;
  });

  // prevent reloading page on pjax
  $('#pa-add-blocks-id-pjax').on('pjax:error', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var newRowsCount = $('#pa-addl-blocks-input').val();
    $.ajax({
      url: '$addNewRows',
      type: 'POST',
      async: false,
      data: {
        rows: newRowsCount,
        reload: 'true'
      },
      success: function(data){
        // reload box
        $('.add-blocks-refresh-btn').click();
      }
    });	
    return false;	
  });

  // remove row
  $(document).on('click','.pa-remove-block',function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    var blockCount = $(this).parent().prev().prev().find('.pa-no-of-blocks').val();
    if(blockCount < 1) blockCount = 1;
    $('#blockCount').html(blockCount);

    $('#delete-blocks-confirmation-modal').modal('show');

    var id = $(this).data('id');
    $('#confirm-delete-blocks-btn').data('blockId',id);
    $('#confirm-delete-blocks-btn').removeClass('disabled');
  });

  // confirm delete blocks
  $(document).on('click', '#confirm-delete-blocks-btn', function(e) {
      $('#delete-block-progress').removeClass('hidden');
      $('#confirm-delete-blocks-btn').addClass('disabled');
      var blockId = $(this).data('blockId');
      $('.preloader-wrapper').removeClass('hidden');
      $.ajax({
        url: '$removeRow',
        type: 'POST',
        async: false,
        data: {
          id: blockId
        },
        success: function(data){
          location.reload();
        }
      });
  });

  var windowHeight = $(window).height();
  var maxHeight = (windowHeight) - 350;

  $(".kv-grid-container").css("max-height", maxHeight);

  // save nursery blocks
  function saveBlocks(){

    // validate all sub-blocks
    var ifHasError = [];
    $(".pa-no-of-blocks").each(function( index ) {

      var obj = $(this);
      var val = obj.val();
      var savedVal = obj.data('saved');

      if(val > 50){
        obj.focus();
        $('.red-text').remove();
        obj.addClass('invalid');
        obj.after("<span class='red-text'>Value must be < 50</span>");
        ifHasError.push(1);
      }else if(val < savedVal || (val == '' && savedVal != 0)){
        obj.focus();
        $('.red-text').remove();
        obj.addClass('invalid');
        obj.after("<span class='red-text'>Value must not be < "+savedVal+"</span></span>");
        ifHasError.push(1);
      }
    });

    if(ifHasError.length < 1){
      $('.preloader-wrapper').removeClass('hidden');

      $.ajax({
        url: '$saveNurseryBlocks',
        type: 'POST',
        async: false,
        success: function(data){
          $('.toast').css('display','none');

          var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully added blocks.</span>";
                   Materialize.toast(notif, 5000);
        
          $('.preloader-wrapper').addClass('hidden');
          
          }
      });
    }

    return false;
  }

  $(document).on('click','.pa-add-blocks-continue-btn',function(e){
  });

  // validate add new blocks
  $(document).on('focusout','#pa-addl-blocks-input',function(e){
    var obj = $(this);
    var val = obj.val();

    $('.red-text').remove();
    // if more than 10
    if(val > 10){
      obj.addClass('invalid');
      obj.focus();
      obj.after("<span class='red-text'>Value must be <= 10</span>");
    }else if(val < 1){
      obj.after("<span class='red-text'>Value must be at least 1</span>");
    }else{
      obj.removeClass('invalid');
    }
  });

  // force numeric input
  $('body').on('propertychange input','.pa-no-of-blocks',forceNumeric);
  $('body').on('propertychange input','#pa-addl-blocks-input',forceNumeric);

  // validate numeric
  function forceNumeric(){
      var input = $(this);
      input.val(input.val().replace(/[^\d]+/g,''));
  }
  
  $(document).on('change','.pa-no-of-blocks',function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    var obj = $(this);
    var rowId = obj.data('id');
    var val = obj.val();
    var savedVal = obj.data('saved');

    if(val > 50){
      $('.red-text').remove();
      obj.focus();
      obj.addClass('invalid');
      obj.after("<span class='red-text'>Value must be < 50</span>");
    }else if(val < savedVal || (val == '' && savedVal != 0)){
      obj.focus();
      obj.addClass('invalid');
      $('.red-text').remove();
      obj.after("<span class='red-text'>Value must not be < "+savedVal+"</span>");
    }else{
      // validate and save
      $.ajax({
        url: '$addSubBlock',
        type: 'POST',
        async: false,
        data: {
          rowId: rowId,
          newVal: val,
          savedVal: savedVal	
        },
        success: function(data){
              obj.removeClass('invalid');
              $('.red-text').remove();
              saveBlocks();
        }
      });
    }

    return false;
  });

  // start assign parents
  $(document).on('click','.search-btn',function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    $.pjax.reload({
      container: '#pa-assign-parents-search-parents-pjax',
      url: "$searchParents",
      replace: false
    });

    $.pjax.xhr = null;
  });

  // assign entries
  $(document).on('click','.pa-add-blocks-continue-btn',function(){
      $('.pa-assign-entries-tab').click();
  });
}
JS;

$this->registerJs($js);
?>