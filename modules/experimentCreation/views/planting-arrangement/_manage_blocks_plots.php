<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting arrangement manage block form
*/

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\experimentCreation\models\PlantingArrangement;
use app\models\Entry;
use kartik\dynagrid\DynaGrid;
use app\dataproviders\ArrayDataProvider;

// get plots
$data = \Yii::$container->get('app\modules\experimentCreation\models\PlantingArrangement')->getPlotsByBlock($subBlockId, '', 'asc');

$resultsCount = count($data);

$columns = [
    [
        'label'=> "Checkbox",
        'headerOptions'=>['class'=>'check-box-header check-box-header-'.$subBlockId,'data-id'=>$subBlockId],
        'header' => '
            <input type="checkbox" class="filled-in" id="pa-manage-blocks-manage-plots-select-plots-'.$subBlockId.'" data-id="'.$subBlockId.'"/>
            <label for="pa-manage-blocks-manage-plots-select-plots" style="height: 11px;"></label>',
        'content'=>function($model) use ($subBlockId){
            return '
                <input class="pa-manage-plots-grid_select-'.$subBlockId.' filled-in" data-id="'.$subBlockId.'" type="checkbox" data-plotno="'.$model['plotno'].'" />
            <label for="'.$model['plotno'].'"></label>
            ';
        },
        'hAlign'=>'center',
        'vAlign'=>'middle',
        'contentOptions' => function($model) use ($subBlockId,$subBlockName){
            return ['class' => 'pa-manage-plots-reorder-plots-row-'.$subBlockId, "data-plotno"=>$model['plotno'], 'data-id'=>$subBlockId,'data-blockname'=>$subBlockName];
        }
    ],
    [
        'attribute'=>'plotno',
        'label' => '<span title="Sequential numbering of plots">'.Yii::t('app', 'Plot No.').'</span>',
        'content'=>function($model) {
            return '<span class="plotnumber">'.$model['plotno'].'</span>';
        },
        'format' => 'raw',
        'encodeLabel'=>false
    ],
    [
        'attribute'=>'entno',
        'label' => '<span title="Sequential numbering of entries">'.Yii::t('app', 'Entry No.').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false
    ],
    [
        'attribute'=>'repno',
        'label' => Yii::t('app', 'Rep'),
    ],
    [
        'attribute'=>'designation',
        'label' => '<span title="Germplasm">'.Yii::t('app', 'Germplasm Name').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
    ],
];

// Only show parent role column when experiment type is Cross Parent Nursery or Intentional Crossing Nursery

if($experimentType == 'Cross Parent Nursery' || $experimentType == 'Intentional Crossing Nursery') {
    // get all entry ids to get their parent roles
    if(!empty($data)){
        $entryIds = array_column($data, 'entry_id');
        $entryIds = array_unique($entryIds);
        $entryIds = 'equals '.implode('|equals ', $entryIds);
    }

    $entryResponse = \Yii::$container->get('app\models\Entry')->searchAll(['fields'=>'entry.id AS entryDbId|entry.entry_role AS entryRole','entryDbId' => $entryIds]);
    $entryIdAndRole = [];

    if(isset($entryResponse['success']) && $entryResponse['success']) {
        $entries = $entryResponse['data'];
        $entryIdsKey = array_column($entries, 'entryDbId');
        $entryRoleVal = array_column($entries, 'entryRole');
        $entryIdAndRole = array_combine($entryIdsKey, $entryRoleVal);

        // add parent role to each entry in a plot
        foreach($data as $key => $val) {
            $data[$key]['parentRole'] = $entryIdAndRole[$data[$key]['entry_id']];
        }
    }

    $columns[] = [
        'attribute'=>'parentRole',
        'label' => Yii::t('app', 'Parent Role'),
    ];
}

$dataProvider = new ArrayDataProvider([
    'allModels' => $data,
    'pagination' => false,
    'sort' => false
]);

$gridColumns = array_merge($columns, $varCols);
?>

<?= DynaGrid::widget([
    'options'=>['id'=>'pa-manage-blocks-manage-plots-grid-'.$subBlockId,'class'=>'manage-plots-grid','data-id'=>$subBlockId,'data-blockname'=>$subBlockName],
    'columns' => $gridColumns,
    'theme'=>'simple-default',
        'showPersonalize'=>false,
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions'=>[
            'rowOptions' => [
                'class' => 'plot-sortable'
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => null,
            'showPageSummary'=>false,
            'pjax'=>true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options'=>[
                        'id'=>'pa-manage-blocks-manage-plots-grid-'.$subBlockId,
                    'enablePushState'=>false
                    ],
                    'beforeGrid'=>'',
                    'afterGrid'=>''
                ],
                'responsiveWrap'=>false,
        'panel' => [
            'heading'=>false,
            'before' => \Yii::t('app', 'This lists the plots assigned in this block. You may remove or reorder (drag and drop) plots.').' {summary} ',
            'after' => false,
        ],
        'floatHeader' => true,
            'floatOverflowContainer'=> true,
            'toolbar' => [
                'content' => Html::a('Remove plot','#',[
                    'style'=>'margin-left:10px;','type'=>'button', 
                    'title'=>\Yii::t('app',"Remove selected plots"),
                    'class'=>'tabular-btn btn waves-effect waves-light red darken-3 disabled',
                    'data-id'=>$subBlockId,
                    'data-blockname'=>$subBlockName,
                    'id'=>'pa-manage-block-remove-plot-'.$subBlockId
                ])
            ]
        ]
]) ?>

<script type="text/javascript">
    var subBlockId = '<?= $subBlockId ?>';
    var resultsCount = <?= $resultsCount ?>;

    $('.ui-sortable').attr('data-id',subBlockId);

    // enable sorting of plots
    if(resultsCount <= 50) {
        $("#pa-manage-blocks-manage-plots-grid-"+subBlockId+" tbody").sortable({
            tolerance: 'pointer',
            cursor: 'move'
        });
    } else {
        var $sortable1 = $("#pa-manage-blocks-manage-plots-grid-"+subBlockId+" tbody").sortable({
            connectWith: "#pa-manage-blocks-manage-plots-grid-"+subBlockId+" tbody",
            items: ".sorting-initialize" // only insert element with class sorting-initialize
        });
        $sortable1.find(".plot-sortable").one("mouseenter",function(){
            $(this).addClass("sorting-initialize");
            $sortable1.sortable('refresh');
        });
    }
</script>