<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders browser for saved list for entry list selection
 */

use kartik\grid\GridView;
use kartik\widgets\Select2;

use yii\helpers\Html;
use yii\helpers\Url;
?>

<p><?= \Yii::t('app', 'Select reorder method and then enter the new order number. Click Confirm to save.') ?></p>
<div id="pa-reorder-working-list-modal-notification" class="hidden" style="color: red;"></div>
<div>
    <h6>Reorder Method</h6>
    <?php
        echo '<div style="width:auto;margin-bottom:5px">'.Select2::widget([
            'name' => 'data_filter_tool',
            'id' => 'select2-reorder-method',
            'data' => ['Cascade','Swap'],
            'maintainOrder' => true,
            'pluginOptions' => [
                'minimumResultsForSearch' => -1,
                'dropdownParent' => new yii\web\JsExpression('$("#pa-reorder-working-list-modal")')
            ]
        ]).'</div>';
    ?>
</div>
<?php

    $columns = [
        [
            'attribute' => 'index',
            'label' => 'Original Order Number',
            'format' => 'raw',
            'filter' => false,
            'visible' => true,
            'hidden' => false,
            'value' => function ($data) {
                return '<div class="old-order-number-field" style="text-align: left;">'.($data['index']+1).'</div>';
            }
        ],
        [
            'attribute' => 'index',
            'label' => 'New Order Number',
            'format' => 'raw',
            'filter' => false,
            'visible' => true,
            'hidden' => false,
            'value' => function ($data) {
                return Html::input('number', ($data['index']+1).'_name_value', ($data['index']+1), [
                    'class'=> "editable-field new-order-number-field",
                    'min' => '1'
                ]);
            }
        ],
        [
            'attribute' => 'entryNumber',
            'label' => Yii::t('app', 'Entry No.'),
            'format' => 'raw',
            'filter' => false,
            'visible' => true,
            'hidden' => false,
            'value' => function ($data) {
                return $data['entryNumber'];
            }
        ],
        [
            'attribute' => 'replicate',
            'label' => Yii::t('app', 'Replicate'),
            'format' => 'raw',
            'filter' => false,
            'visible' => true,
            'hidden' => false,
            'value' => function ($data) {
                return $data['replicate'];
            }
        ],
        [
            'attribute' => 'designation',
            'label' => Yii::t('app', 'Germplasm Name'),
            'format' => 'raw',
            'filter' => false,
            'visible' => true,
            'hidden' => false,
            'value' => function ($data) {
                return $data['designation'];
            }
        ]
    ];

    echo $grid = GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'reorder-replicate-table-grid',
        'tableOptions' => ['id' => 'reorder-replicate-table', 'class' => 'table table-bordered white z-depth-3'],
        'columns' => $columns,
        'rowOptions' => [
            "class" => "reorder-replicate-row"
        ]
    ]);


?>
<script>
$(document).ready(function() {
});
</script>