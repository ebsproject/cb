<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting arrangement add blocks
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\grid\GridView;

use kartik\dynagrid\DynaGrid;
use marekpetras\yii2ajaxboxwidget\Box;
?>

<?php
	$toolsButtons = [
		'reload' => function(){
			return 
				'&nbsp;&nbsp;<a href="#!" title="'.\Yii::t('app','Refresh data').'" class="text-muted fixed-color add-blocks-refresh-btn" onclick="$(&quot;#pa-add-blocks&quot;).box(&quot;reload&quot;);"><i class="material-icons widget-icons">loop</i></a>';
		}
	];
	
	$options = [
		'id' => 'pa-add-blocks',
		'bodyLoad' => ['/experimentCreation/planting-arrangement/add-block?program='.$program.'&id='.$id.'&processId='.$processId],
		'toolsTemplate' => '{reload}',
		'toolsButtons' => $toolsButtons
	];
?>
<?= Box::widget($options); ?>

<style type="text/css">
	#pa-add-blocks .box-body{
		padding: 1px 5px 5px 10px;
	}
</style>
