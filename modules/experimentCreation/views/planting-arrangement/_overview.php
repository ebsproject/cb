<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting arrangement overview
*/

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;

// blocks browser columns
$columns = [
    [
        'header'=>'',
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute'=>'experimentBlockCode',
        'label' => 'Block Code',
    ],
    [
        'attribute'=>'experimentBlockName',
        'label' => 'Block Name',
    ],
    [
        'attribute'=>'experimentType',
        'format'=>'raw',
        'contentOptions'=>function($data){
            if($data['experimentType'] == 'Parent block'){
                return ['class' => 'amber lighten-4'];
            }else{
                return ['class' => 'green lighten-4'];
            }
        }
    ],
    [
        'attribute'=>'startingCorner',
        'contentOptions'=>function($data){
            if($data['rowgroup'] % 2){
                return ['class' => 'light-green lighten-4'];
            }else{
                return ['class' => 'teal lighten-4'];
            }
        },
    ],
    [
        'attribute'=>'plotNumberingOrder',
        'contentOptions'=>function($data){
            if($data['rowgroup'] % 2){
                return ['class' => 'light-green lighten-4'];
            }else{
                return ['class' => 'teal lighten-4'];
            }
        },
    ],
    [
        'header'=>'<span title="No. of rows in a block">Rows</div>',
        'attribute'=>'noOfRanges',
        'contentOptions'=>function($data){
            if($data['noOfRanges'] == 0){
                return ['class' => 'red lighten-4'];
            }
            else if($data['rowgroup'] % 2){
                return ['class' => 'light-green lighten-4'];
            }else{
                return ['class' => 'teal lighten-4'];
            }
        },
    ],
    [
        'header'=>'<span title="No. of columns in a block">Cols</div>',
        'attribute'=>'noOfCols',
        'contentOptions'=>function($data){
            if($data['noOfCols'] == 0){
                return ['class' => 'red lighten-4'];
            }
            else if($data['rowgroup'] % 2){
                return ['class' => 'light-green lighten-4'];
            }else{
                return ['class' => 'teal lighten-4'];
            }
        },
    ],
    [
        'attribute'=>'entryCount',
        'format'=>'raw',
        'contentOptions'=>function($data){
            if($data['entryCount'] == 0){
                return ['class' => 'red lighten-3'];
            }
            else if($data['rowgroup'] % 2){
                return ['class' => 'light-green lighten-3'];
            }else{
                return ['class' => 'teal lighten-3'];
            }
        },
        'label' => 'No. of entries',
    ],
    [
        'attribute'=>'plotCount',
        'format'=>'raw',
        'label' => 'No. of plots',
        'contentOptions'=>function($data){
            if($data['plotCount'] == 0){
                return ['class' => 'red lighten-3'];
            }
            else if($data['rowgroup'] % 2){
                return ['class' => 'light-green lighten-3'];
            }else{
                return ['class' => 'teal lighten-3'];
            }
        }
    ]
];

$replicateCountAssigned = (empty($replicateCountAssigned)) ? 0 : $replicateCountAssigned;
$percent = ($replicateCount > 0)? round(($replicateCountAssigned / $replicateCount) * 100, 2) : 0;
$percentClass = ($percent == 100) ? 'green-text' : 'red-text';
$panel = [
    'heading'=> false,
    'after'=> false,
    'before' => '<table><tr>
            <td style="padding:0px 10px 0px 0px">
                <div class="progress">
                <div class="determinate" style="width: '.$percent.'%"></div>
                </div>
                <div class="pull-left"> <b>'.$replicateCountAssigned.'</b> of <b>'.$replicateCount.'</b> <b class="'.$percentClass.' text-darken-2">('.$percent.'%)</b> replicates are assigned to blocks</div>
                <div class="pull-right">Total no. of plots: <b>'.$plotCountAssigned.'</b></div>
            </td>
        </tr></table>',
    'footer' => false
];
// blocks browser
DynaGrid::begin([
    'options'=>['id'=>'pa-overview-grid'],
    'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>false,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel' => null,
        'pjax'=>true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
                'id' => 'pa-overview-grid'
            ],
        ],
        'panel'=> $panel,
        'responsiveWrap'=>false,
        'showPageSummary'=>false,
        'toolbar' =>  false,
        'rowOptions'=>function($data){
            if($data['rowgroup'] % 2){
                return ['class' => 'light-green lighten-5'];
            }else{
                return ['class' => 'teal lighten-5'];
            }
        },
    ]
]);
DynaGrid::end();
echo '<div class="clearfix"></div><div class="row"><br/></div>';

$this->registerJs(<<<JS
    $(document).ready(function(){
        $('#change-design-switch-id').removeAttr('disabled')
    });
JS);
?>
<style type="text/css">
    #pa-overview-grid .clearfix{
        clear: none !important;
    }
    #pa-overview-grid .clearfix:before,#pa-overview-grid  .clearfix:after{
        display: none;
    }
    #pa-overview-grid .table > tbody > tr > td{
        border-top: transparent;
    }
    #pa-overview-grid td, th {
        border-radius: 0px !important;
    }
</style>
