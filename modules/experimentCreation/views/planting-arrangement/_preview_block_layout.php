<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders preview layout of block
*/

use yii\helpers\Url;
use app\modules\experimentCreation\models\PlantingArrangement;

if(empty($data)){
    if(!isset($isEntryOrder)){
        echo '<div class="alert alert-warning" style="padding: 10px;">Incompelete or invalid information. Please correct block parameters first to generate preview.</div>';
    } else {
        echo '<div class="alert alert-warning" style="padding: 10px;">Layout cannot be shown for this arrangement. Please specify the dimension to generate preview.</div>';
    }
?>
<script>

    var data = JSON.parse('<?= json_encode($data) ?>'); 
   
    $(document).ready(function(){
         if(data.length == 0){
            $('#fullscreenMax-btn').addClass('hidden');
        } else {
            $('#fullscreenMax-btn').removeClass('hidden');
        }
    });
</script>
<?php
}else{
    // get layout data info
    $layoutOrderData = \Yii::$container->get('app\modules\experimentCreation\models\PlantingArrangement')->getPlotsByBlock($blockId, '', 'asc');
    foreach($data as $key => $val) {    // $data only contains up to 100 objects, so its safe to loop
        /**
         * This checker exists because the $data from the actionRefreshPreviewLayoutOfBlock is an object
         * while the $data from actionPreviewLayoutOfBlock is an array. The way to access the values
         * from an object differs from that of an array. Just PHP things.
         */
        if($isRefreshLayout) {
            $val = $data[$key]['value'];
            if(!is_numeric($val) || $val == '...') continue;
            $plotOrderIndex = intval($val) - $startingPlotNo;
            if($plotOrderIndex >= 0 && isset($layoutOrderData[$plotOrderIndex])) $data[$key]['entNo'] = $layoutOrderData[$plotOrderIndex]['entno'];
            else unset($data[$key]);
        } else {
            $val = $data[$key]->value;
            if(!is_numeric($val) || $val == '...') continue;
            $plotOrderIndex = intval($val) - $startingPlotNo;
            if($plotOrderIndex >= 0 && isset($layoutOrderData[$plotOrderIndex])) $data[$key]->entNo = $layoutOrderData[$plotOrderIndex]['entno'];
            else unset($data[$key]);
        }
    }

    echo "<a id='fullscreenExit-btn' class='waves-effect waves-light btn pull-right fullscreen-btn pull-right tooltipped hidden' style='margin-right:25px;' data-position='top' data-delay='50' title='Click to toggle fullscreen'><i class='material-icons'>fullscreen_exit</i></a>";

    $viewLayoutUrl = Url::to(['view/view-block-layout', 'blockDbId'=>$blockId]);
?>

<div id="pa-block-layout-container-<?= $blockId ?>" class="chartContainer" style=" margin: '100px';overflow: auto;"></div>

<script type="text/javascript">
var blockId = <?= $blockId ?>;
var startingPlotNo = <?= $startingPlotNo ?>;
var startingCorner = '<?= $startingCorner ?>';
var noOfCols = <?= $noOfCols ?>;
var noOfRows = <?= $noOfRows ?>;
var data = JSON.parse('<?= json_encode($data) ?>'); 
var panelWidth = <?= $panelWidth ?>;
var fullscreenFlag = null;

var xAxis = null;
var yAxis = null;
var wbWidth = 65;
var hbWidth = 40;
var height = 0;
var datafull = null;
$(document).ready(function(){
    renderBlockLayout();
    if(data.length == 0){
        $('#fullscreenMax-btn').addClass('hidden');
    } else {
        $('#fullscreenMax-btn').removeClass('hidden');
    }
});

var width = panelWidth;

function getPointCategoryName(point, dimension) {
    var series = point.series,
        isY = dimension === 'y',
        axis = series[isY ? 'yAxis' : 'xAxis'];
    return axis.categories[point[isY ? 'y' : 'x']];
}

function renderBlockLayout(){
    width = panelWidth;
    setTimeout(function(){
        try {
            layout = Highcharts.chart('pa-block-layout-container-'+blockId,{
                chart: {
                    marginTop: 70,
                    marginRight: 14,
                    plotBorderWidth: 1,
                    type: 'heatmap',
                    plotBorderWidth: 1,
                    plotBorderRadius: 5,
                    animation: false,
                    width: width
                },
                title: {
                    text: 'Planting Arrangement Preview'
                },
                xAxis: {
                    reversed: (startingCorner.includes('Right'))? true: false,
                    opposite: true,
                    categories: ['0,','1','2','3','4','5','6','7','8','9',noOfCols],
                    title:{
                        text:"Column"
                    }
                },
                yAxis: {
                    reversed: (startingCorner.includes('Top'))? true: false,
                    categories: ['0,','1','2','3','4','5','6','7','8','9',noOfRows],
                    title:{
                        text:"Row"
                    }
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            useHTML: true,
                            formatter: function () {
                                if(getPointCategoryName(this.point, 'y') <= 10 && getPointCategoryName(this.point, 'x') <= 10) {
                                    if(this.point.entNo == null || this.point.value == null) {
                                        return ''
                                    }
                                    return this.point.entNo + ' <sup>' + this.point.value + '</sup>';
                                } else {
                                    return '...';
                                }
                            }
                        }
                    }
                },
                colorAxis:{
                    min: startingPlotNo,
                    minColor: '#c5e1a5',
                    maxColor: '#33691e'
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    useHTML: true,
                    outside: true,
                    formatter: function () {
                        if(getPointCategoryName(this.point, 'y') <= 10 && getPointCategoryName(this.point, 'x') <= 10) {
                            if(this.point.entNo == null || this.point.value == null) {
                                return ''
                            }
                            return '<b>ENTNO: ' + this.point.entNo + ' PLOTNO: '+this.point.value+'</b><br>' +
                                'ROW ' + getPointCategoryName(this.point, 'y') + ' COLUMN ' + getPointCategoryName(this.point, 'x');
                        } else {
                            return '...';
                        }
                    }
                },
                series: [{
                    name: 'Layout',
                    borderWidth: 1,
                    borderColor: '#fff',
                    color: '#7cb342',
                    data: data,
                    dataLabels: {
                        enabled: true
                    }
                }]
            });
        } catch (error) {
            /**
             * Catches Highcharts Error #13 which requires a `renderTo: 'container'` inside chart property
             * which cannot be added (since it will not find the container) because this .php is rendered
             * first before being added to the view file. Just Yii things.
             */
        }
    },50);

}

    $('.fullscreen-btn').click(function(e){
        // trigger action ongoing
        e.preventDefault();
        e.stopImmediatePropagation();

        if(fullscreenFlag == null || fullscreenFlag == 0){
            fullscreenFlag = 1;
            $('.pa-full-layout-preview-panel').toggleClass('fullscreen');
            $('#fullscreenExit-btn').removeClass('hidden');
            $('#pa-block-layout-container-'+blockId).html('<div class="alert alert-warning" style="padding: 10px;">Please wait while loading the layout.</div>');

            $.ajax({
                url: '<?= $viewLayoutUrl ?>',
                type: 'post',
                data: {},
                success: function(response) {
                    layoutData = JSON.parse(response);
                    datafull = layoutData['data'];
                    
                    xAxis = layoutData['xAxis'];
                    yAxis = layoutData['yAxis'];   

                    var row = Object.keys(yAxis).length;
                    var col = Object.keys(xAxis).length;

                    //computeDimension
                    if(col <= 20){
                        wbWidth = 80;
                    }
                    if(row <= 20){
                        hbWidth = 40;
                    }
                    //compute width of chart
                    width = wbWidth * col;
                    if(width < 200){
                        width = 200;
                    }

                    //compute height of chart
                    height = hbWidth * row;
                    if(height < 200){
                        height = 200;
                    }

                    renderFullLayout();
                },
                error: function(e) {
                    console.log(e);
                }
            });
        } 
        else if(fullscreenFlag == 1){
            fullscreenFlag = 0;
            $('#main-left-panel').removeClass('hidden');
            $('#pa-block-layout-container-'+blockId).html('<div class="progress"><div class="indeterminate"></div></div>');
            $('.pa-full-layout-preview-panel').toggleClass('fullscreen');
            $('#fullscreenExit-btn').addClass('hidden');
            renderBlockLayout();
        }
    });

    function renderFullLayout(){
        setTimeout(function(){
        try {
            layout = Highcharts.chart('pa-block-layout-container-'+blockId,{
                chart: {
                    marginTop: 70,
                    marginRight: 14,
                    plotBorderWidth: 1,
                    type: 'heatmap',
                    plotBorderWidth: 1,
                    plotBorderRadius: 5,
                    animation: false,
                    height: height,
                    width: width
                },
                title: {
                    text: 'Planting Arrangement Preview'
                },
                xAxis: {
                    reversed: (startingCorner.includes('Right'))? true: false,
                    opposite: true,
                    categories: xAxis,
                    title:{
                        text:"Column"
                    }
                },
                yAxis: {
                    reversed: (startingCorner.includes('Top'))? true: false,
                    categories: yAxis,
                    title:{
                        text:"Row"
                    }
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            useHTML: true,
                            formatter: function () {
                                if(this.point.entNo == null || this.point.value == null) {
                                    return ''
                                }
                                return this.point.entNo + ' <sup>' + this.point.value + '</sup>';
                            }
                        }
                    }
                },
                colorAxis:{
                    min: startingPlotNo,
                    minColor: '#c5e1a5',
                    maxColor: '#33691e'
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    useHTML: true,
                    outside: true,
                    formatter: function () {
                        if(this.point.entNo == null || this.point.value == null) {
                            return ''
                        }
                        return '<b>ENTNO: ' + this.point.entNo + ' PLOTNO: '+this.point.value+'</b><br>' +
                            'ROW ' + getPointCategoryName(this.point, 'y') + ' COLUMN ' + getPointCategoryName(this.point, 'x');

                    }
                },
                series: [{
                    name: 'Layout',
                    borderWidth: 1,
                    borderColor: '#fff',
                    color: '#7cb342',
                    turboThreshold:Number.MAX_VALUE,
                    data: datafull,
                    dataLabels: {
                        enabled: true
                    }
                }]
            });
        } catch (error) {
            /**
             * Catches Highcharts Error #13 which requires a `renderTo: 'container'` inside chart property
             * which cannot be added (since it will not find the container) because this .php is rendered
             * first before being added to the view file. Just Yii things.
             */
        }
    },50);
    }
</script>

<style type="text/css">
    tspan{
        font-size: 13px;
    }
    .highcharts-container{
        width:100%;
    }
    .chartContainer{
        overflow:auto !important;
    }
    .pa-full-layout-preview-panel.fullscreen{
        padding: 25px;
        z-index: 9999; 
        width: 100%; 
        height: 100%; 
        position: fixed; 
        overflow:auto !important;
        top: 0; 
        left: 0;
        background: #ffffff;
    }
</style>

<?php
}
?>