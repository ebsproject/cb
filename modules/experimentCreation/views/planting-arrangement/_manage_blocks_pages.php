<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting arrangement manage block form
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\modules\experimentCreation\models\PlantingArrangement;
use app\models\Entry;
use app\models\Experiment;
use kartik\dynagrid\DynaGrid;
use app\dataproviders\ArrayDataProvider;

$varCols = json_encode($varCols);

extract($params);

// if no plots assigned, hide manage plots button
$managePlotsClass = empty($totalPlots) ? 'hidden': '';
$fullscreenBtn = "<a id='fullscreenMax-btn' class='waves-effect waves-light btn pull-right fullscreen-btn pull-right tooltipped ".$managePlotsClass."' style='margin-left:15px;' data-position='top' data-delay='50' title='Click to toggle fullscreen'><i class='material-icons'>fullscreen</i></a>";
echo '<h5>'.$subBlockName.
    Html::a('Save','#',[
        'style'=>'margin-right:0px;',
        'type'=>'button',
        'title'=>\Yii::t('app',"Save information of this block"),
        'data-id'=>$subBlockId,
        'data-blockname'=>$subBlockName,
        'class'=>'disabled tabular-btn btn waves-effect waves-light pa-manage-blocks-save-btn pull-right',
        'id'=>'pa-manage-blocks-save-btn-'.$subBlockId
    ]).
    Html::a('Remove block','#',[
        'style'=>'margin-right:3px;',
        'type'=>'button',
        'title'=>\Yii::t('app',"Remove block"),
        'data-id'=>$subBlockId,
        'data-blockname'=>$subBlockName,
        'data-plotcount'=>$totalPlots,
        'class'=>'red darken-3 pull-right tabular-btn btn waves-effect waves-light pa-manage-blocks-delete-block-btn'
    ]).
    Html::a('Manage plots','#',[
        'style'=>'margin-right:3px;',
        'type'=>'button',
        'title'=>\Yii::t('app',"Manage plots in the block"),
        'data-id'=>$subBlockId,
        'data-blockname'=>$subBlockName,
        'data-plotcount'=>$totalPlots,
        'class'=>'grey lighten-3 black-text pull-right tabular-btn btn waves-effect waves-light '.$managePlotsClass.' pa-manage-blocks-manage-plots-btn-'.$subBlockId
    ]). $fullscreenBtn.
    '
    <div class="preloader-wrapper small active right" style="width:25px;height:25px;margin: 5px 10px;">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>&emsp;
    '.
    '</h5></br>';

if(!empty($totalPlots)){

$activeTabClass = '';
$display = 'none';
?>
<!-- Manage plots panel -->
<ul class="collapsible" style="border-top: transparent;margin-top: 0px;">
    <li class="<?= $activeTabClass ?>">
        <div class="collapsible-header collapsible-<?= $subBlockId . ' '.$activeTabClass ?> hidden"></div>
        <div id="manage-plots-browser" class="collapsible-body" style="padding: 0;display: <?= $display ?>">
        </div>
    </li>
</ul>
<?php
}
// if there are no assigned plots yet
if(empty($totalPlots)){
    echo '<div class="alert alert-warning" style="padding: 10px">There are no assigned plots yet in this block. Please add first in "Assign entries" tab.</div>';
}else{

    echo '<div class="col col-md-6 manage-blocks-panel pa-blocks-width-'.$subBlockId.'">';

    foreach ($subBlockParams as $key => $value) {

        $class = ($key == 'no_of_entries_in_block' || $key == 'total_no_of_plots_in_block') ? true : false;

        $data = \Yii::$container->get('app\modules\experimentCreation\models\PlantingArrangement')->getFormDisplay($key);
        
        $label = isset($data['name']) && !empty($data['name']) ? $data['name'] : ucfirst(str_replace('_', ' ', $key));
        $description = !empty($data['description']) ? $data['description'] : ucfirst(str_replace('_', ' ', $key));

        if(in_array($key, ['plot_numbering_order','starting_corner'])){
            $options = ($key == 'plot_numbering_order') ? $plotNumberingValues : $startingCornerValues;

            $options = str_replace('value="'.$value.'"', 'value="'.$value.'" selected', $options);

            echo '<div class="col col-md-6"><div class="col col-md-5"><label class="control-label" title="'.$description.'">'.$label.'</label></div>';

            $select = '<select class="browser-default pa-manage-block-params pa-manage-block-params-'.$key.'-'.$subBlockId.'" data-abbrev="'.$key.'" style="height:auto;" data-id="'.$subBlockId.'">'.$options.'</select>';

            echo '<div class="col col-md-7">'.$select.'</div></div>';
        } else if($key == 'remarks') {
            echo '<div class="col col-md-12"><div class="col col-md-2"><label class="control-label"    title="'.$description.'">'.$label.'</label></div>';
            echo '<div class="col col-md-10">'.Html::input('text', 'pa-manage-block-params-'.$subBlockId.'_'.$key.'-'.$subBlockId, $value, ['class'=>'pa-manage-block-remarks-params pa-param-computer input pa-manage-block-params-'.$key.'-'.$subBlockId,'data-abbrev'=>$key,'data-id'=>$subBlockId,'disabled'=>$class]).'</div></div>';
        }else{

            echo '<div class="col col-md-6"><div class="col col-md-5"><label class="control-label"    title="'.$description.'">'.$label.'</label></div>';
            echo '<div class="col col-md-7">'.Html::input('number', 'pa-manage-block-params-'.$subBlockId.'_'.$key.'-'.$subBlockId, $value, ['class'=>'pa-manage-block-params pa-param-computer input-integer pa-manage-block-params-'.$key.'-'.$subBlockId,'data-abbrev'=>$key,'data-id'=>$subBlockId,'data-plot'=>$totalPlots,'min'=>1,'disabled'=>$class]).'</div></div>';
        }
    }

    echo '<div class="col col-md-12" id="reset-note" style="margin-top:10px;">'.'&emsp;</div>';
    echo '<div class="clearfix"></div><div class="col col-md-6" ><div class="col col-md-5" ></div>';

    echo '<div class="col col-md-6" style="margin-top:10px;">'.'&emsp;</div></div>';
    echo '</div>';

    echo '<div class="col col-md-6 pa-manage-blocks-preview pa-manage-blocks-preview-panel-'.$subBlockId.' pa-full-layout-preview-panel" style="padding-right:0px">';
    echo '</div>';
}

// delete block modal body
Modal::begin([
    'id' => 'pa-manage-block-delete-block-modal-'.$subBlockId,
    'header' => '<h4><i class="material-icons" style="vertical-align: bottom;">delete</i> '.\Yii::t('app','Remove block'). '</h4>',
    'footer' =>
        Html::a('Cancel', ['#'], ['id' => 'pa-delete-block-modal-cancel-btn', 'class'=>'margin-right', 'data-dismiss'=> 'modal']).
        Html::a(\Yii::t('app','Confirm'),'#',[
            'class'=>'btn waves-effect waves-light pa-remove-block-confirm-btn margin-right',
            'data-id'=>$subBlockId,
            'data-blockname'=>$subBlockName
        ]),
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);
echo '<div class="pa-remove-block-modal-body"></div>';

Modal::end();

// remove a plot modal body
Modal::begin([
    'id' => 'pa-manage-block-remove-plot-modal-'.$subBlockId,
    'header' => '<h4><i class="material-icons" style="vertical-align: bottom;">delete</i> '.\Yii::t('app','Remove plot'). '</h4>',
    'footer' =>
        Html::a('Cancel', ['#'], ['id' => 'pa-manage-block-remove-plot-modal-cancel-btn', 'class'=>'margin-right', 'data-dismiss'=> 'modal']).
        Html::a(\Yii::t('app','Confirm'),'#',[
            'class'=>'btn waves-effect waves-light pa-manage-block-remove-plot-confirm-btn margin-right',
            'data-id'=>$subBlockId,
            'data-blockname'=>$subBlockName
        ]),
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);
echo '<div class="pa-manage-block-remove-plot-modal-body"></div>';

Modal::end();

$updateBlockParamsUrl = Url::to(['/experimentCreation/planting-arrangement/update-block-params','id'=>$id]);
$deleteBlockUrl = Url::to(['/experimentCreation/planting-arrangement/delete-block','id'=>$id]);
$managePlotsUrl = Url::to(['/experimentCreation/planting-arrangement/manage-plots-in-block']);
$removePlotsUrl = Url::to(['/experimentCreation/planting-arrangement/remove-plots-in-block','id'=>$id]);
$reorderPlotsUrl = Url::to(['/experimentCreation/planting-arrangement/reorder-plots-in-block','id'=>$id]);
$refreshPreviewLayoutOnChangeUrl = Url::to(['/experimentCreation/planting-arrangement/refresh-preview-layout-of-block','id'=>$id]);
$getActiveBlockTabUrl = Url::to(['/experimentCreation/planting-arrangement/get-active-block-tab','id'=>$id]);

$checkStatusUrl  = Url::to(['create/check-status','id'=>$id, 'action'=>'cross-design']);

Yii::$app->view->registerJs(
    "
        checkStatusUrl = '" . $checkStatusUrl . "',
        experimentId = '" . $id . "',
        requiredFieldJson = 'null',
        checkStatValue = 'design generated'
        ;",
    \yii\web\View::POS_HEAD
);

$this->registerJsFile("@web/js/check-required-field-status.js", [
    'depends' => ['app\assets\AppAsset'],
    'position' => \yii\web\View::POS_END
]);
?>
<style type="text/css">
    .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus {
        background-color: #bcdaf5;
    }
    .manage-plots-grid .clearfix{
        clear: none !important;
    }
    .manage-plots-grid .kv-panel-before{
        padding-bottom: 20px;
    }
    .manage-plots-grid .clearfix:before,.manage-plots-grid .clearfix:after{
        display: none;
    }
    .tabs-krajee.tabs-left .nav-tabs, .tabs-krajee.tabs-right .nav-tabs {
        width: auto;
    }
    .ui-sortable-handle {
        cursor: move !important;
    }
</style>
<script type="text/javascript">
    var subBlockId = '<?= $subBlockId ?>';
    var activeBlockId = <?= $activeTabBlockId ?>;
    var panelWidth = $('.pa-blocks-width-'+subBlockId).width() + 10;
    var plotNos = [];
    var plotCountRemove = 0;
    var checkbox = null;
    var managePlotsIsOpen = false;

    $(document).ready(function(){
        $('.collapsible').collapsible();
    });
    checkStatus();

    // upon click manage plots button
    $(document).on('click','.pa-manage-blocks-manage-plots-btn-'+subBlockId,function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var obj = $(this);

        var sbId = obj.data('id');
        if(!managePlotsIsOpen) {
            managePlotsIsOpen = true;
            $('.preloader-wrapper').removeClass('hidden');

            $.ajax({
                type: 'POST',
                url: '<?= $managePlotsUrl?>',
                data: {
                    blockId: sbId,
                    subBlockName : '<?= $subBlockName ?>',
                    experimentType : '<?= $experimentType ?>',
                    varCols : '<?= $varCols ?>'
                },
                async: true,
                success: function(data) {
                    $('#manage-plots-browser').html(data);

                    $('.preloader-wrapper').addClass('hidden');
                    $('.collapsible-'+sbId).click();
                },
                error: function(){
                    var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading plots. Please try again.';
                    Materialize.toast(notif, 5000);
                }
            });
        } else {
            managePlotsIsOpen = false;
            $('.collapsible-'+sbId).click();
        }
    });

    // reorder plots
    $(document).on('sortupdate','#pa-manage-blocks-manage-plots-grid-'+subBlockId+' tbody',function(event,ui){
        var grid = $(this).closest('.manage-plots-grid');
        var sbId = grid.data('id');
        var subBlockName = grid.data('blockname');

        $('.preloader-wrapper').removeClass('hidden');

        var list = new Array();
        $(this).find('.pa-manage-plots-reorder-plots-row-'+sbId).each(function(){
            var plotNo = $(this).attr("data-plotno");
            list.push(parseInt(plotNo));
        });

        $.ajax({
            type: 'POST',
            url: '<?= $reorderPlotsUrl?>',
            data: {
                plotNos: list,
                blockId: sbId
            },
            async: false,
            success: function(data) {
                $('.toast').css('display','none');
                var notif = "<i class='material-icons green-text left'>done</i> Successfully reordered plots in&nbsp;<b>"+subBlockName+"</b>.";

                // update plot nos in browser using jQuery because pjax.reload() refreshes the whole page
                var startingPlotNo = Math.min(...list);
                $('#pa-manage-blocks-manage-plots-grid-'+sbId+' tbody tr').each(function() {
                    var index = $(this).index();
                    var newPlotNo = startingPlotNo+index;
                    $(this).find('.plotnumber').html(newPlotNo);
                    $(this).find('.pa-manage-plots-reorder-plots-row-'+sbId).attr('data-plotno', newPlotNo);  // also update the data attribute value
                    $(this).find('.pa-manage-plots-grid_select-'+sbId).attr('data-plotno', newPlotNo); 
                });

                // show preview button
                $('.pa-manage-blocks-preview-panel-'+sbId).html(
                    '<a type="button" class="grey lighten-3 black-text tabular-btn btn waves-effect waves-light pa-manage-blocks-preview-layout-btn-'+sbId+'" href="#" title="Preview layout" style="margin-right:5px;" data-id="'+sbId+'"><i class="material-icons right tabular-icon">search</i> Preview layout</a>'
                );

                Materialize.toast(notif,5000);
                $('.preloader-wrapper').addClass('hidden');
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while updating the order of plots. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });
    });

    // if number of row is changed
    $(document).on('propertychange input','.pa-manage-block-params-no_of_rows_in_block-'+subBlockId,function(e){
        e.stopImmediatePropagation();
        var obj = $(this);
        var sbId = obj.data('id');

        // number of rows
        var rows = (obj.val()) != '' ? parseInt(obj.val()) : 0;

        var totalPlots = parseInt($('.pa-manage-block-params-total_no_of_plots_in_block-'+sbId).val());

        $(".red-text").each(function(){
            $(this).remove();
        });
        $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).removeClass('invalid');
        $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).removeClass('invalid');
        $('.manage-blocks-panel').removeClass('invalid');
        if(rows > totalPlots){
            $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).focus();
            $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).addClass('invalid');
            $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).after("<span class='red-text'>Value must be <= "+totalPlots+"</span>");
        }else if(rows < 1){
            $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).focus();
            $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).addClass('invalid');
            $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).after("<span class='red-text'>Value must be > 0</span>");
        }else{
            $(".red-text").each(function(){
                $(this).remove();
            });
            $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).removeClass('invalid');
            $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).removeClass('invalid');
        }

        var column = Math.ceil(totalPlots/rows);

        $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).val(column);
        checker(sbId);
    });

    // if number of columns is changed
    $(document).on('propertychange input','.pa-manage-block-params-no_of_cols_in_block-'+subBlockId,function(e){
        e.stopImmediatePropagation();
        var obj = $(this);
        var sbId = obj.data('id');

        // number of cols
        var cols = (obj.val()) != '' ? parseInt(obj.val()) : 0;

        var totalPlots = parseInt($('.pa-manage-block-params-total_no_of_plots_in_block-'+sbId).val());

        $(".red-text").each(function(){
            $(this).remove();
        });
        $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).removeClass('invalid');
        $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).removeClass('invalid');
        if(cols > totalPlots){
            $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).focus();
            $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).addClass('invalid');
            $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).after("<span class='red-text'>Value must be <= "+totalPlots+"</span>");
        }else if(cols < 1){
            $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).focus();
            $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).addClass('invalid');
            $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).after("<span class='red-text'>Value must be > 0</span>");
        }else{
            $(".red-text").each(function(){
                $(this).remove();
            });
            $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).removeClass('invalid');
            $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).removeClass('invalid');
        }

        var row = Math.ceil(totalPlots/cols);

        $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).val(row);
        checker(sbId);
    });

    // validate upon change
    $(document).on('propertychange input','.pa-manage-block-params',function(e){

        var obj = $(this);
        var sbId = obj.data('id');
        checker(sbId);

    });

    // enable saving for remarks upon change
    $(document).on('propertychange input','.pa-manage-block-remarks-params',function(e){
        var obj = $(this);
        var sbId = obj.data('id');
        $('#pa-manage-blocks-save-btn-'+sbId).removeClass('disabled');
        $('#pa-manage-blocks-save-btn-'+sbId).addClass('pulse');

    });

    // upon resize window
    $(window).resize(function(e){
        e.stopImmediatePropagation();
        e.stopImmediatePropagation();

        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: '<?= $getActiveBlockTabUrl?>',
                async: false,
                success: function(data) {
                    var abid = data;
                    var values = {};
                    $(".pa-manage-block-params").each(function( index ) {
                        var obj = $(this);
                        var attr = obj.data('abbrev');
                        var value = $('.pa-manage-block-params-'+attr+'-'+abid).val();
                        values[attr] = value;
                    });

                    var newWidth = $('.pa-blocks-width-'+abid).width() + 10;

                    $.ajax({
                        type: 'POST',
                        url: '<?= $refreshPreviewLayoutOnChangeUrl?>',
                        data: {
                            values: values,
                            blockId: abid,
                            panelWidth: newWidth
                        },
                        async: false,
                        success: function(data) {
                            $('.pa-manage-blocks-preview-panel-'+abid).html(data);
                        }
                    });
                }
            });

        },100);
    });

    // preview layout
    $(document).on('click','.pa-manage-blocks-preview-layout-btn-'+subBlockId,function(e){
        e.stopImmediatePropagation();
        e.stopImmediatePropagation();

        var obj = $(this);
        var sbId = obj.data('id');

        $('.preloader-wrapper').removeClass('hidden');

        var values = {};
        $(".pa-manage-block-params").each(function( index ) {
            var obj = $(this);
            var attr = obj.data('abbrev');
            var value = $('.pa-manage-block-params-'+attr+'-'+sbId).val();
            values[attr] = value;
        });

        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: '<?= $refreshPreviewLayoutOnChangeUrl?>',
                data: {
                    values: values,
                    blockId: sbId,
                    panelWidth: panelWidth
                },
                async: false,
                success: function(data) {
                    $('.preloader-wrapper').addClass('hidden');
                    $('.pa-manage-blocks-preview-panel-'+sbId).html(data);
                }
            });
        },100);

    });

    // checks if all fields are specified
    function checker(sbId){

        var error = [];
        $(".pa-blocks-width-"+sbId+" .invalid").each(function(){
            error.push(1);
        });

        var rows = $('.pa-manage-block-params-no_of_rows_in_block-'+sbId).val();
        var cols = $('.pa-manage-block-params-no_of_cols_in_block-'+sbId).val();

        // checks if all are valid
        if( (rows && cols && !error.length) ){
            $('#pa-manage-blocks-save-btn-'+sbId).removeClass('disabled');
            $('#pa-manage-blocks-save-btn-'+sbId).addClass('pulse');
            // show preview button
            $('.pa-manage-blocks-preview-panel-'+sbId).html(
                '<a type="button" class="grey lighten-3 black-text tabular-btn btn waves-effect waves-light pa-manage-blocks-preview-layout-btn-'+sbId+'" href="#" title="Preview layout" style="margin-right:5px;" data-id="'+sbId+'"><i class="material-icons right tabular-icon">search</i> Preview layout</a>'
            );

        }else{
            $('.pa-manage-blocks-preview-layout-btn-'+sbId).each(function( index ) {
                $(this).addClass('disabled');
                $(this).removeClass('pulse');
            });

            $('#pa-manage-blocks-save-btn-'+sbId).addClass('disabled');
            $('#pa-manage-blocks-save-btn-'+sbId).removeClass('pulse');

        }
    }

    // save block info
    $(document).on('click','.pa-manage-blocks-save-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var obj = $(this);
        var sbId = obj.data('id');
        var subBlockName = obj.data('blockname');

        $('.preloader-wrapper').removeClass('hidden');
        $('#pa-manage-blocks-save-btn-'+sbId).addClass('disabled');
        $('#pa-manage-blocks-save-btn-'+sbId).removeClass('pulse');    

        var values = {};
        $(".pa-manage-block-params").each(function( index ) {
            var obj = $(this);
            var attr = obj.data('abbrev');
            if(attr != 'no_of_entries_in_block'){
                var value = $('.pa-manage-block-params-'+attr+'-'+sbId).val();
                values[attr] = value;
            }
        });
        values['remarks'] = $('.pa-manage-block-params-remarks-'+sbId).val();

        $.ajax({
            type: 'post',
            url: '<?= $updateBlockParamsUrl ?>',
            data: {
                blockId: sbId,
                params: values
            },
            success: function(data) {

                var hasToast = $('body').hasClass('toast');            

                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully saved block details for <b>"+subBlockName+"</b>.</span>";
                    Materialize.toast(notif, 5000);
                }

                $('.preloader-wrapper').addClass('hidden');
                $('#reset-note').html('');

                if(managePlotsIsOpen) { // Close Manage Plots Browser
                    $('.pa-manage-blocks-manage-plots-btn-'+subBlockId).trigger('click');
                }

                // show preview button
                $('.pa-manage-blocks-preview-panel-'+sbId).html(
                    '<a type="button" class="grey lighten-3 black-text tabular-btn btn waves-effect waves-light pa-manage-blocks-preview-layout-btn-'+sbId+'" href="#" title="Preview layout" style="margin-right:5px;" data-id="'+sbId+'"><i class="material-icons right tabular-icon">search</i> Preview layout</a>'
                );
            },
            error: function() {}
        });
    });

    // delete block
    $(document).on('click','.pa-manage-blocks-delete-block-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var obj = $(this);
        var sbId = obj.data('id');
        var subBlockName = obj.data('blockname');
        var totalPlots = obj.data('plotcount');

        $('#pa-manage-block-delete-block-modal-'+sbId).modal('show');

        var warningIcon = '<i class="material-icons orange-text left">warning</i>';
        var message = 'You are about to remove <b>'+subBlockName+'</b> with <b>'+totalPlots+'</b> plots. Click "Confirm" to proceed.';
        $('.pa-remove-block-modal-body').html(warningIcon +' '+ message);

    });

    // confirm delete block
    $(document).on('click','.pa-remove-block-confirm-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        $('.pa-remove-block-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        var obj = $(this);
        var sbId = obj.data('id');

        var subBlockName = obj.data('blockname');

        $.ajax({
            type: 'post',
            url: '<?= $deleteBlockUrl ?>',
            data: {
                blockId: sbId
            },
            success: function(data) {

                var hasToast = $('body').hasClass('toast');

                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully removed <b>"+subBlockName+"</b>.</span>";
                    Materialize.toast(notif, 5000);

                    $('#pa-manage-block-delete-block-modal-'+sbId).modal('hide');

                    $('.modal-backdrop').remove();
                    $('body').removeClass('modal-open');
                    checkStatus();

                    $('.pa-manage-blocks-tab').click();
                }

            },
            error: function() {}
        });
    });

    // select all plots
    $(document).on('click','.check-box-header',function(e){
        e.preventDefault();
            e.stopImmediatePropagation();

            var sbId = $(this).data('id');
            checkbox=$('.check-box-header-'+sbId).find('input:checkbox')[0];

        if(!checkbox.checked){
            $('#pa-manage-blocks-manage-plots-select-plots-'+sbId).prop('checked',true);
             $('#pa-manage-blocks-manage-plots-select-plots-'+sbId).attr('checked','checked');
            $('.pa-manage-plots-grid_select-'+sbId).attr('checked','checked');
            $('.pa-manage-plots-grid_select-'+sbId).prop('checked',true);
            $(".pa-manage-plots-grid_select-"+sbId+":checkbox").parent("td").parent("tr").addClass("grey lighten-4");
            $('#pa-manage-block-remove-plot-'+sbId).removeClass('disabled');
        }else{
            $('#pa-manage-blocks-manage-plots-select-plots-'+sbId).prop('checked',false);
             $('#pa-manage-blocks-manage-plots-select-plots-'+sbId).removeAttr('checked');
            $('.pa-manage-plots-grid_select-'+sbId).prop('checked',false);
            $('.pa-manage-plots-grid_select-'+sbId).removeAttr('checked');
            $("input:checkbox.pa-manage-plots-grid_select-"+sbId).parent("td").parent("tr").removeClass("grey lighten-4");
            $('#pa-manage-block-remove-plot-'+sbId).addClass('disabled');
        }
    });

    // click row in manage plots grid
    $(document).on('click',"#pa-manage-blocks-manage-plots-grid-"+subBlockId+" tbody tr",function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        this_row=$(this).find('input:checkbox')[0];
        var sbId = $(this_row).data('id');

        if(this_row.checked){
            this_row.checked=false;

            $('#pa-manage-blocks-manage-plots-select-plots-'+sbId).prop("checked", false);
            $('#pa-manage-blocks-manage-plots-select-plots-'+sbId).removeAttr('checked');
            $(this).removeClass("grey lighten-4");
            checkbox=$('.check-box-header-'+sbId).find('input:checkbox')[0];
            checkbox.checked = false;
        }else{
            $(this).addClass("grey lighten-4");
            this_row.checked=true;
        }

        if($('.pa-manage-plots-grid_select-'+sbId).is(':checked')){
            $('#pa-manage-block-remove-plot-'+sbId).removeClass('disabled');
        }else{
            $('#pa-manage-block-remove-plot-'+sbId).addClass('disabled');
        }
    });

    // remove plots in block
    $(document).on('click','#pa-manage-block-remove-plot-'+subBlockId,function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var obj = $(this);
        var sbId = obj.data('id');
        var subBlockName = obj.data('blockname');

        plotNos = [];
        $('input:checkbox.pa-manage-plots-grid_select-'+sbId).each(function(){
            if($(this).prop('checked') === true) {
                plotId = $(this).attr('data-plotno');
                plotNos.push(plotId);
            }
        });

        $('#pa-manage-block-remove-plot-modal-'+sbId).modal('show');

        plotCountRemove = plotNos.length;
        var ant = (plotCountRemove > 1) ? 'plots' : 'plot';
        var warningIcon = '<i class="material-icons orange-text left">warning</i>';
        var message = 'You are about to remove <b>'+plotCountRemove+'</b> '+ant+' in <b>'+subBlockName+'</b>. If you wish to proceed, number of rows and columns will be reset. Click "Confirm" to proceed.';
        $('.pa-manage-block-remove-plot-modal-body').html(warningIcon +' '+ message);

    });

    // confirm remove plot
    $(document).on('click','.pa-manage-block-remove-plot-confirm-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var obj = $(this);
        var sbId = obj.data('id');
        var subBlockName = obj.data('blockname');

        $('.pa-manage-block-remove-plot-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            type: 'post',
            url: '<?= $removePlotsUrl ?>',
            data: {
            blockId: sbId,
            plotNos: plotNos
        },
        success: function(data) {

            var hasToast = $('body').hasClass('toast');
            var ant = (plotCountRemove > 1) ? 'plots' : 'plot';

            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully removed <b>"+plotCountRemove+"</b> "+ant+" for <b>"+subBlockName+"</b>.</span>";
                Materialize.toast(notif, 5000);
            }

            // go to active tab
            $('.pa-manage-blocks-tab').click();
            $('.modal-backdrop').remove();
            $('body').removeClass('modal-open');
            $('#pa-manage-block-remove-plot-modal-'+sbId).modal('hide');

            if(managePlotsIsOpen) { // Close Manage Plots Browser
                $('.pa-manage-blocks-manage-plots-btn-'+subBlockId).trigger('click');
            }

            // show preview button
            $('.pa-manage-blocks-preview-panel-'+sbId).html(
                '<a type="button" class="grey lighten-3 black-text tabular-btn btn waves-effect waves-light pa-manage-blocks-preview-layout-btn-'+sbId+'" href="#" title="Preview layout" style="margin-right:5px;" data-id="'+sbId+'"><i class="material-icons right tabular-icon">search</i> Preview layout</a>'
            );

            $('.pa-manage-blocks-tab').click();

            },
            error: function() {}
        });
    });

</script>