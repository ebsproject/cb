<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders upload planting arrangement web form
 */
use kartik\widgets\FileInput;
use macgyer\yii2materializecss\widgets\Button;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$design = '';
$designInfo = '';

echo '<p>Choose and select the file to be uploaded. Required headers are Entry number and Plot number. <a href="#" class="download-file" id="pa-file"><b>Click here to download the template.</b></a></p>';
echo '<div id="download-link-div"></div>';
echo '<div class="col col-md-6 upload-panel" style="margin-top:15px">';

echo FileInput::widget([
    'name' => 'upload_pa_file',
    'pluginOptions' => [
        'uploadAsync' => false,
        'uploadUrl' => Url::to(['/experimentCreation/upload/pa-file', 'id'=>$id, 'program'=>$program]),
        'allowedFileExtensions' => ['csv'],
        'browseClass' => 'btn waves-effect waves-light browse-btn',
        'showCaption' => false,
        'showRemove' => false,
        'showZoom' => false,
        'browseLabel' => 'Browse',
        'removeLabel' => '',
        'uploadLabel' => 'Validate',
        'uploadTitle' => 'Validate file',
        'uploadIcon' => '<i class="material-icons inline-material-icon">file_upload</i>',
        'uploadClass' => 'btn',
        'removeClass' => 'btn btn-danger',
        'cancelClass' => 'btn grey lighten-2 black-text',
        'dropZoneTitle' => \Yii::t('app', 'Drag and drop file here or<br/>click Browse<br/><small>Use the planting arrangement files in CSV format. Specify required fields to validate.</small>'),
        'overwriteInitial' => true,
        'maxFileCount' => 1,
        'autoReplace' => true,
        'disabledPreviewTypes' => ['text'],
        'fileActionSettings' => [
            'showUpload' => false,
            'showZoom' => false,
            'removeIcon' => '<i class="fa fa-trash"></i>',
            'showRotate' => false,
            'indicatorNew' => '<i class="fa fa-plus-circle text-warning"></i>',
            'indicatorSuccess' => '<i class="fa fa-check-circle text-success"></i>',
            'indicatorError' => '<i class="fa fa-exclamation-circle text-danger"></i>',
            'indicatorLoading' => '<i class="fa fa-hourglass-half text-muted"></i>',
            'indicatorPaused' => '<i class="fa fa-pause-circle text-info"></i>',
        ],
        'theme'=>'fa'
    ],
    'pluginEvents' => [
        'fileclear' => 'function() { validate("clear"); }',
        'filereset' => 'function() { validate("test"); }',
        'fileloaded' => 'function() { validate(); }',
        'fileuploaderror' => 'function() { validate("clear"); }',
    ],
    'options' => [
        'multiple' => false,
        'accept' => 'csv/*',
        'id' => 'pa-file-file-input'
    ]
]);
echo '</div>';

echo '<div id="preview-panel" class="col col-md-5 pull-right hidden" style="margin-top:50px"></div>';
$submitPaFileUrl = Url::to(['upload/submit-pa-file', 'id'=>$id, 'program'=>$program, 'blockId'=> $blockId]);
$directDownloadUrl = Url::to(['/experimentCreation/upload/download-file']);

$this->registerJs(<<<JS
    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
    var validateSuccess = false;
    var tmpfilename = '';
    var uploadedFileContent = null;
    var totalPlots = 0;
    var information = null;

    // Validate if required fields specified upon file load
    function validate(action){
        // if file is cleared
        if(action == 'clear'){
            $('#preview-panel').html('');
            $('.confirm-upload-pa-file').addClass('hidden');
        }
    }

    // Remove file upon clicking re-upload
    $(document).on('click', '.browse-btn', function(e){
        // remove previously uploaded file
        $('.fileinput-remove').trigger('click');
        // change browse label
        $('.browse-btn span').text('Browse');
        $('#cancel-upload-pa-file').addClass('modal-close-button');
    });

    // Hide validate button upon remove of file
    $(document).on('click','.fileinput-remove, .kv-file-remove', function(e){
        $('#preview-panel').html('');
        $('.confirm-upload-pa-file').addClass('hidden');
        $('.validate-upload-pa-file').addClass('hidden');
    });

    // Cancel upload, reset modal content
    $(document).on('click', '#cancel-upload-pa-file', function(){
        $('.upload-pa-file-modal-body').html('');
    });

    // Hide validate button upon remove of file
    $(document).on('click','.confirm-upload-pa-file', function(e) {
        $('#upload-pa-file-modal').modal('show')
        var modalBody = '.upload-pa-file-modal-body';
        $(modalBody).html(loading);

        $.ajax({
            url: '$submitPaFileUrl',
            type: 'post',
            data:{
                'fileContent': JSON.stringify(uploadedFileContent)
            },
            success: function(response) {
            },
            error: function(err) {
            }
        });
    });

    // Download sample planting arrangement file
    $(document).on('click', '.download-file', function(){
        let path = '/modules/experimentCreation/views/planting-arrangement/';
        let filename = 'SamplePlantingArrangement.csv';
        if('$experimentType' == 'Observation') filename = 'SamplePlantingArrangement_Observation.csv';
        var a = document.createElement('a');
        a.setAttribute('href', '$directDownloadUrl'+'?path=$sampleFileDirectory'+path+'&filename='+filename);
        a.setAttribute('download', filename);

        var aj = $(a);
        aj.appendTo('download-link-div');
        aj[0].click();
        aj.remove();
    });

    // If validation is successful
    $('#pa-file-file-input').on('filebatchuploadsuccess', function(e, data) {
        var response = data.response;
        $('#preview-panel').html('');

        // if successfully validated, display preview
        if(response.success){
            var htmlString = '';
            validateSuccess = true;
            tmpfilename = response.fileName;
            uploadedFileContent = response.fileContent

            htmlString = '<div class="card-panel"><div class="row"> <h5 class="card-header">Summary Information</h5>';
            information = response.information;
            for (var prop in information) {
                if(prop == 'totalPlots'){
                    totalPlots = information[prop]['value'];
                }
                if(information[prop]['label'] != undefined){
                    htmlString += '<div>'+
                    '<div class="col col-md-8"><label>'+information[prop]['label']+'</label></div>'+
                    '<div class="col col-md-4 bold" style="word-wrap:break-word;">'+information[prop]['value']+'</div>'+
                    '</div>';
                }
            }

            htmlString += '<div class="clearfix"></div></div></div>';
            $('#preview-panel').html(htmlString);
            $('#preview-panel').removeClass("hidden");

            $('.confirm-upload-pa-file').removeClass('hidden');
            $('#cancel-upload-pa-file').removeClass('modal-close-button');
        }
    });
JS
);
?>

<style>
    #summarytable td,th {
        padding: 0px 0px !important;
        display: table-cell;
        text-align: left;
        vertical-align: middle;
        border-radius: 2px;
    }

    .card-header {
        margin-left: 11px;
        font-weight: bold;
    }

    .border-top {
        border-top: 1px solid #eee;
        padding-top: 14px;
        margin-top: 15px;
        margin-bottom: 10px;
    }
</style>