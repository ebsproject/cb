<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting arrangement manage nursery blocks and sub blocks
*/
use app\models\Variable;
use yii\helpers\Url;
use kartik\tabs\TabsX;
use app\models\Experiment;

$activeTabBlockId = 0;
$blocksDataArr = [];
// if there are no assigned entries yet
if(empty($blocksData)){
    echo '<div class="alert alert-warning" style="padding: 10px;">There are no added blocks yet. Please add first in "Add blocks" tab.</div>';
}else{
    $tabs = [];

    // build starting corner values
    $startingCornerValuesOpts = '';
    foreach ($startingCornerValues as $a => $b) {
        $startingCornerValuesOpts .= '<option value="'.$b.'">'.$b.'</option>';
    }

    // build plot numbering values
    $plotNumberingOrderValuesOpts = '';
    foreach ($plotNumberingOrderValues as $a => $b) {
        $plotNumberingOrderValuesOpts .= '<option value="'.$b.'">'.$b.'</option>';
    }

    // build dynamic entry list columns
    $varCols = [];
    foreach ($entryListVars as $key => $value) {
      $label = isset($value['field_label']) ? $value['field_label'] : Variable::getAttrByAbbrev($value['variable_abbrev']);

      $attribute = strtolower($value['variable_abbrev']);
      $varCols[] = [
          'attribute' => $attribute,
          'label' => '<span title="'.$label.'">'.$label.'</span>',
          'format' => 'raw',
          'encodeLabel'=>false,
          'filter'=>false
      ];
    }

    $blockButtonIds = [];

    // loop through each nursery blocks
    foreach ($blocksData as $key => $value) {
        $blockId = $value['id'];
        $code = $value['code'];
        $name = $value['name'];
        $idArr = $value['block_ids'];
        $codeArr = $value['block_codes'];
        $entryCountArr = $value['block_entry_counts'];
        $repArr = $value['block_reps'];
        $noOfPlotsArr =$value['total_no_of_plots_in_block'];
        $nameArr = $value['block_names'];
        $colsArr = $value['block_cols'];
        $rowsArr = $value['block_rows'];
        $plotNumberingArr = $value['block_plot_numbering_order'];
        $startingCornerArr = $value['block_starting_corner'];

        $noSub = false;
        if($code == $codeArr[0]){
            $noSub = true;
        }

        $needle = 'NB';
        $pos = strpos($code, $needle, 1);
        $label = substr($code, $pos);

        $blockParams = [];
        $subBlockTabs = [];
        $subBlockParams = [];

        // loop through each sub blocks
        foreach ($codeArr as $k => $v) {
            $sbName = $nameArr[$k];
            $sBlockTotalEntries = $entryCountArr[$k];
            $sBlockTotalPlots = $noOfPlotsArr[$k];

            $sBlockTotalEntries = ($sBlockTotalEntries == 'NULL') ? '0' : $sBlockTotalEntries;
            $sBlockTotalPlots = ($sBlockTotalPlots == 'NULL') ? '0' : $sBlockTotalPlots;
            $sBlockId = $idArr[$k];
            $plotNumberingVal = $plotNumberingArr[$k];
            $startingCornerVal = $startingCornerArr[$k];

            $blockParams = [
                'code'=>$v,
                'noSub'=>$noSub,
                'name'=>$name,
                'subBlockName'=>$sbName
            ];

            $needle = ($noSub) ? 'NB' : 'SB';
            $pos = strpos($v, $needle, 1);
            $sbLabel = substr($v, $pos);

            // set active tab
            if(empty($activeTab) && $key == 0 && $k == 0){
                Yii::$app->session->set('pa-manage-block-active-tab-'.$id, $sBlockId);
                $activeTabBlockId = $sBlockId;
            }else{
                $activeTabBlockId = Yii::$app->session->get('pa-manage-block-active-tab-'.$id);
            }

            $blockData = [
                'subBlockId'=>$sBlockId,
                'totalPlots'=>$sBlockTotalPlots,
                'code'=>$v,
                'params'=>$blockParams,
                'startingCornerValues'=>$startingCornerValuesOpts,
                'plotNumberingValues'=>$plotNumberingOrderValuesOpts,
                'varCols'=>$varCols,
                'id'=>$id,
                'activeTabBlockId'=>$activeTabBlockId,
                'experimentType' => $experimentType,
            ];

            $blocksDataArr['pa-sb-tabs-'.$sBlockId] = $blockData;
            $blockButtonIds[] = '#pa-sb-tabs-'.$sBlockId;

            $subBlockTabs[] = [
                'label'=>$sbLabel,
                'linkOptions'=>['title'=>$sbName,'id'=>'pa-sb-tabs-'.$sBlockId,'class'=>'pa-sb-tabs','data-id'=>$sBlockId],
                'active'=>($activeTabBlockId == $sBlockId) ? true : false,
                'encode'=>false,
                'content'=>''
            ];
        }

        $class = ($noSub) ? 'no-sub-block-a' : '';
        $defTabOpts = [
            'label'=>$label,
            'linkOptions'=>['class'=>$class,'data-id'=>$blockId,'id'=>'pa-manage-blocks-blocks-tab-'.$blockId],
            'items'=>$subBlockTabs
        ];

        $noSubTabOpts = ['headerOptions'=>['class'=>'hide-caret no-sub-block','data-id'=>$blockId,'title'=>$name]];

        $opts = ($noSub) ? array_merge($defTabOpts, $noSubTabOpts) : $defTabOpts;
        $tabs[] = $opts;
    }
    
    echo TabsX::widget([
        'items' => $tabs,
        'position'=>TabsX::POS_LEFT,
        'sideways'=>true,
        'encodeLabels' => false,
        'pluginOptions'=>[
            'enableCache' => false
        ],
        'enableStickyTabs' => true,
        'containerOptions'=>[
            'id' => 'manage-blocks-tab-container',
            'class'=>'pa-manage-blocks-tab'
        ]
    ]);
}

$blockButtonIds = !empty($blockButtonIds)? implode(',', $blockButtonIds): '';
$saveActiveTabToSessionUrl = Url::to(['/experimentCreation/planting-arrangement/manage-block-save-active-tab','id'=>$id]);
$refreshPreviewLayoutUrl = Url::to(['/experimentCreation/planting-arrangement/preview-layout-of-block','id'=>$id]);
$manageBlockTabUrl = Url::to(['/experimentCreation/planting-arrangement/manage-block-tab']);
?>
<script type="text/javascript">
    var blocksDataArr = <?php echo json_encode($blocksDataArr); ?>;
    var blockButtonIds = '<?= $blockButtonIds ?>';
    var activeBlock = <?= $activeTabBlockId ?>;
    var panelWidth = $('.pa-blocks-width-'+activeBlock).width() + 10;

    $(document).ready(function(){
        $('#change-design-switch-id').removeAttr('disabled')
    });
    
    $(blockButtonIds).on('click', function(e) {
        var blockId = $(this).data('id');
        var selectedBlockId = $(this).attr('id');

        // upon click of block, store active block to session
        $.ajax({
            url: '<?= $saveActiveTabToSessionUrl ?>',
            type: 'post',
            data:{
                blockId: blockId
            },
            async: false
        });

        // load block information
        $('#manage-blocks-tab-container').children().eq(1).html(`
            <div class="preloader-wrapper small active left" style="width:25px;height:25px;margin: 5px 10px;">
                <div class="spinner-layer spinner-green-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div> 
                    </div>
                </div>
            </div>&emsp;
        `);
        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: '<?= $manageBlockTabUrl ?>',
                data: {
                    blockData: JSON.stringify(blocksDataArr[selectedBlockId])
                },
                async: false,
                success: function(data) {
                    $('#manage-blocks-tab-container').children().eq(1).html(data);
                    previewLayout(blockId);
                }
            });
        },100);
    });

    // trigger the current block
    $('#pa-sb-tabs-'+activeBlock).trigger('click');

    // upon click on nursery block that do not have sub blocks
    $(document).on('click','.no-sub-block-a',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var blockId = $(this).data('id');

        $('#pa-sb-tabs-'+blockId).click();

    });

    // preview layout upon load
    function previewLayout(blockId){
        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: '<?= $refreshPreviewLayoutUrl?>',
                data: {
                    blockId: blockId,
                    panelWidth: panelWidth
                },
                async: false,
                success: function(data) {
                    if(data == '') {
                        data = '<a type="button" class="grey lighten-3 black-text tabular-btn btn waves-effect waves-light pa-manage-blocks-preview-layout-btn-'+blockId+'" href="#" title="Preview layout" style="margin-right:5px;" data-id="'+blockId+'"><i class="material-icons right tabular-icon">search</i> Preview layout</a>'
                    }
                    $('.preloader-wrapper').addClass('hidden');
                    $('.pa-manage-blocks-preview-panel-'+blockId).html(data);
                }
            });
        },100);
    }

</script>
<style type="text/css">
    .hide-caret .caret, .hide-caret .dropdown-menu{
        display: none;
    }
    .pa-manage-blocks-tab .dropdown-menu{
        min-width: 70px !important;
    }
    .tabs-krajee.tab-sideways .nav-tabs > li {
        height: 20px;
        width: 70px;
        margin-bottom: 50px;
    }
    .tabs-krajee.tabs-left.tab-sideways .nav-tabs {
        left: -40px;
        margin-right: -85px;
        z-index: 997 !important;
    }
    .tab-sideways .nav-tabs {
        margin-top: 25px;
    }
    .tabs-left .tab-content {
        border-left: none;
    }
    .tabs-krajee.tabs-left.tab-sideways .tab-content {
        margin-left: 26px;
    }
</style>