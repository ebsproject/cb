    <?php
    /*
    * This file is part of EBS-Core Breeding.
    *
    * EBS-Core Breeding is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * EBS-Core Breeding is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
    */

    /**
     * Renders variables per data level in remove operational data
     */

    use yii\helpers\Html;
    use yii\helpers\Url;
    use kartik\dynagrid\DynaGrid;
    use app\modules\experimentCreation\models\PlantingArrangement;

    // blocks browser columns
    $columns = [
        [
            'label' => "",
            'header' => '',
            // temporarily disable select all for blocks
            // 'header' => '
            // <input type="checkbox" class="filled-in" id="pa-select-entries-assign-block"/>
            // <label for="pa-select-entries-assign-block" id="label-for-pa-select-entries-assign-block" style="height: 11px;"></label>
            // ',
            'contentOptions' => ['style' => ['max-width' => '100px;',]],
            'content' => function ($data) {
              return '
              <input class="pa-select-entries-assign-block-grid_select filled-in" type="radio" id="' . $data["experimentBlockDbId"] . '" data-entcount="' . $data['entryCount'] . '" />
              <label for="' . $data["experimentBlockDbId"] . '"></label>';
            },
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'mergeHeader' => true,
            'width' => '50px'
        ],
        [
            'attribute' => 'experimentBlockCode',
            'enableSorting' => false,
            'label' => 'Block Code',
        ],
        [
            'attribute' => 'experimentBlockName',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'blockType',
            'format' => 'raw',
            'contentOptions' => function ($data) {
                if (isset($data['type']) && $data['type'] == 'Parent block') {
                    return ['class' => 'amber lighten-4'];
                } else {
                    return ['class' => 'green lighten-4'];
                }
            }
        ],
        [
            'attribute' => 'entryCount',
            'format' => 'raw',
            'label' => 'No. of entries',
            'contentOptions' => function ($data) {
                if ((isset($data['cols']) && $data['cols'] == 0) || (isset($data['entryCount']) && $data['entryCount'] == null)) {
                   return ['class' => 'red lighten-4'];
                } else if (isset($data['rowGroup']) && $data['rowGroup'] % 2) {
                   return ['class' => 'light-green lighten-4'];
                } else {
                   return ['class' => 'teal lighten-4'];
                }
            },
            'value' => function ($data) {
                $entryCount = $data['entryCount'] == null ? 0 : $data['entryCount'];
                  return $entryCount;
            }
        ],
        [
          'attribute' => 'plotCount',
          'format' => 'raw',
          'label' => 'No. of plots',
          'value' => function ($data) {
              $plotCount = $data['plotCount'] == null ? 0 : $data['plotCount'];
                return $plotCount;
          },
          'contentOptions' => function ($data) {
              if ((isset($data['plot_count']) && $data['plot_count'] == 0) || (isset($data['plot_count']) && $data['plot_count'] == null)) {
                return ['class' => 'red lighten-4'];
              } else if (isset($data['rowGroup']) && $data['rowGroup'] % 2) {
                return ['class' => 'light-green lighten-4'];
              } else {
                return ['class' => 'teal lighten-4'];
              }
          }
        ]
    ];
    $entryLabel = ($entryIdsCount > 1) ? 'entries' : 'entry';
    $panel = [
      'heading' => false,
      'before' => \Yii::t('app', ' Select only one block where you want to assign ') . '<b class="green-text text-darken-3">' . $entryIdsCount . '</b> selected ' . $entryLabel . '. {summary}',
      'after' => false,
      'footer' => false
    ];

    // blocks browser
    DynaGrid::begin([
      'options' => ['id' => 'pa_assign-entries-blocks-grid', 'style' => 'margin-right:-10px;'],
      'columns' => $columns,
      'theme' => 'simple-default',
      'showPersonalize' => false,
      'storage' => 'cookie',
      'showFilter' => false,
      'showSort' => false,
      'allowFilterSetting' => false,
      'allowSortSetting' => false,
      'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'pjax' => true,
        'pjaxSettings' => [
          'options' => [
            'enablePushState' => false,
            'id' => 'pa_assign-entries-blocks-grid'
          ],
        ],
        'panel' => $panel,
        'responsiveWrap' => false,
        'showPageSummary' => false,
        'toolbar' =>  false,
        'rowOptions' => function ($data) {
          if (isset($data['rowGroup']) && $data['rowGroup'] % 2) {
            return ['class' => 'light-green lighten-5'];
          } else {
            return ['class' => 'teal lighten-5'];
          }
        },
        'floatHeader' => true,
        'floatOverflowContainer' => true
      ]
    ]);
    DynaGrid::end();

    $varCountLimit = (count($variables)) / 2;
    $counter = 0;
    $colStr1 = '';
    $colStr2 = '';

    // total number of plots in block
    $totalPlotsFieldStr = Html::input('number', 'pa-block-params-TOTAL_NO_OF_PLOTS_IN_BLOCK', $totalPlotsInBlock, [
        'class' => 'pa-block-params pa-param-compute input-integer pa-block-params-TOTAL_NO_OF_PLOTS_IN_BLOCK',
        'data-abbrev' => 'TOTAL_NO_OF_PLOTS_IN_BLOCK',
        'min' => 0,
        'disabled' => true
    ]);
    $fieldLabel = '<div class="col col-md-4"><label class="control-label" title="Total number of plots in block">Total no of plots in block</label></div>';
    $inputField = '<div class="col col-md-8 pa-block-params-field">';
    $totalPlotsFieldStr = '<div class="row" style="margin-bottom:0px">' . $fieldLabel . $inputField . $totalPlotsFieldStr . '</div>' . '</div>';
    $colStr1 .= $totalPlotsFieldStr;
    $counter++;

    // render block parameters form
    foreach ($variables as $key => $value) {
        $data = \Yii::$container->get('app\modules\experimentCreation\models\PlantingArrangement')->getFormDisplay($value['abbrev']);

        $dataType = $value['dataType'];
        $scaleValues = $value['scaleValues']; //substr($value['scaleValues'], 1, -1);
        $abbrev = $value['abbrev'];
        $label = ucfirst(strtolower($data['name']));
        $style = '';
        $styleNoMargin = ' style="margin-bottom:0px" ';
        $defVal = ($abbrev == 'NO_OF_REPS_IN_BLOCK') ? 1 : '';
        if($abbrev == 'NO_OF_REPS_IN_BLOCK') continue;

        // if has scale values
        if (!empty($scaleValues)) {
            $options = '<select data-row="' . $key . '" class="pa-block-params browser-default" data-abbrev="' . $abbrev . '" style="height:auto;">';
            $values = explode(',', $scaleValues);

            foreach ($values as $k => $v) {

              $firstCharacter = substr($v, 0, 1);
              if ($firstCharacter == '"') {
                $v = substr($v, 1, -1);
              }
              $options .= '<option value="' . $v . '">' . $v . '</option>';
            }

            $options .= '</select>';

            $fieldStr = '<div class="pa-block-params-scale-values">' . $options . '</div>';
        } else if (in_array($dataType, ['integer', 'numeric', 'double', 'number'])) {
            $style = $styleNoMargin;
            $fieldStr = Html::input('number', 'pa-block-params-' . $abbrev, $defVal, ['class' => 'pa-block-params pa-param-compute input-integer pa-block-params-' . $abbrev, 'data-abbrev' => $abbrev, 'min' => 1]);
        } else {
            $style = $styleNoMargin;
            $fieldStr = Html::input('text', 'pa-block-params-' . $abbrev, $defVal, ['class' => 'pa-block-params', 'data-abbrev' => $abbrev]);
        }

        $fieldLabel = '<div class="col col-md-4"><label class="control-label" title="' . $data['description'] . '">' . $label . '</label></div>';

        $inputField = '<div class="col col-md-8 pa-block-params-field">';
        $fieldStr = '<div class="row" ' . $style . '>' . $fieldLabel . $inputField . $fieldStr . '</div>' . '</div>';
        if ($counter < $varCountLimit) {
            $colStr1 .= $fieldStr;
        } else {
            $colStr2 .= $fieldStr;
        }

        $counter++;
    }

    $instruction = '<br/><p>Specify details for <b id="pa-selected-blocks-count" class="red-text text-darken-3">0</b> selected <span id="block-label">block</span>. Below details will be applied for all selected blocks without assigned entries. All fields are required.</p>';
    $instruction = $isSelectedRemainingReps ? $instruction . '<p style="color:red">Note: You will be assigning the remaining replicates to the selected block. After all replicates have been assigned, no more replicates can be assigned to a block.</p>' : $instruction;

    echo '<div class="row pa-block-params-panel">' . $instruction . '<div class="col col-md-6">' . $colStr1 . '</div>';
    echo '<div class="col col-md-6">' . $colStr2 . '</div></div>';
    echo '<div class="row pa-block-params-instructions-panel"></div>';

    $saveBlockParamsUrl = Url::to(['/experimentCreation/planting-arrangement/save-block-params', 'id' => $id]);
    $emptySelectedItemsUrl = Url::to(['/experimentCreation/planting-arrangement/empty-user-selection', 'id' => $id]);
    $entryIds = json_encode($entryIds);
    $entryListIdList = json_encode($entryListIdList);
    $initialLayoutOrderData = json_encode($initialLayoutOrderData);

    $this->registerJs(<<<JS
    var blockIds = []; // block ids
    var entryIds = JSON.parse('$entryIds');
    var values = {};
    var totalEntries = $entryIdsCount;
    var isValid = false;
    var entryListIdList = '$entryListIdList';
    var initialLayoutOrderData = '$initialLayoutOrderData';
    var assignBlockStatus = 'overwrite';

    check();
    // check row in lists browser
    $("#pa_assign-entries-blocks-grid tbody tr").on('click',function(e){
        // temporary: remove other selected so user would only be able to select 1 block
        $("input:radio.pa-select-entries-assign-block-grid_select").each(function(){
            if($(this).prop("checked") === true)  {
                $(this).prop("checked",false)
                $('#pa-select-entries-assign-block').prop("checked", false);
                $(this).parent().parent().removeClass("grey lighten-4");
            }
        });

        this_row=$(this).find('input:radio')[0];
        if(this_row.checked){
            this_row.checked=false; 
            $('#pa-select-entries-assign-block').prop("checked", false);
            $(this).removeClass("grey lighten-4");
        }else{
            $(this).addClass("grey lighten-4");
            this_row.checked=true;  
            $("#"+this_row.id).prop("checked");
        }

        getSelectedBlocks();
        check();
        return false;
    });

    //For blocks with assigned entries
    $(document).on('change',".assign-block-status-switch",function() {
        var blockStatus = $(this).prop('checked');
        
        if(blockStatus){
            assignBlockStatus ='append';
        }else{
            assignBlockStatus ='overwrite';
        }
    });

    var values = {};
    // assign entries confirm
    $(document).on('click','.pa-assign-entries-save-btn', function(e){
        var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
        
        e.preventDefault();
        e.stopImmediatePropagation();
        var values = {};
        var blockIds = getSelectedBlocks('ids');
    
        $(".pa-block-params").each(function( index ) {
            var obj = $(this);
            var attr = obj.data('abbrev');

            var value = obj.val();
            values[attr] = value;
        });

        var blockCount = blockIds.length;
        var checkIcon = '<i class="material-icons green-text left">check_circle</i>';

        var entriesStr = (totalEntries > 1) ? ' entries ' : ' entry ';
        var blocksStr = (blockCount > 1) ? ' blocks' : ' block';
        var successMessage = checkIcon + ' Successfully assigned <b>'+totalEntries+'</b>'+entriesStr+'to <b>'+blockCount+'</b>'+blocksStr+'.';

        $('.pa-assign-entries-modal-body').html(loading);
        $('.pa-assign-entries-save-btn').attr('disabled','disabled');

        $.ajax({
            type: 'post',
            url: '$saveBlockParamsUrl',
            cache: false,
            data: {
                blockIds: JSON.stringify(blockIds),
                entryIds: JSON.stringify(entryIds),
                entryListIdList: entryListIdList,
                initialLayoutOrderData: initialLayoutOrderData,
                params: values,
                assignBlockStatus: assignBlockStatus
            },
            success: function(data) {
                $('.pa-assign-entries-save-btn').removeAttr('disabled','disabled');
                $('.pa-assign-entries-modal-body').html(successMessage);
                $('.pa-assign-entries-assign-more-btn').removeClass('hidden');
                $('.pa-assign-entries-manage-block-btn').removeClass('hidden');
                $('#pa-assign-entries-modal-cancel-btn').addClass('hidden');
                $('.pa-assign-entries-save-btn').addClass('hidden');
                $('#selected-count').html('0');

                $('#reset-working-list-grid').trigger('click');
                emptySessionUserSelection();
            },
            error: function() {}
        });
    });

    // validate 
    $(document).on('propertychange input','.pa-param-compute',validate);

    // if number of row is changed
    $(document).on('propertychange input','.pa-block-params-NO_OF_ROWS_IN_BLOCK',function(){
        var obj = $(this);
        // number of rows
        var rows = parseInt(obj.val());

        var totalPlots = $('.pa-block-params-TOTAL_NO_OF_PLOTS_IN_BLOCK').val();

        $('.red-text').remove();
        obj.removeClass('invalid');
        $('.pa-block-params-NO_OF_ROWS_IN_BLOCK').removeClass('invalid');
        $('.pa-block-params-NO_OF_COLS_IN_BLOCK').removeClass('invalid');

        if(rows > totalPlots){
            obj.focus();
            obj.addClass('invalid');
            obj.after("<span class='red-text'>Value must be <= "+totalPlots+"</span>");
        }else if(rows < 1){
            obj.focus();
            obj.addClass('invalid');
            obj.after("<span class='red-text'>Value must be > 0</span>");
        }else{
            $('.red-text').remove();
            obj.removeClass('invalid');
        }

        var column = Math.ceil(totalPlots/rows);
        $('.pa-block-params-NO_OF_COLS_IN_BLOCK').val(column);
        check();
    });

    // if number of columns is changed
    $(document).on('propertychange input','.pa-block-params-NO_OF_COLS_IN_BLOCK',function(){
        var obj = $(this);
        // number of rows
        var cols = parseInt(obj.val());

        var totalPlots = $('.pa-block-params-TOTAL_NO_OF_PLOTS_IN_BLOCK').val();

        $('.red-text').remove();
        obj.removeClass('invalid');
        $('.pa-block-params-NO_OF_ROWS_IN_BLOCK').removeClass('invalid');
        $('.pa-block-params-NO_OF_COLS_IN_BLOCK').removeClass('invalid');

        if(cols > totalPlots){
            obj.focus();
            obj.addClass('invalid');
            obj.after("<span class='red-text'>Value must be <= "+totalPlots+"</span>");
        }else if(cols < 1){
            obj.focus();
            obj.addClass('invalid');
            obj.after("<span class='red-text'>Value must be > 0</span>");
        }else{
            $('.red-text').remove();
            obj.removeClass('invalid');
        }

        var row = Math.ceil(totalPlots/cols);
        $('.pa-block-params-NO_OF_ROWS_IN_BLOCK').val(row);
        check();
    });

    // checks if value greater than 0
    function validate(e){
        var obj = $(this);
        var value = obj.val();

        $('.red-text').remove();
        obj.removeClass('invalid');
        if(value < 1){
            obj.addClass('invalid');
            obj.focus();
            obj.after("<span class='red-text'>Value must be > 0</span>");
        }
    }

    // checks if all fields are specified
    function check(){
        var hasError = $('.pa-assign-entries-modal-body').hasClass('invalid');
        var error = [];
        $(".invalid").each(function(){
            error.push(1);
        });

        var blockCounter = getSelectedBlocks();
        var rows = $('.pa-block-params-NO_OF_ROWS_IN_BLOCK').val();
        var cols = $('.pa-block-params-NO_OF_ROWS_IN_BLOCK').val();
     
        // checks if all are valid
        if( (rows && cols && blockCounter && !error.length) || isValid){
            $('.pa-assign-entries-save-btn').removeClass('disabled');
        }else{
            $('.pa-assign-entries-save-btn').addClass('disabled');
        }
    }

    // get selected blocks
    function getSelectedBlocks(returnVal='count'){
        var blockIds = [];
        var entryCounts = [];
        $("input:radio.pa-select-entries-assign-block-grid_select").each(function(){
            if($(this).prop("checked") === true)  {
                var entCount = $(this).data('entcount');
                if(entCount > 0){
                   entryCounts.push(entCount);
                }

                blockId = $(this).attr("id");
                blockIds.push(blockId);
            }
        });

        var blockCount = blockIds.length;
        var blockLabel = (blockCount > 1) ? 'blocks' : 'block';

        if(blockCount == 0){
            $('#pa-selected-blocks-count').removeClass('green-text');
            $('#pa-selected-blocks-count').addClass('red-text');
        }else{
            $('#pa-selected-blocks-count').removeClass('red-text');
            $('#pa-selected-blocks-count').addClass('green-text');
        }

        $('#pa-selected-blocks-count').html(blockCount);
        $('#block-label').html(blockLabel);

        var entryCount = entryCounts.length;
        // all selected blocks have assigned entries
        if(entryCount > 0 && entryCounts.length == blockCount){
            $('.pa-block-params-panel').addClass('hidden');
            var selectRadio = '<div style="float:left;margin-bottom:5px;"><label style="font-color:black;">For blocks with assigned entries: </label>&nbsp;&nbsp;&nbsp;<div class="switch" style="display:inline"><label><span >Overwrite</span><input type="checkbox" class="assign-block-status-switch" id="block-radio-btn"><span class="lever"></span><span >Append</span></label></div></div>';
            var instructions = '<br/><div class="alert alert-warning" style="padding: 10px">All selected blocks have assigned entries. If you wish to proceed, number of rows and columns will be reset for the selected blocks. You can modify them in "Manage blocks" tab.</div>'+
                                '<br/>'+selectRadio;
            
            $('.pa-block-params-instructions-panel').html(instructions);
            isValid = true;
        }else if(entryCount > 0){ // if has selected block with assigned entries
            $('.pa-block-params-panel').removeClass('hidden');
            var count = (entryCount > 1) ? 'are <b>'+entryCount+'</b> selected blocks that have' : 'is <b>'+entryCount+'</b> selected block that has';
            var selectRadio = '<div style="float:left;margin-bottom:5px;"><label style="font-color:black;">For blocks with assigned entries: </label>&nbsp;&nbsp;&nbsp;<div class="switch" style="display:inline"><label><span >Overwrite</span><input type="checkbox" class="assign-block-status-switch" id="block-radio-btn"><span class="lever"></span><span >Append</span></label></div></div>';
            var instructions = '<br/><div class="alert alert-warning" style="padding: 10px">There '+count+' assigned entries. If you wish to proceed, number of rows and columns for selected blocks with assigned entries will be reset. You can modify them in "Manage blocks" tab.</div>'+
                                '<br/>'+selectRadio;
            $('.pa-block-params-instructions-panel').html(instructions);
            isValid = false;
        }else{
            $('.pa-block-params-panel').removeClass('hidden');
            $('.pa-block-params-instructions-panel').html('');
            isValid = false;
        }
   

        return (returnVal == 'count') ? blockCount : blockIds;
    }
    // Temporarily disabled: select all variables
    // $(document).on('click','#pa-select-entries-assign-block',function(e){
    //     if($(this).prop("checked") === true)  {
    //         $(this).attr('checked','checked');
    //         $('.pa-select-entries-assign-block-grid_select').attr('checked','checked');
    //         $('.pa-select-entries-assign-block-grid_select').prop('checked',true);
    //         $(".pa-select-entries-assign-block-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
    //     }else{
    //         $(this).removeAttr('checked');
    //         $('.pa-select-entries-assign-block-grid_select').prop('checked',false);
    //         $('.pa-select-entries-assign-block-grid_select').removeAttr('checked');
    //         $("input:checkbox.pa-select-entries-assign-block-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
    //     }
    //     getSelectedBlocks();
    //     check();
    // });

          //Assign null value to the user session's selection from add to list
        function emptySessionUserSelection(){
            var empty = null;

            $.ajax({
                type: 'POST',
                url: '$emptySelectedItemsUrl',
                data: {
                    selectedItems:empty
                },
                dataType: 'json',
                success: function(response) {
                    $('#reset-working-list-grid').trigger('click');
                }
            }); 
        }

    JS);
    ?>

    <style type="text/css">
      #pa_assign-entries-blocks-grid-pjax .kv-grid-wrapper {
        max-height: 200px;
      }

      #pa_assign-entries-blocks-grid .table>tbody>tr>td {
        border-top: transparent;
      }

      #pa_assign-entries-blocks-grid td,
      th {
        border-radius: 0px !important;
      }

      .grey.lighten-4,
      .grey.lighten-4 td {
        background-color: #f5f5f5 !important;
      }
      .pa-block-params-panel{
        margin-bottom: 5px;
        margin-top: -10px;
      }
    </style>