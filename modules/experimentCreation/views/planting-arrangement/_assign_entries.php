<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders planting arrangement assign parents
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Variable;
use kartik\dynagrid\DynaGrid;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use app\modules\experimentCreation\models\PlantingArrangement;

use yii\web\JsExpression;

// render filter form
// first columns
$col1Str = '<div class="col col-md-4"><label class="control-label">' . \Yii::t('app', 'Entry no.') . '</label></div>';

$col1Str .= '<div class="col col-md-8 search-input-field" style="padding-left:0">' .
    '<div class="col col-md-5">' .
    Select2::widget([
        'id' => 'pa_select_entryNumber_operator',
        'name' => 'pa_data_filter_entryNumber_operator',
        'maintainOrder' => true,
        'data' => [
            'range' => 'range',
            '>' => '>',
            '>=' => '>=',
            '<' => '<',
            '<=' => '<=',
            '=' => '='
        ],
        'value' => 'range',
        'options' => [
            'placeholder' => 'Select operator',
            'tags' => true,
        ]
    ]) .
    '</div>' .
    '<div class="col col-md-7" style="padding:0">' .
    Html::input('number', 'filter-entryNumber-number', '', ['class' => 'pa-filter-entryNumber-field input-integer hidden', 'min' => 0]) .
    '<span class="entryNumber-range-panel">' .
    Html::input('number', 'filter-entryNumber-number-from', '', ['class' => 'pa-filter-entryNumber-field-from input-integer', 'autofocus' => true, 'min' => 1, 'style' => 'width:45%']) . 'to ' .
    Html::input('number', 'filter-entryNumber-number-to', '', ['class' => 'pa-filter-entryNumber-field-to input-integer', 'min' => 1, 'style' => 'width:45%']) .
    '</span>' .
    Html::input('text', 'filter-entryNumber', '', ['class' => 'pa-filter-field hidden', 'data-attr' => 'entryNumber', 'data-isvar' => 'false']) .
    '<span class="entryNumber-multiple-panel hidden">' .
    Select2::widget([
        'id' => 'pa_select_entryNumber',
        'name' => 'pa_data_filter_entryNumber',
        'maintainOrder' => true,
        'options' => [
            'placeholder' => 'Select entry number',
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'multiple' => true
        ]
    ]) . '</span>' .
    '</div>'
    . '</div>';

// column 2
$col2Str = '<div class="col col-md-4"><label class="control-label">' . \Yii::t('app', 'Germplasm Name') . '</label></div>';
$col2Str .= Html::input('text', 'filter-designation', '', ['class' => 'pa-filter-field hidden', 'name'=>'designation','data-attr' => 'designation', 'data-isvar' => 'false']);

$dataUrl = Url::home() . '/experimentCreation/' . Url::to('planting-arrangement/get-filter-data');

//Notification
echo '<div id="page-notif" class="hidden"></div>';

$pluginOptions = [
    'allowClear' => true,
    'minimumInputLength' => 0,
    'placeholder' => 'Select Germplasm',
    'minimumResultsForSearch' => 1,
    'language' => [
        'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
     ], 'ajax' => [
            'url' => $dataUrl,
            'dataType' => 'json',
            'delay' => 250,
            'data' => new JsExpression(
                'function (params){
                    var entryListId = "'.$entryListId.'";
                    var attr = $(this).attr("data-attr");
                    return {
                        q: params.term, 
                        attr: attr,
                        page: params.page,
                        listId: entryListId,
                        id: "'.$id.'"
                    }
                }'
            ),
            'processResults' => new JSExpression(
                'function (data, params) {
                    
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                        more: (params.page * 5) < data.totalCount
                        },
                    };
                }'
            ),
            'cache' => true
        ]
];
$col2Str .= '<div class="col col-md-8 search-input-field">' . Select2::widget([
    'id' => 'pa_select_designation',
    'name' => 'pa_data_filter_designation',
    'maintainOrder' => true,
    'options' => [
        'tags' => true,
        'multiple' => true,
        'style' => 'width:100%',
        'data-attr' => 'designation',
        'minimumResultsForSearch' => 1,
    ],
    'pluginOptions' => $pluginOptions,
    'pluginEvents' => [
        "change" => "
            function(){
                if($(this).val()!=''){
                    var value = $(this).val()+'';
                    var values = value.split(',');
                    var val = '', counter = 1;

                    $.each(values,function(i,v){ 
                        val = val+v;
                        
                        if(counter < values.length){
                            val = val+',';
                        }
                        counter += 1;
                    })
                    $('#pa_select_designation'+'-hidden').val(val);
                }else{
                    $('#pa_select_designation'+'-hidden').val($(this).val());
                }
            }",
        ],
        'showToggleAll' => false
]) . '</div>';

$varCountLimit = (count($entryListVars)) / 2;
$counter = 0;
$isDisplay = true;

foreach ($entryListVars as $key => $value) {
    if (!isset($value['is_hidden'])) {
        $data = \Yii::$container->get('app\modules\experimentCreation\models\PlantingArrangement')->getFormDisplay($value['variable_abbrev']);

        // if Generation Nursery experiment, hide parent type
        if (strtoupper($value['variable_abbrev']) == 'PARENT_TYPE' && $processName == 'GENERATION_NURSERY_DATA_PROCESS') {
            continue;
        }

        $label = isset($data['name']) ? $data['name'] : \Yii::$container->get('app\models\Variable')->getAttrByAbbrev($value['variable_abbrev']);
        // display in first column
        if ($counter <= $varCountLimit) {
            $col1Str .= '<div class="col col-md-4"><label class="control-label" title="' . $data['description'] . '">' . $label . '</label></div>';
            $col1Str .= Html::input('text', 'filter-' . $value['variable_abbrev'], '', ['class' => 'pa-filter-field hidden', 'data-attr' => $value['variable_abbrev'], 'data-isvar' => 'true']);
            $col1Str .= '<div class="col col-md-8 search-input-field">' . Select2::widget([
                'id' => 'pa_select_' . $value['variable_abbrev'],
                'name' => 'pa_data_filter_' . $value['variable_abbrev'],
                'maintainOrder' => true,
                'options' => [
                    'placeholder' => 'Select ' . $label,
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'multiple' => true
                ]
            ]) . '</div>';
        } else {
            $col2Str .= '<div class="col col-md-4"><label class="control-label">' . $label . '</label></div>';
            $col2Str .= Html::input('text', 'filter-' . $value['variable_abbrev'], '', ['class' => 'pa-filter-field hidden', 'data-attr' => $value['variable_abbrev'], 'data-isvar' => 'true']);
            $col2Str .= '<div class="col col-md-8 search-input-field">' . Select2::widget([
                'id' => 'pa_select_' . $value['variable_abbrev'],
                'name' => 'pa_data_filter_' . $value['variable_abbrev'],
                'maintainOrder' => true,
                'options' => [
                    'placeholder' => 'Select ' . $label,
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'multiple' => true
                ]
            ]) . '</div>';
        }


        $attribute = strtolower($value['variable_abbrev']);
        if (strpos($attribute, '_') !== false) {
            $attribute = ucwords(strtolower($attribute), '_');
            $attribute = str_replace('_', '', $attribute);
            $attribute = lcfirst($attribute);
        }

        $varCols[] =
            [
                'attribute' => $attribute,
                'label' => '<span title="' . $label . '">' . $label . '</span>',
                'format' => 'raw',
                'encodeLabel' => false,
                'filter' => false
            ];

        $counter++;
    }
}

// checkbox whether to include already added entries or not
$col2Str .= '<div class="col col-md-4"></div><div class="col col-md-8"><input type="checkbox" checked id="pa_select_already_used" class="pa-filter-field-boolean" value="1" style="opacity:0"/>
  <label for="pa_select_already_used">' . \Yii::t('app', 'Include entries already used in a block?') . '</label></div>';

$col1Str .= '<div class="col col-md-4"></div><div class="col col-md-8">' . Html::a('Submit', '#', ['style' => 'margin-right:5px;', 'type' => 'button', 'title' => \Yii::t('app', "Search entries"), 'class' => 'tabular-btn btn waves-effect waves-light', 'id' => 'pa-search-entries-btn']) .
    Html::a('Reset all', '#', ['style' => 'margin-right:5px;', 'type' => 'button', 'title' => \Yii::t('app', "Reset filters"), 'class' => 'tabular-btn btn waves-effect waves-light black-text grey lighten-2', 'id' => 'pa-search-entries-reset-btn']) .
    '</div>';
?>

<ul class="collapsible">
    <li id="collapsible-flag" class="collapsible-search-filter">
        <div class="collapsible-header active" id="search-filter"><i class="material-icons">search</i>Filter entries</div>
        <div class="collapsible-body">
            <?php
            echo '<div class="row"><div class="col col-md-6">' . $col1Str . '</div>';
            echo '<div class="col col-md-6">' . $col2Str . '</div></div>';
            ?>
        </div>
    </li>
</ul>

<?php
// if filters are set
$selectedItems = isset($selectedItems) ? $selectedItems : [];
$selectedItemsCount = count($selectedItems) > 0 ? count($selectedItems) : 'No';
$columns = [
    [
        'label' => "Checkbox",
        'header' => '
            <input type="checkbox" class="filled-in" id="pa-select-entries-all"/>
            <label for="pa-select-entries-all" id="label-for-pa-select-entries-all" style="height: 11px;"></label>',
        'content'=>function($data) use ($selectedItems){
            $entryDbId = $data['entryDbId'];

            if(in_array($entryDbId, $selectedItems)){
                $checkbox = '<input class="pa-entries-grid_select filled-in" type="checkbox" checked="checked" id="'.$data['entryDbId'].'" />';
            }
            else{
                $checkbox = '<input class="pa-entries-grid_select filled-in" type="checkbox" id="'.$data['entryDbId'].'" />';
            }
            return $checkbox.'
                <label for="'.$data['entryDbId'].'"></label>       
            ';
        },  
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'visible' => true,
        'hidden' => false
    ],
    [
        'attribute' => 'entryNumber',
        'label' => '<span title="Sequential numbering of the entries">' . Yii::t('app', 'Entry No.') . '</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'visible' => true,
        'hidden' => false
    ],
    [
        'attribute' => 'designation',
        'label' => '<span title="Germplasm">' . Yii::t('app', 'Germplasm Name') . '</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'visible' => true,
        'hidden' => false
    ],
    [
        'label' => '<span title="Number of Replications per entry">' . Yii::t('app', 'No. of Reps') . '</span>',
        'format' => 'raw',
        'content'=>function($data) use ($entryReplicates){
            $notSet = '<span class="not-set">(not set)</span>';
            $value = isset($entryReplicates[$data['entryDbId']]) ? $entryReplicates[$data['entryDbId']]["repno"] : 1;
            
            $input = Html::input('number', 'entryRepField', $value, ['id'=>'entryRep-'.$data['entryDbId'],'class' => 'entryRep-class numeric-input-validate entry-class-integer', 'autofocus' => true, 'min' => 1, 'style' => 'width:100%', 'data-id' => $data['entryDbId'], 'data-attribute' => 'entryRep']);
            return $input;
        },
        'encodeLabel' => false,
        'visible' => true,
        'hidden' => false,
    ],
    [
        'attribute' => 'parentage',
        'label' => '<span title="Pedigree Information">' . Yii::t('app', 'Pedigree') . '</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'filter' => true,
        'visible' => true,
        'hidden' => false,
        'value' => function ($data) {
            $parentage = $data['parentage'];
            $strLen = strlen($parentage);
            return ($strLen > 45) ? '<span title="' . $parentage . '">' . substr($parentage, 0, 45) . '...' : $parentage;
        }
    ],

];

if (!empty($varCols)) {
    $gridColumns = array_merge($columns, $varCols);
} else {
    $gridColumns = $columns;
}
?>
<div class="entries-result col-md-12">
    <ul class="collapsible collapsible-result-browser" id="collapsible-result-browser">
        <li class="result-browser">
            <div class="collapsible-header" id="result-browser"><i class="material-icons">list</i>Entries</div>
            <div class="collapsible-body" id="query-results">
                <?php
                    $selectedEntryIdsCount = empty($selectedItems) ? 'No' : count($selectedItems);
                    // render data browser
                    DynaGrid::begin([
                            'options' => ['id' => 'pa-search-entries-grid', 'class' => $gridClass],
                            'columns' => $gridColumns,
                            'theme' => 'simple-default',
                            'storage' => 'cookie',
                            'showPersonalize' => true,
                            'showFilter' => false,
                            'showSort' => false,
                            'allowFilterSetting' => false,
                            'allowSortSetting' => false,
                            'gridOptions' => [
                                'dataProvider' => $dataProvider,
                                'showPageSummary' => false,
                                'pjax' => true,
                                'pjaxSettings' => [
                                    'neverTimeout' => true,
                                    'options' => [
                                        'id' => 'pa-search-entries-grid'
                                    ],
                                    'beforeGrid' => '',
                                    'afterGrid' => ''
                                ],
                                'responsive' => true,
                                'responsiveWrap' => false,
                                'floatHeader' => false,
                                'floatOverflowContainer' => true,
                                'panel' => [
                                    'heading' => false,
                                    'before' => \Yii::t('app', 'This lists the entries based from filters specified above. Tick the checkbox header to select across all pages.')
                                    . '<p id="total-selected-entries-text" class="pull-right summary" style="margin-left: -5px;"><b id = "selected-entry-count">'.$selectedEntryIdsCount.'</b> selected items.</p>'
                                    . ' {summary} ',
                                    'after' => false,
                                ],
                                'toolbar' => [
                                    Html::button('<i class="glyphicon glyphicon-edit"></i> ', ['type'=>'button', 'title'=>Yii::t('app', 'Bulk update replicates'), 'class'=>'tabular-btn btn waves-effect waves-light btn btn-primary', 'id'=>'bulk-updateRep-btn']),
                                    [
                                        'content' => ""
                                            .Html::button('<i class="glyphicon glyphicon-repeat"></i>', ['type'=>'button', 'id' => 'reset-entries-browser-btn', 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid')])
                                    ],
                                    Html::a('Manage Entry Reps', '#', ['style' => 'margin:0px 5px;', 'type' => 'button', 'title' => \Yii::t('app', "Add Entries"), 'class' => 'tabular-btn btn waves-effect waves-light btn btn-primary light-green darken-3', 'id' => 'pa-add-to-list'])
                                        
                                ],
                                'pager' => [
                                    'firstPageLabel' => 'First',
                                    'lastPageLabel' => 'Last'
                                ],
                            ],
                            'submitButtonOptions' => [
                                'class' => 'btn btn-primary dynagrid-submit',
                                'id' => 'entry-browser-grid-settings-submit-btn'
                            ]
                        ]);
                        
                        DynaGrid::end();
                ?>
            </div>
        </li>
    </ul>
</div>

<div class="entries-result col-md-12">
    <ul class="collapsible collapsible-result-browser" id="collapsible-working-list-browser">
        <li class="working-list-browser">
            <div class="collapsible-header" id="working-list-browser"><i class="material-icons">list</i>Working List</div>
            <div class="collapsible-body" id="query-working">
                <div id="working-list-browser-container"></div>
            </div>
        </li>
    </ul>
</div>
<div class="clearfix"></div>
<?php
// reorder working list modal body
Modal::begin([
    'id' => 'pa-reorder-working-list-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align: bottom;">reorder</i> ' . \Yii::t('app', 'Reorder Entry Replicates') . '</h4>',
    'footer' =>
        Html::a('Cancel', ['#'], [
            'id' => 'pa-reorder-working-list-modal-cancel-btn',
            'class' => 'margin-right',
            'data-dismiss' => 'modal'
        ]) .
        Html::a(\Yii::t('app', 'Confirm') . '<i class="material-icons right">send</i>', '#', [
            'class' => 'btn waves-effect waves-light pa-reorder-working-list-save-btn margin-right'
        ]),
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;']
]);
echo '<div class="pa-reorder-working-list-modal-body"></div>';

Modal::end();

// assign to block modal body
Modal::begin([
    'id' => 'pa-assign-entries-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align: bottom;">playlist_add</i> ' . \Yii::t('app', 'Assign entries to blocks') . '</h4>',
    'footer' =>
    Html::a(\Yii::t('app', 'Assign more'), '#', [
        'class' => 'black-text grey lighten-2 btn waves-effect waves-light pa-assign-entries-assign-more-btn hidden margin-right',
        'data-dismiss' => 'modal',
        'title' => 'Go back to assign more entries to block'
    ]) .
        Html::a(\Yii::t('app', 'Manage blocks'), '#', [
            'class' => 'btn waves-effect waves-light pa-assign-entries-manage-block-btn hidden margin-right',
            'data-dismiss' => 'modal',
            'title' => 'Go to manage blocks'
        ]) .
        Html::a('Cancel', ['#'], [
            'id' => 'pa-assign-entries-modal-cancel-btn',
            'class' => 'margin-right',
            'data-dismiss' => 'modal'
        ]) .
        Html::a(\Yii::t('app', 'Save'), '#', [
            'class' => 'btn waves-effect waves-light pa-assign-entries-save-btn disabled margin-right'
        ]),
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;']
]);
echo '<div class="pa-assign-entries-modal-body"></div>';

Modal::end();

//Modal for replicates' bulk update 
Modal::begin([
    'id' => 'replicate-bulkUpdate-modal',
    'header' => \Yii::t('app','<h4>Bulk Update Replicates</h4>'),
    'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="bulk-update-entry-btn-selected" class="btn btn-primary green waves-effect waves-light bulk-update-entry-btn" mode ="selected">'.\Yii::t('app','Selected').'</a>&nbsp;'.
        '<a id="bulk-update-entry-btn-selected-page" class="btn btn-primary green waves-effect waves-light bulk-update-entry-btn" mode ="current-page">'.\Yii::t('app','Selected in Page').'</a>&nbsp;'.
        '<a id="bulk-update-entry-btn-all" class="btn btn-primary teal waves-effect waves-light bulk-update-entry-btn" mode ="all">'.\Yii::t('app','All').'</a>',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
]);
echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','No. of replicates'.' '.'<span class="required">*</span>').'</label>';
        echo '<div style="width:44%">';
            $input = Html::input('number', 'entryRepBulkField', 1, ['id'=>'entryRepFieldId','class' => 'numeric-input-validate entry-class-integer', 'autofocus' => true, 'min' => 1, 'style' => 'width:100%']);
            echo $input;
        echo '</div>';
echo '<div id="replicate-bulkUpdate-modal-confirm-notif" class="hidden"></div>';
Modal::end();

$plantingArrangementUrl = Url::to(['/experimentCreation/create/specify-cross-design', 'id' => $id, 'program' => $program, 'processId' => $processId]);
$getFilterTagsUrl = Url::to(['/experimentCreation/planting-arrangement/search-tags', 'id' => $id]);
$assignEntriesUrl = Url::to(['/experimentCreation/planting-arrangement/assign-entries', 'id' => $id, 'program' => $program, 'processId' => $processId]);
$manageWorkingListUrl = Url::to(['/experimentCreation/planting-arrangement/manage-working-list', 'id' => $id, 'program' => $program, 'processId' => $processId]);
$removeSelectedFromWorkingListUrl = Url::to(['/experimentCreation/planting-arrangement/remove-selected-from-working-list', 'id' => $id]);
$manageReplicateOrderInWorkingListUrl = Url::to(['/experimentCreation/planting-arrangement/manage-replicate-order-in-working-list', 'id' => $id]);
$reorderSelectedInWorkingListUrl = Url::to(['/experimentCreation/planting-arrangement/reorder-selected-in-working-list', 'id' => $id]);
$saveFiltersUrl = Url::to(['/experimentCreation/planting-arrangement/save-filters', 'id' => $id]);
$renderAssignEntriesFormUrl = Url::to(['/experimentCreation/planting-arrangement/render-assign-entries-form', 'id' => $id]);
$addEntriesToListUrl = Url::to(['/experimentCreation/planting-arrangement/add-entries-to-list', 'id' => $id]);
$saveSelectedItemsUrl = Url::to(['/experimentCreation/planting-arrangement/save-user-selection', 'id' => $id]);
$emptySelectedItemsUrl = Url::to(['/experimentCreation/planting-arrangement/empty-user-selection', 'id' => $id]);
$getSelectedItemsUrl = Url::to(['/experimentCreation/planting-arrangement/get-user-selection', 'id' => $id]);
$storeSessionItemsUrl = Url::to(['/experimentCreation/planting-arrangement/store-session-items']);
$getSelectedItemsUrl2 = Url::to(['/experimentCreation/planting-arrangement/get-selected-items']);
$resetSessionFilterUrl = Url::to(['/experimentCreation/planting-arrangement/reset-session-filter', 'id' => $id]);
$updateEntryRep = Url::to(['/experimentCreation/planting-arrangement/update-entry-rep', 'id' => $id]);

extract($filters);

$filters = json_encode($filters);
$already_used = isset($already_used) && !empty($already_used) ? $already_used : "true";

$checkStatusUrl  = Url::to(['create/check-status','id'=>$id, 'action'=>'cross-design']);

Yii::$app->view->registerJs(
    "
        checkStatusUrl = '" . $checkStatusUrl . "',
        experimentId = '" . $id . "',
        requiredFieldJson = 'null',
        checkStatValue = 'design generated'
        ;",
    \yii\web\View::POS_HEAD
);

$this->registerJsFile("@web/js/check-required-field-status.js", [
    'depends' => ['app\assets\AppAsset'],
    'position' => \yii\web\View::POS_END
]);

$this->registerJsFile("@web/js/generic-form-validation.js", [
  'depends' => ['app\assets\AppAsset'],
  'position'=>\yii\web\View::POS_END
]);
$this->registerJs(<<<JS
    var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
    $('#page-notif').addClass('hidden');
    var entryInformation = [];
    var totalEntriesCount = '$dataProvider->totalCount';

    $(document).ready(function(){
        $('#change-design-switch-id').removeAttr('disabled')
        $('.collapsible').collapsible();

        var selectedItems = getSessionUserSelection();
        resizePanel(selectedItems);
        $('#page-notif').addClass('hidden');

        if($('#pa-select-entries-all').prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.pa-entries-grid_select').attr('checked','checked');
            $('.pa-entries-grid_select').prop('checked',true);
            $(".pa-entries-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
        }else{
            $(this).removeAttr('checked');
            $('.pa-entries-grid_select').prop('checked',false);
            $('.pa-entries-grid_select').removeAttr('checked');
            $("input:checkbox.pa-entries-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
        }
    });
    checkStatus();
    $('#pa-search-entries-grid').removeClass('hidden');

    var includeSeedlist=false;
    var desigArr = [];

    var selectedItems = getSessionUserSelection();
    resizePanel(selectedItems);

    // when Entries Browser collapsible is opened/closed ensure that the checkboxes are active
    $(document).on('click','#result-browser',function(){
        var selectedIds = [];
        $.ajax({
            url: '$getSelectedItemsUrl2',
            type: 'post',
            dataType:'json',
            data: {
                experimentDbId:'$id'
            },
            async: false,
            success: function(response) {
                selectedIds = response;
            },
            error: function(xhr, status, error) {
            }
        });
    });

    // refreshes page on pjax success
   $(document).on('ready pjax:success','#pa-search-entries-grid-pjax', function(e) {
        e.stopPropagation();
        e.preventDefault();

        // trigger select all button for every page
        $.ajax({
            url: '$getSelectedItemsUrl2',
            type: 'post',
            dataType:'json',
            async:false,
            data: {
                experimentDbId: '$id'
            },
            success: function(response) {
                if(response['isSelectedAll']){
                    $('#pa-select-entries-all').trigger('click')
                }
            },
            error: function() {
            }
        });
    });

    //adjust height of seed list and results data browser after pjax
    $(document).on('ready pjax:complete','#search-grid-pjax', function(e) {
        e.stopPropagation();
        e.preventDefault();
    });

    // reset filters
    $(document).on('click','#pa-search-entries-reset-btn', function(e){
    
        document.getElementById("pa_select_already_used").checked = true;
        $(".pa-filter-field").each(function( index ) {
            var obj = $(this);
            var attr = obj.data('attr');

            if(attr == 'entryNumber'){
                $('#pa_select_entryNumber_operator').val("range").trigger("change");
                $('.pa-filter-entryNumber-field').val('');
                $('.pa-filter-entryNumber-fielautod-from').val('');
                $('.pa-filter-entryNumber-field-to').val('');
                $('.pa-filter-entryNumber-field-from').val('');
            }else{
                $('#pa_select_'+attr).val("-1").trigger("change");
            }
        });
        //Reset the session filter for assign entries 
        $.ajax({
            url: '$resetSessionFilterUrl',
            type: 'post',
            success: function(response) {
                $('#pa-search-entries-btn').click();
            },
            error: function() {
            }
        });
    });

    // search entries
    $(document).on('click','#pa-search-entries-btn',function(e){
        e.preventDefault();

        $('.collapsible-entry-working-list').addClass('hidden');

        // Close search filter
        if($('#search-filter').hasClass('active')) {
            $('#search-filter').trigger('click')
        }

        // Open result browser
        if(!$('#result-browser').hasClass('active')) {
            $('#result-browser').trigger('click')
        }

        // Close and disable working list
        $('#working-list-browser').addClass('ui-state-disabled')
        if($('#working-list-browser').hasClass('active')) {
            $('#working-list-browser').trigger('click')
        }

        var entryNumberOperator = $('#pa_select_entryNumber_operator').val();
        var filters = {};

        // clear selected items
        $.ajax({
            url: '$storeSessionItemsUrl',
            type: 'post',
            data: {
                entryDbId: 0,
                unset: true,
                storeAll: true,
                experimentDbId : '$id'
            },
            async: false,
            success: function(response) {
                $('#selected-entry-count').html('No');
            },
            error: function(e) {
                console.log(e);
            }
        });

        // build query parameters
        $(".pa-filter-field").each(function( index ) {
            var obj = $(this);
            var attr = obj.data('attr');
            var isvar = obj.data('isvar');
            var values = [];
            // build query parameters
            if(attr == 'entryNumber'){
                if(entryNumberOperator == 'range'){ // if range
                    var from = $('.pa-filter-entryNumber-field-from').val();
                    var to = $('.pa-filter-entryNumber-field-to').val();

                    values = {operator:entryNumberOperator,from:from,to:to};
                }else if(entryNumberOperator == '='){
                    $('#pa_select_'+attr).select2('data').map(function(elem){ 
                            values.push(elem.text);
                        });
                }else{
                    var val = $('.pa-filter-entryNumber-field').val();
                    values = {operator:entryNumberOperator,val:val};
                }
            }else{
                if(attr == 'designation'){
                    $.each($('#pa_select_'+attr+' option:selected'),function(){
                        values.push($(this).text() );
                    });                    
                }else{
                    $('#pa_select_'+attr).select2('data').map(function(elem){ 
                        values.push(elem.text);
                    });
                }
            }

            filters[[attr]] = values;
        });
        
        var includeAlreadyUsed = document.getElementById("pa_select_already_used").checked;
        filters['already_used'] = includeAlreadyUsed;
        $.ajax({
            url: '$saveFiltersUrl',
            type: 'post',
            data:{
                filters: JSON.stringify(filters)
            },
            success: function(response) {
                $.pjax.reload({
                    type: 'POST',
                    url: '$assignEntriesUrl',
                    data: {pjax: true},
                    container: '#pa-search-entries-grid-pjax', 
                    replace:false
                });
                $('#pa-search-entries-grid').removeClass('hidden');

                var selectedItems = getSessionUserSelection();
                resizePanel(selectedItems);
            },
            error: function() {
            }
        });

    });

    $(document).on('click','#reset-entries-browser-btn',function(e){
        // clear selected items
        $.ajax({
            url: '$storeSessionItemsUrl',
            type: 'post',
            data: {
                entryDbId: 0,
                unset: true,
                storeAll: true,
                experimentDbId : '$id'
            },
            async: false,
            success: function(response) {
                $('#selected-entry-count').html('No');
            },
            error: function(e) {
                console.log(e);
            }
        });

        // trigger the search entries button
        $('#pa-search-entries-btn').click();
    });

    // assign more entries
    $(document).on('click','.pa-assign-entries-assign-more-btn',function(){
        $('#grid-entries-working-list').addClass('kv-grid-loading');
        $('#working-list-browser-container').html('<div class="progress"><div class="indeterminate"></div></div>');
        $.pjax.reload({
            type: 'POST',
            url: '$assignEntriesUrl',
            data: {pjax: true},
            container: '#pa-search-entries-grid-pjax', 
            replace:false
        });
        checkStatus();

        // reload working list browser
        $.ajax({
            type: 'POST',
            url: '$manageWorkingListUrl',
            data: {
                reset: true
            },
            async: false,
            success: function(data) {
                $('#working-list-browser-container').parent().html('<div id="working-list-browser-container"></div>');
                $('#working-list-browser-container').append(data);
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading entries. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });
    });

    // manage blocks
    $(document).on('click','.pa-assign-entries-manage-block-btn',function(){
        // reload working list browser
        $.ajax({
            type: 'POST',
            url: '$manageWorkingListUrl',
            data: {
                reset: true
            },
            async: false,
            success: function(data) {
                $('#working-list-browser-container').parent().html('<div id="working-list-browser-container"></div>');
                $('#working-list-browser-container').append(data);
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading entries. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });

        $('.pa-manage-blocks-tab').click();

        // Force remove modal backdrop
        $('.modal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    });

    // force numeric input
    $('body').on('propertychange input','.input-integer',forceNumeric);

    // upon hitting enter key
    $(".input-integer").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#pa-search-entries-btn").click();
        }
    });

    // set value for already used
    document.getElementById("pa_select_already_used").checked = $already_used;

    // validate numeric
    function forceNumeric(){
        var input = $(this);
        input.val(input.val().replace(/[^\d]+/g,''));
    }

    // force numeric input
    $('body').on('propertychange input','.entry-class-integer',forceIntegerValues);

    // upon hitting enter key
    $(".entry-class-integer").keyup(function(event) {

    });
    // validate numeric
    function forceIntegerValues(){
        var input = $(this);
        input.val(input.val().replace(/[^\d]+/g,''));
        if(input.val() == 0){
            input.val(input.val().replace(/0/g,''));   
        }
    }

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
    });

    // upon select operator
    $('#pa_select_entryNumber_operator').change(function(){
        var opSelected = this.value;

        if(opSelected == 'range'){
            $('.pa-filter-entryNumber-field').addClass('hidden');
            $('.entryNumber-range-panel').removeClass('hidden');
            $('.entryNumber-multiple-panel').addClass('hidden');
            $('#pa-entryNumber-from').focus();
        }else if (opSelected == '='){
            $('.entryNumber-range-panel').addClass('hidden');
            $('.pa-filter-entryNumber-field').addClass('hidden');
            $('.entryNumber-multiple-panel').removeClass('hidden');
        }else{
            $('.pa-filter-entryNumber-field').removeClass('hidden');
            $('.entryNumber-range-panel').addClass('hidden');
            $('.entryNumber-multiple-panel').addClass('hidden');
        }
    });

    //get tags
    $(".pa-filter-field").each(function( index ) {
        
        var obj = $(this);
        var attr = obj.data('attr');
        var isvar = obj.data('isvar');
        var filters = JSON.parse('$filters');
        
        if(attr !== 'entryNumber' && attr !== 'designation'){
            $.ajax({
                url: '$getFilterTagsUrl',
                type: 'post',
                dataType: 'json',
                async: true,
                data:{
                    attr: attr,
                    isvar: isvar
                },
                success: function(response) {
                    option = response;
                    
                    var lookup = {};
                    var sattr = document.getElementById('pa_select_'+attr);
                
                    for(var i=0; i<option.length;i++){
                
                        var newOption = document.createElement("option");

                        if (!(parseInt(option[i]['id']) in lookup)) {

                            lookup[parseInt(option[i]['id'])] = 1;

                            newOption.value = parseInt(option[i]['id']);
                            newOption.innerHTML = option[i]['text'];
                            sattr.options.add(newOption);
                        }
                    }
                
                    // load previously specified data filters
                    for (let [key, value] of Object.entries(filters)) {
                        var selectedValues = [];

                        if(key == 'entryNumber' && value['operator'] !== undefined){
                            var entryNumberOperator = value['operator'];
                            $('#pa_select_entryNumber_operator').val(entryNumberOperator).change();
                            
                            if(entryNumberOperator == 'range'){ // if range
                                var from = (value['from'] !== undefined) ? value['from'] : '';
                                var to = (value['to'] !== undefined) ? value['to'] : '';

                                $('.pa-filter-entryNumber-field-from').val(from);
                                $('.pa-filter-entryNumber-field-to').val(to);
                            }else{
                                var val = (value['val'] !== undefined) ? value['val'] : '';
                                $('.pa-filter-entryNumber-field').val(val);
                            }
                        }else if(key == attr){
                            // if entlistno
                            if(attr == 'entryNumber' && value['operator'] === undefined){
                                $('#pa_select_entryNumber_operator').val('=').change();
                            }

                            // loop through values
                            var valuesCount = value.length;

                            for(i=0; i < valuesCount; i++){
                                var val = value[i];
                                if(val != ''){
                                    var text = $( "#pa_select_"+attr+" option:contains("+val+")").val();
                                    selectedValues.push(text);
                                }
                            }

                            $('#pa_select_'+attr).val(selectedValues).change();
                        }
                    }
                },
                error: function() {
                }
            });
        }
    });


    //select grid
    $(document).on('click','.pa-entries-grid_select',function(e){
  
        e.preventDefault();
        e.stopImmediatePropagation();

        var unset;
        var url = '$storeSessionItemsUrl';

        if($(this).prop('checked')){
            unset = false;
            $('#pa-add-to-list').removeClass('disabled');
        }else{
            unset = true;
            $('#pa-select-entries-all').prop('checked',false);
            $('#pa-select-entries-all').removeAttr('checked');
        }

        $.ajax({
            url: url,
            type: 'post',
            data: {
                entryDbId: $(this).prop('id'),
                unset: unset,
                storeAll: false,
                experimentDbId : '$id'
            },
            success: function(response) {
                var totalEntriesCountTemp = $('#pa-search-entries-grid').find('div.summary>b:nth-child(2)');
                totalEntriesCountTemp = (totalEntriesCountTemp.length > 0) ? totalEntriesCountTemp.text() : totalEntriesCount;
                totalEntriesCount = isNaN(totalEntriesCountTemp) ? totalEntriesCount : parseInt(totalEntriesCountTemp);
                if(totalEntriesCount == response) {
                    $('#pa-select-entries-all').trigger('click');
                } else {
                    $('#pa-select-entries-all').prop('checked',false);
                    $('#pa-select-entries-all').removeAttr('checked');
                }

                response = response == 0 ? 'No': response;
                $('#selected-entry-count').html(response);
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    // select all entries
    $(document).on('click','#pa-select-entries-all',function(e){
 
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.pa-entries-grid_select').attr('checked','checked');
            $('.pa-entries-grid_select').prop('checked',true);
            $(".pa-entries-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
            var totalEntriesCountTemp = $('#pa-search-entries-grid').find('div.summary>b:nth-child(2)');
            totalEntriesCount = (totalEntriesCountTemp.length > 0) ? totalEntriesCountTemp.text() : totalEntriesCount;
            $('#selected-entry-count').html(totalEntriesCount);
        }else{
            $(this).removeAttr('checked');
            $('.pa-entries-grid_select').prop('checked',false);
            $('.pa-entries-grid_select').removeAttr('checked');
            $("input:checkbox.pa-entries-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
            $('#selected-entry-count').html('No');
        }

        var unset = $(this).prop('checked') ? false : true;
        var url = '$storeSessionItemsUrl';

        $.ajax({
            url: url,
            type: 'post',
            data: {
                entryDbId: 0,
                unset: unset,
                storeAll: true,
                experimentDbId : '$id'
            },
            success: function(response) {
                $('#selected-entry-count').html(response);
                
                if(unset){
                    $('#selected-entry-count').html('No');
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    // add to block
    $(document).on('click','#add-to-block-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        setTimeout(function() {
            $.ajax({
                type: 'POST',
                url: '$renderAssignEntriesFormUrl',
                async: false,
                cache: false,
                data: {},
                success: function(data) {
                    var obj = '';
                    if(data.includes('blockFlag')){
                        obj = JSON.parse(data);
                    }

                    if(obj.blockFlag !== undefined && obj.blockFlag == 0){
                        $('#page-notif').removeClass('hidden');
                        $('#page-notif').html('<div class="alert alert-warning" style="padding: 10px">There are no saved blocks yet. Please save a block first.</div>');
                    }else{
                        $('#page-notif').addClass('hidden');
                        $('#pa-assign-entries-modal').modal('show');
                        $('.pa-assign-entries-assign-more-btn').addClass('hidden');
                        $('.pa-assign-entries-manage-block-btn').addClass('hidden');
                        $('#pa-assign-entries-modal-cancel-btn').removeClass('hidden');
                        $('.pa-assign-entries-save-btn').removeClass('hidden');

                        $('.pa-assign-entries-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
                        $('.pa-assign-entries-modal-body').html(data);
                    }
                },
                error: function(data) {
                    $('.pa-assign-entries-modal-body').html('<i>There was a problem in loading content. Please report using "Feedback".</i>');
                }
            });
        }, 300);
    });

    // click row in browser
    $(document).on('click',"#pa-search-entries-grid tbody tr",function(e){

        e.preventDefault();
        e.stopImmediatePropagation();
        var thisRow = $(this).find('input:checkbox')[0];

        $("#"+thisRow.id).trigger("click");
        if(thisRow.checked){
            $(this).addClass("grey lighten-4");
        }else{
            $(this).removeClass("grey lighten-4");
        }

       
    });


    // click add entries
    $(document).on('click','#pa-add-to-list', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var ids = [];
        var isSelectAll = false;

        $.ajax({
            url: '$getSelectedItemsUrl2',
            type: 'post',
            dataType:'json',
            async:false,
            data: {
                experimentDbId: '$id'
            },
            success: function(response) {
                if(response['selectedItemsCount'] > 0){
                    ids = response['selectedItems'];
                    isSelectAll = true;
                }
                
            },
            error: function() {
            
            }
        });
    
        if(isSelectAll == false){
            $("input:checkbox.pa-entries-grid_select").each(function(){
                if($(this).prop("checked") === true)  {
                    entryId = $(this).attr("id");
                    ids.push(entryId);
                }
            });
        }
     
        storeSelectionToSession(ids);

        var sessionSelection = getSessionUserSelection();

        if(ids.length == 0){
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please select at least one(1) entry.</span>";
                Materialize.toast(notif, 5000);
            }

            e.preventDefault();
            e.stopImmediatePropagation();
        }else{
            $.ajax({
                url: '$addEntriesToListUrl',
                type: 'POST',
                dataType: 'json',
                cache: false,
                async: false,
                data: {
                    ids: JSON.stringify(sessionSelection)
                },
                success: function(response){
                    if(response['blockFlag'] == 0){
                        $('#page-notif').removeClass('hidden');
                        $('.working-list-container').addClass('hidden');
                        $('#page-notif').html('<div class="alert alert-warning" style="padding: 10px">There are no saved blocks yet. Please save a block first.</div>');
                    }else{
                        // "Soft Reset" for Entries Browser just for the select all button since after clicking the Manage Entry Reps button,
                        // the Entries Browser's Select All button does not work for some reason
                        var entryNumberOperator = $('#pa_select_entryNumber_operator').val();
                        var filters = {};
                        // build query parameters
                        $(".pa-filter-field").each(function( index ) {
                            var obj = $(this);
                            var attr = obj.data('attr');
                            var isvar = obj.data('isvar');
                            var values = [];
                            // build query parameters
                            if(attr == 'entryNumber'){
                                if(entryNumberOperator == 'range'){ // if range
                                    var from = $('.pa-filter-entryNumber-field-from').val();
                                    var to = $('.pa-filter-entryNumber-field-to').val();

                                    values = {operator:entryNumberOperator,from:from,to:to};
                                }else if(entryNumberOperator == '='){
                                    $('#pa_select_'+attr).select2('data').map(function(elem){
                                            values.push(elem.text);
                                        });
                                }else{
                                    var val = $('.pa-filter-entryNumber-field').val();
                                    values = {operator:entryNumberOperator,val:val};
                                }
                            }else{
                                if(attr == 'designation'){
                                    $.each($('#pa_select_'+attr+' option:selected'),function(){
                                        values.push($(this).text() );
                                    });
                                }else{
                                    $('#pa_select_'+attr).select2('data').map(function(elem){
                                        values.push(elem.text);
                                    });
                                }
                            }

                            filters[[attr]] = values;
                        });

                        var includeAlreadyUsed = document.getElementById("pa_select_already_used").checked;
                        filters['already_used'] = includeAlreadyUsed;
                        $.ajax({
                            url: '$saveFiltersUrl',
                            type: 'post',
                            data:{
                                filters: JSON.stringify(filters)
                            },
                            success: function(response) {
                                $.pjax.reload({
                                    type: 'POST',
                                    url: '$assignEntriesUrl',
                                    data: {pjax: true},
                                    container: '#pa-search-entries-grid-pjax', 
                                    replace:false
                                });
                                $('#pa-search-entries-grid').removeClass('hidden');

                                var selectedItems = getSessionUserSelection();
                                resizePanel(selectedItems);
                            },
                            error: function() {
                            }
                        });
                        // End of "Soft Reset"

                        $('.working-list-container').removeClass('hidden');
                        $('.collapsible-entry-working-list').removeClass('hidden');
                        $('.collapsible-entry-working-list').show();

                        // Close result browser
                        if($('#result-browser').hasClass('active')) {
                            $('#result-browser').trigger('click')
                        }

                        // Show and enable working list
                        $('#working-list-browser').removeClass('ui-state-disabled')
                        if(!$('#working-list-browser').hasClass('active')) {
                            $('#working-list-browser').trigger('click')
                        }

                        $.ajax({
                            type: 'POST',
                            url: '$manageWorkingListUrl',
                            data: {
                                reset: true
                            },
                            async: true,
                            success: function(data) {
                                $('#working-list-browser-container').parent().html('<div id="working-list-browser-container"></div>');
                                $('#working-list-browser-container').append(data);
                            },
                            error: function(){
                                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading entries. Please try again.';
                                Materialize.toast(notif, 5000);
                            }
                        });

                        $('#pa-search-entries-grid').removeClass('hidden');

                        $('.working-list-container').removeClass('hidden');

                        $('#list-view-body').css('display','block');

                        $('.pa-entries-grid_select').prop('checked',false);
                        $('.pa-entries-grid_select').removeAttr('checked');
                        $(".pa-entries-grid_select:checkbox").parent("td").parent("tr").removeClass("grey lighten-4");

                        $('#pa-select-entries').prop('checked',false);
                        $('#pa-select-entries').removeAttr('checked');
                    }
                }
            });
        }
    });

    $(document).on('click', '#reset-working-list-grid', function(e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: '$manageWorkingListUrl',
            data: {
                reset: true
            },
            async: true,
            success: function(data) {
                $('#working-list-browser-container').parent().html('<div id="working-list-browser-container"></div>');
                $('#working-list-browser-container').append(data);
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading replicates. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });
    });

    //Remove entry from the working list
    $(document).on('click','#remove-replicate-btn', function(e){
        e.stopPropagation();
        e.preventDefault();

        $('#grid-entries-working-list').addClass('kv-grid-loading');

        $.ajax({
            type: 'POST',
            url: '$removeSelectedFromWorkingListUrl',
            data: {},
            async: true,
            success: function(data) {
                $.ajax({
                    type: 'POST',
                    url: '$manageWorkingListUrl',
                    data: {
                        reset: true
                    },
                    async: true,
                    success: function(data) {
                        $('#working-list-browser-container').parent().html('<div id="working-list-browser-container"></div>');
                        $('#working-list-browser-container').append(data);
                    },
                    error: function(){
                        var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading entries. Please try again.';
                        Materialize.toast(notif, 5000);
                    }
                });
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading entries. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });
    });

    // open reorder modal
    $(document).on('click','#reorder-modal-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var selectedCount = $('#selected-rep-count').text();

        if(isNaN(selectedCount) || !selectedCount || selectedCount > 50) {  // trying to reorder more than 50 replicates or invalid
            var notif = '<i class="material-icons orange-text">warning</i> Please only select up to 50 replicates for reordering and try again.';
            Materialize.toast(notif, 5000);
            return;
        }

        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: '$manageReplicateOrderInWorkingListUrl',
                async: false,
                data: {},
                success: function(response) {
                    response = JSON.parse(response);
                    if(response['success']) {   // open modal
                        $('#pa-reorder-working-list-modal').modal('show');
                        $('.pa-reorder-working-list-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
                        $('.pa-reorder-working-list-modal-body').html(response['data']);
                        updateOrderFields();
                    } else {    // display error
                        var notif = '<i class="material-icons red-text">close</i> '+response['message'];
                        Materialize.toast(notif,5000);
                    }
                },
                error: function(data) {
                    var notif = '<i class="material-icons red-text">close</i> There was a problem in loading content. Please report using "Feedback".';
                    Materialize.toast(notif,5000);
                }
            });
        }, 300);
    });

    // update fields and notification when user selects cascade or swap
    $(document).on('select2:select', '#select2-reorder-method', function(e) {
        updateOrderFields();
    });

    function updateOrderFields() {
        var reorderMethod = $('#select2-reorder-method').select2('data')[0].text;
        var message = '';

        if(reorderMethod == 'Swap') {
            // enable all input fields
            $('.reorder-replicate-row').each(function() {
                $(this).find('.new-order-number-field').prop('disabled', false);
            });
            message = 'For Swap, a minimum of 2 replicates is needed.'
        } else if(reorderMethod == 'Cascade') {
            // disable input fields after the first row
            var i = 0;
            $('.reorder-replicate-row').each(function() {
                if(i>0) {
                    $(this).find('.new-order-number-field').prop('disabled', true);
                }
                i++;
            });
            message = 'For Cascade, only the first row will be cascaded.'
        }

        $('#pa-reorder-working-list-modal-notification').removeClass('hidden');
        $('#pa-reorder-working-list-modal-notification').html(message);

        setTimeout(function() {
            $('#pa-reorder-working-list-modal-notification').addClass('hidden');
        }, 10000);
    }

    $(document).on('input','.new-order-number-field', function(e) {
        var newOrderInputValue = $(this).val().trim();
        var inputChecker = parseInt(newOrderInputValue);
        var inputFinalVal = '';
        if(!newOrderInputValue || isNaN(inputChecker) || inputChecker <= 0) {
            inputFinalVal = '1';
        } else {
            inputFinalVal = (Math.floor(inputChecker)).toString();
        }
        $(this).val(inputFinalVal);
    });

    // reorder working list confirm
    $(document).on('click','.pa-reorder-working-list-save-btn', function(e){
        var reorderMethod = $('#select2-reorder-method').select2('data')[0].text;
        var originalOrderNo = [];
        var newOrderNo = [];

        $('.reorder-replicate-row').each(function() {
            originalOrderNo.push($(this).find('.old-order-number-field').text())
            newOrderNo.push($(this).find('.new-order-number-field').val())
            if(reorderMethod == 'Cascade') {    // only cascade first row
                return false;
            }
        });

        $.ajax({
            type: 'POST',
            url: '$reorderSelectedInWorkingListUrl',
            data: {
                reorderMethod: reorderMethod,
                originalOrderNo: originalOrderNo,
                newOrderNo: newOrderNo
            },
            async: true,
            success: function(response) {
                response = JSON.parse(response);
                if(response['success']) {
                    // hide modal
                    $('#pa-reorder-working-list-modal').modal('hide');

                    // then refresh working list browser
                    $.ajax({
                        type: 'POST',
                        url: '$manageWorkingListUrl',
                        data: {
                            reset: true
                        },
                        async: true,
                        success: function(data) {
                            $('#working-list-browser-container').parent().html('<div id="working-list-browser-container"></div>');
                            $('#working-list-browser-container').append(data);
                        },
                        error: function(){
                            var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading entries. Please try again.';
                            Materialize.toast(notif, 5000);
                        }
                    });
                } else {    // display error
                    $('#pa-reorder-working-list-modal-notification').removeClass('hidden');
                    $('#pa-reorder-working-list-modal-notification').html(response['message']);

                    setTimeout(function(){ 
                        $('#pa-reorder-working-list-modal-notification').addClass('hidden');
                    }, 5000);
                }

            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading entries. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });
    });

    //retrieve user's session selection
    function getSessionUserSelection(){
        var userSelection;

        $.ajax({
          type: 'POST',
          url: '$getSelectedItemsUrl',
          async: false,
          success: function(response){
             userSelection = JSON.parse(response);
          }
        });
        return userSelection;
    }

    //Assign null value to the user session's selection
    function emptySessionUserSelection(){
        var empty = null;

        $.ajax({
            type: 'POST',
            url: '$emptySelectedItemsUrl',
            data: {
                selectedItems:empty
            },
            async: false,
            dataType: 'json',
            success: function(response) {
            }
        }); 
    }

     // save selected items to session
     // these are the added entries to list
     function storeSelectionToSession(selectedItems){
        if(selectedItems != null && selectedItems != []){
            $.ajax({
                type: 'POST',
                url: '$saveSelectedItemsUrl',
                data: {
                    selectedItems: JSON.stringify(selectedItems)
                },
                dataType: 'json',
                async: false,
                success: function(response) {
                }
            });
        }
    }

    //Resize result panel
    function resizePanel(selectedItems){
        if(selectedItems != null && selectedItems != []){
            $('#working-list-container').removeClass('hidden');
            $('#list-view-body').css('display','block');
        }else{
            $('#working-list-container').addClass('hidden');
        }
    }

    $(document).on('change', '.entryRep-class', function(){
        var entryArray = (this.id).split('-');
        var varValue = this.value;

        if(varValue != '' && varValue != undefined){
            $.ajax({
                url: '$updateEntryRep', 
                type: 'post',
                dataType: 'json',
                data: {
                    entryDbId: entryArray[1],
                    varValue: varValue
                },
                success: function(response) {
                    if(response){
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons green-text left'>check</i>Successfully updated.";
                            Materialize.toast(notif,5000);
                        }
                    } else {
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons red-text left'>close</i>Update failed. Please try again.";
                            Materialize.toast(notif,5000);
                        }
                    }
                },
                error: function() {
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons red-text left'>close</i>Update failed. Please try again.";
                        Materialize.toast(notif,5000);
                    }
                }
            });
        } else {
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons red-text left'>close</i>No value specified.";
                Materialize.toast(notif,5000);
            }
        }
        
    });

    $(document).on('click', '#bulk-updateRep-btn', function(e){
        var update_modal = '';
       
        var entryIdArray = [];
        update_modal = '#replicate-bulkUpdate-modal > .modal-dialog > .modal-content > .modal-body';
        

        $('#replicate-bulkUpdate-modal').modal('show');
    });

    $(document).on('click', '.bulk-update-entry-btn', function(){
        switch($(this).text().toLowerCase()) {
            case 'all':
                    // trigger select all in controller action first then proceed to same steps as selected mode
                    $.ajax({
                        url: '$storeSessionItemsUrl',
                        type: 'post',
                        data: {
                            entryDbId: 0,
                            unset: false,
                            storeAll: true,
                            experimentDbId : '$id'
                        },
                        async: false,
                        success: function(response) {
                        },
                        error: function(e) {
                            console.log(e);
                        }
                    });
            case 'selected':
                    // get selected entries
                    $.ajax({
                        url: '$getSelectedItemsUrl2',
                        type: 'post',
                        dataType:'json',
                        data: {
                            experimentDbId:'$id'
                        },
                        async: false,
                        success: function(response) {
                            entryInformation = response;
                        },
                        error: function(xhr, status, error) {
                            $('#replicate-bulkUpdate-modal').modal('hide');
                            var hasToast = $('body').hasClass('toast');
                            if(!hasToast){
                                $('.toast').css('display','none');
                                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Unable to process the request. Please try again.</span>";
                                Materialize.toast(notif, 5000);
                            }
                            e.preventDefault();
                            e.stopImmediatePropagation();
                        }
                    });
                    break;
            case 'selected in page':
                    // get selected in page and manually build entryInformation
                    selectedItemsInPage = [];

                    $('.pa-entries-grid_select').each(function( index ) {
                        if($(this).prop("checked") === true) {
                            selectedItemsInPage.push($(this).attr('id'))
                        }
                    });

                    entryInformation = {
                        isSelectedAll: false,
                        selectedItems: selectedItemsInPage,
                        selectedItemsCount: selectedItemsInPage.length
                    };
                    break;
            default:
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Unable to process the request. Please try again.</span>";
                Materialize.toast(notif, 5000);
                return;
        }

        // check if there are selected records
        if(entryInformation.selectedItemsCount < 1) {
            var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> No selected records.</span>";
            Materialize.toast(notif, 5000);
            return;
        }

        var bulkValue = $('#entryRepFieldId').val();

        if(bulkValue != '' && bulkValue !== null){
            $.ajax({
                url: '$updateEntryRep',
                type: 'post',
                dataType:'json',
                data: {
                    varValue:bulkValue,
                    entryInformation:entryInformation
                },
                success: function(response) {
                    if(response){
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons green-text left'>check</i>Successfully updated.";
                            Materialize.toast(notif,5000);
                        }

                        $('#replicate-bulkUpdate-modal').modal('hide');

                        // trigger reset button
                        $('#reset-entries-browser-btn').trigger('click');
                    } else {
                        $('#replicate-bulkUpdate-modal').modal('hide');
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons red-text left'>close</i>Update failed. Please try again.";
                            Materialize.toast(notif,5000);
                        }
                    }
                },
                error: function() {
                    $('#replicate-bulkUpdate-modal').modal('hide');
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons red-text left'>close</i>Update failed. Please try again.";
                        Materialize.toast(notif,5000);
                    }
                }
            });
        } else {
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons red-text left'>close</i>No value specified.";
                Materialize.toast(notif,5000);
            }
        }

    });   

JS);
?>

<style>
    .search-input-field {
        margin-bottom: 8px;
    }

    .box {
        background: transparent;
        box-shadow: none;
    }

    .select2-search__field {
        width: 100% !important;
    }

    span.badge.new {
        font-weight: 500;
    }

    .margin-right {
        margin-right: 15px !important;
    }

    .entries-result {
        padding-right: 0px;
        -webkit-transition: .3s ease all;
        -o-transition: .3s ease all;
        -moz-transition: .3s ease all;
        transition: .3s ease all;
        -webkit-backface-visibility: hidden;
        -moz-backface-visibility: hidden;
        backface-visibility: hidden;
    }

    .working-list-container {
        padding-left: 15px;
        padding-right: 0px;
    }

    #pa-assign-entries-modal>.modal-dialog>.modal-content>.modal-header>.close {
        display: none;
    }
</style>