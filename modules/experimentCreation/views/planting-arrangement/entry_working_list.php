<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders the working list view of the entries
 */

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;

\yii\jui\JuiAsset::register($this);
?>
<?php Pjax::begin(['id'=>'grid-entries-working-list-pjax','timeout' => false, 'enablePushState' => false]); ?>
    <div class="" id="list-view-body">
        <?php
        $selectedRepIdsCount = count($selectedRepIds);
        $selectAllButtonChecked = $workingListDataProvider->totalCount == $selectedRepIdsCount? 'checked' : '';
        $selectAllButtonDisabled = $workingListDataProvider->totalCount == 0? 'disabled="true"' : '';
        $isButtonDisabled = $selectedRepIdsCount > 0? false : true;
        $columns = [
            [
                'attribute' => 'index',
                'label' => '',
                'format' => 'raw',
                'filter' => false,
                'visible' => true,
                'hidden' => false,
                'value' => function ($data) {
                    return '<div style="text-align: center;">'.($data['index']+1).'</div>';
                },
                'order' => DynaGrid::ORDER_FIX_LEFT
            ],
            // [    // Comment out for now
            //     'label' =>  'Reorder',
            //     'contentOptions' => ['style' => ['cursor' => 'pointer;'], 'class' => 'drag-handle-column'],
            //     'content' => function ($data) {
            //         return '
            //                 <i class="material-icons">drag_handle</i>
            //             ';
            //     },
            //     'encodeLabel' => false,
            //     'hAlign' => 'center',
            //     'vAlign' => 'middle',
            //     'width' => '1%',
            //     'visible' => true,
            //     'hidden' => false,
            //     'order' => DynaGrid::ORDER_FIX_LEFT
            // ],
            [
                'label' => "Checkbox",
                'mergeHeader' => true,
                'header' => '
                        <input type="checkbox" class="filled-in" data-id-string="" id="working-list-checkbox" '.$selectAllButtonChecked.' '.$selectAllButtonDisabled.'/>
                        <label style="padding-left: 20px;" for="working-list-checkbox"></label>',
                'content' => function ($data) use ($selectedRepIds) {
                    $checked = '';
                    if(in_array($data['index'], $selectedRepIds)) $checked = 'checked';

                    return '
                        <input class="assign-entries-entry-working-list filled-in" type="checkbox" id="replicate-index-'.$data['index'].'" data-index="'.$data['index'].'" '.$checked.'/>
                        <label for="'.$data['index'].'"></label>
                    ';
                },
                'width' => '1%',
                'visible' => true,
                'hidden' => false,
                'order' => DynaGrid::ORDER_FIX_LEFT
            ],
            [
                'attribute' => 'entryNumber',
                'label' => Yii::t('app', 'Entry No.'),
                'format' => 'raw',
                // 'filter' => false,
                'visible' => true,
                'hidden' => false,
                'value' => function ($data) {
                    return $data['entryNumber'];
                }
            ],
            [
                'attribute' => 'replicate',
                'label' => Yii::t('app', 'Replicate'),
                'format' => 'raw',
                // 'filter' => false,
                'visible' => true,
                'hidden' => false,
                'value' => function ($data) {
                    return $data['replicate'];
                }
            ],
            [
                'attribute' => 'designation',
                'label' => Yii::t('app', 'Germplasm Name'),
                'format' => 'raw',
                // 'filter' => false,
                'visible' => true,
                'hidden' => false,
                'value' => function ($data) {
                    return $data['designation'];
                }
            ]
        ];

        DynaGrid::begin([
            'options' => ['id' => 'grid-entries-working-list'],
            'columns' => $columns,
            'theme' => 'simple-default',
            'showPersonalize' => false,
            'storage' => 'cookie',
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => [
                'id' => 'grid-entries-working-list2',
                'dataProvider' => $workingListDataProvider,
                'filterModel' => $workingListFilterModel,
                'floatHeader' => false,
                'floatOverflowContainer' => true,
                'showPageSummary' => false,
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
                'responsive' => true,
                'responsiveWrap' => false,
                'panel' => [
                    'heading' => false,
                    'before' => \Yii::t('app', 'Add these replicates to a block.')
                    . '<p id="total-selected-text" class="pull-right summary" style="margin-left: -5px;"><b id = "selected-rep-count">'.$selectedRepIdsCount.'</b> selected items.</p>'
                    . '{summary}',
                    'after' => false
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'id' => 'reset-working-list-grid', 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid')])
                            //. '{dynagrid}'    // Comment out for now because of weird bug
                    ],
                    [
                        'content' => '&emsp;' .
                            Html::a(
                                '<i class="material-icons right">reorder</i>',
                                '#',
                                [
                                    'class' => 'btn btn-default',
                                    'id' => 'reorder-modal-btn',
                                    'disabled' => $isButtonDisabled,
                                    'title' => \Yii::t('app', 'Reorder item'),
                                    'data-pjax' => true
                                ]
                            ) .
                            Html::a(
                                '<i class="material-icons right">delete_forever</i>',
                                '#',
                                [
                                    'class' => 'btn btn-default',
                                    'id' => 'remove-replicate-btn',
                                    'disabled' => $isButtonDisabled,
                                    'title' => \Yii::t('app', 'Remove item'),
                                    'data-pjax' => true
                                ]
                            ) .
                            Html::a(
                                'Add to Block',
                                '#',
                                [
                                    'data-pjax' => true,
                                    'class' => 'btn btn-default',
                                    'id' => 'add-to-block-btn',
                                    'disabled' => $isButtonDisabled,
                                    'title' => \Yii::t('app', 'Add to Block')
                                ]
                            )
                    ]
                ],
            ]
        ]);

        DynaGrid::end();
        ?>
    </div>
<?php Pjax::end(); ?>
<div class="clearfix"></div>
<?php

$selectRowUrl = Url::to(['/experimentCreation/planting-arrangement/select-row-in-working-list', 'id' => $id]);
$toggleSelectAllUrl = Url::to(['/experimentCreation/planting-arrangement/toggle-working-list-select-all', 'id' => $id]);
$resetGridUrl = Url::to(['/experimentCreation/planting-arrangement/manage-working-list', 'id' => $id, 'program' => $program, 'processId' => $processId]);
$removeWorkingListEntryUrl = Url::to(['/experimentCreation/planting-arrangement/remove-working-list-entry', 'id' => $id]);
$reorderWorkingListEntryUrl = Url::to(['/experimentCreation/planting-arrangement/reorder-working-list-entry', 'id' => $id]);
$saveSelectedItemsUrl = Url::to(['/experimentCreation/planting-arrangement/save-user-selection', 'id' => $id]);
$emptySelectedItemsUrl = Url::to(['/experimentCreation/planting-arrangement/empty-user-selection', 'id' => $id]);
$getSelectedItemsUrl = Url::to(['/experimentCreation/planting-arrangement/get-user-selection', 'id' => $id]);
$assignEntriesUrl = Url::to(['/experimentCreation/planting-arrangement/assign-entries', 'id' => $id, 'program' => $program, 'processId' => $processId]);

$script = <<<JS
    var selectRowUrl = '$selectRowUrl';
    var toggleSelectAllUrl = '$toggleSelectAllUrl';
    var totalWorkingListCount = '$workingListDataProvider->totalCount';

    $(document).on('submit', function(e) {
        e.stopPropagation();
        e.preventDefault();
    });

    $(document).ready(function() {
    //Get all the previously added entries to be added to the block
    var entryListArr = [];

    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();;
        // renderReorder();
    });
    // renderReorder();
    });
    $(document).on('ready pjax:complete','#grid-entries-working-list-pjax', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
    });

    //select all entries
    $(document).off('click','#working-list-checkbox').on('click','#working-list-checkbox', function(e){
        var count = 0;
        var selectAll = true;

        if($(this).prop("checked") === true){
            selectAll = true;
            $(this).attr('checked', 'checked');
            $(".assign-entries-entry-working-list").attr('checked','checked');
            $(".assign-entries-entry-working-list").prop('checked',true);
            $(".assign-entries-entry-working-list:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
        }else{
            selectAll = false;
            $(this).removeAttr('checked');
            $('.assign-entries-entry-working-list').prop('checked',false);
            $('.assign-entries-entry-working-list').removeAttr('checked');
            $("input:checkbox.assign-entries-entry-working-list").parent("td").parent("tr").removeClass("grey lighten-4");
        }

        $.ajax({
            type: 'POST',
            url: toggleSelectAllUrl,
            data: {
                selectAll: selectAll,
                totalWorkingListCount: totalWorkingListCount
            },
            async: false,
            dataType: 'json',
            success: function(totalFilteredCount) {
                if(selectAll) {
                    $('#selected-rep-count').html(totalFilteredCount);
                    $('#reorder-modal-btn').attr('disabled',false);
                    $('#remove-replicate-btn').attr('disabled',false);
                    $('#add-to-block-btn').attr('disabled',false);
                }
                else {
                    $('#selected-rep-count').html(0);
                    $('#reorder-modal-btn').attr('disabled',true);
                    $('#remove-replicate-btn').attr('disabled',true);
                    $('#add-to-block-btn').attr('disabled',true);
                }
            },
            error: function(){
                var notif = "<i class='material-icons red-text'>close</i> There was a problem in selecting item.";
                Materialize.toast(notif,5000);
            }
        });
    });

    // click row in browser
    $(document).off('click', '#grid-entries-working-list tbody tr:not(a)').on('click', '#grid-entries-working-list tbody tr:not(a)',function(e){
        e.stopPropagation();
        e.preventDefault();

        this_row=$(this).find('input:checkbox')[0];
        var index = $(this_row).data('index')

        $.ajax({
            type: 'POST',
            url: selectRowUrl,
            data: {
                index: index
            },
            async: false,
            dataType: 'json',
            success: function(response) {
                if(this_row.checked){
                    this_row.checked=false;
                    $(this).removeClass("grey lighten-4");
                } else {
                    $(this).addClass("grey lighten-4");
                    this_row.checked=true;
                    $("#replicate-index-"+this_row.id).prop("checked");
                }

                // get total selected
                var selectedCount = response['selectedCount']
                var totalFilteredCount = response['totalFilteredCount']

                // toggle select all button
                if(totalFilteredCount == selectedCount)
                    $('#working-list-checkbox').prop('checked',true);
                else
                    $('#working-list-checkbox').prop('checked',false);

                if(selectedCount > 0) {
                    $('#reorder-modal-btn').attr('disabled',false);
                    $('#remove-replicate-btn').attr('disabled',false);
                    $('#add-to-block-btn').attr('disabled',false);
                }
                else {
                    $('#reorder-modal-btn').attr('disabled',true);
                    $('#remove-replicate-btn').attr('disabled',true);
                    $('#add-to-block-btn').attr('disabled',true);
                }

                $('#selected-rep-count').html(selectedCount);
            },
            error: function(){
                var notif = "<i class='material-icons red-text'>close</i> There was a problem in selecting item.";
                Materialize.toast(notif,5000);
            }
        });

    });

    // //Reorder the list
    // function renderReorder(){
    //     var sortable1 = $('#grid-entries-working-list tbody').sortable({
    //         opacity: 0.325,
    //         tolerance: 'pointer',
    //         cursor: 'move',
    //         update: function(event, li){},
    //         cancel: "#grid-entries-working-list td:not('[class*=drag-handle-column]')",
    //         revert: true,
    //         connectWith: '#grid-entries-working-list tbody',
    //         items: ".sorting-initialize" // only insert element with class sorting-initialize
    //     });
    //     sortable1.find(".entry-sortable").one("mouseenter",function(){
    //         $(this).addClass("sorting-initialize");
    //         sortable1.sortable('refresh');
    //     });
    //     var count = 1;
    //     $(".entry-sortable").each(function() {  // initialize sortable to first few entries
    //         $(this).trigger('mouseenter');
    //         if(count >= 15) return false;
    //         count += 1;
    //     });

    //     $('#grid-entries-working-list-container').css("overflow","auto");
    // }

    // validate numeric
    $(document).on("keypress keyup blur", ".repno-input-validate", function() {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    }); 


    //retrieve user's session selection
    function getSessionUserSelection(){
        var userSelection;

        $.ajax({
          type: 'POST',
          url: '$getSelectedItemsUrl',
          async: false,
          success: function(response){
             userSelection = JSON.parse(response);
          }
        });
        return userSelection;
    }

    //Assign null value to the user session's selection
    function emptySessionUserSelection(){
        var empty = null;

        $.ajax({
            type: 'POST',
            url: '$emptySelectedItemsUrl',
            data: {
                selectedItems:empty
            },
            dataType: 'json',
            success: function(response) {
            }
        }); 
    }

     // save selected items to session
     // these are the added entries to list
     function storeSelectionToSession(selectedItems){
        var index =-1;
        if(selectedItems != null && selectedItems != []){
            $.ajax({
                type: 'POST',
                url: '$saveSelectedItemsUrl',
                data: {
                    selectedItems: JSON.stringify(selectedItems)
                },
                dataType: 'json',
                success: function(response) {
                }
            });
        }
    }

JS;
$this->registerJs($script);
?>
<style>
    #collapsible-entry-working-list {
        padding-right: 0px;
    }

    #grid-entries-working-list-container {
        overflow: auto;
    }

    /* #grid-entries-working-list2-filters {
        display: none;
    } */
</style>