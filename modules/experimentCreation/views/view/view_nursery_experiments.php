<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders browser for experiment Creation
 */
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\Experiment;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use app\modules\experimentCreation\models\ExperimentModel;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

?>
<div id="notification" class="hidden"></div>
<div class="col col-sm-12 view-entity-content">
    
    <!-- Browse Experiments -->
    <?php
        $browserId = 'dynagrid-ec-nurseryexperiment-browser';

        // all the columns that can be viewed in experiment browser
        $columns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
            ],
            [
                'class'=>'kartik\grid\ActionColumn',
                'header' => false,
                'template' => '{select}',
                'buttons' => [
                    'select' => function ($url, $model, $key) {
                        return Html::a('<i class="material-icons">exit_to_app</i>',
                            '#',
                            [
                                'class'=>'select-experiment',
                                'title' => Yii::t('app','Import entries of this experiment'),
                                'data-experiment-id' => $model['experimentDbId'],
                            ]
                        );
                    },
                ]
            ],
            [
                'attribute' => 'experimentName',
                'label' => Yii::t('app', 'Experiment'),
                'contentOptions' => [
                    'style'=>'min-width: 150px;'
                ],
            ],
            [
                'attribute' => 'experimentCode',
            ],
            [
                'attribute' => 'projectName',
                'label' => 'Project',
            ],
            [
                'attribute' => 'experimentYear',
                'label' => 'Year',
            ],
            [
                'attribute' => 'stageCode',
                'label' => 'Stage',
            ],
            [
                'label' => Yii::t('app', 'Harvested Plot Count'),
                'value' => function ($data) {
                    $count = \Yii::$container->get('app\modules\experimentCreation\models\ExperimentModel')->getHarvestedPlotsCount($data['experimentDbId']);
                    if($count == 0){
                        return '<span class="new badge red">0</span>';
                    } else {
                        return '<span class="new badge green">'.$count.'</span>';
                    }
                },
                'format' => 'raw',
            ]
        ];

        //dynagrid configuration
        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'simple-default',
            'showPersonalize'=>true,
            'storage' => DynaGrid::TYPE_SESSION,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'id' => 'grid-copyexperiment-list',
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'showPageSummary'=>false,
                'pjax'=>true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => ['id' =>'grid-copyexperiment-list'],
                    'beforeGrid' => '',
                    'afterGrid' => ''
                ],
                'responsiveWrap'=>false,
                'panel'=>[
                    'before' => '<a class="waves-effect waves-light btn pull-left" id="goback-experiments-id"><i class="material-icons left">arrow_back</i>Go back to experiment</a><br><br>'.\Yii::t('app', "Below are the list of nursery experiments you can get entries from. Click the import icon to proceed adding the entries."),
                    'after' => false,
                ],
                'toolbar' => [
                    [
                        'content'=> 
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/experimentCreation/view/view-all-nursery-experiments','id'=>$id, 'program'=>$program, 'processId'=>$processId], ['data-pjax'=>true, 'class' => 'btn btn-default', 'id'=>'reset-dynagrid-copyexperiment-browser','title'=>\Yii::t('app','Reset grid')]).'{dynagridFilter}{dynagridSort}{dynagrid}',
                    ],
                ],
                'floatHeader' =>true,
                'floatOverflowContainer'=> true,
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
            ],                                         
            
            'options'=>['id'=>$browserId] // a unique identifier is important
        ]);
        DynaGrid::end();
    ?>    
</div>
<?php
    $copyExperimentUrl = Url::to(['create/add-entries-from-plots','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $getNurseryEntries = Url::to(['view/get-harvested-plots']);
    $entryListUrl = Url::to(['create/specify-entry-list','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $checkThreshold = Url::to(['create/check-threshold-count','id'=>$id]); 
    $setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
    $currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
    (new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

    // variables for change page size
    Yii::$app->view->registerJs("
        var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
        browserId = '".$browserId."',
        currentDefaultPageSize = '".$currentDefaultPageSize."'
        ;",
    \yii\web\View::POS_HEAD);

    // js file for data browsers
    $this->registerJsFile("@web/js/data-browser.js", [
        'depends' => ['app\assets\AppAsset'],
        'position'=>\yii\web\View::POS_END
    ]);

    Modal::begin([
        'id' => 'nofication-modal',
        'header' => '<h4><i class="fa fa-bell"></i> '.\Yii::t('app','Adding of Entries').'</h4>',
        'footer' => '<p></p>',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static']
    ]);
    echo '<div class="row">
          <div class="alert ">
          <div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">
              <i class="fa fa-info-circle"></i>
                  Adding of entries in progress
          </div></div>
        </div>';
    Modal::end();

$this->registerJs(<<<JS

    $(document).on('click', '#goback-experiments-id', function(){ 
        var url = '$entryListUrl';
        window.location = url;
    });
    $(document).on('click','.select-experiment', function(e){

        var loadingIndicator = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
        var selectedExperimentId = $(this).attr('data-experiment-id');
        var currentExperimentId = '$id';

        var update_modal = '#nofication-modal > .modal-dialog > .modal-content > .modal-body';

        var url = '$copyExperimentUrl';
        var checkThresholdCountUrl = '$checkThreshold';
        var isBgUsedFlag = false;
        $('#nofication-modal').modal('show');
        $(update_modal).html(loadingIndicator);

        var dataArray = getHarvestedPlotsInfo(selectedExperimentId);

        //Notification while background job is being created
        $.ajax({
            url: checkThresholdCountUrl,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'createEntries',
                toolStep: 'entries',
                method: 'create',
                 selectedItemsCount: dataArray['count']
            },
            async: false,
            success: function(response) {
                isBgUsedFlag = response;
                if(isBgUsedFlag){
                    $(update_modal).html('<div class="alert ">'+ 
                            '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                '<i class="fa fa-info-circle"></i>'+
                                    ' A background job is being created.' +
                            '</div></div>');
                }
            }
        });

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                experimentId: currentExperimentId,
                selectedExperimentId: selectedExperimentId,
                processId: '$processId',
                entries: JSON.stringify(dataArray['entries'])
            },
            success: function(response) {
                if(response){
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully added the selected records.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    $('#nofication-modal').modal('hide');
                    $('#goback-experiments-id').addClass('pulse');
                }
            },
            error: function() {
                if(isBgUsedFlag){
                    $('#update_modal').html('<div class="alert ">'+ 
                            '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                '<i class="fa fa-info-circle"></i>'+
                                    ' A background job is being created.' +
                            '</div></div>');
                } else {
                    $(update_modal).html('<div class="card-panel red"><span class="white-text">There seems to be a problem loading the experiments.</span></div>');
                }
            }
        });
    });

    function getHarvestedPlotsInfo(experimentId){
        var dataArray;
        //get number of harvested plots
        $.ajax({
            url: '$getNurseryEntries'+'?id='+experimentId,
            type: 'post',
            dataType: 'json',
            data: {
            },
            async: false,
            success: function(response) {
                dataArray = response;
            }
        });
        return dataArray;
    }

    $(document).ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
    });
JS
);

/**
 * View file for experiment creation browser
 */
Yii::$app->view->registerCss('
    .kv-grid-wrapper {
        position: relative;
        overflow: auto;
        height: 100% !important;
    }
');
?>

