<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders data browser for all packages
 */
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;
?>

<div class="col col-sm-12 view-entity-content">

    <!-- Browse Packages -->
    <?php
        $browserId = 'dynagrid-ec-package-browser-id';

        // all the columns that can be viewed
        $columns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
            ],
            [
                'label' => Yii::t('app', 'Radio Button'),
                'header' => false,
                'headerOptions'=>['style'=>'visibility: hidden;'],
                'value' => function ($data) use ($entryNumbers) {

                    $checked = ($data['packageDbId'] == $data['currentPackageDbId'])? 'checked' : '';

                    if($entryNumbers[$data['entryNumber']] == 1){
                        $checked = 'checked';
                    }

                    return '<p>
                              <input data-entryId="'.$data['entryDbId'].'" id="'.$data['packageDbId'].'_'.$data['entryNumber'].'" class="with-gap action-page-radio-package radio-'.$data['entryDbId'].'" name="SeedLotModal['.$data['entryDbId'].']" value="'.$data['seedDbId'].'_'.$data['packageDbId'].'" type="radio" '.$checked.' />
                              <label for="'.$data['packageDbId'].'_'.$data['entryNumber'].'"></label>
                          </p>';

                },
                'format' => 'raw',
                'enableSorting' => false,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
            ],
            [
                'attribute' => 'entryNumber',
                'label' => Yii::t('app', 'Entry no.'),
                'value' => function ($data){
                    return $data['entryNumber'];
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'programName',
                'label' => Yii::t('app', 'Program'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'germplasmName',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'seedName',
                'label' => Yii::t('app', 'Seed name'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'packageLabel',
                'label' => Yii::t('app', 'Package label'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'packageQuantity',
                'label' => Yii::t('app', 'Qty'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'packageUnit',
                'label' => Yii::t('app', 'Unit'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentYear',
                'label' => Yii::t('app', 'Year'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentName',
                'label' => Yii::t('app', 'Experiment'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentType',
                'label' => Yii::t('app', 'Experiment Type'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentSeason',
                'label' => Yii::t('app', 'Season'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentStageCode',
                'label' => Yii::t('app', 'Stage'),
                'format' => 'raw',
                'enableSorting' => false,
            ],

        ];

        $dynagrid = DynaGrid::begin([
            'columns' => $columns,
            'theme'=>'simple-default',
            'showPersonalize'=>true,
            'storage' => DynaGrid::TYPE_SESSION,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => [
                'id' => 'grid-package-list',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary'=>false,
                'pjax'=>true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => ['id' =>'grid-package-list'],
                    'beforeGrid' => '',
                    'afterGrid' => ''
                ],
                'responsiveWrap'=>false,
                'panel'=>[
                    'before' => '<a class="waves-effect waves-light btn pull-left" id="goback-entry-list-id"><i class="material-icons left">arrow_back</i>Go back to entry list</a><br><br>'.\Yii::t('app', "Below are packages you can use."),
                    'after' => false,
                ],
                'toolbar' => [
                    [
                        'content'=>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/experimentCreation/view/view-all-packages?program='.$program.'&id='.$id.'&processId='.$processId.'&'.$defaultFilterStr], ['data-pjax'=>true, 'class' => 'btn btn-default', 'id'=>'reset-dynagrid-experiment-browser','title'=>\Yii::t('app','Reset grid')]).'{dynagridFilter}{dynagridSort}{dynagrid}',
                    ],
                ],
                'floatHeader' =>true,
                'floatOverflowContainer'=> true,
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
            ],
            'options'=>['id'=>$browserId]
        ]);
        DynaGrid::end();
    ?>
</div>
<?php
    $entryListUrl = Url::to(['create/specify-entry-list', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $savePackageUrl = Url::to(['create/save-package', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
    $currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
    (new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

    // variables for change page size
    Yii::$app->view->registerJs("
        var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
        browserId = '".$browserId."',
        currentDefaultPageSize = '".$currentDefaultPageSize."'
        ;",
    \yii\web\View::POS_HEAD);

    // js file for data browsers
    $this->registerJsFile("@web/js/data-browser.js", [
        'depends' => ['app\assets\AppAsset'],
        'position'=>\yii\web\View::POS_END
    ]);
?>

<?php
    $this->registerJs(<<<JS
        // Return to entry list page
        $(document).on('click', '#goback-entry-list-id', function() {
            var url = '$entryListUrl'
            window.location = url
        })

        // reload grid on pjax
        $(document).on('ready pjax:success', function(e) {
            e.stopPropagation();
            e.preventDefault();
            // refresh();
        });
        
        // Save packages upon radio button selection
        $(document).on('click', '.action-page-radio-package', function() {
            var entryDbId = $(this).attr('data-entryId')
            var seedDbId = $(this).val().split('_')[0]
            var packageDbId = $(this).val().split('_')[1]
            
            $.ajax({
                url: '$savePackageUrl',
                type: 'post',
                data: {
                    entryDbId: entryDbId,
                    seedDbId: seedDbId,
                    packageDbId: packageDbId
                },
                success: function(response) {
                    var hasToast = $('body').hasClass('toast');
                    if (!hasToast) {
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully updated the entry and reserved the package.</span>";
                        Materialize.toast(notif, 5000);
                    }
                },
                error: function() { }
            })
        })

        $(document).ready(function() {
            // Adjust browser page size, if the page size was changed in another browser
            adjustBrowserPageSize();
        });
    JS);

    Yii::$app->view->registerCss('
        .kv-grid-wrapper {
            position: relative;
            overflow: auto;
            height: 100% !important;
        }
    ');
?>

<style>
    [type="radio"]:not(:checked)+label, [type="radio"]:checked+label {
        position: relative;
        padding-left: 15px;
        cursor: pointer;
        display: inline-block;
        height: 15px;
        line-height: 35px;
        font-size: 0.5rem;
        -webkit-transition: .28s ease;
        transition: .28s ease;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
</style>