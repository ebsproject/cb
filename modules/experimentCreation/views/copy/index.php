<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use kartik\builder\Form;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\tabs\TabsX;
use app\components\GenericFormWidget;
use app\modules\experimentCreation\models\PlantingArrangement;


$copyUrl = Url::to(['/experimentCreation/create/index','program' => $program]);
$experimentName = isset($model['experimentName']) ? $model['experimentName'] : "";
echo 
'<h3>' . Html::a( Yii::t('app', 'Copy Experiment'), $copyUrl) .
    ' <small>» '. $experimentName.'</small>'.
'</h3>'; 

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/copy/basic.php',[
  'basicInformation' => $basicInformation
]);
foreach($activities as $activity){
    echo '<ul class="collapsible expandable">';
        echo '<li id="'.$activity['action'].'">';
            echo '<div class="collapsible-header">'.ucwords($activity['label']).'</div>';

            $style = '';
            if(($activity['action'] == 'specify-cross-design' && $experimentDesignType == 'Entry Order')){
                if($experimentType == 'Observation'){
                    if( isset($activity['data']['blockData']['no_of_rows_in_block']) && $activity['data']['blockData']['no_of_rows_in_block'] != NULL && $activity['data']['blockData']['no_of_rows_in_block'] > 0){
                        $style = 'style="min-height:650px;"';
                    } else $style = 'style="min-height:520px;"';
                    
                } elseif($experimentDesignType == 'Entry Order'){
                    if(isset($activity['data']['blockData']['no_of_rows_in_block']) && $activity['data']['blockData']['no_of_rows_in_block'] != NULL && $activity['data']['blockData']['no_of_rows_in_block'] > 0){
                        $style = 'style="min-height:650px;"';
                    } else $style = 'style="min-height:300px;"';
                }
                else $style = 'style="min-height:550px;"';
            } else if($activity['action'] == 'specify-design'){
                $style = 'style="min-height:600px;"';
            } else if($activity['action'] == 'specify-protocols'){
                $style = 'style="min-height:550px;"';
            }
            echo '<div class="collapsible-body" '.$style.'>';
                if(in_array($activity['action'],['specify-entry-list','manage-crosses'])){
          
                    //dynagrid configuration
                    DynaGrid::begin([
                        'columns' => $activity['data']['columns'],
                        'theme'=>'simple-default',
                        'showPersonalize'=>true,
                        'storage' => 'cookie',
                        'showFilter' => false,
                        'showSort' => false,
                        'allowFilterSetting' => false,
                        'allowSortSetting' => false,
                        'gridOptions'=>[
                            'id' => 'grid-'.$activity['action'],
                            'dataProvider' => $activity['data']['dataProvider'],
                            'showPageSummary' => false,
                            'pjax' => true,
                            'pjaxSettings' => [
                                'neverTimeout' => true,
                                'options' => ['id' =>'grid-'.$activity['action']],
                                'beforeGrid' => '',
                                'afterGrid' => ''
                            ],
                            'responsiveWrap'=>false,
                            'panel' => [
                                'heading' => false,
                                'before' => \Yii::t('app', "This is the browser for the ".$activity['data']['displayName'].".{summary}"),
                                'after' => false,
                            ],
                            'toolbar' => [
                                ['content' => '&nbsp;'],//just for the space between summary and export button
                                // 'exportConfig' => $export,   // temporary comment until export all is solved
                            ],
                            'floatHeader' => true,
                            'floatOverflowContainer' => true,
                            'resizableColumns' => true,
                        ],
                        
                        'options'=>['id'=>'dynagrid-'.$activity['action']] // a unique identifier is important
                    ]);
                    
                    DynaGrid::end();
                }
                if($activity['action'] == 'specify-occurrences'){
                    echo $grid = GridView::widget([
                        'dataProvider' => $activity['data']['dataProvider'],
                        'id' => 'view-'.$activity['action'],
                        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
                        'columns' => $activity['data']['columns'],
                        'export' => false, 
                        'panel'=>[
                            'heading'=>false,
                            'before'=> \Yii::t('app', 'This is the browser for the created occurrences. {summary}'),
                            'after' => false,
                        ],
                        'toggleData'=>true,
                        'toolbar' => [
                            ['content' => '&nbsp;'],//just for the space between summary and export button
                        ]
                    ]);
                }
                if($activity['action'] == 'specify-protocols'){
                    echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/copy/protocol.php',[
                      'data' => $activity['data'],
                      'id' => $id,
                      'program' => $program,
                      'dataProcessDbId' => $dataProcessDbId,
                    ]);
                }
                if($activity['action'] == 'specify-cross-design' && $experimentDesignType == 'Systematic Arrangement'){

                    // blocks browser
                    DynaGrid::begin([
                      'options'=>['id'=>'pa-overview-grid'],
                      'columns' => $activity['data']['columns'],
                      'theme'=>'simple-default',
                      'showPersonalize'=>false,
                      'storage' => 'cookie',
                      'showFilter' => false,
                      'showSort' => false,
                      'allowFilterSetting' => false,
                      'allowSortSetting' => false,
                      'gridOptions'=>[
                          'dataProvider'=>$activity['data']['dataProvider'],
                          'filterModel' => null,
                          'pjax'=>true,
                          'pjaxSettings' => [
                              'options' => [
                                  'enablePushState' => false,
                                  'id' => 'pa-overview-grid'
                              ],
                          ],
                          'panel'=> $activity['data']['panel'],
                          'responsiveWrap'=>false,
                          'showPageSummary'=>false,
                          'toolbar' =>  false,
                          'rowOptions'=>function($data){
                              if($data['rowgroup'] % 2){
                                  return ['class' => 'light-green lighten-5'];
                              }else{
                                  return ['class' => 'teal lighten-5'];
                              }
                          },
                      ]
                    ]);
                    DynaGrid::end();
                }
                if($activity['action'] == 'specify-cross-design' && $experimentDesignType == 'Entry Order'){
                    echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/copy/planting-arrangement.php',$activity['data']);
                }
                if($activity['action'] == 'specify-design'){
                    if($activity['data']['isParameterSet']){
                        echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/copy/copy_design.php',$activity['data']);
                    } else {
                        echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/copy/design_view.php',$activity['data']);
                    }
                }
            echo'</div>';
        echo '</li>';
    echo '</ul>';
}
$this->registerJsFile('https://code.highcharts.com/stock/highstock.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://code.highcharts.com/modules/heatmap.js',['position' => \yii\web\View::POS_END]);

$this->registerCssFile("@web/css/box.css", [
  'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'box-css');
$this->registerJsFile("@web/js/check-required-field-status.js", [
  'depends' => ['app\assets\AppAsset'],
  'position' => \yii\web\View::POS_END
]);

$resetGridUrl = Url::toRoute(['/experimentCreation/create/index', 'program' => $program]);
$checkJsonUrl = Url::to(['copy/get-layout', 'id'=>$id, 'program'=>$program]);
$startCopy =  Url::to(['copy/start-copy-experiment', 'id'=>$id, 'program'=>$program]);
$copyInformationStr = json_encode($copyInformation);
$this->registerJs(<<<JS
    var panelWidth = $('#layout-view-panel-div').width() + 10;
    var copyInformation = '$copyInformationStr';

    $("document").ready(function(){
        previewLayout();

        $('.hidden').hide();
    });

    $(document).on('click','#cancel-copy-expt-btn', function() {
        window.location = '$resetGridUrl';
    });

    $(document).on('click','#save-copy-expt-btn', function() {
        $.ajax({
            url: '$startCopy',
            type: 'post',
            dataType: 'json',
            data: {
                copyExperimentDbId:'$id',
                copyInformation: copyInformation,
                copyDesign: '$copyDesign'
            },
            success: function(response) {
            },
            error: function() {
            }
        });
    });

    function previewLayout(){
        $.ajax({
            url: '$checkJsonUrl',
            type: 'post',
            dataType: 'json',
            data: {
              panelWidth:panelWidth
            },
            success: function(response) {
                $('#layout-view-panel-div').html(response.htmlData);
            },
            error: function() {
            }
        });
    }
JS
);
?>
