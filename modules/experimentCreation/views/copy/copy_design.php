<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders design step for experiment creation
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use app\models\Program;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;

use app\modules\experimentCreation\models\TransactionModel;
?>
    <div id="mod-notif" class="hidden"></div>
    <div class="row col col-md-12">
    <div class="col col-md-12">
        <div class="col col-md-6">
            <div id="notif_field" class="hidden"></div>
            
            <?php

                echo $grid = GridView::widget([
                    'pjax' => true, // pjax is set to always true for this demo
                    'dataProvider' => $entryInfoDataProvider,
                    'id' => 'entry-info-design-browser', 
                    'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-seed-lot-table'],
                    'striped'=>false,
                    'showPageSummary'=>false,
                    'columns'=>[
                        [
                            'attribute'=>'test',
                            'label' => Yii::t('app', 'No. of Test Entries'),
                            'format' => 'raw',
                            'enableSorting' => false,
                        ],  
                        [
                            'attribute'=>'check',
                            'label' => Yii::t('app', 'No. of Checks'),
                            'format' => 'raw',
                            'enableSorting' => false,
                        ], 
                        [
                            'attribute'=>'local',
                            'label' => Yii::t('app', 'No. of Local checks'),
                            'format' => 'raw',
                            'enableSorting' => false,
                        ], 
                        [
                            'attribute'=>'total',
                            'label' => Yii::t('app', 'Total Entries'),
                            'format' => 'raw',
                            'enableSorting' => false,
                        ]
                    ]
                ]);
            ?>
            <div class="row" style="margin-bottom:0px !important">
                <div class="col col-sm-10" style="margin-left:20px; margin-bottom: 10px;">
                    <div class="col col-sm-5">
                    <?php
                        //Switch for the new UI
                        echo '<label class="control-label">'.\Yii::t('app', 'Across Environment Design').'</label>';

                        echo Select2::widget([
                            'name' => 'ExperimentDesign[ACROSS_ENV_DESIGN]',
                            'value' => isset($acrossEnvDesign) ? $acrossEnvDesign:'none',// initial value
                            'data' => ['none'=>'None', 'random'=>'Random'], //this will be hardcoded for now
                            'id' => 'acrossEnvDesign_id',
                            'options' => ['placeholder' => Yii::t('app', 'Select value'), 'class' => 'select2-Drop pull-left', 'style'=>'margin-top:25px;','disabled'=>'disabled',
                            ],
                        ]);
                    ?>
                    </div>
                </div>
                <div class="col col-sm-12" style="margin-left:25px;">
                    <div class="row" style="margin-bottom:0px !important">
                        <!-- switch for generate or upload workflow -->
                    
                    <form method="POST" id="additional-details-form" accept-charset="UTF-8" role="form" class="form-horizontal re-form form-inv">
                        <div class="col" style="margin-top:5px;">
                            <label class="cbx-label " title="Design workflow transaction" for="Design workflow" style="font-size: 1rem;color: #9e9e9e;font-weight: bold;">
                                <label data-label="Design workflow" for="workflow-switch-id">Design Workflow</label>
                            </label>
                            <div class="row switch">
                                <label>Generate&nbsp;
                                    <input type="checkbox" disabled id="workflow-switch-id" class="boolean-input-validate " name="Additional[workflow-switch-id]" value="" <?=$workflow== 'upload' ? "checked":""?> >
                                    <span class="lever "></span>Upload&nbsp;
                                </label>
                            </div>
                        </div>

                        <?php
                            if($model['stageCode'] != 'OFT'){
                                foreach($addDesignFields as $field){
                                    if(isset($field['value'])) { 
                                        echo $field['value'];
                                    }
                                }
                            }
                        ?>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col col-md-12 pull-right view-entity-content" style="margin-top:10px;overflow: hidden !important;" id="parameter-sets-panel">

            <?php 
                $parameterSetCount = $parameterSetsProvider->getCount();
                echo $grid = GridView::widget([
                    'pjax' => true, // pjax is set to always true for this demo
                    'dataProvider' => $parameterSetsProvider,
                    'id' => 'parameter-sets-browser', 
                    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
                    'striped'=>false,
                    'showPageSummary'=>false,
                    'columns'=>$columns
                ]);
            ?>
        </div>
</div>
<?php                
$this->registerJs(<<<JS
    $(document).ready(function(){
        $(".randomization").each(function(){
              
            if(this.id != '' && this.id != null && this.id != undefined){
                $("#"+this.id).attr('disabled', 'disabled');
            }
        });
        $(".layout").each(function(){
          
            if(this.id != '' && this.id != null && this.id != undefined){
                $("#"+this.id).attr('disabled', 'disabled');
            }
        });

        $("#experiment-design-form").find('input[type="checkbox"]').each( function () {
            $("#"+this.id).attr('disabled', 'disabled');
        });


    });
JS
);
?>
<style>
input.disabled {
    pointer-events: none;
    opacity: 0.5;
    cursor: not-allowed;
}
.readonly-select {
    background-color:#d5d5d5;
    opacity:0.5;
    border-radius:3px;
    cursor:not-allowed;
    position:absolute;
    top:0;
    bottom:0;
    right:0;
    left:0;
}
.fa-download:hover, .fa-download:active{
    color: #005580;
    cursor: pointer;
}
#notif_field{
    font-size: 14px;
    padding: 10px;
    height: 30px;
    line-height: 0px;
}
label.active {
    color: #9e9e9e;
}
span.badge {
    margin-left: 14px;
    float: left;
}
.collapsible span.badge {
    margin-top: 0px;
}

.summary{
    display:none;
}
.container-design {
    /*background-color: white; */
    min-height: 300px;
    max-height: 600px;
    position: relative;
    overflow: auto;
    /* margin: 5px 0; */
    border: 1px solid #ccd5d8;
    border-radius: 10px;
}
.panel-error > .card-panel {
    background: #ffcdd2;
    color: #F44336;
}
.panel-warning > .card-panel {
    background: #d9edf7;
    color: #31708f;
}
</style>