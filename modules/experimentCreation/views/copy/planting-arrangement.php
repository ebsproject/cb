<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use app\modules\experimentCreation\models\PlantingArrangement;

echo '<h5> Arrangement Information</h5>';

// build starting corner values
$startingCornerValuesOpts = '';
foreach ($startingCornerValues as $a => $b) {
    $startingCornerValuesOpts .= '<option value="'.$b.'">'.$b.'</option>';
}

// build plot numbering values
$plotNumberingOrderValuesOpts = '';
foreach ($plotNumberingOrderValues as $a => $b) {
    $plotNumberingOrderValuesOpts .= '<option value="'.$b.'">'.$b.'</option>';
}

//show fields
echo '<div class="col col-md-12"><div class="col col-md-6 entry-order-panel pa-blocks-width">';

foreach ($blockData as $key => $value) {

    $class = true;

    $data = \Yii::$container->get('app\modules\experimentCreation\models\PlantingArrangement')->getFormDisplay($key);
    
    $label = isset($data['name']) && !empty($data['name']) ? $data['name'] : ucfirst(str_replace('_', ' ', $key));
    $description = !empty($data['description']) ? $data['description'] : ucfirst(str_replace('_', ' ', $key));

    if(in_array($key, ['plot_numbering_order','starting_corner'])){
        $options = ($key == 'plot_numbering_order') ? $plotNumberingOrderValuesOpts : $startingCornerValuesOpts;

        $options = str_replace('value="'.$value.'"', 'value="'.$value.'" selected', $options);

        echo '<div class="col col-md-6"><div class="col col-md-5"><label class="control-label" title="'.$description.'">'.$label.'</label></div>';
        
        $select = '<select id="'.$key.'" disabled class="browser-default pa-entry-order-params pa-entry-order-params-'.$key.' pa-dimension-fields pa-fields-select" data-abbrev="'.$key.'" style="height:auto;" data-id="'.$subBlockId.'">'.$options.'</select>';

        echo '<div class="col col-md-7">'.$select.'</div></div>';
    }else{
        
        $dimension = (in_array($key, ['no_of_entries_in_block', 'total_no_of_plots_in_block', 'no_of_reps_in_block','no_of_occurrences'])) ? '' : 'pa-dimension-fields';
        
        echo '<div class="col col-md-6"><div class="col col-md-5"><label class="control-label"    title="'.$description.'">'.$label.'</label></div>';
        echo '<div class="col col-md-7">'.Html::input('number', 'pa-entry-order-params_'.$key, $value, ['class'=>'pa-entry-order-params pa-param-computer input-integer numeric-input-validate pa-entry-order-params-'.$key.' '.$dimension, 'id'=>$key, 'data-abbrev'=>$key,'data-id'=>$subBlockId,'data-plot'=>$totalPlots,'min'=>1,'disabled'=>$class]).'</div></div>';
    }
}

$checked = '';
$noOfRows = 0;
if(isset($blockData['no_of_rows_in_block']) && $blockData['no_of_rows_in_block'] != NULL && $blockData['no_of_rows_in_block'] > 0){
    $checked = 'checked';
    $noOfRows = $blockData['no_of_rows_in_block'];
}

echo '<div class="col col-md-12"><p>
        <input type="checkbox" disabled class="pull left filled-in" '.$checked.' id="define-dimension-chkbox"/>
        <label for="define-dimension-chkbox">Define dimension</label>
    </p></div>
';

if($experimentType == 'Observation'){
    echo '<div class="col col-md-12">';
    echo '<b> Check Information</b>';
    echo '<div class="row" id="check-info-div" style="margin-right:30px;margin-left:30px;">';
    
    echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/entry-order/_check_groups_view.php',[
        'checkInformation' => $checkInformation,
        'id' => $id,
        'viewOnly' => 1,
        'subBlockId' => $subBlockId
    ]);

    echo '</div>';
    echo '</div>';
}

echo '</div>';

echo ' <!--- start results area -->
<div class="col col-md-6" id="layout-view-panel-div"></div>';
echo '</div>';
?>