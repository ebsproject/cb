<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


use yii\bootstrap\ActiveForm;
use app\components\GenericFormWidget;
use yii\helpers\Html;
use yii\helpers\Url;

echo '<div class="row">';
echo '<div class="row"><span><br>&nbsp;<b>Basic Information</b></span>&nbsp;'.
Html::a('Copy Experiment','#',[
    'style'=>'margin-right:0px;',
    'type'=>'button',
    'title'=>\Yii::t('app',"Click to start copying of experiment"),
    'class'=>'tabular-btn btn waves-effect waves-light pull-right',
    'id'=>'save-copy-expt-btn'
]).
Html::a('Cancel','#',[
    'style'=>'margin-right:5px;',
    'type'=>'button',
    'title'=>\Yii::t('app',"Cancel process"),
    'id'=>'cancel-copy-expt-btn',
    'class'=>'grey lighten-3 black-text pull-right tabular-btn btn waves-effect waves-light'
])."</div>";
$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'basic-form',
    'action' => '#'
]);

echo GenericFormWidget::widget([
    'options' => $basicInformation['data']
]);

ActiveForm::end();
echo '</div>';

$this->registerJs(<<<JS
    $( "form#basic-form" ).find( "#basic-info1" ).addClass('widen-basic-info1');
    $(document).ready(function(){
        $('#basic-info1').addClass('col col-md-6');
        $(".text-input-validate").each(function(){
            $("#"+this.id).attr('disabled', 'disabled');
        });
        $(".numeric-input-validate").each(function(){
            $("#"+this.id).attr('disabled', 'disabled');
        });
        $(".select2-input-validate").each(function(){
            $("#"+this.id).attr('disabled', 'disabled');
        });
    });
JS
);
?>

<style>
.required{
    color:red;
}
label{
    font-size: .8rem;
    color: #333333;
  }
.readonly{
    cursor: not-allowed;
    user-select: none;
    -webkit-user-modify: read-only;
    pointer-events: none;
    border: 1px solid grey;
    background-color: lightgrey;
}
.div-class{
    margin-bottom: 10px;
}
.my-container{
    min-width: 50%;
    max-width: 70% !important;
}
.card{
    height: 100%;
    margin-left:-10px;
    margin-top:-10px;
    padding: 30px;
}
.form-inline input{
    flex-direction: column;
    align-items: stretch;
    vertical-align: middle;
}
.form-inline { 
    display: flex;
    flex-flow: row wrap;    
    align-items: center;
}
.view-entity-content{
    background: #ffffff;
}
#basic-info1{
    margin-left:auto;
    margin-right:auto;
    margin-top:15px;
    display: inline-block;
}
#basic-info2{
    margin-left:auto;
    margin-right:auto;
    margin-top:20px;
    display: inline-block;
}
.parent-box{
    position: relative;
    margin-top: 1rem;
}
.main-panel{
    margin-left: 5%;
}
</style>