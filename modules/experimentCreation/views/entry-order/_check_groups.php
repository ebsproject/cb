<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders check groups settings
*/


use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use app\dataproviders\ArrayDataProvider;

// all the columns that can be viewed in seasons browser
$columns = [
    [
        'header' =>Yii::t('app', 'Start checks'),
        'format' => 'raw',
        'headerOptions'=>['style' => 'width:50%'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data) use ($checkInformation){
            $options = '';

            foreach($checkInformation['check_array'] as $key=>$value){
                $selected = '';
                if(in_array($key,$checkInformation['begins_with_check_group'])){
                    $selected = 'selected="selected"';
                }
                $options .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
            }
            $value = '<select id="begins_with_check_group" class="check-select2 form-control" name="startChecks[]" multiple>'.$options.'</select>';
    
            return $value;
        },
      ],
      [
        'header' =>Yii::t('app', 'End checks'),
        'format' => 'raw',
        'headerOptions'=>['style' => 'width:50%'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data) use ($checkInformation){
          $options = '';

          foreach($checkInformation['check_array'] as $key=>$value){
              $selected = '';
              if(in_array($key,$checkInformation['ends_with_check_group'])){
                  $selected = 'selected="selected"';
              }
              $options .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
          }
          $value = '<select id="ends_with_check_group" class="check-select2 form-control" name="endChecks[]" multiple>'.$options.'</select>';
    
            return $value;
        },
    ]
];  
$dataProvider1 = new ArrayDataProvider([
  'allModels' => [$checkInformation],
  'key' => 'check_group_interval',
  'pagination' => false,
  'sort' => false
]);

echo GridView::widget([
  'pjax' => true, // pjax is set to always true for this demo
  'dataProvider' => $dataProvider1,
  'id' => 'check-information-grid',
  'tableOptions' => ['id' => 'check-information-table', 'class' => 'table table-bordered white z-depth-3'],
  'columns' => $columns,
  'showPageSummary'=>false,
  'striped'=>false
]);

echo '<b style="margin-top:5px; margin-bottom:0px;">Specify Check Groups &nbsp;</b>';

echo '<p style="margin-bottom:15px;"></p><div class="col col-md-6"><label class="control-label pull-left check-interval_class" style="margin: 10px;" title="Change value of Check Group interval">Group Interval</label>'.Html::input('number', 'check-interval-apply', $checkInformation['check_group_interval'], ['id' => 'check-interval-field-id', 'class' => 'check-interval_class check-interval-validate numeric-input-validate pull-left', 'style'=>'width:75px;', 'min'=> 1, 'disabled'=>'disabled']).'</div>'.
'<div class="col col-md-6">'.Html::a('Add group','#',['style'=>'margin-right:0px;','type'=>'button', 'title'=>\Yii::t('app',"Add Nursery blocks"), 'class'=>'pull-right tabular-btn btn black-text grey lighten-2 waves-effect waves-light','id'=>'check-add-rows-btn']).'</div>';

echo '<table id="check-groups-table2" class="table table-bordered white kv-grid-table kv-table-wrap">';
echo '<th class="kv-align-middle" style="width:95%">CHECK GROUPS</th><th class="kv-align-middle" style="width:5%"></th>';
echo '<div id="check-groups-body-div">';
    if(isset($checkInformation['check_groups']) && !empty($checkInformation['check_groups'])){

        foreach($checkInformation['check_groups'] as $group){

            $options = '';

            foreach($checkInformation['check_array'] as $key=>$value){
                $selected = '';
                if(in_array($key,$group['members'])){
                    $selected = 'selected="selected"';
                }
                $options .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
            }
            $groupStr = '<select id="'.$group['group'].'" class="check-groups-select2 form-control" name="checkGroups[]" multiple>'.$options.'</select>';
            $removeStr = Html::a('<i class="material-icons">delete</i>',false, 
                [
                  'class'=>'pa-remove-row-checkGrp',
                  'title' => 'Remove row',
                  'style'=>"cursor:pointer;",
                  'data-id'=>$group['group']
                ]);
            $trStr = '<tr data-id="'.$group['group'].'" class="tr-checkgrp-class">'.
                '<td>'.$groupStr.'</td><td>'.$removeStr.'</td></tr>';

            echo $trStr;
        }
    } else {
        echo '<tr><td id="no-results-id" colspan="2">No results found.</td></tr>';
    }
echo '</div>';
echo '</table>';

$optionsStr = '';
foreach($checkInformation['check_array'] as $key=>$value){
  $optionsStr .= '<option value="'.$key.'">'.$value.'</option>';
}
$addCheckGroup = Url::to(['/experimentCreation/entry-order/add-check-group','id'=>$id]);

$this->registerJsFile("@web/js/generic-form-validation.js", [
  'depends' => ['app\assets\AppAsset'],
  'position'=>\yii\web\View::POS_END
]);
?>

<script>
    var check_groups = JSON.parse('<?= json_encode($checkInformation['check_groups']) ?>');
    var begins_with_check_group = JSON.parse('<?= json_encode($checkInformation['begins_with_check_group']) ?>');
    var ends_with_check_group = JSON.parse('<?= json_encode($checkInformation['ends_with_check_group']) ?>');
    var check_group_interval = JSON.parse('<?= json_encode($checkInformation['check_group_interval']) ?>');
    var check_array = JSON.parse('<?= json_encode($checkInformation['check_array']) ?>');
    var optionsStr = '<?=$optionsStr?>';
    var flagChange = false;
    $(document).ready(function() {

        $('.check-select2').select2();
        $('.check-groups-select2').select2();
        $('#check-information-grid .summary').html('&nbsp;');
        $('#check-groups-grid .summary').html('&nbsp;');

        if(check_groups.length > 0){
            $('#check-interval-field-id').removeAttr('disabled');
        }
    });

    $(document).on('click','.pa-remove-row-checkGrp', function(){
        flagChange = true;
        check();
        var id = $(this).attr("data-id");
        $(this).closest('tr').remove();
        removeRow(id);
    });

    function removeRow(id){
        for(var i=0; i<(check_groups).length;i++){
            if(check_groups[i]['group'] == id){
                check_groups.splice(i, 1);
            }
        }

        if(check_groups.length==0){
            var newRow = '<tr id="no-results-id"><td colspan="2">No results found.</td></tr>';
            var tableBody = $("#check-groups-table2 tbody");
            tableBody.append(newRow);
            $('#check-interval-field-id').val(0);
            $('#check-interval-field-id').attr('disabled', 'disabled');
        } else {
            $('#check-interval-field-id').removeAttr('disabled');
        }
    }

    function check(){
        
        check_group_interval = $('#check-interval-field-id').val();
        if(check_groups.length > 0 && check_group_interval == 0){
            flagChange = false;
        }
        //check if there's no error
        var errors = 0;
        $(".error-notif").each(function(){
            errors++;
        });

        if(flagChange && errors == 0){
            $('#reset-note').html('<span class="red-text">Note: The ordering of plots will be reset once these changes are saved.</span>');
            $('#pa-entry-order-save-btn').removeClass('disabled');
            $('#pa-entry-order-save-btn').addClass('pulse');

            $('#pa-entry-order-entries-btn').addClass('disabled');
            
        } else {
            $('#pa-entry-order-save-btn').addClass('disabled');
            $('#pa-entry-order-save-btn').removeClass('pulse');
        }
    }

    $(document).on('change','.check-groups-select2', function(){
        var values = $(this).val();
        var id = this.id;
        
        storeCheckGroup(id, values);
    });

    function storeCheckGroup(id, values){
        var flagChecks = 0;
        for(var i=0; i<(check_groups).length;i++){
            if(check_groups[i]['group'] == id){
                check_groups[i]['members'] = values;
            }
            if(check_groups[i]['members'].length > 0){
                flagChecks = 1;
            }
        }
        if(flagChecks == 1){
            $('#check-interval-field-id').removeAttr("disabled");
            check_group_interval = $('#check-interval-field-id').val();
            if(check_group_interval > 0){
                flagChange = true;
            } else {
                flagChange = false;
            }
            if(!$('#check-interval-field-id').hasClass('invalid') && $('#check-interval-field-id').attr('disabled') === undefined){
                if(check_group_interval > 0){
                    $(".red-text").each(function(){
                        $(this).remove();
                    });
                    $('#check-interval-field-id').removeClass('invalid');
                } else {
                  $('#check-interval-field-id').focus();
                  $('#check-interval-field-id').addClass('invalid');
                  $('#check-interval-field-id').after("<span class='red-text'><br>Value must be > 0</span>");
                }
            }
        } else{
            flagChange = true;
            $('#check-interval-field-id').val(0);
            $('#check-interval-field-id').attr("disabled", "disabled");
            $(".red-text").each(function(){
                $(this).remove();
            });
            $('#check-interval-field-id').removeClass('invalid');
        }
        check();
    }

    $(document).on('change','.check-select2', function(){
        var values = $(this).val();
        var id = this.id;
        
        storeChecks(id, values);
    });

    function storeChecks(id, values){
        if(id == 'begins_with_check_group'){
            begins_with_check_group = values;
        }
        if(id == 'ends_with_check_group'){
            ends_with_check_group = values;
        }
        
        flagChange = true;
        check();
    }

    $("#check-interval-field-id").change(function(e, params) {
        if(this.value > 0){
            $(".red-text").each(function(){
                $(this).remove();
            });
            $('#check-interval-field-id').removeClass('invalid');
            flagChange = true;
        } else {
          $('#check-interval-field-id').focus();
          $('#check-interval-field-id').addClass('invalid');
          $('#check-interval-field-id').after("<br><span class='red-text'>Value must be > 0</span>");
          flagChange = false;
        }
        checker();
    });

    $('#check-add-rows-btn').click(function(){
        //Add new rows
        check();
        if(check_groups.length < 10){
            var groupNo = (check_groups.length > 0) ? check_groups.length+1 : 1;
            if(groupNo > 1){
                var maxGrp = check_groups[check_groups.length-1]['group'].split("_");
                groupNo = parseInt(maxGrp[2])+1;
            } else {
                $("#no-results-id").closest('tr').remove();
            }

            check_group_interval = $('#check-interval-field-id').val();

            if(!$('#check-interval-field-id').hasClass('invalid') && $('#check-interval-field-id').attr('disabled') === undefined){
                if(check_group_interval > 0){
                    $(".red-text").each(function(){
                        $(this).remove();
                    });
                    $('#check-interval-field-id').removeClass('invalid');
                } else {
                  $('#check-interval-field-id').focus();
                  $('#check-interval-field-id').addClass('invalid');
                  $('#check-interval-field-id').after("<span class='red-text'><br>Value must be > 0</span>");
                }
            }
        
            var selectStr = '<td><select id="check_group_'+groupNo+'" class="check-groups-select2 form-control" name="checkGroups[]" multiple>'+optionsStr+'</select></td>';
            var tableBody = $("#check-groups-table2 tbody");
            var removeBtn = '<td><a class="pa-remove-row-checkGrp" data-id="check_group_'+groupNo+'" title="Remove row" style="cursor:pointer;"><i class="material-icons">delete</i></a></td>';
            var newRow = '<tr>'+selectStr+removeBtn+'</tr>';

            check_groups.push({'members':[],'group':'check_group_'+groupNo});
            tableBody.append(newRow);
            $('.check-groups-select2').select2();
        } else {
            var hasToast = $('body').hasClass('toast');            

            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>The maximum number of check groups is 10.</span>";
                Materialize.toast(notif, 5000);
            }
        }
    });

    
</script>

<style>
#check-information-grid > .summary, #check-groups-grid > .summary{
    padding-top: 10px;
}
#check-groups-grid > .table-responsive {
    min-height: 0.01%;
    overflow-x: inherit !important;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #337ab7;
    color: #fff;
}
.select2-container--default .select2-results__option[aria-selected=true] {
    color: #31708f;
    background-color: #d9edf7;
}
.select2-container--default.select2-container--open.select2-container--below .select2-selection--single, .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-color: transparent;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    color: #555555;
    background: #f5f5f5;
    border: 1px solid #ccc;
    border-radius: 4px;
    cursor: default;
    float: left;
    margin: 5px 0 0 6px;
    padding: 0 6px;
}
.select2-container--default.select2-container--focus .select2-selection--multiple {
    border: solid black 1px;
    outline: 0;
    border-color: #66afe9;
    border-radius: 4px;
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%), 0 0 6px rgb(102 175 233 / 60%);
}
#check-groups-grid > .select2-container .select2-selection--multiple {
    box-sizing: border-box;
    cursor: pointer;
    display: flex;
    height: 32px;
    user-select: none;
    -webkit-user-select: none;
}
</style>