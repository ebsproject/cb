<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders entry order settings
*/
use app\models\Variable;
use yii\helpers\Url;
use kartik\tabs\TabsX;
use yii\helpers\Html;
use app\modules\experimentCreation\models\PlantingArrangement;

echo '<div id="manage-entries-browser" style="padding: 0">';

$managePlotsUrl = Url::to(['/experimentCreation/entry-order/manage-cross-pattern-plots-in-block', 'id'=>$id, 'program'=>$program, 'processId'=>$processId, 'blockId'=> $subBlockId, 'experimentType'=>$experimentType]);
$checkStatusUrl = Url::to(['/experimentCreation/create/check-status','id'=>$id, 'action'=>'cross-design']);

Yii::$app->view->registerJs(
    "
        checkStatusUrl = '" . $checkStatusUrl . "',
        experimentId = '" . $id . "',
        requiredFieldJson = 'null',
        checkStatValue = 'design generated'
        ;",
    \yii\web\View::POS_HEAD
);
?>

<script type="text/javascript">
    var experimentStatus = '<?= $experiment['experimentStatus'] ?>';

    $(document).ready(function(){
        if(experimentStatus.includes('design generated')){
            $("#next-btn").removeAttr('disabled');
        }

        $.ajax({
            type: 'POST',
            url: '<?= $managePlotsUrl?>',
            data: {},
            async: true,
            success: function(data) {
                $('#manage-entries-browser').html(data);
                $('.preloader-wrapper').addClass('hidden');
                $('.collapsible-entry-grid').click();
            },
            error: function(){
                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading entries. Please try again.';
                Materialize.toast(notif, 5000);
            }
        });
    });
    checkStatus();
</script>

<style>
    .tabs-left .tab-content {
        border-bottom: none !important;
        /* border-left: 1px solid #ddd; */
    }
    .box-body {
        padding: 0px 15px 10px 15px;
        max-height: 800px;
        overflow-y: hidden;
        overflow-x: hidden;
        background-color: #f1f4f5;
        /* border: none; */
    }
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
        opacity: 0;
    }

</style>