<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting arrangement manage block form
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\dataproviders\ArrayDataProvider;
use app\modules\experimentCreation\models\PlantingArrangement;
use app\models\Entry;
use kartik\dynagrid\DynaGrid;

$checkColumn = [[
    'label'=> "Checkbox",
    'headerOptions'=>['class'=>'check-box-header check-box-header-entries','data-id'=>$subBlockId],
    'header' => '
        <input type="checkbox" class="filled-in" id="pa-entry-order-manage-entries-select" data-id="'.$subBlockId.'"/>
        <label for="pa-entry-order-manage-entries-select" style="height: 11px;"></label>',
    'content'=>function($model) use ($subBlockId, $checksUsed){
        $disabled = '';
        if(in_array($model['entryDbId'], $checksUsed)){
            $disabled = 'disabled="disabled"';
        }
        return '
            <input type="checkbox" class="pa-manage-entries-grid_select filled-in" '.$disabled.' data-id="'.$subBlockId.'"  id="'.$model['entryDbId'].'" data-entryDbId="'.$model['entryDbId'].'" />
            <label for="'.$model['entryDbId'].'"></label>
        ';
    },
    'hAlign'=>'center',
    'vAlign'=>'middle',
    'contentOptions' => function($model) use ($subBlockId){
        return ['class' => 'pa-manage-entries-reorder-entries-row', "data-entryDbId"=>$model['entryDbId'], 'data-id'=>$subBlockId];
    }
]];
$columns = [
    [
        'attribute'=>'entryNumber',
        'label' => '<span title="Sequential numbering of entries">'.Yii::t('app', 'Entry No.').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false
    ],
    [
        'attribute'=>'designation',
        'label' => '<span title="Germplasm">'.Yii::t('app', 'Germplasm Name').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
    ],
    [
      'attribute'=>'parentage',
      'label' => '<span title="Pedigree">'.Yii::t('app', 'Pedigree').'</span>',
      'format' => 'raw',
      'encodeLabel'=>false,
    ],
    [
      'attribute'=>'entryType',
      'label' => '<span title="Entry type">'.Yii::t('app', 'Entry Type').'</span>',
      'format' => 'raw',
      'encodeLabel'=>false
    ]
];
$disableBulk = '';
if($experimentType == 'Observation'){
    $disableBulk = 'hidden';  
} else {
    $columns = array_merge($checkColumn, $columns);
}

// Only show parent role column when experiment type is Cross Parent Nursery or Intentional Crossing Nursery
if($experimentType == 'Cross Parent Nursery' || $experimentType == 'Intentional Crossing Nursery') {

    $columns[] = [
        'attribute'=>'entryRole',
        'label' => Yii::t('app', 'Parent Role'),
    ];
}
$columns[] = [
  'label' => '<span title="Number of Replicates">'.Yii::t('app', 'No of Replicates').'</span>',
  'attribute'=>'repno',
  'format' => 'raw',
  'filter' =>false,
  'encodeLabel'=>false,
  'value' => function ($data) use ($entryIdAndRepno, $checksUsed){
      
      $repno = $entryIdAndRepno[$data['entryDbId']];
      if(in_array($data['entryDbId'], $checksUsed)){
          $value = Html::input('number', 'repno-input', $repno, ['id' => 'repno-' . $data['entryDbId'], 'class' => 'repno-input-editable numeric-input-validate', 'min'=> 1,'data-name' => 'repno', 'disabled'=>'disabled']);
      } else {
          $value = Html::input('number', 'repno-input', $repno, ['id' => 'repno-' . $data['entryDbId'], 'class' => 'repno-input-editable numeric-input-validate', 'min'=> 1,'data-name' => 'repno']);
      }
      
      return $value;
  }
];

$dynagrid = DynaGrid::begin([
    'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>false,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'dataProvider' => $dataProvider,
        'filterModel'=>$filterModel,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>'pa-entry-order-manage-entries-grid2',
                'enablePushState'=>false
            ],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel' => [
            'heading'=>false,
            'before' => \Yii::t('app', '<b>NOTE:</b>The arrangement will automatically reset upon updating no. of replicates of the entries.').' {summary} ',
            'after' => false,
        ],
        'floatHeader' => true,
        'floatOverflowContainer'=> true,
        'toolbar' => [
                'content' => '<label class="control-label pull-left apply-no-of-reps_class '.$disableBulk.'" style="margin: 10px;" title="Change value of No. of Replicates">No Of Replicates</label>'.
                Html::input('number', 'repno-field-apply', 1, ['id' => 'repno-field-apply-id', 'class' => 'apply-no-of-reps_class repno-input-validate numeric-input-validate pull-left '.$disableBulk, 'style'=>'width:75px;', 'min'=> 1,'data-name' => 'repno']).
                Html::a('Save Changes','#',[
                  'type'=>'button', 
                  'title'=>\Yii::t('app',"Save changes in the browser"),
                  'class'=>'save-updates_class tabular-btn btn waves-effect waves-light disabled pull-right',
                  'id'=>'pa-entry-order-save-update-btn'
                ]).
                Html::a('Bulk Apply','#',[
                  'style'=>'margin-left:10px;','type'=>'button', 
                  'title'=>\Yii::t('app',"Apply to selected entries"),
                  'class'=>'grey lighten-3 black-text apply-no-of-reps_class tabular-btn btn waves-effect waves-light disabled '.$disableBulk,
                  'id'=>'pa-entry-order-apply-btn'
              ])
              
        ],
      ],
      'options'=>['id'=>'pa-entry-order-manage-entries-grid'],
  ]);
  DynaGrid::end();
?>
<?php
// remove a plot modal body
Modal::begin([
  'id' => 'pa-manage-entries-apply-modal',
  'header' => '<h4>'.\Yii::t('app','Apply changes'). '</h4>',
  'footer' =>
      Html::a('Cancel', ['#'], ['id' => 'pa-manage-entries-apply-modal-cancel-btn', 'style'=>'margin-right:15px', 'data-dismiss'=> 'modal']).'&nbsp;'.
      Html::a(\Yii::t('app','Confirm').'<i class="material-icons right">send</i>','#',[
          'class'=>'btn waves-effect waves-light pa-manage-entries-apply-confirm-btn',
          'data-id'=>$subBlockId,
          'style'=>'margin-right:15px'
      ]),
  'size' =>'modal-md',
  'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);
echo '<div class="pa-manage-entries-apply-modal-body"></div>';

Modal::end();

$count = $dataProvider->getTotalCount();
$initialFilterDisappear = ($count <= getenv('CB_DATA_BROWSER_DEFAULT_PAGE_SIZE'))? true : false;

$saveRepnoUrl = Url::to(['/experimentCreation/entry-order/update-block-information?experimentId='.$id]);
$managePlotsUrl = Url::to(['/experimentCreation/entry-order/manage-entries-in-block', 'id'=>$id,'blockId'=> $subBlockId, 'experimentType'=>$experimentType]);

$this->registerJsFile("@web/js/generic-form-validation.js", [
  'depends' => ['app\assets\AppAsset'],
  'position'=>\yii\web\View::POS_END
]);

?>

<script type="text/javascript">
    var blockId = '<?= $subBlockId ?>';
    var entryDbIds = [];
    var selectAll = false;
    var excludeEntryDbIds = [];
    var valueArray = [];
    var column = '';
    var experimentType = '<?= $experimentType ?>';
    var initialFilterDisappear = '<?= $initialFilterDisappear ?>';
    var managePlotsUrl = '<?= $managePlotsUrl ?>';

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
    });

    $(document).ready(function(){
        $('.pagination .active').find('a').trigger('click');

        // This solves the bug where the filter input row disappears and would only appear after scrolling when there is only 1 page of results
        // The bug is that after loading the browser for some reason will not trigger a pjax:success event (which it normally does when there are more than 1 page of results)
        if(initialFilterDisappear) {
            $.pjax.reload({
                container: '#pa-entry-order-manage-entries-grid-pjax',
                replace: false,
                url: managePlotsUrl
            });
        }
    });

    // select all plots
    $(document).on('click','.check-box-header-entries',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        
        var sbId = $(this).data('id');
        checkbox=$('.check-box-header-entries').find('input:checkbox')[0];

        if(!checkbox.checked){
            $('#pa-entry-order-manage-entries-select').prop('checked',true);
            $('#pa-entry-order-manage-entries-select').attr('checked','checked');
            $('.pa-manage-entries-grid_select').attr('checked','checked');
            $('.pa-manage-entries-grid_select').prop('checked',true);
            $(".pa-manage-entries-grid_select"+":checkbox").parent("td").parent("tr").addClass("grey lighten-4");
            $('#pa-entry-order-apply-btn').removeClass('disabled');
            selectAll = true;
            excludeEntryDbIds = [];
        }else{
            $('#pa-entry-order-manage-entries-select').prop('checked',false);
            $('#pa-entry-order-manage-entries-select').removeAttr('checked');
            $('.pa-manage-entries-grid_select').prop('checked',false);
            $('.pa-manage-entries-grid_select').removeAttr('checked');
            $("input:checkbox.pa-manage-entries-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");
            $('#pa-entry-order-apply-btn').addClass('disabled');
            selectAll = false;
            excludeEntryDbIds = [];
        }
    });

    // click row in manage plots grid
    $(document).on('click',"#pa-entry-order-manage-entries-grid tbody tr",function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        if(experimentType != 'Observation'){
            this_row=$(this).find('input:checkbox')[0];
            
            var sbId = $(this_row).data('id');
            if(typeof this_row.getAttribute('disabled') === 'undefined' || this_row.getAttribute('disabled') === false || this_row.getAttribute('disabled') === null){
                if(this_row.checked){
                    this_row.checked=false;
                    $('#pa-entry-order-manage-entries-select').prop("checked", false);
                    $('#pa-entry-order-manage-entries-select').removeAttr('checked');
                    $(this).removeClass("grey lighten-4");
                    checkbox=$('.check-box-header').find('input:checkbox')[0];
                    checkbox.checked = false;

                    if(selectAll == true){
                        var entryId = $(this).find('input:checkbox.pa-manage-entries-grid_select').attr('data-entryDbId');
                        excludeEntryDbIds.push(entryId);
                    }
                }else{
                    $(this).addClass("grey lighten-4");
                    this_row.checked=true;
                }

                if($('.pa-manage-entries-grid_select').is(':checked')){
                    $('#pa-entry-order-apply-btn').removeClass('disabled');
                }else{
                    $('#pa-entry-order-apply-btn').addClass('disabled');
                }
            }
        }
    });

    $(document).on('change', ".repno-input-editable", function(event) {
        var id = this.id;
        var fields = (this.id).split('-');
        column = fields[0];
        var entryDbId = fields[1];
        var value = $('#' + id).val();

        // Save the changes in the database
        if(value != null && value != 0){
            entryDbIds.push(entryDbId);
            valueArray.push(value);            
        } else {
            var hasToast = $('body').hasClass('toast');

            if(!hasToast){
                $('.toast').css('display','none');
                var resetNotif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>Please correct the invalid input for no. of replicates. The minimum input value is 1.</span>";
                Materialize.toast(resetNotif, 5000);
            }
        }

        if(entryDbIds.length > 0){
            $('#pa-entry-order-save-update-btn').removeClass('disabled');
            $('#pa-entry-order-save-update-btn').addClass('pulse');
        }
        
    });

    $(document).on('click','#pa-entry-order-save-update-btn', function(e){
        $.ajax({
            url: '<?=$saveRepnoUrl?>',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                entryDbIds: entryDbIds,
                column: column,
                blockId: blockId,
                updates:true,
                value: valueArray,
                selectAll: selectAll
            },
            success: function(response){
                var hasToast = $('body').hasClass('toast');

                if(!hasToast){
                    $('.toast').css('display','none');
                    var resetNotif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>The ordering of plots was reset.</span>";
                    Materialize.toast(resetNotif, 5000);
                }

                $('.pa-entry-order-tab').click();

                location.reload();
            },
            error: function (err) {
            }
        });
    });

    // reset plots in block
    $(document).on('click','#pa-entry-order-apply-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        entryDbIds = [];
        // Save the changes in the database
        var value = $('#repno-field-apply-id').val();
        if(value != null && value != 0){
            $('input:checkbox.pa-manage-entries-grid_select').each(function(){
                if($(this).prop('checked') === true) {
                    var entryId = $(this).attr('data-entryDbId');
                    entryDbIds.push(entryId);
                }
            });
            
            $('#pa-manage-entries-apply-modal').modal('show');

            var warningIcon = '<i class="material-icons orange-text left">warning</i>';
            var message = 'You are about to update the no. of replicates information. If you wish to proceed, the arrangement will be reset. Click "Confirm" to proceed.';
            $('.pa-manage-entries-apply-modal-body').html(warningIcon +' '+ message);
          } else {
              
              $('#pa-manage-entries-apply-modal-cancel-btn').click();
              
              var hasToast = $('body').hasClass('toast');
  
              if(!hasToast){
                  $('.toast').css('display','none');
                  var resetNotif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>Please correct the invalid input for no. of replicates. The minimum input value is 1.</span>";
                  Materialize.toast(resetNotif, 5000);
              }
          }

    });

    // confirm updating of plots
    $(document).on('click','.pa-manage-entries-apply-confirm-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var obj = $(this);
        var sbId = obj.data('id');

        var column = 'repno';
        
        $('.pa-manage-entries-apply-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        var filters = {};
        var filtered = false;
        $('.filters').find('.form-control').each(function(){

            if(this.value != ""){
                var name = this.name;
                var keyStr = name.replace('EntrySearch[', '');
                keyStr = keyStr.replace(']', '');
                filters[keyStr] = this.value;
            }
        });
        var value = $('#repno-field-apply-id').val();
        $.ajax({
            type: 'post',
            url: '<?= $saveRepnoUrl ?>',
            data: {
              entryDbIds: entryDbIds,
              column: column,
              blockId: blockId,
              value: value,
              excludeEntryDbIds: excludeEntryDbIds,
              urlParams: filters,
              entryListId: '<?= $entryListId?>',
              selectAll: selectAll
            },
            success: function(data) {

                var hasToast = $('body').hasClass('toast');

                if(!hasToast){
                    $('.toast').css('display','none');
                    var resetNotif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>The ordering of plots was reset.</span>";
                    Materialize.toast(resetNotif, 5000);
                }

                // go to active tab
                $('.pa-entry-order-tab').click();
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');

                location.reload();
                },
                error: function() {}
        });
        
    });

</script>

<style>

.input-field {
    margin-top: 0px !important;
}

#entry-grid-section {
    margin-bottom: 0px !important;
    background-color:white;
}
</style>