<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders entry order settings
*/
use app\models\Variable;
use yii\helpers\Url;
use kartik\tabs\TabsX;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use macgyer\yii2materializecss\widgets\Button;
use app\modules\experimentCreation\models\PlantingArrangement;

// if there are no assigned entries yet
if(empty($blockData)){
    echo '<div class="alert alert-warning" style="padding: 10px;">There are no added blocks yet. Please add first in "Add blocks" tab.</div>';
}else{
   
    // if no entries assigned, hide manage entries button
    $fullscreenBtn = "<a id='fullscreenMax-btn' class='waves-effect waves-light btn pull-right fullscreen-btn pull-right tooltipped' style='margin-left:15px;' data-position='top' data-delay='50' title='Click to toggle fullscreen'><i class='material-icons'>fullscreen</i></a>";
    $manageEntriesClass = empty($totalPlots) ? 'hidden': '';
    echo '<h5> Arrangement Information'.
        Html::a('Save','#',[
            'style'=>'margin-right:0px;',
            'type'=>'button',
            'title'=>\Yii::t('app',"Save information of this block"),
            'data-id'=>$subBlockId,
            'data-blockname'=>$subBlockName,
            'class'=>'disabled tabular-btn btn waves-effect waves-light pa-entry-order-save-btn pull-right',
            'id'=>'pa-entry-order-save-btn'
        ]).
        Html::a('Update No of reps','#',[
            'style'=>'margin-right:3px;',
            'type'=>'button',
            'title'=>\Yii::t('app',"Manage entries in the block"),
            'data-id'=>$subBlockId,
            'data-blockname'=>$subBlockName,
            'data-plotcount'=>$totalPlots,
            'id'=>'pa-entry-order-entries-btn',
            'class'=>'grey lighten-3 black-text pull-right tabular-btn btn waves-effect waves-light '.$manageEntriesClass.' pa-manage-blocks-manage-entries-btn'
        ]).
        '<a id="open-upload-pa-file-btn"
            href="#"
            class="grey lighten-3 black-text btn btn-primary waves-effect waves-light pull-right "
            style="margin-right:3px">
                <i class="material-icons">file_upload</i>
        </a>'.
        $fullscreenBtn.
        '
        <div class="preloader-wrapper small active right hidden" style="width:25px;height:25px;margin: 5px 10px;">
            <div class="spinner-layer spinner-green-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>&emsp;
        '.
        '</h5></br>';

    ?>
    <!-- Manage entries panel -->
    <ul class="collapsible" style="border-top: transparent;margin-top: 0px;">
        <li>
            <div class="collapsible-header collapsible-entry-grid  hidden"></div>
            <div id="manage-entries-browser" class="collapsible-body" style="padding: 0">
            </div>
        </li>
    </ul>

    <?php

    // build starting corner values
    $startingCornerValuesOpts = '';
    foreach ($startingCornerValues as $a => $b) {
        $startingCornerValuesOpts .= '<option value="'.$b.'">'.$b.'</option>';
    }

    // build plot numbering values
    $plotNumberingOrderValuesOpts = '';
    foreach ($plotNumberingOrderValues as $a => $b) {
        $plotNumberingOrderValuesOpts .= '<option value="'.$b.'">'.$b.'</option>';
    }

    //show fields
    echo '<div class="col col-md-6 entry-order-panel pa-blocks-width">';
  
    foreach ($blockData as $key => $value) {

        $class = ($key == 'no_of_entries_in_block' || $key == 'total_no_of_plots_in_block') ? true : false;

        $data = \Yii::$container->get('app\modules\experimentCreation\models\PlantingArrangement')->getFormDisplay($key);
        
        $label = isset($data['name']) && !empty($data['name']) ? $data['name'] : ucfirst(str_replace('_', ' ', $key));
        $description = !empty($data['description']) ? $data['description'] : ucfirst(str_replace('_', ' ', $key));

        if(in_array($key, ['plot_numbering_order','starting_corner'])){
            $options = ($key == 'plot_numbering_order') ? $plotNumberingOrderValuesOpts : $startingCornerValuesOpts;

            $options = str_replace('value="'.$value.'"', 'value="'.$value.'" selected', $options);

            echo '<div class="col col-md-6"><div class="col col-md-5"><label class="control-label" title="'.$description.'">'.$label.'</label></div>';
            
            $select = '<select id="'.$key.'" class="browser-default pa-entry-order-params pa-entry-order-params-'.$key.' pa-dimension-fields pa-fields-select" data-abbrev="'.$key.'" style="height:auto;" data-id="'.$subBlockId.'">'.$options.'</select>';

            echo '<div class="col col-md-7">'.$select.'</div></div>';
        } else if($key == 'remarks') {
            echo '<div class="col col-md-6"><div class="col col-md-5"><label class="control-label"    title="'.$description.'">'.$label.'</label></div>';
            echo '<div class="col col-md-7">'.Html::input('text', 'pa-entry-order-params_'.$key, $value, ['class'=>'pa-entry-order-params pa-entry-order-params-'.$key, 'id'=>$key, 'data-abbrev'=>$key,'data-id'=>$subBlockId,'data-plot'=>$totalPlots,'disabled'=>$class]).'</div></div>';
        }else{
            
            $dimension = (in_array($key, ['no_of_entries_in_block', 'total_no_of_plots_in_block', 'no_of_reps_in_block','no_of_occurrences'])) ? '' : 'pa-dimension-fields';
            
            echo '<div class="col col-md-6"><div class="col col-md-5"><label class="control-label"    title="'.$description.'">'.$label.'</label></div>';
            echo '<div class="col col-md-7">'.Html::input('number', 'pa-entry-order-params_'.$key, $value, ['class'=>'pa-entry-order-params pa-param-computer input-integer numeric-input-validate pa-entry-order-params-'.$key.' '.$dimension, 'id'=>$key, 'data-abbrev'=>$key,'data-id'=>$subBlockId,'data-plot'=>$totalPlots,'min'=>1,'disabled'=>$class]).'</div></div>';
        }
    }

    $checked = '';
    $noOfRows = 0;
    if($blockData['no_of_rows_in_block'] != NULL && $blockData['no_of_rows_in_block'] > 0){
        $checked = 'checked';
        $noOfRows = $blockData['no_of_rows_in_block'];
    }

    echo '<div class="col col-md-12"><p>
            <input type="checkbox" class="pull left filled-in" '.$checked.' id="define-dimension-chkbox"/>
            <label for="define-dimension-chkbox">Define dimension</label>
        </p></div>
    ';

    echo '<div class="col col-md-12" id="reset-note" style="margin-top:10px;">'.'&emsp;</div>';
    
    if($experimentType == 'Observation'){
        echo '<div class="col col-md-12">';
        echo '<b> Check Information</b>';
        echo '<div class="row" id="check-info-div" style="margin-right:30px;margin-left:30px;">';
        
        echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/entry-order/_check_groups.php',[
            'checkInformation' => $checkInformation,
            'id' => $id
        ]);

        echo '</div>';
        echo '</div>';
    }
    echo '<div class="col col-md-6" ><div class="col col-md-5" ></div>';

    echo '<div class="col col-md-6" style="margin-top:10px;">'.'&emsp;</div></div>';
    
    echo '</div>';

    echo '<div class="col col-md-6 pa-entry-order-preview pa-entry-order-preview-panel pa-full-layout-preview-panel" style="padding-right:0px">';

    echo '</div>';
  }

  // Upload Planting Arrangement Modal
  Modal::begin([
    'id' => 'upload-pa-file-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">file_upload</i>
            Upload Planting Arangement
        </h4>',
    'footer' =>
    Html::a('Cancel', [''], [
        'id' => 'cancel-upload-pa-file',
        'class' => 'modal-close-button',
        'title' => \Yii::t('app', 'Cancel'),
        'data-dismiss' => 'modal'
    ]) . '&emsp;&nbsp;'
        . Button::widget([
            'label' => 'Confirm',
            'icon' => [
                'name' => 'send',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'confirm-upload-pa-file hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Confirm upload planting arrangement')
            ],
        ])
        . Button::widget([
            'label' => 'Next',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'next-upload-pa-file hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Go to preview')
            ],
        ]). Button::widget([
            'label' => 'Validate',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'validate-upload-pa-file hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Validate and go to preview')
            ],
        ]),
    'closeButton' => [
        'class' => 'hidden'
    ],
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'class' => 'no-right-margin'
    ],
  ]);
  echo '<div class="upload-pa-file-modal-body parent-panel"></div>';
  echo '<div class="upload-pa-file-preview-modal-body child-panel"></div>';
  Modal::end();

  $updateBlockParamsUrl = Url::to(['/experimentCreation/planting-arrangement/update-block-params','id'=>$id]);
  $refreshPreviewLayoutOnChangeUrl = Url::to(['/experimentCreation/planting-arrangement/refresh-preview-layout-of-block','id'=>$id]);
  $refreshPreviewLayoutUrl = Url::to(['/experimentCreation/entry-order/preview-layout-of-block','id'=>$id]);
  $managePlotsUrl = Url::to(['/experimentCreation/entry-order/manage-entries-in-block', 'id'=>$id,'blockId'=> $subBlockId, 'experimentType'=>$experimentType]);
  $checkStatusUrl  = Url::to(['/experimentCreation/create/check-status','id'=>$id, 'action'=>'cross-design']);
  $renderFileUploadFormUrl = Url::to(['planting-arrangement/render-file-upload-form','id'=>$id, 'program'=>$program, 'experimentType'=>$experimentType, 'blockId'=> $subBlockId]);

  Yii::$app->view->registerJs(
      "
          checkStatusUrl = '" . $checkStatusUrl . "',
          experimentId = '" . $id . "',
          requiredFieldJson = 'null',
          checkStatValue = 'design generated'
          ;",
      \yii\web\View::POS_HEAD
  );

  $this->registerJsFile("@web/js/check-required-field-status.js", [
      'depends' => ['app\assets\AppAsset'],
      'position' => \yii\web\View::POS_END
  ]);
?>
<style>
    .tabs-left .tab-content {
        border-bottom: none !important;
        /* border-left: 1px solid #ddd; */
    }
    .box-body {
        padding: 0px 15px 10px 15px;
        max-height: 800px;
        overflow-y: hidden;
        overflow-x: hidden;
        background-color: #f1f4f5;
        /* border: none; */
    }
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
        opacity: 0;
    }

</style>

<script type="text/javascript">

    var manageEntriesIsOpen = false;
    var sbId = '<?= $subBlockId ?>';
    var panelWidth = $('.pa-blocks-width').width() + 10;
    var noOfRows = '<?= $noOfRows ?>';
    var experimentStatus = '<?= $experiment['experimentStatus'] ?>';
    var managePlotsIsOpen = false;
    var experimentType = '<?= $experimentType ?>';
    var noOfRepsFlag = false;
    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';

    $(document).ready(function(){
        $('#change-design-switch-id').removeAttr('disabled')
        $('.collapsible').collapsible();

        setTimeout(function(){
            previewLayout(sbId);
        },100);

        if(noOfRows == 0){
            $(".pa-dimension-fields").attr('disabled', 'disabled');
        }
        
        if(experimentStatus.includes('design generated')){
            $("#next-btn").removeAttr('disabled');
        }

    });
    checkStatus();
    // if number of row is changed
    $(".pa-entry-order-params-no_of_rows_in_block").on('change', function (e, params) {

        e.stopImmediatePropagation();
        var obj = $(this);

        // number of rows
        var rows = (obj.val()) != '' ? parseInt(obj.val()) : 0;
        var totalPlots = parseInt($('.pa-entry-order-params-total_no_of_plots_in_block').val());

        $(".red-text").each(function(){
            $(this).remove();
        });
        $('.pa-entry-order-params-no_of_rows_in_block').removeClass('invalid');
        $('.pa-entry-order-params-no_of_cols_in_block').removeClass('invalid');
        $('.entry-order-panel').removeClass('invalid');
        if(rows > totalPlots){
            $('.pa-entry-order-params-no_of_rows_in_block').focus();
            $('.pa-entry-order-params-no_of_rows_in_block').addClass('invalid');
            $('.pa-entry-order-params-no_of_rows_in_block').after("<span class='red-text error-notif'>Value must be <= "+totalPlots+"</span>");
        }else if(rows < 1){
            $('.pa-entry-order-params-no_of_rows_in_block').focus();
            $('.pa-entry-order-params-no_of_rows_in_block').addClass('invalid');
            $('.pa-entry-order-params-no_of_rows_in_block').after("<span class='red-text error-notif'>Value must be > 0</span>");
        }else{
            $(".red-text").each(function(){
                $(this).remove();
            });
            $('.pa-entry-order-params-no_of_rows_in_block').removeClass('invalid');
            $('.pa-entry-order-params-no_of_cols_in_block').removeClass('invalid');
        }

        var column = Math.ceil(totalPlots/rows);

        $('.pa-entry-order-params-no_of_cols_in_block').val(column);
        checker(sbId);
    });

    // if number of columns is changed
    $(".pa-entry-order-params-no_of_cols_in_block").change(function(e, params) {
        e.stopImmediatePropagation();
        var obj = $(this);

        // number of cols
        var cols = (obj.val()) != '' ? parseInt(obj.val()) : 0;

        var totalPlots = parseInt($('.pa-entry-order-params-total_no_of_plots_in_block').val());

        $(".red-text").each(function(){
            $(this).remove();
        });
        $('.pa-entry-order-params-no_of_cols_in_block').removeClass('invalid');
        $('.pa-entry-order-params-no_of_rows_in_block').removeClass('invalid');
        if(cols > totalPlots){
            $('.pa-entry-order-params-no_of_cols_in_block').focus();
            $('.pa-entry-order-params-no_of_cols_in_block').addClass('invalid');
            $('.pa-entry-order-params-no_of_cols_in_block').after("<span class='red-text error-notif'>Value must be <= "+totalPlots+"</span>");
        }else if(cols < 1){
            $('.pa-entry-order-params-no_of_cols_in_block').focus();
            $('.pa-entry-order-params-no_of_cols_in_block').addClass('invalid');
            $('.pa-entry-order-params-no_of_cols_in_block').after("<span class='red-text error-notif'>Value must be > 0</span>");
        }else{
            $(".red-text").each(function(){
                $(this).remove();
            });
            $('.pa-entry-order-params-no_of_rows_in_block').removeClass('invalid');
            $('.pa-entry-order-params-no_of_cols_in_block').removeClass('invalid');
        }

        var row = Math.ceil(totalPlots/cols);
        $('.pa-entry-order-params-no_of_rows_in_block').val(row);
        checker(sbId);
    });

    // validate upon change
    $(".pa-entry-order-params").change(function(e, params) {
        var obj = $(this);
        var flagError = false;
        var varValue = (obj.val()) != '' ? parseInt(obj.val()) : 0;
        if(this.id = 'no_of_reps_in_block'){
            if(varValue == 0){
                flagError = true;
            } else{ 
                noOfRepsFlag = true;
            }
        }
        if(this.id = 'no_of_occurrences'){
            if(varValue == 0){
                flagError = true;
            }
        }
        if(flagError){
            $(this).focus();
            $(this).addClass('invalid');
            $(this).after("<span class='red-text error-notif'>Value must be > 0</span>");
        } else {
            $(".red-text").each(function(){
                $(this).remove();
            });
            $(this).removeClass('invalid');
            $(this).removeClass('invalid');
        }
        checker(sbId);

    });


    // checks if all fields are specified
    function checker(sbId){
        if(manageEntriesIsOpen) { // Close Manage Entries Browser
            $('#pa-entry-order-entries-btn').trigger('click');
        }
        var error = [];
        $(".pa-blocks-width .invalid").each(function(){
            error.push(1);
        });

        var rows = $('.pa-entry-order-params-no_of_rows_in_block').val();
        var cols = $('.pa-entry-order-params-no_of_cols_in_block').val();
        // checks if all are valid
        if( (rows && cols && !error.length) ){
            $('#pa-entry-order-save-btn').removeClass('disabled');
            $('#pa-entry-order-save-btn').addClass('pulse');

            $('#pa-entry-order-entries-btn').addClass('disabled');
            // show preview button
            $('.pa-manage-blocks-preview-panel').html(
                '<a type="button" class="grey lighten-3 black-text tabular-btn btn waves-effect waves-light pa-manage-blocks-preview-layout-btn-'+sbId+'" href="#" title="Preview layout" style="margin-right:5px;" data-id="'+sbId+'"><i class="material-icons right tabular-icon">search</i> Preview layout</a>'
            );

        }else{
            $('.pa-manage-blocks-preview-layout-btn').each(function( index ) {
                $(this).addClass('disabled');
                $(this).removeClass('pulse');
            });

            $('#pa-entry-order-save-btn').addClass('disabled');
            $('#pa-entry-order-save-btn').removeClass('pulse');

            $('#pa-entry-order-entries-btn').removeClass('disabled');

        }
    }

     // save block info
    $(document).on('click','.pa-entry-order-save-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var obj = $(this);
        var subBlockName = obj.data('blockname');

        $('.preloader-wrapper').removeClass('hidden');
        $('#pa-entry-order-save-btn').addClass('disabled');
        $('#pa-entry-order-save-btn').removeClass('pulse');    

        var values = {};
        $(".pa-entry-order-params").each(function( index ) {
            var obj = $(this);
            var attr = obj.data('abbrev');
            if(this.id != 'no_of_reps_in_block'){
                var value = $('.pa-entry-order-params-'+attr).val();
                values[attr] = value;
            } else {
                if(noOfRepsFlag){
                    var value = $('.pa-entry-order-params-'+attr).val();
                    values[attr] = value;
                }
            }
        });

        if(experimentType == 'Observation'){
            //get Check groups
            var check_groups = [];
            $(".check-groups-select2").each(function( index ) {
                var selectId = this.id;
                var selectValues = $(this).val();
                
                if(selectValues.length > 0){
                    check_groups.push({'members':selectValues,'group':selectId});
                }
            });

            //check group interval
            var check_group_interval = $('#check-interval-field-id').val();
            if(check_groups.length > 0 && parseInt(check_group_interval) == 0){
                if(!$('#check-interval-field-id').hasClass("disabled")){
                    $('#check-interval-field-id').focus();
                    $('#check-interval-field-id').addClass('invalid');
                    $('#check-interval-field-id').after("<br><span class='red-text'>Value must be > 0</span>");
                }
            }

            if(check_groups.length == 0){
                check_group_interval = 0;
                $('#check-interval-field-id').val(0);
            }
            
            var begins_with_check_group = $('#begins_with_check_group').val();
            var ends_with_check_group = $('#ends_with_check_group').val();
            
            var dataParams = {
                blockId: sbId,
                params: values,
                begins_with_check_group: begins_with_check_group,
                ends_with_check_group:ends_with_check_group,
                check_group_interval: check_group_interval,
                check_groups:check_groups
            };
        } else {
            var dataParams = {
                blockId: sbId,
                params: values
            };
        }
        
        $.ajax({
            type: 'post',
            url: '<?= $updateBlockParamsUrl ?>',
            data: dataParams,
            success: function(data) {
                //reset no of rows
                data = JSON.parse(data);
                var resetMsg = '';
                

                $('.pa-entry-order-params-total_no_of_plots_in_block').val(parseInt(data['totalPlots']));
                noOfRows = parseInt($('.pa-entry-order-params-no_of_rows_in_block').val());
                var hasToast = $('body').hasClass('toast');            

                if(!hasToast){
                    $('.toast').css('display','none');
                    
                    var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully saved arrangement details.</span>";
                    Materialize.toast(notif, 5000);

                    if(parseInt(data['noOfRows']) === 0){
                        $('.pa-entry-order-params-no_of_rows_in_block').val(0);
                        $('.pa-entry-order-params-no_of_cols_in_block').val(0);

                        resetMsg = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>The dimension specified is reset. Please specify dimension according to the total no. of plots.</span>";
                        Materialize.toast(resetMsg, 5000);
                    }
                }

                $('.preloader-wrapper').addClass('hidden');
                $('#reset-note').html('');

                if(manageEntriesIsOpen) { // Close Manage Entries Browser
                    $('#pa-entry-order-entries-btn').trigger('click');
                }

                $('#pa-entry-order-entries-btn').removeClass('disabled');
                
                if(noOfRows > 0){
                    // show preview button
                    $('.pa-entry-order-preview-panel').html(
                        '<a type="button" class="grey lighten-3 black-text tabular-btn btn waves-effect waves-light pa-entry-order-preview-layout-btn" href="#" title="Preview layout" style="margin-right:5px;" data-id="'+sbId+'"><i class="material-icons right tabular-icon">search</i> Preview layout</a>'
                    );
                } else {
                  $('.pa-entry-order-preview-panel').html(
                      '<div class="alert alert-warning" style="padding: 10px;">Layout cannot be shown for this arrangement. Please specify the dimension to generate preview.</div>'
                      );
                }
                
            },
            error: function() {}
        });
    });


    // upon resize window
    $(window).resize(function(e){
        e.stopImmediatePropagation();
        e.stopImmediatePropagation();

        setTimeout(function(){
          var values = {};
          $(".pa-entry-order-params").each(function( index ) {
              var obj = $(this);
              var attr = obj.data('abbrev');
              var value = $('.pa-entry-order-params-'+attr).val();
              values[attr] = value;
          });

          var newWidth = $('.pa-blocks-width').width() + 10;

          $.ajax({
              type: 'POST',
              url: '<?= $refreshPreviewLayoutOnChangeUrl?>',
              data: {
                  values: values,
                  blockId: sbId,
                  panelWidth: newWidth,
                  isEntryOrder: true
              },
              async: false,
              success: function(data) {
                  $('.pa-entry-order-preview-panel').html(data);
              }
          });

        },100);
    });

    // preview layout
    $(document).on('click','.pa-entry-order-preview-layout-btn',function(e){
        e.stopImmediatePropagation();
        e.stopImmediatePropagation();

        var obj = $(this);

        $('.preloader-wrapper').removeClass('hidden');

        var values = {};
        $(".pa-entry-order-params").each(function( index ) {
            var obj = $(this);
            var attr = obj.data('abbrev');
            var value = $('.pa-entry-order-params-'+attr).val();
            values[attr] = value;
        });

        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: '<?= $refreshPreviewLayoutOnChangeUrl?>',
                data: {
                    values: values,
                    blockId: sbId,
                    panelWidth: panelWidth
                },
                async: false,
                success: function(data) {
                    $('.preloader-wrapper').addClass('hidden');
                    $('.pa-entry-order-preview-panel').html(data);
                }
            });
        },100);

    });

    // preview layout upon load
    function previewLayout(blockId){
        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: '<?= $refreshPreviewLayoutUrl?>',
                data: {
                    blockId: blockId,
                    panelWidth: panelWidth
                },
                async: false,
                success: function(data) {
                    $('.preloader-wrapper').addClass('hidden');
                    $('.pa-entry-order-preview-panel').html(data);
                }
            });
        },100);
    }

    //for resetting dimension
    $("#define-dimension-chkbox").on('change', function (e, params) {
        if(manageEntriesIsOpen) { // Close Manage Entries Browser
            $('#pa-entry-order-entries-btn').trigger('click');
        }

        if($('#define-dimension-chkbox').prop('checked') == true){
            $(".pa-dimension-fields").removeAttr('disabled');
        } else{
            var currentRows = parseInt($('.pa-entry-order-params-no_of_rows_in_block').val());
            if(noOfRows > 0 && currentRows != 0){
                $('.pa-entry-order-params-no_of_rows_in_block').trigger('change');
            }
            $('.pa-entry-order-params-no_of_rows_in_block').val(0);
            $('.pa-entry-order-params-no_of_cols_in_block').val(0);

            $(".pa-fields-select").each(function(){
            
              var element = document.getElementById(this.id);

              if(element != null && element.tagName === 'SELECT'){
                  var flag = false;
                  if(this.id == 'plot_numbering_order'){
                      var value = 'Column serpentine';
                  } else { 
                      var value = 'Top Left';
                  }
                  $('#'+(this.id)+' option:first').attr('selected',true);
                  if(this.value != value){
                      $('#'+this.id).trigger('change');
                  }
                  
              }

            });
            $(".pa-dimension-fields").attr('disabled', 'disabled');
            $('.pa-entry-order-preview-panel').html('');
            $(".red-text").each(function(){
                $(this).remove();
            });
            $('.pa-entry-order-params-no_of_rows_in_block').removeClass('invalid');
            $('.pa-entry-order-params-no_of_cols_in_block').removeClass('invalid');
        }
    });

    // upon click manage plots button
    $(document).on('click','#pa-entry-order-entries-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var obj = $(this);

        var sbId = obj.data('id');
        if(!manageEntriesIsOpen) {
            manageEntriesIsOpen = true;
            $('.preloader-wrapper').removeClass('hidden');

            $.ajax({
                type: 'POST',
                url: '<?= $managePlotsUrl?>',
                data: {
                  
                },
                async: true,
                success: function(data) {
                    $('#manage-entries-browser').html(data);
                    $('.preloader-wrapper').addClass('hidden');
                    $('.collapsible-entry-grid').click();
                },
                error: function(){
                    var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading entries. Please try again.';
                    Materialize.toast(notif, 5000);
                }
            });
        } else {
            manageEntriesIsOpen = false;
            $('.collapsible-entry-grid').click();
        }
    });

    // validate numeric
    $(document).on("keypress keyup blur", ".numeric-input-validate", function() {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    }); 

    // Access Upload Planting Arrangement
    $('#open-upload-pa-file-btn').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        $('#upload-pa-file-modal').modal('show')
        var modalBody = '.upload-pa-file-modal-body';
        $(modalBody).html(loading);

        $.ajax({
            url: '<?= $renderFileUploadFormUrl?>',
            type: 'post',
            dataType: 'json',
            data:{},
            success: function(response) {
                $(modalBody).html(response);
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                $(modalBody).html(errorMessage);
            }
        });
    });


</script>
