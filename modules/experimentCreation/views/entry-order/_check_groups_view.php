<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders check groups settings
*/


use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use app\dataproviders\ArrayDataProvider;

$startChecks = '';
$startChecksArr = [];
$endChecks = '';
$endChecksArr = [];

foreach($checkInformation['check_array'] as $key=>$value){
    if(in_array($key,$checkInformation['begins_with_check_group'])){
        $startChecksArr[] = $value;
    }
}
$startChecks = implode(", ", $startChecksArr);

foreach($checkInformation['check_array'] as $key=>$value){
  if(in_array($key,$checkInformation['ends_with_check_group'])){
      $endChecksArr[] = $value;
  }
}
$endChecks = implode(", ", $endChecksArr);
// all the columns that can be viewed in seasons browser
$columns = [
		[
				'header' =>Yii::t('app', 'Start checks'),
				'format' => 'raw',
				'headerOptions'=>['style' => 'width:50%'],
				'encodeLabel' => false,
				'vAlign'=>'middle',
				'value' => function($data) use ($checkInformation, $startChecks){
						return ($startChecks == '') ? '<span class="not-set">(not set)</span>' : $startChecks;
				},
			],
			[
				'header' =>Yii::t('app', 'End checks'),
				'format' => 'raw',
				'headerOptions'=>['style' => 'width:50%'],
				'encodeLabel' => false,
				'vAlign'=>'middle',
				'value' => function($data) use ($checkInformation, $endChecks){
            return ($endChecks == '') ? '<span class="not-set">(not set)</span>' : $endChecks;
				},
		]
];  
$dataProvider1 = new ArrayDataProvider([
	'allModels' => [$checkInformation],
	'key' => 'check_group_interval',
	'pagination' => false,
	'sort' => false
]);

echo GridView::widget([
	'pjax' => true, // pjax is set to always true for this demo
	'dataProvider' => $dataProvider1,
	'id' => 'check-information-grid',
	'tableOptions' => ['id' => 'check-information-table', 'class' => 'table table-bordered white z-depth-3'],
	'columns' => $columns,
	'showPageSummary'=>false,
	'striped'=>false
]);

echo '<b style="margin-top:5px; margin-bottom:0px;">Specify Check Groups &nbsp;</b>';

echo '<p style="margin-bottom:15px;"></p><div class="col col-md-6"><label class="control-label pull-left check-interval_class" style="margin: 10px;" title="Change value of Check Group interval">Group Interval</label>'.Html::input('number', 'check-interval-apply', $checkInformation['check_group_interval'], ['id' => 'check-interval-field-id', 'class' => 'check-interval_class check-interval-validate numeric-input-validate pull-left', 'style'=>'width:75px;', 'min'=> 1, 'disabled'=>'disabled']).'</div>';

echo '<table id="check-groups-table2" class="table table-bordered white kv-grid-table kv-table-wrap">';
echo '<th class="kv-align-middle" style="width:95%">CHECK GROUPS</th>';
echo '<div id="check-groups-body-div">';
		if(isset($checkInformation['check_groups']) && !empty($checkInformation['check_groups'])){

				foreach($checkInformation['check_groups'] as $group){

						$options = '';
            $checkGroupArr = [];

						foreach($checkInformation['check_array'] as $key=>$value){
								$selected = '';
								if(in_array($key,$group['members'])){
                    $checkGroupArr[] = $value;
								}
						}
						// $groupStr = '<select id="'.$group['group'].'" class="check-groups-select2 form-control" name="checkGroups[]" multiple>'.$options.'</select>';
						$groupStr = implode(", ", $checkGroupArr);
						$trStr = '<tr data-id="'.$group['group'].'" class="tr-checkgrp-class">'.
								'<td>'.$groupStr.'</td></tr>';

						echo $trStr;
				}
		} else {
				echo '<tr><td id="no-results-id" colspan="2">No results found.</td></tr>';
		}
echo '</div>';
echo '</table>';

$optionsStr = '';
foreach($checkInformation['check_array'] as $key=>$value){
	$optionsStr .= '<option value="'.$key.'">'.$value.'</option>';
}
$addCheckGroup = Url::to(['/experimentCreation/entry-order/add-check-group','id'=>$id]);

$this->registerJsFile("@web/js/generic-form-validation.js", [
	'depends' => ['app\assets\AppAsset'],
	'position'=>\yii\web\View::POS_END
]);
?>

<style>
#check-information-grid > .summary, #check-groups-grid > .summary{
		padding-top: 10px;
}
#check-groups-grid > .table-responsive {
		min-height: 0.01%;
		overflow-x: inherit !important;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background-color: #337ab7;
		color: #fff;
}
.select2-container--default .select2-results__option[aria-selected=true] {
		color: #31708f;
		background-color: #d9edf7;
}
.select2-container--default.select2-container--open.select2-container--below .select2-selection--single, .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
		border-bottom-left-radius: 0;
		border-bottom-right-radius: 0;
		border-bottom-color: transparent;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice {
		color: #555555;
		background: #f5f5f5;
		border: 1px solid #ccc;
		border-radius: 4px;
		cursor: default;
		float: left;
		margin: 5px 0 0 6px;
		padding: 0 6px;
}
.select2-container--default.select2-container--focus .select2-selection--multiple {
		border: solid black 1px;
		outline: 0;
		border-color: #66afe9;
		border-radius: 4px;
		box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%), 0 0 6px rgb(102 175 233 / 60%);
}
#check-groups-grid > .select2-container .select2-selection--multiple {
		box-sizing: border-box;
		cursor: pointer;
		display: flex;
		height: 32px;
		user-select: none;
		-webkit-user-select: none;
}
</style>