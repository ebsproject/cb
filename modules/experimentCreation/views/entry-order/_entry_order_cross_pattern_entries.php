<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting arrangement manage block form
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\dynagrid\DynaGrid;

$browserId = 'ec-pa-entry-order-cross-pattern-plots-grid';

$columns = [
    [
        'label' => '<span title="Sequential numbering of entries">'.Yii::t('app', 'Entry No.').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
        'content'=>function($data){
            return $data['entno'].'<span class="row-color '.($data['batch_no']%2 ? 'darken' : 'lighten').'"></span>';
        }
    ],
    [
        'attribute'=>'repno',
        'label' => Yii::t('app', 'Replicate'),
        'format' => 'raw',
        'encodeLabel'=>false
    ],
    [
        'attribute'=>'designation',
        'label' => '<span title="Germplasm">'.Yii::t('app', 'Germplasm Name').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
    ],
    [
      'attribute'=>'pattern',
      'label' => Yii::t('app', 'Pattern'),
      'format' => 'raw',
      'encodeLabel'=>false,
    ],
];

$dynagrid = DynaGrid::begin([
    'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>false,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'dataProvider' => $dataProvider,
        'filterModel'=>null,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>$browserId.'2',
                'enablePushState'=>false
            ],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel' => [
            'heading'=>false,
            'before' => \Yii::t('app', 'This is the browser for the auto generated plot records. You can delete this planting arrangement and generate a new set of plot records.').' {summary} ',
            'after' => false,
        ],
        'floatHeader' => false,
        'floatOverflowContainer'=> false,
        'toolbar' => [
            Html::a('Delete Planting Arrangement','#',[
                'type'=>'button',
                'title'=>\Yii::t('app',"Delete the planting arrangement and all the created crosses"),
                'class'=>'btn pa-delete-cross-pattern-plots-btn tabular-btn btn waves-effect waves-light pull-right',
                'id'=>'pa-entry-order-save-update-btn'
            ])

        ],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
      ],
      'options'=>['id'=>$browserId],
  ]);
  DynaGrid::end();
?>
<?php
// remove a plot modal body
Modal::begin([
  'id' => 'pa-delete-cross-pattern-plots-modal',
  'header' => '<h4><i class="material-icons" style="vertical-align: bottom;">delete</i> '.\Yii::t('app','Delete Planting Arrangement?'). '</h4>',
  'footer' =>
      Html::a('Cancel', ['#'], ['id' => 'pa-delete-cross-pattern-plots-modal-cancel-btn', 'style'=>'margin-right:15px', 'data-dismiss'=> 'modal']).'&nbsp;'.
      Html::a(\Yii::t('app','Confirm'),'#',[
            'id'=>'pa-delete-cross-pattern-plots-confirm-btn',
            'class'=>'btn waves-effect waves-light',
            'data-id'=>$subBlockId,
            'style'=>'margin-right:15px'
      ]),
  'size' =>'modal-md',
  'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);
echo '<div id="pa-delete-cross-pattern-plots-modal-body"></div>';

Modal::end();

$deleteCrossPatternPlotsInBlockUrl = Url::to(['/experimentCreation/entry-order/delete-cross-pattern-plots-in-block', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);

?>

<script type="text/javascript">
    var blockId = '<?= $subBlockId ?>';

    $(document).ready(function() {
        colorRows();
    });

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();

        colorRows();
    });

    function colorRows() {
        $('.row-color').each(function() {
            if($(this).hasClass('darken')) $(this).parent().parent().addClass('green lighten-3');
        });
    }

    // confirm updating of plots
    $(document).on('click','.pa-delete-cross-pattern-plots-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        $('#pa-delete-cross-pattern-plots-modal').modal('show');
        $('#pa-delete-cross-pattern-plots-modal-body').html('<div>This will delete the planting arrangement and all the created crosses. Click Confirm to proceed.</div>');

        $('#pa-delete-cross-pattern-plots-confirm-btn').prop('disabled', false);
        $('#pa-delete-cross-pattern-plots-confirm-btn').removeAttr('disabled');
    });

    // confirm updating of plots
    $(document).on('click','#pa-delete-cross-pattern-plots-confirm-btn',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        $('#pa-delete-cross-pattern-plots-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#pa-delete-cross-pattern-plots-confirm-btn').prop('disabled', true);
        $('#pa-delete-cross-pattern-plots-confirm-btn').attr('disabled', 'disabled');

        $.ajax({
            type: 'post',
            url: '<?= $deleteCrossPatternPlotsInBlockUrl ?>',
            data: {
            },
            success: function(data) {
                var resetNotif = "<i class='material-icons green-text'>check</i>&nbsp;The planting arrangement and all the created crosses have been successfully deleted.</span>";
                Materialize.toast(resetNotif, 5000);
            },
            error: function() {
            }
        });
    });

</script>

<style>

.input-field {
    margin-top: 0px !important;
}

#entry-grid-section {
    margin-bottom: 0px !important;
    background-color:white;
}
</style>