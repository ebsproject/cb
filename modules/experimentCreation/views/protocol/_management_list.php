<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders management variable list display
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\VariableInfo;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;

\yii\jui\JuiAsset::register($this);


// variables columns

$columns = [
    [
        'label'=> "checkbox",
        'header'=>'
            <input type="checkbox" class="filled-in" id="management-list-variables-grid-select-all-id" />
            <label for="management-list-variables-grid-select-all-id"></label>
        ',
        'contentOptions' => ['style' => ['max-width' => '100px;',]],
        'content'=>function($data) {
            return '
                <input class="mgt-final-grid_select filled-in" type="checkbox" id="'.$data['listMemberDbId'].'" data-id ="'.$data['variableDbId'].'" />
                <label for="'.$data['listMemberDbId'].'"></label>
            ';
        },
        'hiddenFromExport'=>true,
        'mergeHeader'=>true,
        'width'=>'50px'
    ],
    [
        'attribute'=>'label',
        'format'=>'raw',
        'vAlign'=>'middle',
        'value'=> function($data){
            return Html::a($data['label'],
                ['#'], 
                [
                    'id' => $data['variableDbId'],
                    'label' => $data['label'],
                    'data-toggle' => 'modal',
                    'data-target'=> '#mgt-view-variable-modal',
                    'class' => 'mgt-view-variable'
                ]
            );
        },
        'contentOptions' => function($data) {
            return ['class' => 'order_tr', "data-varId"=>$data['variableDbId']];
        }
    ],
    [
        'attribute'=>'displayName',
        'label'=>'Name',
        'format'=>'raw',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'remarks',
        'label'=> 'Instructions',
        'format' => 'raw',
        'vAlign'=>'middle',
        'value' => function($data){
            return Html::input('text', 'instructions-input', $data['remarks'], ['id'=>'instructions-'.$data['listMemberDbId'],'class'=>'mgt-instructions-input-validate','data-name'=>'remarks']);
        },
        'width'=>'300px'
    ],
];

$actionUpdateRemarks = Html::a('<i class="glyphicon glyphicon-edit"></i>', '#!', [ 'class' => 'btn management-list-actions', 'id' => 'management-update-remarks-btn', 'title'=>\Yii::t('app',' Bulk Update')]);
$actionSaveList = Html::a('<i class="material-icons widget-icons">add_shopping_cart</i>', '#!', [ 'class' => 'btn light-green darken-3 waves-light waves-effect management-list-actions ', 'id' => 'management-save-new-list-btn', 'title'=>\Yii::t('app',' Save as a new management variable list')]); 
$actionDeleteList = Html::a('<i class="material-icons widget-icons">delete</i>', '#!', [ 'class' => 'btn management-list-actions red darken-3', 'id' => 'management-discard-btn', 'title'=>\Yii::t('app',' Remove item(s) from the list')]);
$confirmAddVarListUrl = Url::to(['/experimentCreation/protocol/add-mgt-var-list']);

$targetLevelValue = null;
$switchButton = '';

$panel = [
    'heading' => '<i class="material-icons left">folder_special</i> '.\Yii::t('app', 'Selected management attributes'),
    // 'before' => \Yii::t('app', 'You may drag the row across the table to reorder the management variables.'), // TODO: To be refactored
    'footer' => false
];
// variables browser
DynaGrid::begin([
    'options'=>['id'=>'management-list-selected-vars-grid'],
    'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions'=>[
        'id' => 'management-list-selected-vars-grid',
        'dataProvider'=>$dataProvider,
        'pjax'=>true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
                'id' => 'management-list-selected-vars-grid'
            ],
        ],
        'panel'=> $panel,
        'responsiveWrap'=>false,
        'toolbar' =>  [
            'content' => "$switchButton $actionUpdateRemarks $actionSaveList $actionDeleteList",
        ],
        'floatHeader' =>true,
        'floatOverflowContainer'=> true
    ]
]);
DynaGrid::end();

//Modal for bulk updating instructions
Modal::begin([
    'id'=> 'mgt-update-remarks-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i> '.\Yii::t('app','Bulk Update').'</h4>',
    'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="bulk-update-selected" data-operation="update-selected" class="btn btn-primary green waves-effect waves-light mgt-confirm-update-btn" disabled>'.\Yii::t('app','Selected').'</a>&nbsp;'.
        '<a id="bulk-update-all" data-operation="update-all" class="btn btn-primary teal waves-effect waves-light mgt-confirm-update-btn" disabled>'.\Yii::t('app','All').'</a>',
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static'],
]);

echo "<label for = 'mgt-remarks-bulk-input'>".\Yii::t('app','Management Variable Instructions')."</label>";
echo Html::input('text', 'mgt-remarks-bulk-input','', ['id'=>'mgt-remarks-bulk-input', 'placeholder'=>'Add instructions here']);

Modal::end();

// Modal for deleting management variables
Modal::begin([
    'id' => 'mgt-delete-modal',
    'header' => '<h4><i class="material-icons orange-text left">warning</i>'. \Yii::t('app','Remove management variables').'</h4>',				
    'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',['id' => 'mgt-back-delete-btn','data-dismiss'=>'modal']).'&nbsp;&emsp;'.Html::a(\Yii::t('app','Confirm'),'#',['id' => 'mgt-proceed-delete-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static']
    ]);
    echo \Yii::t('app','Selected management variable/s will be deleted from your experiment. This action cannot be undone.');
Modal::end();

// Modal for management variable list preview
Modal::begin([
    'id' => 'mgt-list-preview-modal',
    'header' => '<h4 id="mgt-list-preview-header"></h4>',				
    'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',['id' => 'mgt-back-preview-btn','data-dismiss'=>'modal']).'&nbsp;&emsp;'.Html::a(\Yii::t('app','Confirm'),'#',['id' => 'mgt-ok-preview-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop'=>'static']
]);
echo '<p id="mgt-list-preview-modal-note">'.\Yii::t('app','Below is the list of management variables that will be added in this experiment. Click "Confirm" to proceed.').'</p>';
echo '<p id="mgt-list-preview-modal-body">';
Modal::end();

// Modal for management variable list group preview
Modal::begin([
    'id' => 'mgt-group-list-preview-modal',
    'header' => '<h4 id="mgt-group-list-preview-header"></h4>',				
    'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',['id' => 'mgt-back-preview-btn','data-dismiss'=>'modal']).'&nbsp;&emsp;'.Html::a(\Yii::t('app','Confirm'),'#',['id' => 'mgt-ok-preview-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop'=>'static']
]);
echo '<p id="mgt-group-list-preview-modal-note">'.\Yii::t('app','Below is the list of management variables that will be added in this experiment. Click "Confirm" to proceed.').'</p>';
echo '<p id="mgt-group-list-preview-modal-body">';
Modal::end();

// modal for saving as new list
Modal::begin([
    'id' => 'management-save-list-modal',
    'header' => '<h4><i class="material-icons" style="vertical-align:bottom">add</i> '.\Yii::t('app', 'Save as a new Management Variable List').'</h4>',
    'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
        .Html::a('Submit' . '<i class="material-icons right">send</i>',
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light disabled',
                'url' => '#',
                'id' => 'management-save-list-confirm-btn'
            ]
        ) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
]);


echo '<p id="management-save-list-preview-error"></p>';

$form = ActiveForm::begin([
    'enableClientValidation'=>false,
    'id'=>'create-saved-list-form',
    'action'=>['/experimentCreation/protocol/managements/_managements'],
    'fieldConfig' => function($model,$attribute){
        if(in_array($attribute, ['abbrev','display_name','name','remarks'])){
            return ['options' => ['class' => 'input-field form-group col-md-6']];
        }else{
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    }
]); 

$model = \Yii::$container->get('\app\models\PlatformList');
echo 
$form->field($model, 'name')->textInput([
    'maxlength' => true, 
    'id'=>'management-list-name',
    'title'=>'Name identifier of the list',
    'onkeyup' => 'js:
        var nameValue = this.value;
        $("#management-list-display_name").val(nameValue).trigger("change");
        $("#management-list-abbrev").val(nameValue.replace(/[^A-Z0-9]/ig, "_").toUpperCase()).trigger("change");
    '
]).  
$form->field($model, 'abbrev')->textInput([
    'maxlength' => true, 
    'id'=>'management-list-abbrev',
    'title'=>'Short name identifier or abbreviation of the list',
    'oninput' => 'js: $("#management-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());'
]).  
$form->field($model, 'description')->textarea([ 'class'=>'materialize-textarea', 'title'=>'Description about the list', 'id'=>'management-list-description']).
$form->field($model, 'display_name')->textInput(['id'=>'management-list-display-name', 'maxlength' => true, 'title'=>'Name of the list to show to users', 'id'=>'management-list-display_name',]).
$form->field($model, 'remarks')->textarea(['class'=>'materialize-textarea', 'title'=>'Additional details about the list','row'=>2, 'id'=>'management-list-remarks']); 

ActiveForm::end();

echo '<div class="management-modal-loading"></div>';
echo '<div class="management-save-list-preview-variables"></div>';

Modal::end();

// view variable modal
Modal::begin([
    'id' => 'mgt-view-variable-modal',
    'header' => '<h4 id="entity-label"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);
echo '<div class="mgt-view-variable-modal-body"></div>';
Modal::end();

$variableUrl = Url::to(['/variable/default/view-info']);
$this->registerJs(<<<JS
    var targetLevelValue = '$targetLevelValue';
   
    //Load the saved target level value if there is
    $("document").ready(function(){
        if(typeof targetLevelValue != 'undefined' || targetLevelValue != null){
            //Set the saved value here
            if(targetLevelValue == 'occurrence'){
                $('#mgt-type-switch-id').prop("checked",false);
            }else if(targetLevelValue == 'location'){
                //location
                $('#mgt-type-switch-id').prop("checked",true);
            }else{
                //Default is occurrence
                $('#mgt-type-switch-id').prop("checked",false);
            }
        }
    });
// view variable info
$(document).on('click', '.mgt-view-variable', function(e) {
    var obj = $(this);
    
    var varId = obj.attr('id');
    var varLabel = obj.attr('label');

    $('#entity-label').html('<i class="fa fa-info-circle"></i> ' + varLabel + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $('.mgt-view-variable-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    //Load con tent of view variable information
    setTimeout(function(){
        $.ajax({
            url: '$variableUrl',
            data: {
                id: varId
            },
            type: 'POST',
            async: true,
            success: function(data){
                $('.mgt-view-variable-modal').modal('show');
                $('.mgt-view-variable-modal-body').html(data);
            },
            error: function(){
                $('.mgt-view-variable-modal-body').html('<i>There was a problem while loading record information.</i>');
            }
        });
    },300);  
});
JS
);
?>
