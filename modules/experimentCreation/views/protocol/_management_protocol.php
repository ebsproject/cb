<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders display of adding variables from management variable lists
*/

use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;

\yii\jui\JuiAsset::register($this);

$hasMultipleSelectedOccurrences = $hasMultipleSelectedOccurrences ?? '';
$occurrenceDbIdsString = $occurrenceDbIdsString ?? '';
$shouldUpdateOccurrenceProtocols = $shouldUpdateOccurrenceProtocols ?? '';
$occurrenceDbId = $occurrenceDbId ?? '';

echo '<div class="management-group-panel">';

echo '<label class="control-label" >'.\Yii::t('app','Management Variable List') .'</label>';

echo Select2::widget([
    'data' => $mgtProtocolTags,
    'name' => 'management-select-group',
    'id' => 'management-select-group',
    'maintainOrder' => true,
    'options' => [
      'placeholder' => \Yii::t('app','Select a list'),
      'tags' => true
    ]
]);

echo '</div>';
// Modal for replacing management variables
Modal::begin([
    'id' => 'mgt-group-replace-all-modal',
    'header' => '<h4><i class="material-icons left">swap_horiz</i> '. \Yii::t('app','Replace All Management Variables').'</h4>',
    'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',['id' => 'mgt-back-replace-btn','data-dismiss'=>'modal']).'&nbsp;&emsp;'.Html::a(\Yii::t('app','Confirm'). ' <i class="material-icons right">send</i>','#',['id' => 'mgt-proceed-replace-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static']
    ]);
    echo \Yii::t('app','<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You have chosen to replace the selected management variables with <span id="management-name"></span>. Please note that this will replace <b>ALL</b> the current management variables in this experiment. Click the <b>Confirm</b> button to proceed with the operation. Otherwise, click the <b>Cancel</b> button.');
  Modal::end();

// Modal for management variable list preview
Modal::begin([
    'id' => 'mgt-list-preview-modal',
    'header' => '<h4 id="mgt-list-preview-header"></h4>',				
    'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',['id' => 'mgt-back-preview-btn','data-dismiss'=>'modal']).'&nbsp;&emsp;'.Html::a(\Yii::t('app','Confirm'). ' <i class="material-icons right">send</i>','#',['id' => 'mgt-ok-preview-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop'=>'static']
]);
echo '<p id="mgt-list-preview-modal-note">'.\Yii::t('app','Below is the list of management variables that will be added in this experiment. Click "Confirm" to proceed.').'</p>';
echo '<p id="mgt-list-preview-modal-body">';
Modal::end();
?>

<div style="margin-top:10px">
  <button class="btn waves-effect waves-light mgt-add-replace-list-btn" title="<?= \Yii::t('app','Add variables') ?>" id="management-select-group-add-btn" data-operation="added" disabled style="margin: 0px 7px 0px 0px;"><?= \Yii::t('app','Add') ?></button>

  <button class="btn waves-effect waves-light grey lighten-3 grey-text text-darken-2 mgt-add-replace-list-btn" id="management-select-group-replace-btn" data-operation="replaced" disabled style="margin: 0px 10px 0px 0px;" title="<?= \Yii::t('app','Replace all') ?>"><i class="material-icons">swap_horiz</i></button>

  <div class="hidden preloader-wrapper small active" style="width:20px;height:20px;">
      <div class="spinner-layer spinner-green-only">
          <div class="circle-clipper left">
              <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
              <div class="circle"></div>
          </div>
      </div>
  </div>
</div>


<?php
//routes
$addVariablesUrl = Url::to(['/experimentCreation/protocol/add-variables']);
$confirmAddVarListUrl = Url::to(['/experimentCreation/protocol/add-mgt-var-list']);

$this->registerJs(<<<JS

     var link = '/index.php/experimentCreation/protocol/management-protocol?program=' + "$program" + '&id='+"$id"+'&processId=' + "$processId" +'&shouldUpdateOccurrenceProtocols='+"$shouldUpdateOccurrenceProtocols" + '&occurrenceDbIdsString='+"$occurrenceDbIdsString";

    // check if there is a selected list
    $('#management-select-group').bind("change keyup input",function() {
        var selectedList = $('#management-select-group').val();
        
        if(selectedList == ''){
            $('.mgt-add-replace-list-btn').attr('disabled',true);
        }else{
            $('.mgt-add-replace-list-btn').attr('disabled',false);
        }
    });

    //add/replace management via list	
    $('.mgt-add-replace-list-btn').on('click', function(){
        var obj = $(this);
        var operation = obj.data('operation');
        var selectedList = $('#management-select-group').val();
        var list = $('#select2-management-select-group-container').attr('title');
        var header = '<i class="material-icons left">add</i> Add management variables from ' + list;
        
        $('.preloader-wrapper').removeClass('hidden');
        $.ajax({
            url: '$confirmAddVarListUrl',
            type: 'post',
            dataType: 'json',
            data: {
                listId: selectedList
            },
            success: function(response) {

                $('.preloader-wrapper').addClass('hidden');
                let note = '';
                if(operation == 'replaced'){
                    header = '<i class="material-icons left">swap_horiz</i> Replace All Management Variables';
                    note = '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You have chosen to replace the selected management variables with <b>' + list + '</b>. Please note that this will replace <b>ALL</b> current management variables in this experiment. Click the <b>Confirm</b> button to proceed with the operation. Otherwise, click the <b>Cancel</b> button.'
                }
                $('#mgt-list-preview-header').html(header);
                $('#mgt-list-preview-modal-note').html(note);
                $('#mgt-list-preview-modal-body').html(response.htmlData);
                $('#mgt-list-preview-modal').modal('show');
                
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while displaying preview of variables. ";

                $('.toast').css('display','none');
                Materialize.toast(notif,5000);
            }
        });
    });


    //proceed to replacing all management
    $('#mgt-proceed-replace-btn').on('click',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        
        var selectedVar = $('#management-select-variable').val();

        $('.preloader-wrapper').removeClass('hidden');

        $.ajax({
            url:'$addVariablesUrl',
            type: 'post',
            dataType: 'json',
            data: {
                listId: '$listId',
                id: selectedVar,
                type: 'variable',
                operation: 'replaced',
                protocolType: 'management',
                experimentId: '$id'
            },
            success: function(response) {

                $('.toast').css('display','none');
                Materialize.toast(response,5000);

                $('.preloader-wrapper').addClass('hidden');
                if(response.includes('Success')){
                    $.pjax.reload({
                        url: link,
                        container: '#management-list-selected-vars-grid-pjax',
                        replace: false,
                    });
                }

                $('#mgt-group-replace-all-modal').modal('hide');

            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";

                $('.toast').css('display','none');
                Materialize.toast(notif,5000);
            }
        });
    });


    //confirm add from management protocol
    $('#mgt-ok-preview-btn').on('click', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var obj = $('#mgt-list-preview-header').html();
        var operation = obj.includes('Add') ? 'added':'replaced';
        var selectedList = $('#management-select-group').val();
        
        $('.preloader-wrapper').removeClass('hidden');

        $.ajax({
            url: '$addVariablesUrl',
            type: 'post',
            dataType: 'json',
            data: {
                listId: '$listId',
                id: selectedList,
                type: 'list',
                operation: operation,
                protocolType: 'management',
                experimentId: '$id'
            },
            success: function(response) {

                $('.toast').css('display','none');
                Materialize.toast(response,5000);

                if(response.includes('Success')){
                    $.pjax.reload({
                        url: link,
                        container: '#management-list-selected-vars-grid-pjax',
                        replace: false
                    });
                }
                $('.preloader-wrapper').addClass('hidden');
                $('#mgt-list-preview-modal').modal('hide');
                
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";

                $('.toast').css('display','none');
                Materialize.toast(notif,5000);
            }
        });
    });


JS);
?>