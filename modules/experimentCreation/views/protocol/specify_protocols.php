<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Renders protocol tab for the experiment creation
*/
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\builder\TabularForm;
use yii\widgets\Pjax;
use kartik\dynagrid\DynaGrid;
use marekpetras\yii2ajaxboxwidget\Box;
use kartik\tabs\TabsX;

$disabled = '';

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/protocol/tabs.php',[
    'id' => $id,
    'program' => $program,
    'processId' => $processId,
    'model' => $model,
    'viewArr' => $viewArr,
    'protocolChildren' => $protocolChildren
]);
?>

<style>
.tabular-icon{
    margin-left: 4px !important;
}
.tabular-btn{
    padding: 0 0.9rem 0 0.9rem;
}
.row{
    margin-bottom: 10px;
}
.fa{
    font-size: 95%;
} 
</style>
