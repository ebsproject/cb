<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting protocol's browser
*/
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\widgets\Select2;
use yii\web\JqueryAsset;
use yii\bootstrap\ActiveForm;
use app\components\GenericFormWidget;
?>

<?php

    if(!empty($widgetOptions)){
        echo GenericFormWidget::widget([
            'options' => $widgetOptions
        ]);
    } else {
        echo '<div class="alert alert-warning" style="padding: 10px;">There is no configuration saved for this plot type.</div>';
    }
    
?>

<?php
    
$computeFormulaUrl = Url::to(['protocol/compute-formula','id'=>$id]);
 
$this->registerJs(<<<JS
    var timeout = null;
    var triggerComputePAFlag = false;
    $('.computed').each(function(e){
        var id = $(this).attr('id');
        var formulaVariable  = ($(this).attr('id')).split("-")[0];
        var variableVal = $('#'+id).val();
        var isReadOnly = $(this).attr('readonly');
        formulaComputation(id,formulaVariable,variableVal,isReadOnly);
    });

    $(document).on("change keydown paste blur cut input", "input[type='number']", function() {
        var min, max, value = null;
        var key = ($(this).attr('id')).split("-")[1];
        var variable = ($(this).attr('id')).split("-")[0];
        var year_min_value = parseInt('$year_min_value');
        var year_max_value = parseInt('$year_max_value'); 

        if(this.value != null && this.value != ''){    
            if(this.hasAttribute('step')){
                value = parseFloat(this.value);
            } else {
                value = parseInt(this.value);
            }
            
            if(this.hasAttribute('min') && this.hasAttribute('max')){
                min = $(this).attr('min');
                max = $(this).attr('max');
                                   
                if(variable == 'year' || variable == 'YEAR'){
                    if(year_min_value == '' || year_min_value == null){
                        min = 1920;
                    }else{
                        min = year_min_value;
                    }
                
                    if(year_max_value == '' || year_max_value == null || isNaN(year_max_value) == true){
                        max = '';
                    }else{
                        max = year_max_value;
                    }
                }
                
                if((value < min || value > max) && (max != null && max != '' && max != 0)){
                 
                    $(this).addClass('invalid');
                    $(this).attr('title', 'Input should be greater than '+min+' and less than '+max);
                    $('#save-planting-btn').addClass('disabled');
                }else if((value < min || value > max) && (max == null || max == '' || max == 0)){
                    //When MAX is null
                    if(value < min){
                        if ( $('input').is('[readonly]') ) {
                            
                        }else{
                            $(this).addClass('invalid');
                            $(this).attr('title', 'Input should be greater than '+min);
                            $('#save-planting-btn').addClass('disabled');
                        }
                    }else{
                    
                        $(this).removeClass('invalid');
                        $(this).removeAttr('title');
                        $('#save-planting-btn').removeClass('disabled');
                    }
                }else{
                  
                    $(this).removeClass('invalid');
                    $(this).removeAttr('title');
                    $('#save-planting-btn').removeClass('disabled');
                }
            }else if(this.hasAttribute('min')){
             
                min = $(this).attr('min');

                if(variable == 'year' || variable == 'YEAR'){
                    if(year_min_value == '' || year_min_value == null){
                        min = 1920;
                    }else{
                        min = year_min_value;
                    }
                }
                if(value < min){
                    if ( $('input').is('[readonly]') ) {
                    }else{
                        $(this).addClass('invalid');
                        $(this).attr('title', 'Input should be greater than '+min);
                        $('#save-planting-btn').addClass('disabled');
                    }
                } else {
                    
                    $(this).removeClass('invalid');
                    $(this).removeAttr('title');
                    $('#save-planting-btn').removeClass('disabled');
                }
                
            } else if(this.hasAttribute('max')){

                max = $(this).attr('max');
                if(variable == 'year' || variable == 'YEAR'){
                    if(year_max_value == '' || year_max_value == null){
                        max = '';
                    }else{
                        max = year_max_value;
                    }
                }
                if(value > max){
                    if ( $('input').is('[readonly]') ) {
                    }else{
                        $(this).addClass('invalid');
                        $(this).attr('title', 'Input should be less than '+max);
                        $('#save-planting-btn').addClass('disabled');
                    }
                }else {
                    $(this).removeClass('invalid');
                    $(this).removeAttr('title');
                    $('#save-planting-btn').removeClass('disabled');
                }
            } else{
                $(this).removeClass('invalid');
                $(this).removeAttr('title');
                $('#save-planting-btn').removeClass('disabled');
            }
        } else if (value != ''){
            // not a number
            if ( $('input').is('[readonly]') ) {
            }else{
                $(this).addClass('invalid');
                $(this).attr('title', 'Input should be numeric');
                $('#save-planting-btn').addClass('disabled');
            }
        }
        
    });

    $(document).on("change keydown", ".computed", function() {
        var fieldId = $(this).attr('id');
        fieldVal = $('#'+fieldId).val();

         clearTimeout(timeout);

        // Make a new timeout set to go off in 1000ms (1 second)
        timeout = setTimeout(function () {
       
            $('.computed').each(function(){
                var id = $(this).attr('id');
                var formulaVariable  = ($(this).attr('id')).split("-")[0];
                var variableVal = $('#'+id).val();
                var isReadOnly = $(this).attr('readonly');
                formulaComputation(id,formulaVariable,variableVal,isReadOnly,'update',fieldId,fieldVal);
            });
        }, 500);
    });

    function formulaComputation(id,formulaVariable,variableVal,isReadOnly, action, fieldId=null, fieldVal=null){       
        var hasVariable = formulaVariable.includes('PLOT_AREA');
        var hasPlotWidth = formulaVariable.includes('PLOT_WIDTH');
        var hasPlotLength = formulaVariable.includes('PLOT_LN');

        if(hasVariable == true || hasPlotWidth || hasPlotLength){
            isReadOnly = null;
        }
        
        //If the field with formula is not disabled, then run the formula computation
        if(fieldId == null && id != fieldId && (variableVal !== '' && variableVal !== 0 && variableVal !== null)){

            if((isReadOnly != 'readonly' && isReadOnly != null) && (variableVal !== '' && variableVal !== 0 && variableVal !== null)){ 
                var formData = $("#planting-protocol-form").serialize();

                $.ajax({
                    url: '$computeFormulaUrl',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        formData: formData,
                        formulaVariable: formulaVariable
                    },
                    success: function(response){
                        if(response != 0){
                            $('#'+id).val(response);
                        }
                    }
                });
            }
        }else if(isReadOnly != 'readonly' && action != 'update' && variableVal == ''){
            
            var formData = $("#planting-protocol-form").serialize();

            $.ajax({
                url: '$computeFormulaUrl',
                type: 'post',
                dataType: 'json',
                async: false,
                data: {
                    formData: formData,
                    formulaVariable: formulaVariable
                },
                success: function(response){
                    if(response != 0){
                        $('#'+id).val(response);
                    }
                }
            });
        }else if(action == 'update'){

            variableVal = $('#'+fieldId).val();
            if(variableVal !== '' && variableVal !== 0 && variableVal !== null){
                var formData = $("#planting-protocol-form").serialize();
                
                if(fieldId.includes("DIST_BET_HILLS") || fieldId.includes("HILLS_PER_ROW")){
                    $.ajax({
                        url: '$computeFormulaUrl',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {
                            formData: formData,
                            formulaVariable: formulaVariable
                        },
                        success: function(response){
                            if(response != 0){
                                $('#'+id).val(response);
                            }
                        }
                    });
                } else {

                    $.ajax({
                        url: '$computeFormulaUrl',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            formData: formData,
                            formulaVariable: formulaVariable
                        },
                        success: function(response){
                            if(response != 0){
                                $('#'+id).val(response);

                                // if form has plot width field that is computed, 
                                // trigger key down to compute plot area with plot width value
                                // trigger only once
                                if(formulaVariable.includes("PLOT_WIDTH")){

                                    if(!triggerComputePAFlag){
                                        $('#'+id).trigger('keydown');
                                        triggerComputePAFlag = true;                                    
                                    }
                                }
                            }
                        }
                    });
                }
                
            }
        }   
    } 

    // validate numeric
    function forceNumeric(){
        var input = $(this);
        input.val(input.val().replace(/[^\d]+/g,''));
    } 

    function forceDecimal(evt){
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.]/g, ''));
    
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
        {
            evt.preventDefault();
        }
    }
JS
);
?>
<style>
input[type=number]:not(.browser-default)[readonly="readonly"]{
    cursor: not-allowed;
}
</style>