<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders harvest protocol's browser
*/
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\GenericFormWidget;
?>

<div class="col col-sm-12 view-entity-content">
    <div id="content-body">
        <div id="default-content" class="col col-sm-12 col-md-12 col-lg-12">
            <?php
                $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'id' => 'harvest-protocol-form',
                    'action' => ['/experimentCreation/protocol/harvest-protocol','id'=>$id,'program'=>$program,'processId'=>$processId]
                ]);
            ?>
                <div class="parent-box"  id="filter-panel">
                    <?php
                        if(isset($configValues)){
                            echo GenericFormWidget::widget([
                                'options' => $defaultWidgetOptions
                            ]);
                        }
                    else{
                        echo '<p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'No harvest config found.').'</span></p>';
                    }
                    ?>
                </div>
                    <div
                        id="dynamic-fields"
                        class="pull-right"
                    >
                        <?=($shouldUpdateOccurrenceProtocols) ? Html::a(
                            'Cancel',
                            Url::toRoute('/occurrence?program=' . $program),
                            [
                                'id' => 'cancel-bulk-update-occurrences-harvest-protocol',
                                'title' => \Yii::t('app', 'Cancel update'),
                                'style' => [
                                    'vertical-align' => '-10px',
                                    'margin-right' => '12px',
                                ],
                            ]
                        ) : null?>
                        <?=Html::button(\Yii::t('app','Save'),[
                            'class' => 'waves-effect waves-light btn btn-primary',
                            'style' => 'margin-right:20px;margin-top:15px;margin-bottom:10px;' .
                                $cssDisplaySaveButton, // This variable came from /experimentCreation/controllers/ProtocolController
                            'id' => 'save-harvest-btn',
                            'disabled' => true,
                            'title' => Yii::t('app', 'Save Harvest Protocols'),
                        ])?>
                    </div>
                <div class="clearfix"></div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div> <!--End of View entity content class-->

<?php
    Modal::begin([
        'id' => 'generic-modal',
    ]);
    Modal::end();

    // Modal for validation
    Modal::begin([
        'id' => 'required-field-modal',
        'header' => '<h4><i class="fa fa-bell"></i>  '.\Yii::t('app','Notification').'</h4>',
        'footer' => Html::a(\Yii::t('app','OK'),['#'],['id'=>'cancel-save-btn','class'=>'btn btn-primary waves-effect waves-light','data-dismiss'=>'modal']),
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static']
    ]);
    echo '<div class="row">
        <p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'Please correct the errors below before proceeding.').'</span></p>
        </div><div class="row" id="required-field-notif"></div>';
    Modal::end();
?>

<?php
//TODO: Remove unused URLs once refactoring is done
// $loadVarValueUrl = Url::to(['protocol/load-variable-value','id'=>$id]);
// $checkPlantingProtocolUrl = Url::to(['protocol/check-protocol','id'=>$id]);
$defaultFieldValues = json_encode($defaultFieldArr);
$protocolChildren = json_encode($protocolChildren);
// $defaultVars = json_encode($defaultVariables);
$reqFieldJson = json_encode($configValues);
$updateFilterUrl = Url::to(['protocol/update-filter-tags','id'=>$id]);

if(!empty($configValues)){
    $genericRenderConfigUrl = Url::to(['create/render-config-form', 'id'=>$id, 'program'=>$program]);

    Yii::$app->view->registerJs("
    var genericConfUpdateUrl = '".Url::toRoute(['/experimentCreation/create/update-config-variable','id'=>$id, 'program'=>$program])."',
    genericRenderConfigUrl = '".$genericRenderConfigUrl."',
    experimentId = '".$id."',
    requiredFieldJson = '".$reqFieldJson."'
    ;",
    \yii\web\View::POS_HEAD);

    $this->registerJsFile("@web/js/generic-form-validation.js", [
        'depends' => ['app\assets\AppAsset'],
        'position'=>\yii\web\View::POS_END
    ]);
}

$this->registerJs(<<<JS
    var submitFlag = 0;
    var select2Id = '';
    var establishmentVal = '';
    var plantingTypeVal = '';
    var plotTypeVal = '';
    var variableAbbrev = '';
    var tempDefault = '';
    var flag = false;  
    var defaultFieldValues = JSON.parse('$reqFieldJson');
    var establishmentId = '';
    var plantingTypeId = '';
    var plotTypeId = '';
    var harvMethodId = null;
    var harvMethodVal = null;
    
    $( "form#harvest-protocol-form" ).find( ".basic-info1" ).addClass('widen-basic-info');
    // $('#basic-info1').addClass('col col-md-8');
    // $('.box-1').removeClass("col-md-6 s6 parent-box");
    // $('.box-1').addClass("col-md-12 s12 parent-box");
 
    if($('.select2-input-validate').val() != null){
        $('#save-harvest-btn').attr('disabled', false);
    }

    // $('.box-2').remove();
  
    $("document").ready(function(){
        $(document).on('change','.select2-input-validate', function(e, params){
            e.preventDefault();
            var selectedId = $(this).val();
            
            var elementId = $(this).attr('id');
            if(elementId.includes('HV_METH_DISC')){
                harvMethodId = elementId;
            }

            if(selectedId !== '' && selectedId !== null){
                $('#save-harvest-btn').attr('disabled', false);
            }else{
                $('#save-harvest-btn').attr('disabled', true);
            }
        });
    });
  

    $(document).on('click','#save-harvest-btn', function(){
        var zeroFlag = false;
        $('input[type="number"]').each(function(){
            var checkVal = $(this).val();
            if(checkVal == 0){
                zeroFlag = true;
            }
        })
      
        var form_data = $.param($.map($("#harvest-protocol-form").serializeArray(), function(v,i) {
            return (v.name == "add_variable_info_select") ? null : v;
        }));
        var nextUrl = $(this).attr('data-url');
        form_data = form_data + '&' + $.param({
                        ['nextUrl'] : nextUrl
                    });
        form_data =  form_data + '&' + $.param({
                        ['processId'] : '$processId'
                    });

        if ('$shouldUpdateOccurrenceProtocols') {
            form_data =  form_data + '&' + $.param({
                ['shouldUpdateOccurrenceProtocols'] : '$shouldUpdateOccurrenceProtocols'
            });

            form_data =  form_data + '&' + $.param({
                ['occurrenceDbIdsString'] : '$occurrenceDbIdsString'
            });

            let notif = "<i class='material-icons blue-text'>info</i> <span class='white-text'> Please wait for the update to complete.</span>"; 
            Materialize.toast(notif, 5000);
        }
        
        var action_url = $("#harvest-protocol-form").attr("action");
        
        //Manually retrieve again the ID
        if(harvMethodId == null){
            $('.select2-input-validate').each(function(){
                var elementId = $(this).attr('id');
                if(elementId.includes('HV_METH_DISC')){
                    harvMethodId = elementId;                    
                }
            })
        }

        harvMethodVal = $('#'+harvMethodId).val();
        
        if(submitFlag == 0 && harvMethodVal != null){
            $.ajax({
                type:"POST",
                dataType: 'json',
                url: action_url,
                data: form_data,
                async: true,
                success: function(data) {
                    var hasToast = $('body').hasClass('toast');
                    
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons green-text'>check</i> <span class='white-text'>Successfully saved harvest protocol variables.</span>"; 
                        Materialize.toast(notif, 5000);
                    }
                    $('#next-btn').attr('disabled', false);

                    if ('$shouldUpdateOccurrenceProtocols' && '$hasMultipleSelectedOccurrences') {
                        location.reload()
                    }
                },
                error: function(){
                }
            });
        }else{
            var hasToast = $('body').hasClass('toast');
                    
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please specify a Harvest Method.</span>";
                Materialize.toast(notif, 5000);
            }
        }
    });
JS
);
?>
<style>
#content-body{
    margin-top: 10px;
}
.required{
    color:red;
}
label{
    font-size: .8rem;
    color: #333333;
}
#basic-info1{
    margin-left:auto;
    margin-right:auto;
    margin-top:15px;
    display: inline-block;
}
.widen-basic-info{
    width: 50% !important;
}
#basic-info2{
    margin-left:auto;
    margin-right:auto;
    margin-top:15px;
    display: inline-block;
}
#filter-panel{
    margin-top: 0px;
}
.my-container{
    min-width: 50%;
    max-width: 100%
}
.select2-results__option.loading-results,
.select2-results__option.select2-results__option--load-more {
    background-image: url('/images/theme/loader.gif');
    background-repeat: no-repeat;
    padding-left: 35px;
    background-position: 10px 50%;
}
.generic-instructions{
    display: none;
}
.parent-box{
    position: relative;
    margin-top: 1rem;
}
.div-protocol-save{
   float: right;
   display:block;
}
</style>