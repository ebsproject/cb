<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders traits protocol box view
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use marekpetras\yii2ajaxboxwidget\Box;

$isViewOnly = $isViewOnly ?? '';
$previewOnly = $previewOnly ?? '';
$hasMultipleSelectedOccurrences = $hasMultipleSelectedOccurrences ?? '';
$occurrenceDbIdsString = $occurrenceDbIdsString ?? '';
$shouldUpdateOccurrenceProtocols = $shouldUpdateOccurrenceProtocols ?? '';
$occurrenceDbId = $occurrenceDbId ?? '';

$helperText = ($isViewOnly === '1' || $previewOnly === '1' ) ? '' : 'Specify traits to be observed. You may add by selecting from the list of traits or existing trait lists. <b> Changes are auto-saved</b>.';
?>

<p style="margin-bottom:5px"><?= \Yii::t('app', $helperText)?></p>
<div id = "traits-protocol-div" class = "row col col-sm-12 col-md-12 col-lg-12">	
	<?php
		$toolsButtons = [
			'reload' => function(){
				return 
					'&nbsp;&nbsp;<a href="#!" title="'.\Yii::t('app','Refresh data').'" class="text-muted fixed-color" onclick="$(&quot;#pr-traits-protocols-box&quot;).box(&quot;reload&quot;);"><i class="material-icons widget-icons">loop</i></a>';
			}
		];
		
		$options = [
			'id' => 'pr-traits-protocols-box',
			'bodyLoad' => ['/experimentCreation/protocol/traits-protocols?program=' . $program . '&id=' . $id . '&processId=' . $processId . '&isViewOnly=' . $isViewOnly. '&previewOnly=' . $previewOnly . '&occurrenceDbId=' . $occurrenceDbId . '&shouldUpdateOccurrenceProtocols=' . $shouldUpdateOccurrenceProtocols . '&occurrenceDbIdsString=' . $occurrenceDbIdsString . '&hasMultipleSelectedOccurrences=' . $hasMultipleSelectedOccurrences],
			'toolsTemplate' => '{reload}',
			'toolsButtons' => $toolsButtons
		]
	?>
	<?= Box::widget($options); ?>
</div>

<style>
#pr-traits-protocols-box, #traits-protocol-div{
	background-color: #f1f4f5;
}
.box-body, #traits-protocol-div{
	padding: 0px;
} 
.trait-div{
	padding: 0px 10px 0px 0px!important;
}
.box{
	margin: 0px;
}
.panel-title{
	margin-top:10px;
}
.panel-heading .summary {
    margin-top: 0px;
}
</style>