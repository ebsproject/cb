<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders trait protocols view
 */

use yii\helpers\Url;
use kartik\tabs\TabsX;

?>

<!-- Add variables panel -->
<div class="col col-md-5 trait-div">
    <div class="card" style="padding: 5px;">
        <div class="row">
            <div class="col col-md-12">
                <?php
				$traitsVarLabel = Yii::t('app', 'Traits');
				$traitsVarListLabel = Yii::t('app', 'Trait Lists');

                $subTabs = [
                    //trait variables
                    [
                        'label' => '<i class="fa fa-leaf"></i> ' . $traitsVarLabel,
                        'linkOptions' => ['class' => 'sub-tab', 'data-id' => 1],
                        'items' => [[
                            'label' => $traitsVarLabel,
                            'linkOptions' => ['id' => 'pr-sub-tab-1'],
                            'active' => true,
                            'encode' => false,
                            'content' => Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/protocol/traits/_trait_variables.php', [
                                'id' => $id,
                                'traitVariableTags' => $traitVariableTags,
                            ])
                        ]],
                        'headerOptions' => ['class' => 'hide-caret', 'data-id' => 1, 'title' => 'Trait variables']
                    ],
                    //trait protocols
                    [
                        'label' => '<i class="fa fa-list-alt"></i> ' . $traitsVarListLabel,
                        'linkOptions' => ['class' => 'sub-tab', 'data-id' => 2],
                        'items' => [[
                            'label' => $traitsVarListLabel,
                            'linkOptions' => ['id' => 'pr-sub-tab-2'],
                            'encode' => false,
                            'content' => Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/protocol/traits/_trait_protocols.php', [
                                'id' => $id,
                                'traitProtocolTags' => $traitProtocolTags,
                            ])
                        ]],
                        'headerOptions' => ['class' => 'hide-caret', 'data-id' => 2, 'title' => 'Add variables from a trait list']
                    ]
                ];

                echo TabsX::widget([
                    'items' => $subTabs,
                    'position' => TabsX::POS_LEFT,
                    'sideways' => true,
                    'encodeLabels' => false,
                    'pluginOptions' => [
                        'enableCache' => false
                    ],
                    'enableStickyTabs' => true,
                    'containerOptions' => ['class' => 'pr-traits-protocols-tab']
                ]);

                ?>
            </div>
        </div>
    </div>
</div>

<!-- Selected traits -->
<div class="col col-md-7 trait-div">
    <div class="card" style="padding: 5px">
        <div class="row">
            <div class="col col-md-12">
                <?php
                if ($hasMultipleSelectedOccurrences) {
                    # TODO: Empty trait list for bulk update
                }
                echo Yii::$app->controller->render('@app/modules/experimentCreation/views/protocol/traits/_trait_list.php', [
                    'id' => $id,
                    'dataProvider' => $dataProvider,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
//routes
$addVariablesUrl = Url::to(['/experimentCreation/protocol/add-variables']);
$reorderUrl = Url::to(['/experimentCreation/protocol/reorder-variables']);
$removeVariablesUrl = Url::to(['/experimentCreation/protocol/remove-variables']);
$confirmDeleteVariablesUrl = Url::to(['/experimentCreation/protocol/confirm-delete-variables']);
$restoreVariablesUrl = Url::to(['/experimentCreation/protocol/restore-variables']);
$saveAsNewListUrl = Url::to(['/experimentCreation/protocol/save-as-list']);
$saveRemarksUrl = Url::to(['/experimentCreation/protocol/save-remarks']);
$saveBulkRemarksUrl = Url::to(['/experimentCreation/protocol/save-bulk-remarks']);
$retrieveVarListPreviewUrl = Url::to(['/experimentCreation/protocol/retrieve-variables-preview', 'id' => $id]);
$confirmAddVarListUrl = Url::to(['/experimentCreation/protocol/add-var-list']);
$confirmSaveVariableListUrl = Url::to(['/experimentCreation/protocol/save-variable-list']);
$resetUrl =  "/index.php/experimentCreation/protocol/traits-protocols?$program" . '&id=' . $id . '&processId=' . $processId;

?>

<?php
$this->registerJs(<<<JS

	var link = '/index.php/experimentCreation/protocol/traits-protocols?program=' + "$program" + '&id='+"$id"+'&processId=' + "$processId" +'&shouldUpdateOccurrenceProtocols='+"$shouldUpdateOccurrenceProtocols" + '&occurrenceDbIdsString='+"$occurrenceDbIdsString";
	
	//calls subtab content upon click
	$(document).on('click','.sub-tab',function(e){
		var subTabId = $(this).data('id');
		
		$('#pr-sub-tab-'+subTabId).click();

	});

	//disable action buttons when there is no record
	var varCount = document.getElementsByClassName("final-grid_select");
	if(varCount.length == 0){
		$('.trait-list-actions').addClass('disabled');
	}else{
		$('.trait-list-actions').removeClass('disabled');
	}

	// check if there is selected list
	$('#trait-select-variable').bind("change keyup input",function() {
		var selectedList = $('#trait-select-variable').val();
		
		if(selectedList == ''){
			$('.add-replace-vars-btn').attr('disabled',true);
		}else{
			$('.add-replace-vars-btn').attr('disabled',false);
		}
	});
	
	// add/replace variables upon click
		$('.add-replace-vars-btn').on('click',function(){

		var obj = $(this);
		var operation = obj.data('operation');

		if(operation == 'replaced'){
			let traitLabel = $('#select2-trait-select-variable-container').attr('title')
			$('#trait-name').html('<b>' + traitLabel + '</b>')

			$('#replace-all-modal').modal('show');
		}
		else{
			var selectedVar = $('#trait-select-variable').val();

			$('.preloader-wrapper').removeClass('hidden');

			$.ajax({
				url:'$addVariablesUrl',
				type: 'post',
				dataType: 'json',
				data: {
					listId: '$listId',
					id: selectedVar,
					type: 'variable',
					operation: operation,
					experimentId:'$id'
				},
				success: function(response) {
					$('.toast').css('display','none');
					Materialize.toast(response,5000);

					$('.preloader-wrapper').addClass('hidden');

					if(response != null && response != undefined && response.includes('Success')){
						$.pjax.reload({
							url: link,
							container: '#trait-list-selected-vars-grid-pjax',
							replace: false
                        });
					}
				},
				error: function() {
					var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";

					$('.toast').css('display','none');
					Materialize.toast(notif,5000);
				}
			});
		}
	});  

	//proceed to replacing all traits
	$('#proceed-replace-btn').on('click',function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		
		var selectedVar = $('#trait-select-variable').val();

		$('.preloader-wrapper').removeClass('hidden');

		$.ajax({
			url:'$addVariablesUrl',
			type: 'post',
			dataType: 'json',
			data: {
				listId: '$listId',
				id: selectedVar,
				type: 'variable',
				operation: 'replaced',
				experimentId: '$id'
			},
			success: function(response) {

				$('.toast').css('display','none');
				Materialize.toast(response,5000);

				$('.preloader-wrapper').addClass('hidden');
				if(response.includes('Success')){
					$.pjax.reload({
						url: link,
						container: '#trait-list-selected-vars-grid-pjax',
						replace: false,
        			});
				}

				$('#replace-all-modal').modal('hide');

			},
			error: function() {
				var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";

				$('.toast').css('display','none');
				Materialize.toast(notif,5000);
			}
		});
	});

	// check if there is a selected list
	$('#trait-select-group').bind("change keyup input",function() {
		var selectedList = $('#trait-select-group').val();
		
		if(selectedList == ''){
			$('.add-replace-list-btn').attr('disabled',true);
		}else{
			$('.add-replace-list-btn').attr('disabled',false);
		}
	});


	//confirm add from trait protocol

	$('#ok-preview-btn').on('click', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();

		var obj = $('#list-preview-header').html();
		var operation = obj.includes('Add') ? 'added':'replaced';
		var selectedList = $('#trait-select-group').val();
		
		$('.preloader-wrapper').removeClass('hidden');

		$.ajax({
			url: '$addVariablesUrl',
			type: 'post',
			dataType: 'json',
			data: {
				listId: '$listId',
				id: selectedList,
				type: 'list',
				operation: operation,
				experimentId: '$id'
			},
			success: function(response) {

				$('.toast').css('display','none');
				Materialize.toast(response,5000);

				if(response.includes('Success')){
					$.pjax.reload({
						url: link,
						container: '#trait-list-selected-vars-grid-pjax',
						replace: false
					});
				}
				$('.preloader-wrapper').addClass('hidden');
				$('#list-preview-modal').modal('hide');
				
			},
			error: function() {
				var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";

				$('.toast').css('display','none');
				Materialize.toast(notif,5000);
			}
		});
	});

	//add/replace traits via list	
	$('.add-replace-list-btn').on('click', function(){

		var obj = $(this);
		var operation = obj.data('operation');
		var selectedList = $('#trait-select-group').val();
		var list = $('#select2-trait-select-group-container').attr('title');
		var header = '<i class="material-icons left">add</i> Add traits from ' + list;

		$('.preloader-wrapper').removeClass('hidden');

		$.ajax({
			url: '$confirmAddVarListUrl',
			type: 'post',
			dataType: 'json',
			data: {
				listId: selectedList,
			},
			success: function(response) {

				$('.preloader-wrapper').addClass('hidden');
				let note = ''

				if(operation == 'replaced'){
					header = '<i class="material-icons left">swap_horiz</i> Replace All Traits';
					note = '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You have chosen to replace the selected traits with <b>' + list + '</b>. Please note that this will replace <b>ALL</b> current traits in this experiment. Click the <b>Confirm</b> button to proceed with the operation. Otherwise, click the <b>Cancel</b> button.'
				}
				$('#list-preview-header').html(header);
				$('#list-preview-modal-note').html(note);
				$('#list-preview-modal-body').html(response.htmlData);
				$('#list-preview-modal').modal('show');
				
			},
			error: function() {
				var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while displaying preview of variables. ";

				$('.toast').css('display','none');
				Materialize.toast(notif,5000);
			}
		});
	});

	// hide loader if action is cancelled
	$('#back-replace-btn, #back-delete-btn, #back-preview-btn').on('click', function(){
		$('.preloader-wrapper').addClass('hidden');
	});

	//TODO: to be refactored
	//enable row reordering
	// $("#trait-list-selected-vars-grid tbody").sortable({
	// 	tolerance: 'pointer',
	// 	cursor: 'move',
	// });
	
	$(document).on('ready pjax:success', function(e) {
		e.preventDefault();
		refresh();
	});
	
	refresh();

	// refresh gridview
	function refresh(){
		$(".trait-div").css("height", ($(window).height() - 270));

		$(".kv-grid-wrapper").css("height",$('.trait-div').height() - 140);

	}

	// check row in variables browser
	$(document).on('click', '#trait-list-selected-vars-grid tbody tr', function(e) {

		this_row=$(this).find('input:checkbox')[0];
		if(this_row.checked){
			this_row.checked=false;  
			$('#trait-list-variables-grid-select-all-id').prop("checked", false);
			$(this).removeClass("grey lighten-4");
		}else{
			$(this).addClass("grey lighten-4");
			this_row.checked=true;  
			$("#"+this_row.id).prop("checked");  
		}

	});

	// select all variables
	$(document).on('click', '#trait-list-variables-grid-select-all-id', function(e) {
		if($(this).prop("checked") === true)  {
			$(this).attr('checked','checked');
			$('.final-grid_select').attr('checked','checked');
			$('.final-grid_select').prop('checked',true);
			$(".final-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");

		}else{

			$(this).removeAttr('checked');
			$('.final-grid_select').prop('checked',false);
			$('.final-grid_select').removeAttr('checked');
			$("input:checkbox.final-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");      
		}

	});

	// view save new list modal
	$(document).on('click', '#trait-save-new-list-btn', function(e) {

		$('#trait-save-list-preview-error').html('');
		$('#trait-save-list-modal').modal('show');

		$('#trait-save-list-modal').on('shown.bs.modal', function() {
			$('#trait-list-name').trigger('focus');
		});

		$('.trait-modal-loading').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
		$('.trait-save-list-preview-variables').html('');

		var varIdsSavedList=[];
		var retrieveVarListPreviewUrl = '$retrieveVarListPreviewUrl';

		$("input:checkbox.final-grid_select").each(function(){

			if($(this).prop("checked") === true)  {
				varIdsSavedList.push($(this).attr("id"));
			}

		});

		$("#trait-list-abbrev").trigger("change");
		setTimeout(function(){
			$.ajax({
				type: 'POST',
				url: retrieveVarListPreviewUrl,
				data: {
					id: '$id',
					idList: varIdsSavedList,
				},
				async: false,
				dataType: 'json',
				success: function(response) {
		
					$('.trait-save-list-preview-variables').html(response.htmlData);

					$('.trait-modal-loading').html('');

				}
			});
		}, 500);
	});

	// validate if all required fields are specified
	$('.form-control').bind("change keyup input",function() {
		var abbrev = $('#trait-list-abbrev').val();
		var name = $('#trait-list-name').val();
		var display_name = $('#trait-list-display_name').val();

		if(abbrev != '' && name != '' && display_name != ''){
			$('#trait-save-list-confirm-btn').removeClass('disabled');
		}else{
			$('#trait-save-list-confirm-btn').addClass('disabled');
		}
	});

	// confirm save variable list
	$(document).on('click', '#trait-save-list-confirm-btn', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		
		var confirmSaveVariableListUrl = '$confirmSaveVariableListUrl';
		var varIdsSavedList=[];

		var abbrev = $('#trait-list-abbrev').val();
		var name = $('#trait-list-name').val();
		var displayName = $('#trait-list-display_name').val();
		var description = $('#trait-list-description').val();
		var remarks = $('#trait-list-remarks').val();

		$("input:checkbox.final-grid_select").each(function(){
			if($(this).prop("checked") === true)  {
				varIdsSavedList.push($(this).attr("data-id"));
			}
		});

		$.ajax({
			type: 'POST',
			url: confirmSaveVariableListUrl,
			data: {
				id: '$id',
				idList: varIdsSavedList,
				abbrev: abbrev,
				name: name,
				displayName: displayName,
				description: description,
				remarks: remarks
			},
			dataType: 'json',
			async: false,
			success: function(response) {
				// with error
				if(response.msg != ''){
					var notif = "<i class='material-icons red-text left'>close</i> Problem in creating variable list. " + response.msg;
				}else{
					var notif = "<i class='material-icons green-text left'>done</i> Successfully saved " + name + '.';
				}
				//reload page
				$.pjax.reload({
					url: link,
					container: '#trait-list-selected-vars-grid-pjax',
					replace: false
				});
				$('#trait-save-list-modal').modal('hide');
				$('.toast').css('display','none');
				Materialize.toast(notif,5000);

			},
			error: function() {
				var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while saving the list. ";

				$('.toast').css('display','none');
				Materialize.toast(notif,5000);

				$('#trait-save-list-confirm-btn').addClass('disabled');
			}
		});
	});

	// reorder variables
	$('#trait-list-selected-vars-grid tbody').on('sortupdate',function(event,ui){

		var list = new Array();
		var reorderUrl = '$reorderUrl';
		var order = $(this).find('.order_tr').each(function(){
			var id = $(this).attr("data-varId");    
			list.push(parseInt(id));
		});

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: reorderUrl,
			data: {
				id: '$id', 
				idList: list},
			async: false,
			success: function(data) {
				var notif = "<i class='material-icons green-text left'>done</i> Successfully reordered variables.";

				$('.toast').css('display','none');
				Materialize.toast(notif,5000);
			}
		});
	});

	// remove variable
	$(document).on('click', '#trait-discard-btn', function(e) {
		
		var idList=[];

		$("input:checkbox.final-grid_select").each(function(){

			if($(this).prop("checked") === true)  {
				idList.push($(this).attr("id"));
			}

		});
		
		//check variable list
		var idListCnt = idList.length;

		if(idListCnt == 0){
			var notif = "<i class='material-icons orange-text left'>warning</i> No selected trait/s.";

			$('.toast').css('display','none');
			Materialize.toast(notif,5000);
		}else{
			$('#delete-modal').modal('show');
		}
	});

	// confirm delete traits
	$(document).on('click', '#proceed-delete-btn', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();

		var idList=[];
		var removeVariablesUrl = '$removeVariablesUrl';

		$("input:checkbox.final-grid_select").each(function(){

			if($(this).prop("checked") === true)  {
				idList.push($(this).attr("id"));
			}

		});

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: removeVariablesUrl,
			data: {
				idList: idList
			},
			async: false,
			success: function(data) {
				let notif = ''

                if (!data.success) {
                    notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while removing traits. "
                    $('.toast').css('display','none')
                    Materialize.toast(notif,5000)
                } else {
					let successfulCount = data.successfulCount
					let failedCount = data.failedCount
					let noPermissionCount = data.noPermissionCount
					let totalCount = successfulCount + failedCount + noPermissionCount

					// Toast settings
					let icon = 'close'
					let iconColor = 'red'
					let message = ''

					if (successfulCount) {
						message += `Successfully removed&nbsp;<b>\${successfulCount}</b>&nbsp;trait/s.<br>`
						icon = 'done'
						iconColor = 'green'
					}
					if (failedCount) {
						message += `Failed to remove&nbsp;<b>\${failedCount}</b>&nbsp;trait/s.<br>`
					}
					if (noPermissionCount) {
						message += `You do not have permission to remove&nbsp;<b>\${noPermissionCount}</b>&nbsp;trait/s.`
					}
            
					if (totalCount) {
						notif = `<i class='material-icons \${iconColor}-text left'>\${icon}</i> <span>\${message}</span>`;
					}

					$.pjax.reload({
						url: link,
						container: '#trait-list-selected-vars-grid-pjax',
						replace: false
					});
				}

				$('.toast').css('display','none');
				Materialize.toast(notif,5000);
				$('#delete-modal').modal('hide');
					
				return false;
			}
		});

	});

    // check if instruction is updated (on one variable)
    $(document).on('change paste', ".instructions-input-validate", function(event) {
        var id = this.id;
        var instructionsArr = (this.id).split('-');
        var listMemberId = instructionsArr[1];
        var remarks = $('#'+id).val();
        
        $('.add-replace-vars-btn').attr('disabled',true);
        
        //Save the changes in the database
        $.ajax({
            url: '$saveRemarksUrl',
            type: 'post',
            dataType: 'json',
            cache: false,
            data: {
                listMemberId: listMemberId, 
                remarks: remarks
            },
            success: function(response) {
                 $('.add-replace-vars-btn').attr('disabled',false);
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while updating instructions. ";
                $('.toast').css('display','none');
                Materialize.toast(notif,5000)
            }
        });
    });	
	
	// check if there is a remark
	$('#remarks-bulk-input').bind("keypress keyup blur paste",function() {
		var remarks = $('#remarks-bulk-input').val();
		
		if(remarks == ''){
			$('.confirm-update-btn').attr('disabled',true);
		}else{
			$('.confirm-update-btn').attr('disabled',false);
		}
	});
	
	// view buk update modal
	$(document).on('click', '#trait-update-remarks-btn', function(e) {
		$('#update-remarks-modal').modal('show');
	});

	// bulk update remarks
	$(document).on('click', '.confirm-update-btn', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();

		var obj = $(this);
		var operation = obj.data('operation');
		var remarks = $('#remarks-bulk-input').val();

		var listMemberIdList=[];

		$("input:checkbox.final-grid_select").each(function(){

			if($(this).prop("checked") === true)  {
				listMemberIdList.push($(this).attr("id"));
			}

		});

		//check variable list
		var listMemberIdListCnt = listMemberIdList.length;

		if(listMemberIdListCnt == 0 && operation !== 'update-all'){

			var notif = "<i class='material-icons orange-text left'>warning</i> No selected variable(s).";

			$('.toast').css('display','none');
			Materialize.toast(notif,5000);
		}else{
			listMemberIdList = (operation == 'update-all') ? null : listMemberIdList;
			$.ajax({
				url: '$saveBulkRemarksUrl',
				type: 'post',
				dataType: 'json',
				data: {
					listId: '$listId',
					listMemberId: listMemberIdList,
					remarks: remarks,
					operation: operation,
				},
				success: function(response) {
					
                                if(!response.success){
                                    var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while updating instructions. ";
                                } else {
                                    var notif = response.message;
                                    //reload page
						$.pjax.reload({
							url: link,
							container: '#trait-list-selected-vars-grid-pjax',
							replace: false
						});
					}

					$('#update-remarks-modal').modal('hide');
					$('.toast').css('display','none');
					Materialize.toast(notif,5000);

				},
				error: function() {
					var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while updating instructions. ";

					$('.toast').css('display','none');
					Materialize.toast(notif,5000);
				}
			});
		}
	});

JS);
?>

<style type="text/css">
    .hide-caret .caret,
    .hide-caret .dropdown-menu {
        display: none;
    }

    .tabs-left .tab-content {
        border-left: none;
    }

    .trait-div {
        margin-bottom: 10px;
    }

    .kv-panel-after {
        display: none;
    }
</style>