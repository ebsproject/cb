<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders trait list display
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\widgets\VariableInfo;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use app\models\PlatformList;
use marekpetras\yii2ajaxboxwidget\Box;

\yii\jui\JuiAsset::register($this);


// variables columns
$columns = [
	[
		'label'=> "checkbox",
		'header'=>'
			<input type="checkbox" class="filled-in" id="trait-list-variables-grid-select-all-id" />
			<label for="trait-list-variables-grid-select-all-id"></label>
		',
        'contentOptions' => ['style' => ['max-width' => '100px;',]],
        'content'=>function($data) {
            return '
                <input class="final-grid_select filled-in" type="checkbox" id="'.$data['listMemberDbId'].'" data-id ="'.$data['variableDbId'].'" />
                <label for="'.$data['listMemberDbId'].'"></label>
            ';
        },
        'hiddenFromExport'=>true,
        'mergeHeader'=>true,
        'width'=>'50px'
	],
    [
		'attribute'=>'label',
		'format'=>'raw',
        'vAlign'=>'middle',
    	'value'=> function($data){
			// render view variable info widget
    		$varInfo = VariableInfo::widget([
    			'id' => $data['variableDbId']
            ]);

			// return $varInfo;
			return Html::a($data['label'],
				['#'], 
				[
					'id' => $data['variableDbId'],
					'label' => $data['label'],
					'data-toggle' => 'modal',
					'data-target'=> '#view-variable-modal',
					'class' => 'view-variable'
				]
			);
    	},
    	'contentOptions' => function($data) {
            return ['class' => 'order_tr', "data-varId"=>$data['variableDbId']];
        }
    ],
    [
		'attribute'=>'displayName',
		'label'=>'Name',
		'format'=>'raw',
        'vAlign'=>'middle',
    ],
    [
		'attribute'=>'remarks',
		'label'=> 'Instructions',
		'format' => 'raw',
        'vAlign'=>'middle',
        'value' => function($data){
            $value = '';
            $value = Html::input('text', 'instructions-input', $data['remarks'], ['id'=>'instructions-'.$data['listMemberDbId'],'class'=>'instructions-input-validate','data-name'=>'remarks']);
            return $value;
		},
		'width'=>'300px'
    ],
];

$actionUpdateRemarks = Html::a('<i class="glyphicon glyphicon-edit"></i>', '#!', [ 'class' => 'btn trait-list-actions', 'id' => 'trait-update-remarks-btn', 'title'=>\Yii::t('app',' Bulk Update')]);
$actionSaveList = Html::a('<i class="material-icons widget-icons">add_shopping_cart</i>', '#!', [ 'class' => 'btn light-green darken-3 waves-light waves-effect trait-list-actions ', 'id' => 'trait-save-new-list-btn', 'title'=>\Yii::t('app',' Save as a new Trait Protocol'), 'disabled'=>true]); // TODO: To be refactored
$actionDeleteList = Html::a('<i class="material-icons widget-icons">delete</i>', '#!', [ 'class' => 'btn trait-list-actions red darken-3', 'id' => 'trait-discard-btn', 'title'=>\Yii::t('app',' Remove item(s) from the list')]);

$panel = [
	'heading' => '<i class="material-icons left">folder_special</i> '.\Yii::t('app', 'Selected Traits'),
	// 'before' => \Yii::t('app', 'You may drag the row across the table to reorder the traits.'), // TODO: To be refactored
    'footer' => false
];

// variables browser
DynaGrid::begin([
	'options'=>['id'=>'trait-list-selected-vars-grid'],
	'columns' => $columns,
	'theme'=>'simple-default',
	'showPersonalize'=>true,
	'storage' => 'cookie',
	'showFilter' => false,
	'showSort' => false,
	'allowFilterSetting' => false,
	'allowSortSetting' => false,
	'gridOptions'=>[
		'id' => 'trait-list-selected-vars-grid',
		'dataProvider'=>$dataProvider,
		'pjax'=>true,
		'pjaxSettings' => [
			'options' => [
				'enablePushState' => false,
				'id' => 'trait-list-selected-vars-grid'
			],
		],
		'panel'=> $panel,
		'responsiveWrap'=>false,
		'toolbar' =>  [
			'content' => "$actionUpdateRemarks $actionSaveList $actionDeleteList",
		],
		'floatHeader' =>true,
		'floatOverflowContainer'=> true
	]
]);
DynaGrid::end();

//Modal for bulk updating instructions
Modal::begin([
    'id'=> 'update-remarks-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i> '.\Yii::t('app','Bulk Update').'</h4>',
    'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="bulk-update-selected" data-operation="update-selected" class="btn btn-primary green waves-effect waves-light confirm-update-btn" disabled>'.\Yii::t('app','Selected').'</a>&nbsp;'.
        '<a id="bulk-update-all" data-operation="update-all" class="btn btn-primary teal waves-effect waves-light confirm-update-btn" disabled>'.\Yii::t('app','All').'</a>',
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static'],
]);

echo "<label for = 'remarks-bulk-input'>".\Yii::t('app','Trait Instructions')."</label>";
echo Html::input('text', 'remarks-bulk-input','', ['id'=>'remarks-bulk-input', 'placeholder'=>'Add instructions here']);

Modal::end();

// Modal for replacing traits
Modal::begin([
	'id' => 'replace-all-modal',
	'header' => '<h4><i class="material-icons left">swap_horiz</i> '. \Yii::t('app','Replace All Traits').'</h4>',
	'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',['id' => 'back-replace-btn','data-dismiss'=>'modal']).'&nbsp;&emsp;'.Html::a(\Yii::t('app','Confirm'),'#',['id' => 'proceed-replace-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
	'size' =>'modal-md',
	'options' => ['data-backdrop'=>'static']
	]);
	echo \Yii::t('app','<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You have chosen to replace the selected traits with <span id="trait-name"></span>. Please note that this will replace <b>ALL</b> the current traits in this experiment. Click the <b>Confirm</b> button to proceed with the operation. Otherwise, click the <b>Cancel</b> button.');
Modal::end();

// Modal for deleting traits
Modal::begin([
	'id' => 'delete-modal',
	'header' => '<h4><i class="material-icons orange-text left">warning</i>'. \Yii::t('app','Remove Traits').'</h4>',				
	'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',['id' => 'back-delete-btn','data-dismiss'=>'modal']).'&nbsp;&emsp;'.Html::a(\Yii::t('app','Confirm'),'#',['id' => 'proceed-delete-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
	'size' =>'modal-md',
	'options' => ['data-backdrop'=>'static']
	]);
	echo \Yii::t('app','Selected trait/s will be deleted from your experiment. This action cannot be undone.');
Modal::end();


// Modal for trait list preview
Modal::begin([
	'id' => 'list-preview-modal',
	'header' => '<h4 id="list-preview-header"></h4>',				
	'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',['id' => 'back-preview-btn','data-dismiss'=>'modal']).'&nbsp;&emsp;'.Html::a(\Yii::t('app','Confirm'),'#',['id' => 'ok-preview-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
	'size' => 'modal-lg',
	'options' => ['data-backdrop'=>'static']
]);
echo '<p id="list-preview-modal-note">'.\Yii::t('app','Below is the list of traits that will be added in this experiment. Click "Confirm" to proceed.').'</p>';
echo '<p id="list-preview-modal-body">';
Modal::end();

// modal for saving as new list
Modal::begin([
	'id' => 'trait-save-list-modal',
	'header' => '<h4><i class="material-icons" style="vertical-align:bottom">add_shopping_cart</i> Add as a new Trait Protocol</h4>',
	'footer' => Html::a('Cancel', ['#'], ['id' => 'cancel-save', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'
		.Html::a('Submit' . '<i class="material-icons right">send</i>',
			'#',
			[
				'class' => 'btn btn-primary waves-effect waves-light disabled',
				'url' => '#',
				'id' => 'trait-save-list-confirm-btn'
			]
		) . '&emsp;',
	'size' => 'modal-lg',
	'options' => ['data-backdrop' => 'static']
]);


echo '<p id="trait-save-list-preview-error"></p>';

$form = ActiveForm::begin([
    'enableClientValidation'=>false,
    'id'=>'create-saved-list-form',
    'action'=>['/experimentCreation/protocol/traits/_traits'],
    'fieldConfig' => function($model,$attribute){
        if(in_array($attribute, ['abbrev','display_name','name','remarks'])){
            return ['options' => ['class' => 'input-field form-group col-md-6']];
        }else{
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    }
]); 

$model = \Yii::$container->get('\app\models\PlatformList');
echo 
$form->field($model, 'name')->textInput([
    'maxlength' => true, 
    'id'=>'trait-list-name',
	'title'=>'Name identifier of the list',
	'onkeyup' => 'js:
		var nameValue = this.value;
		$("#trait-list-display_name").val(nameValue).trigger("change");
		$("#trait-list-abbrev").val(nameValue.replace(/[^A-Z0-9]/ig, "_").toUpperCase()).trigger("change");
	'
]).  
$form->field($model, 'abbrev')->textInput([
    'maxlength' => true, 
    'id'=>'trait-list-abbrev',
    'title'=>'Short name identifier or abbreviation of the list',
    'oninput' => 'js: $("#trait-list-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());'
]).  
$form->field($model, 'description')->textarea([ 'class'=>'materialize-textarea', 'title'=>'Description about the list', 'id'=>'trait-list-description']).
$form->field($model, 'display_name')->textInput(['id'=>'trait-list-display-name', 'maxlength' => true, 'title'=>'Name of the list to show to users', 'id'=>'trait-list-display_name',]).
$form->field($model, 'remarks')->textarea(['class'=>'materialize-textarea', 'title'=>'Additional details about the list','row'=>2, 'id'=>'trait-list-remarks']); 

ActiveForm::end();

echo '<div class="trait-modal-loading"></div>';
echo '<div class="trait-save-list-preview-variables"></div>';

Modal::end();

// view variable modal
Modal::begin([
    'id' => 'view-variable-modal',
    'header' => '<h4 id="entity-label"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);
echo '<div class="view-variable-modal-body"></div>';
Modal::end();

$variableUrl = Url::to(['/variable/default/view-info']);
$this->registerJs(<<<JS
// view variable info
$(document).on('click', '.view-variable', function(e) {
    var obj = $(this);
    var varId = obj.attr('id');
	var varLabel = obj.attr('label');

	$('#entity-label').html('<i class="fa fa-info-circle"></i> ' + varLabel + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('.view-variable-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
	//Load con tent of view variable information
	setTimeout(function(){
		$.ajax({
			url: '$variableUrl',
			data: {
				id: varId
			},
			type: 'POST',
			async: true,
			success: function(data){
				$('.view-variable-modal').modal('show');
				$('.view-variable-modal-body').html(data);
			},
			error: function(){
				$('.view-variable-modal-body').html('<i>There was a problem while loading record information.</i>');
			}
		});
	},300);  
});
JS
);
?>
