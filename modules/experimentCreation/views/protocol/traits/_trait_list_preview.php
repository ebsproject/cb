<?php

/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* Preview list of trait variables
**/

use kartik\grid\GridView;


// columns

$columns = [
	['class' => 'yii\grid\SerialColumn'],
    [
        'attribute'=>'label',
    	'enableSorting' => false
    ],
    [
        'attribute'=>'displayName',
		'label' => 'Name',
        'enableSorting' => false
    ],
    [
        'attribute'=>'remarks',
        'label'=> 'Instructions',
        'enableSorting' => false
    ]
];

echo $grid = GridView::widget([
	'pjax' => true,
	'dataProvider' => $dataProvider,
	'id' => 'trait-preview-list-grid',
	'columns' => $columns,
	'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
]);

?>

<style type="text/css">
.table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #fff;
}
</style>
