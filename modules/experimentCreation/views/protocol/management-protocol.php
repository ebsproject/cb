<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders management protocol box view
*/

use marekpetras\yii2ajaxboxwidget\Box;

$isViewOnly = $isViewOnly ?? '';
$previewOnly = $previewOnly ?? '';
$hasMultipleSelectedOccurrences = $hasMultipleSelectedOccurrences ?? '';
$occurrenceDbIdsString = $occurrenceDbIdsString ?? '';
$shouldUpdateOccurrenceProtocols = $shouldUpdateOccurrenceProtocols ?? '';
$occurrenceDbId = $occurrenceDbId ?? '';

$helperText = ($isViewOnly === '1' || $previewOnly === '1' ) ? '' : \Yii::t('app','Specify management attributes to be used. You may add by selecting from the list of management variables or existing management variable lists. <b> Changes are auto-saved</b>.');
?>

<p style="margin-bottom:5px"><?= \Yii::t('app', $helperText)?></p>
<div id = "management-protocol-div" class = "row col col-sm-12 col-md-12 col-lg-12">	
	<?php
		$toolsButtons = [
			'reload' => function(){
				return 
					'&nbsp;&nbsp;<a href="#!" title="'.\Yii::t('app','Refresh data').'" class="text-muted fixed-color" onclick="$(&quot;#pr-management-protocols-box&quot;).box(&quot;reload&quot;);"><i class="material-icons widget-icons">loop</i></a>';
			}
		];
		
		$options = [
			'id' => 'pr-management-protocols-box',
			'bodyLoad' => ['/experimentCreation/protocol/management-protocol?program=' . $program . '&id=' . $id . '&processId=' . $processId . '&isViewOnly=' . $isViewOnly. '&previewOnly=' . $previewOnly . '&occurrenceDbId=' . $occurrenceDbId . '&shouldUpdateOccurrenceProtocols=' . $shouldUpdateOccurrenceProtocols . '&occurrenceDbIdsString=' . $occurrenceDbIdsString . '&hasMultipleSelectedOccurrences=' . $hasMultipleSelectedOccurrences],
			'toolsTemplate' => '{reload}',
			'toolsButtons' => $toolsButtons
		]
	?>
	<?= Box::widget($options); ?>
</div>

<style>
#pr-management-protocols-box, #management-protocol-div{
	background-color: #f1f4f5;
}
.box-body, #management-protocol-div{
	padding: 0px;
} 
.trait-div{
	padding: 0px 10px 0px 0px!important;
}
.box{
	margin: 0px;
}
.panel-title{
	margin-top:10px;
}
.panel-heading .summary {
    margin-top: 0px;
}
</style>