<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders display of adding management Attributes
 */

use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;

\yii\jui\JuiAsset::register($this);

$hasMultipleSelectedOccurrences = $hasMultipleSelectedOccurrences ?? '';
$occurrenceDbIdsString = $occurrenceDbIdsString ?? '';
$shouldUpdateOccurrenceProtocols = $shouldUpdateOccurrenceProtocols ?? '';
$occurrenceDbId = $occurrenceDbId ?? '';

echo '<div class="management-var-panel">';

echo '<label class="control-label" >'.\Yii::t('app','Management Attribute') .'</label>';

echo Select2::widget([
    'data' => $mgtVariableTags,
    'name' => 'management-select-variable',
    'id' => 'management-select-variable',
    'maintainOrder' => true,
    'options' => [
      'placeholder' => \Yii::t('app','Select a management variable'),
      'tags' => true
    ]
]);
echo '</div>';
// Modal for replacing management variables
Modal::begin([
  'id' => 'mgt-replace-all-modal',
  'header' => '<h4><i class="material-icons left">swap_horiz</i> '. \Yii::t('app','Replace All Management Variables').'</h4>',
  'footer' =>	Html::a(\Yii::t('app','Cancel'),'#',['id' => 'mgt-back-replace-btn','data-dismiss'=>'modal']).'&nbsp;&emsp;'.Html::a(\Yii::t('app','Confirm'). ' <i class="material-icons right">send</i>','#',['id' => 'mgt-proceed-replace-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
  'size' =>'modal-md',
  'options' => ['data-backdrop'=>'static']
  ]);
  echo \Yii::t('app','<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You have chosen to replace the selected management variables with <span id="management-name"></span>. Please note that this will replace <b>ALL</b> the current management variables in this experiment. Click the <b>Confirm</b> button to proceed with the operation. Otherwise, click the <b>Cancel</b> button.');
Modal::end();
?>

<div style="margin-top:10px">
  <button class="btn waves-effect waves-light mgt-add-replace-vars-btn" title="<?= \Yii::t('app','Add variables') ?>" id="management-select-variable-add-btn" data-operation="added" disabled style="margin: 0px 7px 0px 0px;"><?= \Yii::t('app','Add') ?></button>

  <button class="btn waves-effect waves-light grey lighten-3 grey-text text-darken-2 mgt-add-replace-vars-btn" id="management-select-variable-replace-btn" data-operation="replaced" disabled style="margin: 0px 10px 0px 0px;" title="<?= \Yii::t('app','Replace all') ?>"><i class="material-icons">swap_horiz</i></button>

  <div class="hidden preloader-wrapper small active" style="width:20px;height:20px;">
      <div class="spinner-layer spinner-green-only">
          <div class="circle-clipper left">
              <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
              <div class="circle"></div>
          </div>
      </div>
  </div>
</div>

<?php
//routes
$addVariablesUrl = Url::to(['/experimentCreation/protocol/add-variables']);
$confirmAddVarListUrl = Url::to(['/experimentCreation/protocol/add-mgt-var-list']);

$this->registerJs(<<<JS

     var link = '/index.php/experimentCreation/protocol/management-protocol?program=' + "$program" + '&id='+"$id"+'&processId=' + "$processId" +'&shouldUpdateOccurrenceProtocols='+"$shouldUpdateOccurrenceProtocols" + '&occurrenceDbIdsString='+"$occurrenceDbIdsString";

    // check if there is selected list
    $('#management-select-variable').bind("change keyup input",function() {
        var selectedList = $('#management-select-variable').val();
        
        if(selectedList == ''){
            $('.mgt-add-replace-vars-btn').attr('disabled',true);
        }else{
            $('.mgt-add-replace-vars-btn').attr('disabled',false);
        }
    });

    // add/replace variables upon click
    $('.mgt-add-replace-vars-btn').on('click',function(){
        var obj = $(this);
        var operation = obj.data('operation');

        if(operation == 'replaced'){
            let managementLabel = $('#select2-management-select-variable-container').attr('title')
            $('#management-name').html('<b>' + managementLabel + '</b>');
      
            $('#mgt-replace-all-modal').modal('show');
        }
        else{
            var selectedVar = $('#management-select-variable').val();

            $('.preloader-wrapper').removeClass('hidden');

            $.ajax({
                url:'$addVariablesUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    listId: '$listId',
                    id: selectedVar,
                    type: 'variable',
                    operation: operation,
                    protocolType: 'management',
                    experimentId: '$id'
                },
                success: function(response) {
                    $('.toast').css('display','none');
                    Materialize.toast(response,5000);

                    $('.preloader-wrapper').addClass('hidden');

                    if(response != null && response != undefined && response.includes('Success')){
                        $.pjax.reload({
                            url: link,
                            container: '#management-list-selected-vars-grid-pjax',
                            replace: false
                        });
                    }
                },
                error: function() {
                    var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";

                    $('.toast').css('display','none');
                    Materialize.toast(notif,5000);
                }
            });
        }
    }); 

    //add/replace management via list	
    $('.add-replace-list-btn').on('click', function(){
        var obj = $(this);
        var operation = obj.data('operation');
        var selectedList = $('#management-select-group').val();
        var list = $('#select2-management-select-group-container').attr('title');
        var header = '<i class="material-icons left">add</i> Add management variables from ' + list;

        $('.preloader-wrapper').removeClass('hidden');
        
        $.ajax({
            url: '$confirmAddVarListUrl',
            type: 'post',
            dataType: 'json',
            data: {
                listId: selectedList,
            },
            success: function(response) {

                $('.preloader-wrapper').addClass('hidden');
                let note = ''
                
                if(operation == 'replaced'){
                    header = '<i class="material-icons left">swap_horiz</i> Replace All Management';
                    note = '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You have chosen to replace the selected management with <b>' + list + '</b>. Please note that this will replace <b>ALL</b> current management in this experiment. Click the <b>Confirm</b> button to proceed with the operation. Otherwise, click the <b>Cancel</b> button.'
                }
                $('#list-preview-mgt-header').html(header);
                $('#list-preview-mgt-modal-note').html(note);
                $('#list-preview-mgt-modal-body').html(response.htmlData);
                $('#list-preview-mgt-modal').modal('show');
                
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while displaying preview of variables. ";

                $('.toast').css('display','none');
                Materialize.toast(notif,5000);
            }
        });
    });


    //proceed to replacing all management
    $('#mgt-proceed-replace-btn').on('click',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        
        var selectedVar = $('#management-select-variable').val();

        $('.preloader-wrapper').removeClass('hidden');

        $.ajax({
            url:'$addVariablesUrl',
            type: 'post',
            dataType: 'json',
            data: {
                listId: '$listId',
                id: selectedVar,
                type: 'variable',
                operation: 'replaced',
                protocolType: 'management',
                experimentId: '$id'
            },
            success: function(response) {

                $('.toast').css('display','none');
                Materialize.toast(response,5000);

                $('.preloader-wrapper').addClass('hidden');
                if(response.includes('Success')){
                    $.pjax.reload({
                        url: link,
                        container: '#management-list-selected-vars-grid-pjax',
                        replace: false,
                    });
                }

                $('#mgt-replace-all-modal').modal('hide');

            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";

                $('.toast').css('display','none');
                Materialize.toast(notif,5000);
            }
        });
    });

JS);
?>