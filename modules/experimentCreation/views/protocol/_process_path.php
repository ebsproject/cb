<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders trait protocols view
*/
use kartik\select2\Select2;
use yii\helpers\Url;

?>


<div class="col col-md-6" style="padding:10px">
	<?php

	$disabled = (empty($defaultValue) || (!empty($defaultValue) && $saveDisabled)) ? 'disabled' : '';

	echo '<label class="control-label" >'.\Yii::t('app','Process Path') .'</label>';

	echo Select2::widget([
		'data' => $processPathTags,
		'name' => 'process-path-select',
		'id' => 'process-path-select',
		'value' => $defaultValue,
		'maintainOrder' => true,
		'options' => [
			'placeholder' => \Yii::t('app','Select process path'),
			'tags' => true
		]
	]);
	?>

	<div style="margin-top:10px">
		<button class="right btn waves-effect waves-light save-process-path-btn" title="<?= \Yii::t('app','Save process path') ?>" id="process-path-save-btn" <?=$disabled?>><i class="material-icons right tabular-icon">send</i><?= \Yii::t('app','Save') ?></button>
		<div class="hidden preloader-wrapper small active" style="width:20px;height:20px;">
			<div class="spinner-layer spinner-green-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
						</div><div class="gap-patch">
							<div class="circle"></div>
						</div><div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php

$saveProcessPathUrl = Url::to(['/experimentCreation/protocol/save-process-path']);

$this->registerJs(<<<JS
	// check if there is selected process path
	$('#process-path-select').bind("change keyup input",function() {
		var selectedPath = $('#process-path-select').val();
		
		if(selectedPath == ''){
			$('#process-path-save-btn').attr('disabled',true);
		}else{
			$('#process-path-save-btn').attr('disabled',false);
		}
	});

	//save process path value
	$('#process-path-save-btn').on('click', function(){
		var obj = $(this);
		var operation = obj.data('operation');
		var selectedPath = $('#process-path-select').val();

		$('.preloader-wrapper').removeClass('hidden');
		$('.toast').css('display','none');

		$.ajax({
			url:'$saveProcessPathUrl',
			type: 'post',
			dataType: 'json',
			data: {
				id: '$id',
				processPathId: selectedPath
			},
			success: function(response) {
				if(response){
					var notif = "<i class='material-icons green-text left'>done</i> " + response;
					Materialize.toast(notif,5000);
					$('.preloader-wrapper').addClass('hidden');
				}
			},
			error: function() {
				var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while adding variables. ";
				Materialize.toast(notif,5000);
			}
		});
	});  
JS
);
?>