<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting protocol's browser
*/
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\GenericFormWidget;
?>

    <div class="col col-sm-12 view-entity-content">
       <div id="content-body">
           <div id="default-content" class="col col-sm-12 col-md-12 col-lg-12">
                <?php
                    $form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'id' => 'planting-protocol-form',
                        'action' => ['/experimentCreation/protocol/planting-protocols','id'=>$id,'program'=>$program,'processId'=>$processId]
                    ]);
                ?>
                    <div class="parent-box"  id="filter-panel">
                        <?php
                            if(isset($configValues)){
                                echo GenericFormWidget::widget([
                                    'options' => $defaultWidgetOptions
                                ]);
                            
                            }
                        else{
                            echo '<p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'No plot type config found.').'</span></p>';
                        }
                        ?>
                        <div class="col col-md-12">
                        <div class="col col-md-4"></div>
                    
                        </div>
                    </div>
                    <div id="dynamic-fields">
                        <div id="plot-type-fields"></div>
                    </div>
                    <div class="clearfix"></div>
           </div>
        </div>
    </div>
    <div 
        id="planting-protocols-lower-form-input-div"
        class="pull-right"
    >
        <?=($shouldUpdateOccurrenceProtocols) ? Html::a(
            'Cancel',
            Url::toRoute('/occurrence?program=' . $program),
            [
                'id' => 'cancel-bulk-update-occurrences-planting-protocols',
                'title' => \Yii::t('app', 'Cancel update'),
                'style' => [
                    'vertical-align' => '-12px',
                    'margin-right' => '12px',
                ],
            ]
        ) : null?>
        <?=Html::button(\Yii::t('app','Save'),[
            'class' => 'waves-effect waves-light btn btn-primary',
            'style' => 'margin-right:20px;margin-top:15px;margin-bottom:10px;' .
                $cssDisplaySaveButton, // This variable came from /experimentCreation/controllers/ProtocolController
            'id' => 'save-planting-btn',
            'disabled' => true,
            'title' => Yii::t('app', 'Save Planting Protocols'),
        ])?>
    </div>
<?php ActiveForm::end(); ?>
</div> <!--End of View entity content class-->

<?php

    Modal::begin([
        'id' => 'generic-modal',
    ]);
    Modal::end();

    // Modal for validation
    Modal::begin([
        'id' => 'required-field-modal',
        'header' => '<h4><i class="fa fa-bell"></i>  '.\Yii::t('app','Notification').'</h4>',
        'footer' => Html::a(\Yii::t('app','OK'),['#'],['id'=>'cancel-save-btn','class'=>'btn btn-primary waves-effect waves-light','data-dismiss'=>'modal']),
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static']
    ]);
    echo '<div class="row">
        <p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'Please correct the errors below before proceeding.').'</span></p>
        </div><div class="row" id="required-field-notif"></div>';
    Modal::end();
?>

<?php
//TODO: Remove unused URLs once refactoring is done
//   $filterUrl = Url::to(['protocol/get-tags','id'=>$id]);
    $entityDbId = $id;
    if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
        $entityDbId = $occurrenceDbId;
    }
  $loadPlotTypeFieldsUrl = Url::to(['protocol/load-plot-type-fields', 'id'=>$entityDbId,'program'=>$program]);
  $loadVarValueUrl = Url::to(['protocol/load-variable-value','id'=>$entityDbId]);
  $checkPlantingProtocolUrl = Url::to(['protocol/check-protocol','id'=>$id]);
  $defaultFieldValues = json_encode($defaultFieldArr);
  $protocolChildren = json_encode($protocolChildren);
//   $defaultVars = json_encode($defaultVariables);

  $reqFieldJson = json_encode($configValues);
  $updateFilterUrl = Url::to(['protocol/update-filter-tags','id'=>$id]);


  if(!empty($configValues)){
    $genericRenderConfigUrl = Url::to(['create/render-config-form', 'id'=>$id, 'program'=>$program]);
    
    Yii::$app->view->registerJs("
    var genericConfUpdateUrl = '".Url::toRoute(['/experimentCreation/create/update-config-variable','id'=>$id, 'program'=>$program])."',
    genericRenderConfigUrl = '".$genericRenderConfigUrl."',
    experimentId = '".$id."',
    requiredFieldJson = '".$reqFieldJson."'
    ;",
    \yii\web\View::POS_HEAD);

    $this->registerJsFile("@web/js/generic-form-validation.js", [
        'depends' => ['app\assets\AppAsset'],
        'position'=>\yii\web\View::POS_END
    ]);
  }


  $this->registerJs(<<<JS
    var submitFlag = 0;
    var select2Id = '';
    var establishmentVal = '';
    var plantingTypeVal = '';
    var plotTypeVal = '';
    var variableAbbrev = '';
    var tempDefault = '';
    var flag = false;  
    var defaultFieldValues = JSON.parse('$reqFieldJson');
    var establishmentId = '';
    var plantingTypeId = '';
    var plotTypeId = '';
    
 
    $("document").ready(function(){
        if (!'$hasMultipleSelectedOccurrences') {
            loadDefaultValues();
        }
        $('.box-1').removeClass("col-md-6 s6 box-1 parent-box");
        $('.box-1').addClass("col-md-12 s12 box-1 parent-box");
        //Check if all required fields have been populated or is saved in the database
        $(document).on('change','.select2-input-validate', function(e, params){
            e.preventDefault();
            var memtype = $(this).attr('data-mem');
            var selectedId = $(this).val();
            memtype = memtype.toLowerCase();
            if(memtype == 'establishment'){
                $('#plot-type-fields').html('');

                updateFilterTags(e,memtype);
                updateFilterTags(e,'planting_type');

                // if crop establishment is "not applicable", enable the save button
                var saveButtonState = (selectedId.toLowerCase().trim() == 'not applicable') ? false : true;
                $('#save-planting-btn').attr('disabled', saveButtonState);

            }else if(memtype == 'planting_type'){
                $('#plot-type-fields').html('');
                $('#save-planting-btn').attr('disabled', true);
                updateFilterTags(e,memtype);
                updateFilterTags(e,'plot_type');
            }else if(memtype == 'plot_type'){
                //reset the other panels
                updateFilterTags(e,memtype);
                $('#plot-type-fields').html('');
                $('#save-planting-btn').attr('disabled', true);
                if(selectedId !== '' && selectedId !== null){
                    loadPlotTypeFields(selectedId,memtype);
                }else{
                    $('select2-plot_type').trigger('change');
                    updateFilterTags(e,memtype);
                }
            }
        })
    });

    //disable plot type by default
    $('#filter-panel').addClass('col col-sm-6 col-md-6 col-lg-6 parent-box');
    
    function updateFilterTags(e,memtype, action=null, estab =null,plantTypeKey =null, plotTypeKeyTemp = null){
            var fieldId = $('#select2-'+memtype);
            var dataMemtype = memtype
            var inputValue = $('#select2-'+memtype).val();
                    
            $.each($('.select2-input-validate'), function(i,v){
                var procFieldId = $(v).attr('id');
                if(procFieldId.includes('ESTABLISHMENT')){
                   establishmentId = procFieldId;
                }

                if(procFieldId.includes('PLANTING_TYPE')){
                   plantingTypeId = procFieldId;
                }

                if(procFieldId.includes('PLOT_TYPE')){
                   plotTypeId = procFieldId;
                }
            });
            var establishment = $('#'+establishmentId).val();
            var plantingType = $('#'+plantingTypeId).val();
            var plotType = $('#'+plotTypeId).val();
         
            if(estab !== null){
                establishment = estab;
            }
             
            if(plantTypeKey !== null && action !== null){
                plantingType = plantTypeKey;
            }

            if(plotTypeKeyTemp !== null && action !== null){
                plotType = plotTypeKeyTemp;
            }
            
            $.ajax({
                url: '$updateFilterUrl',
                type: 'post',
                dataType: 'json',
                async: true,
                data: {
                   establishment: establishment,
                   plantingType: plantingType,
                   plotType: plotType,
                   memType: dataMemtype
                },
                success: function(response){
                    var establishment_1 = establishmentId; 
                    var plantingType_1 = plantingTypeId; 
                    var plotType_1 = plotTypeId; 
                    
                    if(dataMemtype == 'establishment'){
                        $('#select2-planting_type').find('option').remove();
                        $.each(response, function (key, value) {
                            var newOption = document.createElement("option");
                            newOption.value = key;
                            newOption.innerHTML = value;
                        });
                        if(action==null){
                            $('#'+plantingType_1).val('').trigger('change');
                            $('#'+plotType_1).trigger('change');    
                            $('#plot-type-fields').html('');
                        }else if (action=='load'){
                            $('#'+establishment_1).val(estab).trigger('change');
                            $('#'+plantingType_1).val(plantTypeKey).trigger('change');
                            $('#'+plantingType_1).trigger('change');
                        }

                        // if crop establishment is "not applicable", enable the save button
                        var saveButtonState = (establishment.toLowerCase().trim() == 'not applicable') ? false : true;
                        $('#save-planting-btn').attr('disabled', saveButtonState);

                    }else if(dataMemtype == 'planting_type'){
                        
                        $.each(response, function (key, value) { 
                            var newOption = document.createElement("option");
                            newOption.value = key;
                            newOption.innerHTML = value;
                        });
                      
                        if(action==null){
                            
                            $('#'+plotType_1).val('');
                            $('#'+plotType_1).trigger('change');
                            $('#plot-type-fields').html('');
                            $('.div-protocol-save').css('display','none');
                            
                            // if crop establishment is "not applicable", enable the save button
                            var saveButtonState = (establishment.toLowerCase().trim() == 'not applicable') ? false : true;
                            $('#save-planting-btn').attr('disabled', saveButtonState);

                        }else if (action=='load'){
                            
                            $('#'+plantingType_1).trigger('change');
                            $('#'+plotType_1).val(plotTypeKeyTemp).trigger('change');
                            $('#plot-type-fields').html('');
                            $('#'+plotType_1).trigger('change');
                        }
                    }else if(dataMemtype == 'plot_type'){

                        if(plotType !== null && plotType !== ""){

                            loadPlotTypeFields(plotType,dataMemtype);
                        }else{

                            // update plot type options based on selected crop establishment from config
                            $('#'+plotType_1).find('option').remove();

                            $.each(response, function (key, value) { 
                                var newOption = document.createElement("option");
                                newOption.value = value;
                                newOption.innerHTML = value;
                                document.getElementById(plotType_1).options.add(newOption);
                            });

                            $('#'+plotType_1).val('');

                            if(action == null){
                                $('#select2-plot_type').val('');
                                $('#select2-plot_type').trigger('change');
                                $('#plot-type-fields').html('');
                                $('.div-protocol-save').css('display','none');
                                
                                // if crop establishment is "not applicable", enable the save button
                                var saveButtonState = (establishment.toLowerCase().trim() == 'not applicable') ? false : true;
                                $('#save-planting-btn').attr('disabled', saveButtonState);

                            }else if(action == 'load'){
                                $('#select2-plot_type').val(plotTypeKeyTemp).trigger('change');
                                $('#select2-plot_type').trigger('change');
                            }
                        }
                        
                    }
                },
                error: function(){
                }

            });
        }

    function loadPlotTypeFields(plotType, memtype){  

        $.ajax({
            url: '$loadPlotTypeFieldsUrl',
            type: 'post',
            data: {
                selected: plotType,
                memtype: memtype,
                isViewOnly: '$isViewOnly',
                shouldUpdateOccurrenceProtocols: '$shouldUpdateOccurrenceProtocols',
                hasMultipleSelectedOccurrences: '$hasMultipleSelectedOccurrences',
                previewOnly: '$previewOnly'
            },
            success: function(response){
                $('#plot-type-fields').html('');
                $('#plot-type-fields').html(response);

                $('#filter-panel').removeClass('col col-sm-6 col-md-6 col-lg-6 parent-box');
                $('#dynamic-fields').removeClass('col col-sm-8 col-md-8 col-lg-8 parent-box');
                
                $('#filter-panel').addClass('col col-sm-4 col-md-4 col-lg-4 parent-box');
                $('#dynamic-fields').addClass('col col-sm-8 col-md-8 col-lg-8 parent-box');
        
                $('#save-planting-btn').attr('disabled', false);
                
                if(response != "" || response != null){
                   $('.div-protocol-save').css('display','block');
                }
               
            },
            error: function(){
                
            }
        });
    }  

    $(document).on('click','#save-planting-btn', function(){
        var zeroFlag = false;
        $('input[type="number"]').each(function(){
            var checkVal = $(this).val();
            if(checkVal == 0){
                zeroFlag = true;
            }
        })
     
        var response = checkFields2();  
        var form_data = $.param($.map($("#planting-protocol-form").serializeArray(), function(v,i) {
            
            return (v.name == "add_variable_info_select") ? null : v;
        }));
        var nextUrl = $(this).attr('data-url');
        form_data = form_data + '&' + $.param({
                        ['nextUrl'] : nextUrl
                    });
        form_data =  form_data + '&' + $.param({
                        ['processId'] : '$processId'
                    });

        if ('$shouldUpdateOccurrenceProtocols') {
            form_data =  form_data + '&' + $.param({
                ['shouldUpdateOccurrenceProtocols'] : '$shouldUpdateOccurrenceProtocols'
            });

            form_data =  form_data + '&' + $.param({
                ['occurrenceDbIdsString'] : '$occurrenceDbIdsString'
            });

            let notif = "<i class='material-icons blue-text'>info</i> <span class='white-text'> Please wait for the update to complete.</span>"; 
            Materialize.toast(notif, 5000);
        }
        
        if(response['success']){
            
            var action_url = $("#planting-protocol-form").attr("action");
            if(submitFlag == 0){
                $.ajax({
                    type:"POST",
                    dataType: 'json',
                    url: action_url,
                    data: form_data,
                    async: true,
                    success: function(data) {
                    
                        var hasToast = $('body').hasClass('toast');
                        
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons green-text'>check</i> <span class='white-text'>Successfully saved planting protocol variables.</span>"; 
                            Materialize.toast(notif, 5000);
                        }
                        $('#next-btn').attr('disabled', false);

                        if ('$shouldUpdateOccurrenceProtocols' && '$hasMultipleSelectedOccurrences') {
                            location.reload()
                        }
                    },
                    error: function(){
                    }
                });
            }
        }
    });

    function loadDefaultValues(){ 
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: '$loadVarValueUrl',
            async: true,
            cache: false,
            data:{
                defaultVariables: JSON.stringify($reqFieldJson),
                isViewOnly: '$isViewOnly',
                shouldUpdateOccurrenceProtocols: '$shouldUpdateOccurrenceProtocols',
                previewOnly: '$previewOnly'
            },
            success: function(data) {
                if(data.length != 0){
                    var plotTypeKey  = '';
                    var memberType = '';
                    var estab = '';
                    var plantTypeKey = '';
                    $.each(data, function (key, value) { 
                        $('#select2-select2-'+key+'-container').text(value['value']);
                        $('#select2-select2-'+key+'-container').attr('title',value['value']);
                        $('#select2-'+key).val(value['value']);

                        if(key == 'plot_type'){
                            plotTypeKey = value['key'];
                            memberType = key
                        }
                        if(key == 'establishment'){
                            estab = value['key'];
                        }
                        if(key == 'planting_type'){
                            plantTypeKey = value['key'];
                        }
                        
                    });
                    updateFilterTags('establishment','load',estab,plantTypeKey, plotTypeKey);
                    updateFilterTags('planting_type','load',estab,plantTypeKey, plotTypeKey);
                    loadPlotTypeFields(plotTypeKey,memberType);
                }
            },
            error: function(){
            }
        });
    }
JS
);
?>
<style>
#content-body{
    margin-top: 10px;
}
.required{
    color:red;
}
label{
    font-size: .8rem;
    color: #333333;
}
#basic-info1{
    margin-left:auto;
    margin-right:auto;
    margin-top:15px;
    display: inline-block;
}
#basic-info2{
    margin-left:auto;
    margin-right:auto;
    margin-top:15px;
    display: inline-block;
}
#filter-panel{
    margin-top: 25px;
}
.my-container{
    min-width: 50%;
    max-width: 100%
}
.select2-results__option.loading-results,
.select2-results__option.select2-results__option--load-more {
    background-image: url('/images/theme/loader.gif');
    background-repeat: no-repeat;
    padding-left: 35px;
    background-position: 10px 50%;
}
.generic-instructions{
    display: none;
}
.parent-box{
    position: relative;
    margin-top: 1rem;
}
.div-protocol-save{
   float: right;
   display:none;
}
</style>