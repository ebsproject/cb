<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders pollination protocol box view
*/

use marekpetras\yii2ajaxboxwidget\Box;

$isViewOnly = $isViewOnly ?? '';
$previewOnly = $previewOnly ?? '';
$hasMultipleSelectedOccurrences = $hasMultipleSelectedOccurrences ?? '';
$occurrenceDbIdsString = $occurrenceDbIdsString ?? '';
$shouldUpdateOccurrenceProtocols = $shouldUpdateOccurrenceProtocols ?? '';
$occurrenceDbId = $occurrenceDbId ?? '';

if ($shouldUpdateOccurrenceProtocols) {
    $helperText = 'Specify the pollination instructions of your ' . (($hasMultipleSelectedOccurrences) ? 'occurrences.' : 'occurrence.');
} else if ($isViewOnly === '1' || $previewOnly === '1') {
    $helperText = '';
} else {
    $helperText = 'Specify the pollination instructions of your experiment.';
}
?>

<div id = "pollination-protocol-div" class = "row col col-sm-12 col-md-12 col-lg-12">
    <p><?= \Yii::t('app', $helperText);?></p>
    <?php
        $toolsButtons = [
            'reload' => function () {
                return 
                    '&nbsp;&nbsp;<a href="#!" title="'.\Yii::t('app','Refresh data').'"  class="text-muted fixed-color protocol-refresh-vars" onclick="$(&quot;#pr-planting-protocols&quot;).box(&quot;reload&quot;);"><i class="material-icons widget-icons">loop</i></a>';
            }
        ];
        $options = [
            'id' => 'pr-pollination-protocols',
            'bodyLoad' => ['/experimentCreation/protocol/pollination-protocol?program=' . $program . '&id=' . $id . '&processId=' . $processId . '&isViewOnly=' . $isViewOnly . '&occurrenceDbId=' . $occurrenceDbId . '&previewOnly=' . $previewOnly . '&shouldUpdateOccurrenceProtocols=' . $shouldUpdateOccurrenceProtocols . '&occurrenceDbIdsString=' . $occurrenceDbIdsString . '&hasMultipleSelectedOccurrences=' . $hasMultipleSelectedOccurrences],
            'toolsTemplate' => '{reload}',
            'toolsButtons' => $toolsButtons
        ];
    ?>
    <?= Box::widget($options); ?>
</div>