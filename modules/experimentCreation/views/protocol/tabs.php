<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Renders the default page for protocols
*/

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\tabs\TabsX;
use kartik\widgets\Select2;
use yii\bootstrap\Modal;

$disabled = '';

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model' => $model,
    'program' => $program,
    'saveBtnLabel' => 'Next',
    'nextBtn' => '',
    'btnName' => 'next-btn'
]);
?>

<?php
$items = [];
$viewArrCnt = sizeof($viewArr);
$content = 'View file does not exist!';

if($viewArrCnt > 0){
    foreach($viewArr as $key => $value){
        $isActive = ($key == 0) ? true : false;
        $filename = $value['action_id'];

        //view file exists
        if(isset($filename)){
        
            $content = Yii::$app->controller->renderPartial("@app/modules/experimentCreation/views/protocol/$filename.php",[
                'id' => $id,
                'program' => $program,
                'processId' => $processId
            ]);
        }
        //there is only one protocol defined for the experiment template
        if($viewArrCnt == 1){
            echo $content;
        }
        else{
            $items[] = [
                'label' => '<i class="'.$value['item_icon'].'"></i> '.Yii::t('app',$value['display_name']),
                'active' => $isActive,
                'content' => $content,
                'linkOptions'=> ['class'=>"pr-tabs pr-$filename-tab"]
            ];     
        }
    }
    echo TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_LEFT,
        'encodeLabels' => false,
        'pluginOptions' => [
            'enableCache' => false
        ]
    ]);
}
else{
    Yii::$app->session->setFlash('error',\Yii::t('app','No protocols found in the database configuration. Refresh the page or please contact web admin.'));
}

$this->registerCssFile("@web/css/box.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'box-css');

$validateUrl = Url::to(['protocol/validate-protocols','id'=>$id, 'program'=>$program,'processId'=>$processId]);
$protocolChildren = json_encode($protocolChildren);
?>

<?php
    // Modal for validation
    Modal::begin([
        'id' => 'required-protocol-modal',
        'header' => '<h4><i class="fa fa-bell"></i>  '.\Yii::t('app','Notification').'</h4>',
        'footer' => Html::a(\Yii::t('app','OK'),['#'],['id'=>'cancel-save-btn','class'=>'btn btn-primary waves-effect waves-light','data-dismiss'=>'modal']),
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static']
    ]);
    echo '<div class="row">
        <p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'Please correct the errors below before proceeding.').'</span></p>
        </div><div class="row" id="required-protocol-notif"></div>';
    Modal::end();

$this->registerJs(<<<JS
   $(document).on('click', '#next-btn', function(e) {

        $('#next-btn').addClass('disabled');
        e.stopPropagation();
        e.preventDefault();
        var protocolChildren = '$protocolChildren';
      
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: '$validateUrl',
            data: {protocolChildren: protocolChildren},
            success: function(data) {
                if(data['flag'] == true){
                    $('#required-protocol-modal').modal();
                    $("#required-protocol-notif").html('');
                    $("#required-protocol-notif").html(data['htmlData']);

                    $('#required-protocol-modal').modal('show');
                }else{
                    window.location = $('#next-btn').attr('data-url');
                }
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }
        });
            
    });
JS
);
?>
<style>
.fa{
    font-size: 95%;
}
input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
    margin: 0 0 5px 0;
}
input[type=text]:not(.browser-default),input[type=number]:not(.browser-default){
    height:2.5rem;
} 
</style>