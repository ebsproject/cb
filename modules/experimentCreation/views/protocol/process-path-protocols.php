<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders process path protocol box view
*/

use yii\helpers\Html;
use marekpetras\yii2ajaxboxwidget\Box;

?>

<p><?= \Yii::t('app',"Specify the process path to be used in each location reps.")?></p>

<div id = "process-path-protocol-div" class = "row col col-sm-12 col-md-12 col-lg-12">	

	<?php

		$toolsButtons = [
			'reload' => function(){
				return 
					'&nbsp;&nbsp;<a href="#!" title="'.\Yii::t('app','Refresh data').'" class="text-muted fixed-color" onclick="$(&quot;#pr-process-path-protocols-box&quot;).box(&quot;reload&quot;);"><i class="material-icons widget-icons">loop</i></a>';
			}
		];
		
		$options = [
			'id' => 'pr-process-path-protocols-box',
			'bodyLoad' => ['/experimentCreation/protocol/process-path-protocols?program='.$program.'&id='.$id.'&processId='.$processId],
			'toolsTemplate' => '{reload}',
			'toolsButtons' => $toolsButtons
		]
	?>
	<?= Box::widget($options); ?>
</div>