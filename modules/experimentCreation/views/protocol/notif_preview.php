<?php

/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$counter = 1;
foreach($listOfNoRecordVal as $key => $value){

    $display_name = $value['display_name'];
    $protocolVal = $value['value'];

    echo '<p>';
    echo '<b> '.$display_name.'</b>';
    
    foreach($value as $index=>$item){
        if(is_array($item)){
            foreach($item as $index_1 => $item_l){
                $label = isset($item_l['label']) ? $item_l['label'] : $item_l; 
                $count = $index_1+1;
              
                echo ($counter).'.'. $label;
                echo '<br>';
                $counter++;
            }
            
        }
        
    echo '</p>';
    }
}