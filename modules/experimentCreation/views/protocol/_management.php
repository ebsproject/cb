<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders management protocols view
 */

use yii\helpers\Url;
use kartik\tabs\TabsX;

$hasMultipleSelectedOccurrences = $hasMultipleSelectedOccurrences ?? '';
$occurrenceDbId = $occurrenceDbId ?? '';
$occurrenceDbIdsString = $occurrenceDbIdsString ?? '';
$shouldUpdateOccurrenceProtocols = $shouldUpdateOccurrenceProtocols ?? '';
?>

<!-- Add variables panel -->
<div class="col col-md-5 management-div">
    <div class="card" style="padding: 5px;">
        <div class="row">
            <div class="col col-md-12">
                <?php
                $mgtVarLabel = Yii::t('app', 'Management Variables');
                $mgVarListLabel = Yii::t('app', 'Management Variable Lists');
                $subTabs = [
                    //management variables
                    [
                        'label' => '<i class="fa fa-leaf"></i> ' . $mgtVarLabel,
                        'linkOptions' => ['class' => 'subTab-management', 'data-id' => 'mgt-var'],
                        'items' => [[
                            'label' => $mgtVarLabel,
                            'linkOptions' => ['id' => 'pr-sub-management-mgt-var'],
                            'active' => true,
                            'encode' => false,
                            'content' => ''
                        ]],
                        'headerOptions' => ['class' => 'hide-caret management', 'data-id' => 'mgt-var', 'id'=>'mgt-var', 'title' => 'Management variables']
                    ],
                    //management variable lists
                    [
                        'label' => '<i class="fa fa-list-alt"></i> ' . $mgVarListLabel,
                        'linkOptions' => ['class' => 'subTab-management', 'data-id' => 'mgt-group'],
                        'items' => [[
                            'label' => $mgVarListLabel,
                            'linkOptions' => ['id' => 'pr-sub-management-mgt-group'],
                            'encode' => false,
                            'content' => ''
                        ]],
                        'headerOptions' => ['class' => 'hide-caret management', 'data-id' => 'mgt-group', 'id'=>'mgt-group','title' => 'Add variables from a management variable list']
                    ]
                ];

                echo TabsX::widget([
                    'items' => $subTabs,
                    'position' => TabsX::POS_LEFT,
                    'sideways' => true,
                    'encodeLabels' => false,
                    'pluginOptions' => [
                        'enableCache' => false
                    ],
                    'enableStickyTabs' => true,
                    'containerOptions' => [
                        'class' => 'pr-management-protocol-tab',
                        'id' => 'manage-mgt-tab-container'
                    ]
                ]);

                ?>
            </div>
        </div>
    </div>
</div>

<!-- Selected management -->
<div class="col col-md-7 management-div">
    <div class="card" style="padding: 5px">
        <div class="row">
            <div class="col col-md-12">
                <?php
                echo Yii::$app->controller->render('@app/modules/experimentCreation/views/protocol/_management_list.php', [
                    'id' => $id,
                    'dataProvider' => $dataProvider,
                    'targetLevelValue' => $targetLevelValue
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
//routes
$addVariablesUrl = Url::to(['/experimentCreation/protocol/add-variables']);
$reorderUrl = Url::to(['/experimentCreation/protocol/reorder-variables']);
$removeVariablesUrl = Url::to(['/experimentCreation/protocol/remove-variables']);
$confirmDeleteVariablesUrl = Url::to(['/experimentCreation/protocol/confirm-delete-variables']);
$restoreVariablesUrl = Url::to(['/experimentCreation/protocol/restore-variables']);
$saveAsNewListUrl = Url::to(['/experimentCreation/protocol/save-as-list']);
$saveRemarksUrl = Url::to(['/experimentCreation/protocol/save-remarks']);
$saveBulkRemarksUrl = Url::to(['/experimentCreation/protocol/save-bulk-remarks']);
$retrieveVarListPreviewUrl = Url::to(['/experimentCreation/protocol/retrieve-mgt-variables-preview', 'id' => $id]);
$confirmAddVarListUrl = Url::to(['/experimentCreation/protocol/add-mgt-var-list']);
$confirmSaveVariableListUrl = Url::to(['/experimentCreation/protocol/save-variable-list']);
$resetUrl =  "/index.php/experimentCreation/protocol/management-protocol?$program" . '&id=' . $id . '&processId=' . $processId;
$manageVariableUrl = Url::to(['/experimentCreation/protocol/management',
    'id'=>$id,
    'program'=>$program,
    'processId'=>$processId,
    'occurrenceDbIdsString'=>$occurrenceDbIdsString,
    'shouldUpdateOccurrenceProtocols'=>$shouldUpdateOccurrenceProtocols,
]);
$saveMgtTargetUrl = Url::to(['/experimentCreation/protocol/save-mgt-target-level', 'id'=>$id]);
?>

<?php
$this->registerJs(<<<JS

    var link = '/index.php/experimentCreation/protocol/management-protocol?program='+"$program" + '&id='+"$id"+'&processId='+"$processId" +'&shouldUpdateOccurrenceProtocols='+"$shouldUpdateOccurrenceProtocols" + '&occurrenceDbIdsString='+"$occurrenceDbIdsString";
    var targetLevelValue = '$targetLevelValue';

    //Load the saved target level value if there is
    $("document").ready(function(){
        if(typeof targetLevelValue != 'undefined' || targetLevelValue != null){
            //Set the saved value here
            if(targetLevelValue == 'occurrence'){
                $('#mgt-type-switch-id').prop("checked",false);
            }else if(targetLevelValue == 'location'){
                //location
                $('#mgt-type-switch-id').prop("checked",true).trigger('change');
            }else{
                //Default is occurrence
                $('#mgt-type-switch-id').prop("checked",false);
            }
        }
    });
    
    //Load management attributes sub-tab as the default
    setTimeout(function(){
        $.ajax({
            type: 'POST',
            url: '$manageVariableUrl',
            async: false,
            data:{
                listId: '$listId',
                program: '$program',
                id: '$id',
                processId: '$processId',
                subtab: 'mgt-var'
            },
            success: function(data) {
                $('.preloader-wrapper').addClass('hidden');
                $('.management-var-panel').removeClass('kv-grid-loading');
                $('#manage-mgt-tab-container').children().eq(1).html(data);
            },
            beforeSend: function(){
                $('.management-var-panel').addClass('kv-grid-loading');
            }
        });
    },100);

    //calls subtab content upon click
    $(document).on('click','.subTab-management',function(e){
        var subTabId = $(this).data('id');
        
        //To avoid js conflict 
        if(subTabId == 'mgt-var'){
            $('#mgt-var').addClass('active');
            $('#mgt-group').removeClass('active');

            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: '$manageVariableUrl',
                    async: false,
                    data:{
                        listId: '$listId',
                        program: '$program',
                        id: '$id',
                        processId: '$processId',
                        subtab: 'mgt-var'
                    },
                    success: function(data) {
                        $('.preloader-wrapper').addClass('hidden');
                        $('#manage-mgt-tab-container').children().eq(1).html(data);
                    }
                });
            },100);
        }else{
            $('#mgt-var').removeClass('active');
            $('#mgt-group').addClass('active');

            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: '$manageVariableUrl',
                    async: false,
                    data:{
                        listId: '$listId',
                        program: '$program',
                        id: '$id',
                        processId: '$processId',
                        subtab: 'mgt-group'
                    },
                    success: function(data) {
                        $('.preloader-wrapper').addClass('hidden');
                        $('#manage-mgt-tab-container').children().eq(1).html(data);
                    }
                });
            },100);
        }
    });

    //Save the management type -- Occurrence of Location
    $(document).on('change','#mgt-type-switch-id', function(){
        var mgtTypeOption = $(this).prop('checked');
        if(mgtTypeOption){
            mgtType='location';
        }else{
            mgtType='occurrence';
        }
    
        //Save the switched option in the tenant.protocol
        $.ajax({
            type: 'POST',
            url: '$saveMgtTargetUrl',
            async: false,
            data:{
                targetLevel: mgtType
            },
            success: function(data) {
            
            }
        });
    
    });

    //disable action buttons when there is no record
    var varCount = document.getElementsByClassName("mgt-final-grid_select");
    if(varCount.length == 0){
        $('.management-list-actions').addClass('disabled');
    }else{
        $('.management-list-actions').removeClass('disabled');
    } 

    // hide loader if action is cancelled
    $('#mgt-back-replace-btn, #mgt-back-delete-btn, #mgt-back-preview-btn').on('click', function(){
        $('.preloader-wrapper').addClass('hidden');
    });

    $(document).on('ready pjax:success', function(e) {
        e.preventDefault();
        refresh();
    });
    
    refresh();

    // refresh gridview
    function refresh(){
        $(".management-div").css("height", ($(window).height() - 270));

        $(".kv-grid-wrapper").css("height",$('.management-div').height() - 140);

        var varCount = document.getElementsByClassName("mgt-final-grid_select");
        if(varCount.length == 0){
            $('.management-list-actions').addClass('disabled');
        }else{
            $('.management-list-actions').removeClass('disabled');
        } 
    }

    // check row in variables browser
    $(document).on('click', '#management-list-selected-vars-grid tbody tr', function(e) {

        this_row=$(this).find('input:checkbox')[0];
        if(this_row.checked){
            this_row.checked=false;  
            $('#management-list-variables-grid-select-all-id').prop("checked", false);
            $(this).removeClass("grey lighten-4");
        }else{
            $(this).addClass("grey lighten-4");
            this_row.checked=true;  
            $("#"+this_row.id).prop("checked");  
        }

    });

    // select all variables
    $(document).on('click', '#management-list-variables-grid-select-all-id', function(e) {
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.mgt-final-grid_select').attr('checked','checked');
            $('.mgt-final-grid_select').prop('checked',true);
            $(".mgt-final-grid_select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");

        }else{

            $(this).removeAttr('checked');
            $('.mgt-final-grid_select').prop('checked',false);
            $('.mgt-final-grid_select').removeAttr('checked');
            $("input:checkbox.mgt-final-grid_select").parent("td").parent("tr").removeClass("grey lighten-4");      
        }

    });

    // view save new list modal
    $(document).on('click', '#management-save-new-list-btn', function(e) {

        var varIdsSavedList=[];
        var retrieveVarListPreviewUrl = '$retrieveVarListPreviewUrl';

        $("input:checkbox.mgt-final-grid_select").each(function(){

            if($(this).prop("checked") === true)  {
                varIdsSavedList.push($(this).attr("id"));
            }

        });
    
        if(varIdsSavedList.length == 0){
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please select at least one(1) management attribute.</span>";
                Materialize.toast(notif, 5000);
            }
            e.preventDefault();
            e.stopImmediatePropagation();
        }else{
            $('.toast').css('display','none');
            
            $('#management-save-list-preview-error').html('');
            $('#management-save-list-modal').modal('show');

            $('#management-save-list-modal').on('shown.bs.modal', function() {
                $('#management-list-name').trigger('focus');
            });

            $('.management-modal-loading').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');
            $('.management-save-list-preview-variables').html('');
                
            $("#management-list-abbrev").trigger("change");
           
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: retrieveVarListPreviewUrl,
                    data: {
                        id: '$id',
                        idList: varIdsSavedList,
                    },
                    async: false,
                    dataType: 'json',
                    success: function(response) {
                        $('.management-save-list-preview-variables').html(response.htmlData);

                        $('.management-modal-loading').html('');

                    }
                });
            }, 500);
        }
    });

    // validate if all required fields are specified
    $('.form-control').bind("change keyup input",function() {
        var abbrev = $('#management-list-abbrev').val();
        var name = $('#management-list-name').val();
        var display_name = $('#management-list-display_name').val();

        if(abbrev != '' && name != '' && display_name != ''){
            $('#management-save-list-confirm-btn').removeClass('disabled');
        }else{
            $('#management-save-list-confirm-btn').addClass('disabled');
        }
    });

    // confirm save variable list
    $(document).on('click', '#management-save-list-confirm-btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        
        var confirmSaveVariableListUrl = '$confirmSaveVariableListUrl';
        var varIdsSavedList=[];

        var abbrev = $('#management-list-abbrev').val();
        var name = $('#management-list-name').val();
        var displayName = $('#management-list-display_name').val();
        var description = $('#management-list-description').val();
        var remarks = $('#management-list-remarks').val();

        $("input:checkbox.mgt-final-grid_select").each(function(){
            if($(this).prop("checked") === true)  {
                varIdsSavedList.push($(this).attr("data-id"));
            }
        });
        
        $.ajax({
            type: 'POST',
            url: confirmSaveVariableListUrl,
            data: {
                id: '$id',
                idList: varIdsSavedList,
                abbrev: abbrev,
                name: name,
                displayName: displayName,
                description: description,
                remarks: remarks,
                protocolType: 'management'
            },
            dataType: 'json',
            async: false,
            success: function(response) {
                // with error
                if(response.msg != ''){
                    var notif = "<i class='material-icons red-text left'>close</i> Problem in creating variable list. " + response.msg;
                }else{
                    var notif = "<i class='material-icons green-text left'>done</i> Successfully saved " + name + '.';

                    //reload page
                    $.pjax.reload({
                        url: link,
                        container: '#management-list-selected-vars-grid-pjax',
                        replace: false
                    });
                }
                $('#management-save-list-modal').modal('hide');
                $('.toast').css('display','none');
                Materialize.toast(notif,5000);

            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while saving the list. ";

                $('.toast').css('display','none');
                Materialize.toast(notif,5000);

                $('#management-save-list-confirm-btn').addClass('disabled');
            }
        });
    });

    // reorder variables
    $('#management-list-selected-vars-grid tbody').on('sortupdate',function(event,ui){

        var list = new Array();
        var reorderUrl = '$reorderUrl';
        var order = $(this).find('.order_tr').each(function(){
            var id = $(this).attr("data-varId");    
            list.push(parseInt(id));
        });

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: reorderUrl,
            data: {
                id: '$id', 
                idList: list},
            async: false,
            success: function(data) {
                var notif = "<i class='material-icons green-text left'>done</i> Successfully reordered variables.";

                $('.toast').css('display','none');
                Materialize.toast(notif,5000);
            }
        });
    });

    // remove variable
    $(document).on('click', '#management-discard-btn', function(e) {
        
        var idList=[];

        $("input:checkbox.mgt-final-grid_select").each(function(){

            if($(this).prop("checked") === true)  {
                idList.push($(this).attr("id"));
            }

        });
        
        //check variable list
        var idListCnt = idList.length;

        if(idListCnt == 0){
            var notif = "<i class='material-icons orange-text left'>warning</i> No selected management variable/s.";

            $('.toast').css('display','none');
            Materialize.toast(notif,5000);
        }else{
            $('#mgt-delete-modal').modal('show');
        }
    });

    // confirm delete management variables
    $(document).on('click', '#mgt-proceed-delete-btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var idList=[];
        var removeVariablesUrl = '$removeVariablesUrl';

        $("input:checkbox.mgt-final-grid_select").each(function(){

            if($(this).prop("checked") === true)  {
                idList.push($(this).attr("id"));
            }

        });

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: removeVariablesUrl,
            data: {
                idList: idList
            },
            async: false,
            success: function(data) {
                let notif = ''

                if (!data.success) {
                    notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while removing management variables. "
                    $('.toast').css('display','none')
                    Materialize.toast(notif,5000)
                } else {
					let successfulCount = data.successfulCount
					let failedCount = data.failedCount
					let noPermissionCount = data.noPermissionCount
					let totalCount = successfulCount + failedCount + noPermissionCount

					// Toast settings
					let icon = 'close'
					let iconColor = 'red'
					let message = ''

					if (successfulCount) {
						message += `Successfully removed&nbsp;<b>\${successfulCount}</b>&nbsp;management variable/s.<br>`
						icon = 'done'
						iconColor = 'green'
					}
					if (failedCount) {
						message += `Failed to remove&nbsp;<b>\${failedCount}</b>&nbsp;management variable/s.<br>`
					}
					if (noPermissionCount) {
						message += `You do not have permission to remove&nbsp;<b>\${noPermissionCount}</b>&nbsp;management variable/s.`
					}
            
					if (totalCount) {
						notif = `<i class='material-icons \${iconColor}-text left'>\${icon}</i> <span>\${message}</span>`;
					}

                    $.pjax.reload({
                        url: link,
                        container: '#management-list-selected-vars-grid-pjax',
                        replace: false
                    });
                }

				$('.toast').css('display','none');
				Materialize.toast(notif,5000);
				$('#mgt-delete-modal').modal('hide');

                $('#mgt-delete-modal').modal('hide');
                    
                return false;
            }
        });

    });

    // check if instruction is updated (on one variable)
    $(document).on('change paste', ".mgt-instructions-input-validate", function(event) {
        var id = this.id;
        var instructionsArr = (this.id).split('-');
        var listMemberId = instructionsArr[1];
        var remarks = $('#'+id).val();
        
        $('.mgt-add-replace-vars-btn').attr('disabled',true);``
        
        //Save the changes in the database
        $.ajax({
            url: '$saveRemarksUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                listMemberId: listMemberId, 
                remarks: remarks
            },
            success: function(response) {
                $('.mgt-add-replace-vars-btn').attr('disabled',false);
            },
            error: function() {
                var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while updating instructions. ";
                $('.toast').css('display','none');
                Materialize.toast(notif,5000)
            }
        });
    });	
    
    // check if there is a remark
    $('#mgt-remarks-bulk-input').bind("keypress keyup blur paste",function() {
        var remarks = $('#mgt-remarks-bulk-input').val();
        
        if(remarks == ''){
            $('.mgt-confirm-update-btn').attr('disabled',true);
        }else{
            $('.mgt-confirm-update-btn').attr('disabled',false);
        }
    });
    
    // view buk update modal
    $(document).on('click', '#management-update-remarks-btn', function(e) {
        $('#mgt-update-remarks-modal').modal('show');
    });

    // bulk update remarks
    $(document).on('click', '.mgt-confirm-update-btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var obj = $(this);
        var operation = obj.data('operation');
        var remarks = $('#mgt-remarks-bulk-input').val();

        var listMemberIdList=[];

        $("input:checkbox.mgt-final-grid_select").each(function(){

            if($(this).prop("checked") === true)  {
                listMemberIdList.push($(this).attr("id"));
            }

        });

        //check variable list
        var listMemberIdListCnt = listMemberIdList.length;

        if(listMemberIdListCnt == 0 && operation !== 'update-all'){

            var notif = "<i class='material-icons orange-text left'>warning</i> No selected variable(s).";

            $('.toast').css('display','none');
            Materialize.toast(notif,5000);
        }else{
            listMemberIdList = (operation == 'update-all') ? null : listMemberIdList;
            $.ajax({
                url: '$saveBulkRemarksUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    listId: '$listId',
                    listMemberId: listMemberIdList,
                    remarks: remarks,
                    operation: operation,
                },
                success: function(response) {
                    if(!response.success){
                        var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while updating instructions. ";
                    } else {
                        var notif = response.message;
                        //reload page
                        $.pjax.reload({
                            url: link,
                            container: '#management-list-selected-vars-grid-pjax',
                            replace: false
                        });
                    }
                    $('#mgt-update-remarks-modal').modal('hide');
                    $('.toast').css('display','none');
                    Materialize.toast(notif,5000);

                },
                error: function() {
                    var notif = "<i class='material-icons red-text left'>close</i> There seems to be a problem while updating instructions. ";

                    $('.toast').css('display','none');
                    Materialize.toast(notif,5000);
                }
            });
        }
    });

JS);
?>

<style type="text/css">
    .hide-caret .caret,
    .hide-caret .dropdown-menu {
        display: none;
    }

    .tabs-left .tab-content {
        border-left: none;
    }

    .management-div {
        margin-bottom: 10px;
    }

    .kv-panel-after {
        display: none;
    }

    .tabs-krajee.tab-sideways .nav-tabs > li {
        height: 100px;
        width: 200px;
    }
    .switch{
        margin-top: 3px;
    }
</style>