<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders planting protocol box view
*/

use marekpetras\yii2ajaxboxwidget\Box;

$isViewOnly = $isViewOnly ?? '';
$previewOnly = $previewOnly ?? '';
$hasMultipleSelectedOccurrences = $hasMultipleSelectedOccurrences ?? '';
$occurrenceDbIdsString = $occurrenceDbIdsString ?? '';
$shouldUpdateOccurrenceProtocols = $shouldUpdateOccurrenceProtocols ?? '';
$occurrenceDbId = $occurrenceDbId ?? '';

$helperText = ($isViewOnly === '1' || $previewOnly === '1' )? '' : 'Fields marked with <span class="required">*</span> are required. Greyed out fields are automatically provided by the system and cannot be modified.';
?>

<div id = "planting-protocol-div" class = "row col col-sm-12 col-md-12 col-lg-12">
    <p><?= \Yii::t('app', $helperText);?></p>
	<?php
        $toolsButtons = [
            'reload' => function () {
                return '&nbsp;&nbsp;<a href="#!" title="'.\Yii::t('app','Refresh data').'"  class="text-muted fixed-color protocol-refresh-vars" onclick="$(&quot;#pr-planting-protocols&quot;).box(&quot;reload&quot;);"><i class="material-icons widget-icons">loop</i></a>';
            }
        ];

		$options = [
			'id' => 'pr-planting-protocols',
			'bodyLoad' => ['/experimentCreation/protocol/planting-protocols?program=' . $program . '&id=' . $id . '&processId=' . $processId . '&isViewOnly=' . $isViewOnly . '&occurrenceDbId=' . $occurrenceDbId . '&previewOnly=' . $previewOnly . '&shouldUpdateOccurrenceProtocols=' . $shouldUpdateOccurrenceProtocols . '&occurrenceDbIdsString=' . $occurrenceDbIdsString . '&hasMultipleSelectedOccurrences=' . $hasMultipleSelectedOccurrences],
			'toolsTemplate' => '{reload}',
			'toolsButtons' => $toolsButtons
		];
	?>
	<?= Box::widget($options); ?>
</div>