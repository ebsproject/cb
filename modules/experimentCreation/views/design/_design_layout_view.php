<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\dropdown\DropdownX;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\helpers\Html;

/**
* Renders layout view for the parameter set
*/

if (!isset($model) || empty($model)) {
    Yii::$app->session->setFlash('error',
        \Yii::t('app', 'Unable to retrieve the layout records. Please try again or file a report for assistance.')
    );
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model' => $model,
    'program' => $program,
    'saveBtnLabel' => 'Next',
    'nextBtn' => 'disabled',
    'btnName' => 'next-btn',

]);

?>


<?php
/**
* Renders preview layout of design
*
*/
if(empty($data) || ($statusDesign == 'draft')){
    echo '<br><div class="alert alert-warning" style="padding: 10px;">No records generated</div>';
    echo Html::a(
        '<i class="material-icons inline-material-icon">arrow_back</i>',
        [
            '/experimentCreation/create/specify-design',
            'program' => $program,
            'id' => $id,
            'processId' => $processId
        ],
        [
            'id' => 'goto-design-tab',
            'class' => 'waves-effect waves-light btn btn-default pull-right',
            'title' => \Yii::t('app', 'Go back to specifying design'),
            'style' => 'margin-right: 4px;',
        ]
    );
}else if(empty($data) && ($statusDesign == 'in progress' || $statusDesign == 'upload in progress')){
	  echo '<div class="alert alert-warning" style="padding: 10px;">Record generation is still on going.</div>';
} else if(empty($data) && $statusDesign == 'deletion in progress'){
    echo '<div class="alert alert-warning" style="padding: 10px;">Deletion of plot records is still on going.</div>';
}else if((empty($data['plotno']) || $data['plotno']== null) && $statusDesign == 'completed'){
    echo '<div class="alert alert-success" style="padding: 10px;">Experiment successfully randomized. Layout cannot be shown for this design configuration.</div>';
} else if(!empty($data['plotno']) && $data['plotno'] != null && ($statusDesign != 'in progress' || $statusDesign != 'deletion in progress')){
    $entnoLabel ='';
    $plotnoLabel ='';
    $repnoLabel = '';
    $codeLabel = '';
    $blkLabel = '';
    $hideBlk = "";
    if($label == 'entno'){
        $entnoLabel = 'teal darken-4 white-text';
    } else if($label == 'plotno'){
        $plotnoLabel = 'teal darken-4  white-text';
    } else if($label == 'block'){
        $blkLabel = 'teal darken-4  white-text';
    } else if($label == 'plotcode'){
        $codeLabel = 'teal darken-4  white-text';
    }
  
    if(empty($maxBlk) || $maxBlk == NULL  || in_array($design, ['Partially Replicated', 'Augmented Design'])){
        $hideBlk = "hidden";
    }

    $checked = '';
    if(!$flipped){
        $checked = 'checked';
    }
?>

    <div id="layout-viewer-panel" style="overflow:auto;padding:20px;">

        <!-- Dropdown Trigger -->
        <a id="entno" class='dropdown-change-label btn <?=$entnoLabel?>' href='#' >Entry number</a>
        <a id="plotno" class='dropdown-change-label btn <?=$plotnoLabel?>' href='#' >Plot number</a>
        <a id="plotcode" class='dropdown-change-label btn <?=$codeLabel?>' href='#' >Plot Code</a>
        <?php
          if($design != 'Row-Column'){
        ?>
            <a id="block"  class='dropdown-change-label btn <?=$blkLabel?> <?=$hideBlk?>' href='#' >Block number</a>
        <?php } ?>

        <a class="waves-effect waves-light btn pull-right fullscreen-btn pull-right tooltipped" data-position="top" data-delay="50" title="Click to toggle fullscreen"><i class="material-icons">fullscreen</i></a>

        <?php
            echo Html::a(
                    '<i class="material-icons inline-material-icon">arrow_back</i>',
                    [
                        '/experimentCreation/create/specify-design',
                        'program' => $program,
                        'id' => $id,
                        'processId' => $processId
                    ],
                    [
                        'id' => 'goto-design-tab',
                        'class' => 'waves-effect waves-light btn btn-default pull-right',
                        'title' => \Yii::t('app', 'Go back to specifying design'),
                        'style' => 'margin-right: 4px;',
                    ]
                );
        ?>

        <?php
            // Download occurrence
            $occurrenceDownload = "<a class='export-occurrence' id='export-btn-id' style='cursor:pointer;' title='Export Occurrence' data-id='".$occurrenceDbId."' data-target='#'><i style='padding-top: 8px;font-size: 160% !important;' class='material-icons  inline-material-icon'>file_download</i></a>";

            echo '<div id="select-occurrences-div" style="width: 270px;margin: 10px;">'.Select2::widget([
                'name' => 'occurrenceSelect-dropdown',
                'value' => isset($occurrenceDbId) ? $occurrenceDbId:'',// initial value
                'data' => $occurrenceArray,
                'options' => ['placeholder' => Yii::t('app', 'Select Occurrence'), 'class' => 'dropdown-change-label select2-Drop pull-left', 'style'=>'margin-bottom:15px;width:35px;',
                'id' => 'occurrence-selection-id'
                ],
            ]).'</div>';


        ?>
        <div id="layout-view-pane"  class="col col-sm-12 view-entity-content">
        <div id="notifications-div" hidden>
            <div class="progress"><div class="indeterminate"></div></div>
        </div>
        <div id="design-layout-panel"  class="chartContainer"  style="margin-top:15px; overflow: auto;">
            <div class="progress"><div class="indeterminate"></div></div>
        </div>
        <div class="row col-sm-6">
            <label>First Plot Position</label>
            <div class="switch">
                <label>
                    Top Left
                    <input type="checkbox" id="flipped-switch-id" <?=$checked?>>
                    <span class="lever"></span>
                    Bottom Left
                </label>
            </div> 
        </div>
    </div>
</div>

<?php

$updateLayoutUrl = Url::to(['create/change-occurrence-layout']);
$updateFirstPlot = Url::to(['create/change-first-plot-pos', 'id'=>$experimentId]);

$encodedData = json_encode($data);
$xAxisEncode = json_encode($xAxis);
$yAxisEncode = json_encode($yAxis);

$this->registerJsFile('https://code.highcharts.com/stock/highstock.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://code.highcharts.com/modules/heatmap.js',['position' => \yii\web\View::POS_END]);

$this->registerJs(<<<JS
var experimentId = '$experimentId'
var startingPlotNo = '$startingPlotNo'
var noOfCols = parseInt('$noOfCols')
var noOfRows = parseInt('$noOfRows')
var data = JSON.parse('$encodedData');
var label = '$label';
var dataArray = data[label];

var panelWidth = '$panelWidth';
var flipped = '$flipped';
var updateLayoutUrl =  '$updateLayoutUrl'
var updateFirstPlot =  '$updateFirstPlot'
var fullscreenFlag = null;
var occurrenceDbId = '$occurrenceDbId'
var occurrenceName = '$occurrenceName'
var xAxis = JSON.parse('$xAxisEncode');
var yAxis = JSON.parse('$yAxisEncode');

var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
var width = 0;
var height = 0;
var wbWidth = 50;
var hbWidth = 40;
var row = Object.keys(yAxis).length;
var col = Object.keys(xAxis).length;

if($('#flipped-switch-id').prop('checked') == true){
    flipped = false;
} else {
    flipped = true;
}

$(document).ready(function(){

    if(label == 'entno'){
        $("#select-occurrences-div").removeClass('hidden');
    } else {
        $("#select-occurrences-div").addClass('hidden');
    }
    
    $('.dropdown-button').dropdown();
    if(noOfCols > 0){
        computeDimension();
        renderDesignLayout();
    }

    $(".dropdown-change-label").click(function(){
        label = this.id;
        dataArray = data[label];
        
        $(".dropdown-change-label").removeClass("teal darken-4");

        $(this).addClass("teal darken-4 white-text");

        if(fullscreenFlag == 1){
            $('#design-layout-panel').html('<div class="progress"><div class="indeterminate"></div></div>');
        }
        
        if(label == 'entno'){
            $("#select-occurrences-div").removeClass('hidden');
        } else {
            $("#select-occurrences-div").addClass('hidden');
        }
        computeDimension();
        renderDesignLayout();
    });

    $("#flipped-switch-id").on('change', function (e, params) {

        if($('#flipped-switch-id').prop('checked') == true){
            flipped = false;
        } else{
            flipped = true;
        }
        $.ajax({
            url: updateFirstPlot,
            type: 'post',
            data: {
                flipped: flipped
            },
            success: function(response) {
                if(fullscreenFlag == 1){
                    $('#design-layout-panel').html('<div class="progress"><div class="indeterminate"></div></div>');
                }
                computeDimension();
                renderDesignLayout();
            },
            error: function(e) {
                console.log(e);
            }
        });
        
    });

    $("#occurrence-selection-id").on('change', function (e, params) {
        occurrenceDbId = this.value;
        occurrenceName = document.getElementById(this.id).innerText;

        $('#design-layout-panel').html('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: updateLayoutUrl,
            type: 'post',
            data: {
                occurrenceDbId: occurrenceDbId,
                fullScreen:fullscreenFlag,
                experimentId:experimentId
            },
            success: function(response) {
                layoutData = JSON.parse(response);
                data = layoutData['dataArrays'];
                dataArray = data[label];
                xAxis = layoutData['xAxis'];
                yAxis = layoutData['yAxis'];

                row = Object.keys(yAxis).length;
                col = Object.keys(xAxis).length;

                computeDimension();
                renderDesignLayout();
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    $('.fullscreen-btn').click(function(e){
        if(fullscreenFlag == null || fullscreenFlag == 0){
            $('.fullscreen-btn').html('<i class="material-icons">fullscreen_exit</i>');
            fullscreenFlag = 1;
        } 
        else if(fullscreenFlag == 1){
            fullscreenFlag = 0;
            $('#main-left-panel').removeClass('hidden');
            $('.fullscreen-btn').html('<i class="material-icons">fullscreen</i>');
        }
        $('#design-layout-panel').html('<div class="alert alert-warning" style="padding: 10px;">Please wait while loading the layout.</div>');
        $.ajax({
            url: updateLayoutUrl,
            type: 'post',
            data: {
                occurrenceDbId: occurrenceDbId,
                fullScreen:fullscreenFlag,
                experimentId:experimentId
            },
            success: function(response) {
                layoutData = JSON.parse(response);

                data = layoutData['dataArrays'];
                dataArray = data[label];
                
                xAxis = layoutData['xAxis'];
                yAxis = layoutData['yAxis'];

                noOfRows = layoutData['maxRows'];
                noOfCols = layoutData['maxCols'];

                row = Object.keys(yAxis).length;
                col = Object.keys(xAxis).length;

                computeDimension();
                renderDesignLayout();
                if(fullscreenFlag == 1){
                    $("#occurrence-selection-id").attr('disabled', 'disabled');
                } else {
                    $("#occurrence-selection-id").removeAttr('disabled');
                }
                $('#layout-viewer-panel').toggleClass('fullscreen');
                
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

});
function computeDimension(){

    if(col <= 11){
        wbWidth = 80;
    } else {
        wbWidth = 50;
    }

    if(row <= 11){
        hbWidth = 40;
    } else {
        hbWidth = 30;
    }

    if(fullscreenFlag == 1){
        if(col <= 20){
            wbWidth = 80;
        }
        if(row <= 20){
            hbWidth = 40;
        }
    }

    //compute width of chart
    width = wbWidth * col;
    if(width < 200){
        width = 200;
    }

    //compute height of chart
    height = hbWidth * row;
    if(height < 200){
        height = 200;
    }

}

function renderDesignLayout(){
    var layout = null

    var maxX = xAxis.length-1
    if(noOfCols <= 10){
        maxX = noOfCols
    }

    var maxY = yAxis.length-1
    if(noOfRows <= 10){
        maxY = noOfRows
    }

    setTimeout(function(){

        layout = Highcharts.chart('design-layout-panel',{
            chart: {
                marginTop: 70,
                marginRight: 14,
                plotBorderWidth: 1,
                type: 'heatmap',
                plotBorderWidth: 1,
                plotBorderRadius: 5,
                animation: false,
                height: height,
                width: width
            },
            title: {
                text: 'Design Result Preview'
            },
            xAxis: {
              opposite: true,
              categories: xAxis,
              min: xAxis[1],
              max: maxX,
              title:{
                text:"Column"
              }
            },
            yAxis: {
                reversed: flipped,
                categories: yAxis,
                min: yAxis[1],
                max: maxY,
                title:{
                    text:"Row"
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            series: [{
                name: 'Layout',
                borderWidth: 1,
                borderColor: '#fff',
                color: '#7cb342',
                turboThreshold:Number.MAX_VALUE,//set it to a larger threshold, it is by default to 1000
                data: dataArray,
                dataLabels: {
                    enabled: true
                }
            }]
        });
    },50);
}
JS);

?>

<style type="text/css">
    tspan{
        font-size: 13px;
    }
    .view-entity-content {
        background: #ffffff;
    }
    #occurrence-name-span {
        font-size: 1rem;
    }
    #layout-viewer-panel.fullscreen{
        padding: 25px;
        z-index: 9999; 
        width: 100%; 
        height: 100%; 
        position: fixed; 
        top: 0; 
        left: 0;
        background: #ffffff;
    }
    .highcharts-container{
        width:100%;
    }
    .chartContainer{
        overflow:auto !important;
    }
    .dropdown-content {
        overflow: auto !important;;
    }
</style>

<?php
}
?>