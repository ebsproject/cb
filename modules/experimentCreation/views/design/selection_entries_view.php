<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\dynagrid\DynaGrid;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/**
* Renders entry browser for selection
*/
$resetGridUrl = Url::toRoute(['design/selection-entries']);

if (!isset($model) || empty($model)) {
    Yii::$app->session->setFlash('error',
        \Yii::t('app', 'Unable to retrieve the entry records. Please try again or file a report for assistance.')
    );
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model' => $model,
    'program' => $program,
    'saveBtnLabel' => 'Next',
    'nextBtn' => $isNextButtonDisabled,
    'btnName' => 'next-btn',

]);

?>

<div class="col col-sm-12 view-entity-content">
<!-- Browse Entry List -->
<?php
$browserId = 'dynagrid-ec-select-entries-type-based';

$columns = [
    [ // Checkbox
        'label' => 'Checkbox',
        'header' => '
            <input type="checkbox" class="filled-in" id="checkbox-select-all" />
            <label for="checkbox-select-all" title="Select all entry records"></label>
        ',
        'content' => function ($data) use ($selectedIds) {
            $entryDbId = $data['entryDbId'];
            $checked = (in_array($entryDbId,$selectedIds)) ? "checked":"";
            return "<input
                class='grid-select filled-in'
                type='checkbox'
                id='$entryDbId'
                $checked 
            /><label for='$entryDbId'></label>";
        },
        'hAlign'=>'center',
        'vAlign'=>'top',
        'hiddenFromExport'=>true,
        'mergeHeader'=>true,
        'order'=> DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryNumber',
        'label' => '<span title="Sequential numbering of the entries">'.Yii::t('app', 'Entry No.').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryName',
        'label' => '<span title="Gemplasm name">'.Yii::t('app', 'Germplasm Name').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryType',
        'label' => '<span title="Entry Type">'.Yii::t('app', 'Entry Type').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryRole',
        'label' => '<span title="Entry Role">'.Yii::t('app', 'Entry Role').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryClass',
        'label' => '<span title="Entry Class">'.Yii::t('app', 'Entry Class').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ]
];

// Set dynagrid data browser configuration
$dynagrid = DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize'=> false,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsiveWrap' => false,
        'panel' => [
            'heading' => false,
            'before' => '<h5>' . \Yii::t('app', ucfirst($selectEntryType).' Entries Selection') . '</h5><br/>' .
               \Yii::t('app', 'Select '.$selectEntryType.' entries to include in the parameter set.') . '&emsp;' .
                '{summary}<br/>
                <p
                    id = "selected-items-paragraph"
                    class = "pull-right"
                    style = "margin-right: 4px"
                >
                    <b><span id="selected-items-count"> </span></b>
                    <span id="selected-items-text"> </span>
                </p>',
            'after' => false,
        ],
        'toolbar' => [
            [ 'content' => Html::a(
                    'Save',
                    '#',
                    [
                        'id' => 'save-selection-entries',
                        'class' => 'btn btn-default green',
                        'title' => \Yii::t('app', 'Save selected entries'),
                        'style' => 'margin-right: 4px;',
                    ]
                ) . Html::a(
                    '<i class="material-icons inline-material-icon">arrow_back</i>',
                    [
                        '/experimentCreation/create/specify-design',
                        'program' => $program,
                        'id' => $id,
                        'processId' => $processId
                    ],
                    [
                        'id' => 'goto-design-tab',
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app', 'Go back to specifying design'),
                        'style' => 'margin-right: 4px;',
                    ]
                ) . Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    [
                        'selection-entries',
                        'program' => $program,
                        'id' => $id,
                        'processId' => $processId,
                        'selectEntryType' => $selectEntryType
                    ],
                    [
                        'data-pjax' => true,
                        'id' => "reset-$browserId",
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app', 'Reset grid'),
                        'style' => 'margin-right: 4px;',
                    ]
                ) . '{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
        ],
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-delete',
        'label' => 'Remove',
    ],
    'options' => ['id' => $browserId] // set data browser id
]);
DynaGrid::end();

?>
</div>
<?php
// Set up data browser session
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$saveSelectedEntries = Url::to([ 'design/save-selected-entries' ]);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    const loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>'
    const program = '$program'
    const experimentDbId = '$id'
    const browserId = '$browserId'
    const planTemplateId = '$planTemplateId'
    const selectEntryType = '$selectEntryType'
    const saveSelectedEntries = '$saveSelectedEntries'
    const checkboxSessionStorageName = 'experimentEntryCheckboxIds-' + experimentDbId + '-' + planTemplateId
    const totalCheckboxesSessionStorageName = 'experimentEntryTotalCheckboxIds-' + experimentDbId + '-' + planTemplateId
    const selectAllModeSessionStorageName = 'experimentEntrySelectAllMode-' + experimentDbId + '-' + planTemplateId
    const totalCount = $totalCount

    sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')

    if (!sessionStorage.getItem(checkboxSessionStorageName))
        sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify([]))
    if (!sessionStorage.getItem(totalCheckboxesSessionStorageName))
        sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')
    if (!sessionStorage.getItem(selectAllModeSessionStorageName))
        sessionStorage.setItem(selectAllModeSessionStorageName, 'include')

    let checkedBoxes = JSON.parse(sessionStorage.getItem(checkboxSessionStorageName))
    let totalCheckboxes = parseInt(sessionStorage.getItem(totalCheckboxesSessionStorageName))
    let selectAllMode = sessionStorage.getItem(selectAllModeSessionStorageName)
    let isSelectAllChecked = null

    $(document).ready(function readyDocument () {
        adjustBrowserPageSize()
        refresh()
    })

    // Reinitialize event bindings after pjax reload
    $(document).on('ready pjax:success', function reinitializeEventBindingsAfterPjaxReload (e) {
        e.stopPropagation()
        e.preventDefault()
        refresh()
    })

    function refresh ()
    {
        // Set data browser height
        const maxHeight = ($(window).height() - 320)
        $(".kv-grid-wrapper").css("height", maxHeight)


        // Store total entry count
        totalCheckboxes = totalCount
        sessionStorage.setItem(totalCheckboxesSessionStorageName, totalCount)

        if (
            (selectAllMode === 'include' && totalCheckboxes === checkedBoxes.length) ||
            (selectAllMode === 'exclude' && checkedBoxes.length === 0)
        ) { // auto-tick Select All checkbox
            $('#checkbox-select-all').trigger('click')
        } else if (checkedBoxes.length > 0) { // auto-tick tracked checkedboxes
            $('#checkbox-select-all').prop('checked', false)
            $(".grid-select:checkbox").each(function () {
                let entryDataDbId = this.id
                
                if (
                    (selectAllMode === 'include' && checkedBoxes.includes(entryDataDbId)) ||
                    (selectAllMode === 'exclude' && !checkedBoxes.includes(entryDataDbId))
                ) {
                    $(this)
                        .prop('checked',true)
                        .parent("td")
                        .parent("tr")
                        .addClass('grey lighten-4')
                }
            });

            compareCheckBoxCountWithTotalCount()
        }
    }

    // prevent default behavior of checkbox
    $(document).on('click', '.grid-select', function clickCheckBox (e) {
        e.preventDefault()
        e.stopImmediatePropagation()
    })

    // select all entries
    $(document).on('click', '#checkbox-select-all', function clickSelectAllCheckbox (e) {
        if ($(this).prop("checked") === true) {
            isSelectAllChecked = true

            selectAllMode = 'exclude'
            sessionStorage.setItem(selectAllModeSessionStorageName, selectAllMode)

            $(this).prop('checked', true)
            $(".grid-select")
                .prop('checked',true)
                .parent("td")
                .parent("tr")
                .addClass("grey lighten-4")
        } else {
            isSelectAllChecked = false

            selectAllMode = 'include'
            sessionStorage.setItem(selectAllModeSessionStorageName, selectAllMode)

            $(this).prop('checked', false)
            $(".grid-select")
                .prop('checked',false)
                .parent("td")
                .parent("tr")
                .removeClass("grey lighten-4")
        }

        checkedBoxes = []
        
        sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
        
        compareCheckBoxCountWithTotalCount()
    })

    // click checkbox in entry browser
    $(document).on('click', `#\${browserId} tbody tr`, function clickTableRow (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        let thisRow = $(this).find('input:checkbox')[0]

        // Handle checkedBoxes
        if (checkedBoxes.includes(thisRow.id)) {
            checkedBoxes.splice(checkedBoxes.indexOf(thisRow.id), 1)
        } else {
            checkedBoxes.push(thisRow.id)
        }

        $('#' + thisRow.id).trigger('click')

        // Update clicked checkbox's state
        if (thisRow.checked) {
            $(this).addClass('grey lighten-4')
        } else {
            $(this).removeClass('grey lighten-4')
        }

        if (isSelectAllChecked) {
            isSelectAllChecked = false
            $('#checkbox-select-all').prop('checked', false)
        }

        sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
        compareCheckBoxCountWithTotalCount()
    })

    /**
     * Checks current state of checkedBoxes and updates text for summary count conditionally
     */
    function compareCheckBoxCountWithTotalCount ()
    {
        if (selectAllMode === 'exclude') {
            let currentCount = totalCheckboxes - checkedBoxes.length

            if (checkedBoxes.length > 0) {
                isSelectAllChecked = false
                $('#checkbox-select-all').prop('checked', false)
            } else {
                isSelectAllChecked = true
                $('#checkbox-select-all').prop('checked', true)
            }

            // update checked box counter text
            if (currentCount === 1) {
                $('#selected-items-count').html(currentCount)
                $('#selected-items-text').html('selected item.')
            } else if(currentCount > 1) {
                $('#selected-items-count').html(currentCount)
                $('#selected-items-text').html('selected items.')
            } else {
                $('#selected-items-count').html(totalCount)
                $('#selected-items-text').html('selected items.')
            }
        } else if (selectAllMode === 'include') {
            if (totalCheckboxes === checkedBoxes.length) {
                isSelectAllChecked = true
                $('#checkbox-select-all').prop('checked', true)
            } else {
                isSelectAllChecked = false
                $('#checkbox-select-all').prop('checked', false)
            }

            // update checked box counter text
            if (checkedBoxes.length === 1) {
                $('#selected-items-count').html(checkedBoxes.length)
                $('#selected-items-text').html('selected item.')
            } else if(checkedBoxes.length > 1) {
                $('#selected-items-count').html(checkedBoxes.length)
                $('#selected-items-text').html('selected items.')
            } else if (checkedBoxes.length === 0) {
                $('#selected-items-count').html('No')
                $('#selected-items-text').html('selected items.')
            }
        }
    }

    // prevent default behavior of checkbox
    $('#save-selection-entries').on('click',function(){
        let selectedIds = []
        selectedIds = JSON.parse(sessionStorage.getItem(checkboxSessionStorageName))

        $.ajax({
            type: 'POST',
            url: saveSelectedEntries,
            data: {
                program: program,
                id: experimentDbId,
                selectedIds: selectedIds.length === 0 ? JSON.stringify([]) : selectedIds,
                selectEntryType: selectEntryType
            },
            dataType: 'json',
            success: function (response) {
                
                let message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i> Successfully saved selected entries.'
                $('.toast').css('display','none')
                Materialize.toast(message, 5000)
            },
            error: function (response) {}
        })
    })
JS);

$this->registerCss('
    .help-block-error {
        color: red;
    }
');?>
