<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders list of created parameter sets
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

?>

<div class="row" style="margin-top:15px;">
    <div id="hidden-right-side-div" class="pull-left" style="display:none; width:25px;">
        <a class="tooltipped pull-right side-design-nav" id="show-right-design-nav"  href="#" data-tooltip="<?= \Yii::t('app', 'Show Experimental Design Parameters')?>" style="color:#616161 !important" data-position="top" ><i class="material-icons">chevron_right</i></a>
    </div>
    <a class="waves-effect waves-light btn pull-right green" id="generate-design-btn" title="Click to randomize">Submit</a>
    <a class="waves-effect waves-light btn pull-right grey" disabled="disabled" style="width: 20px;
    padding-left: 5px;margin-right:4px;" id="delete-all-btn" title="Click to delete generated design"><i class="material-icons">delete</i></a>
    <div class="pull-right" id="total-occurrences-notif"></div>
    <div style="width:250px;margin-left: 30px;margin-top: 20px;"><h6>List of Parameter Sets</h6></div>
</div>
<?php
echo $grid = GridView::widget([
    'pjax' => true, // pjax is set to always true for this demo
    'dataProvider' => $parameterSetsProvider,
    'id' => 'parameter-sets-browser', 
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'striped'=>false,
    'showPageSummary'=>false,
    'columns'=>$columns
]);

?>



