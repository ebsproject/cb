<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\dynagrid\DynaGrid;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/**
* Renders entry list browser for assigning replications
*/
$resetGridUrl = Url::toRoute(['design/assign-number-of-replications-to-entries']);
$resetParams = [
    'assign-number-of-replications-to-entries',
    'program' => $program,
    'id' => $id,
    'processId' => $processId,
];
if($entryTypeSet == 1){
    $resetParams =  [
        'assign-number-of-replications-to-entries',
        'program' => $program,
        'id' => $id,
        'processId' => $processId,
        'entryType' => $entryType
    ];
} 
if (!isset($model) || empty($model)) {
    Yii::$app->session->setFlash('error',
        \Yii::t('app', 'Unable to retrieve the entry records. Please try again or file a report for assistance.')
    );
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model' => $model,
    'program' => $program,
    'saveBtnLabel' => 'Next',
    'nextBtn' => $isNextButtonDisabled,
    'btnName' => 'next-btn',

]);

?>

<div class="col col-sm-12 view-entity-content">
<!-- Browse Entry List -->
<?php
$browserId = 'dynagrid-ec-assign-number-of-replications-to-entries';

$columns = [
    [ // Checkbox
        'label' => 'Checkbox',
        'header' => '
            <input type="checkbox" class="filled-in" id="checkbox-select-all" />
            <label for="checkbox-select-all" title="Select all entry records"></label>
        ',
        'content' => function ($data, $key, $index, $column) {
            $entryDbId = $data['entryDbId'];

            return "<input
                class='grid-select filled-in'
                type='checkbox'
                id='$entryDbId'
            /><label for='$entryDbId'></label>";
        },
        'hAlign'=>'center',
        'vAlign'=>'top',
        'hiddenFromExport'=>true,
        'mergeHeader'=>true,
        'order'=> DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryNumber',
        'label' => '<span title="Sequential numbering of the entries">'.Yii::t('app', 'Entry No.').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryName',
        'label' => '<span title="Gemplasm name">'.Yii::t('app', 'Germplasm Name').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryType',
        'label' => '<span title="Entry Type">'.Yii::t('app', 'Entry Type').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryRole',
        'label' => '<span title="Entry Role">'.Yii::t('app', 'Entry Role').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryClass',
        'label' => '<span title="Entry Class">'.Yii::t('app', 'Entry Class').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'label' => '<span title="Total NReps">'.Yii::t('app', 'Total NReps').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'order' =>  DynaGrid::ORDER_FIX_LEFT,
        'value' => function ($data) use ($repsArray) {
            $entryDbId = $data['entryDbId'];
            $dataValue = isset($repsArray[$entryDbId]) ? $repsArray[$entryDbId]:1;

            return "<input
                type='number'
                id='TIMES_REP-metadata-$entryDbId'
                class='editable-field numeric-editable-field'
                value='$dataValue'
                min='1'
                max=''
                step='any'>
                    <span
                        id='TIMES_REP-helper-text-$entryDbId'
                        class='helper-text hidden grey-text text-darken-4'
                    >Saving...</span>";
        },
    ],
    
];

// Set dynagrid data browser configuration
$dynagrid = DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize'=> true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsiveWrap' => false,
        'panel' => [
            'heading' => false,
            'before' => '<h5>' . \Yii::t('app', 'Total Number of Replicates per '.ucfirst($entryType)." ".'Entry of the Experiment') . '</h5>' .
                \Yii::t('app', 'Use the "Total NReps" column to update individual entries. Select multiple entries to bulk update.') . '&emsp;' .
                '{summary}<br/>
                <strong>Note</strong>: Changing individual NRep values are automatically saved.
                <p
                    id = "selected-items-paragraph"
                    class = "pull-right"
                    style = "margin-right: 4px"
                >
                    <b><span id="selected-items-count"> </span></b>
                    <span id="selected-items-text"> </span>
                </p>',
            'after' => false,
        ],
        'toolbar' => [
            [ 'content' => Html::a(
                    '<i class="material-icons inline-material-icon">arrow_back</i>',
                    [
                        '/experimentCreation/create/specify-design',
                        'program' => $program,
                        'id' => $id,
                        'processId' => $processId
                    ],
                    [
                        'id' => 'goto-design-tab',
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app', 'Go back to specifying design'),
                        'style' => 'margin-right: 4px;',
                    ]
                ) . Html::button(
                    '<i class="glyphicon glyphicon-edit"></i> ',
                    [
                        'type' => 'button',
                        'title' => Yii::t('app', 'Bulk update Total NReps'),
                        'class' => 'btn',
                        'id' => 'bulk-update-btn',
                        'data-toggle' => 'modal',
                        'data-target' => '#bulk-update-total-nreps-modal',
                        'style' => 'margin-right: 4px;',
                    ]
                ). Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    $resetParams,
                    [
                        'data-pjax' => true,
                        'id' => "reset-$browserId",
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app', 'Reset grid'),
                        'style' => 'margin-right: 4px;',
                    ]
                ) . '{dynagridFilter}{dynagridSort}{dynagrid}'
            ],
        ],
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-delete',
        'label' => 'Remove',
    ],
    'options' => ['id' => $browserId] // set data browser id
]);
DynaGrid::end();

// Modal for bulk updating Total NReps
Modal::begin([
    'id'=> 'bulk-update-total-nreps-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i>  '.\Yii::t('app','Bulk Update Total NReps').'</h4>',
    'footer' => Html::a(
            \Yii::t('app','Close'),
            [ '#' ],
            [
                'id' => 'close-bulk-update-btn',
                'title' => 'Cancel bulk updating',
                'data-dismiss' => 'modal'
            ]
        ) . '&emsp;&nbsp;' .
        '<a
            id="bulk-update-selected"
            class="btn btn-primary green waves-effect waves-light confirm-update-btn"
            title="' . \Yii::t('app', 'Bulk update Total NReps of ALL SELECTED entries') . '"
            mode="selected"
        >'.\Yii::t('app','Selected').'</a>&nbsp;'.
        '<a
            id="bulk-update-selected-page"
            class="btn btn-primary green waves-effect waves-light confirm-update-btn"
            title="' . \Yii::t('app', 'Bulk update Total NReps of SELECTED entries within the CURRENT PAGE') . '"
            mode="current-page"
        >'.\Yii::t('app','Selected in Page').'</a>&nbsp;'.
        '<a
            id="bulk-update-all"
            class="btn btn-primary teal waves-effect waves-light confirm-update-btn"
            title="' . \Yii::t('app', 'Bulk update Total NReps of ALL available entries') . '"
            mode ="all"
        >'.\Yii::t('app','All').'</a>',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static', 'tabindex' => false],
]);
    echo '<div id = "modal-progress" class = "progress hidden"><div class="indeterminate"></div></div>';
    echo '<div id = "modal-content">
        <p id="instructions" class="row">Changes will be reflected upon choosing an update option provided below.</p>';
    echo '<span id="note"></span>';
    echo '<div
        id="config-var"
        style="margin-left:30px;"
        class="row"
    >
        <div class="col s4">
            <input
                required
                type="number"
                id="TIMES_REP-number-field"
                class="numeric-editable-field"
                placeholder="1"
                min="1"
                max="
                step="any"
            >
                    <span
                        id="TIMES_REP-number-field-helper-text"
                        class="grey-text text-darken-1"
                    >Enter a number here</span>
        </div>
    </div></div>';
Modal::end();
?>
</div>
<?php
$saveRowFieldUrl = Url::to([ 'design/save-row-field-nonsparse' ]);
$bulkUpdateTotalNrepsUrl = Url::to([ 'design/bulk-update-total-nreps-nonsparse' ]);

// Set up data browser session
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    const loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>'
    const program = '$program'
    const experimentDbId = '$id'
    const saveRowFieldUrl = '$saveRowFieldUrl'
    const bulkUpdateTotalNrepsUrl = '$bulkUpdateTotalNrepsUrl'
    const browserId = '$browserId'
    const checkboxSessionStorageName = 'experimentEntryCheckboxIds-' + experimentDbId
    const totalCheckboxesSessionStorageName = 'experimentEntryTotalCheckboxIds-' + experimentDbId
    const selectAllModeSessionStorageName = 'experimentEntrySelectAllMode-' + experimentDbId
    let totalCount = $totalCount
    const entryTypeSet = '$entryTypeSet'
    const entryType = '$entryType'
    const repType = '$repType'

    sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')

    if (!sessionStorage.getItem(checkboxSessionStorageName))
        sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify([]))
    if (!sessionStorage.getItem(totalCheckboxesSessionStorageName))
        sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')
    if (!sessionStorage.getItem(selectAllModeSessionStorageName))
        sessionStorage.setItem(selectAllModeSessionStorageName, 'include')

    let checkedBoxes = JSON.parse(sessionStorage.getItem(checkboxSessionStorageName))
    let totalCheckboxes = parseInt(sessionStorage.getItem(totalCheckboxesSessionStorageName))
    let selectAllMode = sessionStorage.getItem(selectAllModeSessionStorageName)
    let isSelectAllChecked = null

    $(document).ready(function readyDocument () {
        adjustBrowserPageSize()
        refresh()

        //disable filter for entry type if entryType is not null
        if(entryTypeSet === '1'){
            $('input[name="ExperimentEntryModel[entryType]"').attr('disabled', 'disabled')
            $('input[name="ExperimentEntryModel[entryRole]"').attr('disabled', 'disabled')
        }
    })

    // Reinitialize event bindings after pjax reload
    $(document).on('ready pjax:success', function reinitializeEventBindingsAfterPjaxReload (e) {
        e.stopPropagation()
        e.preventDefault()
        refresh()
        
        //disable filter for entry type if entryType is not null
        if(entryTypeSet === '1'){
            $('input[name="ExperimentEntryModel[entryType]"').attr('disabled', 'disabled')
            $('input[name="ExperimentEntryModel[entryRole]"').attr('disabled', 'disabled')
        }
    })

    function refresh ()
    {
        // Set data browser height
        const maxHeight = ($(window).height() - 320)
        $(".kv-grid-wrapper").css("height", maxHeight)


        // Store total entry count
        totalCount = $('#$browserId').find('.summary').children().eq(1).html();
        totalCheckboxes = totalCount
        sessionStorage.setItem(totalCheckboxesSessionStorageName, totalCount)

        if (
            (selectAllMode === 'include' && totalCheckboxes === checkedBoxes.length) ||
            (selectAllMode === 'exclude' && checkedBoxes.length === 0)
        ) { // auto-tick Select All checkbox
            $('#checkbox-select-all').trigger('click')
        } else if (checkedBoxes.length > 0) { // auto-tick tracked checkedboxes
            $('#checkbox-select-all').prop('checked', false)
            $(".grid-select:checkbox").each(function () {
                let entryDbId = this.id
                
                if (
                    (selectAllMode === 'include' && checkedBoxes.includes(entryDbId)) ||
                    (selectAllMode === 'exclude' && !checkedBoxes.includes(entryDbId))
                ) {
                    $(this)
                        .prop('checked',true)
                        .parent("td")
                        .parent("tr")
                        .addClass('grey lighten-4')
                }
            });

            compareCheckBoxCountWithTotalCount()
        }

        // Save Total NReps value upon input
        $('.editable-field').on('input', function (e) {
            e.stopImmediatePropagation()

            let id = this.id
            let fields = id.split('-')

            let entryDbId = fields[2]
            let value = $('#' + id).val()

            if (!value) { // Prep and display "Please enter a number here" helper text
                $(`#TIMES_REP-helper-text-\${entryDbId}`).removeClass('green-text text-darken-4')
                $(`#TIMES_REP-helper-text-\${entryDbId}`).removeClass('grey-text text-darken-1')
                $(`#TIMES_REP-helper-text-\${entryDbId}`).addClass('red-text text-darken-2')

                $(`#TIMES_REP-helper-text-\${entryDbId}`).removeClass('hidden')
                
                $(`#TIMES_REP-helper-text-\${entryDbId}`).html('Please enter a number here')

                return
            }

            // Prep "Saving..." helper text
            $(`#TIMES_REP-helper-text-\${entryDbId}`).removeClass('green-text text-darken-4')
            $(`#TIMES_REP-helper-text-\${entryDbId}`).removeClass('red-text text-darken-2')
            $(`#TIMES_REP-helper-text-\${entryDbId}`).addClass('grey-text text-darken-4')

            $(`#TIMES_REP-helper-text-\${entryDbId}`).html('Saving...')

            // Show helpertext
            $(`#TIMES_REP-helper-text-\${entryDbId}`).removeClass('hidden')

            // Save the changes in the database
            $.ajax({
                url: saveRowFieldUrl,
                type: 'post',
                datatType: 'json',
                data: {
                    entryDbId: entryDbId,
                    value: value,
                    entryType: entryType,
                    id: experimentDbId,
                    repType: repType
                },
                success: function (response) {
                    $(`#TIMES_REP-helper-text-\${entryDbId}`).removeClass('grey-text text-darken-4')
                    $(`#TIMES_REP-helper-text-\${entryDbId}`).addClass('green-text text-darken-4')
                    $(`#TIMES_REP-helper-text-\${entryDbId}`).html('Changes saved.')
                },
            })
        })

        // Hide helper text when user leaves Total NReps input field
        $('.editable-field').on('focusout', function (e) {
            e.stopImmediatePropagation()

            let id = this.id
            let fields = id.split('-')

            let entryDbId = fields[2]
            let value = $('#' + id).val()

            $(`#TIMES_REP-helper-text-\${entryDbId}`).addClass('hidden')
            $(`#TIMES_REP-helper-text-\${entryDbId}`).removeClass('green-text text-darken-4')
            $(`#TIMES_REP-helper-text-\${entryDbId}`).removeClass('red-text text-darken-2')
            $(`#TIMES_REP-helper-text-\${entryDbId}`).addClass('grey-text text-darken-4')
            $(`#TIMES_REP-helper-text-\${entryDbId}`).html('Saving...')
        })

        // Bulk update Total NReps based on the selected update option
        $('.confirm-update-btn').on('click', function beginBulkUpdatingTotalNReps (e) {
            e.preventDefault();
            e.stopImmediatePropagation()

            const obj = $(this)
            const mode = obj.attr('mode')
            const value = $('#TIMES_REP-number-field').val().trim()
            let selectedIds = []

            // User has not entered a non-zero input
            if (!value) {
                $('#TIMES_REP-number-field').addClass('has-error')

                $('#TIMES_REP-number-field-helper-text').removeClass('grey-text text-darken-1')
                $('#TIMES_REP-number-field-helper-text').addClass('red-text text-darken-2')
                $('#TIMES_REP-number-field-helper-text').html('Please enter a number here')
                
                return
            }
            
            if (mode === 'current-page') {
                $('.grid-select').each(function () {
                    if ($(this).prop('checked')) {
                        selectedIds.push($(this).attr('id'))
                    }
                })
            } else if (mode === 'selected') {
                selectedIds = JSON.parse(sessionStorage.getItem(checkboxSessionStorageName))
            }
            
            // If no entries were selected
            if (
                (mode === 'current-page' || mode === 'selected') &&
                selectedIds.length === 0 &&
                selectAllMode == 'include'
            ) {
                message = '<i class="material-icons orange-text" style="margin-right: 8px;">warning</i> Please select at least one (1) entry data'
                $('.toast').css('display','none')
                Materialize.toast(message, 5000)
                
                return
            }
            
            $('#modal-progress').removeClass('hidden')

            $.ajax({
                type: 'POST',
                url: bulkUpdateTotalNrepsUrl,
                data: {
                    program: program,
                    id: experimentDbId,
                    selectedIds: selectedIds.length === 0 ? JSON.stringify([]) : selectedIds,
                    selectAllMode: (mode === 'current-page') ? 'include' : selectAllMode,
                    mode: mode,
                    value: value,
                    repType: repType
                },
                dataType: 'json',
                success: function (response) {
                    let message

                    if (response['success']) {
                        $('#modal-progress').addClass('hidden')
                        $('#bulk-update-total-nreps-modal').modal('hide')

                        // Refresh data browser
                        $.pjax.reload({
                            container: `#\${browserId}-pjax`,
                        })

                        message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i>' + response['message']
                    } else {
                        message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + response['message']
                    }

                    $('.toast').css('display','none')
                    Materialize.toast(message, 5000)
                },
                error: function (response) {}
            })
        })

        // Validate Total NReps input
        $('.numeric-editable-field').on('keypress', function validateTotalNRepsInputFieldOnKeypress (e) {
            const totalNRepsAcceptedFormat = /^[1-9]\d*/
            const candidateValue = $(this).val() + e.key

            if (!totalNRepsAcceptedFormat.test(candidateValue)) {
                e.preventDefault()
            }
        })

        // Reset CSS of Total NReps input field
        $('#TIMES_REP-number-field').on('input', function resetCssOfTotalNRepsInputField (e) {
            $('#TIMES_REP-number-field').removeClass('has-error')

            $('#TIMES_REP-number-field-helper-text').removeClass('red-text text-darken-2')
            $('#TIMES_REP-number-field-helper-text').addClass('grey-text text-darken-1')
            $('#TIMES_REP-number-field-helper-text').html('Enter a number here')
        })

        // Clear TOtal NReps input field on modal close
        $('#bulk-update-total-nreps-modal').on('hidden.bs.modal', function clearTotalNrepsInputField (e) {
            $('#TIMES_REP-number-field').val('')
        })

        // Set focus on Total NReps input field
        $('#bulk-update-total-nreps-modal').on('shown.bs.modal', function setFocusOnTotalNrepsInputField (e) {
            $('#TIMES_REP-number-field').focus()
            $('#TIMES_REP-number-field').val('1')
        })
    }

    // prevent default behavior of checkbox
    $(document).on('click', '.grid-select', function clickCheckBox (e) {
        e.preventDefault()
        e.stopImmediatePropagation()
    })

    // select all entries
    $(document).on('click', '#checkbox-select-all', function clickSelectAllCheckbox (e) {
        if ($(this).prop("checked") === true) {
            isSelectAllChecked = true

            selectAllMode = 'exclude'
            sessionStorage.setItem(selectAllModeSessionStorageName, selectAllMode)

            $(this).prop('checked', true)
            $(".grid-select")
                .prop('checked',true)
                .parent("td")
                .parent("tr")
                .addClass("grey lighten-4")
        } else {
            isSelectAllChecked = false

            selectAllMode = 'include'
            sessionStorage.setItem(selectAllModeSessionStorageName, selectAllMode)

            $(this).prop('checked', false)
            $(".grid-select")
                .prop('checked',false)
                .parent("td")
                .parent("tr")
                .removeClass("grey lighten-4")
        }

        checkedBoxes = []
        
        sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
        
        compareCheckBoxCountWithTotalCount()
    })

    // click checkbox in entry browser
    $(document).on('click', `#\${browserId} tbody tr`, function clickTableRow (e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        let thisRow = $(this).find('input:checkbox')[0]

        // Handle checkedBoxes
        if (checkedBoxes.includes(thisRow.id)) {
            checkedBoxes.splice(checkedBoxes.indexOf(thisRow.id), 1)
        } else {
            checkedBoxes.push(thisRow.id)
        }

        $('#' + thisRow.id).trigger('click')

        // Update clicked checkbox's state
        if (thisRow.checked) {
            $(this).addClass('grey lighten-4')
        } else {
            $(this).removeClass('grey lighten-4')
        }

        if (isSelectAllChecked) {
            isSelectAllChecked = false
            $('#checkbox-select-all').prop('checked', false)
        }

        sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
        compareCheckBoxCountWithTotalCount()
    })

    /**
     * Checks current state of checkedBoxes and updates text for summary count conditionally
     */
    function compareCheckBoxCountWithTotalCount ()
    {
        if (selectAllMode === 'exclude') {
            let currentCount = totalCheckboxes - checkedBoxes.length

            if (checkedBoxes.length > 0) {
                isSelectAllChecked = false
                $('#checkbox-select-all').prop('checked', false)
            } else {
                isSelectAllChecked = true
                $('#checkbox-select-all').prop('checked', true)
            }

            // update checked box counter text
            if (currentCount === 1) {
                $('#selected-items-count').html(currentCount)
                $('#selected-items-text').html('selected item.')
            } else if(currentCount > 1) {
                $('#selected-items-count').html(currentCount)
                $('#selected-items-text').html('selected items.')
            } else {
                $('#selected-items-count').html(totalCount)
                $('#selected-items-text').html('selected items.')
            }
        } else if (selectAllMode === 'include') {
            if (totalCheckboxes === checkedBoxes.length) {
                isSelectAllChecked = true
                $('#checkbox-select-all').prop('checked', true)
            } else {
                isSelectAllChecked = false
                $('#checkbox-select-all').prop('checked', false)
            }

            // update checked box counter text
            if (checkedBoxes.length === 1) {
                $('#selected-items-count').html(checkedBoxes.length)
                $('#selected-items-text').html('selected item.')
            } else if(checkedBoxes.length > 1) {
                $('#selected-items-count').html(checkedBoxes.length)
                $('#selected-items-text').html('selected items.')
            } else if (checkedBoxes.length === 0) {
                $('#selected-items-count').html('No')
                $('#selected-items-text').html('selected items.')
            }
        }
    }
JS);

$this->registerCss('
    #TIMES_REP-number-field.has-error {
      border-bottom: 1px solid red;
      box-shadow: 0 1px 0 0 red;
    }

    .help-block-error {
        color: red;
    }
');?>
