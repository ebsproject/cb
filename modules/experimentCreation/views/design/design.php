<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders design step for experiment creation (new UI)
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use app\models\Program;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use macgyer\yii2materializecss\widgets\Button;


$disabled = '';
if (strpos($model['experimentStatus'],'design generated') === false) {
    $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model'=>$model,
    'program'=>$program,
    'saveBtnLabel'=>'Next',
    'nextBtn' => $disabled,
    'btnName' => 'no_action_btn'
]);

$assignUrl = Url::to(['/experimentCreation/design/assign-number-of-replications-to-entries', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>
<div class="row col col-md-12" style="margin-top:10px;padding-top:10px; ">
    <div class="col col-md-12">
        <div class="col col-md-6">
            <div id="warning-design-div-panel" class="panel-warning hidden"><div id="warning-design-div-msg-panel" class="card-panel"></div></div>
            <div id="error-design-div-panel" class="panel-error hidden"><div id="error-design-div-msg-panel" class="card-panel"></div></div>
            <div id="notif_field" class="hidden"></div>

            
    	    <?php

    			echo $grid = GridView::widget([
    				'pjax' => true, // pjax is set to always true for this demo
    				'dataProvider' => $entryInfoDataProvider,
    				'id' => 'entry-info-design-browser', 
    				'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-seed-lot-table'],
    				'striped'=>false,
    				'showPageSummary'=>false,
    				'columns'=>[
    					[
    						'attribute'=>'test',
    						'label' => Yii::t('app', 'No. of Test Entries'),
    						'format' => 'raw',
    						'enableSorting' => false,
    					],  
    					[
    						'attribute'=>'check',
    						'label' => Yii::t('app', 'No. of Checks'),
    						'format' => 'raw',
    						'enableSorting' => false,
    					], 
                        [
                            'attribute'=>'local',
                            'label' => Yii::t('app', 'No. of Local checks'),
                            'format' => 'raw',
                            'enableSorting' => false,
                        ], 
    					[
    						'attribute'=>'total',
    						'label' => Yii::t('app', 'Total Entries'),
    						'format' => 'raw',
    						'enableSorting' => false,
    					]
    				]
    	        ]);
            ?>
            <div class="row" style="margin-bottom:0px !important">
                <div class="col col-sm-10" style="margin-left:20px; margin-bottom: 10px;">
                   	<div class="col col-sm-5">
                  	<?php
                      	//Switch for the new UI
                      	echo '<label class="control-label">'.\Yii::t('app', 'Across Environment Design').'</label>';

                      	echo Select2::widget([
                          	'name' => 'ExperimentDesign[ACROSS_ENV_DESIGN]',
                          	'value' => isset($acrossEnvDesign) ? $acrossEnvDesign:'none',// initial value
                          	'data' => ['none'=>'None', 'random'=>'Random'], //this will be hardcoded for now
                          	'id' => 'acrossEnvDesign_id',
                          	'options' => ['placeholder' => Yii::t('app', 'Select value'), 'class' => 'select2-Drop pull-left', 'style'=>'margin-top:25px;'
                          	],
                      	]);
                  	?>
                 	</div>
                </div>
                <div class="col col-sm-12" style="margin-left:25px;">
                	<div class="row" style="margin-bottom:0px !important">
                        <!-- switch for generate or upload workflow -->
                    
                    <form method="POST" id="additional-details-form" accept-charset="UTF-8" role="form" class="form-horizontal re-form form-inv">
                        <div class="col" style="margin-top:5px;">
                            <label class="cbx-label " title="Design workflow transaction" for="Design workflow" style="font-size: 1rem;color: #9e9e9e;font-weight: bold;">
                                <label data-label="Design workflow" for="workflow-switch-id">Design Workflow</label>
                            </label>
                            <div class="row switch">
                                <label>Generate&nbsp;
                                    <input type="checkbox" id="workflow-switch-id" class="boolean-input-validate " name="Additional[workflow-switch-id]" value="" <?=$workflow== 'upload' ? "checked":""?> >
                                    <span class="lever "></span>Upload&nbsp;
                                </label>
                            </div>
                        </div>

                    	<?php
                            foreach($addDesignFields as $field){
                                if(isset($field['value'])) { 
                                    echo $field['value'];
                                }
                            }
                    	?>
                    </form>
                    <?php

                        if($workflow == 'upload'){
                            echo '&nbsp;<a id="upload_design_array" href="#" class="grey lighten-3 black-text btn btn-primary waves-effect waves-light " style="margin-top:25px"><i class="material-icons">file_upload</i></a>';
                        } else {

                            echo '<a id="assign_rep_btn" href="#" class="btn btn-primary waves-effect waves-light" style="margin-top:10px;margin-bottom:5px;"><span title="Assign total no. of replicates per entry" >'.\Yii::t('app', 'Assign').'</span></a>';
                        }
                    ?>
                	</div>
                </div>
            </div>
        </div>
        <div class="col pull-right" style="margin-top:5px;">
            <div class="row switch" title="Switch design user interface" >
                <label>New interface&nbsp;
                    <input type="checkbox" id="designui-switch-id" class="boolean-input-validate " name="designui-switch-name" value="" <?=$designUiSwitch != 'new' ? "checked":""?> >
                    <span class="lever "></span>Old interface&nbsp;
                </label>
            </div>
        </div>
    </div>


    <div id="mod-notif" class="hidden"></div>
    <div class="row">
    <div id="experiment-design-fields-id" class="col col-md-3">
    	<form method="POST" id="experiment-design-form" accept-charset="UTF-8" role="form" class="form-horizontal re-form form-inv">
	        <div style="margin-top:10px;">
	          <div id="experiment_design_fields">
                
	                <div id="right-side-panel-div" class="view-entity-content col col-md-12">
                      	<div id="experimental-design-div">
                        <ul style="padding:5px 0px;" >
                            <li class="header">

                                 <a href="#" class="waves-effect waves-light btn btn-flat filter-more-actions dropdown-filter-button"  id="hide-right-design-nav" style="top:10px;float:right;right:0px;background-color: #ccc !important;padding:1px 4px !important;" title="<?= \Yii::t('app', 'Hide Experimental Design Parameters') ?>" ><i class="material-icons" style="color:#616161 !iportant;height: 30px;line-height: 30px;margin: 0;">chevron_left</i></a>
                                 <?php if($workflow != 'upload') {?>
                                    <a href="#" class="waves-effect waves-light btn  btn-flat save-data-filter-btn "  id="save-parameters-btn" style="background-color: #009688 !important;top:10px;float:right;right:2px;max-width: 60px;color:#fff !important;" title="<?= \Yii::t('app', 'Save parameters') ?>"><?= \Yii::t('app', 'Save') ?></a>
                                <?php }?>
                            </li>
                        </ul>
                        <?php

                        echo '<div class="row" style="margin: 5px;"><b><h6>Occurrence Design</h6></b></div>';

                        echo '<div class="row" style="margin-left: 10px;">';
                        
                        echo '<label class="control-label">'.\Yii::t('app', 'Design').'</label>';
                        if(isset($experimentDesign) && $experimentDesign != null){ //add info button
                            $designTitle = isset($designDesc[$experimentDesign]) ? $designDesc[$experimentDesign] : 'Please select a design.';
                            $hidden='';
                        } else {
                            $designTitle = 'Please select a design.';
                            $hidden = 'hidden';
                        }
                        echo Select2::widget([
                            'name' => 'ExperimentDesign[DESIGN]',
                            'value' => isset($experimentDesign) ? $experimentDesign:'',// initial value
                            'data' => $designArray,
                            'id' => 'design_select_id',
                            'options' => ['placeholder' => Yii::t('app', 'Select design'), 'class' => 'select2-Drop pull-left', 'style'=>'margin-bottom:15px;'
                            ],
                             'addon' => [
                                  'append' => [
                                      'content' => '<i class="material-icons" '.$hidden.' title="'.Yii::t('app', $designTitle).'">help</i>'
                                  ]
                              ]
                        ]);
                        

                        echo '</div>';

                        $showField = ($showLayout)? "hidden" :"";
                        foreach($designFields as $field){

                            if($field['abbrev'] == 'TEST_ENTRIES'){
                                echo '<div class="row"></div>'; 
                                echo '<div class="row" style="margin-bottom: 0px;"><b><h6>Experimental Design Parameters</h6></b></div>';
                                echo '<div id="total-entries-id" style="margin: 5px;font-weight: bold;font-size: .8rem;color:#616161;"></div>';
                                echo $field['value'];
                            } else if($field['abbrev']=='MAX_PLOTS'){

                                echo '<div id="max-plot-fields" class="'.$showField.'">';
                                echo $field['value'];
                                echo '</div>';
                            }
                            else if(isset($field['value'])) { 
                                echo $field['value'];
                            }
                        }

			        	echo '</div>';
                        if(!$showLayout){

                        }
			        	?>
                        <div id="layout-design-fields" class="row <?=($showLayout)? "" :"hidden"?>" style="margin-bottom: 10px;">
				            <div class="col col-md-10"><h7>Field Layout Details</h7></div>
			              	<?php
			                  foreach($layoutFields as $field){
			                      	if($field['abbrev']=='PERCENT_CHECK_PLOTS' && !$withChecks){
			                          	continue;
			                      	}
			                      	else if(isset($field['value'])) { 
			                          	echo $field['value'];
			                      	}
			                  }
                              echo "<br>";
			                ?>
			        	</div>

                        </div>
 </div>
                      
           
	            </div>
            </form>
	    </div>	    
    
        <div class="col col-md-9 pull-right view-entity-content" style="margin-top:10px;overflow: hidden !important;" id="parameter-sets-panel">

            <?php 
                echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/design/parameter_sets.php',[
                    'model' => $model,
                    'program' => $program,
                    'id' => $id,
                    'processId' => $processId,
                    'parameterSetsProvider' => $parameterSetsProvider,
                    'columns' => $columns
                ]);
            ?>
        </div>
</div>

<?php
Modal::begin([
    'id' => 'design-recently-used-tools-modal'
]);
Modal::end();

//Modal for delete plots
Modal::begin([
  'id' => 'plot-records-confirm-modal',
  'header' => '<h4><div id="plot-records-confirm-header"><i class="left material-icons orange-text">warning</i>Delete Plots</h4>',
  'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
      '<a id="plots_delete_btn" class="btn btn-primary waves-effect waves-light confirm-delete-plots-btn">Confirm</a>',
  'size' =>'modal-lg',
  'options' => ['data-backdrop'=>'static'],
]);
echo '<div id="plot-records-modal-confirm"></div>';
Modal::end();


// upload design array modal
Modal::begin([
  'id' => 'upload-design-array-modal',
  'header' => '<h4>
          <i class="material-icons" style="vertical-align:bottom">file_upload</i>
          Upload design array
      </h4>',
  'footer' =>
  Html::a('Cancel', [''], [
      'id' => 'cancel-upload-design-array',
      'class' => 'modal-close-button',
      'title' => \Yii::t('app', 'Cancel'),
      'data-dismiss' => 'modal'
  ]) . '&emsp;&nbsp;'
      . Button::widget([
          'label' => 'Confirm',
          'icon' => [
              'name' => 'send',
              'position' => 'right'
          ],
          'options' => [
              'class' => 'confirm-upload-design-array hidden green darken-3',
              'style' => 'margin-right: 34px;',
              'href' => '#!',
              'title' => \Yii::t('app', 'Confirm upload design array')
          ],
      ])
      . Button::widget([
          'label' => 'Next',
          'icon' => [
              'name' => 'arrow_forward',
              'position' => 'right'
          ],
          'options' => [
              'class' => 'next-upload-design-array hidden',
              'style' => 'margin-right: 34px;',
              'href' => '#!',
              'title' => \Yii::t('app', 'Go to preview')
          ],
      ]). Button::widget([
          'label' => 'Validate',
          'icon' => [
              'name' => 'arrow_forward',
              'position' => 'right'
          ],
          'options' => [
              'class' => 'validate-upload-design-array hidden',
              'style' => 'margin-right: 34px;',
              'href' => '#!',
              'title' => \Yii::t('app', 'Validate and go to preview')
          ],
      ]),
  'closeButton' => [
      'class' => 'hidden'
  ],
  'size' => 'modal-lg',
  'options' => [
      'data-backdrop' => 'static',
      'class' => 'no-right-margin'
  ],
]);
echo '<div class="upload-design-array-form-modal-body parent-panel"></div>';
echo '<div class="upload-design-array-preview-modal-body child-panel"></div>';
Modal::end();

?>

<?php
$saveEnvDesignUrl = Url::to(['/experimentCreation/design/save-env-value', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$selectDesignUrl = Url::to(['/experimentCreation/design/change-design', 'id'=>$id, 'program'=>$program]);
$currentUrl = Url::to(['/experimentCreation/create/specify-design', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$saveParamsUrl =  Url::to(['/experimentCreation/design/save-parameters', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$saveAddtlUrl =  Url::to(['/experimentCreation/design/save-addtl-details', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$deleteUrl =  Url::to(['/experimentCreation/design/delete-param-set', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$selectEntriesUrl = Url::to(['/experimentCreation/design/selection-entries', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$checkJsonUrl = Url::to(['/experimentCreation/design/check-design-results', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$submitUrl = Url::to(['design/submit-randomization', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$updateUrl =  Url::to(['/experimentCreation/design/update-param-set', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$validateUrl =  Url::to(['/experimentCreation/design/validate-list', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$deleteJsonUrl = Url::to(['create/delete-plots', 'id'=>$id, 'program'=>$program]);
$updateWorkflowUrl = Url::to(['/experimentCreation/design/update-workflow', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$renderFormUploadDesignArray = Url::to(['upload/render-file-upload-form','id'=>$id, 'program'=>$program, 'entityType' => 'parameter_sets']);
$updateDesignUiUrl = Url::to(['/experimentCreation/design/update-design-ui', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$this->registerJsFile("@web/js/generic-form-validation.js", [
  'depends' => ['app\assets\AppAsset'],
  'position'=>\yii\web\View::POS_END
]);
$this->registerJsFile("@web/js/parser/token.js", [
  'depends' => ['app\assets\AppAsset'],
  'position' => \yii\web\View::POS_END
]);
$this->registerJs(<<<JS
	var entryInfo = JSON.parse('$entryInfo')
    var totalEntries = nExptEntries = parseInt(entryInfo['total'])
    var testEntries = nExptTest = parseInt(entryInfo['test'])
    var checkEntries = nExptCheck = parseInt(entryInfo['check'])
    var localChecks = nExptLocalCheck = parseInt(entryInfo['local'])
    var generateLayoutShow = '$generateLayoutShow'
    var paramValues = JSON.parse('$paramValues')
    var design = ($("#design_select_id option:selected").html()).toLowerCase()
    let totalOccurrences = nExptTrial = parseInt($totalOccurrences)
    var assignedOccurrences = parseInt($assignedOccurrences)
    var expStatus = '$expStatus'
    var planStatus = '$planStatus'
    var incompleteSet = '$incompleteSet'
    var deletePlots = '$deletePlots'
    var workflow = '$workflow'
    var planRecordCreated = '$planRecordCreated'
    var updateDesignFromEntry = '$updateDesignFromEntry'
    var totalChecks = nExptTCheck = nExptCheck + nExptLocalCheck
    var totalPlots = 0
    var nEntry = nTest = nCheck = nLocalCheck = 0
    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';

    //add entry information in the params array for easy accessing
    paramValues['totalEntries']=totalEntries
    paramValues['nExptEntries']=totalEntries
    paramValues['nExptTest']=nExptTest
    paramValues['nExptCheck']=nExptCheck
    paramValues['nExptLocalCheck']=nExptLocalCheck
    paramValues['nExptTrial']=nExptTrial
    paramValues['nRep']=1
    paramValues['nExptTCheck']=nExptTCheck
    paramValues['totalPlots']=totalPlots

    var rules = JSON.parse('$rules')
    var validationRules = rules['validation-design']
    var defaultRules = rules['default-value']
    var allowedValRules = rules['allowed-value']
    var row = 0
    var col = 0
    var totalPlots = 0

    var changeFlag = 0
    var errorArray = []
    var errorArrayField = []
    var warningArray = []
    var flag = 0
    var initflag = 0
    var addtlValidation = {}
    var addtlValidationFlag = false

    checkResults()
    setInterval(function(){
        checkResults()
    }, 3000)

    function checkResults(){
        if(expStatus.includes('design requested') || planStatus == 'deletion in progress' || planStatus == 'randomization in progress'|| planStatus == 'in progress' || planStatus == 'upload in progress'){

            $.ajax({
                url: '$checkJsonUrl',
                type: 'post',
                dataType: 'json',
                data: {},
                success: function(response) {
                    expStatus = response.expStatus;
                    deletePlots = response.deletePlots;
                    planStatus = response.planStatus;
                    var message = response.message;
                    if(response.success == 'deletion in progress'){
                        //to be filled next sprint
                        $('#generate-design-btn').attr('disabled', 'disabled');
                        $("#delete-all-btn").attr('disabled', 'disabled');
                        $('#assign_rep_btn').attr('disabled', 'disabled');
                        $("#acrossEnvDesign_id").attr('disabled', 'disabled');
                        $("#designui-switch-id").attr('disabled', 'disabled');
                    }else if(response.success == 'delete successful'){
                        //to be filled next sprint
                        $('#generate-design-btn').attr('disabled', 'disabled');
                        $("#delete-all-btn").attr('disabled', 'disabled');

                        $('#assign_rep_btn').removeAttr('disabled');
                        $("#acrossEnvDesign_id").removeAttr('disabled');
                        $("#designui-switch-id").removeAttr('disabled');

                        var notif = "<i class='material-icons green-text left'>check</i> Plots successfully deleted. Please proceed in submitting your request.";
                            Materialize.toast(notif,5000);

                        location.reload()
                    }else if(response.success == 'deletion failed'){
                        //to be filled next sprint
                        $('#generate-design-btn').attr('disabled', 'disabled');
                        $("#delete-all-btn").removeAttr('disabled');
                        $('#assign_rep_btn').attr('disabled', 'disabled');
                        $("#acrossEnvDesign_id").attr('disabled', 'disabled');
                        $("#designui-switch-id").attr('disabled', 'disabled');
                        
                        var notif = "<i class='material-icons green-text left'>check</i> Plots deletion failed.";
                            Materialize.toast(notif,5000);

                        location.reload();
                    } else if(response.success !== 'failed'){
                        // enableFields(false);
                        if(response.success == 'success'){
                            enableAddtlFields(false)
                            $('#generate-design-btn').attr('disabled', 'disabled');
                            $("#delete-all-btn").removeAttr('disabled');
                            $('#assign_rep_btn').attr('disabled', 'disabled');
                            $("#acrossEnvDesign_id").attr('disabled', 'disabled');
                            $("#designui-switch-id").attr('disabled', 'disabled');

                            var notif = "<i class='material-icons green-text left'>check</i> Successfully randomized.";
                            Materialize.toast(notif,5000);

                            location.reload();
                        } 
                    }else if(response.success == 'in progress'){                   
                        enableAddtlFields(false)
                        $('#assign_rep_btn').attr('disabled', 'disabled');
                        $("#delete-all-btn").attr('disabled', 'disabled');
                        $('#generate-design-btn').attr('disabled', 'disabled');
                        $("#acrossEnvDesign_id").attr('disabled', 'disabled');
                        $("#designui-switch-id").attr('disabled', 'disabled');
                    }
                    else {
                        enableAddtlFields(true)                        
                        $('#generate-design-btn').removeAttr('disabled');
                        $("#delete-all-btn").attr('disabled', 'disabled');
                        var notif = "<i class='material-icons red-text left'>close</i> Randomization failed. Please try again. <br>"+message;
                        Materialize.toast(notif,5000);
                        location.reload();
                    }
                },
                error: function(e) {
                    console.log(e);
                }
            });
        }
    }

    $("document").ready(function(){
        validateDesignRules("");
        placeDefault();
        computeTotalPlots()
        updateEntriesInfo();
        updateOccurrencesInfo();

        // if(updateDesignFromEntry == 'updatedEntryList' || updateDesignFromEntry == 'updated entries'){
        //     $("#plots_delete_btn").trigger('click');
        // }
        
        // check if there are empty fields
        $(".numeric-input-validate").each(function(){
            
            if(this.value == '' || this.value == undefined){
                flag = 1;
            }
        });
        checkFields();
        if(flag == 0 && initflag == 0){
            $("#save-parameters-btn").removeAttr('disabled');
        } else {
            $("#save-parameters-btn").attr('disabled', 'disabled');
        }

        if($('[name="ExperimentDesign[genLayout]').prop('checked') == true){
            if($('[name="ExperimentDesign[genLayout]"').prop('checked') == true){
                $("#layout-design-fields").removeClass('hidden');
            } else{
                $("#layout-design-field").addClass('hidden');
            }
        }

        if(generateLayoutShow == 'false'){
            $("#layout-design-field").removeClass('hidden');
            checkFields();
        }

        if((expStatus == 'entry list created' || expStatus == 'entry list incomplete seed sources') && (planStatus.includes('randomized') || planStatus.includes('uploaded'))){
            enableFields(false);
            enableAddtlFields(false)
            $('#generate-design-btn').attr('disabled', 'disabled')
            $("#delete-all-btn").removeAttr('disabled')
            $('#assign_rep_btn').attr('disabled', 'disabled')
            $("#acrossEnvDesign_id").attr('disabled', 'disabled')
            $("#designui-switch-id").attr('disabled', 'disabled')
            
            $('#error-design-div-msg-panel').html('There are changes in the entry list. Please randomize again.');
            $('#error-design-div-panel').removeClass('hidden');
            changeFlag = 1;
        }

        if($("#design_select_id").val() == '' || $("#design_select_id").val() == undefined){
            $("#save-parameters-btn").attr('disabled', 'disabled');
        }

        $(".boolean-input-validate ").each(function(){
            if($(this).prop('checked') == true && this.id == 'methodReplicateCheckEntries'||this.id == 'methodReplicateTestEntries' ){
                checkSelectionEntries(this.id);
            }
        });

        if(expStatus.includes('in progress') || planStatus.includes('in progress')){
            $('#generate-design-btn').attr('disabled', 'disabled');
            $("#acrossEnvDesign_id").attr('disabled', 'disabled');
            $("#delete-all-btn").attr('disabled', 'disabled');
            $('#assign_rep_btn').attr('disabled', 'disabled');
            $("#designui-switch-id").attr('disabled', 'disabled');
        } else {
            $('#generate-design-btn').removeAttr('disabled');
            $("#acrossEnvDesign_id").removeAttr('disabled');
            $("#designui-switch-id").removeAttr('disabled');
        }

        if(expStatus.includes('design generated') || planStatus.includes('randomized') || planStatus == 'deletion in progress'){
            $("#acrossEnvDesign_id").attr('disabled', 'disabled');
            $('#generate-design-btn').attr('disabled', 'disabled');
            $('#assign_rep_btn').attr('disabled', 'disabled');
            $("#designui-switch-id").attr('disabled', 'disabled');
        } else {
            $("#acrossEnvDesign_id").removeAttr('disabled');
            $("#designui-switch-id").removeAttr('disabled');
        }

        if(deletePlots == 'true'){
            $("#acrossEnvDesign_id").attr('disabled', 'disabled');
            $('#generate-design-btn').attr('disabled', 'disabled');
            $("#delete-all-btn").removeAttr('disabled');
            $('#assign_rep_btn').attr('disabled', 'disabled');
            $("#designui-switch-id").attr('disabled', 'disabled');
            enableAddtlFields(false)
        }

        if(expStatus.includes('design generated') || planStatus.includes('in progress')|| planStatus.includes('randomized')){
            $('#assign_rep_btn').attr('disabled', 'disabled');
            $("#acrossEnvDesign_id").attr('disabled', 'disabled');
            $('#generate-design-btn').attr('disabled', 'disabled');
            $("#designui-switch-id").attr('disabled', 'disabled');
            enableAddtlFields(false)
        } else {
            $('#assign_rep_btn').removeAttr('disabled');
        }

        if(generateLayoutShow == 'true'){
            $('[name="ExperimentDesign[genLayout]').prop('checked', true);
            $('[name="ExperimentDesign[genLayout]').trigger('change');
            checkFields();
        }

        updateOccurrencesInfo();

        //check if workflow is upload, hide and disable other elements
        workflowCheck()
    });
	

    $('#assign_rep_btn').on('click',function(e){
        e.preventDefault();
        $('#assign_rep_btn').attr('disabled', 'disabled');

        window.location = '$assignUrl';
    });
    
    $('#generate-design-btn').on('click',function(e){
        e.preventDefault();

        // Save the changes in the database
        $.ajax({
            url: '$submitUrl',
            type: 'post',
            dataType: 'json',
            data: {},
            success: function(response) {
                var notif = ''

                if(response['success']){
                    planStatus = 'randomization in progress'
                    notif = "Request successfully sent."
                    $('#generate-design-btn').attr('disabled', 'disabled');
                    $("#delete-all-btn").attr('disabled', 'disabled');

                    $("#assign_rep_btn").attr('disabled', 'disabled');
                    $("#acrossEnvDesign_id").attr('disabled', 'disabled');
                    $("#designui-switch-id").attr('disabled', 'disabled');
                    message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i>' + notif
                    enableAddtlFields(false)
                    $(".delete-param-set").addClass('hide-action-btn')
                    $(".view-param-set").addClass('hide-action-btn')
                    $(".update-param-set").addClass('hide-action-btn')
                    location.reload();
                } else {
                    $('#generate-design-btn').removeAttr('disabled');
                    $("#delete-all-btn").removeAttr('disabled');
                    $("#assign_rep_btn").removeAttr('disabled');
                    $("#acrossEnvDesign_id").removeAttr('disabled');
                    $("#designui-switch-id").removeAttr('disabled');
                    notif = response['message']
                    message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + notif
                    enableAddtlFields(true)
                }

                $('.toast').css('display','none')
                Materialize.toast(message, 5000)
            },
            error: function (e) {
                console.log(e)
            }
        })
    });

    $("#acrossEnvDesign_id").change(function() {
        var designText = $("#design_select_id option:selected").html();
        var designValue = this.value;

        var action_url = '$saveEnvDesignUrl';
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: action_url,
            data: {
                designValue: designValue
            },
            success: function(data) {
                location.reload();
            },
            error: function(){
                location.reload();
            }
        });   
    });


    $("#plots_delete_btn").click(function() {
        $("#plots_delete_btn").addClass("disabled");
        var modalBody = 'plot-records-confirm-modal-body';
        $(modalBody).html(loading);
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: '$deleteJsonUrl',
            data: {
                'parameterSet':'parameter set'
            },
            success: function(data) {
                if(data['success'] == true){
                    enableFields(false)
                    enableAddtlFields(false)
                    $("#no_action_btn").attr('disabled', 'disabled');
                    $("#generate-design-btn").attr('disabled', 'disabled');
                    $("#delete-all-btn").attr('disabled', 'disabled');
                    $("#acrossEnvDesign_id").attr('disabled', 'disabled');
                    $("#designui-switch-id").attr('disabled', 'disabled');
                    $('#plot-records-confirm-modal').modal('hide');
                    var notif = "<i class='material-icons green-text left'>check</i> Deletion of plot records started.";
                    Materialize.toast(notif,5000);
                    planStatus = 'deletion in progress';

                    enableAddtlFields(false)
                    $(".delete-param-set").addClass('hide-action-btn')
                    $(".view-param-set").addClass('hide-action-btn')
                    $(".update-param-set").addClass('hide-action-btn')
                } else {
                    enableFields(true)
                    enableAddtlFields(false)
                    $("#no_action_btn").attr('disabled', 'disabled');
                    $('#plot-records-confirm-modal').modal('hide');
                    var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                    Materialize.toast(notif,5000);
                }
                $("#plots_delete_btn").removeClass("disabled");
            },
            error: function(){

                $('#plot-records-confirm-modal').modal('hide');
                var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                Materialize.toast(notif,5000);
                $("#plots_delete_btn").removeClass("disabled");
            }
        });  
    });

    $("#delete-all-btn").click(function() {
        $('#plot-records-confirm-modal').modal();
        $('#plot-records-modal-confirm').html('Plot records are already populated for this experiment. Click confirm to delete the records before submitting another request.');
    });


    //show side nav
    $('#hide-right-design-nav').on('click',function(){
        $('#right-side-panel-div').hide();
        $('#parameter-sets-panel').removeClass('col-md-9');
        $('#parameter-sets-panel').addClass('col-md-12');
      	$('#hidden-right-side-div').show();

    });
    $('#show-right-design-nav').on('click',function(){
        $('#parameter-sets-panel').removeClass('col-md-12');
        $('#parameter-sets-panel').addClass('col-md-9');
      	$('#right-side-panel-div').show();
      	$('#hidden-right-side-div').hide();
    });

    $("#genLayout").on('change', function (e, params) {
      
        if($('[name="ExperimentDesign[genLayout]"').prop('checked') == true){
            
            $("#layout-design-fields").removeClass('hidden');
            paramValues['genLayout'] = true;

            if(design == 'p-rep'){
                $('#max-plot-fields').addClass('hidden');
            }

        } else{
            $("#layout-design-fields").addClass('hidden');
            paramValues['genLayout'] = false; 
            if(design == 'p-rep'){
                $('#max-plot-fields').removeClass('hidden');
            }
        }
    });

    $("#methodReplicateCheckEntries").on('change', function (e, params) {
        checkSelectionEntries(this.id);
    });

    $("#methodReplicateTestEntries").on('change', function (e, params) {
        checkSelectionEntries(this.id);
    });

    function checkSelectionEntries(id){
        var fieldName = ''
        var disableField = false
        if($('[name="ExperimentDesign['+id+']"').prop('checked') == true){
            disableField = true
            if(id == 'methodReplicateCheckEntries'){
                var assignBtn = $('<a id="select_checks_btn" href="'+'$assignUrl'+'&entryType=check'+'" class="pull-right" style="margin-top:-5px"><span title="Assign replicates for check entries"><i class="material-icons">format_list_bulleted</i></span></a>')
            } else { 
                var assignBtn = $('<a id="select_test_btn" href="'+'$assignUrl'+'&entryType=test'+'" class="pull-right" style="margin-top:-5px"><span title="Assign rep for entries"><i class="material-icons">format_list_bulleted</i></span></a>')
            }
           
            var element = $('#'+id).closest("label")
            element.append(assignBtn)
        } else{
            //remove button
            if(id == 'methodReplicateCheckEntries'){
                $("#select_checks_btn").remove();
            } else {
                $("#select_test_btn").remove();
            }
            disableField = false
        }
        if(id == 'methodReplicateCheckEntries'){
            fieldName = 'repCheck'
        } else {
            fieldName = 'repTest'
        }
        if(disableField){
            $('#'+fieldName).attr('disabled', 'disabled')
        } else {
            $('#'+fieldName).removeAttr('disabled');
        }
        
    }

    function enableAddtlFields(enableAddtl){
        if(!enableAddtl){
            $(".additional").each(function(){
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).attr('disabled', 'disabled');
                }
            });
        } else {
            $(".additional").each(function(){
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).removeAttr('disabled');
                }
            });
        }
    }

    $("#design_select_id").change(function() {

        if(workflow == 'upload'){
            //show upload button

        } else {
            var designText = $("#design_select_id option:selected").html();
            var designValue = this.value;
            var design =  designValue.split('|');
            var additionalArray = {};
            
            $(".additional").each(function(){
                if(this.id !== ""){
                    if(this.value == '' || this.value == undefined || this.value == 'Select a value'){
                        checkflag = 1;
                    } 

                    additionalArray[this.id] = this.value;
                }
                
            });
            var action_url = '$saveAddtlUrl';

            $.ajax({
                type:"POST",
                dataType: 'json',
                url: '$selectDesignUrl',
                data: {
                    additionalData: JSON.stringify(additionalArray),
                    designId:design[0],
                    designInfo:'$designInfo',
                    acrossEnvDesign:'random'
                },
                success: function(data) {
                    location.reload();
                },
                error: function(){
                    location.reload();
                }
            });      
        }

    });

    $(".randomization").change(function() {
        //check values
        if (parseFloat(this.value) < parseFloat(this.min)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Minimum value is ' + this.min;
            className = 'error';
            var label = $("#"+this.id).closest('label').prevObject[0]['labels'][0]['innerText'];
            errorArrayField.push(label+" "+message);
        } else if (parseFloat(this.value) > parseFloat(this.max)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Maximum value is ' + this.max;
            className = 'error';
            var label = $("#"+this.id).closest('label').prevObject[0]['labels'][0]['innerText'];
            errorArrayField.push(label+" "+message);
        } else{
            var idx = this.id;
            var idx = errorArrayField.indexOf(this.id);
            errorArrayField.splice(idx, 1);
            $('#'+this.id).removeClass('invalid');

            if(Object.keys(errorArrayField).length === 0){
                $('#notif_field').addClass('hidden');
            }

            paramValues[this.id] = this.value; 
        }

        //if parameter is included in totalplots computation
        let plotsParameters = ['repTest', 'repCheck', 'repLocalCheck', 'nTest', 'nCheck', 'nLocalCheck']
        if(design == 'partially replicated' && plotsParameters.includes(this.id)){
            computeTotalPlots()
        }


        if(this.id != 'nEntry'){
            updateEntriesInfo();
        }
       
        checkFields();
    });

    $(".layout").change(function() {
        if (parseFloat(this.value) < parseFloat(this.min)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Minimum value is ' + this.min;
            className = 'error';
            var label = $("#"+this.id).closest('label').prevObject[0]['labels'][0]['innerText'];
            errorArrayField.push(label+" "+message);
        } else if (parseFloat(this.value) > parseFloat(this.max)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Maximum value is ' + this.max;
            className = 'error';
            var label = $("#"+this.id).closest('label').prevObject[0]['labels'][0]['innerText'];
            errorArrayField.push(label+" "+message);
        } else{
            var idx = this.id;
            var idx = errorArrayField.indexOf(this.id);
            errorArrayField.splice(idx, 1);
            $('#'+this.id).removeClass('invalid');

            if(Object.keys(errorArrayField).length === 0){
                $('#error-design-div-msg-panel').html('');
                $('#error-design-div-panel').addClass('hidden');
            }

            paramValues[this.id] = this.value; 
        }

        checkFields();
    });

    $(".additional").change(function() {
        var form_data = $.param($.map($("#additional-details-form").serializeArray(), function(v,i) {
            return (v.name == "add_variable_info_select") ? null : v;
        }));

        if(this.id == 'nExptTrial'){
            totalOccurrences = parseInt(this.value);
            updateOccurrencesInfo();
            paramValues['nExptTrial']=totalOccurrences;
            $("#nTrial").trigger('change');
        }
        var action_url = '$saveAddtlUrl';

        $.ajax({
            type:"POST",
            dataType: 'json',
            url: action_url,
            data: form_data,
            success: function(data) {
                if(data == 'success'){
                    var notif = "<i class='material-icons green-text left'>check</i> Successfully saved changes.";
                    Materialize.toast(notif,5000);
                } else {
                    var notif = "<i class='material-icons red-text left'>close</i> Unable to save changes. Please try again.";
                    Materialize.toast(notif,5000);
                }
            },
            error: function(){
                var notif = "<i class='material-icons red-text left'>close</i> Unable to save changes. Please try again.";
                Materialize.toast(notif,5000);
            }
        });     
    });

    $('.delete-param-set').click(function(){

        var planTemplateId = $(this).attr('data-plan-template-id');
   
        $.ajax({
            url: '$deleteUrl',
            type: 'post',
            dataType: 'json',
            data: {
                planTemplateId: planTemplateId
            },
            success: function(response) {
                var notif = "<i class='material-icons green-text left'>check</i> Parameter successfully deleted.";
                Materialize.toast(notif,5000);
                location.reload();
            },
            error: function() {

            }
        });
    });


    $('.update-param-set').click(function(){

        if(incompleteSet == 1){
            var notif = "<i class='material-icons red-text left'>close</i> Please save first the parameter set being updated/created.";
            Materialize.toast(notif,5000);
        } else {
            var planTemplateId = $(this).attr('data-plan-template-id');
       
            $.ajax({
                url: '$updateUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    planTemplateId: planTemplateId
                },
                success: function(response) {
                    var notif = "<i class='material-icons green-text left'>check</i> You can now update selected parameter set.";
                    Materialize.toast(notif,5000);
                    location.reload();
                },
                error: function() {

                }
            });
        }
    });

    $('#nTrial').change(function(){

        var oneVal = parseInt(this.value);
        var freeOccurrences = parseInt(totalOccurrences - assignedOccurrences);

        if(freeOccurrences < oneVal){
            $('#'+this.id).addClass('invalid');
            message = '<i class="fa fa-times"></i> Value should be less than no. of unassigned occurrence:' + freeOccurrences;
            className = 'error';
            var label = $("#"+this.id).closest('label').prevObject[0]['labels'][0]['innerText'];

            errorArrayField.push(label+" "+message);
        } else {
            var idx = this.id;
            var idx = errorArrayField.indexOf(this.id);
            errorArrayField.splice(idx, 1);
            $('#'+this.id).removeClass('invalid');

            if(Object.keys(errorArrayField).length === 0){
                $('#error-design-div-msg-panel').html('');
                $('#error-design-div-panel').addClass('hidden');
            }
            paramValues[this.id] = this.value; 
        }
        checkFields();
    });

    function defaultTokens(expression){
        var evaluatedArray = evaluateExpression(expression);
        var tokens = evaluatedArray;
        
        var defaultVal = 0;

        if(tokens[0]['value'].includes('min')){
            if(tokens.length > 3){  //there's expression within
                if(tokens[2]['value'].includes('factor')){
                    var options = eval(tokens[2]['value']);

                    if(tokens[3]['value'] == ")"){  //get minimum value first
                        options.sort((a,b)=>a-b);
                        defaultVal = options[0];
                    }
                    if(tokens[4]['value'] != undefined && (tokens[4]['type'] == 'COMP' || tokens[4]['type'] == 'OPER')){ //evaluate
                        var var2 = tokens[5]['value'];
                        if(tokens[5]['type'] == 'VAR'){
                            var2 = checkVarValue(var2);
                        }
                        for(var i=0; i<options.length; i++){
                            var evaluateStr = options[i]+tokens[4]['value']+var2;
                            if(eval(evaluateStr)){
                                defaultVal = options[i];
                                break;
                            }
                        }
                    }

                    return defaultVal;
                }
            }
        }
    }

    function placeDefault(){

        for(let key in defaultRules){
            var defaultVal = 0;
            var value = null;
            if(paramValues[key] == null || paramValues[key] == ""){

                for(var i=0; i<defaultRules[key].length; i++){
                    defaultVal = defaultTokens(defaultRules[key][i]['expression']);
                    if(defaultVal === null || defaultVal == undefined){
                        defaultVal = eval(defaultRules[key][i]['expression'])
                    }
                    paramValues[key] = defaultVal;
                    $('#'+key).val(defaultVal);
                    $('#'+key).value = defaultVal;
                    $('#'+key).trigger('change');
                }
            }
        }
    }
    function updateEntriesInfo(){
        let entryArr = ['nTest', 'nCheck', 'nLocalCheck']
        
        let paramTest = ($('#nTest').val() == undefined || $('#nTest').val() == "")? 0 : $('#nTest').val() 
        let paramCheck = ($('#nCheck').val() == undefined || $('#nCheck').val() == "")? 0 : $('#nCheck').val() 
        var paramLocal = $('#nLocalCheck').val() == undefined ? 0 : $('#nLocalCheck').val() 

        let totalUsed = 0
        totalUsed = parseInt(paramTest) + parseInt(paramCheck) + parseInt(paramLocal)
        $('#nEntry').val(totalUsed)
        $('#nEntry').value = totalUsed
        $('#nEntry').trigger('change')
        paramValues['nEntry'] = totalUsed
    }

    function updateOccurrencesInfo(){
        var freeOccurrences = parseInt(totalOccurrences - assignedOccurrences);
        
        if(freeOccurrences > 0){
            $('#generate-design-btn').attr('disabled', 'disabled');
            $("#design_select_id").removeAttr('disabled');
             $('#total-occurrences-notif').html("<i class='material-icons orange-text left'>warning</i> <div style='margin-top:5px;'><b>"+freeOccurrences+"</b> occurrence/s should be used in &nbsp; parameter set.</div>");
        } else if(freeOccurrences == 0){
            if(planStatus != 'randomized' && !planStatus.includes('in progress')){
                $('#generate-design-btn').removeAttr('disabled');
            }
            $("#design_select_id").attr('disabled', 'disabled');
            $('#total-occurrences-notif').html("<i class='material-icons green-text left'>check</i> <div style='margin-top:5px;'>All<b> "+totalOccurrences+"</b> occurrence/s have been assigned.</div>");
            $('#hide-right-design-nav').trigger('click');
        }
        
    }

    function flashMessage(msg, className){
        var msgDef = (msg==undefined || msg=='') ? 'Please specify valid values.':msg;
        if(className == undefined || className == ''){
            var classText = 'red-text';
        } else {
            var classArray = {
              'error':'red-text',
              'warning':'orange-text'
            };
            var classText = classArray[className];
        }

        $('#notif_field').html('<br/><span class="'+classText+' text-darken-2">'+msgDef+'</span>');
        $('#notif_field').removeClass('hidden');
    }

    function showNotifications(msgArray, notifType){

        var msg = '';
        
        for(let key in msgArray){
            if(msgArray[key] != ""){
              
                msg += msgArray[key] + "<br>"
            }
        }
        
        if(notifType =='warning' && msg != ''){
            $('#warning-design-div-msg-panel').html(msg);
            $('#warning-design-div-panel').removeClass('hidden');
        } else if(notifType =='error' && msg != ''){
            $('#error-design-div-msg-panel').html(msg);
            $('#error-design-div-panel').removeClass('hidden');
        } else if(msg == ''){
            if(notifType=='warning'){
                $('#warning-design-div-msg-panel').html('');
                $('#warning-design-div-panel').addClass('hidden');
            } else {
                $('#error-design-div-msg-panel').html('');
                $('#error-design-div-panel').addClass('hidden');
            }
        }
    }

    function checkFields(){
        var checkflag = 0;
        flag = 0;

        $(".randomization").each(function(){

            var element = document.getElementById(this.id);

            if(this.id !== ""){
                if(this.id == 'maxPlots'){
                    if($('[name="ExperimentDesign[genLayout]').prop('checked') == false && (this.value == '' || this.value == undefined )){
                        checkflag = 1;
                    }
                } else if((this.value == '' || this.value == undefined || this.value == 'Select a value') && !element.className.includes('boolean-input-validate')){
                    checkflag = 1;
                } else if(element != null && element.className.includes('boolean-input-validate')){
                    var isChecked = $(this).is(":checked");

                    var tId = this.name;
                    if(isChecked === false){
                        
                        $('#'+this.id).attr('value', false);
                        
                    } else {

                        $('#'+this.id).attr('value', true);
                    }
                    paramValues[this.id] = isChecked;
                }
            }
            
        });

        if($('[name="ExperimentDesign[genLayout]').prop('checked') == true || generateLayoutShow == 'false'){
            $(".layout").each(function(){
                if(this.id != ''){
                    if(this.value == '' || this.value == undefined  || this.value == 'Select a value'){
                        checkflag = 1;
                    }
                }
            });
        } else {
            $(".layout").each(function(){
              
                var element = document.getElementById(this.id);

                if(element != null && element.tagName === 'SELECT' && this.value == ''){
                    $("#"+(this.id)+" option:first").attr("selected", true);
                    $("#"+(this.id)+"-hidden").val(0);
                    $("#select2-"+(this.id)+"-container").attr('title', 'Select a value');
                    $("#select2-"+(this.id)+"-container").html = 'Select a value';
                    var span = document.getElementById("select2-"+(this.id)+"-container"); 
                  
                    if(span != null && this.value == ''){
                        span.textContent = 'Select a value';
                    }
                } else if(element != null && element.className.includes('boolean-input-validate')){
                    var isChecked = $(this).is(":checked");
                    var tId = this.name;
                    if(isChecked === false){
                        
                        $('#'+this.id).attr('value', false);
                        
                    } else {

                        $('#'+this.id).attr('value', true);
                    }
                    paramValues[this.id] = isChecked;
                }
            });
        }

        validateDesignRules("");
        if(Object.keys(errorArray).length > 0){
            var msg = '';
            for(let key in errorArray){
                if(errorArray[key] != ""){
                    msg += errorArray[key];
                    flag = 1;
                }
            }
        }
        if(Object.keys(errorArrayField).length > 0){
            flag = 1;
            showNotifications(errorArrayField, 'error');
        }

        if(flag == 0  && initflag == 0 && checkflag == 0){
            $("#save-parameters-btn").removeAttr('disabled');
        } else {
            $("#save-parameters-btn").attr('disabled', 'disabled');
        }

        if(checkflag == 1){
            flag = 1;
        }
    }

    function enableFields(enabled){
        if(enabled){
          
            $(".randomization").each(function(){
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).removeAttr('disabled');
                }
            });
            $(".layout").each(function(){
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).removeAttr('disabled');
                }
            });

            $("#experiment-design-form").find('input[type="checkbox"]').each( function () {
                $("#"+this.id).removeAttr('disabled');
            });
        } else {
            
            $(".randomization").each(function(){
              
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).attr('disabled', 'disabled');
                }
            });
            $(".layout").each(function(){
              
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).attr('disabled', 'disabled');
                }
            });

            $("#experiment-design-form").find('input[type="checkbox"]').each( function () {
                $("#"+this.id).attr('disabled', 'disabled');
            });
        }
    }

    $("#save-parameters-btn").click(function() {
        $('#mod-notif').html('');
        $('#mod-notif').addClass('hidden');
        checkFields();

        $("select[disabled]").each(function(){
            $("#"+this.id).removeAttr('disabled', 'disabled');
        });
        
        var action_url = '$saveParamsUrl';

        var form_data = $.param($.map($("#experiment-design-form").serializeArray(), function(v,i) {
            return (v.name == "add_variable_info_select") ? null : v;
        }));

        $("#experiment-design-form").find('input[type="checkbox"]').each( function () {
            var isChecked = $(this).is(":checked");
            
            var tId = this.name;
            if(isChecked === false){
                
                $('#'+this.id).attr('value', 'false');
                
            } else {

                $('#'+this.id).attr('value', 'true');
            }

            form_data = form_data + '&' + $.param({
                [tId] : $(this).val()
            });
        });

        //add design
        var designSelId = $('#design_select_id').prop('name');
        form_data = form_data + '&' + $.param({
            [designSelId] : $('#design_select_id').val()
        });

        if(addtlValidationFlag){
            // Check other validation
            
            $.ajax({
                url: '$validateUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    addtlValidation:JSON.stringify(addtlValidation),
                    checkEntries: nExptTCheck,
                    testEntries:nExptTest
                },
                success: function(response) {
                    var notif = ''
                    if(response['success']){
                        $.ajax({
                            type:"POST",
                            dataType: 'json',
                            url: action_url,
                            data: form_data,
                            success: function(data) {
                                if(data == 'success'){
                                    enableFields(false);
                                    // previewLayout();
                                    $("#no_action_btn").attr('disabled', 'disabled');
                                    
                                    var notif = "<i class='material-icons green-text left'>check</i> Parameters successfully saved.";
                                    Materialize.toast(notif,5000);
                                    location.reload();
                                } else {
                                    enableFields(true);
                                    $('#layout-panel-div').html('');
                                    $("#no_action_btn").attr('disabled', 'disabled');
                                    var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                                    Materialize.toast(notif,5000);
                                    location.reload();
                                }
                            },
                            error: function(){
                                $('#layout-panel-div').html('');
                                var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                                Materialize.toast(notif,5000);
                            }
                        });      
                    } else {
                        $('#generate-design-btn').removeAttr('disabled');
                        notif = response['message']
                        message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + notif

                        $('.toast').css('display','none')
                        Materialize.toast(message, 5000)
                    }

                },
                error: function (e) {
                    console.log(e)
                }
            })
        }
        
    });

    $("#workflow-switch-id").on('change', function (e, params) {
        var workflowVal = '';
        if($(this).prop('checked') == true){
            //check if there's created parameter set; if none, reset view
            //delete created draft parameter set
            //delete nReps, set to default additional fields
            workflowVal = 'upload';
        } else{
            //check if there's created upload parameter set; if none, refresh view to show generate
            workflowVal = 'generate';
        }
        $.ajax({
            url: '$updateWorkflowUrl',
            type: 'post',
            dataType: 'json',
            data: {
                workflowVal: workflowVal
            },
            success: function(response) {
                location.reload();
            },
            error: function() {

            }
        });
    });


    // Access Upload design array
    $('#upload_design_array').on('click', function (e) {

        // trigger action ongoing
        e.preventDefault();
        e.stopImmediatePropagation();

        $('#mod-notif').html('');
        $('#mod-notif').addClass('hidden');
        if(deletePlots != 'true'){
            $('#upload-design-array-modal').modal('show')
            var modalBody = '.upload-design-array-form-modal-body';
            $(modalBody).html(loading);

            $.ajax({
                url: '$renderFormUploadDesignArray',
                type: 'post',
                dataType: 'json',
                data:{
                    designInfo:'$designInfo'
                },
                success: function(response){
                    $(modalBody).html(response);
                },
                error: function(){
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBody).html(errorMessage);
                }
            });
        } else {
            $('#plot-records-confirm-modal').modal();
            $('#plot-records-modal-confirm').html('Plot records are already populated for this experiment. Click confirm to delete the records before submitting another request.');
        }
    });

    $("#designui-switch-id").on('change', function (e, params) {
        var designUiSwitch = '';
        if($(this).prop('checked') == true){
            //check if there's created parameter set; if none, reset view
            //delete created draft parameter set
            //delete nReps, set to default additional fields
            designUiSwitch = 'old';
        } else{
            //check if there's created upload parameter set; if none, refresh view to show generate
            designUiSwitch = 'new';
        }
        $.ajax({
            url: '$updateDesignUiUrl',
            type: 'post',
            dataType: 'json',
            data: {
                designUiSwitch: designUiSwitch
            },
            success: function(response) {
                location.reload();
            },
            error: function() {

            }
        });
    });
    
    /*****************Section for fxns*******************/
    function evaluateExpression(expression, simplify=true){

        var tokens = tokenize(expression);
        //simplify expression with functions
        var simplifiedTokens = [];
        for(var i=0; i < tokens.length; i++){
            if(tokens[i]['type'] == 'FXN'){
                if((tokens[i+2]['type'] == 'NUM') && tokens[i+3]['type'] == 'RPAREN'){
                    simplifiedTokens.push({type:'FXN', value: tokens[i]['value']+'('+tokens[i+2]['value']+')'});
                    i = i+3;
                    continue;
                }else if((tokens[i+2]['type'] == 'VAR') && tokens[i+3]['type'] == 'RPAREN'){
                    var var1 = tokens[i+2]['value']
                    if(simplify){
                        var1 = checkVarValue(tokens[i+2]['value']);
                    }
                    simplifiedTokens.push({type:'FXN', value: tokens[i]['value']+'('+var1+')'});
                    i = i+3;
                    continue;
                } else {
                    simplifiedTokens.push(tokens[i]);
                }
            } else {
                if(tokens[i]['type'] == 'VAR' && tokens[i]['value'] != 'value') {
                    var var1 = tokens[i]['value']
                    if(simplify){
                        var1 = checkVarValue(tokens[i]['value']);
                    }
                    simplifiedTokens.push({type:'NUM', value: "("+var1+")"});
                }
                else {
                    simplifiedTokens.push(tokens[i]);
                }
            }
        }

        tokens = simplifiedTokens;
        
        var LParent = 1;
        var RParent = 1;
        var index = 0;
        while ((LParent != 0 && RParent != 0) && index != 30){
            var updatedArray = searchGroupings(tokens, simplify);
            LParent = updatedArray['LParent'];
            RParent = updatedArray['RParent'];
            tokens = updatedArray['tokens'];
            index++;
        }

        return tokens;
    }   

    function checkVarValue(fieldName){

        var fieldValue = '';
        if(!paramValues.hasOwnProperty(fieldName)){
            try{
                fieldValue = eval(fieldName)
            } catch(e){
                fieldValue = paramValues[fieldName]
            }
        } else {
            try{
                fieldValue = eval(paramValues[fieldName])
            } catch(e){
                fieldValue = paramValues[fieldName]
            }
        }

        return fieldValue;
    }

     function searchGroupings(tokens, simplify=true){
        var LParent = 0;
        var RParent = 0;
        var expressionValue = '';

        var newTokens = [];

        //search last left parenthesis
        for(var i=0; i < tokens.length; i++){
            if(tokens[i]['value'] == "("){
                LParent = i;
            }
        }

        //search partner of left parenthesis
        var expressionArray = [];
        for(var i=LParent+1; i < tokens.length; i++){
            if(tokens[i]['value'] == ")"){
                RParent = i;
                break;
            } else {
                expressionArray.push(tokens[i]);
            }
        }

        if(LParent != 0 && RParent != 0){
          
            //evaluate expressionArray
            if(expressionArray.length == 1 && expressionArray[0]['type'] == 'VAR'){ //assume variable value
                var fieldName = expressionArray[0]['value'];
                expressionValue = fieldName
                if(simplify){
                    expressionValue = checkVarValue(fieldName)
                }
            } else if(expressionArray.length > 1){ //assume simple expression
                //check if variable
                let methodSwitch = false
                if(simplify){

                    if(expressionArray[0]['type'] == 'NUM' && expressionArray[2]['type'] == 'BOOL'){ //for methodReplicate parameters
                        let boolInt = ['Random', 'Manual']
                        var var1 = expressionArray[0]['value']+""
                        if(var1.includes('(')){
                            var tempVar1 = var1.replace("(", "")
                            var1 = tempVar1.replace(")", "")+""
                            var1 = boolInt.indexOf(var1) == -1 ? var1 : boolInt.indexOf(var1)
                        }
                        var var2 = boolInt.indexOf(expressionArray[2]['value']+"")
                        if(typeof var1 === "boolean"){
                            var2 = var2 == 0 ? false : true
                        }
                        
                        methodSwitch = true
                    } else {
                       
                        if(expressionArray[0]['type'] == 'VAR'){
                            var var1 = expressionArray[0]['value']
                        } else {
                            var var1 = expressionArray[0]['value']+""
                            if(var1.includes('(')){
                                var tempVar1 = expressionArray[0]['value'].replace("(", "");
                                var1 = tempVar1.replace(")", "");
                            }
                            var1 = parseInt(var1);
                        }

                        if(expressionArray[2]['type'] == 'VAR'){
                            var var2 = expressionArray[2]['value']
                        } else {
                            var var2 = expressionArray[2]['value']+""
                            if(var2.includes('(')){
                                var tempVar1 = expressionArray[2]['value'].replace("(", "");
                                var2 = tempVar1.replace(")", "");
                            }
                            var2 = parseInt(var2);
                        }
                    }
                } else {

                    if(expressionArray[0]['type'] == 'VAR'){
                        var var1 = checkVarValue(expressionArray[0]['value'])
                    } else {
                        var var1 = expressionArray[0]['value']+""
                        if(var1.includes('(')){
                            var tempVar1 = expressionArray[0]['value'].replace("(", "");
                            var1 = tempVar1.replace(")", "");
                        }
                        var1 = parseInt(var1);
                    }

                    if(expressionArray[2]['type'] == 'VAR'){
                        var var2 = checkVarValue(expressionArray[2]['value']) 
                    } else {
                        var var2 = expressionArray[2]['value']+""
                        if(var2.includes('(')){
                            var tempVar1 = expressionArray[2]['value'].replace("(", "");
                            var2 = tempVar1.replace(")", "");
                        }
                        var2 = parseInt(var2);
                    }
                }

                try{
                    expressionValue = eval(var1 + expressionArray[1]['value'] + var2)
                    if(methodSwitch) expressionValue="("+expressionValue+")" 
                } catch(e){
                    expressionValue = "("+var1 + expressionArray[1]['value'] + var2+")"
                }

            }

            if(expressionValue != ''){

                //rewrite tokens
                for(var i=0; i < tokens.length; i++){
                    if(i == LParent){
                        if(tokens[i-1]['type'] != undefined && tokens[i-1]['type'] != 'FXN'){
                            newTokens.push({ type:'NUM', value:expressionValue });
                        } else if(tokens[i-1]['type'] == 'FXN'){
                            newTokens.pop(); //removes fxn
                            newTokens.push({ type: 'FXN', value: tokens[i-1]['value']+'('+expressionValue+')'});
                        }
                        i = RParent;
                    } else {
                        newTokens.push(tokens[i]);
                    }
                }
            } else {
                LParent = 0; RParent = 0; newTokens = tokens;
            }
        } else {
            newTokens = tokens;
        }

        return {
            'LParent':LParent,
            'RParent':RParent,
            'tokens' : newTokens
        }
    }

    function validateTokens(expression, value){

        var evaluatedArray = evaluateExpression(expression, true);

        var tokens = evaluatedArray;
        var passValidation = 1;
        if(tokens.length == 1){ //function
            if(tokens[0]['value'].includes('factor')){
                var options = eval(tokens[0]['value']);
                
                if(!options.includes(parseInt(value))){
                    passValidation = 0;
                }
                
            }else if(tokens[0]['value'].includes('prime')){
              
                passValidation = eval(tokens[0]['value']) ? 1 : 0;
            } 
        } else {
            var tokenString = '';
            for(var i=0; i<tokens.length; i++){
                tokenString += tokens[i]['value'];
            }

            if(tokenString.includes('each')){
                passValidation = 1;
            } else {
                passValidation = eval(tokenString) ? 1 : 0;
            }

            if(tokenString.includes('if')){
                //call function each
                passValidation = !passValidation ? 1 : 0;

            }
            
        }
        return passValidation;
    }

    function validateDesignRules(fieldName){

        if(fieldName != ""){
            if(validationRules[fieldName] != undefined){
              
                key = fieldName;
                var label = $("#"+this.id).closest('label').prevObject[0]['labels'][0]['innerText'];
                if(paramValues[key] != undefined && paramValues[key] != null ){

                    var value = paramValues[key];

                    warningArray[key] = '';
                    errorArray[key] = '';
                    for(var i=0; i<validationRules[key].length; i++){
                        
                        var passValidation = validateTokens(validationRules[key][i]['expression'], value);

                        if(passValidation == 0){
                            if(validationRules[key][i]['action'] == 'warning'){
                                warningArray[key] += validationRules[key][i]['notification'];
                            } else if(validationRules[key][i]['action'] == 'error'){
                                if(errorArray[key] == undefined){
                                    errorArray[key] = '';
                                }
                                errorArray[key] += label+" "+validationRules[key][i]['notification'] ;
                            }
                        }
                    }
                }
            }
        } else {
            for(let key in validationRules){
                var passValidation = 1;
                var value = null;
                if(paramValues[key] != undefined && paramValues[key] != null ){
                    var value = paramValues[key];
                }

                if(paramValues[key] != null || key == 'entryList'){
                    warningArray[key] = '';
                    errorArray[key] = '';
                    for(var i=0; i<validationRules[key].length; i++){
                        
                        var passValidation = validateTokens(validationRules[key][i]['expression'], value);

                        if(passValidation == 0){
                            if(validationRules[key][i]['action'] == 'warning'){
                                if(warningArray[key] == undefined){
                                    warningArray[key] = '';
                                }
                                warningArray[key] += validationRules[key][i]['notification'];
                            } else if(validationRules[key][i]['action'] == 'error'){
                                if(errorArray[key] == undefined){
                                    errorArray[key] = '';
                                }
                                errorArray[key] += validationRules[key][i]['notification'] ;
                            }
                        }
                    }
                } else if(key == 'checkList' || key == 'testList'){
                    addtlValidationFlag = true;
                    var evaluatedArray = evaluateExpression(validationRules[key][0]['expression'], false)

                    //NOTE: simple condition use case only e.g. each(nRep<1)
                    var firstVar = null
                    var condVar = null
                    var valVar = null
                    for(var i=0; i<evaluatedArray.length; i++){
                        if(evaluatedArray[i]['type'] !== 'FXN' ||evaluatedArray[i]['type'] !== 'LPAREN' || evaluatedArray[i]['type'] !== 'RPAREN'){
                            if(firstVar == null && evaluatedArray[i]['type'] == 'NUM'){
                                firstVar = evaluatedArray[i]['value'].replace('(', '')
                                firstVar = firstVar.replace(')', '')
                            } else if(condVar == null && evaluatedArray[i]['type'] == 'COMP'){
                                condVar = evaluatedArray[i]['value']
                            } else if(valVar == null && evaluatedArray[i]['type'] == 'NUM'){
                                valVar = evaluatedArray[i]['value'].replace('(', '')
                                valVar = valVar.replace(')', '')
                            }
                        }
                    }
                  
                    addtlValidation[key] = {'variable':firstVar, 'condition':condVar, 'value':valVar}

                }
            }

            //manually adding checking for checklist and testlist
            addtlValidationFlag = true
            addtlValidation['testList'] = {'variable':'nRep', 'condition':'>=', 'value':'1'}
            addtlValidation['checkList'] = {'variable':'nRep', 'condition':'>=', 'value':'2'}
            
        }

        if(Object.keys(warningArray).length  > 0){
            showNotifications(warningArray, 'warning');
        } else {
            $('#warning-design-div-msg-panel').html('');
            $('#warning-design-div-panel').addClass('hidden');
        }

        if(Object.keys(errorArray).length > 0){
            fieldName = "";
            for(let key in errorArray){
                if(errorArray[key] != ""){
                    flag = 1;
                    fieldName = key;
                } else {
                    $("#"+key).removeClass("invalid");
                    const index = errorArray.indexOf(key);

                    if (index > -1) { // only splice array when item is found
                        errorArray.splice(index, 1); // 2nd parameter means remove one item only
                    }
                };
            }
            if(flag == 1){
                $("#submit_design_btn").attr('disabled', 'disabled');
                showNotifications(errorArray, 'error');

                if(fieldName != ""){
                    $('#'+fieldName).addClass('invalid');
                }
            } else {
                errorArray = [];
                $('#error-design-div-msg-panel').html('');
                $('#error-design-div-panel').addClass('hidden');
            }
        } else {
            $('#error-design-div-msg-panel').html('');
            $('#error-design-div-panel').addClass('hidden');
        }
    }

    function workflowCheck(){
        if(workflow == 'upload'){
            //hide elements
            $('#save-parameters-btn').addClass('hide-action-btn');

            //disable elements
            // $("#workflow-switch-id").attr('disabled', 'disabled');
            $(".additional").each(function(){
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).attr('disabled', 'disabled');
                }
            });
            $("#assign_rep_btn").attr('disabled', 'disabled');
            $('#hide-right-design-nav').trigger('click');
            $("#show-right-design-nav").addClass('hide-action-btn');
            $("#generate-design-btn").addClass('hide-action-btn');
            $("#total-occurrences-notif").addClass('hide-action-btn');
        }

        if(planRecordCreated == 'true') {
            $("#workflow-switch-id").attr('disabled', 'disabled');
        }
    }

    function computeFormula(fieldId){
        key = fieldId;

        var options = [];

        for(var i=0; i<allowedValRules[key].length; i++){
            var expression = allowedValRules[key][i]['expression'];

            if(expression.includes('if(')){
                var proceedArr = checkProceed(expression);

                expression = proceedArr['expression'];
                if(!proceedArr['proceed']){
                    continue;
                }
            }
            options = computeTokens(expression, options);
        }

        if(options!= null && options != undefined && Object.keys(options).length > 0){
            options.sort((a,b)=>a-b);
            addDropDownOptions(fieldId, options);
        }
    }

    function checkProceed(expression){ //check 1st for if condition

        var tokens = tokenize(expression);
        var proceed = true;

        if(expression.includes("if(")){
            var condArray  = extractExpression(tokens);

            proceed = condArray['proceed'];
            tokens = condArray['expression'];
        }

        return {
            'proceed' : proceed,
            'expression' : tokens
        };
    }

    function computeTokens(expression, options){
     
        var evaluatedArray = evaluateExpression(expression, false);

        var tokens = evaluatedArray;
        var passValidation = 1;

        if(tokens.length == 1){ //function
            if(tokens[0]['value'].includes('factor')){
                var options = eval(tokens[0]['value']);
                return options;
            }
        } else if(tokens.length == 3 && tokens[0]['value'] == 'value' && Object.keys(options).length == 0){
            //generate list of values
            if(tokens[1]['value'] == '<='){
                var options = [ ...Array(tokens[2]['value']).keys() ].map( i => i+1);
                return options;
            } else if(tokens[1]['value'] == '<'){
                var options = [ ...Array((tokens[2]['value']-1)).keys() ].map( i => i+1);
                return options;
            } 
        } else if(tokens.length == 3 && tokens[0]['value'] == 'value' && Object.keys(options).length > 0){
            var newOptions = [];

            var var2 = tokens[2]['value'];
            if(tokens[2]['type'] == 'VAR'){
                var2 = checkVarValue(var2);
            }
            for(var i=0; i<options.length; i++){
                var evaluateStr = options[i]+tokens[1]['value']+var2;
                if(eval(evaluateStr)){
                    newOptions.push(options[i]);
                }
            }
            return newOptions;
        } else if(tokens.length == 3 && tokens[0]['value'].includes('factor')){

            var options = eval(tokens[0]['value']);

            var newOptions = [];
            
            var var2 = tokens[2]['value'];
            if(tokens[2]['type'] == 'VAR'){
                var2 = checkVarValue(var2);
            }
            
            for(var i=0; i<options.length; i++){

                var evaluateStr = options[i]+tokens[1]['value']+var2;
                newOptions.push(eval(evaluateStr));
            }
            return newOptions;
        }
    }

    function addDropDownOptions(dropDownName, valueArray) {

        var dropDown = document.getElementById(dropDownName);

        var dropDownHidden = document.getElementById(dropDownName+"-hidden");
        if(dropDownHidden != undefined){
          dropDown = dropDownHidden;
        }

        if(dropDown !== null){
          
            $('#' + dropDownName).empty();
            $('#' + dropDownName).append('<option hidden><i style="color:gray;">Select a value</i></option>');
            for (var fieldIndex in valueArray) { // then populatem them
                var flagValue = 0;
          
                if(dropDown != undefined && dropDown.min != "null" && parseInt(valueArray[fieldIndex]) < parseInt(dropDown.min)){
                    flagValue = 1;
                }
                if (dropDown != undefined && dropDown.max != "null" && parseInt(valueArray[fieldIndex]) > parseInt(dropDown.max)){
                    flagValue = 1;
                }
                if(flagValue == 0){
                    $('#' + dropDownName).append('<option value="' + valueArray[fieldIndex] + '">' + valueArray[fieldIndex] + '</option>');
                }
            }

            var reindex = valueArray.filter(val => val);
            if(reindex.length === 1){
                $('#'+dropDownName).val(valueArray[Object.keys(valueArray)[0]]);
                $('#'+dropDownName).trigger('change');
            }
        }
    }

    function computeTotalPlots(){
        //compute for test entries
        var testPlots = checkPlots = localCheckPlots = 0
        if(paramValues['methodReplicateTestEntries'] == false || paramValues['methodReplicateTestEntries'] == 'Random'){
            var repTestVar = $('#repTest').val() == undefined ? 0 : $('#repTest').val()
            var nTestVar = $('#nTest').val()  == undefined ? 0 : $('#nTest').val() 
            testPlots = parseInt(repTestVar) * parseInt(nTestVar)
        }

        //compute for check entries
        if(paramValues['methodReplicateCheckEntries'] == false || paramValues['methodReplicateCheckEntries'] == 'Random'){
            var repCheckVar = $('#repCheck').val() == undefined ? 0 : $('#repCheck').val() 
            var nCheckVar = $('#nCheck').val() == undefined ? 0 : $('#nCheck').val() 
            checkPlots = parseInt(repCheckVar) * parseInt(nCheckVar)
        }

        //compute for local check entries
        var repLocalCheckVar = $('#repLocalCheck').val() == undefined ? 0 : $('#repLocalCheck').val() 
        var nLocalCheckVar = $('#nLocalCheck').val() == undefined ? 0 : $('#nLocalCheck').val() 
        localCheckPlots = parseInt(repLocalCheckVar) * parseInt(nLocalCheckVar)

        totalPlots = testPlots + checkPlots + localCheckPlots
        paramValues['totalPlots'] = totalPlots

    }


JS
);
?>
<style>
input.disabled {
    pointer-events: none;
    opacity: 0.5;
    cursor: not-allowed;
}
.readonly-select {
    background-color:#d5d5d5;
    opacity:0.5;
    border-radius:3px;
    cursor:not-allowed;
    position:absolute;
    top:0;
    bottom:0;
    right:0;
    left:0;
}
.fa-download:hover, .fa-download:active{
    color: #005580;
    cursor: pointer;
}
#notif_field{
    font-size: 14px;
    padding: 10px;
    height: 30px;
    line-height: 0px;
}
span.badge {
    margin-left: 14px;
    float: left;
}
.collapsible span.badge {
    margin-top: 0px;
}

.summary{
    display:none;
}
.container-design {
    /*background-color: white; */
    min-height: 300px;
    max-height: 600px;
    position: relative;
    overflow: auto;
    /* margin: 5px 0; */
    border: 1px solid #ccd5d8;
    border-radius: 5px;
}

.container-color {
    background-color: #bab9b9;
}
.panel-error > .card-panel {
    background: #ffcdd2;
    color: #F44336;
}
.panel-warning > .card-panel {
    background: #d9edf7;
    color: #31708f;
}
#uploadSummary td,th {
    padding: 5px 0px !important;
    display: table-cell;
    text-align: left;
    vertical-align: middle;
    border-radius: 2px;
}
.side-design-nav{
	border-radius: 4px 0px 0px 4px;
    padding: 5px;
    background: #ccc;
    margin-top: 10px;
}
.input-field.col label, label {
    left: 0.75rem;
    font-size: .8rem;
    color: #333333;
}
.view-entity-content{
    background: #ffffff;
    min-height: 300px;
    max-height: 620px;
    position: relative;
    overflow: auto;
}
.left{
    margin-right:5px !important;
}
.hide-action-btn{
    display:none;
}
select#serpentinePrep option[value="CO"]   { background-image:url(../../../images/column-order.png);}
select#serpentinePrep option[value="CS"] { background-image:url(../../../images/column-order.png); }
select#serpentinePrep option[value="RS"] { background-image:url(../../../images/column-order.png); }
select#serpentinePrep option[value="R0"] { background-image:url(../../../images/column-order.png); }
</style>