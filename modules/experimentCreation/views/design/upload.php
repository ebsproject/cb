<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders upload planting array web form
 */
use kartik\widgets\FileInput;
use macgyer\yii2materializecss\widgets\Button;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


$hiddenRice = 'hidden';
$hiddenMaize = 'hidden';

if(empty($design) && $acrossEnvDesign == 'random'){ //currently Unspecified only

    $design = 'Sparse Testing';

    echo '<p>Accepted Sparse testing fieldbook templates: <a href="#" class="download-file" id="template1file"><b>Sparse Template</b></a></p>';
    echo '<p>Required columns per each design:<a style="margin-left:10px;" href="#" class="download-file" id="requiredheaders"><b>Required Columns</b></a></p>';
} else {
    echo '<p>Accepted fieldbook templates: <a href="#" class="download-file" id="template1file"><b>Design Template</b></a></p>';
    echo '<p>Required columns per each design:<a style="margin-left:10px;" href="#" class="download-file" id="requiredheaders"><b>Required Columns</b></a></p>';
}
echo '<div id="download-link-div"></div>';
echo '<div class="col col-md-6 upload-panel" style="margin-top:15px">';

echo FileInput::widget([
    'name' => 'upload_design_array',
    'pluginOptions' => [
        'uploadAsync' => false,
        'uploadUrl' => Url::to(['/experimentCreation/upload/prevalidate-file', 'id'=>$id, 'program'=>$program, 'occurrenceDbIds'=>$occurrenceDbIds]),
        'allowedFileExtensions' => ['csv'],
        'browseClass' => 'btn waves-effect waves-light browse-btn',
        'showCaption' => false,
        'showRemove' => false,
        'showZoom' => false,
        'browseLabel' => 'Browse',
        'removeLabel' => '',
        'uploadLabel' => 'Prevalidate',
        'uploadTitle' => 'Validate file',
        'uploadIcon' => '<i class="material-icons inline-material-icon">file_upload</i>',
        'uploadClass' => 'btn',
        'removeClass' => 'btn btn-danger',
        'cancelClass' => 'btn grey lighten-2 black-text',
        'dropZoneTitle' => \Yii::t('app', 'Drag and drop file here or<br/>click Browse<br/><small>Use the design array files in CSV format. Specify required fields to validate.</small>'),
        'overwriteInitial' => true,
        'maxFileCount' => 1,
        'autoReplace' => true,
        'disabledPreviewTypes' => ['text'],
        'fileActionSettings' => [
            'showUpload' => false,
            'showZoom' => false,
            'removeIcon' => '<i class="fa fa-trash"></i>',
            'showRotate' => false,
            'indicatorNew' => '<i class="fa fa-plus-circle text-warning"></i>',
            'indicatorSuccess' => '<i class="fa fa-check-circle text-success"></i>',
            'indicatorError' => '<i class="fa fa-exclamation-circle text-danger"></i>',
            'indicatorLoading' => '<i class="fa fa-hourglass-half text-muted"></i>',
            'indicatorPaused' => '<i class="fa fa-pause-circle text-info"></i>',
        ],
        'theme'=>'fa'
    ],
    'pluginEvents' => [
        'fileclear' => 'function() { validate("clear"); }',
        'filereset' => 'function() { validate("test"); }',
        'fileloaded' => 'function() { validate(); }',
        'fileuploaderror' => 'function() { validate("clear"); }',
    ],
    'options' => [
        'multiple' => false,
        'accept' => 'csv/*', 
        'id' => 'design-array-file-input'
    ]
]);
echo '</div>';

echo '<div id="preview-panel" class="col col-md-6 pull-right hidden" style="overflow:auto"></div>';

$submitDesignFile = Url::to(['upload/submit-file-new','id'=>$id, 'program'=>$program, 'occurrenceDbIds'=>implode("|",$occurrenceDbIds)]);
$downloadUrl = Url::to(['/experimentCreation/upload/check-file']);
$directDownloadUrl = Url::to(['/experimentCreation/upload/download-file']);
$validateUrl = Url::to(['/experimentCreation/upload/validate-via-api','id'=>$id, 'program'=>$program]);
$occurrenceDbIdsStr = implode(";", $occurrenceDbIds);
$this->registerJs(<<<JS

var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>'
var validateSuccess = false
var tmpfilename = ''
var totalOccurrences = 0
var information = null
var fileType = ''
var fileTmpName = ''
var paramDesignStr = ''
var paramSetInfo = null
var design = '$design'

// validate if required fields specified upon file load
function validate(action){
    // if file is cleared
    if(action == 'clear'){
        $('#preview-panel').html('');
        $('.confirm-upload-design-array').addClass('hidden');  
    }
}
// remove file upon clicking re-upload
$(document).on('click', '.browse-btn', function(e){
    // remove previously uploaded file
    $('.fileinput-remove').trigger('click');
    // change browse label
    $('.browse-btn span').text('Browse');
    $('#cancel-upload-design-array').addClass('modal-close-button');
});

// hide validate button upon remove of file
$(document).on('click','.fileinput-remove, .kv-file-remove', function(e){
    $('#preview-panel').html('');
    $('.confirm-upload-design-array').addClass('hidden');  
    $('.validate-upload-design-array').addClass('hidden');
});
// cancel upload mapped plots and generate location, reset modal centent
$(document).on('click', '#cancel-upload-design-array', function(){
    $('.upload-design-array-form-modal-body').html('');
});
// hide validate button upon remove of file
$(document).on('click','.confirm-upload-design-array', function(e){
    $('#upload-design-array-modal').modal('show')
    var modalBody = '.upload-design-array-form-modal-body';
    $(modalBody).html(loading);
    $.ajax({
        url: '$submitDesignFile',
        type: 'post',
        dataType: 'json',
        data:{
            'occurrences':totalOccurrences,
            'tmpfilename':tmpfilename,
            'designInfo': '$designInfo',
            'information':information,
            'paramDesignStr': paramDesignStr
        },
        success: function(response){
            $('#cancel-upload-design-array').trigger('click');
            
            location.reload();
            
        },
        error: function(){
            var errorMessage = '<i>There was a problem while submitting the file. Please try to upload again.</i>';
            $(modalBody).html(errorMessage);
        }
    });
});

// cancel upload mapped plots and generate location, reset modal centent
$(document).on('click', '.download-file', function(){
    var id = this.id;
    $.ajax({
        url: '$downloadUrl'+'?filename='+id,
        type: 'post',
        dataType: 'json',
        data:{
            design:design
        },
        success: function(response){
            if(!response['success']){
                var notif = "<i class='material-icons red-text left'>close</i> There is no available sample file for this crop.";
                Materialize.toast(notif,5000);
            } else {
        
                var a = document.createElement('a');
                a.setAttribute('href', '$directDownloadUrl'+'?path='+response['path']+'&filename='+response['filename']);
                a.setAttribute('download', response['filename']);

                var aj = $(a);
                aj.appendTo('download-link-div');
                aj[0].click();
                aj.remove();
               
            }
        },
        error: function(){
            var notif = "<i class='material-icons red-text left'>close</i> A design must be selected first to download the corresponding template. Please try again.";
            Materialize.toast(notif,5000);
        }
    });
});

// if validation is successful
$('#design-array-file-input').on('filebatchuploadsuccess', function(e, data) {

    var response = data.response;
    $('#preview-panel').html('');
    // if successfully validated, display preview
    if(response.success){
        var htmlString = ''
        validateSuccess = true
        tmpfilename = response.fileName
        fileTmpName = response.tmpFileName
        fileType = response.type
        paramSetInfo = response.parameterOccurrence

        htmlString = '<div class="card-panel"><div class="row"> <h5 class="card-header">Summary Information</h5>'
        information = response.information
        for (var prop in information) {
            if(prop == 'totalOccurrences'){
                totalOccurrences = information[prop]['value']
            }
            if(information[prop]['label'] != undefined){
                 htmlString += '<div>'+
                  '<div class="col col-md-8"><label>'+information[prop]['label']+'</label></div>'+
                  '<div class="col col-md-4 bold" style="word-wrap:break-word;">'+information[prop]['value']+'</div>'+
                  '</div>'
            }   
        }

        var paramSet = response.parameter_set
        var designs = response.designArray
        

        //create ui for design array
        var designSelect = '<select '
        var designOptions = '<option value="" disabled selected>Select design</option>'

        for(var i=0; i < designs.length; i++){
            designOptions += '<option value="'+designs[i]['id']+'">'+designs[i]['value']+'</option>'
        }
        designOptions += '</select>'

        htmlString += '<br><br><p class="red-text">Select design for each parameter set.</p><table>'
        for(var i=0; i < parseInt(paramSet); i++){
            htmlString += '<tr><td style="padding:0px;">Parameter Set '+(i+1)+'</td><td style="padding:0px;">'+designSelect+' id="paramSet-'+(i+1)+'" class="browser-default parameter-sets">'+designOptions+'</td></tr>'
        }
        htmlString += '</table>'

        htmlString += '<a id="validate-api-btn" href="#" class="btn btn-primary waves-effect waves-light disabled" style="margin-top:10px;margin-bottom:5px;"><span title="Validate file" >Validate</span></a>'
        htmlString += '<div id="success-nofif-id" class="kv-upload-progress" style="display: none;"><div class="progress"><div class="progress-bar bg-success progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">Successfully Validated</div></div></div>'
        htmlString += '<div class="clearfix"></div></div></div>'
        $('#preview-panel').html(htmlString)
        $('#preview-panel').removeClass("hidden")

        // $('.confirm-upload-design-array').removeClass('hidden')
        $('#cancel-upload-design-array').removeClass('modal-close-button')
    }
});  

// cancel upload mapped plots and generate location, reset modal centent
$(document).on('change', '.parameter-sets', function(){
    //check all parameter sets if design is assigned
    var flag = 0;
    $(".parameter-sets").each(function(){
        
        if(this.value == '' || this.value == undefined){
            flag = 1;
        }
    });

    if(flag == 0){
        //enable validate button
        $('#validate-api-btn').removeClass('disabled')
        $('#validate-api-btn').addClass('pulse')
    } else {
        $('#validate-api-btn').addClass('disabled')
        $('#validate-api-btn').removeClass('pulse')
    }
});

// validate file via api call
$(document).on('click', '#validate-api-btn', function(){
    var paramDesign = []
    $(".parameter-sets").each(function(){
        if(this.value != '' || this.value != undefined){
            paramDesign.push(this.value)
        }
    });
    paramDesignStr = paramDesign.join(',')
    
    $.ajax({
        url: '$validateUrl',
        type: 'post',
        dataType: 'json',
        data:{
            tmpFileName:fileTmpName,
            type:fileType,
            designStr:paramDesignStr,
            paramSetInfo:paramSetInfo
        },
        success: function(response){
            if(response.success){
                $('.confirm-upload-design-array').removeClass('hidden')
                $('.confirm-upload-design-array').addClass('pulse')
                $('#validate-api-btn').addClass('disabled')
                $('#validate-api-btn').removeClass('pulse')
                $('#success-nofif-id').attr('style', 'display:block')

            } else {
                $('#design-array-file-input').trigger('fileuploaderror')

                //show error notification
                $('#design-array-file-input').fileinput('showUserError', response.error, false, false);
                $('.file-upload-indicator').html('<i class="fa fa-exclamation-circle text-danger"></i>');
                $('.file-upload-indicator').attr('title', 'Upload Error')
                $('.kv-upload-progress').attr('style', 'display:none')
                $('.confirm-upload-design-array').addClass('hidden')
            }
        },
        error: function(){
            var notif = "<i class='material-icons red-text left'>close</i> Please try again";
            Materialize.toast(notif,5000);
        }
    });
});
JS
);
?>

<style>
#summarytable td,th {
    padding: 0px 0px !important;
    display: table-cell;
    text-align: left;
    vertical-align: middle;
    border-radius: 2px;
}

.card-header {
    margin-left: 11px;
    font-weight: bold;
}

.border-top {
    border-top: 1px solid #eee;
    padding-top: 14px;
    margin-top: 15px;
    margin-bottom: 10px;
}
</style>


