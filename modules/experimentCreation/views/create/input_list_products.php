<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for input products for entry list selection
 */
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div id="loading-list-div" hidden>
  <div class="progress"><div class="indeterminate"></div></div>
</div>
<div id="input-notif-container" class="hidden">
    <div class="alert warning"><div class="card-panel"><span id="input-notif"></span></div></div>
</div>
<div id="notification" class="hidden"></div>
<div style="height:25px !important" class="row">
    <div class="col col-sm-6 view-entity-content">
        <h4>
            Valid Germplasm
        </h4>
    </div>
    <div style="padding:0px !important" class="col col-sm-6">

        <?php if($dataProvider->getTotalCount() > 0){ ?>

            <button id="add-list-products-btn" class="btn btn-primary waves-effect waves-light pull-right">
                <?= \Yii::t('app', 'Add Entries') ?>
                <i class="material-icons right">add</i>
            </button>

        <?php
            }
        ?>


        <button id="back-btn" class="btn btn-primary waves-effect waves-light pull-right">
            <?= \Yii::t('app', 'Back') ?>
            <i class="material-icons right">keyboard_backspace</i>
        </button>


    </div>
</div>
<div id="input-list-products-view">

<style>
    .differentRow {
        background-color: salmon;
    }
</style>
<div class="row">
    <?php
    echo $grid = GridView::widget([
        'pjax' => true, // pjax is set to always true for this demo
        'dataProvider' => $dataProvider,
        'id' => 'input-list-prod-gridview',
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
        'toggleData'=>true,
        'columns'=>[
            ['class' => 'yii\grid\SerialColumn'], 
            [
                'attribute'=>'inputProductName',
                'label' => Yii::t('app', 'Input Germplasm'),
                'format' => 'raw',
                'enableSorting' => false,
                'value' => function($data){
                    return $data['inputProductName'];
                },
                'contentOptions' => function ($model, $key, $index, $column)  use ($germplasmCounter){
                    if($model['designation'] != $model['inputProductName']){
                        return ['style' => 'background-color:salmon'];
                    }
                    if($germplasmCounter[$model['germplasmDbId']] > 1){
                        return ['style' => 'background-color:#e3a868; font-weigth:bold;'];
                    }
                },
            ],
            [
                'attribute'=>'designation',
                'label' => Yii::t('app', 'Standard Germplasm'),
                'format' => 'raw',
                'enableSorting' => false,
                'value' => function($data){
                    return $data['designation'];
                },
                'contentOptions' => function ($model, $key, $index, $column) {
                    if($model['designation'] != $model['inputProductName']){
                        return ['style' => 'background-color:#4CAF50'];
                    }
                },
            ]
        ]
    ]);

    ?>
</div>

<div class="row">

    Invalid Germplasm:

    <?php
        $values =  implode(", ", $invalidProducts);
    ?>

    <?= Html::textArea('product-list-textarea', $values,[
    'id'=>'product-list-textarea',
    'readonly'=>true,
    'style'=>'background-color:white; resize: none; min-height: 30px !important; '
]); ?>

</div>

</div>

<?php
    $addEntriesUrl = Url::to(['create/add-entries-from-list','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $checkThresholdCountUrl = Url::to(['create/check-threshold-count','id'=>$id]);
?>

<script>
    var germplasmCounterMax = parseInt('<?php echo $germplasmCounterMax; ?>');
    var exceededValidationLimitMessage = '<?php echo $exceededValidationLimitMessage; ?>';
    $(document).ready(function(){

        if(germplasmCounterMax > 1 || exceededValidationLimitMessage != '') {
            var inputNotifMessage = exceededValidationLimitMessage;
            var duplicateMessage = 'There are duplicate germplasm in your list.';

            if(germplasmCounterMax > 1 && exceededValidationLimitMessage != '') inputNotifMessage += '<br>'
            if(germplasmCounterMax > 1) inputNotifMessage += duplicateMessage

            $('#input-notif').html(inputNotifMessage);
            $('#input-notif-container').removeClass('hidden');
        }

        $('#add-list-products-btn').click(function(e){
            
            var url = '<?php echo $addEntriesUrl; ?>';
            $('#loading-list-div').show();
            $('#add-list-products-btn').attr('disabled', 'disabled');
            $('#back-btn').attr('disabled', 'disabled');

            //Notification while background job is being created
            $.ajax({
                url: checkThresholdCountUrl,
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'createEntries',
                    toolStep: 'entries',
                    method: 'create',
                    selectedItemsCount: '<?php echo isset($validProducts) ? count($validProducts) : 0;?>'
                },
                success: function(response) {
                    isBgUsedFlag = response;
                    $('#notification').removeClass('hidden');
                    if(isBgUsedFlag == true){
                        $('#notification').html('<div class="alert ">'+ 
                                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                    '<i class="fa fa-info-circle"></i>'+
                                        ' A background job is being created.' +
                                '</div></div>');
                    }
                }
            });
            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    experimentId: '<?php echo $id; ?>',
                    productList: '<?php echo str_replace("'","\'",json_encode($validProducts)); ?>',
                    processId: '<?php echo $processId;?>'
                },
                success: function(response) {
                    $('#option-germplasm').removeAttr('disabled');
                    $('#option-package-label').removeAttr('disabled');
                    $('#option-package-code').removeAttr('disabled');

                    $('#loading-list-div').hide();
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully added the selected records.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();

                    $('a[href="#from-input-list-link"]').click();
                    $('#products-view').html('');

                    $('#add-entries-modal').modal('hide');

                    $.pjax.reload({
                      container: '#dynagrid-ec-entry-list-pjax',
                      replace:false
                    });

                    $("textarea").val("");
                    $('#product-textarea').show();

                },
                error: function() {

                }
            });

        });

    });


</script>