<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for harvested plots for entry list selection
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Place;
use kartik\editable\Editable;
?>

<div style="height:25px !important" class="row">
    <div class="col col-sm-6 view-entity-content">
        <h4>
            <?php
            echo  \Yii::t('app','Create location list');
            ?>
        </h4>
    </div>
    <div style="padding:0px !important" class="col col-sm-6">

        <?php if($dataProvider->getTotalCount() > 0){ ?>

            <button id="add-locations-btn" class="btn btn-primary waves-effect waves-light pull-right disabled" title="<?= \Yii::t('app', 'Save location list') ?>" style="margin-right:20px;max-width: 105px;">
                <?= \Yii::t('app', 'Save') ?>
                <i class="material-icons right">send</i>
            </button>

        <?php
            }
        ?>
        <button id="back-btn" class="btn btn-primary waves-effect waves-light grey pull-right" title="<?= \Yii::t('app', 'Go to list') ?>" style="margin-right:5px;max-width: 105px;">
            <?= \Yii::t('app', 'Back') ?>
            <i class="material-icons right">keyboard_backspace</i>
        </button>

    </div>
</div>

<div class="row">

    <?php
        echo '<div class="col col-md-6">';
        echo '<div class="input-field required field-saved-list-name form-group"><label class="control-label" for="saved-list-name">'.\Yii::t('app', 'Location list name').'</label>';
        echo Html::input('text','saved_list_name_value', null, ['id'=>'saved_list_name']).'</div>';
        echo '<p id="name-help-block" class="help-block-info has-error hidden">'.\Yii::t('app', 'Name cannot be blank').'</p></div>';

        echo '<div class="col col-md-6">';
        echo '<div class="input-field field-saved-list-abbrev required form-group"><label for="saved-list-abbrev" class="control-label">'.\Yii::t('app', 'Location list abbrev').'</label>';
        echo Html::input('text','saved_list_abbrev_value', null, ['id'=>'saved_list_abbrev']).'</div>';
        echo '<p id="abbrev-help-block" class="help-block-info has-error hidden">'.\Yii::t('app', 'Abbrev cannot be blank').'</p></div>';
        
        echo $grid = GridView::widget([
            'pjax' => true, // pjax is set to always true for this demo
            'dataProvider' => $dataProvider,
            'options'=>['id' => 'location-excluded-browser', 'class'=>'col col-md-12'],
            'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
            'toggleData'=>true,
            'columns'=>[
                [
                    'class' => 'yii\grid\CheckboxColumn', 
                    'name' => 'location_create_selection[]',
                    'checkboxOptions' => function ($data, $key, $index, $column) {
                        return ['value' => $data['id'], 'class'=>'add-to-list-class'];
                    },
                    'header' => Html::checkBox('location_members_all', false, [
                        'class' => 'select-on-check-all'
                    ]),
                ],
                [
                    'class' => 'yii\grid\SerialColumn'
                ],
                [
                    'attribute'=>'abbrev',
                    'enableSorting' => false
                ],
                [
                    'attribute'=>'display_name',
                    'enableSorting' => false
                ],
                [
                    'attribute'=>'remarks',
                    'enableSorting' => false
                ],
            ]
        ]);

    ?>
</div>

<?php
    $saveListUrl = Url::to(['create/save-location-list','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>

<script>
    $(document).ready(function(){
        $('#add-locations-btn').click(function(){
            var flag = 0;
            var nameVal = $("#saved_list_name").val();
            if(nameVal.length > 0){
                $('#saved_list_name').removeClass('invalid');
                $('.field-saved-list-name > label').removeClass("has-error");
                $('#name-help-block').addClass("hidden");
            }else{
                $('#saved_list_name').addClass('invalid');
                $('.field-saved-list-name > label').addClass("active");
                $('.field-saved-list-name > label').addClass("has-error");
                $('#name-help-block').removeClass("hidden");
                flag = 1;
            }

            var abbrevVal = $("#saved_list_abbrev").val();
            if(abbrevVal.length > 0){
                $('#saved_list_abbrev').removeClass('invalid');
                $('.field-saved-list-abbrev > label').removeClass("has-error");
                $('#abbrev-help-block').addClass("hidden");
            }else{
                $('#saved_list_abbrev').addClass('invalid');
                $('.field-saved-list-abbrev > label').addClass("active");
                $('.field-saved-list-abbrev > label').addClass("has-error");
                $('#abbrev-help-block').removeClass("hidden");
                flag=1;
            }
            
            if(flag == 0){
                var selectedArray = [];

                $('[name="location_create_selection[]"]:checked').each (function (i, e){
                    selectedArray.push ($(this).val());
                });

                var url = '<?php echo $saveListUrl; ?>';

                //get the entries of this experiment
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        location: selectedArray,
                        nameVal:nameVal,
                        abbrevVal:abbrevVal
                    },
                    success: function(response) {

                    },
                    error: function() {

                    }
                });
            }
        });
        
        $('.add-to-list-class').on('click', function() {
            var selectedArray = [];
            $('[name="location_create_selection[]"]:checked').each (function (i, e){
                selectedArray.push ($(this).val());
            });
            
            if(selectedArray.length > 0){
                $('#add-locations-btn').removeClass('disabled');
            } else{
                $('#add-locations-btn').addClass('disabled');
            }
        });
        
        $('[name="location_members_all"]').on('click', function() {
            $("[name='location_create_selection[]']").prop('checked', $(this).prop('checked')).trigger('change');
            var selectedArray = [];
            $('[name="location_create_selection[]"]:checked').each (function (i, e){
                selectedArray.push ($(this).val());
            });
            if(selectedArray.length > 0){
                $('#add-locations-btn').removeClass('disabled');
            } else{
                $('#add-locations-btn').addClass('disabled');
            }
        });

        $("#saved_list_name").keyup(function() {
            $('.field-saved-list-abbrev > label').addClass("active");
            $("#saved_list_abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase()); 
        });
    });
</script>

<style>
.has-error{
    color:red;
}
</style>