<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /**
  * Renders view for copy experiment
  */
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<?php
    if($partitionView){
?> 
        <span class="new badge">Limited view</span>&nbsp;&nbsp;<em><?= \Yii::t('app', 'We limit the view for this page due to large number of records. Click the View All button to show all records in page.') ?></em>
        <a class="waves-effect waves-light btn pull-right" id="view-all-experiments-id"><i class="material-icons right">visibility</i>View All</a>
<?php 
    }
?>
<p><?= \Yii::t('app', 'Below are the list of existing experiments you can copy entries from. Click the search icon to filter the list.') ?></p>

<div class="input-field no-margin search-entry-field-div" style="width:35%">
  <input id="searchTextExperiment" type="search" placeholder="Type filter" class="autocomplete search-text-add-entry-search-input" autocomplete="off" autofocus="" value="">
  <i class="material-icons search-text-entry-clear-all-search-btn" title="Clear all" style="opacity: 0; margin-right:20px; font-size:1.5rem;margin-top:10px;">clear</i>
  <i class="material-icons search-text-entry-submit-btn" style="margin-top:10px; font-size:1.5rem" title="Search">search</i>
</div>


<div id="experiment-browser">
<?php

echo $grid = GridView::widget([
    'pjax' => true, // pjax is set to always true for this demo
    'dataProvider' => $dataProvider,
    'id' => 'copy-experiment',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-entry-table'],
    'striped'=>false,
    'columns' => [
        [
          'class' => 'yii\grid\SerialColumn',
          'header' => false
        ], 
        [
            'class'=>'kartik\grid\ActionColumn',
            'header' => false,
            'template' => '{select}',
            'buttons' => [
                'select' => function ($url, $model, $key) {
                    
                    return Html::a('<i class="material-icons">folder_open</i>',
                        '#',
                        [
                            'class'=>'select-experiment',
                            'title' => Yii::t('app','View entries of this experiment'),
                            'data-experiment-id' => $model['experimentDbId'],
                        ]
                    );
                },
            ]
        ],
        [
            'attribute'=>'experimentName',
            'label' => Yii::t('app', 'Experiment'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'experimentCode',
            'label' => Yii::t('app', 'Experiment Code'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'entryCount',
            'label' => Yii::t('app', 'Entry Count'),
            'value' => function ($data) {
              return '<span class="new badge">'.$data['entryCount'].'</span>';
            },
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'projectName',
            'label' => Yii::t('app', 'Project'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'experimentYear',
            'label' => Yii::t('app', 'Year'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'stageCode',
            'label' => Yii::t('app', 'Stage'),
            'format' => 'raw',
            'enableSorting' => false,
        ]
        
    ],
    'toggleData'=>true,
]);

?>
</div>


<div id="entries-browser"></div>
<div id="notification" class="hidden"></div>

<?php
    $entryListExperimentUrl = Url::to(['create/render-experiment-entries']); 
    $viewAllExperiments = Url::to(['view/view-all-copy-experiments', 'id'=>$currentExperimentId, 'program'=>$program, 'processId'=>$processId]);
?>

<script>
    $(document).on('click', '#back-btn', function(){ 
        $('#experiment-browser').show();
        $('#entries-browser').hide();      
        $('.search-entry-field-div').show();
    });
    $(document).on('click', '#view-all-experiments-id', function(){ 
        var url = '<?php echo $viewAllExperiments; ?>';
        window.location = url;
    });

    $(document).ready(function(){
        $('.select-experiment').click(function(){
            var experimentId = $(this).attr('data-experiment-id');
            var currentExperimentId = '<?php echo $currentExperimentId; ?>';
            var url = '<?php echo $entryListExperimentUrl; ?>';
            $('#experiment-browser').hide();
            $('#entries-browser').show();

            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    experimentId: experimentId,
                    currentExperimentId: currentExperimentId,
                    processId: '<?php echo $processId;?>',
                    program: '<?php echo $program;?>'
                },
                success: function(response) {
                    $('.search-entry-field-div').hide();
                    $('#entries-browser').html(response);
                },
                error: function() {

                }
            });
        });
    });
</script>



