<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div style="height:25px !important" class="row">
    <div class="col col-sm-6 view-entity-content">
        <h4>
            <?php
            echo isset($experiment['experimentName']) ? $experiment['experimentName'] : "";
            ?>
        </h4>
    </div>
    <div style="padding:0px !important" class="col col-sm-6">
        
        <?php if(count($dataProvider->allModels) > 0){ ?>

            <button id="select-btn" class="btn btn-primary waves-effect waves-light pull-right">
                <?= \Yii::t('app', 'Add Entries') ?>
                <i class="material-icons right">add</i>
            </button>

        <?php
            }
        ?>
        <button id="back-btn" class="btn btn-primary waves-effect waves-light pull-right">
            <?= \Yii::t('app', 'Back') ?>
            <i class="material-icons right">keyboard_backspace</i>
        </button>
    </div>
</div>

<div class="row">
    <?php
    echo GridView::widget([
        'pjax' => false, // pjax is set to always true for this demo
        'dataProvider' => $dataProvider,
        'id' => 'copy-experiment-entries',
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
        'striped'=>false,
        'toggleData'=>true,
        'columns'=>[
            [
              'class' => 'yii\grid\SerialColumn',
              'header' => false
            ], 
            [
                'attribute'=>'entryName',
                'label' => Yii::t('app', 'Entry Name'),
                'format' => 'raw',
                'enableSorting' => false,
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ]
            ],
            [
                'attribute'=>'designation',
                'label' => Yii::t('app', 'Germplasm Name'),
                'format' => 'raw',
                'enableSorting' => false,
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ]
            ],
            'germplasmState',
            'germplasmNameType',
            [
                'attribute'=>'entryCode',
                'label' => Yii::t('app', 'Entry Code'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'entryType',
                'label' => Yii::t('app', 'Entry Type'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'entryClass',
                'label' => Yii::t('app', 'Entry Class'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'parentage',
                'label' => Yii::t('app', 'Parentage'),
                'format' => 'raw',
                'enableSorting' => false,
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ]
            ],

        ]
    ]);
    ?>
</div>

<?php
    $copyExperimentUrl = Url::to(['create/add-entries-experiment']);
?>
<script>
    $(document).ready(function(){
        $('#select-btn').click(function(e){
            var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
            $('#entries-browser').html(loading);

            var url = '<?php echo $copyExperimentUrl; ?>';
            var experimentId = '<?php echo $experimentId; ?>';
            var currentExperimentId = '<?php echo $currentExperimentId; ?>';
            var checkThresholdCountUrl = '<?php echo Url::to(['create/check-threshold-count']); ?>';
             
            //Notification while background job is being created
            $.ajax({
                url: checkThresholdCountUrl+'?id='+experimentId,
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'createEntries',
                    toolStep: 'entries',
                    method: 'create',
                    mode: 'copy'
                },
                async: false,
                success: function(response) {
                    isBgUsedFlag = response;
                    $('#notification').removeClass('hidden');
                    if(isBgUsedFlag == true || isBgUsedFlag == 1){
                        $('#notification').html('<div class="alert ">'+ 
                                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                    '<i class="fa fa-info-circle"></i>'+
                                        ' A background job is being created.' +
                                '</div></div>');
                    }
                }
            });

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    experimentId: experimentId,
                    currentExperimentId: currentExperimentId,
                    processId: '<?php echo $processId;?>',
                    program: '<?php echo $program;?>'
                },
                success: function(response) {
                    if(response){
                        $('#entries-browser').hide();
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully added the selected records.</span>";
                            Materialize.toast(notif, 5000);
                        }
                        e.preventDefault();
                        e.stopImmediatePropagation();
                        $('#experiment-browser').show();
                        $('.search-entry-field-div').show();

                        $('a[href="#from-input-list-link"]').click();
                        $('#add-entries-modal').modal('hide');
                        
                        $.pjax.reload({
                          container: '#dynagrid-ec-entry-list-pjax',
                          replace:false
                        });
                    } else {
                        $('#entries-browser').hide();
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Unable to add entries. Some entries might not be valid. Please contact data managers.</span>";
                            Materialize.toast(notif, 5000);
                        }
                        e.preventDefault();
                        e.stopImmediatePropagation();
                        $('#experiment-browser').show();
                        $('.search-entry-field-div').show();

                        $('a[href="#from-input-list-link"]').click();
                        $('#add-entries-modal').modal('hide');
                        
                        $.pjax.reload({
                          container: '#dynagrid-ec-entry-list-pjax',
                          replace:false
                        });
                    }
                },
                error: function() {  
                }
            });
        });
    });
</script>
