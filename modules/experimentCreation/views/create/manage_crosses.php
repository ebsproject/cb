<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders crosses tab for the experiment creation
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\grid\GridView;

use kartik\dynagrid\DynaGrid;
use kartik\tabs\TabsX;

$disabled = '';

if (strpos($model['experimentStatus'],'cross list created') === false) {
  $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php', [
  'processId' => $processId,
  'model' => $model,
  'program' => $program,
  'saveBtnLabel' => 'Next',
  'nextBtn' => $disabled,
  'btnName' => 'next-btn',

]);

$items = [
  [
    'label' => '<i class="fa fa-plus-square"></i>' . Yii::t('app', ' Add Crosses'),
    'active' => true,
    'content' => Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/_input_crosses.php', [
      'id' => $id,
      'femaleSearchModel' => $femaleSearchModel,
      'maleSearchModel' => $maleSearchModel,
      'program' => $program,
      'processId' => $processId,
      'femaleDataProvider' => $femaleDataProvider,
      'maleDataProvider' => $maleDataProvider,
      'maxCrossCount' => $maxCrossCount,
      'type'=>$type,
      'changeTypeUrl' => Url::to(['manage-crosses', 'id' => $id, 'program' => $program, 'processId' => $processId])
    ]),
    'url' => Url::to(['manage-crosses', 'id' => $id, 'program' => $program, 'processId' => $processId]),
  ],
  [
    'label' => '<i class="fa fa-plus-square"></i>' .  Yii::t('app', ' Cross Pattern'),
    'active' => false,
    'url' => Url::to(['cross-pattern/manage-cross-pattern', 'id' => $id, 'program' => $program, 'processId' => $processId]),
  ],
  [
    'label' => '<i class="fa fa-edit"></i>' . Yii::t('app', ' Manage Crosses'),
    'active' => false,
    'url' => Url::to(['manage-crosslist', 'id' => $id, 'program' => $program, 'processId' => $processId]),
  ]
];
?>
<div class="white crosses-tab">
  <br />
  <?php
  echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_LEFT,
    'sideways' => true,
    'encodeLabels' => false,
    'pluginOptions' => [
      'enableCache' => false
    ],
  ]);

  // Modal for validation
  Modal::begin([
    'id' => 'required-field-modal',
    'header' => '<h4><i class="fa fa-bell"></i> ' . \Yii::t('app', 'Notification') . '</h4>',
    'footer' => Html::a(\Yii::t('app', 'OK'), ['#'], ['id' => 'cancel-save-btn', 'class' => 'btn btn-primary waves-effect waves-light', 'data-dismiss' => 'modal']),
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static']
  ]);
  echo '<div class="row">
    <p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>' . \Yii::t('app', 'Please correct the errors below before proceeding.') . '</span></p>
    </div><div class="row" id="required-field-notif"></div>';
  Modal::end();

  //Modal for unused parents
  Modal::begin([
    'id' => 'unused-parents-modal',
    'header' => "<h4><i class='material-icons orange-text left'>warning</i> Unused Parents</h4>",
    'footer' =>	Html::a(\Yii::t('app', ' Back '), '#', ['id' => 'back-nxt-btn', 'data-dismiss' => 'modal', 'class' => 'btn btn-primary waves-effect waves-light modal-close black-text grey lighten-2']) . '&nbsp;' . Html::a(\Yii::t('app', 'Proceed'), '#', ['id' => 'proceed-nxt-btn', 'class' => 'btn btn-primary waves-effect waves-light modal-close teal']) . '&emsp;',
    'size' => 'modal-md',
    'options' => ['data-backdrop' => 'static']
  ]);
  echo \Yii::t('app', ' Some of the parents are not used in crossing. You may filter or sort the #♀ and #♂ columns to check. If you wish to proceed, you will be redirected to the next step.');
  Modal::end();

  $checkParentsUrl = Url::to(['create/check-parents', 'id' => $id, 'program' => $program]);

  ?>
</div>
<?php
//javascript
$viewEntityUrl = Url::to(['/search/default/viewentity']);
$addToCrossesUrl = Url::to(['create/add-crosses']);
$addStoredCrossesUrl = Url::to(['create/add-stored-crosses']);
$getDuplicateListUrl = Url::to(['create/get-duplicate-list']);
$pairParentsUrl = Url::to(['create/pair-parents', 'id' => $id]);
$createSelfCrossesUrl = Url::to(['create/create-self-crosses']);
$updateSessionUrl = Url::to(['create/update-session']);
$checkStatusUrl  = Url::to(['create/check-status', 'id' => $id, 'action' => 'manage-crosses']);
$configValues = json_encode($configValues);
$getAllEntriesUrl = Url::to(['create/get-all-entries']);
$checkCrossesSelectionStatusUrl = Url::to(['create/check-crosses-selection-status']);
$validationCrossStepUrl = Url::to(['default/cross-step-validation','id'=>$id]);
$getSelectedItemsUrl = Url::to(['default/get-user-selection', 'id' => $id]);
$changeTypeUrl  = Url::to(['create/manage-crosses', 'id' => $id, 'program' => $program, 'processId'=>$processId]);

$browserIdFemale = 'dynagrid-ec-add-crosses-female';
$browserIdMale = 'dynagrid-ec-add-crosses-male';

$maxCrossCountDisplay = number_format($maxCrossCount);
Yii::$app->view->registerJs(
  "
    checkStatusUrl = '" . $checkStatusUrl . "',
    experimentId = '" . $id . "',
    requiredFieldJson = '" . $configValues . "',
    checkStatValue = 'cross list created'
    ;",
  \yii\web\View::POS_HEAD
);

$this->registerJsFile("@web/js/check-required-field-status.js", [
  'depends' => ['app\assets\AppAsset'],
  'position' => \yii\web\View::POS_END
]);

$this->registerJs(<<<JS
  resetSessionVariables();
  refresh();
  var resetSelection = false;
  var switchInputType = '$type';
  $(document).ready(function(){
    refresh();
    validationCrossStep();
  });

  //Update the female count
  $(document).on('ready pjax:complete','#$browserIdFemale-pjax', function(e) {
      e.stopPropagation();
      e.preventDefault();
      var femaleSelected = localStorage.getItem('ec_crosses_female_entry_ids');
      femaleSelected = getSelectedEntries(femaleSelected);
      var femaleCount = getSelectedCount(femaleSelected);
      checkSelectedParentCounter(femaleCount, 'selectedFemaleCount', 'selectedFemaleText');
  });


  //Update the male count
  $(document).on('ready pjax:complete','#$browserIdMale-pjax', function(e) {
      e.stopPropagation();
      e.preventDefault();
      var maleSelected = localStorage.getItem('ec_crosses_male_entry_ids');
      maleSelected = getSelectedEntries(maleSelected);
      var maleCount = getSelectedCount(maleSelected);
      checkSelectedParentCounter(maleCount, 'selectedMaleCount', 'selectedMaleText');
  });

  $(document).on('ready pjax:success', function(e) {
    e.stopPropagation();
    e.preventDefault();
    if(resetSelection){
        $('#reset-$browserIdMale').trigger('click');
        resetSelection = false;
    }
    refresh();
    validationCrossStep();
  });

  // after pjax
  $(document).on('pjax:end', function(e) {

    // check to see if female and male browser selection should be reset
    setTimeout(function(){
      $.ajax({
        url: '$checkCrossesSelectionStatusUrl',
        data: {
        },
        type: 'POST',
        async: true,
        success: function(response) {
          response = JSON.parse(response);

          // reset female selection
          if(response.resetFemaleSelection) {
            localStorage.removeItem('ec_crosses_female_entry_ids');
            $('.female-loader').addClass('hidden');
            checkSelectedParentCounter(0, 'selectedFemaleCount', 'selectedFemaleText');
            $("input:checkbox.$browserIdFemale-select").each(function(){
                $("#pair-btn").prop('disabled', true);
                $(this).removeAttr('checked');
                $('.$browserIdFemale-select').prop('checked',false);
                $('.$browserIdFemale-select').removeAttr('checked');
                $("input:checkbox.$browserIdFemale-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');
            });
          }

          // reset male selection
          if(response.resetMaleSelection) {
            localStorage.removeItem('ec_crosses_male_entry_ids');
            $('.male-loader').addClass('hidden');
            checkSelectedParentCounter(0, 'selectedMaleCount', 'selectedMaleText');
            $("input:checkbox.$browserIdMale-select").each(function(){
                $("#pair-btn").prop('disabled', true);
                $(this).removeAttr('checked');
                $('.$browserIdMale-select').prop('checked',false);
                $('.$browserIdMale-select').removeAttr('checked');
                $("input:checkbox.$browserIdMale-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');
            });
          }
        },
        error: function(e){
        }
      });
    }, 300);
  });

  $( window ).resize(function() {
    refresh();
  });

  function resetSessionVariables(){
      //reset male
      localStorage.removeItem('ec_crosses_male_entry_ids');
      $('.male-loader').addClass('hidden');
      checkSelectedParentCounter(0, 'selectedMaleCount', 'selectedMaleText');
      $("input:checkbox.$browserIdMale-select").each(function(){
          $("#pair-btn").prop('disabled', true);
          $(this).removeAttr('checked');
          $('.$browserIdMale-select').prop('checked',false);
          $('.$browserIdMale-select').removeAttr('checked');
          $("input:checkbox.$browserIdMale-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');
      });

      //reset female
      localStorage.removeItem('ec_crosses_female_entry_ids');
      $('.female-loader').addClass('hidden');
      checkSelectedParentCounter(0, 'selectedFemaleCount', 'selectedFemaleText');

      $("input:checkbox.$browserIdFemale-select").each(function(){
          $("#pair-btn").prop('disabled', true);
          $(this).removeAttr('checked');
          $('.$browserIdFemale-select').prop('checked',false);
          $('.$browserIdFemale-select').removeAttr('checked');
          $("input:checkbox.$browserIdFemale-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');
      });
  }

  //Set grid table and panel sizes
  function refresh(){
    var windowHeight = $(window).height();        
    var maxHeight = windowHeight - 320;
    $("#parent-list, #cross-list").css("height", maxHeight);        
    $("#$browserIdFemale-pjax .kv-grid-wrapper, #$browserIdMale-pjax .kv-grid-wrapper").css("height", (maxHeight - 30));
    $("#crosses-textarea").css("height", (maxHeight - 15));   
    $(".lines").css("height", (maxHeight - 15));
    $("#crosses-textarea").css("width", $('.linedwrap').width() - $('.codelines').width() - 15);
    $('.crosses-tab').css('width', $('.tabs').width());
    $('.crosses-tab').css('height', windowHeight - 175);
    $('#parent-list').css('width', $('.crosses-tab').width() - 70);
    $('.crosses-tab').css('width', $('.tabs').width());
    $("input:checkbox.$browserIdFemale-select").each(function(){
      if($(this).prop("checked") === true)  {
        $(this).parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5');
      }
    });
    $("input:checkbox.$browserIdMale-select").each(function(){
      if($(this).prop("checked") === true)  {
        $(this).parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5');
      }
    });
    showSelectionOnBrowsers();
  }

  $(document).on('click','#reset-$browserIdFemale',function(e){
    localStorage.removeItem('ec_crosses_female_entry_ids');
    $('.female-loader').addClass('hidden');
    checkSelectedParentCounter(0, 'selectedFemaleCount', 'selectedFemaleText');
  });

  $(document).on('click','#reset-$browserIdMale',function(e){
    localStorage.removeItem('ec_crosses_male_entry_ids');
    $('.male-loader').addClass('hidden');
    checkSelectedParentCounter(0, 'selectedMaleCount', 'selectedMaleText');
  });

  //when view more information is clicked
  $(document).on('click', '.view-more-info', function(e) {
    var obj = $(this);
    var entity_label = obj.data('entity_label');
    var entity_id = obj.data('entity_id');
    var entity_type = obj.data('entity_type');

    $('#entity-label').html('<i class="fa fa-info-circle"></i> ' + entity_label + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $('.view-entity-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
    
    //load content of view more information
    setTimeout(function(){ 
      $.ajax({
      url: '$viewEntityUrl',
      data: {
        entity_id: entity_id,
        entity_type: entity_type,
        entity_label: entity_label
      },
      type: 'POST',
      async: true,
      success: function(data) {
        $('.view-entity-modal-body').html(data);          
      },
      error: function(){
        $('.view-entity-modal-body').html('<i>There was a problem while loading record information.</i>'); 
      }
    });
    }, 300);
    
  });

  $(document).on('click', '#li-3', function(e) {
    e.preventDefault(); //for prevent default behavior of <a> tag.
    $('#view-entity-modal').modal('show').find('#view-entity-3').load($(this).attr('value')); //load view studies

    $('#li-2').removeClass('active');
    $('#li-1').removeClass('active');
    $('#li-3').addClass('active');
    $('#li-4').removeClass('active');
    $('#li-5').removeClass('active');
    $('#li-6').removeClass('active');
    $('#li-7').removeClass('active');
    $('#li-8').removeClass('active');

    $('#view-entity-3').removeClass('hidden').addClass('active');
    $('#view-entity-1').removeClass('active').addClass('hidden');
    $('#view-entity-2').removeClass('active').addClass('hidden');
    $('#view-entity-4').removeClass('active').addClass('hidden');
    $('#view-entity-5').removeClass('active').addClass('hidden');
    $('#view-entity-6').removeClass('active').addClass('hidden');
    $('#view-entity-7').removeClass('active').addClass('hidden');
    $('#view-entity-8').removeClass('active').addClass('hidden');
  });

  $(document).on('click', '#li-4', function(e) {
    e.preventDefault(); //for prevent default behavior of <a> tag.
    $('#view-entity-modal').modal('show').find('#view-entity-4').load($(this).attr('value')); //load view seed lots

    $('#li-2').removeClass('active');
    $('#li-1').removeClass('active');
    $('#li-4').addClass('active');
    $('#li-3').removeClass('active');
    $('#li-5').removeClass('active');
    $('#li-6').removeClass('active');
    $('#li-7').removeClass('active');
    $('#li-8').removeClass('active');

    $('#view-entity-4').removeClass('hidden').addClass('active');
    $('#view-entity-1').removeClass('active').addClass('hidden');
    $('#view-entity-2').removeClass('active').addClass('hidden');
    $('#view-entity-3').removeClass('active').addClass('hidden');
    $('#view-entity-5').removeClass('active').addClass('hidden');
    $('#view-entity-6').removeClass('active').addClass('hidden');
    $('#view-entity-7').removeClass('active').addClass('hidden');
    $('#view-entity-8').removeClass('active').addClass('hidden');
  });

  $(document).on('click', '#li-8', function(e) {
    e.preventDefault(); //for prevent default behavior of <a> tag.
    $('#view-entity-modal').modal('show').find('#view-entity-8').load($(this).attr('value')); //load view passport data

    $('#li-2').removeClass('active');
    $('#li-1').removeClass('active');
    $('#li-6').removeClass('active');
    $('#li-3').removeClass('active');
    $('#li-4').removeClass('active');
    $('#li-5').removeClass('active');
    $('#li-7').removeClass('active');
    $('#li-8').addClass('active');

    $('#view-entity-8').removeClass('hidden').addClass('active');
    $('#view-entity-7').removeClass('active').addClass('hidden');
    $('#view-entity-6').removeClass('active').addClass('hidden');
    $('#view-entity-1').removeClass('active').addClass('hidden');
    $('#view-entity-2').removeClass('active').addClass('hidden');
    $('#view-entity-3').removeClass('active').addClass('hidden');
    $('#view-entity-4').removeClass('active').addClass('hidden');
    $('#view-entity-5').removeClass('active').addClass('hidden');
  });
  
  // Check text area on change
  $('#crosses-textarea').on('input change keyup', function() {
    var crossCount = $('#crosses-textarea').val().split('\\n').filter(Boolean).length;

    displayAddReplaceButton();
    $('#pair-btn').prop('disabled', checkSelectedParents());
    $('#cross-count').html(crossCount);
    
  });

  /**
   * Display add and replace buttons
   * Reset line numbers
   */
  function displayAddReplaceButton(){
    if($('#crosses-textarea').val().trim() != ''){
      $("#add-cross-btn, #replace-cross-btn, #delete-cross-btn").prop('disabled', false);
    }else{
      $("#add-cross-btn, #replace-cross-btn, #delete-cross-btn").prop('disabled', true);
      var lineCount = $('.lineno').length;
      for(i = 1; i <= lineCount; i++){
        $('#row' + i ).html(i);
      }
    }
  }

  /**
   * Add cross action
   */
  $(document).on('click', '#add-cross-btn', function(){
    crossCount = $('#cross-count').html();

    if(crossCount > 0 && crossCount <= $maxCrossCount){
      textAreaVal = $('#crosses-textarea').val();
      $('#validation-header').html('Adding crosses...');		
      $('#validation-body').html(' <div class="progress"><div class="indeterminate"></div></div>');
      $('#clear-btn, #ok-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-cross-list-btn').css('display','none');
      $('#validation-modal').modal('show');
        
      if($('#crossInputSwitch-id').prop('checked') == true){
          var switchInputType = 'entryCode';
      } else {
          var switchInputType = 'designation';
      }

      //validate crosses
      setTimeout(function(){ 
        $.ajax({
          url: '$addToCrossesUrl',
          type: 'post',
          dataType: 'json',
          data: {
            experimentId:'$id',
            crosses:textAreaVal,
            operation: 'add',
            switchInputType: switchInputType
          },
          success: function(data) {
            dataCount = 0;
            resetTextarea();

            if(data['invalidCount'] > 0){
              crossInput = '';
              dataCount = data['crossCount'];
              $.each( data['crosses'], function(key, value) {
                if(value['status'] == 'invalid'){
                  cnt = (typeof cnt == 'undefined') ? 1 : cnt + 1;
                  if(crossInput == ''){
                    crossInput = value['crossInput'];
                  }else{
                    crossInput = crossInput+'\\n' + value['crossInput'];
                  }
                  $('#row'+ cnt).html("<div title = '" + value['remarks']  + "'class='lineno lineselect'><i class='smaller fa fa-question-circle'></i> " + cnt + "</div>");
                }
              });
              cnt = 0;
              $("#crosses-textarea").val(crossInput);
              $('#cross-count').html(data['invalidCount']);
              displayAddReplaceButton();
            }
            header = '<i class="glyphicon glyphicon-list-alt"></i> Crosses Creation Summary';
            
            //set footer options
            $('#add-more-btn').css('display','');
            $('#manage-cross-list-btn').css('display','');
            $('#clear-btn, #ok-btn, #back-btn, #yes-btn, #no-btn').css('display','none');
            
            $('#validation-header').html(header);
            $('#validation-body').html(data['message']);
          },
          error: function(e) {
            showRefreshModal();
          }
        });
      }, 300);
    }
    else{
      showLimitModal();
    }
    
  });

  function sleep(milliseconds) {
      const date = Date.now();
      let currentDate = null;
      do {
        currentDate = Date.now();
      } while (currentDate - date < milliseconds);
  }

  //check experiment status
  $(document).on('click', '#add-more-btn', function(){
    checkStatus();
    resetSelection = true;

    $('#reset-$browserIdFemale').trigger('click');
  });

  //check experiment status
  $(document).on('click', '#load-duplicate-btn', function(e) {
    var operation = $('#load-duplicate-btn').prop('dataset').operation;
    var sessionName = $('#load-duplicate-btn').prop('dataset').session;
    $('#add-proceed-duplicate-btn').attr('operation', operation)
    $('#back-btn, #add-proceed-duplicate-btn').css('display','');
    $('#validation-modal').modal('hide');
    //validate crosses
    var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
    $('#designation-modal-body').html(loading);

    setTimeout(function(){
      // retrieve selected crosses
      $.ajax({
        url: '$getDuplicateListUrl',
        data: {
          experimentDbId: '$id',
          operation: operation,
          session: sessionName
        },
        type: 'POST',
        dataType: 'json',
        success: function(data) {
          $('#designation-modal-body').html(data);
        },
        error: function(){
          $('#designation-modal-body').html('<i>There was a problem while loading information.</i>'); 
        }
      });
    },10);
    
  });

  /**
   * Proceed to add duplicates action
   */
  $(document).on('click', '#add-proceed-duplicate-btn', function(){
      var operation = $('#add-proceed-duplicate-btn').attr('operation');
      $('#entry-form-matrix-grid, #back-btn, #add-proceed-duplicate-btn, #entry-form-instructions').hide();
      $('#entry-form-loading').show();

      setTimeout(function(){ 
        $.ajax({
          url: '$addStoredCrossesUrl',
          type: 'post',
          dataType: 'json',
          data: {
            experimentId:'$id',
            operation: operation
          },
          success: function(data) {
            if(data!==null){
              //reset text area
              var invalidCount = data['invalidCount'];
              var totalCount = data['totalCount'];
              var validCount = totalCount - invalidCount;
              resetTextarea();
              message = '<i class="green-text fa fa-check-circle fa-2x"></i> A total of <b>' + validCount + '</b> crosses were successfully created.';

              if(invalidCount && invalidCount < totalCount){
                message = message + ' Crosses which are already in the cross list were excluded.';
              }else if(invalidCount == totalCount){
                message = '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> No new crosses added. All crosses already exist in the cross list.</p>';
              }

              $('#designation-modal-body').html(message);
              //set footer options
              $('#add-more-btn, #manage-cross-list-btn').show();
              $('#add-proceed-duplicate-btn, #back-btn').hide();
            }else{
              showRefreshModal();
            }
          },
          error: function(e) {
            showRefreshModal();
          }
        });
      }, 300);
  });

  /**
   * Reset textarea
   */
  function resetTextarea(){
    $("#crosses-textarea").val('');
    $("#cross-count").html(0);

    var lineCount = $('.lineno').length;
    for(i = 1; i <= lineCount; i++){
      $('#row' + i ).html(i);
    }
    displayAddReplaceButton();
  }

  /**
   * Female entry no selection update
   */
  $(document).on('change', '.female-entno', function(e){
    var femaleEntryDbId = $(this).val();
    var key = this.getAttribute('data-row');
    var maleEntryDbId = $('#male-' + key).val();
    var parentDbId = femaleEntryDbId +','+ maleEntryDbId;

    setTimeout(function(){ 
        $.ajax({
          url: '$updateSessionUrl',
          type: 'post',
          dataType: 'json',
          data: {
            experimentId:'$id',
            femaleEntryDbId: femaleEntryDbId,
            maleEntryDbId: maleEntryDbId,
            parentDbId: parentDbId,
            key: key,
          },
          success: function(count) {
          },
          error: function(e) {
          }
        });
      }, 300);
    
  });
  
  /**
   * Male entry no selection update
   */
  $(document).on('change', '.male-entno', function(e){
    var maleEntryDbId = $(this).val();
    var key = this.getAttribute('data-row');
    var femaleEntryDbId = $('#female-' + key).val();
    var parentDbId = femaleEntryDbId +','+ maleEntryDbId;

    setTimeout(function(){ 
        $.ajax({
          url: '$updateSessionUrl',
          type: 'post',
          dataType: 'json',
          data: {
            experimentId:'$id',
            femaleEntryDbId: femaleEntryDbId,
            maleEntryDbId: maleEntryDbId,
            parentDbId: parentDbId,
            key: key,
          },
          success: function(count) {
          },
          error: function(e) {
          }
        });
      }, 300);
    
    });

  /**
   * Replace crosses action
   */
  $(document).on('click', '#replace-cross-btn', function(){
    crossCount = $('#cross-count').html();

    if(crossCount > 0 && crossCount <= $maxCrossCount){
      var header = '<i class="material-icons left">swap_vert</i> Replace all Crosses';
      var message = '<p>Are you sure that you want to replace all crosses? This will erase <b>ALL</b> the current crosses in this experiment and add the valid crosses in the text area.</p>'
      //set footer options
      $('#yes-btn, #no-btn').css('display','');
      $('#clear-btn, #ok-btn, #back-btn, #add-more-btn, #manage-cross-list-btn').css('display','none');

      //set modal display
      $('#validation-header').html(header);
      $('#validation-body').html(message);
      $('#validation-modal').modal('show');
    }else{
      showLimitModal();
    }
  });

  /**
   * Delete crosses action
   */
  $("#delete-cross-btn").on('click', function(){

      crossCount = $('#cross-count').html();
      var header = "<h4><i class='material-icons left'>delete_sweep</i> Clear input crosses</h4>";
      var message = '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> This will erase <b>ALL</b> the crosses in the text area.'
      //set footer options
      $('#clear-btn, #back-btn').css('display','');
      $('#ok-btn, #no-btn, #add-more-btn, #manage-cross-list-btn, #yes-btn').css('display','none');

      //set modal display
      $('#validation-header').html(header);
      $('#validation-body').html(message);
      $('#validation-modal').modal('show');
  });

  // Reset text area
  $(document).on('click', '#clear-btn', function(){
    $('#validation-modal').modal('hide');
    resetTextarea();
  });

  // Confirmed replace crosses action
  $(document).on('click', '#yes-btn', function(){
    crossCount = $('#cross-count').html();

    if(crossCount > 0 && crossCount <= $maxCrossCount){
      textAreaVal = $('#crosses-textarea').val();
      $('#validation-header').html('Replacing crosses...');		
      $('#validation-body').html(' <div class="progress"><div class="indeterminate"></div></div>');
      $('#clear-btn, #ok-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-cross-list-btn').css('display','none');
      $('#validation-modal').modal('show');

      if($('#crossInputSwitch-id').prop('checked') == true){
          var switchInputType = 'entryCode';
      } else {
          var switchInputType = 'designation';
      }
      //validate crosses
      setTimeout(function(){ 
        $.ajax({
          url: '$addToCrossesUrl',
          type: 'post',
          dataType: 'json',
          data: {
            experimentId:'$id',
            crosses:textAreaVal,
            operation: 'replace',
            switchInputType: switchInputType
          },
          success: function(data) {
            dataCount = 0;
            resetTextarea();

            if(data['invalidCount'] > 0){
              crossInput = '';
              dataCount = data['crossCount'];
              $.each( data['crosses'], function(key, value) {
                if(value['status'] == 'invalid'){
                  cnt = (typeof cnt == 'undefined') ? 1 : cnt + 1;
                  if(crossInput == ''){
                    crossInput = value['crossInput'];
                  }else{
                    crossInput = crossInput+'\\n' + value['crossInput'];
                  }
                  $('#row'+ cnt).html("<div title = '" + value['remarks']  + "'class='lineno lineselect'><i class='smaller fa fa-question-circle'></i> " + cnt + "</div>");
                }
              });
              cnt = 0;

              $("#crosses-textarea").val(crossInput);
              $('#cross-count').html(data['invalidCount']);
              displayAddReplaceButton();
            }
            header = '<i class="glyphicon glyphicon-list-alt"></i> Crosses Replacement Summary';
            
            //set footer options
            $('#add-more-btn').css('display','');
            $('#manage-cross-list-btn').css('display','');
            $('#clear-btn, #ok-btn, #back-btn, #yes-btn, #no-btn').css('display','none');
            
            $('#validation-header').html(header);
            $('#validation-body').html(data['message']);
          },
          error: function(e) {
            showRefreshModal();
          }
        });
      }, 300);
    }
    else{
      showLimitModal();
    }
  });

  /**
   *  Select/unselect all female parents
   */
  $(document).on('click', '#$browserIdFemale-select-all', function(e) {
    var pair = true;
    var unset = false;

    //check if there's at least 1 male
    $("input:checkbox.$browserIdMale-select").each(function(){
      if($(this).prop("checked") === true)  {
        pair = false;
      }
    });

    $("#pair-btn").prop('disabled', pair);
    if($(this).prop("checked") === true)  {
      $(this).attr('checked','checked');
      $('.$browserIdFemale-select').attr('checked','checked');
      $('.$browserIdFemale-select').prop('checked',true);
      $('.$browserIdFemale-select:checkbox').parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5');
    }else{
      unset = true;
      $("#pair-btn").prop('disabled', true);
      $(this).removeAttr('checked');
      $('.$browserIdFemale-select').prop('checked',false);
      $('.$browserIdFemale-select').removeAttr('checked');
      $("input:checkbox.$browserIdFemale-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');    
    }
    $('.female-loader').removeClass('hidden');
    $('#selectedFemaleCount').html('');

    if(unset) { // clear all
        localStorage.removeItem('ec_crosses_female_entry_ids');
        $('.female-loader').addClass('hidden');
        checkSelectedParentCounter(0, 'selectedFemaleCount', 'selectedFemaleText');
    } else {  // select all
      // retrieve all entries (ajax call)
      $.ajax({
        url: '$getAllEntriesUrl',
        type: 'post',
        data: {
          sessionName: 'female_entry_ids',
          switchInputType: switchInputType
        },
        cache: false,
        success: function(response) {
          response = JSON.parse(response)
          localStorage.setItem('ec_crosses_female_entry_ids', JSON.stringify(response));  // put entries to localStorage
          var femaleCount = getSelectedCount(response); // update count
          $('.female-loader').addClass('hidden');
          checkSelectedParentCounter(femaleCount, 'selectedFemaleCount', 'selectedFemaleText');
        },
        error: function(e) {
          console.log(e);
        }
      });
    }

  });

  /**
   *  Select/unselect all male parents
   */
  $(document).on('click', '#$browserIdMale-select-all', function(e) {
    var pair = true;
    var unset = false;

    //check if there's at least 1 female
    $("input:checkbox.$browserIdFemale-select").each(function(){
      if($(this).prop("checked") === true)  {
        pair = false;
      }
    });

    $("#pair-btn").prop('disabled', pair);
    if($(this).prop("checked") === true)  {
      $(this).attr('checked','checked');
      $('.$browserIdMale-select').attr('checked','checked');
      $('.$browserIdMale-select').prop('checked',true);
      $('.$browserIdMale-select:checkbox').parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5'); 

    }else{
      unset = true;
      $("#pair-btn").prop('disabled', true);
      $(this).removeAttr('checked');
      $('.$browserIdMale-select').prop('checked',false);
      $('.$browserIdMale-select').removeAttr('checked');
      $("input:checkbox.$browserIdMale-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');      
    }
    $('.male-loader').removeClass('hidden');
    $('#selectedMaleCount').html('');

    if(unset) { // clear all
        localStorage.removeItem('ec_crosses_male_entry_ids');
        $('.male-loader').addClass('hidden');
        checkSelectedParentCounter(0, 'selectedMaleCount', 'selectedMaleText');
    } else {  // select all
      // retrieve all entries (ajax call)
      $.ajax({
        url: '$getAllEntriesUrl',
        type: 'post',
        data: {
          sessionName: 'male_entry_ids',
          switchInputType: switchInputType
        },
        cache: false,
        success: function(response) {
          response = JSON.parse(response)
          localStorage.setItem('ec_crosses_male_entry_ids', JSON.stringify(response));  // put entries to localStorage
          var maleCount = getSelectedCount(response); // update count
          $('.male-loader').addClass('hidden');
          checkSelectedParentCounter(maleCount, 'selectedMaleCount', 'selectedMaleText');
        },
        error: function(e) {
          console.log(e);
        }
      });
    }
  });

  // click row in female browser
  $(document).on('click', '#$browserIdFemale tbody tr', function(e) {
    var pair = true;
    var this_row = $(this).find('input:checkbox')[0];
    var unset = false;
    var entryDbId = $(this).attr('data-key');

    if($('#crossInputSwitch-id').prop('checked') == true){
        var designation = $('#' + this_row.id).attr('data-entryCode');
    } else {
        var designation = $('#' + this_row.id).attr('data-designation');
    }
    
    if(this_row.checked){
      unset = true;
      this_row.checked = false;  
      $('#$browserIdFemale-select-all').prop("checked", false);
      $(this_row.id).prop("checked", false);  
      $(this).find('*').addClass('lighten-5').removeClass('lighten-4');
    }else{
      $(this).find('*').addClass('lighten-4').removeClass('lighten-5');    
      this_row.checked = true;
      $(this_row.id).prop("checked");
    }

    pair = checkSelectedParents();
    $("#pair-btn").prop('disabled', pair);

    var femaleSelected = localStorage.getItem('ec_crosses_female_entry_ids');
    femaleSelected = getSelectedEntries(femaleSelected);
    femaleSelected = (femaleSelected == null) ? {} : femaleSelected;
    if(unset) { // remove selected
      delete femaleSelected[entryDbId];
      femaleSelected = (Object.keys(femaleSelected).length === 0) ? null : femaleSelected;
    } else {  // add selected
      femaleSelected[entryDbId] = designation;
    }
    localStorage.setItem('ec_crosses_female_entry_ids', JSON.stringify(femaleSelected));  // save selected to localStorage

    var femaleCount = getSelectedCount(femaleSelected); // update count
    checkSelectedParentCounter(femaleCount, 'selectedFemaleCount', 'selectedFemaleText');
  });

  //set selected parent counter
  function checkSelectedParentCounter(response, countId, textId){
    count = parseInt(response).toLocaleString();
    text = ' selected items';
    if (response == 1) {
      text = ' selected item';
    } else if(response == 0) {
      count = 'No';
    }
    $('#' + countId).html(count);
    $('#' + textId).html(text);
  }

  //check if there's at least 1 male and 1 female selected
  function checkSelectedParents(){
    var male, female = false;			
    $("input:checkbox.$browserIdMale-select").each(function(){
      if($(this).prop("checked") === true)  {
        male = true;
      }
    });

    $("input:checkbox.$browserIdFemale-select").each(function(){
      if($(this).prop("checked") === true)  {
        female = true;
      }
    });

    return !(female && male);
  }

  // click row in male browser
  $(document).on('click', '#$browserIdMale tbody tr', function(e) {
    var pair = true;
    var this_row = $(this).find('input:checkbox')[0];
    var unset = false;
    var entryDbId = $(this).attr('data-key');

    if($('#crossInputSwitch-id').prop('checked') == true){
        var designation = $('#' + this_row.id).attr('data-entryCode');
    } else {
        var designation = $('#' + this_row.id).attr('data-designation');
    }

    if(this_row.checked){
      unset = true;
      this_row.checked = false;  
      $('#$browserIdMale-select-all').prop("checked", false);
      $(this_row.id).prop("checked", false);
      $(this).find('*').addClass('lighten-5').removeClass('lighten-4');
    }else{
      $(this).find('*').addClass('lighten-4').removeClass('lighten-5');    
      this_row.checked = true;
      $(this_row.id).prop("checked");
    }

    pair = checkSelectedParents();
    $("#pair-btn").prop('disabled', pair);

    var maleSelected = localStorage.getItem('ec_crosses_male_entry_ids');
    maleSelected = getSelectedEntries(maleSelected);
    maleSelected = (maleSelected == null) ? {} : maleSelected;
    if(unset) { // remove selected
      delete maleSelected[entryDbId];
      maleSelected = (Object.keys(maleSelected).length === 0) ? null : maleSelected;
    } else {  // add selected
      maleSelected[entryDbId] = designation;
    }
    localStorage.setItem('ec_crosses_male_entry_ids', JSON.stringify(maleSelected));  // save selected to localStorage

    var maleCount = getSelectedCount(maleSelected); // update count
    checkSelectedParentCounter(maleCount, 'selectedMaleCount', 'selectedMaleText');
  });

  /**
   * Pair male and female parents
   */
  $(document).on('click', '#pair-btn', function (e){
    //remove previous validation
    var lineCount = $('.lineno').length;
    for(i = 1; i <= lineCount; i++){
      $('#row' + i ).html(i);
    }
    $('#validation-header').html('Pairing parents...');		
    $('#validation-body').html(' <div class="progress"><div class="indeterminate"></div></div>');
    $('#clear-btn, #ok-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-cross-list-btn').css('display','none');
    $('#validation-modal').modal('show');
    $("#pair-btn").prop('disabled', true);

    var femaleDesignations = localStorage.getItem('ec_crosses_female_entry_ids');
    var maleDesignations = localStorage.getItem('ec_crosses_male_entry_ids');

    //validate crosses
    setTimeout(function(){
      $.ajax({
        url: '$pairParentsUrl',
        type: 'post',
        dataType: 'json',
        data: {
          femaleDesignations: femaleDesignations,
          maleDesignations: maleDesignations
        },
        success: function(data) {
          $('.toast').css('display','none');
          var withInvalidCross = data['invalidCross'];
          var textAreaVal = $('#crosses-textarea').val();
          var crosstxt = data['crossString'];
          
          if(textAreaVal != ''){
            textAreaVal = $.trim(textAreaVal) + '\\r\\n';
          }
          $('#crosses-textarea').val(textAreaVal + crosstxt);

          if(crosstxt != ''){
            var notif = "<i class='material-icons green-text left'>done</i> Added to the text area.";
            if(withInvalidCross == true){
              notif = notif + '<br/>Some crosses are not added</br>because they are invalid.'
            }
          }
          else{
            var notif = "<i class='material-icons orange-text left'>warning</i> Same parent germplasm. Please use SELF feature for selfing.";
          }

          $("#crosses-textarea").animate({
            scrollTop:$("#crosses-textarea")[0].scrollHeight - $("#crosses-textarea").height()
          }, 0, notify(notif,e));
          
          updatedCrossCount = $('#crosses-textarea').val().split('\\n').filter(Boolean).length;

          $("#cross-count").html(updatedCrossCount);
        },
        error: function(e) {
          showRefreshModal();
        }
      });
    }, 300);
  });

  //Display modal for showing cross limit
  function showLimitModal(){
    $('#validation-header').html("<i class='material-icons orange-text left'>warning</i> Exceeded maximum crosses");		
    $('#validation-body').html("Only <b>$maxCrossCountDisplay</b> crosses are allowed per validation. Please remove the excess crosses in the text area before adding it to the cross list.");
    $('#ok-btn').css('display','');
    $('#clear-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-cross-list-btn').css('display','none');
    $('#validation-modal').modal('show');
  }

  //Display modal for refreshing page
  function showRefreshModal(){
    $('#validation-header').html("<i class='material-icons red-text left'>warning</i> Crosses not found!");		
    $('#validation-body').html('There was a problem while adding crosses. Please refresh page.');
    $('#ok-btn').css('display','');
    $('#clear-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-cross-list-btn').css('display','none');
    $('#validation-modal').modal('show');
  }

  /**
   * Display toast notification once pairing is done
   */
  function notify(notif,e){
    setTimeout(function() {
      $('#validation-modal').modal('hide');
      $("#pair-btn").prop('disabled', false);
      displayAddReplaceButton();
      var hasToast = $('body').hasClass('toast');
      if(!hasToast){
        $('.toast').css('display','none');
        Materialize.toast(notif, 5000);
      }
      e.preventDefault();
      e.stopImmediatePropagation();
    }, 300); 
  }
  /**
   * Add line numbers in the text area
   * Copyright (c) 2010 Alan Williamson
   */
  (function($) {

  $.fn.linedtextarea = function(options) {
    
    // Get the Options
    var opts = $.extend({}, $.fn.linedtextarea.defaults, options);
    
    
    /*
    * Helper function to make sure the line numbers are always
    * kept up to the current system
    */
    var fillOutLines = function(codeLines, h, lineNo){
      while ( (codeLines.height() - h ) <= 0 ){
        if ( lineNo == opts.selectedLine )
          codeLines.append("<div class='lineno lineSelect'><i class='fa fa-info-circle'></i> " + lineNo + "</div>");
        else
          codeLines.append("<div id = 'row" + lineNo + "' class='lineno'>" + lineNo + "</div>");
        
        lineNo++;
      }

      return lineNo;
    };
    
    
    /*
    * Iterate through each of the elements are to be applied to
    */
    return this.each(function() {
      var lineNo = 1;
      var textarea = $(this);
      
      /* Turn off the wrapping of as we don't want to screw up the line numbers */
      textarea.attr("wrap", "off");
      textarea.css({resize:'none'});
      var originalTextAreaWidth	= textarea.outerWidth();

      /* Wrap the text area in the elements we need */
      textarea.wrap("<div class='linedtextarea'></div>");
      var linedTextAreaDiv = textarea.parent().wrap("<div class='linedwrap'></div>");
      var linedWrapDiv = linedTextAreaDiv.parent();
      
      linedWrapDiv.prepend("<div class='lines' style='width:50px'></div>");
      
      var linesDiv = linedWrapDiv.find(".lines");
      linesDiv.height( textarea.height() + 6 );
      
      
      /* Draw the number bar; filling it out where necessary */
      linesDiv.append( "<div class='codelines'></div>" );
      var codeLinesDiv = linesDiv.find(".codelines");
      lineNo = fillOutLines( codeLinesDiv, linesDiv.height(), 1 );

      /* Move the textarea to the selected line */ 
      if ( opts.selectedLine != -1 && !isNaN(opts.selectedLine) ){
        var fontSize = parseInt( textarea.height() / (lineNo-2) );
        var position = parseInt( fontSize * opts.selectedLine ) - (textarea.height()/2);
        textarea[0].scrollTop = position;
      }

      
      /* Set the width */
      var sidebarWidth = linesDiv.outerWidth();
      var paddingHorizontal = parseInt( linedWrapDiv.css("border-left-width") ) + parseInt( linedWrapDiv.css("border-right-width") ) + parseInt( linedWrapDiv.css("padding-left") ) + parseInt( linedWrapDiv.css("padding-right") );
      var linedWrapDivNewWidth = originalTextAreaWidth - paddingHorizontal;
      var textareaNewWidth = originalTextAreaWidth - sidebarWidth - paddingHorizontal - 20;

      textarea.width(textareaNewWidth );
      // linedWrapDiv.width(linedWrapDivNewWidth );
      
      
      /* React to the scroll event */
      textarea.scroll( function(tn){
        var domTextArea = $(this)[0];
        var scrollTop = domTextArea.scrollTop;
        var clientHeight = domTextArea.clientHeight;
        codeLinesDiv.css( {'margin-top': (-1*scrollTop) + "px"} );
        lineNo = fillOutLines( codeLinesDiv, scrollTop + clientHeight, lineNo );
      });


      /* Should the textarea get resized outside of our control */
      textarea.resize( function(tn){
        var domTextArea	= $(this)[0];
        linesDiv.height( domTextArea.clientHeight + 6 );
      });

    });
  };

  // default options
  $.fn.linedtextarea.defaults = {
    selectedLine: -1,
    selectedClass: 'lineselect'
  };
  })(jQuery);

  $("#crosses-textarea").linedtextarea();

  $('#next-btn').click(function(e){
    //Add checking for required fields
    e.preventDefault();
    e.stopImmediatePropagation();
    var response = checkFields();

    requiredFieldExists = response.requiredFieldExists;
    response = response.respo;
    
    if(response['success']){
      var nextUrl = $('#next-btn').attr('data-url');

      $.ajax({
        url: '$checkParentsUrl',
        success: function(unusedParentCount){
          if(unusedParentCount > 0){
            $('#unused-parents-modal').modal();                      
          }else{
            window.location = nextUrl;
          }
        }
      });
    } else {
      $('#required-field-notif').html('');
      $('#required-field-modal').modal();

      if(response['required'] != null && response['required'] != undefined && response['required'] != ''){
        $("#required-field-notif").append('<p> <b>Required Fields</b> <br>'+response['required']+'</p>');
      }
      if(response['datatype'] != null && response['datatype'] != undefined && response['datatype'] != ''){
        $("#required-field-notif").append('<p> <b>Invalid Input</b> <br>'+response['datatype']+'</p>');
      }

      $('#required-field-modal').modal('open');
    }
  });

  function checkFields(){
    var response = [];
    var respoArr = [];
    response['success'] = true;
    response['required'] = null;
    response['datatype'] = null;
    var tempArr = [];
    var arrLabel = [];
    var count = 0;
    var requiredFieldExists = false;

    $(".required-field").each(function(){

      var test1 = $(this).val();

      requiredFieldExists = true;

      if($(this).val() == '' || $(this).val() == null || $(this).val() == ""){

        response['success'] = false;
        var idCol = this.id;
        var label =  $('#'+idCol).attr("data-label");
        var count = tempArr.length;

        if(arrLabel.indexOf(label) > -1){

        }else{
          tempArr.push((count+1)+'. '+label);
          arrLabel.push(label);
        }

      }
    });
    response['required'] = tempArr.join("<br>");

    tempArr = [];
    count = 0;
    arrLabel = [];

    $(".invalid").each(function(){
      response['success'] = false;
      var idCol = this.id;
      var label =  $('#'+idCol).attr("data-label");
      count = tempArr.length;


      if(arrLabel.indexOf(label) > -1){

      }else{
        tempArr.push((count+1)+'. '+label);
        arrLabel.push(label);
      }

    });

    response['datatype'] = tempArr.join("<br>");
    respoArr['respo'] = response;
    respoArr['requiredFieldExists'] = requiredFieldExists;

    return respoArr;
  }

  //Validation before proceeding to the next step for the Add crosses tab
  function validationCrossStep(){
    $.ajax({
        url: '$validationCrossStepUrl',
        type: 'post',
        dataType: 'json',
        success: function(response) {
          if(response == true){
            $('#next-btn').attr('disabled','true');
          }else{
            $('#next-btn').attr('disabled','false');
          }
        },
        error: function(e) {
        }
    });
  }

  // yes, proceed to next tab 
  $(document).on('click','#proceed-nxt-btn',function(e){
    
    window.location = $('#next-btn').attr('data-url');

  });

  // selfing
  $(document).on('click','.self-btn',function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var dataType = $(this).attr('data-type');
    var notif = '<i class="material-icons orange-text left">warning</i> No selected ' + dataType + ' parents.';
    var counter = dataType == 'male' ? '#selectedMaleCount' : '#selectedFemaleCount';
    var selectedParentCount = parseInt($(counter).html());
    if(selectedParentCount > 0){
      $('#validation-header').html('Adding self crosses...');		
      $('#validation-body').html(' <div class="progress"><div class="indeterminate"></div></div>');
      $('#ok-btn, #yes-btn, #no-btn, #back-btn, #add-more-btn, #manage-cross-list-btn').css('display','none');
      $('#validation-modal').modal('show');

      var designations = '{}';
      if(dataType == 'male') {
        designations = localStorage.getItem('ec_crosses_male_entry_ids');
      } else {
        designations = localStorage.getItem('ec_crosses_female_entry_ids');
      }

      setTimeout(function(){ 
        $.ajax({
          url: '$createSelfCrossesUrl',
          type: 'post',
          dataType: 'json',
          data: {
            experimentDbId: '$id',
            designations: designations
          },
          success: function(data) {
            $('#validation-modal').modal('hide');
            if(data > 0){
              notif = "<i class='material-icons green-text left'>done</i> Successfully added self crosses!";
              if(data !== selectedParentCount){
                notif = notif + '<br/>Some of the parents have already been used in selfing.';
              }
              $.pjax.reload({container: '#$browserIdFemale-pjax'}).done(function () {
                $.pjax.reload({container: '#$browserIdMale-pjax'});
              });
            }else{
              notif = "<i class='material-icons red-text left'>close</i> Selected parent/s have already been used in selfing!";
            }

            $('.toast').css('display','none');
            Materialize.toast(notif, 5000);
            checkStatus();
          },
          error: function(e) {
            showRefreshModal();
          }
        });
      }, 300);
    }
    else{
      $('.toast').css('display','none');
      Materialize.toast(notif, 5000);
    }

  });

  //retrieve user's session selection
  function getSessionUserSelection(sessionName){
    var userSelection;

    $.ajax({
      type: 'POST',
      url: '$getSelectedItemsUrl',
      data:{sessionName:sessionName},
      async: false,
      success: function(response){
          userSelection = response;
      }
    });
    
    return userSelection;
  }

    // shows selection for female and male browser
    function showSelectionOnBrowsers() {
        // female browser
        var femaleSelected = localStorage.getItem('ec_crosses_female_entry_ids');
        femaleSelected = getSelectedEntries(femaleSelected);
        femaleSelected = (femaleSelected == null) ? {} : femaleSelected;

        $("input:checkbox.$browserIdFemale-select").each(function(){
            var entryDbId = $(this).parent("td").parent("tr").attr('data-key');
            if(femaleSelected.hasOwnProperty(entryDbId)) {
                $(this).attr('checked','checked');
                $(this).attr('checked','checked');
                $(this).prop('checked',true);
                $(this).parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5'); 
            }
        });

        var femaleCount = getSelectedCount(femaleSelected); // update count
        checkSelectedParentCounter(femaleCount, 'selectedFemaleCount', 'selectedFemaleText');

        // male browser
        var maleSelected = localStorage.getItem('ec_crosses_male_entry_ids');
        maleSelected = getSelectedEntries(maleSelected);
        maleSelected = (maleSelected == null) ? {} : maleSelected;

        $("input:checkbox.$browserIdMale-select").each(function(){
            var entryDbId = $(this).parent("td").parent("tr").attr('data-key');
            if(maleSelected.hasOwnProperty(entryDbId)) {
                $(this).attr('checked','checked');
                $(this).attr('checked','checked');
                $(this).prop('checked',true);
                $(this).parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5'); 
            }
        });

        var maleCount = getSelectedCount(maleSelected); // update count
        checkSelectedParentCounter(maleCount, 'selectedMaleCount', 'selectedMaleText');
    }

    // count items inside a json object which contains selected items in a browser
    function getSelectedCount(jsonObj) {
        return (jsonObj == null) ? 0 : Object.keys(jsonObj).length;
    }

    // parse json string from localStorage and return a json object
    function getSelectedEntries(jsonStringFromLocalStorage) {
        try {
            var entriesObj = JSON.parse(jsonStringFromLocalStorage);
            if (entriesObj && typeof entriesObj === "object") {
                return entriesObj;
            }
        }
        catch (e) { }

        return null;
    }

JS);
?>

<style>
  .linedwrap {
    border: 1px solid #c0c0c0;
    padding: 3px;
  }

  .linedtextarea {
    padding: 0px;
    margin: 0px;
  }

  .linedtextarea textarea {
    padding-right: 0.3em;
    padding-top: 0.3em;
    border: 0;
  }

  .linedwrap .lines {
    margin-top: 0px;
    width: 50px;
    float: left;
    overflow: hidden;
    border-right: 1px solid #c0c0c0;
    margin-right: 10px;
  }

  .linedwrap .codelines {
    padding-top: 5px;
  }

  .linedwrap .codelines .lineno {
    color: #AAAAAA;
    padding-right: 0.5em;
    padding-top: 0.0em;
    text-align: right;
    white-space: nowrap;
  }

  .linedwrap .codelines .lineselect {
    color: red;
    padding-right: 0px;
  }

  .linedwrap .codelines .linevalid {
    color: green;
    padding-right: 0px;

  }

  .smaller {
    font-size: 12px;
  }

  .parent-box {
    margin: 0px;
    padding: 0px !important;
  }

  .pull-right {
    margin-right: 3px;
  }

  input[type=text]:not(.browser-default) {
    height: 2.5rem;
  }

  .crosses-tab {
    margin-top: 75px;
    padding-left: 0px;
  }

  .tabs-krajee.tab-sideways .nav-tabs>li {
    height: 20px;
    width: 150px;
    margin-bottom: 130px;
  }

  .tabs-krajee.tabs-left.tab-sideways .nav-tabs {
    left: -55px;
    top: 8px;
    margin-right: -75px;
    z-index: 997 !important;
  }

  .tab-content.printable {
    padding-top: 0px;
  }

  .tabs-left .tab-content {
    border-left: none;
  }

  @media(max-width:990px) {
    .crosses-tab {
      margin-top: 0px !important;
    }
  }
</style>
