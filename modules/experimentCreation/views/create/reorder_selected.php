<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for reordering entries
 */
use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;
use kartik\editable\Editable;

?>

<p><?= \Yii::t('app', 'Reorder the following entries by editing the entry number field below.') ?></p>

<?php 

    $columns = [
        [
            'attribute'=>'entlistno',
            'label' => Yii::t('app', 'Entry no.'),
            'format' => 'raw',
            'value' => function($model) use ($entryIds){

                return Editable::widget([
                    'name'=>'entry_list_number_editable', 
                    'id'=> $model['id'],
                    'asPopover' => false,
                    'value' => $model['entlistno'],
                    'formOptions' => ['action' => ['/experimentCreation/create/update-entry-number?allEntries='.implode(',',$entryIds).'&id='.$model['id']]],
                    'size'=>'sm',
                    'options' => ['class'=>'form-control editable-entries', 'placeholder'=>'Not Set']
                ]);

            },
            'enableSorting' => false,
        ],
        [
            'attribute'=>'product_name',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'generation',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'entry_class',
            'enableSorting' => false,
        ],
    ];
?>

    <span id="conflict-error" style="display:none; color:red">
        <?= \Yii::t('app', 'You have conflicting entry list numbers. Please check your inputs below.'); ?>
    </span>
    <br>
    <br>

<?php
    echo $grid = GridView::widget([
        'pjax' => true, // pjax is set to always true for this demo
        'dataProvider' => $dataProvider,
        'id' => 'reorder-selected-modal',
        'columns' => $columns,
    ]);

    $reorderSelectedEntriesUrl = Url::to(['create/reorder-selected-entries', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    
?>


<script>

    function hasDuplicates(array) {
        var valuesSoFar = [];
        for (var i = 0; i < array.length; ++i) {
            var value = array[i];
            if (valuesSoFar.indexOf(value) !== -1) {
                return true;
            }
            valuesSoFar.push(value);
        }
        return false;
    }

    $(document).ready(function(){


        $('#reorder-by-sort').click(function(){
            
            var valArr = [];
            var jsonArr = [];
            
            $('#reorder-selected-modal-container .kv-grid-table tr').each(function(item, value){

                if($(value).attr('data-key')){

                    var elem = $(value).find('button#'+$(value).attr('data-key')+'-targ.kv-editable-value.kv-editable-link');
                    valArr.push(elem.text());
                    var jsonObj  = {};
                    jsonObj.id = $(value).attr('data-key');
                    jsonObj.value = elem.text();
                    jsonArr.push(jsonObj);

                }

            });

            if(hasDuplicates(valArr)){
                $('#conflict-error').show();
            }else{
                $('#conflict-error').hide();

                $.ajax({
                    url: '<?php echo $reorderSelectedEntriesUrl; ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        jsonArray: jsonArr
                    },
                    success: function(response) {
                    },
                    error: function() {
                    }
                });

            }

        });



    });

</script>