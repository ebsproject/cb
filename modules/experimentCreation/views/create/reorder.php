<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for reordering entries
 */
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\sortable\Sortable;

?>
<?php 
    if($reorderType == 'sorting'){ 
?>

<div class="row" style="margin-bottom:0px;">
    <p>
        <i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> 
        <?= \Yii::t('app', '<span>You are about to reorder <b>all</b> entries.</span>') ?>
    </p>
    <p>
        <?= \Yii::t('app', '<b>NOTE: </b>The entry list numbers will be updated. The reordering will be based on the <b>current data sort settings</b>.') ?>
        <?php
            if (!empty($existingExpData)) {
                echo \Yii::t('app', '<p><b>Found existing records under this experiment. Reordering will delete the following records:</b></p>');
                echo Html::ul($existingExpData, ['item' => function($item) {
                    return Html::tag('li', '• '. $item);
                }]);
            } 
        ?>
    </p>
</div>
<div class="progress hidden" style="margin-bottom:0px;"><div class="indeterminate"></div></div>

<?php
    }
?>

<input id="sort-type" type="hidden" value="<?php echo $reorderType?>"/>

<script>
    var experimentId =  '<?= $experimentDbId ?>';
    var threshold =  '<?= $threshold ?>';
    
    var resetGridUrl = '<?=Url::toRoute(['create/specify-entry-list']).'?program='.$program.'&id='.$id.'&processId='.$processId.'&reset=hard_reset'?>'
    var getEtriesCountUrl = '<?= Url::to(['create/get-entries-count', 'id'=>$experimentDbId]); ?>'
    var reorderAllBySortUrl = '<?= Url::to(['create/reorder-all-by-sort', 'id'=>$experimentDbId]); ?>'
    var reorderAllBgProcessUrl = '<?= Url::to(['create/reorder-all-bg-process', 'experimentId'=>$experimentDbId]); ?>'

    $(".confirm-sort-all-btn").click(function(e) {
        $(".progress").removeClass('hidden');

        var  currUrl = window.location.search
        
        var urlSplit = currUrl.split('&');
        var sort = urlSplit[urlSplit.length-1].split('=')[1];

        // check entry count
        $.ajax({
            url: getEtriesCountUrl,
            type: 'post',
            dataType:'json',
            data: {
                experimentId:experimentId
            },
            success: function(response) {
                var message = '';

                $(".progress").removeClass('hidden').addClass('hidden');
                $('#entry-list-reorder-all-modal').modal('hide');

                if(response.entryCount > 0){
                    if(response.entryCount > parseInt(threshold)){
                        // bg process
                        message = ' Update process sent for background processing.';

                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons blue-text left'>info</i> <span class='white-text'>"+message+"</span>";
                            Materialize.toast(notif, 5000);
                        }
                        
                        $.ajax({
                            url: reorderAllBgProcessUrl,
                            type: 'post',
                            data: {
                                sort:sort
                            },
                            success: function(response) {
                                
                            }
                        });
                    }
                    else{
                        $.ajax({
                            url: reorderAllBySortUrl,
                            type: 'post',
                            dataType: 'JSON',
                            data: {
                                sort:sort
                            },
                            success: function(response) {
                                var hasToast = $('body').hasClass('toast');
                                if(!hasToast){
                                    $('.toast').css('display','none');
                                    var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'> Successfully reordered selected entries.</span>";
                                    Materialize.toast(notif, 5000);
                                }
                                e.preventDefault();
                                e.stopImmediatePropagation();

                                location.reload();
                            },
                            error: function() {
                                var hasToast = $('body').hasClass('toast');
                                if(!hasToast){
                                    $('.toast').css('display','none');
                                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Error encountered in updating entry numbers.</span>";
                                    Materialize.toast(notif, 5000);
                                }
                                e.preventDefault();
                                e.stopImmediatePropagation();
                            }
                        });
                    }
                }
                else{
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons red-text left'>error</i> <span class='white-text'> No entries were updated. Please try again.</span>";
                        Materialize.toast(notif, 5000);
                    }
                }

                e.preventDefault();
                e.stopImmediatePropagation();  
            },
            error: function() {
                $('#entry-list-reorder-all-modal').modal('hide');
                
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Unable to reorder entries. Please try again.</span>";
                    Materialize.toast(notif, 5000);
                }
                
                e.preventDefault();
                e.stopImmediatePropagation();
            }
        });
    });

</script>
