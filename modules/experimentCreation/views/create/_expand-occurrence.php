<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders embedded browser for plots
 */
use kartik\grid\GridView;
use app\models\Occurrence;

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute'=>'entryName',
        'label' => Yii::t('app', 'Germplasm Name'),
        'format' => 'raw',
        'encodeLabel'=>false,
    ],
    [
        'attribute'=>'plotNumber',
        'label' => '<span title="Sequential numbering of the plots">'.Yii::t('app', 'Plot No.').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
    ],
    [
        'attribute'=>'entryNumber',
        'label' => '<span title="Sequential numbering of the entries">'.Yii::t('app', 'Entry No.').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
    ],
    'rep',
    'designX',
    'designY'
];  

echo \Yii::t('app', 'This is the browser for the plot list.');
$plotTableName = 'view-plot-table-'.$occurrenceDbId;
$plotDataProvider = \Yii::$container->get('\app\models\Occurrence')->getPlotDataProvider($occurrenceDbId);
echo $grid = GridView::widget([
    'pjax' => true,
    'dataProvider' => $plotDataProvider['dataProvider'],
    'id' => $plotTableName,
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => $columns,
    'toggleData'=>true,
    'pager' => [
        'firstPageLabel' => 'First',
        'lastPageLabel' => 'Last'
    ],
]);
?>