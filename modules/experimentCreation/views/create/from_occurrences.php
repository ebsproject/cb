<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders view occurrences from other experiments for parent list selection
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\Phase;
use app\models\Season;



if($partitionView){

    echo '<span class="new badge">Limited view</span>&nbsp;&nbsp;<em>'.\Yii::t('app', 'We limit the view for this page due to large number of records. Click the View All button to show all records in page.').'</em>';
    echo '<a class="waves-effect waves-light btn pull-right" id="view-all-experiments-id"><i class="material-icons right">visibility</i>View All</a>';
}
$expLabel = ($gridName == 'parent') ? ' Cross Parent Nursery (Phase I)' : '';

if($dataProcessAbbrev == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
    $note = \Yii::t('app', "Below is the list of occurrences from other $expLabel experiments that you can use. Note that only occurrences within the program and have the same <b>site, season, and year</b> will appear in this browser.");
} else $note = \Yii::t('app', "Below is the list of occurrences from other $expLabel experiments that you can use. Note that only occurrences within the program and have the same <b>season and year</b> will appear in this browser.");
echo "<p>$note</p>";
?>

<div class="input-field no-margin search-entry-field-div" style="width:35%">
  <input id="searchTextOccurrences" type="search" placeholder="Type filter" class="autocomplete search-text-add-entry-search-input" autocomplete="off" autofocus="" value="">
  <i class="material-icons search-text-entry-clear-all-search-btn" title="Clear all" style="opacity: 0; margin-right:20px; font-size:1.5rem;margin-top:10px;">clear</i>
  <i class="material-icons search-text-entry-submit-btn" style="margin-top:10px; font-size:1.5rem" title="Search">search</i>
</div>

<?php
$browserId = "$gridName-browser";
echo "<div id='$browserId'>";
echo $grid = GridView::widget([
    'pjax' => true,
    'dataProvider' => $dataProvider,
    'id' => "copy-$gridName",
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-entry-table'],
    'columns' => [
        [
          'class' => 'yii\grid\SerialColumn',
          'header' => false
        ], 
        [
            'class'=>'kartik\grid\ActionColumn',
            'header' => false,
            'template' => '{select}',
            'buttons' => [
                'select' => function ($url, $model, $key) {
                    
                    return Html::a('<i class="material-icons">folder_open</i>',
                        '#',
                        [
                            'class'=>'select-experiment',
                            'title' => Yii::t('app','View entries used in this occurrence'),
                            'data-occurrence-id' => $model['occurrenceDbId'],
                            'data-experiment-name' => $model['experiment'],
                        ]
                    );
                },
            ]
        ],
        [
            'attribute'=>'experiment',
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'experimentCode',
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'occurrenceName',
            'label' => Yii::t('app', 'Occurrence'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'occurrenceCode',
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'entryCount',
            'value' => function ($data) {
                return '<span class="new badge">'.$data['entryCount'].'</span>';
              },
            'format' => 'raw',
            'enableSorting' => false
        ],
        [
            'attribute'=>'experimentYear',
            'label' => Yii::t('app', 'Experiment Year'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'experimentSeasonCode',
            'label' => Yii::t('app', 'Season'),
            'format' => 'raw',
            'enableSorting' => false
        ],
        [
            'attribute'=>'experimentStageCode',
            'label' => Yii::t('app', 'Stage'),
            'format' => 'raw',
            'enableSorting' => false
        ],
    ],
    'toggleData'=>true,
]);

?>
</div>

<div id="planting-instructions-<?= $gridName ?>"></div>
<div id="planting-notification-<?= $gridName ?>" class="hidden"></div>

<?php
    $plantingInsUrl = Url::to(['create/render-planting-instructions','id'=>$id, 'program'=>$program, 'processId'=>$processId, 'gridName' => $gridName]);
    $viewAllExperiments = Url::to(['view/view-all-occurrences', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>

<script>

    $(document).on('click', '#view-all-experiments-id', function(){ 
        var url = '<?php echo $viewAllExperiments; ?>';
        window.location = url;
    });

    $(document).on('click', '#back-btn', function(){
        var browserId =  '#<?= $browserId ?>';
        
        $(browserId).show();
        $('.search-entry-field-div').show(); 
        $('#planting-instructions-<?= $gridName ?>').hide();    
    });

    $(document).ready(function(){
        $('.select-experiment').click(function(){
            var browserId =  '#<?= $browserId ?>';
            var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
		    $('#planting-instructions-<?= $gridName ?>').html(loading); 
            $('.search-entry-field-div').hide(); 

            var occurrenceDbId = $(this).attr('data-occurrence-id');
            var experimentName = $(this).attr('data-experiment-name');
            var experimentDbId = '<?= $id; ?>';
            var url = '<?= $plantingInsUrl; ?>';
            $(browserId).hide();
            $('#planting-instructions-<?= $gridName ?>').show();

            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    occurrenceDbId: occurrenceDbId,
                    experimentDbId: experimentDbId,
                    experimentName: experimentName
                },
                success: function(response) {
                    $('#planting-instructions-<?= $gridName ?>').html(response);
                    
                },
                error: function() {

                }
            });
        });
    });
</script>
