<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders the cross list panel
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\dropdown\DropdownX;
use yii\bootstrap\Button;
use yii\bootstrap\Collapse;
use kartik\tabs\TabsX;
use yii\bootstrap\Nav;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

?>
<div id = "cross-list" class = "row col col-sm-12 col-md-12 col-lg-12" style = "margin-bottom: 100px;padding-right:0px">
<?php
$browserId = 'cross-list-grid';
//Crosses table
$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
		'header'=>'
			<input type="checkbox" class="filled-in" id="select-cross" />
			<label for="select-cross" id="label-for-select-cross"></label>
		',
        'content'=>function($data) {
            $disabled = false;
            if(isset($data['method']) && !empty($data['method'])){
                $disabled = 'readonly';
            }
            return '
                <input class="operational-cross-select filled-in" type="checkbox" id="'.$data['crossDbId'].'" />
                <label for="'.$data['crossDbId'].'" id="label-for-checkbox"></label>

            ';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'crossName',
        'format' => 'raw',
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['crossName']) ? $data['crossName'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossFemaleParent',
        'label' => '<span title="Female Parent"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENT').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['crossFemaleParent']) ? $data['crossFemaleParent'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleParentage',
        'label' => '<span title="Female Parentage"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENTAGE').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['femaleParentage']) ? $data['femaleParentage'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossMaleParent',
        'label' => '<span title="Male Parent">'.Yii::t('app', '<i class="fa fa-mars"></i> MALE PARENT').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['crossMaleParent']) ? $data['crossMaleParent'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleParentage',
        'label' => '<span title="Male Parentage"><i class="fa fa-mars"></i>'.Yii::t('app', ' MALE PARENTAGE').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['maleParentage']) ? $data['maleParentage'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'crossMethod',
        'label' => Yii::t('app','METHOD').' '.'<span class="required">*</span>',
        'format' => 'raw',
        'headerOptions'=>['style'=>'min-width:200px !important'],
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data) use ($methodTags){
            $placeHolder = 'Select a method';
            $val = !empty($data['crossMethod']) ? $data['crossMethod'] : '';
            $disabled = false;
            if($data['isMethodAutofilled'] == true){
                $disabled = 'readonly';
                $placeHolder = $val;
            }

            $value =  Select2::widget([
                'name' => 'input_type',
                'data' => $methodTags,
                'value' => $val,
                'options' => [
                    'placeholder' => $placeHolder,
                    'id' => 'cross-' . $data['crossDbId'],
                    'class' => 'select-cross-method required-field',
                    'data-name' => 'method',
                    'data-label' => 'method',
                    'multiple' => false,
                    'allowClear' => true,
                    'disabled' => $disabled
                ],
            ]);
    
            return '<div class="method_input">'.$value.'</div>';
        }
    ]
];
   
$crossPatternCol = [
    [
        'label' => 'Cross Pattern',
        'format' => 'raw',
        'vAlign'=>'middle',
        'value' => function($data) use ($crossAttributes){
            $value = isset($crossAttributes[$data['crossDbId']]) ? $crossAttributes[$data['crossDbId']] : '';
            return $value;
        }
    ]
];

    if(count($crossAttributes) > 0){
        $columns = array_merge($columns, $crossPatternCol);
    }

    $columns2= [
    [
        'attribute' => 'crossRemarks',
        'format' => 'raw',
        'vAlign'=>'middle',
        'value' => function($data){
            $value = '';
            $value = Html::input('text', 'remarks-input', $data['crossRemarks'], ['id'=>'remarksInput-'.$data['crossDbId'],'class'=>'remarks-input-validate','data-name'=>'remarks']);
            return $value;
        }
    ],
    [
        'attribute' => 'femaleParentEntryNumber',
        'label' => '<span title="Female Source Entry"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENT ENTRY NO.').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            return !empty($data['femaleParentEntryNumber']) ? $data['femaleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleParentEntryNumber',
        'label' => '<span title="Male Source Entry"><i class="fa fa-mars"></i>'.Yii::t('app', ' MALE PARENT ENTRY NO.').'</span>',
        'encodeLabel' => false,
        'format' => 'raw',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'vAlign'=>'middle',
        'value' => function($data){
            if($data['crossMethod'] == 'selfing'){
                return !empty($data['femaleParentEntryNumber']) ? $data['femaleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
            } else return !empty($data['maleParentEntryNumber']) ? $data['maleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'femaleParentSeedSource',
        'label' => '<span title="Female Seed Source"><i class="fa fa-venus"></i>'.Yii::t('app', 'FEMALE SEED SOURCE').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'headerOptions'=>['class'=>'red lighten-4'],
        'contentOptions'=>['class'=>'red lighten-5'],
        'value' => function($data){
            return !empty($data['femaleParentSeedSource']) ? $data['femaleParentSeedSource'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute' => 'maleParentSeedSource',
        'label' => '<span title="Male Seed Source"><i class="fa fa-mars"></i>'.Yii::t('app', 'MALE SEED SOURCE').'</span>',
        'format' => 'raw',
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'headerOptions'=>['class'=>'blue lighten-4'],
        'contentOptions'=>['class'=>'blue lighten-5'],
        'value' => function($data){
            return !empty($data['maleParentSeedSource']) ? $data['maleParentSeedSource'] : '<span class="not-set">(not set)</span>';
        }
    ]
];
$columns = array_merge($columns, $columns2);

// //actions dropdown
$actionsDropdown = "<a id='remove-crosses' class='dropdown-button btn dropdown-button-pjax' href='#' data-activates='grid-dropdown-actions' data-beloworigin='true' data-constrainwidth='false'><i class='glyphicon glyphicon-cog'></i> <span class='caret'></span></a>
    <ul id='grid-dropdown-actions' class='dropdown-content' style='width:160px !important' data-beloworigin='false'>
        <li><a href='#' class='dropdown-button dropdown-button-pjax' data-activates='remove-dropdown-actions' data-hover='hover' data-alignment'='left'>Remove Crosses</a></li>
    </ul>

    <ul id='remove-dropdown-actions' class='dropdown-content' style='width: 160px !important'>
       <li><a href='#!' id='selected' class='remove-cross'>Remove Selected</a></li>
       <li><a href='#!' id='all' class='remove-cross'>Remove All</a></li>
    </ul>
";

$itemsColumn = [
    [
        'label' => Yii::t('app', 'Remove Crosses'),
        'options' => ['class'=>'pull-left'],
        'items' => [
            ['label' => Yii::t('app','Remove Selected'), 'options'=>['class'=>'pull-left','id'=>'selected', 'class'=>'remove-cross'], 'url'=>'#'],
            ['label' => Yii::t('app','Remove All'), 'options'=>['class'=>'pull-left','id'=>'all', 'class'=>'remove-cross'], 'url'=>'#']
        ]
    ]
];

DynaGrid::begin([
    'options' => ['id'=>$browserId],
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        // 'filterModel'=>$searchModel,
        'showPageSummary' => false,
        'id'=>$browserId,
        'pjax' => true,
        'pjaxSettings'=>[
            'neverTimeout' => true,
            'options'=>['id'=>$browserId,'enablePushState'=>false],
            'beforeGrid' => '',
            'afterGrid' => '',
        ],
        'responsiveWrap' => false,
        'panel' => [
            'heading'=> false,
            'before' =>'<b><i class="fa fa-list"></i> CROSS LIST</b>{summary}<br/><div id = "selected-items" class = "pull-right"><b><span id = "selected-count">No</span></b> selected items.</div>',
            'after' => false
        ],
       'toolbar' => [
            [
				'content'=> Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
				["create/manage-crosslist?id=$id&program=$program&processId=$processId"], ['id'=>'reset-cross-lists-grid', 'class' => 'btn btn-default', 'title'=>\Yii::t('app','Reset grid'),'data-pjax'=>true]).
                Html::a(
                    '<i class="material-icons widget-icons">download</i>',
                    '#!',
                    [
                        'class' => 'btn pull-right',
                        'id' => 'download-cross-list',
                        'title'=>\Yii::t('app',' Download')
                    ]
                ),
            ],
            [
                'content' => Html::beginTag('div',['class'=>'text-left dropdown']).
                    Html::button('<i class="glyphicon glyphicon-edit"></i> ',['type'=>'button', 'title'=>\Yii::t('app','Bulk Update'), 'class'=>'btn','id'=>'update-crosses-btn','data-toggle'=>'modal', 'data-target'=>'#manage-cross-modal']).
                    "$actionsDropdown".'{dynagrid}'.
                    Html::endTag('div')
            ],

       ],
       'floatHeader' =>true,
       'floatOverflowContainer'=> true,
       'resizableColumns'=>true,
       'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],

]);

DynaGrid::end();

//New Modal for cross list single/bulk update, actions button - select variables
Modal::begin([
    'id'=> 'manage-cross-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i>  '.\Yii::t('app','Bulk Update').'</h4>',
    'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="bulk-update-selected" class="btn btn-primary green waves-effect waves-light confirm-update-btn">Selected</a>&nbsp;'.
        '<a id="bulk-update-all" class="btn btn-primary teal waves-effect waves-light confirm-update-btn">All</a>',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
    'closeButton' => ['class' => 'hidden'],
]);
echo '<p id="instructions"></p>';
echo '<div id="config-var" style="margin-left:30px;"></div>';

Modal::end();

//Modal for cross management
Modal::begin([
    'id' => 'cross-list-notif-modal',
    'footer' =>
        Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
    'size' =>'small-lg',
    'options' => ['data-backdrop'=>'static'],
]);
echo '<div id="cross-modal-notif"></div>';

Modal::end();

//Modal for delete cross management
Modal::begin([
    'id' => 'cross-list-confirm-modal',
    'header' => '<h4>Delete Crosses</h4>',
    'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-delete-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="all-delete" class="btn btn-primary waves-effect waves-light confirm-delete-btn">Confirm</a>'.
        '<a id="selected-delete" class="btn btn-primary waves-effect waves-light confirm-delete-btn">Confirm</a>',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
    'closeButton' => ['class' => 'hidden'],
]);
echo '<div id="cross-modal-confirm"></div>';
Modal::end();


// Modal for validation
Modal::begin([
    'id' => 'required-field-modal',
    'header' => '<h4><i class="fa fa-bell"></i> '.\Yii::t('app','Notification').'</h4>',
    'footer' => Html::a(\Yii::t('app','OK'),['#'],['id'=>'cancel-save-btn','class'=>'btn btn-primary waves-effect waves-light','data-dismiss'=>'modal']),
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static']
]);
echo '<div class="row">
    <p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'Please correct the errors below before proceeding.').'</span></p>
    </div><div class="row" id="required-field-notif"></div>';
Modal::end();

//Modal for unused parents
Modal::begin([
    'id' => 'unused-parents-modal',
    'header' => "<h4><i class='material-icons orange-text left'>warning</i> Unused Parents</h4>",					
    'footer' =>	Html::a(\Yii::t('app',' Back '),'#',['id' => 'back-nxt-btn','data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close black-text grey lighten-2']).'&nbsp;'.Html::a(\Yii::t('app','Proceed'),'#',['id' => 'proceed-nxt-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static']
    ]);
    echo \Yii::t('app',' Some of the parents are not used in crossing. You may filter or sort the #♀ and #♂ columns to check. If you wish to proceed, you will be redirected to the next step.');
Modal::end();

?>
<br/>
<?php
    Yii::$app->view->registerJs("
        var genericConfUpdateUrl = '".Url::toRoute(['/experimentCreation/create/update-config-variable','id'=>$id, 'program'=>$program])."',
        experimentId = '".$id."'
        ;", \yii\web\View::POS_HEAD);

    $this->registerJsFile("@web/js/experiment-creation/dynamic_fields.js", [
        'depends' => ['app\assets\AppAsset'],
        'position'=>\yii\web\View::POS_END
    ]);

    $this->registerJsFile("@web/js/generic-form-validation.js", [
        'depends' => ['app\assets\AppAsset'],
        'position'=>\yii\web\View::POS_END
    ]);

$totalProviderCount = $dataProvider->getTotalCount();

$exportEntityUrl = Url::to(['create/export-entity']);
$prepareDataForCsvDownloadUrl = Url::to(['create/prepare-data-for-csv-download', 'id'=>$id, 'program'=>$program, 'entity'=>'cross']);
$getCrossesBatchCountUrl = Url::to(["create/get-crosses-batch-count","id"=>$id]);
$removeCrossUrl = Url::to(["create/remove-crosses","id"=>$id, "program"=>$program]);
$bulkUpdateUrl = Url::to(["create/cross-bulk-update","id"=>$id, "program"=>$program]);
$saveCrossUrl = Url::to(["create/save-crosses","id"=>$id]);
$enableNextUrl = Url::to(["create/enable-next","id"=>$id]);
$genericRenderConfigUrl = Url::to(['create/render-config-form', 'id'=>$id, 'program'=>$program]);
$updateCrossUrl = Url::to(['create/update-cross-variable','id'=>$id, 'program'=>$program]);
$checkParentsUrl = Url::to(['create/check-parents','id'=>$id, 'program'=>$program]);
$checkStatusUrl  = Url::to(['create/check-status','id'=>$id, 'action'=>'manage-crosses']);
$getAllCrossListCrossesUrl = Url::to(['create/get-all-cross-list-crosses','id'=>$id]);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);
?>

<?php
$reqFieldJson = json_encode($requiredFields);
Yii::$app->view->registerJs("
    checkStatusUrl = '".$checkStatusUrl."',
    experimentId = '".$id."',
    requiredFieldJson = '".$reqFieldJson."',
    checkStatValue = 'cross list created'
    ;",
    \yii\web\View::POS_HEAD);

$this->registerJsFile("@web/js/check-required-field-status.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
  var variable = '';
  var fieldIdSelected = '';
  var exportRecordsThresholdValue = $exportRecordsThresholdValue;
  var deleteCrossThresholdValue = $deleteCrossThresholdValue;

$("document").ready(function() {
    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
        checkStatus();
        refresh();
    });
    refresh();

    function refresh(){
        $('.dropdown-button-pjax').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false,
            hover: false,
            gutter: 0,
            alignment: 'right',
            stopPropagation: false
        });

        //Update selected items and count
        var selected = localStorage.getItem('ec_'+'$browserId'+'_cross_ids_'+'$id');
        selected = getSelectedCosses(selected);
        selected = (selected == null) ? {} : selected;

        $("input:checkbox.operational-cross-select").each(function(){
            var entryDbId = $(this).parent("td").parent("tr").attr('data-key');
            if(selected.hasOwnProperty(entryDbId)) {
                $(this).attr('checked','checked');
                $(this).attr('checked','checked');
                $(this).prop('checked',true);
                $(this).parent("td").parent("tr").addClass("grey lighten-4")
            }
        });

        var count = getSelectedCount(selected); // update count
        setSelectedCounter(count);
    }
});

// yes, proceed to next tab
$(document).on('click','#proceed-nxt-btn',function(e){
    window.location = $('#next-btn').attr('data-url');
});

// reset selection
$(document).on('click','#reset-cross-lists-grid',function(e){
    localStorage.removeItem('ec_'+'$browserId'+'_cross_ids_'+'$id');
});

// select all crosses
$(document).on('click','#select-cross',function(e){
    if($(this).prop("checked") === true)  {
        $(this).attr('checked','checked');
        $('.operational-cross-select').attr('checked','checked');
        $('.operational-cross-select').prop('checked',true);
        $(".operational-cross-select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
    }else{
        $(this).removeAttr('checked');
        $('.operational-cross-select').prop('checked',false);
        $('.operational-cross-select').removeAttr('checked');
        $("input:checkbox.operational-cross-select").parent("td").parent("tr").removeClass("grey lighten-4");
    }

    var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
    var unset = $(this).prop('checked') ? false : true;

    if(unset) { // clear all
        localStorage.removeItem('ec_'+'$browserId'+'_cross_ids_'+'$id');
        setSelectedCounter(0);
    } else {  // select all
        // retrieve all crosses (ajax call)
        $.ajax({
            url: '$getAllCrossListCrossesUrl',
            type: 'post',
            data: {
                sessionName: 'ec-entry-list'
            },
            cache: false,
            success: function(response) {
                response = JSON.parse(response)
                localStorage.setItem('ec_'+'$browserId'+'_cross_ids_'+'$id', JSON.stringify(response));  // put crosses to localStorage
                var count = getSelectedCount(response); // update count
                setSelectedCounter(count);
            },
            error: function(e) {
                console.log(e);
            }
        });
    }
});

// click row in browser
$(document).on('click', '#$browserId tbody tr', function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var thisRow = $(this).find('input:checkbox')[0];

    $("#"+thisRow.id).trigger("click");
    if(thisRow.checked){
        $(this).addClass("grey lighten-4");
    }else{
        $(this).removeClass("grey lighten-4");
    }
});

// click checkbox (select cross)
$(document).on('click','.operational-cross-select',function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
    var unset;
    var entryDbId = $(this).prop('id');

    if($(this).prop('checked')){
        unset = false;
    }else{
        unset = true;
        $('#select-cross').prop('checked',false);
        $('#select-cross').removeAttr('checked');
    }

    var selected = localStorage.getItem('ec_'+'$browserId'+'_cross_ids_'+'$id');
    selected = getSelectedCosses(selected);
    selected = (selected == null) ? {} : selected;
    if(unset) { // remove selected
        delete selected[entryDbId];
        selected = (Object.keys(selected).length === 0) ? null : selected;
    } else {  // add selected
        selected[entryDbId] = entryDbId;
    }
    localStorage.setItem('ec_'+'$browserId'+'_cross_ids_'+'$id', JSON.stringify(selected));  // save selected to localStorage

    var count = getSelectedCount(selected); // update count
    setSelectedCounter(count);
});

$(document).on('change','.select-cross-method', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();

    var id = this.id;
    var crossnoArr = (this.id).split('-');
    var crossno = crossnoArr[1];

    var experimentId = '<?php echo $id;?>';
    var input = $('#'+id).val();
    var column = 'method';
    //Save the changes in the database
    $.ajax({
        url: '$saveCrossUrl',
        type: 'post',
        datatType: 'json',
        cache: false,
        data: {crossNo: crossno,'mode':'single',input:input,column: column},
        success: function(response){
            checkStatus();
        }
    });
});

$(document).on('keypress keyup blur paste', ".remarks-input-validate", function(event) {
    var id = this.id;
    var crossnoArr = (this.id).split('-');
    var crossno = crossnoArr[1];
    var column = $(this).attr('data-name');
    var input = $('#'+id).val();

    //Save the changes in the database
    $.ajax({
        url: '$saveCrossUrl',
        type: 'post',
        datatType: 'json',
        cache: false,
        data: {crossNo: crossno,'mode':'single', input: input,column: column},
        success: function(response){

        }
    });
});

$(document).on('click','#update-crosses-btn', function(e){
    var ids = [];
    var selected = localStorage.getItem('ec_'+'$browserId'+'_cross_ids_'+'$id');
    selected = getSelectedCosses(selected);
    selected = (selected == null) ? {} : selected;
    for(var crossId in selected) {
        ids.push(crossId)
    }

    $('#bulk-update-selected').removeClass('disabled');
    $('#bulk-update-all').removeClass('disabled');
    $('#cancel-save-btn').show();

    $('#config-var').removeAttr('tabindex');
    $('#manage-cross-modal').removeAttr('tabindex');
    $.ajax({
        url: '$bulkUpdateUrl',
        type: 'post',
        dataType: 'json',
        cache: false,
        data: {ids: ids},
        success: function(response){
            $('#instructions').html("Bulk update the following values for the cross list records. Note: Autofilled cross methods are not editable.");
            $('#config-var').html(response);
            $('.loading-req-vars').remove();
            $('#req-vars').css('visibility','visible');
            $('#req-vars').css('width','');
            $('#req-vars').css('height','');
        }
    });
});

$(document).on('click','.remove-cross', function(e){
    var ids = [];
    var selected = localStorage.getItem('ec_'+'$browserId'+'_cross_ids_'+'$id');
    selected = getSelectedCosses(selected);
    selected = (selected == null) ? {} : selected;
    for(var crossId in selected) {
        ids.push(crossId)
    }

    $('#cancel-delete-btn').show();
    if(this.id == 'all'){
        $('#cross-list-confirm-modal').modal();
        $('#cross-modal-confirm').html('<div class="row"><p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>You are about to remove <b>all</b> draft crosses.</span></p><p><b>NOTE: </b>removing all crosses will delete all cross list related information of this experiment if any.</p></div>');
        $('#selected-delete').addClass('hidden');
        $('#all-delete').removeClass('hidden');
        $('#all-delete').removeClass('disabled');
    } else {
        if(ids.length > 0){
            $('#cross-list-confirm-modal').modal();
            $('#cross-modal-confirm').html('<div class="row"><p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>You are about to remove <b>'+ids.length+'</b> selected draft crosses along with the <b>batch of crosses if it is created via cross pattern </b>.</span></p><p><b>NOTE: </b>removing selected crosses will delete the cross list related information of this experiment if any.</p></div>');
            $('#all-delete').addClass('hidden');
            $('#selected-delete').removeClass('hidden');
            $('#selected-delete').removeClass('disabled');
        } else {
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>Select at least one (1) cross.</span>";
                Materialize.toast(notif, 5000);
            }
        }
    }
});

$(document).on('click','.confirm-delete-btn', function(e){
    var type = 'all';
    var ids = [];
    if(this.id == "selected-delete"){
        var selected = localStorage.getItem('ec_'+'$browserId'+'_cross_ids_'+'$id');
        selected = getSelectedCosses(selected);
        selected = (selected == null) ? {} : selected;
        for(var crossId in selected) {
            ids.push(crossId)
        }
        type = 'selected';
    }

    var idsCount = ids.length;

    // If cross pattern is used, get total batch count of selected ids
    $.ajax({
        url: '$getCrossesBatchCountUrl',
        type: 'post',
        async: false,
        dataType: 'json',
        data: {
            idsArr : JSON.stringify(ids)
        },
        success: function(response) {
            if(response > 0) idsCount = response;
        },
        error: function() {
        }
    });

    //show loading of deletion
    $('#all-delete, #selected-delete').addClass('disabled');
    $('#cancel-delete-btn').hide();
    //Fires the background process validation if the conditions are met
    if (idsCount >= deleteCrossThresholdValue) {
        $('#cross-modal-confirm').html('<div class="alert ">'+
                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                    '<i class="fa fa-info-circle"></i>'+
                        ' A background job is being created.' +
                '</div></div>');
    }else{
        $('#cross-modal-confirm').html("");
        $('#cross-modal-confirm').html(' <div class="progress"><div class="indeterminate"></div></div>');
    }
 
    $.ajax({
        url: '$removeCrossUrl',
        type: 'post',
        dataType: 'json',
        data: {
            idsArr : JSON.stringify(ids),
            mode : type
        },
        success: function(response) {
            if(type == 'all'){
                $('#next-btn').attr('disabled');
            }else{
                $('#next-btn').removeAttr('disabled');
            }
            $('#cross-list-confirm-modal').modal('hide');
            localStorage.removeItem('ec_'+'$browserId'+'_cross_ids_'+'$id');
            setSelectedCounter(0);
            $.pjax.reload({
                container: '#$browserId-pjax',
                replace:false
            });

        },
        error: function() {
        }
    });
});

$(document).on('click','#next-btn', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var response = checkFields();

    requiredFieldExists = response.requiredFieldExists;
    response = response.respo;

    if(response['success']){
        var nextUrl = $(this).attr('data-url');

        if(requiredFieldExists == false){
            $('#required-field-notif').html('');
            $('#required-field-modal').modal();
            $("#required-field-notif").append('<p><b>1. No cross list results.</b></p>');
            $('#required-field-modal').modal('open');
        }
        else{
            $.ajax({
                url: '$checkParentsUrl',
                success: function(unusedParentCount){
                    if(unusedParentCount > 0){
                        $('#unused-parents-modal').modal();
                    }else{
                        window.location = nextUrl;
                    }
                }
            });
        }
    } else {
        $('#required-field-notif').html('');
        $('#required-field-modal').modal();

        if(response['required'] != null && response['required'] != undefined && response['required'] != ''){
            $("#required-field-notif").append('<p> <b>Required Fields</b> <br>'+response['required']+'</p>');
        }
        if(response['datatype'] != null && response['datatype'] != undefined && response['datatype'] != ''){
            $("#required-field-notif").append('<p> <b>Invalid Input</b> <br>'+response['datatype']+'</p>');
        }
        $('#required-field-modal').modal('open');
    }
});

function checkFields(){
    var response = [];
    var respoArr = [];
    response['success'] = true;
    response['required'] = null;
    response['datatype'] = null;
    var tempArr = [];
    var arrLabel = [];
    var count = 0;
    var requiredFieldExists = false;

    $(".required-field").each(function(){

        var test1 = $(this).val();

        requiredFieldExists = true;

        if(($(this).val() == '' || $(this).val() == null || $(this).val() == "") && !$(this).text().includes('selfing')){

            response['success'] = false;
            var idCol = this.id;
            var label =  $('#'+idCol).attr("data-label");
            var count = tempArr.length;

            if(arrLabel.indexOf(label) > -1){

            }else{
                tempArr.push((count+1)+'. '+label);
                arrLabel.push(label);
            }
        }
    });

    response['required'] = tempArr.join("<br>");
    tempArr = [];
    count = 0;
    arrLabel = [];

    $(".invalid").each(function(){
        response['success'] = false;
        var idCol = this.id;
        var label =  $('#'+idCol).attr("data-label");
        count = tempArr.length;
        if(arrLabel.indexOf(label) > -1){

        }else{
            tempArr.push((count+1)+'. '+label);
            arrLabel.push(label);
        }
    });

    response['datatype'] = tempArr.join("<br>");
    respoArr['respo'] = response;
    respoArr['requiredFieldExists'] = requiredFieldExists;

    return respoArr;
}

//set selected counter
function setSelectedCounter(response){
    count = parseInt(response).toLocaleString();
    if(response == 0) {
        count = 'No';
    }
    $('#selected-count').html(count);
}

// count items inside a json object which contains selected items in a browser
function getSelectedCount(jsonObj) {
    return (jsonObj == null) ? 0 : Object.keys(jsonObj).length;
}
// parse json string from localStorage and return a json object
function getSelectedCosses(jsonStringFromLocalStorage) {
    try {
        var crossesObj = JSON.parse(jsonStringFromLocalStorage);
        if (crossesObj && typeof crossesObj === "object") {
            return crossesObj;
        }
    }
    catch (e) { }
    return null;
}

$('.select2-container--disabled').find('.select2-selection__placeholder').css({ color: '#555555' });

// Download cross list
$('#download-cross-list').off('click').on('click', function (e) {
    e.preventDefault()

    if ($totalProviderCount >= exportRecordsThresholdValue) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '$prepareDataForCsvDownloadUrl',
            data: {},
            success: async function (ajaxResponse) {},
            error: function (xhr, error, status) {}
        })
    } else {
        let exportEntityUrl = '$exportEntityUrl'
        let expEntUrl = window.location.origin + exportEntityUrl +'?id=' + '$id'+'&entity=cross'

        message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'

        $('.toast').css('display','none')

        Materialize.toast(message, 5000)
        window.location.replace(expEntUrl)

        $('#system-loading-indicator').html('')
    }
})

JS
);
?>
</div>
<style>
.method_input{
    width: 90%;
    margin-bottom: -10px;
    align: center
}
.table > tbody > tr > td{
    vertical-align: initial;
}
#select-cross{
    margin: 0px;
    padding: 0px;
}
#label-for-select-cross{
    margin:0px;
    padding:0px 20px 0px 0px;
}
#label-for-checkbox{
    margin:0px;
    padding:0px 20px 0px 0px;
}
body{
    overflow-x: hidden;
}
.dropdown-content {
    overflow-y: visible;
}
.dropdown-content .dropdown-content {
    margin-left: -100%;
}
.grid-view td {
    white-space: nowrap;
}
.summary{
    padding-top : 0 !important;
    padding-right: 5px !important;
}
.tabs-krajee.tabs-left .nav-tabs, .tabs-krajee.tabs-right .nav-tabs {
    width: 151px;
    z-index: 0;
}
.tabs-krajee.tabs-left .tab-content {
    margin-left: 150px;
}
.modal-body{
    overflow-y: hidden;
}
#cross-list-grid-grid-modal .modal-body {
    overflow-y: auto;
}
.generic-instructions{
   display: none;
}
#req-vars{
    visibility: visible;
    width: '';
    height: '';
}
.input-field input[type=search]:focus{
    background-color: inherit;
}
.table > tbody > tr > td{
    vertical-align: middle;
}
.tabs-krajee.tabs-left.tab-sideways .nav-tabs{
    z-index: 0 !important;
}
</style>