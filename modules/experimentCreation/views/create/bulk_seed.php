<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /**
  * Renders filter for seed sources
  */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;

?>

<p
><?= \Yii::t('app', 'Filter the appropriate seed sources for your enrties using the filter items below.') ?></p>

<div>

<?php
    echo '<label class="control-label">Source Year</label>';
    echo Select2::widget([
        'id' => 'source-year',
        'name' => 'source_year',
        'value' => [], // initial value
        'data' => [
            'owner' => "Owner",
            'producer' => "Producer",
            'consumer' => "Consumer"
        ],
        'maintainOrder' => true,
        'toggleAllSettings' => [
            'selectLabel' => '<i class="glyphicon glyphicon-ok-circle"></i>'.\Yii::t('app','Select All'),
            'unselectLabel' => '<i class="glyphicon glyphicon-remove-circle"></i>'.\Yii::t('app','Unselect All'),
            'selectOptions' => ['class' => 'text-success'],
            'unselectOptions' => ['class' => 'text-danger'],
        ],
        'options' => ['placeholder' => \Yii::t('app','Select year'), 'multiple' => true, 'class' => 'select2-Drop',],
        'pluginOptions' => [
            'tags' => true,
            'maximumInputLength' => 10
        ],
        'pluginEvents' => [
            'select2:focus' => "function(e) { 

                populateYear();
            
            }",
        ],
    ]);
?>
</div>