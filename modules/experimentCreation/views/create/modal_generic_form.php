<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders generic form inside a modal
 */
use yii\helpers\Url;

use app\components\GenericFormWidget;


echo GenericFormWidget::widget([
    'options' => $widgetOptions
]);

$yearMinValue = isset($yearMinValue) ? $yearMinValue : null;
$yearMaxValue = isset($yearMaxValue) ? $yearMaxValue : null;

$this->registerJs(<<<JS

    var dataEndpoint = "$dataEndpoint";
    
    $('.kv-plugin-loading').remove();
    $('[class*=input-validate]').next().css( 'width', $('#req-vars').next().width());
    if(dataEndpoint != ''){
        $('[class*=input-validate]').attr("data-endpoint" , dataEndpoint);
    }

    $(".numeric-input-validate").on("keypress keyup blur paste",function (event) { 
    var max = $(this).attr('max');
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});

//For decimal inputs validation
$(".float-input-validate").on("keypress keyup blur paste",function (event) {    
    var key = window.event ? event.keyCode : event.which;
     
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57) {
        return false;
    } else {
        return true;
    }
});

//Validate the input fields of type number
$("input[type='number']" ).on( "change keydown paste blur cut input", function(){
    var min, max, value = null;
    var key = ($(this).attr('id')).split("-")[1];
    var variable = ($(this).attr('id')).split("-")[0];
    var yearMinValue = parseInt('$yearMinValue');
    var yearMaxValue = parseInt('$yearMaxValue');

    var divFieldId = 'field-'+$(this).attr('id');

    if(parseInt(this.value) || parseFloat(this.value)){
        if(this.hasAttribute('step')){
            value = parseFloat(this.value);
        } else {
            value = parseInt(this.value);
        }
        if ((variable.includes('year') || variable.includes('YEAR')) && value.toString().length != 4) {
            $('.'+divFieldId).addClass('has-error');
            $('.'+divFieldId).find(".help-block").html('Invalid input for field Year.');
        }else{
            $('.'+divFieldId).removeClass('has-error');
            $('.'+divFieldId).find(".help-block").html('');

            if(this.hasAttribute('min') && this.hasAttribute('max')){
                min = $(this).attr('min');
                max = $(this).attr('max');

                if(variable.includes('year') || variable.includes('YEAR')){
                    if(yearMinValue == '' || yearMinValue == null){
                        min = 1920;
                    }else{
                        min = yearMinValue;
                    }
            
                    if(yearMaxValue == '' || yearMaxValue == null || isNaN(yearMaxValue) == true){
                        max = '';
                    }else{
                        max = yearMaxValue;
                    }
                }
        
                if((value < min || value > max) && (max != null && max != '' && max != 0)){
                    $('.'+divFieldId).addClass('has-error');
                    $('.'+divFieldId).find(".help-block").html('Input should be greater than '+min+' and less than '+max);
                }else if((value < min || value > max) && (max == null || max == '' || max == 0)){
                    //Do nothing
                }else{
                    $('.'+divFieldId).removeClass('has-error');
                    $('.'+divFieldId).find(".help-block").html('');
                }
            }
            else if(this.hasAttribute('min')){
                min = $(this).attr('min');
                if(variable.includes('year') || variable.includes('YEAR')){
                    if(yearMinValue == '' || yearMinValue == null){
                        min = 1920;
                    }else{
                        min = yearMinValue;
                    }
                }
                if(value < min){
                    $('.'+divFieldId).addClass('has-error');
                    $('.'+divFieldId).find(".help-block").html('Input should be greater than '+min);
                } else {
                    $('.'+divFieldId).removeClass('has-error');
                    $('.'+divFieldId).find(".help-block").html('');
                }
            
            } else if(this.hasAttribute('max')){
                max = $(this).attr('max');
                if(variable.includes('year') || variable.includes('YEAR')){
                    if(yearMaxValue == '' || yearMaxValue == null){
                        max = '';
                    }else{
                        max = yearMaxValue;
                    }
                }
                if(value > max){
                    $('.'+divFieldId).addClass('has-error');
                    $('.'+divFieldId).find(".help-block").html('Input should be less than '+max);
                }else {
                    $('.'+divFieldId).removeClass('has-error');
                    $('.'+divFieldId).find(".help-block").html('');
                }
            } else{
                $('.'+divFieldId).removeClass('has-error');
                $('.'+divFieldId).find(".help-block").html('');
            }
        }
    } else if(this.value != ''){
        // not a number
        $('.'+divFieldId).addClass('has-error');
        $('.'+divFieldId).find(".help-block").html('Input should be numeric');
    }
});

function checkField(){
    var response = [];
    response['success'] = true;
    response['value'] = null;
    response['endpoint'] = null;
    response['inputType'] = null;
    var tempArr = [];
    var count = 0;
    var reqVar = $('#req-vars').val();
    
    if(reqVar !== ''){
        $('[id*='+ reqVar +']').each(function( key, value){
            var fieldIdName = $(this).attr('id');
            
            if(key == 0 && fieldIdName.indexOf('modal') === -1){
                var divFieldId = 'field-'+$(this).attr('id');
                response['value'] = $(this).val();
                response['endpoint'] = $(this).attr('data-endpoint');
                response['inputType'] = $(this).attr('data-select2-id') !== undefined ? 'dropdown' : 'text';
                
                if($(this).val().length == 0){
                    response['success'] = false;
                    var strArr = (this.id).split('-');
                    var label =  $('label[for=' + strArr[0] + ']').attr("data-label");
                    var count = tempArr.length;
                    tempArr.push((count+1)+'. '+label);
                    $('.'+divFieldId).addClass('has-error');
                    $('.'+divFieldId).find(".help-block").html('Please enter a value for '+label+'.');
                }else{
                    $('.'+divFieldId).removeClass('has-error');
                    $('.'+divFieldId).find(".help-block").html('');
                }
            }else{
                
                if(fieldIdName.includes('modal') && fieldIdName.indexOf('container') === -1){
                    var divFieldId = 'field-'+$(this).attr('id');
                    response['value'] = $(this).val();
                    response['endpoint'] = $(this).attr('data-endpoint');
                    response['inputType'] = $(this).attr('data-select2-id') !== undefined ? 'dropdown' : 'text';
                    response['targetColumn'] = $(this).attr('target-column');
                    response['valueText'] = $(this).find(":selected").text();
              
                    if($(this).val().length == 0){
                        response['success'] = false;
                        var strArr = (this.id).split('-');
                        var label =  $('label[for=' + strArr[1] + ']').attr("data-label");
                        var count = tempArr.length;
                        tempArr.push((count+1)+'. '+label);
                        $('.'+divFieldId).addClass('has-error');
                        $('.'+divFieldId).find(".help-block").html('Please enter a value for '+label+'.');
                    }else{
                        response['success'] = true;
                        $('.'+divFieldId).removeClass('has-error');
                        $('.'+divFieldId).find(".help-block").html('');
                    }
                }
            }
        });
    }

  
    return response;
  }

JS
);
?>
