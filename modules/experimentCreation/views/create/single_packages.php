<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders browser for package
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\Phase;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;

?>

<p><?= \Yii::t('app', 'Below are packages you can use. Click the search icon to filter the list.') ?></p>

<div class="input-field no-margin search-entry-field-div" style="width:35%">
  <input id="searchTextSeeds" type="search" placeholder="Type filter" class="autocomplete search-text-entry-search-input" autocomplete="off" autofocus="" value="">
  <i class="material-icons search-text-entry-clear-all-search-btn" title="Clear all" style="opacity: 0; margin-right:20px; font-size:1.5rem;margin-top:10px;">clear</i>
  <i class="material-icons search-text-entry-submit-btn" style="margin-top:10px; font-size:1.5rem" title="Search">search</i>
</div>

<?php 
        // all the columns that can be viewed
        $columns = [
            [
              'header' => false,
              'value' => function ($data) use ($currentPackageDbId, $entryId) {
                 
                  $checked = ($data['packageDbId'] ==  $currentPackageDbId)? 'checked' : '';
                  
                  return '<p>
                      <input id="'.$data['packageDbId'].'-'.$entryId.'" value="'.$data['seedDbId'].'-'.$data['packageDbId'].'" class="with-gap action-radio-package radio-'.$entryId.'" name="'.'PackageSelect['.$entryId.']'.'" type="radio" '.$checked.' />
                      <label for="'.$data['packageDbId'].'-'.$entryId.'"></label>
                  </p>';

                },
              'format' => 'raw',
              'enableSorting' => false,
            ],
            [
                'label' => Yii::t('app', 'Entry No.'),
                'value' => function ($data) use ($entry){
                    return $entry[0]['entryNumber'];
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'programName',
                'label' => Yii::t('app', 'Program'),
                'format' => 'raw',
                'enableSorting' => false,
            ],  
            [
                'attribute'=>'germplasmName',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'seedName',
                'label' => Yii::t('app', 'Seed name'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'packageLabel',
                'label' => Yii::t('app', 'Package label'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'packageQuantity',
                'label' => Yii::t('app', 'QTY'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'packageUnit',
                'label' => Yii::t('app', 'UNIT'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentYear',
                'label' => Yii::t('app', 'Year'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentName',
                'label' => Yii::t('app', 'Experiment'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentType',
                'label' => Yii::t('app', 'Experiment Type'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentSeason',
                'label' => Yii::t('app', 'Season'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentStageCode',
                'label' => Yii::t('app', 'Stage'),
                'format' => 'raw',
                'enableSorting' => false,
            ]
        ];  
        
		echo $grid = GridView::widget([
        'pjax' => true, // pjax is set to always true for this demo
        'dataProvider' => $data,
        'id' => 'select-single-package',
        'columns' => $columns,
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-seed-lot-table'],
        'striped'=>false
    ]);
 ?>
<a class="grid-select-expts btn-floating btn-small waves-effect waves-light pull-right hidden red"><i class="material-icons">add</i></a>


<?php
    $url = Url::to(['create/save-single-package','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>
<script>
$(document).ready(function(){

    $('#save-package-btn').click(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var selectedId = $("input:radio.action-radio-package:checked").val();
        if(selectedId != null){
            var selectedIdArr = selectedId.split('-');
            var selectedPackageId = selectedIdArr[1];
            var selectedSeedId = selectedIdArr[0];
            var entryIdStr = $("input:radio.action-radio-package:checked").attr('id');
            var entryArray = entryIdStr.split('-');
            var entryId = entryArray[1];
            var getUrlVarsVal = getUrlVars();
            var page = '';
            var perPage = '';
            var dp1_page = '';
            var dp1_perPage = '';

            if(typeof (getUrlVars()['page']) != undefined && (typeof (getUrlVars()['per-page']) != undefined)){
                page = getUrlVars()['page'];
                perPage = getUrlVars()['per-page'];
            }
            if(typeof (getUrlVars()['dp-1-per-page']) != undefined){
                dp1_perPage = getUrlVars()['dp-1-per-page'];
            }

            if(typeof (getUrlVars()['dp-1-page']) != undefined){
                dp1_page = getUrlVars()['dp-1-page'];
            }
           
            if(typeof selectedPackageId === 'undefined'){
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> You have not selected a seedlot.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            }else{
                //select the seedlot
                if(selectedPackageId){

                    //get the entries of this experiment
                    $.ajax({
                        url: '<?php echo $url; ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            entryId: entryId,
                            selectedPackageId: selectedPackageId,
                            selectedSeedId: selectedSeedId,
                            page: page,
                            perPage: perPage,
                            dp1_page: dp1_page,
                            dp1_perPage: dp1_perPage
                        },
                        success: function(response) {
                            if(response == 'success'){
                              
                                var hasToast = $('body').hasClass('toast');
                                if(!hasToast){
                                    $('.toast').css('display','none');
                                    var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully updated the entry and saved the selected package.</span>";
                                    Materialize.toast(notif, 5000);
                                }
                                e.preventDefault();
                                e.stopImmediatePropagation();

                                $('#select-package-modal').modal('hide');
                                $.pjax.reload({
                                  container: '#dynagrid-ec-entry-list-pjax',
                                  replace:false
                                });
                            }
                        },
                        error: function() {

                        }
                    });
                }
            }
        } else {
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons red-text left'>close</i> <span class='white-text'>No record found.</span>";
                Materialize.toast(notif, 5000);
            }
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    });

});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    
    return vars;
}

</script>

<style>
[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {
    position: relative;
    padding-left: 15px;
    cursor: pointer;
    display: inline-block;
    height: 15px;
    line-height: 35px;
    font-size: 0.5rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>