<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders browser for selected entries
 */

use kartik\grid\GridView;
use kartik\widgets\Select2;

use yii\helpers\Html;
use yii\helpers\Url;
?>

<p><?= \Yii::t('app', 'Reorder the selected entries by changing the entry number. Click Confirm to save.') ?></p>
<div id="mod-reorder-notif" class="hidden"></div>
<?php 
    $actionColumns = [
      [
        'class' => 'yii\grid\SerialColumn',
        'header' => false
      ]
    ];
    // all the columns that can be viewed in seasons browser
    $columns = [
        [
            'header' =>Yii::t('app', 'Entry No.'),
            'format' => 'raw',
            'headerOptions'=>['style'=>'min-width:200px !important'],
            'encodeLabel' => false,
            'vAlign'=>'middle',
            'value' => function($data) use ($options){
                $value = Select2::widget([
                    'name' => 'EntryNumberOrder',
                    'id' => 'reorder-'.$data['entryDbId'].'-'.$data['entryNumber'],
                    'data' => $options,
                    'value' => $data['entryNumber'],
                    'options' => [
                        'id' => $data['entryDbId'].'-'.$data['entryNumber'],
                        'placeholder' => 'Select a value',
                        'disabled'=> false,
                        'class'=>'reorder-entry-fields'
                    ],
                    'pluginOptions' => [
                        'allowClear' => false
                    ]
                ]);
        
                return $value;
            },
        ],
        [   'header' =>Yii::t('app', 'Germplasm Name'),
            'attribute'=>'designation',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'parentage',
            'enableSorting' => false,
        ],
    ];  

    $gridColumns = array_merge($actionColumns,$columns);
    
    echo $grid = GridView::widget([
      'pjax' => true, // pjax is set to always true for this demo
      'dataProvider' => $dataProvider,
      'id' => 'reorder-entry-table-grid',
      'tableOptions' => ['id' => 'reorder-entry-table', 'class' => 'table table-bordered white z-depth-3'],
      'columns' => $gridColumns,
      'toggleData'=>true,
      'striped'=>false
    ]);


$saveReorderedEntries = Url::to(['create/save-reordered-entries','id'=>$id]);
?>

<p>
    <?php
        if (!empty($existingExpData)) {
            echo \Yii::t('app', '<p><b>NOTE: Found existing records under this experiment. Reordering will delete the following records:</b></p>');
            echo Html::ul($existingExpData, ['item' => function($item) {
                return Html::tag('li', '• '. $item);
            }]);
        } 
    ?>
</p>
<script>

    var saveReorderedEntries =  '<?= $saveReorderedEntries ?>';
    var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
    $(".reorder-entry-fields").change(function(){
        var usedEntno = [];
        var errorFlag = false;
        $(".reorder-entry-fields").each(function(){
            if(usedEntno.includes(this.value)){
                //show notif and highlight row
                errorFlag = true;
            } else {
                usedEntno.push(this.value);
            }
        });

        if(errorFlag){
            $("#mod-reorder-notif").html('<div class="alert-danger" style="padding: 10px;">There is a duplicate entry number. Please correct the other one to proceed.</div>');
            $("#mod-reorder-notif").removeClass("hidden");
            $('#reorder-by-sort').attr('disabled', 'disabled');
        } else {
            $("#mod-reorder-notif").html('');
            $("#mod-reorder-notif").addClass("hidden");
            $('#reorder-by-sort').removeAttr('disabled');
        }
    });

    $("#reorder-by-sort").click(function(e){
        $('#reorder-by-sort').attr('disabled', 'disabled');
        var entryInfo = [];
        var targetEntryNo = [];
        $(".reorder-entry-fields").each(function(){
            var entryStr = (this.id).split("-");
            entryInfo.push({'entryDbId':entryStr[0], 'entryNumber':entryStr[1], 'targetEntryNumber':this.value});
        });

        $("#reorder-modal-content").html(loadingIndicator);

        localStorage.removeItem('ec_entry_list_entry_ids'); // reset selection
        if(entryInfo.length > 0){
            $.ajax({
                url: saveReorderedEntries,
                type: 'post',
                dataType:'json',
                async: true,
                cache: false,
                data: {
                    entryInfo:entryInfo
                },
                success: function(response) {
                    var notif = '';
                    
                    if(response){
                        notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'> Successfully reordered selected entries.</span>";

                        // reset selection
                        localStorage.removeItem('ec_entry_list_entry_ids');
                        $('#selected-count').html(0);
                        $("input:checkbox.entry-grid-select").each(function(){
                            $(this).removeAttr('checked');
                            $('.entry-grid-select').prop('checked',false);
                            $('.entry-grid-select').removeAttr('checked');
                            $("input:checkbox.entry-grid-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');
                        });

                        location.reload();
                    }
                    else{
                        notif = "<i class='material-icons red-text left'>error</i> <span class='white-text'> Selected entries were not reordered. Please try again.</span>";
                    }

                    $('#entry-list-reorder-modal').modal('hide');

                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        Materialize.toast(notif, 5000);
                    }

                    e.preventDefault();
                    e.stopImmediatePropagation();
                },
                error: function() {
                    $('#entry-list-reorder-modal').modal('hide');
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Unable to reorder entries. Please try again.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            });
        }
        else{
            $('#entry-list-reorder-modal').modal('hide');
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons red-text left'>warning</i> <span class='white-text'> No entry records to be processed. Please try again.</span>";
                Materialize.toast(notif, 5000);
            }
        }
    });


</script>