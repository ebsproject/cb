<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders browser for selected entries
 */

use kartik\grid\GridView;
use kartik\widgets\Select2;

use yii\helpers\Html;
use yii\helpers\Url;
?>

<p><?= \Yii::t('app', 'Reorder the selected entries by changing the entry number within the range values based on the starting entry number. Click Confirm to save.') ?></p>
<div id="mod-reorder-notif" class="hidden"></div>
<?php 
    $startEntryNumber = Select2::widget([
        'name' => 'startEntryNumber',
        'id' => 'startEntryNumber-id',
        'data' => $options,
        'value' => '',
        'options' => [
            'id' => 'entryNumberStart-id',
            'placeholder' => 'Select a value',
            'disabled'=> false,
        ],
        'pluginOptions' => [
            'allowClear' => false
        ]
    ]);
    echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Starting Entry number'.' '.'<span class="required">*</span>').'</label>';
        echo '<div style="width:30%">';
            echo $startEntryNumber;
        echo '</div>';
    $actionColumns = [
      [
        'class' => 'yii\grid\SerialColumn',
        'header' => false
      ]
    ];
    // all the columns that can be viewed in seasons browser
    $columns = [
        [
            'header' =>Yii::t('app', 'New Entry No.'),
            'format' => 'raw',
            'headerOptions'=>['style'=>'min-width:200px !important'],
            'encodeLabel' => false,
            'vAlign'=>'middle',
            'value' => function($data) use ($options){
                $value = Select2::widget([
                    'name' => 'EntryNumberOrder',
                    'id' => 'reorder-'.$data['entryDbId'].'-'.$data['entryNumber'],
                    'data' => [],
                    'value' => $data['entryNumber'],
                    'options' => [
                        'id' => $data['entryDbId'].'-'.$data['entryNumber'],
                        'placeholder' => 'Select a value',
                        'disabled'=> false,
                        'class'=>'reorder-entry-fields'
                    ],
                    'pluginOptions' => [
                        'allowClear' => false
                    ]
                ]);
        
                return $value;
            },
        ],
        [   'header' =>Yii::t('app', 'Current Entry Number'),
            'attribute'=>'entryNumber',
            'enableSorting' => false,
        ],
        [   'header' =>Yii::t('app', 'Germplasm Name'),
            'attribute'=>'designation',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'parentage',
            'enableSorting' => false,
        ],
    ];  

    $gridColumns = array_merge($actionColumns,$columns);
    
    echo $grid = GridView::widget([
      'pjax' => true, // pjax is set to always true for this demo
      'dataProvider' => $dataProvider,
      'id' => 'reorder-entry-table-grid',
      'tableOptions' => ['id' => 'reorder-entry-table', 'class' => 'table table-bordered white z-depth-3'],
      'columns' => $gridColumns,
      'toggleData'=>true,
      'striped'=>false
    ]);


$saveReorderedEntries = Url::to(['create/save-reorder-by-insert','experimentId'=>$id]);
?>

<p>
    <?php
        if (!empty($existingExpData)) {
            echo \Yii::t('app', '<p><b>NOTE: Found existing records under this experiment. Reordering will delete the following records:</b></p>');
            echo Html::ul($existingExpData, ['item' => function($item) {
                return Html::tag('li', '• '. $item);
            }]);
        } 
    ?>
</p>
<script>
    $(".reorder-entry-fields").attr('disabled', 'disabled');
    $("#reorder-by-sort").attr('disabled', 'disabled');
    var saveReorderedEntries =  '<?= $saveReorderedEntries ?>';
    var selectedCount = parseInt('<?=$selectedCount ?>');
    var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';

    $("#entryNumberStart-id").change(function(){
        var startValue = parseInt(this.value);
        var startValueCnt = startValue;
        var valueObject = [];
        var limit = startValue+selectedCount;

        for(var i=startValue; i<limit; i++){
            var tempStr = '"'+i+'":'+i+'';
            valueObject.push(tempStr);
        }
        var valueString = JSON.parse("{"+valueObject.join(",")+"}");
        
        $(".reorder-entry-fields").each(function(){
            updateFilterTags(this.id, valueString, startValueCnt);
            startValueCnt++;
        });
        $(".reorder-entry-fields").removeAttr('disabled');
        $('#reorder-by-sort').removeAttr('disabled');
    });

    function updateFilterTags(id, values, initial){
        var s1 = document.getElementById(id);

        $('#'+id).find('option').remove();

        $.each(values, function (key, value) {
         
          var newOption = document.createElement("option");
          newOption.value = key;
          newOption.innerHTML = value;
          s1.options.add(newOption);
        });
        $('#'+id).val(initial);
    }

    $(".reorder-entry-fields").change(function(){
        var usedEntno = [];
        var errorFlag = false;
        $(".reorder-entry-fields").each(function(){
            if(usedEntno.includes(this.value)){
                //show notif and highlight row
                errorFlag = true;
            } else {
                usedEntno.push(this.value);
            }
        });

        if(errorFlag){
            $("#mod-reorder-notif").html('<div class="alert-danger" style="padding: 10px;">There is a duplicate entry number. Please correct the other one to proceed.</div>');
            $("#mod-reorder-notif").removeClass("hidden");
            $('#reorder-by-sort').attr('disabled', 'disabled');
        } else {
            $("#mod-reorder-notif").html('');
            $("#mod-reorder-notif").addClass("hidden");
            $('#reorder-by-sort').removeAttr('disabled');
        }
    });

    $("#reorder-by-sort").click(function(e){

        $('#reorder-by-sort').attr('disabled', 'disabled');
        $(".close").addClass("hidden");
        
        var entryInfo = [];
        var targetEntryNo = [];
        $(".reorder-entry-fields").each(function(){
            var entryStr = (this.id).split("-");
            entryInfo.push({'entryDbId':entryStr[0], 'entryNumber':entryStr[1], 'targetEntryNumber':this.value});
        });
        $("#reorder-modal-content").html(loadingIndicator);

        if(entryInfo.length > 0){
             // bg process
            message = ' Request is being processed.';

            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons blue-text left'>info</i> <span class='white-text'>"+message+"</span>";
                Materialize.toast(notif, 5000);
            }
            
            localStorage.removeItem('ec_entry_list_entry_ids'); // reset selection
            $.ajax({
                url: saveReorderedEntries,
                type: 'post',
                dataType:'json',
                async: true,
                cache: false,
                data: {
                    entryInfo:entryInfo
                },
                success: function(response) {
                   var notif = '';
                    
                    if(response){
                        notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'> Successfully reordered selected entries.</span>";

                        // reset selection
                        localStorage.removeItem('ec_entry_list_entry_ids');
                        $('#selected-count').html(0);
                        $("input:checkbox.entry-grid-select").each(function(){
                            $(this).removeAttr('checked');
                            $('.entry-grid-select').prop('checked',false);
                            $('.entry-grid-select').removeAttr('checked');
                            $("input:checkbox.entry-grid-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');
                        });

                        location.reload();
                    }

                    $('#entry-list-reorder-modal').modal('hide');

                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        Materialize.toast(notif, 5000);
                    }

                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            });
        }
        else{
            $('#entry-list-reorder-modal').modal('hide');
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons red-text left'>warning</i> <span class='white-text'> No entry records to be processed. Please try again.</span>";
                Materialize.toast(notif, 5000);
            }
        }
    });

</script>