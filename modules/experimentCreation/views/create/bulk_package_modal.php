<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders data browser for packages (previously seedlots)
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;

?>

<?php
    if ($partitionView) {
?>
    <span class="new badge">Limited view</span>&nbsp;&nbsp;<em><?= \Yii::t('app', 'We limit the view for this page due to large number of records. Click the View All button to show all records in page.') ?></em>
    <a class="waves-effect waves-light btn pull-right" id="view-all-packages-id"><i class="material-icons right">visibility</i>View All</a>
<?php
    }
?>
<p><?= \Yii::t('app', 'Below are packages you can use.') ?></p>

<?php 
        // all the columns that can be viewed
        $columns = [
            [
              'header' => false,
              'value' => function ($data) use ($entryNumbers){

                  $checked = ($data['packageDbId'] == $data['currentPackageDbId'])? 'checked' : '';

                  if($entryNumbers[$data['entryNumber']] == 1){
                      $checked = 'checked';
                  }
                  
                  return '<p>
                      <input data-entryId="'.$data['entryDbId'].'" id="'.$data['packageDbId'].'_'.$data['entryNumber'].'" class="with-gap action-modal-radio-seedlot radio-'.$data['entryDbId'].'" name="SeedLotModal['.$data['entryDbId'].']" value="'.$data['seedDbId'].'_'.$data['packageDbId'].'" type="radio" '.$checked.' />
                      <label for="'.$data['packageDbId'].'_'.$data['entryNumber'].'"></label>
                  </p>';

                },
              'format' => 'raw',
              'enableSorting' => false,
            ],
            [
                'label' => Yii::t('app', 'Entry no.'),
                'value' => function ($data){
                    return $data['entryNumber'];
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'programName',
                'label' => Yii::t('app', 'Program'),
                'format' => 'raw',
                'enableSorting' => false,
            ],  
            [
                'attribute'=>'germplasmName',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'seedName',
                'label' => Yii::t('app', 'Seed name'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'packageLabel',
                'label' => Yii::t('app', 'Package label'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'packageQuantity',
                'label' => Yii::t('app', 'Qty'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'packageUnit',
                'label' => Yii::t('app', 'Unit'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentYear',
                'label' => Yii::t('app', 'Year'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentName',
                'label' => Yii::t('app', 'Experiment'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentType',
                'label' => Yii::t('app', 'Experiment Type'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentSeason',
                'label' => Yii::t('app', 'Season'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentStageCode',
                'label' => Yii::t('app', 'Stage'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            
        ];  
        
		echo $grid = GridView::widget([
			'pjax' => true, // pjax is set to always true for this demo
      'dataProvider' => $data,
			'id' => 'bulk-select-seed-lot-modal',
      'columns' => $columns,
      'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
      'striped'=>false
    ]);


 ?>
<style>
[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {
    position: relative;
    padding-left: 15px;
    cursor: pointer;
    display: inline-block;
    height: 15px;
    line-height: 35px;
    font-size: 0.5rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>

<?php
    $viewAllPackagesUrl = Url::to(['view/view-all-packages', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>

<script>
    // Display all packages on a different page
    $(document).on('click', '#view-all-packages-id', function() {
        let filters = JSON.parse('<?php echo json_encode($filters); ?>')
        let packageFilters = {
            yearFilter: filters['yearFilter'] !== undefined ? filters['yearFilter'] : '',
            seasonFilter: filters['seasonFilter'] !== undefined ? filters['seasonFilter'] : '',
            experimentFilter: filters['experimentFilter'] !== undefined ? filters['experimentFilter'] : '',
            stageFilter: filters['stageFilter'] !== undefined ? filters['stageFilter'] : ''
        }
        let filterString = Object.keys(packageFilters).map(key=>key+'='+packageFilters[key]).join('&')
        var url = '<?php echo $viewAllPackagesUrl; ?>' + '&' + filterString
        window.location = url;
    })
</script>
