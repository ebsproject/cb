<?php
/**
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Preview update product name
 */

use app\models\Program;
use app\modules\experimentCreation\models\ExperimentModel;
use kartik\select2\Select2;
use yii\helpers\Url;
use kartik\grid\GridView;



echo $grid = GridView::widget([
    'pjax' => true, // pjax is set to always true for this demo
    'dataProvider' => $dataProvider,
    'id' => 'product-name-preview',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'entlistno',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'product_name',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'new_product_name',
            'label' => Yii::t('app', 'New Germplasm'),
            'format' => 'raw',
            'value' => function($data){
                return $data['new_product_name'];
            },
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'background-color:#4CAF50'];
            },
        ]
    ]
]);

?>