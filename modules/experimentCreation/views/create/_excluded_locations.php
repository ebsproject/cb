<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for harvested plots for entry list selection
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Place;

?>

<div style="height:25px !important" class="row">
    <div class="col col-sm-6 view-entity-content">
        <h4>
            <?php
                echo $savedList->name;
            ?>
        </h4>
    </div>
    <div style="padding:0px !important" class="col col-sm-6">

        <?php if($dataProvider->getTotalCount() > 0){ ?>

            <button id="add-locations-btn" class="btn btn-primary waves-effect waves-light pull-right disabled" title="<?= \Yii::t('app', 'Add to location list') ?>" style="margin-right:20px;max-width: 105px;">
                <?= \Yii::t('app', 'Add') ?>
                <i class="material-icons right">add</i>
            </button>

        <?php
            }
        ?>
        <button id="back-btn" class="btn btn-primary waves-effect waves-light grey pull-right" title="<?= \Yii::t('app', 'Go to list') ?>" style="margin-right:5px;max-width: 105px;">
            <?= \Yii::t('app', 'Back') ?>
            <i class="material-icons right">keyboard_backspace</i>
        </button>

    </div>
</div>

<div class="row">
    <?php
    echo $grid = GridView::widget([
        'pjax' => true, // pjax is set to always true for this demo
        'dataProvider' => $dataProvider,
        'options'=>['id' => 'location-excluded-browser'],
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
        'toggleData'=>true,
        'columns'=>[
            [
                'class' => 'yii\grid\CheckboxColumn', 
                'name' => 'location_excluded_selection[]',
                'checkboxOptions' => function ($data, $key, $index, $column) {
                    return ['value' => $data['id'], 'class'=>'add-to-list-class'];
                },
                'header' => Html::checkBox('location_members_all', false, [
                    'class' => 'select-on-check-all'
                ]),
            ],
            [
                'class' => 'yii\grid\SerialColumn'
            ], 
            [
                'attribute'=>'abbrev',
                'enableSorting' => false
            ],
            [
                'attribute'=>'display_name',
                'enableSorting' => false
            ],
            [
                'attribute'=>'remarks',
                'enableSorting' => false
            ],
        ]
    ]);

    ?>
</div>

<?php
    $addLocationUrl = Url::to(['create/save-location-to-list','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>

<script>
    $(document).ready(function(){

        $('#add-locations-btn').click(function(){
            var selectedArray = [];

            $('[name="location_excluded_selection[]"]:checked').each (function (i, e){
                selectedArray.push ($(this).val());
            });
            var url = '<?php echo $addLocationUrl; ?>';
            
            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    savedListId: '<?php echo $savedList->id; ?>',
                    experimentId: '<?php echo $experiment->id?>',
                    selectedArray: selectedArray
                },
                success: function(response) {

                },
                error: function() {

                }
            });
        });

        $('.add-to-list-class').on('click', function() {
            var selectedArray = [];
            $('[name="location_excluded_selection[]"]:checked').each (function (i, e){
                selectedArray.push ($(this).val());
            });
            
            if(selectedArray.length > 0){
                $('#add-locations-btn').removeClass('disabled');
            } else{
                $('#add-locations-btn').addClass('disabled');
            }
        });
        
        $('[name="location_members_all"]').on('click', function() {
            $("[name='location_excluded_selection[]']").prop('checked', $(this).prop('checked')).trigger('change');
            var selectedArray = [];
            $('[name="location_excluded_selection[]"]:checked').each (function (i, e){
                selectedArray.push ($(this).val());
            });
            if(selectedArray.length > 0){
                $('#add-locations-btn').removeClass('disabled');
            } else{
                $('#add-locations-btn').addClass('disabled');
            }
        });
    });
</script>