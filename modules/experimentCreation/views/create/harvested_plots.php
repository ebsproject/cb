<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for harvested plots for entry list selection
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;


$selectedExperimentName = !empty($selectedExperiment) ? $selectedExperiment['experimentName'] : "";
$selectedExperimentId = !empty($selectedExperiment) ? $selectedExperiment['experimentDbId'] : null;
$experimentId = !empty($experiment) ? $experiment['experimentDbId']: null;
?>

<div style="height:25px !important" class="row">
    <div class="col col-sm-6 view-entity-content">
        <h4>
            <?php
            echo $selectedExperimentName;
            ?>
        </h4>
    </div>
    <div style="padding:0px !important" class="col col-sm-6">

        <?php if($dataProvider->getTotalCount() > 0){ ?>

            <button id="add-products-btn" class="btn btn-primary waves-effect waves-light pull-right">
                <?= \Yii::t('app', 'Add Entries') ?>
                <i class="material-icons right">add</i>
            </button>

        <?php
            }
        ?>


        <button id="back-btn" class="btn btn-primary waves-effect waves-light pull-right">
            <?= \Yii::t('app', 'Back') ?>
            <i class="material-icons right">keyboard_backspace</i>
        </button>


    </div>
</div>

<div class="row">
    <?php
     echo GridView::widget([
        'pjax' => false, // pjax is set to always true for this demo
        'dataProvider' => $dataProvider,
        'id' => 'harvested-plots-pd',
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
        'striped'=>false,
        'toggleData'=>true,
        'columns'=>[
            [
                'header' => false,
                'class' => 'yii\grid\SerialColumn'
            ], 
            [
                'attribute'=>'designation',
                'label' => Yii::t('app', 'Germplasm Name'),
                'format' => 'raw',
                'enableSorting' => false,            
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ]
            ],
            'germplasmState',
            'germplasmType',
            [
                'attribute'=>'label',
                'label' => Yii::t('app', 'Package Label'),
                'format' => 'raw',
                'enableSorting' => false,            
            ],
            [
                'attribute'=>'generation',
                'label' => Yii::t('app', 'Generation'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'year',
                'label' => Yii::t('app', 'Year'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'parentage',
                'label' => Yii::t('app', 'Parentage'),
                'format' => 'raw',
                'enableSorting' => false,
                'contentOptions'=>[
                    'class' => 'germplasm-name-col'
                ]
            ],
        ]
    ]);

    ?>
</div>

<?php
    $addEntriesUrl = Url::to(['create/add-entries-from-plots','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $checkThresholdCountUrl = Url::to(['create/check-threshold-count','id'=>$id]);
?>

<script>
    $(document).ready(function(){

        $('#add-products-btn').click(function(e){
            
            var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';

            $('#harvested-plots-trials-browser,#harvested-plots-browser').html(loading); 
            
            var url = '<?php echo $addEntriesUrl; ?>';

            //Notification while background job is being created
            $.ajax({
                url: checkThresholdCountUrl,
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'createEntries',
                    toolStep: 'entries',
                    method: 'create',
                    selectedItemsCount: '<?php echo isset($totalCount) ? $totalCount : 0;?>'
                },
                success: function(response) {
                    isBgUsedFlag = response;
                    $('#notification').removeClass('hidden');
                    if(isBgUsedFlag == true){
                        $('#harvested-plots-trials-browser,#notification').html('<div class="alert ">'+ 
                                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                    '<i class="fa fa-info-circle"></i>'+
                                        ' A background job is being created.' +
                                '</div></div>');
                    }
                }
            });

            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    selectedExperimentId: '<?php echo $selectedExperimentId; ?>',
                    experimentId: '<?php echo $experimentId?>',
                    processId: '<?php echo $processId?>',
                    entries:'<?php echo json_encode($entries)?>',
                    totalCount: '<?php echo $totalCount?>'
                },
                success: function(response) {
                    $('#harvested-plots-trials-browser,#harvested-plots-browser').hide();
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully added the selected list.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    $('#nurseries-browser').show();
                    $('.search-entry-field-div').show(); 
                    
                    $('a[href="#from-input-list-link"]').click();
                    $('#add-entries-modal').modal('hide');

                    e.preventDefault();
                    e.stopImmediatePropagation();

                    $.pjax.reload({
                      container: '#dynagrid-ec-entry-list-pjax',
                      replace:false
                    });
                },
                error: function() {

                }
            });

        });

    });


</script>
<style>
#harvested-plots-pd-container.table-responsive.kv-grid-container{
    max-height: 380px !important;
}
</style>