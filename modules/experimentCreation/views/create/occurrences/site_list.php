<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders browser for site list for occurrence site selection
 */

use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\Url;

use yii\helpers\Html;
?>

<p><?= \Yii::t('app', 'Below are the site lists that you can use in your occurrence list. Please make sure that site list count is less than or equal to the number of occurrences. <em>Applying the list will overwrite the existing occurrence site values.</em>') ?></p>

<div id="mod-progress" class="hidden"></div>
<div id="mod-notif" class="hidden"></div>

<?php 
    $actionColumns = [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => false
        ], 
        [
            'header' => false,
            'value' => function ($data) use ($occurrenceCount, $experimentId){
                  
                return '<p>
                    <input id="'.$data['listDbId'].'-'.$occurrenceCount.'-'.$experimentId.'" value="'.$data['memberCount'].'-'.$data['listDbId'].'" class="with-gap action-radio-sitelist" name="ListSelect-'.$occurrenceCount.'" type="radio" />
                    <label for="'.$data['listDbId'].'-'.$occurrenceCount.'-'.$experimentId.'"></label>
                </p>';

            },
            'format' => 'raw',
            'enableSorting' => false,
        ]
    ];
    // all the columns that can be viewed in seasons browser
    $columns = [
        [
            'attribute'=>'abbrev',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'name',
            'enableSorting' => false,
        ],
        [
            'header' => Yii::t('app', 'No. of Members'),
            'value' => function ($data) {
                return '<span class="new badge">'.$data['memberCount'].'</span>';
            },
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'header' => Yii::t('app', 'List Type'),
            'value' => function ($data) {
                return ucfirst($data['type']);
            },
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'header' => Yii::t('app', 'List Sub Type'),
            'value' => function ($data) {
                return empty($data['subType']) ? '<span class="not-set">(not set)</span>' : ucfirst($data['subType']);
            },
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'remarks',
            'enableSorting' => false,
        ],
    ];  

    $gridColumns = array_merge($actionColumns,$columns);
    
    echo $grid = GridView::widget([
      'pjax' => true, // pjax is set to always true for this demo
      'dataProvider' => $data,
      'id' => 'saved-list-table',
      'tableOptions' => ['class' => 'table table-bordered white z-depth-3  search-entry-table'],
      'columns' => $gridColumns,
      'toggleData'=>true,
      'striped'=>false
    ]);
    $populateSitesUrl = Url::to(['occurrence/populate-sites', 'id'=>$experimentId]);
 ?>


<script>
$(document).ready(function(){

    $('#confirm-site-btn').click(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var selectedId = $("input:radio.action-radio-sitelist:checked").val();
        if(selectedId != null){
            var selectedIdArr = selectedId.split('-');
            var selectedListId = selectedIdArr[1];
            var selectedMemberCount = selectedIdArr[0];
            
            var listStr = $("input:radio.action-radio-sitelist:checked").attr('id');
            var listStrArray = listStr.split('-');

            var experimentId = listStrArray[2];
            var occurrenceCount = listStrArray[1];
            
            if(parseInt(selectedMemberCount) <= parseInt(occurrenceCount)){

                var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
                var content_modal = '#site-list-selection-modal > .modal-dialog > .modal-content > .modal-body';
                $(content_modal).html(loadingIndicator);
                $('#confirm-site-btn').attr('disabled', 'disabled');

                $.ajax({
                    url: '<?php echo $populateSitesUrl;?>',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        listDbId: selectedListId,
                    },
                    success: function(response) {
                        if(response == 'success'){
                            $('#confirm-site-btn').removeAttr('disabled');
                            var hasToast = $('body').hasClass('toast');
                            if(!hasToast){
                                $('.toast').css('display','none');
                                var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully applied the site list.</span>";
                                Materialize.toast(notif, 5000);
                            }
                            e.preventDefault();
                            e.stopImmediatePropagation();

                            $('#site-list-selection-modal').modal('hide');
                            $.pjax.reload({
                              container: '#dynagrid-ec-site-list-pjax',
                              replace:false
                            });
                        }
                    },
                    error: function() {
                
                    }
                });
            } else {
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons red-text left'>close</i> <span class='white-text'>Unable to apply the site list. <b>Site list count should be less than or equal to the number of occurrences</b>.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            }
        } else {
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons red-text left'>close</i> <span class='white-text'>Please select a site list.</b>.</span>";
                Materialize.toast(notif, 5000);
            }
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    });

});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    
    return vars;
}

</script>

<style>
[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {
    position: relative;
    padding-left: 15px;
    cursor: pointer;
    display: inline-block;
    height: 15px;
    line-height: 35px;
    font-size: 0.5rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>