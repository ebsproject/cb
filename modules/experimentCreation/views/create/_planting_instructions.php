<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for displaying entries extracted from planting instructions records
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div style="height:25px !important" class="row">
    <div class="col col-sm-6 view-entity-content">
        <h4>
            <?= $experimentName;?>
        </h4>
    </div>
    <div style="padding:0px !important" class="col col-sm-6">

        <?php if($dataProvider->getTotalCount() > 0){ ?>

            <button id="add-<?=$gridName;?>-btn" class="btn btn-primary waves-effect waves-light pull-right">
                <?= \Yii::t('app', 'Add Parents') ?>
                <i class="material-icons right">add</i>
            </button>

        <?php
            }
        ?>

        <button id="back-btn" class="btn btn-primary waves-effect waves-light pull-right">
            <?= \Yii::t('app', 'Back') ?>
            <i class="material-icons right">keyboard_backspace</i>
        </button>

    </div>
</div>

<div class="row">
    <?php
    echo $grid = GridView::widget([
        'pjax'=>true,
        'dataProvider'=>$dataProvider,
        'id'=>"$gridName-entries",
        'tableOptions'=>['class' => 'table table-bordered white z-depth-3'],
        'toggleData'=>true,
        'columns'=>[
            [
                'header'=>false,
                'class'=>'yii\grid\SerialColumn'
            ],
            [
                'attribute'=>'entryNumber',
                'label' => Yii::t('app', 'Entry No'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'entryCode',
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'entryName',
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'designation',
                'label' => Yii::t('app', 'Germplasm Name'),
                'format' => 'raw',
                'enableSorting' => false,            
            ],
            [
                'attribute'=>'parentage',
                'format' => 'raw',
                'enableSorting' => false,            
            ],
        ]
    ]);

    ?>
</div>

<?php
    $addEntriesUrl = Url::to(['create/add-entries-from-plots','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $checkThresholdCountUrl = Url::to(['create/check-threshold-count','id'=>$id]);
?>

<script>
    $(document).ready(function(){

        $('#add-<?=$gridName;?>-btn').click(function(e){
            
            var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
            var url = "<?= $addEntriesUrl; ?>";
            $('#planting-instructions-<?= $gridName ?>').html(loading); 

            //Notification while background job is being created
            $.ajax({
                url: checkThresholdCountUrl,
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'createEntries',
                    toolStep: 'entries',
                    method: 'create',
                    mode: 'copy'
                },
                success: function(response) {
                    isBgUsedFlag = response;
                    $('#planting-notification-<?= $gridName ?>').removeClass('hidden');
                    if(isBgUsedFlag == true){
                        $('#planting-notification-<?= $gridName ?>').html('<div class="alert ">'+ 
                                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                    '<i class="fa fa-info-circle"></i>'+
                                        ' A background job is being created.' +
                                '</div></div>');
                    }
                }
            });

            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    experimentId: "<?= $id; ?>",
                    occurrenceDbId: "<?= $occurrenceDbId; ?>"
                },
                success: function(response){
                    $('#planting-instructions-<?= $gridName ?>').hide();
                    $('#occurrence-browser').show();
                    $('.search-entry-field-div').show(); 
                    $('a[href="#parent-list-link"]').click();

                    $('#add-entries-modal').modal('hide');

                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully added the selected list.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();

                    $.pjax.reload({
                      container: '#dynagrid-ec-entry-list-pjax',
                      replace:false
                    });
                },
                error: function(){

                }
            });

        });

    });
</script>
