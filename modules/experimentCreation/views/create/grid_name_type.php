<?php
/**
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Preview update product name
 */

echo '<div style="display:none">';
  $this->widget('bootstrap.widgets.TbExtendedGridView', array(
        'id' => 'modify-product-name-browser',
        'type'=> 'bordered condensed',
        'enablePagination' => false,
        'template' => '{items}',
        'dataProvider'=>$dataAll,
        'columns'=>array(
            array(
                'name'=>'new_product_name',
                'type'=>'raw',
                'value' => function($data){
                    return '<input type="hidden" class="new-product-name" name="'.$data['id'].'" value="'.$data['new_product_name'].'">';
                }
            )
        )
    ));
echo '</div>';

$this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'type'=> 'bordered condensed',
    'dataProvider'=>$dataProvider,
    'enablePagination' => false,
    'template' => '{items}',
    'columns'=>array(
        array(
            'class' => "bimii.widgets.BimButtonColumn",
            'value' => "rowNumber",
            'pageSizeSelector'=>false,
        ),
        array(
            'header' => '<span title="Entry number" data-toggle="tooltip">ENTRY NO.</span>',
            'name' => 'entno'
        ),
        array(
            'header'=>'<span title="Old value for germplasm" data-toggle="tooltip">OLD GERMPLASM</span>',
            'name'=>'old_product_name',
            'type'=>'raw'
        ),
        array(
            'header'=>'<span title="New value for germplasm after update confirmation" data-toggle="tooltip">NEW GERMPLASM</span>',
            'name'=>'new_product_name',
            'type'=>'raw',
            'value' => function($data){
                return '<span class="text-color-changed-value">'.$data['new_product_name'].'</span>';
            }
        ),
        array(
            'header'=>'<span title="Name type of the new germplasm" data-toggle="tooltip">NAME TYPE</span>',
            'name'=>'name_type',
            'type'=>'raw',
            'value' => function($data){
                return '<b>'.$data['name_type'].'</b>';
            }
        )
    )
));