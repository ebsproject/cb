<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice. If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders upload planting array web form
 */
use kartik\widgets\FileInput;
use macgyer\yii2materializecss\widgets\Button;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


// $hiddenRice = 'hidden';
// $hiddenMaize = 'hidden';

// if(in_array($design, ['Augmented RCB','Partially Replicated', 'Unspecified', 'Alpha-Lattice', 'RCBD', 'Row-Column'])){
//     $hiddenRice = '';
// }
// if(in_array($design, ['Alpha-Lattice'])){
//     $hiddenMaize = '';
// }
// echo '<p>Note: The design accepted for uploading is only for <em>'.$design.'</em>. Accepted fieldbook templates: <a href="#" class="download-file '.$hiddenRice.'" id="template1-file"><b>Template 1</b></a><a style="margin-left:10px;" href="#" class="download-file '.$hiddenMaize.'" id="template2-file"><b>Template 2</b></a></p>';

echo '<p>Accepted fieldbook templates: <a href="#" class="download-file" id="template1file"><b>Design Template</b></a></p>';
echo '<p>Required columns per each design:<a style="margin-left:10px;" href="#" class="download-file" id="requiredheaders"><b>Required Columns</b></a></p>';
echo '<div id="download-link-div"></div>';
echo '<div class="col col-md-6 upload-panel" style="margin-top:15px">';

echo FileInput::widget([
    'name' => 'upload_design_array',
    'pluginOptions' => [
        'uploadAsync' => false,
        'uploadUrl' => Url::to(['/experimentCreation/upload/design-array', 'id'=>$id, 'program'=>$program, 'occurrenceDbIds'=>$occurrenceDbIds]),
        'allowedFileExtensions' => ['csv'],
        'browseClass' => 'btn waves-effect waves-light browse-btn',
        'showCaption' => false,
        'showRemove' => false,
        'showZoom' => false,
        'browseLabel' => 'Browse',
        'removeLabel' => '',
        'uploadLabel' => 'Validate',
        'uploadTitle' => 'Validate file',
        'uploadIcon' => '<i class="material-icons inline-material-icon">file_upload</i>',
        'uploadClass' => 'btn',
        'removeClass' => 'btn btn-danger',
        'cancelClass' => 'btn grey lighten-2 black-text',
        'dropZoneTitle' => \Yii::t('app', 'Drag and drop file here or<br/>click Browse<br/><small>Use the design array files in CSV format. Specify required fields to validate.</small>'),
        'overwriteInitial' => true,
        'maxFileCount' => 1,
        'autoReplace' => true,
        'disabledPreviewTypes' => ['text'],
        'fileActionSettings' => [
            'showUpload' => false,
            'showZoom' => false,
            'removeIcon' => '<i class="fa fa-trash"></i>',
            'showRotate' => false,
            'indicatorNew' => '<i class="fa fa-plus-circle text-warning"></i>',
            'indicatorSuccess' => '<i class="fa fa-check-circle text-success"></i>',
            'indicatorError' => '<i class="fa fa-exclamation-circle text-danger"></i>',
            'indicatorLoading' => '<i class="fa fa-hourglass-half text-muted"></i>',
            'indicatorPaused' => '<i class="fa fa-pause-circle text-info"></i>',
        ],
        'theme'=>'fa'
    ],
    'pluginEvents' => [
        'fileclear' => 'function() { validate("clear"); }',
        'filereset' => 'function() { validate("test"); }',
        'fileloaded' => 'function() { validate(); }',
        'fileuploaderror' => 'function() { validate("clear"); }',
    ],
    'options' => [
        'multiple' => false,
        'accept' => 'csv/*', 
        'id' => 'design-array-file-input'
    ]
]);
echo '</div>';

echo '<div id="preview-panel" class="col col-md-5 pull-right hidden" style="margin-top:50px"></div>';

$submitDesignFile = Url::to(['upload/submit-file','id'=>$id, 'program'=>$program, 'occurrenceDbIds'=>implode("|",$occurrenceDbIds)]);
$downloadUrl = Url::to(['/experimentCreation/upload/check-file']);
$directDownloadUrl = Url::to(['/experimentCreation/upload/download-file']);
$occurrenceDbIdsStr = implode(";", $occurrenceDbIds);
$this->registerJs(<<<JS

var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
var validateSuccess = false;
var tmpfilename = '';
var totalOccurrences = 0;
var information = null;

// validate if required fields specified upon file load
function validate(action){
    // if file is cleared
    if(action == 'clear'){
        $('#preview-panel').html('');
        $('.confirm-upload-design-array').addClass('hidden');  
    }
}
// remove file upon clicking re-upload
$(document).on('click', '.browse-btn', function(e){
    // remove previously uploaded file
    $('.fileinput-remove').trigger('click');
    // change browse label
    $('.browse-btn span').text('Browse');
    $('#cancel-upload-design-array').addClass('modal-close-button');
});

// hide validate button upon remove of file
$(document).on('click','.fileinput-remove, .kv-file-remove', function(e){
    $('#preview-panel').html('');
    $('.confirm-upload-design-array').addClass('hidden');  
    $('.validate-upload-design-array').addClass('hidden');
});
// cancel upload mapped plots and generate location, reset modal centent
$(document).on('click', '#cancel-upload-design-array', function(){
    $('.upload-design-array-form-modal-body').html('');
});
// hide validate button upon remove of file
$(document).on('click','.confirm-upload-design-array', function(e){
    $('#upload-design-array-modal').modal('show')
    var modalBody = '.upload-design-array-form-modal-body';
    $(modalBody).html(loading);
    $.ajax({
        url: '$submitDesignFile',
        type: 'post',
        dataType: 'json',
        data:{
            'occurrences':totalOccurrences,
            'tmpfilename':tmpfilename,
            'designInfo': '$designInfo',
            'information':information
        },
        success: function(response){
            if(response == 'failed'){
                var notif = "<i>There was a problem while submitting the file. Please try to upload again.</i>";
                Materialize.toast(notif,3000);
            }

            $('#cancel-upload-design-array').trigger('click');
            
            location.reload();
            
        },
        error: function(){
            var errorMessage = '<i>There was a problem while submitting the file. Please try to upload again.</i>';
            $(modalBody).html(errorMessage);
        }
    });
});

// cancel upload mapped plots and generate location, reset modal centent
$(document).on('click', '.download-file', function(){
    var id = this.id;
    $.ajax({
        url: '$downloadUrl'+'?filename='+id,
        type: 'post',
        dataType: 'json',
        data:{
            design:'$design'
        },
        success: function(response){
            if(!response['success']){
                var notif = "<i class='material-icons red-text left'>close</i> There is no available sample file for this crop.";
                Materialize.toast(notif,5000);
            } else {
        
                var a = document.createElement('a');
                a.setAttribute('href', '$directDownloadUrl'+'?path='+response['path']+'&filename='+response['filename']);
                a.setAttribute('download', response['filename']);

                var aj = $(a);
                aj.appendTo('download-link-div');
                aj[0].click();
                aj.remove();
               
            }
        },
        error: function(){
            var notif = "<i class='material-icons red-text left'>close</i> A design must be selected first to download the corresponding template. Please try again.";
            Materialize.toast(notif,5000);
        }
    });
});

// if validation is successful
$('#design-array-file-input').on('filebatchuploadsuccess', function(e, data) {

    var response = data.response;
    $('#preview-panel').html('');
    // if successfully validated, display preview
    if(response.success){
        var htmlString = '';
        validateSuccess = true;
        tmpfilename = response.fileName;

        htmlString = '<div class="card-panel"><div class="row"> <h5 class="card-header">Summary Information</h5>';
        information = response.information;
        for (var prop in information) {
            if(prop == 'totalOccurrences'){
                totalOccurrences = information[prop]['value'];
            }
            if(information[prop]['label'] != undefined){
                 htmlString += '<div>'+
                  '<div class="col col-md-8"><label>'+information[prop]['label']+'</label></div>'+
                  '<div class="col col-md-4 bold" style="word-wrap:break-word;">'+information[prop]['value']+'</div>'+
                  '</div>';
            }   
        }

        htmlString += '<div class="clearfix"></div></div></div>';
        $('#preview-panel').html(htmlString);
        $('#preview-panel').removeClass("hidden");

        $('.confirm-upload-design-array').removeClass('hidden');
        $('#cancel-upload-design-array').removeClass('modal-close-button');
    }
});  
JS
);
?>

<style>
#summarytable td,th {
    padding: 0px 0px !important;
    display: table-cell;
    text-align: left;
    vertical-align: middle;
    border-radius: 2px;
}

.card-header {
    margin-left: 11px;
    font-weight: bold;
}

.border-top {
    border-top: 1px solid #eee;
    padding-top: 14px;
    margin-top: 15px;
    margin-bottom: 10px;
}
</style>


