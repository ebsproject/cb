<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders design step for experiment creation
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use app\models\Program;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use macgyer\yii2materializecss\widgets\Button;

$disabled = '';
if (strpos($model['experimentStatus'],'design generated') === false) {
    $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model'=>$model,
    'program'=>$program,
    'saveBtnLabel'=>'Next',
    'nextBtn' => $disabled,
    'btnName' => 'no_action_btn'
]);

$uiDisabled = 'hidden';
if($uiTemplate != 'none'){
    $uiDisabled = '';
}
echo '<p>'.Yii::t('app', 'Specify the minimum required parameters for the selected design.').'</p>';

?>
    <div id="mod-notif" class="hidden"></div>
    <div class="row" style="margin-top:10px;padding-top:10px; ">
        <div class="col col-md-6">
        <div id="warning-design-div-panel" class="panel-warning hidden"><div id="warning-design-div-msg-panel" class="card-panel"></div></div>
        <div id="error-design-div-panel" class="panel-error hidden"><div id="error-design-div-msg-panel" class="card-panel"></div></div>
        <div id="notif_field" class="hidden"></div>
        <?php
            echo $grid = GridView::widget([
              'pjax' => true, // pjax is set to always true for this demo
              'dataProvider' => $entryInfoDataProvider,
              'id' => 'entry-info-design-browser', 
              'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-seed-lot-table'],
              'striped'=>false,
              'showPageSummary'=>false,
              'columns'=>[
                  [
                    'attribute'=>'test',
                    'label' => Yii::t('app', 'No. of Test Entries'),
                    'format' => 'raw',
                    'enableSorting' => false,
                  ],  
                  [
                    'attribute'=>'check',
                    'label' => Yii::t('app', 'No. of Checks'),
                    'format' => 'raw',
                    'enableSorting' => false,
                  ], [
                    'attribute'=>'total',
                    'label' => Yii::t('app', 'Total Entries'),
                    'format' => 'raw',
                    'enableSorting' => false,
                  ]
              ]
            ]);

        ?>
        
        <div class="row" style="margin-bottom:15px;">
            <div class="col col-sm-10" style="margin-left:40px;">
               <div class="col col-sm-5">
              <?php
                  //Switch for the new UI
                  echo '<label class="control-label">'.\Yii::t('app', 'Across Environment Design').'</label>';

                 
                  echo Select2::widget([
                      'name' => 'ExperimentDesign[ACROSS_ENV_DESIGN]',
                      'value' => isset($acrossEnvDesign) ? $acrossEnvDesign:'none',// initial value
                      'data' => ['none'=>'None', 'random'=>'Random'], //this will be hardcoded for now
                      'id' => 'acrossEnvDesign_id',
                      'options' => ['placeholder' => Yii::t('app', 'Select value'), 'class' => 'select2-Drop pull-left', 'style'=>'margin-top:25px;'
                      ],
                      // 'addon' => [
                      //     'append' => [
                      //         'content' => '<i class="material-icons" title="'.Yii::t('app', '').'">help</i>'
                      //     ]
                      // ]
                  ]);

              ?>
              </div>
            </div>
            <div class="col col-sm-5" style="margin-left:50px;">
             
                <?php

                    $upload = '';
                    
                    echo '<label class="control-label">'.\Yii::t('app', 'Design').'</label>';
                   
                    if(isset($experimentDesign) && $experimentDesign != null){ //add info button
                        $designTitle = isset($designDesc[$experimentDesign]) ? $designDesc[$experimentDesign] : 'Please select a design.';
                        $hidden='';
                    } else {
                        $designTitle = 'Please select a design.';
                        $hidden = 'hidden';
                    }
                    echo Select2::widget([
                        'name' => 'ExperimentDesign[DESIGN]',
                        'value' => isset($experimentDesign) ? $experimentDesign:'',// initial value
                        'data' => $designArray,
                        'id' => 'design_select_id',
                        // 'title' => Yii::t('app', $designDesc[$experimentDesign]),
                        'options' => ['placeholder' => Yii::t('app', 'Select design'), 'class' => 'select2-Drop pull-left', 'style'=>'margin-top:25px;'
                        ],
                         'addon' => [
                              'append' => [
                                  'content' => '<i class="material-icons" '.$hidden.' title="'.Yii::t('app', $designTitle).'">help</i>'
                              ]
                          ]
                    ]);
                    
                ?>
            </div>
            <div class="col col-sm-5">
              <?php

                  if(isset($planDesign['status']) && in_array($planDesign['status'], ['randomization in progress', 'upload in progress'])){
              ?>
                  <a id="submit_design_btn" disabled href="#" class="btn btn-primary waves-effect waves-light pull-left" style="margin-top:25px;margin-bottom:5px;"><?= \Yii::t('app', 'Generate Design') ?></a>
              <?php   
                  } else if(isset($planDesign['status']) && $planDesign['status'] == 'deletion in progress'){
              ?>
                  <a id="submit_design_btn" disabled href="#" class="btn btn-primary waves-effect waves-light pull-left" style="margin-top:25px;margin-bottom:5px;"><?= \Yii::t('app', 'Generate Design') ?></a>
              <?php
                  } else {
                      $label = 'Generate Design';
                      if(isset($planDesign['status']) && $planDesign['status'] == 'randomized'){
                          $label = 'Delete Design';
                      }
              ?>
                  <a id="submit_design_btn" href="#" class="btn btn-primary waves-effect waves-light pull-left" style="margin-top:25px;margin-bottom:5px;"><?= \Yii::t('app', $label) ?></a>
              <?php
                  }
                  if((in_array(strtolower($design), ['alpha-lattice', 'rcbd', 'augmented rcbd', 'partially replicated', 'unspecified', 'row-column'])) && (empty($planDesign['status']) || (isset($planDesign['status']) && !in_array($planDesign['status'], ['randomization in progress', 'deletion in progress', 'upload in progress'])))){
                      echo '&nbsp;<a id="upload_design_array" href="#" class="grey lighten-3 black-text btn btn-primary waves-effect waves-light pull-left " style="margin-top:25px;margin-left:5px"><i class="material-icons">file_upload</i></a>';
                  } else {
                    echo '&nbsp;<a id="upload_design_array" href="#" class="grey lighten-3 black-text btn btn-primary waves-effect waves-light pull-left hidden" style="margin-top:25px;margin-left:10px"><i class="material-icons">file_upload</i></a>';
                  }
              ?>
            </div>
        </div>
        <div class="col col-md-12 container-design <?=$uiDisabled?>" id="uiTemplate-panel-div">
            <?php 
                if($uiTemplate == 'checkList'){
                    echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/design/_checkList.php',[
                        'uiTemplateInfo' => $uiTemplateInfo,
                        'id' => $id,
                        'entryListId' => $entryListId,
                        'dataProvider'=>$dataProvider,
                        'program' => $program,
                        'processId' => $processId,
                        'selectedItems' => $selectedItems,
                        'configData' => $configData,
                        'totalEntries' => $totalEntries
                    ]);
                } else if($uiTemplate == 'testList'){
                    echo '<div class="row" style="margin-top:8px;margin-bottom:0px;padding-left:0px;"><span class="new badge">NOTE</span>&nbsp;&nbsp; The changes will be save upon clicking the Generate Design button.
                      <div class="pull-right"><h6>Grouped entries: <b><span id="no-entries-div"><span id="no-entries-span"></span>'.\Yii::t('app', ' / '.$totalEntries).'</b></span></h6></div></div>';
                    echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/design/_testList.php',[
                        'uiTemplateInfo' => $uiTemplateInfo,
                        'id' => $id,
                        'entryListId' => $entryListId,
                        'gridClass' => $gridClass,
                        'dataProvider'=>$dataProvider,
                        'filters' => $filters,
                        'program' => $program,
                        'processId' => $processId,
                        'selectedItems' => $selectedItems,
                        'totalEntries' => $totalEntries
                    ]);
                }
            ?>
        </div>
        <form method="POST" id="experiment-design-form" accept-charset="UTF-8" role="form" class="form-horizontal re-form form-inv">
        <div class="col col-sm-6" style="margin-top:10px;">
          <div id="experiment_design_fields">
                <?php 
                    if($experimentDesign != ''){
                      
                        if($uploaded){
                          
                            $uploadParams = '<div class="card-panel"><div class="row"> <h5 class="card-header">Summary Information</h5><table style="margin-left:15px;table-layout: fixed; width: 100%" id="uploadSummary">';
                            foreach($designFields as $field){
                                if(isset($field['label'])){
                                    $uploadParams .= '<tr><td><label>'.$field['label'].'</label></td><td class="bold"><div style="word-wrap:break-word;">'.$field['value'].'</div></td></td>';
                                }
                            }
                            echo $uploadParams.'</table><div class="clearfix"></div></div></div>';
                        } else if($design != 'Unspecified'){
                            echo '<div class="container-design">';
                          
                            echo '<div class="row" style="margin: 10px;"><h6>Experimental Design Parameters</h6></div>';
                            foreach($designFields as $field){
                                if($field['abbrev']=='PERCENT_CHECK_PLOTS' && !$withChecks){
                                    continue;
                                }
                                else if(isset($field['value'])) { 
                                    echo $field['value'];
                                }
                            }
                            echo '</div>';
                        }
                    }
                ?>
            </div>
    </div>


    <div class="col col-md-6" style="margin-top:10px;">
        <div id="layout_design_fields" class="<?= ($showLayout)? "" :"hidden";?>">
            <div class="container-design">
            <div class="row" style="margin: 10px;"><h6>Field Layout Details</h6></div>
              <?php
                  foreach($layoutFields as $field){
                      if($field['abbrev']=='PERCENT_CHECK_PLOTS' && !$withChecks){
                          continue;
                      }
                      else if(isset($field['value'])) { 
                          echo $field['value'];
                      }
                  }
              ?>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="col pull-right" style="margin-top:5px;">
        <div class="row switch" title="Switch design user interface" >
            <label>New interface&nbsp;
                <input type="checkbox" id="designui-switch-id" class="boolean-input-validate " name="designui-switch-name" value="" <?=$designUiSwitch != 'new' ? "checked":""?> >
                <span class="lever "></span>Old interface&nbsp;
            </label>
        </div>
    </div>
    <!--- start results area -->
    <div class="col col-md-6" id="layout-panel-div">
      
    </div>
    <!--- end results area -->
    
    </div>
                

<?php
Modal::begin([
    'id' => 'design-recently-used-tools-modal'
]);
Modal::end();

//Modal for delete plots
Modal::begin([
  'id' => 'plot-records-confirm-modal',
  'header' => '<h4><div id="plot-records-confirm-header"><i class="left material-icons orange-text">warning</i>Delete Plots</h4>',
  'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
      '<a id="plots_delete_btn" class="btn btn-primary waves-effect waves-light confirm-delete-plots-btn">Confirm</a>',
  'size' =>'modal-lg',
  'options' => ['data-backdrop'=>'static'],
]);
echo '<div id="plot-records-modal-confirm"></div>';
Modal::end();

// upload design array modal
Modal::begin([
  'id' => 'upload-design-array-modal',
  'header' => '<h4>
          <i class="material-icons" style="vertical-align:bottom">file_upload</i>
          Upload design array
      </h4>',
  'footer' =>
  Html::a('Cancel', [''], [
      'id' => 'cancel-upload-design-array',
      'class' => 'modal-close-button',
      'title' => \Yii::t('app', 'Cancel'),
      'data-dismiss' => 'modal'
  ]) . '&emsp;&nbsp;'
      . Button::widget([
          'label' => 'Confirm',
          'icon' => [
              'name' => 'send',
              'position' => 'right'
          ],
          'options' => [
              'class' => 'confirm-upload-design-array hidden',
              'style' => 'margin-right: 34px;',
              'href' => '#!',
              'title' => \Yii::t('app', 'Confirm upload design array')
          ],
      ])
      . Button::widget([
          'label' => 'Next',
          'icon' => [
              'name' => 'arrow_forward',
              'position' => 'right'
          ],
          'options' => [
              'class' => 'next-upload-design-array hidden',
              'style' => 'margin-right: 34px;',
              'href' => '#!',
              'title' => \Yii::t('app', 'Go to preview')
          ],
      ]). Button::widget([
          'label' => 'Validate',
          'icon' => [
              'name' => 'arrow_forward',
              'position' => 'right'
          ],
          'options' => [
              'class' => 'validate-upload-design-array hidden',
              'style' => 'margin-right: 34px;',
              'href' => '#!',
              'title' => \Yii::t('app', 'Validate and go to preview')
          ],
      ]),
  'closeButton' => [
      'class' => 'hidden'
  ],
  'size' => 'modal-lg',
  'options' => [
      'data-backdrop' => 'static',
      'class' => 'no-right-margin'
  ],
]);
echo '<div class="upload-design-array-form-modal-body parent-panel"></div>';
echo '<div class="upload-design-array-preview-modal-body child-panel"></div>';
Modal::end();

$currentUrl = Url::to(['/experimentCreation/create/specify-design', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$saveDesignUrl = Url::to(['/experimentCreation/create/change-design', 'id'=>$id, 'program'=>$program]);
$nextUrl = Url::to(['create/specify-experiment-groups', 'id'=>$id, 'program'=>$program]);
$generateJsonUrl = Url::to(['create/generate-json-input-analytics', 'id'=>$id, 'program'=>$program]);
$checkJsonUrl = Url::to(['create/check-design-results', 'id'=>$id, 'program'=>$program]);
$deleteJsonUrl = Url::to(['create/delete-plots', 'id'=>$id, 'program'=>$program]);
$downloadUrl = Url::to(['create/download-results']);
$getContentUrl = Url::to(['create/get-contents']);
$checkJobUrl = Url::to(['create/check-download-results']);
$removeTransactionUrl = Url::to(['create/remove-transaction']);
$getTransactionsUrl = Url::to(['create/get-transactions']);
$status = $model['experimentStatus'];
$renderFormUploadDesignArray = Url::to(['upload/render-file-upload-form','id'=>$id, 'program'=>$program]);
$uiTemplateStr = json_encode($uiTemplateInfo);
$saveEnvDesignUrl = Url::to(['/experimentCreation/design/save-env-value', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$updateDesignUiUrl = Url::to(['/experimentCreation/design/update-design-ui', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$this->registerJsFile('https://code.highcharts.com/stock/highstock.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://code.highcharts.com/modules/heatmap.js',['position' => \yii\web\View::POS_END]);

$this->registerJsFile("@web/js/generic-form-validation.js", [
  'depends' => ['app\assets\AppAsset'],
  'position'=>\yii\web\View::POS_END
]);
$this->registerJsFile("@web/js/parser/token.js", [
  'depends' => ['app\assets\AppAsset'],
  'position' => \yii\web\View::POS_END
]);

$this->registerJs(<<<JS

    var entryInfo = JSON.parse('$entryInfo');
    var totalEntries = parseInt(entryInfo['total']);
    var nEntries = parseInt(entryInfo['test']);
    var nTest = parseInt(entryInfo['test']);
    var nCheck = parseInt(entryInfo['check']);
    var design = ($("#design_select_id option:selected").html()).toLowerCase();
    var deletePlots = '$deletePlots';
    var updateDesignFromEntry = '$updateDesignFromEntry';
    var generateLayoutShow = '$generateLayoutShow';
    var paramValues = JSON.parse('$paramValues');
    var uploadedVar = '$uploaded';

    //add entry information in the params array for easy accessing
    paramValues['totalEntries']=totalEntries;
    paramValues['nEntries']=nEntries;
    paramValues['nCheck']=nCheck;
    
    var rules = JSON.parse('$rules');
    var validationRules = rules['validation-design'];
    var defaultRules = rules['default-value'];
    var allowedValRules = rules['allowed-value'];

    var row = 0;
    var col = 0;
    var totalPlots = 0;

    if(uiGroups === undefined){
        var uiGroups = [];
    }
    if(design == 'p-rep'){
        computeTotalPlots(0);
    }

    var changeFlag = 0;
    var errorArray = [];
    var errorArrayField = [];
    var warningArray = [];
    var flag = 0;
    var initflag = 0;

    var status = '$status';
    var planStatus = '$planStatus';
    var panelWidth = $('#layout-panel-div').width() + 10;
    var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
    
    var errorFlag = 0;
    var changeFlag = 0;

    checkResults();
    setInterval(function(){
        checkResults();
    }, 3000);

    function checkResults(){
        if(status.includes('design requested') || planStatus == 'deletion in progress' || planStatus == 'in progress'){
            
            $.ajax({
                url: '$checkJsonUrl',
                type: 'post',
                dataType: 'json',
                data: {
                  panelWidth:panelWidth
                },
                success: function(response) {
                    status = response.status;
                    deletePlots = response.deletePlots;
                    planStatus = response.planStatus;
                    var message = response.message;
                    if(response.success == 'deletion in progress'){
                        enableFields(false);
                        $("#no_action_btn").attr('disabled', 'disabled');
                        $("#submit_design_btn").attr('disabled', 'disabled');
                        $("#upload_design_array").attr('disabled', 'disabled');
                        $('#layout-panel-div').html(response.htmlData);
                        $("#submit_design_btn").html('Generate Design');
                    }else if(response.success == 'delete successful'){
                        enableFields(true);
                        $('#layout-panel-div').html('');
                        var notif = "<i class='material-icons green-text left'>check</i>"+message;
                        Materialize.toast(notif,5000);
                        $("#no_action_btn").attr('disabled', 'disabled');
                        $("#submit_design_btn").removeAttr('disabled');
                        $("#upload_design_array").removeAttr('disabled');
                        $('#layout-panel-div').html('');
                        $("#submit_design_btn").html('Generate Design');
                        location.reload();
                    }else if(response.success == 'deletion failed'){
                        enableFields(true);
                        $("#no_action_btn").attr('disabled', 'disabled');
                        $("#submit_design_btn").removeAttr('disabled');
                        $("#upload_design_array").removeAttr('disabled');
                        $('#layout-panel-div').html('');
                        var notif = "<i class='material-icons red-text left'>close</i>"+message;
                        Materialize.toast(notif,5000);
                        $("#submit_design_btn").html('Generate Design');
                    } else if(response.success != 'failed'){
                        enableFields(false);
                        if(response.success == 'success'){
                            enableFields(true);
                            $("#submit_design_btn").removeAttr('disabled');
                            $("#upload_design_array").removeAttr('disabled');
                            $("#no_action_btn").removeAttr('disabled');
                            $("#submit_design_btn").html('Delete Design');
                        } else if(status.includes('design requested')){
                            $("#submit_design_btn").attr('disabled', 'disabled');
                            $("#upload_design_array").attr('disabled', 'disabled');
                        }
                        $('#layout-panel-div').html(response.htmlData);
                        
                    } else {
                        enableFields(true);
                        $('#layout-panel-div').html('');
                        var notif = "<i class='material-icons red-text left'>close</i> Randomization failed. Please try again.";
                        Materialize.toast(notif,5000);
                        $("#no_action_btn").attr('disabled', 'disabled');
                        $("#submit_design_btn").removeAttr('disabled');
                        $("#upload_design_array").removeAttr('disabled');
                        $("#submit_design_btn").html('Generate Design');

                        if(planStatus == 'upload failed' && uploadedVar == true){
                            $("#design_select_id").trigger('change');
                        }
                    }
                },
                error: function(e) {
                    console.log(e);
                }
            });
        }
    }
    $("document").ready(function(){
        
        if(updateDesignFromEntry == 'updatedEntryList'){
            $("#plots_delete_btn").trigger('click');
        }

        if(planStatus == 'draft' && uploadedVar == true){
            $("#design_select_id").trigger('change');
        }

        validateDesignRules("");
        placeDefault();

        if(status.includes('design generated')){
            previewLayout();
            $("#submit_design_btn").html('Delete Design');
            $("#acrossEnvDesign_id").attr('disabled', 'disabled');
            $("#designui-switch-id").attr('disabled', 'disabled');
        } else {
            $("#submit_design_btn").html('Generate Design');
        }

        if(status.includes('design requested')){
            enableFields(false);
            $("#submit_design_btn").attr('disabled', 'disabled');
            $("#upload_design_array").attr('disabled', 'disabled');
            $("#acrossEnvDesign_id").attr('disabled', 'disabled');
            $("#designui-switch-id").attr('disabled', 'disabled');
        }

        if(planStatus.includes('deletion in progress')){
            enableFields(false);
            $("#submit_design_btn").html('Generate Design');
            $("#no_action_btn").attr('disabled', 'disabled');
            $("#submit_design_btn").attr('disabled', 'disabled');
            $("#upload_design_array").attr('disabled', 'disabled');
            $("#acrossEnvDesign_id").attr('disabled', 'disabled');
            $("#designui-switch-id").attr('disabled', 'disabled');
        }
        
        // check if there are empty fields
        $(".numeric-input-validate").each(function(){
            
            if(this.value == '' || this.value == undefined){
                flag = 1;
            }
        });
        checkFields();
        if(flag == 0 && initflag == 0){
            $("#submit_design_btn").removeAttr('disabled');
        } else {
            $("#submit_design_btn").attr('disabled', 'disabled');
        }
        if(generateLayoutShow == 'true'){
            $('[name="ExperimentDesign[genLayout]').attr('checked', 'checked')
            $("#layout_design_fields").removeClass('hidden');
            checkFields();
        }
        if($('[name="ExperimentDesign[genLayout]').prop('checked') == true){
            if($('[name="ExperimentDesign[genLayout]"').prop('checked') == true){
                $("#layout_design_fields").removeClass('hidden');
            } else{
                $("#layout_design_fields").addClass('hidden');
            }
        }

        if(generateLayoutShow == 'false'){
            $("#layout_design_fields").addClass('hidden');
            checkFields();
        }

        //design needs to be regenerated due to entry list changes
        if((status == 'entry list created' || status == 'entry list incomplete seed sources') && (planStatus.includes('randomized') || planStatus.includes('uploaded'))){
            previewLayout();
            enableFields(false);
            $("#no_action_btn").attr('disabled', 'disabled');
            $("#design_select_id").attr('disabled', 'disabled');
            $('#mod-notif').html('<div class="alert-danger" style="padding: 10px;">There are changes in the entry list. Please randomize again.</div>');
            $('#mod-notif').removeClass('hidden');
            changeFlag = 1;
        }

        if(planStatus.includes('randomization failed')){
            enableFields(true);
            $("#no_action_btn").attr('disabled', 'disabled');
            $("#submit_design_btn").removeAttr('disabled');
            $("#upload_design_array").removeAttr('disabled');
            $("#submit_design_btn").html('<i class="material-icons right">play_arrow</i> Generate Design');

            $("#acrossEnvDesign_id").removeAttr('disabled');
            $("#designui-switch-id").removeAttr('disabled');
        }

        if(design == 'unspecified'){
            $("#submit_design_btn").html('Delete Design');
            $("#layout_design_fields").addClass('hidden');

            if(planStatus.includes('uploaded')){
                $("#submit_design_btn").removeAttr('disabled');
                $("#acrossEnvDesign_id").attr('disabled', 'disabled');
                $("#designui-switch-id").attr('disabled', 'disabled');
            } else {
                $("#submit_design_btn").attr('disabled','disabled');
            }
        }

        if(deletePlots == 'true'){
            $("#submit_design_btn").html('Delete Design');
            $("#submit_design_btn").removeAttr('disabled');
            $("#upload_design_array").attr('disabled','disabled');
            $("#acrossEnvDesign_id").attr('disabled', 'disabled');
            $("#designui-switch-id").attr('disabled', 'disabled');
        }
    });

    // Access Upload design array
    $('#upload_design_array').on('click', function (e) {

        // trigger action ongoing
        e.preventDefault();
        e.stopImmediatePropagation();

        $('#mod-notif').html('');
        $('#mod-notif').addClass('hidden');
        if(deletePlots != 'true'){
            $('#upload-design-array-modal').modal('show')
            var modalBody = '.upload-design-array-form-modal-body';
            $(modalBody).html(loading);

            $.ajax({
                url: '$renderFormUploadDesignArray',
                type: 'post',
                dataType: 'json',
                data:{
                    designInfo:'$designInfo'
                },
                success: function(response){
                    $(modalBody).html(response);
                },
                error: function(){
                    var errorMessage = '<i>There was a problem loading the content.</i>';
                    $(modalBody).html(errorMessage);
                }
            });
        } else {
            $('#plot-records-confirm-modal').modal();
            $('#plot-records-modal-confirm').html('Plot records are already populated for this experiment. Click confirm to delete the records before submitting another request.');
        }
    });

    $("#genLayout").on('change', function (e, params) {
      
        if($('[name="ExperimentDesign[genLayout]"').prop('checked') == true){
            if(design == 'p-rep'){
                var totalEntryIds = 0;
                totalPlots = 0;
                for(var i=0; i<uiGroups.length; i++){
                    totalEntryIds = totalEntryIds + uiGroups[i]['entryIds'].length;
                    totalPlots = (uiGroups[i]['entryIds'].length *  uiGroups[i]['repno']) + totalPlots;
                }
                paramValues['totalPlots']=totalPlots;

                if(totalEntryIds != totalEntries){
                    $('#mod-notif').html('<div class="alert-danger" style="padding: 10px;">Please specify the entry groups and assign all entries in the groups.</div>');
                    $('#mod-notif').removeClass('hidden');
                } else{
                    $('#mod-notif').html('');
                    $('#mod-notif').addClass('hidden');
                }
            }
            
            $("#layout_design_fields").removeClass('hidden');
            paramValues['genLayout'] = true;

        } else{
            $("#layout_design_fields").addClass('hidden');
            paramValues['genLayout'] = false; 
        }
        checkFields();
    });

    function previewLayout(){
        $.ajax({
            url: '$checkJsonUrl',
            type: 'post',
            dataType: 'json',
            data: {
              panelWidth:panelWidth
            },
            success: function(response) {
                status = response.status;
                deletePlots = response.deletePlots;
                if(response.success != 'failed'){
                    enableFields(false);
                    if(response.success == 'success'){
                        enableFields(true);
                        $("#submit_design_btn").removeAttr('disabled');
                        if(changeFlag != 1){
                            $("#no_action_btn").removeAttr('disabled');
                        } else {
                            $("#no_action_btn").attr('disabled', 'disabled');
                        }
                    }
                    $('#layout-panel-div').html(response.htmlData);
                } else {
                    enableFields(true);
                    $('#layout-panel-div').html('');
                    var notif = "<i class='material-icons red-text left'>close</i> Randomization failed. Please try again.";
                    Materialize.toast(notif,5000);
                    $("#no_action_btn").attr('disabled', 'disabled');
                    $("#submit_design_btn").removeAttr('disabled');
                }
            },
            error: function() {
            }
        });
    }

    function computeTotalPlots(nRep){
        if(design == 'p-rep'){
            totalPlots = 0;
            for(var i=0; i<uiGroups.length; i++){
                totalPlots = (uiGroups[i]['entryIds'].length *  uiGroups[i]['repno']) + totalPlots;
            }
        } else {
            if(nRep == 0){
                nRep = $('#nRep').val();
            } 
            if(design.includes('augmented') == false){
                totalPlots = totalEntries * nRep;
            } else {
                totalPlots = (nCheck * nRep) + nEntries;
            }
        }
        paramValues['totalPlots']=totalPlots;
    }

    $("#nRep").on('change', function (e, params) {
        computeTotalPlots(this.value);
        paramValues['nRep']=parseInt(this.value);
        validateDesignRules('nRep');
    });

    function flashMessage(msg, className){
        var msgDef = (msg==undefined || msg=='') ? 'Please specify valid values.':msg;
        if(className == undefined || className == ''){
            var classText = 'red-text';
        } else {
            var classArray = {
              'error':'red-text',
              'warning':'orange-text'
            };
            var classText = classArray[className];
        }
        $('#notif_field').html('<br/><span class="'+classText+' text-darken-2">'+msgDef+'</span>');
        $('#notif_field').removeClass('hidden');
    }

    $(".randomization").change(function() {
        //check values
        if (parseFloat(this.value) < parseFloat(this.min)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Minimum value is ' + this.min;
            className = 'error';
            errorArrayField.push(this.id);
            flashMessage(message, className);
        } else if (parseFloat(this.value) > parseFloat(this.max)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Maximum value is ' + this.max;
            className = 'error';
            errorArrayField.push(this.id);
            flashMessage(message, className);
        } else{
            var idx = this.id;
            var idx = errorArrayField.indexOf(this.id);
            errorArrayField.splice(idx, 1);
            $('#'+this.id).removeClass('invalid');

            if(Object.keys(errorArrayField).length === 0){
                $('#notif_field').addClass('hidden');
            }

            paramValues[this.id] = this.value; 
        }
        checkFields();
    });

    $(".layout").change(function() {
        if (parseFloat(this.value) < parseFloat(this.min)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Minimum value is ' + this.min;
            className = 'error';
            errorArrayField.push(this.id);
            flashMessage(message, className);
        } else if (parseFloat(this.value) > parseFloat(this.max)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Maximum value is ' + this.max;
            className = 'error';
            errorArrayField.push(this.id);
            flashMessage(message, className);
        } else{
            var idx = this.id;
            var idx = errorArrayField.indexOf(this.id);
            errorArrayField.splice(idx, 1);
            $('#'+this.id).removeClass('invalid');

            if(Object.keys(errorArrayField).length === 0){
                $('#notif_field').addClass('hidden');
            }

            paramValues[this.id] = this.value; 
        }
        checkFields();
    });

    function computeTokens(expression, options){
     
        var evaluatedArray = evaluateExpression(expression);
        var tokens = evaluatedArray;
        var passValidation = 1;

        if(tokens.length == 1){ //function
            if(tokens[0]['value'].includes('factor')){
                var options = eval(tokens[0]['value']);
                return options;
            }
        } else if(tokens.length == 3 && tokens[0]['value'] == 'value' && Object.keys(options).length == 0){
            //generate list of values
            if(tokens[1]['value'] == '<='){
                var options = [ ...Array(tokens[2]['value']).keys() ].map( i => i+1);
                return options;
            } else if(tokens[1]['value'] == '<'){
                var options = [ ...Array((tokens[2]['value']-1)).keys() ].map( i => i+1);
                return options;
            } 
        } else if(tokens.length == 3 && tokens[0]['value'] == 'value' && Object.keys(options).length > 0){
            var newOptions = [];

            var var2 = tokens[2]['value'];
            if(tokens[2]['type'] == 'VAR'){
                var2 = checkVarValue(var2);
            }
            for(var i=0; i<options.length; i++){
                var evaluateStr = options[i]+tokens[1]['value']+var2;
                if(eval(evaluateStr)){
                    newOptions.push(options[i]);
                }
            }
            return newOptions;
        } else if(tokens.length == 3 && tokens[0]['value'].includes('factor')){
            var options = eval(tokens[0]['value']);
            var newOptions = [];
            
            var var2 = tokens[2]['value'];
            if(tokens[2]['type'] == 'VAR'){
                var2 = checkVarValue(var2);
            }
            
            for(var i=0; i<options.length; i++){

                var evaluateStr = options[i]+tokens[1]['value']+var2;
                newOptions.push(eval(evaluateStr));
            }
            return newOptions;
        }
        
    }

    function computeFormula(fieldId){
        key = fieldId;
        
        var options = [];
        for(var i=0; i<allowedValRules[key].length; i++){
            var expression = allowedValRules[key][i]['expression'];
            
            if(expression.includes('if(')){
                var proceedArr = checkProceed(expression);

                expression = proceedArr['expression'];
                if(!proceedArr['proceed']){
                    continue;
                }
            }
            options = computeTokens(expression, options);
        }
        if(options!= null && options != undefined && Object.keys(options).length > 0){
            options.sort((a,b)=>a-b);
            addDropDownOptions(fieldId, options);
        }
    }

    function checkVarValue(fieldName){

        var fieldValue = '';
        if(!paramValues.hasOwnProperty(fieldName)){
            fieldValue = eval(fieldName);
        } else {
            fieldValue = eval(paramValues[fieldName]);
        }

        return fieldValue;
    }

    function extractExpression(tokens){
        var condFlag='';
        var mainExpression = '';

        for(var i=0; i<tokens.length; i++){
            //evaluate from left to right
            if(tokens[i]['value'] != undefined && tokens[i]['value'] == 'if'){
                //evaluate first condition string
                var condArray = [];
                var idx = i+2;
                
                while(tokens[idx]['value'] != undefined && tokens[idx]['value'] != ')'){
                    condArray.push(tokens[idx]);
                    idx++;
                }
                
                if(condArray.length == 1 && condArray[0]['type'] == 'VAR'){ //assume variable value
                    var fieldName = condArray[0]['value'];
                    if(fieldName.includes('!')){
                        fieldName = fieldName.replace('!', '');
                        var tempStr = checkVarValue(fieldName);
                        condFlag = eval('!'+tempStr);
                    } else {
                        condFlag = eval(checkVarValue(fieldName));
                    }

                } else { //assume simple expression or variable value
                    //check if variable
                    var var1 = condArray[0]['type'] == 'VAR' ? checkVarValue(condArray[0]['value']) : parseInt(condArray[0]['value']);
                    var var2 = condArray[2]['type'] == 'VAR' ? checkVarValue(condArray[2]['value']) : parseInt(condArray[2]['value']);
                    condFlag = eval(var1 + condArray[1]['value'] + var2);
                }
                break;
            }
        }

        //cleanup expression
        for(var i=(idx+2); i<(tokens.length-1);i++){
            mainExpression += tokens[i]['value'];
        }
        return {
            'expression': mainExpression,
            'proceed': condFlag
        }
    }


    function searchGroupings(tokens){
        var LParent = 0;
        var RParent = 0;
        var expressionValue = '';

        var newTokens = [];

        //search last left parenthesis
        for(var i=0; i < tokens.length; i++){
            if(tokens[i]['value'] == "("){
                LParent = i;
            }
        }

        //search partner of left parenthesis
        var expressionArray = [];
        for(var i=LParent+1; i < tokens.length; i++){
            if(tokens[i]['value'] == ")"){
                RParent = i;
                break;
            } else {
                expressionArray.push(tokens[i]);
            }
        }

        if(LParent != 0 && RParent != 0){
          
            //evaluate expressionArray
            if(expressionArray.length == 1 && expressionArray[0]['type'] == 'VAR'){ //assume variable value
                var fieldName = expressionArray[0]['value'];
                expressionValue = checkVarValue(fieldName);
            } else if(expressionArray.length > 1){ //assume simple expression
                //check if variable
                var var1 = expressionArray[0]['type'] == 'VAR' ? checkVarValue(expressionArray[0]['value']) : parseInt(expressionArray[0]['value']);
                var var2 = expressionArray[2]['type'] == 'VAR' ? checkVarValue(expressionArray[2]['value']) : parseInt(expressionArray[2]['value']);

                expressionValue = eval(var1 + expressionArray[1]['value'] + var2);
            }

            if(expressionValue != ''){
                //rewrite tokens
                for(var i=0; i < tokens.length; i++){
                    if(i == LParent){
                        if(tokens[i-1]['type'] != undefined && tokens[i-1]['type'] != 'FXN'){
                            newTokens.push({ type:'NUM', value:expressionValue });
                        } else if(tokens[i-1]['type'] == 'FXN'){
                            newTokens.pop(); //removes fxn
                            newTokens.push({ type: 'FXN', value: tokens[i-1]['value']+'('+expressionValue+')'});
                        }
                        i = RParent;
                    } else {
                        newTokens.push(tokens[i]);
                    }
                }
            } else {
                LParent = 0; RParent = 0; newTokens = tokens;
            }
        } else {
            newTokens = tokens;
        }

        return {
            'LParent':LParent,
            'RParent':RParent,
            'tokens' : newTokens
        }
    }

    function checkProceed(expression){ //check 1st for if condition

        var tokens = tokenize(expression);
        var proceed = true;

        if(expression.includes("if(")){
            var condArray  = extractExpression(tokens);
            proceed = condArray['proceed'];
            tokens = condArray['expression'];
        }

        return {
            'proceed' : proceed,
            'expression' : tokens
        };
    }

    function evaluateExpression(expression){

        var tokens = tokenize(expression);
        
        //simplify expression with functions
        var simplifiedTokens = [];
        for(var i=0; i < tokens.length; i++){
            if(tokens[i]['type'] == 'FXN'){
                if((tokens[i+2]['type'] == 'NUM') && tokens[i+3]['type'] == 'RPAREN'){
                    simplifiedTokens.push({type:'FXN', value: tokens[i]['value']+'('+tokens[i+2]['value']+')'});
                    i = i+3;
                    continue;
                }else if((tokens[i+2]['type'] == 'VAR') && tokens[i+3]['type'] == 'RPAREN'){
                    var var1 = checkVarValue(tokens[i+2]['value']);
                    
                    simplifiedTokens.push({type:'FXN', value: tokens[i]['value']+'('+var1+')'});
                    i = i+3;
                    continue;
                } else {
                    simplifiedTokens.push(tokens[i]);
                }
            } else {
                simplifiedTokens.push(tokens[i]);
            }
        }

        tokens = simplifiedTokens;
        
        var LParent = 1;
        var RParent = 1;
        var index = 0;
        while ((LParent != 0 && RParent != 0) && index != 30){
            var updatedArray = searchGroupings(tokens);
            LParent = updatedArray['LParent'];
            RParent = updatedArray['RParent'];
            tokens = updatedArray['tokens'];
            index++;
        }

        return tokens;
    }

    function removeDropDownOptions(design, dropDownName){
        $('#' + dropDownName).empty();
    }

    function checkFields(){
        var checkflag = 0;
        flag = 0;
        $(".randomization").each(function(){
          
            var element = document.getElementById(this.id);

            if(element != null && element.className.includes('boolean-input-validate')){
              
                var isChecked = $(this).is(":checked");

                var tId = this.name;
                if(isChecked === false){
                    
                    $('#'+this.id).attr('value', false);
                    
                } else {

                    $('#'+this.id).attr('value', true);
                }
            } else if(this.id !== ""){
                if(this.value == '' || this.value == undefined || this.value == 'Select a value'){
                    checkflag = 1;
                } 
            }
            
        });

        if($('[name="ExperimentDesign[genLayout]').prop('checked') == true || generateLayoutShow == 'true'){
            $(".layout").each(function(){
                if(this.id != ''){
                    if(this.value == '' || this.value == undefined  || this.value == 'Select a value'){
                        checkflag = 1;
                    }
                }
            });
        } else {
            $(".layout").each(function(){
              
                var element = document.getElementById(this.id);

                if(element != null && element.tagName === 'SELECT' && this.value == ''){
                    $("#"+(this.id)+" option:first").attr("selected", true);
                    $("#"+(this.id)+"-hidden").val(0);
                    $("#select2-"+(this.id)+"-container").attr('title', 'Select a value');
                    $("#select2-"+(this.id)+"-container").html = 'Select a value';
                    var span = document.getElementById("select2-"+(this.id)+"-container"); 
                  
                    if(span != null && this.value == ''){
                        span.textContent = 'Select a value';
                    }
                } else if(element != null && element.className.includes('boolean-input-validate')){
                  
                  var isChecked = $(this).is(":checked");

                  var tId = this.name;
                  if(isChecked === false){
                      
                      $('#'+this.id).attr('value', false);
                      
                  } else {

                      $('#'+this.id).attr('value', true);
                  }
                } else if(element != null){

                }

            });
        }
        validateDesignRules("");
        if(Object.keys(errorArray).length > 0){
            var msg = '';
            for(let key in errorArray){
                if(errorArray[key] != ""){
                    msg += errorArray[key];
                    flag = 1;
                }
            }
        }

        if(Object.keys(errorArrayField).length > 0){
            flag = 1;
        }

        if(design == 'p-rep'){
            var totalEntryIds = 0;
            totalPlots = 0;
            var checkGrps = 0;
            var testGrps = 0;
            var testReps = [];
            var testRepsCheck = 0;
            for(var i=0; i<uiGroups.length; i++){
                totalEntryIds = totalEntryIds + uiGroups[i]['entryIds'].length;
                totalPlots = (uiGroups[i]['entryIds'].length *  uiGroups[i]['repno']) + totalPlots;

                if(uiGroups[i]['type'] == 'check'){
                    checkGrps++;
                    if(parseInt(uiGroups[i]['repno']) < 2){
                        checkflag = 1;
                    }
                } else {
                    if(testReps.includes(parseInt(uiGroups[i]['repno'])) == false){
                        testReps.push(parseInt(uiGroups[i]['repno']));
                        testGrps++;
                    }

                    if(parseInt(uiGroups[i]['repno']) >= 2){
                        testRepsCheck += uiGroups[i]['entryIds'].length;
                    }

                }
            }

            if(checkGrps < 1 || testGrps < 1){
                checkflag = 1;
            }
           
            paramValues['totalPlots']=totalPlots;
            //Add condition for upload
            if((totalEntryIds != totalEntries || checkflag == 1) && uploadedVar != true){
                $('#mod-notif').html('<div class="alert-danger" style="padding: 10px;">Please specify the entry groups <em>(at least 1 check group with min value of 2 and 1 test group)</em> and assign all entries in the groups.</div>');
                $('#mod-notif').removeClass('hidden');
                checkflag = 1;
            } else{
                $('#mod-notif').html('');
                $('#mod-notif').addClass('hidden');
            }

            // //check if 20% of test entries has more than 2 repno as values
            // var passingCnt = Math.ceil(nEntries * 0.2);
            // if((passingCnt > testRepsCheck) && checkflag != 1){
            //     $('#mod-notif').html('<div class="alert-danger" style="padding: 10px;">At least 20% of the test entries shoulde have at most 2 number of replicates.</div>');
            //     $('#mod-notif').removeClass('hidden');
            //     checkflag = 1;
            // }
        }

        if(flag == 0  && initflag == 0 && checkflag == 0){
            $("#submit_design_btn").removeAttr('disabled');
        } else {
            $("#submit_design_btn").attr('disabled', 'disabled');
        }

        if(checkflag == 1){
            flag = 1;
        }
    }

    function enableFields(enabled){
        if(enabled){
          
            $(".randomization").each(function(){
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).removeAttr('disabled');
                }
            });
            $(".layout").each(function(){
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).removeAttr('disabled');
                }
            });

            $("#experiment-design-form").find('input[type="checkbox"]').each( function () {
                $("#"+this.id).removeAttr('disabled');
            });
        } else {
            
            $(".randomization").each(function(){
              
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).attr('disabled', 'disabled');
                }
            });
            $(".layout").each(function(){
              
                if(this.id != '' && this.id != null && this.id != undefined){
                    $("#"+this.id).attr('disabled', 'disabled');
                }
            });

            $("#experiment-design-form").find('input[type="checkbox"]').each( function () {
                $("#"+this.id).attr('disabled', 'disabled');
            });
        }
    }

    $("#design_select_id").change(function() {
        var designText = $("#design_select_id option:selected").html();
        var designValue = this.value;
        var design =  designValue.split('|');

        $.ajax({
            type:"POST",
            dataType: 'json',
            url: '$saveDesignUrl',
            data: {
                designId:design[0],
                designInfo:'$designInfo'
            },
            success: function(data) {
                window.location = '$currentUrl';
            },
            error: function(){
                location.reload();
            }
        });      

    });

    $('[type="checkbox"]').click(function(e) {
        var isChecked = $(this).is(":checked");

        $(this).val(isChecked);

        paramValues[this.id] = isChecked; 
    });

    $("#plots_delete_btn").click(function() {
        $("#plots_delete_btn").addClass("disabled");
        var modalBody = 'plot-records-confirm-modal-body';
        $(modalBody).html(loading);
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: '$deleteJsonUrl',
            data: {},
            success: function(data) {
                if(data['success'] == true){
                    enableFields(false);
                    $("#no_action_btn").attr('disabled', 'disabled');
                    $("#submit_design_btn").attr('disabled', 'disabled');
                    $("#upload_design_array").attr('disabled', 'disabled');
                    $("#acrossEnvDesign_id").attr('disabled', 'disabled');
                    $("#designui-switch-id").attr('disabled', 'disabled');
                    $('#plot-records-confirm-modal').modal('hide');
                    var notif = "<i class='material-icons green-text left'>check</i> Deletion of plot records started.";
                    Materialize.toast(notif,5000);
                    planStatus = 'deletion in progress';
                } else {
                    enableFields(true);
                    $("#no_action_btn").attr('disabled', 'disabled');
                    $('#plot-records-confirm-modal').modal('hide');
                    var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                    Materialize.toast(notif,5000);
                }
                $("#plots_delete_btn").removeClass("disabled");
            },
            error: function(){

                $('#plot-records-confirm-modal').modal('hide');
                var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                Materialize.toast(notif,5000);
                $("#plots_delete_btn").removeClass("disabled");
            }
        });  
    });

    $("#submit_design_btn").click(function() {
        $('#mod-notif').html('');
        $('#mod-notif').addClass('hidden');
        if(deletePlots != 'true'){
            $("select[disabled]").each(function(){
                $("#"+this.id).removeAttr('disabled', 'disabled');
            });
            
            var action_url = '$generateJsonUrl';

            var form_data = $.param($.map($("#experiment-design-form").serializeArray(), function(v,i) {
                return (v.name == "add_variable_info_select") ? null : v;
            }));

            $("#experiment-design-form").find('input[type="checkbox"]').each( function () {
                var isChecked = $(this).is(":checked");
                
                var tId = this.name;
                if(isChecked === false){
                    
                    $('#'+this.id).attr('value', 'false');
                    
                } else {

                    $('#'+this.id).attr('value', 'true');
                }

                form_data = form_data + '&' + $.param({
                    [tId] : $(this).val()
                });
            });

            //add design
            var designSelId = $('#design_select_id').prop('name');
            form_data = form_data + '&' + $.param({
                [designSelId] : $('#design_select_id').val()
            });

            if(design == 'p-rep'){
                form_data = form_data + '&' + $.param({
                    ['uiGroups'] : uiGroups
                });

            }
            $('#layout-panel-div').html('<div class="alert alert-warning" style="padding: 10px;">Please wait while processing your request.</div>');
            $.ajax({
                type:"POST",
                dataType: 'json',
                url: action_url,
                data: form_data,
                success: function(data) {
                    if(data=='success'){
                        enableFields(false);
                        previewLayout();
                        $("#no_action_btn").attr('disabled', 'disabled');
                        $("#submit_design_btn").attr('disabled', 'disabled');
                        $("#upload_design_array").attr('disabled', 'disabled');
                        var notif = "<i class='material-icons green-text left'>check</i> Request successfully sent.";
                        $("#acrossEnvDesign_id").attr('disabled', 'disabled');
                        $("#designui-switch-id").attr('disabled', 'disabled');
                        Materialize.toast(notif,5000);
                    } else {
                        enableFields(true);
                        $('#layout-panel-div').html('');
                        $("#no_action_btn").attr('disabled', 'disabled');
                        var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                        $("#acrossEnvDesign_id").removeAttr('disabled');
                        $("#designui-switch-id").removeAttr('disabled');
                        Materialize.toast(notif,5000);
                    }
                },
                error: function(){
                    $('#layout-panel-div').html('');
                    var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                    Materialize.toast(notif,5000);
                }
            });      
            
        } else {
            $('#plot-records-confirm-modal').modal();
            $('#plot-records-modal-confirm').html('Plot records are already populated for this experiment. Click confirm to delete the records before submitting another request.');
        }
    });

    $("#check_results_btn").click(function() {
        var action_url = '$checkJsonUrl';
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: action_url,
            success: function(data) {
                location.reload();
            },
            error: function(){
                location.reload();
            }
        });      
    });

    $("#next-btn").click(function() {
        window.location = '$nextUrl';
    });

    $("#acrossEnvDesign_id").change(function() {
        var designText = $("#design_select_id option:selected").html();
        var designValue = this.value;

        var action_url = '$saveEnvDesignUrl';
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: action_url,
            data: {
                designValue: designValue
            },
            success: function(data) {
                location.reload();
            },
            error: function(){
                location.reload();
            }
        });   
    });

    $("#designui-switch-id").on('change', function (e, params) {
        var designUiSwitch = '';
        if($(this).prop('checked') == true){
            //check if there's created parameter set; if none, reset view
            //delete created draft parameter set
            //delete nReps, set to default additional fields
            designUiSwitch = 'old';
        } else{
            //check if there's created upload parameter set; if none, refresh view to show generate
            designUiSwitch = 'new';
        }
        $.ajax({
            url: '$updateDesignUiUrl',
            type: 'post',
            dataType: 'json',
            data: {
                designUiSwitch: designUiSwitch
            },
            success: function(response) {
                location.reload();
            },
            error: function() {

            }
        });
    });

    function getAllFactorsOfInteger(n) {
        var factors_array = [];
        for (var x = 1; x <= Math.sqrt(Math.abs(n)); x++) {
            if (n % x === 0) {
                var z = n / x;
                if (factors_array.indexOf(z) === -1) {
                    factors_array.push(z);
                }
                if (factors_array.indexOf(x) === -1) {
                    factors_array.push(x);
                }
            }
        }
        return factors_array;
    }
        
    function addDropDownOptions(dropDownName, valueArray) {

        var dropDown = document.getElementById(dropDownName);

        var dropDownHidden = document.getElementById(dropDownName+"-hidden");
        if(dropDownHidden != undefined){
          dropDown = dropDownHidden;
        }

        if(dropDown !== null){
          
            $('#' + dropDownName).empty();
            $('#' + dropDownName).append('<option hidden><i style="color:gray;">Select a value</i></option>');
            for (var fieldIndex in valueArray) { // then populatem them
                var flagValue = 0;
          
                if(dropDown != undefined && dropDown.min != "null" && parseInt(valueArray[fieldIndex]) < parseInt(dropDown.min)){
                    flagValue = 1;
                }
                if (dropDown != undefined && dropDown.max != "null" && parseInt(valueArray[fieldIndex]) > parseInt(dropDown.max)){
                    flagValue = 1;
                }
                if(flagValue == 0){
                    $('#' + dropDownName).append('<option value="' + valueArray[fieldIndex] + '">' + valueArray[fieldIndex] + '</option>');
                }
            }

            var reindex = valueArray.filter(val => val);
            if(reindex.length === 1){
                $('#'+dropDownName).val(valueArray[Object.keys(valueArray)[0]]);
                $('#'+dropDownName).trigger('change');
            }
        }
    }

    function createTags(data, dropDownName){
        $('#' + dropDownName).html();
        var selectStr = '<select id="DDAlertFreq" ></select>';
        $("#" + dropDownName).select2({
          createSearchChoice: function (term, data) {
            if ($(data).filter(function () {
              return this.text.localeCompare(term) === 0;
            }).length === 0) {
              return {
                id: term,
                text: term
              };
            }
          },
          data: data,
          placeholder: "Select value",
          allowClear:true
        });
    }

    function defaultTokens(expression){
        var evaluatedArray = evaluateExpression(expression);
        var tokens = evaluatedArray;
        
        var defaultVal = 0;

        if(tokens[0]['value'].includes('min')){
            if(tokens.length > 3){  //there's expression within
                if(tokens[2]['value'].includes('factor')){
                    var options = eval(tokens[2]['value']);

                    if(tokens[3]['value'] == ")"){  //get minimum value first
                        options.sort((a,b)=>a-b);
                        defaultVal = options[0];
                    }
                    if(tokens[4]['value'] != undefined && (tokens[4]['type'] == 'COMP' || tokens[4]['type'] == 'OPER')){ //evaluate
                        var var2 = tokens[5]['value'];
                        if(tokens[5]['type'] == 'VAR'){
                            var2 = checkVarValue(var2);
                        }
                        for(var i=0; i<options.length; i++){
                            var evaluateStr = options[i]+tokens[4]['value']+var2;
                            if(eval(evaluateStr)){
                                defaultVal = options[i];
                                break;
                            }
                        }
                    }

                    return defaultVal;
                }
            }
        }
    }

    function placeDefault(){
        for(let key in defaultRules){
            var defaultVal = 0;
            var value = null;
            if(paramValues[key] == null ){
                var value = paramValues[key];

                for(var i=0; i<defaultRules[key].length; i++){
                  
                    defaultVal = defaultTokens(defaultRules[key][i]['expression']);
                    
                    $('#'+key).val(defaultVal);
                    $('#'+key).trigger('change');
                }
            }
        }
    }

    function validateTokens(expression, value){
        var evaluatedArray = evaluateExpression(expression);
        var tokens = evaluatedArray;
        var passValidation = 1;
        if(tokens.length == 1){ //function
            if(tokens[0]['value'].includes('factor')){
                var options = eval(tokens[0]['value']);
                
                if(!options.includes(parseInt(value))){
                    passValidation = 0;
                }
                
            }else if(tokens[0]['value'].includes('prime')){
              
                passValidation = eval(tokens[0]['value']) ? 1 : 0;
            }
        } else {
            var tokenString = '';
            for(var i=0; i<tokens.length; i++){
                tokenString += tokens[i]['value'];
            }
            
            passValidation = eval(tokenString) ? 1 : 0;
        }
        return passValidation;
    }

    function validateDesignRules(fieldName){
        if(fieldName != ""){
            if(validationRules[fieldName] != undefined){
              
                key = fieldName;
                
                if(paramValues[key] != undefined && paramValues[key] != null ){

                    var value = paramValues[key];

                    warningArray[key] = '';
                    errorArray[key] = '';
                    for(var i=0; i<validationRules[key].length; i++){
                        var passValidation = validateTokens(validationRules[key][i]['expression'], value);

                        if(passValidation == 0){
                            if(validationRules[key][i]['action'] == 'warning'){
                                warningArray[key] += validationRules[key][i]['notification'];
                            } else if(validationRules[key][i]['action'] == 'error'){
                                if(errorArray[key] == undefined){
                                    errorArray[key] = '';
                                }
                                errorArray[key] += validationRules[key][i]['notification'] ;
                            }
                        }
                    }
                }
            }
        } else {
          
            for(let key in validationRules){
                var passValidation = 1;
                var value = null;
                if(paramValues[key] != undefined && paramValues[key] != null ){
                    var value = paramValues[key];
                }

                if(paramValues[key] != null || key == 'entryList'){

                    warningArray[key] = '';
                    errorArray[key] = '';
                    for(var i=0; i<validationRules[key].length; i++){
                      
                        var passValidation = validateTokens(validationRules[key][i]['expression'], value);
                        if(passValidation == 0){
                            if(validationRules[key][i]['action'] == 'warning'){
                                if(warningArray[key] == undefined){
                                    warningArray[key] = '';
                                }
                                warningArray[key] += validationRules[key][i]['notification'];
                            } else if(validationRules[key][i]['action'] == 'error'){
                                if(errorArray[key] == undefined){
                                    errorArray[key] = '';
                                }
                                errorArray[key] += validationRules[key][i]['notification'] ;
                            }
                        }
                    }
                }
            }
        }

        if(Object.keys(warningArray).length  > 0){
            showNotifications(warningArray, 'warning');
        } else {
            $('#warning-design-div-msg-panel').html('');
            $('#warning-design-div-panel').addClass('hidden');
        }
        if(Object.keys(errorArray).length > 0){
            for(let key in errorArray){
                if(errorArray[key] != ""){
                    flag = 1;
                } else continue;
            }
            if(flag == 1){
                $("#submit_design_btn").attr('disabled', 'disabled');
                showNotifications(errorArray, 'error');
            }
        } else{
            $('#error-design-div-msg-panel').html('');
            $('#error-design-div-panel').addClass('hidden');
        }
    }

    function showNotifications(msgArray, notifType){

        var msg = '';

        for(let key in msgArray){
            if(msgArray[key] != ""){
              
                msg += msgArray[key] + "<br>"
            }
        }
        
        if(notifType =='warning' && msg != ''){
            $('#warning-design-div-msg-panel').html(msg);
            $('#warning-design-div-panel').removeClass('hidden');
        } else if(notifType =='error' && msg != ''){
            $('#error-design-div-msg-panel').html(msg);
            $('#error-design-div-panel').removeClass('hidden');
        } else if(msg == ''){
            if(notifType=='warning'){
                $('#warning-design-div-msg-panel').html('');
                $('#warning-design-div-panel').addClass('hidden');
            } else {
                $('#error-design-div-msg-panel').html('');
                $('#error-design-div-panel').addClass('hidden');
            }
        }
    }

JS
);
?>
<style>
input.disabled {
    pointer-events: none;
    opacity: 0.5;
    cursor: not-allowed;
}
.readonly-select {
    background-color:#d5d5d5;
    opacity:0.5;
    border-radius:3px;
    cursor:not-allowed;
    position:absolute;
    top:0;
    bottom:0;
    right:0;
    left:0;
}
.fa-download:hover, .fa-download:active{
    color: #005580;
    cursor: pointer;
}
#notif_field{
    font-size: 14px;
    padding: 10px;
    height: 30px;
    line-height: 0px;
}
span.badge {
    margin-left: 14px;
    float: left;
}
.collapsible span.badge {
    margin-top: 0px;
}

.summary{
    display:none;
}
.container-design {
    /*background-color: white; */
    min-height: 300px;
    max-height: 600px;
    position: relative;
    overflow: auto;
    /* margin: 5px 0; */
    border: 1px solid #ccd5d8;
    border-radius: 10px;
}
.panel-error > .card-panel {
    background: #ffcdd2;
    color: #F44336;
}
.panel-warning > .card-panel {
    background: #d9edf7;
    color: #31708f;
}
#uploadSummary td,th {
    padding: 5px 0px !important;
    display: table-cell;
    text-align: left;
    vertical-align: middle;
    border-radius: 2px;
}
select#serpentinePrep option[value="CO"]   { background-image:url(../../../images/column-order.png);}
select#serpentinePrep option[value="CS"] { background-image:url(../../../images/column-order.png); }
select#serpentinePrep option[value="RS"] { background-image:url(../../../images/column-order.png); }
select#serpentinePrep option[value="R0"] { background-image:url(../../../images/column-order.png); }
</style>