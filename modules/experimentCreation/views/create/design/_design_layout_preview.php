<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\dropdown\DropdownX;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;


/**
* Renders preview layout of design
*/

$program = \Yii::$app->userprogram->get('id');
$prepareDataForCsvDownloadUrl = Url::to(['/occurrence/view/prepare-data-for-csv-download', 'program'=>$program,'entity'=>'plot', 'experimentId'=>$experimentId, 'tool'=>'experiment-creation']);
$exportPlotsUrl = $urlExport = Url::to(['/occurrence/view/export-plots', 'program'=>$program]);
$exportRecordsThresholdValue = \Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportRecords');
if(empty($data) && ($statusDesign == 'in progress' || $statusDesign == 'upload in progress')){
	  echo '<div class="alert alert-warning" style="padding: 10px;">Record generation is still on going.</div>';
} else if(empty($data) && $statusDesign == 'deletion in progress'){
    echo '<div class="alert alert-warning" style="padding: 10px;">Deletion of plot records is still on going.</div>';
}else if((empty($data['plotno']) || $data['plotno']== null) && $statusDesign == 'completed'){
    echo '<div class="alert alert-success" style="padding: 10px;">Experiment successfully randomized. Layout cannot be shown for this design configuration.</div><br>';

    $occurrenceParam = '&id=';
    if($plotCount >= $exportRecordsThresholdValue){
        $urlExport = $prepareDataForCsvDownloadUrl;
        $occurrenceParam = '&occurrenceId=';
    }

    $gridColumns = [
        [
            'class' => 'kartik\grid\ActionColumn',
            'header' => false,
            'noWrap' => true,
            'template' => '{download}',
            'buttons' => [
                'download' => function ($url, $data, $key) use ($urlExport, $occurrenceParam) {
                    return Html::a('<i class="material-icons inline-material-icon">file_download</i>',
                        '#',
                        [
                            'id' =>'export-'.$data['occurrenceDbId'],
                            'class' => 'export-occurrence',
                            'title' => Yii::t('app', 'Export Occurrence'),
                            'data-id' => $data['occurrenceDbId'],
                            'data-target' => '#',
                        ]
                    );
                }
            ]
        ],
        [
            'attribute' => 'occurrenceName',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'occurrenceCode',
            'label' => '<span title="Occurrence Code">'.Yii::t('app', 'Code').'</span>',
            'format' => 'raw',
            'encodeLabel'=>false,
            'enableSorting' => false
        ],
        [
            'attribute'=>'experimentStageCode',
            'label' => '<span title="Stage Code">'.Yii::t('app', 'Stage').'</span>',
            'format' => 'raw',
            'encodeLabel'=>false,
            'enableSorting' => false
        ],
        [
            'attribute'=>'experimentYear',
            'label' => '<span title="Experiment Year">'.Yii::t('app', 'Year').'</span>',
            'format' => 'raw',
            'encodeLabel'=>false,
            'enableSorting' => false
        ],
        [
            'attribute'=>'experimentSeason',
            'label' => '<span title="Experiment Season">'.Yii::t('app', 'Season').'</span>',
            'format' => 'raw',
            'encodeLabel'=>false,
            'enableSorting' => false
        ],
        [
            'attribute' => 'entryCount',
            'enableSorting' => false,
            'value' => function($data) use ($entryCount){
                return $entryCount;
            }
        ],
        [
            'attribute' => 'plotCount',
            'enableSorting' => false,
            'value' => function($data) use ($plotCount){
                return $plotCount;
            }
        ]
    ];

    echo GridView::widget([
        'dataProvider' => $occurrenceDataProvider['dataProvider'],
        'id' => 'view-occurrences-table-export-design',
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
        'columns' => $gridColumns,
        'export' => false,
        'pjax' => true,
        'striped'=>false,
        'showPageSummary'=>false,
        'panel'=>[
            'heading'=>false,
            'before'=> \Yii::t('app', '<span class="new badge">NOTE</span> &nbsp;This is the browser for the created occurrences.'),
            'after' => false,
        ],
        'toggleData'=>false
    ]);

} else if(!empty($data['plotno']) && $data['plotno'] != null && ($statusDesign != 'in progress' || $statusDesign != 'deletion in progress')){
    $entnoLabel ='';
    $plotnoLabel ='';
    $repnoLabel = '';
    $codeLabel = '';
    $blkLabel = '';
    if($label == 'entno'){
        $entnoLabel = 'teal darken-4 white-text';
    } else if($label == 'plotno'){
        $plotnoLabel = 'teal darken-4  white-text';
    } else if($label == 'block'){
        $blkLabel = 'teal darken-4  white-text';
    } else if($label == 'plotcode'){
        $codeLabel = 'teal darken-4  white-text';
    }
    if(empty($maxBlk) || $maxBlk == NULL  || in_array($design, ['Partially Replicated', 'Augmented Design'])){
        $hideBlk = "hidden";
    } else {
        $hideBlk = "";
    }

    $checked = '';
    if(!$flipped){
        $checked = 'checked';
    }
?>

    <div id="layout-viewer-panel" style="overflow:auto;">
        <!-- Dropdown Trigger -->
        <a id="entno" class='dropdown-change-label btn <?=$entnoLabel?>' href='#' >Entry number</a>
        <a id="plotno" class='dropdown-change-label btn <?=$plotnoLabel?>' href='#' >Plot number</a>
        <a id="plotcode" class='dropdown-change-label btn <?=$codeLabel?>' href='#' >Plot Code</a>
        <?php
          if($design != 'Row-Column'){
        ?>
            <a id="block"  class='dropdown-change-label btn <?=$blkLabel?> <?=$hideBlk?>' href='#' >Block number</a>
        <?php } ?>
        <?php
            $actionsDropdown = "<a class='waves-effect waves-light btn pull-right fullscreen-btn pull-right tooltipped' style='margin-left:15px;' data-position='top' data-delay='50' title='Click to toggle fullscreen'><i class='material-icons'>fullscreen</i></a>";
            $occurrenceSelect = "<a id='occurrence-selection-id' class='dropdown-button pull-left' href='#' data-activates='occurrences-dropdown-actions' style='margin:5px;' data-beloworigin='true' title='Current selected occurrence' data-constrainwidth='false'><span id='occurrence-name-span' class='new badge'>".$occurrenceName."</span><span class='caret'></span></a>".
                "<ul id='occurrences-dropdown-actions' class='dropdown-content' data-constrainwidth='false' data-beloworigin='false'>";
            foreach($occurrencesList as $occurrence){
                $occurrenceSelect .=  "<li><a href='#' class='dropdown-button occurrence-selection-list' data-hover='hover' id='".$occurrence['occurrenceDbId']."' data-name='".$occurrence['occurrenceName']."' data-alignment'='left' >".$occurrence['occurrenceName']."</a></li>";
            }
            $occurrenceSelect .= "</ul>";

            // Download occurrence
            $occurrenceDownload = "<a class='export-occurrence' id='export-btn-id' style='cursor:pointer;' title='Export Occurrence' data-id='".$occurrenceDbId."' data-target='#'><i style='padding-top: 8px;font-size: 160% !important;' class='material-icons  inline-material-icon'>file_download</i></a>";

            echo $actionsDropdown."<br>".$occurrenceDownload.$occurrenceSelect;
        ?>
        <div id="layout-view-pane"  class="col col-sm-12 view-entity-content">
        <div id="notifications-div" hidden>
            <div class="progress"><div class="indeterminate"></div></div>
        </div>
        <div id="design-layout-panel"  class="chartContainer"  style="margin-top:15px; overflow: auto;">
            <div class="progress"><div class="indeterminate"></div></div>
        </div>
        <div class="row col-sm-6">
            <label>First Plot Position</label>
            <div class="switch">
                <label>
                    Top Left
                    <input type="checkbox" id="flipped-switch-id" <?=$checked?>>
                    <span class="lever"></span>
                    Bottom Left
                </label>
            </div> 
        </div>
    </div>
</div>
<?php

$updateLayoutUrl = Url::to(['create/change-occurrence-layout']);
$updateFirstPlot = Url::to(['create/change-first-plot-pos', 'id'=>$experimentId]);

?>
<script type="text/javascript">
var experimentId = <?= $experimentId ?>;
var startingPlotNo = <?= $startingPlotNo ?>;
var noOfCols = <?= $noOfCols ?>;
var noOfRows = <?= $noOfRows ?>;
var data = JSON.parse('<?= json_encode($data) ?>');
var label = '<?= $label?>';
var dataArray = data[label];
var panelWidth = <?= $panelWidth ?>;
var flipped = '<?= $flipped ?>';
var updateLayoutUrl =  '<?= $updateLayoutUrl ?>';
var updateFirstPlot =  '<?= $updateFirstPlot ?>'
var fullscreenFlag = null;
var occurrenceDbId = <?= $occurrenceDbId ?>;
var occurrenceName = JSON.stringify("<?= $occurrenceName?>");
var xAxis = JSON.parse('<?= json_encode($xAxis) ?>');
var yAxis = JSON.parse('<?= json_encode($yAxis) ?>');

var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
var width = 0;
var height = 0;
var wbWidth = 40;
var hbWidth = 40;
var row = Object.keys(yAxis).length;
var col = Object.keys(xAxis).length;

computeDimension();
$(document).ready(function(){

    if(label == 'entno'){
        $("#occurrence-selection-id").removeClass('hidden');
        $(".export-occurrence").removeClass('hidden');
    } else {
        $("#occurrence-selection-id").addClass('hidden');
        $(".export-occurrence").addClass('hidden');
    }

    $('.dropdown-button').dropdown();
    if(noOfCols > 0){
        renderDesignLayout();
    }

    $(".dropdown-change-label").click(function(){
        label = this.id;
        dataArray = data[label];
        
        $(".dropdown-change-label").removeClass("teal darken-4");

        $(this).addClass("teal darken-4 white-text");

        if(fullscreenFlag == 1){
            $('#design-layout-panel').html('<div class="progress"><div class="indeterminate"></div></div>');
        }
        
        if(label == 'entno'){
            $("#occurrence-selection-id").removeClass('hidden');
            $(".export-occurrence").removeClass('hidden');
        } else {
            $("#occurrence-selection-id").addClass('hidden');
            $(".export-occurrence").addClass('hidden');
        }
        renderDesignLayout();
    });

    $("#flipped-switch-id").on('change', function (e, params) {

        if($('#flipped-switch-id').prop('checked') == true){
            flipped = false;
        } else{
            flipped = true;
        }
        $.ajax({
            url: updateFirstPlot,
            type: 'post',
            data: {
                flipped: flipped
            },
            success: function(response) {
                if(fullscreenFlag == 1){
                    $('#design-layout-panel').html('<div class="progress"><div class="indeterminate"></div></div>');
                }
                renderDesignLayout();
            },
            error: function(e) {
                console.log(e);
            }
        });
        
    });

    $(".occurrence-selection-list").click(function(){
        occurrenceDbId = this.id;
        occurrenceName = document.getElementById(this.id).innerText;

        $('#design-layout-panel').html('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: updateLayoutUrl,
            type: 'post',
            data: {
                occurrenceDbId: occurrenceDbId,
                fullScreen:fullscreenFlag,
                experimentId:experimentId
            },
            success: function(response) {
                layoutData = JSON.parse(response);
                data = layoutData['dataArrays'];
                dataArray = data[label];
                xAxis = layoutData['xAxis'];
                yAxis = layoutData['yAxis'];

                row = Object.keys(yAxis).length;
                col = Object.keys(xAxis).length;

                //change currently selected occurrence
                var aField = document.getElementById("occurrence-name-span");
                aField.textContent = occurrenceName;
                computeDimension();
                renderDesignLayout();

                //update export-action id
                $('#export-btn-id').attr('data-id',occurrenceDbId);

            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    $('.fullscreen-btn').click(function(e){
        if(fullscreenFlag == null || fullscreenFlag == 0){
            $('.fullscreen-btn').html('<i class="material-icons">fullscreen_exit</i>');
            fullscreenFlag = 1;
        } 
        else if(fullscreenFlag == 1){
            fullscreenFlag = 0;
            $('#main-left-panel').removeClass('hidden');
            $('.fullscreen-btn').html('<i class="material-icons">fullscreen</i>');
        }
        $('#design-layout-panel').html('<div class="alert alert-warning" style="padding: 10px;">Please wait while loading the layout.</div>');
        $.ajax({
            url: updateLayoutUrl,
            type: 'post',
            data: {
                occurrenceDbId: occurrenceDbId,
                fullScreen:fullscreenFlag,
                experimentId:experimentId
            },
            success: function(response) {
                layoutData = JSON.parse(response);
                data = layoutData['dataArrays'];
                dataArray = data[label];
                
                xAxis = layoutData['xAxis'];
                yAxis = layoutData['yAxis'];

                noOfRows = layoutData['maxRows'];
                noOfCols = layoutData['maxCols'];

                row = Object.keys(yAxis).length;
                col = Object.keys(xAxis).length;

                $('#layout-viewer-panel').toggleClass('fullscreen');
                
                computeDimension();
                renderDesignLayout();
            },
            error: function(e) {
                console.log(e);
            }
        });
    });
});

function computeDimension(){

    if(col <= 11){
        wbWidth = 80;
    } else {
        wbWidth = 50;
    }

    if(row <= 11){
        hbWidth = 40;
    } else {
        hbWidth = 30;
    }

    if(fullscreenFlag == 1){
        if(col <= 20){
            wbWidth = 80;
        }
        if(row <= 20){
            hbWidth = 40;
        }
    }

    //compute width of chart
    width = wbWidth * col;
    if(width < 200){
        width = 200;
    }

    //compute height of chart
    height = hbWidth * row;
    if(height < 200){
        height = 200;
    }
}

function renderDesignLayout(){
    var layout = null;
    var maxX = xAxis.length-1
    if(noOfCols <= 10){
        maxX = noOfCols
    }

    var maxY = yAxis.length-1
    if(noOfRows <= 10){
        maxY = noOfRows
    }
    setTimeout(function(){

        layout = Highcharts.chart('design-layout-panel',{
            chart: {
                marginTop: 70,
                marginRight: 14,
                plotBorderWidth: 1,
                type: 'heatmap',
                plotBorderWidth: 1,
                plotBorderRadius: 5,
                animation: false,
                height: height,
                width: width
            },
            title: {
                text: 'Design Result Preview'
            },
            xAxis: {
              opposite: true,
              categories: xAxis,
              min: xAxis[1],
              max: maxX,
              title:{
                text:"Column"
              }
            },
            yAxis: {
                reversed: flipped,
                categories: yAxis,
                min: yAxis[1],
                max: maxY,
                title:{
                    text:"Row"
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            series: [{
                name: 'Layout',
                borderWidth: 1,
                borderColor: '#fff',
                color: '#7cb342',
                turboThreshold:Number.MAX_VALUE,//set it to a larger threshold, it is by default to 1000
                data: dataArray,
                dataLabels: {
                    enabled: true
                }
            }]
        });
    },50);
}
</script>

<style type="text/css">
    tspan{
        font-size: 13px;
    }
    .view-entity-content {
        background: #ffffff;
    }
    #occurrence-name-span {
        font-size: 1rem;
    }
    #layout-viewer-panel.fullscreen{
        padding: 25px;
        z-index: 9999; 
        width: 100%; 
        height: 100%; 
        position: fixed; 
        top: 0; 
        left: 0;
        background: #ffffff;
    }
    .highcharts-container{
        width:100%;
    }
    .chartContainer{
        overflow:auto !important;
    }

    .dropdown-content {
        overflow: auto !important;;
    }
</style>

<?php
}

$plotCount = isset($plotCount)? $plotCount : 0;
$exportRecordsThresholdValue = \Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportRecords');
$prepareDataForCsvDownloadUrl = Url::to(['/occurrence/view/prepare-data-for-csv-download', 'program'=>$program,'entity'=>'plot', 'experimentId'=>$experimentId, 'tool'=>'experiment-creation']);
$exportPlotsUrl = Url::to(['/occurrence/view/export-plots']);
?>

<script type="text/javascript">
    $(document).on('click', '.export-occurrence', function (e) {
        e.preventDefault()
        e.stopImmediatePropagation()
        $('.export-occurrence').prop('disabled', true);
        var occurrenceDbId = $(this).attr('data-id') == '0' ? occurrenceDbId : $(this).attr('data-id')
        exportOccurrences(occurrenceDbId)
    });

    function exportOccurrences(occurrenceDbId){
        if ( parseInt(<?= $plotCount ?>) >= parseInt(<?= $exportRecordsThresholdValue?>) ) {
    
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '<?= $prepareDataForCsvDownloadUrl ?>'+'&occurrenceId='+occurrenceDbId,
                data: {},
                success: async function (ajaxResponse) {},
                error: function (xhr, error, status) {}
            })
        } else {
            let exportPlotsUrl = '<?= $exportPlotsUrl ?>'
            let expPlotUrl = window.location.origin + exportPlotsUrl + '?program=' + '<?= $program ?>' + '&id=' + occurrenceDbId

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
            $('.toast').css('display','none')
            Materialize.toast(message, 5000)

            window.location.replace(expPlotUrl)

            $('#system-loading-indicator').html('')
            $('.export-occurrence').prop('disabled', false);
        }
    }
</script>