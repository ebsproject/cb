<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders design step for experiment creation
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use app\models\Program;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;

use app\modules\experimentCreation\models\Transaction;


$disabled = '';
if (strpos($model['experimentStatus'],'design generated') === false) {
    $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model'=>$model,
    'program'=>$program,
    'saveBtnLabel'=>'Next',
    'nextBtn' => $disabled,
    'btnName' => 'noActionBtn'
]);
echo '<p>'.Yii::t('app', 'Specify the minimum required parameters for the selected design.').'</p>';

?>

    <div class="row" style="margin-top:10px;padding-top:10px; ">
    <div id="error_field" class="hidden"></div>

        <div class="col col-md-6">
        <div id="notif_field" class="hidden"></div>
        <?php
            echo $grid = GridView::widget([
              'pjax' => true, // pjax is set to always true for this demo
              'dataProvider' => $entryInfoDataProvider,
              'id' => 'entry-info-design-browser', 
              'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-seed-lot-table'],
              'striped'=>false,
              'showPageSummary'=>false,
              'columns'=>[
                  [
                    'attribute'=>'entry',
                    'label' => Yii::t('app', 'No. of Entries'),
                    'format' => 'raw',
                    'enableSorting' => false,
                  ],  
                  [
                    'attribute'=>'check',
                    'label' => Yii::t('app', 'No. of Checks'),
                    'format' => 'raw',
                    'enableSorting' => false,
                  ], [
                    'attribute'=>'total',
                    'label' => Yii::t('app', 'Total Entries'),
                    'format' => 'raw',
                    'enableSorting' => false,
                  ]
              ]
            ]);

        ?>
        <form method="POST" id="experiment-design-form" accept-charset="UTF-8" role="form" class="form-horizontal re-form form-inv">
            <div class="col col-sm-5" style="margin-left:50px;">
              <div class="row" style="margin-bottom:25px;">
                <?php
                    
                    echo '<label class="control-label">'.\Yii::t('app', 'Design').'</label>';
                    
                    echo Select2::widget([
                        'name' => 'ExperimentDesign[DESIGN]',
                        'value' => isset($experimentDesign) ? $experimentDesign:'',// initial value
                        'data' => $designArray,
                        'id' => 'design_select_id',
                        'options' => ['placeholder' => Yii::t('app', 'Select design'), 'class' => 'select2-Drop', 'style'=>'margin-top:25px;',
                        ],
                    ]);
                ?>
              </div>
                <div id="experiment_design_fields">
                    <div class="container-design">
                        
                      <div class="row" style="margin: 10px;"><h6>Experimental Design Parameters</h6></div>
                        <?php
                            foreach($designFields as $field){
                                if($field['abbrev']=='PERCENT_CHECK_PLOTS' && !$withChecks){
                                    continue;
                                }
                                else if(isset($field['value'])) { 
                                    echo $field['value'];
                                    
                                    // Needed for next sprint, for viewing results
                                    // echo '<div class="control-label row  col-md-6" style="margin-bottom:10px; padding-left:5px;">';
                                    //   echo '<div class="col-md-12" title="Year when the trial or nursery was conducted" style="">';
                                    //     echo $field['rawLabel'];
                                    //   echo '</div>';

                                    //   echo '<div class="col-md-12" style="padding-right:0px;">';
                                    //     echo $field['rawValue'];
                                    //   echo '</dvi>';
                                    // echo '</dvi>';
                                }
                            }
                        ?>
                    </div>
                </div>
        </div>
    

        <div class="col col-md-5" style="margin-left:10px;">
          
              <?php
                  if(isset($planDesign['status']) && $planDesign['status'] != 'randomization in progress'){
              ?>
                  <a id="submit_design_btn" disabled href="#" class="btn btn-primary waves-effect waves-light" style="margin:25px;margin-left:0px;"><?= \Yii::t('app', '<i class="material-icons right">play_arrow</i> Generate Design') ?></a>
              <?php
                  } elseif(isset($planDesign['status']) && $planDesign['status'] == 'randomization in progress'){
              ?>
                  <a id="check_results_btn" href="#" class="btn btn-primary waves-effect waves-light hidden" style="margin:25px;margin-left:0px;"><?= \Yii::t('app', '<i class="material-icons right">check</i> Check Results') ?></a>
              <?php
                  }
              ?>
            <div id="layout_design_fields" class="<?= ($showLayout)? "" :"hidden";?>">
                <div class="container-design">
                <div class="row" style="margin: 10px;"><h6>Field Layout Details</h6></div>
                  <?php
                      foreach($layoutFields as $field){
                          if($field['abbrev']=='PERCENT_CHECK_PLOTS' && !$withChecks){
                              continue;
                          }
                          else if(isset($field['value'])) { 
                              echo $field['value'];
                          }
                      }
                  ?>
                </div>
            </div>
        </div>
    </form>
    </div>

    <!--- start results area -->
    <div class="col col-md-4">
        <!-- Displays progress of background process transactions -->
        <?php 
            $transactionsStr = Transaction::getActiveGearmanTransactions($userId, $id, $program);

            echo '<span id="transaction-panel">';
            if(!empty($transactionsStr)){
                echo $transactionsStr;
            }
            echo '</span>';
        ?>
    </div>
    <!--- end results area -->
    </div>
                

<?php
Modal::begin([
    'id' => 'design-recently-used-tools-modal'
]);
Modal::end();
$getFieldsUrl = Url::to(['/experimentCreation/create/specify-design', 'id'=>$id, 'program'=>$program]);
$saveDesignUrl = Url::to(['/experimentCreation/create/change-design', 'id'=>$id, 'program'=>$program]);
$nextUrl = Url::to(['create/specify-experiment-groups', 'id'=>$id, 'program'=>$program]);
$generateJsonUrl = Url::to(['create/generate-json-input-analytics', 'id'=>$id, 'program'=>$program]);
$checkJsonUrl = Url::to(['create/check-design-results', 'id'=>$id, 'program'=>$program]);
$downloadUrl = Url::to(['create/download-results']);
$getContentUrl = Url::to(['create/get-contents']);
$checkJobUrl = Url::to(['create/check-download-results']);
$removeTransactionUrl = Url::to(['create/remove-transaction']);
$getTransactionsUrl = Url::to(['create/get-transactions']);
$status = $model['experimentStatus'];

$this->registerJs(<<<JS
    var entryInfo = JSON.parse('$entryInfo');
    var noEntries = parseInt(entryInfo['total']);
    var test_entries = parseInt(entryInfo['entry']);
    var no_of_checks = parseInt(entryInfo['check']);
    var design = ($("#design_select_id option:selected").html()).toLowerCase();
   
    var blkno = $('#nRep').val();
    var row = $('#nFieldRow').val();
    var col = 0;
    var totalPlots = 0;
    var trmtBlkRep = 0;
    var changeFlag = 0;
    var errorArray = [];
    var blkSize = 0;
    var flag = 0;
    var initflag = 0;
    var  nRowBlock = 0;
    
    $("document").ready(function(){
        // **** TO BE REFACTORED for the next sprint *****//
        //  setTimeout(function(){ $(".success").fadeOut("slow"); },1000);

        //  setTimeout(function(){ $(".error").fadeOut("slow"); },1000);
        //  //refresh background process transaction
        //  setInterval(function(){
        //     $.ajax({
        //         url: '$getTransactionsUrl',
        //         type: 'post',
        //         dataType: 'json',
        //         data: {
        //             userId: $userId,
        //             id: $id,
        //             program: '$program'
        //         },
        //         success: function(response) {
        //             if(response === 'reload'){
        //                 location.reload();
        //             } else{
        //                 $('#transaction-panel').html(response);
        //             }
        //         },
        //         error: function() {
        //         }
        //     });
        // }, 1000);
        
        if(design.includes('alpha-lattice') || design.includes('row-column')){
            if(noEntries < 9){
                $("#submit_design_btn").attr('disabled', 'disabled');
                $("#error_field").html("");
                $("#error_field")[0].innerHTML = "<div class='alert warning'><div class='card-panel'><i class='fa-fw fa fa-times'></i> The number of entries is invalid for this design. (Minimum = 9 and Maximum = 750) <a href='#' class='close' data-dismiss='alert'>&times;</a></div></div>";
                $("#error_field").removeClass("hidden");
                errorArray.push('submit_design_btn');
                initflag=1;
            }
            
        }
        if(design.includes('augmented rcb')){
            if(no_of_checks < 2){
                $("#submit_design_btn").attr('disabled', 'disabled');
                $("#error_field").html("");
                $("#error_field")[0].innerHTML = "<div class='alert warning'><div class='card-panel'><i class='fa-fw fa fa-times'></i> Entries with <b>CHECK entry role MINIMUM count should be 2</b>. Kindly update your entry list. <a href='#' class='close' data-dismiss='alert'>&times;</a></div></div>";
                $("#error_field").removeClass("hidden");
                errorArray.push('submit_design_btn');
                initflag=1;
            }
            if(test_entries < 2){
                $("#submit_design_btn").attr('disabled', 'disabled');
                $("#error_field").html("");
                $("#error_field")[0].innerHTML = "<div class='alert warning'><div class='card-panel'><i class='fa-fw fa fa-times'></i>Entries with <b>TEST entry role MINIMUM count should be 2</b>. Kindly update your entry list. <a href='#' class='close' data-dismiss='alert'>&times;</a></div></div>";
                $("#error_field").removeClass("hidden");
                errorArray.push('submit_design_btn');
                initflag=1;
            }
            
        }

        // check if there are empty fields
        $(".numeric-input-validate").each(function(){
            
            if(this.value == '' || this.value == undefined){
                flag = 1;
            } 
            checkFields();
        });

        if(flag == 0 && initflag == 0){
            $("#submit_design_btn").removeAttr('disabled');
        } else {
            $("#submit_design_btn").attr('disabled', 'disabled');
        }

        //populate select fields if there are saved values
        if(design.includes('rcbd')){
            if(row > 0 && row != null){
                populateDimensionFields('nFieldRow');
                var repBlk = $('#nRowPerRep').val();
                $('#nFieldRow').val(row);
                changeRows(row);
                $('#nRowPerRep').val(repBlk);
            }
        } else if(design.includes('augmented rcb')){
            if(row > 0 && row != null){
                populateDimensionFields('nFieldRow');
                var repBlk = $('#nRowPerRep').val();
                $('#nFieldRow').val(row);
                changeRows(row);
                $('#nRowPerRep').val(repBlk);
            }
        } else if(design.includes('row-column')){
            nRowBlock = $('#nRowBlk').val();
            populateDimensionFields('nRowBlk');
            $("#nRowBlk").removeAttr('disabled');

            if(nRowBlock > 0 && nRowBlock != null){
                $('#nRowBlk').val(nRowBlock);
                if(row > 0){
                    var factorVal = populateSelectData(design, 'nFieldRow', null, null);
                    
                    addDropDownOptions("nFieldRow", factorVal);
                    $('#nFieldRow').val(row);
                }
            }
        } else if(design.includes('alpha-lattice')){
            trmtBlkRep = $('#nBlk').val();
            populateDimensionFields('nBlk');
            $("#nBlk").removeAttr('disabled');
            
            if(trmtBlkRep > 0 && trmtBlkRep != null){
                $('#nBlk').val(trmtBlkRep);
                var rowPerBlk = $('#nRowPerBlk').val();
                if(rowPerBlk > 0){
                    var factorVal = populateSelectData(design, 'nRowPerBlk', null, null);
                    addDropDownOptions("nRowPerBlk", factorVal);
                    $('#nRowPerBlk').val(rowPerBlk);
                    
                    var rowPerRep = $('#nRowPerRep').val();
                    if(rowPerRep > 0){
                        var factorVal = populateSelectData(design, 'nRowPerRep', null, null);
                        addDropDownOptions("nRowPerRep", factorVal);
                        $('#nRowPerRep').val(rowPerRep);

                        if(row > 0){
                            var factorVal = populateSelectData(design, 'nFieldRow', null, null);
                            addDropDownOptions("nFieldRow", factorVal);
                            $('#nFieldRow').val(row);
                        }
                    }
                    
                }
                
            }

        }

        $("#nFieldRow").on('change', function (e, params) {
            changeRows(this.value);
            if(design.includes('rcbd') || design.includes('augmented rcb')){
                var element = document.getElementById('nRowPerRep');
                if(element != undefined){
                    $("#nRowPerRep").removeAttr('disabled');
                } else {
                    checkFields();
                }
            } else if(design.includes('row-column')){
                // $('#nRowPerRep').removeAttr('disabled');
                checkFields();
            } else if(design.includes('alpha-lattice')){
                checkFields();
            }
        });

    });

    function changeBlk(changeField, value){
        var changeValue = parseInt(noEntries / value);
        
        computeBlockSize(changeValue);
    }

    function changeRows(value){
    
        var factorVal = populateSelectData(design, 'nRep', null, null);
        if (design.includes('rcbd')) {
            var element = document.getElementById('nRowPerRep');
            if(element != undefined){
                if (factorVal.length === 0) {
                    var notif = "<i class='material-icons red-text left'>close</i> The specified row cannot accommodate the defined no. of blocks. ";
                    Materialize.toast(notif,5000);
                    
                    $('#nRowPerRep').empty();
                } else {
                  
                    $("#nRowPerRep").removeAttr('disabled').tooltip('destroy');
                    $('#nRowPerRep').empty();
                    addDropDownOptions("nRowPerRep", factorVal);
                    
                }
            }
        } else if(design.includes('augmented rcb')) {
            if (parseInt(factorVal.length) === 0) {
                
                var notif = "<i class='material-icons red-text left'>close</i> The specified row cannot accommodate the defined no. of blocks. ";
                Materialize.toast(notif,5000);
              
                $('#nRowPerRep').empty();
            } else {
                $("#nRowPerRep").removeAttr('disabled').tooltip('destroy');
                $('#nRowPerRep').empty();
                addDropDownOptions("nRowPerRep", factorVal);
                
            }
        } else if(design.includes('row-column')){
          
        } else if(design.includes('alpha-lattice')){

        }
    }

    // $('[name="generate-layout"]').change(function(){
      // $('#genLayout').change(function(){
      $("#genLayout").on('change', function (e, params) {
        alert("TEST");
        console.log($('[name="ExperimentDesign[genLayout]"]').prop('checked'));
        if($('[name="ExperimentDesign[genLayout]"').prop('checked') == true){
            $("#layout_design_fields").removeClass('hidden');
            checkFields();
            if(design.includes('rcbd')){
                populateDimensionFields('nFieldRow');
                $('#nFieldRow').removeAttr('disabled');
            } else if(design.includes('row-column')){
                populateDimensionFields('nFieldRow');
                $('#nFieldRow').removeAttr('disabled');
            } else if(design.includes('alpha-lattice')){
                changeBlk('nBlockSize', $('#nBlk').val());

                $('#nRowPerBlk').removeAttr('disabled');
            } else if(design.includes('augmented rcb')){
                populateDimensionFields('nFieldRow');
                $('#nFieldRow').removeAttr('disabled');
            }
        } else{
            $("#layout_design_fields").addClass('hidden');
        }
        checkFields();
    });

    function flashMessage(msg, className){
        $('#notif_field').html('<br/><span class="red-text text-darken-2">Please specify valid values.</span>');
        $('#notif_field').removeClass('hidden');
    }

    $(".randomization").change(function() {
        //check values
        if (parseFloat(this.value) < parseFloat(this.min)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Minimum value is ' + this.min;
            className = 'warning';
            errorArray.push(this.id);
            flashMessage(message, className);
        } else if (parseFloat(this.value) > parseFloat(this.max)) {
            $('#'+this.id).addClass('invalid');
            message = '<br><i class="fa fa-times"></i> Incorrect value. Maximum value is ' + this.max;
            className = 'warning';
            errorArray.push(this.id);
            flashMessage(message, className);
        } else{
            var idx = this.id;
            var idx = errorArray.indexOf(this.id);
            errorArray.splice(idx, 1);
            $('#'+this.id).removeClass('invalid');

            if(Object.keys(errorArray).length === 0){
                $('#notif_field').addClass('hidden');
            }
        }
        checkFields();
    });
    $(".layout").change(function() {
        checkFields();
    });

    $('#nBlk').change(function(){
        changeBlk('nBlockSize', this.value);

        $('#nRowPerBlk').removeAttr('disabled');
    });

    function computeBlockSize(changeValue){
        blkSize = changeValue;
        changeFlag = 1;
        if (changeValue != 0) {
            if (design.includes('alpha-lattice')) {
                removeDropDownOptions(design, 'nFieldRow');
                removeDropDownOptions(design, 'nRowPerBlk');
                removeDropDownOptions(design, 'nRowPerRep');
                var factorVal = populateSelectData(design, 'nRowPerBlk', null, null);

                if (factorVal.length === 0) {
                    if($('[name="generate-layout"]').prop('checked') == true){
                        var notif = "<i class='material-icons red-text left'>close</i> The specified row cannot accommodate the selected blocks. ";
                        Materialize.toast(notif,5000);
                    }
                  
                    $('#nRowPerBlk').empty();
                } else {
              
                    $("#nRowPerBlk").removeAttr('disabled').tooltip('destroy');
                    addDropDownOptions("nRowPerBlk", factorVal);
                }
            }
        } else {
            $(this).addClass('error').tooltip({
                'title': 'Select a value',
                'placement': 'right'
            }).tooltip('show');
        }
    };

    $("#nRep").on('change', function (e, params) {

        if(design.includes('rcbd')){
            removeDropDownOptions(design, 'nFieldRow');
            removeDropDownOptions(design, 'nRowPerRep');
            $("#nRowPerRep").attr('disabled', 'disabled');
            populateDimensionFields('nFieldRow');
            $('#nFieldRow').removeAttr('disabled');
        } else if(design.includes('row-column')){
            removeDropDownOptions(design, 'nFieldRow');
            // removeDropDownOptions(design, 'nRowPerRep');
            // $("#nRowPerRep").attr('disabled', 'disabled');
            populateDimensionFields('nFieldRow');
            $('#nFieldRow').removeAttr('disabled');
        } else if(design.includes('alpha-lattice')){
            removeDropDownOptions(design, 'nBlk');

            removeDropDownOptions(design, 'nFieldRow');
            // $("#nFieldRow").attr('disabled', 'disabled');
            removeDropDownOptions(design, 'nRowPerBlk');
            // $("#nRowPerBlk").attr('disabled', 'disabled');
            removeDropDownOptions(design, 'nRowPerRep');
            // $("#nRowPerRep").attr('disabled', 'disabled');
            populateDimensionFields('nBlk');
        } else if(design.includes('augmented rcb')){
            //check if no. of checks are divisible by nRep
            var checkValid = no_of_checks % this.value;
            if(checkValid ==  0){
                removeDropDownOptions(design, 'nFieldRow');
                removeDropDownOptions(design, 'nRowPerRep');
                $("#nRowPerRep").attr('disabled', 'disabled');
                populateDimensionFields('nFieldRow');
                $('#nFieldRow').removeAttr('disabled');
            } else{
                var notif = "<i class='material-icons red-text left'>close</i> The total no. of checks in the entry list should be divisible by the no. of replicates. ";
                Materialize.toast(notif,5000);
                $("#submit_design_btn").attr('disabled', 'disabled');
            }
            
        }
    });


    //variable specific computation for repcount field
    $("#nRowPerBlk, #nRowPerRep, #nRowBlk").on('change', function (e, params) {
        changeFlag = 1;
        rows = parseInt($('#nFieldRow').val());
        
        if (no_of_checks === undefined || isNaN(no_of_checks) || no_of_checks === "") {
            no_of_checks = 0;
        }
        var noEntries = parseInt(test_entries) + parseInt(no_of_checks);
        if (design.includes('alpha-lattice')) {
            if (this.id === "nRowPerBlk") {
                // document.getElementById('COL').value = null;
                removeDropDownOptions(design, 'nFieldRow');
                removeDropDownOptions(design, 'nRowPerRep');
                // var blkSize = $('#nBlockSize').val();
                var no_of_blocks = parseInt(totalPlots) / parseInt(blkSize);

                //enable nRowPerRep
                var factorVal = populateSelectData(design, 'nRowPerRep', null, null);

                if (factorVal.length === 0) {
                    var notif = "<i class='material-icons red-text left'>close</i> The specified row cannot accommodate the selected blocks. ";
                    Materialize.toast(notif,5000);
                        
                    $('#nRowPerRep').empty();
                } else {
                    $("#nRowPerRep").removeAttr('disabled').tooltip('destroy');
                    addDropDownOptions("nRowPerRep", factorVal);
                }
            } else if (this.id === "nRowPerRep") {
                // document.getElementById('COL').value = null;
                removeDropDownOptions(design, 'nFieldRow');
                var factorVal = populateSelectData(design, 'nFieldRow', null, null);
                if (factorVal.length === 0) {
                    var notif = "<i class='material-icons red-text left'>close</i> The specified row cannot accommodate the selected blocks. ";
                    Materialize.toast(notif,5000);

                    $('#nFieldRow').empty();
                } else {
                    
                    $("#nFieldRow").removeAttr('disabled').tooltip('destroy');
                    addDropDownOptions("nFieldRow", factorVal);
                }
            }
        } else if(design.includes('row-column')){
            if (this.id === "nRowBlk") {
                rep = parseInt($('#nRep').val());
                
                if(!isNaN(rep) && rep != undefined && rep != null){
                  removeDropDownOptions(design, 'nFieldRow');
                    var factorVal = populateSelectData(design, 'nFieldRow', null, null);
                    if (factorVal.length === 0) {
                        var notif = "<i class='material-icons red-text left'>close</i> The specified row cannot accommodate the selected blocks. ";
                        Materialize.toast(notif,5000);
                        
                        $('#nFieldRow').empty();
                    } else {
                        addDropDownOptions("nFieldRow", factorVal);
                        $('#nFieldRow').removeAttr('disabled');
                    }
                }
            } 
        } else if(design.includes('augmented rcb')){
            checkFields();
        } else if(design.includes('rcbd')){
            checkFields();
        }
    });
    
    function removeDropDownOptions(design, dropDownName){
        $('#' + dropDownName).empty();
    }

    function checkFields(){
        var flag = 0;
        $(".randomization").each(function(){
            if(this.value == '' || this.value == undefined || this.value == 'Select a value'){
                flag = 1;
            } 
        });
        if($('[name="generate-layout"]').prop('checked') == true){
            $(".layout").each(function(){
                if(this.value == '' || this.value == undefined  || this.value == 'Select a value'){
                    flag = 1;
                }
            });
        }
        if(Object.keys(errorArray).length > 0){
            flag=1;
        }
        if(flag == 0  && initflag == 0){
            $("#submit_design_btn").removeAttr('disabled');
        } else {
            $("#submit_design_btn").attr('disabled', 'disabled');
        }
    }

    $("#design_select_id").change(function() {
        var designText = $("#design_select_id option:selected").html();
        var designValue = this.value;
        var design =  designValue.split('|');
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: '$saveDesignUrl',
            data: {
                designId:design[0],
                design:designText,
                designCode:design[1]
            },
            success: function(data) {
                location.reload();
            },
            error: function(){
                location.reload();
            }
        });      

    });

    $("#submit_design_btn").click(function() {
        $("select[disabled]").each(function(){
            $("#"+this.id).removeAttr('disabled', 'disabled');
        });
        var form_data = $("#experiment-design-form").serialize();
        var action_url = '$generateJsonUrl';
        
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: action_url,
            data: form_data,
            success: function(data) {
                if(data == 'success'){
                    $("#submit_design_btn").attr('disabled', 'disabled');
                    var notif = "<i class='material-icons green-text left'>check</i> Request successfully sent.";
                    Materialize.toast(notif,5000);
                } else {
                    var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                    Materialize.toast(notif,5000);
                }
            },
            error: function(){
                var notif = "<i class='material-icons red-text left'>close</i> Unable to send the request. Please try again.";
                Materialize.toast(notif,5000);
            }
        });      
    });

    $("#check_results_btn").click(function() {
        var action_url = '$checkJsonUrl';
        $.ajax({
            type:"POST",
            dataType: 'json',
            url: action_url,
            success: function(data) {
                location.reload();
            },
            error: function(){
                location.reload();
            }
        });      
    });

    $(document).on("click","a[class='download-results-btn']",function(e){
    // $(".download-results-btn").click(function() {
        var filename = $(this).attr('filename');
        var planId = this.id;
        var labelType = $(this).attr('labelType');
        location.href = '$downloadUrl'+'?planId='+planId+'&fileName='+filename+'&labelType='+labelType;
        $('#system-loading-indicator').hide('');

    });
    $("#next-btn").click(function() {
        window.location = '$nextUrl';
    });

    function populateSelectData(design, paramName, row, column) {
        var factorArray = [];
        rows =$('#nFieldRow').val();
        cols = totalPlots / rows;
        if (no_of_checks === undefined || isNaN(no_of_checks) || no_of_checks === "") {
            no_of_checks = 0;
        }
        var noEntries = parseInt(test_entries) + parseInt(no_of_checks);
        if (design.includes('rcbd')) {
            if (no_of_checks === undefined || isNaN(no_of_checks) || no_of_checks === "") {
                no_of_checks = 0;
            }
            var factor_entries = getAllFactorsOfInteger(noEntries);
            var factor_rep = getAllFactorsOfInteger(blkno);
            for (var i = 0; i < factor_rep.length; i++) {
                for (var j = 0; j < factor_entries.length; j++) {
                    var totalRows = factor_rep[i] * factor_entries[j];
                    if (totalRows === parseInt(rows)) {
                        var totalCols = totalPlots / totalRows;
                        if (parseFloat(totalCols) === parseFloat(cols)) {
                            factorArray[factor_entries[j]] = factor_entries[j];
                        }
                    }
                }
            }
        } else if (design.includes('augmented rcb')) {

            blkno = parseInt(document.getElementById('nRep').value);
            totalPlots = (no_of_checks * blkno) + parseInt(test_entries);
            rows =$('#nFieldRow').val();

            cols = totalPlots / rows;
            var blkSize = totalPlots / blkno;
            var factor_rows = getAllFactorsOfInteger(rows);
            var factor_columns = getAllFactorsOfInteger(cols);
            for (var index in factor_rows) {
                var temp = factor_rows[index];
                temp = parseInt(temp);
                for (var col in factor_columns) {
                    var temp2 = factor_columns[col];
                    temp2 = parseInt(temp2);
                    if ((parseInt(temp * temp2) === parseInt(blkSize)) && ((parseInt(rows) / parseInt(temp)) * (parseInt(cols) / parseInt(temp2)) === parseInt(blkno))) {
                        factorArray[temp] = temp;
                    }
                }
            }
        } else if (design.includes('row-column')) {
            var blkFactors = getAllFactorsOfInteger(blkno);
            var rowBlock = $('#nRowBlk').val();

            for (var blk in blkFactors) {
                var temp = parseInt(blkFactors[blk]) * parseInt(rowBlock);
                factorArray[temp] = temp;
            }
        } else if (design.includes('alpha-lattice')) {
            totalPlots = blkno * noEntries;
            if (paramName === 'nBlockSize') {
                var factor_entries = getFactorsWithLimit(2, noEntries);
                factorArray = factor_entries;
            } else if (paramName === 'nRowPerBlk') {
                var nBlock = $('#nBlk').val();
                blkSize = parseInt(noEntries / nBlock);
                var factorArray1 = getAllFactorsOfInteger(blkSize);
                var factorArray = factorArray1.sort(function (a, b) {  return a - b;  });

            } else if (paramName === 'nRowPerRep') {

                var nBlock = $('#nBlk').val();
                blkSize = parseInt(noEntries / nBlock);
                var blocks = noEntries / blkSize;
                
                var blkFactors = getAllFactorsOfInteger(blocks);
                var rowPerBlk = $('#nRowPerBlk').val();
                var blkNoFactors = getAllFactorsOfInteger(blkno);
                
                //get all combinations for the rowPerBlk based on the no. of blocks per rep
                var allRowPerBlocks = [];
                for (var blk in blkFactors) {
                    var temp = rowPerBlk * parseInt(blkFactors[blk]);
                    allRowPerBlocks.push(temp);
                }
                for (var blkEle in blkNoFactors) {
                    for (var allIdx in allRowPerBlocks) {
                        var temp = allRowPerBlocks[allIdx] * parseInt(blkNoFactors[blkEle]);
                        if (parseInt(allRowPerBlocks[allIdx]) <= parseInt(noEntries)) {
                            factorArray[allRowPerBlocks[allIdx]] = allRowPerBlocks[allIdx];
                        }
                    }
                }
            } else if(paramName === 'nFieldRow'){
                var blkFactors = getAllFactorsOfInteger(blkno);
                var rowPerRep = $('#nRowPerRep').val();

                for (var blk in blkFactors) {
                    var temp = parseInt(blkFactors[blk]) * parseInt(rowPerRep);
                    factorArray[temp] = temp;
                }
            }
        }
        return factorArray;
    }

    function getFactorsWithLimit(min, n) {
            var factors_array = [];
            for (var x = 1; x <= Math.sqrt(Math.abs(n)); x++) {
                if (n % x === 0) {
                    var z = n / x;
                    if (z > min) {
                        factors_array.push(z);
                    }
                    if (x > min) {
                        factors_array.push(x);
                    }
                }
            }
            return factors_array;
    }
    function getAllFactorsOfInteger(n) {
        var factors_array = [];
        for (var x = 1; x <= Math.sqrt(Math.abs(n)); x++) {
            if (n % x === 0) {
                var z = n / x;
                if (factors_array.indexOf(z) === -1) {
                    factors_array.push(z);
                }
                if (factors_array.indexOf(x) === -1) {
                    factors_array.push(x);
                }
            }
        }
        return factors_array;
    }
    function populateDimensionFields(fieldId) {
        design = $("#design_select_id option:selected").html().toLowerCase();
        blkno = parseInt(document.getElementById('nRep').value);
        if(!design.includes('p-rep')){
            if (no_of_checks === undefined || isNaN(no_of_checks) || no_of_checks === "") {
                no_of_checks = 0;
            }
            var noEntries = parseInt(test_entries) + parseInt(no_of_checks);
        }
        viewArray = [];
        rowPerBlkValArr = [];
        rowArray = [];
        rowColArr = [];
        colArray = [];
        colRowArr = [];

        if(fieldId != 'nBlk'){

            if (design.includes("rcbd")) {
                totalPlots = blkno * noEntries;
                var rowPerBlkArray = getAllFactorsOfInteger(noEntries);
                var blknoFlag = getAllFactorsOfInteger(blkno);
                for (var i = 0; i < rowPerBlkArray.length; i++) {
                    var row = 0;
                    var column = 0;
                    var val = rowPerBlkArray[i];
                    for (var j = 0; j < blknoFlag.length; j++) {
                        var blkVal = blknoFlag[j];
                        row = parseInt(val * blkVal);
                        column = parseInt(noEntries / val) * parseInt(blkno / blkVal);

                        if (rowArray.indexOf(row) === -1) {
                            rowArray[row] = row;
                            rowColArr[row] = column;
                        }
                        if (colArray.indexOf(column) === -1) {
                            colArray[column] = column;
                            colRowArr[column] = row;
                        }
                        viewArray[row] = row;
                            
                    }
                }
                } else if(design.includes('row-column')){
                    if(fieldId == 'nRowBlk'){
                        var rowPerBlkArray = getAllFactorsOfInteger(noEntries);
                        var blknoFlag = getAllFactorsOfInteger(1);
                    
                        for (var i = 0; i < rowPerBlkArray.length; i++) {
                            var row = 0;
                            var column = 0;
                            var val = rowPerBlkArray[i];
                            for (var j = 0; j < blknoFlag.length; j++) {
                                var blkVal = blknoFlag[j];
                                row = parseInt(val * blkVal);
                                column = parseInt(noEntries / val) * parseInt(blkno / blkVal);
                                if(row > 1 && row < noEntries){
                                    viewArray[row] = row;
                                }
                                
                            }
                        }
                    } else{
                        var rowPerBlkArray = getAllFactorsOfInteger(noEntries);
                        var blknoFlag = getAllFactorsOfInteger(blkno);
                        totalPlots = blkno * noEntries;
                        for (var i = 0; i < rowPerBlkArray.length; i++) {
                            var row = 0;
                            var column = 0;
                            var val = rowPerBlkArray[i];
                            for (var j = 0; j < blknoFlag.length; j++) {
                                var blkVal = blknoFlag[j];
                                row = parseInt(val * blkVal);
                                column = parseInt(noEntries / val) * parseInt(blkno / blkVal);
                                if((totalPlots/row) < noEntries){
                                    viewArray[row] = row;
                                }
                            }
                        }
                    }
                } else if(design.includes('augmented rcb')){
                    totalPlots = (parseInt(no_of_checks) * blkno) + parseInt(test_entries);
                    var rowPerBlkArray = getAllFactorsOfInteger(totalPlots);
                    var blknoFlag = getAllFactorsOfInteger(blkno);

                    for (var i = 0; i < rowPerBlkArray.length; i++) {
                        var val = rowPerBlkArray[i];
                        if(val >= blkno){
                            var row =  val;
                            var column = parseInt(totalPlots / val);
                            viewArray[row] = row;
                        }
                    }
                    
                } else if (design.includes('augmented lsd')){
                    blkno = $('#CHECK_COUNT').val();
                    var rowPerBlkArray = getAllFactorsOfInteger(totalPlots);
                    var blknoFlag = getAllFactorsOfInteger(blkno);

                    for (var i = 0; i < rowPerBlkArray.length; i++) {
                        var val = rowPerBlkArray[i];
                        if(val >= blkno){
                            var row =  val;
                            var column = parseInt(totalPlots / val);
                            if (column >= blkno){
                                viewArray[row] = row;
                            }
                        }
                    }
                } else if(design.includes('p-rep')){
                    blkno = 1;
                    var rowPerBlkArray = getAllFactorsOfInteger(totalPlots);
                    var blknoFlag = getAllFactorsOfInteger(blkno);
                    for (var i = 0; i < rowPerBlkArray.length; i++) {
                        var val = rowPerBlkArray[i];
                        if(val >= blkno){
                            var row =  val;
                            var column = parseInt(totalPlots / val);
                            viewArray[row] = row;
                        }
                    }

                }  else if (design.includes('alpha-lattice')){
                    totalPlots = blkno * noEntries;
                    var rowPerBlkArray = getAllFactorsOfInteger(noEntries);
                    var blknoFlag = getAllFactorsOfInteger(1);

                    for (var i = 0; i < rowPerBlkArray.length; i++) {
                        var row = 0;
                        var column = 0;
                        var val = rowPerBlkArray[i];
                        for (var j = 0; j < blknoFlag.length; j++) {
                            var blkVal = blknoFlag[j];
                            row = parseInt(val * blkVal);
                            column = parseInt(noEntries / val) * parseInt(blkno / blkVal);
                            viewArray[row] = row;
                        }
                    }
                }
            } else if(fieldId == 'nBlk'){
                if(design.includes('alpha-lattice')){
                    var rowPerBlkArray = getAllFactorsOfInteger(noEntries);
                    var blknoFlag = getAllFactorsOfInteger(1);
                   
                    for (var i = 0; i < rowPerBlkArray.length; i++) {
                        var row = 0;
                        var column = 0;
                        var val = rowPerBlkArray[i];
                        for (var j = 0; j < blknoFlag.length; j++) {
                            var blkVal = blknoFlag[j];
                            row = parseInt(val * blkVal);
                            column = parseInt(noEntries / val) * parseInt(blkno / blkVal);
                            viewArray[row] = row;
                        }
                    }
                }
            }
            addDropDownOptions(fieldId, viewArray);
        }
        
        function addDropDownOptions(dropDownName, valueArray) {
           
            var dropDown = document.getElementById(dropDownName);
           
            var dropDownHidden = document.getElementById(dropDownName+"-hidden");
            if(dropDownHidden != undefined){
              dropDown = dropDownHidden;
            }

            if(dropDown !== null){
              
                $('#' + dropDownName).empty();
                // $('#' + dropDownName).append('<option hidden><i style="color:gray;">Select a value</i></option>');
                // for (var fieldIndex in valueArray) { // then populatem them
                //     var flagValue = 0;
              
                //     if(dropDown != undefined && dropDown.min != "null" && parseInt(valueArray[fieldIndex]) < parseInt(dropDown.min)){
                //         flagValue = 1;
                //     }
                //     if (dropDown != undefined && dropDown.max != "null" && parseInt(valueArray[fieldIndex]) > parseInt(dropDown.max)){
                //         flagValue = 1;
                //     }
                //     if(flagValue == 0){
                //         $('#' + dropDownName).append('<option value="' + valueArray[fieldIndex] + '">' + valueArray[fieldIndex] + '</option>');
                //     }
                // }

                var reindex = valueArray.filter(val => val);
                if(reindex.length === 1){
                    $('#'+dropDownName).val(valueArray[Object.keys(valueArray)[0]]);
                    $('#'+dropDownName).trigger('change');
                }
            }
            // createTags(valueArray, dropDownName);
        }

        function createTags(data, dropDownName){
          $('#' + dropDownName).html();
          var selectStr = '<select id="DDAlertFreq" ></select>';
          $("#" + dropDownName).select2({
            createSearchChoice: function (term, data) {
              if ($(data).filter(function () {
                return this.text.localeCompare(term) === 0;
              }).length === 0) {
                return {
                  id: term,
                  text: term
                };
              }
            },
            data: data,
            placeholder: "Select value",
            allowClear:true
          });
        }

JS
);
?>
<style>
input.disabled {
    pointer-events: none;
    opacity: 0.5;
    cursor: not-allowed;
}
.readonly-select {
    background-color:#d5d5d5;
    opacity:0.5;
    border-radius:3px;
    cursor:not-allowed;
    position:absolute;
    top:0;
    bottom:0;
    right:0;
    left:0;
}
.fa-download:hover, .fa-download:active{
    color: #005580;
    cursor: pointer;
}
#notif_field{
    font-size: 14px;
}
span.badge {
    margin-left: 14px;
    float: left;
}
.collapsible span.badge {
    margin-top: 0px;
}

.summary{
    display:none;
}
.container-design {
    /*background-color: white; */
    min-height: 300px;
    max-height: 600px;
    position: relative;
    overflow: auto;
    /* margin: 5px 0; */
    border: 1px solid #ccd5d8;
    border-radius: 10px;
</style>