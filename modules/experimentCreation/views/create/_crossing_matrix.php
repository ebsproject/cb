<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders manage cross list for the experiment creation
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\grid\GridView;

use kartik\dynagrid\DynaGrid;
use marekpetras\yii2ajaxboxwidget\Box;

?>

<div id = "exp-creation-crossing-matrix-cont" class = "row col col-sm-12 col-md-12 col-lg-12">

	<?php
		$toolsButtons = [
			'reload' => function(){
				return 
					'<a href="#!" title="'.\Yii::t('app','Reset crossing matrix').'" class="text-muted fixed-color btn box-reload-btn" onclick="resetCrossingMatrix();" style="margin-left:-4px"><i class="material-icons widget-icons">loop</i></a>'.
					'<a href="#!" class="hidden" onclick="reloadCrossingMatrix();"></a>';
			},
			'save' => function(){
				return 
					'<span> Selected crosses: <b id="selected-crosses-in-matrix"></b></span>&emsp;<a href="#!" title="'.\Yii::t('app','Update crossing list').'" class="btn" data-toggle="modal" data-target="#preview-cross-list-modal" id="save-crossing-matrix">Save</a>';
			},
		];
		
		$options = [
			'id' => 'exp-creation-crossing-matrix',
			'bodyLoad' => ['/experimentCreation/create/crossing-matrix?program='.$program.'&id='.$id.'&processId='.$processId],
			'toolsTemplate' => '{save} {reload} {actions}',
			'toolsButtons' => $toolsButtons,
			'title' => '
				<div class="input-field col s6 no-margin-top">
					<i class="material-icons prefix">♀</i>
					<input class="smaller-input-field" id="matrix-search-female" name="female-parent-search" type="text" placeholder="Search female parent">
					<label for="matrix-search-female"></label>
				</div>
				<div class="input-field col s6 no-margin-top">
					<i class="material-icons prefix">♂</i>
					<input class="smaller-input-field" id="matrix-search-male" name="male-parent-search" type="text" placeholder="Search male parent">
					<label for="matrix-search-male"></label>
				</div>
			'
		];
	?>
	<?= Box::widget($options); ?>
</div>
