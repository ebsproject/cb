<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders manage cross list webform for the experiment creation
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\grid\GridView;

use kartik\dynagrid\DynaGrid;
use marekpetras\yii2ajaxboxwidget\Box;
use kartik\tabs\TabsX;

$disabled = '';

if (strpos($model['experimentStatus'],'cross list created') === false) {
    $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model'=>$model,
    'program'=>$program,
    'saveBtnLabel'=>'Next',
    'nextBtn' => $disabled,
    'btnName' => 'next-btn',
]);

$items = [
    [
        'label'=>'<i class="fa fa-plus-square"></i>'.Yii::t('app',' Add Crosses'),
        'active'=>false,
        'url'=>Url::to(['manage-crosses', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]),
    ],
    [
        'label' => '<i class="fa fa-plus-square"></i>' .  Yii::t('app', ' Cross Pattern'),
        'active' => false,
        'url' => Url::to(['cross-pattern/manage-cross-pattern', 'id' => $id, 'program' => $program, 'processId' => $processId]),
      ],
    [
        'label'=>'<i class="fa fa-edit"></i>'. Yii::t('app',' Manage Crosses'),
        'active'=> true,
        'url'=>Url::to(['manage-crosslist', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]),
        'content' => Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/_cross_list.php',[
            'id'=>$id,
            'program' => $program,
            'processId'=> $processId,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'methodTags' => $methodTags,
            'requiredFields' => $varConfigValues,
            'crossAttributes' => $crossAttributes,
            'exportRecordsThresholdValue' => $exportRecordsThresholdValue,
            'deleteCrossThresholdValue' => $deleteCrossThresholdValue
        ]),
    ]
];
?>
<div class = "white crosses-tab">
<br/>
<?php
 echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_LEFT,
    'sideways'=>true,
    'encodeLabels'=>false,
    'pluginOptions'=>[
        'enableCache'=>false
    ],
]);

$this->registerJs(<<<JS
    refresh();

    $(document).ready(function(){
        refresh();
    });

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
        refresh();
    });

    $( window ).resize(function() {
        refresh();
    });
    //Set grid table and panel sizes
    function refresh(){
        checkStatus();
        var windowHeight = $(window).height();        
        var maxHeight = windowHeight - 300;
        $("#cross-list").css("height", maxHeight);        
        $("#cross-list-grid-pjax .kv-grid-wrapper").css("height", (maxHeight));
        $('.crosses-tab').css('width', $('.tabs').width());
        $('.crosses-tab').css('height', windowHeight - 180);
        $('#cross-list').css('width', $('.crosses-tab').width() - 100);
        $('.crosses-tab').css('width', $('.tabs').width());
    }
JS);
?>
</div>

<style>
.box {
    position: relative;
    background: #ffffff;
    -webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    border-top:none;
    margin: .5rem 0 1rem 0;
    border-radius: 2px;
    width: 100%;
}
.box-header.with-border {
    border-bottom: none;
}
.box-header > .box-tools {
    top: 10px;
}
.box-header {
    padding-left: 15px;
}
.box-body {
    padding: 0px 15px 10px 15px;
    max-height: 800px;
    overflow-y: hidden;
    overflow-x: hidden;
}
.summary{
    padding-top : 15px;
    padding-right: 5px !important;
}
.panel-default{
    margin : 0px;
}
.panel-footer{
    height: 0px;
    padding-bottom : 5px;
}

.parent-box{
    margin: 0px;
    padding: 0px !important;
}
.pull-right{
    margin-right: 3px;
}
.display-none{
    display: none;
}
.row{
    margin-bottom: 10px;
}
input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
    margin: 0 0 5px 0;
}
input[type=text]:not(.browser-default){
    height:2.5rem;
}
.tabs-krajee.tab-sideways .nav-tabs > li {
    height: 20px;
    width: 150px;
    margin-bottom: 130px;
}

.tabs-krajee.tabs-left.tab-sideways .nav-tabs {
    left: -55px;
    top: 8px;
    margin-right: -75px;
    z-index: 0 !important;
}

.box-header.with-border {
    display: none;
}

.tab-content.printable{
    padding-top: 0px;
}

.tabs-left .tab-content {
    border-left: none;
}
.kv-grid-container{
    box-shadow: none;
    overflow-x: auto;
}
.crosses-tab {
    margin-top: 75px;
    padding-left: 0px;
  }

@media(max-width:900) {
    .crosses-tab {
        margin-top: 0px !important;
    }
}

</style>
