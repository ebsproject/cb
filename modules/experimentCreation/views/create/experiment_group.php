<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders view for experiment group tab for experiment creation
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use app\models\Program;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\sortable\Sortable;
use kartik\editable\Editable;

$disabled = '';
if (strpos($model->experiment_status,'experiment group specified') === false) {
    $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model'=>$model,
    'program'=>$program,
    'saveBtnLabel'=>'<i class="material-icons right">navigate_next</i>Next',
    'nextBtn' => $disabled,
    'btnName' => 'noActionBtn'
]);
echo '<div class="row"><p>'.Yii::t('app', 'Create and specify the experiment groups.').Html::a('<i class="material-icons right">add</i> '.\Yii::t('app', 'Create'), 
['#'], 
[
    'class' => 'btn btn-primary create-exptgrp-class pull-right',
    'style' => 'margin-right:10px;',
    'title' => \Yii::t('app', 'Add new experiment group'),
    'id'=>'create-exptgrp-btn',
    'data-toggle' => 'modal',
    'data-target' => '#create-exptgrp-modal',
]).'</p></div>';
echo '<div class="col col-sm-12 view-entity-content">';
?>
    <?php
            
            echo '<div class="col col-md-6" id="exp-grp-panel"><div class="row">'; 
            if(count($experimentTables) > 0){
                
                    echo '<ul class="collapsible popout collapsible-accordion" data-collapsible="accordion" style="margin-top:-10px;">';
                    foreach($experimentTables as $table){
                    echo '<li class="active">
                        <div class="collapsible-header active group-tab-header" id="'.$table['expGrpId'].'"><div class="col col-sm-9">'.Editable::widget([
                            'name'=>'experiment_group_name_value', 
                            'asPopover' => true,
                            'value' => $table['experimentGrpName'],
                            'formOptions' => ['action' => ['/experimentCreation/create/update-record-column-expt-grp?type=experiment_group_name&id='.$table['expGrpId']]],
                            'size'=>'sm',
                            'options' => ['class'=>'form-control experiment_groupRole-fld', 'placeholder'=>'Not Set']
                        ]).Yii::t('app', $table['experimentGrp']).'</div><div class="col col-sm-3"><span class="new badge">'.Yii::t('app', $table['expGrpType']).$table['expGrpTypePattern'].'</div></span>'.Html::a('<i class="fa fa-close"></i>',
                        '#',
                        [
                            'class'=>'delete-experiment-group',
                            'title' => 'Delete experiment group',
                            'data-experiment_group_id' => $table['expGrpId'],
                            'data-experiment_group_name' => $table['experimentGrpName']
                        ]
                    ).'</i></div>
                        <div class="collapsible-body">
                        <dl class="dl-horizontal">
                        <div class="row" style="margin-top:10px;">
                        ';
                            echo '<div class="box-header with-border row">'.
                                Yii::t('app', 'Experiments already included in the group.')
                                .'</div>'.Sortable::widget([
                                'connected'=>'exp-connected-'.$table['expGrpId'],
                                'type'=>'list',
                                'id'=>'included-exps-'.$table['expGrpId'],
                                'options'=>['class'=>'white z-depth-1-1 exp-grp-connected'],
                                'itemOptions'=>['class'=>'exptGrpId-'.$table['expGrpId']],
                                'pluginEvents' => [
                                    'sortupdate' => 'function(e) { 
                                        e.preventDefault();
                                        e.stopImmediatePropagation();
                                        updateSort(this.id, "set-experiment");
                                    }',
                                ],
                                'items'=>$table['expGrpexp']
                            ]);
                            echo '
                        </div>
                        </dl>
                    </div>
                </li>';
            }
            echo '</ul></div></div>';
            echo '<div class="card-panel card-left-panel grey lighten-4 col col-md-6" id="exps-panel">';
                echo '<div class="box-header with-border row">'.
                    Yii::t('app', 'Displays all the experiments in the same Project, evaluation year, and evaluation season. These are not included in the current group.')
                    .'</div>';
            foreach($experimentTables as $table){
                echo '<div class="row exps-notInc-class hidden" id="exps-'.$table['expGrpId'].'">'.Sortable::widget([
                    'connected'=>'exp-connected-'.$table['expGrpId'],
                    'type'=>'list',
                    'id'=>'excluded-exps-'.$table['expGrpId'],
                    'options'=>['class'=>'white z-depth-1-1'],
                    'itemOptions'=>['class'=>'exptGrpId-'.$table['expGrpId']],
                    'pluginEvents' => [
                        'sortupdate' => 'function(i,e) {
                            updateSort(this.id, "notSet-experiment");
                        }',
                    ],
                    'items'=>$table['notExpGrpexp']
                ]).'</div>';   
            } 
            echo '</div>';
            } else {
        ?>
            <div class="col col-sm-12">
            <div class="card">
                <div class="card-content">
                    <i><?= \Yii::t('app', 'No results found. Click the Create button to create experiment group.') ?></i>
                </div>
            </div>
            </div>
        <?php
                }
    ?>
</div>
<!-- Create a new group -->
<?php 
    $form = ActiveForm::begin([
        'enableClientValidation'=>false,
        'id'=>'create-exptgrp-form',
        'action'=>['/experimentCreation/create/specify-experiment-groups','id'=>$id,'program' => $program, 'processId'=>$processId],
        'fieldConfig' => function($model,$attribute){
            if(in_array($attribute, ['abbrev','name','remarks'])){
                return ['options' => ['class' => 'input-field form-group col-md-6']];
            }else if(in_array($attribute, ['experiment_group_type','type_pattern'])){
                return ['options' => ['class' => 'col-md-6']];
            }else{
                return ['options' => ['class' => 'input-field form-group col-md-12']];
            }
        }
    ]); 
?>
<?php
    Modal::begin([
        'id' => 'create-exptgrp-modal',
        'header' => '<h4><i class="material-icons">add</i> '.\Yii::t('app','Create').' '. \Yii::t('app','Experiment Group').'</h4>',
        'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            Html::a(\Yii::t('app','Submit').'<i class="material-icons right">send</i>',
                ['#'],
                [
                    'class'=>'btn btn-primary waves-effect waves-light',
                    'url'=>'#',
                    'id'=>'save-exptgrp-btn'
                ]).'&emsp;',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static', 'tabindex' => false]
    ]);
?>
<?= 
    '<div class="modal-notif"></div>'. //modal notification field
    
    $form->field($experimentGroupModel, 'name')->textInput([
        'maxlength' => true, 
        'id' => 'exptgrp-display-name',
        'autofocus' => 'autofocus',
        'title'=>Yii::t('app','Name identifier of the experiment group'),
        'onkeyup' => 'js: $("#exptgrp-display-name").val(this.value.charAt(0).toUpperCase() + this.value.slice(1)); $("#exptgrp-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase()); $(".field-exptgrp-abbrev > label").addClass("active");'
    ]).
    $form->field($experimentGroupModel, 'abbrev')->textInput([
        'maxlength' => true, 
        'id'=>'exptgrp-abbrev',
        'title'=>Yii::t('app','Short name identifier or abbreviation of the experiment group'),
        'oninput' => 'js: $("#exptgrp-abbrev").val(this.value.replace(/[^A-Z0-9]/ig, "_").toUpperCase());'
    ]).
    $form->field($experimentGroupModel, 'experiment_group_type')->widget(Select2::classname(), [
        'data' => ['Decision Block' => 'Decision Block', 'Analysis Block' => 'Analysis Block'],
        
        'options' => ['placeholder' => Yii::t('app','Select type'), 'id'=>'experiment_group_type_field_id'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]).
    '<div id="type_pattern_field" class="hidden">'.$form->field($experimentGroupModel, 'type_pattern')->widget(Select2::classname(), [
        'data' => ['X Pattern' => 'X Pattern', 'Diagonal Pattern' => 'Diagonal Pattern', 'Row Pattern' => 'Row Pattern', 'Column Pattern'=>'Column Pattern'],
        'options' => ['placeholder' => Yii::t('app','Select pattern')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]).'</div>';
?>
<?=
    //advanced options
    '<div class="col col-md-12" style="margin-top:20px;">
        <a class="collapse-toggle collapsed" href="#advance-option" data-toggle="collapse" aria-expanded="false" style="margin-top:20px";>Advanced options...</a>'.
        '<div id="advance-option" class="panel-collapse collapse" >'.
        $form->field($experimentGroupModel, 'description')->textarea([ 'class'=>'materialize-textarea', 'title'=>Yii::t('app','Description about the experiment group')]).
        $form->field($experimentGroupModel, 'remarks')->textarea(['class'=>'materialize-textarea', 'title'=>Yii::t('app','Additional details about the experiment group'),'row'=>2]).
        '</div>
    </div>' 
?>
<?php Modal::end(); ?>
<?php ActiveForm::end(); 
    //delete experiment group modal
    Modal::begin([
        'id' => 'delete-experiment-group-modal',
        'header' => '<h4 id="delete-experiment-group-label"></h4>',
        'footer' =>
            Html::a(\Yii::t('app','Cancel'),['#'],['data-dismiss'=>'modal']).'&nbsp;&nbsp'.
            Html::a(\Yii::t('app','Confirm').'<i class="material-icons right">send</i>',['#'],['class'=>'btn btn-primary waves-effect waves-light delete-experiment-group-btn','url'=>'#','id'=>'delete-btn']).'&nbsp;&nbsp',
        'options' => ['data-backdrop'=>'static']
    ]);
?>
<?= \Yii::t('app', '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete <span id="experiment-group-delete"></span>. Click confirm to proceed.') ?>
<?php Modal::end(); ?>

<?php
    $addExptGrp = Url::to(['/experimentCreation/create/get-experiments']);
    $addSelectedToGrp = Url::to(['/experimentCreation/create/add-selected-to-group']);
    $nextUrl = Url::to(['create/specify-occurrences', 'id'=>$id, 'program'=>$program]);
    $addMovedToGrp = Url::to(['/experimentCreation/create/add-moved-to-group', 'id'=>$id]);
    $deleteUrl = Url::to(['create/delete-experiment-group', 'id'=>$id,'program'=>$program, 'processId'=>$processId]);
    $this->registerJs(<<<JS
    var oldExpIdMovedStr = null;
    var oldExpGrpIdMoved = null;
    var oldExpChanged = null;

    var newExpIdMovedStr = null;
    var newExpGrpIdMoved = null;
    var newExpChanged = null;

    $(document).ready(function(){
        $(".group-tab-header").each(function(){
            if($("#"+this.id).hasClass('active')){
                $("#exps-"+this.id).removeClass('hidden');
            }
        });
    });

    var exptGrpIdCurr = '';

    $("#save-exptgrp-btn").on( "click", function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $("#create-exptgrp-form").submit();

        $('.modal-notif').html('<div class="progress"><div class="indeterminate"></div></div>');

        $("#create-exptgrp-form").submit(function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $(".form-group").removeClass("has-error"); //remove error class
            $(".help-block").html(""); //remove existing error messages
            $('.modal-notif').html("");

            var form_data = $("#create-exptgrp-form").serialize();
            var action_url = $("#create-exptgrp-form").attr("action");

            $.ajax({
                type:"POST",
                dataType: 'json',
                url: action_url,
                data: form_data,
                success: function(data) {
                    $('.modal-notif').html('');
                    if(data.success == true){//data saved successfully 
                        location.reload();
                    }else{ //validation errors occurred
                        $('.modal-notif').html('');
                        $.each(data.error, function(ind, vl) { //show errors to user
                            $(".field-season-"+ind).addClass("has-error");
                            $(".field-season-"+ind).find(".help-block").html(vl[0]);
                        });
                    }
                },
                error: function(){
                    $('.modal-notif').html('');
                    $('.modal-notif').html('<div id="w1-error-0" class="alert error"><div class="card-panel"><i class="fa fa-times"></i> There were problems while saving experiment group.</div></div>');
                }
            });            
            return false;
        });    
    });

    $("select" ).on( "change", function(){
        if(this.id == 'experiment_group_type_field_id'){
            var typeValue = $('#experiment_group_type_field_id').val();
            if(typeValue == 'Decision Block'){
                $('#type_pattern_field').removeClass('hidden');
            } else {
                $('#type_pattern_field').addClass('hidden');
            }
        }
    });

    $(".group-tab-header").click(function() {
        $(".exps-notInc-class").each(function(){
            $(this).addClass('hidden').siblings();
        });
        $("#exps-"+this.id).removeClass('hidden');
    });

    function updateSort(id, className){
        var element = document.getElementById(id);
        if(className == "set-experiment"){
            $("#"+id).find("li").each(function(i,e){
                $(this).find('.set-Field').removeClass('hidden');
            });
        } else {
            $("#"+id).find("li").each(function(i,e){
                $(this).find('.set-Field').addClass('hidden');
                //remove from experiment group
            });
        }
    }

    //check if the program is being moved
    $("ul.sortable").on('dragstart', 'li', function (e) {
        var sortableId = $(this).parent().attr('id');
        var expIdStr = $(this).find('.expId-class').attr('id');
        var expArr = (expIdStr).split('-'); 

        oldExpIdMovedStr = expIdStr;
        oldExpChanged = sortableId;
    });

    //check if program changed role
    $("ul.sortable").on('dragend', 'li', function (e) {
        var sortableId = $(this).parent().attr('id');
        var expIdStr = $(this).find('.expId-class').attr('id');
        var expArr = (expIdStr).split('-');
        
        newExpIdMovedStr = expIdStr;
        newExpChanged = sortableId;
        if(oldExpChanged != newExpChanged){
            // add to experiment group
            if(newExpChanged.includes("included-exps")){
                $.ajax({
                    type:"POST",
                    dataType: 'json',
                    url: '$addMovedToGrp',
                    data: {
                        dataStr : newExpIdMovedStr,
                        toDo : 'add'
                    },
                    success: function(data) {
                        
                    },
                    error: function(){
                        
                    }
                });   
            } else {
                $.ajax({
                    type:"POST",
                    dataType: 'json',
                    url: '$addMovedToGrp',
                    data: {
                        dataStr : newExpIdMovedStr,
                        toDo : 'remove'
                    },
                    success: function(data) {
                        
                    },
                    error: function(){
                        
                    }
                });   
            }
        }
    });

    //delete experiment
    $(".delete-experiment-group").click(function() {
        var obj = $(this);
        var experiment_label = obj.data('experiment_group_name');
        var experiment_id = obj.data('experiment_group_id');

        $('#delete-experiment-group-label').html('<i class="fa fa-close"></i> Delete ' + experiment_label);
        $('#experiment-group-delete').html(experiment_label);
        document.getElementById("delete-btn").setAttribute("data-experiment_group_id",experiment_id);
        $('#delete-experiment-group-modal').modal('show');
    });

    $(document).on('click', '.delete-experiment-group-btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var obj = $(this);
        var experiment_group_id = obj.data('experiment_group_id');
        var url = '$deleteUrl';

        $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          data: {
            experiment_group_id: experiment_group_id
          },
          success: function(response) {
          }
        });
    });
    //end delete experiment
JS
);
?>

<style>
span.badge{
    float: right;
}
.table {
    margin-bottom: 10px;
}
.kv-editable, .kv-editable-value {
    display: initial;
}
.experiment_role.kv-editable-link {
    color: #428bca;
    background: none;
    border: none;
    float: right;
    padding: 2px 1px;
    text-decoration: none;
    cursor: pointer;
    border-bottom: 1px dashed;
}
label.label-role {
    display: unset;
    max-width: 100%;
    line-height: 22px;
    font-weight: bold;
    margin-right:5px;
}
li.disabled
{
    pointer-events: all;
}
.sortable li.disabled {
    opacity:1.0;
}
.sortable {
    -moz-user-select: none;
    padding: 0;
    border-radius: 4px;
    border: 1px dashed #ddd;
}
</style>