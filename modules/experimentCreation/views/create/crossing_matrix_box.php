<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders crossing matrix for experiment creation
*/
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

$session = Yii::$app->session;
$session->set('reload-crossing-matrix-'.$id, false);
$session->set('reset-crossing-matrix-'.$id, false);
?>

<div id="crossing-matrix-container" style=" margin: '200px' auto '100px' auto"></div>

<?php
// preview modal
Modal::begin([
	'id' => 'preview-cross-list-modal',
	'header' => '<h4 id="preview-crossing-matrix-modal-header">Update Cross list</h4>',
	'footer' =>
		Html::a(\Yii::t('app','Cancel'),'#',['data-dismiss'=>'modal','class'=>'cancel-btn']).'&emsp;'
		.Html::a(\Yii::t('app','Confirm') . '<i class="material-icons right">send</i>','#',['class'=>'btn btn-primary waves-effect waves-light confirm-update-cross-list', 'title'=>'Confirm update cross list']).'&emsp;'
		.'&nbsp;'.Html::a(\Yii::t('app','Add more'),'#',['id' => 'add-more-btn','data-dismiss'=>'modal','class'=>'btn black-text grey lighten-2 waves-effect waves-light modal-close','title'=>'Add more crosses','style' =>'display:none'])
		.'&nbsp;&nbsp;'.Html::a(\Yii::t('app','Manage cross list'),Url::to(['manage-crosslist', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]),['id' => 'manage-cross-list-btn','title'=>'Manage cross list','class'=>'btn btn-primary waves-effect waves-light modal-close','style' =>'display:none']).
		'&emsp;',
	'closeButton' => [
		'class' => 'hidden'
	],
	'size' =>'modal-lg',
	'options' => ['data-backdrop'=>'static'],
	]);
	echo '<div class="preview-cross-list-modal-body"></div>';
Modal::end();

$preloader = '
	<div class="preloader-wrapper small active" style="width:20px;height:20px;">
      <div class="spinner-layer spinner-green-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
              <div class="circle"></div>
          </div>
      </div>
  </div>
';
$preloader = trim(preg_replace('/\s+/', ' ', $preloader));
$previewUpdateCrossListUrl = Url::to(['/experimentCreation/create/preview-crossing-matrix']);
$confirmUpdateCrossListUrl = Url::to(['/experimentCreation/create/confirm-update-crossing-matrix']);
$updateCrossingMatrixUrl = Url::to(['/experimentCreation/create/update-crossing-matrix','experimentId'=>$id]);
$getSelectedCrossesCountUrl = Url::to(['/experimentCreation/create/get-no-of-selected-crosses']);
$reloadCrossingMatrixUrl = Url::to(['/experimentCreation/create/reload-crossing-matrix']);
$resetCrossingMatrixUrl = Url::to(['/experimentCreation/create/reset-crossing-matrix']);

if(empty($data['data']) && isset($data['data'])){
	echo '<p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'All parents have the same parent role. Please update first to proceed.').'</span></p>';
?>
<script type="text/javascript">
	$('.box-reload-btn').addClass('disabled');
	$('#save-crossing-matrix').addClass('disabled');
</script>
<?php
}else{
?>

<script type="text/javascript">
	var matrix = null;
	var selectedColor = '#00838f'; // selected color
	var unselectedColor = '#7cb342'; // unselected color
	var selectedVal = '✓'; // selected value
	var unselectedVal = ''; // unselected value
	var colorPoint = null; // color to assign in plot
	var valPoint = null; // value to assign in plot
	// for crossing matrix dimension
	var windowHeight = $(window).height();
    var maxHeight = windowHeight - 283;
	var windowWidth = $(window).width();
	var maxWidth = windowWidth - 500;
	//crossing matrix
	var dataArr = JSON.parse('<?= json_encode($data) ?>'); 
	var data = dataArr.data;
	var xLabels = dataArr.xLabels;
	var yLabels = dataArr.yLabels;
	var xCount = dataArr.xCount;
	var yCount = dataArr.yCount;
	var xMaxLength = dataArr.xMaxLength;

	$(document).ready(function(){		

		// set dimension of box
        $('.box-body').css('max-height', maxHeight);
        $('#crossing-matrix-container').css('height', maxHeight+'px');

		renderCrossingMatrix();
		loadNoOfSelectedCrosses();

		// save crossing matrix to temp table
		$(document).on('click', '#save-crossing-matrix', function(e){
			e.preventDefault();
			e.stopImmediatePropagation();

			$('#add-more-btn').css('display','none');
            $('#manage-cross-list-btn').css('display','none');
            $('.confirm-update-cross-list').css('display','');
            $('.cancel-btn').css('display','');

            var header = 'Update Cross List';
        	$('#preview-crossing-matrix-modal-header').html(header);

			var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
			$('.preview-cross-list-modal-body').html(loading);

			setTimeout(function(){
				// retrieve selected crosses
				$.ajax({
		            url: '<?= $previewUpdateCrossListUrl?>',
		            data: {
		                id: '<?= $id?>',
		            },
		            type: 'POST',
		            async: true,
					dataType: 'json',
		            success: function(data) {
		                $('.preview-cross-list-modal-body').html(data);
		            },
		            error: function(){
		                $('.preview-cross-list-modal-body').html('<i>There was a problem while loading information.</i>'); 
		            }
		        });
			},10);
		});

		// update crossing list
		$(document).on('click', '.confirm-update-cross-list', function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
			$('.preview-cross-list-modal-body').html(loading);

			setTimeout(function(){
				// retrieve selected crosses
				$.ajax({
		            url: '<?= $confirmUpdateCrossListUrl?>',
		            data: {
		                id: '<?= $id?>',
		            },
		            type: 'POST',
		            async: true,
		            success: function(data) {
		            	var header = '<i class="material-icons green-text left">check_circle</i> Success!';
		            	$('#preview-crossing-matrix-modal-header').html(header);

		            	var term = (data > 1) ? 'crosses' : 'cross';
		            	var message = '<p>Successfuly updated crossing list with <b>' + data + '</b> '+term+'.</p>';

		                $('.preview-cross-list-modal-body').html(message);

		                $('#add-more-btn').css('display','');
		                $('#manage-cross-list-btn').css('display','');
		                $('.confirm-update-cross-list').css('display','none');
            			$('.cancel-btn').css('display','none');

            			$('.box-reload-btn').attr('disabled', 'disabled');
		            },
		            error: function(){
		                $('.preview-cross-list-modal-body').html('<i>There was a problem while loading information.</i>'); 
		            }
		        });
			},10);
		});

		//enable next button when add more crosses is clicked
		$("#add-more-btn").on('click', function(){
			$('#next-btn').removeAttr('disabled');
		});
		
		// search female parent
		$("input[name='female-parent-search']").on("input click", function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var keyword = $(this).val().trim();
			$(".yaxis-header").each(function() {
				var source = $(this).html();
				source = source.replace(/<xx>/g,'');
				if(keyword != ''){
					keyword = keyword.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
					var pattern = new RegExp("("+keyword+")", "gi");
					source = source.replace(pattern, "<xx>$1</xx>");
					source = source.replace(/(<xx>[^<>]*)((<[^>]+>)+)([^<>]*<\/xx>)/,"$1</xx>$2<xx>$4");
				}
				$(this).html(source);
			});
		});
		// search male parent
		$("input[name='male-parent-search']").on("input click", function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
		  	var keyword = $(this).val().trim();
		  	$(".xaxis-header").each(function() {
				var source = $(this).html();
				source = source.replace(/<xx>/g,'');
				if(keyword != ''){
					keyword = keyword.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
					var pattern = new RegExp("("+keyword+")", "gi");
					source = source.replace(pattern, "<xx>$1</xx>");
					source = source.replace(/(<xx>[^<>]*)((<[^>]+>)+)([^<>]*<\/xx>)/,"$1</xx>$2<xx>$4");
				}
				$(this).html(source);
			});
		});
	});

	// render crossing matrix
	function renderCrossingMatrix(){
		setTimeout(function(){
			matrix = new Highcharts.chart('crossing-matrix-container',{
				chart: {
					marginTop: 0,
					marginTop: 70,
					plotBorderWidth: 1,
					type: 'heatmap',
					plotBorderWidth: 1,
					plotBorderRadius: 5,
					animation: false
				},
				title: {
					text: ''
				},
				xAxis: {
					opposite: true,
					categories: xLabels,
					title:{
						text:"Male",
						
					},
					labels: {
						formatter: function() {
							var obj = this.value;
							var pos = this.pos;

							return '<span class="xaxis-header" title="'+this.value.val+'" onclick="clickHeader(\''+this.value.id+'\',\''+pos+'\',\''+0+'\')">' + this.value.val + '</span>';
						},
						useHTML: true,
						style: {
							cursor: 'pointer'
						}
					},
					events:{
						setExtremes: function (e) {
		                    setTimeout(function(){
		                    	$("#matrix-search-male").click();
		                    },10);
		                }
	                }
				},
				yAxis: {
					categories: yLabels,
					title:{
						text:"Female"
					},
					events: {
		                setExtremes: function (e) {
		                    setTimeout(function(){
		                    	$("#matrix-search-female").click();
		                    },10);
		                }
		            },			
					labels: {
						formatter: function() {
							var obj = this.value;
							var pos = this.pos;

							return '<span class="yaxis-header" title="'+this.value.val+'" onclick="clickHeader(\''+this.value.id+'\',\''+pos+'\',\''+1+'\')">' + this.value.val + '</span>';

						},
						useHTML: true,
						style: {
							cursor: 'pointer'
						}
					}
				},
				legend: {
					enabled: false
				},
				exporting: {
					enabled: false
				},
				credits: {
					enabled: false
				},
				tooltip: {
					outside: true,
				    useHTML: true,
				    backgroundColor: "rgba(246, 246, 246, 1)",
				    style: {
				      opacity: 1,
				      background: "rgba(246, 246, 246, 1)"
				    },
					formatter: function () {
						var cross = this.series.yAxis.categories[this.point.y].val + '/' +this.series.xAxis.categories[this.point.x].val;
						var invalidLabel = '';
						if(this.series.yAxis.categories[this.point.y].entno == this.series.xAxis.categories[this.point.x].entno ||
							this.series.yAxis.categories[this.point.y].product_id == this.series.xAxis.categories[this.point.x].product_id){
							invalidLabel = '<b style="color:#b71c1c">Invalid cross</b><br/>';
						}

						return invalidLabel+'<b>Cross: </b>' + cross +
						'<br/><b>Female</b>: ' + this.series.yAxis.categories[this.point.y].val +
						'<br/><b>Female Entno</b>: ' + this.series.yAxis.categories[this.point.y].entno +
						'<br/><b>Male</b>: ' + this.series.xAxis.categories[this.point.x].val +
						'<br><b>Male Entno:</b> ' + this.series.xAxis.categories[this.point.x].entno;
					}
				},
				plotOptions: {
					series: {
						turboThreshold: Number.MAX_VALUE,
						dataLabels: {
							crop: true,
							allowOverlap: true
						},
						cursor: 'pointer',
						events: {
							click: function (e) {
								if(e !== undefined && (e.point.femaleentryid != e.point.maleentryid)){
									changeColorForHeatMap(e.point.femaleentryid, e.point.maleentryid, this);
								}
							}
						}
					}
				},
				scrollbar : { enabled : true },
				series: [{
					name: 'Crosses',
					borderWidth: 1,
					borderRadius: 5,
					borderColor: '#fff',
					color: '#7cb342',
					data: data,
					dataLabels: {
						enabled: true
					}
				}]
			}, function(chart) {
				updateDimension(chart, xCount, yCount);
			});
		},50);	
	}

	// dynamically update dimension of matrix
	function updateDimension(matrix, xCount, yCount){
		var boxWidth = $('.highcharts-point ').width();
		var boxHeight = $('.highcharts-point ').height();
		var modifyHeight = false;
		var modifyWidth = false;

		if(boxWidth < 25){
			var modifyWidth = true;
		}else{
			var modifyWidth = false;
		}

		if(boxHeight < 23){
			var modifyHeight = true;
		}else{
			var modifyHeight = false;
		}

		var xDiff = 10;
		var yDiff = 10;
		
		if(yCount > 10){
			yDiff = (maxHeight - 80)/25;
			yDiff = Math.floor(yDiff);
		}
		else if(yCount < 10 && yCount > 2){
			yDiff = yCount - 2;
		}else if(yCount < 3){
			yDiff = yCount;
		}

		if(xCount > 10){
			xDiff = maxWidth/25;
			xDiff = Math.floor(xDiff);
		}
		else if(xCount < 10 && xCount > 2){
			xDiff = xCount - 2;
		}else if(xCount < 3){
			xDiff = xCount;
		}

		if(yDiff > yCount){
			yDiff = yCount-2;
		}

		if(xDiff > xCount){
			xDiff = xCount-2;
		}

        if(modifyHeight && modifyWidth){
        	// modify both height and width
			matrix.xAxis[0].update({
				max: xDiff,
				min:0,
				scrollbar: {
		        	enabled: false
		     	},
			});

			matrix.yAxis[0].update({
				max: yDiff,
				min: 0,
				scrollbar: {
		        	enabled: true
		     	},
			});
		}else if(modifyHeight && !modifyWidth){
			// modify height only
			matrix.yAxis[0].update({
				max: yDiff,
				min: 0,
				scrollbar: {
		        	enabled: true
		     	},
			});
		}else if(!modifyHeight && modifyWidth){
			// modify width only
			matrix.xAxis[0].update({
				min: 0,
				max: xDiff,
				scrollbar: {
		        	enabled: false
		     	},
			});
		}else if (!modifyHeight && !modifyWidth){
			$('.highcharts-scrollbar').css('display','none');
		}

		if(boxWidth <= 77){

			var margin = 80;
			if(xMaxLength > 10 && xMaxLength < 15){
				margin = 100;
			}else if(xMaxLength >= 15 && xMaxLength < 21){
				margin = 110;
			}else if(xMaxLength >= 21 && xMaxLength < 24){
				margin = 120;
			}else if(xMaxLength >= 24 && xMaxLength < 26){
				margin = 130;
			}else if(xMaxLength >= 26 && xMaxLength < 28){
				margin = 150;
			}else if(xMaxLength >= 28 && xMaxLength < 32){
				margin = 160;
			}else if(xMaxLength >= 32 && xMaxLength < 37){
				margin = 180;
			}else if(xMaxLength <= 10){
				margin = 80;
			}
			else {
				margin = 220;
			}
			// add margin
			matrix.update({
				chart: {
					marginTop: margin
				}
			});
		}
	}

	// when header is clicked, select or unselect all
	function clickHeader(entryId, pos, type){
		matrix.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');

		setTimeout(function(){
			// update temporary table
			$.ajax({
				url: '<?= $updateCrossingMatrixUrl ?>',
				data: {
					type: type,
					pos: pos,
					method: 'bulk'
				},
				type: 'POST',
				async: true,
				success: function(data) {
				reloadCrossingMatrix();
					$('.box-reload-btn').removeAttr('disabled');
				},
				error: function(){
				}
			});
		},100);
	}

	// change color of heatmap upon select
	function changeColorForHeatMap(femaleEntryId, maleEntryId, series){
		$('.box-reload-btn').removeAttr('disabled');

		var dataPoints = series.data;
		colorPoint = selectedColor;
		valPoint = selectedVal;

		for(var i=0;i<dataPoints.length;i++){

			if(dataPoints[i].femaleentryid == femaleEntryId && dataPoints[i].maleentryid == maleEntryId && (dataPoints[i].value !== null)){
				var currentColor = dataPoints[i].color;

				// not selected
				if(currentColor == selectedColor){
					colorPoint = unselectedColor;
					valPoint = unselectedVal;
				}
				dataPoints[i].update({
					color: colorPoint,	 
					value: valPoint
				});
				break;
			}
		}

		setTimeout(function(){
			$.ajax({
	            url: '<?= $updateCrossingMatrixUrl ?>',
	            data: {
	                femaleId: femaleEntryId,
	                maleId: maleEntryId,
	                value: valPoint
	            },
	            type: 'POST',
	            success: function(data) {
	            	loadNoOfSelectedCrosses();
	            },
	            error: function(){
	            }
	        });
		}, 10);
	}

	// load no of selected crosses
	function loadNoOfSelectedCrosses(){
		// retrieve no of selected crosses
		$.ajax({
            url: '<?= $getSelectedCrossesCountUrl?>',
            type: 'POST',
            data:{
            	id: '<?= $id ?>'
            },
            success: function(data) {
                $('#selected-crosses-in-matrix').html(data);
            },
            error: function(){
            }
        });
	}

	// reload crossing matrix with temporary data
	function reloadCrossingMatrix(){
		// retrieve tempoary crossing list
		$.ajax({
            url: '<?= $reloadCrossingMatrixUrl?>',
            type: 'POST',
            data:{
            	id: '<?= $id ?>'
            },
            success: function(data) {
            	$('#exp-creation-crossing-matrix').box('reload');
            },
            error: function(){
            }
        });
	}

	// reset crossing matrix with temporary data
	function resetCrossingMatrix(){
		// reset crossing matrix with actual data
		$.ajax({
            url: '<?= $resetCrossingMatrixUrl?>',
            type: 'POST',
            data:{
            	id: '<?= $id ?>'
            },
            success: function(data) {
            	$('#exp-creation-crossing-matrix').box('reload');
            },
            error: function(){
            }
        });
	}
	
</script>

<?php } ?>
