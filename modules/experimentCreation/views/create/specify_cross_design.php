<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Renders design tab for the nursery experiment creation
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\tabs\TabsX;
use kartik\sortable\Sortable;
use yii\web\JqueryAsset;
\yii\jui\JuiAsset::register($this);


$disabled = '';
if (strpos($model['experimentStatus'],'design generated') === false) {
    $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' =>$processId,
    'model' => $model,
    'program' => $program,
    'saveBtnLabel' => 'Next',
    'nextBtn' => $disabled,
    'btnName' => 'next-btn'
]);

$itemsEntryOrder = [
    [
        'label'=>Yii::t('app','<i class="fa fa-plus-square"></i> Entry order'),
        'active' => ($tabDesign == 'design-entry-order') ? true : false,
        'linkOptions'=>[
            'class'=>'pa-tabs pa-entry-order-tab',
            'data-url'=>Url::to(['/experimentCreation/entry-order/manage-entry-order', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]),
        ]
    ]
];
$items = [
    [
        'label'=>Yii::t('app','<i class="fa fa-plus-square"></i> Add blocks'),
        'active' => ($tab == 'add-blocks') ? true : false,
        'linkOptions'=>[
            'id'=>'add-blocks-tab',
            'class'=>'pa-tabs pa-add-blocks-tab pa-add-blocks-steps-tab'
        ]
    ],
    [
        'label' => Yii::t('app','<i class="fa fa-list-alt"></i> Assign entries'),
        'active' => ($tab == 'assign-entries') ? true : false,
        'linkOptions'=>[
            'id'=>'assign-entries-tab',
            'class'=>'pa-tabs pa-assign-entries-tab  pa-add-blocks-steps-tab'
        ]
    ],
    [
        'label' => Yii::t('app','<i class="fa fa-edit"></i> <span style="font-size:95%">Manage blocks</span>'),
        'active' => ($tab == 'manage-blocks') ? true : false,
        'linkOptions'=>[
            'id'=>'manage-blocks-tab',
            'class'=>'pa-tabs pa-manage-blocks-tab  pa-add-blocks-steps-tab'
        ]
    ],
    [
        'label' => Yii::t('app','<i class="fa fa-search"></i> Overview'),
        'active' => ($tab == 'overview') ? true : false,
        'linkOptions'=>[
            'id'=>'overview-tab',
            'class'=>'pa-tabs pa-overview-tab  pa-add-blocks-steps-tab'
        ]
    ]
];

$checked = '';
if($tabDesign == 'design-entry-order'){
    $checked = 'checked';
}
echo '<div class="row switch hidden" id="arrangement-option">
    <span class="new badge">Select arrangement</span> &nbsp;&nbsp;
    <label>
        Add Blocks
        <input type="checkbox" id="change-design-switch-id" name="DesignArrangement" disabled '.$checked.'>
        <span class="lever"></span>
        Entry Order
    </label>
</div>';
echo '<div id="planting-arrangement-view">';
if($tabDesign == 'design-entry-order'){
    echo TabsX::widget([
        'items' => $itemsEntryOrder,
        'position' => TabsX::POS_LEFT,
        'encodeLabels' => false,
        'pluginOptions'=>[
            'enableCache' => false
        ],
        'containerOptions'=>[
            'id'=>'pa-entry-order-tabsX'
        ],
        'pluginEvents' => [
            'tabsX.beforeSend' => 'function() { $(".pa-tabs").addClass("disabled"); }',
            'tabsX.success' => 'function() { $(".pa-tabs").removeClass("disabled"); }',
        ]
    ]);
} else {
    echo TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_LEFT,
        'encodeLabels' => false,
        'pluginOptions'=>[
            'enableCache' => false
        ],
        'containerOptions'=>[
            'id'=>'pa-systematic-arrangement-tabsX',
            'class' => $experimentType == 'Observation' || $experimentUsesCrossPattern? 'hidden': ''
        ],
        'pluginEvents' => [
            'tabsX.beforeSend' => 'function() { $(".pa-tabs").addClass("disabled"); }',
            'tabsX.success' => 'function() { $(".pa-tabs").removeClass("disabled"); }',
        ]
    ]);
}

echo '</div>';
//Modal for change design
Modal::begin([
    'id' => 'pa-design-confirm-modal',
    'header' => '<h4><div id="entry-list-confirm-header"><i class="left material-icons orange-text">warning</i>Change Arrangement</h4>',
    'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-change-pa-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="confirm-change-pa-btn" class="btn btn-primary waves-effect waves-light confirm-delete-btn">Confirm<i class="material-icons right">send</i></a>',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
'closeButton' => ['class' => 'hidden'],
]);
echo '<div id="pa-design-modal-confirm"></div>';
echo '<div id="pa-design-modal-confirm-notif" class="hidden"></div>';
Modal::end();

$this->registerCssFile("@web/css/box.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'box-css');

$checkExptStatusUrl = Url::to(['/experimentCreation/create/check-experiment-status','id'=>$id,'program'=>$program,'processId'=>$processId]);
$checkPAUpdatedUrl = Url::to(['/experimentCreation/planting-arrangement/check-pa-updated','id'=>$id]);

$this->registerJsFile('https://code.highcharts.com/stock/highstock.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('https://code.highcharts.com/modules/heatmap.js',['position' => \yii\web\View::POS_END]);
$checkStatusUrl  = Url::to(['create/check-status','id'=>$id, 'action'=>'cross-design']);
$checkPreviousRecords = Url::to(['entry-order/check-design-type','id'=>$id]);
$changePreviousRecords = Url::to(['entry-order/change-design-type','id'=>$id]);
$experimentStatus = $model['experimentStatus'];

$currentUrlChange = Url::to(['/experimentCreation/create/specify-cross-design','id'=>$id,'program'=>$program,'processId'=>$processId, 'changed'=>true]);
Yii::$app->view->registerJs(
    "
      checkStatusUrl = '" . $checkStatusUrl . "',
      experimentId = '" . $id . "',
      requiredFieldJson = 'null',
      checkStatValue = 'design generated'
      ;",
    \yii\web\View::POS_HEAD
  );

$addBlockUrl = Url::to(['/experimentCreation/planting-arrangement/add-block', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$assignEntriesUrl = Url::to(['/experimentCreation/planting-arrangement/assign-entries', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$manageBlocksUrl = Url::to(['/experimentCreation/planting-arrangement/manage-blocks', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$overviewUrl = Url::to(['/experimentCreation/planting-arrangement/overview', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$paSaveActiveTabToSessionUrl = Url::to(['/experimentCreation/planting-arrangement/pa-save-active-tab','id'=>$id]);

$paActiveTab = Yii::$app->session->get('pa-active-tab-'.$id);

$this->registerJsFile("@web/js/check-required-field-status.js", [
    'depends' => ['app\assets\AppAsset'],
    'position' => \yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    var tab = '$tab';
    var tabDesign = '$tabDesign';
    var experimentType = '$experimentType';
    var experimentStatus = '$experimentStatus';
    var changeToEntryOrder = '$changeToEntryOrder';
    var currentUrl = '$currentUrlChange';
    var paActiveTab = '$paActiveTab';
    var experimentUsesCrossPattern = '$experimentUsesCrossPattern';

    $('#add-blocks-tab,#assign-entries-tab,#manage-blocks-tab,#overview-tab').on('click', function(e) {
        var selectedTab = $(this).attr('id');

        var tabUrl = '';

        switch (selectedTab) {
            case 'add-blocks-tab':
                url = '$addBlockUrl';
                break;
            case 'assign-entries-tab':
                url = '$assignEntriesUrl';
                break;
            case 'manage-blocks-tab':
                url = '$manageBlocksUrl';
                break;
            case 'overview-tab':
                url = '$overviewUrl';
                break;
        }

        // upon click of block, store active block to session
        $.ajax({
            url: '$paSaveActiveTabToSessionUrl',
            type: 'post',
            data:{
                tab: selectedTab
            },
            async: false
        });

        // load tab
        $('#pa-systematic-arrangement-tabsX').children().eq(1).html(`
            <div class="preloader-wrapper small active left" style="width:25px;height:25px;margin: 5px 10px;">
                <div class="spinner-layer spinner-green-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div> 
                    </div>
                </div>
            </div>&emsp;
        `);
        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                },
                async: false,
                success: function(data) {
                    $('#pa-systematic-arrangement-tabsX').children().eq(1).html(data);
                }
            });
        },100);
    });
    
    $(document).ready(function() {
        if(tabDesign == 'design-entry-order'){
            setTimeout(function() {
                $(".tabs-krajee").find("li.active a").click();
            },10);
        }

        if(changeToEntryOrder == 'true'){
            $('#change-design-switch-id').removeAttr('disabled')
            $('#change-design-switch-id').click();
        }

        // trigger the current or default block
        if(paActiveTab == '' || paActiveTab == 'null') {
            $('#add-blocks-tab').trigger('click');
        } else {
            $('#'+paActiveTab).trigger('click');
        }

        if(experimentType == 'Observation' || experimentUsesCrossPattern) {
            if($('[name="DesignArrangement"]').prop('checked') == false) {
                $('#change-design-switch-id').removeAttr('disabled')
                $('#change-design-switch-id').click()
            }
        } else {
            $('#arrangement-option').removeClass('hidden')
        }
    });
    // refresh add block panel
    $(document).on('click','.pa-add-blocks-tab',function(){

        $('.add-blocks-refresh-btn').click();    
        
    });

    $("#change-design-switch-id").on('change', function (e, params) {
        $('#change-design-switch-id').attr('disabled','disabled')
        if($('[name="DesignArrangement"]').prop('checked') == true){
            designType = 'design-entry-order';
        } else{
            designType = 'design-add-blocks';
        }
        $('#planting-arrangement-view').html('<div class="progress"><div class="indeterminate"></div></div>');
        $.ajax({
            url: '$checkPreviousRecords',
            type: 'post',
            dataType: 'json',
            data: {
                designType: designType,
            },
            success: function(response) {
                if(response['confirmFlag']){
                    $('#pa-design-confirm-modal').modal();
                    var note = 'You are about to change the planting arrangement of this experiment.<br/> <br/> NOTE: The previous records that were generated will be deleted upon <b>confirmation</b>.';
                    
                    $('#pa-design-modal-confirm').html(note);
                } else {
                    confirmChange();
                }
            },
            error: function(error) {
            }
        });
    });

    $('#cancel-change-pa-btn').click(function(){
        //reset switch
        if($('[name="DesignArrangement"]').prop('checked') == true){
            $('[name="DesignArrangement"]').prop('checked', false);
            designType = 'design-add-blocks';
        } else{
            $('[name="DesignArrangement"]').prop('checked', true);
            designType = 'design-entry-order';
        }
        location.reload();
    });

    $('#confirm-change-pa-btn').click(function(){
        
        confirmChange();
    });

    function confirmChange(){
        $.ajax({
            url: '$changePreviousRecords',
            type: 'post',
            dataType: 'json',
            data: {
                designType: designType,
            },
            success: function(response) {
                if(response['success']){
                    location.reload();
                }
            },
            error: function(error) {
            }
        });
    }

    function updateTabs(idName){
        if(idName == 'entry-order-option-id'){
            //hide the other tabs
            $('#pa-systematic-arrangement-tabsX').addClass('hidden');
            $('#pa-entry-order-tabsX').removeClass('hidden');

        } else {
            //view the hidden tabs
            $('#pa-systematic-arrangement-tabsX').removeClass('hidden');
            $('#pa-entry-order-tabsX').addClass('hidden');
        }
    }

    //validate upon click of next btn
    $(document).on('click','#next-btn', function(e){
        setTimeout(function(){  
            $.ajax({
                type: 'POST',
                url: '$checkPAUpdatedUrl',
                async: false,
                success: function(data) {
                    window.location = $('#next-btn').attr('data-url');
                }
            });
        },100);
    });
JS
);
?>
<style>
    .fa{
        font-size: 95%;
    }
    input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
        margin: 0 0 5px 0;
    }
    input[type=text]:not(.browser-default),input[type=number]:not(.browser-default){
        height:2.5rem;
    }

    .tabular-icon{
        margin-left: 4px !important;
    }
    .tabular-btn{
        padding: 0 0.9rem 0 0.9rem;
    }
    .row{
        margin-bottom: 10px;
    }
    .fa{
        font-size: 95%;
    }

    .pa-add-blocks-before-heading {
        content-visibility: auto;
    }
</style>