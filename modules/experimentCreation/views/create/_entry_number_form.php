<?php

/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* Preview list crosses in crossing matrix
**/

use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\helpers\Html;

$columns = [
	['class' => 'yii\grid\SerialColumn'],
    [
    	'attribute'=>'crossName',
    	'enableSorting' => false,
        'format'=>'raw',
        'vAlign'=>'middle',
        'value'=> function($data){
            return '<span data-attr="'.$data['crossName'].'">'.$data['crossName'].'</span>';
        }
    ],
    [
        'attribute'=>'femaleNo',
        'label' => 'Female entry no',
        'enableSorting' => false,
        'format' => 'raw',
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data, $key){
            $disabled = $data['femaleCount'] == 1 ? 'disabled' : '';
            $options = '<select data-row = "'.$key.'" data-cross = "'.$data['crossName'].'" id = "female-'.$key.'" class="female-entno browser-default" style="height:auto" '.$disabled.'>';
            
            foreach ( $data['femaleNo'] as $k => $value) {
                $display = '';
                if($value == $data['maleEntryId'] && $data['femaleCount'] !== 1){
                    $display = 'none';
                }
                $options = $options.'<option value="'.$k.'" style="display:'.$display.'">'.$value.'</option>';
            }
            $value = $options.'</select>';

            return '<div class="female_entno_input">'.$value.'</div>';
        }
    ],
    [
        'attribute'=>'maleNo',
        'label' => 'Male entry no',
        'enableSorting' => false,
        'format' => 'raw',
        'encodeLabel' => false,
        'vAlign'=>'middle',
        'value' => function($data, $key){
            $disabled = $data['maleCount'] == 1 ? 'disabled' : '';
            $options = '<select data-row = "'.$key.'" data-cross = "'.$data['crossName'].'" id = "male-'.$key.'" class="male-entno browser-default" style="height:auto" '.$disabled.'>';
            
            foreach ( $data['maleNo'] as $k => $value) {
                $display = '';
                if($value == $data['femaleEntryId'] && $data['maleCount'] !== 1){
                    $display = 'none';
                }
                $options = $options.'<option value="'.$k.'" style="display:'.$display.'">'.$value.'</option>';
            }
            $value = $options.'</select>';

            return '<div class="male_entno_input">'.$value.'</div>';
        }
    ],
    [
        'attribute'=>'femaleGen',
        'label' => 'Female generation',
        'enableSorting' => false,
        'format'=>'raw',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'maleGen',
        'label' => 'Male generation',
        'enableSorting' => false,
        'format'=>'raw',
        'vAlign'=>'middle',
    ],
];

echo '<p id = "entry-form-instructions" >'.\Yii::t('app','Select entry number for each parent. Same entry number is not allowed (use SELF feature instead). Crosses which are already in the cross list will not be counted.').'</p>';
echo '<div id = "entry-form-loading" class="progress" style = "display:none"><div class="indeterminate"></div></div>';
echo $grid = GridView::widget([
	'dataProvider' => $data,
	'id' => 'entry-form-matrix-grid',
	'columns' => $columns,
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
]);

?>

<style type="text/css">
.table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #fff;
}
.summary{
    text-align: right;
}
</style>
