<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders input crosses form for the experiment creation
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

use kartik\dynagrid\DynaGrid;
use marekpetras\yii2ajaxboxwidget\Box;

$browserIdFemale = 'dynagrid-ec-add-crosses-female';
$browserIdMale = 'dynagrid-ec-add-crosses-male';

$notSet = '<span class="not-set">(not set)</span>';
?>

	<div id = "parent-list" class = "row col col-sm-12 col-md-12 col-lg-12" style = "margin-bottom: 100px;padding-right:0px">
		<p><?= \Yii::t('app' , 'Pair parents by selecting from the female and male browsers. Click "Add" to add pairings to the cross list. Max of <b>'.number_format($maxCrossCount).'</b> crosses per validation.');?></p>
		<!-- female data browser -->
		<div class="col col-sm-4 col-md-4 col-lg-4 parent-box" style="padding-left: 10px !important;">
      <?php
        $columns = [// variables columns
          [
            'label'=> "Checkbox",
            'header' => '
              <input type="checkbox" class="filled-in" id="dynagrid-ec-add-crosses-female-select-all" style="margin:0px; padding:0px;"/>
              <label for="dynagrid-ec-add-crosses-female-select-all" style="margin:0px; padding:0px 20px 0px 0px;"></label>',
            'content'=>function($data) {
              $gridId = 'female_'.$data["entryNumber"];
              $femaleEntryDbId = $data["entryDbId"];
              $checkbox = '<input class="dynagrid-ec-add-crosses-female-select filled-in" type="checkbox" id="'.$gridId.'" data-entryCode = "'.$data["entryCode"].'" data-designation = "'.$data["designation"].'"/>';

              return $checkbox.
                '<label for="'.$gridId.'" style="margin:0px; padding:0px 20px 0px 0px;"></label>';
            },
            'contentOptions'=>['class'=>'red lighten-5'],
            'hAlign'=>'center',
            'vAlign'=>'top',
            'hiddenFromExport'=>true,
            'mergeHeader'=>true,
            'order'=> DynaGrid::ORDER_FIX_LEFT,
          ],
          [
            'attribute'=>'entryNumber',
            'label' => '<span title="Sequential numbering of the entries">'.Yii::t('app', 'Entry No.').'</span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false,
            'order' => DynaGrid::ORDER_FIX_LEFT

          ],
          [
            'attribute'=>'designation',
            'label' => '<span title="Germplasm">'.Yii::t('app', 'Germplasm Name').'</span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'format' => 'raw',
            'value' => function($data) use ($notSet) {
              $entityName = (isset($data['designation'])) ? $data['designation'] : $notSet;

              $linkAttrs = [
                'title' => \Yii::t('app', 'Click to view more information'),
                'class' => 'view-germplasm',
                'data-entity_id' => isset($data['germplasmDbId']) ? $data['germplasmDbId'] : null,
                'data-entity_label' => $entityName,
                'data-entity_type' => 'germplasm',
                'data-target' => '#view-germplasm-modal',
                'data-toggle' => 'modal'
              ];

              $entityClass = ['class' => 'blue-text text-darken-4 view-more-info header-highlight'];
              $viewMoreInfoClass = ['class' => 'view-more-info fixed-color'];

              return Html::a($entityName, '#', array_merge($entityClass,$linkAttrs));
            },
            'vAlign'=>'middle',
            'encodeLabel'=>false,
          ],
          [
            'attribute'=>'femaleCount',
            'label' => '<span title="'.Yii::t('app', 'Number of crosses this parent was used as a female.').'"># <b style="display:none">♀</b><i class="fa fa-venus" style="font-size:120%"></i></span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false,
          ],
          [
            'attribute'=>'maleCount',
            'label' => '<span title="'.Yii::t('app', 'Number of crosses this parent was used as a male.').'"># <b style="display:none">♂</b><i class="fa fa-mars" style="font-size:120%"></i></span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false,
          ],
          [
            'attribute'=>'entryRole',
            'label' => '<span>'.Yii::t('app', 'Parent Role').'</span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'format' => 'raw',
            'filterType'=>GridView::FILTER_SELECT2,
            'filter'=>[''=>'--Select--',
              'female'=>'female',
              'female-and-male'=>'female-and-male'],
            'vAlign'=>'middle',
            'encodeLabel'=>false,
            'visible' => false
          ],
          [
            'attribute'=>'parentage',
            'label' => '<span title="Pedigree Information">'.Yii::t('app', 'Parentage').'</span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false,
            'visible' => false
          ],
          [
            'attribute'=>'entryCode',
            'label' => '<span title="Pedigree Information">'.Yii::t('app', 'Entry Code').'</span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false
          ],
        ];

        $selfButton = '<a data-type="female" class="self-btn light-green darken-3 btn btn-success" style="margin-right:5px;" title="'
          .Yii::t('app','Create self crosses for Female parents').'">&nbsp;<b>'.\Yii::t('app','Self').'</b></a>';

        // female browser
        DynaGrid::begin([
          'options'=>['id'=>$browserIdFemale],
          'columns' => $columns,
          'theme'=>'simple-default',
          'showPersonalize'=>true,
          'storage' => DynaGrid::TYPE_SESSION,
          'showFilter' => false,
          'showSort' => false,
          'allowFilterSetting' => false,
          'allowSortSetting' => false,
          'gridOptions'=>[
            'filterModel'=> $femaleSearchModel,
            'dataProvider' => $femaleDataProvider,
            'showPageSummary'=>false,
            'pjax'=>true,
            'pjaxSettings'=>[
              'neverTimeout' => true,
              'options'=>[
                'id'=> $browserIdFemale,
                'enablePushState'=>false
              ],
              'beforeGrid' => '',
              'afterGrid' => ''
            ],
            'responsiveWrap'=>false,
            'panel'=>[
              'heading'=> false,
              'before' =>'<strong class="larger-text">'.\Yii::t('app',' Female').'&emsp;'.'</strong>{summary}<br/>
                <span id = "selected-items" class = "pull-right" style = "margin-right:3px">
                <div class="hidden preloader-wrapper female-loader small active" style="width:10px;height:10px;">
                <div class="spinner-layer spinner-green-only">
                  <div class="circle-clipper left">
                    <div class="circle"></div>
                  </div><div class="gap-patch">
                    <div class="circle"></div>
                  </div><div class="circle-clipper right">
                    <div class="circle"></div>
                  </div>
                </div>
              </div> 
                  <b id = "selectedFemaleCount"></b><span id="selectedFemaleText"> selected items</span>.
                </span>
            ',
              'after' => false,
            ],
            'toolbar' =>[
              [
                'content'=>
                  "$selfButton ".
                  Html::a('<i class="glyphicon glyphicon-repeat"></i>',  
                    ["create/manage-crosses?id=$id&program=$program&processId=$processId&reset=female_hard_reset"], 
                    [
                      'id'=>'reset-'.$browserIdFemale,
                      'class' => 'btn btn-default', 
                      'title'=>\Yii::t('app','Reset female data browser'),
                      'data-pjax' => true
                    ]
                  ).'{dynagrid}'
              ],
            ],
            'floatHeader' =>true,
            'floatOverflowContainer'=> true,
            'resizableColumns'=>true,
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ]
            ]
        ]);

        DynaGrid::end();

       //view germplasm modal
        Modal::begin([
          'id' => 'view-germplasm-modal',
          'header' => '<h4 id="view-entity-label"></h4>',
          'footer' =>
              Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']),
              'closeButton' => [
                  'class' => 'hidden'
              ],
          'size' =>'modal-lg',
          'options' => ['data-backdrop'=>'static'],
        ]);
        echo '<div class="view-germplasm-modal-body"></div>';
        Modal::end();

        $placeholder = "Parent format: DESIGNATION \n\nUse pipe ('|') to separate female germplasm and male germplasm \nlike the following format:\nIR 1XXXX|IR 1YYYY\nIR 2XXXX|IR 2YYYY\nIR 3XXXX|IR 3YYYY";
        if($type == 'entryCode'){
            $placeholder = "Parent format: ENTRY_CODE \n\nUse pipe ('|') to separate female parent and male parent \nlike the following format:\nENTRY_CODE1|ENTRY_CODE2\nENTRY_CODE3|ENTRY_CODE4";
        }
      ?>
		</div>

    <!-- cross input text area -->
    <div class="cross-input col-sm-4 col-md-4 col-lg-4" style="background-color:white;">
      <strong class="larger-text"> <?= \Yii::t('app','Crosses'); ?> </strong><strong id = "cross-count" class = "pull-right larger-text">0</strong><strong class = "pull-right larger-text" title = "Combinations of selected parents and input crosses"> <?= \Yii::t('app','Total: '); ?></strong>
      <div class="switch pull-right" style="width: fit-content;margin-right: 10px; ">
        <label>
          Designation
          <input type="checkbox" <?= $type=='entryCode' ? 'checked':''?> id="crossInputSwitch-id">
          <span class="lever"></span>
          Entry code
        </label>
      </div><br>
      <?= Html::textArea('crosses-textarea',"",[
      'id'=>'crosses-textarea',
      'style'=>'background-color:white; resize: none; !important;',
      'placeholder'=>$placeholder
      ]); ?>
      <button id="add-cross-btn" disabled class="btn waves-effect waves-light teal pull-right" title = "Add Crosses" style="margin: 5px 0px 0px 5px;">
        <strong><?=\Yii::t('app','Add');?></strong>
      </button>
      <button id="pair-btn" disabled class="btn waves-effect waves-light teal darken-1" title = "Match female and male parents" style="margin: 5px 0px 0px 0px;">
        <strong><?=\Yii::t('app','Pair');?></strong>
      </button>
      <button id="delete-cross-btn" disabled class="btn waves-effect waves-light grey lighten-3 black-text" title = "Clear Input List" style="margin: 5px 0px 0px 5px;">
        <strong><?=\Yii::t('app','Clear');?></strong>
      </button>
      <button id="replace-cross-btn" disabled class="btn waves-effect waves-light light-blue darken-4" style="margin: 5px 0px 0px 5px;" title = "Replace all Crosses">
        <strong><?=\Yii::t('app','Replace');?></strong>
      </button>
      
			<?php
				//Modal for validation messages
				Modal::begin([
					'id' => 'validation-modal',
					'header' => '<h4 id= "validation-header"></h4>',
					'footer' =>	Html::a(\Yii::t('app',' No '),'#',['id' => 'no-btn','data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close black-text grey lighten-2','style' =>'display:none'])
						.'&nbsp;'.Html::a(\Yii::t('app',' Yes '),'#',['id' => 'yes-btn','class'=>'btn btn-primary waves-effect waves-light modal-close teal','style' =>'display:none'])
						.Html::a('<i class="material-icons left">arrow_back</i>'.\Yii::t('app',' Back '),'#',['id' => 'back-btn','data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close black-text grey lighten-2','style' =>'display:none']).'&nbsp;'
						.Html::a(\Yii::t('app','Ok'),'#',['id' => 'ok-btn','data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close','style' =>'display:none'])
						.'&nbsp;'.Html::a('<i class="material-icons right">arrow_forward</i>'.\Yii::t('app','Proceed'),'#',['id' => 'clear-btn','class'=>'btn btn-primary waves-effect waves-light modal-close teal','style' =>'display:none'])
						.'&nbsp;'.Html::a(\Yii::t('app','Add more'),'#',['id' => 'add-more-btn','data-dismiss'=>'modal','class'=>'btn black-text grey lighten-2 waves-effect waves-light modal-close','style' =>'display:none'])
						.'&nbsp;&nbsp;'.Html::a(\Yii::t('app','Manage cross list'),Url::to(['create/manage-crosslist', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]),['id' => 'manage-cross-list-btn','class'=>'btn btn-primary waves-effect waves-light modal-close','style' =>'display:none']),
					'size' =>'modal-md',
					'options' => ['data-backdrop'=>'static'],
					'closeButton' => ['class' => 'hidden'],
				]);
				echo '<div id="validation-body"></div>';
				Modal::end();

				//Modal for duplicate designation entry no form
				Modal::begin([
					'id' => 'designation-modal',
					'header' => "<h4><i class='fa fa-flickr'></i> Duplicate germplasm</h4>",				
					'footer' =>	Html::a('<i class="material-icons left">arrow_back</i>'.\Yii::t('app',' Back '),'#',['id' => 'back-btn','data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close black-text grey lighten-2'])
						.'&nbsp;'.Html::a('<i class="material-icons right">arrow_forward</i>'.\Yii::t('app','Proceed'),'#',['id' => 'add-proceed-duplicate-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal' ,'style' =>'display:none', 'operation' => 'add'])
						.'&nbsp;'.Html::a(\Yii::t('app','Add more'),'#',['id' => 'add-more-btn','data-dismiss'=>'modal','class'=>'btn black-text grey lighten-2 waves-effect waves-light modal-close','style' =>'display:none'])
						.'&nbsp;&nbsp;'.Html::a(\Yii::t('app','Manage cross list'),Url::to(['manage-crosslist', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]),['id' => 'manage-cross-list-btn','class'=>'btn btn-primary waves-effect waves-light modal-close','style' =>'display:none']),
					'size' =>'modal-lg',
					'options' => ['data-backdrop'=>'static'],
					'closeButton' => ['class' => 'hidden'],
				]);
				echo '<div id="designation-modal-body"></div>';
				Modal::end();
			?>
		</div>

		<!-- male data browser -->
		<div class="col col-sm-4 col-md-4 col-lg-4 parent-box">
      <?php
        $columns = [// variables columns
          [
            'label'=> "Checkbox",
            'header' => '
              <input type="checkbox" class="filled-in" id="dynagrid-ec-add-crosses-male-select-all" style="margin:0px; padding:0px;"/>
              <label for="dynagrid-ec-add-crosses-male-select-all" style="margin:0px; padding:0px 20px 0px 0px;"></label>',
            'content'=>function($data) {
              $gridId = 'male_'.$data["entryNumber"];
              $maleEntryDbId = $data['entryDbId'];
              $checkbox = '<input class="dynagrid-ec-add-crosses-male-select filled-in" type="checkbox" id="'.$gridId.'" data-entryCode = "'.$data["entryCode"].'" data-designation = "'.$data["designation"].'"/>';

              return $checkbox.
                '<label for="'.$gridId.'" style="margin:0px; padding:0px 20px 0px 0px;"></label>';
            },
            'contentOptions'=>['class'=>'blue lighten-5'],
            'hAlign'=>'center',
            'vAlign'=>'top',
            'hiddenFromExport'=>true,
            'mergeHeader'=>true,
            'order'=> DynaGrid::ORDER_FIX_LEFT,
          ],
          [
            'attribute'=>'entryNumber',
            'label' => '<span title="Sequential numbering of the entries">'.Yii::t('app', 'Entry No.').'</span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false,
            'order' => DynaGrid::ORDER_FIX_LEFT
          ],
          [
            'attribute'=>'designation',
            'label' => '<span title="Germplasm">'.Yii::t('app', 'Germplasm Name').'</span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'format' => 'raw',
            'value' => function($data) use ($notSet) {
              $entityName = (isset($data['designation'])) ? $data['designation'] : $notSet;

              $linkAttrs = [
                'title' => \Yii::t('app', 'Click to view more information'),
                'class' => 'view-germplasm',
                'data-entity_id' => isset($data['germplasmDbId']) ? $data['germplasmDbId'] : null,
                'data-entity_label' => $entityName,
                'data-entity_type' => 'germplasm',
                'data-target' => '#view-germplasm-modal',
                'data-toggle' => 'modal'
              ];

              $entityClass = ['class' => 'blue-text text-darken-4 view-more-info header-highlight'];
              $viewMoreInfoClass = ['class' => 'view-more-info fixed-color'];

              return Html::a($entityName, '#', array_merge($entityClass,$linkAttrs));
            },
            'vAlign'=>'middle',
            'encodeLabel'=>false,
          ],
          [
            'attribute'=>'femaleCount',
            'label' => '<span title="'.Yii::t('app', 'Number of crosses this parent was used as a female.').'"># <b style="display:none">♀</b><i class="fa fa-venus" style="font-size:120%"></i></span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false,
          ],
          [
            'attribute'=>'maleCount',
            'label' => '<span title="'.Yii::t('app', 'Number of crosses this parent was used as a male.').'"># <b style="display:none">♂</b><i class="fa fa-mars" style="font-size:120%"></i></span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false,
          ],
          [
            'attribute'=>'entryRole',
            'label' => '<span>'.Yii::t('app', 'Parent Role').'</span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'format' => 'raw',
            'filterType'=>GridView::FILTER_SELECT2,
            'filter'=>[''=>'--Select--',
              'male'=>'male',
              'female-and-male'=>'female-and-male'],
            'vAlign'=>'middle',
            'encodeLabel'=>false,
            'visible' => false
          ],
          [
            'attribute'=>'parentage',
            'label' => '<span title="Pedigree Information">'.Yii::t('app', 'Parentage').'</span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false,
            'visible' => false
          ],
          [
            'attribute'=>'entryCode',
            'label' => '<span title="Pedigree Information">'.Yii::t('app', 'Entry Code').'</span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'format' => 'raw',
            'vAlign'=>'middle',
            'encodeLabel'=>false
          ],
        ];

        $selfButton = '<a data-type="male" class="self-btn light-green darken-3 btn btn-success" style="margin-right:5px;" title="'
          .Yii::t('app','Create self crosses for Male parents').'">&nbsp;<b>'.\Yii::t('app','Self').'</b></a>';
        // male browser
        DynaGrid::begin([
          'options'=>['id'=>$browserIdMale],
          'columns' => $columns,
          'theme'=>'simple-default',
          'showPersonalize'=>true,
          'storage' => DynaGrid::TYPE_SESSION,
          'showFilter' => false,
          'showSort' => false,
          'allowFilterSetting' => false,
          'allowSortSetting' => false,
          'gridOptions'=>[
            'filterModel'=> $maleSearchModel,
            'dataProvider' => $maleDataProvider,
            'showPageSummary'=>false,
            'pjax'=>true,
              'pjaxSettings'=>[
                'neverTimeout'=>true,
                'options'=>[
                  'id'=> $browserIdMale,
                  'enablePushState'=>false
                ],
                'beforeGrid'=>'',
                'afterGrid'=>''
              ],
            'responsiveWrap'=>false,
            'panel'=>[
              'heading'=> false,
              'before' =>'<strong class="larger-text"> '.\Yii::t('app', 'Male').'&emsp;'.'</strong>{summary}<br/>
                <span id = "selected-items" class = "pull-right" style = "margin-right:3px">
                  <div class="hidden preloader-wrapper male-loader small active" style="width:10px;height:10px;">
                    <div class="spinner-layer spinner-green-only">
                      <div class="circle-clipper left">
                        <div class="circle"></div>
                      </div><div class="gap-patch">
                        <div class="circle"></div>
                      </div><div class="circle-clipper right">
                        <div class="circle"></div>
                      </div>
                    </div>
                  </div> 
                  <b id = "selectedMaleCount"></b><span id="selectedMaleText"> selected items</span>.
                </span>',
              'after' => false,
            ],
            'toolbar' =>[
              [
                'content'=> 
                  "$selfButton ".
                  Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                  ["create/manage-crosses?id=$id&program=$program&processId=$processId&reset=male_hard_reset"], 
                  [
                    'id'=>'reset-'.$browserIdMale,
                    'class' => 'btn btn-default', 
                    'title'=>\Yii::t('app','Reset male data browser'), 
                    'data-pjax' => true
                  ]
                ).'{dynagrid}'
              ],
            ],
            'floatHeader' =>true,
            'floatOverflowContainer'=> true,
            'resizableColumns'=>true,
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ],
            ]
        ]);
        DynaGrid::end();
      ?>

		</div>
	</div>


<?php

$viewEntityUrl = Url::to(['/germplasm/default/view-info']);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserIdFemale, $browserIdMale]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ['".$browserIdFemale."', '".$browserIdMale."'],
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
  //tabs for view germplasm modal
    $(document).on('click', '#li-3', function(e) {
        e.preventDefault(); //for prevent default behavior of <a> tag.
        $('#view-germplasm-modal').modal('show').find('#view-entity-3').load($(this).attr('value')); //load view studies

    });
    $(document).on('click', '#li-4', function(e) {
        e.preventDefault(); //for prevent default behavior of <a> tag.
        $('#view-germplasm-modal').modal('show').find('#view-entity-4').load($(this).attr('value')); //load view seed lots
    });
    $(document).on('click', '#li-8', function(e) {
        e.preventDefault(); //for prevent default behavior of <a> tag.
        $('#view-germplasm-modal').modal('show').find('#view-entity-8').load($(this).attr('value')); //load view passport data
    });
    //end tabs

    // view information about the germplasm
    $(document).on('click', '.view-germplasm', function(e) {
        var obj = $(this);
        var entity_label = obj.data('entity_label');
        var entity_id = obj.data('entity_id');
        var entity_type = obj.data('entity_type');

        $('#view-entity-label').html('<i class="fa fa-info-circle"></i> ' + entity_label + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        $('.view-germplasm-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        //load content of view more information
        setTimeout(function() {
            $.ajax({
            url: '$viewEntityUrl',
            data: {
                entity_id: entity_id,
                entity_type: entity_type,
                entity_label: entity_label
            },
            type: 'POST',
            async: true,
            success: function(data) {
                $('.view-germplasm-modal-body').html(data);
            },
            error: function(){
                $('.view-germplasm-modal-body').html('<i>There was a problem while loading record information.</i>'); 
            }
        });
        }, 300);
    });

    $(document).on('change', '#crossInputSwitch-id', function(e) {
        if($('#crossInputSwitch-id').prop('checked') == true){
            var switchInputType = 'entryCode';
        } else {
            var switchInputType = 'designation';
        }
        var url = '$changeTypeUrl'+'&type='+switchInputType;
        window.location.href = url;
    });

    $(document).ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
    });
JS
);
?>