<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /**
  * Renders filter for packages (previously seedlots)
 */

use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\Html;

?>

<p><?= \Yii::t('app', 'Filter package sources by using the filters below. You can clear all the filters by clearing the value for the <b>Year</b> field. Note fields with <span class="required">*</span> are required.') ?></p>



<div class="col-md-12">
<div class="row" style="margin-top:10px;padding-top:10px; ">
    <div class="control-label row  col-md-6" style="margin-bottom:10px; padding-left:5px;">
        <div class="col-md-12" title="Year when the trial or nursery was conducted" style="">
            <label data-label="Evaluation Year" style="margin-top:10px;" for="YEAR">Year <span class="required">*</span></label>
        </div>
        <div class="col-md-12" style="padding-right:0px;">
            <?php
                // echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Year <span class="required">*</span>').'</label>';
                echo Select2::widget([
                    'name' => 'filter[YEAR][]',
                    'data' => $yearFilter,
                    'id' => 'select-year-select',
                    'maintainOrder' => true,
                    'options' => [
                        'class'=> 'filter-tag',
                        'placeholder' => \Yii::t('app','Select value'),
                        'tags' => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'pluginEvents' => [

                        'select2:unselect' => "function(e){

                            $('#select-year-select').val(null);
                            $('#select-experiment-select').val(null);
                            $('#select-season-select').val(null);
                            $('#select-stage-select').val(null);
                            
                            $('#search-seeds-btn').attr('disabled', true);
                            $('#seeds-content').html('');
                            // updateFilterTags();

                        }",

                        'select2:close' => "function(e) { 

                            var year = $('#select-year-select').val();
                            
                            if(year){
                                $('#search-seeds-btn').attr('disabled', false);
                            } else {
                                $('#search-seeds-btn').attr('disabled', true);
                                $('#seeds-content').html('');
                            }

                        }",

                    ],
                ]);
            ?>
            </div>
            <div class="" style="margin-bottom:5px; padding-left:5px;">
            </div>
        </div>


     <div class="control-label row  col-md-6" style="margin-bottom:10px; padding-left:5px;">
        <div class="col-md-12" title="Season the experiment was conducted" style="">
            <label data-label="Evaluation Season" style="margin-top:10px;" for="SEASON">Season</label>
        </div>
        <div class="col-md-12" style="padding-right:0px;">
        <?php
            // echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Season').'</label>';
            echo Select2::widget([
                'name' => 'filter[SEASON][]',
                'data' => $seasonFilter,
                'id' => 'select-season-select',
                'maintainOrder' => true,
                'options' => [
                    'class'=> 'filter-tag',
                    'placeholder' => \Yii::t('app','Select season'),
                    'tags' => true,
                ],
              
            ]);
        ?>
        </div>
        <div class="" style="margin-bottom:5px; padding-left:5px;"></div>
    </div>


    <div class="control-label row  col-md-6" style="margin-bottom:10px; padding-left:5px;">
        <div class="col-md-12" title="Name of the Experiment" style="">
            <label data-label="Experiment" style="margin-top:10px;" for="EXPERIMENT">Experiment</label>
        </div>
        <div class="col-md-12" style="padding-right:0px;">
        <?php
            // echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Experiment').'</label>';
            echo Select2::widget([
                'name' => 'filter[EXPERIMENT][]',
                'data' => $experimentFilter,
                'id' => 'select-experiment-select',
                'maintainOrder' => true,
                'options' => [
                    'class'=> 'filter-tag-excluded',
                    'placeholder' => \Yii::t('app','Select experiment'),
                    'tags' => true,
                ],

            ]);
        ?>
        </div>
        <div class="" style="margin-bottom:5px; padding-left:5px;"></div>
    </div>

    <div class="control-label row  col-md-6" style="margin-bottom:10px; padding-left:5px;">
        <div class="col-md-12" title="Evaluation Stage" style="">
            <label data-label="Stage" style="margin-top:10px;" for="STAGE">Stage</label>
        </div>
        <div class="col-md-12" style="padding-right:0px;">
        <?php
            // echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Stage').'</label>';
            echo Select2::widget([
                'name' => 'filter[STAGE][]',
                'data' => $stageFilter,
                'id' => 'select-stage-select',
                'maintainOrder' => true,
                'options' => [
                    'class'=> 'filter-tag',
                    'placeholder' => \Yii::t('app','Select value'),
                    'tags' => true,
                ],
                
            ]);
        ?>
        </div>
        <div class="" style="margin-bottom:5px; padding-left:5px;"></div>
    </div>

  </div>
</div>


</div>
<div class="row">
<div class="col pull-right">
    <button disabled id="search-seeds-btn" class="btn btn-primary waves-effect waves-light pull-right">
        <?= \Yii::t('app', 'Search') ?>
    </button>
</div>
</div>

<div id="seeds-content">
</div>

<?php
    $entryIdArray = isset($entryIdArray) ? $entryIdArray : '';
    $changeFilter = Url::to(['create/change-filter','id'=>$id, 'program'=>$program]);
    $searchPackagesUrl = Url::to(['create/render-search-packages','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>

<script>
    $('#search-seeds-btn').click(function(){
        var year = $('#select-year-select').val();
        var season = $('#select-season-select').val();
        var experiment = $('#select-experiment-select').val();
        var stage = $('#select-stage-select').val();
        var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';

        $('#seeds-content').html(loadingIndicator);
        $.ajax({
            url: '<?php echo $searchPackagesUrl; ?>',
            type: 'post', 
            dataType: 'json',
            data: {
                entryIdArray: '<?php echo $entryIdArray; ?>',
                yearFilter: year,
                seasonFilter: season,
                experimentFilter:experiment,
                stageFilter:stage
            },
            success: function (response) {

                $('#seeds-content').html(response);

                // var hasChecked = false;
                // $('.action-modal-radio-seedlot').each(function(){
                //     if($(this).is(':checked')){
                //         hasChecked = true;
                //     }
                // });
                // if(hasChecked){
                    $('#bulk-seedlot-confirm').removeClass('disabled');
                // }else{
                //     $('#bulk-seedlot-confirm').addClass('disabled');
                // }
            }
        });
    });

    
    $('.filter-tag').change(function(){
        var year = $('#select-year-select').val();
        var season = $('#select-season-select').val();
        var experiment = $('#select-experiment-select').val();
        var stage = $('#select-stage-select').val();

        if(this.id = 'select-year-select'){
            $('#select-experiment-select').val('');
            $('#select-season-select').val('');
            $('#select-stage-select').val('');
        } else if(this.id = 'select-experiment-select'){
            $('#select-season-select').val('');
            $('#select-stage-select').val('');
        } else if(this.id = 'select-season-select'){
            $('#select-stage-select').val('');
        }

        $.ajax({
            url: '<?php echo $changeFilter; ?>',
            type: 'post',
            dataType: 'json',
            data: {
                entryIdArray: '<?php echo $entryIdArray; ?>',
                yearFilter: year,
                seasonFilter: season,
                experimentFilter:experiment,
                stageFilter:stage
            },
            success: function (response) {
    
                if(Object.keys(response['experiment']).length > 0 ){
                    updateFilterTags('select-experiment-select', response['experiment'], experiment);
                }

                if(Object.keys(response['year']).length > 0 ){
                    updateFilterTags('select-year-select', response['year'], year);
                }

                if(Object.keys(response['stage']).length > 0 ){
                    updateFilterTags('select-stage-select', response['stage'], stage);
                }

                if(Object.keys(response['season']).length > 0 ){
                    updateFilterTags('select-season-select', response['season'], season);
                }

                if(year){
                    $('#search-seeds-btn').attr('disabled', false);
                }else{
                    $('#search-seeds-btn').attr('disabled', true);
                    $('#select-season-select').val('');
                    $('#select-stage-select').val('');
                    $('#select-experiment-select').val('');
                }
            }
        });
    });

    function updateFilterTags(id, values, initial){
      var s1 = document.getElementById(id);

      $('#'+id).find('option').remove();

      $.each(values, function (key, value) {
         
          var newOption = document.createElement("option");
          newOption.value = key;
          newOption.innerHTML = value;
          s1.options.add(newOption);
      });
      $('#'+id).val(initial);
    }
</script>