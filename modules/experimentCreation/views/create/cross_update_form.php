<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use yii\helpers\Url;
?>

	<?php
        
        echo '<label class="control-label" style="margin-top:10px">'.\Yii::t('app','Variable'.'<span class="required">*</span>').'</label>';
        echo '<div style="width:86%">';
        echo Select2::widget([
            'name' => 'BulkUpdate',
            'id' => 'req-vars',
            'data' => $data,
            'value' => '',
            'options' => [
                'placeholder' => 'Select a value',
                'tags' => true,
                'class'=>'select2-input-validate',
                'style' => 'overflow-x: visible; overflow-y: visible;width:88%;',   
            ],
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [','],
                'maximumInputLength' => 100
            ],
        ]);
        echo '</div>';

        echo '<div id="input-field-var" style="margin-top:30px;width:88%"></div>';
  
    ?> 


 <script>
    var $variable = '';
    var $fieldIdSelected = '';
      $(document).ready(function(){
       $('#req-vars').show();
       $('.loading-req-vars').remove();
       
       
       $('#req-vars').on('change',function(){
           var value = $(this).val();
           $variable = value.toUpperCase(); 
       
           $fieldIdSelected = $variable;

           if(value != ""){
                $.ajax({
                    url: '<?php echo Url::to(["create/render-config-form","id"=>$id]);?>',
                    type: 'post',
                    dataType: 'json',
                    data:{selected: value, configVars: '<?php echo json_encode($configVars);?>',formType: 'modal'},
                    success: function(response){
                 
                        $('#input-field-var').html(response);
                        $('.select-input').css('visibility','visible');
                        $('.select-input').css('width','');
                        $('.select-input').css('height','');
                    },
                    error: function(){
                        
                    }
                });
           }else{
            $('#input-field-var').html('');
           }
       });
    
        $('#bulk-update-selected').click(function(e){
            
            if(typeof $fieldIdSelected === 'undefined' || $fieldIdSelected == null || $fieldIdSelected  == ''){
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> You have not selected a variable.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            }else{  
    
                var selectedFieldId = $('.control-group').attr('class').split("control-group field-")[1];
                
                var divFieldId = $('.control-group').attr('class');
                
                var reqField =  $('#'+selectedFieldId).val();
                
                var $fieldId = selectedFieldId;
                
                var getUrlVarsVal = getUrlVars();
                var page = '';
                var perPage = '';
        
                if(typeof page != undefined && (typeof perPage != undefined)){
                    page = getUrlVars()['page'];
                    perPage = getUrlVars()['per-page'];
                }

                var dp1_page = '';
                var dp1_perPage = '';

                if(typeof (getUrlVars()['dp-1-per-page']) != undefined){
                    dp1_perPage = getUrlVars()['dp-1-per-page'];
                }

                if(typeof (getUrlVars()['dp-1-page']) != undefined){
                    dp1_page = getUrlVars()['dp-1-page'];
                }
                
                var ids = [];
                var selected = localStorage.getItem('ec_cross-list-grid_cross_ids_'+'<?= $id ?>');
                var crossesObj = JSON.parse(selected);
                if (crossesObj && typeof crossesObj === "object") {
                    selected = crossesObj;
                } else {
                    selected = null
                }
                selected = (selected == null) ? {} : selected;
                for(var crossId in selected) {
                    ids.push(crossId)
                }
            
                if(typeof reqField === 'undefined'){
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> You have not selected a variable.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
                
                if ($('#'+selectedFieldId).val() == "" || $('#'+selectedFieldId).val() == null){
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> You have not selected/entered a value.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }else{
                
                    if(ids.length > 0){
                        var response = checkFields2();
                        $('#'+$fieldId).removeClass('invalid');

                        //show loading of update
                        $('#instructions').html(' <div class="progress"><div class="indeterminate"></div></div>');
                        $('#config-var').html('');
                        $('#bulk-update-selected').addClass('disabled');
                        $('#bulk-update-all').addClass('disabled');
                        $('#cancel-save-btn').hide();

                        $.ajax({
                            url: '<?php echo Url::to(['create/update-cross-variable','id'=>$id, 'program'=>$program])?>',
                            type: 'post',
                            dataType: 'json',
                            cache: false,
                            data: {
                                idsArr: JSON.stringify(ids),
                                page: page,
                                perPage: perPage,
                                dp1_page: dp1_page,
                                dp1_perPage: dp1_perPage,
                                variable_abbrev: $variable,
                                mode: 'selected',
                                inputVal: reqField
                            },
                            success: function(response) {
                                $('#manage-cross-modal').modal('hide');
                                $.pjax.reload({
                                    container: '#cross-list-grid-pjax',
                                    replace:false
                                });
                            },
                            error: function(xhr, ajaxOptions, thrownError){
                            },
                        });
                    }else{
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> You have not selected any entry. Select at least one using the checkbox in the gridview below.</span>";
                            Materialize.toast(notif, 5000);
                        }
                        e.preventDefault();
                        e.stopImmediatePropagation();
                    }
            }
        }
        });
        $('#bulk-update-all').click(function(e){
   
            if(typeof $fieldIdSelected === 'undefined' || $fieldIdSelected == null || $fieldIdSelected  == ''){
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> You have not selected a variable.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            }else{
                var selectedFieldId = $('.control-group').attr('class').split("control-group field-")[1];

                var divFieldId = $('.control-group').attr('class');
                
                var reqField =  $('#'+selectedFieldId).val();
                var $fieldId = selectedFieldId;

                var getUrlVarsVal = getUrlVars();
                var page = '';
                var perPage = '';
            
                if(typeof page != undefined && (typeof perPage != undefined)){
                    page = getUrlVars()['page'];
                    perPage = getUrlVars()['per-page'];
                }

                var dp1_page = '';
                var dp1_perPage = '';

                if(typeof (getUrlVars()['dp-1-per-page']) != undefined){
                    dp1_perPage = getUrlVars()['dp-1-per-page'];
                }

                if(typeof (getUrlVars()['dp-1-page']) != undefined){
                    dp1_page = getUrlVars()['dp-1-page'];
                }

                if($('#'+selectedFieldId).length){
                    //Field exists
                }else{
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> You have not selected a variable.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            
                if ($('#'+selectedFieldId).val() == "" || $('#'+selectedFieldId).val() == null){
                    
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> You have not selected/entered a value.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }else{
                    $('#'+$fieldId).removeClass('invalid');

                    //show loading of update
                    $('#instructions').html(' <div class="progress"><div class="indeterminate"></div></div>');
                    $('#config-var').html('');
                    $('#bulk-update-selected').addClass('disabled');
                    $('#bulk-update-all').addClass('disabled');
                    $('#cancel-save-btn').hide();

                    $.ajax({
                        url: '<?php echo Url::to(['create/update-cross-variable','id'=>$id, 'program'=>$program])?>',
                        type: 'post',
                        dataType: 'json',
                        cache: false,
                        data: {
                            page: page,
                            perPage: perPage,
                            dp1_page: dp1_page,
                            dp1_perPage: dp1_perPage,
                            variable_abbrev: $variable,
                            inputVal: reqField,
                            mode: 'all'
                        },
                        success: function(response) {
                            $('#manage-cross-modal').modal('hide');
                            $.pjax.reload({
                              container: '#cross-list-grid-pjax',
                              replace:false
                            });
                        },
                        error: function() {
                        }
                    });
                }
            }
        });
        
    });
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    function checkField(){
        var response = [];
        response['success'] = true;
        response['required'] = null;
        response['datatype'] = null;
        var tempArr = [];
        var count = 0;
        $(".required-field").each(function(){
            if($(this).val() == ''){
                response['success'] = false;
                var strArr = (this.id).split('-');
                var label =  $('label[for=' + strArr[0] + ']').attr("data-label");
                var count = tempArr.length;
                tempArr.push((count+1)+'. '+label);
            }
        });
        response['required'] = tempArr.join("<br>");

        tempArr = [];
        count = 0;
        $(".invalid").each(function(){
            response['success'] = false;
            var strArr = (this.id).split('-');
            var label =  $('label[for=' + strArr[0] + ']').attr("data-label");
            count = tempArr.length;
            tempArr.push((count+1)+'. '+label);
            
        });
        response['datatype'] = tempArr.join("<br>");
        return response;
    }
</script>

<style>
    .modal-open .select2-dropdown {
        z-index: 10060;
    }
    #req-vars{
        visibility: visible;
        width: '';
        height: '';
    }
</style>