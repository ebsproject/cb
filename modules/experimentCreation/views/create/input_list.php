<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for input list for entry list selection
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div id="germplasm-inst-div">
    <p><?= \Yii::t('app', 'Enter a list of germplasm/lines in the text area below. Entries can be separated by comma (,) or new line (Enter key). Max of '.$inputListLimitBgThreshold.' records per validation.') ?></p>
    <p><?= \Yii::t('app', 'Highlighted rows means that your input is different from the standard germplasm.') ?></p>
</div>
<div id="package-inst-div" class="hidden">
    <p><?= \Yii::t('app', 'Enter a list of package labels in the text area below. Entries can be separated by comma (,) or new line (Enter key). Max of '.$inputListLimitBgThreshold.' records per validation.') ?></p>
    <div class='alert alert-warning' style='padding: 10px;'>NOTE: Only <b>unique package labels within the program</b> will be VALID.</div>
</div>
 <div class="switch pull-right">
    <input type="radio" id="option-germplasm" name="option" value="option-germplasm" checked>
    <label for="option-germplasm">Germplasm</label>&nbsp;&nbsp;&nbsp;
    <input type="radio" id="option-package-label" name="option" value="option-package-label">
    <label for="option-package-label">Package Label</label>&nbsp;&nbsp;&nbsp;
    <input type="radio" id="option-package-code" name="option" value="option-package-code">
    <label for="option-package-code">Package Code</label>
  </div>
<div id="loading-div" hidden>
  <div class="progress"><div class="indeterminate"></div></div>
</div>

<div id="product-textarea">

<?= Html::textArea('product-list-textarea',"",[
    'id'=>'product-list-textarea',
    'style'=>'background-color:white; resize: none; min-height: 300px !important; '
]); ?>

<button disabled id="search-products-btn" class="btn btn-primary waves-effect waves-light pull-right">
    <?= \Yii::t('app', 'Search') ?>
    <i class="material-icons right">search</i>
</button>

</div>

<div id="products-view" style="margin-top: 35px;">

</div>


<?php
    $searchProductsUrl = Url::to(['create/search-product-list','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $searchPackageLabelsUrl = Url::to(['create/search-package-labels','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $searchPackageCodesUrl = Url::to(['create/search-package-codes','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>

<script>
    $(document).ready(function(){

        $('#product-list-textarea').on('input change keyup', function() {

            if($(this).val().trim() != ''){
                $("#search-products-btn").prop('disabled', false);

            }else{
                $("#search-products-btn").prop('disabled', true);
            }

        });


        $(document).on('click', '#back-btn', function(){ 
            $('#product-textarea').show();
            $('#products-view').html('');
            $('#products-view').hide();     
            $('#option-germplasm').removeAttr('disabled');
            $('#option-package-label').removeAttr('disabled');
            $('#option-package-code').removeAttr('disabled');
        });

        $('#search-products-btn').click(function(){
            $('#option-germplasm').attr('disabled', 'true');
            $('#option-package-label').attr('disabled', 'true');
            $('#option-package-code').attr('disabled', 'true');

            var url = '<?php echo $searchProductsUrl; ?>';
            if($("#option-package-label").prop('checked') == true) {
                var url = '<?php echo $searchPackageLabelsUrl; ?>';
            } else if($("#option-package-code").prop('checked') == true) {
                var url = '<?php echo $searchPackageCodesUrl; ?>';
            }

            var productList = $('#product-list-textarea').val();

            if (productList.trim() != '') {

                $('#product-textarea').hide();
                $('#products-view').show();      
                $('#loading-div').show();
                //get the entries of this experiment
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        experimentId: '<?php echo $id; ?>',
                        productList: productList
                    },
                    success: function(response) {
                        $('#loading-div').hide();
                        $('#products-view').html(response);
                    },
                    error: function() {

                    }
                });

            }

            

        });

    });


</script>