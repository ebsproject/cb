<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders site tab for experiment creation
 */

use yii\bootstrap\Modal;

use yii\helpers\Html;
use yii\helpers\Url;

use kartik\dynagrid\DynaGrid;
use app\modules\experimentCreation\models\ExperimentModel;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;


$disabled = '';
if (strpos($model['experimentStatus'], 'occurrences created') === false) {
    $disabled = 'disabled';
}

echo '<div class="bgStatus-notif hidden"></div>';

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model'=>$model,
    'program'=>$program,
    'saveBtnLabel'=>'Save',
    'nextBtn' => '',
    'btnName' => 'next-btn'
]);

$selectedItems = isset($selectedItems) ? $selectedItems : [];
$selectedItemsCount = count($selectedItems) > 0 ? count($selectedItems) : 'No';

echo '<div class="col col-sm-12 view-entity-content">';

$browserId = 'dynagrid-ec-site-list';

$columns = [
    // TODO: To be enabled once bulk update feature is refactored
    [
        'label'=> "Checkbox",
        'header'=>'
            <input type="checkbox" class="filled-in" id="occurrence-select-all" />
            <label for="occurrence-select-all"></label>
        ',
        'content'=>function($data) use ($selectedItems){
            $occurrenceDbId = $data['occurrenceDbId'];

            if(in_array($occurrenceDbId, $selectedItems)){
                $checkbox = '<input class="occurrence-grid-select filled-in" type="checkbox" checked="checked" id="'.$data['occurrenceDbId'].'" />';
            }
            else{
                $checkbox = '<input class="occurrence-grid-select filled-in" type="checkbox" id="'.$data['occurrenceDbId'].'" />';
            }
            return $checkbox.'
                <label for="'.$data['occurrenceDbId'].'"></label>       
            ';
        },        
        'hAlign'=>'center',
        'vAlign'=>'top',
        'hiddenFromExport'=>true,
        'mergeHeader'=>true,
        'order'=> DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'class' => 'yii\grid\SerialColumn',
        'header' => false,
        'order'=> DynaGrid::ORDER_FIX_LEFT
    ]
];

$columns = \Yii::$container->get('app\modules\experimentCreation\models\ExperimentModel')->buildColumns($id, $configData, $columns, 'occurrence');

$addSitesBtn = '';
if(in_array($model['experimentType'], ['Observation', 'Breeding Trial'])){
    $addSitesBtn = Html::button('Use Site list', ['type'=>'button', 'title'=>\Yii::t('app', 'Select site list'), 'class'=>'light-green darken-3 btn', 'style'=>'margin-right:5px;', 'id'=>'select-sitelist-btn']);
}
//dynagrid configuration
$dynagrid = DynaGrid::begin([
    'columns' => $columns,
    'theme'=>'simple-default',
    'showPersonalize'=>true,
    'storage' => DynaGrid::TYPE_SESSION,
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => 'grid-occurrence-id',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' =>'grid-occurrence-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsiveWrap'=>false,
        'panel'=>[
            'heading' => false,
            'before' => \Yii::t('app', "Specify the required information for each of the occurrences. All changes are autosaved.").'&emsp;'.'{summary}<br/><p id = "selected-items" class = "pull-right" style = "margin:-1px"><b><span id = "selected-count">'.$selectedItemsCount.'</span></b> selected items.</p>',
            'after' => false,
        ],
        'toolbar' => [
            ['content' => '&nbsp;'],//just for the space between summary and action buttons
            ['content' => $addSitesBtn],
            [
                'content' =>
                    // Html::button('<i class="glyphicon glyphicon-plus"></i> ', ['type'=>'button', 'title'=>Yii::t('app', 'Add Sites from list'), 'class'=>'light-green darken-3 btn', 'id'=>'add-location-list-btn']).
                    Html::button('<i class="glyphicon glyphicon-edit"></i> ', ['type'=>'button', 'title'=>Yii::t('app', 'Bulk update site'), 'class'=>'btn', 'id'=>'add-bulk-location-btn', 'data-toggle' => 'modal','data-target' => '#bulk-update-location-modal'])    
            ],
            [
                'content'=> Html::a('<i class="glyphicon glyphicon-repeat"></i>',
                    ['create/specify-occurrences?id='.$id.'&program='.$program.'&processId='.$processId.'&reset=hard_reset'],
                    ['data-pjax'=>0, 'class' => 'btn btn-default reset-btn', 'title'=>\Yii::t('app','Reset grid'), 'data-pjax'=>true]).'{dynagridFilter}{dynagridSort}{dynagrid}',
            ], 
        ],
        'floatHeader' =>true,
        'floatOverflowContainer'=> true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => 'Remove',
    ],
    'options'=>['id'=>$browserId] // a unique identifier is important
]);

DynaGrid::end();

echo '</div>';

//modal for record generation
Modal::begin([
    'id' => 'show-status-modal',
    'header' => Yii::t('app', '<h4 id="gen-modal-header">Generate Records</h4>'),
    'footer' => Html::a(\Yii::t('app','Close'),
            ['#'],
            [
                'class'=>'btn btn-primary waves-effect waves-light',
                'id'=>'cancel-save-status-btn',
                'data-dismiss'=>'modal'
            ]).'&emsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
]);

echo '<div id="message-status"></div>';
echo '<div id="loading-indicator" class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
echo '<div id="message-notif" class="hidden"></div>';
echo '<br/>';

Modal::end();

//modal for field validation
Modal::begin([
    'id' => 'required-field-modal',
    'header' => '<i class="material-icons red-text left">warning</i> <h4>'.\Yii::t('app','Unable to proceed').'</h4>',
    'footer' => Html::a(\Yii::t('app','Close'),['#'],['data-dismiss'=>'modal', 'class'=>'ok-btn btn btn-primary waves-effect waves-light modal-close']),
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static']
]);
echo '<br/>
    <p>'.\Yii::t('app', 'Please correct the errors below before proceeding.').'</p>
    <p id="required-field-notif""></p>';
Modal::end();

//New Modal for site management, Bulk Update button - select variables
Modal::begin([
    'id'=> 'bulk-update-location-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i>  '.\Yii::t('app','Bulk Update').'</h4>',
    'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'close-occ-bulk-update-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        '<a id="bulk-update-selected" class="btn btn-primary green waves-effect waves-light confirm-update-btn" mode ="selected">'.\Yii::t('app','Selected').'</a>&nbsp;'.
        '<a id="bulk-update-all" class="btn btn-primary teal waves-effect waves-light confirm-update-btn" mode ="all">'.\Yii::t('app','All').'</a>',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static', 'tabindex' => false],
]);
echo '<div id = "modal-progress" class = "progress hidden"><div class="indeterminate"></div></div>';
echo '<div id = "modal-content"><p id="instructions"></p>';
echo '<span id="note"></span>';
echo '<div id="config-var" style="margin-left:30px;"></div></div>';
Modal::end();

//Modal for selection of site list
    Modal::begin([
        'id' => 'site-list-selection-modal',
        'header' => \Yii::t('app','<h4>Select Site List</h4>'),
        'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            '<a id="confirm-site-btn" class="btn btn-primary waves-effect waves-light sort-btn">Confirm</a>',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static'],
    ]);
    Modal::end();

$saveRowFieldUrl = Url::to(['create/save-row-field']);
$checkUrl = Url::to(['create/check-occurrences', 'id'=>$id, 'program'=>$program]);
$saveUrl = Url::to(['create/generate-records', 'id'=>$id, 'program'=>$program]);
$removePlotRecUrl = Url::to(['remove/remove-occurrence-records','id'=>$id]);
$createOccurrenceRecUrl = Url::to(['create/create-occurrence-record', 'id'=>$id, 'program'=>$program]);
$checkPAFlagUrl = Url::to(['create/check-pa-flag','id'=>$id]);
$deleteRecordUrl = Url::toRoute(['/experimentCreation/browse/background-occurrence-check','id'=>$id]);
$checkPlotRecordUrl = Url::toRoute(['/experimentCreation/browse/check-plot-record','id'=>$id]);
$actionUpdateOccFilterUrl  = Url::toRoute(['create/update-occ-filter','id'=>$id]);
$renderBulkUpdateUrl = Url::to(['create/render-bulk-update','id'=>$id,'program'=>$program]);
$genericCheckReqVarsUrl = Url::to(['create/generic-check-required', 'id'=>$id, 'program'=>$program]);
$genericRenderConfigUrl = Url::to(['create/render-config-form', 'id'=>$id, 'program'=>$program]);
$checkThresholdCountUrl = Url::to(['create/check-threshold-count','id'=>$id]);
$getSelectedItemsUrl = Url::to(['create/get-selected-items']);
$bulkUpdateFieldsUrl = Url::to(['create/bulk-update-fields']);
$checkStatusUrl  = Url::to(['create/check-status','id'=>$id, 'action'=>'specify-occurrences']);
$storeSelectedSessionUrl = Url::to(['default/store-selected-session']);
$getSelectedSessionUrl = Url::to(['default/get-selected-session']);
$occurrenceUrl = Url::to(['default/get-selected-session', 'id'=>$id, 'program'=>$program,'processId'=>$processId]);
$destroySessionUrl = Url::to(['default/destroy-session', 'id'=>$id]);
$deleteFlag = ($deleteFlag == false) ? 'false' : 'true';
$configValues = json_encode($requiredFields);
$dataProviderCount = sizeOf($dataProvider->allModels);
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);
$retrieveSiteList = Url::to(['occurrence/render-site-list', 'id'=>$id, 'program'=>$program, 'occurrenceCount'=>$occurrenceCount]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

Yii::$app->view->registerJs("
    var genericConfUpdateUrl = '".Url::toRoute(['/experimentCreation/create/update-config-variable','id'=>$id, 'program'=>$program, 'processId' => $processId])."',
    genericRenderConfigUrl = '".$genericRenderConfigUrl."',
    experimentId = '".$id."',
    requiredFieldJson = '".$configValues."',
    getSelectedItemsUrl = '".$getSelectedSessionUrl."',
    checkThresholdCountUrl = '".$checkThresholdCountUrl."',
    bulkUpdateFieldsUrl = '".$bulkUpdateFieldsUrl."',
    dataProviderCount = '".$dataProviderCount."',
    checkStatusUrl = '".$checkStatusUrl."',
    renderedView = 'specify-occurrences',
    checkStatValue = 'occurrences created'
    ;",
    \yii\web\View::POS_HEAD);

$this->registerJsFile("@web/js/generic-form-validation.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJsFile("@web/js/experiment-creation/modal_bulk_update.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
    var deleteFlag = '$deleteFlag';
    var isPlotsGenerated = false;
    var deleteRecordUrl = '$deleteRecordUrl';
    var checkPlotRecordUrl = '$checkPlotRecordUrl';
    var actionUpdateOccFilterUrl = '$actionUpdateOccFilterUrl';
    var getExperimentTotalPlots = 0;
    var bgMsgDelete = '';
    var bgMsgCreate = '';
    var bgDeleteFailed = false;
    var bgCreateFailed = false;
    var createPlantingInstruction = '$createPlantingInstruction';
    var siteColumnId = '';
    var fieldColumnId = '';
    var dataMem = '';
    var checkAll = false;
    
    if(deleteFlag == true || deleteFlag == 1 || deleteFlag == 'true'){
        removeOccurrenceRecord();
        $('.bgStatus-notif').removeClass('hidden');
        $('.bgStatus-notif').html('<div class="alert ">'+ 
                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                    '<i class="fa fa-info-circle"></i>'+
                        ' Deletion of previously generated occurrence, plot and/or planting instruction records is still in progress.' + 
                '</div></div>');
        $('#next-btn').attr('disabled', true);

        notificationInterval=setInterval(updateNotif, 15000);
        
    }else{
        //This means that there's no need to check the background job
        if(bgDeleteFailed == true && bgMsgDelete != null){
            $('.bgStatus-notif').removeClass('hidden');
            $('.bgStatus-notif').html('<div class="alert ">'+ 
                    '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                        '<i class="fa fa-info-circle"></i>'+
                             bgMsgDelete
                             +
                    '</div></div>');
        }else{
            $('.bgStatus-notif').addClass('hidden');
        }
        
        $('#next-btn').attr('disabled', false);
    }
    
    if((isPlotsGenerated == false || isPlotsGenerated == 0)){
        notificationInterval=setInterval(updateNotifRecord, 15000);
    }
    
    if(createPlantingInstruction == true || createPlantingInstruction == 'true' || createPlantingInstruction == '1' || createPlantingInstruction == 1){
        createOccurrenceRecord(getExperimentTotalPlots);
        $('.bgStatus-notif').removeClass('hidden');
        $('.bgStatus-notif').html('<div class="alert ">'+ 
                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                    '<i class="fa fa-info-circle"></i>'+
                        ' Generation of plot and/or planting instruction records is still in progress.' + 
                '</div></div>');
        $('#next-btn').attr('disabled', true);
    }else{
        if(bgCreateFailed == true && bgMsgCreate != null){
            $('.bgStatus-notif').removeClass('hidden');
            $('.bgStatus-notif').html('<div class="alert ">'+ 
                    '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                        '<i class="fa fa-info-circle"></i>'+
                             bgMsgCreate
                             +
                    '</div></div>');
        }else{
            $('.bgStatus-notif').addClass('hidden');
        }
        
        $('#next-btn').attr('disabled', false);
    }

    $("document").ready(function() {
        //call destroy session here
        destroySession();

        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
    });
    

    //*For REference
    // $(document).on('keypress keyup blur paste change', '[class*=input-validate]', function(e) {
    //     var fields = this.id;
    //     var field = this.id.split('-');
    //     var dataType = field[1];
    //     var targetColumn = $(this).attr('target-column');
    //     var baseModel = 'Occurrence';
    //     var value = dataType !== 'metadata' ? $('#' + fields).val() : $('#' + fields).find(':selected').text();
    //     var dataMem = $(this).attr('data-mem');
    //     console.log('Data Member > ',dataMem);
    //     // Save the changes in the database
    //     $.ajax({
    //         url: '$saveRowFieldUrl',
    //         type: 'post',
    //         datatType: 'json',
    //         cache: false,
    //         data: {
    //             fields: fields,
    //             targetColumn: targetColumn,
    //             baseModel: baseModel,
    //             value: value
    //         },
    //         success: function(response){

    //             if(targetColumn == 'siteDbId' || targetColumn == 'fieldDbId'){
    //                 $.pjax.reload({container: '#$ browserId-pjax'});
    //                 var hasToast = $('body').hasClass('toast');
    //                 if(!hasToast){
    //                     $('.toast').css('display','none');
    //                     var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully saved.</span>";
    //                     Materialize.toast(notif, 5000);
    //                 }
    //             }
    //         },
    //     });
    // });
        
    //Row updates
    $(document).on('change', "[class*=editable-field]", function(e) {
        var fields = this.id;
        var field = this.id.split('-');
        var dataType = field[1];
        var targetColumn = $(this).attr('target-column');
        var baseModel = 'Occurrence';
        var value = dataType !== 'metadata' ? $('#' + fields).val() : $('#' + fields).find(':selected').text();
        var dataMem = $(this).attr('data-mem');

        // Save the changes in the database
        e.preventDefault();
        
        $('#$browserId-pjax').addClass('kv-grid-loading');
        var formType = '';
        if(fields.indexOf('modal') > -1){
            formType = 'bulk-update';
        }
        updateFilterTags(e,dataMem, this.id, value, baseModel,targetColumn, formType);
    });
    refresh();

    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
        refresh();

        //select grid
        $(document).on('click','.occurrence-grid-select',function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
            var unset;

            if($(this).prop('checked')){
                unset = false;
            }else{
                unset = true;
                $('#occurrence-select-all').prop('checked',false);
                $('#occurrence-select-all').removeAttr('checked');
                checkAll = false;
            }
        });

        //Show site list
        $('#select-sitelist-btn').click(function(){
            
            showSiteSelection();
        });
    });

    //select grid occurrences
    $(document).on('click','.occurrence-grid-select',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var count = $('#selected-count').val() == 'No' ? 0 : $('#selected-count').val();
        var unset;
        var occurrenceId = $(this).attr("id");
        var url = '$storeSelectedSessionUrl';
     
        if($(this).prop('checked')){
            unset = false;
        }else{
            unset = true;
            $('#occurrence-select-all').prop('checked',false);
            $('#occurrence-select-all').removeAttr('checked');
        }

        $.ajax({
            url: url,
            type: 'post',
            async:false,
            data: {
                occurrenceDbId: $(this).prop('id'),
                unset: unset,
                storeAll: false,
                id : '$id'
            },
            success: function(response) {
                setSelectedCounter(response)
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    // select all occurrences
    $(document).on('click','#occurrence-select-all',function(e){
        var url = '$storeSelectedSessionUrl';

        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.occurrence-grid-select').attr('checked','checked');
            $('.occurrence-grid-select').prop('checked',true);
            $(".occurrence-grid-select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");

        }else{

            $(this).removeAttr('checked');
            $('.occurrence-grid-select').prop('checked',false);
            $('.occurrence-grid-select').removeAttr('checked');
            $("input:checkbox.occurrence-grid-select").parent("td").parent("tr").removeClass("grey lighten-4");
        }

        var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
        var unset = $(this).prop('checked') ? false : true;

        $.ajax({
            url: url,
            type: 'post',
            async: false,
            cache: false,
            data: {
                occurrenceDbId: 0,
                unset: unset,
                storeAll: true,
                id : '$id'
            },
            success: function(response) {
                setSelectedCounter(response)
            },
            error: function(e) {
                console.log(e);
            }
        });

    });

    //For the Bulk update of Sites
    $(document).on('click','#add-bulk-location-btn',function(){
        $('#config-var').removeAttr('tabindex');
        $.ajax({
            url: '$renderBulkUpdateUrl', 
            type: 'post',
            dataType: 'json',
            data: {
                configVars: '$configValues'
            },
            success: function(response){
                $('#instructions').html("Bulk update the following values for the occurrence records. The changes will be reflected after closing the modal.");
                              
                $('#config-var').html(response);
                $('.loading-req-vars').remove();
                $('#req-vars').css('visibility','visible');
                $('#req-vars').css('width','');
                $('#req-vars').css('height','');
            }
        });
    });

    function refresh(){
        var maxHeight = ($(window).height() - 320);
        $(".kv-grid-wrapper").css("height", maxHeight);

        // update selected count
        var url = '$getSelectedSessionUrl';

        $.ajax({
            url: url,
            type: 'post',
            async: false,
            cache: false,
            data: {
                action: 'occurrences',
                experimentDbId : '$id'
            },
            success: function(response) {
                response = JSON.parse(response)
                setSelectedCounter(response.selectedItemsCount)

                $("input:checkbox.occurrence-grid-select").each(function(){
                    var occurrenceDbId = $(this).parent("td").parent("tr").attr('data-key');
                    var selected = response.selectedItems

                    for (const index in selected) {
                        if(parseInt(occurrenceDbId) == parseInt(selected[index])) {
                            $(this).attr('checked','checked');
                            $(this).prop('checked',true);
                            $(this).parent("td").parent("tr").addClass('grey lighten-4');
                            break;
                        }
                    }
                });
            },
            error: function(e) {
                console.log(e);
            }
        });
    }
  
    // click row in occurrence browser
    $(document).on('click', '#$browserId tbody tr', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var thisRow = $(this).find('input:checkbox')[0];
    
        $("#"+thisRow.id).trigger("click");
        if(thisRow.checked){
            $(this).addClass("grey lighten-4");
        }else{
            $(this).removeClass("grey lighten-4");
        }
    });

    // reset selection
    $(document).on('click','.reset-btn',function(e){
        var url = '$storeSelectedSessionUrl';

        $('#occurrence-select-all').removeAttr('checked');
        $('.occurrence-grid-select').prop('checked',false);
        $('.occurrence-grid-select').removeAttr('checked');
        $("input:checkbox.occurrence-grid-select").parent("td").parent("tr").removeClass("grey lighten-4");

        $.ajax({
            url: url,
            type: 'post',
            async: false,
            data: {
                occurrenceDbId: 0,
                unset: true,
                storeAll: true,
                id : '$id'
            },
            success: function(response) {
                setSelectedCounter(response)
            },
            error: function(e) {
                console.log(e);
            }
        });

    });

    $('#next-btn').click(function(e){
        $('#next-btn').attr('disabled', true);
        var response = checkFields2();
        var showStatusModal = '#show-status-modal > .modal-dialog > .modal-content > .modal-body';
         
        if(response['success']){
            e.preventDefault();
            e.stopImmediatePropagation();

            var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
         
            var nextUrl = $(this).attr('data-url');
            $.ajax({
                url: '$checkUrl',
                type: 'post',
                dataType: 'json',
                data:{},
                async: false,
                success: function(response) {
                    var data = response['response'];
                    deleteFlag = response['deleteFlag'];
                    getExperimentTotalPlots = response['getExperimentTotalPlots'];
                    
                    if(data !== null){
                        if(data['proceedNext']){
                            window.location = nextUrl;
                        } else{
                            createOccurrenceRecord(getExperimentTotalPlots);
                        }
                    } else{
                        console.log('There seems to be a problem in retrieving the responses.');
                        location.reload();
                    }
                    
                },
                error: function(e) {
                    console.log('There seems to be a problem in retrieving the responses.' + e);
                }
            });
        } else {
            $('#required-field-notif').html('');
            errorMsg = response['errorMsg'];
            if(errorMsg != null && errorMsg != undefined && errorMsg != ''){
                $("#required-field-notif").append('<p> <b>Required Fields</b> <br>'+errorMsg+'</p>');
            }
            if(response['datatype'] != null && response['datatype'] != undefined && response['datatype'] != ''){
                $("#required-field-notif").append('<p> <b>Invalid Input</b> <br>'+response['datatype']+'</p>');
            }

            $('#required-field-modal').modal('show');
        }
    });

    //Show site list
    $('#select-sitelist-btn').click(function(){
        
        showSiteSelection();
    });

    function showSiteSelection(){
        var url = '$retrieveSiteList';
        var content_modal = '#site-list-selection-modal > .modal-dialog > .modal-content > .modal-body';
            
        $('#site-list-selection-modal').modal('show');
        $(content_modal).html(loadingIndicator);

        setTimeout(function(){
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                },
                success: function(response) {
                    if (response) {
                        $(content_modal).html(response.htmlData);
                    }
                },
                error: function() {
                    $(content_modal).html('<div class="card-panel red"><span class="white-text">There seems to be a problem loading the site list  .</span></div>');
                }
            });
        },10);
    }
    function endSaveData(dataArray){
        var nextUrl = $('#next-btn').attr('data-url');
        
        if(dataArray['success']){
            var message = dataArray['message'];
            $('#message-status').html(message);
            $('#loading-indicator').html('');
            $('#show-status-modal').modal('hide');
            window.location.href = nextUrl;
        } else {
            var message = "<h6><i class='material-icons red-text left'>warning</i> Process for "+dataArray['name']+" failed! Please try again.</h6><br>" +
                '<b>ERROR:</b> <em><span class="red-text">'+dataArray['status']+'</span></em>';
            $('#message-status').html(message);
            $('#loading-indicator').html('');
            $('#cancel-save-status-btn').removeAttr('disabled');
        }
    }

    //Removes the previously generated records
    function removeOccurrenceRecord(){
        var showStatusModal = '#show-status-modal > .modal-dialog > .modal-content > .modal-body';

        $.ajax({
            url: '$removePlotRecUrl',
            type: 'post',
            dataType: 'json',
            beforeSend: function (response) {
                $('#gen-modal-header').html('<i class="fa fa-bell"></i> Notification');
                $('#show-status-modal').modal('show');
                message = '<h6><em>Please wait while the deletion of previously generated records is in progress.<em></h6><br>'
                $('#message-status').html(message);
                $('#system-loading-indicator').html('');
                $('#cancel-save-status-btn').attr('disabled', 'disabled');
                var hideCloseBtn = '#show-status-modal > .modal-dialog > .modal-content > button.close';
                $(hideCloseBtn).addClass('hidden');
            },
            success: function(response) {
                if(response !== null && response['success']){
                    var dataArray = {};
                    message = '<h6><em>Please wait while the deletion of previously generated records is in progress.<em></h6><br>'
                    $('#message-status').html(message);
                
                    $('#loading-indicator').html('');
                    $('#show-status-modal').modal('hide');
                    deleteFlag = response['deleteFlag'];

                    if(deleteFlag == 0 || deleteFlag == false){
                        $('.bgStatus-notif').addClass('hidden');
                        $('#next-btn').attr('disabled', false);
                        deleteFlag = true;

                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>"+response['message']+"</span>";
                            Materialize.toast(notif, 5000);
                        }
                        location.reload();
                    }else{
                        $('.bgStatus-notif').removeClass('hidden');
                        $('.bgStatus-notif').html('<div class="alert ">'+ 
                                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                    '<i class="fa fa-info-circle"></i>'+
                                        ' Deletion of previously generated occurrence, plot and/or planting instruction records is still in progress. Please reload the page to check the updates.' + 
                                '</div></div>');
                        $('#next-btn').attr('disabled', true);
                    }
                } else{
                    //Deletion failed
                    $('#loading-indicator').html('');

                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>"+response['message']+"</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    
                }
                
            }
        });
    }

    //Insert New Occurrence plot and/or planting instruction records
    function createOccurrenceRecord(getExperimentTotalPlots){
        var loadingIndicator2 = '<div class="progress"><div class="indeterminate"></div></div>';
         
        $.ajax({
            url: '$createOccurrenceRecUrl',
            type: 'post',
            dataType: 'json',
            data: {
                getExperimentTotalPlots:getExperimentTotalPlots
            },
            cache: false,
            beforeSend: function (response) {
                $('#show-status-modal').modal('show');
                message = '<h6><em>Please wait while the generation of plot and/or planting instruction records is in progress.<em></h6><br>'
                $('#message-status').html(message);
                $('#gen-modal-header').html('<i class="fa fa-bell"></i> Generate Records');
                $('#cancel-save-status-btn').attr('disabled', 'disabled');
                var hideCloseBtn = '#show-status-modal > .modal-dialog > .modal-content > button.close';
                $(hideCloseBtn).addClass('hidden');
            },
            success: function(response) {
                var dataArray = {};
              
                if(response !== null && response['success']){
                    const keys = Object.keys(response)
                    for(var j=0; j<keys.length; j++){
                        dataArray[keys[j]] = response[keys[j]];
                    }
                    
                    message = '<h6><em>Please wait while the generation of plot and/or planting instruction records is in progress.<em></h6><br>'
                    $('#message-status').html(message);
                    
                    if(typeof response['createPlantingInstruction'] != undefined && (response['createPlantingInstruction'] == true || response['createPlantingInstruction'] == 'true')){
                         //Do nothing
                        $('.bgStatus-notif').removeClass('hidden');
                        $('#next-btn').attr('disabled', true);
                        if(bgMsgCreate != null && bgMsgCreate != ''){
                            $('.bgStatus-notif').html('<div class="alert ">'+ 
                                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                    '<i class="fa fa-info-circle"></i>'+
                                        bgMsgCreate + 
                                '</div></div>');
                        }else{
                            $('.bgStatus-notif').html('<div class="alert ">'+ 
                                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                    '<i class="fa fa-info-circle"></i>'+
                                        ' Creation of plot and/or planting instruction records is still in progress. Please reload the page to check the updates.' + 
                                '</div></div>');
                        }
                    }else{
                        endSaveData(dataArray);    
                    }           
                } else{

                    if(createPlantingInstruction == true || createPlantingInstruction == 'true'){
                        //Do nothing
                    }else{  
                        dataArray['success'] = false;
                        dataArray['message'] = response['message'];
                        dataArray['name'] = 'generation of plot and/or planting instruction records';
                        dataArray['status'] = response['message'];
                        $('#loading-indicator').html('');
                        endSaveData(dataArray);
                    }
                    
                }   
            }
        });
    }

    //for the deletion of plot and planting instruction records
    function updateNotif() {
        $.getJSON(deleteRecordUrl, 
        function(json){
            if(Array.isArray(json) || typeof json == 'object'){
                deleteFlag = json['success'];
                if(typeof json['message'] != undefined && json['message'] != null){
                    bgMsgDelete = json['message'];
                    var dataArray = {};
                    dataArray['success'] = false;
                    dataArray['message'] = 'Processor Error: '+json['message'];
                    dataArray['name'] = 'deletion of plot and/or planting instruction records';
                    dataArray['status'] = json['message'];
                    $('#loading-indicator').html('');
                    endSaveData(dataArray);
                }
                if(typeof json['status'] != undefined && json['status'] == 'failed'){
                    bgDeleteFailed = true;
                }
               
            }else{
                if(json == 0 || json == false || json == 'false'){
                    deleteFlag = false;
                }else{
                    //this can be true or ["success"=>true]
                    deleteFlag = json;
                }
            }
        });
    }

    // Check the generated plot records
    function updateNotifRecord() {
        if($('#show-status-modal').hasClass('in') === false){
            $.getJSON(checkPlotRecordUrl, 
            function(json){
                
                if(Array.isArray(json) || typeof json === 'object'){
                    
                    isPlotsGenerated = json['success'];
                    if(typeof json['message'] != undefined && json['message'] != null){
                        bgMsgCreate = json['message'];
                        var dataArray = {};
                        dataArray['success'] = false;
                        dataArray['message'] = 'Processor Error: '+json['message'];
                        dataArray['name'] = 'generation of plot and/or planting instruction records';
                        dataArray['status'] = 'Processor Error: '+json['message'];
                        $('#loading-indicator').html('');
                        endSaveData(dataArray);
                    }
                    if(typeof json['status'] != undefined && json['status'] == 'failed'){
                        bgCreateFailed = true;
                    }
                    if(typeof json['createPlantingInstruction'] != undefined && (json['createPlantingInstruction'] == true || json['createPlantingInstruction'] == 'true')){
                        createPlantingInstruction = true;
                        createOccurrenceRecord(getExperimentTotalPlots);
                        
                    }else{
                        $('#next-btn').attr('disabled', false);
                        $('.bgStatus-notif').addClass('hidden');
                    }
                    
                }else{
                    if(json == 0 || json == false || json == 'false'){
                        isPlotsGenerated = false;
                    }else{
                        //this can be true or ["success"=>true]
                        isPlotsGenerated = json;
                    }
                }
            });
        }
    }
                              
    function updateFilterTags(e, memtype, rowId, value, baseModel, targetColumn, updateMode){
        $.ajax({
            url: '$actionUpdateOccFilterUrl',
            type: 'post',
            dataType: 'json',
            async: true,
            cache: false,
            data: {
                datamem: memtype,
                rowId: rowId,
                value: value, 
                baseModel: baseModel,
                targetColumn: targetColumn,
                updateMode:updateMode
            },
            success: function(response){
                var subMemId = response['submem'];
                var isChanged = response['isChanged']; //true if current saved site value is not equal to selected
                       
                if(memtype == 'SITE'){          
                    //If site is changed, remove prior values selected for the field column
                    if(isChanged){
                        $('#'+subMemId).find('option').remove();
                        var s1 = document.getElementById(subMemId);
                        $.each(response['data'], function (key, value) {
                            var newOption = document.createElement("option");
                            newOption.value = key;
                            newOption.innerHTML = value;
                            s1.options.add(newOption);
                        });
                        $('#'+subMemId).val('').trigger('change');
                        $('#next-btn').removeAttr('disabled');
                    }
                    $('#$browserId-pjax').removeClass('kv-grid-loading');
                }else if(memtype == 'FIELD'){
                    if(isChanged){
                        $("#"+subMemId).val(response['newVal']);
                        $('#'+subMemId).trigger('change');
                    }
                    $('#$browserId-pjax').removeClass('kv-grid-loading');
                }else{
                    $('#$browserId-pjax').removeClass('kv-grid-loading');
                }
            },
            error: function(){
            }

        });
    }

    //set selected counter
    function setSelectedCounter(response){
        count = parseInt(response).toLocaleString();
        if(response == 0) {
            count = 'No';
        }
        $('#selected-count').html(count);
    }

    //Destroy Session
    function destroySession(){
        $.ajax({
            url: '$destroySessionUrl',
            type: 'post',
            dataType: 'json',
            cache: false,
            success: function(response){
            },
            error: function(){
            }

        });
    }
JS
);
?>