<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders view for nursery studies for entry list selection
 */
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Season;
use app\models\Phase;
?>
<?php
    if($partitionView){
?> 
        <span class="new badge">Limited view</span>&nbsp;&nbsp;<em><?= \Yii::t('app', 'We limit the view for this page to lessen the loading time. Click the View All button to show all records in page.') ?></em>
        <a class="waves-effect waves-light btn pull-right" id="view-all-nurseries-id"><i class="material-icons right">visibility</i>View All</a>
<?php 
    }

    if(isset($parentList) && $parentList) { 
        echo '<p>'.Yii::t('app', 'Below are the list of parent list experiments you can import. Note that only parent list that have the same <b>season and year</b> will appear in this browser.').'</p>';
        
        $recordNumAttr = 'no_of_entries';
        $recordNumLabel = Yii::t('app', 'No. of Entries');
    } else {
        echo '<p>'.Yii::t('app', 'Below are the list of nursery experiment you can get your entries from.').'</p>';
        
        $recordNumAttr = 'entryWithSeeds';
        $recordNumLabel = Yii::t('app', 'No. of Entries');
    }

?>

<div class="input-field no-margin search-entry-field-div" style="width:35%">
  <input id="searchTextNurseries" type="search" placeholder="Type filter" class="autocomplete search-text-add-entry-search-input" autocomplete="off" autofocus="" value="">
  <i class="material-icons search-text-entry-clear-all-search-btn" title="Clear all" style="opacity: 0; margin-right:20px; font-size:1.5rem;margin-top:10px;">clear</i>
  <i class="material-icons search-text-entry-submit-btn" style="margin-top:10px; font-size:1.5rem" title="Search">search</i>
</div>
<div id="nurseries-browser">

<?php
echo $grid = GridView::widget([
    'pjax' => true, // pjax is set to always true for this demo
    'dataProvider' => $dataProvider,
    'id' => 'from-nursery',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-entry-table'],
    'columns' => [
        [
          'class' => 'yii\grid\SerialColumn',
          'header' => false
        ], 
        [
            'class'=>'kartik\grid\ActionColumn',
            'header' => false,
            'template' => '{select}',
            'buttons' => [
                'select' => function ($url, $model, $key) {
                    
                    return Html::a('<i class="material-icons">folder_open</i>',
                        '#',
                        [
                            'class'=>'select-experiment',
                            'title' => Yii::t('app','View germplasms coming from this experiment'),
                            'data-experiment-id' => $model['experimentDbId'],
                        ]
                    );
                },
            ]
        ],
        [
            'attribute'=>'experimentName',
            'label' => Yii::t('app', 'Experiment'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'experimentCode',
            'label' => Yii::t('app', 'Experiment Code'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'experimentYear',
            'label' => Yii::t('app', 'Experiment Year'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'seasonCode',
            'label' => Yii::t('app', 'Season'),
            'format' => 'raw',
            'enableSorting' => false
        ],
        [
            'attribute'=>'stageCode',
            'label' => Yii::t('app', 'Stage'),
            'format' => 'raw',
            'enableSorting' => false
        ],
        [
            'label' => Yii::t('app', 'Harvested Plot Count'),
            'value' => function ($data) {
                $count = \Yii::$container->get('app\modules\experimentCreation\models\ExperimentModel')->getHarvestedPlotsCount($data['experimentDbId']);
                if($count == 0){
                    return '<span class="new badge red">0</span>';
                } else {
                    return '<span class="new badge green">'.$count.'</span>';
                }
            },
            'format' => 'raw',
        ]
        
    ],
    'toggleData'=>true,
]);

?>
</div>

<div id="harvested-plots-browser"></div>
<div id="notification"></div>

<?php
    $harvestedPlotsUrl = Url::to(['create/render-harvested-plots','id'=>$id, 'program'=>$program, 'processId'=>$processId, 'parentList'=>$parentList]);
    $viewAllExperiments = Url::to(['view/view-all-nursery-experiments', 'id'=>$currentExperimentId, 'program'=>$program, 'processId'=>$processId]);
?>

<script>
    $(document).on('click', '#back-btn', function(){ 
        $('#nurseries-browser').show();
        $('.search-entry-field-div').show(); 
        $('#harvested-plots-browser').hide();    
    });

    $(document).on('click', '#view-all-nurseries-id', function(){ 
        var url = '<?php echo $viewAllExperiments; ?>';
        window.location = url;
    });

    $(document).ready(function(){
        $('.select-experiment').click(function(){
            var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
			      $('#harvested-plots-browser').html(loading); 
            $('.search-entry-field-div').hide(); 

            var experimentId = $(this).attr('data-experiment-id');
            var currentExperimentId = '<?php echo $currentExperimentId; ?>';
            var url = '<?php echo $harvestedPlotsUrl; ?>';
            
            $('#nurseries-browser').hide();
            $('#harvested-plots-browser').show();      

            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    experimentId: experimentId,
                    currentExperimentId: currentExperimentId
                },
                success: function(response) {
                    $('#harvested-plots-browser').html(response);
                },
                error: function() {

                }
            });
        });
    });
</script>



