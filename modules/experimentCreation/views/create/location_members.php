<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders view for harvested plots for entry list selection
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Place;

$checked='';
if($woLocation > 0){
    $checked = 'checked';
}
?>

<div class="col col-md-12">
    <div class="col col-md-6  view-entity-content">
        <h4>
            <?php
            echo $savedList->name;
            ?>
        </h4>
        <div class="switch">
            <label>
            <span title="This will overwrite the existing values">Populate from start</span>
            <input <?=$checked?> type="checkbox" name="populate-flag">
            <span class="lever"></span>
            <span title="This will populate the empty values">Populate empty locations</span>
            </label>
         </div>
    </div>
    <div class="col col-md-6  view-entity-content">

        <?php if($dataProvider->getTotalCount() > 0){ ?>

            <button id="add-locations-btn" class="btn btn-primary waves-effect waves-light pull-right" title="Add to location reps" style="margin-right:20px;max-width: 105px;">
                <?= \Yii::t('app', 'Add') ?>
                <i class="material-icons right">add</i>
            </button>

        <?php
            }
        ?>
        <button id="back-btn" class="btn btn-primary waves-effect waves-light grey pull-right" title="Go to list" style="margin-right:5px;max-width: 105px;">
            <?= \Yii::t('app', 'Back') ?>
            <i class="material-icons right">keyboard_backspace</i>
        </button>

    </div>
    
</div>
<div class="row">

    <?php
    echo $grid = GridView::widget([
        'pjax' => true, // pjax is set to always true for this demo
        'dataProvider' => $dataProvider,
        'options'=>['id' => 'location-selection-browser'],
        'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
        'toggleData'=>true,
        'columns'=>[
            [
                'class' => 'yii\grid\CheckboxColumn', 
                'name' => 'location_list_selection[]',
                'checkboxOptions' => function ($data, $key, $index, $column) {
                    return ['value' => $data['data_id'], 'class'=>'add-to-list-class'];
                },
                'header' => Html::checkBox('location_members_all', true, [
                    'class' => 'select-on-check-all'
                ]),
            ],
            ['class' => 'yii\grid\SerialColumn'], 
            [
                'label' => Yii::t('app', 'Entity'),
                'value' => function($data){
                    return Place::findOne($data['data_id'])->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute'=>'order_number',
                'label' => Yii::t('app', 'Order'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'remarks',
                'label' => Yii::t('app', 'Remarks'),
                'format' => 'raw',
                'enableSorting' => false,
            ]
        ]
    ]);

    ?>
</div>

<?php
    $addLocationUrl = Url::to(['create/save-location-to-reps','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>

<script>
    $(document).ready(function(){
        $("[name='location_list_selection[]']").trigger('click');
        $('#add-locations-btn').click(function(){

            var selectedArray = [];
            var populateRecord = '';

            if($('[name="populate-flag"]').prop('checked') == true){
                populateRecord = 'populate-empty';
            } else{
                populateRecord = 'populate-start';
            }
            
            $('[name="location_list_selection[]"]:checked').each (function (i, e){
                selectedArray.push ($(this).val());
            });
            var url = '<?php echo $addLocationUrl; ?>';
            
            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    selectedArray: selectedArray,
                    populateRecord: populateRecord
                },
                success: function(response) {

                },
                error: function() {

                }
            });

        });

        $('.add-to-list-class').on('click', function() {
            var selectedArray = [];
            $('[name="location_list_selection[]"]:checked').each (function (i, e){
                selectedArray.push ($(this).val());
            });
            
            if(selectedArray.length > 0){
                $('#add-locations-btn').removeClass('disabled');
            } else{
                $('#add-locations-btn').addClass('disabled');
            }
        });

        $('[name="location_members_all"]').on('click', function() {
            $("[name='location_list_selection[]']").prop('checked', $(this).prop('checked')).trigger('change');

            var selectedArray = [];
            $('[name="location_list_selection[]"]:checked').each (function (i, e){
                selectedArray.push ($(this).val());
            });
            if(selectedArray.length > 0){
                $('#add-locations-btn').removeClass('disabled');
            } else{
                $('#add-locations-btn').addClass('disabled');
            }
        });
       
    });


</script>