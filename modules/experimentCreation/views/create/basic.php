<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders browser for experiment Creation
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use kartik\builder\Form;
use app\models\Program;
use yii\helpers\Url;
use app\components\GenericFormWidget;

    $disabled = '';
    $experimentStatus = isset($model['experimentStatus']) ? $model['experimentStatus'] : '';
    $experimentDbId = isset($model['experimentDbId']) ? $model['experimentDbId'] : '';
    if (strpos($experimentStatus,'created') === false) {
        $disabled = 'disabled';
    }
    echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
        'processId' => $processId,
        'model'=>$model,
        'program'=>$program,
        'saveBtnLabel'=>'Save',
        'nextBtn' => '',
        'btnName' => 'next-btn',
        'type' => $experimentType,
        'disabled' => $disabled
    ]);
    $form = ActiveForm::begin([
        'enableClientValidation'=>false,
        'id'=>'create-basic-experiment-form',
        'action'=>['/experimentCreation/create/specify-basic-info','id'=>$experimentDbId,'program'=>$program, 'processId'=>$processId],
    ]);
?>
        <?php
            echo GenericFormWidget::widget([
                'options' => $widgetOptions
            ]);
        ?>
     <?php ActiveForm::end(); ?>
</div>

<?php

    Modal::begin([
        'id' => 'generic-modal',
    ]);
    Modal::end();

    // Modal for validation
    Modal::begin([
        'id' => 'required-field-modal',
        'header' => '<h4><i class="fa fa-bell"></i>  '.\Yii::t('app','Notification').'</h4>',
        'footer' => Html::a(\Yii::t('app','OK'),['#'],['id'=>'cancel-save-btn','class'=>'btn btn-primary waves-effect waves-light','data-dismiss'=>'modal']),
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static']
    ]);
    echo '<div class="row">
        <p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'Please correct the errors below before proceeding.').'</span></p>
        </div><div class="row" id="required-field-notif"></div></div>';
    Modal::end();

    //Modal for experiment name updates
    Modal::begin([
        'id' => 'experiment-name-modal',
        'header' => '<h4><i class="fa fa-bell"></i>  '.\Yii::t('app','Notification').'</h4>',
        'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'name-cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
        Html::a(\Yii::t('app','Confirm'),'#',['id'=>'update-experimentName-confirm','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static']
    ]);
    echo '<div class="row" id="experiment-name-notif"></div>';
    Modal::end();
?>

<?php

$addVariableUrl = Url::to(['/experimentCreation/create/addvariable']);
$tempId = isset($_GET['id']) ? $_GET['id'] : '';
$tempNextUrl = Url::to(['create/specify-entry-list', 'id'=>$tempId, 'program'=>$program, 'processId'=>$processId]);
$genericRenderConfigUrl = Url::to(['create/render-config-form', 'id'=>$tempId, 'program'=>$program]);
$checkExperimentNameUrl = Url::to(['default/check-experiment-name','id'=>$tempId]);
$updateExperimentNameUrl = Url::to(['default/update-experiment-name','id'=>$tempId]);

$this->registerJsFile("@web/js/generic-form-validation.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    var submitFlag = 0;
    var updateFlag = false;
    var message = '';
    var exptNameFieldId = '';
    var inputStage = '';
    var inputSeason = '';
    var inputYear = '';
    var applyCodePattern = false;
    var finalUpdateFlag = true;
    var duplicateFlag = false;

    $(document).ready()
    {
        $('.hidden').hide();

        $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 200, 
            format: 'yyyy//mm/dd'
        });

        $('.timepicker').pickatime({
            twelveHour: true,
        });
    }
     
    $(document).on('click', '#next-btn', function() {
        //Before submit remove disable
        $('.my-container').find('select').each(function(){
            var idLocal = this.id;
            var fieldId = idLocal.includes('EXPERIMENT_TYPE');
        
            if(fieldId == true){
                $('#'+idLocal).removeAttr('disabled');
            }
        });

        var form_data = $.param($.map($("#create-basic-experiment-form").serializeArray(), function(v,i) {
            return (v.name == "add_variable_info_select") ? null : v;
        }));
        
        //After submit, disable the fields again
        $('.my-container').find('select').each(function(){    
            var idLocal = this.id;
            var fieldId = idLocal.includes('EXPERIMENT_TYPE');
        
            if(fieldId == true){
                $('#'+idLocal).attr('disabled','disabled');
            }
        });

        $("#addtl-fields-area input").each(function(){
            var tId = this.name;
            form_data = form_data + '&' + $.param({
                [tId] : $(this).val()
            });
        });

        $("input[type='checkbox']" ).each(function(){
            if($("#"+this.id).hasClass("required-field")){
                if($(this).val() == '0' || $(this).val() == ''){
                    var tId = this.name;
                    $('#'+this.id).attr('value', '0');

                    form_data = form_data + '&' + $.param({
                        [tId] : $(this).val()
                    });
                }
            }
        });

        var nextUrl = $(this).attr('data-url');
        
        if(typeof nextUrl == 'undefined' && '$tempId' != null){
            nextUrl = '$tempNextUrl';
        }
        form_data = form_data + '&' + $.param({
                        ['nextUrl'] : nextUrl
                    });
        form_data =  form_data + '&' + $.param({
                        ['processId'] : '$processId'
                    });
        
        //function that checks required input fields are filled in
        var response = checkFields2();
        
        if(response['success']){
            var action_url = $("#create-basic-experiment-form").attr("action");
            $(".control-group").removeClass("has-error"); //remove error class
            $(".help-block").html(""); //remove existing error messages
         
            if(submitFlag == 0){
                submitFlag = 1;
                //Check the experiment name pattern change
                checkExperimentName();
                
                if(updateFlag == true || duplicateFlag == true){
                    
                    $('#experiment-name-modal').modal('show');
                    
                    if(duplicateFlag == true){
                        var duplicateNotif = " The experiment name specified is not unique. Click <b>Confirm</b> to proceed.";
                        if(updateFlag == true){
                            duplicateNotif = " The experiment name specified is not unique. This value will be override when code pattern is applied.";    
                        }

                        $('#experiment-name-notif').html('<div class="alert ">'+ 
                            '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                '<i class="fa fa-info-circle"></i>'+
                                    duplicateNotif+ 
                            '</div></div>');
                    }
                    if(updateFlag == true){
                        $('#experiment-name-notif').append('<div class="alert ">'+ 
                            '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                '<i class="fa fa-info-circle"></i>'+
                                    ' A change of values in <b>' + message + '</b> is detected.'+ 
                            '</div></div><div class="row"><div class="switch pull-left">'+
                            '<span class="new badge"><b>Apply Experiment Name code pattern?</b></span> &nbsp;'+
                            '<label>'+
                                'Yes'+
                                '<input type="checkbox" id="codePattern-type-switch-id"><span class="lever"></span>'+
                                'No'+
                            '</label>'+
                        '</div></div>');
                        
                    }

                }else{
                    $.ajax({
                        type:"POST",
                        dataType: 'json',
                        url: action_url,
                        data: form_data,
                        success: function(data) {
                            
                        },
                        error: function(){

                        }
                    });
                }
            }
        } 
    });
   

    $("#cancel-save-btn").on("click",function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        $("#required-field-notif").html('');
    });

    $("#name-cancel-save-btn").on("click",function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        $("#required-field-notif").html('');
        $('#experiment-name-modal').modal('hide');
        submitFlag = 0;
    });

    $("input[type='checkbox']" ).on( "click", function(){
        if ($(this).prop('checked')){
            $(this).attr('value', '1');
            if((this.id).includes("HAS_EXPERIMENT_GROUP")){
                $('#experiment_group_tab_id').removeClass('hidden');
            }
        } else {
            $(this).attr('value', '0');
            if((this.id).includes("HAS_EXPERIMENT_GROUP")){
                $('#experiment_group_tab_id').addClass('hidden');
            }
        }
    });

    $("select" ).on( "change", function(){
        if((this.id).includes("EXPERIMENT_TYPE")){
            
        } 
    });

    $(".default-namegen-switch").on('change', function () {
        var fid = this.id;
        var fieldId = fid.split('generate-');
        if($(this).prop('checked') == false){
            $('#'+fieldId[1]).removeClass('disabled');
            $('#'+fieldId[1]).removeClass('generated-pattern');
            $('#'+fieldId[1]).removeAttr('readonly');
        } else {
            $('#'+fieldId[1]).addClass('disabled');
            $('#'+fieldId[1]).addClass('generated-pattern');
            $('#'+fieldId[1]).attr('readonly','readonly');
            $('#'+fieldId[1]).val(''); 
        }
    });

    $('#update-experimentName-confirm').on('click', function (){
        var codePatternStatus = $('#codePattern-type-switch-id').prop('checked');
        
        if(codePatternStatus || codePatternStatus == undefined){
            applyCodePattern = false;
        }else{
            applyCodePattern = true;
        }
        
        if(applyCodePattern == true){
             $.ajax({
                url: '$updateExperimentNameUrl',
                type: 'post',
                dataType: 'json',
                async: false,
                cache: false,
                data: {
                    stage: inputStage,
                    year: inputYear,
                    season: inputSeason
                },
                success: function(response) {
                    $('#update-experimentName-confirm').addClass('disabled')
                    location.reload();
                }
            });
        }else{
            submitFlag = 0;
            updateFlag = false;
            finalUpdateFlag = false;
            duplicateFlag = false;
            $('#next-btn').trigger('click');
            $('#experiment-name-modal').modal('hide');
        }
    });


    //Check for updates
    function checkExperimentName(){
    
        $('.required-field').each(function(){
            var fieldId = $(this).attr('id');
            
            if(fieldId.includes('EXPERIMENT_NAME')){
                exptNameFieldId = fieldId;
            }

            if(fieldId.includes('STAGE')){
                inputStage = $('#'+fieldId).val();
                var tempStr = inputStage.split('-');
                inputStage = tempStr[1];
            }
            if(fieldId.includes('EXPERIMENT_YEAR')){
                inputYear = $('#'+fieldId).val();
            }
            if(fieldId.includes('SEASON')){
                inputSeason = $('#'+fieldId).val();
            }
            if(fieldId.includes('EXPERIMENT_NAME')){
               inputExperimentName =  $('#'+fieldId).val();
            }
        });
       
        $.ajax({
            url: '$checkExperimentNameUrl',
            type: 'post',
            dataType: 'json',
            data: {
                stage: inputStage,
                year: inputYear,
                season: inputSeason,
                inputExperimentName: inputExperimentName
            },
            async: false,
            cache: false,
            success: function(response) {
                if(finalUpdateFlag == true){
                    updateFlag = response['updateFlag'];
                    duplicateFlag = response['duplicateFlag']
                }
                message = response['message'];
            }
        });

    }

JS
);

?>
<style>
.required{
    color:red;
}
label{
    font-size: .8rem;
    color: #333333;
  }
.readonly{
    cursor: not-allowed;
    user-select: none;
    -webkit-user-modify: read-only;
    pointer-events: none;
    border: 1px solid grey;
    background-color: lightgrey;
}
.div-class{
    margin-bottom: 10px;
}
.my-container{
    min-width: 50%;
    max-width: 90%
}
.card{
    height: 100%;
    margin-left:-10px;
    margin-top:-10px;
    padding: 30px;
}
.form-inline input{
    flex-direction: column;
    align-items: stretch;
    vertical-align: middle;
}
.form-inline { 
    display: flex;
    flex-flow: row wrap;    
    align-items: center;
}
.view-entity-content{
    background: #ffffff;
}
#basic-info1{
    margin-left:auto;
    margin-right:auto;
    margin-top:15px;
    display: inline-block;
}
#basic-info2{
    margin-left:auto;
    margin-right:auto;
    margin-top:20px;
    display: inline-block;
}
.parent-box{
    position: relative;
    margin-top: 1rem;
}
.main-panel{
    margin-left: 5%;
}
.generated-pattern{
    cursor:not-allowed;
    user-select: none;
    -webkit-user-modify: read-only;
    pointer-events: none;
    border: 1px solid grey;
    background-color: lightgrey;
}
</style>