<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders view for trial studies for entry list selection
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Season;
use app\models\Phase;

?>

<p><?= \Yii::t('app', 'Below are the list of trial studies you can get your entries from. Note that only trials which have already been harvested will appear in this browser.') ?></p>

<div class="input-field col s6">
  <span class="new badge" style="font-weight:500; height:35px;line-height:35px;margin-right:15px;">Search Filter</span>
  <input id="searchTextTrial" placeholder="Type to filter" type="text" class="search-text-entry" style="width:50%">
</div>

<div id="trials-browser">
<?php
echo $grid = GridView::widget([
    'pjax' => true, // pjax is set to always true for this demo
    'dataProvider' => $dataProvider,
    'id' => 'copy-experiment',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-entry-table'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'], 
        [
            'class'=>'kartik\grid\ActionColumn',
            'header' => 'Actions',
            'template' => '{select}',
            'buttons' => [
                'select' => function ($url, $model, $key) {
                    
                    return Html::a('<i class="material-icons">folder_open</i>',
                        '#',
                        [
                            'class'=>'select-study',
                            'title' =>  Yii::t('app', 'View lines coming from this study'),
                            'data-study-id' => $model['id'],
                        ]
                    );
                },
            ]
        ],
        [
            'attribute'=>'name',
            'label' => Yii::t('app', 'Name'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'study',
            'label' => Yii::t('app', 'Study'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'year',
            'label' => Yii::t('app', 'Year'),
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'season',
            'label' => Yii::t('app', 'Season'),
            'format' => 'raw',
            'enableSorting' => false,
            'value' => function($model){
                $season = Season::findOne($model['season_id']);
                return $season->abbrev;
            }
        ],
        [
            'attribute'=>'phase',
            'label' => Yii::t('app', 'Phase'),
            'format' => 'raw',
            'enableSorting' => false,
            'value' => function($model){
                $phase = Phase::findOne($model['phase_id']);
                return $phase->abbrev;
            }
        ],
        [
            'attribute'=>'no_of_products',
            'label' => Yii::t('app', 'No. of Products'),
            'format' => 'raw',
            'enableSorting' => false,
        ]
        
    ],
    'toggleData'=>true,
]);

?>
</div>

<div id="harvested-plots-trials-browser"></div>

<?php
    $harvestedPlotsUrl = Url::to(['create/render-harvested-plots','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>

<script>
    $(document).on('click', '#back-btn', function(){ 
        $('#trials-browser').show();
        $('#harvested-plots-trials-browser').hide();      
    });

    $(document).ready(function(){        
        $('.select-study').click(function(){
            var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
			$('#harvested-plots-trials-browser').html(loading); 

            var studyId = $(this).attr('data-study-id');
            var currentExperimentId = '<?php echo $currentExperimentId; ?>';
            var url = '<?php echo $harvestedPlotsUrl; ?>';
            
            $('#trials-browser').hide();
            $('#harvested-plots-trials-browser').show();      

            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    studyId: studyId,
                    currentExperimentId: currentExperimentId
                },
                success: function(response) {
                    $('#harvested-plots-trials-browser').html(response);
                },
                error: function() {

                }
            });
        });
    });
</script>

