<?php

/*
* This file is part of Breeding4Rice.
*
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* Preview list crosses in crossing matrix
**/

use kartik\grid\GridView;
use yii\helpers\Html;

$columns = [
	['class' => 'yii\grid\SerialColumn'],
    [
    	'attribute'=>'cross_name',
    	'enableSorting' => false,
        'format'=>'raw',
        'value'=> function($data){
            return !empty($data['cross_name']) ? $data['cross_name'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute'=>'female_entno',
        'label' => 'Female entry no.',
        'enableSorting' => false,
        'format'=>'raw',
        'value'=> function($data){
            return !empty($data['female_entno']) ? $data['female_entno'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute'=>'male_entno',
        'label' => 'Male entry no.',
        'enableSorting' => false,
        'format'=>'raw',
        'value'=> function($data){
            return !empty($data['male_entno']) ? $data['male_entno'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute'=>'female_generation',
        'enableSorting' => false,
        'format'=>'raw',
        'value'=> function($data){
            return !empty($data['female_generation']) ? $data['female_generation'] : '<span class="not-set">(not set)</span>';
        }
    ],
    [
        'attribute'=>'male_generation',
        'enableSorting' => false,
        'format'=>'raw',
        'value'=> function($data){
            return !empty($data['male_generation']) ? $data['male_generation'] : '<span class="not-set">(not set)</span>';
        }
    ]
];

echo \Yii::t('app','You are about to update crossing list with below crosses. Click "Confirm" to proceed.');
echo $grid = GridView::widget([
	'pjax' => true,
	'dataProvider' => $data,
	'id' => 'preview-crossing-matrix-grid',
	'columns' => $columns,
	'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
]);

?>

<style type="text/css">
.table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #fff;
}
.summary{
    text-align: right;
}
</style>
