<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders browser for saved list for entry list selection
 */

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<p><?= \Yii::t('app', 'Below are the saved list that you can use in populating your location reps.') ?>
<a class="waves-effect waves-light btn tooltipped pull-right" style="margin-right:13px;" id="create-location-btn" title="<?= \Yii::t('app','Create location list') ?>"><i class="material-icons pull-right">add</i>&nbsp;<span><?= \Yii::t('app','Create') ?></span></a>
</p>
<div id = "location_browser">

<?php $actionColumns = [
            ['class' => 'yii\grid\SerialColumn'], 
            [
                'class'=>'kartik\grid\ActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'template' => '{select}{add}',
                'options'=>['class'=>'col-md-1'],
                'buttons' => [
                    'select' => function ($url, $model, $key) {
                        return Html::a('<i class="material-icons">exit_to_app</i>',
                            '#',
                            [
                                'class'=>'select-list',
                                'title' => Yii::t('app', 'Select saved list'),
                                'data-saved_list_id' => $model->id,
                                'data-type' => 'selection'
                            ]
                        );
                    },
                    'add' => function ($url, $model, $key) {
                        return Html::a('<i class="material-icons">add</i>',
                            '#',
                            [
                                'class'=>'select-list',
                                'title' => Yii::t('app', 'Add to location list'),
                                'data-saved_list_id' => $model->id,
                                'data-type' => 'addition'
                            ]
                        );
                    },
                ]
            ],
        ];
        // all the columns that can be viewed in seasons browser
        $columns = [
            [
                'attribute'=>'abbrev',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'name',
                'enableSorting' => false,
            ],
            [
                'header' => 'No. of Members',
                'value' => function ($data) {
                    return '<span class="new badge">'.Yii::$app->db->createCommand('select count(1) from platform.list_member where list_id = '.$data->id.' and is_void=false;')->queryScalar().'</span>';
				},
                'format' => 'raw', 
                'enableSorting' => false,
			],
			[
                'attribute'=>'remarks',
                'enableSorting' => false,
            ],
        ];  

        $gridColumns = array_merge($actionColumns,$columns);
		echo $grid = GridView::widget([
			'pjax' => true, // pjax is set to always true for this demo
			'dataProvider' => $data,
			'id' => 'saved-list-table',
			'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
            'columns' => $gridColumns,
            'toggleData'=>true,
		]);
 ?>
 </div>

<div id="location_list_members">
</div>

<?php
    $listMembersUrl = Url::to(['create/render-location-list-members','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
    $createListUrl = Url::to(['create/create-location-list','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
?>

<script>

    $(document).on('click', '#back-btn', function(){ 
        $('#location_browser').show();
        $('#create-location-btn').show();
        $('#location_list_members').hide();      
    });

    
    $(document).ready(function(){
        
        $('.select-list').click(function(){

            var savedListId = $(this).attr('data-saved_list_id');
            var type = $(this).attr('data-type');
            var currentExperimentId = '<?php echo $id; ?>';
            var url = '<?php echo $listMembersUrl; ?>';
            
            $('#location_browser').hide();
            $('#create-location-btn').hide();
            $('#location_list_members').show();

            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    savedListId: savedListId,
                    currentExperimentId: currentExperimentId,
                    type: type
                },
                success: function(response) {
                    $('#location_list_members').html(response);
                },
                error: function() {

                }
            });
        });

        $('#create-location-btn').click(function(){

            var url = '<?php echo $createListUrl; ?>';

            $('#location_browser').hide();
            $('#create-location-btn').hide();
            $('#location_list_members').show();

            //get the entries of this experiment
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                },
                success: function(response) {
                    $('#location_list_members').html(response);
                },
                error: function() {

                }
            });

        });

    });

</script>



                       
<style>
[type="checkbox"]:not(:checked), [type="checkbox"]:checked {
    position: inherit;
    opacity: 1;
    pointer-events: auto;
}
</style>