<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders manage cross list webform for the experiment creation
*/
?>
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/modules/heatmap.js"></script>

<?php

/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
* Renders manage cross list webform for the experiment creation
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\grid\GridView;

use kartik\dynagrid\DynaGrid;
use marekpetras\yii2ajaxboxwidget\Box;
use kartik\tabs\TabsX;

$disabled = '';

if (strpos($model->experiment_status,'cross list created') === false) {
    $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model'=>$model,
    'program'=>$program,
    'saveBtnLabel'=>'Next',
    'nextBtn' => $disabled,
    'btnName' => 'next-btn',
]);

$items = [
    [
        'label'=>Yii::t('app','<i class="glyphicon glyphicon-edit"></i> Input'),
        'url'=>Url::to(['manage-crosses', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]),
    ],
    [
        'label'=>Yii::t('app','<i class="glyphicon glyphicon-th"></i> Matrix'),
        'active'=>true,
        'url'=>Url::to(['manage-crossing-matrix', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]),
        'content' => Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/_crossing_matrix.php',[
            'id'=>$id,
            'program' => $program,
            'processId'=> $processId
        ]),
    ],
    [
        'label'=>Yii::t('app','<i class="glyphicon glyphicon-pencil"></i> Manage'),
        'url'=>Url::to(['manage-crosslist', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]),
    ]
];

 echo TabsX::widget([
        'items'=>$items,
        'position'=>TabsX::POS_LEFT,
        'encodeLabels'=>false,
        'pluginOptions'=>[
            'enableCache'=>false
        ],
    ]);

// Modal for validation
Modal::begin([
    'id' => 'required-field-modal',
    'header' => '<h4><i class="fa fa-bell"></i> '.\Yii::t('app','Notification').'</h4>',
    'footer' => Html::a(\Yii::t('app','OK'),['#'],['id'=>'cancel-save-btn','class'=>'btn btn-primary waves-effect waves-light','data-dismiss'=>'modal']),
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static']
]);
echo '<div class="row">
    <p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'Please correct the errors below before proceeding.').'</span></p>
    </div><div class="row" id="required-field-notif"></div>';
Modal::end();

//Modal for unused parents
Modal::begin([
    'id' => 'unused-parents-modal',
    'header' => "<h4><i class='material-icons orange-text left'>warning</i> Unused Parents</h4>",					
    'footer' =>	Html::a('<i class="material-icons left">arrow_back</i>'.\Yii::t('app',' Back '),'#',['id' => 'back-nxt-btn','data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close black-text grey lighten-2']).'&nbsp;'.Html::a('<i class="material-icons right">arrow_forward</i>'.\Yii::t('app','Proceed'),'#',['id' => 'proceed-nxt-btn', 'class'=>'btn btn-primary waves-effect waves-light modal-close teal']).'&emsp;',
    'size' =>'modal-md',
    'options' => ['data-backdrop'=>'static']
    ]);
    echo \Yii::t('app',' Some of the parents are not used in crossing. If you wish to proceed, you will be redirected to the next step.');
Modal::end();

$checkParentsUrl = Url::to(['create/check-parents','id'=>$id, 'program'=>$program]);
$checkCrossesUrl = Url::to(['create/check-crosses','id'=>$id, 'program'=>$program]);

$this->registerJs(<<<JS
    $('#next-btn').click(function(e){
        //Add checking for required fields
        e.preventDefault();
            e.stopImmediatePropagation();
            var response = checkFields();

            requiredFieldExists = response.requiredFieldExists;
            response = response.respo;
            
            if(response['success']){
                var nextUrl = $('#next-btn').attr('data-url');
                $.ajax({
                        url: '$checkCrossesUrl',
                        success: function(withCross){
                            if(requiredFieldExists == false && !withCross){
                                $('#required-field-notif').html('');
                                $('#required-field-modal').modal();
                                $("#required-field-notif").append('<p><b>1. No cross list results.</b></p>');
                                $('#required-field-modal').modal('open');
                            }
                            else{
                                $.ajax({
                                    url: '$checkParentsUrl',
                                    success: function(unusedParentCount){
                                        if(unusedParentCount > 0){
                                            $('#unused-parents-modal').modal();                      
                                        }else{
                                            window.location = nextUrl;
                                        }
                                    }
                                });
                            }
                        }
                    });
            } else {
                $('#required-field-notif').html('');
                $('#required-field-modal').modal();

                if(response['required'] != null && response['required'] != undefined && response['required'] != ''){
                    $("#required-field-notif").append('<p> <b>Required Fields</b> <br>'+response['required']+'</p>');
                }
                if(response['datatype'] != null && response['datatype'] != undefined && response['datatype'] != ''){
                    $("#required-field-notif").append('<p> <b>Invalid Input</b> <br>'+response['datatype']+'</p>');
                }

                $('#required-field-modal').modal('open');
            }
    });

function checkFields(){
    var response = [];
    var respoArr = [];
    response['success'] = true;
    response['required'] = null;
    response['datatype'] = null;
    var tempArr = [];
    var arrLabel = [];
    var count = 0;
    var requiredFieldExists = false;

    $(".required-field").each(function(){

        var test1 = $(this).val();

        requiredFieldExists = true;

        if($(this).val() == '' || $(this).val() == null || $(this).val() == ""){

            response['success'] = false;
            var idCol = this.id;
            var label =  $('#'+idCol).attr("data-label");
            var count = tempArr.length;

            if(arrLabel.indexOf(label) > -1){

            }else{
                tempArr.push((count+1)+'. '+label);
                arrLabel.push(label);
            }

        }
    });
    response['required'] = tempArr.join("<br>");

    tempArr = [];
    count = 0;
    arrLabel = [];

    $(".invalid").each(function(){
        response['success'] = false;
        var idCol = this.id;
        var label =  $('#'+idCol).attr("data-label");
        count = tempArr.length;


        if(arrLabel.indexOf(label) > -1){

        }else{
            tempArr.push((count+1)+'. '+label);
            arrLabel.push(label);
        }

    });

    response['datatype'] = tempArr.join("<br>");
    respoArr['respo'] = response;
    respoArr['requiredFieldExists'] = requiredFieldExists;

    return respoArr;
}


// yes, proceed to next tab 
$(document).on('click','#proceed-nxt-btn',function(e){
    
    window.location = $('#next-btn').attr('data-url');

});

JS
);
?>

<style>
.box {
    position: relative;
    background: #ffffff;
    -webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    border-top:none;
    margin: .5rem 0 1rem 0;
    border-radius: 2px;
    width: 100%;
}
.box-header.with-border {
    border-bottom: none;
}
.box-header > .box-tools {
    top: 10px;
}

.box-body {
    padding: 9px 15px 10px 15px;
    overflow-y: hidden;
    overflow-x: hidden;
}
.box-header {
    padding-left: 15px;
    margin-bottom: 20px;
}
.parent-box{
    margin: 0px;
    padding: 0px !important;
}
.display-none{
    display: none;
}
.box-header.with-border {
    margin-bottom: -20px;
}
.box-reload-btn{
    margin: 0 12px 0 -12px;
    padding: 0px 12px;
}
.input-field .prefix ~ input, .input-field .prefix ~ textarea, .input-field .prefix ~ label, .input-field .prefix ~ .validate ~ label, .input-field .prefix ~ .autocomplete-content {
    margin-left: 1.5rem !important;
    width: 92%;
    width: calc(100% - 3rem);
    font-size: 70%;
}
.smaller-input-field{
    height: 2rem !important;
    font-size: 60%;
}
.input-field .prefix {
    font-size: 90%;
}
.no-margin-top {
    margin-top: 0;
}
.box-title{
    width:50%;
}
.box-header .box-title {
    display: inline-flex;
}
xx{
    background-color: #ff0;
    color:inherit;
    padding: 0.1em 0.2em;
}
</style>

