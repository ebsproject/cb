<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders modal content for viewing experiment
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
?>
<div class="col col-sm-12 view-entity-content">
    
    <!-- Browse Entry List -->
    <?php
        
        $columns = [
            ['class' => 'yii\grid\SerialColumn'], 
            [
                'attribute'=>'gid',
                'label' => Yii::t('app', 'Gid'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'product_name',
                'label' => Yii::t('app', 'Germplasm Name'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'entlistno',
                'label' => Yii::t('app', 'Entry no.'),
                'format' => 'raw',
                'enableSorting' => false,
                
            ],

        ];  

        $columns = array_merge($columns, $entryRequiredCols);


        echo Yii::t('app', '<p>This is the browser for the entry list of the experiment.</p>');
        echo $grid = GridView::widget([
			'pjax' => true, // pjax is set to always true for this demo
			'dataProvider' => $entryDataProvider,
			'id' => 'view-entlist-table',
			'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
            'columns' => $columns,
            'toggleData'=>true,
		]);

        //Studies
        // all the columns that can be viewed in study browser
        $studyColumns = [
            [
                'attribute'=>'title',
                'visible' => true,
                'enableSorting' => false,
            ],
            [
                'attribute'=>'name',
                'visible' => true,
                'enableSorting' => false,
            ],
            [
                'attribute'=>'place',
                'visible' => true,
                'enableSorting' => false,
            ],
            [
                'attribute'=>'phase',
                'visible' => true,
                'enableSorting' => false,
            ],
            [
                'attribute'=>'year',
                'visible' => true,
                'enableSorting' => false,
            ],
            [
                'attribute'=>'season',
                'visible' => true,
                'enableSorting' => false,
            ],
            [
                'attribute'=>'study',
                'visible' => true,
                'filter' => true,
                'enableSorting' => false,
            ],
            [
                'attribute'=>'entry_count',
                'visible' => true,
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'plot_count',
                'visible' => true,
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'remarks',
                'visible' => false,
                'filter' => true,
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'creator',
                'header' => 'Creator',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'modifier',
                'header' => 'Modifier',
                'visible' => false,
                'enableSorting' => false,
            ],
            [
                'attribute'=>'creation_timestamp',
                'format' => 'date',
                'visible' => true,
                'enableSorting' => false,
            ],
            [
                'attribute'=>'modification_timestamp',
                'format' => 'date',
                'visible' => false,
                'format' => 'date',
                'enableSorting' => false,
            ],
            
        ];  
        if($model->experiment_status == 'finalized'){
            echo Yii::t('app', '<p>This is the browser for the created location reps (studies).</p>');
        } else{
            echo Yii::t('app', '<p><span class="new badge">NOTE</span> &nbsp;This is the browser for the created location reps (studies). These are <b>not yet visible</b> in the Drafts browser.  <b>FINALIZE</b> the experiment to make the studies active.</p>');
        }
        
        echo $grid = GridView::widget([
			'pjax' => true, // pjax is set to always true for this demo
			'dataProvider' => $studyDataProvider,
			'id' => 'view-study-table',
			'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
            'columns' => $studyColumns,
            'toggleData'=>true,
		]);
    ?>    
</div>
