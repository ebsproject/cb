<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\Button;
use yii\bootstrap\Collapse;
use yii\bootstrap\ActiveForm;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\widgets\Select2;
use kartik\dropdown\DropdownX;
use yii\bootstrap\Nav;
use kartik\tabs\TabsX;
use app\models\Variable;
use kartik\sortable\Sortable;
use yii\web\JsExpression;
use app\components\ManageSortsWidget;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

/**
* Renders entry list tab for the experiment creation
*/
$resetGridUrl = Url::toRoute(['create/specify-entry-list']);

$disabled = '';

if(!isset($model) || empty($model)){
    Yii::$app->session->setFlash('error', \Yii::t('app', 'Unable to retrieve the entry list item records. Please try again or file a report for assistance.'));
}

$experimentStatus = !empty($model) ? $model['experimentStatus'] : '';
$experimentName = !empty($model) ? $model['experimentName'] : '';
if (strpos($experimentStatus,'entry list created') === false) {
    $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model'=>$model,
    'program'=>$program,
    'saveBtnLabel'=>'Next',
    'nextBtn' => $disabled,
    'btnName' => 'next-btn',

]);

$selectedItems = isset($selectedItems) ? $selectedItems : [];
$selectedItemsCount = count($selectedItems) > 0 ? count($selectedItems) : 'No';
$dataProcessAbbrev = isset($model['dataProcessAbbrev']) ? $model['dataProcessAbbrev'] : '';
$isCrossList = ($dataProcessAbbrev == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS');
?>

<div class="col col-sm-12 view-entity-content">
    
    <!-- Browse Entry List -->
    <?php
        $browserId = 'dynagrid-ec-entry-list';

        // action columns
        $actionColumns = [
            [//checkbox
                'label'=> "Checkbox",
                'header'=>'
                    <input type="checkbox" class="filled-in" id="entry-select-all" />
                    <label for="entry-select-all"></label>
                ',
                'content'=>function($data) use ($selectedItems){
                    $entryDbId = $data['entryDbId'];

                    if(in_array($entryDbId, $selectedItems)){
                        $checkbox = '<input class="entry-grid-select filled-in" type="checkbox" checked="checked" id="'.$data['entryDbId'].'" />';
                    }
                    else{
                        $checkbox = '<input class="entry-grid-select filled-in" type="checkbox" id="'.$data['entryDbId'].'" />';
                    }
                    return $checkbox.'
                        <label for="'.$data['entryDbId'].'"></label>       
                    ';
                },        
                'hAlign'=>'center',
                'vAlign'=>'top',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
                'order'=> DynaGrid::ORDER_FIX_LEFT,
            ],
        ];

        // Format columns from config
        foreach ($columns as $key => $value) {
            $columns[$key]['headerOptions'] = ['id' => $columns[$key]['attribute']];
            $columns[$key]['label'] = '<span title="'.$columns[$key]['description'].'">'.Yii::t('app', $columns[$key]['title']).'</span>';
            unset($columns[$key]['title']);
            unset($columns[$key]['description']);
            unset($columns[$key]['abbrev']);    // TO DO: add checking if title and description is empty then use the abbrev to get the title and description
            $columns[$key]['format'] = 'raw';
            $columns[$key]['encodeLabel'] = false;
            $columns[$key]['visible'] = true;
            if(in_array($columns[$key]['attribute'], ['designation', 'parentage'])) {
                $columns[$key]['contentOptions'] = [
                    'class' => 'germplasm-name-col'
                ];
            }
        }

        $columns = \Yii::$container->get('app\modules\experimentCreation\models\ExperimentModel')->buildColumns($id, $requiredFields, $columns);
        $hidden = $isCrossList ? 'hidden' : '';
        //For gridview toolbar options, constant
        $actionsDropdown = "<a id='manage-entries-action' class='dropdown-button btn dropdown-button-pjax' href='#' data-activates='manage-entries-dropdown-actions' data-beloworigin='true' data-constrainwidth='false'><i class='glyphicon glyphicon-cog'></i> <span class='caret'></span></a>".
              "<ul id='manage-entries-dropdown-actions' class='dropdown-content' style='width:160px !important' data-beloworigin='false'>".
                  "<li><a href='#' class='dropdown-button dropdown-button-pjax' data-activates='remove-entries-dropdown-actions' data-hover='hover' data-alignment'='left'>Remove Entries</a></li>".
                  "<li class = '$hidden' ><a href='#' class='dropdown-button dropdown-button-pjax' data-activates='bulk-seeds-dropdown-actions' data-hover='hover' data-alignment'='left'>Select Packages</a></li>".
                  "<li class = '$hidden' ><a href='#' class='dropdown-button dropdown-button-pjax' data-activates='reorder-entries-dropdown-actions' data-hover='hover' data-alignment'='left'>Reorder Entries</a></li>".
              "</ul>".
              "<ul id='remove-entries-dropdown-actions' class='dropdown-content' style='width: 160px !important'>".
                "<li><a href='#!' id='selected' class='remove_entries'>Remove Selected</a></li>".
                "<li><a href='#!' id='current-page' class='remove_entries'>Remove Selected in Page</a></li>".
                "<li><a href='#!' id='all' class='remove_entries'>Remove All</a></li>".
              "</ul>".
              "<ul id='bulk-seeds-dropdown-actions' class='dropdown-content' style='width: 160px !important'>".
                  "<li><a href='#!' id='sel-seed-selected' class='select-lots'>Selected</a></li>".
                  "<li><a href='#!' id='sel-seed-all' class='select-lots'>All</a></li>".
              "</ul>".
              "<ul id='reorder-entries-dropdown-actions' class='dropdown-content' style='width: 160px !important'>".
                  "<li><a href='#!' id='selected' class='reorder_entries'>Reorder by Swapping</a></li>".
                  "<li><a href='#!' id='reorder-all' class='reorder_entries'>Reorder by Sort Settings</a></li>".
                  "<li><a href='#!' id='reorder-individual' class='reorder_entries'>Reorder Individually</a></li>".
                  "<li><a href='#!' id='reorder-by-group' class='reorder_entries'>Reorder by Group</a></li>".
              "</ul>";

        $availabilityCol = [
            [
                'attribute'=>'availability',
                'visible' => !$isCrossList,
                'vAlign' => 'middle',
                'label' => '<span title="Package availability information for entry">'.Yii::t('app', 'Availability').' <span class="required">*</span></span>',
                'format' => 'raw',
                'encodeLabel'=>false,
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>['selected'=>'Selected','unavailable'=>'Unavailable','select'=>'For Selection'], 
                'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true,'id'=>"select-level-data-browser-id"]],
                'filterInputOptions'=>['placeholder'=>'--Select--','id'=>"select-level-data-browser-id"],
                'value' => function($model) use ($program){
                    if(!empty($model['packageDbId'])) {
                        return  '<a class="waves-effect waves-light btn btn-flat green package-record-option" data-id="'.$model['entryDbId'].'" data-gid="'.$model['germplasmDbId'].'" title="'.\Yii::t('app', 'Package is already selected').'"><i class="material-icons left">check_circle</i>Selected</a>';
                    } else {

                        $filterPackages = [
                            "fields"=>"seed.id AS seedDbId|germplasm.id AS germplasmDbId|program.program_code AS programCode|package.id as \"packageDbId\"",
                            "germplasmDbId" => "equals ".$model['germplasmDbId'],
                            "programCode" => "equals ".$program,
                            "distinctOn"=>"germplasmDbId",
                            "addPackageCount"=>true
                        ];
                        $packagesArray = \Yii::$container->get('app\models\Seed')->searchPackageInfo($filterPackages, 'limit=2',false);
                        $packageInfo = $packagesArray['body']['result']['data'];
                        if(isset($packageInfo[0])){
                            if($packageInfo[0]['packageCount'] == 0) {
                                return  '<a class="waves-effect waves-light btn btn-flat red" data-id="'.$model['entryDbId'].'" data-gid="'.$model['germplasmDbId'].'" title="'.\Yii::t('app', 'No seed lots available').'"><i class="material-icons left">warning</i> &nbsp; Unavailable </a>';
                            } elseif($packageInfo[0]['packageCount'] > 1) {
                                return  '<a class="waves-effect waves-light btn package-record-option" data-id="'.$model['entryDbId'].'" data-gid="'.$model['germplasmDbId'].'" title="'.\Yii::t('app', 'Multiple packages available').'"><i class="material-icons left">edit</i>Select </a>';
                            } 
                            else {
                                return  '<a class="waves-effect waves-light btn package-record-option" data-id="'.$model['entryDbId'].'" data-gid="'.$model['germplasmDbId'].'" title="'.\Yii::t('app', 'Multiple packages available').'"><i class="material-icons left">edit</i>Select </a>';
                            }
                        } else {
                            return  '<a class="waves-effect waves-light btn btn-flat red" data-id="'.$model['entryDbId'].'" data-gid="'.$model['germplasmDbId'].'" title="'.\Yii::t('app', 'No seed lots available').'"><i class="material-icons left">warning</i> &nbsp; Unavailable </a>';
                        }
                    }
                },
                'order'=> DynaGrid::ORDER_FIX_RIGHT,
                'hiddenFromExport'=>true
            ]
        ];
        
        $experimentStatus = ucfirst($experimentStatus);
        $exportColumns = $columns;
        $columns = array_merge($columns, $availabilityCol);
        $gridColumns = array_merge($actionColumns, $columns);

        $leftColumns = $midColumns = $rightColumns = [];
        foreach($exportColumns as $val) {
            $temp = [
                'attribute' => $val['attribute'],
                'label' => $val['label'],
            ];

            $order = isset($val['order']) ?? null;
            switch($order) {
                case DynaGrid::ORDER_FIX_LEFT:
                    $leftColumns[] = $temp;
                    break;
                case DynaGrid::ORDER_FIX_RIGHT:
                    $rightColumns[] = $temp;
                    break;
                case DynaGrid::ORDER_MIDDLE:
                default:
                    $midColumns[] = $temp;
                    break;
            }
        }

        $exportColumns = array_merge($leftColumns, $midColumns, $rightColumns);

        $totalProviderCount = $dataProvider->getTotalCount();

        $timeStamp = date("Y-m-d_H-i-s");
        $experimentName = preg_replace("![^a-z0-9]+!i", "_", $experimentName);
        $exportFilename = $experimentName.' '.ucwords($displayName).' '.$timeStamp;

        // export configuration
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $exportColumns,
            'fontAwesome' => false,
            'filename' => $exportFilename,
            'container' => ['class' => 'btn-group'],
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_EXCEL_X => false,
            ],
            'showColumnSelector' => false
        ]);

        // filter column configuration
        foreach($gridColumns as $key => $val) {
            // exclude the checkbox, action, availability (if present) and hybrid role (for next time) columns
            if(isset($val['attribute']) && $val['attribute'] != 'availability' && $val['attribute'] != 'parent_type') {
                $gridColumns[$key]['filterType'] = GridView::FILTER_SELECT2;
                $gridColumns[$key]['filterWidgetOptions'] = [
                    'pluginOptions'=>[
                        'allowClear'=>true,
                        'ajax' => [
                            'url' => '/index.php/experimentCreation/create/get-filter-data?id='.$entryListId,
                            'dataType' => 'json',
                            'delay' => 150,
                            'type'=>'POST',
                            'data' => new JsExpression(
                                'function (params) {
                                    var value = (params.term === "")? undefined: params.term

                                    return {
                                        "filter": {
                                            "'.$val['attribute'].'": value
                                        },
                                        "column": "'.$val['attribute'].'",
                                        "page": params.page || 1
                                    }
                                }'
                            ),
                            'processResults' => new JSExpression(
                                'function (data, params) {
                                    params.page = params.page || 1;

                                    return {
                                        results: data.items,
                                        pagination: {
                                            more: data.more
                                        }
                                    };
                                }'
                            ),
                            'cache' => true
                        ]
                    ],
                    'options'=>[
                        'id'=>''.$val['attribute'].'FilterId'
                    ]
                ];
                $gridColumns[$key]['filterInputOptions'] = [
                    'placeholder'=>''
                ];
            }
        }

        //dynagrid configuration
        $dynagrid = DynaGrid::begin([
            'columns' => $gridColumns,
            'theme' =>'simple-default',
            'showPersonalize'=>true,
            'storage' => DynaGrid::TYPE_SESSION,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' =>[
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => ['id' =>'grid-entry-list'],
                    'beforeGrid' => '',
                    'afterGrid' => ''
                ],
                'responsiveWrap'=>false,
                'panel' => [
                    'heading' => false,
                    'before' => \Yii::t('app', "This is the browser for all the entries in your experiment. You can manage your entries here and all changes are autosaved.").'&emsp;'.'{summary}<br/><p id = "selected-items" class = "pull-right" style = "margin:-1px"><b id = "selected-count">'.$selectedItemsCount.'</b> selected items.</p>',
                    'after' => false,
                ],
                'toolbar' => [
                    ['content' => '&nbsp;'],//just for the space between summary and export button
                    [
                        'content' =>
                            Html::beginTag('div', ['class'=>'tex-left dropdown']).
                            Html::button('<i class="glyphicon glyphicon-plus"></i> ', ['type'=>'button', 'title'=>\Yii::t('app', 'Add Entries'), 'class'=>'light-green darken-3 btn', 'id'=>'add-entry-list-btn', 'data-toggle' => 'modal','data-target' => '#add-entries-modal']).
                            Html::button('<i class="glyphicon glyphicon-edit"></i> ', ['type'=>'button', 'title'=>Yii::t('app', 'Bulk update'), 'class'=>'btn', 'id'=>'bulk-update-btn', 'data-toggle' => 'modal','data-target' => '#manage-entry-modal']).  
                            $actionsDropdown.
                            Html::a(
                                '<i class="material-icons inline-material-icon">file_download</i>',
                                '',
                                [
                                    'data-pjax' => true,
                                    'id' => 'export-entries-btn',
                                    'class' => 'btn btn-default',
                                    'title' => \Yii::t('app', 'Export entries in CSV format')
                                ]
                            ).
                            Html::endTag('div')
                    ],
                    [
                        'content' => //$export. // temporary comment until export all is solved
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
                            ['create/specify-entry-list?program='.$program.'&id='.$id.'&processId='.$processId.'&reset=hard_reset'],
                            ['data-pjax'=>0, 'class' => 'btn btn-default reset-btn', 'title'=>\Yii::t('app','Reset grid'),'data-pjax'=>true])
                            . '{dynagridFilter}{dynagridSort}{dynagrid}'
                            . ManageSortsWidget::widget([
                                'dataBrowserId' => 'entry-list',
                                'gridId' => $browserId,
                                'searchModel' => 'EntrySearch',
                                'btnClass' => 'sort-action-button',
                                'resetGridUrl' => $resetGridUrl.'?program='.$program.'&id='.$id.'&processId='.$processId.'&reset=hard_reset'
                            ]),
                    ],
                ],
                'floatHeader' =>true,
                'floatOverflowContainer'=> true,
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
            ],
            'submitButtonOptions' => [
                'icon' => 'glyphicon glyphicon-ok',
            ],
            'deleteButtonOptions' => [
                'icon' => 'glyphicon glyphicon-remove',
                'label' => Yii::t('app', 'Remove'),
            ],
            'options' => ['id' => $browserId] // a unique identifier is important
        ]);
        DynaGrid::end();
    ?>    
</div>
<?php

    Modal::begin([
        'id' => 'generic-modal',
    ]);
    Modal::end();

    // Modal for validation
    Modal::begin([
        'id' => 'required-field-modal',
        'header' => '<h4><i class="fa fa-bell"></i> '.\Yii::t('app','Notification').'</h4>',
        'footer' => Html::a(\Yii::t('app','OK'),['#'],['id'=>'cancel-save-btn','class'=>'btn btn-primary waves-effect waves-light','data-dismiss'=>'modal']),
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static']
    ]);
    echo '<div class="row">
        <p><i class="fa fa-fw fa-warning txt-color-orangeDark font-md"></i> <span>'.\Yii::t('app', 'Please correct the errors below before proceeding.').'</span></p>
        </div><div class="row" id="required-field-notif"></div>';
    Modal::end();

    $headerName = strpos($displayName, 'parent') === false ? 'Entries' : 'Parents'; 

    //add entries
    Modal::begin([
        'id' => 'add-entries-modal',
        'header' => '<h4><i class="material-icons">add</i> '.\Yii::t('app','Add').' '. \Yii::t('app', "$headerName").'</h4>',
        'footer' =>
            '<div class="row" style="margin-top:10px; margin-right:10px;">'.Html::a(\Yii::t('app','Close'),'#',['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'</div>&emsp;',
        'size' =>'modal-xl',
        'options' => ['data-backdrop'=>'static'],
    ]);

    if($isCrossList){
        $items = [
            // [
            //     'label' => Yii::t('app','<i class="glyphicon glyphicon-th"></i> Parent List'),
            //     'active' => true,
            //     'options'=>['id'=>'parent-list-link'],
            //     'linkOptions' => ['data-url'=>Url::to(['title'=>'From parent lists','render-occurrence-list', 'id'=>$id, 'program'=>$program, 'processId'=>$processId, 'entryListType'=>'parent list', 'gridName' => 'parent'])]
            // ],
            [
                'label' => '<i class="glyphicon glyphicon-equalizer"></i>'.Yii::t('app',' Occurrence List'),
                'active' => true,
                'linkOptions' => ['title'=>'From other occurrences', 'data-url'=>Url::to(['render-occurrence-list', 'id'=>$id, 'program'=>$program, 'processId'=>$processId, 'entryListType'=>'parent list|entry list', 'gridName' => 'occurrence'])]
            ],
        ];
        $activeTab = 'parent-list-link';
    } else{
        
        $items = [
            [
                'label'=>Yii::t('app','<i class="glyphicon glyphicon-edit"></i> Input List'),
                'active'=>true,
                'options'=>['id'=>'from-input-list-link'],
                'content' => '<div class="progress"><div class="indeterminate"></div></div>',
                'linkOptions'=>['data-url'=>Url::to(['render-input-list', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]), 'class'=>'import-entries-link']
            ],
            // [
            //     'label'=>Yii::t('app','<i class="glyphicon glyphicon-inbox"></i> Advanced Trial'),
            //     'active'=>false,
            //     'linkOptions'=>['data-url'=>Url::to(['render-from-trial', 'id'=>$id, 'program'=>$program, 'processId'=>$processId])]
            // ],
            [
                'label'=>Yii::t('app','<i class="glyphicon glyphicon-th"></i> Advanced Nursery'),
                'options'=>['id'=>'render-from-nursery-link'],
                'active'=>false,
                'linkOptions'=>['data-url'=>Url::to(['render-from-nursery', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]), 'class'=>'import-entries-link']
            ],
            [
                'label'=>Yii::t('app','<i class="fa fa-copy"></i> Copy Entry List'),
                'active'=>false,
                'options'=>['id'=>'render-copy-experiment-link'],
                'linkOptions'=>['data-url'=>Url::to(['render-copy-experiment', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]), 'class'=>'import-entries-link']
            ],
            [
                'label'=> Yii::t('app','<i class="glyphicon glyphicon-th-list"></i> Saved List'),
                'active'=>false,
                'options'=>['id'=>'render-saved-list-link'],
                'linkOptions'=>['data-url'=>Url::to(['render-saved-list']), 'class'=>'import-entries-link']
            ],
        ];
        $activeTab = 'from-input-list-link';
    }

    echo TabsX::widget([
        'items'=>$items,
        'position'=>TabsX::POS_LEFT,
        'encodeLabels'=>false,
        'pluginOptions'=>[
            'enableCache'=>false
        ],
    ]);
    Modal::end();
    
    //Modal for entry management
    Modal::begin([
        'id' => 'entry-list-notif-modal',
        'footer' =>
            Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'size' =>'small-lg',
        'options' => ['data-backdrop'=>'static'],
    ]);
    echo '<div id="entry-modal-notif"></div>';
   
    Modal::end();

    //New Modal for entry management, actions button - select variables
    Modal::begin([
        'id'=> 'manage-entry-modal',
        'header' => '<h4><i class="glyphicon glyphicon-edit"></i>  '.\Yii::t('app','Bulk Update').'</h4>',
        'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'close-bulk-update-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            '<a id="bulk-update-selected" class="btn btn-primary green waves-effect waves-light confirm-update-btn" mode ="selected">'.\Yii::t('app','Selected').'</a>&nbsp;'.
            '<a id="bulk-update-selected-page" class="btn btn-primary green waves-effect waves-light confirm-update-btn" mode ="current-page">'.\Yii::t('app','Selected in Page').'</a>&nbsp;'.
            '<a id="bulk-update-all" class="btn btn-primary teal waves-effect waves-light confirm-update-btn" mode ="all">'.\Yii::t('app','All').'</a>',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static', 'tabindex' => false],
    ]);
    echo '<div id = "modal-progress" class = "progress hidden"><div class="indeterminate"></div></div>';
    echo '<div id = "modal-content"><p id="instructions"></p>';
    echo '<span id="note"></span>';
    echo '<div id="config-var" style="margin-left:30px;"></div></div>';
    Modal::end();

    //Modal for delete entry management
    Modal::begin([
        'id' => 'entry-list-confirm-modal',
        'header' => '<h4><div id="entry-list-confirm-header"><i class="left material-icons orange-text">warning</i>Delete Entries</h4>',
        'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-delete-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            '<a id="all-delete" class="btn btn-primary waves-effect waves-light confirm-delete-btn">Confirm</a>'.
            '<a id="current-page-delete" class="btn btn-primary waves-effect waves-light confirm-delete-btn">Confirm</a>'.
            '<a id="selected-delete" class="btn btn-primary waves-effect waves-light confirm-delete-btn">Confirm</a>',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static'],
		'closeButton' => ['class' => 'hidden'],
    ]);
    echo '<div id="entry-modal-confirm"></div>';
    echo '<div id="entry-modal-confirm-notif" class="hidden"></div>';
    Modal::end();

    //Modal for reorder entry management
    Modal::begin([
        'id' => 'entry-list-reorder-modal',
        'header' => \Yii::t('app','<h4>Reorder Entry List</h4>'),
        'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            '<a id="reorder-by-sort" class="btn btn-primary waves-effect waves-light sort-btn">Confirm</a>',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static'],
    ]);
    
    echo '<div id="reorder-modal-content"></div>';
    echo '<div id="reorder-modal-confirm-notif" class="hidden"></div>';
    Modal::end();

    //Modal for reorder all entries management
    Modal::begin([
        'id' => 'entry-list-reorder-all-modal',
        'header' => \Yii::t('app','<h4>Reorder Entry List</h4>'),
        'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-all-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            '<a id="reorder-all-by-sort" class="btn btn-primary waves-effect waves-light confirm-sort-all-btn">Confirm</a>',
        'size' =>'modal-lg',
        'options' => ['data-backdrop'=>'static'],
    ]);
    
    echo '<div id="reorder-all-modal-content"></div>';
    echo '<div id="reorder-all-modal-confirm-notif" class="hidden"></div>';
    Modal::end();

     //Modal for seedlot management
     Modal::begin([
        'id' => 'select-package-modal',
        'header' => \Yii::t('app','<h4>Select Package</h4>'),
        'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            '<a id="save-package-btn" class="btn btn-primary waves-effect waves-light sort-btn">Save</a>',
        'size' =>'modal-xl',
        'options' => ['data-backdrop'=>'static'],
    ]);
  
    
    Modal::end();

    //Modal for bulk update packages
    Modal::begin([
        'id' => 'entry-list-bulk-seeds-modal',
        'header' => \Yii::t('app','<h4>Select Packages</h4>'),
        'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            Html::a(\Yii::t('app','Confirm'),'#',['id'=>'bulk-seedlot-confirm','class'=>'disabled btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'size' =>'modal-lg',
        'options' => [
            'data-backdrop'=>'static',
            'tabindex' => false
        ],
    ]);
    echo '<div id="bulk-seed-lot-form"></div>';
    
    Modal::end();

    //Modal for updating designations
    Modal::begin([
        'id' => 'modify-designations-modal',
        'header' => \Yii::t('app','<h4>Modify Designations</h4>'),
        'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            Html::a(\Yii::t('app','Confirm'),'#',['id'=>'modify-designations-confirm','class'=>'disabled btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'size' =>'modal-lg',
        'options' => [
            'data-backdrop'=>'static',
            'tabindex' => false
        ],
    ]);
    echo '<div id="modify-designations-form"></div>';
    
    Modal::end();

    //Generic modal for the dynamic configuration
    Modal::begin([
        'id' => 'generic-modal',
        'header' => \Yii::t('app','<h4 id="generic-modal-header"></h4>'),
        'footer' => Html::a(\Yii::t('app','Cancel'),['#'],['id'=>'cancel-save-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
            Html::a(\Yii::t('app','Confirm'),'#',['id'=>'generic-modal-btn-confirm','class'=>'disabled btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'size' =>'modal-lg',
        'options' => [
            'data-backdrop'=>'static',
            'tabindex' => false
        ],
    ]);
    echo '<div id ="generic-modal-form-notif"></div>';
    echo '<div id="generic-modal-form"></div>';
    
    Modal::end();

   
?>

<?php
$importUrl = Url::to(['create/import-saved-list', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$renderSeedLotUrl = Url::to(['create/render-seed-lot', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);

$renderPackagesUrl = Url::to(['create/render-packages', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$renderBulkPackageUrl = Url::to(['create/render-bulk-package', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$renderModifyDesignationsUrl = Url::to(['create/render-modify-preferred-designations', 'id'=>$id, 'program'=>$program,'processId'=>$processId]);
$removeEntriesUrl = Url::to(['create/remove-entries', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$saveEntryFieldUrl = Url::to(['create/save-row-field', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$getAllEntryListEntriesUrl = Url::to(['create/get-all-entry-list-entries']);
$checkEntryListSelectionStatusUrl = Url::to(['create/check-entry-list-selection-status']);
$getSelectedItemsUrl = Url::to(['create/get-selected-items']);
$bulkUpdateFieldsUrl = Url::to(['create/bulk-update-fields']);

$renderReorderUrl = Url::to(['create/render-reorder', 'sort'=>isset($_GET['sort'])? $_GET['sort'] : '', 'id'=>$id, 'program'=>$program,'processId'=>$processId]);
$reorderEntriesSortUrl = Url::to(['create/reorder-entries-sort', 'sort'=>isset($_GET['sort'])? $_GET['sort'] : '', 'id'=>$id, 'program'=>$program,'processId'=>$processId]);
$saveBulkPackagesUrl = Url::to(['create/save-bulk-packages', 'id'=>$id, 'program'=>$program, 'processId'=>$processId]);

$genericConfUpdateUrl = Url::to(['create/update-config-variable','id'=>$id, 'program'=>$program, 'processId'=>$processId]);
$genericRenderConfigUrl = Url::to(['create/render-config-form', 'id'=>$id, 'program'=>$program]);
$renderBulkUpdateUrl = Url::to(['create/render-bulk-update','id'=>$id,'program'=>$program]);
$genericCheckReqVarsUrl = Url::to(['create/generic-check-required', 'id'=>$id, 'program'=>$program]);
$validateRequiredUrl  = Url::to(['create/validate-required','id'=>$id]);
$checkStatusUrl  = Url::to(['create/check-status','id'=>$id, 'action'=>'specify-entry-list']);
$checkThresholdCountUrl = Url::to(['create/check-threshold-count','id'=>$id]);
$getEntriesCurrentPageUrl = Url::to(['remove/entries-current-page','id'=>$id, 'program'=>$program, 'processId'=>$processId, 'sort'=>$entryListBrowserSort]);
$renderReorderSelectedUrl = Url::to(['create/reorder-entries-selected','id'=>$id]);

$exportEntityUrl = Url::to(['create/export-entity']);
$prepareDataForCsvDownloadUrl = Url::to(['create/prepare-data-for-csv-download','id'=>$id, 'program'=>$program, 'entity'=>'entry']);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$configValues = json_encode($requiredFields);
$requiredFields = json_encode($columns);
$urlParams = json_encode($urlParams);
$jsonActionDropdown = json_encode($actionsDropdown);

Yii::$app->view->registerJs("
    var genericConfUpdateUrl = '".Url::toRoute(['/experimentCreation/create/update-config-variable','id'=>$id, 'program'=>$program, 'processId' => $processId])."',
    genericRenderConfigUrl = '".$genericRenderConfigUrl."',
    checkStatusUrl = '".$checkStatusUrl."',
    experimentId = '".$id."',
    requiredFieldJson = '".$configValues."',
    genericCheckReqVarsUrl = '".$genericCheckReqVarsUrl."',
    bulkUpdateFieldsUrl = '".$bulkUpdateFieldsUrl."',
    getSelectedItemsUrl = '".$getSelectedItemsUrl."',
    checkThresholdCountUrl = '".$checkThresholdCountUrl."',
    getEntriesCurrentPageUrl = '".$getEntriesCurrentPageUrl."',
    urlParams = '".$urlParams."',
    renderedView = 'specify-entry-list',
    checkStatValue = 'entry list created|entry list incomplete seed sources'
    ;",
    \yii\web\View::POS_HEAD);

$this->registerJsFile("@web/js/experiment-creation/modal_entry_list_bulk_update.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJsFile("@web/js/check-required-field-status.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$totalEntries = isset($model['entryCount']) ? $model['entryCount'] : 0;
$updateEntryRoleUrl = isset($updateEntryRoleUrl) ?? '';
$currentPage = isset($currentPage) ?? '';
$entryColumns = isset($entryColumns) ?? '';
$entryDataColumns = isset($entryDataColumns) ?? '';

$this->registerJs(<<<JS
    var loadingIndicator = '<div class="progress"><div class="indeterminate"></div></div>';
    var experimentStatus = '$experimentStatus';
    var exportRecordsThresholdValue = $exportRecordsThresholdValue
    //reinitialize event bindings after pjax reload

    checkIfRequireBrowserReset();

    $(document).on('ready pjax:success', function(e) {
        
        checkStatus();
        $('.dropdown-button').dropdown();

        $(".package-record-option").click(function(){
            var entryId = $(this).attr("data-id");
            var germplasmId = $(this).attr("data-gid");
            packageRecOption(entryId, germplasmId);
        });

        $('#sel-seed-selected').click(function(e){
            selectedSeed(e);
        });

        $('#sel-seed-all').click(function(e){
            selectSeedAll(e);
        });

        $(".remove_entries").click(function(e) {
            var id = this.id;
            removeEntries(id, e);
        });

        $(".reorder_entries").click(function(e) {
            var id = this.id;
            reorderEntries(id, e);
        });

        $(document).on('click', '.confirm-delete-btn', function(e) {
            deleteEntries(e, this.id);
        });

        //select grid
        $(document).on('click','.entry-grid-select',function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
            var unset;
            var entryDbId = $(this).prop('id');

            if($(this).prop('checked')){
                unset = false;
            }else{
                unset = true;
                $('#entry-select-all').prop('checked',false);
                $('#entry-select-all').removeAttr('checked');
            }

            var selected = localStorage.getItem('ec_entry_list_entry_ids');
            selected = getSelectedEntries(selected);
            selected = (selected == null) ? {} : selected;
            if(unset) { // remove selected
                delete selected[entryDbId];
                selected = (Object.keys(selected).length === 0) ? null : selected;
            } else {  // add selected
                selected[entryDbId] = entryDbId;
            }
            localStorage.setItem('ec_entry_list_entry_ids', JSON.stringify(selected));  // save selected to localStorage

            var count = getSelectedCount(selected); // update count
            checkSelectedParentCounter(count);
        });
    });
    checkStatus();
    $(document).on('click', '.select-list', function(e) {
        var savedListId = $(this).attr('data-saved_list_id');
        var activeTabId = '$activeTab';

        $('#mod-progress').removeClass('hidden');
        var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
        $('#mod-progress').html(loading);


        //Notification while background job is being created
        $.ajax({
            url: checkThresholdCountUrl,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'createEntries',
                toolStep: 'entries',
                method: 'create',
                mode: 'savedList',
                savedListId: savedListId
            },
            success: function(response) {
                isBgUsedFlag = response;
                $('#mod-notif').removeClass('hidden');
                $('.search-entry-field-div').addClass('hidden');
                $('#saved-list-table').addClass('hidden');

                if(isBgUsedFlag == true){
                    $('#mod-notif').html('<div class="alert ">'+ 
                            '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                '<i class="fa fa-info-circle"></i>'+
                                    ' A background job is being created.' +
                            '</div></div>');
                }
            }
        });

        $.ajax({
            url: '$importUrl',
            type: 'post',
            dataType: 'json',
            data: {
                savedListId: savedListId,
                experimentId: $id,
                program: '$program',
                processId: '$processId'
            },
            success: function(response) {
                $('#mod-progress').html('');
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully added the selected list.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();

                $('.search-entry-field-div').removeClass('hidden');
                $('#saved-list-table').removeClass('hidden');
                
                $('a[href="#' + activeTabId + '"]').click();
                $('#add-entries-modal').modal('hide');

                $.pjax.reload({
                  container: '#$browserId-pjax',
                  replace:false
                });
            },
            error: function() {
                location.reload();
            }
        });
    });

    $(document).on('click', '.reset-btn', function(e) {
        localStorage.removeItem('ec_entry_list_entry_ids');
        checkSelectedParentCounter(0);
        location.reload();
    });

    $(".package-record-option").click(function(){
        var entryId = $(this).attr("data-id");
        var germplasmId = $(this).attr("data-gid");
        packageRecOption(entryId, germplasmId);
    });

    function packageRecOption(entryId, germplasmId){
        
        var url = '$renderPackagesUrl';
        var update_modal = '#select-package-modal > .modal-dialog > .modal-content > .modal-body';
            
        $('#select-package-modal').modal('show');
        $(update_modal).html(loadingIndicator);

        setTimeout(function(){
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    entryId: entryId,
                    germplasmId: germplasmId
                },
                success: function(response) {
                    if (response) {
                        $(update_modal).html(response.htmlData);
                    }
                },
                error: function() {
                    $(update_modal).html('<div class="card-panel red"><span class="white-text">There seems to be a problem loading the packages  .</span></div>');
                }
            });
        },10);
    }
    
    //select grid entries
    $(document).on('click','.entry-grid-select',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var count = $('#selected-count').val() == 'No' ? 0 : $('#selected-count').val();
        var unset;
        var entryDbId = $(this).prop('id');

        if($(this).prop('checked')){
            unset = false;
        }else{
            unset = true;
            $('#entry-select-all').prop('checked',false);
            $('#entry-select-all').removeAttr('checked');
        }

        var selected = localStorage.getItem('ec_entry_list_entry_ids');
        selected = getSelectedEntries(selected);
        selected = (selected == null) ? {} : selected;
        if(unset) { // remove selected
            delete selected[entryDbId];
            selected = (Object.keys(selected).length === 0) ? null : selected;
        } else {  // add selected
            selected[entryDbId] = entryDbId;
        }
        localStorage.setItem('ec_entry_list_entry_ids', JSON.stringify(selected));  // save selected to localStorage

        var count = getSelectedCount(selected); // update count
        checkSelectedParentCounter(count);
    });

    // select all entries
    $(document).on('click','#entry-select-all',function(e){

        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.entry-grid-select').attr('checked','checked');
            $('.entry-grid-select').prop('checked',true);
            $(".entry-grid-select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
        }else{

            $(this).removeAttr('checked');
            $('.entry-grid-select').prop('checked',false);
            $('.entry-grid-select').removeAttr('checked');
            $("input:checkbox.entry-grid-select").parent("td").parent("tr").removeClass("grey lighten-4");
        }

        var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
        var unset = $(this).prop('checked') ? false : true;
        var entryDbId = $(this).prop('id');

        if(unset) { // clear all
            localStorage.removeItem('ec_entry_list_entry_ids');
            checkSelectedParentCounter(0);
        } else {  // select all
            // retrieve all entries (ajax call)
            $.ajax({
                url: '$getAllEntryListEntriesUrl',
                type: 'post',
                data: {
                    sessionName: 'ec-entry-list'
                },
                cache: false,
                success: function(response) {
                    response = JSON.parse(response)
                    localStorage.setItem('ec_entry_list_entry_ids', JSON.stringify(response));  // put entries to localStorage
                    var count = getSelectedCount(response); // update count
                    checkSelectedParentCounter(count);
                },
                error: function(e) {
                    console.log(e);
                }
            });
        }

    });

    $(document).on('keypress keyup blur paste change', ".editable-field", function(e) {
        e.stopImmediatePropagation();
        var id = this.id;
        var fields = (this.id).split('-');
        var column = fields[0];
        var targetTable = fields[1];
        var entryDbId = fields[2];
        var value = $('#' + id).val();

        // Save the changes in the database
        $.ajax({
            url: '$saveEntryFieldUrl',
            type: 'post',
            datatType: 'json',
            cache: false,
            data: {
                entryDbId: entryDbId,
                column: column,
                targetTable: targetTable,
                value: value,
                experimentDbId: '$id'
            },
            success: function(response){
                checkStatus();
            },
        });
    });
    
    $('#bulk-seedlot-confirm').click(function(e){
        var entryPackageArray = [];
        var uniqueEntryDbIds = []
        $('.action-modal-radio-seedlot').each(function(){
            var entryDbId = $(this).attr('data-entryId')
            if($(this).is(':checked')){
                let obj = {
                    entryDbId: entryDbId,
                    seedDbId: $(this).val().split('_')[0],
                    packageDbId: $(this).val().split('_')[1]
                };
                entryPackageArray.push(obj);
            }
            if (!uniqueEntryDbIds.includes(entryDbId)) {
                uniqueEntryDbIds.push(entryDbId)
            }
        });
        var getUrlVarsVal = getUrlVars();
        var page = '';
        var perPage = '';
        var dp1_page = '';
        var dp1_perPage = '';
      
        if(typeof page != undefined && (typeof perPage != undefined)){
            page = getUrlVars()['page'];
            perPage = getUrlVars()['per-page'];
        }
        if(typeof (getUrlVars()['dp-1-per-page']) != undefined){
            dp1_perPage = getUrlVars()['dp-1-per-page'];
        }
        if(typeof (getUrlVars()['dp-1-page']) != undefined){
            dp1_page = getUrlVars()['dp-1-page'];
        }
        $.ajax({
            url: '$saveBulkPackagesUrl',
            type: 'post',
            data: {
                entryPackageArray: JSON.stringify(entryPackageArray),
                page: page,
                perPage: perPage,
                dp1_page: dp1_page,
                dp1_perPage: dp1_perPage
            },
            success: function(response) {
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    
                    if (entryPackageArray.length < uniqueEntryDbIds.length) {
                        let missingEntriesSize =  uniqueEntryDbIds.length - entryPackageArray.length
                        let message = (missingEntriesSize == 1) ? "No package selected for 1 entry." : "No package selected for "+missingEntriesSize+" entries."
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>"+message+"</span>";
                        Materialize.toast(notif, 5000);
                    }
                    
                    var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully updated the entry and reserved the package.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();

                $.pjax.reload({
                  container: '#$browserId-pjax',
                  replace:false
                });
            },
            error: function() {
            }
        });
    });

    $("document").ready(function() {
        
        setTimeout(function() {
            $(".tabs-krajee").find("li.active a").click();
        },10);

        checkStatus();

        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        $(".import-entries-link").click(function(e) {
            var importTab = $(this).attr('href');
            
            if ($(importTab).html().length === 12) { //<br><br><br> returned when empty
                $(importTab).html(loadingIndicator);
            }               
        });
    });
    
    var grid = $('#grid-id');
    $('#saved-list-table').on('kvexprow:toggle', function (event, ind, key, extra, state) {
    });
    $(document).on('kvexprow:toggle', '#saved-list-table', function() {
        alert("TOGGLED");
    });
    
    $(document).on('click', '.confirm-delete-btn', function(e) {
        deleteEntries(e, this.id);
    });

    function deleteEntries(e, id){
        e.preventDefault();
        e.stopImmediatePropagation();
        
        var entryIds = null;
        var entryIdsCount = null;
        var type = 'all';
        var mode = null;
        
        $('#entry-modal-confirm').html(loadingIndicator);
        $('.confirm-delete-btn').addClass('hidden');
        $('#cancel-delete-btn').addClass('hidden');
        $('.close').addClass('hidden');
        if(!experimentStatus.includes('design requested')){
            
            if(id == "selected-delete"){
                var selected = localStorage.getItem('ec_entry_list_entry_ids');
                selected = getSelectedEntries(selected);
                selected = (selected == null) ? {} : selected;

                entryIds = Object.keys(selected);
                entryIdsCount = getSelectedCount(selected);
                type = 'selected';
            }else if (id == "current-page-delete"){
                type = 'current-page';
                mode = 'current-page';
            
                var entryData = selectCurrentPage();
              
                if(typeof entryData['entryIds'] != undefined){
                    entryIds = entryData['entryIds'];
                }

                if(typeof entryData['entryIdsCount'] != undefined){
                    entryIdsCount = entryData['entryIdsCount'];
                }
              
            }
            
            //Fires the background process validation if the conditions are met
            $.ajax({
                url: checkThresholdCountUrl,
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'deleteEntries',
                    toolStep: 'entries',
                    selectedItemsCount: entryIdsCount,
                    method: 'delete',
                    mode: mode,
                    selectedItemsCount: entryIdsCount
                },
                async: false,
                success: function(response) {
                   
                    if(response){
                        $('#entry-modal-confirm-notif').removeClass('hidden');
                        $('#entry-modal-confirm-notif').html('<div class="alert ">'+ 
                                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                    '<i class="fa fa-info-circle"></i>'+
                                        ' A background job is being created.' +
                                '</div></div>');
                    } else {
                        $('#entry-modal-confirm').html("");
                        $('#entry-modal-confirm').html(loadingIndicator);
                    }
                }
            });

            $.ajax({
                url: '$removeEntriesUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    entryIds : JSON.stringify(entryIds),
                    type : type,
                    processId: '$processId'
                },
                async: false,
                success: function(response) {
                    $('.confirm-delete-btn').attr("disabled", false);
                    
                    $('#entry-list-confirm-modal').modal('hide');
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        Materialize.toast(response, 5000);
                    }

                    // reset selection
                    localStorage.removeItem('ec_entry_list_entry_ids');
                    checkSelectedParentCounter(0);
                    $("input:checkbox.entry-grid-select").each(function(){
                        $(this).removeAttr('checked');
                        $('.entry-grid-select').prop('checked',false);
                        $('.entry-grid-select').removeAttr('checked');
                        $("input:checkbox.entry-grid-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');
                    });

                    e.preventDefault();
                    e.stopImmediatePropagation();
                    if(!response.includes('Unable')){
                        $.pjax.reload({
                            container: '#$browserId-pjax'
                        });
                    }

                    // disable succeeding tabs
                    $('#section-3').addClass('disabled');
                    $('#section-4').addClass('disabled');
                    $('#section-5').addClass('disabled');
                    $('#section-6').addClass('disabled');
                    $('#section-7').addClass('disabled');
                },
                error: function() {
                }
            });
        } else {
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> There's an ongoing generation of records. Please try again.</span>";
                Materialize.toast(notif, 5000);
            }
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    }


    $('#update-role-all').click(function(){
      
        $('#update-role-form-notif').html('');
        $('#update-role-form').removeClass('hidden');
        $("#update-role-confirm").show();
        $("#update-role-confirm").removeClass('disabled');
        $('#entry-list-bulk-role').modal('show');


        $('#update-role-confirm').click(function(){

            var entryRole = $('#entry-role-dropdown').val();

            var getUrlVarsVal = getUrlVars();
            var page = '';
            var perPage = '';
            var dp1_page = '';
            var dp1_perPage = '';

            if(typeof (getUrlVars()['dp-1-per-page']) != undefined){
                dp1_perPage = getUrlVars()['dp-1-per-page'];
            }

            if(typeof (getUrlVars()['dp-1-page']) != undefined){
                dp1_page = getUrlVars()['dp-1-page'];
            }
          
            if(typeof page != undefined && (typeof perPage != undefined)){
                page = getUrlVars()['page'];
                perPage = getUrlVars()['per-page'];
            }
         
            $.ajax({
                url: '$updateEntryRoleUrl',
                type: 'post',
                data: {
                    entryRole: entryRole,
                    mode: 'all',
                    page: page,
                    perPage: perPage,
                    dp1_page: dp1_page,
                    dp1_perPage: dp1_perPage,
                    experimentId:'$id',
                },
                success: function(response) {
                
                },
                error: function() {
                
                }
            });
        });
    });

    $('#update-role-selected').click(function(){

        var entryIdArray = $('#grid-entry-list').yiiGridView('getSelectedRows');
        
        $('#entry-list-bulk-role').modal('show');

        if(entryIdArray.length > 0){
            $("#update-role-confirm").removeClass('disabled');
        }else{
            $('#update-role-form').addClass("hidden");
            $('#update-role-form-notif').html('<div class="card-panel red"><span class="white-text">You have not selected any entry. Select at least one using the checkbox in the gridview below.</span></div>');
            
        }

        var getUrlVarsVal = getUrlVars();
        var page = '';
        var perPage = '';
      
        if(typeof page != undefined && (typeof perPage != undefined)){
            page = getUrlVars()['page'];
            perPage = getUrlVars()['per-page'];
        }

        var dp1_page = '';
        var dp1_perPage = '';

        if(typeof (getUrlVars()['dp-1-per-page']) != undefined){
            dp1_perPage = getUrlVars()['dp-1-per-page'];
        }

        if(typeof (getUrlVars()['dp-1-page']) != undefined){
            dp1_page = getUrlVars()['dp-1-page'];
        }
            
  
        $('#update-role-confirm').click(function(){
            var entryRole = $('#entry-role-dropdown').val();
         
            $.ajax({
                url: '$updateEntryRoleUrl',
                type: 'post',
                data: {
                    entryIdArray: entryIdArray,
                    entryRole: entryRole,
                    page: page,
                    perPage: perPage,
                    dp1_page: dp1_page,
                    dp1_perPage: dp1_perPage,
                    experimentId:'$id'
                },
                success: function(response) {
                
                },
                error: function() {
                
                }
            });

        });

    });

    $('#sel-seed-selected').click(function(e){
        selectedSeed(e);
    });

    $('#sel-seed-all').click(function(){
        selectSeedAll();
    });

    function selectSeedAll(e){
        $('#bulk-seedlot-confirm').addClass('disabled');

        //ajax call to update content
        var url = '$renderBulkPackageUrl';

        $('#entry-list-bulk-seeds-modal').modal('show');

        $('#bulk-seed-lot-form').html(loadingIndicator);

        setTimeout(function(){
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {

                },
                success: function(response) {
                    $('#bulk-seed-lot-form').html(response);
                },
                error: function() {

                }
            });
        },100);
    }

    function selectedSeed(e){
        $('#bulk-seedlot-confirm').addClass('disabled');

        //ajax call to update content 
        var url = '$renderBulkPackageUrl';

        var entryIdArray = [];
        var selected = localStorage.getItem('ec_entry_list_entry_ids');
        selected = getSelectedEntries(selected);
        selected = (selected == null) ? {} : selected;

        entryIdArray = Object.keys(selected);
        entryIdCount = getSelectedCount(selected);

        if(entryIdCount > 0){
            $('#entry-list-bulk-seeds-modal').modal('show');

            $('#bulk-seed-lot-form').html(loadingIndicator);

            setTimeout(function(){

                $.ajax({
                    url: '$renderBulkPackageUrl',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        entryIdArray: JSON.stringify(entryIdArray)
                    },
                    success: function(response) {
                        $('#bulk-seed-lot-form').html(response);
                    },
                    error: function() {

                    }
                });

            },100);

        }else{
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please select at least one(1) entry.</span>";
                Materialize.toast(notif, 5000);
            }
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    }

    $('#modify_selected').click(function(){

        var entryIdArray = $('#grid-entry-list').yiiGridView('getSelectedRows');

        var url = '$renderModifyDesignationsUrl';
        
        $('#modify-designations-modal').modal('show');

        var getUrlVarsVal = getUrlVars();
        var page = '';
        var perPage = '';
        var dp1_page = '';
        var dp1_perPage = '';
      
        if(typeof page != undefined && (typeof perPage != undefined)){
            page = getUrlVars()['page'];
            perPage = getUrlVars()['per-page'];
        }

        if(typeof (getUrlVars()['dp-1-per-page']) != undefined){
            dp1_perPage = getUrlVars()['dp-1-per-page'];
        }

        if(typeof (getUrlVars()['dp-1-page']) != undefined){
            dp1_page = getUrlVars()['dp-1-page'];
        }


        if(entryIdArray.length > 0){

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    type: 'selected',
                    experimentId: '$id',
                    totalEntCount: $totalProviderCount,
                    idArray: entryIdArray,
                    page: page,
                    perPage: perPage,
                    dp1_page: dp1_page,
                    dp1_perPage: dp1_perPage
                },
                success: function(response) {

                    if(response.count === '0' || response.count === 0){
                        $("#modify-designations-confirm").attr("disabled", true);
                    }else{
                        $("#modify-designations-confirm").attr("disabled", false);
                    }

                    $('#modify-designations-form').html(response.data);
                    
                },
                error: function() {

                }
            });

        }else{

            $('#modify-designations-form').html('<div class="card-panel red"><span class="white-text">You have not selected any entry. Select at least one using the checkbox in the gridview below.</span></div>');

        }
    });


    $('#modify_all').click(function(){

        var url = '$renderModifyDesignationsUrl';
        
        $('#modify-designations-modal').modal('show');
        var getUrlVarsVal = getUrlVars();
        var page = '';
        var perPage = '';
      
        if(typeof page != undefined && (typeof perPage != undefined)){
            page = getUrlVars()['page'];
            perPage = getUrlVars()['per-page'];
        }


        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
              type: 'all',
              experimentId: '$id',
              totalEntCount: $totalProviderCount,
              page: page,
              perPage: perPage
            },
            success: function(response) {

                if(response.count === '0' || response.count === 0){
                    $("#modify-designations-confirm").attr("disabled", true);
                }else{   if(!empty($currentPage)){
                
            }else 
                    $("#modify-designations-confirm").attr("disabled", false);
                }

                $('#modify-designations-form').html(response.data);
                
            },
            error: function() {

            }
        });

    });

    $(".remove_entries").click(function(e) {
        var id = this.id;
        removeEntries(id, e);
    });

    $(".reorder_entries").click(function(e) {
        var id = this.id;
        reorderEntries(id, e);
    });

    function removeEntries(id, e){

        var entryTotal = '$totalEntries';
        var limit = 500;
        var note = '<br/> <br/> <b>NOTE:</b> Entries that have already been used in the succeeding steps e.g crosses, design tabs will not be removed, if any.';

        if(id == 'all'){
            if(entryTotal > limit){
                confirmMsg = 'You are about to remove more than <b>' + limit + '</b> draft entries. You may be momentarily redirected to the Experiment browser and will be notified once the entries were successfully deleted.';
            } else {
                confirmMsg = 'You are about to remove <b>all</b> draft entries.';
            }

            $('.confirm-delete-btn').attr("disabled", false);
            $('#entry-list-confirm-modal').modal();
            $('#entry-modal-confirm').html(confirmMsg + note);
            $('#selected-delete').addClass('hidden');
            $('#current-page-delete, #cancel-delete-btn').addClass('hidden');
            $('#all-delete, #cancel-delete-btn').removeClass('hidden');
        } 
        else if (id == 'current-page'){
            confirmMsg = '';
            var entryData = selectCurrentPage();
           
            if(typeof entryData['entryIdsCount'] != undefined){
                entryIdCount = entryData['entryIdsCount'];
            }

            if(entryIdCount === 0){
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please select at least one(1) entry.</span>";
                    Materialize.toast(notif, 5000);
                }
            } else {
                if(entryIdCount > limit){
                    confirmMsg = 'You are about to remove more than <b>' + limit + '</b> draft entries in the page. You may be momentarily redirected to the Experiment browser and will be notified once the entries were successfully deleted.';
                } else {
                    confirmMsg = 'You are about to remove <b>'+ entryIdCount +'</b> selected entries in the <b>page</b>.';
                }

                $('.confirm-delete-btn').attr("disabled", false);
                $('#entry-list-confirm-modal').modal();
                $('#entry-modal-confirm').html('');
                $('#entry-modal-confirm').html(confirmMsg + note);
                $('#selected-delete').addClass('hidden');
                $('#all-delete, #cancel-delete-btn').addClass('hidden');
                $('#current-page-delete, #cancel-delete-btn').removeClass('hidden');
            }
        }
        else {  // selected
            var entryIdArray = [];
            var selected = localStorage.getItem('ec_entry_list_entry_ids');
            selected = getSelectedEntries(selected);
            selected = (selected == null) ? {} : selected;

            entryIdArray = Object.keys(selected);
            entryIdCount = getSelectedCount(selected);

            if(entryIdCount > 0){
                if(entryIdCount > limit){
                    confirmMsg = 'You are about to remove more than <b>' + limit + '</b> selected draft entries. You may be momentarily redirected to the Experiment browser and will be notified once the entries were successfully deleted.';
                } else {
                    confirmMsg = 'You are about to remove <b>'+ entryIdCount +'</b> selected draft entries.';
                }
                $('.confirm-delete-btn').attr("disabled", false);
                $('#entry-list-confirm-modal').modal();
                $('#entry-modal-confirm').html(confirmMsg + note);
                $('#all-delete').addClass('hidden');
                $('#current-page-delete').addClass('hidden');
                $('#selected-delete, #cancel-delete-btn').removeClass('hidden');
            }else{
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please select at least one(1) entry.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            }
        }
    }

    function reorderEntries(id, e){

        var entryTotal = '$totalEntries';
        var limit = 500;
        
        var update_modal = '';
        if(id == 'selected' || id == 'reorder-individual' || id == 'reorder-by-group'){

            var entryIdArray = [];
            update_modal = '#reorder-modal-content';
            $(update_modal).html(loadingIndicator);
            var selected = localStorage.getItem('ec_entry_list_entry_ids');
            selected = getSelectedEntries(selected);
            selected = (selected == null) ? {} : selected;

            entryIdArray = Object.keys(selected);
            entryIdCount = getSelectedCount(selected);

            var entryLimit = 1;
            if(id == 'reorder-individual'){
                entryLimit = 0;
            }
            if(entryIdCount > entryLimit){
                if(entryIdCount <= 20){

                    $('#entry-list-reorder-modal').modal();

                    $.ajax({
                        url: '$renderReorderSelectedUrl',
                        type: 'post',
                        dataType:'json',
                        data: {
                            entryIdArray:entryIdArray,
                            reorderAction:id,
                            experimentId:'$id'
                        },
                        success: function(response) {
                            $(update_modal).html(response);

                        },
                        error: function() {
                            $('#entry-list-reorder-modal').modal('hide');
                            var hasToast = $('body').hasClass('toast');
                            if(!hasToast){
                                $('.toast').css('display','none');
                                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Unable to reorder entries. Please try again.</span>";
                                Materialize.toast(notif, 5000);
                            }

                            // reset selection
                            localStorage.removeItem('ec_entry_list_entry_ids');
                            checkSelectedParentCounter(0);
                            $("input:checkbox.entry-grid-select").each(function(){
                                $(this).removeAttr('checked');
                                $('.entry-grid-select').prop('checked',false);
                                $('.entry-grid-select').removeAttr('checked');
                                $("input:checkbox.entry-grid-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');
                            });
                            e.preventDefault();
                            e.stopImmediatePropagation();
                        }
                    });
                } else {
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please select at most twenty(20) entries.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            }else{
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Please select at least "+parseInt(entryLimit+1)+" entry/ies.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            }
        }
        else if(id == 'reorder-all'){
            var url = '$renderReorderUrl';
            var experimentId = '$id';

            var  currUrl = new URLSearchParams(window.location.search)
            var hasNewSort = currUrl.has('sort')

            if(hasNewSort){
                update_modal = '#reorder-all-modal-content';
                $(update_modal).html(loadingIndicator);
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType:'json',
                    data: {
                        reorderType:'sorting',
                        experimentDbId:experimentId
                    },
                    success: function(response) {
                        if(response.htmlData){
                            $(update_modal).html(response.htmlData);
                            $('#entry-list-reorder-all-modal').modal();
                        }
                        else{
                            var hasToast = $('body').hasClass('toast');
                            if(!hasToast){
                                $('.toast').css('display','none');
                                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Unable to reorder entries. Please try again.</span>";
                                Materialize.toast(notif, 5000);
                            }
                        }
                    },
                    error: function() {
                        $('#entry-list-reorder-all-modal').modal('hide');
                        
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> Unable to reorder entries. Please try again.</span>";
                            Materialize.toast(notif, 5000);
                        }
                        
                        e.preventDefault();
                        e.stopImmediatePropagation();
                    }
                });
            } 
            else{
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> No new sorting configuration.</span>";
                    Materialize.toast(notif, 5000);
                }
            }
        }
    }

    $('#next-btn').click(function(){
        var nextTabUrl = $(this).attr('data-url');

        window.location = nextTabUrl;
    });

    $(document).on('click', '#bulk-update-btn', function() {
        var note;
        var experimentStatus = "$experimentStatus";
        experimentStatus = experimentStatus.toLowerCase();
        $('#config-var').removeAttr('tabindex');
        $.ajax({
            url: '$renderBulkUpdateUrl', 
            type: 'post',
            dataType: 'json',
            data: {
                entryColumns: '$entryColumns', 
                entryDataColumns: '$entryDataColumns', 
                configVars: '$configValues'
            },
            success: function(response){
                $('#instructions').html("Bulk update the following values for the entry records. The changes will be reflected after closing the modal.");
                
                if(experimentStatus.includes('cross') || experimentStatus.includes('design')){
                    note = '<div class="alert alert-warning" style="padding: 10px"> If you wish to proceed, all related information (e.g. crosses, design) will be updated for the selected entries.</div>';
                    $('#note').html(note);
                }

                $('#config-var').html(response);
                $('.loading-req-vars').remove();
                $('#req-vars').css('visibility','visible');
                $('#req-vars').css('width','');
                $('#req-vars').css('height','');
            }
        });
    });

    
     $(document).on('input change keyup', '.search-text-entry-search-input, .search-text-add-entry-search-input', function() {
        typeSearch();
    });

    function typeSearch(){
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementsByClassName("search-text-entry-search-input");
        
        if(input[0] !== null && input[0] != undefined){

            filter = input[0].value.toUpperCase();

            if(filter != ''){
                $('.search-text-entry-clear-all-search-btn').css("opacity", "0.5");
            }else{
                $('.search-text-entry-clear-all-search-btn').css("opacity", "0");
            }
        }

        // for add entries
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementsByClassName("search-text-add-entry-search-input");
        
        if(input[0] !== null && input[0] != undefined){
            filter = input[0].value.toUpperCase();

            if(filter != ''){
                $('.search-text-entry-clear-all-search-btn').css("opacity", "0.5");
            }else{
                $('.search-text-entry-clear-all-search-btn').css("opacity", "0");
            }
        }
    }
    
    $(document).on('click', '.search-text-entry-submit-btn', function(e) {
        var selectedOption = $(this).parent();

        submitSearch(selectedOption.children()[0].value, $('#' + selectedOption.next().attr('id')).find('.search-entry-table'));
    });

    function submitSearch(input2,table2){
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementsByClassName("search-text-entry-search-input");

        if(input[0] !== null && input[0] != undefined){
            filter = input[0].value.toUpperCase();

            table = document.getElementsByClassName("search-seed-lot-table");

            tr = table[0].getElementsByTagName("tr"),
            th = table[0].getElementsByTagName("th");

            for (i = 1; i < tr.length; i++) {
                tr[i].style.display = "none";
                for(var j=2; j<th.length; j++){
                    td = tr[i].getElementsByTagName("td")[j];      
                    if (td) {
                        if (td.innerText.toUpperCase().indexOf(filter.toUpperCase()) > -1){
                            tr[i].style.display = "";
                            break;
                        }
                    }
                }
            }
        }

        // for add entries
        input = document.getElementsByClassName("search-text-add-entry-search-input");

        if(input2 !== null && input2 != undefined){
            filter = input2.toUpperCase();

            table = document.getElementsByClassName("search-entry-table");

            tr = table2[0].getElementsByTagName("tr"),
            th = table2[0].getElementsByTagName("th");

            for (i = 1; i < tr.length; i++) {
                tr[i].style.display = "none";
                for(var j=2; j<th.length; j++){
                    td = tr[i].getElementsByTagName("td")[j];      
                    if (td) {
                        if (td.innerText.toUpperCase().indexOf(filter.toUpperCase()) > -1){
                            tr[i].style.display = "";
                            break;
                        }
                    }
                }
            }
        }
    }

    // clear all texts in search input
    $(document).on('click', '.search-text-entry-clear-all-search-btn', function(e) {
        clearSearch();
    });

    function clearSearch(){
        var inputId = $(".search-text-entry-search-input").attr('id');
        $('.search-text-entry-clear-all-search-btn').css("opacity", "0");
        $('#'+inputId).val('');
        $('.search-text-entry-submit-btn').trigger('click');

        // for add entries
        var inputId = $(".search-text-add-entry-search-input").attr('id');
        $('.search-text-entry-clear-all-search-btn').css("opacity", "0");
        $('#'+inputId).val('');
        $('.search-text-entry-submit-btn').trigger('click');
    }

    refresh();
    // reload grid on pjax
    $(document).on('ready pjax:success', function(e) {
        e.stopPropagation();
        e.preventDefault();
        refresh();
       
        $(document).on('click', '.search-text-entry-submit-btn', function(e) {
            submitSearch();
        });

        // clear all texts in search input
        $(document).on('click', '.search-text-entry-clear-all-search-btn', function(e) {
            clearSearch();
        });

        $(document).on('input change keyup', '.search-text-entry-search-input, .search-text-add-entry-search-input', function() {
            typeSearch();
        });

        $(document).on('click', '.search-text-entry-submit-btn', function(e) {
            submitSearch();
        });

        $(".import-entries-link").click(function(e) {
            var importTab = $(this).attr('href');
            
            if ($(importTab).html().length === 12) { //<br><br><br> returned when empty
                $(importTab).html(loadingIndicator);
            }               
        });
    });

    // after pjax
    $(document).on('pjax:end', function(e) {
        checkIfRequireBrowserReset();
    });

    function refresh(){
        var maxHeight = ($(window).height() - 320);

        $(".kv-grid-wrapper").css("height", maxHeight);

        //Update selected items and count
        var selected = localStorage.getItem('ec_entry_list_entry_ids');
        selected = getSelectedEntries(selected);
        selected = (selected == null) ? {} : selected;

        $("input:checkbox.entry-grid-select").each(function(){
            var entryDbId = $(this).parent("td").parent("tr").attr('data-key');
            if(selected.hasOwnProperty(entryDbId)) {
                $(this).attr('checked','checked');
                $(this).attr('checked','checked');
                $(this).prop('checked',true);
                $(this).parent("td").parent("tr").find('*').addClass('lighten-4').removeClass('lighten-5');
            }
        });

        var count = getSelectedCount(selected); // update count
        checkSelectedParentCounter(count);

        $('#export-entries-btn').off('click').on('click', function (e) {
            e.preventDefault()

            if ($totalProviderCount >= exportRecordsThresholdValue) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '$prepareDataForCsvDownloadUrl',
                    data: {},
                    success: async function (ajaxResponse) {},
                    error: function (xhr, error, status) {}
                })
            } else {
                let exportEntityUrl = '$exportEntityUrl'
                let expEntUrl = window.location.origin + exportEntityUrl +'?id=' + '$id'+'&entity=entry'

                message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'

                $('.toast').css('display','none')

                Materialize.toast(message, 5000)
                window.location.replace(expEntUrl)

                $('#system-loading-indicator').html('')
            }
        })
    }
  

    // click row in entry browser
    $(document).on('click', '#$browserId tbody tr', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var thisRow = $(this).find('input:checkbox')[0];

        $("#"+thisRow.id).trigger("click");
        if(thisRow.checked){
            $(this).addClass("grey lighten-4");
        }else{
            $(this).removeClass("grey lighten-4");
        }
    });

    // check to see if browser selection should be reset
    function checkIfRequireBrowserReset(response){
        setTimeout(function(){
            $.ajax({
                url: '$checkEntryListSelectionStatusUrl',
                data: {
                },
                type: 'POST',
                async: true,
                success: function(response) {
                    response = JSON.parse(response);

                    // reset selection
                    if(response.resetEntryListParamsSelection) {
                        localStorage.removeItem('ec_entry_list_entry_ids');
                        checkSelectedParentCounter(0);
                        $("input:checkbox.entry-grid-select").each(function(){
                            $(this).removeAttr('checked');
                            $('.entry-grid-select').prop('checked',false);
                            $('.entry-grid-select').removeAttr('checked');
                            $("input:checkbox.entry-grid-select").parent("td").parent("tr").find('*').addClass('lighten-5').removeClass('lighten-4');
                        });
                    }
                },
                error: function(e){
                }
            });
        }, 300);
    }

    //set selected parent counter
    function checkSelectedParentCounter(response){
        count = parseInt(response).toLocaleString();
        if(response == 0) {
            count = 'No';
        }
        $('#selected-count').html(count);
    }

    // count items inside a json object which contains selected items in a browser
    function getSelectedCount(jsonObj) {
        return (jsonObj == null) ? 0 : Object.keys(jsonObj).length;
    }

    // parse json string from localStorage and return a json object
    function getSelectedEntries(jsonStringFromLocalStorage) {
        try {
            var entriesObj = JSON.parse(jsonStringFromLocalStorage);
            if (entriesObj && typeof entriesObj === "object") {
                return entriesObj;
            }
        }
        catch (e) { }

        return null;
    }

JS
);
?>

<style>
.btn-flat{
    color: #fff;
}
#entry-list-bulk-seeds-modal .modal-dialog{
    width: 80% !important;
}
#select-seedlot-modal .modal-dialog{
    width: 80% !important;
}
#select-package-modal .modal-dialog{
    width: 80% !important;
}
.tabs-krajee.tabs-left .nav-tabs, .tabs-krajee.tabs-right .nav-tabs {
    width: 151px;
}
.tabs-krajee.tabs-left .tab-content {
    margin-left: 150px;
}
.tab-content{
    height: 480px !important;
}
#from-nursery-container.table-responsive.kv-grid-container,
#copy-occurrence-container.table-responsive.kv-grid-container,
#occurrence-entries-container.table-responsive.kv-grid-container,
#copy-parent-container.table-responsive.kv-grid-container,
#parent-entries-container.table-responsive.kv-grid-container,
#copy-experiment-container.table-responsive.kv-grid-container,
#copy-experiment-entries-container.table-responsive.kv-grid-container,
#saved-list-table-container.table-responsive.kv-grid-container
#harvested-plots-pd-container.table-responsive.kv-grid-container{
    max-height: 380px !important;
}
#select-seed-lot-container.table-responsive.kv-grid-container{
    max-height: 440px !important;
}
#select-single-package-container.table-responsive.kv-grid-container{
    max-height: 440px !important;
}

#input-list-products-view{
  max-height: 300px !important;
  overflow-y: auto !important;
}

#entry-list-bulk-seeds-modal .modal-body{
    overflow-y: auto !important;
    overflow-x: hidden;
}

#entry-list-reorder-modal .modal-body{
    overflow-y: auto !important;
    overflow-x: hidden;
}

.modal-body{
    overflow-y: hidden;
}
.required{
    color:red;
}
#req-vars{
    visibility: visible;
    width: '';
    height: '';
}
.input-field input[type=search]:focus{
    background-color: inherit;
}
.dropdown-content {
    overflow-y: visible;
}
.dropdown-content .dropdown-content {
    margin-left: -100%;
}
</style>

