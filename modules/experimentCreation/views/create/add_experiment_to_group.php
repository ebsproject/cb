<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Renders list of experiments that can be grouped
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\Phase;
use kartik\dynagrid\DynaGrid;
?>

<p><?= \Yii::t('app', 'Below are the experiments created that you can include in the group.') ?></p>

<?php 
        // all the columns that can be viewed
        $columns = [
            ['class' => 'kartik\grid\CheckboxColumn'],
            [
                'attribute'=>'experiment_name',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'year',
                'enableSorting' => false,
            ],
            [
                'header' => \Yii::t('app','Phase'),
                'value' => function ($data) {
                    $phase = Phase::findOne($data->phase_id);
                    return $phase->abbrev;
                },
                'format' => 'raw',
                'enableSorting' => false,
			],
        ];  
        
		echo $grid = GridView::widget([
			'pjax' => true, // pjax is set to always true for this demo
            'dataProvider' => $data,
			'id' => 'select-expt-to-grp',
            'columns' => $columns
        ]);
 ?>
<a id="<?= $experimentGroupId?>" class="grid-select-expts btn-floating btn-small waves-effect waves-light pull-right hidden red"><i class="material-icons">add</i></a>
                                    
<style>
[type="checkbox"]:not(:checked), [type="checkbox"]:checked {
    position: inherit;
    opacity: 1;
    pointer-events: auto;
}
</style>

