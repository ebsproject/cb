<?php
/**
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Preview update product name
 */

use app\models\Program;
use app\modules\experimentCreation\models\ExperimentModel;
use kartik\select2\Select2;
use yii\helpers\Url;

echo Yii::t('app', '<p class="instruction">Select preferred name type below. Please review new designation values before clicking "Confirm".</p>');

//get name types of entries of the study
$resultNameTypes = ExperimentModel::getNameTypesOfEntriesOfStudy($experimentId);

//get default name type from json config
$getDefSql = "
    select c.config_value
    from platform.config c
    where c.abbrev ilike 'name_type_per_program'
    and c.is_void = false;
";
$result = Yii::$app->db->createCommand($getDefSql)->queryAll();
$prog = $module;

$optionStr = '';
if(!empty($result)){
    $configVal = json_decode($result[0]['config_value'], true);
    if(isset($configVal['Values'])){
        if(isset($configVal['Values'][$prog]) && isset($configVal['Values'][$prog]['NAME_TYPE'])){
            $defs = $configVal['Values'][$prog]['NAME_TYPE'];
            foreach($configVal['Values'][$prog]['NAME_TYPE'] as $val){   
                if(in_array($val, $resultNameTypes)){ //check if in name types of entries
                    $optionStr = $optionStr . '<option value="'.$val.'">'.$val.'</option>';
                }
            }
        }else if(isset($configVal['Values']['default'])){
            $defs = $configVal['Values']['default']['NAME_TYPE'];
            foreach($configVal['Values']['default']['NAME_TYPE'] as $val){
                if(in_array($val, $resultNameTypes)){ //check if in name types of entries
                    $optionStr = $optionStr . '<option value="'.$val.'">'.$val.'</option>';
                }
            }
        }
    }
}

$nameTypes = array_diff($resultNameTypes,$defs); //get name types of entries not in name types in config
foreach($nameTypes as $res){
    $optionStr = $optionStr . '<option value="'.$res.'">'.$res.'</option>';
}

$final_array = array_combine($nameTypes, $nameTypes);

echo Select2::widget([
    'data' => $final_array,
    'name' => 'select-name-type',
    'id' => 'name-type-select',
    'maintainOrder' => true,
    'options' => [
        'placeholder' => \Yii::t('app','Select Name Type'),
        'tags' => true,
    ]
]);

$searchDesignationsUrl = Url::to(['create/render-preferred-designations-preview','id'=>$id, 'program'=>$program]);
$updateDesignationsUrl = Url::to(['create/update-preferred-designations','id'=>$id, 'program'=>$program, 'processId'=>$processId]);

?>

<br>
<div id="preview-content">

</div>

<script>

    $(document).ready(function(){

        var productIds = [];
        var nameType = '';

        $('#name-type-select').on('change', function(){

            let val = $(this).val();
            nameType = val;
            $.ajax({
                url: '<?php echo $searchDesignationsUrl; ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    nameType: $(this).val(),
                    productIdArr: '<?php echo json_encode($idArray); ?>'
                },
                success: function (response) {

                    if(response.count > 0){

                        productIds = response.productIds;

                        $('#modify-designations-confirm').removeClass('disabled');
                        $('#modify-designations-confirm').removeAttr('disabled');
                    }

                    $('#preview-content').html(response.htmlData);

                }
            });

        });
        
        $('#modify-designations-confirm').click(function(){
            var getUrlVarsVal = getUrlVars();
            var page = '';
            var perPage = '';
          
            if(typeof page != undefined && (typeof perPage != undefined)){
                page = getUrlVars()['page'];
                perPage = getUrlVars()['per-page'];
            }

            $.ajax({
                url: '<?php echo $updateDesignationsUrl; ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    productIds: productIds,
                    nameType: nameType,
                    page: page,
                    perPage: perPage,
                    pagesParam: '<?php echo $pagesParam;?>',
                    params_temp: '<?php echo $params_temp;?>'
                },
                success: function (response) {

                }
            });

        });

    });
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }
</script>