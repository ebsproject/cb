<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders browser for saved list for entry list selection
 */

use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

use yii\helpers\Html;
?>

<p><?= \Yii::t('app', 'Below are the saved lists that you can use in your entry list. Click the search icon to filter the list.') ?></p>
<?php Pjax::begin();?>

<div id="mod-progress" class="hidden"></div>
<div id="mod-notif" class="hidden"></div>

<div class="input-field no-margin search-entry-field-div" style="width:35%">
  <input id="searchTextSavedList" type="search" placeholder="Type filter" class="autocomplete search-text-add-entry-search-input" autocomplete="off" autofocus="" value="">
  <i class="material-icons search-text-entry-clear-all-search-btn" title="Clear all" style="opacity: 0; margin-right:20px; font-size:1.5rem;margin-top:10px;">clear</i>
  <i class="material-icons search-text-entry-submit-btn" style="margin-top:10px; font-size:1.5rem" title="Search">search</i>
</div>

<?php 
    $actionColumns = [
      [
        'class' => 'yii\grid\SerialColumn',
        'header' => false
      ], 
      [
          'class'=>'kartik\grid\ActionColumn',
          'header' => false,
          'template' => '{select}',
          'buttons' => [
              'select' => function ($url, $data, $key) {
                  return Html::a('<i class="material-icons">exit_to_app</i>',
                      '#',
                      [
                          'class'=>'select-list',
                          'title' => Yii::t('app', 'Select saved list'),
                          'data-saved_list_id' => $key,
                      ]
                  );
              },
          ]
      ],
    ];
    // all the columns that can be viewed in seasons browser
    $columns = [
        [
            'attribute'=>'abbrev',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'name',
            'enableSorting' => false,
        ],
        [
            'header' => Yii::t('app', 'No. of Members'),
            'value' => function ($data) {
                return '<span class="new badge">'.$data['memberCount'].'</span>';
            },
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'header' => Yii::t('app', 'List Type'),
            'value' => function ($data) {
                return ucfirst($data['type']);
            },
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'header' => Yii::t('app', 'List Sub Type'),
            'value' => function ($data) {
                return empty($data['subType']) ? '<span class="not-set">(not set)</span>' : ucfirst($data['subType']);
            },
            'format' => 'raw',
            'enableSorting' => false,
        ],
        [
            'attribute'=>'remarks',
            'enableSorting' => false,
        ],
    ];  

    $gridColumns = array_merge($actionColumns,$columns);
    
    echo $grid = GridView::widget([
      'pjax' => true, // pjax is set to always true for this demo
      'dataProvider' => $data,
      'id' => 'saved-list-table',
      'tableOptions' => ['class' => 'table table-bordered white z-depth-3  search-entry-table'],
      'columns' => $gridColumns,
      'toggleData'=>true,
      'striped'=>false
    ]);
 ?><?php Pjax::end();?>
