<?php
/**
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* Renders the header tab for the experiment creation
*/

use app\components\FavoritesWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\User;

//Add condition since not all steps will be refactored for now
$experimentDbId = isset($model['experimentDbId']) ? $model['experimentDbId'] : ''; 
$experimentName = isset($model['experimentName']) ? $model['experimentName'] : ''; 
$experimentStatus = isset($model['experimentStatus']) ? $model['experimentStatus'] : ''; 
$experimentsUrl = Url::to(['/experimentCreation/create', 'program' => $program]);

if(!empty($experimentDbId)){
    $id = $experimentDbId;
} else {
    $id=null;
}

$userModel = new User();
$userId = $userModel->getUserId();
$filteredIdName = "ec_$userId"."_filtered_entry_ids_$id";
$selectedIdName = "ec_$userId"."_selected_entry_ids_$id";

//destroy selected items session
if(((isset($_GET['reset']) && $_GET['reset'] == 'hard_reset' && !isset($_GET['grid-entry-list-grid-page'])) ||
    (!isset($_GET['reset']) && !isset($_GET['grid-entry-list-grid-page']))) && isset($_SESSION[$selectedIdName])){
    unset($_SESSION[$selectedIdName]);
    if(isset($_SESSION[$filteredIdName])){
        unset($_SESSION[$filteredIdName]);
    }
}

// If current URL points to "Assign Number of Replications to Entries" page, set current URL to "/experimentCreation/create/specify-design"
$currentUrl = '/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
if (Yii::$app->controller->action->id === 'assign-number-of-replications-to-entries' ||
    Yii::$app->controller->action->id === 'selection-entries' || Yii::$app->controller->id.'/'.Yii::$app->controller->action->id === 'design/view-layout') {
    $currentUrl = '/'.Yii::$app->controller->module->id.'/create/specify-design';
}

$processId = isset($processId) ? $processId : '';
$id = isset($id) ? $id : '';

$activities = \Yii::$container->get('app\modules\experimentCreation\models\FormModel')->prepare($currentUrl, $program, $id, $processId);
if(!empty($activities)){
    $activeArr[$activities['active']] = 'active';
    $urlActive[$activities['active']] = '#';
}

if(!empty($activities)){
    $activeArr[$activities['active']] = 'active';
    $urlActive[$activities['active']] = '#';

    if(strpos($activities['active'],'manage-crosslist') !== false){
        $activities['active'] = '/experimentCreation/create/manage-crosses';
    }

    $activeIndex = array_search($activities['active'], array_column($activities['children'], 'url'));
}

$dataUrl = '#';
$nextUrl = '#';

if(!empty($activities)){
    if(($activeIndex < count($activities['children'])-1) && $btnName != 'next-btn'){
        $nextUrl = Url::to([$activities['children'][$activeIndex+1]['url'], 'id'=>$id,'program'=>$program, 'processId'=>$processId]);
    } else if(count($activities['children']) != $activeIndex+1){
        $nextUrl = '#';
        $dataUrl =  Url::to([$activities['children'][$activeIndex+1]['url'], 'id'=>$id,'program'=>$program, 'processId'=>$processId]);
    }
}
$type = isset($type) && !empty($type) ? $type : '';

if(empty($id) && empty($processId)){
    $isDefault = true;
?>
    <h3>
        <?=
            Yii::t('app', 'Create Experiment').
            FavoritesWidget::widget([
                'module' => 'experimentCreation',
                'controller' => 'create'
            ]).' <small><strong>'.$experimentName.'</strong></small>'
        ?>
        <a id="<?=$btnName?>" type="submit" <?= $nextBtn?> href="<?=$nextUrl?>" class="btn btn-primary waves-effect waves-light pull-right" style="margin-right:15px; margin-bottom:10px;"><?= \Yii::t('app', $saveBtnLabel) ?></a>
    </h3>
<?php } elseif(empty($id) && !empty($processId)){
        $isDefault = false;
    ?>
    <h3>
        <?=
            Yii::t('app', 'Create').
            FavoritesWidget::widget([
                'module' => 'experimentCreation',
                'controller' => 'create'
            ]).' <small><strong>'.$experimentName.'</strong></small>'
        ?>
        <a id="<?=$btnName?>" type="submit" <?= $nextBtn?> href="<?=$nextUrl?>" class="btn btn-primary waves-effect waves-light pull-right" style="margin-right:15px; margin-bottom:10px;"><?= \Yii::t('app', $saveBtnLabel) ?></a>
    </h3>
<?php }else{
    $isDefault = false;
    if(!empty($activities)){
    ?>
    <h3>
        <?=
            Html::a(Yii::t('app', 'Experiments'), $experimentsUrl).
            " <small> &raquo; $experimentName (".Yii::t('app', $activities['display_name']).')</small>'
        ?>
        <a id="<?=$btnName?>" type="submit" <?= $nextBtn?> href="<?=$nextUrl?>" data-url="<?=$dataUrl?>" class="btn btn-primary waves-effect waves-light pull-right" style="margin-right:15px; margin-bottom:10px;"><?= \Yii::t('app', $saveBtnLabel) ?></a>
    </h3>
<?php }?>
<?php }?>
<div class="col col-md-12 row" style="z-index: 99">
<ul class="tabs">
    <?php
    if(!empty($activities)){
        for($i=0; $i<count($activities['children']); $i++){
            $disabled=$nextBtn;

            if(empty($id) && empty($processId)){
                $disabled = 'disabled';
            }elseif(empty($id) && !empty($processId)){
                $disabled = 'disabled';
            }else{
                $disabled = $activities['children'][$i]['disabled'];
            }

            if(!isset($id)){
                $url = '#';
                $disabled = 'disabled';
            } else{
                if(isset($activities['children'][$i])){
                    $url = Url::to([$activities['children'][$i]['url'],'id'=>$id,'program'=>$program, 'processId'=>$processId]);
                }
            }

            $tabId = '';
            if(isset($activities['children'][$i]) && $activities['children'][$i]['abbrev'] == 'EXPERIMENT_CREATION_EXPT_GROUP_ACT'){
                $tabId = 'experiment_group_tab_id';
            }

            if($disabled == 'disabled'){
                $url = Url::to([$activities['children'][$i]['url'],'id'=>$id,'program'=>$program, 'processId'=>$processId]);
            }

            $active = '';

            if(isset($activities['children'][$i]) && ($activities['children'][$i]['url'] == $currentUrl || in_array( $currentUrl, $activities['children'][$i]['child_url']))){
                $active = 'active';
            }

            if($isDefault == true && strpos($activities['children'][$i]['url'], 'specify-basic-info') !== false){
                $active = 'active';
                $disabled = false;
                $url = Url::to([$currentUrl,'program'=>$program]);
            }

            if(isset($activities['children'][$i])){
                echo '<li class="tab col '.$disabled.'" title="'.Yii::t('app', $activities['children'][$i]['name']).'" id="'.$tabId .'">
                    <a href="'.$url.'" id="section-'.$activities['children'][$i]['order_number'].'" class="'.$active.' a-tab '.$disabled.' "><span class="badge-step" style="padding:4px 7px;">'.($i+1).'</span>'.Yii::t('app', $activities['children'][$i]['display_name']).'</a>
                    </li>';
            }
        }
    }
    ?>
</ul>
</div>

<?php
$parsedExperimentStatus = explode(';',$experimentStatus);
$latestStatus = end($parsedExperimentStatus);
$finalStatus = (in_array($latestStatus,['created', 'mapped', 'planted', 'planted;crossed'])) || (strpos($latestStatus,'in progress') !== false || strpos($latestStatus,'in queue') !== false) ? 'true' : 'false';
$browserUrl = Url::to(['create/redirect-to-index','id' => $id, 'program' => $program, 'latestStatus' => $latestStatus]);
if(isset($model['creatorDbId'])){
    $renderAccessUpdate = Yii::$app->access->renderAccess("EXPERIMENT_CREATION_UPDATE","EXPERIMENT_CREATION", $model['creatorDbId'], usage:'url');
} 
$this->registerJs(<<<JS
    $("document").ready(function(){
        var status = '$finalStatus';
        var experimentStatus = '$experimentStatus';

        if(status === 'true'){
            window.location.href = '$browserUrl';
        }

        if(experimentStatus.includes('design requested')){
            var currentUrl = '$currentUrl';
            if(!currentUrl.includes('specify-design')){
                window.location.href = '$browserUrl';
            }
        }

    });
    //reset if tool is accessed via favorites and recently used tools modals
    $(document).on('click', '.a-tab', function(e) {
        var actionLink = $(this).attr("href");
        var currId = this.id;
        if(actionLink != '#'){
            removeActiveTab(currId);
            window.location.href = actionLink;
        }
    });

    var defaultTab =  '$isDefault';

    function removeActiveTab(currId){

        $('.a-tab').each(function () {
            if(currId != this.id){
                this.classList.remove("active");
            }
        });
    }

JS
);
?>

<style>
.disabled{
    pointer-events: none;
}

.badge-step {
    background-color: #ee6e73;
    display: inline-block;
    font-size: 11.84px;
    font-weight: 700;
    margin-right: 5px;
    line-height: 14px;
    color: #fff;
    vertical-align: baseline;
    white-space: nowrap;
    text-shadow: 0 -1px 0 rgba(0,0,0,.25);
    -webkit-border-radius: 50%;
}
</style>
