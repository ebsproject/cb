<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Renders review tab for experiment creation
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\bootstrap\Modal;


$disabled = '';
$btnName = 'next-btn';
$saveBtnLabel = 'Complete Creation';

if (strpos($model['experimentStatus'],'occurrences created') === false || strpos($model['experimentStatus'],'entry list incomplete seed sources') !== false ) {
    $disabled = 'disabled';
}

echo Yii::$app->controller->renderPartial('@app/modules/experimentCreation/views/create/tab_main.php',[
    'processId' => $processId,
    'model'=>$model,
    'program'=>$program,
    'saveBtnLabel'=>$saveBtnLabel,
    'nextBtn'=>$disabled,
    'btnName' => $btnName
]);

if($packageSourceNote != ''){
    echo '<br><br><br><div class="notif alert-warning" style="padding:10px;margin-right:5px;margin-top:0px;">'.$packageSourceNote.'</div>';
}
if($plotPlantRecordNote != ''){
    if($packageSourceNote == ''){
        echo '<br><br>';
    }
    echo '<br><div class="notif alert-warning" style="padding:10px;margin-right:5px;margin-top:0px;">'.$plotPlantRecordNote.'</div>';
}
echo '<p>'.Yii::t('app', 'Review and complete the experiment creation process.').'</p>
      <div class="col col-sm-12 view-entity-content">';

$timeStamp = date("Y-m-d_H-i-s");
$experimentName = preg_replace("![^a-z0-9]+!i", "_", $model['experimentName']);
$entryfilename = $experimentName.' '.ucwords($displayName)." $timeStamp";
$crossfilename = $experimentName." Cross List $timeStamp";
$occurrencefilename = $experimentName." Occurrence List  $timeStamp";
// Parent/Entry List
if(!empty($entryDataProvider)){
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'entryNumber',
            'label' => '<span title="Sequential numbering of the entries">'.Yii::t('app', 'Entry No.').'</span>',
            'format' => 'raw',
            'encodeLabel'=>false,
        ],     
        [
            'attribute'=>'seedName',
            'label' => '<span title="Seed code">'.Yii::t('app', 'Seed name').'</span>',
            'format' => 'raw',
            'encodeLabel'=>false,
        ],
        [
            'attribute'=>'designation',
            'label' => '<span title="Gemplasm">'.Yii::t('app', 'Germplasm Name').'</span>',
            'format' => 'raw',
                'format' => 'raw',                
            'format' => 'raw',
            'encodeLabel'=>false,
        ],
    ];

    $gridColumns = array_merge($gridColumns,$configReviewColumns);

    $export = ExportMenu::widget([
        'dataProvider' => $entryDataProvider,
        'columns' => $gridColumns,
        'filename' => $entryfilename,
        'fontAwesome' => true,
        'exportConfig' => [
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::JSON => FALSE,
        ],
        'showColumnSelector' => false,
        'asDropdown' => true,
        'showConfirmAlert' => false
    ]);

    //dynagrid configuration
    DynaGrid::begin([
        'columns' => $gridColumns,
        'theme'=>'simple-default',
        'showPersonalize'=>true,
        'storage' => 'cookie',
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions'=>[
            'id' => 'grid-review-entry-list',
            'dataProvider' => $entryDataProvider,
            'showPageSummary' => false,
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => ['id' =>'grid-review-entry-list'],
                'beforeGrid' => '',
                'afterGrid' => ''
            ],
            'responsiveWrap'=>false,
            'panel' => [
                'heading' => false,
                'before' => \Yii::t('app', "This is the browser for the $displayName.{summary}"),
                'after' => false,
            ],
            'toolbar' => [
                ['content' => '&nbsp;'],//just for the space between summary and export button
                // 'exportConfig' => $export,   // temporary comment until export all is solved
            ],
            'floatHeader' => true,
            'floatOverflowContainer' => true,
            'resizableColumns' => true,
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ],
        ],
        
        'options'=>['id'=>'dynagrid-review-entry-list'] // a unique identifier is important
    ]);
    
    DynaGrid::end();
}
echo '<br/>';
// Cross List
if(isset($crossDataProvider)){
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'crossName',
            'format' => 'raw',
            'vAlign'=>'middle',
            'value' => function($data){
                return !empty($data['crossName']) ? $data['crossName'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'crossFemaleParent',
            'label' => '<span title="Female Parent"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENT').'</span>',
            'encodeLabel' => false,
            'format' => 'raw',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'vAlign'=>'middle',
            'value' => function($data){
                return !empty($data['crossFemaleParent']) ? $data['crossFemaleParent'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'femaleParentage',
            'label' => '<span title="Female Parentage"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENTAGE').'</span>',
            'encodeLabel' => false,
            'format' => 'raw',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'vAlign'=>'middle',
            'value' => function($data){
                return !empty($data['femaleParentage']) ? $data['femaleParentage'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'crossMaleParent',
            'label' => '<span title="Male Parent">'.Yii::t('app', '<i class="fa fa-mars"></i> MALE PARENT').'</span>',
            'encodeLabel' => false,
            'format' => 'raw',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'vAlign'=>'middle',
            'value' => function($data){
                return !empty($data['crossMaleParent']) ? $data['crossMaleParent'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'maleParentage',
            'label' => '<span title="Male Parentage"><i class="fa fa-mars "></i>'.Yii::t('app', ' MALE PARENTAGE').'</span>',
            'encodeLabel' => false,
            'format' => 'raw',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'vAlign'=>'middle',
            'value' => function($data){
                return !empty($data['maleParentage']) ? $data['maleParentage'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'crossMethod',
            'label' => Yii::t('app','METHOD'),
            'format' => 'raw',
            'encodeLabel' => false,
            'vAlign'=>'middle',
            'value' => function($data){
                return !empty($data['crossMethod']) ? $data['crossMethod'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'crossRemarks'
        ],
        [
            'attribute' => 'femaleParentEntryNumber',
            'label' => '<span title="Female Parent Entry number"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENT ENTRY NUMBER').'</span>',
            'encodeLabel' => false,
            'format' => 'raw',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'vAlign'=>'middle',
            'value' => function($data){
                return !empty($data['femaleParentEntryNumber']) ? $data['femaleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'maleParentEntryNumber',
            'label' => '<span title="Male Parent Entry number"><i class="fa fa-mars"></i>'.Yii::t('app', ' MALE PARENT ENTRY NUMBER').'</span>',
            'encodeLabel' => false,
            'format' => 'raw',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'vAlign'=>'middle',
            'value' => function($data){
                if($data['crossMethod'] == 'selfing'){
                    return !empty($data['femaleParentEntryNumber']) ? $data['femaleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
                } else return !empty($data['maleParentEntryNumber']) ? $data['maleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'femaleParentSeedSource',
            'label' => '<span title="Female Seed Source"><i class="fa fa-venus"></i>'.Yii::t('app', 'FEMALE SEED SOURCE').'</span>',
            'format' => 'raw',
            'encodeLabel' => false,
            'vAlign'=>'middle',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'value' => function($data){
                return !empty($data['femaleParentSeedSource']) ? $data['femaleParentSeedSource'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'maleParentSeedSource',
            'label' => '<span title="Male Seed Source"><i class="fa fa-mars"></i>'.Yii::t('app', 'MALE SEED SOURCE').'</span>',
            'format' => 'raw',
            'encodeLabel' => false,
            'vAlign'=>'middle',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'value' => function($data){
                return !empty($data['maleParentSeedSource']) ? $data['maleParentSeedSource'] : '<span class="not-set">(not set)</span>';
            }
        ]
    ];

    $export = ExportMenu::widget([
        'dataProvider' => $crossDataProvider,
        'columns' => $gridColumns,
        'filename' => $crossfilename,
        'fontAwesome' => true,
        'exportConfig' => [
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::JSON => FALSE,
        ],
        'showColumnSelector' => false,
        'asDropdown' => true,
        'showConfirmAlert' => false
    ]);

    //dynagrid configuration
    DynaGrid::begin([
        'columns' => $gridColumns,
        'theme' => 'simple-default',
        'showPersonalize' => true,
        'storage' => 'cookie',
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions'=>[
            'id' => 'grid-review-cross-list',
            'dataProvider' => $crossDataProvider,
            'showPageSummary' => false,
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => ['id' =>'grid-review-cross-list'],
                'beforeGrid' => '',
                'afterGrid' => ''
            ],
            'responsiveWrap'=>false,
            'panel' => [
                'heading' => false,
                'before' => \Yii::t('app', "This is the browser for the cross list.{summary}"),
                'after' => false,
            ],
            'toolbar' => [
                ['content' => '&nbsp;'],//just for the space between summary and export button
                // 'exportConfig' => $export,
            ],
            'floatHeader' => true,
            'floatOverflowContainer' => true,
            'resizableColumns' => true,
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ],
        ],
        'options'=>['id'=>'dynagrid-review-cross-list'] // a unique identifier is important
    ]);
    
    DynaGrid::end();
}

// Occurrence List
$gridColumns = [
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function ($model, $key, $index, $column){
            return Yii::$app->controller->renderPartial('_expand-occurrence', [
                'occurrenceDbId' => $model['occurrenceDbId']
            ]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => false,
        'noWrap' => true,
        'template' => '{export}',
        'buttons' => [
            'export' => function ($url, $data, $key) {                   
                return Html::a('<i class="material-icons  inline-material-icon">file_download</i>',
                    '#',
                    [
                        'class' => 'export-occurrence',
                        'title' => Yii::t('app', 'Export Occurrence'),
                        'data-id' => $data['occurrenceDbId'],
                        'data-target' => '#'
                    ]
                );
            }
        ]
    ],
    [
        'attribute' => 'occurrenceName',
        'enableSorting' => false
    ],
    [
        'attribute'=>'occurrenceCode',
        'label' => '<span title="Occurrence Code">'.Yii::t('app', 'Code').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
        'enableSorting' => false
    ],
    [
        'attribute' => 'site',
        'enableSorting' => false
    ],
    [
        'attribute' => 'field',
        'enableSorting' => false
    ],
    [
        'attribute'=>'experimentStageCode',
        'label' => '<span title="Stage Code">'.Yii::t('app', 'Stage').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
        'enableSorting' => false
    ],
    [
        'attribute'=>'experimentYear',
        'label' => '<span title="Experiment Year">'.Yii::t('app', 'Year').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
        'enableSorting' => false
    ],
    [
        'attribute'=>'experimentSeason',
        'label' => '<span title="Experiment Season">'.Yii::t('app', 'Season').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
        'enableSorting' => false
    ],
    [
        'attribute' => 'entryCount',
        'enableSorting' => false
    ],
    [
        'attribute' => 'plotCount',
        'enableSorting' => false
    ]
];

$export = ExportMenu::widget([
    'dataProvider' => $occurrenceDataProvider,
    'columns' => $gridColumns,
    'filename' => $occurrencefilename,
    'fontAwesome' => true,
    'container' =>[
        'class' => 'pull-right',
    ],
    'exportConfig' => [
        ExportMenu::FORMAT_PDF => false,
        ExportMenu::JSON => FALSE,
    ],
    'showColumnSelector' => false,
    'asDropdown' => true,
    'showConfirmAlert' => false
]);

echo $grid = GridView::widget([
    'dataProvider' => $occurrenceDataProvider,
    'id' => 'view-occurrences-table',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => $gridColumns,
    'export' => false, 
    'panel'=>[
        'heading'=>false,
        'before'=> \Yii::t('app', '<span class="new badge">NOTE</span> &nbsp;This is the browser for the created occurrences. These are <b>not yet visible</b> in the '.Html::a('Experiment Manager browser', ['/occurrence', 'program' => $program]).'. Click <b>COMPLETE CREATION</b> button to make the occurrences active. {summary}'),
        'after' => false,
    ],
    'toggleData'=>true,
    'toolbar' => [
        ['content' => '&nbsp;'],//just for the space between summary and export button
        // 'exportConfig' => $export,
    ]
]);

echo '</div>';

//modal for record generation
Modal::begin([
    'id' => 'show-status-modal',
    'header' => Yii::t('app', '<h4 id="gen-modal-header">Complete Creation of Experiment</h4>'),
    'footer' => Html::a(\Yii::t('app','Close'),
            ['#'],
            [
                'class'=>'btn btn-primary waves-effect waves-light',
                'id'=>'cancel-save-status-btn',
                'data-dismiss'=>'modal'
            ]).'&emsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
]);

echo '<div id="message-status"></div>';
echo '<div id="loading-indicator" class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
echo '<div id="message-notif" class="hidden"></div>';
echo '<br/>';

Modal::end();
$finalizeUrl = Url::to(['create/finalize-experiment', 'id'=>$id, 'program'=>$program]);

// export mapping files url
$exportPlotsUrl = Url::to(['/occurrence/view/export-plots']);
$prepareDataForCsvDownloadUrl = Url::to(['/occurrence/view/prepare-data-for-csv-download', 'program'=>$program,'entity'=>'plot', 'experimentId'=>$id, 'tool'=>'experiment-creation']);

$this->registerJs(<<<JS
    var warning = '$warning';
    var packageSourceNote = '$packageSourceNote';
    var plotPlantRecordNote = '$plotPlantRecordNote';
    
    if(warning !== ''){
        $('#next-btn').attr('disabled', true);
        var hasToast = $('body').hasClass('toast');			

        if(!hasToast){
            $('.toast').css('display','none');
            var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> "+ warning +" records are missing.</span>";
            Materialize.toast(notif, 5000);
        }
    }else{
        $('#next-btn').attr('disabled', false);
    }

    if(packageSourceNote !== '' ){
        $('#next-btn').attr('disabled', true);
    }

    if(plotPlantRecordNote !== '' ){
        $('#next-btn').attr('disabled', true);
    }

    $("#next-btn").click(function() {
        $('#system-loading-indicator').html('<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>');

        $.ajax({
            url: '$finalizeUrl',
            type: 'post',
            data:{},
            beforeSend: function (response) {
                $('#gen-modal-header').html('<i class="fa fa-bell"></i> Notification');
                $('#show-status-modal').modal('show');
                message = '<h6><em>Please wait while completing the creation of your experiment.<em></h6><br>'
                $('#message-status').html(message);
                $('#system-loading-indicator').html('');
                $('#cancel-save-status-btn').attr('disabled', 'disabled');
                var hideCloseBtn = '#show-status-modal > .modal-dialog > .modal-content > button.close';
                $(hideCloseBtn).addClass('hidden');
            },
            success: function(response) {
                location.reload();
            },
            error: function() {
            }
        });
    });

    $('.export-occurrence').off('click').on('click', function (e) {
        e.preventDefault()
        var occurrenceDbId = $(this).attr('data-id')
        if ($plotCount >= $exportRecordsThresholdValue) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '$prepareDataForCsvDownloadUrl'+'&occurrenceId='+occurrenceDbId,
                data: {},
                success: async function (ajaxResponse) {},
                error: function (xhr, error, status) {}
            })
        } else {
            let exportPlotsUrl = '$exportPlotsUrl'
            let expPlotUrl = window.location.origin + exportPlotsUrl + '?program=' + '$program' + '&id=' + occurrenceDbId

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
            $('.toast').css('display','none')
            Materialize.toast(message, 5000)

            window.location.replace(expPlotUrl)

            $('#system-loading-indicator').html('')
        }
    })

JS
);
?>